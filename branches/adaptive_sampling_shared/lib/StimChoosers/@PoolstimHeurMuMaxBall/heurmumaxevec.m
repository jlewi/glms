%function stimpool=heurmumaxevec(sobj,post,magcon)
%   sobj - object
%   post - posterior
%   magcon -magcon
%Explanation: This heuristic takes the mean and a random vector in the
%eigenspace of the max eigenvector. We than form a basis from this two
%vectors and take a random combination of these vectors. 
%
%Note we set the stimulus magnitude= magnitude constraint so this may not
%be ideal for nonlinearities other than Poissexp
function stimpool=heurmumaxevec(sobj,post,mobj)
magcon=getmag(mobj);
%get the mean of just the stimulus terms
fullpostm=getm(post);
mustim=fullpostm(1:getstimlen(mobj),1);

%normalize the mean
mustim=normmag(mustim);

nstim=getnstim(sobj);

%get the maximum eigenvectors
efull=getefull(post);
[maxeigd mind]=maxeval(efull);

%max eigenvector
vmax=getevecs(efull,mind);


%if the maxeigenvector has a multiplicity greater than 1 take random linear
%combinations of the vectors in this eigenspace
%generating these random numbers will be expensive when the multiplicity is
%high (i.e on early trials)
if (length(mind)>1)
    bpts=normrnd(zeros(length(mind),nstim),ones(length(mind),nstim));
    bpts=normmag(bpts);
    %form a vector by multiplying bpts by the maximum eigenvector
    vbasis=vmax*bpts;
    
    %make these vectors orthogonal to the mean
    vbasis=vbasis-mustim*(mustim'*vbasis);
else    
    vbasis=vmax;
    %make it orthogonal to the mean
    vbasis=vbasis-mustim*(mustim'*vbasis);
    
    vbasis=vbasis*ones(1,nstim);
    
end
%normalize vbasis
vbasis=normmag(vbasis);

%remove from vbasis projection in direction mustim
vbasis=vbasis-mustim*(mustim'*vbasis);


%mustim and vbasis are orthonormal 
%randomly generate magnitude in direction of the mean
mumag=rand(1,nstim)*2*magcon-magcon;

%randomly generate magnitude and sign in direction of vbasis
%energy can be a number between 0 and (magcon^2-mumag);
vmaxmag=(magcon^2-mumag.^2).^.5;

vmag=rand(1,nstim)*2.*vmaxmag-vmaxmag;


stimpool=vbasis.*(ones(getstimlen(mobj),1)*vmag);
stimpool=stimpool+mustim*mumag;



