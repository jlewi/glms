%function t=isglmspecific(obj)
%
% Return value
%          1 - if the object for picking stimuli is dependent on a specific GLM
%          0 - if the object is not dependent on a specific GLM
function t=isglmspecific(obj)
    t=obj.glmspecific;