%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
%
%Revision: 12-26-2008
%   first version of the class to use Matlab's new oop model. We may need
%   to modify this load function before we will be able to load objects
%   which used the old MCOS model. 
%
function obj=loadobj(lobj)

     
cname='StimChooserObj';

%indicates whether we have already performed a copy
%copying the fields of lobj to obj when lobj is a structure
%because we only want to do the copying once
cstruct=false;

%get the latest version
%save the latest version so we can check to make sure all
%conversions have been completed.
eval(sprintf('newobj=%s();',cname));

latestver=newobj.version.(cname);

if isstruct(lobj)
    obj=newobj;
    if isfield(lobj,'version')
    obj.version.(cname)=lobj.version(cname);
    else
        %object is old version before we convert to new object model
       obj.version.(cname)=0; 
    end
else
    %we don't need to create a new object
    obj=lobj;
end



%******************************************************************
%Sequentially convert one version of the object to the next
%until we get to the latest version
%
%Warning: If lobj is a structure then we haven't copied the data from lobj
% to obj yet.
%******************************************************************
%object is old version before we converted to new matlab oop model
%we just need to copy the fields
if (obj.version.(cname) <= 081226)
    %do conversion
    %copy fields from old version to new version
    %if lobj is a structure
    if (isstruct(lobj) && ~cstruct)
        fskip={};    %fields not to copy
        obj=copyfields(lobj,obj,fskip);
        cstruct=true;
    end
    obj.version.(cname)=081226;
end

%check if converted all the way to latest version
if (obj.version.(cname) <latestver)
    error('Object has not been fully converted to latest version. \n');
end

%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
%
%Copy fields must be declared within loadobj because otherwise it won't
%have permssion to set the private fields of the object.
function dest=copyfields(source,dest,skip)
%we just need to copy the fields
%and handle any special cases if required
fnames=fieldnames(source);

%struct for object
sobj=struct(dest);
for j=1:length(fnames)
    switch fnames{j}
        case skip
            %do nothing we skip this field
        otherwise
            %set the new field this will cause an error
            %if the field isn't a member of the new object
            dest.(fnames{j})=source.(fnames{j});
    end
end

