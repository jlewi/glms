%function [stim,varargout]=choosestim(obj,simobj,post)
%   obj - the stimchooserobj
%   simobj - the simulation object
%   post the posterior
%
% Explanation this is the function that gets called to choose the stimulus
%   method for choosing the stimulus actually depends on the object called.
%
% Revisions:
%   12-30-2008 - changed the signature fore the choosestim function
%
function [stim,varargout]=choosestim(obj,simobj,post)
error('choosestim should not be called on StimChooserObj but one of its child classes.');

