%function [stim,varargout]=choosestim(obj,simobj,post)
%   obj - the stimchooserobj
%   simobj - the simulation object
%   post the posterior
% Explanation: Select the stimulus by sampling the Gaussian process.
%
function [stim]=choosestim(obj,simobj,post)

mobj=simobj.mobj;
%we need to get past values of the stimuli if they are needed
%we then compute the appropriate Gaussian conditioning formulas. 
if (mobj.ktlength>1)
    tlast=simobj.niter;
    if (simobj.niter-(mobj.ktlength-1)>=0)
        sr=simobj.sr(tlast-(mobj.ktlength-1)+1:tlast);
        spast=getinput(sr);
        spast=getData(spast);
    else
        if (tlast>0)
        sr=simobj.sr(1:tlast);
        spast=getinput(sr); 
        spast=getData(spast);
        else
            spast=0;
        end
    end
    
    %zero padd spast with zeros for any stimuli which are missing
    spast=[zeros(mobj.klength,(mobj.ktlength-1) -size(spast,2)) spast];
    
    %convert to a vector;
    spast=spast(:);
    
    
    %compute the conditional distribution
    a=obj.gp.mus(1:end-mobj.klength);
    b=obj.gp.mus(end-mobj.klength+1:end);
    
    A=obj.gp.cs(1:end-mobj.klength,1:end-mobj.klength);
    B=obj.gp.cs(end-mobj.klength+1:end,end-mobj.klength+1:end);
    C=obj.gp.cs(1:end-mobj.klength,end-mobj.klength+1:end);

    
    invA=inv(A);
    smu=b+C'*invA*(spast-a);
    svar=B-C'*invA*C;
    
    stim=mvgausssamp(smu,svar);
    
    
else
   error('Need to write code to just sample the gaussian distribution'); 
end

