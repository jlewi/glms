%function [obj,optstim]=optbatchinwav(obj,simobj,windex,post)
%   obj - The stimulation chooser object
%   simobj - the simulation object
%   windex - the index of the wave file we are searching for the optimal
%          stimulus
%   post   - the posterior object
% Explanation - this function constructs  a pool of psize stimuli
%       from the trials corresponding to wavefile wfile
%       We then select from this pool the optimal stimulus
%Return value:
%  	opstim.windex - windex - wave file
%   optstim.repeat - which repeat of this wavefile
%   opstim.trial - which trial of this wavefile
%   opstim.mi -
%
%
%
%Explanation: Computes the mutual information.
%
%Revisions:
%   08282008 - Set the mutual information to nan if no batches are left
function [optstim]=optbatchinwav(obj,simobj,windex,post)


mobj=simobj.mobj;

bdata=getbdata(obj.bspost);


%    wcount=wcount+1;
%fprintf('BSlogpost:labindex=%d compllike  file index %d of %d wave files \n',labindex,windex,length(windexes));


%to compute the mutual information we need to compute
%muproj and sigmaproj
%compute muproj and sigmaproj setting spike history to zero.
[muproj]=compglmprojnoshist(obj.bspost,windex,mobj,getm(post));

%since spike history is zero muproj will be the same for all repeats
%because the inputs are the same across repeats
muproj=muproj(1,:);

[ntrials,cstart]=getntrialsinwave(bdata,windex);


%get the stimuli b\c we need to comput sigma for each input
repeat=1;
[stim]=gettrialwfilerepeat(bdata,windex,repeat*ones(1,ntrials),[1:ntrials],mobj);
inp=projinpvec(mobj,stim,zeros(getshistlen(mobj),ntrials));


sigma=sum(inp.*rotatebyc(post,inp));

%compute the mutual information for each individual stimulus in the pool
%PoissExpCompMi computes the logarithm so we have to raise it to the
%exponential power before summing across stimuli in the batch
%this is the same for all repeats because we set spike history to zero.
stimlogmi=complogminfo(obj.miobj, muproj,sigma);

optrepeat=struct('mi',zeros(1,obj.nrepeats),'ntrial',zeros(1,obj.nrepeats));

%NOTE: updating pasttrials is handled by BSBatchBase:choosetrial

for repeat=1:obj.nrepeats
   
    rtrial=getrpasttrial(obj,windex,repeat,simobj);
   
    [optrepeat.maxmi(repeat),optrepeat.ntrial(repeat)]=optbatchinrepeat(obj,rtrial,stimlogmi);
end

%select the optimal batch across repeats
[maxmi ropt]= max([optrepeat.maxmi]);

%if maxmi is nan then there are no batches in this wave file
%containing bsize stimuli
if ~isnan(maxmi)
    optstim.windex=windex;
    optstim.repeat=ropt;
    optstim.ntrial=optrepeat.ntrial(ropt);
    optstim.mi=maxmi;
else
    %no stimuli left in this file
    optstim.mi=nan;
    optstim.windex=windex;
    optstim.repeat=nan;
    optstim.ntrial=nan;
    
end


%function optbatchinrepeat(stimindr,stimlogmi)
%   stimindr - a logical array 1xntrials
%            - a value of true means that stimulus has already been
%            presented
%   stimlogmi - the mutual information for each individual stimulus
%               from this batch
function [maxmi,mind]=optbatchinrepeat(obj,paststim,stimlogmi)

%set the mutual information for any trials which we've already presented to
%-inf
stimlogmi(logical(paststim))=nan;
ntrials=length(stimlogmi);

%we need to raise the mutual information to the exponential power
%and then add the mutual information for all stimuli in a batch
%its possible this will cause overflow  (i.e exp(stimlogmi) is inf)
%We handle overlow by using multiple precision objects to compute
%exp(stimlogmi) with higher precision.
%
%stimlogmi= \stim^ \mu +1/2 \stim^t c_t \stim
%thus stimlogmi could be very large (uncertainty is large or stim has large
%magnitude). This will cause exp(stimlogmi) to explode
%we don't simply want to subtract the largest value of stimlogmi from all
%values, because if stimlogmi is small number we will end up forcing all
%stimulilogmi to be essentially zero.
%a better way to do it is to represent large numbers using higher precision

stimmi=exp(stimlogmi);

%********************************************************************
%Select the batch of stimuli with the largest value for the lower bound
%The value of the lower bound is just the sum of the information
%from each of the stimuli
%so we compute the value by convolving with a filter of ones

%compute how many overlapping batches we can get from this wave file
nbatches=ntrials-obj.bsize+1;
bmi=conv(stimmi,ones(1,obj.bsize));
bmi=bmi(obj.bsize:obj.bsize+nbatches-1);

%**********************************************************************
%handle overflow
%**********************************************************************

%find all entries which mi is infinite
indinf=find(isinf(bmi));

if ~isempty(indinf)
    ninf=length(indinf);
    
    omi=mp(zeros(ninf,1),obj.miobj.nbits);
    for ind=1:ninf
        %the first stimulus in this batch.
        startind=indinf(ind);

        %to speed things up, if the start indexes of the batches are
        %consectuive then we use omi(ind-1) to compute omi(ind) recursively
        if (ind>1 && startind==indinf(ind-1)+1)
            endind=startind+obj.bsize-1;
            smilog=mp([stimlogmi(startind-1) stimlogmi(endind)],obj.miobj.nbits);

            emi=exp(smilog);
            omi(ind)=omi(ind-1)-emi(1)+emi(2);
        else
            %represent the log of the mutual information for each stimulus
            %using a multiple precision object.
            %How many bits should we use? use 64*3=192 - 3 times as many as a double
            smilog=mp(stimlogmi(startind:startind+obj.bsize-1),obj.miobj.nbits);

            omi(ind)=sum(exp(smilog));
        end
    end

    if any(isinf(omi))
        me=MException('CompMI:MIInf','The mutual information for some of the batches is infinite. Most likely you just need to increase the number of bits of precision.');
            throw(me);
    end


    %find the maximum;
    %clearly the maxiiimum must be one of the batches for which the mutual
    %information overflowed. However, its possible the stimulus for which the
    %mutual information overflowed is not in a valid batch (i.e the other
    %stimuli in the batch have already been presented)
    [maxmi, maxindinf]=max(omi);
    if isnan(maxmi)
        %the optimal stimulus is in one of the matches for which the mutual
        %information did not overflow

        [maxmi,mind]=max(bmi);
    else
        mind=indinf(maxindinf);
    end
else

    %find the maximum;
    [maxmi,mind]=max(bmi);
end





