%function stim=choosestim(finf,simobj, post)
%       finfo - the object
%       simobj - the simulation object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3 
%Explanation: maximize the mutual information for the canonical poisson
%model
%
%Revisions:
%   09-19-2009: revise for new calling syntax
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,simobj,post)
oreturn=[];

%we want to maximize the stimuli
%check if we are ignoring shistory terms or if no spike history
%terms are provided.
if ((sobj.ignoreshist==0) &&(getshistlen(simobj.mobj) >0) )
    %[sobj,xpts]=fonucirc(post,shist,mparam.mmag);
    %[fopt,ind]=max(sobj);
    %xmax=xpts(:,ind);

    %einfo is nonempty only if were able to calculate the
    %eigenvectors as in the newton1d case where the
    %eigenvectors are just the rank1 symettric update problem
    %otherwise einfo is empty and fishermax just performs an
    %svd

    %einfo should be the eigenvectors of just the Ck terms
    %[xmax,fopt,einfo]=poissexpmaxmi(post,shist,mparam.mmag,einfo,simparam.optparam);


    %pad shist if its less then the length
    shist=[shist;zeros(getshistlen(mparam)-length(shist),1)];
   [xmax,muprojopt,fopt,ftime,fcase,qobj]=poissexpmaxmi(sobj,post,shist,simobj.mobj);
        %error('Should above line be postnostim or post?')


    %                [xmax,muporjopt,fopt,ftime,srdata.fcase(tr-1)]=poissexpmaxmi(post,shist,einfo,mparam,simparam.finfofunc,simparam.optparam);
    oreturn.searchtime=ftime;
    oreturn.muprojopt=muprojopt;


else
    %when we optimize the stimulus we ignore the spike history
    %coefficents, either because there are no spike history coefficients coefficents
    %or because we want to ignore them
    m=getm(post);
    c=getc(post);
    klength=getklength(simobj.mobj);
    if (klength ==getparamlen(simobj.mobj))
        postnostim=post;
    else
        postnostim=GaussPost('m',m(1:klength,1),'c',c(1:klength,1:klength));
    end
    %[xmax,fopt,einfo,eigtime,searchtime]=fishermax(postnostim,mparam.mmag,einfo,simparam.optparam);
    shist=[];


%     if (getshistlen(simobj.mobj)==0)
%         eigck=getefull(post);
%     else
%         error('Have not checked this to make sure its correct');
%     end
    [xmax,muproropt,fopt,ftime,fcase,qobj]=poissexpmaxmi(sobj,postnostim,shist,simobj.mobj);
    oreturn.searchtime=ftime;
end



if (isnan(fopt))
    error('Fisher information is nan');
end

if (isinf(fopt))
    error('fisher information is infinite');
end

%toptimize=toc;


%check to make sure xmax is valid
if (sum(isnan(xmax))>0)
    error('Optimal stimulus is invalid');
end


if (sum(imag(xmax))~=0)
    error('optimal stimulus is not real');
end

if (nargout>3)
    %return the quad object
    varargout{1}=qobj;
end