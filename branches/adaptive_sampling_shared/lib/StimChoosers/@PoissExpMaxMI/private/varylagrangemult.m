%function [xopt,muprojopt,lfopt,timing,fcase]=varylagrangemult(muprojr,quad,objfunc,mukmag,mparam,optim)
%
%
%
%
%Explanation: compute the maximum by varying the lagrange multiplier.
%
%Return value:
%    timing.slambda - This is the time required to find a value of lam>emax which
%           maximizes the mutual information.
%     timing.spcon   - THe time to find the solution which maximizes the mutual
%                inforation at lam=emax
%     timing.fisheramx - total time for finding the solution which maximizes
%                   the mutual information.
%**************************************************************************
function [xopt,muprojopt,lfopt,timing,fcase]=varylagrangemult(muprojr,quad,objfunc,mukmag,mparam,optim)
%**************************************************************************
%Find the max
%*********************************************************************
%Have to consider to disjoint intervals
%   i) max occurs for lambda>emax
%           we handle this by doing search over lambda
%   ii) max occurs with lambda=emax
%           handle this by doing search over range of pcon values
%            corresponding to lambda=emax

%define the function to pass to fminbind

%*******************************************************************
%case 1) search over lambda>emax
%to speed things up we call maxquadla once to get all the internal
%quantities it will use on repeated calls so it doesn't have to recompute
%them value of v is irrelavant for this call
tic;
fmaxobj=@(v)(funcformaxv(quad,v,getmag(mparam),mukmag));
[vopt vlfopt exitflag voutput] = fminbnd(fmaxobj,0,1,optim);
timing.slambda=toc;
vlfopt=-vlfopt; %flip sign because we took negative to find min

%case 2) search over range of pcon corresponding to lambda=emax
%determine the range of possible pcon corresponding to lambda=emax
tic;
pconrange=pconsingmax(quad,getmag(mparam));
fmaxobj=@(pcon)(-maxpcon(pcon,muprojr,quad,objfunc,mukmag,getmag(mparam)));

poutput=[];
if (length(pconrange)==2)
    [pconopt plfopt exitflag poutput]=fminbnd(fmaxobj,pconrange(1),pconrange(2),optim);
    plfopt=-plfopt;
elseif(length(pconrange)==1)
    pconopt=pconrange;
    plfopt=fmaxobj(pconopt);
else
    pconopt=[];
    plfopt=-inf;
end
timing.spcon=toc;

%if isempty (poutput)
%    fprintf('fishermaxshist.m: Iterations=%d \t funccounts=%d \n',voutput.iterations,voutput.funcCount);
%else
%    fprintf('fishermaxshist.m: lam.Iter=%d \t lam.fcounts=%d \t pcon.iter=%d \t pcon.fcoounts=%d\n',voutput.iterations,voutput.funcCount,poutput.iterations,poutput.funcCount);
%end

%determine which solution is optimal
if (vlfopt>plfopt)
    tic

    [pcon,qmax,xmax]= maxquadlam(quad,vopt,getmag(mparam));
    %we need to compute the actual stimulus which yields this optimal value
    %compute xmax

    sobj=zeros(1,length(pcon));
    for ind=1:length(pcon)
        sobj(ind)=finfopoissexp(pcon(ind)*mukmag+muprojr,qmax(ind));
    end
%     %return the solution which is larger
     [lfopt fi]=max(sobj);
     muprojopt=pcon(fi);
     xopt=xmax(:,fi);
    timing.txopt=toc;
    fcase=0;
else
    tic
    lfopt=plfopt;
    muprojopt=pconopt;
    [qmax,xopt]= sigmamaxsing(quad,pconopt,getmag(mparam));
    timing.txopt=toc;
    fcase=1;
end
%***********************************************
%Manipulate the timing
%if we didn't need to do a search over the singularity then
%just count the timing of searching over lambda
%I also don't bother including the timing of the last call to 
if isempty(pconrange)
    timing.fishermax=timing.slambda+timing.txopt;
else
    timing.fishermax=timing.txopt+timing.spcon+timing.slambda;
end

%**************************************************************************
%funcformaxv
%
%   v            - value between 0 and 1which we map to intervale emax to
%                    infinity
%   murpojr - projeciton onto mean of spike history - we can't control this
%   quad    - quadratic function to optimize
%
%Explanation: evaluates our objective function as a function of lam>emax.
%   It does this by computing sigmamax and muproj for lambda and then evaluating the
%   fisher information for these values.
function [funcval]= funcformaxv(qobj,v,mag,mukmag)

[pcon,qmax]=maxquadlam(qobj,v,mag);
%for each solution we evaluate finfopoisson and return the max.
sobj=zeros(1,length(qmax));
for ind=1:length(qmax)
    %multiply by negative one because we are using fminbnd
    sobj(ind)=-1*finfopoissexp(pcon(ind)*mukmag,qmax(ind));
end
%return the solution which is smaller
%b\c we multiply the mutual info by -1
[funcval]=min(sobj);


%****************************************************************
%Utility functions
%*****************************************************************
%
%   v            - value between 0 and 1which we map to intervale emax to
%                    infinity
%   murpojr - projeciton onto mean of spike history - we can't control this
%
% optimization function for case 1
function [funcval]= maxpcon(pcon,muprojr,quad,objfunc,mukmag,mag)
%compute sigmax
[qmax,xmax]= sigmamaxsing(quad,pcon,mag);

%evaluate the fisher information
funcval=objfunc(pcon*mukmag+muprojr,qmax);



