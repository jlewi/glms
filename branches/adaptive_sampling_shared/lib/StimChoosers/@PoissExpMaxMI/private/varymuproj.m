%function [xopt,muprojopt,lfopt,timing,fcase]=varymuproj(muprojr,quad,objfunc,muk,mukmag,mparam,optim)
%
%
%Explanation:
%This is s the code to compute the maximum by varying the projection
%of the stimulus along the mean
%
% 12-21-2007
%   made this a separate function from poissexpmaxmi
% 11-18-07
%   moidified it so we can do search over limited range of mu
%*************************************************************************
%**************************************************************************
function [xopt,muprojopt,lfopt,timing,fcase]=varymuproj(muprojr,quad,objfunc,muk,mukmag,mparam,optim)
tic;
%define the function to pass to fminbind
murange(1)=-getmag(mparam);
murange(2)=getmag(mparam);
fmaxobj=@(m)(-funcformax(m,muprojr,quad,objfunc,muk,mukmag,getmag(mparam),optim));
[muprojopt lfopt] = fminbnd(fmaxobj,murange(1),murange(2),optim);

%compute the max stimulus
[qmax,qmin,xmax,xmin]=maxminquad(quad,muprojopt,getmag(mparam),optim);
xopt=xmax;
lfopt=-lfopt;
timing.fishermax=toc;

%
%   muproj - projection onto mean of stimulus-this can vary
%   murpojr - projeciton onto mean of spike history - we can't control this
%   quad    - quadratic function to optimize
%
function [funcval]= funcformax(muproj,muprojr,quad,objfunc,muk,mukmag,mmag,optim)
%find sigma max for this value
[qmax,qmin,xmax,xmin]=maxminquad(quad,muproj,mmag,optim);
%evaluate the objective function
funcval=objfunc(muproj*mukmag+muprojr,qmax);

