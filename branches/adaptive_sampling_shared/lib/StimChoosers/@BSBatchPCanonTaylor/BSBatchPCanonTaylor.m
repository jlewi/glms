%function sim=BSBatchPCanonTaylor(nterms,'bdata')
%
%
% Explanation: Choose an optimal batch of stimuli for the canonical
% Poisson. Approximate the objective function using a taylor expansion with
% the specified number of terms. 
%
%
classdef (ConstructOnLoad=true) BSBatchPCanonTaylor < BSBatchBase
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %birddata - a BSData object
    %miobj  - object to compute the expected value of the terms in our
    %summation
    %  %windexes - which wave files to choose the stimuli from
    %           by excluding some wave files you can create a test set
    %
    %nrepeats   -how many   repeats of each wave file should we include
    %           - its pointless to include more than 1 repeat because only
    %             the spike history would change and in a normal experiemnt
    %             we would treat that as fixed anyway
    properties(SetAccess=private, GetAccess=public)
        version=080904;
        miobj=[];
        birddata=[];
                
    end

   
    methods
        
          
        %a function which chooses the batch of stimuli when
        %necessary. This should be overloaded in the derived classes
        %it gets called by choosetrial.
        %returns a structure with fields windex, repeat, and trial
        %which identify the start of the batch.
        [bstart,blogminfo]=choosebatch(obj,simobj,post);
      
       %Function to rollback the stimulus chooser to an earlier trial
       function [stimobj]=rollback(stimobj,simobj,trial)
           error('No rollback function implemented for class %s \n',class(stimobj));
       end
       
        function obj=BSBatchPCanonTaylor(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'nterms','bdata','windexes'};
            con(1).cfun=1;



            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=[];
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=[];             
                otherwise
                    bparams=rmfield(params,'nterms');
            end

            
            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@BSBatchBase(bparams);
            
            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 0
                    %do nothing used by load object
                   return; 
                case 1
                   
            obj.miobj=CompMIPoissExpTaylor('nterms',params.nterms);
            obj.birddata=params.bdata;
            obj.windexes=params.windexes;
                otherwise
                    error('Constructor not implemented')
            end
            

        end
        %get the bdata object stored in bpost
        function bdata=getbdata(obj)
          bdata=obj.birddata;
        end
    end
    
    methods(Static=true)
       success=test(); 
    end
end


