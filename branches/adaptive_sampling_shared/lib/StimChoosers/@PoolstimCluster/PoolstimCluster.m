%function stim=Poolstim('fname',fname,)
%   fname - file contianing 'stim' matrix which contains the possible
%   stimuli
%       stimuli are one per column
%
%   stimuli are selected randomly
%
%function stim=Poolstim('fname',fname,'miobj',iobj)
%       iobj - object which evaluates the mutual information of a stimulus
%             as a function of \mu_proj  and sigma
%
% function stim=Poolstim('stimpool', spool,'miobj',iobj)
%       spool - an object-array which are the stimuli
%
% Optional parameters:
%   rand - 0 use infomax stimuli
%        - 1 use random stimuli
%   nstim    - number of stimuli to use
%            - select the first nstim in the pool
%           - if resample is true
%             then nstim is how many stimuli we compute mutual info for on
%             each trial
%             these nstim however will be randomly drawn on each trial from
%             the entire pool.
%   srescale - structure to define how stimuli should be rescaled
%            - scaling factor for the stimuli
%           srescale.sfactor - just scale stimuli by sfactor
%   resample -
%              resample means the nstim we consider on each trial
%              are randomly drawn from the pool.
%              false - default value
%
%   nkeepmax - if we do resample, then we can potentially keep around
%               some number of the maximum stimuli in between iterations
%              nkeepmax says we should retain the nkeepmax sitmuli with
%              largest mutual info between trials
%
% Explanation:
%
%Return value
%   sfactor - amount by which stimuli are scaled.
%
%
% 4-17-07
%   added fields resample and nstim
%
% Revision History:
% $Revision$ - added field "stimrepeat" which determines whether we allow the
%       same stimulus to be selected on multiple trials.
%       If false - then we change the pool after each trial
%           - subclasses need to deal with this
%       Default: true
function [obj]=Poolstim(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'fname'};
con(1).cfun=1;

%2nd constructor
con(2).rparams={'fname','miobj'};
con(2).cfun=2;

con(3).rparams={'stimpool','miobj'};
con(3).cfun=3;

%**********************************************************
%Define Members of object
%**************************************************************
% degree          - description
% coef              -coefficients for the fitted surface

%declare the structure
%   rolloverind - indexes of stimuli with largest mutual info on previous
%     trial which we want to rollover
%   poolfile - structure describing file from which stimulus pool is
%       .fname
%       .fileid
%   constructed
%   stimloaded - variable to kee track of whether we loaded stimuli or not
%                added in version 070511
obj=struct('stimloaded',false,'stimpool',[],'infomax',false,'miobj',[],'nstim',inf,'resample',false,'nkeepmax',0,'rolloverind',[],'poolfile',[],'srescale',[],'version',070703,'stimrepeat',true);


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    obj=class(obj,'Poolstim',StimChooserObj());
    return
end

%convert varargin into a structure array
if (nargin==1)
    params=varargin{1};
else
    params=parseinputs(varargin);
end
%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    ismatch=1;
    for f=1:length(con(j).rparams)
        %check if  parameter was passed in
        if isempty(strmatch(con(j).rparams{f},pnames))
            ismatch=0;
            break;
        end
    end
    if (ismatch==1)
        cind=[cind j];
    end
end

if isempty(cind)
    error('Constructor:missing_arg','Required parameters for one of the constructors was not passed in');
elseif (length(cind)>1)
    %we might be able to resolve the constructor if one constructor has
    %more parameters then the other
    %if it matches constructor 2 it also matches constructor 1 but we
    %should call constructor 2.
    cind=2;
    %    error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
%instantiate a base class if there is one
%fields which should be passed to base class
fbase={'debug'};
bparam=[];
for j=1:length(fbase)
    if isfield(params,fbase(j))
        bparam.(fbase{j})=params.(fbase{j});
    end
end
pbase=StimChooserObj(bparam);
poolfile=[];
poolfile.fileid=nan;    %indicate it hasn't been set
switch cind
    case 1
        poolfile.fname=params.fname;
        %stimuli are random so we are not glm specific
        pbase=set(pbase,'glmspecific',0);
    case 2
        poolfile.fname=params.fname;
        obj.infomax=1;
        if isempty(params.('miobj'))
            error('You must specify an miobj which computes the mutual information for a stimulus');
        end
        obj.miobj=params.('miobj');
    case 3
 
        obj.stimpool=params.stimpool;
        if isempty(params.('miobj'))
            error('You must specify an miobj which computes the mutual information for a stimulus');
        end
        obj.miobj=params.('miobj');
        %parameters which are ignored
        param.stimrescale=[];
        poolfile=0;
        obj.stimloaded=true;
        obj.infomax=true;

    otherwise
        error('Constructor not implemented')
end

obj.poolfile=poolfile;

%check if optional parameter rand was specified
if isfield(params,'rand')
    if (params.rand~=0)
        obj.infomax=0;
        fprintf('Poolstim will select stimuli randomly \n');
    else
        obj.infomax=1;
        if (cind~=2)
            error('Parameters required for info max are not supplied.');
        end
        fprintf('Poolstim will select info. max. stimuli \n');
    end
end

%process the other fields
fnames=fieldnames(params);
for j=1:length(fnames)
    switch fnames{j}
        case'resample'
            obj.resample=params.resample;
        case 'nkeepmax'
            obj.nkeepmax=params.nkeepmax;
        case'nstim'
            obj.nstim=params.nstim;
        case'stimrescale'
            obj.srescale=params.stimrescale;
        case 'stimrepeat'
            obj.stimrepeat=params.stimrepeat;
        case con(1).rparams
              %required parameters for constructor do nothing
         case con(2).rparams
              %required parameters for constructor do nothing
         case con(3).rparams
              %required parameters for constructor do nothing
        case 'debug'
            %used by base class
        otherwise
            error('not a valid field');
    end
end



%*********************************************************
%Create the object
%****************************************
obj=class(obj,'Poolstim',pbase);

%create the stimulus pool from the file and apply any scaling necessary
%5-24
fprintf('Delaying load of stimuli till needed \n');
%[obj]=processpool(obj,'file',poolfile);


%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case {'fname','filename'}
            params.('fname')=vars{j+1};
        otherwise
            params.(vars{j})=vars{j+1};
    end
end


