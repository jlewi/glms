%function [pobj]=processpool(pobj,type,rawpool)
%       type - 'file' or 'matrix'
%               -specifies whether rawpool is a file containing the
%                   matrix of data or the actual matrix of stimuli
%       rawpool - matrix or name of file
%       srescale - parameters for rescaling the stimuli
%
% Explanation: This function handles all processing 
%   of the raw stimulus pool to get the actual stimulus pool
%   processing entails three steps
%   1. selecting a subset of the stimuli in the pool or matrix
%   2. rescaling the stimuli
function [pobj]=processpool(pobj,type,rawpool)

%***************************************************
%create a matrix of rawpool stimuli
switch lower(type)
    case {'file','fname','filename'}
        %load the pool from a file
        fname=rawpool;
       try
            rawpool=load(fname,'stim')
            rawpool=rawpool.stim;
            
            pobj.poolfile.fname=fname;
        catch
            error('Could not load variable stim in file %s',fname);
       end
       try
           
            fileid=load(fname,'fileid')            
            pobj.poolfile.fileid=fileid.fileid;
       catch
           warning('File containing stimuli did not have a fileid. Appending one now');
            rand('state', sum(100*clock));
           fileid=rand(1)+floor(rand(1)*10);
           pobj.poolfile.fileid=fileid;
           save(fname,'fileid','-append');
       end
    case {'matrix'}
        %do nothing
    otherwise
        error('unrecognized value of type: %s',type);
end


%************************************************************************
%Create the appropriate subsample of the rawpool
%***********************************************************************
%we keep the entire pool if we are doing one of the following
%   i) number of stimuli = size of pool
%       -turn resampling off if this is the case
%   ii) resampling is turned on
%       -we need to keep the entire pool so we can redraw stimuli on each
%       trial
%   
nraw=size(rawpool,2);
pobj.poolfile.nraw=nraw;
%if nstim is empty or infinity we use entire pool
if isempty(pobj.nstim)
    pobj.nstim=nraw;
end

if isinf(pobj.nstim)
    pobj.nstim=nraw;
end

%error checking
if (nraw<pobj.nstim)
    error('Number of stimuli in pool is less than nstim=%d \n',pobj.nstim);
end
    
%only trucate the pool if we are not resampling
if ((pobj.resample)||(nraw==pobj.nstim))
     if (pobj.resample)
         (params.nstim==size(rawpool,2))
                fprintf('Turning resampling off because #of stimuli = size of stimulus pool\n');
                pobj.resample=false;
     end
     pobj.stimpool=rawpool;
else
            fprintf('Selecting first %d stimuli in pool \n',pobj.nstim);
            pobj.stimpool=rawpool(:,1:pobj.nstim);
end


%*************************************************************************
%rescaling
%rescale the stimuli if told to do so
if ~isempty(pobj.srescale)
    srescale=pobj.srescale;
    if (length(fieldnames(pobj.srescale))>1)
        %determine the stimulus which yields the highest firing rate
        epsvar=srescale.theta'*pobj.stimpool;

        [mesp sind]=max(epsvar);
        glm=srescale.glm;

        %compute the scale factor so that when this stimulus is scaled
        %by sfactor the maxfiring rate is achieved
        optim=optimset('TolX',10^-12,'TolF',10^-12);
        fsetstimscale=@(s)(glm.fglmmu(s*mesp)-srescale.maxrate);
        sfactor=fsolve(fsetstimscale,.5,optim);

%        pobj=setstimpool(obj,sfactor*obj.stimpool);
        pobj.stimpool=sfactor*pobj.stimpool;
        %just save the sfactor
        pobj.srescale=[];
        pobj.srescale.sfactor=sfactor;
    else
        %srescale contains the scale factor for the stimuli

        sfactor=srescale.sfactor;
        pobj.stimpool=sfactor*pobj.stimpool;
    end
end
