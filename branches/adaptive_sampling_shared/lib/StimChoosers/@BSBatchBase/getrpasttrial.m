%function getrpasttrial(obj,windex,repeat)
%
%Return value:
%  rtrial - is a vector whose length = the number of trials in this wavefile. Each
%          element is 0 and 1. A
%
%Revisions:
%   11-25-2008 take as input simobj so that we can call initrtrial if
%   rtrial is empty
function rtrial=getrpasttrial(obj,windex,repeat,simobj)
if isempty(obj.paststim)
    %paststim is a structure array
    %each row corresponds to a wave file, each column
    %corresponds to a repeat
    %each entry stores an array of logicals indicating whether
    %the stimulus corresponding to that entry has already been presented
    obj.paststim=struct('rtrial',[]);

    %to save space rtrial is only updated when it is accessed
    %this is because when run in parallel each lab processes a different wave
    %file so no need to set it unless its accessed
    obj.paststim=repmat(obj.paststim,max(obj.windexes),obj.nrepeats);
end

%check if rtrial has been initialized for this windex,repeat yet.
if isempty(obj.paststim(windex,repeat).rtrial)

    [ntrials]=getntrialsinwave(getbdata(obj.bspost),windex);

    obj.paststim(windex,repeat).rtrial=zeros(1,ntrials);


    %determine which trials from this wave file have already been presented
    %find those past trials which came from repeat=repeat of wavefile windex
    ind=(simobj.paststim(1,:)==windex) & (simobj.paststim(2,:)==repeat);

    rind=simobj.paststim(3,ind);

    obj.paststim(windex,repeat).rtrial(rind)=true;
end

rtrial=obj.paststim(windex,repeat).rtrial;