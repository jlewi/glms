%function [obj, stim, shist, obsrv,stimindex,blogminfo]= choosetrial(obj, simobj);
%   obj - A BSOptStim object
%   simobj - a BSSim object
%
%Explanation:
%   stim - vector representing the stimulus
%   shist - vector of the spike history
%   obsrv - the response
%   stimindex=3 x1 vector indicating the trial corresponding to the
%   stimulus
%           = we use this to keep track of the order the stimuli were
%           chosen in
%       stimindex(1,1) - windex of the stimulus
%       stimindex(2,1) - the repeat of the wfile
%       stimindex(3,1) - the relative trial within the
%   blogminfo - on trials when we pick a new batch this is the log of the
%               mutual info for this batch. Otherwise its nan.
%   We choose the optimal batch of stimuli by maximizing a lower bound for
%   the informativeness of the batch.
%   To make computing this lower bound efficient we set the spike history
%   to zero.
%
%Revisions
%   08-28-2008
%       When we don't have any batches of stimuli we set the mutual
%       information to nan (instead of inf). We throw an appropriate
%       MEXception so that we can catch this exception and save the data
function [stim, shist, obsrv,stimindex,blogminfo]= choosetrial(obj, simobj,post)

blogminfo=nan;

%check if we are still presenting stimuli in the most recently selected
%optimal batch of stimuli
%obj.binfo.bindex is how many stimuli in the batch have already been
%presented
%thus when bindex==obj.bsize we need to pick a new batch.
%obj.binfo.bindex is initialized to nan to indicate its not set and we need
%to choose a batch.
if ((obj.binfo.bindex == obj.bsize) || isnan(obj.binfo.bindex))
    %select a new batch
    [bstart,blogminfo]=choosebatch(obj,simobj,post);
    %set the batch information
    obj.binfo.bstart=bstart;
    obj.binfo.bindex=0;
    
    
    %mark the trials in this batch as being presented 
    rtrial=getrpasttrial(obj,bstart.windex,bstart.repeat,simobj);
    
    %update the past trials to indicate we have already selected this batch
    if ~isempty(rtrial)
        rtrial(bstart.trial:bstart.trial+obj.bsize-1)=true;
        setrpasttrial(obj,bstart.windex,bstart.repeat,rtrial);
    end
end

%check to make sure we are aligned with simobj
if (simobj.niter~=obj.binfo.trial)
    error('The number of trials in the simulation object is not the same as the current trial in BSBatchPoissLB');
end
obj.binfo.bindex=obj.binfo.bindex+1;

%get the appropriate stimulus in the batch

bdata=obj.bdata;
optwindex=obj.binfo.bstart.windex;
optrepeat=obj.binfo.bstart.repeat;
opttrial=obj.binfo.bstart.trial+obj.binfo.bindex-1;
[stim,shist,obsrv]=gettrialwfilerepeat(bdata,optwindex,optrepeat,opttrial,simobj.mobj);

stimindex=[optwindex;optrepeat;opttrial];

%increment the trial
obj.binfo.trial=obj.binfo.trial+1;