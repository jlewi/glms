%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older baseversion of the class.
function obj=loadobj(lobj)

%get the latest baseversion
%save the latest baseversion so we can check to make sure all
%conbaseversions have been completed.

newobj=BSBatchBase();

latestver=newobj.baseversion;

if isstruct(lobj)
    obj=newobj;

    error('You must replace baseversion with the correct name of the baseversion field and delete this error');
    obj.baseversion=lobj.baseversion;
else
    %we don't need to create a new object
    obj=lobj;
end



%******************************************************************
%Sequentially convert one baseversion of the object to the next
%until we get to the latest baseversion
%
%Warning: If lobj is a structure then we haven't copied the data from lobj
% to obj yet. 
%******************************************************************
if (obj.baseversion < baseversion1)
    %do conbaseversion
    %copy fields from old baseversion to new baseversion
    %if lobj is a structure
    if isstruct(lobj)
    fskip={};    %fields not to copy
    obj=copyfields(lobj,obj,fskip);
    end
end

%check if converted all the way to latest baseversion
if (obj.baseversion <latestver)
    error('Object has not been fully converted to latest baseversion. \n');
end

%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
%
%Copy fields must be declared within loadobj because otherwise it won't
%have permssion to set the private fields of the object.
function dest=copyfields(source,dest,skip)
 %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(source);

        %struct for object
        sobj=struct(dest);
        for j=1:length(fnames)
            switch fnames{j}
                case skip
                    %do nothing we skip this field
                otherwise
                    %set the new field this will cause an error
                    %if the field isn't a member of the new object
                      dest.(fnames{j})=source.(fnames{j});
            end
        end
    
