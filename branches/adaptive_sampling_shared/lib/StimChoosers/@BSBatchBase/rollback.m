%function rollback(stimobj,simobj,startrial)
%   simobj - the stimulus object
%   starttrial  - the trial to start on
%             this will be the first trial we execute
%
%Explanation: This function rolls the BSBatch stimulus object back to an
%earlier trial.
%
function rollback(stimobj,simobj,starttrial)

pasttrials=starttrial-1;
binfo=struct('bindex',nan,'bstart',[]);


%determine how many full batches have been presented so far
%we subtract 1 from trial b\c  is how many
nbatchprev=floor(pasttrials/stimobj.bsize);

%special case pasttrial is a multiple of bsize
%this means we need to pick a new batch
%so we leave binfo with bindex=nan and bstart=[]
if (nbatchprev*stimobj.bsize~=pasttrials)

    %subtract 1 b\c when bindex=0 when presenting the first stimulus in a batch
    %so bindex should range from 0 to simobj.bsize-1
    binfo.bindex=starttrial-(nbatchprev*stimobj.bsize)-1;


    %determine the pasttrial which is the first trial in the batch
    bstart=(nbatchprev)*stimobj.bsize+1;
    
    binfo.bstart=struct('windex',simobj.paststim(1,bstart),'repeat',simobj.paststim(2,bstart),'trial',simobj.paststim(3,bstart));


end
binfo.trial=pasttrials;
stimobj.binfo=binfo;



