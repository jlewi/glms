%function [obj,optstim]=optbatchinwav(obj,simobj,windex,post)
%   obj - The stimulation chooser object
%   simobj - the simulation object
%   windex - the index of the wave file we are searching for the optimal
%          stimulus
%   post   - the posterior object
% Explanation - this function constructs  a pool of psize stimuli
%       from the trials corresponding to wavefile wfile
%       We then select from this pool the optimal stimulus
%Return value:
%  	opstim.windex - windex - wave file
%   optstim.repeat - which repeat of this wavefile
%   opstim.trial - which trial of this wavefile
%   opstim.mi -
%
%
%
%Explanation: Computes the mutual information.
%
%Revisions:
%   08282008 - Set the mutual information to nan if no batches are left
function [optstim]=optbatchinwav(obj,simobj,windex,post)


mobj=simobj.mobj;

bdata=getbdata(obj.bspost);


%    wcount=wcount+1;
%fprintf('BSlogpost:labindex=%d compllike  file index %d of %d wave files \n',labindex,windex,length(windexes));


%to compute the mutual information we need to compute
%muproj and sigmaproj
%compute muproj and sigmaproj setting spike history to zero.
[muproj]=compglmprojnoshist(obj.bspost,windex,mobj,getm(post));

%since spike history is zero muproj will be the same for all repeats
%because the inputs are the same across repeats
muproj=muproj(1,:);

[ntrials,cstart]=getntrialsinwave(bdata,windex);

%batchlogmi - is a matrix 1xntrials long which stores
%the mutual info for each batch starting at that column.
%since the spike history is the same for all batches we just compute it
%once for all repeats
batchlogmi=zeros(1,ntrials);

%
%compute the mutual information for each individual stimulus in the pool
%PoissExpCompMi computes the logarithm so we have to raise it to the
%exponential power before summing across stimuli in the batch
%this is the same for all repeats because we set spike history to zero.

%determine which rtrials haven't been selected yet.
%so now we need to determine the valid batches
%compute how many overlapping batches we can get from this wave file

nbatches=ntrials-obj.bsize+1;
batchesleft=zeros(obj.nrepeats,nbatches);

for repeat=1:obj.nrepeats
    %1 indicates trial has already been presented
    rtrial=getrpasttrial(obj,windex,repeat,simobj);

    %conv it with a matrix of ones
    %if the batch hasn't been selected
    %then the entry in batchesleft should be 0
    cf=conv(rtrial,ones(1,obj.bsize));
    batchesleft(repeat,:)=cf(obj.bsize:obj.bsize+nbatches-1);
end
%each entry of batches left which=obj.bsize indicates that a batch starting
%at that location is left


optstim.mi=nan;
optstim.windex=windex;
optstim.repeat=nan;
optstim.ntrial=nan;

inpbatch=zeros(getparamlen(simobj.mobj),obj.bsize);

%stores the start of the last batch to be processed
blaststart=nan;
for bstart=1:nbatches

    if (mod(bstart,10)==0)
       fprintf('Batch %d of %d \n',bstart,nbatches); 
    end
    %check if any of the repeats have a batch at this location
    repeat=find(batchesleft(:,bstart)==0,1,'first');

    if ~isempty(repeat)
        %check if inpbatch already contains some of the inputs in this batch
        if (blaststart+obj.bsize-1>=bstart)
            %noverlap- the number of stimuli in the previous batch that we
            %can copy to the new batch
            noverlap=(blaststart+obj.bsize-1)-bstart+1;
            bstartind=noverlap+1;
            inpbatch(:,1:noverlap)=inpbatch(:,end-noverlap+1:end);
        else
            bstartind=1;
        end
        for bind=bstartind:obj.bsize
            %get the input from the first repeat because it doesn't matter
            %which repeat we use

            [stim]=gettrialwfilerepeat(obj.bdata,windex,repeat,bstart+bind-1,simobj.mobj);
            inpbatch(:,bind)=projinp(simobj.mobj,stim,zeros(getshistlen(simobj.mobj),1));
        end
        batchlogmi=complogminfo(obj.miobj, inpbatch,post);

        if ((batchlogmi>optstim.mi) || isnan(optstim.mi))
            optstim.ntrial=bstart;
            optstim.mi=batchlogmi;
            optstim.repeat=repeat;
        end

        %update blaststart
        blaststart=bstart;
    end

end


