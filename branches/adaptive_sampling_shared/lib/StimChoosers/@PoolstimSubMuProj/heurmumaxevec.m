%function stimpool=heurmu(sobj,post,magcon)
%   sobj - object
%   post - posterior
%   magcon -magcon
%Explanation: This is my original heuristic which just varies the
%projection along the mean but does not take into account the eigenvectors.
function stimpool=heurmumaxevec(sobj,post,mobj)
magcon=getmag(mobj);
%get the mean of just the stimulus terms
fullpostm=getm(post);
mustim=fullpostm(1:getstimlen(mobj),1);

%normalize the mean
mustim=normmag(mustim);

nstim=getpoolsize(sobj);
%adjust the stimuli accordingly
%1. remove projection along mu
stimpool=getstimpool(sobj);
stimpool=stimpool-mustim*(mustim'*stimpool);

%remove the projection along all of the max eigenvector
efull=getefull(post);
[maxeigd mind]=maxeval(efull);

%max eigenvector
vmax=getevecs(efull,mind);

%project the stimulus onto each eigenvector
%each column is projection of a stimulus onto the eigenspace of the maximum
%eigenvalue
%vproj=vmax'*stimpool;


%if the maxeigenvector has a multiplicity greater than take random linear
%combinations of the vectors in this eigenspace
%generating these random numbers will be expensive when the multiplicity is
%high (i.e on early trials)
if (length(mind)>1)
    bpts=normrnd(zeros(length(mind),nstim),ones(length(mind),nstim));
    bpts=normmag(bpts);
    %form a vector by multiplying bpts by the maximum eigenvector
    vbasis=vmax*bpts;
    
    %make these vectors orthogonal to the mean
    vbasis=vbasis-mustim*(mustim'*vbasis);
else    
    vbasis=vmax;
    %make it orthogonal to the mean
    vbasis=vbasis-mustim*(mustim'*vbasis);
    
    vbasis=vbasis*ones(1,nstim);
    
end
%normalize vbasis
vbasis=normmag(vbasis);

%remove projection of vbasis from stimpool
stimpool=stimpool-vbasis.*(ones(size(vbasis,1),1)*sum(vbasis.*stimpool,1));
if (length(mind)==size(stimpool,1))
    %if multiplicity of max eigenvector = length of stimpool
    %then after removing projection along max eigenvtor stimpool is zero.
    
    y=normrnd(zeros(2,nstim),ones(2,nstim));
y=magcon*normmag(y);


stimpool=vbasis.*(ones(size(stimpool,1),1)*y(1,:));
stimpool=stimpool+mustim*y(2,:);

else
%normalize stimpool;
stimpool=normmag(stimpool);

%mustim and vbasis are orthonormal and should be orthogonal to stimpool
%randomly generate triplets which specify the amount of energy along
%vbasis, stimpool and mustim.
y=normrnd(zeros(3,nstim),ones(3,nstim));
y=magcon*normmag(y);

stimpool=stimpool.*(ones(size(stimpool,1),1)*y(1,:));
stimpool=stimpool+vbasis.*(ones(size(stimpool,1),1)*y(2,:));
stimpool=stimpool+mustim*y(3,:);
end


