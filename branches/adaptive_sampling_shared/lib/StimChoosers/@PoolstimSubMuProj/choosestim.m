%function stim=choosestim(sobj,post,shist,einfo,mparam,varargin)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history 
%               should be a column vector
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   stimpool - information about the stimulus pool
%            - only returned if number of arguments is >3
%       .muproj
%       .sigma
%Explanation: maximize the mutual information for the canonical poisson
%model
%
%Revision:
%   >1504 - work with spike history as well
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mobj,varargin)

oreturn=[];

sobj=loadstim(sobj);

magcon=getmag(mobj);

%call the heuristic which constructs the stimulus pool
stimpool=sobj.heur(sobj,post,mobj);

if (getdebug(sobj))
    %check magnitudes sum to magcon
    mag=sum(stimpool.^2,1).^.5;
%    if ~isempty(find(abs((mag-sobj.magcon))>10^-8))
    if ~isempty(find(abs((mag-magcon))>10^-8))
        error('Magnitudes not properly normalized');
    end
end
%*****************************************************************
% 2 possibilities
%1) select stimuli randomly from the pool
%2) select stimuli from pool which maximizes the mutual info
%******************************************************************
%nstim=size(sobj.stimpool,2);
nstim=getpoolsize(sobj);
if (sobj.Poolstim.infomax==0)
    %randomly select stimulus
    %keyboard
    sind=ceil(rand(1)*nstim);
    xmax=stimpool(:,sind);
    oreturn.sind=sind;
else
    if (getshistlen(mobj)>0)
        %spike history dependence we need to append spike history to
        %stimulus
        nspikes=shist;
      %  if ~isempty(shist)
       % nspikes=[shist];
       % else 
       %     nspikes=[];
       % end
        %nspikes could be less than shistlen so we may have to append o's
        nspikes=[nspikes;zeros(getshistlen(mobj)-length(nspikes),1)];
        stim=[stimpool;nspikes*ones(1,nstim)];
        
    else

        stim=stimpool;
    end
    stimind=[];

    muproj=getm(post)'*stim;

    % sigma=zeros(1,nstim);
    %4-19-2007
    %try to speed this up using matrix operations
    %   for j=1:nstim
    %      sigma(j)=stim(:,j)'*post.c*stim(:,j);
    %    end
    %each column is the post.c*(:,j)
    sigma=getc(post)*stim;
    sigma=stim.*sigma;
    sigma=sum(sigma,1);

    mi=compmi(getmiobj(sobj),muproj,sigma);

    %maximize the mi

    [maxmi indmax]=max(mi);



    poolind=indmax;

    xmax=stimpool(:,poolind);
    oreturn.sind=poolind;

end

%if nargout is > 3 return parameters related to the pool
if (nargout>3)
    if (getinfomax(sobj))
        varargout{1}.muproj= muproj;
        varargout{1}.sigma=sigma;
    end
end
