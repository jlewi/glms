%
%optional parameters:
%   heur - string specifying the heuristic
%       'mu' - original heuristic which just varies the mean
% Explanation: Constructor for choosing pool stimuli. 
%   This function adjusts the stimulus pool so that the projection on the
%   mean is a random sample in between the maximum and minimum possible
%   values.
%
% Constructors:
% PoolstimNonLinInp('Model',mobj,'fname',fname)
%   'Model' - this is used to precompute the projection of the stimuli
%             it is not SAVED.
%           - we only need this if we aren't selecting the stimuli randomly
%           -must call this constructor if using info. max sitmuli
%   'fname' - file containing the pool of stimuli
%
%Return value
%   sfactor - amount by which stimuli are scaled.
%
%Revisions:
%   12-17-2007
%       add additional heuristics along with a variable to control which
%       heuristic is used
%   11-07-2007: don't pass in magcon as argument. Get it from mobj.
function [obj,sfactor]=PoolstimSubMuProj(varargin)

sfactor=1;
%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={};
con(1).cfun=1;

%con(2).rparams={'rand'};
%con(2).cfun=2;

%**********************************************************
%Define Members of object
%**************************************************************
% magcon  - magnitude constraint
%           all stimuli have this magnitude
%           this is also used to determine the values of the projection 
%           along muproj. 
%           -removed in 071107
% version - added in version 070513 to store version info
%
% heur    - pointer to heuristic function
%               
obj=struct('version',071217,'heur',@heurmumaxevec);


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    pbase=Poolstim();
    obj=class(obj,'PoolstimSubMuProj',pbase);
    return
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);


%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
       %do nothing     
    otherwise
        error('Constructor not implemented')
end


srescale=[];
if isfield(params,'stimrescale')
    srescale=params.stimrescale;
    %remove the field so its not passed to the base class
    %b\c we don't want to rescale the stimuli
    fprintf('stimrescale ignored \n');
    params=rmfield(params,'stimrescale');
end

if isfield(params,'heur')
    %proccess the heuristic
    %(convert it to a function pointer)
    switch (params.heur)
        case 'heurmu'
            obj.heur=@heurmu;
        case 'heurmumaxevec'
            obj.heur=@heurmumaxevec;
        otherwise
            error('Unknown heuristic');
    end
    params=rmfield(params,'heur');
end

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=Poolstim(params);
obj=class(obj,'PoolstimSubMuProj',pbase);


