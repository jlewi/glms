%function [obj, stim, shist, obsrv,stimindex]= choosetrial(obj, simobj);
%   obj - A BSOptStim object
%   simobj - a BSSim object
%
%Explanation:
%   stim - vector representing the stimulus
%   shist - vector of the spike history
%   obsrv - the response
%   stimindex=3 x1 vector indicating the trial corresponding to the
%   stimulus
%           = we use this to keep track of the order the stimuli were
%           chosen in
%       stimindex(1,1) - windex of the stimulus
%       stimindex(2,1) - the repeat of the wfile
%       stimindex(3,1) - the relative trial within the
%
%Revisions
%   07-20-2008
%       take the object and a pointer to the simulation as input
function [stim, shist, obsrv,stimindex]= choosetrial(obj, simobj,post)

windexes=obj.windexes;

%we need to loop over the wavefiles and select the maximally informative
%stimulus. Each node processes a different wave file.
%Each node constructs a pool for each of the files it processes
%And selects the optimal stimulus in this pool
%
%We use a distributed array to store these results
%   dpoolopt - this is an 4xnwaveefiles  matrix
%   dpoolopt(1,j) - the index of this wavefile
%   dpoolopt(2,j) - the repeat of the optimal stimulus from the j'th
%   wavefile
%   dpoolopt(3,j) - the relative trial for this wave file of the optimal
%                   stimulus
%   dpoolopt(4,j) - the mutual information for this optimal stimulus

dpoolopt=zeros(4,length(windexes),distributor());

%we are using a parfor loop
%therefore I don't think we can use a distributed array because that
%requires MPI communication
%each node creates a structure dpoolopt and dpoolwind
%dpoolwind - stores the values for the windex being evaluated
%if that is better than dppool opt we override dpool opt
%dpoolopt=zeros(5,1);
%dpoolopt(4,1)=nan;
optstim=[];
%fprintf('choosetrial');


%manually compute which wave files each node will process
%we do this manually because we want need to know in order to handle the
%broad casting of the pool
%nwindexes is an array which stores the number of windexes processed
%by each node
nwindexes=ones(numlabs,1)*floor(length(windexes)/numlabs);
remainder=mod(length(windexes),numlabs);
nwindexes(1:remainder)=nwindexes(1:remainder)+1;

startind=[0;cumsum(nwindexes(1:end-1))]+1;
endind=startind+nwindexes-1;

%create a matrix to store the pool for just the wave files processed by
%this node
nrows=obj.wavpoolind(endind(labindex),2)-obj.wavpoolind(startind(labindex),1)+1;

poolmi=zeros(nrows,3);

startrow=1;

%************************
%use a try block and properly handle possibility
%that an error occurs on some threads but not others
%*********************************************
err=0;
emsg='';
try
    for windexind=startind(labindex):startind(labindex)+nwindexes(labindex)-1

        windex=windexes(windexind);

        dpoolopt(1,windexind)=windex;

        %compute the indexes for the pool for this wave file
        endrow=startrow+diff(obj.wavpoolind(windexind,:),1,2);
        [optstim, poolmi(startrow:endrow,:)]=optstiminwav(obj,simobj,windex,post);
        dpoolopt(1,windexind)=windex;
        dpoolopt(2,windexind)=optstim.repeat;
        dpoolopt(3,windexind)=optstim.ntrial;
        dpoolopt(4,windexind)=optstim.mi;

        startrow=endrow+1;
    end
catch e

    err=1;
    emsg=e.message;
    emsg=e.message;
    emsg=fprintf('\n%s stack is: Line \t file:',emsg);
    for index=1:length(e.stack)
        fprintf('%s\n %d \t %s',emsg, e.stack(index).line, e.stack(index).file);
    end
end

%check if error occured on any threads
if (gplus(err)>0)
    %determine which labs threw an error
    lerr=gcat(err);
    lerr=find(lerr>0);

    if (err==0)
        emsg=sprintf('Error was thrown on lasbs %s', num2str(lerr));
    end
    error(emsg);
end

%now we gather the full dpoolopt on each matrix
%dpoolopt=gcat(dpoolopt);
dpoolopt=gather(dpoolopt);

%***********************************************************
%save the pool if poolfile isn't empty
%*************************************************************
if ~isempty(obj.poolfile)
    %now we concatenate all the poolmi's
    poolmi=gcat(poolmi,1);

    err=0;
    emsg='';

    %we execute writematrix on all nodes
    %the WriteMAtrix class should handle syncrhonizing the threads properly
    try
        writematrix(obj.poolfile,poolmi,simobj.niter+1);
    catch e
        err=1;

    end

    %labBarrier;
    %fprintf('Do broadcast');
    errb=labBroadcast(1,err);
    if (errb==1)
        if (labindex==1)
            error('Error occured while trying to write the stimulus pool to a file. Message was %s' ,e.message);
        else
            error('Error occured on lab 1.');
        end
    end
end

%find the optimal stimulus
[maxmi mind]=max(dpoolopt(4,:));
mind=mind(1);

optwindex=dpoolopt(1,mind);
optrepeat=dpoolopt(2,mind);
opttrial=dpoolopt(3,mind);

bdata=getbdata(obj.bspost);
[stim,shist,obsrv]=gettrialwfilerepeat(bdata,optwindex,optrepeat,opttrial,simobj.mobj);

stimindex=[optwindex;optrepeat;opttrial];