%function [obj, stim, shist, obsrv,stimindex]= choosetrial(obj, post,psize);
%   post - current posterior 
%   psize - size of the pool to use for selecting the optimal stimulus
%Explanation: 
%   stim - vector representing the stimulus
%   shist - vector of the spike history 
%   obsrv - the response
%   stimindex=3 x1 vector indicating the trial corresponding to the
%   stimulus
%           = we use this to keep track of the order the stimuli were
%           chosen in
%       stimindex(1,1) - windex of the stimulus
%       stimindex(2,1) - the repeat of the wfile
%       stimindex(3,1) - the relative trial within the 
function [obj, stim, shist, obsrv,stimindex]= choosetrial(obj, post,mobj,psize,trial)


if ((isinf(obj.iterfullmax) || mod(trial,obj.iterfullmax+1)~=0) && (trial>obj.startfullmax))
fpost=getfullpost(post);
tanpost=gettanpost(post);
tspace=gettanspace(post);
basis=getbasis(tspace);

minfo=getm(fpost)+getbasis(tspace)*getm(tanpost);
cinfo=basis*getc(tanpost)*basis';
%compute eigendecomp of cinfo because we will need it for the stimulus
%optimization.
pinfo=GaussPost('m',minfo,'c',EigObj('matrix',cinfo));

else
    
    %info. max on full space set posterior to full posterior
    pinfo=getfullpost(post);
end

%choose the stimulus by calling choosestim on the base class using 
%pinfo as our posterior.
[obj.BSOptStim, stim, shist, obsrv,stimindex]= choosetrial(obj.BSOptStim, pinfo,mobj,psize,trial);

