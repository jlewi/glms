%function [stim]=modstimpoolmusigma(sobj,post,,mparam)
%   sobj - PoolStimQuadInpHeur
%   post - posterior
%   
%   rank - not used but needed as part of generic function signature
%Return value:
%   stim - set of stimuli 
%
%Explanation this function uses a heuristic to create a set of stimuli
%   which will likely span the feasible region in mglmproj,sglmproj space
%
%   This heuristic tries to get sigma inaddition to mu to span the full
%   range.
function [stimpool,sobj]=heurmumaxevecstim(sobj,post,mobj,rank)

dim=getstimlen(mobj);

%number of stimuli
psize=getnstim(sobj);

magcon=getmag(mobj);

%compute the vector in stimulus space most correlated with the mean of the
%posterior
%get a quadratic matrix
%i.e returna matrix qmat such that x'*qmat*x;
m.mat=qmatquad(getninpobj(mobj),getm(post));
%due an svd 
[m.evecs m.eigd]=svd(m.mat);

%pull out the max eigenvector
%version:070531 - instead of always selecting 
%   the max eigenvector select one of the principal components
%   sample the principal components in proprotion to the relative energy as
%   measured by the svd.
m.eigd=diag(m.eigd);
%normalize the energy of the singular values
neigd=m.eigd/sum(m.eigd);

%each eigenvector appears a number of times proportional to probability
%of its eigenvalue.
m.emax=zeros(dim,psize);
sind=1;
for j=1:dim
    eind=sind+floor(neigd(j)*psize)-1;
   m.emax(:,sind:eind)=m.evecs(:,j)*ones(1,eind-sind+1); 
   sind=eind+1;
end

%any leftover due to rounding error we set to max evector
if (eind<psize);
   m.emax(:,eind+1:end)=m.evecs(:,1)*ones(1,psize-eind); 
end

%rnums=rand(1,getpoolsize(sobj));

%reigind=find(cumsum(neigd)>rand(1,1),1);
%m.emax=m.evecs(:,reigind);


%randomly generate the projections along the mean
muproj=rand(1,psize)*2*magcon-magcon;


%******************************************************************
%To vary sigma
%*******************************************************************
%get the maximum eigenvector of the covariance matrix
efull=getefull(post);
[maxeigd mind]=maxeval(efull);

%max eigenvector
cpmax=getevecs(efull,mind(1));

%%compute the svd of the covariance matrix
%[csvd usvd]=svd(getc(post));

%pick the max eigenvector of the svd.
%cpmax=csvd(:,1);

%take the mag eigenvector
cmax.mat=qmatquad(getninpobj(mobj),cpmax);
%due an svd 
[cmax.evecs cmax.eigd]=svd(cmax.mat);
cmax.max=cmax.evecs(:,1);

cmax.eigd=diag(cmax.eigd);
%normalize the energy of the singular values
neigd=cmax.eigd/sum(cmax.eigd);

%each eigenvector appears a number of times proportional to probability
%of its eigenvalue.
cmax.max=zeros(dim,psize);
sind=1;
for j=1:dim
     eind=sind+floor(neigd(j)*psize)-1;
   cmax.max(:,sind:eind)=cmax.evecs(:,j)*ones(1,eind-sind+1); 
   sind=eind+1;
end

%any leftover due to rounding error we set to max evector
if (eind<psize);
   cmax.max(:,eind+1:end)=cmax.evecs(:,1)*ones(1,psize-eind); 
end

%make the vectors in cmax.max orthogonal to m.emax
cmax.max=cmax.max-(ones(dim,1)*sum(cmax.max.*m.emax,1)).*m.emax;

%normalize the vectors
cmax.max=normmag(cmax.max);

%any energy not along the mean is along the max eigenvector
cpproj=(magcon^2-muproj.^2).^.5;

%********************************************************************
%add the projections along the cpmax and m.emax
stimpool=m.emax.*(ones(dim,1)*muproj)+cmax.max.*(ones(dim,1)*cpproj);

