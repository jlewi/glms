%function RandStimNorm('mmag',val,'klength', klength)
%
% Explanation: Object which picks stimuli randomly from a sphere of radius
% mmag. Parameters are passed in as 'name', value pairs. The inputs are the
% magnitude to which all stimuli should be normalized and klength is the
% dimensionality of the stimuli.
%
%Revision:
%   01-08-2009 - convert to new object model
classdef (ConstructOnLoad=true) RandStimNorm<StimChooserObj

    properties(SetAccess=private,GetAccess=public)
        mmag=0;
        klength=0;
    end

    methods
        function [obj]=RandStimNorm(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={'mmag','klength'};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=rmfield(params,{'mmag','klength'});
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@StimChooserObj(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    obj.mmag=params.mmag;
                    obj.klength=params.klength;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %set the version to be a structure
            obj.version.RandStimNorm=090108;

            %

        end

    end
end
