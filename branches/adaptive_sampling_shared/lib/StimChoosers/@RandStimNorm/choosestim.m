%function [stim]=choosestim(obj,varargin)
%   varargin - whatever parameters are passed in don't matter
%               varargin is used so that the code doesn't have to change
%               depending on the method called
%
%Explanation: chooses stimuli by generating them randomly from the unit
%sphere of radius mmag
function [stim,oreturn,obj,varargout]=choosestim(obj,varargin)

   x=normrnd(0,1,obj.klength,1);

            %set the first d stimuli to be indepenent, orthogonal vectors (just choose unit vectors
            xmag=x.*x;
            xmag=sum(xmag,1);
            normconst=obj.mmag/xmag^.5;
            stim=normconst*x;
            
            oreturn=[];
            
                  %the following allows other choosestim functions to return more
    %arguments
    if (nargout>3)
        for j=1:(nargout-3)
            varargout{j}=[];
        end
    end