%function [thetadir,thetaproj]=samptheta(obj,post,nsdir,nsproj)
%   nsdir - how man samples of the direction of theta to use
%   nsproj - how many samples of the magnitude to draw
%
% Return value
%   thetadir - dim(theta)xnsdir - each column is a different sample
%            for the direction of theta
%   thetaproj - nsprojxnsdir - each column is the different samples
%               of the projection of theta along the corresponding
%               direction.
%
%Explanation: Draw samples from the posterior on theta which can then be
%used to approximate the expectation of our objective function over theta
function [thetadir,thetaproj]=samptheta(obj,post,nsdir,nsproj)


theta=mvgausssamp(getm(post),getc(post),nsdir);

thetadir=normmag(theta);

thetaproj=zeros(nsproj,nsdir);
for dind=1:nsdir
    %for each sample of theta, draw nsproj samples of the projection
    v=(thetadir(:,dind)'*getinvc(post)*thetadir(:,dind));
    mu=(getm(post)'*getinvc(post)*thetadir(:,dind))/v;
    
   thetaproj(:,dind)=normrnd(mu,v,nsproj,1);
    
end