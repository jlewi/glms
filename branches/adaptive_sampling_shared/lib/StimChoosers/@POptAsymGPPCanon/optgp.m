%function optgp(obj,post,pcon,nsdir,nsproj)
%   post - posterior on theta
%   pcon - value of the power constraint - used to constrain the variance
%           and mean of the GP
%   nsdir - how many samples of the direction of theta to use to comptue
%           the objective function
%   nsproj - how many samples of the magnitude to use to comptue the
%             objective function
%
%function optgp(obj,post,pcon,thetadir,thetaproj,gpinit)
%
%   thetadir - dim(theta)xnsdir - each column is a different sample
%            for the direction of theta
%   thetaproj - nsprojxnsdir - each column is the different samples
%               of the projection of theta along the corresponding
%               direction.
%
%   gpinit (optional) - initial guess for the gaussian process
%Return value:
%   fval - This is the negative of the funciton we want to maximize because
%           we are using fmincon but we want to find the maximum.
%Explanation: Use fmincon to find the optimal Gaussian process
function [gopt,fval,exitflag,output,thetadir,thetaproj]=optgp(obj,post,pcon,param4,param5,gpinit)


if ~exist('gpinit','var')
    gpinit=[];
end

%generate samples of theta
%we use the same samples on all iterations of fmincon because otherwise
%changing the samples will cause the value of the function to change
if (isscalar(param4) && isscalar(param5))
    [thetadir,thetaproj]=samptheta(obj,post,param4,param5);
    nsdir=param4;
    nsproj=param5;
elseif (~isscalar(param4) && size(param5,2)==size(param4,2))
    thetadir=param4;
    thetaproj=param5;
    
    nsdir=size(thetadir,2);
    nsproj=size(thetaproj,1);
else
    error('must either specify nsdir and nsproj or thetadir and thetaproj');
end
    
A=[];
b=[];
Aeq=[];
beq=[];
lb=[];
ub=[];

%initialize gp to a white gaussian process
if isempty(gpinit)
mu=zeros(obj.klength);
c=cell(1,obj.ktlength);

c{1}=eye(obj.klength,obj.klength)*pcon/obj.klength;
for cind=2:obj.ktlength
   c{cind}=zeros(obj.klength,obj.klength); 
end

gpinit=StimGP('mu',mu,'c',c);
end
xinit=gptovec(obj,gpinit);

[xopt, fval,exitflag,output]= fmincon(@(x)(ofun(obj,post,x, thetadir, thetaproj)),xinit,A,b,Aeq,beq,lb,ub,@(x)(nonlincon(obj,x,pcon)));
gopt=vectogp(obj,xopt);

%the function to minimize
function f=ofun(obj,post,x, thetadir, thetaproj)
    gp=vectogp(obj,x);
    ex=exlogdetexss(obj,post,gp, thetadir, thetaproj);
    f=-1*ex;
    

%evaluate the nonlinear equality conditions
function [c,ceq]=nonlincon(obj,x,pcon)
    gp=vectogp(obj,x);
    
    p=power(gp,pcon);
    
    c=p;
    
    pd=posdef(gp);
    
    if (pd)
        ceq=0;
    else
        ceq=-1;
    end
    
    
    

%from the vectors of parameters construct the gaussian process
function gp=vectogp(obj,x)
    dimx=obj.klength;
    mu=x(1:dimx,1);
    
    c=cell(1,obj.ktlength);
    
    rs=dimx+1;
    re=rs+(dimx^2+dimx)/2-1;
        
    lind=obj.v0tulind;
    c1=zeros(obj.klength,obj.klength);
    c1(lind)=x(rs:re);
    c{1}=c1+triu(c1,1)';
    
    for cind=2:obj.ktlength
        rs=dimx+(dimx^2+dimx)/2+(cind-2)*dimx^2+1;
        re=rs+dimx^2-1;
        c{cind}=reshape(x(rs:re),obj.klength,obj.klength);
    end
    
    gp=StimGP('mu',mu,'c',c);
    
    
    
%form the vectors of parameters from a gaussian process
function x=gptovec(obj,gp)
    dimvec=obj.klength+(obj.klength^2+obj.klength)/2+obj.klength^2*(obj.ktlength-1);
    
    x=zeros(dimvec,1);
    
    x(1:obj.klength)=gp.mu;
    
    rs=obj.klength+1;
    re=rs+(obj.klength^2+obj.klength)/2-1;
    c0=gp.c{1};
    x(rs:re)=c0(obj.v0tulind);
    
    for cind=2:obj.ktlength
         rs=obj.klength+(obj.klength^2+obj.klength)/2+(cind-2)*obj.klength^2+1;
         re=rs+obj.klength^2-1;
         c=gp.c{cind};
         x(rs:re)=c(:);
    end


%check if the variance of the gaussian process satisifis the power
%constraint
%this should be less than 0. 
function v=power(gp,pcon)
    p=sum(diag(gp.c{1}))+gp.mu'*gp.mu;
    
    v=p-pcon;

%check if the covariance matrix of the gp is positive definite
function v=posdef(gp)
    %compute the eigenvalues 
       %DO NOT USE SVD b\c SVD can return positive "singular values"
        %but U!=V (i.e the eigenvalue is negative and the negative sign is
        %absorbed intothe V matrix);
    ev=eig(gp.cs);
    
    if (any(ev<=0))
        v=false;
    else
        v=true;
    end
    