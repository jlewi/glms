%function [obj, stim, shist, obsrv,stimindex]= choosetrial(obj, simobj);
%   obj - A BSOptStim object
%   simobj - a BSSim object
%
%Explanation: 
%   stim - vector representing the stimulus
%   shist - vector of the spike history 
%   obsrv - the response
%   stimindex=3 x1 vector indicating the trial corresponding to the
%   stimulus
%           = we use this to keep track of the order the stimuli were
%           chosen in
%       stimindex(1,1) - windex of the stimulus
%       stimindex(2,1) - the repeat of the wfile
%       stimindex(3,1) - the relative trial within the 
%
%Explanation: Select the trial by uniformly sampling all trials not
%presented. We do this in a two step process
%   first we choose the wave file
%   then we choose the trial
function [stim, shist, obsrv,stimindex]= choosetrial(obj, simobj,post)

windexes=obj.windexes;

%code assumes only 1 repeat
if (obj.nrepeats>1)
    error('code needs to be changed to allow repeats >1');
end
repeat=1;
%***********************************************************************
%select the wave file
%*************************************************************************
%we randomly select the wave file by weighting 
%each wave file by what fraction of the remaining trials it contains
prob=obj.ntrialsleft./sum(obj.ntrialsleft);

%find those elements which have more than 1 trial left
indnz=find(obj.ntrialsleft>0);
prob=prob(indnz);
windexes=obj.windexes(indnz);

cprob=cumsum(prob);
rnum=rand(1,1);

ind=binarysubdivide(cprob,rnum);

windex=windexes(ind);

%decrement the number of trials remaining for this wave file
wind=find(obj.windexes==windex);
obj.ntrialsleft(wind)=obj.ntrialsleft(wind)-1;

%*********************************************************************
%Select the actual trial
%**********************************************************************
ntrialsinwave=getntrialsinwave(obj.bdata,obj.windexes(wind));

%figure out which trials remain
tnotselected=ones(1,ntrialsinwave);

%find past trials when we presented this wavefile
indpast=find(simobj.paststim(1,1:simobj.niter)==windex);
pasttrials=simobj.paststim(3,indpast);

tnotselected(pasttrials)=0;

tnotselected=find(tnotselected==1);

rnum=rand(1,1)*length(tnotselected);

trial=tnotselected(ceil(rnum));


%**********************************************************************
%select the stimulus
%***********************************************************************
[stim,shist,obsrv]=gettrialwfilerepeat(obj.bdata,windex,repeat,trial,simobj.mobj);

stimindex=[windex;repeat;trial];