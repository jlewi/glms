%function sim=TanSpaceInfoMax()
%   nstim - number of stimuli to select for the stimulus pool
%
% Explanation: Maximize the information about some tangent space and for
% the canonical PoissonModel.
%
function obj=TanSpaceQuadInpInfoMaxCanon(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={''};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%  stimobj  - stimobj - object to actually maximize the stimulus
%  iterfullmax - how often we do an infomax step using the full posterior
%  startfullmax - how many trials at start do we do full info. max.
%                 before switching to the tangent space.
obj=struct('version',080124,'stimobj',[],'iterfullmax',inf,'startfullmax',0);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'TanSpaceQuadInpInfoMaxCanonFeature',PoissExpMaxMI());
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=PoissExpMaxMI();
%if no base object
obj=class(obj,'TanSpaceQuadInpInfoMaxCanonFeature',pbase);

if (isfield(params,'iterfullmax'))
    obj.iterfullmax=params.iterfullmax;
end

if (isfield(params,'startfullmax'))
    obj.startfullmax=params.startfullmax;
end



    