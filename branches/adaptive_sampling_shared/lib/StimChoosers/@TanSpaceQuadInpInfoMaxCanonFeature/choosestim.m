%function stim=choosestim(sobj,simobj,trial)
%       sobj - the stimulus object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3
%Explanation: maximize the mutual information for the canonical poisson
%model
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mparam,varargin)
oreturn=[];
error('3-11-2008 this code has not been updated to use new method for computing posterior on the tangent space.');

if (strcmp(varargin{1},'trial')==1)
    trial=varargin{2};
else
    error('Trial was not passed in.');
end

%check whether we are doing infomax on the full posterior or tangent space
if ((isinf(sobj.iterfullmax) || mod(trial,sobj.iterfullmax+1)~=0) && (trial>sobj.startfullmax))

    pinfo=gettanpost(post);
 
else

    %info. max on full space set posterior to full posterior
    pinfo=getfullpost(post);
    
   rank=getstimlen(mparam);
end

%choose the stimulus by calling choosestim on the base class using 
%pinfo as our posterior.
mopt=mparam;
%the power constraint in feature space is the magnitude in input space
%squared. This is not exact just an approximation
mopt=setmmag(mopt,getmag(mparam)^2);
[optfeat,oreturn,sobj.PoissExpMaxMI,varargout]=choosestim(sobj.PoissExpMaxMI,pinfo,shist,mopt,varargin);

%project the features back into input space
[stimpool s]=svd(qmatquad(getninpobj(mparam),optfeat));

%multiply u by the magnitude 
stimpool=getmag(mparam)*stimpool;

%compute the mutual info for each column of u and select the one which
%maximizes the information.
miobj=PoissExpCompMI();

%psuh the inputs through the nonlinearity
stimnonlin=projinp(mparam,stimpool,shist);
muproj=getm(pinfo)'*stimnonlin;


sigma=getc(pinfo)*stimnonlin;
sigma=stimnonlin.*sigma;
sigma=sum(sigma,1);

mi=compmi(miobj,muproj,sigma);

%maximize the mi
[maxmi indmax]=max(mi);
xmax=stimpool(:,indmax);

oreturn.indmax=indmax;
%compute the percent of the energy that is the first singular value. 
s=diag(s);
oreturn.epercent=s(1)^2/(sum(s.^2));
