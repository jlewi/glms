%function getstartfullmax=startfullmax(obj)
%	 obj=TanSpaceInfoMaxCanon object
% 
%Return value: 
%	 startfullmax=obj.startfullmax 
%
function startfullmax=getstartfullmax(obj)
	 startfullmax=obj.startfullmax;
