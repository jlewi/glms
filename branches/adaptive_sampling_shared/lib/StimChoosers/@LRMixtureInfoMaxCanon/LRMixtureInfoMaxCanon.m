%constructor
%
% Explanation: Perform infomax for a low rank miture model which has the
% canonical glm.

function obj=LRMixtureInfoMaxCanon(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={};
con(1).cfun=1;


%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%  bname  - name of base class. This allows us to refer to it templates
%declare the structure
obj=struct('version',071119);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        pbase=PoissExpMaxMI();
        obj=class(obj,'LRMixtureInfoMaxCanon',pbase);
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
    
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=PoissExpMaxMI();
obj=class(obj,'LRMixtureInfoMax',pbase);





    