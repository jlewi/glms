%function sim=ClassName(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Constructor for choosing pool stimuli when the model has an
% input transforming the input. The constructor projects all of the stimuli
% in the pool into the higher space. THis is memory intensive but should
% speedup the choosing stimuli step
%
% Constructors:
% PoolstimNonLinInp('Model',mobj,'fname',fname)
%   'Model' - this is used to precompute the projection of the stimuli
%             it is not SAVED.
%           - we only need this if we aren't selecting the stimuli randomly
%           -must call this constructor if using info. max sitmuli
%   'fname' - file containing the pool of stimuli
%
%Return value
%   sfactor - amount by which stimuli are scaled.

function [obj,sfactor]=PoolstimNonLinInp(varargin)

sfactor=1;
%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'model'};
con(1).cfun=1;

con(2).rparams={'rand'};
con(2).cfun=2;

%**********************************************************
%Define Members of object
%**************************************************************
% projinp - the input projected into the higher dimensional space
%         - depreceated in version 070511
%         - the projected stimulus is now stored in stimpool of the base
%         class
% version - added in version 070511 to store version info
% stimpool- added in version 070511
%           this stores the actual stimulus i.e before projecting into a
%           higher space
% ninpobj   - added version 070601
%           stores the object response for the input transformation
%ninpfunc - depreciated version 070601 
%         -store the function used to transform the inputs
%fparam   - depreciated version 070601
%           parameters for function transforming the inputs
%
%onshist  - true - means the input nonlinearity acts on the stimulus
%                  and spike history. this limits what we can do
%         - false - only operates on stimulus
%                 - default value
%         -added in version 070511
%declare the structure
obj=struct('ninpobj',[],'version','070601','stimpool',[],'onshist',false);


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    pbase=Poolstim();
    obj=class(obj,'PoolstimNonLinInp',pbase);
    return
elseif(nargin==1)
    %loading params from subclass
    params=varargin{1};
else
    
%convert varargin into a structure array

params=parseinputs(varargin);
end


%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    ismatch=1;
    for f=1:length(con(j).rparams)
        %check if  parameter was passed in
        if isempty(strmatch(con(j).rparams{f},pnames))
            ismatch=0;
            break;
        end
    end
    if (ismatch==1)
        cind=[cind j];
    end
end

if isempty(cind)
    error('Constructor:missing_arg','Required parameters for one of the constructors was not passed in');
elseif (length(cind)>1)
    %resolve multiple matches
    %call constructor 1 if constructors 1 & 2 match
    cind=1;
    %error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        %check the model parameter is  of MGLMNonLinInp
        if ~isa(params.model,'MGLMNonLinInp')
            error('Model needs to be derived from MGLMNonLinINp');
        end
        mobj=params.model;
        %remove this field so we can pass remaining fields to constructor
        %for base class
        params=rmfield(params,'model');

        %obj.ninpfunc=mobj.ninpfunc;
        %obj.fparam=mobj.fparam;
        obj.ninpobj=mobj.ninpobj;

        %determine whether nonlinearity acts on the spike history
        %assume it does if there are spike history terms
        if (getshistlen(mobj)>0)
            pobj.onshist=true;
        end
    case 2
        if (params.rand==0)
            error('wrong constructor called for selecting info. max stimuli.');
        end
    otherwise
        error('Constructor not implemented')
end

srescale=[];
%5-24-07
%for nonlinear inputs we still still need srescale. This should be properly
%handled by the call to procells pool by loadstim.
%if isfield(params,'stimrescale')
%    srescale=params.stimrescale;
    %remove the field so its not passed to the base class
%    params=rmfield(params,'stimrescale');
%end

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=Poolstim(params);
obj=class(obj,'PoolstimNonLinInp',pbase);





%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case {'fname','filename'}
            params.('fname')=vars{j+1};
        otherwise
            params.(vars{j})=vars{j+1};
    end
end


