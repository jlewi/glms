%function getstimpool=stimpool(obj)
%	 obj=PoolstimNonLinInp object
% 
%Return value: 
%	 stimpool=obj.stimpool 
%
%     We override getstimpool because we want to get the actual stimuli
%     not the stimuli after projecting into a higher space
function stimpool=getstimpool(obj)
	 stimpool=obj.stimpool;
