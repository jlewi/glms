%function stim=choosestim(sobj,post,shist,einfo,mparam,varargin)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
% Return value
%   xmax -
%   oreturn
%       .sind - index of the stimulus in the stimulus pool
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3 
%Explanation: maximize the mutual information for the canonical poisson
%model
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,miobj,varargin)
oreturn=[];

%load stimuli if not already loaded
sobj=loadstim(sobj);

nstim=size(sobj.stimpool,2);
%if its random stimulus we're selecting then we didn't bother to compute
%the projection of the input
if (sobj.Poolstim.infomax==false)
   %randomly select stimulus
   sind=ceil(rand(1)*nstim);
   oreturn.sind=sind;
else   
    %infomax
%**************************************************************************
%1. Call choosestim on the base class
%       -the base class treats the stimulus after pushing through the
%       nonlinearities as the input

if (nargout<=3)
[xmax,oreturn,sobj.Poolstim]=choosestim(sobj.Poolstim,post,shist,miobj,varargin);
elseif (nargout==4)
[xmax,oreturn,sobj.Poolstim, varargout{1}]=choosestim(sobj.Poolstim,post,shist,miobj,varargin);
end
end
%*****************************************************************
%2. return the actual stimulus
%       -xmax currently is the stimulus after pushing through the
%       nonlinearity
%       -we set it to the actual stimulus
%******************************************************************
stimpool=getstimpool(sobj);
xmax=stimpool(:,oreturn.sind);

