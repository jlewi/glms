% Explanation: Template function for dereferencing a user defined object
%
%   obj - pointer to object
%   index -
%           .type 
%           .subs
% 2-2-07:
%   check if field is member of base class.
function [out] = subsref(obj,index)

% SUBSREF Define field name indexing for GLModel objects
switch index(1).type
    case '.'
        
        if (strcmp(index(1).subs,'stimpool'))
           obj=loadstim(obj); 
        end
        if (strcmp(index(1).subs,'Poolstim'))
            keyboard
           %if we were accessing the stimpol of the base class
           %wee need to call loadstim. Otherwise if we let Poolstim.subsref
           %call load stim the stimuli won't be properly scaled
           if (size(index,2)>1)
              if (strcmp(index(2).subs,'stimpool'))
                %load the stimuli
                obj=loadstim(obj);
              end
            end
        end
        %*************************************************
        %Special Handlers
        %***************************************************
        if (strcmp(index(1).subs,'glm'))
            %glm is treated specially by the base class
            if (size(index,2)==1)
                %only 1 level of indexing is provided
                out=obj.Poolstim.(index(1).subs);
            else
                %call the indexing on the appropriate field
                fname=index(1).subs;
                %recursively process any further dereferencing
                out=subsref(obj.Poolstim.(fname),index(2:end));
            end
        else
        %*************************************************
        %Generic Handlers
        %***********************************************
        %check if its field of base class
        if ~isempty(strmatch(index(1).subs,fieldnames(obj)))
            %field is member of this structure
            if (size(index,2)==1)
                %only 1 level of indexing is provided
                out=obj.(index(1).subs);
            else
                %call the indexing on the appropriate field
                fname=index(1).subs;
                %recursively process any further dereferencing
                out=subsref(obj.(fname),index(2:end));
            end

        else
            %check if its a member of the base class
            if ~isempty(strmatch(index(1).subs,fieldnames(obj.Poolstim)))
                fname=index(1).subs;
                out=subsref(obj.Poolstim,index);
            end
        end
        end
end