%function stimpool=getstimpool(sobj)
%   sobj - stim chooser object
%
%Return value:
%   stimpool - the stimuli in the pool
function stimpool=getstimpool(sobj)

sobj=loadstim(sobj);
    stimpool=sobj.stimpool;