%function infomax=getinfomax(sobj)
%   sobj - PoolStim object
%
%Return value:
%   infomax - value of infomax
function infomax=getinfomax(sobj)
    infomax=sobj.infomax;