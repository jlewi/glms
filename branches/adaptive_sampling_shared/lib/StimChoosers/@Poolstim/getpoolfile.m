%function getpoolfile=poolfile(obj)
%	 obj=Poolstim object
% 
%Return value: 
%	 poolfile=obj.poolfile 
%
function poolfile=getpoolfile(obj)
	 poolfile=obj.poolfile.fname;
