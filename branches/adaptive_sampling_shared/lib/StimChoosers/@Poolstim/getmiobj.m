%function getmiobj=miobj(obj)
%	 obj=Poolstim object
% 
%Return value: 
%	 miobj=obj.miobj 
%
function miobj=getmiobj(obj)
	 miobj=obj.miobj;
