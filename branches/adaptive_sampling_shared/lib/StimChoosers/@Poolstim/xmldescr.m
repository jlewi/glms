%function node =xmldescr(sobj,docnode)
%   docnode - document object 
%
% Return value
%   node - node used to represent this object
%Explanation create an xml description for this object
function simparnode=xmldescr(sobj,docnode)
    simparnode=docnode.createElement('simparam');
    simparnode.setAttribute('name','Stim');
    %simparnode.setTextContent(class(sobj));

    %add an object node to describe this object
    onode=docnode.createElement('object');
    onode.setAttribute('name',class(sobj));
    
    oparam=docnode.createElement('simparam');
    oparam.setAttribute('name','nstim');
    oparam.setTextContent(num2str(sobj.nstim));
    
    onode.appendChild(oparam);
    
    
    if ~isempty(sobj.srescale);
    oparam=docnode.createElement('simparam');
    oparam.setAttribute('name','sfactor');
    oparam.setTextContent(num2str(sobj.srescale.sfactor));
    onode.appendChild(oparam);
    end
    oparam=docnode.createElement('simparam');
    oparam.setAttribute('name','resample');
    oparam.setTextContent(num2str(sobj.resample));
    onode.appendChild(oparam);

    oparam=docnode.createElement('simparam');
    oparam.setAttribute('name','infomax');
    if (sobj.infomax)
        oparam.setTextContent('true');
    else
        oparam.setTextContent('false');
    end
    onode.appendChild(oparam);

    simparnode.appendChild(onode);
    
    