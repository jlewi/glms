%function err=checkfileid(sobj)
%
%Explanation: Verifies that the fileid stored in the object matches that in
%the file.
function err=checkfileid(sobj)

 %check the fileid hasn't changed
    %this fileid provides a check to ensure the file containing the
    %stimulus pool hasn't changed since we first ran a simulation.
    if isa(getpoolfile(sobj),'FilePath')
        fname=getpath(getpoolfile(sobj));
    else
        fname=getpoolfile(sobj);
    end
    
    fileid=load(fname,'fileid');
    %only match a fileid if it has been set
    if ~isnan(sobj.poolfile.fileid)
        if (fileid.fileid ~= sobj.poolfile.fileid)
            error('File id for file containing stimuli does not match file id in file \n');
        end
    end