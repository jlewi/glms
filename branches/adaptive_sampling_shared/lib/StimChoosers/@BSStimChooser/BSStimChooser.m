%classdef BSStimChosoer
%
%Explanation: Provide an abstract base class for all bird song stimulus
%chooser
%
%
%Revisions
%   080828
%      Added the rollback function to the interface.
classdef (ConstructOnLoad=true) BSStimChooser < handle
    
    properties(SetAccess=protected,GetAccess=public)
        bversion=080828;
    end
    
    methods
        
        function obj=BSStimChooser()
            
        end
    end
    methods(Abstract)
        
       [stim, shist, obsrv,stimindex]= choosetrial(obj, simobj,post) 
       
       %Function to rollback the stimulus chooser to an earlier trial
       [stimobj]=rollback(stimobj,simobj,trial);
    end
end