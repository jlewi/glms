%function stim=choosestim(sobj,post,shist,einfo,mparam)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
% varargin ('ctrial',ctrial,poolparam)
%            poolparam.oldcovar - covariance from t-1
%            poolparam.oldglmproj=\mu_t'*\stim_t
%            poolparam.oldobsrv=\obsrv_t);
%            poolparam.oldstim=\stim_t;
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3
%Explanation: maximize the mutual information for the canonical poisson
%model
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mparam,varargin)
oreturn=[];

ctrial=varargin{2};
poolparam=varargin{3};
%load the stimulus pool if its not already loaded

if isempty(sobj.Poolstim.stimpool);
    fprintf('Loading stimpool from file \n');
    %check the fileid hasn't changed
    %this fileid provides a check to ensure the file containing the
    %stimulus pool hasn't changed since we first ran a simulation.
    fileid=load(sobj.Poolstim.poolfile.fname,'fileid');
    if (fileid.fileid ~= sobj.poolfile.fileid)
        error('File id for file containing stimuli does not match file id in file \n');
    end
    [sobj.Poolstim]=processpool(sobj.Poolstim,'file',sobj.Poolstim.poolfile.fname)
end

%*****************************************************************
% 2 possibilities
%1) select stimuli randomly from the pool
%2) select stimuli from pool which maximizes the mutual info
%******************************************************************
%nstim=size(sobj.stimpool,2);
nstim=sobj.Poolstim.nstim;
if (sobj.Poolstim.infomax==0)
    %randomly select stimulus

    sind=ceil(rand(1)*nstim);
    xmax=sobj.Poolstim.stimpool(:,sind);
    oreturn.sind=sind;
else
    %if we resample the stimuli to construct our pool
    %compute the mi for each stimulus
    %concatenate the stimulus onto the spikehistory

    if ~isempty(shist)
        error('Rank1 only works if spike history is empty');
    end
    stim=[sobj.Poolstim.stimpool];
    stimind=[];

    %compute muproj and sigma
    muproj=post.m'*stim;

    %if sigmalast is empty then we have to recompute it
    %otherwise we can compute it efficiently because its a rank 1
    %update
    if (isempty(sobj.sigmalast) | isempty(poolparam))
        sigma=post.c*stim;
        sigma=stim.*sigma;
        sigma=sum(sigma,1);
    else
        %we need to compute x_i c_t-1 x_t for all i
        dotold=poolparam.oldcovar*poolparm.oldstim;

        %compute cross sigma
        csigma=dotold'*stim;

        sigmat=poolparam.oldstim'*poolparam.oldcovar*poolparam.oldstim;

        %we need the second derivate
        d2eps=d2glmeps(poolparam.oldstim'*poolparam.oldglmproj,poolparam.oldobsrv,mparam.glm)
        %compute the change in sigma for each stimulus
        csquare=(csigma).^2;
        sigma=sigmalast-(csquare)*d2eps/(1+d2eps*sigmat);

    end

    if (sobj.Poolstim.StimChooserObj.debug)
        fprintf('Verifying Rank 1 computation of Sigma \n');
        correct.sigma=post.c*stim;
        correct.sigma=stim.*correct.sigma;
        correct.sigma=sum(correct.sigma,1);
        if ~isempty(find(abs(correct.sigma-sigma)>10^-8))
            error('Rank 1 computation of sigma does not appear to be correct');
        else
            fprintf('Rank 1 computation is correct \n');
        end
    end
    mi=compmi(sobj.Poolstim.miobj,muproj,sigma);

    %maximize the mi
    [maxmi indmax]=max(mi);

    xmax=sobj.Poolstim.stimpool(:,indmax);
    oreturn.sind=indmax;
end

