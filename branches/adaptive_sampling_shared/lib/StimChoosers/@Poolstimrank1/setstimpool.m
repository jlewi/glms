%function obj=setstimpool(obj,stim)
%   stim - matrix representing the pool of stimuli
%%we need to modify the stimpool in order to allow
                %PoolStimNonLinInp to rescale the stimuli in the
                %constructor               
function obj = setstimpool(obj,stim)
obj.stimpool=stim;
