%function stim=Poolstimrank1('fname',fname,)
%   fname - file contianing 'stim' matrix which contains the possible
%   stimuli
%       stimuli are one per column
%
%   stimuli are selected randomly
%
%Constructors: see constructors for Poolstim
%   
% Explanation: 
%       Regular pool sampling. No resampling. Computes the updated
%       covariance for each stimulus efficiently as a rank 1 update.
%Return value
%
%   sfactor - amount by which stimuli are scaled.
%
%
% 4-17-07
%   added fields resample and nstim
function [obj]=Poolstimrank1(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
%basic constructor: exactly the same as constructors for poolstim
con(1).rparams={};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% degree          - description
% coef              -coefficients for the fitted surface

%declare the structure
%   sigmalast - this stores \stim'\covar[t]\stim from the previous
%   iteration
%
obj=struct('sigmalast',[]);


%convert varargin into a structure array
if (nargin==0)
    params=[];
elseif (nargin==1)
    params=varargin{1};
else
    params=parseinputs(varargin);
end
%****************************************************
%Set the parameters for the base class
%****************************************************
params.resample=false; %we don't do any resampling

%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    obj=class(obj,'Poolstimrank1',Poolstim(params));
    return;
end


%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    ismatch=1;
    for f=1:length(con(j).rparams)
        %check if  parameter was passed in
        if isempty(strmatch(con(j).rparams{f},pnames))
            ismatch=0;
            break;
        end
    end
    if (ismatch==1)
        cind=[cind j];
    end
end

if isempty(cind)
    error('Constructor:missing_arg','Required parameters for one of the constructors was not passed in');
elseif (length(cind)>1)
    %we might be able to resolve the constructor if one constructor has
    %more parameters then the other
    %if it matches constructor 2 it also matches constructor 1 but we
    %should call constructor 2.
    cind=2;
    %    error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
%instantiate a base class if there is one
pbase=Poolstim(params);
switch cind
    case 1
        %just call the base constructor  
        obj=class(obj,'Poolstimrank1',pbase);
    otherwise
        error('Constructor not implemented')
end




%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case {'fname','filename'}
            params.('fname')=vars{j+1};
        otherwise
            params.(vars{j})=vars{j+1};
    end
end


