%function obj=saveobj(obj)
%
% Preprocessing before saving object:
%   1) empty the stimpool matrix so we don't save this to the file
%
%   Following processing is performed
%       1.) construct the stimulus pool from the file containing the
%       stimuli
function obj=saveobj(obj)
    %make sure a filename is specified
    if ~isempty(obj.poolfile)
        if ~isempty(obj.poolfile.fname)
            obj.stimpool=[];
        end
    end
    