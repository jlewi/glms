%function sim=BSStimReplay('simfile',fname)
%   simfile - the name of a file containing a simulation whose data we want
%   to replay
%
%   bdata - bdata isn't actually needed by BSStimReplay but we need it to
%   be compatible with my existing code.
% Explanation: This class replays the trials in the order they were
% selected 
%
%
%Revisions:
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) BSStimReplay <BSStimChooser
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        version=081122;
        
        %this is the name of the file containing the simulation from which
        %we loaded paststim
        oldstimfile=[];

        %stimorder this is the order in which the trials are to be
        %presented
        stimorder=[];
        
        bdata=[];
        windexes=[];
        nrepeats=[];
    end

    
    methods
        function obj=BSStimReplay(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'simfile','bdata'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     obj.oldstimfile=params.simfile;
                     v=load(getpath(params.simfile));
                     obj.stimorder=v.bssimobj.paststim;
                     
                     obj.bdata=params.bdata;
                     obj.windexes=v.bssimobj.stimobj.windexes;
                     obj.nrepeats=v.bssimobj.stimobj.nrepeats;
                     
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


        end
        
         %Return Value:
        %     ntrials - the number of trials we have data for
        function ntrials=getntrials(obj)
            %we need to compute the number of trials in the allowed indexes
            nwindex=length(obj.windexes);

            ntrials=0;
            for wind=1:nwindex

                [ntrialsinwave]=getntrialsinwave(obj.bdata,obj.windexes(wind));
                ntrials=ntrials+ntrialsinwave*obj.nrepeats;
            end

        end
    end
end


