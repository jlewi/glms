%function sim=QuadInpHeur(fielname,value,...)
%   fieldname, value - pairs of fields and values used to initialize the object
%
%
% 'modheur' - text identifying which heuristic to use for modifying the
% pool
%       'muorig' - original heuristic
%           -all stimuli use same eigenvector
%       'mu' - vary the eigenvector for each sample
%       'musigma' - try to optimize for sigma as well.
%
% Explanation: Constructor for choosing pool stimuli.
%   This function adjusts the stimulus pool so that the projection on the
%   mean is a random sample in between the maximum and minimum possible
%   values.
%
% Constructors:
% PoolstimNonLinInp('Model',mobj,'fname',fname)
%   'Model' - this is used to precompute the projection of the stimuli
%             it is not SAVED.
%           - we only need this if we aren't selecting the stimuli randomly
%           -must call this constructor if using info. max sitmuli
%   'fname' - file containing the pool of stimuli
%
%Return value
%   sfactor - amount by which stimuli are scaled.
%
%Revisions
%   071025 - added support for additional heuristics for modifying the
%   stimuli
function [obj,sfactor]=PoolStimQuadInpHeur(varargin)

sfactor=1;
%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'magcon'};
con(1).cfun=1;

%con(2).rparams={'rand'};
%con(2).cfun=2;

%**********************************************************
%Define Members of object
%**************************************************************
% magcon  - magnitude constraint
%           all stimuli have this magnitude
%           this is also used to determine the values of the projection
%           along muproj.
% version - added in version 070525 to store version info
% modheur - pointer to which function we want to use to modify the
% heuristic
obj=struct('version','071025','magcon',[],'bname','PoolstimNonLinInp','modheur',@modstimpoolmuvary);


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    pbase=PoolstimNonLinInp();
    obj=class(obj,'PoolstimQuadInpHeur',pbase);
    return
end

%convert varargin into a structure array
params=parseinputs(varargin);

%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    ismatch=1;
    for f=1:length(con(j).rparams)
        %check if  parameter was passed in
        if isempty(strmatch(con(j).rparams{f},pnames))
            ismatch=0;
            break;
        end
    end
    if (ismatch==1)
        cind=[cind j];
    end
end

if isempty(cind)
    error('Constructor:missing_arg','Required parameters for one of the constructors was not passed in');
elseif (length(cind)>1)
    %resolve multiple matches
    %call constructor 1 if constructors 1 & 2 match
    cind=1;
    %error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        obj.magcon=params.magcon;
        params=rmfield(params,'magcon');
        if (obj.magcon<=0)
            error('magcon must be >0');
        end
    otherwise
        error('Constructor not implemented')
end

srescale=[];
if isfield(params,'stimrescale')
    srescale=params.stimrescale;
    %remove the field so its not passed to the base class
    %b\c we don't want to rescale the stimuli
    fprintf('stimrescale ignored \n');
    params=rmfield(params,'stimrescale');
end

if isfield(params,'modheur')
    switch lower(params.modheur)
        case 'mu'
            obj.modheur=@modstimpoolmuvary;
        case 'muorig'
            obj.modheur=@modstimpoolmu;
        case 'musigma'
            obj.modheur=@modstimpoolmusigma;
        otherwise
            error ('unrecognized value for the modification heuristic');
    end
    params=rmfield(params,'modheur');
end

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=PoolstimNonLinInp(params);
obj=class(obj,'PoolstimQuadInpHeur',pbase);




%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case {'fname','filename'}
            params.('fname')=vars{j+1};
        otherwise
            params.(vars{j})=vars{j+1};
    end
end


