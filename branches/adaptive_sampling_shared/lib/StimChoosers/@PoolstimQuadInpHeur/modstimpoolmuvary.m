%function [stim,sobj]=modstimpoolmu(sobj,post,shist,mparam)
%   sobj - PoolStimQuadInpHeur
%   post - posterior
%   
%
%Return value:
%   stim - set of stimuli 
%
%Explanation this function uses a heuristic to create a set of stimuli
%   which will likely span the feasible region in mglmproj,sglmproj space
%
%   This heuristic just looks at getting mu to span the full
%   range.
function [stimpool,sobj]=modstimpoolmuvary(sobj,post,shist,mobj)

dim=getstimlen(mobj);
psize=getpoolsize(sobj);


bname=sobj.bname;   %name of base class;
sobj.(bname)=loadstim(sobj.(bname));

%compute the vector in stimulus space most correlated with the mean of the
%posterior
%get a quadratic matrix
%i.e returna matrix qmat such that x'*qmat*x;
m.mat=qmatquad(getninpobj(mobj),getm(post));
%due an svd 
[m.evecs m.eigd]=svd(m.mat);

%pull out the max eigenvector
%version:070531 - instead of always selecting 
%   the max eigenvector select one of the principal components
%   sample the principal components in proprotion to the relative energy as
%   measured by the svd.
m.eigd=diag(m.eigd);
%normalize the energy of the singular values
neigd=m.eigd/sum(m.eigd);


%each eigenvector appears a number of times proportional to probability
%of its eigenvalue.
m.emax=zeros(dim,psize);
sind=1;
for j=1:dim
    eind=sind+floor(neigd(j)*psize)-1;
   m.emax(:,sind:eind)=m.evecs(:,j)*ones(1,eind-sind+1); 
   sind=eind+1;
end

%any leftover due to rounding error we set to max evector
if (eind<psize);
   m.emax(:,eind+1:end)=m.evecs(:,1)*ones(1,psize-eind); 
end

%randomly generate the projections along the mean
muproj=rand(1,sobj.(bname).nstim)*2*getmagcon(sobj)-getmagcon(sobj);

%adjust the stimuli accordingly 
%1. remove projection along the max eigenvector correlated with the mean
stimpool=getstimpool(sobj);

vset=m.emax.*(ones(dim,1)*muproj);
venergy=sum(vset.^2,1);

%1. remove projection along the max eigenvector correlated with the mean
stimpool=stimpool-(ones(dim,1)*(sum(stimpool.*vset,1)./venergy)).*vset;


%2. scale the stimuli so that all energy not in muproj is along current
%direction
energy=sobj.magcon^2-muproj.^2;

%normalize
mag=sum(stimpool.^2,1).^.5;
stimpool=(ones(size(stimpool,1),1)*(energy.^.5./(mag))).*stimpool;

%3. add in projection along the mean
stimpool=stimpool+vset;

