% Explanation: Template function for dereferencing a user defined object
%
%   obj - pointer to object
%   index -
%           .type
%           .subs
% 2-2-07:
%   check if field is member of base class.
function [out] = subsref(obj,index)

%name of base classe
pbase='PoolstimNonLinInp';
% SUBSREF Define field name indexing for GLModel objects
switch index(1).type
    case '.'

        %*************************************************
        %Special Handlers
        %***************************************************
        %glm
        if (strcmp(index(1).subs,'glm'))
            %we are accesing the glm for this stimchooser
            %we are only glm specific if we are choosing info. max stimuli
            %and not randomly selecting stimuli from the pool
            %in this case the glm is specified by the miobj

            if (size(index,2)==1)
                out=obj.(pbase).glm;
            else
                %recursively process further dereferencing
                out=subsref(obj.(pbase).glm,index(2:end));
            end
        else
        %*****************************************
            %Generic Handlers
            %***********************************************
            %check if its a field
            if ~isempty(strmatch(index(1).subs,fieldnames(obj)))
                %field is member of this structure
                if (size(index,2)==1)
                    %only 1 level of indexing is provided
                    out=obj.(index(1).subs);
                else
                    %call the indexing on the appropriate field
                    fname=index(1).subs;
                    %recursively process any further dereferencing
                    out=subsref(obj.(fname),index(2:end));
                end

            else
                %check if its a member of the base class
                  if ~isempty(strmatch(index(1).subs,fieldnames(obj.PoolstimNonLinInp)))
                    fname=index(1).subs;
                  out=subsref(obj.PoolstimNonLinInp,index);
                 end
            end
        end
end