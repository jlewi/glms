%function sim=POptAsymGPPCanonLB
%
%
%Explanation: This function finds a Gaussian proccess which maximizes
%   a lower bound on the mutual information in the infinite horizon.
%   We do not enforce stationarity constraints.
%
classdef (ConstructOnLoad=true) POptAsymGPPCanonLB < StimChooserObj    
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %
    %powcon   - the constraint we impose on the power 
    %optim    - options for the optimization
    %minfo    - store the value of the mutual information
    %           for the optimized gaussian process on each trial
    %normalize - normalize means all stimuli should have a magnitude of
    %            mobj.mmag
    %          - if set to false then the magnitude is just the result of
    %          sampling our gaussian process with the appropriate power
    %          constraint. 
    %lastbatch is a structure storing information about when we last
    %           sampled a stimulus and the stimulus on that trial
    %       .stim  - matrix of dimensions klength x ktlength
    %       .trial - trial on which it was drawn.
    properties(SetAccess=private, GetAccess=public)
        powcon=[];
        optim=optimset('display','off');
        minfo=[];
        normalize=false;
        

        lastbatch=struct('stim',[],'trial',0);
    end

    %Gaussian process that we sample. We save it so that we can manually
    %inspect it for debugging purposes
    properties(SetAccess=private,GetAccess=public,Transient)
       gp=[]; 
    end
    methods
        function obj=POptAsymGPPCanonLB(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'powcon'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case 1
                    bparams=rmfield(params,{'powcon'});
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            bparams.glmspecific=true;
            obj=obj@StimChooserObj(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     obj.powcon=params.powcon;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end
            if isfield(params,'normalize')
               obj.normalize=params.normalize; 
            end

            %set the version to be a structure
            obj.version.POptAsymGPPCanonLB=090123;
        end
    end
end


