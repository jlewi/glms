%function [stim,fopt]=choosestim(obj,varargin)
%   varargin - whatever parameters are passed in don't matter
%               varargin is used so that the code doesn't have to change
%               depending on the method called
%
%Explanation: chooses stimuli by generating them randomly from the unit
%sphere of radius mmag
%
%5-15-07
%   I wrote the function testRandStimBall to check the sampling everything
%   seems correct
function [stim, oreturn,obj,varargout]=choosestim(obj,simobj,post)

klength=simobj.mobj.klength;
mmag=simobj.mobj.mmag;

x=normrnd(0,1,klength,1);

%set the first d stimuli to be indepenent, orthogonal vectors (just choose unit vectors
xmag=x.*x;
xmag=sum(xmag,1);

%to generate the radius we draw a sample from the uniform
%distribution on the allowed radii
%radius=rand(1)*obj.mmag;

%to generate the radius we draw a sample from the beta
%distribution
radius=mmag*betarnd(klength,1);
normconst=radius/xmag^.5;
stim=normconst*x;


oreturn=[];

%the following allows other choosestim functions to return more
%arguments
if (nargout>3)
    for j=1:(nargout-3)
        varargout{j}=[];
    end
end