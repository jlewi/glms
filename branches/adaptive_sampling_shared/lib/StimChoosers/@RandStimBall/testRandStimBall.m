%testRandStimBall(sobj)
%   sobj - the object
%
%Explanation:
%   test if it really is selecting points uniformly from the ball
function x=testRandStimBall(sobj)

nstim=10000;

%generate nstim
x=zeros(sobj.klength,nstim);

for j=1:nstim
    x(:,j)=choosestim(sobj);
end

%compute the radii
r=sum(x.^2,1).^.5;

%histogram of the radii
fradii.hf=figure;
[counts bcenters]=hist(r,100);

dr=mean(diff(bcenters));
epdf=counts/nstim;

hold on;
bar(bcenters,cumsum(epdf));


%probability of r if uniform
rpts=[0:.05:1];
%need to check formulas
%this is the cdf
pr=rpts.^sobj.klength/((sobj.mmag)^sobj.klength);

plot(rpts,pr);

title('Cumulative Distribution of radii.');

