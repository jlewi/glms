%function sim=BSSeqStim('bdata')
%   datafile
%   miobj  - the object which computes the mutual information as a function
%           of mu and sigma
%
%Optional Parameters:
%   windexes - wave files to use
%
%Explanation: A class to choose the optimal stimulus from the bird song
%data
%   Revisions:
%       08-25-2008: Add nrepeats
%       08-23-2008: add windexes. When calling choose trial we compute
%           what trial to get based on windexes
classdef (ConstructOnLoad=true) BSSeqStim <handle

    %***************************************************
    %properties
    %****************************************************
    %bdata - A BSData object from which we get the data
    %
    %niter - number of iterations we have run
    %
    %bstrials - this is a structure which keeps track of the
    %           information we need to map the simulation trial
    %           into a valid trial in the bird song data, given
    %           that we only want to use wave files in windexes
    %           validtrials- n x 2
    %               this is an array of intervals corresponding to trials
    %               that we want to include in our data
    %
    %nrepeats  - how many repeats of each wave file to include in the
    %           stimulus set
    properties(SetAccess=private,GetAccess=public)
        version=080725;
        bdata=[];
        niter=0;
        windexes=[];
        bstrials=struct('validtrials',[],'cumntrials',[]);
        nrepeats=1;
    end


    methods
        function obj=BSSeqStim(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'bdata'};
            con(1).cfun=1;



            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    return
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    obj.bdata=params.bdata;
                    if isfield(params,'windexes')
                        obj.windexes=params.windexes;
                    else
                        obj.windexes=1:getnwavefiles(params.bdata);
                    end
                otherwise
                    error('Constructor not implemented')
            end



        end


        function prop=get.bstrials(obj)
            %if we haven't computed bstrials yet we need to do so
            if isempty(obj.bstrials.validtrials)
                nwindex=length(obj.windexes);

                %determine how many times each eave file is repeated
                nrepeats=zeros(nwindex,1);
                for wind=1:nwindex
                    %nrepeats(wind)=getnrepeats(obj.bdata,obj.windexes(wind)) ;
                    nrepeats(wind)=obj.nrepeats;
                end

                trialint=zeros(sum(nrepeats),2);

                startrow=1;
                for wind=1:nwindex
                    endrow=startrow+nrepeats(wind)-1;
                    [tind]=gettrialindforwave(obj.bdata,obj.windexes(wind));
                    trialint(startrow:endrow,:)=tind(1:nrepeats(wind),:);
                    startrow=endrow+1;
                end

                %now we sort the intervals of valid trials by the first
                %row, so that the trials are sequential
                trialint=sortrows(trialint,1);

                %now we compact trialint, that is if two intervals are
                %next to each other we form one giant interval.
                obj.bstrials.validtrials=trialint(1,:);

                for row=2:size(trialint,1)
                    if (obj.bstrials.validtrials(end,2)+1==trialint(row,1))
                        obj.bstrials.validtrials(end,2)=trialint(row,2);
                    else
                        obj.bstrials.validtrials(end+1,:)=trialint(row,:);
                    end
                end


                %compute the cumulative number of trials over all previous
                %intervals
                %this will make finding the correct interval on each trial
                %faster
                obj.bstrials.cumntrials=cumsum(diff(obj.bstrials.validtrials,1,2)+1);
            end
            
            prop=obj.bstrials;
        end
        %Return Value:
        %     ntrials - the number of trials we have data for
        function ntrials=getntrials(obj)
            ntrials=obj.bstrials.cumntrials(end);
        end
        
        function bdata=getbdata(obj)
            bdata=obj.bdata;
        end
    end
end



