%function [stim, shist, obsrv,stimindex]= choosetrial(obj,simobj);
%  simobj - a handle to the BSSim object
%
%Return Value:
%   stim - a GLMInput object representing the stimulus
%   shist - a vector representing the spike history
%   obsrv - the observation
%   stimindex
%           = we use this to keep track of the order the stimuli were
%           chosen in
%       stimindex(1,1) - windex of the stimulus
%       stimindex(2,1) - the repeat of the wfile
%       stimindex(3,1) - the relative trial within the 
function [stim, shist, obsrv,stimindex]= choosetrial(obj,simobj,prior)

%what trial are we getting
trial=simobj.niter+1;

%we need to map this into an appropriate bsdata trial taking into account
%windexes
bstrials=obj.bstrials;

rowint=binarysubdivide(bstrials.cumntrials,trial);

npast=0;
if (rowint>1)
   npast=bstrials.cumntrials(rowint-1); 
end
%comptue the bird song trial
bt=bstrials.validtrials(rowint,1)-1+(trial-npast);

[bdata,stim,shist,obsrv,stimindex]=gettrial(obj.bdata,bt,getshistlen(simobj.mobj));