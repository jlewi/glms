%function [obj,optstim]=optbatchinwav(obj,simobj,windex,post)
%   obj - The stimulation chooser object
%   simobj - the simulation object
%   windex - the index of the wave file we are searching for the optimal
%          stimulus
%   post   - the posterior object
% Explanation - this function constructs  a pool of psize stimuli
%       from the trials corresponding to wavefile wfile
%       We then select from this pool the optimal stimulus
%Return value:
%  	opstim.windex - windex - wave file
%   optstim.repeat - which repeat of this wavefile
%   opstim.trial - which trial of this wavefile
%   opstim.nbatches - number of batches in this wave file
%
%
%
%Explanation: Randomly select one of the batches in this wave file
function [optstim]=sampbatchinwav(obj,simobj,windex)


mobj=simobj.mobj;

bdata=getbdata(obj.bspost);


%loop for the repeats and select one of the batches in each repeat
for repeat=1:obj.nrepeats
    rtrial=getrpasttrial(obj,windex,repeat,simobj);
      
    [optrepeat(repeat)]=sampbatchinrepeat(obj,rtrial);
end

ind=find([optrepeat.nbatches]>0);

if ~isempty(ind)
    optrepeat=optrepeat(ind);
    
    %randomly select one of the batches in proportion to number of batches
    %in the repeat
    nbatches=[optrepeat.nbatches];
    p=nbatches/sum(nbatches);
    cp=cumsum(p);
    ind=binarysubdivide(cp,rand(1,1));
    
    optstim=optrepeat(ind);
    optstim.nbatches=sum(nbatches);
    optstim.windex=windex;
    optstim.repeat=ind;
else
   %no batches left
   optstim=optrepeat(1);
   optstim.windex=nan;
   optstim.repeat=nan;
end




%function [optstim,nbatches]=sampbatchinrepeat(paststim)
%   paststim - an array as long as the number of the stimuli in the wave
%   file. A 1 indicates that stimulus has already been presented
%       
%Return value:
%   optstim - the randomly selected batch in this wave file
%   since all repeats are the same input, all we need to know is the
%   relative trials on which stimuli were presented
function [optstim]=sampbatchinrepeat(obj,paststim)

ntrials=length(paststim);

%compute how many overlapping batches we can get from this wave file
nbatches=ntrials-obj.bsize+1;

%to identify valid batches we conv trials with a filter of ones
%whenever one of the stimuli in the batches has already been presented
%the result of convolving with the filter will be nan
trials=zeros(1,ntrials);
trials(logical(paststim))=nan;

bmi=conv(trials,ones(1,obj.bsize));
bmi=bmi(obj.bsize:obj.bsize+nbatches-1);

%find the start of all batches
bstart=find(~isnan(bmi));

%if bstart is empty then there are no remaining batches in this wave file
if ~isempty(bstart)
    
    %randomly select one of the batches
    optstim.ntrial=bstart(ceil(rand(1,1)*length(bstart)));
    optstim.nbatches=length(bstart);
else

    optstim.ntrial=nan;
    optstim.nbatches=0;
end




