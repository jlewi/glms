%function [bstart]= choosebatch (obj, simobj,post);
%   obj - A BSOptStim object
%   simobj - a BSSim object
%
%Returun value:
%   bstart- structure with fields
%       .windex
%       .repeat
%       .trial
%   which specifies the start of the trial
%   maxmi - the log of the mutual info for this batch
%
%Explanation: This function randomly selects one of the batches.
%
%Revisions:
%   08-10-2008 - Return blogminfo=nan to be consistent with other functions
%
function [bstart,blogminfo]= choosebatch(obj, simobj,post)

blogminfo=nan;

%select a new optimal bactch
windexes=obj.windexes;

%we need to loop over the wavefiles and select the maximally informative
%stimulus. Each node processes a different wave file.
%Each node constructs a pool for each of the files it processes
%And selects the optimal stimulus in this pool
%
%We use a distributed array to store these results
%   dpoolopt - this is an 4xnwaveefiles  matrix
%   dpoolopt(1,j) - the index of this wavefile
%   dpoolopt(2,j) - the repeat of the optimal stimulus from the j'th
%   wavefile
%   dpoolopt(3,j) - the relative trial for this wave file of the optimal
%                   stimulus
%   dpoolopt(4,j) - the number of batches in this wave file

dpoolopt=zeros(4,length(windexes),distributor());

%we are using a parfor loop
%therefore I don't think we can use a distributed array because that
%requires MPI communication
%each node creates a structure dpoolopt and dpoolwind
%dpoolwind - stores the values for the windex being evaluated
%if that is better than dppool opt we override dpool opt
%dpoolopt=zeros(5,1);
%dpoolopt(4,1)=nan;
optstim=[];
%fprintf('choosetrial');


%manually compute which wave files each node will process
%we do this manually because we want need to know in order to handle the
%broad casting of the pool
%nwindexes is an array which stores the number of windexes processed
%by each node
nwindexes=ones(numlabs,1)*floor(length(windexes)/numlabs);
remainder=mod(length(windexes),numlabs);
nwindexes(1:remainder)=nwindexes(1:remainder)+1;

startind=[0;cumsum(nwindexes(1:end-1))]+1;
endind=startind+nwindexes-1;



%************************
%use a try block and properly handle possibility
%that an error occurs on some threads but not others
%*********************************************
err=0;
emsg='';
try
    for windexind=startind(labindex):startind(labindex)+nwindexes(labindex)-1

        windex=windexes(windexind);

        dpoolopt(1,windexind)=windex;

        %compute the indexes for the pool for this wave file

        [optstim]=sampbatchinwav(obj,simobj,windex);
        dpoolopt(1,windexind)=windex;
        dpoolopt(2,windexind)=optstim.repeat;
        dpoolopt(3,windexind)=optstim.ntrial;
        dpoolopt(4,windexind)=optstim.nbatches;

    end
catch e

    err=1;
    emsg=e.message;
    emsg=sprintf('%s\nstack is: Line \t file:',emsg);
    for index=1:length(e.stack)
        emsg=sprintf('%s\n %d \t %s',emsg, e.stack(index).line, e.stack(index).file);
    end
end

%check if error occured on any threads
if (gplus(err)>0)
    %determine which labs threw an error
    lerr=gcat(err);
    lerr=find(lerr>0);

    if (err==0)
        emsg=sprintf('Error was thrown on labs %s', num2str(lerr));
    end
    error(emsg);
end

%now we gather the full dpoolopt on each matrix
%dpoolopt=gcat(dpoolopt);
dpoolopt=gather(dpoolopt);

%randomly pick one of the wave files
%we sample the wave files in proportion to the number of batches in
%each wave file
cnbatches=cumsum(dpoolopt(4,:));

if (cnbatches(end)==0)
    %thrown an exception because we are out of trials
    me=MException('BirdSong:OutOfData','We have run out of batches of stimuli because when processing the stimuli in batches the number of trials ends up being less than the number of trials in the data.');
    throw(me);
end
%cumulatic probability of picking each wave file
pwave=cnbatches/cnbatches(end);
%generate a random number
r=rand(1,1);
mind=binarysubdivide(pwave,r);


optwindex=dpoolopt(1,mind);
optrepeat=dpoolopt(2,mind);
opttrial=dpoolopt(3,mind);

%set the batch information
bstart=struct('windex',optwindex,'repeat',optrepeat,'trial',opttrial);