%function sim=TanSpaceInfoMax()
%
% Explanation: Maximize the information about some tangent space and for
% the canonical PoissonModel.
%
%
%Version:
%   01-08-2009 - Conver to new object model
%       version is now a structure and defined in the base class
%   03-11-2008 - Compute the posterior the new way and comput the mean and
%   covariance of sglmproj the new way
classdef (ConstructOnLoad=true) TanSpaceInfoMaxCanon < PoissExpMaxMI
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %  stimobj  - stimobj - object to actually maximize the stimulus
    %  iterfullmax - how often we do an infomax step using the full posterior
    %  startfullmax - how many trials at start do we do full info. max.
    %                 before switching to the tangent space.

    properties(SetAccess=private,GetAccess=public)
        stimobj=[];
        iterfullmax=inf;
        startfullmax=0;

    end

    methods
        function obj=TanSpaceInfoMaxCanon(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=params;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@PoissExpMaxMI(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %set the version to be a structure
            obj.version.TanSpaceInfoMaxCanon=090108;


        end
    end
end



