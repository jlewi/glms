%function iterfullmax=getiterfullmax(obj)
%	 obj=TanSpaceInfoMaxCanon object
% 
%Return value: 
%	 iterfullmax=obj.iterfullmax 
%
function iterfullmax=getiterfullmax(obj)
	 iterfullmax=obj.iterfullmax;
