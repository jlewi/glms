%function m=mi(robj,x,y)
%       robj    -rsm object
%       x - vector at which to evaluate the 
%       y - vector at which to evaluate
%
% Return m
%   m(j,k)=f(dotmu(j),sigmasq(k))
%
% Explanation: Computes the mutual info at the values specified
function m=mi(robj,x,y)
      %************************************
    %error checking
    %****************************************
    if ~isvector(x)
         error('dotmu must be vector');
    end
    
    if ~isvector(y)
         error('sigmasq must be vector');
    end
    
    %make x,y col vectors
   if (size(x,2)>1)
       x=x';
   end
   if (size(y,2)>1)
       y=y';
   end
   
   %*****************************************************************
   %form the augmented vectors
   %for each data point the augmented vector is (degree+1)^2 long
  
   %each row of x is a different input
  % m=zeros(length(x),length(y));
   

   %we need to loop over dotmu in the outer loop
   %because we reshape imutual to be a vctor which means we take the
   %elements column wise   
%    for dind=1:length(dotmu)
%            daug=(dotmu(dind).^[0:robj.degree])';
%        for sind=1:length(sigmasq)
%             saug=(sigmasq(sind).^[0:robj.degree]);
%             x=reshape(daug*saug,1,(robj.degree+1)^2);
%            m(dind,sind)=x*robj.coef;
%        end
%    end

  npts=length(x)*length(y);
   xy=zeros(npts,2);
   
   %so get the indexes of x and y corresponding to each point in fxy
   [xind,yind]=ind2sub([length(x) length(y)],1:(length(x)*length(y)));
   xy=[x(xind) y(yind)];
   %form the augmented points
   pts=augpts(robj,xy);
   m=pts*robj.coef;
   
   m=reshape(m,[length(x),length(y)]);