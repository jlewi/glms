%function d= cdegree(robj)
%
%return value
%   d - (degree+1)^2  x2 array
%       -each column corresponds to a coefficient in obj.coef
%       -the first column lists the degree of dotmu that that point
%       corresponds to
%       2nd column degree of sigmasq
%Explanation returns a
function d=cdegree(robj)

    robj.cdegree;
    