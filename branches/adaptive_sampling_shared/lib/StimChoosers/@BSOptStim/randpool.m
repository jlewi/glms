
%function pooltrials=randpool(muproj,psize)
%
%Return Value:
%  %pooltrials - this is a psizex2 matrix
%each row specifies a different trial from this wave file
%that we want to include in our pool
%pooltrials(:,1) -which repeat of this stimulus to use
%pooltrials(:,2) - what trial relative to the start of the wave file to
%       use - i.e what column of the spectrogram
%Explanation: selecting a subset of the trials corresponding to this
%wavefile randomly
%
function [pooltrials,poolmu]=randpool(obj,muproj,mupind,nrepeats,ntrials,psize)

%randomly generate the entries of muproj and mupind to select for this pool

ind=ceil(rand(1,psize)*length(muproj));



poolmu=muproj(ind);
poolmu=colvector(poolmu);
%

pooltrials=zeros(obj.poolsize,2);
[pooltrials(:,1), pooltrials(:,2)]=ind2sub([nrepeats,ntrials],mupind(ind));