%function pooltrials=heurmumax(obj,muproj,mupind)
%   obj    - BSOptStim object
%   muproj - the value of muproj on the different trials
%   mupind - "trial" num corresponding to mupind
%             -this is the linear index corresponding 
%              to the entry of muproj in glmproj as returned by compglmproj
%   nrepeats - how many times this wave file is repeated
%   ntrials  - how many trials we divide this wave file into
%   psize   - actual pool size for this wave file
%             because we don't reuse trials, its poosible that
%             psize < obj.poolsize
%Return Value:
%  %pooltrials - this is a psizex2 matrix
%each row specifies a different trial from this wave file
%that we want to include in our pool
%pooltrials(:,1) -which repeat of this stimulus to use
%pooltrials(:,2) - what trial relative to the start of the wave file to
%       use - i.e what column of the spectrogram
%Explanation: a heuristic for selecting a pool of obj.poolsize trials from this
%wavefile. Selects the stimuli with the largest mu
%
function [pooltrials,poolmu]=heurmumax(obj,muproj,mupind,nrepeats,ntrials,psize)
    
   %heuristic for selecting a pool of stimuli from which we will pick the
    %optimal stimulus
    %make muproj a vector. The different repeats could have different
    %values because of different spike history
    [smuproj, sind]=sort(muproj(:));
    smupind=mupind(sind);
    

    %indexes for the entries to include in the pool
    pind=length(muproj)-psize+1:length(muproj);
    %determine the trial numbers for our pool
    %mupind is the linear index into muproj where muproj
    %is a 2d marix consisting of muproj for all repeats of thisis wave
    %fille
    %we convert this to an index
    smupind=smupind(pind);

    %
    poolmu=smuproj(pind);
    pooltrials=zeros(psize,2);
    [pooltrials(:,1), pooltrials(:,2)]=ind2sub([nrepeats ntrials],smupind);
    