%function [obj,optstim]=optstiminwav(obj,simobj,windex,post)
%   obj - The stimulation chooser object
%   simobj - the simulation object
%   windex - the index of the wave file we are searching for the optimal
%          stimulus
%   post   - the posterior object
% Explanation - this function constructs  a pool of psize stimuli
%       from the trials corresponding to wavefile wfile
%       We then select from this pool the optimal stimulus
%Return value:
%  	opstim.windex - windex - wave file
%   optstim.repeat - which repeat of this wavefile
%   opstim.trial - which trial of this wavefile
%   opstim.mi -
%
%   stimpool - return the stimulus pool
%       .mu  - projection of stimulus on the mean
%       .sigma - sigma
%       .mi  - mutual info

%Explanation: Computes the mutual information.
function [optstim,varargout]=optstiminwav(obj,simobj,windex,post)


mobj=simobj.mobj;

bdata=getbdata(obj.bspost);


%    wcount=wcount+1;
%fprintf('BSlogpost:labindex=%d compllike  file index %d of %d wave files \n',labindex,windex,length(windexes));


%to compute the mutual information we need to compute
%muproj and sigmaproj
[muproj]=compglmproj(obj.bspost,windex,mobj,getm(post));
[ntrials,cstart]=getntrialsinwave(bdata,windex);
nrepeats=getnrepeats(bdata,windex);

%we need to remove from muproj the trials which we have already
%presented
%This could probably be more efficient
muproj=muproj(:);

%muproj is a nrepeatsxnstim matrix
%where nstim is the number of trials we divide this wave file into
if  (simobj.niter>0)
    %get the linear indexes of the elements
    %of the stimuli which we have already presented
    %get those stimulis which occured on this wave file
    pthiswave=find(simobj.paststim(1,1:simobj.niter)==windex);

    if isempty(pthiswave)
        mupind=[1:length(muproj(:))]';
    else
        linindex=sub2ind([nrepeats,ntrials],simobj.paststim(2,pthiswave),simobj.paststim(3,pthiswave));
        muproj(linindex)=nan;

        %throw out past stimuli because we don't want to include them in our
        %pool of stimuli
        %mupind is the linear index corresponding to the enteries of muproj
        %which are not nan's.

        mupind=find(~isnan(muproj(:)));
        muproj=muproj(mupind);
    end
else
    mupind=[1:length(muproj(:))]';
end

psize=min([obj.poolsize,length(mupind)]);
%make sure we still have some stimuli left in this wave file
if (psize>0)

    %select the trials in the pool and compute poolmu

    [pooltrials,poolmu]=obj.mpool(obj,muproj,mupind,nrepeats,ntrials,psize);
    poolsigma=zeros(psize,1);


    %compute poolsigma for all stimuli in the pool

    [stim,shist,obsrv]=gettrialwfilerepeat(bdata,windex,pooltrials(:,1),pooltrials(:,2),mobj);

    bias=1;
    for pind=1:psize
        %compute the full input
        inp=packtheta(mobj,stim(:,pind),shist(:,pind),bias);
        %fprintf('Optstiminwav: getc \n');
        poolsigma(pind)=inp'*rotatebyc(post,inp);
    end


    %compute the mutual information for the points in the pool
    poolmi=compmi(obj.miobj, poolmu,poolsigma);

    %find the maximum;
    [maxmi,mind]=max(poolmi);
    optstim.windex=windex;
    optstim.repeat=pooltrials(mind,1);
    optstim.ntrial=pooltrials(mind,2);
    optstim.mi=maxmi;

    %this is used to update paststim if we end up selecting this stimulus
    optstim.linindex=sub2ind([nrepeats,ntrials],optstim.repeat,optstim.ntrial);


    if (nargout==2)
        varargout{1}.mu=poolmu;
        varargout{1}.sigma=poolsigma;
        varargout{1}.mi=poolmi;
    end
else
    %no stimuli left in this file
    opstim.mi=-inf;
    optstim.windex=windex;
    optstim.repeat=[];
    optstim.ntrial=[];

    if (nargout==2)
        varargout{1}.mu=[];
        varargout{1}.sigma=[];
        varargout{1}.mi=[];
    end
end



%function pooltrials=heurpool(muproj,psize)
%
%Return Value:
%  %pooltrials - this is a psizex2 matrix
%each row specifies a different trial from this wave file
%that we want to include in our pool
%pooltrials(:,1) -which repeat of this stimulus to use
%pooltrials(:,2) - what trial relative to the start of the wave file to
%       use - i.e what column of the spectrogram
%Explanation: a heuristic for selecting a pool of psize trials from this
%wavefile
%
function [pooltrials,poolmu]=heurpool(muproj,psize)

%heuristic for selecting a pool of stimuli from which we will pick the
%optimal stimulus
%make muproj a vector. The different repeats could have different
%values because of different spike history
[smuproj, mupind]=sort(muproj(:));

%select a set of psize evenly spaced points
%I should fix this heuristic so that we select mu evenly spaced on the
%mu axis. Currently I'm just spacing out the indexes which is 't what I
%want to do
error('This heuristic isn terrible. I should improve this heuristic by binning muproj into bins and then randomly selecting one point in each bin');
ind=floor(linspace(1,length(smuproj),psize));

%determine the trial numbers for our pool
%mupind is the linear index into muproj
%we convert this to an index
mupind=mupind(ind);

%
poolmu=muproj(mupind);
pooltrials=zeros(psize,2);
[pooltrials(:,1), pooltrials(:,2)]=ind2sub(size(muproj),mupind);

