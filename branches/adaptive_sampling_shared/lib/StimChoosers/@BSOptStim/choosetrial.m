%function [obj, stim, shist, obsrv,stimindex]= choosetrial(obj, simobj);
%   obj - A BSOptStim object
%   simobj - a BSSim object
%
%Explanation: 
%   stim - vector representing the stimulus
%   shist - vector of the spike history 
%   obsrv - the response
%   stimindex=3 x1 vector indicating the trial corresponding to the
%   stimulus
%           = we use this to keep track of the order the stimuli were
%           chosen in
%       stimindex(1,1) - windex of the stimulus
%       stimindex(2,1) - the repeat of the wfile
%       stimindex(3,1) - the relative trial within the 
%
%Revisions
%   07-20-2008
%       take the object and a pointer to the simulation as input
function [stim, shist, obsrv,stimindex]= choosetrial(obj, simobj,post)

windexes=obj.windexes;

%we need to loop over the wavefiles and select the maximally informative
%stimulus. Each node processes a different wave file.
%Each node constructs a pool for each of the files it processes
%And selects the optimal stimulus in this pool
%
%We use a distributed array to store these results 
%   dpoolopt - this is an 4xnwaveefiles  matrix
%   dpoolopt(1,j) - the index of this wavefile
%   dpoolopt(2,j) - the repeat of the optimal stimulus from the j'th
%   wavefile
%   dpoolopt(3,j) - the relative trial for this wave file of the optimal
%                   stimulus
%   dpoolopt(4,j) - the mutual information for this optimal stimulus

dpoolopt=zeros(5,length(windexes),distributor());

%we are using a parfor loop
%therefore I don't think we can use a distributed array because that
%requires MPI communication
%each node creates a structure dpoolopt and dpoolwind
%dpoolwind - stores the values for the windex being evaluated
%if that is better than dppool opt we override dpool opt
%dpoolopt=zeros(5,1);
%dpoolopt(4,1)=nan;
optstim=[];
%fprintf('choosetrial');
for windexind=drange(1:length(windexes))
%for windexind=1:2
%fprintf('Startinging loop\n');
     windex=windexes(windexind);
%fprintf('1\n');   
     dpoolopt(1,windexind)=windex;
%fprintf('2\n');        
      [optstim]=optstiminwav(obj,simobj,windex,post);
   % fprintf('3\n');   
   %   if (isnan(dpoolopt(4,1)) || (dpoolopt(4,1)<optstim.mi))
      dpoolopt(1,windexind)=windex;     
      dpoolopt(2,windexind)=optstim.repeat;
      dpoolopt(3,windexind)=optstim.ntrial;
      dpoolopt(4,windexind)=optstim.mi;
      dpoolopt(5,windexind)=optstim.linindex;
      %end
end

%now we gather the full dpoolopt on each matrix
%dpoolopt=gcat(dpoolopt);
dpoolopt=gather(dpoolopt);

%find the optimal stimulus 
[maxmi mind]=max(dpoolopt(4,:));
mind=mind(1);

optwindex=dpoolopt(1,mind);
optrepeat=dpoolopt(2,mind);
opttrial=dpoolopt(3,mind);
optlinindex=dpoolopt(5,windexind);

bdata=getbdata(obj.bspost);
[stim,shist,obsrv]=gettrialwfilerepeat(bdata,optwindex,optrepeat,opttrial,simobj.mobj);

stimindex=[optwindex;optrepeat;opttrial];