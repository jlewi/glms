%function sim=BSOptStim('bdata','miobj'
%   datafile
%   miobj  - the object which computes the mutual information as a function
%           of mu and sigma
%
%Optional Parameters:
%   windexes - wave files to use. Only trials from these wave files will be
%             selected
%
%Explanation: A class to choose the optimal stimulus from the bird song
%data
%
%Revisions:
%   07-22-2008
%       -add the field windexes to limit which wave files we can select
%       inputs from to train with 
%
%   07-20-2008
%       -Use New Matlab OOP
%       -PastStim is now stored as part of the Simulation object which is
%       passed in by reference to choosestim
classdef (ConstructOnLoad=true) BSOptStim < handle

    % mpool - method for selecting the pool of stimuli
    %         -a pointer to a function
    %bspost - a BSLogPOst object
    %poolsize - how large of a pool to make
    %windexes - which wave files to choose the stimuli from
    %           by excluding some wave files you can create a test set
    
    properties(SetAccess=private,GetAccess=public)
        version=080722;
        miobj=[];
        bspost=[];
        windexes=[];
    end
    properties(SetAccess=public,GetAccess=public)
        poolsize=1000;       
        mpool=@heurmumax; 
    end
    methods

        function obj=BSOptStim(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'bdata','miobj'};
            con(1).cfun=1;




            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
          
                    return
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

         
            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1

                    %data=load(getpath(params.datafile));
                    obj.bspost=BSlogpost('bdata',params.bdata);
                    obj.miobj=params.miobj;

                    if isfield(params,'windexes')
                        obj.windexes=params.windexes;
                    else
                        obj.windexes=1:getnwavefiles(params.bdata);
                    end
                otherwise
                    error('Constructor not implemented')
            end



        end
        %Return Value:
        %     ntrials - the number of trials we have data for
        function ntrials=getntrials(obj)
            %we need to compute the number of trials in the allowed indexes
              nwindex=length(obj.windexes);

                %determine how many times each eave file is repeated
                nrepeats=zeros(nwindex,1);
                for wind=1:nwindex
                    nrepeats(wind)=getnrepeats(obj.bdata,obj.windexes(wind)) ;
                end

                trialint=zeros(sum(nrepeats),2);

                startrow=1;
                for wind=1:nwindex
                    endrow=startrow+nrepeats(wind)-1;
                    [trialint(startrow:endrow,:)]=gettrialindforwave(obj.bdata,obj.windexes(wind));
                    startrow=endrow+1;
                end
                
                ntrials=diff(trialint,1,2)+1;
                ntrials=sum(ntrials);
                
        end
        function bdata=getbdata(obj)
            bdata=obj.bspost.bdata;
        end
    end
end



