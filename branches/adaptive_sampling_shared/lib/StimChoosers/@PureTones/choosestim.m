%function [stim,stiminfo,obj]=choosestim(obj,post,shist)
%   obj - the stimchooserobj
%   post - the current posterior on the model parameters
%           .m
%           .c
%   shist - spike history
%
% Explanation: Create a random sinusoid.
function [stim,stiminfo,obj]=choosestim(obj,post,shist,mparam,varargin)
stiminfo=[];

width=obj.dim(1);

x=0:width-1;



%draw the frequency
k=2*pi/(rand*diff(obj.period)+obj.period(1));


%draw the phase 
phi=rand*2*pi;
stim=sin(k*x+phi);

%normalize the power
stim=normmag(stim')*getmag(mparam);

stim=GlMInputVec('data',stim);

