%function getdebug(sobj)
%   sobj - stimchooser object
%
%Return value:
%   debug - value of debug
function d=getdebug(sobj)
    d=sobj.debug;
