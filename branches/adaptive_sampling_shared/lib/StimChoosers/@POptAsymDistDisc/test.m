%function test()
%
%Explanation: A function mainly to test for syntax errors
function success=test()

success=true;

%create a stimulus distribution and try computing POptAsymDistDisc

nx=10;
dx=1;
ktlength=2;
stim=normrnd(0,1, dx,nx);

sobj=POptAsymDistDisc('stim',stim,'ktlength',ktlength);

theta=[1;-1];
glm=GLMPoisson('canon');

pstim=StatDistDisc('stim',stim,'ktlength',ktlength);
ld=logdetexss(sobj,theta,pstim,glm);

c=normrnd(0,1,pstim.dims,pstim.dims);
c=c*c';
post=GaussPost(normrnd(0,1,pstim.dims,1),c);

nsamp=5;
theta=normrnd(0,1,pstim.dims,nsamp);
ex=exlogdetexss(sobj,post,pstim,glm,theta);

nsamp=5;
ex=exlogdetexss(sobj,post,pstim,glm,nsamp);



