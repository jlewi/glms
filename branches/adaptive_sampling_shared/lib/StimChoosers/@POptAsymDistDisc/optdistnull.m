%function optdist(obj,post,psinit,glm,nsamp)
%   post - posterior on theta
%   nsamp - how many samples of theta to use
%
%function optdist(obj,post,psinit,glm,theta)
%   post - posterior on theta
%   theta samples of theta to use
%
%   gpinit (optional) - initial guess for the gaussian process
%Return value:
%   fval - This is the negative of the funciton we want to maximize because
%           we are using fmincon but we want to find the maximum.
%Explanation: Use fmincon to find the optimal Gaussian process
%
%   This optimization routine elimnates the constraints needed to force
%   stationarity by representing the stimulus distribution in the null
%   space of those constraints.
function [psopt,fval,exitflag,output,theta]=optdistnull(obj,post,psinit,glm,param4)


if ~exist('psinit','var')
    psinit=[];
end

%generate samples of theta
%we use the same samples on all iterations of fmincon because otherwise
%changing the samples will cause the value of the function to change
if (isscalar(param4) )
    nsamp=param4;
    theta=mvgausssamp(getm(post),getc(post),nsamp);

elseif (~isscalar(param4) )
    theta=param4;
    nsamp=size(theta,2);
else
    error('must either specify nsdir and nsproj or thetadir and thetaproj');
end


%initialize psinit to a uniform distribution
if isempty(psinit)
    psinit=StatDistDisc('stim',obj.stim,'ktlength',obj.ktlength);
end

%********************************************************
%Error checking
%*********************************************************
if (size(theta,1)~=psinit.dims)
    error('Theta and s do not have correct dimensions');
end


%equality constratints enforce the constraint that the distribution is
%stationary and that it sums to 1;

%compute the null space of the constraints to enforce stationarity
nstatcon=null(psinit.statcon);


%use this constraint to enforce constraint that p is positive
A=-1*nstatcon;
b=zeros(size(nstatcon,1),1);

%normalization constraint
Aeq=[ones(1,size(nstatcon,1))*nstatcon];
beq=[1];

%probabilities must be positive
lb=[];
ub=[];


nlcon=[];


%we pass psinit to our objective function.
%We use psinit to store the s and ss matrices
%which we use to evaluate our objective function
%this way we can avoid recomputing them.
psopt=StatDistDisc('ps',psinit);
lpmat=inv(nstatcon'*nstatcon)*nstatcon';
fobj=@(x)(ofun(obj,post,x, theta,glm,psopt,nstatcon,lpmat));

%function for the hessian
hfun=@(b,lambda)(lhess(obj,b,lambda,post, theta, glm, psopt,nstatcon,lpmat));

obj.optim=optimset(obj.optim,'GradObj','on');

%don't use the Hessian its too expensive to compute
obj.optim=optimset(obj.optim,'Hessian','off');

%obj.optim=optimset(obj.optim,'HessFcn',hfun,'Hessian','user-supplied');
obj.optim=optimset(obj.optim,'Algorithm','Interior-point');
obj.optim=optimset(obj.optim,'TolCon',10^-10);
xinit=lpmat*psinit.p(:);

%scale xinit so distribution is normalized. It should 

[xopt, fval,exitflag,output]= fmincon(fobj,xinit,A,b,Aeq,beq,lb,ub,nlcon,obj.optim);
psopt.p=nstatcon*xopt;

%multiply by negative one because we multiplied by negative 1 
%to get a minimum funciton.
fval=-1*fval;

%the function to minimize
%b - the projection of the stimulus distribuiton in the null space
%     of statcon
%lpmat - matrix for computing least squares projeciton of dp
function [f varargout]=ofun(obj,post,b, theta, glm, pstim,nullstatcon,lpmat)
p=nullstatcon*b;
pstim.p=reshape(p,ones(1,obj.ktlength)*pstim.nstim);

if (nargout==1)
    ex=exlogdetexss(obj,post,pstim, glm, theta );
else
    [ex,dex]=exlogdetexss(obj,post,pstim, glm, theta );
    varargout{1}=(-1*lpmat*dex');
end
f=-1*ex;

%hessian of our lagrange function
%since our constraints our linear the hessian doesn't depend on the value of
%the lagrange multiplier
function [h]=lhess(obj,b,lambda,post, theta, glm, pstim,nullstatcon,lpmat)
p=nullstatcon*b;
psopt.p=reshape(p,ones(1,obj.ktlength)*pstim.nstim);


[ex,dex,d2ex]=exlogdetexss(obj,post,pstim, glm, theta );

%multiply by
h=- lpmat*d2ex*lpmat';


%function to call at each iteration to allow for inspection
function stop = outfun(x, optimValues, state)
    stop=false;
    g=optimValues.gradient;
    fprintf('Magnitude of gradient: %d \t stepsize %d \n',(g'*g)^.5,optimValues.stepsize);
    

