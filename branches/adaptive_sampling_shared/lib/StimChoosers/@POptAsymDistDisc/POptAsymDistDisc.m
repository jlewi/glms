%function sim=POptAsymDistDisc('stim','ktlength')
%   stim - matrix of the stimuli
%   ktlength - how many stimuli are in the stationary distribution
%       %p          - a matrix containing the probability
%              s=(x_1=a_1,x_2=a_2,....);
%           - dim(x_t)xdim(x_t)x....=(dim(x_t)^ktlength) matrix
%
% Explanation:
%
%
%Revisions:
%   12-27-2008 - Make version a structure with each field the name of a
%             different class.
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) POptAsymDistDisc < StimChooserObj
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %stim     - matrix of the stimuli
    %ktlength - how many stimuli in our stationary distribution.
    %
    %nthetsamp - how many samples of theta to use to compute the
    %           expectation w.r.t to theta
    %
    %fopt     -keep track of the fopt value on the different trials
    %
    %ps       - the distribution we keep track of it so that we can
    %           reinitilize on the next trial using this distribution
    properties(SetAccess=private, GetAccess=public)
        ktlength=[];
        optim=[];
        nthetasamps=10;
        fopt=[];
        stim=[];
        ps=[];
    end

  
    
    %store some intermediary results used by gradient ascent to optimize
    %the posterior.
    properties(SetAccess=private,GetAccess=public,Transient)
      
       
    end
    
    
    methods
     
        function obj=POptAsymDistDisc(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={'stim','ktlength'};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=rmfield(params,{'stim','ktlength'});

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@StimChooserObj(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    obj.stim=params.stim;
                    obj.ktlength=params.ktlength;
                    params=rmfield(params,{'stim','ktlength'});
                    
                    if (size(obj.stim,2)<1)
                        error('stim must contain at list 1 stimulus.');
                    end
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            
            %optional parameters
            fnames=fieldnames(params);
            for j=1:length(fnames)
                switch fnames{j}
                    case 'nthetasamps'
                        obj.nthetasamps=params.nthetasamps;
                    otherwise
                       error('Unrecognized parameter %s',fnames{j});
                end
            end
            
            %set the version to be a structure
            obj.version.POptAsymDistDisc=090102;
            
            obj.optim=optimset();
        end
    end
    
    
    methods(Static)
       %test a function to test mainly for syntax error
       success=test();
    end
end


