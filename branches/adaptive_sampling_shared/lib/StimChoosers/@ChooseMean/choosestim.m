%function stim=choosestim(finf,post,shist,einfo,mparam)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3 
%Explanation: maximize the mutual information for the canonical poisson
%model
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mparam,varargin)
oreturn=[];

%put all energy along the mean
xmax=normmag(getm(post));
xmax=getmag(mparam)*xmax;


%check to make sure xmax is valid
if (sum(isnan(xmax))>0)
    error('Optimal stimulus is invalid');
end


if (sum(imag(xmax))~=0)
    error('optimal stimulus is not real');
end

if (nargout>3)
    %return the quad object
    varargout{1}=qobj;
end