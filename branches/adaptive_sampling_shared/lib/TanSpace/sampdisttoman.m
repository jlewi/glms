%function dist=sampdisttoman(post,tspace,nsamps)
%   post - the posterior
%   tspace - the tangent space onto which we compute the distance
%
%Return value
%   dist - the distance of nsamps drawn from post from the manifold 
%Explanation: Draws samples of \theta from the Gaussian distribution.
%   Computes the distance of these samples from the manifold.
function [dist,theta]=sampdisttoman(post,tspace,nsamps)

if isa(post,'PostTanSpace')
   %we want to draw our samples from the tangent space
   tpost=gettanpost(post);
   
   %coordinates in the tangent space
   b=mvgausssamp(getm(tpost),getc(tpost),nsamps);
   
   %projectinto theta space
   tspace=gettanspace(post);
   theta=tantotheta(tspace,b);
   
else
theta=mvgausssamp(getm(post),getc(post),nsamps);
end
%ptheta- projection of theta onto the manifold
ptheta=zeros(size(theta,1),nsamps);
for sind=1:nsamps
    ptheta(:,sind)=submanifold(tspace,proj(tspace,theta(:,sind)));
end

%compute the distance
dist=sum((ptheta-theta).^2,1).^.5;