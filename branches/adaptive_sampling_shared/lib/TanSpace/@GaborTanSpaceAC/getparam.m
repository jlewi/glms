%function p=getparam(obj)
%   obj
%
%Return  Value:
%   p - structure containing parameters specific to the derived class
%       .gpstart - this is the current value of the Gabor parameters
%                   we will use these values to initialize the search when
%                   we project the updated posterior onto the Gabor
%                   manifold at the next time step.
%       .omega - known value of omega
%       .sigmasq - known value of sigma
function p=getparam(obj)
    p.gpstart=getsparam(obj);
    p.omega=obj.omega;
    p.sigmasq=obj.sigmasq;