%function grad=gradsubmanifold(obj,gp)
%   obj - GaborTanSpace object
%   gp  - parameters of gabor
%         [A; center];
%
%Return value:
%   grad = dim(theta) x 2 matrix 
%       grad* [dA dcenter ]= change in theta
%
%Explantion: Computes the jacobian of theta w.r.t to the parameters of the
%submanifold (i.e of the gabor function).
%   
function grad=gradsubmanifold(obj,gp)

A=gp(1);
center=gp(2);

sigmasq=obj.sigmasq;
omega=obj.omega;

width=getdimtheta(obj);
x=(1:width)-(1+width)/2;
%shift by cent
x=(x-center)';


dfda=cos(x*omega).*exp(-1/2*x.^2/sigmasq);

dfdcenter=A*sin(x*omega).*exp(-.5*x.^2/sigmasq);
dfdcenter=dfdcenter+A*cos(x*omega).*exp(-.5*x.^2/sigmasq).*(x)/sigmasq;

grad=[dfda dfdcenter ];