%function ptheta=proj(obj,theta,gpstart)
%   theta- value of theta
%   gpstart - point at which to initialize the search for the projection
%           gp= [A; center;];
%
%Return value:
%   ptheta - projection of theta onto the submanifold
%
%If gpstart is provided then we optimize the parameters twice
%   once using gpstart as the initial point
%   once using gpinitauto as the starting point
%
%the parameters returned on the ones which minimize the distance.
function ptheta=proj(obj,theta,gpstart)

if ~exist('gpstart','var')
    gpstart=[];
end

%if isempty(gpinit)

%we need to select a good initialization pt otherwise the search won't
%work well
[mt mind]=max(theta);
ainit=mt;
cinit=mind-(1+length(theta))/2;


gpinitauto=[ainit;cinit];

% %************************************************************
% %Determine which starting point to use (gpinit or gpstart) depending on
% %which one minimizes the error
% %****************************************************************
if ~isempty(gpstart)
if (msdtosub(obj,gpstart,theta)<msdtosub(obj,gpinitauto,theta))
    gpinit=gpstart;
else
    gpinit=gpinitauto;
end
else
    gpinit=gpinitauto;
end
optim=optimset('GradObj','on');
optim=optimset(optim,'FunValCheck','on');
optim=optimset(optim,'TolX',10^-14);
%optim=optimset(optim,'DerivativeCheck','on');

A=[];
b=[];
Aeq=[];
beq=[];
lb=[0;-inf];
ub=[inf;inf];
nlcon=[];


%find the gabor theta that minimizes the MSE between theta
[ptheta,eauto,exitflag,output] =fmincon(@(gp)(msdtosub(obj,gp,theta)),gpinit,A,b,Aeq,beq,lb,ub,nlcon,optim);

switch exitflag
    case  {-2,-1,0}
        % error('fmincon did not converge');
        warning('GaborTanspace/proj: fmincon did not converge');
end


if any(ptheta==gpinitauto)
    warning('optimization does not appear to have changed parameters');
end


