%LowRankTanSpace('u','v')
%   u,v- factorization of the matrix into a rank r matrix      
%         Matrix is sp.u*sp.v'
%
%LowRankTanSpace('mat',m,'rank')
%   mat - the full matrix i.e not factorized
%   rank - what rank of an approximation of the matrix to use
%
%LowRankTanSpace('theta','dimsparam','rank','matdim'}
%   matdim - we need to to know the dimensions of the matrix
%   dimspara - we could potentially compute this from matdim
%Revision History
%   080605 - allow non-square matrices 
%           - to do this matdim is 1x2 array storing the dimensions of the
%           matrix
%           - Theta no longer has to consist of terms which are just
%               the matrix
%          
%
%   010826 - store the singular values separatly from the principal
%   components and compute the manifold with respect to it.
function obj=LowRankTanSpace(varargin)

%we don't want to throw an error because potentially we want to load the
%objects and then convert them.
warning('This class is now obsolete 07-30-2008 use MTLowRank as the model and TangentPoint object instead.');

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'u','v','s'};
con(1).cfun=1;

con(2).rparams={'mat','rank'};
con(2).cfun=1;

con(3).rparams={'theta','dimsparam','rank','matdim'};

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
% rank     - rank of the manifold
%declare the structure
obj=struct('version',080605,'rank',[],'matdim',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'LowRankTanSpace',TanSpaceBase());
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%hack constructor sometimes matches 2 and 3
if length(cind>1)
    cind=cind(end);
end
%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        u=params.u;
        v=params.v;
        s=params.s;
        if (size(u,2)~=size(v,2))
            error('rank of u and v dont match');
        end
        %check for unit vectors
        if (any((sum(u.^2,1).^.5-1)>10^-6))
            error('columns of U must be normalized to 1.');
        end
        if (any((sum(v.^2,1).^.5-1)>10^-6))
            error('columns of U must be normalized to 1.');
        end        
        
        obj.rank=size(u,2);
        
        theta=u*diag(s)*v';
        theta=theta(:);
        
        obj.matdim=[size(u,1) size(v,1)];
        
        %the dimensionality of the manifold is however many
        %terms we need to describe the matrix + the number of components in
        %theta which are not in the matrix
        dimsparam=sum(obj.matdim*obj.rank)+obj.rank;
        
        %instantiate a base class if there is one
        pbase=TanSpaceBase('theta',theta,'dimsparam',dimsparam);
        obj=class(obj,'LowRankTanSpace',pbase);
        
        obj=setsparam(obj,uvtovec(obj,u,s,v));

        obj=setbasis(obj,gradsubmanifold(obj,getsparam(obj)));
    case 2
        m=params.mat;
        obj.rank=params.rank;

        %reshape m as a vector and construct the class
        bparams.theta=reshape(m,[numel(m),1]);
        bparams.dimsparam=2*size(m,1)*obj.rank+obj.rank;
        pbase=TanSpaceBase(bparams);
        
        obj=class(obj,'LowRankTanSpace',pbase);

        %project m onto the manifold
        sparam=proj(obj,m);
        obj=setsparam(obj,sparam);
        
        %compute point on the manifold
        theta=submanifold(obj,sparam);
        obj=setheta(obj,theta);
        
        %compute and set the basis
        obj=setbasis(obj,gradsubmanifold(obj,getsparam(obj)));
    case 3
        pbase=TanSpaceBase(params);
        obj.rank=params.rank;
        obj.matdim=params.matdim;
        obj=class(obj,'LowRankTanSpace',pbase);

        %project theta onto the manifold and reset sparam, and theta
        sparam=proj(obj,gettheta(obj));
        theta=submanifold(obj,sparam);
        
        obj=settheta(obj,theta);
        obj=setsparam(obj,sparam);
        
        
        %compute and set the basis
        obj=setbasis(obj,gradsubmanifold(obj,getsparam(obj)));
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************






    