%function m=uvtovec(obj,u,s,v)
%   u,s,v - are the matrices making up the rank r approximation of m
%         we reshape these a vector, so that they can be stored as vector
%         parameters of the manifold
%           
function m=uvtovec(obj,u,s,v)

m=[s(:);u(:);v(:)];
