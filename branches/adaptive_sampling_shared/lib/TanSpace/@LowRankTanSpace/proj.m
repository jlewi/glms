%function ptheta=proj(obj,theta)
%   theta- value of theta
%
%Return value:
%   ptheta - projection of theta onto the submanifold of rank r matrices
%          - this is a vector, use vectouv to convert vector to matrices
%          u,v
%           - the projection is returned in the space of the submanifold
function ptheta=proj(obj,theta)


m=thetatomat(obj,theta);

[u,s,v]=svd(m);
%select first rank components 
u=u(:,1:obj.rank);
s=s(1:obj.rank);
v=v(:,1:obj.rank);

%now convert this tp a vector
ptheta=uvtovec(obj,u,s,v);