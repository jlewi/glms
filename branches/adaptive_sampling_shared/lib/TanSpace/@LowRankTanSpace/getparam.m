%function p=getparam(obj)
%   obj
%
%Return  Value:
%   p - structure containing parameters specific to the derived class
%       .matdim - we need to know the dimensions of the matrix
function p=getparam(obj)    
    p.rank=obj.rank;
    p.matdim=obj.matdim;

    