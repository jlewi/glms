%function m=uvtovec(obj,evec,eigd)
%   evec - evec are the principal components of the matrix
%           these are unit vectors
%   eigd - scaling factors for these vectors
%
%Explanation: We pack these parameters into a vector so they can be stored in sparam           
function m=packasvec(obj,evec,eigd)

m=[evec(:);eigd(:)];
