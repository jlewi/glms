%function getrank=rank(obj)
%	 obj=LowRankTanSpace object
% 
%Return value: 
%	 rank=obj.rank 
%
function rank=getrank(obj)
	 rank=obj.rank;
