%function gettheta=theta(obj)
%	 obj=TanSpaceBase object
% 
%Return value: 
%	 theta=obj.theta 
%
%Overload gettheta in base object because method in base object doesn't
%appear to work if obj is an array
function theta=gettheta(obj)
	 theta=gettheta([obj.TanSpaceBase]);
