%function p=getparam(obj)
%   obj
%
%Return  Value:
%   p - structure containing parameters specific to the derived class
%
function p=getparam(obj)    
    p.rank=obj.rank;
    p.dimsparam=getdimsparam(obj);
    p.inplen=getinputlen(obj.qinp);

    