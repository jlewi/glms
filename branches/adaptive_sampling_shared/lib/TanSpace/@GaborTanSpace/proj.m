%function ptheta=proj(obj,theta,gpstart)
%   theta- value of theta
%   gpstart - point at which to initialize the search for the projection
%           gp= [A; sigmasq; center; omega];
%
%Return value:
%   ptheta - projection of theta onto the submanifold
%
%If gpstart is provided then we optimize the parameters twice
%   once using gpstart as the initial point
%   once using gpinitauto as the starting point
%
%the parameters returned on the ones which minimize the distance.
function ptheta=proj(obj,theta,gpstart)

if ~exist('gpstart','var')
    gpstart=[];
end

%if isempty(gpinit)

%we need to select a good initialization pt otherwise the search won't
%work well
[mt mind]=max(theta);
ainit=mt;
cinit=mind-(1+length(theta))/2;

%lowpass filter theta
flen=5;
fcoeff=cos(linspace(-pi/2,pi/2,flen));
fcoeff=fcoeff/sum(fcoeff);
lptheta=slidefilter(theta,fcoeff);

%******************************************************
%initialize omega
%***************************************************

%x is the x value associated with each part of the filter
x=[1:length(theta)]-(1+length(theta))/2;


%sampling period
dt=1/(length(theta)-1);
%sampling frequency
Fs=1/dt; %in hz


N=(length(theta));
%radians per sample
dw=2 *pi/(N-1)/dt;
wmax=pi/dt;
w=-wmax:dw:wmax;


freq=w./(2*pi);

%we divide by N to get the proper magnitude
tfft=fftshift(fft(theta))/N;

%length of theta should be odd (enforced by constructor)
%so zero frequency should be in center
cind=ceil(length(freq)/2);

%find the dominant frequency

%get the magnitude by taking the peak frequency.
%mind is the offset relative to the center
[A mind]=max(abs(tfft(cind:end)));
mind=mind-1;
if (length(A)>1)
    warning('Peak is not unique');
end

oinit=2*pi*freq(mind+cind);

%**********************************************
%Initialize sigma
%*****************************************
texp=lptheta./(ainit*cos((x-cinit)*oinit));

ind=find(texp>0);

%fit a line
y=log(texp(ind));
xd=(x(ind)-cinit).^2;

%remove outliers
%clearly all y should be negative
ind=find(y<0);

pcoeff=polyfit(xd(ind),y(ind),1);
sinit=-1/(2*pcoeff(1));

gpinitauto=[ainit;sinit;cinit;oinit];

% %************************************************************
% %Determine which starting point to use (gpinit or gpstart) depending on
% %which one minimizes the error
% %****************************************************************
% if ~isempty(gpstart)
% if (msdtosub(obj,gpstart,theta)<msdtosub(obj,gpinitauto,theta))
%     gpinit=gpstart;
% else
%     gpinit=gpinitauto;
% end
% else
%     gpinit=gpinitauto;
% end
%find the gabor theta that minimizes the MSE between theta

optim=optimset('GradObj','on');
optim=optimset(optim,'FunValCheck','on');
optim=optimset(optim,'TolX',10^-14);
%optim=optimset(optim,'DerivativeCheck','on');

A=[];
b=[];
Aeq=[];
beq=[];
lb=[0;0;-inf;0];
ub=[inf;inf;inf;inf];

%gp= [A; sigmasq; center; omega];

nlcon=[];


[ptheta,eauto,exitflag,output] =fmincon(@(gp)(msdtosub(obj,gp,theta)),gpinitauto,A,b,Aeq,beq,lb,ub,nlcon,optim);

switch exitflag
    case  {-2,-1,0}
        % error('fmincon did not converge');
        warning('GaborTanspace/proj: fmincon did not converge');
end


if any(ptheta==gpinitauto)
    warning('optimization does not appear to have changed parameters');
end

%**************************************************************************
%if gpstart is provided than we also do the serach using those parameters
%and then select the best one
if ~isempty(gpstart)
[pstart,estart,exitflag,output] =fmincon(@(gp)(msdtosub(obj,gp,theta)),gpstart,A,b,Aeq,beq,lb,ub,nlcon,optim);
switch exitflag
    case  {-2,-1,0}
        % error('fmincon did not converge');
        warning('GaborTanspace/proj: fmincon did not converge');
end


if any(pstart==gpstart)
    warning('optimization does not appear to have changed parameters');
end

if (estart<eauto)
    ptheta=pstart;
end
end

