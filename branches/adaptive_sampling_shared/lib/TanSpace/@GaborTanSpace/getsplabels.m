%function getsplabels=splabels(obj)
%	 obj=GaborTanSpace object
% 
%Return value: 
%	 splabels=obj.splabels 
%
function splabels=getsplabels(obj)
	 splabels={'A';'\sigma^2';'\mu_x';'\omega'};         
