%function getgp=gp(obj)
%	 obj=GaborTanSpace object
% 
%Return value: 
%	 gp=obj.gp 
%
function gp=getgp(obj)
	 gp=obj.gp;
