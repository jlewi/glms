%function grad=gradsubmanifold(obj,gp)
%   obj - GaborTanSpace object
%   gp  - parameters of gabor
%         [A; sigmasq; center; omega];
%
%Return value:
%   grad = dim(theta) x 4 matrix 
%       grad* [dA dsigma dcenter dy]= change in theta
%
%Explantion: Computes the jacobian of theta w.r.t to the parameters of the
%submanifold (i.e of the gabor function).
%   
function grad=gradsubmanifold(obj,gp)

A=gp(1);
sigmasq=gp(2);
center=gp(3);
omega=gp(4);

width=getdimtheta(obj);
x=(1:width)-(1+width)/2;
%shift by cent
x=(x-center)';


dfda=cos(x*omega).*exp(-1/2*x.^2/sigmasq);
dfdw=-A*sin(x*omega).*x.*exp(-1/2*x.^2/sigmasq);

dfdsigmasq=A*cos(x*omega).*exp(-1/2*x.^2/sigmasq)*.5.*x.^2./(sigmasq^2);

dfdcenter=A*sin(x*omega).*exp(-.5*x.^2/sigmasq);
dfdcenter=dfdcenter+A*cos(x*omega).*exp(-.5*x.^2/sigmasq).*(x)/sigmasq;

grad=[dfda dfdsigmasq dfdcenter dfdw];