%function sval=submanifold(obj,gp)
%   obj - descendent of TanSpaceObj
%   gp  - 4xn matrix of gabor parameters
%       [A; sigmasq; center; omega];
%
%Explanation: Evaluates the submanifold for this point.
function sval=submanifold(obj,gp)

width=getdimtheta(obj);

ngabors=size(gp,2);

A=ones(width,1)*gp(1,:);
sigmasq=ones(width,1)*gp(2,:);
center=ones(width,1)*gp(3,:);
omega=ones(width,1)*gp(4,:);


x=[((1:width)-(1+width)/2)]';
x=x*ones(1,ngabors);


%shift by cent
x=x-center;

%
npdf=exp(-x.^2./(2*sigmasq));
k=npdf.*cos(x.*omega);

sval=A.*k;
