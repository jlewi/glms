%function GaborTanSpace('theta')
%   theta - point at which to evaluate the tangent space
%         - we find the closest gabor to this point
%           and thats where we evaluate the tangent space
%
% Explanation: Represent the tangent space for the 1-d Gabor function.

function obj=theta(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'sparam','dimtheta'};
con(1).cfun=1;

con(2).rparams={'theta'};
con(2).cfun=2;

%Create a representation of the gabor at the specified values
%   width = width of the gabor
% con(2).rparams={'A','center','sigmasq','omega','width'};
% con(2).cfun=2;
%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%gparam   - gabor parameters at which we evaluate the tangent space
%gparam=struct('A',[],'center',[],'sigmasq',[],'omega',[]);

%declare the structure
obj=struct('version',071123,'gp',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'GaborTanSpace',TanSpaceBase());
        return
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
                    
        %create the base class
        bparams.sparam=params.sparam;
        bparams.dimtheta=params.dimtheta;
        
        pbase=TanSpaceBase(bparams);                
        obj=class(obj,'GaborTanSpace',pbase);
        
        %compute theta corresponding to this sparam
        theta=submanifold(obj,getsparam(obj));
        obj=settheta(obj,theta);
        
        %compute the basis
        basis=gradsubmanifold(obj,getsparam(obj));
        
        obj=setbasis(obj,orth(basis));
    case 2
           %create the base class
        bparams.theta=params.theta;
        bparams.dimsparam=4;
        
        %this is the point to initialize our search for the
        %best gabor
        if isfield(params,'gpstart')
            gpstart=params.gpstart;
        else
            gpstart=[];
        end
        pbase=TanSpaceBase(bparams);
        obj=class(obj,'GaborTanSpace',pbase);
        
        %project theta onto the manifold
        sparam=proj(obj,gettheta(obj),gpstart);
        %compute theta corresponding to sparam
        theta=submanifold(obj,sparam);
        
        obj=setsparam(obj,sparam);
        obj=settheta(obj,theta);
        
        basis=gradsubmanifold(obj,getsparam(obj));
        obj=setbasis(obj,orth(basis));
    otherwise
        error('Constructor not implemented')
end







