%function grad=gradsubmanifold(obj,sparam)
%   obj - child of TanSpaceBase 
%   sparam - parameters of the submanifold on which to evaluate the
%   gradient
%
%Return value:
%   grad = dim(theta) x dimsparam
%       gradient of theta with respect to the submanifold
%
%Explantion: Computes the jacobian of theta w.r.t to the parameters of the
%submanifold (i.e of the gabor function).
%   
function grad=gradsubmanifold(obj,gp)

A=gp(1);
omega=gp(2);
phi=gp(3);

t=linspace(0,1,getdimtheta(obj))';
dA=sin(omega*t+phi);
domega=A*t.*cos(omega*t+phi);

dphi=A*cos(omega*t+phi);

grad=[dA domega dphi];