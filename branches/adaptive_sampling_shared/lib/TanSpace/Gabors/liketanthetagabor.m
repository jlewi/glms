%function ptanthetagabor(x,theta,stim,obsrv,glm,omega,sigma)
%   tind - index of the element in the gabor we want to to comput
%
%   stim - matrix of the stimuli
%   obsrv - the responses
%   glm   - the GLM model
%
%    pparam - is a 2d gaussian distribution on the parameters of the gabor
%           which are the amplitude and center of the gabor
%           [A;C]
%
%
%Return value
%   p=p(theta(x)=theta);
%   fcount = number of function evaluations
%Explanation:
%   theta(x)=A cos((x-c)*omega)*exp(-.5*(sigma^-2)*(x-c))^2;
%
%  Computes the true marginal log likelihood on each value of theta
%
%   Given omega, sigma,
%   This computes the loglikelihood of theta(x)=theta given the data in
%   (stim,obsrv)
%
%   Note this is the marginal distribution; i.e we marginalize over the
%   other components of theta.
function [p,fcount]=logliketanthetagabor(x,theta, stim,obsrv,glm,omega,sigmasq)

if ~strcmp(getdist(glm),'poisson')
    error('code assumes observations are poisson distributed');
end
%set limits of c to length of theta
cmin=-(length(stim)+1)/2;
cmax=-cmin;

cmin=-3;
cmax=3;

%compute the constants for the log likelihood i.e log h(r)
%because otherwise log-likelihoods become very sharply peaked which
%creates problems when we numerically integrate
%for poisson distribution 
%h(r)=1/r!
r=log([1:max(obsrv)]);
r=cumsum(r);
lognc=sum(-r(obsrv(find(obsrv>0))));

[p, fcount] = quad(@(c)(prob(c,x,theta,stim,obsrv,glm,omega,sigmasq,lognc)),cmin,cmax);

%this is the funciton which evaluates the probability as a funciton of c
%c - values of centers for which we want to compute the log likelihood.
% stim - dxn matrix of stimuli
% obsrv - 1xn matrix of responses
% glm   - glm
function p=prob(c,x,val,stim,obsrv,glm,omega,sigmasq,lognc)
dim=size(stim,1);
%for this value of c we need to compute a
a=val./(cos((x-c)*omega).*exp(-.5*1/sigmasq*(x-c).^2));

%we need to evaluate the probability of c,a
params=[a;c];

%we need to evaluate the loglikelihood of each pair of (a,c)
%and each observation.
%for each a, c we need to compute theta so that we can compute the log
%likelihood
ll=zeros(1,size(params,2));
ind=([1:dim]-(1+dim)/2)';



for pind=1:size(params,2);
    theta=a(pind)*(cos((ind-c(pind))*omega).*exp(-.5*1/sigmasq*(ind-c(pind)).^2));  
    ll(pind)=sum(loglike(glm,obsrv,theta'*stim))+lognc;
end

p=exp(ll);