%function theta=submanifold(obj,x)
%   obj - descendent of TanSpaceObj
%   x   - point at which to evaluate the submanifold
%
%Explanation: Evaluates the submanifold for this point.
function theta=submanifold(obj,x)

error('Submanifold should only be evaluated for children of TanSpaceBase.m. TanSPaceBase.m is an abstract class.');
