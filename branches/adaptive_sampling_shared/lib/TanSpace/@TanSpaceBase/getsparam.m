%function getsparam=sparam(obj)
%	 obj=TanSpaceBase object
% 
%Return value: 
%	 sparam=obj.sparam 
%
function sparam=getsparam(obj)
	 sparam=obj.sparam;
