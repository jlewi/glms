%function getdimsparam=dimsparam(obj)
%	 obj=TanSpaceBase object
% 
%Return value: 
%	 dimsparam=obj.dimsparam 
%
function dimsparam=getdimsparam(obj)
	 dimsparam=obj.dimsparam;
