%function tantotheta(obj,b)
%   obj - the TanSpace object
%   b- the coordinates in the tangent spce
%
%Explanation map the coordinates in the tangent space to a point in full
%theta space
function theta=tantotheta(obj,b)

basis=getbasis(obj);
theta=gettheta(obj)*ones(1,size(b,2))+basis*b;