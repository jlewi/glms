%function getdegree=degree(obj)
%	 obj=PolyTanSpace object
% 
%Return value: 
%	 degree=obj.degree 
%
function degree=getdegree(obj)
	 degree=getdimsparam(obj);
