%function ptheta=proj(obj,theta)
%   theta- value of theta
%
%Return value:
%   coeff - projection of theta onto the submanifold
%           Return value is the value in terms of the parameters of the
%           manifold
function coeff=proj(obj,theta)

%compute the x values
x=[1:getdimtheta(obj)]'*ones(1,getdegree(obj));

%exponents to raise x to 
powers=ones(getdimtheta(obj),1)*[0:(getdimsparam(obj)-1)];
x=x.^powers;

%coeff is just the least squared solution
%Solve theta=x'coeff for coeff
%Least squares solution is x'*theta=(x'*x)*coeff
%Use Matlab left division which solves X*A=B for X
%
coeff=(x'*theta)'/(x'*x)';
coeff=coeff';
