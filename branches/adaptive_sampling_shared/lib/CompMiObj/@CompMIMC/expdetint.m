%function [expval]=expdetint(mueps,sigma,opt)
%         mueps - dot product of the mean and the stimulus
%         sigma - covariance =x^t covar_t x
%         opt   -
%               .inttol    - tolerance for numerical integration of outer
%                                expectation
%
%Return Value:
%       expval - the expected value.
%           = explogjobs +t2
%       explogjobs - first term
%       t2  - 2nd term
%
%Explanation: Computes:
% E_\epsilon E_r log2 |(I+U'C_tU)|
% U= X J
% J= diagonal matrix
%  = ith entry (Jobs(r_t+i,x_t+i^t\theta))^.5
% E_\epsilon E_r log2(I+U'C_tU) 
%   = E_\epsilon E_r [log |J| + log |J^-1 I J^-1 +X^T C_t X| ]
% Computes first ter via direct integration because its a sum of 2-d
% integrals. 2nd term we compute using importance sampling.
%
%
%This requires computing an expectation over r which is distributed according 
%to some distribution in the exponential distribution given by the miobj.CompMIBase.glm.
%Distribution is paramterized by glmproj which is a gaussian r.v with mean
%mueps and variance sigma.
%
%Limitations: Currently this only works for responses which are poisson
%distributed. Extending it to other distributions shouldn't it be
%difficult. The only real issue is in computing the bounds of integration
%and we can do this by using the cdf function.
%
%Currently it assumes the likelihood is a discrete function but it would be
%easy to extend this
%
%$Revision$
%   made this a method of the CompMINum
%   miobj.CompMIBase.glm,confint are optained from the clas rather than as parameters
function [expval,explogjobs,t2]=expdetint(miobj,mueps,sigma)
mbounds=zeros(1,2);     %bounds for integration over the gaussian variable.


if ~isscalar(mueps)
    error('mueps should be scalar');
end

confint=miobj.confint;

isdiscrete=1;           %determines whether r is discrete or continuous variable

glm=getglm(miobj.CompMIBase);

%*****************************************************************
%Compute the first term directly
%*********************************************************************
explogjobs=expjobs(miobj,mueps,sigma);


%*****************************************************************
%Compute the 2nd term via monte carlo methods
%*********************************************************************
%1. draw nmcsamps from p(\epsilon)
epsilon=normrnd(mueps,sigma,1,miobj.nmcsamps);

%2. Draw a sample from p(r|epsilon) 
r=glm.sampdist(glm.fglmmu(epsilon));

%estimate the value for each term
[d1 d2]=d2glmeps(glm,epsilon,r,'v');
jobs=-1*d2;

t2=0;

for i=1:miobj.nmcsamps
   t2=t2+log2 (det (diag(jobs(i).^-1)+sigma));
end
t2=1/miobj.nmcsamps*t2;
expval=explogjobs+t2;

