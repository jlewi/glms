%function sim=ClassName(glm,confint)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: This class computes the mutual information as a function of
%  the \glmproj, \sglmproj using numerical integration to evaluate the
%  integrals for each call
%
%Revisions:
%  12-01-2008 - add the field jscale
%             - this is a scaling factor for JObs.
%             We need this scaling factor to compute the lower bound in the
%             case of batch stimuli. Otherwise it equals 1. 
%  09-20-2008 - Converted to the new object model
%      delete field bname
%
classdef (ConstructOnLoad=true) CompMINum < CompMIBase
    %*************************************************
    %properties
    %*************************************************
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %  bname  - name of base class. This allows us to refer to it templates
    %declare the structure
    %  confinit - confidence interval is used to define the interval for
    %             numerical integration
    properties(SetAccess=private,GetAccess=public)
        version=081201;
        confint=.99;
    end

    properties(SetAccess=public,GetAccess=public)
       %jscale needs to be setable because CompMIPoissCanonLB/compminfo
       %needs to be able to set it based on the batch size.
        jscale=1;
    end
        
    
    
    methods
        
        %Return value:
        %  mi - always double
        [mi]=complogminfo(obj,var1,var2); 
    
        %Return value:
        %  mi - double or type MP        
        [mi]=compminfo(obj,var1,var2);
        
        function obj=CompMINum(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'glm'};
            con(1).cfun=1;

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    bparams=[];
                    params=[];
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
         
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=[];
                case 1
                    bparams=params;
                    if isfield(params,'confint');
                    bparams=rmfield(bparams,'confint');
                    end
                otherwise
                    %remove fields which are for this class
                    error('Unrecognized constructor');
            end



            obj=obj@CompMIBase(bparams);

            %************************************
            %optional parameters
            if isfield(params,'confint');
                obj.confint=params.confint;
            end
            
            if isfield(params,'jscale')
               obj.jscale=params.jscale; 
            end

        end
    end

    methods (Static)
        %a simple test function to test construction 
        success=test();
        [tbl,otbl]=testexpdetint();
    end
end
