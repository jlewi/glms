%function compmi(obj, muproj,sigma)
%   muproj -1xn array of values 
%               \mu^t\stim
%   sigma - 1 xn array of values
%       \stim^t\covar\stim
%
% Explanation: evaluates the mutual information for a stimulus as a
% function of the two scalar variables byusing numberical integration.
function mi=compmi(obj,muproj,sigma)

except=MException('CompMI:Obsolete','Function compmi has been replaced by compminfo or complogminfo');
throw(except);

