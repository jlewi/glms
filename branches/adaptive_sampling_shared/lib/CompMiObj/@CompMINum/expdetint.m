%function [expval]=expdetint(mueps,sigma,opt)
%         mueps - dot product of the mean and the stimulus
%         sigma - covariance =x^t covar_t x
%         opt   -
%               .inttol    - tolerance for numerical integration of outer
%                                expectation
%
%Return Value:
%       expval - the expected value.
%
%Explanation: Computes the expected determinant of the new covariance
%matrix by numerically integrating it as a function of mueps and sigma.
%This requires computing an expectation over r which is distributed according 
%to some distribution in the exponential distribution given by the miobj.CompMIBase.glm.
%Distribution is paramterized by glmproj which is a gaussian r.v with mean
%mueps and variance sigma.
%
%Limitations: Currently this only works for responses which are poisson
%distributed. Extending it to other distributions shouldn't it be
%difficult. The only real issue is in computing the bounds of integration
%and we can do this by using the cdf function.
%
%Currently it assumes the likelihood is a discrete function but it would be
%easy to extend this
%
%$Revision$
%   made this a method of the CompMINum
%   miobj.CompMIBase.glm,confint are optained from the clas rather than as parameters
function [expval]=expdetint(miobj,mueps,sigma,opt)
mbounds=zeros(1,2);     %bounds for integration over the gaussian variable.

if ~isscalar(mueps)
    error('mueps should be scalar');
end

if ~exist('opt','var')
    opt=[];
end
confint=miobj.confint;
if ~isfield(opt,'inttol')
    inttol=10^-6;
else
    inttol=opt.inttol; %tolerance for integration function
end
isdiscrete=1;           %determines whether r is discrete or continuous variable


%*****************************
%compute the bounds for mu
stdnorm=norminv(1-(1-confint)/2,0,sigma^.5);
mbounds=[mueps-stdnorm,mueps+stdnorm];

%*****************************************************************
%Distribution of specific code
%****************************************************************
isdiscrete=miobj.glm.isdiscrete;

%****************************************************************
%integrate by calling quad if its discrete distribution
%integrate by calling dblquad if its continuous likelihood funciton
%*************************************************************
if (isdiscrete==1)
    %the function we integrate is the value of the inner expectation
    %times the probability of that value of mu
    
    func=@(g)(intfun(miobj,g,mueps,sigma));
    expval=quad(func,mbounds(1),mbounds(2),inttol);
else
    error('Code to handle continuous distributions is not implemented');
end
