%function success=test()
%
%Return value:
%    success=true if tests passed
%
%Explanation: A simple static function to test construction
function success=test()
success=true;

fprintf('CompMINum: Testing construction for canonical Poisson\n');

glm=GLMPoisson('canon');
mi=CompMINum('glm',glm);

%verify rexpdisc
npts=10;
glmproj=randn(1,npts);
sigma=rand(1,1);

expvals=rexpdisc(mi,glmproj,sigma);

r=rand(1,1);
j=jobs(glm,r,glmproj);

objfun=log2(1+j*sigma);

if (any(abs((expvals-objfun)./objfun))>10^-8)
    success=false;
    fprintf('Error: rexpdisc.m for canonical Poisson does not appear to be correct.\n');
end