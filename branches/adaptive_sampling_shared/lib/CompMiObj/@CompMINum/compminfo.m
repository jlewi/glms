%function compminfo(obj, batches,post)
%   batches - matrix of inputs each column is a different stimulus 
%   post    - GaussPost object        
%   
%function compminfo(obj, muproj,sglmproj)
%   muproj -1xn array of values 
%               \mu^t\stim
%   sigma - 1 xn array of values
%       \stim^t\covar\stim
%
% Explanation: Note there are two different calling syntaxes for this
% function
%
%
function mi=compminfo(obj,var1,var2,jscale)

%***************************************************************
%determine the appropriate calling syntax
%****************************************************************
if isa(var2,'GaussPost')
    if (size(var1,1)~=getdim(var2))
        except=MException('CompMI:WrongInput','Expected three arguments: CompMI object, input matrix, Gauss Posterior.');
        throw(except);
    end
    
    %compute murporj and sigma
    muproj=var1'*getm(var2);
    sigma=qprod(var1,getc(var2));
   
else
    muproj=var1;
    sigma=var2;
end

if (length(muproj)~=length(sigma))
    error('mu and sigma should have same dimension');
end

npts=length(muproj);
mi=zeros(1,length(muproj));

pinc=.1;
plast=0;
for j=1:length(muproj)
    pcomplete=j/npts*100;
    if ((pcomplete-plast)>pinc)
       fprintf('Compute mi, %2.2g%% complete \n',pcomplete);
       pcomplete=plast;
    end
[mi(j)]=expdetint(obj,muproj(j),sigma(j));
end