%function refinemu(muproj)
%       muproj - additional values of mu at which to precompute the dot
%       product
%
% Explanation: allows new values of mu to be added to the grid 
function [pobj]=refinemu(pobj,muproj)

   muproj=sort(muproj,'ascend');
   
    
   %remove any values from sigmasq if we've already computed them
   for i=1:length(muproj)
       if ~isempty(find(pobj.muproj==muproj(i)))
           muproj(i)=NaN;
       end
   end
   ind=find(~isnan(muproj));
   muproj=muproj(ind);
   
   
   %compute the values of the objective for all values of sigma
   nmi=zeros(length(muproj),length(pobj.sigma));
   nmi=compmi(pobj,muproj,pobj.sigma);
   
   
   oldmu=length(pobj.muproj);        %original length
   ntoadd=length(muproj);        %number to add
   newfmi=zeros(oldmu+ntoadd,length(pobj.sigma));
   newmuproj=zeros(oldmu+ntoadd,1);
   %now we need to merge the arrays
   %we do this as a merge sort
   indold=1;
   indnew=1;
   
   fullind=1;
   while (indold<=oldmu && indnew <=ntoadd)
    if (muproj(indnew)<pobj.muproj(indold))
        newmuproj(fullind)=muproj(indnew);
        newfmi(fullind,:)=nmi(indnew,:);
        indnew=indnew+1;
    else
         newmuproj(fullind)=pobj.muproj(indold);
        newfmi(fullind,:)=pobj.mi(indold,:);
        indold=indold+1;
    end
        fullind=fullind+1;
   end
    
   if (indold<=oldmu)
       newmuproj(fullind:end)=pobj.muproj(indold:end);
       newfmi(fullind:end,:)=pobj.mi(indold:end,:);
   end
   if (indnew<=ntoadd)
       newmuproj(fullind:end)=muproj(indnew:end);
       newfmi(fullind:end,:)=nmi(indnew:end,:);
   end
   
   pobj.muproj=newmuproj;
   pobj.mi=newfmi;