%function compmi(obj, muproj,sigma)
%   muproj -1xn array of values 
%               \mu^t\stim
%   sigma - 1 xn array of values
%       \stim^t\covar\stim
%
% Explanation: evaluates the mutual information for a stimulus as a
% function of the two scalar variables. Uses a grid on which the mutual
% information is precomputed. If the point is outside the range of
% precomputed values then we just use the nearest gridpoint.
function mi=compmi(obj,muproj,sigma)


%if muproj or sigma is nan it will causs problems
mi=interp2(obj.muproj,obj.sigma,obj.mi,muproj,sigma);

%find all points outside the boundaries
%these points will have NaN
inan=find(isnan(mi));

if ~isempty(inan)
    %determine the boundary points
    %select the bottom, top left right boundaries
    muind=[1:length(obj.muproj) 1:length(obj.muproj) 1*ones(1,length(obj.sigma)) length(obj.muproj)*ones(1,length(obj.sigma))];
    sigind=[1*ones(1,length(obj.muproj)) length(obj.sigma)*ones(1,length(obj.muproj)) 1:length(obj.sigma) 1:length(obj.sigma)];
    bpts.muproj=obj.muproj(muind);
    bpts.sigma=obj.sigma(sigind);

    miind=sub2ind(size(obj.mi),sigind,muind);
    bpts.mi=obj.mi(miind);
end
for index=inan
   %compute and minimize the distance of this point from the precomputed
   %points on the boundary
   
   dist=(muproj(index)-bpts.muproj)'.^2+(sigma(index)-bpts.sigma).^2;
   [m mind]=min(dist);
   mi(index)=bpts.mi(mind);
 
end
if ~isempty(inan)
fprintf('PreCompMIInterp/compmi.m: %d of %d stimuli our outside the precomputed region. \n',length(inan), length(mi));
end
