%function refinemu(sigma)
%       sigma - additional values of mu at which to precompute the dot
%       product
%
% Explanation: allows new values of mu to be added to the grid 
%
% 
function [pobj]=refinemu(pobj,sigma)

   sigma=sort(sigma,'ascend');
   
    
   %remove any values from sigma if we've already computed them
   for i=1:length(sigma)
       if ~isempty(find(pobj.sigma==sigma(i)))
           sigma(i)=NaN;
       end
   end
   ind=find(~isnan(sigma));
   sigma=sigma(ind);
   
   
   %compute the values of the objective for all values of sigma
   nmi=zeros(length(sigma),length(pobj.muproj));
   nmi=precompmi(pobj,pobj.muproj,sigma);
   
   
   oldsigma=length(pobj.sigma);        %original length
   ntoadd=length(sigma);        %number to add
   newfmi=zeros(oldsigma+ntoadd,length(pobj.muproj));
   newsigma=zeros(oldsigma+ntoadd,1);
   %now we need to merge the arrays
   %we do this as a merge sort
   indold=1;
   indnew=1;
   
   fullind=1;
   while (indold<=oldsigma && indnew <=ntoadd)
    if (sigma(indnew)<pobj.sigma(indold))
        newsigma(fullind)=sigma(indnew);
        newfmi(fullind,:)=nmi(indnew,:);
        indnew=indnew+1;
    else
         newsigma(fullind)=pobj.sigma(indold);
        newfmi(fullind,:)=pobj.mi(indold,:);
        indold=indold+1;
    end
        fullind=fullind+1;
   end
    
   if (indold<=oldsigma)
       newsigma(fullind:end)=pobj.sigma(indold:end);
       newfmi(fullind:end,:)=pobj.mi(indold:end,:);
   end
   if (indnew<=ntoadd)
       newsigma(fullind:end)=sigma(indnew:end);
       newfmi(fullind:end,:)=nmi(indnew:end,:);
   end
   
   pobj.sigma=newsigma;
   pobj.mi=newfmi;