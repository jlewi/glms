%function sim=CompMIBase(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: This is the base abstract class for computing the mutual information
%   as a function of \glmproj, \sigma
%
%
%Revisions:
%   11-25-2008 - don't make complogminfo abstract because we can provide a
%   default implementation
%   09-17-2008 - Define nbits to store the number of bits to use
%                when using a multiple precision object to store the mutual
%                information
%              - Add functions compminfo and complogminfo
%              -compminfo can return a double or an MP object
%                  we do this because to comput the mutual info we 
%                  may need to add very large numbers which we can't do
%                  using doubles
%              - complogminfo - Computes the logarithm of the mutual
%              information by calling compminfo
%                - this should always return a DOUBLE
%
%   09-15-2008 - Make CompMIBase derived from the handle class
%                do this because some sub classes we might want to save
%                intermediary values like in CompMIPoissExpTaylor
%   08-11-2008
%       Use new object model
%       remove field bname
classdef (ConstructOnLoad=true) CompMIBase < handle
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %  bname  - name of base class. This allows us to refer to it templates
    %declare the structure
    properties(SetAccess=private,GetAccess=public)
        bversion=080917;
        
    end

    properties(SetAccess=protected,GetAccess=public)
        glm=[];
        nbits=150;
        
        
    end
    

    methods (Abstract)
        %Return value:
        %  mi - double or type MP    
        %This should return the information in bits
        [mi]=compminfo(obj,batches,post);
        
        
    end
    
    methods
       %Return value:
        %  mi - always double
        [mi]=complogminfo(obj,batches,post);
        function obj=CompMIBase(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'glm'};





            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=[];
                    cind=0;
                case 1
                    %pass in an empty structure from derived classes when
                    %loading the object
                    if isempty(varargin{1})
                        cind=0;
                    else
                     
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
                    end
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            switch cind
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    return
                case 1
                    obj.glm=params.glm;
                    if ~isa(obj.glm,'GLMModel')
                        error('GLM must be an object of type GLMModel');
                    end
            end

        end
    end
end