%function compimat(obj,inpbatch,post)
%
%Explanation:
%The upper bound for the information is
%   log | I + E_\theta E_obsrv U' Ct U|
%   Where U is a matrix of the inputs scaled by the observed fisher
%   information
%
%   This function is used by compminfo. We use a separate function
%   b\c we also use this matrix to determine when the Taylor expansion of 
%   log | I + E_\theta E_obsrv U' Ct U| converges.
%
function imat=compimat(obj,inpbatch,post)

if iscell(inpbatch)
    me=MException('CompMI:WrongType','Batches must be a matrix not a cell array.');
    throw(me);
end
bsize=size(inpbatch,2);
c=getc(post);

%we need to compute the expectations E_\theta E_\obsrv J_obs^.5 J_obs^.5(T)
%This is a symetric matrix so we just compute the upper triangular part
%we start by computing the mean and variance for each term
mu=zeros(bsize,bsize);
sigma=zeros(bsize,bsize);

for i=1:bsize
   %we need to add input_i+input_j for all j>=i
   isum=inpbatch(:,i)*ones(1,bsize-i+1)+inpbatch(:,i:end);
   
   mu(i,i:end)=1/2*getm(post)'*isum;
   sigma(i,i:end)=1/4*qprod(isum,c);
end

expjobs=exp(mu+1/2*sigma);

imat=compquad(post,inpbatch).*expjobs;

%now get the lower triangular part
imat=triu(imat)+triu(imat,1)';
