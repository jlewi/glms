%function [mi,obj]=compmi(obj, batches,post)
%   batches - A matrix containing a single batch of inputs
%           
%           -
%   post
%       -posterior
%
% Return value:
%   mi  - DOUBLE or MP - where MP is a multi-precision object
%
%   obj -we return the object becasue we save intermediary values which
%         are used to compute the terms of the mutual information for all
%         batches
%       -  NOt really necessary anymore because its a handle
%       
% Explanation: We can only process a single batch because we want
%    to return either a double or a MP object depending on how many
%   bits are needed to encode the bits.
%   Therefore, if we compute multiple batches then we either have to use a
%   cell array, or else the mi for all batches must be an MP object which
%   would be inefficient
%
function [mi,obj]=compminfo(obj,inpbatch,post)

bsize=size(inpbatch,2);


mi=0;

imat=compimat(obj,inpbatch,post);

%we want this to be in bits
%the 1/2 factor just makes it bits
mi=1/2*log(det(eye(bsize)+imat))/log(2);
