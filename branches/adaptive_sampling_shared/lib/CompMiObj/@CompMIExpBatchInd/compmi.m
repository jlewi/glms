%function compmi(obj, muproj,sigma)
%   muproj -bsizexn array of values 
%               \mu^t\stim
%   sigma - bsizexbsizexn array of values
%       \stim^t\covar\stim
%
% Explanation: evaluates the mutual information for a stimulus as a
% function of muproj and sigma
function mi=compmi(obj,muproj,sigma)

%we just compute the mutual information for each stimulus and then sum the
%results

nbatch=size(muproj,2);
mi=zeros(1,nbatch);

for sind=1:nbatch
    mi(sind)=sum(compmi(obj.PoissExpCompMi),muproj(:,sind)',diag(sigma(:,:,sind))');
end
  