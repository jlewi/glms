%function testsub(bsize,pow)
%
%Explanation: We want to test to make sure our formula for tr(A^n) is
%correct. Here we test this as follows
%  A=S'C_tS J
%  J= diagonal matrix with the observed fisher information for each element
%  in the batch.
%
%  Thus we consider two cases: S'C_tS = I and J= I
%  For each of these cases we can easily comput tr(A^n) and verify 
%  it matches the output of compcterms and compjterms
function success=testsub(bsize,pow)
success=true;

%for the purposes of this function the number of terms doesn't matter
obj=CompMIPoissExpTaylor('nterms',5');

%***************************************************************
%test cterms
%**************************************************************8
%generate a random posterior
dstim=bsize+1;
mu=randn(dstim,1);
evecs=orth(randn(dstim,dstim));
eigd=rand(dstim,1);
post=GaussPost(mu,evecs*diag(eigd)*evecs');

bstim=randn(dstim,bsize);

cterms=compcterms(obj,bstim,post,pow);

%to comptue the cterms we set the bstim=zero matrix as this ensures
%J is the identity matrix
jterms=compjterms(obj,zeros(dstim,bsize),post,pow);

trval=cterms'*jterms;

trueval=trace((bstim'*getc(post)*bstim)^pow);

if (abs((trval-trueval)/trueval)>10^-10)
    fprintf('Error: Test failed when J is diagonal matrix. Most likely bug in compcterms \n');
    success=false;
end

%***************************************************************
%test cterms
%***************************************************************

%now we set S' C_t S' to the identity matrix
pident=GaussPost(mu,eye(dstim));
bident=[eye(bsize); zeros(dstim-bsize,bsize)];

cterms2=compcterms(obj,bident,pident,pow);

jterms2=compjterms(obj,bstim,post,pow);

trval2=cterms2'*jterms2;

%to compute the true value we just sum the stimuli in the batch
mu=(bstim'*getm(post))';
sigma=qprod(bstim,getc(post));

trueval2=sum(exp(pow*mu+1/2*pow^2*sigma));

if (abs((trval2-trueval2)/trueval2)>10^-10)
    fprintf('Error: Test failed when S C_t S is diagonal matrix. Most likely bug in compjterms \n');
    success=false;
end

if (success)
    fprintf('Success: Both tests passed. \n');
else
    fprintf('Error: Tests failed. \n');
end
