%function [mi,obj]=compmi(obj, batches,post)
%   batches - A matrix containing a single batch of inputs
%           
%           -
%   post
%       -posterior
%
% Return value:
%   mi  - DOUBLE or MP - where MP is a multi-precision object
%
%   obj -we return the object becasue we save intermediary values which
%         are used to compute the terms of the mutual information for all
%         batches
%       -  NOt really necessary anymore because its a handle
%       
% Explanation: We can only process a single batch because we want
%    to return either a double or a MP object depending on how many
%   bits are needed to encode the bits.
%   Therefore, if we compute multiple batches then we either have to use a
%   cell array, or else the mi for all batches must be an MP object which
%   would be inefficient
%
function [mi,obj]=compminfo(obj,inpbatch,post)

if iscell(inpbatch)
    me=MException('CompMI:WrongType','Batches must be a matrix not a cell array.');
    throw(me);
end

c=getc(post);

mi=0;

%we compute the mutual information of each input
%set the size of jscale to the batch size
bsize=size(inpbatch,2);
obj.mihelper.jscale=bsize;
mi=1/bsize*sum(compminfo(obj.mihelper,inpbatch,post));
