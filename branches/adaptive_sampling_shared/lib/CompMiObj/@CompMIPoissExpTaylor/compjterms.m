%function expjext=compjterms(obj,inpbatch,post,pow);
%   inpbatch- batch of stimuli to compute the objective function for
%   post    - the posterior
%   pow     - which power we are raising the matrix to 
%
%Return value:
%   expjext - Double or MP object 
%           - We use an MP object when we have to increase the precision to
%           avoid getting infinity
%
%Explanation: This is a function which should only be called by comptrpow
%to compute the fisherinfo 
%
%We need to compute (E_theta prod_i J_exp(indexes(i))
%where J_exp=exp(\inp^T\theta)
%so we simply sum the stimuli for the corresponding batches
function expjexp=compjterms(obj,inpbatch,post,pow)

bsize=size(inpbatch,2);
[lindex,indexes]=getindexes(obj,bsize,pow);
%We need to compute (E_theta prod_i J_exp(indexes(i))
%where J_exp=exp(\inp^T\theta)
%so we simply sum the stimuli for the corresponding batches
logexpjexp=zeros(size(lindex,1),1);

for ind=1:size(indexes,1)
    s=sum(inpbatch(:,indexes(ind,:)),2);
    mu=s'*getm(post);
    sigma=s'*getc(post)*s;
    logexpjexp(ind)=mu+1/2*sigma;
end

%if the log of the exponential terms exceeds the largest number that can be
%encoded as a double then we increase the precision
if (any(logexpjexp>log(realmax)))
    %convert to a multy prec
	logexpjexp=mp(logexpjexp,obj.nbits);
end

expjexp=exp(logexpjexp);

if (any(isinf(expjexp)))
    me=MException('CompMI:MIInf','The mutual information for some of the batches is infinite. Most likely you just need to increase the number of bits of precision.');
    throw(me);
end