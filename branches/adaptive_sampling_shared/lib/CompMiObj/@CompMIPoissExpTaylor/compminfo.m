%function [mi,obj]=compmi(obj, batches,post)
%   batches - A matrix containing a single batch of inputs
%           
%           -
%   post
%       -posterior
%
% Return value:
%   mi  - DOUBLE or MP - where MP is a multi-precision object
%
%   obj -we return the object becasue we save intermediary values which
%         are used to compute the terms of the mutual information for all
%         batches
%       -  NOt really necessary anymore because its a handle
%       
% Explanation: We can only process a single batch because we want
%    to return either a double or a MP object depending on how many
%   bits are needed to encode the bits.
%   Therefore, if we compute multiple batches then we either have to use a
%   cell array, or else the mi for all batches must be an MP object which
%   would be inefficient
%
%   The Taylor approximation of log(1+x) is only valid if x is <1.
%   When x>=1 it is better to use the approximation log(1+x)\approx log(x)
%   Therefore we compute log(x). When log(x)>0 we use log(x) as the
%   approximation. Otherwise we use the Taylor approximation of log(1+x);
%
function [mi,obj]=compminfo(obj,inpbatch,post)


if iscell(inpbatch)
    me=MException('CompMI:WrongType','Batches must be a matrix not a cell array.');
    throw(me);
end
mi=0;

%******************************************************************
%compute E_theta E_r log (U'C_tU)
%*******************************************************************
logmi=log(det(inpbatch'*getc(post)*inpbatch))+sum(inpbatch,2)'*getm(post);

if (logmi>0)
    %use logmi
    %convert to arbitrary precision object before computing the exponential
    if (logmi>log(realmax))
        logmi=mp(logmi,obj.nbits);                               
    end
    mi=exp(logmi);
else
    %***********************************    **********************
    %compute the taylor approximation
    %*********************************************************
    
    %loop over and compute each terms
    for pow=1:obj.nterms
        %we need to compute tr(A^n)
        %A=ibatch'C_t*ibatch*J
        %J=diagonal matrix whose entries are the observed information of
        %each trial in the batch
        [trval,obj]=comptrpow(obj,inpbatch,post,pow);
        mi=mi+(-1)^(pow-1)*trval/pow;
    end
    
end

%convert mi into bits (currently it is in nats)
%multiply by 1/2 b\c entropy of Guassian is proportional to 1/2 log |C_t| 
mi=1/2*mi/log(2);
