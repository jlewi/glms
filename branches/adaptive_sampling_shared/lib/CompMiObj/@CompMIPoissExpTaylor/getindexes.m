%function [lindex,indexes]=getindexes(obj,bsize,pow)
%   bsize - the size of the batch
%   pow   - what term in the taylor expansion to compute the indexes for
%
%Explanation: This function computes the indexes into A for each term in
%the summation for tr(A^n)
% This function should only be called from compjterms and compcterms
%
function [lindex,indexes]=getindexes(obj,bsize,pow)
%check if we've already computed the indexes of the terms we need to add
%multiple
compindexes=false;
if (length(obj.tinfo)<pow)
    compindexes=true;
elseif(obj.tinfo(pow).bsize ~=bsize)
    %we computed the indexes for this power but the batch size has
    %since changed.
    compindexes=true;
end

if (compindexes)
    %check to make sure the size of the batch hasn't changed
    %from when we computed the indexes

    obj.tinfo(pow).bsize=bsize;
    obj.tinfo(pow).lindexes=[];
    obj.tinfo(pow).indexes=[];
    %****************************************************
    %compute a matrix where each row contains the indexes of a
    %different term in the summation for tr(A^n)

    %since n is unknown we generate a string with the apppriate command
    cmd='[i1';
    for i=2:pow
        cmd=sprintf('%s,i%d',cmd,i);
    end
    cmd=sprintf('%s]=ind2sub([%d',cmd,bsize);
    for i=2:pow
        cmd=sprintf('%s,%d',cmd,bsize);
    end
    cmd=sprintf('%s],[1:%d]);',cmd,bsize^pow);

    eval(cmd);

    %command to concatenate indexes together
    cmd='indexes=[i1''';
    for i=2:pow
        cmd=sprintf('%s i%d''',cmd,i) ;
    end
    cmd=sprintf('%s];',cmd);

    eval(cmd);


    lindex=zeros(size(indexes,1),pow);
    for p=1:pow-1
        %convert to linear indexes
        lindex(:,p)=sub2ind([bsize bsize],indexes(:,p),indexes(:,p+1));


    end
    lindex(:,pow)=sub2ind([bsize bsize],indexes(:,pow),indexes(:,1));


    obj.tinfo(pow).lindexes=lindex;
    obj.tinfo(pow).indexes=indexes;

end

lindex=obj.tinfo(pow).lindexes;
indexes=obj.tinfo(pow).indexes;