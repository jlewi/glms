%function [mi,obj]=compmi(obj, batches,post)
%   batches - a cell array of marices. Each matrix contains
%       the inputs in the batch in the columns of that matrix
%   post
%       -posterior
%
% Return value:
%   mi  - DOUBLE 
%
%   obj -we return the object becasue we save intermediary values which
%         are used to compute the terms of the mutual information for all
%         batches
%       -  NOt really necessary anymore because its a handle
%       
% Explanation: evaluates the mutual information using compminfo.
%      This function also returns a double because we are returning the
%      logarithm of the mutual information
%
function [mi,obj]=complogminfo(obj,batches,post)


if iscell(batches)
    nbatches=length(batches);
else
    nbatches=1;
    batches={batches};
end
mi=zeros(1,nbatches);


for bind=1:nbatches
    inpbatch=batches{bind};
    
    mi(bind)=double(log(compminfo(obj,inpbatch,post)));
    
end
