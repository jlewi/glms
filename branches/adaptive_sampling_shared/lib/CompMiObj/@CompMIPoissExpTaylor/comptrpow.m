%function comptrpow(inpbatch,pow)
%   inpbatch - batch of stimuli
%   pow      - power
%
%Explanation: Compute tr(A^n)
function [trval,obj]=comptrpow(obj,inpbatch,post,pow)


cterms=compcterms(obj,inpbatch,post,pow);


%compute J_exp=exp(\inp^T\theta)
expjexp=compjterms(obj,inpbatch,post,pow);

trval=cterms'*expjexp;

%if trval is infinite then we need to use an mp object before computing the
%dot product
if isinf(trval)
    trval=mp(cterms,obj.nbits)'*mp(expjexp,obj.nbits);
end

if isinf(trval)

    me=MException('CompMI:MIInf','The mutual information for some of the batches is infinite. Most likely you just need to increase the number of bits of precision.');
    throw(me);

end