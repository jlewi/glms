%function convertoldver(mobj,'model',mobserver)
%   mobj
%   mobserver - MParamObj to assign to the observer
%
%Explanation: This function is intended to be used to handle conversion of
%old versions of MParamObj to the new version.
%Thus it allows the GLM field to be set as in the old version this was not
%a field of mparamobj.
function oobj=convertoldver(oobj,varargin)

for j=1:2:length(varargin)
    switch lower(varargin{j})
        case {'model'}
            oobj.model=varargin{j+1};
    end
end
