%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
function obj=loadobj(lobj)

%error('12-30-2008. This function still needs to be updated');


cname='GLMSimulator';

%indicates whether we have already performed a copy
%copying the fields of lobj to obj when lobj is a structure
%because we only want to do the copying once
cstruct=false;

%get the latest version
%save the latest version so we can check to make sure all
%conversions have been completed.
eval(sprintf('newobj=%s();',cname));

latestver=newobj.version.(cname);

if isstruct(lobj)
    obj=newobj;
    if isfield(lobj,'version')
        obj.version.(cname)=lobj.version(cname);
    else
        obj.version.(cname)=0;
    end
else
    %we don't need to create a new object
    obj=lobj;
end



%******************************************************************
%Sequentially convert one version of the object to the next
%until we get to the latest version
%
%Warning: If lobj is a structure then we haven't copied the data from lobj
% to obj yet.
%******************************************************************


version1=081230;
if (obj.version.(cname) < version1)
    %do conversion
    %copy fields from old version to new version
    %if lobj is a structure
    if (isstruct(lobj) && ~cstruct)
        fskip={'deleted','ObserverBase'};    %fields not to copy
        obj=copyfields(lobj,obj,fskip);
        cstruct=true;
    end
    obj.version.(cname)=version1;
end

version1=090125;
if (obj.version.(cname) < version1)
    %do conversion
    %field maxobsrv was added and then removed because it causses problems.
    obj.version.(cname)=version1;
end
%check if converted all the way to latest version
if (obj.version.(cname) <latestver)
    error('Object has not been fully converted to latest version. \n');
end

%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
%
%Copy fields must be declared within loadobj because otherwise it won't
%have permssion to set the private fields of the object.
function dest=copyfields(source,dest,skip)
%we just need to copy the fields
%and handle any special cases if required
fnames=fieldnames(source);

%struct for object
sobj=struct(dest);
for j=1:length(fnames)
    switch fnames{j}
        case skip
            %do nothing we skip this field
        otherwise
            %set the new field this will cause an error
            %if the field isn't a member of the new object
            dest.(fnames{j})=source.(fnames{j});
    end
end




return;

%*************************************************************************
%01-08-2008
%below is the old version of loadobj.m
%********************************************************************
    %check if lobj is a structure
    %this indicates the class structure has changed and we need to handle
    %the conversion   
    if isstruct(lobj)
        warning('Saved object was an older version. Converting to newer version');
        %create a blank object
        obj=GLMSimulator();
        
        %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(lobj);
        
        %struct for object
        sobj=struct(obj);
        for j=1:length(fnames)
            %make sure field hasn't been delete
             if ~isfield(sobj,fnames{j})
                 warning('Field %s has been removed in newest version of class.',fnames{j});
                 %save the deleted field
                 obj.deleted.(fnames{j})=lobj.(fnames{j});
             else
                   obj.(fnames{j})=lobj.(fnames{j});
             end
        end
        %provide warning message about any new fields
        nfields='';
        fnames=fieldnames(sobj);
        for j=1:length(fnames)
            if ~isfield(lobj,fnames{j})
                nfields=sprintf('%s \n',fnames{j});
            end
        end
        warning('The following fields were not in the older version. They will be set to default values. \n %s',nfields);
    else
        obj=lobj;
    end
    
   