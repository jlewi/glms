%function [obj]=ObserverBase.m(varargin)
%
%Explnation: base class for all observers. This is an abstract class.
%Instantiate one of its child classes not the base class.
%
%Revisions:
%   12-30-2008 - Updated to new object model.
%
classdef (ConstructOnLoad=true) ObserverBase < handle

    properties(SetAccess=protected,GetAccess=public)
        version=struct();
    end

    methods
        function oobj=ObserverBase(varargin)
            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.


            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,{});


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %set the version to be a structure
            obj.version.ObserverBase=081230;

        end
    end
end