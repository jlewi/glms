%function [dk,var]=dkldata(lpost,lpapprox,gpost)
%   lpost - 1xm
%            - each entry is sum of the log of the prior +log of
%            observations for each model
%           -unnormalized
%   lpapprox - 1xm
%            -log of each model computed under our gaussian approximation of the posterior
%   gpost - gaussian approximation of the posterior
%           .m - mean
%           .c -covariance matrix
%   Return Value:
%       dk - kL divergence
%       vsum -variance of the integration estimated from the sample
% Explanation: Compute the KL divergence between q and p
%
% Everything is computed in Nat's (as opposed to bits) b\c we use
%    natural logarithm instead of base 2 log
function [dk,vsum]=dkcompute(lpost,lpapprox,gpost)

nsamp=length(lpost);

%pull out smallest value of lpost we can absorb this into the normalization
%constant
lpost=lpost-min(lpost);
%pull out a normalization constant on qm
%approximate the integral of q(m)*log(p(m))dm using monte carlo sampling
%We don't multiply by q(m) this is because we are adding up samples
%This assumes mpo and mll are the log likelihoods not the likelihood
intapprox=1/nsamp*sum(lpost,2);
%intapprox=1/nsamp*sum(lpost-lpapprox,2);

%normalization constant
%numerical issues
%when mq=0 exp(mp0+mll) should also be close to zero because
%mq is the gaussian approximation of the true posterior
%which is exp(mp0+mll)
%to prevent numerical errors from 0/0
%find mq=0. Set mq=1. And the associated numerators to 0.
%this way the entries end up being zero and not contributing to the
%divergence which I believe is the thing to do.
ind=find(lpapprox==0);

%to avoid numerical errors we need to pull out a factor
%before we compute the normalization constant otherwise it will be zero
%when take exponent
%logptot=mpo+mll;
%mfact=max(mtot);

%pest=exp(mpo+mll-mfact);
%if ~isempty(mq)
    %check those entries of pest to make sure they are zero
%    ind2=find(pest(ind)<=10^-16);
 %   if (length(ind2)==length(ind))
  %      pest(ind)=0;
   %     mq(ind)=1;
    %else
     %   warning('Get Divide by zero! in importance sampling !');
        %set it anyway
      %  pest(ind)=0;
      %  mq(ind)=1;
    %end
%end
%compute the importance of the samples
%keyboard
lisamp=lpost-lpapprox;

%now we need to sum the probabilities
%but to avoid numerical issues(0's) we pull out the largest value
mfact=max(lisamp);
lisamp=lisamp-mfact;       
logznorm=-log(nsamp)+mfact+log(sum(exp(lisamp),2));
%logznorm=-log(nsamp)+log(sum(exp(lisamp),2));
%keyboard;;
%***********************************************************************
%compute the entropy of gaussian posterior in Nats
if (length(gpost.m)>1)
    %compute entropy in Nats
    %to avoid numerical error due to log of 0 when post.c is small we
    %evaluate the determinant by summing the logs of the eigenvalues
    leigd=log(svd(gpost.c));

    ldetc=sum(leigd);
     hq=1/2*log((2*pi*exp(1))^length(gpost.m))+1/2*ldetc;
   % hq=1/2*log((2*pi*exp(1))^length(gpost.m)*det(gpost.c));
    %entropy should use base 2
    %hq=1/2*log2((2*pi*exp(1))^length(q.mu)*det(q.c));
else
    hq=1/2*log((2*pi*exp(1))*gpost.c);
    %entropy should use base 2
    %hq=1/2*log2((2*pi*exp(1))*q.c);
end
%have to add mfact back
dk=logznorm-intapprox-hq;

%dk=logznorm-intapprox;

%compute the variance of the integral approximation of 

vsum=1/nsamp^2*sum((lpost-intapprox).^2);
warning('not sure vsum is right');
return;
