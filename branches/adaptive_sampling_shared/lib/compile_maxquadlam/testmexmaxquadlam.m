%12-20-2007
%
%This script tests mexPOissExpObjfun.c

%create a random posterior
m=[2;0];
c=rand(2,2);
c=c*c';

efull=EigObj('matrix',c');
efull=sorteig(efull,1);
qobj=QuadModObj('C',c,'eigck',efull,'muk',m);

v=.25;
mag=1;
[qmax,pcon,xmax]= maxquadlam(qobj,v,mag)


%%
%********************************************************************
%Here I generate some data and save it to a .mat file
%I can then use my test function in .c and my debugger to debug the
%function using this data
%convert qobj to a structure;
qstruct=struct(qobj);

%create a structure which has as fields all the quantities which we need
%as inputs for mexmaxquadlam
data=qstruct.quad;
data.eigd=geteigd(data.eigmod);
data.evecs=getevecs(data.eigmod);
data.vopt=v;

[emax indmax]=maxeval(data.eigmod);
emax=emax(1);   %we want emax to be a scalar but if max eigenvalue is repeated
                %maxeval returns an array

%terms in the quadratic eqn we need to solve
ediff=data.eigd-emax;
ediff(indmax)=0;

data.ediff=ediff;
data.mag=mag;
data.muk=qstruct.muk;

save('./lib/compile_maxquadlam/testdata.mat','data');


[pcon,qmax,xmax]=mexmaxquadlam(data.vopt,data.eigd,data.ediff,data.wmuproj,data.w,data.dmuproj2,qobj.quad.dmuproj,data.d,data.mag,data.evecs,data.muk)
