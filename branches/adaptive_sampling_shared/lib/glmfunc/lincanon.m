%function [r,d1,d2]=lincanon(k,x)
%       k - filter coefficents
%           should be column vector
%       x - stimulus
%
%function r=glm1dexp(u)
%       u - k'x
%       d1 - 1st derivative
%       d2 - 2nd derivative
%       d1,d2 computed w.r.t u=kx if only 1 parameter provided otherwise
%           w.r.t x
%Explanation computes the canonical parameter of a glm
%   assuming the canonical parameter is linearly related to the projection
%   of the stimulus onto the parameters.
%
function [r,d1,d2]=lincanon(k,x)
    
    if exist('x','var')
        r=k'*x;
        %compute the derivatives
        d1=k';

        error('080707. I changed d2 to be a matrix of zeros because I think my previous computation was wrong. You should verify it though.');
        d2=zeros(length(k),length(k))
        %d2=r*k*k';
    else
        r=k;
        %derivatives arejust the rate
        d1=1;
        d2=0;
    end
    
    
    

