%function logisticcanon(pcon)
%   pcon = x'*theta
%           =1xn - n = number of different values to compute at
%Explantion:This is the canonical link function
%   for the logistic distribution. 
function [r,d1,d2]=logisticanon(pcon)
    r=1./(1+exp(-pcon));
    d1=exp(-pcon)./(1+exp(-pcon)).^2;
    d2=2*exp(-2*pcon)./(1+exp(-pcon)).^3-exp(-pcon)./(1+exp(-pcon)).^2;
    