%$Revision$: 06-14-2007
%      Created script to test d2glmeps in 1-d. 
%      I compute the 1st and 2nd derivative two ways
%           1. analytically
%           2. numerically using the analytical expressions for the
%           likelihood and derivative of the likelihood respectively 
%      I verify that these are similar

%Compute the Jacobian using the functions. Compare it to the value computed
%using a finite difference method
glm=GLMModel('binomial','canon');
glm=GLMModel('poisson','canon');
dstep=10^-4;  %stepsize for numerical computations
%the additive constant due to scaling factor of the log likelihood for
%binomial distribution
%logsfactor=@(x)(zeros(size(x));
%WARNING: if you change the distribution you need to account for the
%scaling factor in the log-likelihood
%glm=GLMModel('poisson','canon'); 
epsvar=-10:10;
epsvar=[1 1.5 3];
nepsvar=length(epsvar);
%generate some random observations
obsrv=(0:10:100)';
obsrv=[1 10 100]';
nobsrv=length(obsrv);


%**************************************************************************
%Compute the 1st and 2nd derivative using the functions
%******************************************************
[dfunc.deps,dfunc.d2eps]=d2glmeps(epsvar,obsrv,glm,'m');

%****************************************************
%compute 1st and second derivate numerically
%**********************************************************
dnum.deps=zeros(nobsrv,nepsvar);
dnum.d2eps=zeros(nobsrv,nepsvar);

for j=1:nobsrv
    %derivative of the log likelihood
    floglike=@(x)loglike(glm,obsrv(j),x);
    dnum.deps(j,:)=deriv(floglike,epsvar,dstep,'forward2');

    %for k=1:length(epsvar)
    %analytical function for first derivative
    d1func=@(x)d2glmeps(x,obsrv(j),glm,'m');
    dnum.d2eps(j,:)=deriv(d1func,epsvar,dstep,'forward2');
end

%print the largest difference
dmax1=max(max(abs(dfunc.deps-dnum.deps)));
dmax2=max(max(abs(dfunc.d2eps-dnum.d2eps)));
fprintf('Max difference 1st derivative: %2.2g \n',dmax1);
fprintf('Max difference 2nd derivative: %2.2g \n',dmax2);