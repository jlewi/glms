%$Revision$: 06-14-2007
%   modify it to test new version of d2glmeps
%12-23-06
%
%Test d2glmeps to see if extensions to matrix work
%glm.fglmnc=@logisticA;  %normalizing function for the distribution. 
%glm.fglmetamu=@etamulogistic;   %function to convert mean into canonical parameter depends on the distribution

%glm.fglmmu=@logisticcanon;          %link funciton - funciton to generate the mean
%glm.sampdist=@(mu)(binornd(1,mu));     %distribution from which samples are drawn

%specify what glm to use
%glm=GLMModel('binomial','canon');
glm=GLMModel('poisson','canon');
%genearate some random values of eps
nepsvar=2;
nobsrv=10;
%epsvar=10*(rand(1,2)-.5);
epsvar=-10:10;
nepsvar=length(epsvar);
%generate some random observations
obsrv=zeros(nobsrv,1);
obsrv=(0:10:100)';
nobsrv=length(obsrv);

%for n=1:nobsrv
%obsrv(n)=glm.sampdist(glm.fglmmu(epsvar(ceil(rand(1)*nepsvar))));
%end

%**************************************************************************
%Following tests d2glmeps when we when we want to return a vector
%where ithe element are the derivatives evaluated for (glmproj_i,obsrv_i);
%******************************************************
dvec=min(nobsrv,nepsvar);
[allvec.d1,allvec.d2]=d2glmeps(epsvar(1:dvec),obsrv(1:dvec)',glm,'v');
%2. compute the values 1 at a 1 time
indvals.d1=zeros(dvec,1);
indvals.d2=zeros(dvec,1);
for j=1:dvec    
      [indvec.d1(j), indvec.d2(j)]=d2glmeps(epsvar(j),obsrv(j),glm);    
end
%check if they match
if ~isempty(find((indvec.d1-allvec.d1)>10^-8))
    error('d1 dont match when return of d2glmeps is vector. ');
end

if ~isempty(find((indvec.d2-allvec.d2)>10^-8))
     error('d2 dont match when return of d2glmeps is vector. ');
end
%**************************************************************************
%Following tests d2glmeps when we when we want to return a matrix
%where i,jth element are the derivatives evaluated for (glmproj_i,obsrv_j);
%******************************************************
%compute the derivates two ways
%1. compute the values for all values of obsrv all at once
[allvals.d1 allvals.d2]=d2glmeps(epsvar,obsrv,glm)

%2. compute the values 1 at a 1 time
indvals.d1=zeros(nobsrv,nepsvar);
indvals.d2=zeros(nobsrv,nepsvar);
for j=1:nobsrv
    for k=1:nepsvar
      [indvals.d1(j,k), indvals.d2(j,k)]=d2glmeps(epsvar(k),obsrv(j),glm);
    end
end

%check if they match
if ~isempty(find((indvals.d1-allvals.d1)>10^-8))
    error('d1 dont match');
end

if ~isempty(find((indvals.d2-allvals.d2)>10^-8))
    error('d2 dont match');
end