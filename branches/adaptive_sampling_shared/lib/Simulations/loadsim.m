%function [pdata, simparam, mparam, sr]=loadsim('simfile',simfile,'simvar',simvar);
%             simfile - name of file to which simulation was saved using
%             savesim
%             simvar -name of the variable in the file to which the
%                                SimulationBase derived object was saved
%                        - if this is an old version file this i
%             vsave - save variables into workspace using this name
%                     or leave blank and we will just the name of the
%                     simulation minus sim
%%function [pdata, simparam, mparam,sr]=loadsim('simfile',simfile,'varsuffix',suffix,'oldver',1,'method',mname);
%             var suffix - suffix for variable to load
%             oldver - indicates file is old version before we converted to
%                           object oriented model
%            mname - which update method of the old version data to load
% Explanation: l
%    Warning: This file is only used to load old file formats
%       That is file versions < 070726
%       Those files still used separate objects for pdata, simparam,
%       mparam, and sr
%       In new object model (ver>071022) we have a single SimulationBase
%       object for each class.
%
%loads the data from a file where the data was saved using
% the savesim function. This saves the parameters to a variable named
% 'simvar' with fields   param, mparam, sr 
% where simid is the simid field of the SimulationBase object
%
%10-24-2007
%   To make my new sim model work with my old graphing code 
%   I backwards convert the new simulation model
%3-28-2007
%   for backwards compatibility set glm of the model object and the
%   observer to the glm of the simulation object
%
%4-19-207
%   if passin vsave, variables will automatically be created in the calling
%   class
function [pdata, simparam, mparam, sr]=loadsim(varargin)

oldver=0;
vsave=[];
for j=1:2:nargin
    switch varargin{j}
        case {'simfile','fname'}
            req.simfile=1;
            simfile=varargin{j+1};
            if iscell(simfile)
                simfile=simfile{1};
            end
        case 'simvar'
            req.simvar=1;
            simvar=varargin{j+1};
       %*****
       %parameters for loading old verios/pre object orientated files
        case 'varsuffix'
            varsuffix=varargin{j+1};
        case 'oldver'
            oldver=varargin{j+1};
        case 'method'
            mname=varargin{j+1};
        case 'vsave'
            vsave=varargin{j+1};
        otherwise
            error(sprintf('%s unrecognized parameter',varargin{j}));
    end
end

if (nargout==0 & isempty(vsave))
   %set vsave
   vsave=simvar(4:end);
end
if ~exist(simfile,'file')
    error('File does not exist');
end

if (oldver==0)
        %Simulation is in object format but is possible an older version
        %load the new version of the file

        v=load(simfile,simvar);
        pdata=v.(simvar).pdata;
         mparam=v.(simvar).mparam;
        sr=v.(simvar).sr;
        simparam=v.(simvar).simparam;
        
        %handle conversion issues for old object models
        %Handle conversion of object model older than version 070318
        if (getversion(simparam)<0)
            fprintf('Simulation version older than 03/18/07 converting \n');

            %copy the glm from the simulation object to the mparam object
            mparam=convertoldver(mparam,'GLM',simparam.deleted.glm);
            %simparam.observer.
            %set the model observer
            %in older version only the glm could be different so other
            %fields are copied from the mparam object
            mobserver=MParamObj('glm',simparam.observer.deleted.glm,'klength',mparam.klength,'pinit',mparam.pinit,'mmag',mparam.mmag);
            simparam=convertoldver(simparam,'observermodel',mobserver);
        end
else
        fprintf('Loading file in old format \n');
        %2-16-2007
    %load the old version and do any necessary conversions
        %3-28-2007 this conversion is outdated 
        %   this converts it to version 0 of the object model which then
        %   needs to be converted to the new version model  070318
      pname=sprintf('p%s',varsuffix);
      data=load(simfile,pname);
      pdata=data.(pname).(mname);
      
      srname=sprintf('sr%s',varsuffix);
      sr=load(simfile,srname);
      sr=sr.(srname).(mname);
      
       spname=sprintf('simparam%s',varsuffix);
       simparam=load(simfile,spname);
       simparam=simparam.(spname);
       
       mparam=load(simfile,'mparam');
       mparam=mparam.mparam;
       
       %**************************************************
       %object conversion
       %**************************************************
       %1. Convert the glm structure to a glm model
       % Assume its poisson distribution
       %I think if simparam.glm has no fglmmu specified
       %it should be the exponential function because it was from original
       %code when we could only do optimization for exponential link
       simparam.glm=GLMModel('poisson',simparam.glm.fglmmu);
end

%if vsave is not empty assign the variables in the calling workspace
if (length(vsave)>0)
   assignin('caller',sprintf('p%s',vsave),pdata);
   assignin('caller',sprintf('sim%s',vsave),simparam);
   assignin('caller',sprintf('mp%s',vsave),mparam);
   assignin('caller',sprintf('sr%s',vsave),sr);
   
   fprintf('Created variables in caller workspace: \n');
   fprintf('p%s \t sim%s \t mp%s \t sr%s \n',vsave,vsave,vsave,vsave);
end