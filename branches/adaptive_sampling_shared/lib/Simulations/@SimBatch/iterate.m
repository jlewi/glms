%function simobj=iterate(simobj,trial)
%   trial - trial to perform update for
%   einfo - eigendecomposition from the previous trial
%
%Explanation: perform a single iteration
%
%This function should not becalled directly except by the SimulationClass
%and its children. External functions should call update
%
% Return value
%   simobj - updated simulation object
%   einfo  - eigendecomposition of our new posterior
function [simobj,einfo]=iterate(simobj,trial,einfolast)

mobj=getmobj(simobj);
updater=getupdater(simobj);
sobj=getstimchooser(simobj);
post=getpost(simobj,trial);
observer=getobserver(simobj);

%*****************************************************************
%compute the spike history
%******************************************************************
%spike history is just the previous responses
%we take them to be zero if they happened before the start of the
%experiment
nobsrv=trial-1;    %how many observations have we made
bsize=getnstimuli(getstimchooser(simobj));


%****************************************************************
%choose the stimulus
%****************************************************************
%relevant posterior is stored at post(trial) b\c post(1) = prior


[xobj,stiminfo,sobj]=choosestim(sobj,getpost(simobj,trial-1),[],einfolast,mobj,simobj);    

%if choosestim returned a vector create a GLMInputVec object
if ~isa(xobj,'GLMInput')
    %xobj=GLMInputVec('data',xobj);
    error('Simbatch requires GLMINput objects');
end




%sample the model to generate the response
tinfo.twindow=gettresponse(mobj);

%for the batch experiment we need to combine the stimulus with the stimulus
%history
%to do this efficiently we create one long vector. Subvectors of this
%vector then correspond to the stimulus on the different trials.

ktlength=getktlength(mobj);
klength=getklength(mobj);
if (nobsrv>=(getktlength(mobj)-1))
    bstim=[getData(xobj(end:-1:1)) getstim(simobj,trial-1:trial-getklength(mobj)+1)];
else    
    bstim=[getData(xobj(end:-1:1)) getstim(simobj,(trial-1:1)) ones(getklength(mobj),ktlength-nobsrv-1)];
end
%make bstim a vector;
bstim=bstim(:);
for tib=1:bsize
   
%select the stimulus from bstim
stim=bstim((tib-1)*klength+1:(tib-1)*klength+ktlength*klength);
%******************************************************************
%generate the observation
%******************************************************************
sr.y=getData(xobj(tib));
sr.stiminfo=stiminfo;
sr.nspikes=observe(simobj.observer,stim);
simobj=setsr(simobj,trial+tib,sr);

if isinf(sr.nspikes)
    error('infinite # of spikes');
end
if isnan(sr.nspikes)
    error('observation is nan');
end
%************************************************************
%Full stimulus
%**************************************************************
%form the full stimulus for the update by combining the spike history, the stimulus
%and any nonlinear transformations of the stimulus. This is all handled
%by projinp
%depending on the updater we may need to use the observations from all
%trials or just the most recent
%we set obsrv and stim approriately

%doing this check on each iteration might be slow
%might be better to give each method a unique number
%and then just check that element in array to see if its on or off


switch class(updater)
    case {'BatchML'}
        stim=projinp(mobj,stim,getobsrv(simobj,trial));
        obsrv=getobsrv(simobj,[1:trial]);

    case {'Newton1d'}
        stim=projinp(mobj,stim,getobsrv(simobj,trial));
        obsrv=getobsrv(simobj,trial);
    otherwise
        error('SimulationBase:iterate','Code for updater %s doesnt exist',class(simparam.updater));
end



[post,uinfo,einfo]=update(updater,getpost(simobj,trial-1),stim,mobj,obsrv,einfolast);


%compute the entropy

if ~isempty(einfo)
    %2-11-07
    %changed it from getstimlen(mparam)
    %to getstimlen(mparam)+mparam.alength
    %
    %If we optimize over the sphere we don't keep track of the
    %eigendecomposition of the full matrix but in the poolbased
    %case we do. SO in the poolbased setting we could still
    %compute the entropy
    if (simobj.mobj.alength>0)
        warning('cant compute entropy becaue eigendecomposition is not of full covariance matrix');
    else
        post.entropy=1/2*(getparamlen(mobj)*(log2(2*pi*exp(1))))+1/2*sum(log2(geteigd(einfo)));
    end
end

if any(isnan(post.m))
    error('new mean is nan');
end
if any(isnan(post.c))
    error('Covariance has Nan');
end



%save the updated posterior
post.uinfo=uinfo;

simobj=setpost(simobj,trial+tib-1,post);
simobj=setsimobj(simobj,stimchooser);
simobj=setniter(simobj,getniter(simobj)+1);
end