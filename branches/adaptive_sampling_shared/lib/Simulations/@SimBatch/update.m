%function simobj=update(simobj,niter)
%   simobj - SimulationObject
%   niter  - number of simulations to run
%
% Return value:
%   simobj - updated simulation object
%Explanation:
%   This function runs the specified method and on each iteration
%   uses the stimulus likely to maximize the info.
%
%
% Implementation notes:
function [simobj]=update(simobj,ntorun)
printiter=10;   %how often to print out trial info.
if (ntorun<1)
    error('Number of iterations was not set');
end

%make ntorun a multiple of the batch size
bsize=getnstimuli(getstimchooser(simobj));
ntorun=ceil(ntorun/bsize)*bsize;


mobj=getmobj(simobj);
updater=getupdater(simobj);

%********************************************************************
%Initialize/Load simulation
%*****************************************************
%declare the structures to hold the results for this simulation
einfo=[];
post=[];



%information about the continuing simulation
continfo.piter=getniter(simobj); %number of previous iterations
continfo.ntorun=ntorun;            %additional number of iterations to run
continfo.totaliter=continfo.piter+continfo.ntorun;

%initialize the random number generators to the state we left off
%on
rstate=getrstate(simobj);
if (length(rstate)>1)
randn('state',rstate(end).rfinishn);
rand('state',rstate(end).rfinish);
end



%*******************************************************
%stucture to store timing info
timing.update=zeros(1,ntorun);
timing.choosestim=zeros(1,ntorun);
timing.total=zeros(1,ntorun);

%tell simparam to save the current start state of the random number
%generators as the start state.
%this is not backwards compatible
simobj=savestartstate(simobj);

%*************************************************************************
%Loop over the iterations
%*************************************************************************
%loop through enabled methods
fprintf('Begin trials \n');

stimlen=getstimlen(getmobj(simobj));
%initialize einfolast to the eigeninfo of the prior
%we do this because in the rank 1 case we can use the eigenvectors to
%efficiently compute the entropy
%keyboard
%compeig variable determines whether we compute the eigendecomposition or
%not
%if (isa(simparam.updater,'Newton1d'))

if (getcompeig(updater)~=0)
    %get the initial eigen decomposition
    c=getpostc(simobj,continfo.piter+1);
    einfo=EigObj('matrix',c(1:stimlen,1:stimlen));
end


obsrv.twindow=gettresponse(mobj);

%it is absolutely important that we perform all of the iterations
%for a particular method before going onto a different method
%this is is because I make assumptions about what post, prior
%and newpost store in each methods update.
%
%trial - the absolute number of trials
for trial=(continfo.piter+1):bsize:continfo.totaliter

    if (mod(trial,printiter)==0)

        fprintf('%s: iteration %0.2g \n',getsimid(simobj),trial);
    end


    %iterate
    [simobj,einfo]=iterate(simobj,trial,einfo);


    %save memory by not saving covariance matrices which
    %are no longer needed
    %zero out old matrices which are no longer needed
    %we will save every simparam.lowmem iter
    %set it to inf to save only first and last
    if (getlowmem(simobj)~=0)
        if (trial>1)
            if isinf(getlowmem(simobj))
                %zero out the posterior from the previous trial
                simobj=setpostc(simobj,trial,[]);
            else

                d=(trial)/getlowmem(simobj);
                if (d~=floor(d))
                    simobj=setpostc(simobj,trial,[]);
                end
            end
        end
    end
 
end %loop over trials



%tell simparam to save the current start state of the random number
%generators as the end state.
%this is not backwards compatible
simobj=saveendstate(simobj);

