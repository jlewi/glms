%function bssim=runonsim(datafile,statusfile)
%   datafile - the datafile containing the BSBatchMLSim object
%   statsufile - the file to print status messages to
%
%Explanation: This is a static function intended for launching jobs on the
%cluster.
function bssimobj=runonsim(datafile,statusfile)
v=load(getpath(datafile));

bssimobj=v.bssimobj;

%if we ran this simulation before then initialize with theta from the last
%run
if ~isempty(bssimobj.results)
    thetainit=bssimobj.results(end).theta;
else
    thetainit=[];
end
[bssimobj]=findMAP(bssimobj,thetainit,datafile,statusfile);

