%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
%
%
%Revisions:
% 08-13-2008
%    Added conversions for all previous versions.
function obj=loadobj(lobj)
%check if lobj is a structure
    %this indicates the class structure has changed and we need to handle
    %the conversion   
    if isstruct(lobj)
        %create a blank object
        obj=BSSim();
        %save the latest version so we can check to make sure all
        %conversions have been completed.
        latestver=obj.version;
        
        %******************************************************************
        %Sequentially convert one version of the object to the next
        %until we get to the latest version
        %******************************************************************
        if (obj.version < 080604)
            
            error('Need to write a conversion method to handle objects this old.');
        end
        
        if (obj.version < 080715)
           %do conversion 
           %copy fields from old version to new version
           
           %fields which have been deleted
           fskip={'bdata','windexes'};    %fields not to copy
           obj=copyfields(lobj,obj,fskip);
           obj.version=080715;
        end
        
        if (obj.version < 080718)
            %do nothing we just added the extra field
            obj.version=080718;
        end
            
        if (obj.version < 080725)
            %we just added the label field
            %set the label field based on the stimchooser
             %set the value of the label automatically
                       switch class(obj.stimobj)
                           case {'BSOptStim','BSOptStimNoHeur'}
                               obj.label='Info. Max.';
                           case {'BSSeqStim'}
                               obj.label='Sequential';
                           case {'BSUniformStim'}
                               obj.label='Shuffled';
                           case {'BSTanSpaceInfoMax'}
                               obj.label='Info. Max.: Tan Space';
                           case {'BSBatchPoissLB'}
                               obj.label='Info. Max. BatchLB';
                       end
            obj.version=080725;
        end
        
        if (obj.version<080813)
            %bssimobj.runs().starttrial- was not set correctly 
            %so we reset it
            nruns=length(obj.runs);
            if (nruns>0)                
            ntrials=[obj.runs(:).ntrials];
              obj.runs(1).starttrial=1;
              
              cstrials=cumsum(ntrials);
              for r=2:nruns
                  obj.runs(r).starttrial=ntrials(r-1)+1;    
              end
            end
            obj.version=080813;
        end
        
        if (obj.version<080918)
           %field objfunval was added
           %create an array of size niter and set it nan
           obj.objfunval=nan*ones(1,obj.niter);

           obj.version=080918;
        end
        if (obj.version<081103)
            %we added the field tracemsg, so we don't need to do anything
            %tracemsg will be initialized to false by default.
           obj.version=081103; 
        end
        %check if converted all the way to latest version
        if (obj.version <latestver)
            error('Object has not been fully converted to latest version. \n');
        end
        
    else
        obj=lobj;
    end
