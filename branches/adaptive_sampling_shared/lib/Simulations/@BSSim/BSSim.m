%function sim=BSSim('allpost',[],'updater',[],'bdata',[],'mobj',[],'stimchooser',[])
%
%Explanation: Run a simulation on the bird song data.
%
%Revision:
%   10-28-2008 - Add statusfid as a transient property
%   09-18-2008 - Add field objfun. This stores the value
%                of the objective function for the stimulus
%                presented/selected on each trial
%                or nan, if its not set.
%   08-13-2008
%       The field bsimobj.runs().starttrial was not being set correctly
%            The loadobj method recalculates this value as part of the conversion.
%   07-25-2008
%       add the label field.
%   07-18-2008 - add the extra field
%   07-15-2008 -
%       use the new object model
%       make it a child of handle
%
%       get rid of bdata, windexes, paststim. This should all be members of
%       the classes that need them
%   080604- initial version is not derived from simulation base
%
classdef (ConstructOnLoad=true) BSSim  < handle

    %******************************************************************
    %Properties
    %******************************************************************

    properties(SetAccess=private,GetAccess=public)
        %**********************************************************
        %Define Members of object
        %**************************************************************
        % version - version number for the object
        %           store this as yearmonthdate
        %           using 2 digit format. This way version numbers follow numerical
        %           order
        %           -version numbers make it easier to maintain compatibility
        %               if you add and remove fields
        %
        % stimobj  - the BSOptStim object for choosing the stimuli
        %          - we save the stimulus object
        %            because we want to keep track of which stimuli pick
        %           so that we pick the stimuli without replacement
        % allpost  - Gaussian posterior object
        %
        % runs     - A structure array each entry keeps track of information about
        %            a different run
        %           .starttrial - first trial
        %           .ntrials - number trials run
        %           .svnrevision - revision number of the code
        %           .lab1host  - store the hostname of lab1
        %           .jobid
        %declare the structure
        %
        %niter     - number of trials we've run
        %
        %passtim   - 3xniter matrix
        %          - keeps track of the stimulus presented on each trial 
        %extra     - structure of extra data
        %objfunval - store the value of the objective function used to
        %            optimize the stimuli
        version=081103;
        stimobj=[];
        allpost=[];
        mobj=[];
        runs=[];
        niter=0;
        paststim=[];
        objfunval=[];
    end

    
    properties(SetAccess=public,GetAccess=public)
         %savecint  - how often to save the covariance matrix
        %          - inf means only save the first and last covariance
        %          matrix
        %          = 1 - save it on every trial
        %label - string to describe this simulation in plots
        savecint=inf;
        extra=[];
        label='';        
        updater=[];
        
        %whether or not to print out trace messages in iterate
        %intended to help find MPI deadlocks
        tracemsg=false;
    end
    %********************************************************************
    %methods
    %******************************************************************

    properties(SetAccess=private,GetAccess=public,Transient)
        %the file id where we want to print status messages to
        %we make this a member variable so that other objects
        %can print messages to this file.
        %statusfid gets set in iterate
       statusfid=1;
    end
    
    properties(GetAccess=public,Dependent)
       neuron=[]; 
    end
    methods
        function neuron=get.neuron(obj)
           neuron=obj.stimobj.bdata.neuron; 
        end
        function simobj=BSSim(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'allpost','updater','mobj','stimchooser'};
            con(1).cfun=1;

            %**********************************************************
            %Define Members of object
            %**************************************************************
            % version - version number for the object
            %           store this as yearmonthdate
            %           using 2 digit format. This way version numbers follow numerical
            %           order
            %           -version numbers make it easier to maintain compatibility
            %               if you add and remove fields
            %
            % stimobj  - the BSOptStim object for choosing the stimuli
            %          - we save the stimulus object
            %            because we want to keep track of which stimuli pick
            %           so that we pick the stimuli without replacement
            % allpost  - Gaussian posterior object
            %
            % runs     - A structure array each entry keeps track of information about
            %            a different run
            %           .starttrial - first trial
            %           .ntrials - number trials run
            %           .svnrevision - revision number of the code
            %           .lab1host  - store the hostname of lab1
            %           .jobid
            %windexes - which wave files to use as a training set
            %declare the structure
            %
            %paststim   - matrix to keep track of the stimulus presented on
            %each trial
    %paststim= 3x niter matrix    
%           = we use this to keep track of the order the stimuli were
%           chosen in
%       paststim(1,:) - windex of the stimulus
%       paststim(2,:) - the repeat of the wfile
%       paststim(3,:) - the relative trial within the 

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    return
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************

            switch cind
                case 1
                    simobj.allpost=params.allpost;
                    simobj.updater=params.updater;
                    simobj.mobj=params.mobj;
                    simobj.stimobj=params.stimchooser;
                    
                    if ~isfield('params','label')
                       %set the value of the label automatically
                       switch class(simobj.stimobj)
                           case {'BSOptStim','BSOptStimNoHeur'}
                               simobj.label='Info. Max.';
                           case {'BSSeqStim'}
                               simobj.label='Sequential';
                           case {'BSUniformStim'}
                               simobj.label='Shuffled';
                           case {'BSTanSpaceInfoMax'}
                               simobj.label='Info. Max.: Tan Space';
                           case {'BSBatchPoissLB'}
                               simobj.label='Info. Max. BatchLB';
                           case {'BSBatchShuffle'}
                               simobj.label='Batch shuffle';
                           case {'BSBatchPoissLBTan'}
                               simobj.label='Info. Max.: BatchLB, TanSpace';
                           case {'BSBatchPCanonTaylor'}
                               simobj.label='Info. Max.: Batch Taylor';
                           otherwise
                               simobj.label=class(simobj.stimobj);
                       end
                        
                    end
                    if isfield(params,'savecint')
                        simobj.savecint=params.savecint;
                    end
                otherwise
                    error('Constructor not implemented')
            end

        end
    end

end





