%function plottheta(sim,trial,fname)
%
%   fname - optional name of xml file to save data to
%Plot theta on the specified trial
function [fh,oinfo]=plottheta(sim,trial,fname)

if ~exist('fname','var')
    fname=[];
end
oinfo=cell(length(trial),length(sim));
%loob of sims and trials
for simind=1:length(sim)
    for tind=1:length(trial)
    txt=sprintf('%s\nTrial %d\n',sim(simind).label,trial(tind));
    if (trial(tind)<=sim(simind).niter)
        [fh(tind,simind)]=plottheta(sim(simind).mobj,getm(sim(simind).allpost,trial(tind)),txt);
        oinfo{tind,simind}=fh(tind,simind);
    else
        oinfo{tind,simind}=sprintf('Trial %d exceeds number of trials run = %d',trial(tind),sim(simind).niter);
    end
    
    end
end

%save the xml file
if ~isempty(fname)
    onenotetable(oinfo,seqfname(fname));
end
%adjust the labels of the graph
