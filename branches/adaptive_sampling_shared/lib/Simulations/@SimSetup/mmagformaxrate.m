%function mmagformaxrate(glm, thetamag,maxrate,bias,initmag)
%   bias    - value for the bias
%   initmag - initalization point for the search for the magnitude
%
%Explanation: Compute the maximum magnitude such that when the input is
%100 correlated with theta we get the maximum average spike rate
%
function mmag=mmagformaxrate(glm, thetamag,maxrate,bias,initmag)

if ~exist('maxrate','var')
    maxrate=10000;
end

if ~exist('initmag','var')
   initmag=1; 
end
%set mparam.mmag so that when 50% of energy is along the true parameter
%the avg number of spikes is 200


optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag =@(m)(fglmmu(glm,m*thetamag+bias)-maxrate);
[mmag,fmag,exitflag,fsout]=fsolve(fsetmmag,initmag,optim);
mparam.mmag=mmag;

   %check it converged
    switch (exitflag)
        case {-3,0}
            error('fsolve did not converge for value of stimulus magnitude. Exited with message: \n %s \n trying changing the start point',fsout.message);
            fprintf('postnewton1d: Attempting to solve peak by varying the initializataion point \n');
            %turn messages on
            %increase number of iterations
            options=optimset(options,'Display','off');
            options=optimset(options,'MaxFunEvals',10000000);
            options=optimset(options,'MaxIter',10000000);
    end
    