%function allpost=initgausspostfile(allfiles,mobj)
%   allfiles
%       .mfile
%       .cfile
function allpost=initgausspostfile(allfiles,prior)




allpost=GaussPostFile('mfile',allfiles.mfile,'cfile',allfiles.cfile,'m',getm(prior),'c',getc(prior),'id',0);
