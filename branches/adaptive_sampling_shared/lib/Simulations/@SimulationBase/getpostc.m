%function c=getpostc(simobj,trial)
%   simobj - Simulation Object
%   trial    - trials on which to get the covariance matrix
%           trial=0 -->Prior
%
% Explanation: gets the covariance matrice from the posterior for the
% specified trial
function c=getpostc(simobj,trial)
    %warning('12-30-2008 function is obosolete access allpost directly instead.');
  
     c=getc(simobj.allpost,trial);
