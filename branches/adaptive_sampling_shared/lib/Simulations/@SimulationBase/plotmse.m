%function [ffilt]=plotmse(simobj,subsamp)
%   simobj - a simulation object or array of object
%   subsamp - how often to subsample the data before plotting
%
%Explanation makes a plot of the posterior means as an image
%
function [ffilt]=plotmse(simobj,subsamp)

pstyles=PlotStyles();

nplot=1;

if ~exist('subsamp','var')
   subsamp=[]; 
end

if isempty(subsamp)
    subsamp=1;
end

if (simobj(1).mobj.alength>0)
    hasshist=true;
    nplot=nplot+1;
end

if (simobj(1).mobj.hasbias)
   hasbias=true;
   nplot=nplot+1;
end
ffilt=FigObj('width',10,'height',6,'name','MSE of Map','naxes',[1,nplot],'xlabel','trial','ylabel','MSE');

%*********************************************
%plot the estimated filters
pind=zeros(nplot,1);
for dind=1:length(simobj)
    nplot=0;
    %plot the mean of the full posterior

    m=getm(simobj(dind).allpost);
    
    iind=0:subsamp:getniter(simobj(dind));

    %compute the mse
    theta=gettheta(getobserver((simobj(dind))));
    
    mse=m(:,1:subsamp:simobj(dind).niter+1)-theta*ones(1,length(iind));
    
        
    mobj=simobj(dind).mobj;
    %******************************************************************
    %MSE of stim coefficients       
  %*****************************************************************
  nplot=nplot+1;  
  msestim=mse(mobj.indstim(1):mobj.indstim(2),:);
    msestim=sum(msestim.^2,1);
    msestim=msestim.^.5;
    
    
    setfocus(ffilt.a(1,nplot));
    
    hp=plot(iind,msestim);
    pind(nplot)=pind(nplot)+1;
    pstyle=plotstyle(pstyles,pind(nplot));    
    addplot(ffilt.a(1,nplot),'hp',hp,'pstyle',pstyle);
    
    %***************************************************
    %mse of shist
    %*********************************************************
    if (hasshist) 
    
    nplot=nplot+1;  
    mseshist=mse(mobj.indshist(1):mobj.indshist(2),:);
    mseshist=sum(mseshist.^2,1);
    mseshist=mseshist.^.5;
    
        setfocus(ffilt.a(1,nplot));
    hp=plot(iind,mseshist);
    
    pind(nplot)=pind(nplot)+1;
    pstyle=plotstyle(pstyles,pind(nplot));    

    addplot(ffilt.a(1,nplot),'hp',hp,'pstyle',pstyle);
    end
    %***************************************************
    %mse of bias
    %*********************************************************
if (hasbias)
    nplot=nplot+1;  
    msebias=mse(mobj.indbias,:);
    msebias=sum(msebias.^2,1);
    msebias=msebias.^.5;
    
        setfocus(ffilt.a(1,nplot));
    hp=plot(iind,msebias);
    
    pind(nplot)=pind(nplot)+1;
    pstyle=plotstyle(pstyles,pind(nplot));    

    addplot(ffilt.a(1,nplot),'hp',hp,'pstyle',pstyle,'lbl',getlabel(simobj(dind)));
end
  
    %if its a tanspace object get the mse of the map in the tangent space
%     if isa(post,'PostTanSpace')
%         tanspace=gettanspace(post(1));
%         m=zeros(getdimtheta(tanspace),getniter(simobj(dind))+1);
%         
%         for ind=0:getniter(simobj(dind))
%              tanspace=gettanspace(post(ind+1));
%             m(:,ind+1)=gettheta(tanspace);
%         end
%         mse=m-theta*ones(1,size(m,2));
%         mse=sum(mse.^2,1);
%         mse=mse.^.5;
%         
%          
%     hp=plot(iind,mse);
%     pind=pind+1;
%     pstyle=plotstyle(pind);
%     
%     ffilt.a=addplot(ffilt.a,'hp',hp,'pstyle',pstyle,'lbl',['Tanspace:' getlabel(simobj(dind))]);
%     
%     end
end

if (max(iind)>1000)
   for aind=1:length(ffilt.a)
      set(ffilt.a(1,aind).ha,'xscale','log'); 
   end
end
nplot=1;
title(ffilt.a(1,nplot),'MSE Stim Coeff');

if (hasshist)
nplot=nplot+1;
title(ffilt.a(1,nplot),'MSE Shist Coeff');
    
end

if (hasbias)
nplot=nplot+1;
title(ffilt.a(1,nplot),'MSE bias');
end
lblgraph(ffilt);