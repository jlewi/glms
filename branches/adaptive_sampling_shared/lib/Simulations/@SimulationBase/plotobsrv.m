%function [fh,odata]=plotobsrv(simobj)
%   simobj - simulation object or array of simulations
%
%Explanation- 
%   fh - figure to object
%   odata - cell array - can be saved as onenotetable
function [fh,odata]=plotobsrv(simobj)


fh=FigObj('name','Observations','width',5,'height',6,'xlabel','trial','ylabel','observation');


hold on;
for j=1:length(simobj)
  
    obsrv=getobsrv(simobj(j).obsrv);
    
   hp=plot(1:getniter(simobj(j)),obsrv,'o','MarkerFaceColor','b','MarkerSize',4);
   lbl=getlabel(simobj(j));
  
  
   fh.a=addplot(fh.a,'hp',hp,'lbl',lbl);
   if ~isempty(max(obsrv))
       set(fh.a.ha,'ylim',[0 max(obsrv)+10]);
   else
         set(fh.a.ha,'ylim',[0 1]); 
   end
end

fh=lblgraph(fh);
odata={fh,''};
