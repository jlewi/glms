%function sobj=get(sim)
%   sim - SimulationBase object
%
%Return value:
%   stimchooser stored in this simulation base object 
function mobj=getmobj(sim)
    mobj=sim.mobj;