%function sr=sr(obj)
%	 obj=SimulationBase object
% 
%Return value: 
%	 sr=obj.sr 
%
function sr=getsr(obj,trial)
error('Function is abolsution 1-20-2009 because we write inputs and observations to a file.');

    if ~exist('trial','var')
       trial=1:getniter(obj); 
    end
    if (trial<1)
        sr=[];
    else
	 sr=obj.sr(trial);
    end
