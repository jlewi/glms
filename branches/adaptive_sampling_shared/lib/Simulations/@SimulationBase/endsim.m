%function endsim(simobj,datafile)
%
%
%Explanation: function to handle processing at end of simulation
%
function endsim(simobj,datafile)
%tell simparam to save the current start state of the random number
%generators as the end state.
%this is not backwards compatible
simobj=saveendstate(simobj);


%all nodes save a copy of the file
%only lab1  transfers its file back to the client in the taskfinish
%function
%save this object to the datfaile


if ~isempty(datafile)
    %try saving it one lab at a time
    try


        if isa(datafile,'FilePath')
            save(getpath(datafile),'simobj','-append');
        else
            save(datafile,'simobj','-append');
        end
        fprintf(simobj.statusfid,'Done saving the file \n');
    catch e

        fprintf(simobj.statusfid,'Error saving the file. Message was:\n %s \n', e.message);
        fprintf(simobj.statusfid,'Try saving the data without the append option \n');



        try
            if isa(datafile,'FilePath')
                save(getpath(datafile),'simobj');
            else
                save(datafile,'simobj');
            end
            fprintf(simobj.statusfid,'saved without append option \n');
        catch e
            fprintf(simobj.statusfid,'Error saving to %s file. Message was:\n %s \n', datafile, e.message);
        end

    end



end

if (simobj.statusfid>1)
    fclose(simobj.statusfid);
    %set simobj.statusfid back to 1
    simobj.statusfid=1;
end
