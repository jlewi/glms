%funciton [sobj=saveendstate(sobj)
%   
% Explanation: Save the current state of the random number generators as
% the end state of the current batch of trials. This allows the random
% number generators to be reset to the same state.
function [sobj]=saveendstate(sobj)
    %we save the stae in the last element of the sobj.rstate() structure
    %array
    %the last element should be the element of the last batch
    %the values should have be = to NaN to indicate they have not been set
    %yet. We check this to make sure we aren't overwriting anything
    if (isnan(sobj.rstate(end).rfinishn) &&  isnan(sobj.rstate(end).rfinish))
    sobj.rstate(end).rfinishn=randn('state');
    sobj.rstate(end).rfinish=rand('state');
    else
        error('saveendstate and savestart state our out of sync. State information is not accurate');
    end