%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
%
%  3-28-2007
%       save fields which have been deleted in the .deleted field
function obj=loadobj(lobj)

cname='SimulationBase';

%indicates whether we have already performed a copy
%copying the fields of lobj to obj when lobj is a structure
%because we only want to do the copying once
cstruct=false;

%get the latest version
%save the latest version so we can check to make sure all
%conversions have been completed.
eval(sprintf('newobj=%s();',cname));

latestver=newobj.version.(cname);

if isstruct(lobj)
    obj=newobj;
    if isfield(lobj,'version')
        obj.version.(cname)=lobj.version.(cname);
    else
       obj.version.(cname)=lobj.simver; 
    end
else
    %we don't need to create a new object
    obj=lobj;
end



%******************************************************************
%Sequentially convert one version of the object to the next
%until we get to the latest version
%
%Warning: If lobj is a structure then we haven't copied the data from lobj
% to obj yet.
%******************************************************************
version1=090108;
if (obj.version.(cname) < version1)
    %do conversion
    %copy fields from old version to new version
    %if lobj is a structure
    if (isstruct(lobj) && ~cstruct)
        fskip={'simver','lowmem','simcont','post','svnrevision'};    %fields not to copy
        obj=copyfields(lobj,obj,fskip);
        cstruct=true;
    end
    %fields which were renamed
    obj.savecint=lobj.lowmem;
    obj.version.(cname)=version1;
    %we set allpost to be the array of GaussPost objects
    %the function load old sim converts this array into a GaussPostFile
    %object.
    obj.allpost=lobj.post;
    
    %copy svnrevision
    for ind=1:length(lobj.svnrevision)
       obj.runs(ind).revision=lobj.svnrevision(ind).revision;
       obj.runs(ind).starttrial=lobj.svnrevision(ind).starttrial;
       obj.runs(ind).ntrials=lobj.svnrevision(ind).endtrial-lobj.svnrevision(ind).starttrial+1;
       
       
    end
end

version2=090110;
if (obj.version.(cname)<version2)
    %this version just added fields entropy and compentropy
    %added field idnum
    obj.idnum= str2double([datestr(now,'yymmdd') datestr(now,'HHMMSS')]);
    %so we don't need to do anything
  obj.version.(cname)=version2;
end

version3=090120;
if (obj.version.(cname)<version3)
    %need to convert .sr to InputsFile object and obsrfile.
    error('need to convert .sr to InputsFile object and obsrfile.');
    
    %so we don't need to do anything
  obj.version.(cname)=version3;
end

version4=090123;
if (obj.version.(cname)<version4)
    %deleted field simid and added field about
    %set about to simid
    obj.about=lobj.simid;
    %so we don't need to do anything
     obj.version.(cname)=version4;
end

%check if converted all the way to latest version
if (obj.version.(cname) <latestver)
    error('Object has not been fully converted to latest version. \n');
end

%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
%
%Copy fields must be declared within loadobj because otherwise it won't
%have permssion to set the private fields of the object.
function dest=copyfields(source,dest,skip)
%we just need to copy the fields
%and handle any special cases if required
fnames=fieldnames(source);

%struct for object
sobj=struct(dest);
for j=1:length(fnames)
    switch fnames{j}
        case skip
            %do nothing we skip this field
        otherwise
            %set the new field this will cause an error
            %if the field isn't a member of the new object
            dest.(fnames{j})=source.(fnames{j});
    end
end


   