%function getver=ver(obj)
%	 obj=SimulationBase object
% 
%Return value: 
%	 ver=obj.ver 
%
function ver=getversion(obj)
	 ver=obj.simver;
