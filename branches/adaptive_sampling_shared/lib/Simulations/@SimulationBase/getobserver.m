%function observer=getobserver(obj)
%	 obj=SimulationBase object
% 
%Return value: 
%	 observer=obj.observer 
%
function observer=getobserver(obj)
	 observer=obj.observer;
