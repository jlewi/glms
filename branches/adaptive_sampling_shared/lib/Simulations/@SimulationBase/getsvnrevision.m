%function getsvnrevision=svnrevision(obj)
%	 obj=SimulationBase object
% 
%Return value: 
%	 svnrevision=obj.svnrevision 
%
function svnrevision=getsvnrevision(obj)
	 svnrevision=obj.svnrevision;
