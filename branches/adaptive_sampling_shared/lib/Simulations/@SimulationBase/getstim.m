%function stim=getgetstim(obj,trial)
%	 obj=SimulationBase object
%    trial = which trials on which to get the stimuli
%Return value: 
%	 stim=obj.sr(trial).y
%
function stim=getstim(obj,trial)
if isa(obj.sr,'SRObj')    
	 stim=[getinput(obj.sr(trial))];
else
	 stim=[obj.sr(trial).y];
end
