%function [ffilt]=plotm(simobj,trial)
%   simobj - a simulation object or array of object
%   trial  - the trial on which to plot the map. 
%
%Explanation makes a plot of the posterior means:
%   1. if ktlength=1 we plot an image where each column is a map on a
%   different trial
%   2. if ktlength>1 we plot a series of images
%
%   
function [ffilt,oinfo]=plotm(simobj,trial)

%call the plot function of the model
[ffilt,oinfo]=plottheta(simobj.mobj,getm(simobj.allpost,trial),[],simobj);

% if (simobj(1).mobj.ktlength==1)
%     ffilt=plotnotemp(simobj,trial);
% 
% else
%     ffilt=plotspacetime(simobj,trial);
% end

%this function needs to be updated to make a plot of the map on several
%trials when the STRF is a spatio-temporal receptive field.
% function ffilt=plotspacetime(simobj,trial)
% ffilt=plotspacetime(simobj,trial)
% %make a plot when there's no temporal component
% nrows=length(dsets);
% 
% ncols=length(trials)+1;
% fh=FigObj('name','STRFs','width',width,'height',height,'naxes',[nrows ncols],'fontsize',fsize);
% 
% %**************************************************************************
% %loop over the datesets and plot the strfs
% %*********************************************************
% for sind=1:length(simobj)
%    for tind=1:length(trials)
%       trial= trials(tind);
%       
%       
%        t=simobj(sind).extra.t;
%        f=simobj(sind).extra.f;
%         %multiply t by 1000 so its in ms
%          t=t*1000;
%          
%          %divide freqs by 1000 so its in khz
%          f=f/1000;
%          
%       if (trial<=simobj(sind).niter)
%          setfocus(fh.a,sind,tind);
%          strf=getm(simobj(sind).allpost,trial);
%          strf=reshape(strf,[simobj(sind).mobj.klength simobj(sind).mobj.ktlength]);
%          
%           
%         
%          imagesc(t,f,strf);
%       end
%       
%    
%     
%    end
% 
%     setfocus(fh.a,sind,ncols);
%    %plot the true strf
%    theta=simobj(sind).observer.theta;
%    theta=reshape(theta,[simobj(sind).mobj.klength simobj(sind).mobj.ktlength]);
%    
%    imagesc(t,f,theta);
% end
% 
% 
% 
% %add a colorbar to the final image in the first row
% dind=1;
% tind=ncols;
% 
%          setfocus(fh.a,dind,tind);
%           %add a colorbar
%          fh.a(dind,tind).hc=colorbar;
% 
%       
% 
% %%
% %*****************************************************************
% %adjust the labels
% %************************************************************
% %turn off all tickmars
% set(fh.a,'xtick',[]);
% set(fh.a,'ytick',[]);
% 
% %set x and y limits
% set(fh.a,'xlim',[t(1) t(end)]);
% set(fh.a,'ylim',[f(1) f(end)]);
% 
% %turn on xtick,ytick for appropriate graphs
% set([fh.a(nrows,1)],'ytickmode','auto');
% set(fh.a(nrows,1),'xtickmode','auto');
% 
% %********************************************************
% %clim=[-2*10^-3 2*10^-3];
% %set(fh.a,'clim',clim);
% 
% 
% %add ylabels
% for dind=1:length(dsets)
%     ylbl='';
%     lbl=simobj(dind).label;
%     %split the lbl based on :
%     sind=strfind(lbl,':');
%     if isempty(sind)
%         ylbl=lbl;
%     else
%         while ~isempty(sind)
%            ylbl=sprintf('%s\n%s',ylbl,lbl(1:sind(1)-1));
%            lbl=lbl(sind(1)+1:end);
%            sind=sind(2:end);
%         end
%         ylbl=sprintf('%s\n%s',ylbl,lbl);
%     end
% 
%     if (dind==length(dsets))
%        ylbl=sprintf('%s\nFrequency (KHz)',ylbl);
%     else
%         %don't add ticklables
%         set(fh.a(dind,1),'YTickLabel',[]);
%     end
%     
%        ylabel(fh.a(dind,1),ylbl);
%     
% end
% 
% 
% 
% %add titles 
% for tind=1:length(trials)
%     if (trials(tind)>=10000)
%         tl=sprintf('Trial %02gk',trials(tind)/1000);
%     else
%        tl=sprintf('Trial %03g',trials(tind));
%     end
%     title(fh.a(1,tind),tl);
% end
% 
% %add xlabels
% for tind=1:1
%    xlabel(fh.a(nrows,tind),sprintf('Time(ms)'));
% end
% 
% 
% lblgraph(fh);
% space.cbwidth=.15;
% sizesubplots(fh,space);
% 
% function ffilt=plotnotemp(simobj,trial)
%     error('01-25-2009 this part of the code needs to be updated');
%     ffilt=FigObj('width',6.3,'height',4,'name','Estimated Parameters',);
% ffilt.a=AxesObj('nrows',2,'ncols',length(simobj));
% 
% %*********************************************
% %plot the estimated filters
% for dind=1:length(simobj)
% 
%     %plot the mean of the full posterior
%     setfocus(ffilt.a,1,dind);
%     post=getpost(simobj(dind));
%     m=getm(post);
%     
%     iind=0:getniter(simobj(dind));
%     imagesc(m');
% 
% 
%     ffilt.a(1,dind)=title(ffilt.a(1,dind),sprintf('MAP: simid=%s',getsimid(simobj(dind))));
%     %turn off xtick and ytick
%     set(gca,'xtick',[]);
%     %set(gca,'ytick',[]);
%     xlim([1 size(m,1)]);
%     
%     set(gca,'ydir','reverse');
%     ylim([1 getniter(simobj(dind))]);
%     %plot the truth
%     setfocus(ffilt.a,2,dind);
%     ktrue=gettheta(getobserver(simobj(dind)));
%     imagesc(ktrue');
%     set(gca,'ytick',[]);
% 
%     ffilt.a(2,dind)=xlabel(ffilt.a(2,dind),'i');
%     xlim([1 size(m,1)]);
% end
% 
% %make the color limits the same
% cl=get(ffilt.a,'clim');
% cl=cell2mat(cl);
% cl=[min(cl(:,1)) max(cl(:,2))];
% set(ffilt.a,'clim',cl);
% 
% %****************************************************************
% %label graphs
% %*******************************************************************
% %add a colorbar
% setfocus(ffilt.a,1,length(simobj));
% hc=colorbar;
% ffilt.a(1,length(simobj))=sethc(ffilt.a(1,length(simobj)),hc);
% 
% %left column
% ffilt.a(1,1)=ylabel(ffilt.a(1,1),'Trial');
% setfocus(ffilt.a,1,1);
% %set(gca,'ytickmode','auto');
% 
% sizesubplots(ffilt,[],ones(1,length(simobj)),[.8 .2]);
% 
% lblgraph(ffilt);