%function sim=setniter(sim,niter)
%   niter - value to set niter to
%
%Return value: 
%   sim - modified Simulation object.
function sim=setniter(sim,niter)

error('12-30-2008 this function is oboslete.');
    sim.niter=niter;
    
    %if we set the number of iteratios
    %make sure it matches length of the posterior
    if (niter>(length(sim.post)+1))
        warning('Value of SimulationBase.niter exceeds length of SimulationBase.post');
    end