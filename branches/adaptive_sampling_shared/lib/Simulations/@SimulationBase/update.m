%function simobj=update(simobj,niter)
%   simobj - SimulationObject
%   niter  - number of simulations to run
%   datafile - (optional) file where this object should be saved at the end of the run
%            - This also allows us to save the object at various intervals
%
%  statusfile - (optional) print status information to a
%              file
%
% Return value:
%   simobj - updated simulation object
%Explanation:
%   This function runs the specified method and on each iteration
%   uses the stimulus likely to maximize the info.
%
%
% Implementation notes:
%
%Revision History:
%   091407 - started modifying it to use accessor get/set methods
%            for all objects instead of using .notation to call functions.
%            using getset methods is faster and works better with
%            inheritance.
function [simobj]=update(simobj,ntorun,datafile,statusfile)

error('12-30-2008. New object just call iterate.');


  



