%function getextra=extra(obj)
%	 obj=SimulationBase object
% 
%Return value: 
%	 extra=obj.extra 
%
function extra=getextra(obj)
	 extra=obj.extra;
