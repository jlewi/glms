%function onenotereport(simobj,fname)
%   simobj - objects or array of objects
%   fname  - filename to save report to
%
%Explanation: generates a series of plots summarizing the results of a
%simulation and saves them to an xml file ready to be exported to onenote
%
function [od]=onenotereport(simobj,fname)

fm=plotm(simobj);
fobsrv=plotobsrv(simobj);
finputs=plotinputs(simobj);
od={'Label',getlabel(simobj); fm,'Map';fobsrv,'Observations';finputs,'inputs'};
onenotetable(od,fname);


