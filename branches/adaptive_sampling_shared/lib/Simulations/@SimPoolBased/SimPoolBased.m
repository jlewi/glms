%function sim=SimPoolBased(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Template for the constructor of the simulation class for the
% case when we do pool based sampling. The difference is that since its
% pool based learning, the observer and the stim chooser are the same
% object. Therefore we overload the get method for the observer to return
function obj=SimPoolBased(varargin)

%**************************************************************
%Required parameters
%*****************************************************************
%if fielname is required then set a field of req.fieldname=0
%when we read in that parameter we set req.fieldname=1
req=[];
%**********************************************************
%Define Members of object
%**************************************************************
% var1          - description
% var2          -description

%declare the structure
%no additional fields over base class
obj=struct([]);


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    pbase=SimulationBase();
    %zero out the observer since the purpose of this class is to always return
    %the stimchooser object when the observer is accessed
%    pbase.observer=[];
    obj=class(obj,'SimPoolBased',pbase);
    return;
end

%**************************************************************************
%Parse the input arguments
%create a new simulation
for j=1:2:nargin
    switch varargin{j}
        %make sure the stimchooser is of type NewsGroup20
        case 'stimchooser'
            schooser=varargin{j+1};
            if ~isa(varargin{j+1},'News20Stim')
                error('The stimchooser object must be of type News20Stim for class SimPoolBased');
            end
        case 'observer'
            error('An observer should not be specified with SimPoolBased. stimchooser should serve as the observer');
        otherwise
            %*********************************
            %do nothing
            %arguments are probably required by the constructor
            %for the base class
            %*****************************************
    end
end

%************************************************************
%Check if all required parameters were supplied
if ~isempty(req)
freq=fieldnames(req);
for k=1:length(freq)
    if (req.(freq{k})==0)
        error('Constructor:missing_arg','Required parameter %s not specified',freq{k});
    end
end
end
%**********************************************************************
%add the observer field
%becasue it is required by SimulationBase, we zero it out afterward
varargin{end+1}='observer';
varargin{end+1}=schooser;
%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=SimulationBase(varargin{:});
%zero out the observer since the purpose of this class is to always return
%the stimchooser object when the observer is accessed
%pbase.observer=[];
obj=class(obj,'SimPoolBased',pbase);



