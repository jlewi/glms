%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
function obj=loadobj(lobj)

    %check if lobj is a structure
    %this indicates the class structure has changed and we need to handle
    %the conversion   
    if isstruct(lobj)
        warning('Saved object was an older version. Converting to newer version');
        %create a blank object
        obj=SimPoolBased();
        
        %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(lobj);
        for j=1:length(fnames)
                   obj.(fnames{j})=lobj.(fnames{j});
        end
    else
        obj=lobj;
    end
    
   