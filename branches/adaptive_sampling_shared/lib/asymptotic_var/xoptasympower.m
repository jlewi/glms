%function xopt=xoptasympower(n,theta1,magcon)
%   n - dimensionality
%   theta1 - magnitude of theta
%   magcon - magnitude of stimulus constraint
%
%
%Explanation: This function computes the point on which 
%   The asymptotic distribution has support assuming p(x1) only has support
%   on a single point
%   This is only for the exponential poisson model.
function xopt=xoptasympower(n,theta1,magcon)
%dimensionality
%n=10000;
%theta1=1;

%terms of our 3rd degree polynomial
a=-n*theta1;
b=-2*n;
c=n*magcon^2*theta1;
d=2*magcon^2;

alpha=-b^3/(27*a^3)+b*c/(6*a^2)-d/(2*a);
beta=c/(3*a)-b^2/(9*a^2);


%xopt
xopt=(alpha+(alpha^2+beta^3)^.5)^(1/3);
xopt=xopt+(alpha-(alpha^2+beta^3)^.5)^(1/3);
xopt=xopt-b/(3*a);