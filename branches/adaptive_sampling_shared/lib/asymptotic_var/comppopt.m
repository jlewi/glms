%function comppopt(glm,dims,thetamag,magcon)
%   glm - GLM to compute popt for
%   dims - dimensionality of the model
%   thetamag - magnitude of theta
%   magcon - magnitude constraint
%
%Return value:
%   popt - optimal distribution
%       .x - support points
%       .px - probability of each support point
%Explanation: This function computes the optimal asymptotic distribution
%   for arbitrary GLMs. This function also does not assume optimal
%   distribution is supported on a single point. Does use fact that optimal
%   distribution is supported on two points
function popt=comppopt(glm,dims,thetamag,magcon)


%to compute the optimal distribution we vary the two support points
%and for each pair we solve for the optimal weight
fobj=@(y)(-1*logdetjexp(y(1),y(2),glm,dims,thetamag,magcon));
A=[];
b=[];
Aeq=[];
beq=[];
lb=[-magcon;-magcon];
ub=[magcon;magcon];
yinit=[.25;.75];
[yopt,fval,exitflag,output]=fmincon(fobj,yinit,A,b,Aeq,beq,lb,ub);

switch (exitflag)
    case {-3,0}
        error('fmicon did not converge for optimal distribution: n %s ',fsout.message);
end

popt.x=yopt';
%we need to recompute the weight;
[wopt]=compwopt(yopt(1),yopt(2),glm,dims,thetamag,magcon);

popt.px=[wopt 1-wopt];

%funciton linfo=logdetjexp(y1,y2,glm,dims,thetamag,magcon)
%
%Explanation: This function computes
% log Ex Er|xJexp(r,x)xx'| which is what we want to maximize
function linfo=logdetjexp(y1,y2,glm,dims,thetamag,magcon)
%compute the optimal weight
[wopt,finfoy1,finfoy2]=compwopt(y1,y2,glm,dims,thetamag,magcon);

linfo=complinfo(wopt,y1,y2,finfoy1,finfoy2,dims,magcon);

function linfo=complinfo(wopt,y1,y2,finfoy1,finfoy2,dims,magcon)
linfo=log(wopt*finfoy1*y1^2+(1-wopt)*finfoy2*y2^2);
linfo=linfo+(dims-1)*log(wopt*finfoy1*(magcon^2-y1^2)+(1-wopt)*finfoy2*(magcon^2-y2^2));

%function compwopt(y1,y2,glm,dims,thetamag,magcon)
%   y1,y2 - the two support points for the optimal marginal distribution
%         p(\stim^T\param)
%
% Explanation: Computes the optimal weight to place on y1 to maximize the expected Fishe
%   information.
%
function [wopt,finfoy1,finfoy2]=compwopt(y1,y2,glm,dims,thetamag,magcon)
%we need to compute the expected fisher information as a function
%of y1 and y2
finfoy1=jexp(glm, y1*thetamag);
finfoy2=jexp(glm, y2*thetamag);



%now to find wopt we set the derivative to zero
%which gives us an equation which is linear in w.
%i.e we need to find the root of 
%w*slope+yint=0;
%w=-yint/slope
m=magcon;
%yint=finfoy2*(m^2-y2^2)(finfoy1*y1^2-finfoy2*y2^2);
%yint=yint+finfoy2*(dims-1)*y2^2(finfoy1*(m^2-y1^2)-finfoy2*(m^2-
a=finfoy1;
b=finfoy2;
d=dims;
yint=b*(m^2 - y2^2)*(a *y1^2 - b *y2^2);
yint=yint+ b *(-1 + d)*y2^2*(a *(m^2 - y1^2) - b *(m^2 - y2^2));

slope=a* (m^2 - y1^2) *(a *y1^2 - b *y2^2);
slope=slope- b *(m^2 - y2^2)*(a* y1^2 - b *y2^2);
slope=slope+ a *(-1 + d) *y1^2* (a *(m^2 - y1^2) - b*(m^2 - y2^2));
slope=slope- b *(-1 + d)* y2^2 *(a *(m^2 - y1^2) - b* (m^2 - y2^2));

% fobj=@(w)(dlinfow(w,y1,y2,finfoy1,finfoy2,dims,magcon));
% [wopt,fval,exitflag] = fsolve(fobj,.5);
% %check it converged
% switch (exitflag)
%     case {-3,0}
%         error('fsolve did not converge for wopt Exited with message: n %s ',fsout.message);
% end

wopt=-yint/slope;
%if wopt<0 or wopt>1 then wopt=0 or wopt=1
%we compute the information for each to determine which
if (wopt<0 || wopt>1)
    linfow0=complinfo(0,y1,y2,finfoy1,finfoy2,dims,magcon);
    linfow1=complinfo(1,y1,y2,finfoy1,finfoy2,dims,magcon);
  if (linfow0>linfow1)
      wopt=0;
  else
      wopt=1;
  end
end

%function dlinfow(w,y1,y2,finfoy1,finfoy2,dims)
%
%Explanation: To compute the optimal weight we set the derivative of
%log Ex Er|xJexp(r,x)xx'| to zero.
%This function evaluates the derivative with respect to the weight
function dw=dlinfow(w,y1,y2,finfoy1,finfoy2,dims,magcon)
dw=(finfoy1*y1^2-finfoy2*y2^2)/(w*finfoy1*y1^2+(1-w)*finfoy2*y2^2);
dw=dw+(dims-1)*(finfoy1*(magcon^2-y1^2)-finfoy2*(magcon^2-y2^2))/(w*finfoy1*(magcon^2-y1^2)+(1-w)*finfoy2*(magcon^2-y2^2));