%function [imax]=compimaxasymvar(n,theta, magcon)
%   n - the length of the stimulus
%   theta  - the parameter vector
%   magcon - magnitude constraint on the stimulus
%
%Return Value:
%   imax   - structure containing predicted asymptotic values for info.max
%           .covarexp - the asymptotic covariance matrix
%                      inverse of the expected fisher information over the
%                      optimal sampling distribution
%           .ljexp   - the entropy of covarexp           
%           .xopt    - optimal point of support in direction of theta
%           .asymevals - asymptotic eigenvalues of the covariance matrix
%                      - sorted in descending order
%           .asymevec - asymptotic eigenvectors of the covariance matrix
%
%Explanation: This function computes the asymptotic covariance matrix for
%the infomax design under a powerconstraint with EXPONENTIAL NONLINEARITY. The covariance matrix is
%computed in a coordinate system in which all components of theta except th
%e first one are zeros
%
%   Function assumes asymptotic distribution is supported on a single
%   point.
%
%Revision History
%   3-17-2008
%       Bug Fix: In order to compute the asymptotic eigenvectors correctly
%       we need to know theta not just its magnitude because the first
%       eigenvector is parallel to theta
%   1-08-07 - This function is created from code in compasymvar.m
function imax=compimaxasymvar(n,theta, magcon)
%**************************************************************************
%Info max asymptotic variance
%**************************************************************************
%find the optimal support point for p
%assume its supporte don a single point
%warning('need to uncomment following lines to find xopt');
% dhdx=@(x)(n*theta1+2/x+(n-1)*(-2*x)/(magcon^2-x^2));
% xopt=fsolve(dhdx,magcon-.001);

theta1=(theta'*theta)^.5;
imax.xopt=xoptasympower(n,theta1,magcon);
xopt=imax.xopt;

%compute the asymptotic eigenvalues
%of the covariance matrix
%information matrix for the optimal support distribuiton
imax.asymevals=zeros(n,1);
imax.asymevals(1)=(exp(xopt*theta1)*xopt^2);
imax.asymevals(2:n)=(magcon^2-xopt^2)/(n-1)*exp(xopt*theta1);
%values are currently the fisher information so we take reciprocal to get
%variance
imax.asymevals=imax.asymevals.^-1;
%we need to compute the eigen vectors for the asymptotic covariance matrix
%the first eigenvector is theta. The other eigenvectors are any set of
%othogonal vectors
imax.asymevecs=zeros(n,n);
imax.asymevecs(:,1)=normmag(theta);


%********************************************
%vectors orthonormal to theta
%we need a set of 
imax.asymevecs(:,2:end)=null(imax.asymevecs(:,1)');

%asymptotic covariance matrix
imax.covarexp=imax.asymevecs*diag(imax.asymevals)*imax.asymevecs;