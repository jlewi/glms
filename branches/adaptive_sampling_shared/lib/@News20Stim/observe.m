%function r=observe(news,stim)
%   glmsim - glm simulator
%   stim - input
%
%Return value:
%   r- the response\label for this stim
%
%Explanation: Simulates the active learner by obtaining a label/response
%for the specified input.
function r=observe(news,stim)

%get the index of the stimulus
%this should have been saved from the last call to choosestim
%we verify its the correct stimulus and if not we do a search for it
stimind=news.laststimind;

if (stimind>0)
    %check they match;
    if ~(news.docs(stimind).wcounts==stim)
        stimind=-1;
    end
else
    stimind=-1;
end

%if stimind=-1 we need to do a search
if (stimind==-1)
    fprintf('The index of the stimulus is unknown. Searching for the stimulus.\n');
    %search the trianning set first
    for j=1:length(news.trainset)
        ind=testset(j);
        if (stim==news.docs(ind).wcounts)
            stimind=ind;
            break;
        end
    end
end

if (stimind==-1)
    fprintf('Stimulus not in training set. Searching test set. \n');
    for j=1:length(news.testset)
        ind=news.testset(j);
        if (stim==news.docs(ind).wcounts)
            stimind=ind;
            break;
        end
    end
end

if (stimind==-1)
    error('stimulus couldnt be found in training set or testing set');
else
    r=news.docs(stimind).classid;
end