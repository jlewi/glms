%function acc=testmodel(nobj,m,data)
%   nobj - the news20stim object
%   m - jxk the filters to use
%           each column is a different filter
%   data - 'test', 'train'
%
%
% Return value
%   acc - the fraction of the test set for which the predictions are
%   accurate
% Explanation: test the model on the training set
function acc=testmodel(nobj,m,data)

acc=zeros(1,size(m,2));

if ~exist('data','var')
    data='test';
end
%get the testset stim
switch data
    case 'test'
        stim=[nobj.docs(nobj.testset).wcounts];
        classid=[nobj.docs(nobj.testset).classid];
    case 'train'
        stim=[nobj.docs(nobj.trainset).wcounts];
        classid=[nobj.docs(nobj.trainset).classid];        
end
for j=1:size(m,2)
%compute the accuracy
    pred=m(:,j)'*stim;
    pred=pred>0;
    
    dpred=abs(pred-classid);
    %number of correct answers
    ncorrect=length(find(dpred==0));
    acc(j)=ncorrect/length(nobj.testset)*100;
end