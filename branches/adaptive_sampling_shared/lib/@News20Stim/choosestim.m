%function [stim,optreturn,pobj]=choosestim(finf,post,shist,einfo,mparam,glm)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%       glm   - structure specifying the glm
%
% Return value
%       stim - the stimulus for the next trial
%       opreturn    - optional return information about the procedure
%               .ocase - a number giving information on how the stimulus was
%               selected.
%       pobj - VERY IMPORTANT: choosestim modifies the structure (we store
%       the index of the selected input). Therefore its important that you
%       save the returned object. This facilitates fast lookup when calling
%       observe.
% Explanation: Chooses the stimulus by doing a 2-d optimization over the
% precomputed region.
function [optstim, oreturn,pobj, varargout]=choosestim(pobj,post,shist,einfo,mparam,glm,varargin)


    %initialize the oreturn structure
%we want the fields to always be the same becase otherwise we will have
%problems when we try to convert the cell array of oreturn to a structure
%array
%   nan indicates value not set
    oreturn=struct('dotmu',nan,'sigmasq',nan,'ocase',nan, 'stimind',nan,'stimclass',nan);

    %process the optional arguments to get the trial number
    for j=1:2:length(varargin)
        switch varargin{j}
            case 'trial'
                trial=varargin{j+1};
        end
    end
   
        %possible stimuli from which to choose
        stim=[pobj.docs(pobj.stimset).wcounts];
        
    if (trial>pobj.nrand)
        %compute dotmu  
       dotmu=post.m'*stim;
       sigmasq=stim'*post.c;
       sigmasq=sigmasq.*stim';
       sigmasq=sum(sigmasq,2);
       sigmasq=sigmasq';
       
       %lookup the value of each sigmasq on the precomputed grid
       pbase=pobj.PreCompMIObj;
       optpt=[];
       optpt.dotmu=0;
       optpt.sigmasq=0;
       optpt.mi=-inf;
       optpt.ind=0;     %stores index in stimset of the optimal stimulus
       for j=1:length(pobj.stimset)
           mi=lookup(pbase,dotmu(j),sigmasq(j));
           if (mi>optpt.mi)
               optpt.dotmu=dotmu(j);
               optpt.sigmasq=sigmasq(j); 
               optpt.mi=mi;
               optpt.ind=j;
           end
       end
       %the index into stimset
       stimsetind=optpt.ind;
        oreturn.dotmu=optpt.dotmu;
        oreturn.sigmasq=optpt.sigmasq;
                pbase=pobj.PreCompMIObj;
          ocases=pbase.ocases;
        oreturn.ocase=ocases.normal;
    else
        %random stimulus
        nstim=length(pobj.stimset);
        if (nstim==0)
            error('Out of stimuli; inputs');
        end
        
        stimsetind=ceil(rand(1)*nstim);
          pbase=pobj.PreCompMIObj;
          ocases=pbase.ocases;
          oreturn.ocase=ocases.randstim;
    end
    
    %set the stimulus
   optstim=stim(:,stimsetind);
   oreturn.stimind=pobj.stimset(stimsetind);
   oreturn.stimclass=pobj.docs(oreturn.stimind).classid;
   
    %save the index of this stimulus
   pobj.laststimind=pobj.stimset(stimsetind);

   %remove this stimulus from the set of possible stimuli
   pobj.stimset=[pobj.stimset(1:stimsetind-1) pobj.stimset(stimsetind+1:end)];

   %the following allows other choosestim functions to return more
    %arguments
    if (nargout>3)
        for j=1:(nargout-3)
            varargout{j}=[];
        end
    end