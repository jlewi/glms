%function [pmethods,sr,simparam,mparam]=maxupdateshist(modparam,simparam,methods,ropts)
%
%       mparam - model parameters governing model to use
%           .klength - length of input vectors = length of filter
%           .alength    - length of spike history dependence
%                            - set to 0 to ignore spike history terms
%           .ktrue   - value of the true stimulus filter update
%                           klengthx1 - if parameter is constant
%                           klengthxsimparam.niter - if parameter is
%                           changing on each iteration due to diffusion
%           .atrue   - value of the true spike history coefficents
%                         first coefficent corresponds to most recently
%                         fired spikes
%                           klengthx1 - if parameter is constant
%                           klengthxsimparam.niter - if parameter is
%                           changing on each iteration due to diffusion
%           .tresponse - length of response window in which we
%                        count the number of spikes after presenting the
%                        stimulus
%            .pinit    - initial gaussian matrix
%            .mmag - constraint on stimulus
%            .diffusion - structure specifiying diffusion parameters
%                       .on  - turn diffusion on
%                       .q    -   covariance matrix specifying gaussian diffusion
%                               of parameter
%                             -allows you to track moving parameter
%       sparam - simulation parameters
%           .ignoreshist - if not 0. then when optimizing the stimulus
%                                  we consider only the stimulus
%                                  coefficents and ignore the spike history
%                                  components.
%            .randstimuli   - if not 0 then we randomly generate stimuli
%           .niter - number of iterations
%           .rstate - state of the random number generators
%                     leave blank or set to -1 if you don't want to set it
%                     default is to use a different state each time based
%                     on the clock
%           .optsparam -
%           .normalize - if this field exists, all random stimuli are
%           .glm       -structure describing the GLM
%           .nonlinf  - pointer to the nonlinearity that we use
%                       when generating simulated data
%                       does not have to be the exponential function
%           .truenonlinf - true nonlinearity
%           normalized to have this magnitude. We do this to ensure a valid
%           comparison to when we choose the optimal stimulus as we impose
%           that constraint then.
%                   this should be the square of the magnitude.
%       methods - gets passed to initmethods.m
%               - specifies which methods are on and their options.
%       ropts  - options regarding the results/analysis
%           .fname  - name to save data to
%                   - leave blank to not save data
% Return Value:
%       pmethods - results of updating the posteriors
%       sr   -   stimulus and response
%            sr.y - the stimuli - 1 trial per column
%            sr.nspikes - #of spikes after each stimulus
%       simparam - contains the actual simparam used
%                - i.e rstate will store the state of the random number
%                generator
%       mparam
%               .ktrue
%               .atrue - if a diffusion parameter is specified then the
%                         return values with be matrices
%                         (filterlengthxsimparam.niter) each column will be
%                         the theta used on that observation
%       methods
%               -return value includes any default values which might have
%               been set by this function
%Explanation:
%   This function runs the specified method and on each iteration
%   uses the stimulus likely to maximize the info.
%
function [pmethods,sr,simparam,mparam,methods]=maxupdateshist(mparam,simparam,methods,ropts)

error('this version is broken. Either use the latest maxupdateobj.m or go back to an earlier version before I switched to an object oriented model');
contsim=0;             %nonzero means we are continuing a previous simulation
sr=[];
%printiter is the interval to use when printing the iteration number
printiter=10;

if ~isfield(simparam,'contsim')
    simparam.contsim=0;
end
contsim=simparam.contsim;
if (contsim~=0)
    if ~isfield(simparam,'simfile')
        error('In order to continue a simulation you must supply simparam.simfile');
    end
end


%determine number of iterations to run
%for a new sim its simparam.niter
%for continuing a simulation
%its the number of additional iterations
ntorun=simparam.niter;

if (contsim==0)
    %*************************************
    %initialize the parameters for a new simulation
    [mparam,simparam,methods,ropts,vtosave,saveopts,mon]=initnewsim(mparam,simparam,methods,ropts);

else
    pmethods={};
    fprintf('Continuing an earlier simulation \n');
    if ~exist(simparam.simfile,'file')
        error('Simulation file doesnt exist. Cannot continue previous simulation.');
    end
    if ~isfield(simparam,'varsuffix')
        error('You must provide a suffix for the variables to load e.g "rand" or "max"');
    end
    simfile=simparam.simfile;
    simloaded=simparam;

    %    if varinfile(simparam.simfile,sprintf('mparam%s',simloaded.varsuffix))
    %          params=load(simparam.simfile,sprintf('mparam%s',simloaded.varsuffix),sprintf('simparam%s',simloaded.varsuffix),sprintf('methods%s',simloaded.varsuffix),'ropts');
    %      mparam=param.(sprintf('mparam%s',simloaded.varsuffix));
    % else
    params=load(simparam.simfile,sprintf('mparam'),sprintf('simparam%s',simloaded.varsuffix),sprintf('methods%s',simloaded.varsuffix),'ropts');
    mparam=params.mparam;
    %end

    if isstruct(mparam)
        mparam=MParamObj(mparam);
    end
    simparam=params.(sprintf('simparam%s',simloaded.varsuffix));
    methods=params.(sprintf('methods%s',simloaded.varsuffix));
    ropts=params.ropts;
    clear('params');

    simloaded.piter=simparam.niter; %number of previous iterations
    simloaded.ntorun=ntorun;            %additional number of iterations to run
    simloaded.totaliter=simloaded.piter+simloaded.ntorun;
    simparam.niter=simloaded.totaliter;

    %params=load(simloaded.simfile,sprintf('p%s',simloaded.varsuffix));
    %construct a list of enabled methods
    %and get the parameters
    mon={};
    mnames=fieldnames(methods);
    for index=1:length(fieldnames(methods))
        if isstruct(methods.(mnames{index}))
            if methods.(mnames{index}).on >0
                mon{length(mon)+1}=mnames{index};
            end
        else
            if (methods.(mnames{index})>0)
                mon{length(mon)+1}=mnames{index};
            end
        end
    end
    for mind=1:length(mon)
        mname=mon{mind};
        %        setfield(pmethods,mname,[]);
        pmethods.(mname)=methods.(mname);
    end

    % clear('params');
    clear('mnames')
end

%tell simparam to save the current start state of the random number
%generators as the start state.
%this is not backwards compatible
simparam=savestartstate(simparm);

%*************************************************************************
%Loop over the iterations
%*************************************************************************
%loop through enabled methods
fprintf('Begin infomax \n');

for mindex=1:length(mon)
    mname=mon{mindex};
    %clear variables from previous methods
    einfo=[];
    post=[];
    newpost=[];
    prior=[];




    %*****************************************************
    %declare the structures to hold the results for this simulation
    srdata=[];
    pdata=[];
    %pdata=pmethods.(mname);

    fprintf('Compute the %s approximation of the posterior \n',mname);

    %initialize the data for a new simulation
    %declare a structure to hold the stimuli
    srdata.y=zeros(mparam.klength,ntorun);
    srdata.nspikes=zeros(1,ntorun);
    srdata.fcase=zeros(1,ntorun);
    srdata.stiminfo=cell(1,ntorun);
    %initialize structure to store mean
    pdata.m=zeros(mparam.klength+mparam.alength,ntorun+1);
    %initialize structure to store timing
    pdata.timing=[];
    pdata.timing.update=zeros(1,ntorun);
    pdata.timing.optimize=zeros(1,ntorun);
    %get the prior and an initial shist structure if we are
    %doing
    if (contsim==0)


        %a new simulation
        [pinit,shist]=priornewsim(mparam);

        %initialize the pdf
        pdata.m(:,1)=pinit.m;
        pdata.c{1}=pinit.c;


    else
        %initialize the random number generators to the state we left off
        %on
        randn('state',simparam.(mname).rfinishn{1});
        rand('state',simparam.(mname).rfinish{1});

        ppast=load(simloaded.simfile,sprintf('p%s',simloaded.varsuffix));
        ppast=ppast.(sprintf('p%s',simloaded.varsuffix));
        %initialize the pdf
        %load the data for this method from the file
        %and get the posterior we left off on
        pdata.m(:,1)=ppast.(mname).m(:,end);
        pdata.c{1}=ppast.(mname).c{end};
        clear('ppast');


        %allocate space for shist
        shist=zeros(mparam.alength,1);

        if (mparam.alength>0)
            %get the spike history into shist
            srpast=load(simloaded.simfile,sprintf('sr%s',simloaded.varsuffix));
            srpast=srpast.(sprintf('sr%s',simloaded.varsuffix));
            nobsrv=length(srpast.(mname).nspikes);
            if (mparam.alength <= (nobsrv))
                shist(:,1)=srpast.(mname).nspikes(nobsrv:-1:nobsrv-mparam.alength+1);
            else
                shist(1:nobsrv,1)=srpast.(mname).nspikes(nobsrv:-1:1);
            end
            clear('srpast');
        end

    end

    %initialize einfolast to the eigeninfo of the prior
    %we do this because in the rank 1 case we can use the eigenvectors to
    %efficiently compute the entropy
    %keyboard
    if (strcmp(mon{mindex},'newton1d')==1)
        %get the initial eigen decomposition
        [einfolast.evecs einfolast.eigd]=svd(pdata.c{1}(1:mparam.klength,1:mparam.klength));
        einfolast.eigd=diag(einfolast.eigd);
        einfolast.esorted=-1;   %eigenvalues are in descending order
        einfo=einfolast;
    else
        einfolast=[];
    end
    %store entropy
    pdata.entropy=zeros(1,ntorun+1);
    %compute entropy of prior
    eigd=eig(pdata.c{1});
    pdata.entropy(1)=1/2*mparam.klength*(log2(2*pi*exp(1)))+1/2*sum(log2(abs(eigd)));

    post.m=pdata.m(:,1);
    post.c=pdata.c{1};

    obsrv.twindow=mparam.tresponse;

    %matrix to store stimului - stimulus + spike history terms
    %for all iterations
    %do this because newton's method requires it
    stimmat=zeros(mparam.klength+mparam.alength,ntorun);

    %it is absolutely important that we perform all of the iterations
    %for a particular method before going onto a different method
    %this is is because I make assumptions about what post, prior
    %and newpost store in each methods update.
    for tr=2:ntorun+1

        if (mod(tr-1,printiter)==0)

            fprintf('%s: iteration %0.2g \n',mon{mindex},tr-1);
        end

        %*****************************************************************
        %compute the spike history
        %******************************************************************
        %spike history is just the previous responses
        %we take them to be zero if they happened before the start of the
        %experiment
        %tr-2 is actual number of observations we have already made
        nobsrv=tr-2;    %how many observations have we made

        %only compute shist if we have spike history terms
        if (mparam.alength >0)
            if  isequal(simparam.finfofunc,@finfologistic)
                %spike history is just a one 1 because of the fixed term
                shist=1;
                if (mparam.alength>1)
                    error('logistic function can only have a single fixed term');
                end
            else
                if (mparam.alength <= (nobsrv))
                    shist(:,1)=srdata.nspikes(nobsrv:-1:nobsrv-mparam.alength+1);
                else
                    shist(1:nobsrv,1)=srdata.nspikes(nobsrv:-1:1);
                end
            end
        end
        %******************************************************************
        %maximize the information
        %to choose the stimulus
        %******************************************************************

        param.plots.on=0;

        %tic;
        %initialize time for optimization step
        %and to compute the eigendecomposition
        searchtime=0;
        eigtime=0;

        %if diffusion is on we need to add the diffusion covariance matrix
        %to our posterior matrix before we optimize the information
        %except if this is the first iteration
        if (mparam.diffusion.on~=0 & ((tr>2)|contsim~=0))
            keyboard
            %fprintf('Addiding diffusion matrix');
            post.c=mparam.diffusion.diffc^2*post.c+mparam.diffusion.q;
            %when we multiply it we should also change the variance.
            post.m=post.m*mparam.diffusion.diffc;

            %if the noise is white then we can compute the eigenvectors &
            %eigenvalues of the new posterior matrix
            %this would allow us to use the rank 1 update
            if(mparam.diffusion.iswhite~=0)
                if ~isempty(einfo)
                    %qeig should be computed inside maxupdeshist
                    einfo.eigd=mparam.diffusion.diffc^2*einfo.eigd+mparam.diffusion.qeig;

                    if isfield(post,'invc')

                        if ~isempty(post.invc)
                            post.invc=einfo.evecs*diag(1./einfo.eigd)*einfo.evecs';
                        end
                    end
                else
                    einfo=[];
                    post.invc=[];
                end
                %do we need to recompute first derivative.
                post.dlpdt=[];

            else

                %set post.invc and einfo to empty matrixes becaue our
                %we no longer know their value
                post.invc=[];
                %measurement of 1st derivative is also innaccurate
                post.dlpdt=[];

                %the eigendecomposition is no longer valid
                einfo=[];
            end
        end

        %For backwards compatibility check if simparam.finfo is
        %defined and if its a an object
        useFisherObj=0;
        if isfield(simparam,'finfo')
            if (isa(simparam.finfo,'StimChooserObj'))
                useFisherObj=1;
            end
        end


        if (useFisherObj~=0)
            %need to figure out the absolute trial number.
            if exist('simloaded','var')
                ctrial=simloaded.piter+tr-1;
            else
                ctrial=tr-1;
            end
            %we need to save the object because some calls to choosestim
            %modify the object.
            [xmax,stiminfo,simparam.finfo]=choosestim(simparam.finfo,post,shist,einfo,mparam,simparam.glm,'trial',ctrial);
            clear('ctrial');
        else
            warning('Using old optimization code. i.e not an object');
            %we want to maximize the stimuli
            %check if we are ignoring shistory terms or if no spike history
            %terms are provided.
            if ((simparam.ignoreshist==0) & (mparam.alength >0) )
                %[finfo,xpts]=fonucirc(post,shist,mparam.mmag);
                %[fopt,ind]=max(finfo);
                %xmax=xpts(:,ind);

                %einfo is nonempty only if were able to calculate the
                %eigenvectors as in the newton1d case where the
                %eigenvectors are just the rank1 symettric update problem
                %otherwise einfo is empty and fishermax just performs an
                %svd

                %einfo should be the eigenvectors of just the Ck terms
                %[xmax,fopt,einfo]=fishermaxshist(post,shist,mparam.mmag,einfo,simparam.optparam);



                %backwards compatibility
                if  isequal(simparam.finfofunc,@finfologistic)

                    [xmax,muporjopt,fopt,ftime]=fishermaxlogistic(post,shist,einfo,mparam,simparam.finfofunc,simparam.optparam);
                else
                    [xmax,muporjopt,fopt,ftime,srdata.fcase(tr-1)]=fishermaxshist(post,shist,einfo,mparam,simparam.finfofunc,simparam.optparam);
                    %error('Should above line be postnostim or post?')
                end

                %                [xmax,muporjopt,fopt,ftime,srdata.fcase(tr-1)]=fishermaxshist(post,shist,einfo,mparam,simparam.finfofunc,simparam.optparam);
                searchtime=ftime;
                if isfield(ftime,'eigtime');
                    eigtime=ftime.eigtime;
                else
                    eigtime=0;
                end
                %keyboard

            else
                %when we optimize the stimulus we ignore the stimulus
                %coefficents, either because there are no stimulus coefficents
                %or because we want to ignore them
                postnostim.m=post.m(1:mparam.klength,1);
                postnostim.c=post.c(1:mparam.klength,1:mparam.klength);
                %[xmax,fopt,einfo,eigtime,searchtime]=fishermax(postnostim,mparam.mmag,einfo,simparam.optparam);
                shist=[];


                if  isequal(simparam.finfofunc,@finfologistic)

                    [xmax,muporjopt,fopt,ftime]=fishermaxlogistic(postnostim,shist,einfo,mparam,simparam.finfofunc,simparam.optparam);
                else
                    [xmax,muporjopt,fopt,ftime,srdata.fcase(tr-1)]=fishermaxshist(postnostim,shist,einfo,mparam,simparam.finfofunc,simparam.optparam);
                end

                %[ old.xmax,old.fopt,einfo,eigtime,searchtime]=fishermax(postnostim,mparam.mmag,einfo,simparam.optparam);
                % old.flopt=log(old.fopt);
                %if (old.flopt>fopt)
                %   keyboard
                %end

                %              searchtime=ftime;
                %              eigtime=ftime.eigtime;

            end



            if (isnan(fopt))
                error('Fisher information is nan');
            end

            if (isinf(fopt))
                error('fisher information is infinite');
            end

        end
        %toptimize=toc;


        %check to make sure xmax is valid
        if (sum(isnan(xmax))>0)
            error('Optimal stimulus is invalid');
        end


        if (sum(imag(xmax))~=0)
            error('optimal stimulus is not real');
        end


        %sample the model to generate the response
        tinfo.twindow=mparam.tresponse;

        %************************************************************
        %Full stimulus
        %**************************************************************
        %form the full stimulus by combining the spike history
        %and stim terms
        stim=[xmax;shist];
        stimmat(:,tr-1)=stim;


        %******************************************************************
        %generate the observation
        %stimes=samp1dglmh(@glm1dexp,mparam.ktrue,xmax,tinfo);
        %******************************************************************

        srdata.y(:,tr-1)=xmax;                     %save the stimulus
        srdata.stiminfo{tr-1}=stiminfo;
        %just generate spike counts
        if (mparam.diffusion.on==0)
            %theta is constant
            theta=[mparam.ktrue; mparam.atrue];
        else
            if ~isempty(mparam.atrue)
                theta=[mparam.ktrue(:,tr-1);mparam.atrue(:,tr-1)];
            else
                theta=[mparam.ktrue(:,tr-1)];
            end
        end
        %WARNING SAMPLER FOR DIFFERENT DISTRIBUTIONS NEEDS TO BE CHANGED
        %srdata.nspikes(1,tr-1)=poissrnd(simparam.truenonlinf(theta,stim));
        %for backwards compatibility. check if there is an observer field
        if ~isfield(simparam,'observer')
                %old soon to be obsolete way
                srdata.nspikes(1,tr-1)=simparam.glm.sampdist(simparam.truenonlinf(theta'*stim));
        else
             srdata.nspikes(1,tr-1)=observe(simparam.observer,stim);
        end

        if isinf(srdata.nspikes(1,tr-1))
            error('infinite # of spikes');
        end
        obsrv.n=srdata.nspikes(1,tr-1);

        if (isnan(obsrv.n))
            error('Number of observed spikes is not number');
        end
        %call the appropriate method to update the posterior
        %doing this check on each iteration might be slow
        %might be better to give each method a unique number
        %and then just check that element in array to see if its on or off


        %compute the inverse of the covariance matrix using the
        %eigenvectors/values of the covariance matrix if they are available
        %This can save us some from calculating it in the newtonian
        %update

        if ~(isempty(einfo))
            %we want to time the computation of computing the inverse
            %include it in our update step time
            %if we method doesn't use post.invc then
            %we shouldn't add it later on to the timing
            tic;
            %could proballly optimize this  pdata.timing.searchtime(1,tr-1) by not creating a matrix for the
            %eigenvalues
            post.invc=einfo.evecs*diag(1./einfo.eigd )*einfo.evecs';
            %we add the time of the newton iteration to what ever time
            %it took to compute the inverse of the posterior
            %if we did that earlier using the eigenvectors from the
            %stimulus optimization step
            pdata.timing.update(1,tr-1)=pdata.timing.update(1,tr-1)+toc;
            %set einfo to empty matrix because once we compute the update
            %the eigenvectors will no longer be accurate
            %the efficient update of the eigendecompostion requires the
            %last eigenvectors so we save them
            einfolast=einfo;
            einfo=[];

            %Reset post.invc=to be empty to see if algorithm works if we
            %don't use the eigenvectors to compute the inverse efficiently
            %post.invc=[];
        else
            post.invc=[];
        end


        %with spike history terms
        %we compute ML estimate as before except now the stimulus is
        %the coatenation of the stimulus and spike history term

        switch mon{mindex}
            case {'ekf'}

                [post,pll]=ekfposteriorglm(post,stim,obsrv,@glm1dexp);
            case {'ekfmax'}
                tic;
                [post,pll]=ekfposteriormax(post,stim,obsrv,@glm1dexp);
                pmethods.ekfmax.timing.update(1,tr-1)=toc;

                %currently ekfposteriormax doesn't set the invc or dlpdt
                %fields so set them to zero
                post.invc=[];
                post.dlpdt=[];
            case {'ekf1step'}
                error('Ekf1step not implemented for spike history terms yet.')';
                %currently ekfposteriormax doesn't set the invc or dlpdt
                %fields so set them to zero
                post.invc=[];
                post.dlpdt=[];
                %compute posterior using 1step update
                %every kth iteration we do a 1 step update
                %if (mod((tr-1),pmethods.ekf1step.onestep)==0)
                %first update pekf using just the observation on this trial
                %[post,pll]=pmethods.ekf1step.ekffunc(post,(srdata.y(:,tr-1)),obsrv,@glm1dexp);

                %obsrevations is all observations up to this time pt
                %want a row vector each column a different trial
                %obsrv.n=(srdata.nspikes(1:tr-1));
                %keyboard
                %  [post]=post1stepgrad(pinit,post,srdata.y(:,1:tr-1),obsrv,@glm1dexp);
                %fprintf('Ekf one step correction \n');

                %else
                %for debugging of the ekf I also return
                %pll which describes gaussian approximation of the likleihod
                %   [post,pll]=pmethods.ekf1step.ekffunc(post,stim,obsrv,@glm1dexp);

                % end

            case {'g'}
                [ppost]=gposteriorglm(ppost,stim,obsrv,@glm1dexp);
                %currently ekfposteriormax doesn't set the invc or dlpdt
                %fields so set them to zero
                post.invc=[];
                post.dlpdt=[];
            case {'allobsrv'}

                %11-16
                %compute gaussian approximation by performing gradient
                %ascent on the true posterior
                %Coded this up to see how it would perform if we have
                %misspecification
                %************************************************
                %1. Create the stimulus matrix each column is the stimulus +
                %any spike history appended to it
                %
                % VERY IMPORTANT: Since we are using all observations we
                % need to load any past observations from the file
                % we do this once

                %initialize the structures to store the observations and
                %stimuli do this on just first iteration
                if (tr==2)
                    allobsrv=[];           %store variables for just the all obsrv update rule
                    allobsrv.obsrv=zeros(1,simparam.niter);
                    allobsrv.allstim=zeros(mparam.klength+mparam.alength,simparam.niter);
                end
                %load all previous stimuli and observations from a file on
                %first iteration
                if (contsim~=0 && tr==2)
                    allobsrv.allstim=zeros(mparam.klength+mparam.alength,simparam.niter);
                    allobsrv.srpast=load(simloaded.simfile,sprintf('sr%s',simloaded.varsuffix));
                    allobsrv.srpast=allobsrv.srpast.(sprintf('sr%s',simloaded.varsuffix));
                    allobsrv.allstim(1:mparam.klength,1:simloaded.piter)=allobsrv.srpast.(mname).y;

                    allobsrv.obsrv(1,1:simloaded.piter)=allobsrv.srpast.(mname).nspikes;
                    rmfield(allobsrv,'srpast');
                end

                nobsrv=tr-1;            %number of trials for which stimuli have been presented and responses are known
                %on just this iteration
                %2. Insert the most recent stimulus
                if (contsim~=0)
                    allobsrv.totobsrv=nobsrv+simloaded.piter;
                else
                    allobsrv.totobsrv=nobsrv;
                end
                allobsrv.allstim(1:mparam.klength,allobsrv.totobsrv)=srdata.y(:,nobsrv);
                allobsrv.obsrv(1,allobsrv.totobsrv)=srdata.nspikes(1,nobsrv);

                %3. add the spike history
                if (mparam.alength >0)
                    %if we are continuing a simulation and we have spike
                    %history terms we possible need to load spike history
                    %terms from a past file.
                    if (contsim~=0 && nobsrv < mparam.alength)
                        %we need to append the shist terms which were
                        %loaded from the file
                        %these should be stored in shist and they
                        %should be stored with most recent spike in
                        %first element so we need to reverse them
                        spikes=[shist(end:-1:1) srdata.nspikes(1:tr-1)];
                        warning('Need to check this code is correct');
                    else
                        spikes=[ones(1,mparam.alength) srdata.nspikes(1:tr-1)];
                    end
                    for titer=1:nobsrv
                        %we need to obtain the spikes which could have acted as
                        %stimulus for trial titer
                        npspikes=nobsrv-1;  %number of spikes which came before the most recent trial
                        %most recent observation is first
                        allobsrv.allstim(mparam.klength+1:end,allobsrv.totobsrv)=spikes(npspikes+mparam.alength:-1:npspikes+1);
                    end


                    warning('code assembling observations with spike history has not been debugged');
                end
                ao=[];

                %compute the peak of the posterior using all observations
                %we initialize gradient ascent with the peak from the previous
                %iteration
                ao.initm=post.m;
                post=[];                %clear post to prevent incorect fields from accumulating (i.e invc, eig etc...)
                [post.m]=postgradasc(ao.initm,mparam.pinit,allobsrv.allstim(:,1:allobsrv.totobsrv),allobsrv.obsrv(:,1:allobsrv.totobsrv),simparam.glm);

                %compute the covariance matrix  using all the observations
                [ao.dldpt ao.dlp2dt]=d2glm(post.m,mparam.pinit,allobsrv.obsrv(:,1:allobsrv.totobsrv),allobsrv.allstim(:,1:allobsrv.totobsrv),simparam.glm);

                post.c=-inv(ao.dlp2dt);
                post.invc=-ao.dlp2dt;
                %clear any extra variables

                clear('spikes','titer','nobsrv','npspikes','ao');

            case {'newton1d'}

                if (tr==10)
                    fprintf('test');
                end

                prior=post;
                %observation is just the current observation
                obsrv.n=(srdata.nspikes(tr-1));

                % The mean of our posterior is the peak of the true
                % posterior
                % therefore its a root of the derivative of the log of the posterior
                % we use newton's method to compute it
                % The new covariance matrix is the inverse of the - hessian at the
                % new ut
                % The seed for newton's methods is the mean from the previous
                % iteration

                tolerance=methods.(mname).tolerance;
                [post,pdata.niter(1,tr-1),ntime]=postnewton1d(prior,stim,obsrv,simparam.glm,tolerance);


                if ~isempty(find(isnan(post.m)))
                    error('new mean is nan');
                end
                if ~isempty(find(isnan(post.c)))
                    error('Covariance has Nan');
                end
                pdata.timing.update(1,tr-1)=ntime;


                %save memory by not saving covariance matrices which
                %are no longer needed
                %zero out old matrices which are no longer needed
                %we will save every simparam.lowmem iter
                %set it to inf to save only first and last
                if (simparam.lowmem~=0)
                    if (tr>2)
                        if isinf(simparam.lowmem)
                            pdata.c{tr-1}=[];
                        else
                            %we subtract 2
                            %b\c tr-1 is the number of iterations
                            %but we always need to wait 1 iteration before
                            %getting rid of the matrix because we need it
                            %to serve as the prior
                            d=(tr-2)/simparam.lowmem;
                            if (d~=floor(d))
                                pdata.c{tr-1}=[];
                            end
                        end
                    end
                    %pdata.c{tr-1}=[];
                end

                %*******************************************************
                %efficiently compute the eigenvectors
                %*******************************************************
                %In the nondiffusion case the eigenvectors are just a rank
                %1 update. So we compute einfo efficiently this avoids
                %having to call svd in fishermax
                %
                %The only case when einfolast is empty and diffusion isn't
                %on is if we are using random stimuli and this is the first
                %iteration

                %can we compute the eigenvectors using the rank 1
                %updates yes if:
                %   1. diffusion is not on
                %   2. we have the previous eigendecomposition
                %   (einfolast
                %  3. diffusion is on but diffusion is white
                rank1eig=1; %set it to compute rank1eig and then test if conditons are met
                if (mparam.diffusion.on==1)
                    if (mparam.diffusion.iswhite==0)
                        rank1eig=0;
                    end
                end
                if isempty(einfolast)
                    rank1eig=0;
                end
                if (rank1eig~=0)

                    [gd1, gd2]=d2glmeps(post.m'*stim,obsrv.n,simparam.glm);
                    rho=gd2/(1-gd2*stim'*prior.c*stim);
                    %                    if isnan(rho(
                    %debugging
                    %cnew
                    %cfull=prior.c-rho*(prior.c*stipdata.timing.optimize(1,tr-1)=searchtime+eigtime;m)*(stim'*prior.c);
                    if ~isempty(shist)
                        u=prior.c(1:mparam.klength,1:mparam.klength)*xmax+prior.c(1:mparam.klength,mparam.klength+1:end)*shist;
                    else
                        u=prior.c(1:mparam.klength,1:mparam.klength)*xmax;
                    end
                    upow=u'*u;
                    u=u/upow^.5;
                    rho=rho*upow;

                    %ck
                    %cktrue=post.c(1:mparam.klength,1:mparam.klength);
                    %[cktrue-(prior.c(1:mparam.klength,1:mparam.klength)+rho*u*u')]
                    %debugging check
                    %we properly write post.c the updated matrix as a  rank
                    %1 update
                    %post.c=prior.c+rho*u*u'
                    %      if (tr==9)
                    %      keyboard;
                    %   end

                    %if the eigenvalues are sorted in descending order
                    %reverset the order
                    if isfield(einfolast,'esorted')
                        if (einfolast.esorted==-1)
                            einfolast.eigd=einfolast.eigd(end:-1:1);
                            einfolast.evecs=einfolast.evecs(:,end:-1:1);
                        end
                    end
                    tic;
                    z=einfolast.evecs'*u;
                    [einfo.eigd einfo.evecs]=rankOneEigUpdate(einfolast.eigd,z,einfolast.evecs,rho);
                    if ~isempty(find(isnan(einfo.evecs)))
                        warning('Eigenvectors not defined');
                        warning('Computing Eigenvectors via SVD');
                        [einfo.evecs einfo.eigd]=svd(post.c(1:mparam.klength,1:mparam.klength));
                        einfo.eigd=diag(einfo.eigd);
                        einfo.esorted=-1;   %eigenvalues are in descending order
                    end
                    %keyboard;
                    eigtime=toc;
                    einfo.esorted=1;      %rankOne returns eigenvalues sorted in ascending order
                    %******************************************************
                    %code for debugging verifying the rank1 computed
                    %eigenvalues
                    %*****************************************************
                    %debugging compute the eigenvalues
                    %[evtrue eigd]=svd(post.c);
                    %resort eigd and u in ascending order
                    %eigd=diag(eigd);
                    %eigd=eigd(end:-1:1);
                    %evtrue=evtrue(:,[length(eigd):-1:1]);

                    %eigderr=sum((eigd-einfo.eigd).^2);
                    %if (eigderr>10^-10)
                    %    keyboard
                    %end
                    %everr=sum(sum((evtrue-einfo.evecs).^2));
                    %if (everr>10^-10)
                    %    keyboard
                    %end
                    %check rank 1 expression = the new matrix post.c
                    %mse=post.c-einfolast.evecs*(diag(einfolast.eigd)+rho*z*z')*einfolast.evecs';
                    %mse=sum(sum(mse.^2));
                    %if (mse>10^-10)
                    %    warning('New covariance matrix does not match the rank 1 update');
                    %   keyboard
                    %end
                    %now evaluate accuracy of eigenccomposition
                    %mse=post.c-einfo.evecs*diag(einfo.eigd)*einfo.evecs';
                    %mse=sum(sum(mse.^2));
                    %if (mse>10^-10)
                    %   keyboard
                    %post.c=einfolast.evecs*(diag(einfolast.eigd)+rho*z*z')*einfolast.evecs';
                    %end

                    %compute the entropy
                    pdata.entropy(tr)=1/2*mparam.klength*(log2(2*pi*exp(1)))+1/2*sum(log2(einfo.eigd));

                end
            case {'newton'}


                %only set the prior once
                %on the first trial
                if (tr==2)
                    prior.m=pmethods.newton.m(:,1);
                    prior.c=pmethods.newton.c{1};
                    prior.invc=inv(prior.c);
                end
                obsrv.n=(srdata.nspikes(1:tr-1));

                % The mean of our posterior is the peak of the true posterior
                % therefore its a root of the derivative of the log of the posterior
                % we use newton's method to compute it
                % The new covariance matrix is the inverse of the - hessian at the
                % new ut
                % The seed for newton's methods is the mean from the previous
                % iteration

                %time the update
                tic;
                %post.m=pdata.m(:,tr-1);
                %post.c=pdata.c{tr-1};
                newtonerr=inf;

                while (newtonerr >pdata.tolerance);
                    %observations are all obsrvations upto this point
                    %stimuli are all stimuli upto this point
                    [newpost]=post1stepgrad(prior,post,stimmat(:,1:tr-1),obsrv,@glm1dexp);
                    newtonerr=(newpost.m-post.m);
                    newtonerr=(newtonerr'*newtonerr)^.5;
                    post=newpost;
                    pdata.niter(1,tr-1)=pdata.niter(1,tr-1)+1;
                end
                pdata.newtonerr(1,tr-1)=newtonerr;
                pdata.timing.update(1,tr-1)=toc;
                %we add the time of the newton iteration to what ever time
                %it took to compute the inverse of the posterior
                %if we did that earlier using the eigenvectors from the
                %stimulus optimization step
                pdata.timing.update(1,tr-1)=toc+pdata.timing.update(1,tr-1);
                %pdata.timing.optimize(1,tr-1)=toptimize;
        end

        %if we haven't already computed the entropy then compute it
        %we could potentially improve the running time of this by using the
        %efficient eigendecomposition when appropriate
        if (pdata.entropy(tr)==0)
            %do we need to compute the eigenvalues?
            ceig=0;
            if isempty(einfo)
                ceig=1;
                eigdent=eig(post.c);
            else
                eigdent=einfo.eigd;
            end
            %compute the entropy
            pdata.entropy(tr)=1/2*mparam.klength*(log2(2*pi*exp(1)))+1/2*sum(log2(eigdent));
            eigdent=[];
        end
        pdata.m(:,tr)=post.m;
        pdata.c{tr}=post.c;
        if isstruct(searchtime)
            pdata.timing.optimize(1,tr-1)=searchtime.fishermax+eigtime;
        else
            pdata.timing.optimize(1,tr-1)=searchtime+eigtime;
        end
        pdata.timing.searchtime(1,tr-1)=searchtime;
        pdata.timing.eigtime(1,tr-1)=eigtime;
       
    end %loop over iterations for a particular method


   
    %tell simparam to save the current start state of the random number
   %generators as the end state.
    %this is not backwards compatible
    simparam=saveendstate(simparm);
    
    %*********************************************************
    %end of simulation for this method
    %merge the results
    if (contsim==0)
        pmethods.(mname)=pdata;
        sr.(mname)=srdata;

    else

        %merge results with previous simulation
        ppast=load(simloaded.simfile,sprintf('p%s',simloaded.varsuffix));
        ppast=ppast.(sprintf('p%s',simloaded.varsuffix));

        %get a list of all fields of pdata

        %fnames=fieldnames(pdata);
        %fexclude={'tolerance','maxdm','on','label'}; %fields to exclude from merge
        %merge fields
        %this assumes in arrays the data for different trials goes
        %across columns
        %for fname=fnames
        %see if field is to be exclude
        %  sfind=strmatch(fname,strvcat(fexclude))

        % if isempty(sfind)
        %is it structure array
        %   if isa(ppast.(fname),'double')

        %     mdata=zeros(size(ppast.(fname),1),mparam.alength+mparam.klength,simloaded.totaliter);
        %for each field name merge the data
        %the means
        m=zeros(mparam.alength+mparam.klength,1+simloaded.totaliter);
        m(:,1:(simloaded.piter+1))=ppast.(mname).m;
        m(:,(simloaded.piter+2):end)=pdata.m(:,2:end);
        ppast.(mname).m=m;
        clear('m');

        %covariance matrices
        %the last covariance matrix in ppast should be the same as the
        %first entry in c{index}
        %the last covariance
        for index=1:ntorun
            %we add 1 because we don't want to copy the prior
            ppast.(mname).c{simloaded.piter+1+index}=pdata.c{index+1};
        end

        %entropy
        entropy=zeros(1,simloaded.totaliter+1);
        entropy(:,1:simloaded.piter+1)=ppast.(mname).entropy;
        entropy(:,(simloaded.piter+2):end)=pdata.entropy(1,2:end);
        ppast.(mname).entropy=entropy;
        clear('entropy');

        if isfield(pdata,'niter')
            niter=zeros(1,simloaded.totaliter);
            niter(1,1:simloaded.piter)=ppast.(mname).niter;
            niter(1,simloaded.piter+1:end)=pdata.niter;
            ppast.(mname).niter=niter;
            clear('niter');
        end


        if isfield(pdata,'newtonerr')
            newtonerr=zeros(1,simloaded.totaliter);
            newtonerr(1,1:simloaded.piter)=ppast.(mname).newtonerr;
            newtonerr(1,simloaded.piter+1:end)=pdata.newtonerr;
            ppast.(mname).newtonerr=newtonerr;
            clear('newtonerr');
        end


        timing=[];
        timing.searchtime=[ppast.(mname).timing.searchtime pdata.timing.searchtime];
        timing.optimize=[ppast.(mname).timing.optimize pdata.timing.optimize];
        timing.eigtime=[ppast.(mname).timing.eigtime pdata.timing.eigtime];
        timing.update=[ppast.(mname).timing.update pdata.timing.update];
        ppast.(mname).timing=timing;
        clear('timing');

        %set the pmethod fields of pmethods
        pmethods.(mname)=ppast.(mname);
        clear('ppast');


        %merge the spike history
        %get the spike history into shist
        srpast=load(simloaded.simfile,sprintf('sr%s',simloaded.varsuffix));
        srpast=srpast.(sprintf('sr%s',simloaded.varsuffix));

        srmerge.y=[srpast.(mname).y srdata.y];
        srmerge.nspikes=[srpast.(mname).nspikes srdata.nspikes];
        srmerge.fcase=[srpast.(mname).fcase srdata.fcase];
        srmerge.stiminfo=[srpast.(mname).stiminfo srdata.stiminfo];
        sr.(mname)=srmerge;
        clear('srmerge');
        clear('srpast');





    end % if statement for merging results

    %clean up of any variables specific to that method
    %11-17 variables specific to that method are stored in structure
    %(mname) if needed
    %do this to free up memory and decrease likelihood of risk for
    %interference between methods
    if exist(sprintf('%s',mname),'var')
        clear(sprintf('%s',mname));
    end
end %end loop over the different methods for computing the update
fprintf('Finished ! \n');

clear('priormat');


%save data
%don't save the data anymore because it creates probablems
%if (~(isempty(ropts.fname)))
%    savedata(ropts.fname,vtosave,saveopts);
%    fprintf('Saved to %s \n',ropts.fname)
%end



%**************************************************************************
%
%
%                                           END MAIN FUNCTION
%
%
%
%
%**************************************************************************
%**************************************************************************
%
%
%                                           Update Functions
%
%
%
%
%**************************************************************************


%**************************************************************************
%
%
%                                           Initialization/setup functions
%
%
%
%
%**************************************************************************
%**************************************************************************
%initialize the prior for a new simulation
%**************************************************************************
function [pinit,shist]=priornewsim(mparam);
%**************************************************************************
%mean and covariance of initial prior
pinit=mparam.pinit;

%allocate space for shist
shist=zeros(mparam.alength,1);

%**************************************************************************
%initialize the parameters for a new simulation
%
% Return
%       pmethods - structure containing parameters for each individual
%       methods
%       mon - list of methods which are on
function [mparam,simparam,pmethods,ropts,vtosave,saveopts,mon]=initnewsim(mparam,simparam,methods,ropts)
%***********************
%validate simparam
simparam=validsimparam(simparam);

%mparam can either be a structure or an object
%if its a structure we create an object from it
if isstruct(mparam)
    mparam=MParamObj(mparam);
end


if (mparam.alength==0)
    %if no spike history terms are provided set the length to zero
    %and atrue to the empty vector
    simparam.ignoreshist=1;
end


%*************************************************************************
%setup pmethods structure
%************************************************************************

%initialize the pmethods structure
pmethods=initmethods(methods);

if (pmethods.newton.on~=0)
    pmethods.newton.newtonerr=zeros(1,simparam.niter);
    pmethods.newton.niter=zeros(1,simparam.niter);

end

if (pmethods.newton1d.on~=0)
    pmethods.newton1d.newtonerr=zeros(1,simparam.niter);
    pmethods.newton1d.niter=zeros(1,simparam.niter);

end
%construct a list of enabled methods
mon={};
mnames=fieldnames(pmethods);
for index=1:length(fieldnames(pmethods))
    if pmethods.(mnames{index}).on >0
        mon{length(mon)+1}=mnames{index};
    end
end

%**************************************************************************
%diffusion parameter
%**************************************************************************
%if a diffusion parameter is specified the only update we can use is the 1d
%step
%check if diffusion is on
if (mparam.diffusion.on~=0)
    if ~isfield(mparam.diffusion,'q')
        error('No diffusion matrix given');
    end

    %make sure no update methods are specified other t
    if (length(mon)>1)
        error('A diffusion parameter can only be specified with newton1d update method');
    else
        if (strcmp(mon{1},'newton1d')~=1)
            error('A diffusion parameter can only be specified with newton1d update method');
        end
    end
end


%**************************************************************************
%Options concerning results
if ~exist('ropts','var')
    ropts=[];
end
if ~isfield(ropts,'fname')
    ropts.fname=[RESULTSDIR '\posteriors\' sprintf('%0.2g_%0.2g_pdfs_data.mat',datetime(2),datetime(3))];
    ropts.fname=seqfname(data.fname);
    fprintf('UpdatePosteriors: Date will be saved to: \n \t %s \n',ropts.fname);
else
    if isempty(ropts.fname)
        fprintf('UpdatePosteriors: Data will not be saved \n');
    end
end
%*************************************************************************
%initialization
%************************************************************************
%specify which variables get saved
%vtosave
%name of variables to save
vtosave.pmethods='';
vtosave.kldist='';
vtosave.simparam='';
vtosave.ropts='';
vtosave.mparam='';
vtosave.sr='';

%save options
saveopts.append=1;  %append results so that we don't create multiple files
%each time we save data.
saveopts.cdir =1; %create directory if it doesn't exist



%**************************************************************************
%function to validate the simparam structure
function [simparam]=validsimparam(simparam)
if ~isfield(simparam,'niter')
    simparam.niter=100;
    fprintf('updateposteriors: Using default of %0.3g iterations \n',simparam.niter);
end

if ~isfield(simparam,'rstate')
    simparam.rstate=-1;
end
if (simparam.rstate <0)
    %reset the states of the random number generators to a different state
    %each time
    %save the state so that we can reproduce the results later
    simparam.rstate=sum(100*clock);
end

if ~isfield(simparam,'ignoreshist')
    simparam.ignoreshist=0;
end
if ~isfield(simparam,'randstimuli')
    simparam.randstimuli=0;
end

if ~isfield(simparam,'lowmem')
    simparam.lowmem=0;
end

if ~isfield(simparam,'nonlinf')
    simparam.nonlinf=@glm1dexp;
end

if isfield(simparam,'truenonlinf')
    if ~isequal(simparam.glm.fglmmu,simparam.truenonlinf)
        warning('True nonlinearity and actual nonlinearity are not the same');
    end
else
    simparam.truenonlinf=simparam.nonlinf;
end

%for backwards compabtibility
if ~isfield(simparam,'finfofunc')
    simparam.finfofunc='';
end
randn('state',simparam.rstate);
rand('state',simparam.rstate);