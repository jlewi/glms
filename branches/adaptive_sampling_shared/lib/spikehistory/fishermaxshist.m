%function [xopt,muprojpt,lfopt,timing,fcase]=fishermaxshist(post,shist,eigck,mparam,objfunc,optparam)
%   post - current posterior
%   shist - spike history terms
%           -  needed in order to maximize the fisher info
%   eigck  - eigendecomposition of
%   post.c(1:mparam.klength,1:mparam:klength)
%           .eigd
%           .evecs
%   mparam - model parameters following values are needed
%           .alength
%           .klength
%           .mmag
%   objfunc - pointer to the objective function we want to maximize.
%                -it is a function of muproj and sigmmax(muproj)
%               -leave empty to set to default
%
%   optparam - optional parameters
%           .method   -'varymu' or 'varylagrange'
%           .murange - specify what range of mu to search over
%                                only matters if varymu method is selected
%           .optim      - structure created by optimset, optimget and used
%           by matlab optimization routines
%Explanation: Chooses the stimulus which maximizes the expected fisher information
%  xopt - optimized stimulus
%  fopt - fisher info at optimal stimulus
% 
%Return value
%   lfopt - log of the fisher information at optimal stimulus
%               (value depends on objfunc which may or may not return the log of the fisher info)
%   timing
%       .eigtime - time of the eigendecompositions in obtaining the
%                  modified quadratic form
%       .fishermax-time to compute to find the maximum of the fisher
%       information
%fishermaxshist is outdated
%Following issues have to be resolved
%       1. making sure the eigenvalues are properly sorted before doing
%       anything
function [xopt,muprojopt,lfopt,timing,fcase]=fishermaxshist(post,shist,eigck,mparam,objfunc,optparam)
error('This code is obsolete. Use The MaxPoissEXPMI class instead');
%1-18-07
%for backwards compatability
%optparam used to just be optimset structure
%if optparam has no field optim then 
if ~isfield(optparam,'optim')
    optim=optparam;
    optparam.optim=optim;
    clear('optim');
end

%two different ways to compute the optimium (see below)
%0) vary the projection along mu
%1) vary the lagrange multiplier and compute mu
if ~isfield(optparam,'method')
    optparam.method='varymu';
end
if ~isfield(optparam,'murange')
    optparam.murange=[];
end
switch optparam.method
    case 'varymu'
        optmethod=0;            %0= vary the projection along mu
    case 'varylagrange'
        optmethod=1;
    otherwise
        error('Invalid method specified must be varymu or varylagrange');
end

if (~isempty(optparam.murange) && optmethod==1)
    warning('Range of mu values passed in. But optimizatio method is varylagrange. murange has no effect');
end

if isempty(objfunc)
    objfunc=@finfopoissexp;
end

if isempty(eigck)
    %declare a persistent variable to keep track of how often this warning
    %is printed
    persistent eigwarn;
    if isempty(eigwarn)
        eigwarn=1;
    end
    eigwarn=eigwarn+1;
if (mod(eigwarn,25)==0)
      warning('fishermaxshist: Eigendecomp not supplied Computing SVD. Further warnings suppressed \n');
end
    

    [eigck.evecs eigck.eigd]=svd(post.c(1:mparam.klength,1:mparam.klength));
    eigck.eigd=diag(eigck.eigd);
    eigck.esorted=-1;
end
n=size(post.m,1);   %dimensionality
%**********************************************
%unit vector in direction of mean of just stimulus components
%need its magnitude as well
muk=post.m(1:mparam.klength,1);
mukmag=(muk'*muk)^.5;
%if mukmag happens to be 0 (possible due to intialization)
%then pick a vector in direction [1;...]
%correct solution would to pick stimulus in direction of max eigenvalue
%(but I think we still get that result)
%but I don't want to mess with the code. a mean of 0 should be an anomly
if (mukmag==0)
    warning('Magnitude of muk is zero \n');
    muk=1/(mparam.klength^.5)*ones(mparam.klength,1);
else
muk=muk/mukmag;
end

%projection onto mean of stimulus
if isempty(shist)
    %no spike history terms
    muprojr=0;
else
    muprojr=shist'*post.m(mparam.klength+1:end,1);
end
%*************************************************************************
%Define the quadratic form
%x^t A x + Bx +d
%which has a null space defined by the current mean of the stimulus terms
%so the quadratic function we want to find max and min as function of
%muproj=muk'k; is
%y'*quad.eigd*y +
%(muproj*quad.wmuproj+quad.w)*y+(muproj^2*quad.dmuproj2+muproj*quad.muproj+
%quad.d)
[quad,timing.eigtime]=quadmodshist(post.c,eigck,muk,shist);


%***********************************************************************************
%Code Fork: 2 different ways to compute the maximum
%       there are two different ways to compute the maximum.
%       The methods differ in how we compute sigmamax. In particular do we
%       vary mu_\epsilon (the projection of the stimulus along the mean) or
%       do we vary the lagrange multiplier and compute \mu_epsilon
%************************************************************************
if (optmethod==0)
    [xopt,muprojopt,lfopt,timing]=varymuproj(muprojr,quad,objfunc,muk,mukmag,mparam,optparam);
    fcase=0;
else
    warning('THere are problems with varying the lagrange multiplier to compute the optimium with spike history');
    [xopt,muprojopt,lfopt,timing,fcase]=varylagrangemult(muprojr,quad,objfunc,mukmag,mparam,optparam)
end

%**************************************************************************
%**************************************************************************
%Below is the code to compute the maximum by varying the projection
%of the stimulus along the mean
% 1-18-07
%   moidified it so we can do search over limited range of mu
%*************************************************************************
%**************************************************************************
function [xopt,muprojopt,lfopt,timing,fcase]=varymuproj(muprojr,quad,objfunc,muk,mukmag,mparam,optparam)
%define the function to pass to fminbind
tic;
if ~isfield(optparam,'murange')
    optparam.murange(1)=-mparam.mmag;
    optparam.murange(2)=mparam.mmag;
end
if isempty(optparam.murange)
     optparam.murange(1)=-mparam.mmag;
    optparam.murange(2)=mparam.mmag;
end
fmaxobj=@(m)(-funcformax(m,muprojr,quad,objfunc,muk,mukmag,mparam.mmag,optparam));
[muprojopt lfopt] = fminbnd(fmaxobj,-optparam.murange(1),optparam.murange(2),optparam); 

%compute the max stimulus
[qmax,qmin,xmax,xmin]=maxminquad(quad,muk,muprojopt,mparam.mmag,optparam);
xopt=xmax;
lfopt=-lfopt;
timing.fishermax=toc;

%
%   muproj - projection onto mean of stimulus-this can vary
%   murpojr - projeciton onto mean of spike history - we can't control this
%   quad    - quadratic function to optimize
%   
function [funcval]= funcformax(muproj,muprojr,quad,objfunc,muk,mukmag,mmag,optparam)
    %find sigma max for this value
    [qmax,qmin,xmax,xmin]=maxminquad(quad,muk,muproj,mmag,optparam);
    %evaluate the objective function
    funcval=objfunc(muproj*mukmag+muprojr,qmax);

    
 %computes the fisher information for the poisson case with exponential
 %response
 
%**************************************************************************
%**************************************************************************
%Below is the code to compute the maximum by varying the lagrange
%multiplier
%*************************************************************************
%**************************************************************************
function [xopt,muprojopt,lfopt,timing,fcase]=varylagrangemult(muprojr,quad,objfunc,mukmag,mparam,optparam)
%**************************************************************************
%Find the max
%*********************************************************************
%Have to consider to disjoint intervals
%   i) max occurs for lambda>emax
%           we handle this by doing search over lambda
%   ii) max occurs with lambda=emax
%           handle this by doing search over range of pcon values
%            corresponding to lambda=emax

%define the function to pass to fminbind
tic;
%*******************************************************************
%case 1) search over lambda>emax
fmaxobj=@(v)(-funcformaxv(v,muprojr,quad,objfunc,mukmag,mparam.mmag));
[vopt vlfopt exitflag voutput] = fminbnd(fmaxobj,0,1,optparam.optim); 
timing.slambda=toc;
vlfopt=-vlfopt; %flip sign because we took negative to find min

tic;

%case 2) search over range of pcon corresponding to lambda=emax
%determine the range of possible pcon corresponding to lambda=emax
pconrange=pconsingmax(quad,mparam.mmag);
fmaxobj=@(pcon)(-maxpcon(pcon,muprojr,quad,objfunc,mukmag,mparam.mmag));

poutput=[];
if (length(pconrange)==2)
    [pconopt plfopt exitflag poutput]=fminbnd(fmaxobj,pconrange(1),pconrange(2),optparam.optim); 
    plfopt=-plfopt;
elseif(length(pconrange)==1)
    pconopt=pconrange;
    plfopt=fmaxobj(pconopt);
else
    pconopt=[];
    plfopt=-inf;
end
timing.spcon=toc;

%if isempty (poutput)
%    fprintf('fishermaxshist.m: Iterations=%d \t funccounts=%d \n',voutput.iterations,voutput.funcCount);
%else
%    fprintf('fishermaxshist.m: lam.Iter=%d \t lam.fcounts=%d \t pcon.iter=%d \t pcon.fcoounts=%d\n',voutput.iterations,voutput.funcCount,poutput.iterations,poutput.funcCount);
%end
tic
%determine which solution is optimal
if (vlfopt>plfopt)
    [qmax,pcon,xmax,nsols]= sigmamax(vopt, quad,muk,mparam.mmag);
    finfo=zeros(1,nsols);
    for ind=1:nsols
      finfo(ind)=objfunc(pcon(ind)*mukmag+muprojr,qmax(ind));
    end
    %return the solution which is larger
    [lfopt find]=max(finfo);
    muprojopt=pcon(find);
    xopt=xmax(:,find);
    fcase=0;
else
    lfopt=plfopt;
    muprojopt=pconopt;
    [qmax,xopt]= sigmamaxsing(pconopt, quad,mparam.mmag);
    fcase=1;
end
timing.fishermax=toc+timing.spcon+timing.slambda;

%****************************************************************
%Utility functions
%*****************************************************************
%
%   v            - value between 0 and 1which we map to intervale emax to
%                    infinity
%   murpojr - projeciton onto mean of spike history - we can't control this
%   
% optimization function for case 1
function [funcval]= maxpcon(pcon,muprojr,quad,objfunc,mukmag,mag)
    %compute sigmax
    [qmax,xmax]= sigmamaxsing(pcon, quad,mag);
    
    %evaluate the fisher information
    funcval=objfunc(pcon*mukmag+muprojr,qmax);
    
    
%
%   v            - value between 0 and 1which we map to intervale emax to
%                    infinity
%   murpojr - projeciton onto mean of spike history - we can't control this
%   quad    - quadratic function to optimize
%   
% optimization function for case 1
function [funcval]= funcformaxv(v,muprojr,quad,objfunc,mukmag,mag)

%find sigma max for this value
    [qmax,pcon,xmax,nsols]= sigmamax(v, quad,quad.mu,mag);
    %for each solution we evaluate finfopoisson and return the max.
    finfo=zeros(1,nsols);
    for ind=1:nsols
      finfo(ind)=objfunc(pcon(ind)*mukmag+muprojr,qmax(ind));
    end
    %return the solution which is larger
    [funcval find]=max(finfo);

%    pcon(find)
    
 %computes the fisher information for the poisson case with exponential
 %response
function [finfo]=finfopoissexp(muproj,sigma)
    %compute the log of the fisherinfo
    finfo=muproj+.5*sigma+log(sigma);
    