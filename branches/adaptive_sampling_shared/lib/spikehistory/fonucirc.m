%function fonucirc(post,shist,mmag)
%   post - current posterior
%   shist - the current spike history       
%   mmmag constraint on stimulus
%
% Explanation: Plot the fisher information over the unit circle
%   requires that the stimulus part only be two dimensions
%
%   Return:
%       finfo - fisher information
%       xpts  - xpts where fisher is calculated
function [finfo,xpts]=fonucirc(post,shist,mmag,param)
%script to compute fisher informatin on the unit circle
%post.m=[.2;.1; 0];
%[cvecs d]=svd([.2 .1 .1; .1 .2 .1;.1 .2 .1]);
%post.c=cvecs'*diag([1 2 3])*cvecs;
%shist=[1];
%mmag=1;
if ~exist('param','var')
param.plots.on=0;
end

%**************************************************************************
%make a 1-d plot
%that is a plot of just the pots where ||x||=1
%*************************************************************************
theta=[0:2*pi/90:2*pi];
x=mmag*cos(theta);
y=mmag*sin(theta);

param.xpts=[x;y];
xpts=param.xpts;

%compute the fisher information at each of these points
finfo=zeros(1,length(xpts));
for xind=1:size(xpts,2)
    %full stim
    x=[xpts(:,xind);shist];
    g=exp([x'*post.m]);
    inner=x'*post.c*x;
    h=exp(.5*inner)*inner;
    
    finfo(1,xind)=g*h;

end

if (param.plots.on~=0)

pind=1;    
hf=figure;
set(hf,'Name', 'Fisher Info: Unit Circle');

subplot(3,1,1);
hold on;
plot(theta,finfo);
h_p(1)=plot(theta,finfo,'b.');
lgnd{1}='Fisher Information';
xlabel('\theta');
ylabel('F(x)');
xlim([0 2*pi]);

legend(h_p,lgnd);


%plot x and y
subplot(3,1,2);
plot(theta,xpts(1,:));
ylabel('x');
xlim([0 2*pi])

subplot(3,1,3);
plot(theta,xpts(2,:));
ylabel('y');
xlim([0 2*pi])

end
return;

%*************************************************************************
%Compute the Information at the Eigenvectors and the mean
%*************************************************************************
%We want to compute it at +- the eigenvectors
%plot the eigen values
%compute finfo at the eigen values
pind=pind+1;
%eigenvectors are already normalized 
%we want to plot +-1* the eigenvectors
param.xpts=[evec -1*evec pg.m/(pg.m'*pg.m)^.5];
evec=[evec -1*evec pg.m/(pg.m'*pg.m)^.5];
[feig]=fcurve(pg,param)
thetaeig=acos([evec(1,:)]);
%theta will be in 1st or 2nd quadrant's
%angles in 4quadrant
ind=(evec(2,:)<0).*(evec(1,:)>=0);
thetaeig=thetaeig+(ind)*pi;
%3rd quadrant
ind=(evec(2,:)<0).*(evec(1,:)<0);
thetaeig=thetaeig+(ind)*pi/2;

%now plot the eigenvectors & mean
for index=1:size(evec,2)
    if (index==size(evec,2))
        %its the mean
        pind=pind+1;
        lgnd{pind}='Mean (Normalized)';
    else
        lgnd{pind}='Eigen Vectors'
    end
    plot([thetaeig(index) thetaeig(index)],[0 feig(index)],getptype(pind,2));
    h_p(pind)=plot([thetaeig(index) thetaeig(index)],[0 feig(index)],getptype(pind,1));
    
end
