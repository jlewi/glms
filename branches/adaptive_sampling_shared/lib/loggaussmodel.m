%function loggaussmodel(y,input,k,c)
%      y    -observations - equals a spike train.
%           should be single number
%           mxt
%           -mx1 different observations
%       input - not used for anything just to make it work with my code
%               I needed a place holder
%      k - the different means to compute data probability under
%           - is d x n
%          -  each column is a different model under which to compute
%             the likelihood
%      c -covarance 
%
% Return Value:
%       ll =log likelihood of the data
%           1 x n
%           n - different models under which likelihood is computed 
% Explanation:
%   This computes the likleihood of multiple independendt observations
%   by summing the likelihoods of the individual events
%
%   This assumes data is distributed according to a gaussian distribution
%   of unkown mean but known covariance
%   The purpose of this was to test my monte carlo to measure divergence
function ll=loggaussmodel(y,input,k,c)
    ll=zeros(size(y,1),size(k,2));
    for index=1:size(k,2)
        ll(:,index)=logmvgauss(y',k(:,index),c);
    end
    
    
    ll=sum(ll,1);