%test logistic regression by generating some sample data (which is
%perfectly separable) and compare the accuracy of the two methods
%
%acc.max - %correctly classified by info. max
%acc.rand - %correctly classified by rand
function [acc,facc,data]=logaccuracy(varargin)

%*****************************************
%default names
mname='newton1d';
data=[];
nsamps=1000;
minmag=1;           %minimum magnitude for the test stimuli
for j=1:2:nargin
switch varargin{j}
    case {'fname','filename'}
        fname=varargin{j+1};
    otherwise
        if exist(varargin{j},'var')
            eval(sprintf('%s=varargin{j+1};',varargin{j}));
        else
        fprintf('%s unrecognized parameter \n',varargin{j});
        end
end
end

load(fname);

glm=simparammax.glm;

if (strcmp(glm.dist,'binomial')~=1)
    fprintf('glm must be the binomial distribution \n');
end

%generate random stimuli
%so its possible to avoid recomputing the training set if its passed in as
%an argument
if isempty(data)
    data.stim=randn(mparam.klength,nsamps);
    %normalize the magnitude
    mag=(sum(data.stim.^2,1)).^.5;
    mag=ones(mparam.klength,1)*mag;
    data.stim=(1./mag).*data.stim;
    
    %now randomly select the magnitudes from ball of radius mmag
    %force the minimum magnitude to be 1
    mag=minmag+rand(1,nsamps)*mparam.mmag;
    mag=ones(mparam.klength,1)*mag;
    data.stim=mag.*data.stim
    
    %now classify the data
    data.class=mparam.ktrue'*data.stim;
    data.class=(data.class>0);
end

niter=min(simparammax.niter,simparamrand.niter);


%compute the classification accuracies on each trial
acc.max=zeros(1,niter);
acc.rand=zeros(1,niter);
for k=1:niter
    %for infomax
    pred=pmax.(mname).m(:,k+1)'*data.stim;
    pred=pred>0;
    
    dpred=abs(pred-data.class);
    %number of correct answers
    ncorrect=length(find(dpred==0));
    acc.max(k)=ncorrect/nsamps*100;
    
    %for rand
    pred=prand.(mname).m(:,k+1)'*data.stim;
    pred=pred>0;
    
    dpred=abs(pred-data.class);
    %number of correct answers
    ncorrect=length(find(dpred==0));
    acc.rand(k)=ncorrect/nsamps*100;
    
end

facc.hf=figure;
facc.xlabel='trial';
facc.ylabel='%correct';
facc.name='%correct';
facc.hp=[];
facc.lbls={};
hold on;
pind=1;
facc.hp(pind)=plot(acc.max,getptype(pind));
facc.lbls{pind}='info. max';

pind=pind+1;
facc.hp(pind)=plot(acc.rand,getptype(pind));
facc.lbls{pind}='random';

lblgraph(facc);


