%Test maxquadlam and maxminquad
%
%We randomly generate a matrix, and vector
%We then compute are modified quadratic expression.
%
%we then randomly generate the mean vector
%we maximize the quadratic expression using both methods and check they
%agree
%
%
% This function only tests
%   1) maximizing (not minimizing are quadratic expression)
%   2) Assumes no spike-history/fixed terms
function testmaxquad
%threshold for relative error
threshold=10^-8;
nruns=10;  %how many runs to do
d=10;      %dimensionality

nvals=10;   %how man values of lambda to check
for rind=1:nruns
    %randomly generate a positive definite matrix
    evecs=orth(rand(d,d));
    eigd=rand(1,d)*10;
    
    eorig=EigObj('evecs',evecs,'eigd',eigd);
    eorig=sorteig(eorig,1);
    omatrix=getmatrix(eorig);
    muk=normrnd(zeros(d,1),ones(d,1)*10);
    %truncate it at 3 decimal places
    
    qobj=QuadModObj('C',omatrix,'eigck',eorig,'muk',muk);
    
    %**************************************
    %test modified quadratic form is valid
    rstim=normrnd(zeros(d,1),ones(d,1)*10);
    testquad(qobj,rstim);
    
    mag=rand(1)*10;
    
    %determine the range of pcon for which maximum occurs at the
    %singularity
    [pcon]=pconsingmax(qobj,mag);
    
    if ~(size(pcon,2)==2)    
        fprintf('No local maximum occur at singularity lambda=emax \n');
        pcon=[inf -inf];
    end
    %compute the maximum for a value of v in (0,1) which maps to
    %lam=(emax,infinity)
    for vind=1:nvals
       v=rand(1,1);

       
       [mnonsing.muproj,mnonsing.sigmamax,mnonsing.xmax,mnonsing.nsols]= maxquadlam(qobj,v,mag);
       
       %check if a local ax also exists at singularity
       if (mnonsing.muproj>pcon(1) & mnonsing.muproj<pcon(2))
          fprintf('A local max exists at the singularity: lam=emax. \n') 
          [msing.sigmamax,msing.xmax]= sigmamaxsing(nonsing.muproj, qobj,mag);
          
          if (mnonsing.sigmamax>msing.sigmamax)
              fprintf('Maximum is not at singularity \n');
              lsol=mnonsing.sigmamax;
          else
              lsol=msing;
          end
       else
           lsol=mnonsing;
       end
       
       
       %for each solution check we get same answer as using other method
       for j=1:size(lsol.xmax,2)
          %check xmax yields sigmamax
          lsol.scheck(j)=lsol.xmax(:,j)'*omatrix*lsol.xmax(:,j);
          if ~(compareq(lsol.scheck(j),lsol.sigmamax(j),threshold))
              error('Using Lagrange multipliers. Stimulus does not yield sigmmax');
          end
          
          %*********************************************************
          %use the alternate method
          optim=optimset('TolX',10^-14,'TolF',10^-14);
          [psol.qmax,psol.qmin,psol.xmax,psol.xmin, psol.lmax,psol.lmin]=maxminquad(qobj,lsol.muproj(j),mag,optim) ;
          
          %check relative error is less than threshold
          if ~(compareq(psol.qmax,lsol.sigmamax(j),threshold))
              fprintf('Relative Error %d \n',(abs(psol.qmax-lsol.sigmamax(j))/max(psol.qmax,lsol.sigmamax(j))));
              %check projection long mean
              
              %error('Maximizing with respect to muproj vs. using lagrange multiplier do not aggree. \n could this be because solution is at singularity lam=eigmax');
              if (psol.qmax<lsol.sigmamax(j))
                  error('Error: varying muproj yields smaller value than varying lambda: \n');
              end
          end
       end
    end
    
end

fprintf('Success: Maximizing quadratic expression as function of muproj or lagrange multiplier yield same results \n');
fprintf('Warning: This function does not test optimization under the following conditions:');
fprintf('1) Fixed terms are present \n');
fprintf('2) Optimum occurs at lam=emax \n');

%check whether ttwo values are equal
function v=compareq(q1,q2,threshold)

          if (abs(q1-q2)/max(abs([q1,q2]))>threshold)
                v=false;
          else 
              v=true;
              
          end