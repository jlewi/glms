%10-04-06
%function to test quadmod

error('this script needs to be adapted to work with QuadModObj');

nobj=QuadNonLin('d',10);
test(nobj)
return;
n=3;
nmu=1;

mu=ceil(rand(n,nmu)*10)/10
%mu needs to be orthonormal
mu=orth(mu);

evecs=orth(rand(n,n));
eigd=ceil(rand(n,1)*10)/10;
[eigd ind]=sort(eigd,'ascend');
evecs=evecs(:,ind);
C=evecs*diag(eigd)*evecs';


muproj=ceil(rand(1,nmu)*10)/10;

%generate some random vector
x=ceil(rand(n,1)*10)/10;

%force projections of X along mu to be muproj
r=x-sum(ones(n,1)*(x'*mu).*mu,2)+sum(ones(n,1)*(muproj).*mu,2);

%compute the true quad value r
quadr=r'*C*r;

%compute the value of x using our modified form and see if it returns the
%same value
eigfull.eigd=eigd;
eigfull.evecs=evecs;
[eigmod,w,d]=quadmod(eigfull,mu);

%**************************************************************************
%Debugging
%*******************************************************************
%1. check mu is in nullspace of eigmod
if (abs(sum((eigmod.evecs*diag(eigmod.eigd)*eigmod.evecs')*mu))>10^-10)
    error('mu not in null space of modified matrix');
end
%2. check the individual terms
B=2*(mu'*C-(mu'*C*mu)*mu')';
if (~isempty(find(abs(B-eigmod.evecs*w)>10^-10)))
    error('the linear term of our modified quadratic form doesnt appear to be correct');
end
y=eigmod.evecs'*x;
quadx=eigmod.eigd'*(y.^2)+sum(ones(n,1)*(muproj).*w,2)'*y+sum(muproj.^2.*d,2);
[quadx quadr]
return;

