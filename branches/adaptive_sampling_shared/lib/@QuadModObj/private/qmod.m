%function [eigmod, w,d,eigtime]=quadmod(eigfull,mu)
%   eigfull -eigendecomposition
%           .eigd - diagonal elements. Current eigenvalues
%                   These should be sorted in ascending order!!!!!!
%                   nx1
%           .evecs - existing eigenvectors
%           .esorted = +1 sorted in ascending order
%                         =  -1 sorted in descending order
%   mu -nxk
%           - k is the number of eigenvectors we orthogonalize to
%           vectors in direction of mean
%          - this is direction to which we want the quadratic form to be
%          othroghonal
%           -should be unit vector
%
%
% Explanation:
%       This function returns a quadratic form
%       X^T A X+B^t X +D =R^T Ct R where
%       R= X+ sum_i (-(mu(:,i)^t X)+alpha(i)) (mu(:,1))
%       Ct= evecsfull*diag(eigdfull)*evecsfull'
%       alpha(i) - a parameter which sets the projection of R in certain
%       directions
%
%       So R=X with the projections in certain dimensions forced to be
%       alpha
%       The quadratic form is returned in the eigenbasis of A
%           A=evecs*diag(eigd)*evecs'
%          W=evecs'*B
% Return value:
%       w=nxk
%       d=1xk
%       w,d are returned as matrices because the value of B, D depends on
%       alpha which we want to set
%       therefore to get w we would multiply each column by alpha and sum
%       across columns
%       for d we do the same thing but we multiply by alpha^2
%       eigtime - time for eigendecomposition
%
% 7-29-2007 - Return an EigObj instead of a structure
%
% Revision: 1495 - use EigObj for the rank one modifications
function [emod,w,d,qtime]=qmod(eigfull,mu,debug)

%keep track of time for computations other than the rank 1 computations
qtime=0;

n=size(mu,1);
nmu=size(mu,2);
%normalize mu
%make sure mu is unit vectors
mu=mu./(ones(n,1)*sum(mu.^2,1).^.5);

%if eigfull is not an eigenobject then create one
if ~isa(eigfull,'EigObj')
    eigfull=EigObj('estruct',eigfull);
end
%if eigen values and eigenvectors are sorted in descending order
%then reverse them
%if isfield(eigfull,'esorted')
%    if (eigfull.esorted==-1)
%        eigfull.eigd=eigfull.eigd(end:-1:1);
%        eigfull.evecs=eigfull.evecs(:,end:-1:1);
%    end
%end
eigtime=0;
%intialize the terms
B=zeros(n,nmu);
d=zeros(1,nmu);
eigd=eigfull.eigd;
evecs=eigfull.evecs;

for mind=1:nmu
    tic;
    %project mu onto eigenvectors
    emean=evecs'*mu(:,mind);
    %to compute rank one modiffications we need two quantities
    %mu'Cmu
    qmu=eigd'*(emean.^2);
    erotate=evecs*(eigd.*(emean));

    %compute our rank1 modification vector
    z1=(-qmu+2)/2*mu(:,mind)+erotate;
    
    %using eigobj we do not rotate pertubation into eigen space
    %since the rank one shoud be subtract it we multiply rho by -
    rho1=-1/2;       %because we subtract it
    qtime=qtime+toc;
    [eobj1,opt1]=rankOneEigUpdate(eigfull,z1,rho1);
    if ~isempty(opt1)
    qtime=opt1.eigtime+qtime;
    else
        qtime=nan;
    end
    
    %for debugging check update is correct
%      iscorrect=verifyupdate(eobj1,eigfull,z1,rho1)
%      if (iscorrect==false)
%         error('Eig update is incorrect'); 
%      end
    
    %compute our 2nd rank 1 modification vector
    z=(-qmu-2)/2*mu(:,mind)+erotate;
    rho=1/2;
    [emod,opt2]=rankOneEigUpdate(eobj1,z,rho);
    if ~isempty(opt2)
    qtime=opt2.eigtime+qtime;
    else
        qtime=nan;
    end
    
    %for debugging check update is correct
%      iscorrect=verifyupdate(emod,eobj1,z,rho)
%      if (iscorrect==false)
%         error('Eig update is incorrect'); 
%      end

    %update the linear terms
    B(:,mind)=2*(erotate-qmu*mu(:,mind));
    d(1,mind)=qmu;
    %debugging
    if (debug~=0)
        fprintf('quadmod: check rank 1 perturbations make quadratic form independent of muk \n');
        qmodmu=mu'*(getmatrix(eigfull)+rho*z*z'+rho1*z1*z1')*mu;
        fprintf('quadmod: checking eigendecomp \n');
        
        %
       
        %debug check each rank 1 update is correct
        iscorrect=verifyupdate(eobj1,eigfull,z1,rho1)
            
        if (mind==1)
            Cfull=eigfull.evecs*diag(eigfull.eigd)*eigfull.evecs';
            Clast=Cfull;
        else
            Clast=Cmod;
        end
        b=-mu(:,mind)'*Clast*mu(:,mind);
        v1=((b/2+1)*mu(:,mind)+Clast*mu(:,mind));
        v2=((b/2-1)*mu(:,mind)+Clast*mu(:,mind));
        Cmod=Clast-1/2*v1*v1'+1/2*v2*v2';
        %   [tevecs teigd]=svd(Cmod);
        if ~(isempty(find((Cmod-emod.evecs*diag(emod.eigd)*emod.evecs')>10^-8, 1)))
            error('Modified eigen decomposition is not correct');
          
        else
            fprintf('quadmod: eigendecomp is accurate \n');
        end
    end
end
tic
w=emod.evecs'*B;
qtime=qtime+toc;


if ~isempty(find(isnan(evecs), 1))
    error('Eigenvectors not defined');
end