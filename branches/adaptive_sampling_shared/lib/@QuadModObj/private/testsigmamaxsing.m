%10-14-06
%test sigmamaxsing.m

error('this script needs to be adapted to work with QuadModObj');

clear all;
setpaths
%****************************************************************
%construct a quadratic function
%such that optimium occurs
%at lambda=lammax
%this means we want the mean to be parallel to the max eigenvector
d=3;
tol=10^-10;
mu=ceil(rand(d,1)*10)/10;
mag=1; %magnitude constraint for stimulus

efull.evecs=orth(rand(d,d));
efull.eigd=ceil(rand(d,1)*10)/10;
[efull.eigd ind]=sort(efull.eigd,'ascend');
efull.evecs=efull.evecs(:,ind);
efull.esorted=1;
C=efull.evecs*diag(efull.eigd)*efull.evecs';

%remove projection from mu along max eigenvector
[emax, emind]=max(efull.eigd);
mu=mu-(mu'*efull.evecs(:,emind))*efull.evecs(:,emind);

%*******************************************************************
%construct our modified quadratic form
%****************************************************************
[quad]=quadmodshist(C,efull,mu,[]);

%verify this is a proper quad formfor test sigmamaxsing
%i.e we want w, wmuproj to be zero for maxeigenvalue
[emax]=max(quad.eigd);
emind=find(abs(quad.eigd-emax)<tol);

if ~isempty(find(abs(quad.w(emind))>tol))
    error('quadratic form is not correct for testing sigmamaxsing');
end
if ~isempty(find(abs(quad.wmuproj(emind))>tol))
    error('quadratic form is not correct for testing sigmamaxsing');
end

%*******************************************************************
%compute range of pcon which have sigmamax at lam=emax
 [pcon]=pconsingmax(quad,mag);
 
 %generate a random number in this interval
pconr=rand(1);
pconr=pconr*diff(pcon)+pcon(1);

%********************************************************
%compute sigmaxmax for pconr using
%1). sigmamaxsing
%2) maxminquad
[test.qmax,test.xmax]= sigmamaxsing(pconr, quad,mag);
%
[true.qmax,true.qmin,true.xmax,true.xmin]=maxminquad(quad,quad.mu,pconr,mag,[]);

