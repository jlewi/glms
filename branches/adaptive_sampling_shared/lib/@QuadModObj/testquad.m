%function testquad(qobj,x)
%       x - value of stimulus
%
%Explanation: computex x' Cx using the original C matrix
%   and then using the modified quadratic form. Then checks to see they are
%   equal
function testquad(qobj,x)
%
if (length(x)+length(qobj.shist) ~=size(qobj.c,1))
    error('x has incorrect length');
end

%compute the projection of x along the mean
muproj=x'*qobj.muk;

%compute the true value of the quadratic form
qtrue=[x;qobj.shist]'*qobj.c*[x;qobj.shist];



%compute the value of x using our modified form and see if it returns the
%same value
y=qobj.quad.eigmod.evecs'*x;
quad=qobj.quad;
qmod=(quad.eigmod.eigd'*(y.^2))+muproj*quad.wmuproj'*y+quad.w'*y+muproj*quad.dmuproj+muproj^2*quad.dmuproj2+quad.d;

if (abs(qtrue-qmod)/max(abs([qtrue qmod]))>eps*10^4)
    error('Modified quadratic form appears to be incorrect');
else
    fprintf('Modified quadratic form is correct \n');
end
