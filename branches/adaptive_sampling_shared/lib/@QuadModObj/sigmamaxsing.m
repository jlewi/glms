%function [sigmamax,xmax]= sigmamaxsing(pcon, qobj.quad,mag)
%   pcon - value of projection of stim on muk
%   qobj.quad    - describes our quadratic function
%       .esorted = +1   -eigenvalues sorted in ascending order
%       .esorted =  -1   -eigenvalues sorted in descending order
%       .esorted = 0      -eigenvalues not sorted
%   mag     - magnitude constraint
%
% Return value:
%       sigmax       - max value of  sigma for this lambda
%       xmax           - max input at this value of lambda
%                           -dx2
%                           -there can be two different xmax for the
%                           different values of alpha corresponding to this
%                          value of lambda
%       nsols           -number of valid solutions (values of alpha) which
%                         satisfy the magnitude constraint at this location
%                           =0,1,2
% Explanation:
%   computes the maximum of our quadratic expresion as a function of
%   pcon when lambda=emax;
%
%   This is only possible if the value of pcon is one of the values for
%   which the max occurs at emax; which means the corresponding linear terms of 
%   the quadratic function are zero
%
%
% This finds the maximum of
%so the quadratic function we want to find max and min as function of
%muproj=muk'k; is
%y'*qobj.quad.eigmod.eigd*y +
%(muproj*qobj.quad.wmuproj+qobj.quad.w)*y+(muproj^2*qobj.quad.dmuproj2+muproj*qobj.quad.muproj)
%
%    This only finds all values of alpha if z(emax) is not zero. Otherwise
%    some values of pcon occur at lam=emax
%lambda
% That is it does include the effec of the constant term
%
% Improvements:
%   1. I do a lot of searching to find eigenvalues less than max etcc. I
%   should keep the eigenvalues sorted and make this quick
%
function [qmax,xmax]= sigmamaxsing(qobj,pcon,mag)
%tolerance for deciding numbers are zero
%tol=10^-10;
tol=10^-9;
d=length(qobj.quad.eigmod.eigd);            %dimensionality of y


sigmamax=[];
xmax=[];
nsols=0;

%*********************************************************************
%Get the indexes corresponding to terms of the max eigenvalue
%*********************************************************************
[emax indmax]=maxeval(qobj.quad.eigmod);
emax=emax(1);   %we want emax to be a scalar but if max eigenvalue is repeated
                %maxeval returns an array
                
%get indexes of terms which aren't equal to max eigenvalue
if issorted(qobj.quad.eigmod,1)
    %sorted in ascending order 
    %indexes of terms which don't have max eigenvalue
    indltmax=1:(min(indmax)-1);
    
elseif issorted(qobj.quad.eigmod,-1)
    %eigenvalues are sorted in descending order
     indltmax=max(indmax)+1:d;     %indexes of terms which don't have max eigenvalue
else
    %eigenvalues are not sorted
    indltmax=ones(1,d);
    indltmax(indmax)=0;
    indltmax=find(indltmax==1);
end

%value at this point
%emax=max eigenvalue
%indmax = indexes of terms corresponding to max eigenvalue
%indltmax= indexes of terms not corresponding to max eigenvalue

%**********************************************
%2. Compute range of pcon for which solution exists at singularity
%*****************************************************
[pconrange]=pconsingmax(qobj,mag);

if isempty(pconrange)
    error('This value of pcon does not lead to sigmamax at lam=emax');
end

if (length(pconrange)==1)
    if(pcon~=pconrange)
            error('This value of pcon does not lead to sigmamax at lam=emax');
    end
else
    if ((pcon<pconrange(1)) ||(pcon>pconrange(2)))
            error('This value of pcon does not lead to sigmamax at lam=emax');
    end
end

%*****************************************************************
%3. find y(pcon)
%********************************************************************
ymax=zeros(d,1);
z=pcon*qobj.quad.wmuproj+qobj.quad.w;

%verify z(indmax) is zero
if ~isempty(find(abs(z(indmax))>tol))
    error('linear terms corresponding to max eigenvalue are not zero');
end
%*********************************************************
%special case:
%       if z is zero everywhere solution is to put all energy (mag^2-pcon^2)on max
%       eigenvector - 
%           this case corresponds to mean being parallel to one eigenvector
%           but not the max eigenvector
if isempty(find(abs(z)>tol))
   %ymax(indmax)=((mag^2-pcon^2)/length(indmax))^.5;
    ymax(indmax(1))=((mag^2-pcon^2))^.5;

else
ymax(indltmax)=-z(indltmax)./(2*(qobj.quad.eigmod.eigd(indltmax)-emax));
%all remaining energy is along max eigenvectors
%ymax(indmax)=((mag^2-pcon^2-ymax'*ymax)/length(indmax))^.5;
ymax(indmax(1))=((mag^2-pcon^2-ymax'*ymax))^.5;
end

%compute qmax
 qmax=sum((ymax.^2).*qobj.quad.eigmod.eigd+z.*ymax);
    %now we we need to add the effect of the constant term
    qmax=qmax+pcon*qobj.quad.dmuproj+pcon^2*qobj.quad.dmuproj2+qobj.quad.d;


xmax=qobj.quad.eigmod.evecs*ymax;
%we need to add in projection along muk
xmax=xmax+pcon*qobj.muk;

%**************************************************************************
%debugging
%**************************************************************************
%check xmax* muk =con
if (qobj.debug)
    if (abs(xmax'*qobj.muk -pcon)>tol)
        error('Projection of xmax on muk is not pcon');
    end
if (abs((xmax'*xmax)-mag^2)>tol)
    error('Optimal value should occur with power constraint satisified with equality');
end
end
