%function qmod=evalqmod(qobj,x)
%   qobj - the quadmod obj
%   x    - vector
%
% Explanation: evaluates the quadratic form for this x.
%   Note this computes the value of X'CX using our modified form
%   However, it takes into account the projection of x along muk.
%
function v=evalqmod(qobj,x)

%compute the projection of x along the mean
muproj=x'*qobj.muk;

y=getevecs(qobj.quad.eigmod)'*x;
quad=qobj.quad;
v=(quad.eigmod.eigd'*(y.^2))+muproj*quad.wmuproj'*y+quad.w'*y+muproj*quad.dmuproj+muproj^2*quad.dmuproj2+quad.d;
