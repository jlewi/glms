%function [pcon,qmax,xmax,nsols]= maxquadlam(qobj,v,mag)
%   qobj.quad     - the quadratic expression we want to maximize
%   v        - value at which to compute sigmamax
%            - this is lambda1 rescaled so that the interval (emax,infty)
%               is mapped to v in (0,1)
%   mag      - magnitude constraint on the stimulus
%
%             every time the quadratic object is changed. 
% Return value:
%
%       pcon           - 1x2 value of projection in direction muk for lambda
%                           -in general there can be two possible solutions
%       sigmaxmax - 1x2max value of  sigma for this lambda
%       xmax           - max input at this value of lambda
%                           -dx2
%                           -there can be two different xmax for the
%                           different values of alpha corresponding to this
%                          value of lambda
%       nsols           -number of valid solutions (values of alpha) which
%                         satisfy the magnitude constraint at this location
%                           =0,1,2
%     These variables describe the local max. of our quadratic expression
%     for the inputted values of v.
%       sigmamax - is the value of the quadratic expression
%       pcon     - is the dot product of xmax corresponding to this
%                  solution and the unit vector qobj.quad.muk 
%       
% Explanation:
%   computes the maximum of our quadratic expresion as a function of
%   lambda
%
% This finds the maximum of
%so the quadratic function we want to find max and min as function of
%muproj=muk'k; is
%y'*quad.eigmod.eigd*y +
%(muproj*quad.wmuproj+quad.w)*y+(muproj^2*quad.dmuproj2+muproj*quad.muproj)
%
%    This only finds all values of alpha if z(emax) is not zero. Otherwise
%    some values of pcon occur at lam=emax
%lambda
% That is it does include the effec of the constant term
%
% Improvements:
%   1. I do a lot of searching to find eigenvalues less than max etcc. I
%   should keep the eigenvalues sorted and make this quick
%
% Revision History:
%   12-20-2007 
%       use compiled code 
%       no longer need or compute iq
%
%Caveats:
%   Currently code assumes no spike history but this should be easy to
%   genearlize
function [pcon,qmax,xmax,nsols]= maxquadlam(qobj,v,mag)

if (mag<=0)
    error('mag must be >=0');
end
%init return values;
pcon=[];
sigmamax=[];
xmax=[];
nsols=0;
eigmod=qobj.quad.eigmod;
eigd=geteigd(eigmod);
evecs=getevecs(eigmod);


%*********************************************************************
%Get the indexes corresponding to terms of the max eigenvalue
%*********************************************************************
%if elements are sorted we can get these indexes efficiently
[emax indmax]=maxeval(eigmod);
emax=emax(1);   %we want emax to be a scalar but if max eigenvalue is repeated
                %maxeval returns an array
                

muprojr=0;

%terms in the quadratic eqn we need to solve
ediff=eigd-emax;
ediff(indmax)=0;

%we mexmaxquadlam uses the number of output arguments to determine
%whether or not to compute xmax. computing xmax is expensive so we want to
%avoid it if poosible
if (nargout<3)
[pcon,qmax]=mexmaxquadlam(v,eigd,ediff,qobj.quad.wmuproj,qobj.quad.w,qobj.quad.dmuproj2,qobj.quad.dmuproj,qobj.quad.d,mag,evecs,qobj.muk);
else
[pcon,qmax,xmax]=mexmaxquadlam(v,eigd,ediff,qobj.quad.wmuproj,qobj.quad.w,qobj.quad.dmuproj2,qobj.quad.dmuproj,qobj.quad.d,mag,evecs,qobj.muk);
end

%**************************************************
%maxquadlamuncompiled
%   this code is the same as mexmaxquadlam 
function [pcon,qmax,xmax]=maxquadlamuncompiled(qobj,v,mag,iq)

%init return values;
pcon=[];
sigmamax=[];
xmax=[];
nsols=0;
%***************************************************************
%if we did not pass in iq then we need to compute it
if isempty(iq)
%to speedup acess to the eigenvalues and eigenvectors
%we copy them. This avoids having to go through subsref which is expensive
eigmod=qobj.quad.eigmod;
iq.eigd=geteigd(eigmod);
iq.evecs=getevecs(eigmod);

iq.mag=mag;
iq.d=length(iq.eigd);            %dimensionality of y

%*********************************************************************
%Get the indexes corresponding to terms of the max eigenvalue
%*********************************************************************
%if elements are sorted we can get these indexes efficiently
[emax indmax]=maxeval(eigmod);
emax=emax(1);   %we want emax to be a scalar but if max eigenvalue is repeated
                %maxeval returns an array
                
%get indexes of terms which aren't equal to max eigenvalue
if issorted(eigmod,1)
    %sorted in ascending order 
    %indexes of terms which don't have max eigenvalue
    indltmax=1:(min(indmax)-1);
    
elseif issorted(eigmod,-1)
    %eigenvalues are sorted in descending order
     indltmax=max(indmax)+1:d;     %indexes of terms which don't have max eigenvalue
else
    %eigenvalues are not sorted
    indltmax=ones(1,d);
    indltmax(indmax)=0;
    indltmax=find(indltmax==1);
end

%value at this point
%emax=max eigenvalue
%indmax = indexes of terms corresponding to max eigenvalue
%indltmax= indexes of terms not corresponding to max eigenvalue


%terms in the quadratic eqn we need to solve
iq.ediff=iq.eigd-emax;
iq.ediff(indmax)=0;

iq.wmuprojsq=qobj.quad.wmuproj.^2;
iq.wsq=qobj.quad.w.^2;
iq.prodwmus=.5*qobj.quad.wmuproj.*qobj.quad.w;

end

%**************************
%error checking
%**************************
if (v<=0 || v>=1)
    warning('v should be in open interval (0,1)');
end



%**********************************************
%2. Compute pcon(lambda)
%*****************************************************
%i) we compute y_i(\lambda) (the stimulus expressed in the eigenspace)
%   by solving the first order k.k.t conditions
%   y_i=- w_i(\alpha)/ 2(\eigd[i]-\lam)
%   w_i(\alpha)=\alpha qobj.quad.wmuproj+ qobj.quad.w
%
%ii) Since the local maximum of the quadratic function always occur
%    with the power constraint satisified with equality. we find alpha
%    by setting the power of y_i = to the maximum allowed power.
%   This gives us a quadratic equation which we solve for alpha
%
% \alpha(\lam)= dot product of stimulus and unit vector qobj.quad.muk
%
%solve quadratic equation to find possible values of alpha(lambda)
%solve: a pcon^2+b pcon + c =0
%lam1=emax+v/1-v
%we use the rescaling (ci-lam)=(ci-emax)-v/1-v
%to deal with numerical issues caused by subtracting small numbers from 
%large numbers

denom=iq.ediff-v/(1-v);
denomsq=denom.^2;

a=1+sum(iq.wmuprojsq./(4*denomsq));
b=sum(iq.prodwmus./denomsq);
c=sum(iq.wsq./(4*denomsq))-mag^2;

%check if there exist real solutions for pcon
if (b^2-4*a*c>0)
    %at most 2 real solutions
    %compute the solutions
    nsols=0;
    pcon=[];
    
    %1st solution
    qs=(-b - (b^2-4*a*c)^.5)/(2*a);
    %make sure its valid
    if (abs(qs) <=mag)
        nsols=nsols+1;
        pcon(nsols)=qs;
    end
    %2nd solution
    qs=(-b+ (b^2-4*a*c)^.5)/(2*a);
    
    %make sure its valid
    if (abs(qs) <=mag)
        nsols=nsols+1;
        pcon(nsols)=qs;
    end
elseif  (b^2-4*a*c==0)
     nsols=0;
    %most 1 solution
    
    %1st solution
    qs=(-b /(2*a));
    %make sure its valid
    if (abs(qs) <=mag)
        nsols=nsols+1;
        pcon(nsols)=qs;
    end
else    
    %no real solutions exist to the quadratic eqn for pcon
    nsols=0;
end

%**********************************************************
%3. compute xmax,qmax for every valid solution
if (nsols>0)
    if (nargout>2)
xmax=zeros(iq.d,nsols);
    end
qmax=zeros(1,nsols);
else
    qmax=[];
    xmax=[];
end

for si=1:nsols
    %compute z from pcon
    z=pcon(si)*qobj.quad.wmuproj+qobj.quad.w;
    
    %compute ymax
    %ymax=ykkt(v,z,iq.ediff);
    ymax=-z./(2*(denom));
    %compute qmax
    qmax(si)=sum((ymax.^2).*iq.eigd+z.*ymax);

    %now we we need to add the effect of the constant term
    qmax(si)=qmax(si)+pcon(si)*qobj.quad.dmuproj+pcon(si)^2*qobj.quad.dmuproj2+qobj.quad.d;

    %compute xmax
    %we need to add in projection along muk
    if (nargout>2)
        xmax(:,si)=iq.evecs*ymax+pcon(si)*qobj.muk;
    end
end

%**************************************************************************
%debugging
%**************************************************************************
%check xmax* muk =con
if (qobj.debug)
for si=1:nsols
    if (abs(xmax(:,si)'*qobj.muk -pcon(si))/abs(pcon(si))>(10^6*eps))
        error('Projection of xmax on muk is not pcon');
    end
end
end


%set y from the kkt conditions
%       lambda - lambda
%       z       -z
%       ediff - eigd-emax
%       indmax - indexes of eigenvalues corresponding to max eigenvalue
%
function y=ykkt(v,z,ediff)
%we use the rescaling (ci-lam)=(ci-emax)+v/1-v
%to deal with numerical issues caused by subtracting small numbers from 
%large numbers

%for all eigenvalues numerically equal to the max eigenvalue we force the
%difference with the max eigenvector to be zero.

%I assume ediff(indmax)=0
%ediff(indmax)=0;
denom=ediff-v/(1-v);
%y=zeros(length(ediff),1);
y=-z./(2*(denom));

