%function maxminquad(qobj,muproj,mag,optparam)
%   qobj    - the QuadModObj
%   muproj - 1xk - projection of the stimulus along the unit vecto qobj.muk
%   mag   - magnitude constraint
%   optparam - optional structure giving optimset structure to be used by
%           fzero
% Return value:
%       xmax, xmin - the inputs which generate the max and minimum values
%                   and satisfy the projection constraints
%                   i.e mu'*xmax=alpha'
%                        mu'*xmin=alpha'
%       qmax - max value of our objective function
%       qmin  - minimum value of our objective function
%       lamax - value of the lagrange multiplier associated with xmax,qmax
%       lamin - value of lagrange multiplier associated with xmin, qmin
% Explanation:
%   computes the maximum and minimum values
%   of our quadratic form
%
% This finds the maximum of
%so the quadratic function we want to find max and min as function of
%muproj=muk'k; is
%y'*quad.eigd*y +
%(muproj*quad.wmuproj+quad.w)*y+(muproj^2*quad.dmuproj2+muproj*quad.muproj)
%
%
% That is it does include the effec of the constant term
%
% Improvements:
%   1. I do a lot of searching to find eigenvalues less than max etcc. I
%   should keep the eigenvalues sorted and make this quick
function [qmax,qmin,xmax,xmin, lammax,lammin]=maxminquad(qobj,muproj,mag,optim)
d=size(qobj.muk,1);
%tolerance for deciding numbers are zero
tol=10^-12;

powereps=.001;                           %small number to ensure when we compute the
%upper and lower bounds
%for lambda, the sign
%really changes
%make sure mu is a unit vector
%mu=mu/(mu'*mu)^.5;
mu=qobj.muk;
quad=qobj.quad;

%check if muproj is greater than mag
if (abs(muproj)>mag)
    error('Projection along the mean cant be greater than the magnitude constraint');
end
%**********************************************
%1. Get the eigendecomposition of our matrix
%Warning: we want to modified eigendecomposition
%Not the eigendecomposition of our covariance matrix
%*********************************************
%its already provided
emod=quad.eigmod;
%**********************************************
%2. Compute the linear term of our objective function
%*****************************************************
z=(muproj*quad.wmuproj+quad.w);

%set all entries less than tolerance to zero
%11-27-2007
%changed it to abs(z)^2/m^2 < tol
%I think this is a better check 
%ind=find(abs(z)<tol);
ind=find(z.^2/mag^2<tol);
z(ind)=0;
%get the indexes of all zelements which are nonzero
zindnz=find(abs(z)>0);


%power constraint on terms orthogonal to mu
powercon=mag^2-muproj^2;


tol=10^-13;
if (powercon > 0)



    %******************************************************
    %2-14-2007
    %*******************************************************
    %still having numerical problems with properly determining
    %the mutliplicty of the maxeigenvalue
    %therefore determine the maxeigenvalue and its multiplicty
    %do this once and use all values for all if statements
    %emax.eval - value of max eigenvalue
    %emax.maxind - indexes of the max eigenvalue

    %the max function was not finding all values which should be
    %considered equal. so I modified the code
    [emax.eval, emax.maxind]=maxeval(emod);
    %we want emax.eval to be a scalar, even if multiplicity is > 1
    emax.eval=emax.eval(1);


    if  (isempty(zindnz))
        %***********************************************************
        %special case linear term is zero.
        %Solution in this case is to put all energy in eigenspace of max
        %eigenvalue.
        %
        %Fo possible solution and we begin our search
        %therer multiplicities > 1 we can put energy along any of the max
        %eigenvectors because linear term is zero for all of them

        %*******************************************************
        %1-30-2007
        %******************************************************
        %since the linear term is zero
        %we can choose arbitrary sign for the projection along the
        %max eigenvectors
        %I don't think it matters that we don't split it along all
        %eigenvectors if max eigenvector has multiplicity greater than 1
        %because we would just end up scanning through them. on successive
        %iterations.
        %2-14-07
        %for multiplicities greater than 1 divide energy among max
        %eigenvectors
        powerpevec=((powercon)/length(emax.maxind))^.5;
        %randomly choose the sign for each eigenvector;
        xs=sign(binornd(1,.5,length(emax.maxind),1)-.5);
        ymax=zeros(d,1);
        ymax(emax.maxind)=(xs.*powerpevec);
        xmax=emod.evecs(:,emax.maxind)*(xs.*powerpevec);
        qmax=emax.eval*powercon;
        qmin=0;
        xmin=zeros(d,1);
        lammax=[];
    else
        %**************************************************************
        %Find lamda: Could be 1 or 2 solutions
        %i) lambda > max eigenvalue
        %ii) lambda = max eigenvalue

        %indexes of elements which are not the maximum eigenvalue
        %this is used by both solutions
        indnmax=ones(d,1);
        indnmax(emax.maxind)=0;
        indnmax=find(indnmax==1);

        %******************************************************************
        %Find solution lambda>emax
        %*****************************************************************
        mnonsing=[];

        %***************************************************
        %normal case just do search over lambda>eigmax
        %evaluate upper and lower bounds for lambda
        %we start our search by the largest eigenvalue orthogonal to the mu vectors

        %define the implicit function used by fzero
        %1-22-07
        %changed yind to 2:d because we want to force the component
        %with eigenvector parallel to muk to be zero
        %the corresponding eigenvalue should be zero so this should
        %have no effect
        %yind=1:d;   %b\c don't want to force any entries to zero
        yind=2:d;   %force component parallel to muk to be zero
        fpowerdiff=@(lam)(poweryi(lam,emod.eigd,z,d,yind)-powercon);

        %we need to define lower bound for search
        %There are two cases
        % i) z(emax.maxind)~=0 for some i in emax.maxind
        %    Therefore, the power will approach infinity as we
        %   lam ->emax.eval. To compute a lower bound, we find the
        %   value of lamda such that the power in the eigenspace of the max
        %   eigenvalue equals the maximum allowed power. This guarantees
        %   that any values of lam smaller than this would violate the
        %   power constraint.
        % ii) z(emax.axind)==0 for all i
        %     In this case a solution exist for lam>emax because if
        %  we set y using the first order k.k.t with lam=emax then
        %  the resulting y violates the power constraint. In this case
        %  The power at lam=emax is finite but violates the power
        %  constraint.
        %   We use a similar rule to i to find the lower boundary. Only
        %   instead of using the max eigenvalue we find the largest
        %   eigenvalue for which z is not equal to zero.
        %
        %  deterine the index and eigenvalue to use for lower bound on lam
        if any(abs(z(emax.maxind))>(eps*10^4))
            %just use max eigenvalue
            eforlb=emax;
        else
            %find largest eigenvalue for which z is not equal to zero
            %sort the indexes of the eigenvalues not = to the max
            %eigenvalue in descending order
            if issorted(qobj.quad.eigmod,'1')
                %eigenvalues are in ascending order
                %so reverse order of indnmax
                indnmax=indnmax(end:-1:1);
            elseif issorted(qobj.quad.eigmod,'-1')
                %eigenvalues are in ascending order
                %so do nothing
            else
                %eigenvalues aren't sorted
                enmax=qobj.quad.eigmod(indnmax);
                [enmax sind]=sort(enmax,'descend');
                indnmax=indnmax(sind);
            end
            %find the largest eigenvalue for which z is not zero
            j=1;
            %we need to use the same tolerance as used for check that not
            %all z are zero
            while (abs(z(indnmax(j)))<tol)
                j=j+1;            
            end
            eforlb.maxind=indnmax(j);
            eforlb.eval=qobj.quad.eigmod.eigd(indnmax(j));
        end %ifelse for deterining eforlb

        lblam=eforlb.eval+max(abs(z(eforlb.maxind)))/(2*powercon^.5);
        %verify sign of lblam
        %because of rounding error we sometimes need to decrease it
        if (sign(fpowerdiff(lblam))<0)
            lblam=eforlb.eval+.95*abs(z(eforlb.maxind))/(2*powercon^.5);
        end


        %for upper bound find max of z
        ublam=emax.eval+d^.5*max(abs(z))/(2*powercon^.5);



        [mnonsing.lammax,fval,exitflag,output] = fzero(fpowerdiff,[lblam ublam],optim);%tolerance for deciding numbers are zero

        if (exitflag~=1)
            error('fzero did not find solution');
        end

        %************************************************************
        %Solutions at the singularity
        %***********************************************************
        % Are there solutions at the singularity: lambda=emax?
        % We check for solutions by setting lambda=emax.
        % Compute the power. If the magnitude constraint is violated then this is a possible solution.

        %12-23-2007
        %   my old code for checking if there are solutions at the
        %   singularity was having problems. Therefore, use pconsingmax to
        %   check if there is a solution at the singularity.        
        msing=[];  %describes solution at singularity.
        
        %indicates solution at the singularity
        prange=pconsingmax(qobj,mag);
        solatsing=false;
        if (length(prange)==2)
            if ((muproj)>prange(1) && muproj<prange(2))
                solatsing=true;
            end
        else
            if (muproj==prange);
                solatsing=true;
            end
        end
        if (solatsing)

            %lambda1 =Cmax
            %   this is only valid if its possible to satisfy the magnitude
            %   constraint. Determine components along all but max
            %   eigenvector by solving k.k.t.
            % remaining energy goes in max eigenspace.


            msing.lammax=emax.eval;
            msing.ymax=zeros(d,1);

            %all terms other than those corresponding to the max
            %eigenvalue are determined from solving 1st order
            %k.k.t
            %ymax(ind)=-z(ind)./(2*(emod.eigd(ind)-emax));
            msing.ymax=ykkt(emax.eval,z,geteigd(qobj.quad.eigmod),indnmax);
            %remaining terms corresponding eigenvetor get whatever
            %energy we have left
            msing.ymax(emax.maxind)=((powercon-msing.ymax'*msing.ymax)/length(emax.maxind))^.5;
        end %end check for solution at the singularity
        %**************************************************
        %if we have a solution at the singularity and not the
        %singularity then we need to determine which is correct
        if (~isempty(msing) && ~isempty(mnonsing))
            mnonsing.ymax=-z./(2*(qobj.quad.eigmod.eigd-mnonsing.lammax));
            mnonsing.qmax=sum((mnonsing.ymax.^2).*qobj.quad.eigmod.eigd+z.*mnonsing.ymax);

            msing.qmax=sum((msing.ymax.^2).*qobj.quad.eigmod.eigd+z.*msing.ymax);

            if (msing.qmax>mnonsing.qmax)
                %singularity is better
                ymax=msing.ymax;
                lammax=msing.lammax;
            else
                ymax=mnonsing.ymax;
                lammax=mnonsing.lammax;
            end
        elseif (~isempty(mnonsing))
            %no solution at the singularity
            lammax=mnonsing.lammax;
            ymax=-z./(2*(emod.eigd-mnonsing.lammax));
        else
            %solution is at the singularity
            lammax=msing.lammax;
            ymax=-z./(2*(emod.eigd-msing.lammax));
        end
    end %ifelse for special case of finding max when z(emax.ind)==0
    %**********************************************************
    %find min value
    %***********************************************************
    %min occurs at lam=0
    %When computing the minimum we only want to consider elements of
    %   y for which z is nonzero
    %   if z(i) zero set y(i) to zero.
    %unless)
    %   i) it violates the magnitude constraint
    %   ii) one of the eigenvalues is zero
    %       this can happen because we compute the eigenvectors of
    %       a matrix which is not full rank so some of the eigenvalues
    %       may be zero
    %Therefore we compute an upper bound for lammin
    %
    %start by getting the elements of y which should not be forced to
    %zero (i.e z(i) not= to zero
    yminind=find(abs(z)>tol);


    if isempty(yminind)
        %*********************************************************
        %special case: z==0 for all i
        %***********************************************************
        %z==0 means the mean is parallel to an eigenvector
        %(this most likely occurs because our eigenvalues are all the same).
        %In this case we set ymin=0. The minimum coressponds to muproj*muk
        %where muk is a unit vector in the direction of the min.
        ymin=zeros(d,1);
    else

        %the component parallel to muk should have an eigenvalue of zero
        %so adjusting that component of y should have no effect
        %therefore we should force that component to be zero
        %so yminind should not contain 1
        if (yminind(1)==1)
            yminind=yminind(2:end);
        end
        ublam=0;
        scase=[];
        ezeroind=find(abs(emod.eigd(yminind))<tol);
        %if any of the termz with nonzero z(i) have emod.eigd(i)=0
        %than lam1=0 is not a valid solution
        %otherwise lam1=0 is a possible solution and we begin our search
        %there
        if  ~isempty(ezeroind)
            %some of the terms with nonzero z(i)
            %have a zero eigenvalue. Therefore, lam=0  cannot solve the
            %kkt conditions.

            %find an upper bound for lam1 to begin our search.
            %find the max value of z for the eigenvalues which are zero
            [zmaxzero zind]=max(abs(z(ezeroind)));

            %find largest value of lam1 for which we know magnitude
            %constraint is violated.
            ublam=-zmaxzero/(2*powercon^.5);
           

        end
        %compute the upper bound of lambda for our search
        %only
        %           zmin=min(

        if (poweryi(ublam,emod.eigd,z,d,yminind)>powercon)
            %we need to find lam closest to zero that doesn't violate magnitude
            %constraint
            %upperbound is ublam
            %we need to check power is defined (not inf) for ublam
           
            %lowerbound
            %lblam=min(emod.eigd(eorthmuind))-d^.5*max(abs(z))/(2*powercon^.5);
            lblam=min(emod.eigd)-d^.5*max(abs(z))/(2*powercon^.5);
            %define the implicit function used by fzero
            optim=optimset('tolfun',10^-10);
            eigd=geteigd(emod);
            fpowerdiff=@(lam)(poweryi(lam,eigd,z,d,yminind)-powercon);
            lammin = fzero(fpowerdiff,[lblam ublam],optim);
        else
            lammin=ublam;
        end


        %***********************************************************************
        ymin=zeros(d,1);
        ymin(yminind)=-z(yminind)./(2*(emod.eigd(yminind)-lammin));
    end %ifelse finding ymin when z==0
    xmax=emod.evecs*ymax;
    xmin=emod.evecs*ymin;


    qmax=sum((ymax.^2).*emod.eigd+z.*ymax);
    qmin=sum((ymin.^2).*emod.eigd+z.*ymin);

else
    %no power remains so we are just equal to the mu vectors
    %so xmax,xmin=0 because
    %xmax=sum(ones(d,1)*alpha.*mu,2);
    %xmin=xmax;
    xmax=zeros(d,1);
    xmin=zeros(d,1);
    %powercon=0
    %linear term drops out because no energy is in the space orthogonal
    %to mu
    %qmax=xmax'*Cfull*xmax;
    %qmin=xmin'*Cfull*xmin;
    qmax=0;
    qmin=0;
end

%************************************************************************
%debugging
%************************************************************************
%10-03-06
%Lets check to make sure the xmax and xmin have zero projections along the
%mean. If it has nonzero projection I should check to make sure everything is working
%according to my solution xmax should always have zero projections along
%mu
%xmin might have some projection along mu but we should be able to remove
%it without altering our objective function so far
if (qobj.debug)
    if ~isempty(find(abs(mu'*xmax)>10^-10))
        warning(sprintf('xmax has nonzero projection along mu vectors %d \n',mu'*xmax));
    end
    if ~isempty(find(abs(mu'*xmin)>10^-10))
        warning(sprintf('xmin has nonzero projection along mu vectors %d \n',mu'*xmin));
    end
end


%now we we need to add the effect of the constant term
v=mu*muproj;
vqterm=muproj*quad.dmuproj+muproj^2*quad.dmuproj2+quad.d;

qmax=vqterm+qmax;
qmin=vqterm+qmin;

%we need to add the appropriate components along mu to x
xmax=xmax+v;
xmin=xmin+v;


%set y from the kkt conditions
%       lambda - lambda
%       z       -z
%       emod.eigd - eigenvalues
%       yind - indexes of elements which get set based on first order
%       k.k.td
%                   other elements are zero
function y=ykkt(lambda,z,eigd,yind)
y=zeros(length(eigd),1);
y(yind)=-z(yind)./(2*(eigd(yind)-lambda));

%******************************************************
%evaluate the magnitude of yi given lambda
%   d- dimensionality
%   yind - elements of y which should not be forced to zero
function power=poweryi(lam,eigd,z,d,yind)
y=zeros(d,1);
y(yind)=-z(yind)./(2*(eigd(yind)-lam));
power=y'*y;