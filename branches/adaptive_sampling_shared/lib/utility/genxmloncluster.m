%function genxmloncluster(fname)
%	  fname - name of the simulation file
%                 relative to src.rdir
%         src   - describe the src host (i.e local host)
%               .rdir - path on src host to which path is relative
%         dest  - describe destination host
%               .rdir - path on dest host to which path is relative
%Explanation: this script is intended to run on the cluster
% It creates an xml description of a simulation file and then rsyncs it 
% back to bayes
function genxmloncluster(fname,src,dest)

  %create the xml file
  fxml=createxmldescr(fullfile(src.rdir,fname));
  
  %get the relative path by striping rdir
  if (strcmp(fxml(1:length(src.rdir)),src.rdir)==1)
    rpath=fxml(length(src.rdir)+1:end);
  else
    error('path of xml file does not starter with src.rdir');
  end

  
  %rsync it back to bayes  
    src.fname=fullfile(src.rdir,rpath);


    dest.fname=fullfile(dest.rdir,rpath);
    rsyncfile(src,dest);