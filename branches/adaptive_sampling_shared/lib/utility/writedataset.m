%function writedataset(fname,dsets,info)
%   fname - file to write dataset to
%   dsets  - array of datasets
%       .fname
%       .lbl - optional
%       .simvar
%   info  - field structure array
%         - written as comments in header of file
%
%Explanation: This creates a matlab script which can be executed
%   to create an array which describes a data set.
function writedataset(fname,dsets,info)

error('080605. This file is outdated. Compare to writeminit which is specific for adatpive sampling experiments. \n');
if isa(fname,'FilePath')
    fname=getpath(fname);
end
if exist(fname,'file')
    error('File already exists');
end

fid=fopen(fname,'w');

fprintf(fid,'%%********************************\n');
fprintf(fid,'%%Data set info\n');
fprintf(fid,'%%********************************\n');
fnames=fieldnames(info);
for ind=1:length(fnames)
    if ~isstr(info.(fnames{ind}))
        data=num2str(info.(fnames{ind}));
    else
        data=info.(fnames{ind})
    end
   fprintf(fid,'%% %s=%s \n',fnames{ind},data);
end


fprintf(fid,'\n');
fprintf(fid,'\n');

%globals
fprintf(fid,'dsets=[]; \n');
fprintf(fid,'niter=0; \n');

%loop through the data set
for ind=1:length(dsets)
    fprintf(fid,'dind=%d;\n',ind);
    fprintf(fid,'dsets(dind).fname=FilePath(''bcmd'',''%s'',''rpath'',''%s'');\n',getbcmd(dsets(ind).fname),getrpath(dsets(ind).fname));
    fprintf(fid,'dsets(dind).simvar=''%s'';\n',dsets(ind).simvar);
    if isfield(dsets,'lbl')
           fprintf(fid,'dsets(dind).lbl=''%s'';\n',dsets(ind).lbl); 
    end
    fprintf(fid,'dsets(dind).niter=niter;\n');
    fprintf(fid,'\n\n');
end

fclose(fid);