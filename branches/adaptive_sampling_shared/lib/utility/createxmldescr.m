%function xmlfileinfo(fname)
%   fname - file with stored simulations
%
%explanation: create an xml file with same name as fname
%   which describes contents of this file
%
%Revision:
%   10-30-2007 - rewritten to use objinfo
function [outfile]=xmlfileinfo(fname)

params=[];
%process optional arguments
% if exist('varargin','var');
% params=parseinputs(varargin);
% end

[pathstr,name] = fileparts(fname);

% if isfield(params,'outdir')
%      outfile=fullfile(params.outdir,[name '.xml']);
% else
    outfile=fullfile(pathstr,[name '.xml']);
%end

%get the names of the simulation variables in the file
vinfile=whos('-file',fname);

tinfo={};
%for each sumulation in the file
for vind=1:length(vinfile)
    issim=false;
   switch vinfile(vind).class
       case 'SimulationBase'
           issim=true;
       case 'SimBatch'
           issim=true;
       case 'SimPoolBased'
           issim=true;           
   end
   if (issim)
      simobj=SimulationBase('fname',fname,'simvar',vinfile(vind).name);
  

       tinfo=[tinfo,{'File',fname;vinfile(vind).name,objinfo(simobj)}];   
   end
end
onenotetable(tinfo,outfile);
