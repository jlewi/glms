%function batchll(y,x,k,func,dt)
%      func - pointer to function which computes 
%             the log likelihood of single events under the different models
%             function should return an m x n array 
%               m - different observations
%               n - different models
%               -each row gives likelihood of that observation under the
%                different models
%      y    -observations - equals a spike train.
%           should be single number
%           mxt
%           -m different observations
%           -t length of each spike train
%               or =1 if y is the number of spikes
%      x - value of the input
%           mxd
%           -m different stimuli - for m different trials
%           -d length of trial
%      k - the filter constants
%           - is d x n
%          -  each column is a different model under which to compute
%             the likelihood
%      args - cell array of additional parameters needed by the 
%             likelihood function
% Return Value:
%       ll =log likelihood of the data
%           1 x n
%           n - different models under which likelihood is computed 
% Explanation:
%   This computes the likleihood of multiple independendt observations
%   by summing the likelihoods of the individual events
function ll=batchll(y,x,k,func,args)
    if ~iscell(args)
        args={args};
    end
    newargs=[{y} {x} {k} args];
    ll=callfunc(func,newargs);
    
    ll=sum(ll,1);