%function oinfo=objinfo(obj)
%   obj - object
%
%oinfo - 2d cell array which describes this object
%
%Explanation: idea is to populate a cell array with information about this
%object. We can then pass this cell array to oneNoteTable to create a table
%which can be imported into matlab.
%
%$Revision: 1468 $ - use structinfo
function oinfo=objinfo(obj)

oinfo={'Class',class(obj)};

s=struct(obj);

%%remove fields we don't want to grab info for
fdell={'mname'};
for f=fdell
try
s=rmfield(s,f);
catch
    warning('%s.objinfo: %s \n',class(obj),lasterr);
end
end

%remove from optim any empty fields
fnames=fieldnames(s.optim);
for j=1:length(fnames)
    if isempty(s.optim.(fnames{j}))
        s.optim=rmfield(s.optim,fnames{j});
    end
end
oinfo=[oinfo;structinfo(s)];


    
