%function entropy=compentropy(post)
%   post - posterior
%
%Explanation: computes entropy of the posterior by calling getefull
%   to do something differently override it in a subclass of the updater
%
function entropy=compentropy(uobj,post)


efull=getefull(post);

if ~isempty(efull)
    dim=getdim(post);
   entropy=1/2*(dim*(log2(2*pi*exp(1))))+1/2*sum(log2(geteigd(efull)));
else
    entropy=nan;
end
       

