%function uobj=PostUpdatersBase(varargin)
%
%Explanation: base class the posterior updates
%
%intended as an abstract class.
%
%Revisions:
%   01-08-2009 - define a structure version to store the version numbers
%       make optim settable.
%
%   07-31-2008:
%       Switch to Matlab's new OOP
%       Rename field version to bversion
%11-03-2008
%   make the object a child of handle because some updaters may want to
%   store values internally.
%11-18-2007
%   changed the default value of the TolFun to 10^-12 b\c 10^-16 was
%   causing too much problems
%$Revision$ - added optim parameter which is parameter structure used for
%   numerical optimization
%   added version
classdef (ConstructOnLoad=true) PostUpdatersBase < handle
    %mname - string identifying the update method
    %                  will be set by the derived objects
    %
    %compeig - indicates whether the update method should compute the
    %           eigen decomposition of the covariance matrix
    %           default value: yes
    %
    %optim - - default optimization structure
    %debug   - turn on exra debugging error checking

    properties(SetAccess=protected,GetAccess=public)

        mname='base';
        bversion=090108;
        version=struct();


    end
    properties(SetAccess=public,GetAccess=public)
        debug=0;    
        
        compeig=1;
         optim=optimset('Jacobian','on','Display','off','TolX',10^-10,'TolFun',10^-12,'MaxIter',10000000);
    end
    methods
        function uobj=PostUpdatersBase(varargin)
            con.rparams={};
            con.cfun=1;


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
                    return



                otherwise
                    [cind,params]=constructid(varargin,con);
            end
            if ~isempty(params)

                fnames=fieldnames(params);
                for j=1:length(fnames);
                    switch fnames{j}
                        case 'compeig'
                            uobj.compeig=params.compeig;
                        case 'optim'
                            uobj.optim=params.optim;
                            %force the jacobian to be on
                            %all other options copied from the structure passed in
                            %fprintf('PostUpdatersBase: modifying optim structure. Force Jacobian to be on \n');
                            uobj.optim=optimset(uobj.optim,'Jacobian','on');
                        case 'debug'
                            uobj.debug=true;
                        otherwise
                            error('PostUbdatersBase: unrecognized parameter %s', fnames{j});
                    end
                end
            end
            uobj.version.PostUpdatersBase=090108;
        end
    end
end

