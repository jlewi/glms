%function getdebug=debug(obj)
%	 obj=PostUpdatersBase object
% 
%Return value: 
%	 debug=obj.debug 
%
function debug=getdebug(obj)
	 debug=obj.debug;
