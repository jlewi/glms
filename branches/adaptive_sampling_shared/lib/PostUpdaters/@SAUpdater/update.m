%function [post, uinfo,einfo]=update(uobj,postlast,stim,mparam,obsrv,einfolast)
%       postlast - the most poster after the previous observation
%       stim - full stimulus - spike history and stimulus part
%       obsrv- observation
%
%     einfolast -   if supplied we can compute the eigendecomposition as a
%     rank 1 update
% Return value
%   post - the new posterior
%   uinfo - information about the update
%       .eigtime - time for the rank 1 updat of the eigendecomposition
%       .ndeflate - number of deflated eigenvalues if we computed the
%       eigendecomposition.
%   einfo - eigendecomposition of the new covaraince matrix
%                -this is just a rank one update and can therefore be
%                computed efficiently
%
%   WARNING: post.c and einfo will not always match
%       This is because post.c is computed using sherman-morrison formula
%       einfo is computed if possible using Gu and Eisenstat
%       THis results in slight numerical differences between the post.c and
%       the covariance matrix constructed from the eigendecomposition. Over
%       many iterations these differences accumulate.
%
% 7-23-2007:
%   Partial fix of numerical issue due to using Sherman-Morrison Formula
%   and the Gu-Eisenstat formula
% Revision: 1485 : save eigtime as part of einfo.
%           use EigObj -to handle the rank 1 update
%
% Revision: 1485 : 06-25-2007
%       Use the optim structure stored in the base class
%03-18-2007
%       1. GLM is no longer passed in as input
%       2. Test if mparam is derived from MGLMNonLinInp
%                   if so transform the input
%
function [post,uinfo,einfo]=update(uobj,postlast,stim,mparam,obsrv,einfolast,iter)
glm=mparam.glm;
einfo=[];

%for efficiency only get the eigenvectors and eigenvalues of einfolast once
if ~isempty(einfolast)
evecs=getevecs(einfolast);
eigd=geteigd(einfolast);
end

%observation is just the latest observation
%check if we need to nonlinearly transform the input
if isa(mparam,'MGLMNonLinInp')
    stim=projinp(mparam,stim);
end

% The mean of our posterior is the peak of the true
% posterior
% therefore its a root of the derivative of the log of the posterior
% we use newton's method to compute it
% The new covariance matrix is the inverse of the - hessian at the
% new ut
% The seed for newton's methods is the mean from the previous
% iteration
uinfo=[];
uinfo.niter=[];
uinfo.ntime=[];

%we take a random step
% finv=getfinv(mparam.glm);
% fo=finv(obsrv);
% if (isinf(fo)&& (fo)<0)
%     warning('Inverse of the nonlinearity is not defined.');
%     %we need to invert the exponential function when r=0.
%     %Since log 0 is undefined we set it to the maximum value of the
%     %stimulus times that estiamte of the mean
%     mmag=getmag(mparam);
% %    fo=-1*mmag*(postlast.m'*postlast.m)^.5;
%     fo=-1*mmag^2;
% end


dstep=(obsrv-compexpr(mparam,stim,postlast.m));
%dstep=(obsrv/compexpr(mparam,stim,postlast.m)-1)*compexpr(mparam,stim,postlast.m);
%if isinf(compexpr(mparam,stim,postlast.m))
%   warning('mean firing rate is infinite'); 
%end
%to prevent instabilities
%need to  limit the step size because conditions of global convergence
%require it
%dstep=sign(dstep)*min(abs(dstep),iter^2);
%dstep=sign(dstep)*min(abs(dstep),1);

%dr=obsrv-compexpr(mparam,stim,postlast.m);
%tau=100;
%delta=exp(dr/tau)/(1+exp(dr/tau))-.5;

post.m=postlast.m+postlast.c*getData(stim)*dstep;
%post.m=postlast.m+1/(100*iter)*getData(stim)*dstep;

%if post.m exceeds ball of some radius force it back onto the ball
%radius of this ball needs to be such that f(rmax^2) doesn't cause any blow
%ups in the derivatives
rmax=10^.5;
if (post.m'*post.m >rmax^2)
   post.m=rmax*normmag(post.m); 
   warning('New update exceeds limiting ball');
end
x=getData(stim);
[gd1, gd2]=d2glmeps(getglm(mparam),post.m'*x,obsrv);
if isnan(gd1)
    error('NAN');
end
if isnan(gd2)
    error('NAN');
end
post.c=postlast.c-postlast.c*x*(-gd2/(1-gd2*x'*postlast.c*x))*x'*postlast.c;
%*******************************************************
%efficiently compute the eigenvectors
%*******************************************************
%In the nondiffusion case the eigenvectors are just a rank
%1 update. So we compute einfo efficiently this avoids
%having to call svd in fishermax
%
%The only case when einfolast is empty and diffusion isn't
%on is if we are using random stimuli and this is the first
%iteration

%can we compute the eigenvectors using the rank 1
%updates yes if:
%   2. we have the eigendecomposition

%check if we want to compute the eignedecomposition
if (uobj.PostUpdatersBase.compeig~=0)
if ~isempty(einfolast)

    [gd1, gd2]=d2glmeps(glm,post.m'*stim,obsrv);
    
    %cfull=prior.c-rho*(prior.c*stipdata.timing.optimize(1,tr-1)=searchtime+eigtime;m)*(stim'*prior.c);
    xmax=stim(1:getstimlen(mparam));

    %*******************************************************************
    %Numerical Issue:
    %*******************************************************************
    %we need to comput [x r]'*C*[x r] in order to compute rho
    %but for numerical reasons we don't want to use postlast.c
    %b\c we use the woodbury lemma to compute postlast.c. The woodbury
    %lemma is unstable. Therefore we should use the eigendecomposition. 
    %so we should use the eigendecomposition instead
    %
    %
    z=evecs'*xmax;
    sigma=z'*(eigd.*z);
    rho=gd2/(1-gd2*sigma);
    
    
    if (mparam.alength>0)
        shist=stim(getstimlen(mparam)+1:end);
        u=postlast.c(1:getstimlen(mparam),1:getstimlen(mparam))*xmax+postlast.c(1:getstimlen(mparam),getstimlen(mparam)+1:end)*shist;
        error('double check this as I might have introudced an error when going to the object model');
    else
        %u=postlast.c(1:getstimlen(mparam),1:getstimlen(mparam))*xmax;

        %7-23-2007
        %I changed the code to try to fix numerical errors with
        %eigendecomposition. 
        %don't use the covariance matrix.
        %postlast.c is computed using the Woodbury formula.
        %After several iterations this will no longer be the same matrix as
        %that computed using the rank-1 eigen updates
        %use the eigendecomposition to compute it
        u=evecs*(eigd.*(evecs'*xmax));
    
    end


    %create an EigObj to do the update
    if ~isa(einfolast,'EigObj')        
    einfolast=EigObj('evecs',einfolast.evecs,'eigd',einfolast.eigd); 
    end
    [einfo,oinfo]=rankOneEigUpdate(einfolast,u,rho);    
    
    if any(einfo.eigd<0)        
        error('eigenvalues are not positive');
        ind=find(einfo.eigd<0);
        %if (numel(ind)==1 && ind(1)==length(einfo.eigd
    end
    uinfo.eigtime=oinfo.eigtime;
    uinfo.ndeflate=oinfo.ndeflate;
   
    %*************************************************************
    %error checking
    %*********************************************************
    if (uobj.PostUpdatersBase.debug)
       %print the max difference between the reconstructed covariance and actual covariance
       mdiff=max(max(abs(post.c-eup.evecs*diag(eup.eigd)*eup.evecs')));
       fprintf('Postnewton update: max difference eigen reconstruct %d \n',mdiff);
       
   
    if ~isempty(find(isnan(einfo.evecs)))
        warning('Eigenvectors not defined');
        warning('Computing Eigenvectors via SVD');
        [einfo.evecs einfo.eigd]=svd(post.c(1:getstimlen(mparam),1:getstimlen(mparam)));
        einfo.eigd=diag(einfo.eigd);
        einfo.esorted=-1;   %eigenvalues are in descending order
    end
     end
end
end