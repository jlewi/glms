%function sim=BSBatchFitGLM('bdata',bdata,'maxfunevals','model',mclass)
%   bdata - the data to fit to
%           we make this a member of the object because
%           if we make modifications to the data (i.e compute spectrograms)
%           we want these changes to be saved on iterations of newton's
%           method
%   MaxFunEvals - maximum number of function evaluations for fminunc
%               optional
%   mclass - the name of the model class we will try to fit
%           - we need to specify this because if we are using a model which
%           linearly transforms the parameters into some other space then
%           we need to use BSlogpostlin as opposed to BSlogpost
%
% function sim=BSBatchFitGLM('datafile',file)
%   datafile - file containing the bdata object
%
% Explanation: Fit a GLM to the bird song data using our batch method
%
%
% Revisions:
%   01-26-2009
%       -version is now  a structure defined in the base class
%   11-08-2008 - Allow the parameters to findMAP to be specified as
%                arguments of the class.
%              - for convenience make the parameters settable
%
%   11-01-2008 - mclass is now required
%   07-31-2008
%           Convert to new object Model
%   05-10-2008 - use the BSlogpost object to compute the log posterior and
%               its derivatives all the relevant functions are now members
%               of that class.
%
%   05-06-2008 - add a blank constructor. This will be useful if want to
%        rune BSBatchFitGLM/update as a distributed job on the cluster.
classdef (ConstructOnLoad=true) BSBatchFitGLM <PostUpdatersBase


    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties (SetAccess=private,GetAccess=public)

        bdata=[];
        bpost=[];

    end

    %*******************************************************************
    %define parameters which control how we do the update
    %*******************************************************************
    properties(SetAccess=public,GetAccess=public)
       penalty=[]; 
       iterfile=[];
       usehessian=true;
       whiten=false;
    end
    %the following properties are dependent because the actual values are
    %stored elsewhere
    properties(SetAccess=private,GetAccess=public,Dependent)
        windexes=[];
        maxrepeats=[];
    end
    methods
        function windexes=get.windexes(obj)
           if isempty(obj.bpost)
              windexes=[];
           else
               windexes=obj.bpost.windexes;
           end
        end
        function obj=set.windexes(obj)
           error('can''t set windexes without recreating bpost or modifying access of BSLogPost.windexes'); 
        end
        
       function maxrepeats=get.maxrepeats(obj)
           if isempty(obj.bpost)
              maxrepeats=[];
           else
               maxrepeats=obj.bpost.maxrepeats;
           end
        end
        function obj=set.maxrepeats(obj)
           error('can''t set maxrepeats without recreating bpost or modifying access of BSLogPost.maxrepeats'); 
        end
        
        function obj=BSBatchFitGLM(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};
            
            con(2).rparams={'bdata','mclass'};



            con(3).rparams={'datafile','mclass'};

            %optimization settings
            %I ALSO MODIFY THE OPTIM STRUCTURE AT THE END OF THE CONSDTRUCTOR AFTER
            %PROCESSING PARAMS
            optim=[];
            optim=optimset(optim,'Jacobian','on');
            %optim=optimset(optim,'DerivativeCheck','on');
            optim=optimset(optim,'Display','on');
            optim=optimset(optim,'TolX',10^-10);
            optim=optimset(optim,'TolFun',10^-12);
            optim=optimset(optim,'MaxIter',10000000);

            if isempty(optimget(optim,'MaxFunEvals'))
                optim.MaxFunEvals=1000;
            end

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
                    obj.optim=optim;
                    return
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);
            cind=cind(1);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                case 2
                    obj.bdata=params.bdata;
                case 3
                    data=load(getpath(params.datafile),'bdata');
                    obj.bdata=data.bdata;
                otherwise
                    if isnan(cind)
                        %do nothing
                    else
                    error('Constructor not implemented')
                    end
            end

            bsparam.bdata=obj.bdata;
            
            if isfield(params,'windexes')
                bsparam.windexes=params.windexes;
            end
            if isfield(params,'maxrepeats')
                bsparam.maxrepeats=params.maxrepeats;
            end
            obj.bpost=createbspost(obj,bsparam,params.mclass);

            %*********************************************************
            %Create the object
            %****************************************
            %instantiate a base class if there is one



            optim=getoptim(obj);
            %optional parameters
            if isfield(params,'maxfunevals')
                optim.MaxFunEvals=params.maxfunevals;
            end

            obj=setoptim(obj,optim);

            obj.version.BSBatchFitGLM=090126;
        end
    end
end


