%function [bfdata]=findMAP(uobj,thetainit,mobj,prior)
%       uobj     - BSBatchFitGLM object
%
%       thetainit - optional initial value for search
%           -'cont' - we initialize using the value of theta
%               from the previous run of fminunc
%           -FileObj - name of a file from which we load the value to be
%           continued
%Return value:
%   fmap - a BSBatchMLResults object which containis info about the optimization including the
%   value of the peak.
%
%
%Explanation:
%   Finds the maximum of the posterior using fmincon
%
%Revision
%   11-08-2008 - only uobj,and thetainit should be passed in.
%       The other arguments should be specified as part of the
%       BSBatchFitGLM object.
%       Automatically saving the results is now included as part of the
%       BSBatchMLSim/findMAP function
%
%       New input is prior, and mobj
function [fmap]=findMAP(uobj,thetainit,mobj,prior,statusfile)

if (nargin>5)
    error('windexes,statusfile, penalty, usehessian, whiten, iterfile, maxrepeats, should all be specified by setting the appropriate properties of BSBatchFitGLM');
end

if ~exist('statusfile','var')
    statusfile=[];
end
    
%fmap=struct('theta',[],'exitflag',[],'fsout',[],'lpost',[],'gradient',[],'thetainit',[],'updater',[],'runtime',[],'svnrevision',[],'totaliter',0,'hessian',[],'iterdata',[],'wmat',[],'y',[]);
%create the object to store the results
%
fmap=BSBatchMLResults();

%store the updater as a structure because  uobj is a handle 
%we don't want to store thandle we want to stroe the actual results
fmap.updater=struct(uobj);
fmap.updater.class=class(uobj);

if isempty(statusfile)
    fid=1; %standard output
else
    %make sure the directory to contain the statusfile exists
    fdir=fileparts(getpath(statusfile));
    recmkdir(fdir);
    fid=fopen(getpath(statusfile),'a');
end

if ~isempty(uobj.iterfile)
    iterdata=RAccessFile('fname',uobj.iterfile);
    fmap.iterdata=iterdata;
else
    iterdata=[];
end
optim=getoptim(uobj);


%turn the hessian on
if (uobj.usehessian)
    optim=optimset(optim,'Hessian','on');
else
    optim=optimset(optim,'Hessian','off');
end

optim = optimset(optim,'GradObj','on');

%optim = optimset(optim,'DerivativeCheck','on');
%set the output function to call on each trial
optim=optimset(optim,'OutputFcn',@(x,ov,s)(outfun(x,ov,s,fid,iterdata)));

if ~exist('thetainit','var')
    thetainit=[];
end




if isempty(thetainit)
    thetainit=getm(prior);
end



bpost=uobj.bpost;

%**************************************************************************
%Whitening matrix
%**************************************************************************
%compute matrix for use in premultiplication to whiten the data
wmat=[];
yinit=[];
if (uobj.whiten)
    %we need to compute XX'
    ss=inpcorr(bpost,mobj);

    %hessian of the prior
    hprior=-getinvc(prior);

    wmat=inv((ss+hprior))^.5;
    %wmat=eye(size(hprior));
    yinit=inv(wmat)*thetainit;

    fmap.wmat=wmat;
    fmap.whiten=whiten;
else
    yinit=thetainit;
end

if any(~isreal(yinit))
    error('yinit has imaginary components');
end
%**************************************************************************
%Actual Optimization
%**************************************************************************

fprintf(fid,'findMAP.m: calling fminunc \n');


fun=@(theta)(objfun(theta,bpost,mobj,prior,uobj.penalty,wmat));


tic
if (uobj.usehessian)
    [fmap.y, fval, fmap.exitflag, fmap.fsout,df,d2f]= fminunc(fun,yinit,optim);
    fmap.hessian=-d2f;
else
    [fmap.y, fval, fmap.exitflag, fmap.fsout,df]= fminunc(fun,yinit,optim);
end

if (uobj.whiten)
    fmap.theta=wmat*fmap.y;
else
    fmap.theta=fmap.y;
end

runtime=toc;

%**************************************************************************
fprintf(fid,'findMAP.m: fminunc finished. \n');

fmap.lpost=-fval;
fmap.gradient=-df;

fmap.thetainit=thetainit;
fmap.runtime=runtime;
fmap.svnrevision=nan;

fmap.totaliter=fmap.totaliter+fmap.fsout.iterations;

try
    %    fpath=FilePath('bcmd','HOMEDIR','rpath','svn_trunk/scripts/get-svn-revision');
    [stat svnrevision]=system('~/svn_trunk/scripts/get-svn-revision');
    fmap.svnrevision=svnrevision;
catch
end


if (fid>1)
    fclose(fid);
end
%*************************************************
%save results to the data file
%*****************************************************

%make sure we converged
%exitflag==1 it converged
switch (fmap(end).exitflag)
    case {-3,0}
        %potentially try changing initialization point
        fprintf('BSBatchFitGLM/findMAP: fminunc did not converge after %2.2g Iterations. Exited with message: \n %s',fmap(end).fsout.iterations,fmap(end).fsout.message);
    case {1,2,3}
        %do nothing it terminated ok
    otherwise
        %fprintf('grad: fsolve terminated with message \n %s \n',fsout.message);
end

%error checking
%check derivative is zero
if any(abs(fmap(end).gradient)>10^-5)
    warning('Derivative at MAP is not zero');
end
%***********************************************************************************************************
%objective function
%**********************************************************************************************************
%we want to maximize the posterior
%but we use fminunc so we need to multiple the values by -1
%
% wmat - Linear mapping applied to x to create theta
function [fval,df,varargout]=objfun(x,bpost,mobj,prior,penalty,wmat)

if isempty(wmat)
    theta=x;
else
    theta=wmat*x;
end

if (nargout==3)
    [lpost,dlpost,d2lpost]=complpost(bpost,mobj,theta,prior);
    d2f=-1*d2lpost;
    varargout{1}=d2f;
else
    [lpost,dlpost]=complpost(bpost,mobj,theta,prior);
end
fval=-1*lpost;
df=-1*dlpost;


nshist=getshistlen(mobj);
nstim=getklength(mobj)*getktlength(mobj);
if ~isempty(penalty)
    [stimcoeff, shistcoeff,bias]=parsetheta(mobj,theta);
    tmat=reshape(stimcoeff,getstrfdim(getbdata(bpost)));
    %   p=comppenalty(penalty,tmat);

    if (nargout==3)
        [p,dp,d2p]=dpenaltydtheta(penalty,tmat);

        %the hessian of the smoothing term is only with respect to the
        %stimulus coefficents. We need to set the second derivative of the
        %penalty with respect to the spike history and bias term 2 zero
        d2f(1:nstim,1:nstim)=d2f(1:nstim,1:nstim)+d2p;

        varargout{1}=d2f;
    else
        [p,dp]=dpenaltydtheta(penalty,tmat);
    end


    fval=fval-p;
    df=df-[dp(:);zeros(nshist+1,1)];

end

if ~isreal(fval)
    error('Value of log posterior is not real');
    msg='Value of function is imaginary';
    if any(~isreal(x))
        msg=sprintf('%s.\n x has imaginary elements',msg);
    else
        msg=sprintf('%s.\n x has no imaginary elements',msg);
    end

    if any(~isreal(theta))
        msg=sprintf('%s.\n theta has imaginary elements',msg);
    else
        msg=sprintf('%s.\n theta has no imaginary elements',msg);
    end

    if isdistributed(wmat)
        msg=sprintf('%s. \n wmat is distributed',msg);
    else
        msg=sprintf('%s. \n wmat is not distributed',msg);
    end

    warning(msg);
    fval=real(fval);
    df=real(df);
    if (nargout==3)
        varargout{1}=real(varargout{1});
    end
end

%we have to scale the derivative and hessian appropriately
if ~isempty(wmat)
    df=wmat'*df;
    if (nargout==3)
        varargout{1}=wmat'*varargout{1}*wmat;
    end
end

%outfun is a function that we tell fminunc to call after each iteration
%this allows us to output status information
function stop=outfun(x, optimValues, state,fid,iterdata)
stop=false;
%if (labindex==1)
fprintf(fid,'Iteration %d \n',optimValues.iteration);
%end
if ~isempty(iterdata)
    %save the values to the file
    id=getlastid(iterdata)+1;
    if isnan(id)
        id=1;
    end
    writematrix(iterdata,x,id);
end
