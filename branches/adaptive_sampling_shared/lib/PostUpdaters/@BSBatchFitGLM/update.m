%function [bfdata]=BSBatchFitGLM(uobj,datafile,thetainit)
%       uobj     - BSBatchFitGLM object
%       datafile - file containing the posterior
%           I pass in the file and not simply the GaussPostFile object
%           because I'm not sure whether when we run update as a
%           distributed job, whether having matlab save the GaussPostFile
%           object will work ok.
%
%           also contains the MParamObj object to use.
%       thetainit - optional initial value for search
%       opt  - optional parameters
%Return value:
%
%   This function does not use the findMAP function. 
%   batchfit - structure containing info about the optimization including the
%   value of the peak.
%
%   The return value is also saved to datafile
%
%Explanation:
%   Finds the maximum of the posterior using gradient ascent
%
%
function [batchfit]=update(uobj,datafile,thetainit,opt)

warning('Update does not use findMAP, you should probably update it to use findMAP');

optim=getoptim(uobj);

if ~exist('thetainit','var')
    thetainit=[];
end

if ~exist('opt','var')
    opt=[];
end

%**************************************************************************
%load the allpost, mobj, bdata
%*************************************************************************
data=load(getpath(datafile));
allpost=data.allpost;

%load bdata from the file
bdata=data.bdata;
uobj.bdata=bdata;

mobj=data.mobj;
clear data;


uobj.bpost=BSlogpost('bdata',bdata);

%***********************************************************
%get the prior
%*************************************************************
prior=GaussPost('m',getm(allpost,0),'c',EigObj('matrix',getc(allpost,0)));
%compute the inverse of the prior covariance matrix so that we only do it
%once


%location of zeros of derivative
%initialization:
%thetainit=getm(allpost,getlasttrial(allpost));
if isempty(thetainit)
thetainit=getm(allpost,0);
end
bpost=uobj.bpost;
objfun=@(theta)(compd2lpost(bpost,mobj,theta,prior));
[batchfit.theta, batchfit.gradient, batchfit.exitflag, batchfit.fsout]= fsolve(objfun,thetainit,optim);

batchfit.thetainit=thetainit;
batchfit.opt=opt;

if (labindex==1)
    if (varinfile(datafile,'batchfit'))
        v=load(getpath(datafile),'batchfit');
        if ~isfield(v.batchfit, 'opt')
            v.batchfit.opt=[];
        end
        batchfit=[v.batchfit batchfit]; 
    end
end
save(getpath(datafile),'-append','batchfit');

%*************************************************
%save results to the data file
%*****************************************************

%make sure we converged
%exitflag==1 it converged
switch (batchfit(end).exitflag)
    case {-3,0}
        %potentially try changing initialization point
    error('BSBatchFitGLM/update: fsolve did not converge after %2.2g Iterations. Exited with message: \n %s',batchfit(end).fsout.iterations,batchfit(end).fsout.message);
    case {1,2,3}
        %do nothing it terminated ok
    otherwise
    %fprintf('grad: fsolve terminated with message \n %s \n',fsout.message);
end

%error checking
%check derivative is zero
if any(abs(batchfit(end).gradient)>10^-5)
    warning('Derivative at MAP is not zero');
end


