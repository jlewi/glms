%function [post,]=update(uobj,postlast,stim,mobj,obsrv)
%       postlast - the most poster after the previous observation
%       stim - full stimulus - spike history and stimulus part
%           - also stimulus after any input transformations
%       obsrv- observation
%
function [post]=update(uobj,postlast,stim,mobj,allobsrv,simobj,trial)
glm=getglm(mobj);

options=getoptim(uobj);


trial=simobj.niter+1;

if (trial>uobj.ninit)
    %call update for the newton updater
    post=update(uobj.nupdater,postlast,stim,mobj,allobsrv,simobj,trial);
else
   %call the batch updater
   %first we need to update inputs
   if isempty(uobj.inputs)
      uobj.inputs=zeros(getparamlen(simobj.mobj),uobj.ninit); 
      uobj.obsrv=zeros(1,uobj.ninit);
      %add all of the previous
      %inputs to uobj.inputs
      for t=1:(trial-1) 
          windex=simobj.paststim(1,t);
          repeat=simobj.paststim(2,t);
          rtrial=simobj.paststim(3,t);
          [pstim,pshist,pobsrv]=gettrialwfilerepeat(simobj.stimobj.bdata,windex,repeat,rtrial,simobj.mobj);
          
          input=projinp(simobj.mobj,pstim,pshist);
          
          uobj.inputs(:,t)=input;
          uobj.obsrv(t)=pobsrv;
      end
   end
   
          
   %add the latest inputs to uobj.inputs
   uobj.inputs(:,trial)=stim;
   uobj.obsrv(trial)=allobsrv;
   
   post=update(uobj.bupdater,postlast,uobj.inputs(:,1:trial),mobj,uobj.obsrv(1:trial),simobj,trial);
end
   
    
end
    