%function [post]=comppost(uobj,trial,minit)
%   uobj - the object
%   trial - the trial on which to compute the posterior for
%   minit - optional start guess for theta
%
%Explanation: Use batch methods to compute the posterior on the specified
%trial.
%
function [post]=comppost(uobj,trial,minit)


glm=getglm(uobj.oldsim.mobj);
options=getoptim(uobj);
simobj=uobj.oldsim;
if ~exist('minit','var')
    minit=[];
end

if isempty(minit)
   minit=zeros(getparamlen(uobj.oldsim.mobj),1); 
end

%make sure we have all the inputs and observations needed for this trial
if (size(uobj.inputs,2)<trial)
   oldinputs=uobj.inputs;
   oldobsrv=uobj.obsrv;
   


   uobj.inputs=zeros(getparamlen(uobj.oldsim.mobj),trial);
   uobj.obsrv=zeros(1,trial);

   nold=size(oldinputs,2);
   if (nold>0)
   %copy the existing inputs
    uobj.inputs(:,1:nold)=oldinputs;
     uobj.obsrv(1,1:nold)=oldobsrv;
   
       starttrial=nold+1;
   else
      starttrial=1; 
   end
   simobj=uobj.oldsim;
   %add all of the previous
      %inputs to uobj.inputs
   for t=starttrial:trial
       if (mod(t,100)==0)
           fprintf('Getting input trial %d \n', t);
       end
          windex=simobj.paststim(1,t);
          repeat=simobj.paststim(2,t);
          rtrial=simobj.paststim(3,t);
          [pstim,pshist,pobsrv]=gettrialwfilerepeat(simobj.stimobj.bdata,windex,repeat,rtrial,simobj.mobj);
          
          input=projinp(simobj.mobj,pstim,pshist);
          
          uobj.inputs(:,t)=input;
          uobj.obsrv(t)=pobsrv;
      end

end

%call update using the base class
postlast=GaussPost(minit,getc(simobj.allpost,0));
post=update(uobj,postlast,uobj.inputs(:,1:trial),simobj.mobj,uobj.obsrv(1:trial),simobj,trial);
