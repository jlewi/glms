%function [dw,niter]=newtonsearch(obj,w,info)
%   w         - direction in which we want to find the MAP
%   info      - the quantities we need to compute the gradient
%       .tcow - crossproduct -(theta-mu_o)c_o^-1 w
%               (mu_o,c_o) mean and covariance of prior
%             - theta = thetainit
%       .wcow - w'*co^-1w
%       .inpw - a cell array number of arrays = number of wave files
%             - each matrix contains s_j' w
%               s_j - the input corresponding to column j of this wave file
%               each row is for a different repeat of this wave file
%
%Explanation: Find the value of dw numerically. dw is the value such that
%   the derivative in direction w at
%   theta=thetainit+dw w  is zero
%
%   I copied this code from BSBatchSlice
%   For more information see that function.
%
%
function [dwnew,niter]=newtonsearch(obj,w,info,mobj)

%keep track of the number of times we do a binary search
nbinary=0;
%maximum number of failures of the newton step before switching to binary
maxnewtfails=2;
options=getoptim(obj);
dwerr=inf;
ferr=inf;
lastdwerr=inf;
lastferr=inf;
niter=0;

dw=0;

%objfunv - keeps track of the value at the current iterate
%   .dw - value of dw at which values are computed
%   .dpost - derivative of the log posterior in direction w
%           evaluated at dw
%   .dpostdw - derivative of the derivative of the log posterior
%             in direction w evaluated dw
compderv=true;
objfunv=dlpostonslice(obj,dw,w,mobj,info,compderv);

while (((abs(dwerr) >options.TolX) || (abs(ferr)>options.TolFun)) && (niter<options.MaxIter) && (nbinary<maxnewtfails));

    %indicates whether we should do a binary search.
    dobinary=false;


    %take a newton step
    compderv=true;
    [ovalnew, ferr, dwerr]=newtonstep(obj,objfunv,w,mobj,info);
    niter=niter+1;
    dwnew=ovalnew.dw;
    
    %if error is increasing produce warning
    if ((abs(dwerr)>abs(lastdwerr))&&(abs(ferr)>abs(lastferr)))
        warning('BSBatchSlice: error increased. Try binary search');
        %we are in danger of oscillating try a binary search before
        %continuing
        dobinary=true;

    elseif (isnan(dwnew))
        warning('BSBatchSlice: newton step -> dwnew=nan. Try binary search');
        %set dwnew to zero because that acts as our initialization
        dwnew=0;
        dobinary=true;
    elseif ((abs(dwerr)==abs(lastdwerr)) && abs(ferr)==abs(lastferr))
        warning('BSBatchSlice: Error did not change. Itertation %d.\n Trying binary search',niter);

        if ((abs(dwerr)<options.TolX) && (abs(dwerr)==abs(lastdwerr)))
            warning('BSBatchSlice: dwerr has converged with f=%d\n' ,ferr);
            %terminate the loop
            break;
        else
            dobinary=true;
        end
    elseif ((abs(dwerr)>10) && (abs(dwerr)-abs(lastdwerr))/abs(lastdwerr) <.001)
        warning('BSBatchSlice: dwerr does not appear to be changing will take binary step.');
        dobinary=true;
    elseif (abs(dwerr)==0)
        %4-25-2008
        %dwerr has converged so our estimate is longer changing
        %but since loop hasn't terminated ferrr must violate
        %Tolerance
        %print warning and move on.
        if (abs(ferr) >options.TolFun)
            warning('BSBatchSlice: dwerr has converged with f=%d\n' ,ferr);
        end
        %terminate the loop
        break;
    end

    %do a binary search if newtonstep failed
    if (dobinary && nbinary<maxnewtfails)

        [dwnew,ferr,biter]=binarysearch(obj,w,info,mobj,min(10^-3,abs(dwerr)/10),dwnew);
        dwerr=dwnew-a;
        niter=niter+biter;
        nbinary=nbinary+1;
    end
    objfunv=ovalnew;
    lastdwerr=dwerr;
    lastferr=ferr;
end

if (nbinary>=maxnewtfails)
    warning('BSBatchSlice: Newton search failed. Switching to binary.');
    if (isinf(dwnew) || isnan(dwnew))
        dwnew=0;
    end
    [dwnew,ferr,biter]=binarysearch(obj,w,info,mobj,min(options.TolX),dwnew);
    niter=niter+biter;
end
if (niter==options.MaxIter)
    warning('BSBatchSlice NewtonSearch: Max Iterations Reached. Try binary');
    %04-25-2008
    %try a binary search
    if (isinf(dwnew) || isnan(dwnew))
        dwnew=0;
    end
    [dwnew,ferr,biter]=binarysearch(obj,w,info,mobj,min(options.TolX),dwnew);
    %\n \t dwerr=%d',abs(dwerr));
end


fprintf('Newtonsearch: dwerr=%03.3g \t dlogpost=%03.3g \n',dwerr,objfunv.dpost);


%take a newton step
function [ovalnew, ferr, dwerr]=newtonstep(obj,objfunv,w,mobj,info)
compderv=true;
dwnew=objfunv.dw-1/objfunv.dpostdw*objfunv.dpost;

%evaluate our objective function at this value
ovalnew=dlpostonslice(obj,dwnew,w,mobj,info,compderv);

ferr=abs(ovalnew.dpost);
dwerr=abs(ovalnew.dw-objfunv.dw);
