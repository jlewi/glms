%function dlpostonslice(obj,dw,w,mobj,info,compderv)
%   obj - the bsbatch slice object
%   dw  - how far to step in direction w
%   info - structure containing quantities needed to compute the derivative
%   compderv - indicates whether or not to compute derivative
%evaluate the function whose root we are trying to find
%   and the derivative of that function
%
% Return value
%   objfunv
%       .dpost -gradient of the log posterior in direction w at this dw
%
%Explanation: Computes the derivative of the log posterior in direction w 
% as a function of dw - the size of the step in direction w relative to
% thetainit
%   also computes the derivative of this function w.r.t dw.

function  objfunv=dlpostonslice(obj,dw,w,mobj,info,compderv)
%evaluate the gradient of the posterior in direction w
%this is the function whose root we want to find
%also compute the derivative of this with respect to dw so we can take
%a newton step

%dpost - derivative of th posterior we want to find the root of this
%dpostdw - derivative of the derivative of the posterior w.r.t to dw
dpost=0;
dpostdw=0;

dpost=info.tcow-dw*info.wcow;
dpostdw=-info.wcow;

%nwavefiles=getnwavefiles(getbdata(obj.bpost));
windexes=getwindexes(obj.bpost);
for j=1:length(windexes)
    wind=windexes(j);
    glmproj=info.inptheta{wind}+dw*info.inpw{wind};

    if (compderv)
    [dglmproj,d2glmproj]=d2glmeps(getglm(mobj),glmproj(:),info.trialobsrv{wind}(:),'v');
    else
    [dglmproj]=d2glmeps(getglm(mobj),glmproj(:),info.trialobsrv{wind}(:),'v');
    end
    %we multiply by (s'w) and (s'w)^2 and sum
    dpost=dpost+sum(dglmproj.*info.inpw{wind}(:)');
    if (compderv)
    dpostdw=dpostdw+sum(d2glmproj.*(info.inpw{wind}(:)').^2);
    end
end

objfunv.dpost=dpost;
if (compderv)
objfunv.dpostdw=dpostdw;
else
    objfunv.dpostdw=[];
end
objfunv.dw=dw;