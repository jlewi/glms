%function [theta finfo]=findMAP(uobj,datafile,thetainit, w)
%       uobj     - BSBatchFitGLM object
%       datafile - file containing the posterior
%           I pass in the file and not simply the GaussPostFile object
%           because I'm not sure whether when we run update as a
%           distributed job, whether having matlab save the GaussPostFile
%           object will work ok.
%
%           also contains the MParamObj object to use.
%       thetainit - optional initial value for search
%       w         - vector denoting the direction of the slice
%Return value:
%   theta - value of theta in which derivative in direction w is zero
%
%   finfo - structure containing info about the optimization including the
%   value of (theta-thetainit)'*w;
%
%
%Explanation:
%   Finds the maxium of the posterior along some slice
%
%
function [theta,dw]=findMAPonslice(uobj,thetainit,w,info,mobj)

w=normmag(w);
glm=getglm(mobj);

%how big of a step to take in direction w to find the MAP in direction
%w. We find dw by finding the value of dw for which the derivative is zero
dw=0;


%10-19-2007
%       To handle one sided nonlinearities we need to make sure our initial
%       guess for glmproj>=0 if obsrv is >0.
% if post.m'*x<=0 and obsrv=0 there is no information in the responses
%   we leave the mean unchanged. our uncertainty does not decrease
%   because 2nd derivative is zero. Not sure if this is accurate
%   because this trial should provide evidence that our estimate is
%   correct.
%
%This code was just copied from Newton1d I have not actually
%modified it to work
if (isonesided(glm))
    if ((mueps<0)&&(obsrv==0))
        %special case
        %post.m'*x<=0 and obsrv=0
        %how should we update?
        %the mean should be unchanged
        %leave our uncertainty unchanged

        %the mean is unchanged.
        %prevents loop.
        %leave the posterior unchanged
        % pnew=GaussPost('m',getm(postlast),'c',getc(postlast));

        ntime=toc;
        aopt=0;
        niter=0;

        %this is code from newton1d which has not been updated yet
        error('Need to modify code to handle this.')
    else
        %do a binary search
        %avoid a newton search because we have to keep u positive which
        %is a pain

        %this code was from Newton1d have not modified it to work
        %in this case yet
        abthresh=options.TolX;
        error('Need to modify code to handle this.')
        [aopt,ferr,niter]=binarysearch(uobj,mueps,sigmaeps,xmag,obsrv,glm,abthresh);
    end
else
    %its not a one-sided nonlinearity do regular newtonsearch

    [dw,niter]=newtonsearch(uobj,w,info,mobj);
    

    theta=thetainit+dw*w;
end