%function [dwnew,fval,nbiter]=binarysearch(obj,w,info,mobj,abthresh,dwinit)
%
%   abthresh - threshold for stopping the binary search
%   ainit    - initial guess optional
%               defaults to zero
%
%Return value:
%   dwnew - the new value of dwnew
%   fval  - the value of the function at dwnew
%           this is the value of the function at dwnew whose root we are
%           trying to find
% Explanation:
%       Do a binary search to find the root of the 1-d equation
%
%
% Warnings:
%    Function assumes nonlinearity is log-concave. If its not and its one
%    sided the initialization will fail.
% Bug fix:
%   11-18-2007
%       fixed initialization for non-one-sided glm. Takes advantage of the
%       fact that our objective function is always increasing with a.
%   10-19-2007: I was searching for dlda=0. In reality we want to find
%   la=0. Right?
function [dwnew,fval,nbiter]=binarysearch(obj,w,info,mobj,abthresh,dwinit)
if ~exist('dwinit','var')
    dwinit=0;
end

glm=getglm(mobj);

optim=getoptim(obj);
%one sided glm's are initialized differently b\c left boundary is always
%zero
if (isonesided(glm))
    error('need to write code to handle this possibiltiy');
   


else

    %regular initialization     

    %compute the derivative and use it to determine the sign so we can
    %figure out which way we need to go to find the appropriate bounds.
    compderv=true;
    oval=dlpostonslice(obj,dwinit,w,mobj,info,compderv);
    %we need to find dwleft and dwright to make sure they include the zero
    %the terms dwleft - value for which objective funcition is negative
    %dwright - value for which objective function is positive
    %following assumes ainit=0
    if (oval.dpost<=0)
        dwleft=dwinit;
         ovdwleft=oval;
        dwright=sign(oval.dpostdw)*.1;
        %we need to increase the magnitude of
        %right until the objective function is positive
        compderv=false;
        
        oval=dlpostonslice(obj,dwright,w,mobj,info,compderv);
        while(oval.dpost<0)
            dwright=dwright*10;
                    oval=dlpostonslice(obj,dwright,w,mobj,info,compderv);
        end
        ovdwright=oval;
    else
        dwright=dwinit;

        ovdwright=oval;
        %we need to decrease dwleft until its positive
        dwleft=-sign(oval.dpostdw)*.1;
        compderv=false;
        
        oval=dlpostonslice(obj,dwleft,w,mobj,info,compderv);
        while (oval.dpost>0)
            dwleft=dwleft*10;
            
            oval=dlpostonslice(obj,dwleft,w,mobj,info,compderv);            
        end
        ovdwleft=oval;
    end

    %make sure dwleft and dwright don't both have same sign

    if (sign(ovdwleft.dpost)==sign(ovdwright.dpost))
        error('binary search cannot work initialization is invalid');
    end

end
%while dwleft-dwright is bigger then some threshold do binary step
dwnew=(dwleft+dwright)/2;

nbiter=0;
while ((abs(dwleft-dwright)>abthresh) && (nbiter <optim.MaxIter))
    nbiter=nbiter+1;
    dwnew=(dwleft+dwright)/2;
    
    oval=dlpostonslice(obj,dwnew,w,mobj,info,compderv);
    %assume la is monotonically increasing 
    if (oval.dpost<0)
        dwleft=dwnew;
    else
        dwright=dwnew;
    end
end



if (nbiter==optim.MaxIter)
    warning('Newton1d BinarySearch: Max Iterations Reached, \n \t dwerr=%d',abs(dwleft-dwright));
end

fval=oval.dpost;
