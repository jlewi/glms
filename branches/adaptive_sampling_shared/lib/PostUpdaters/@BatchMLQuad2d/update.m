%function [post]=update(uobj,postlast,stim,mparam,obsrv)
%       postlast - postlast.m - the initialization point for the
%              optimization
%              this should have length 2*d where 
%              d=mparam.klength^.5
%       stim - full stimulus - spike history and stimulus part
%               should be mx n array
%                       m=klength+alength
%                       n = number of observations
%       obsrv- observation
%
% Return value
%   post - the new posterior
%Exlanation:
%   This class estimates the MLE when the stimulus is pushed through a
%   quadratic nonlinearity. The true parameter is A=k1k1^t +k2k2^t
%   BatchML\Newton1d can estimate the higher dimensional model A.
%   This function estimates the lower dimensional model k1, k2 by
%   maximizing the MLE. I don't think the likelihood is concave anymore
%   with respect to k1, k2. (Clearly there are at least two maxima because
%   we can interchange the values of k1 and k2 withouth any loss).
%
% DOES MAXIMIUM LIKELIHOOD- i.e ignores the prior
%
function [post,uinfo,einfo,k1,k2]=update(uobj,postlast,stim,mparam,obsrv)
uinfo=[];
einfo=[];
post=[];

error('Need to update this function based on revisions >1585. Represent post as GaussPost. Do not return uinfo or einfo');
glm=mparam.glm;

%check if we need to nonlinearly transform the input
if isa(mparam,'MGLMNonLinInp')
    stim=projinp(mparam,stim);
    if ~isequal(mparam.ninpfunc,@projinpquad)
        error('This update method assumes the input nonlinearity is projinpquad');
    end
    else
        error('This function is only intended for MGLMNonLinInp models');
    end


%**************************************************************
%special processing for poisson-2-14-07
%********************************************************
%Special processing is needed to deal with nonlinearities
%for which f(u)=0 for u<0
if (strcmp(glm.dist,'poisson') && (glm.fglmonesided==1))
    error('I have not checked this code is valid. It was inherited from BatchML code');
    %if the nonlinearity is one sided (that is fglmmu=0 for
    %\glmproj <0)
    %then for any observations for which r>0
    %we need theta'*stim> 0
    %otherwise we have problems trying to do gradient ascent
    %because gradient is flat.
    %fprintf('Need to check distribution is one sided\n');

    %find any points for which
    ind=find((postlast.m'*stim<0) & (obsrv>0));
    stimmag=sum(stim(:,ind).^2,1).^.5;
    %set projection of postlast.m along these stimuli to be
    %slightly positive.
    %The derivative should be well defined in this case
    %provided the model is well specified
    %I'm not sure however, that as we do gradient ascent we're
    %guaranteed that this condition remains true.
    %doesn't handle spike history
    postlast.m=postlast.m-sum(ones(mparam.klength,1)*(-.0001+postlast.m'*normmag(stim(:,ind))).*normmag(stim(:,ind)),2);
    %check it worked
    ind=find((postlast.m'*stim<0) & (obsrv>0));
    if ~isempty(ind)
        error('Current estimate yields theta such that glmproj<0 for observations>0');
    end
end
optim=[];
optim = optimset(optim,'GradObj','on');
optim=optimset(optim,'Display','on');
optim=optimset(optim,'TolX',10^-10);
optim=optimset(optim,'TolFun',10^-16);

%need to take negative b\c we want to maximize likelihood
%
objfun=@(k)(neglogll(k,obsrv,stim,mparam));

d=mparam.klength^.5;
kmle = fminunc(objfun,postlast.m,optim);
k1=kmle(1:d);
k2=kmle(d+1:end);

%function [ll,dll]=neglogll(k,obsrv,stim,mparam)
%   k
%   stim - stimuli after projecting through the nonlinearity
% Explanation: Computes the loglikelihood and the gradient w.r.t to
% k=[k1;k2]. This is the function we want to maximize w.r.t k to find the
% MLE
%
%Computes the neg log likelihood and the derivative of the negative log
%likelihood
function [ll,dll]=neglogll(k,obsrv,stim,mparam)

%let A= rasterization of k1,k2
d=mparam.klength^.5;
k1=k(1:d,1);
k2=k(d+1:end,1);

A=k1*k1'+k2*k2';
A=A(:);

epsvar=A'*stim;
ll=loglike(mparam.glm,obsrv,epsvar);
ll=sum(ll);

%compute the gradient
%w.r.t to k=[k1;k2];
dll=gradk(k1,k2,obsrv,stim,mparam.glm);

%multiply by -1 because we want the negative log likelihood
ll=-1*ll;
dll=-1*ll;

%function grad(k1,k2,obsrv,stim)
%   k1,k2 - filter values at which to evaluate the gradient
%   pkinit - prior on k
%   obsrv - observations
%   stim  - matrix of stimuli after pushing them through the nonlinearity
%Explanation:
%   compute the gradient of the 
function [dldk]=gradk(k1,k2,obsrv,stim,glm) 
d=length(k1);
A=k1*k1'+k2*k2';
A=A(:);

epsvar=A'*stim;
%compute the gradient with respect to A'stim
%where A is vector representing rasterization of k1*k1+k2*k2'
%d2glmeps will return a matrix
%we only want the diagonal entries
%it might be more efficient to just compute the diagonal entries
[dldeps]=d2glmeps(epsvar,obsrv',glm);
dldeps=(diag(dldeps))';

%multiple by stim to get derivative with respect to A
%dlda=(dldeps'*ones(1,d^2)).*stim';
%dlda=sum(dlda,1);

%construct the matrix reprsenting dA/dK
%this is d^2x2d matrix
dAdk=zeros(d^2,2*d);
[i,j]=ind2sub([d,d],[1:d^2]);
for m=1:d^2    
  dAdk(m,i(m))=k1(i(m))+dAdk(m,i(m));
  dAdk(m,j(m))=k1(j(m))+dAdk(m,j(m));
  
  dAdk(m,d+i(m))=k2(i(m))+dAdk(m,d+i(m));
  dAdk(m,d+j(m))=k2(j(m))+dAdk(m,d+j(m));
end

depsdk=stim'*dAdk;

dldk=(dldeps'*ones(1,2*d)).*depsdk;
dldk=sum(dldk,1);