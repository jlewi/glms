%function [post,]=update(uobj,postlast,stim,mobj,obsrv,sobj)
%       postlast - the posterior after the previous observation
%                - This can be A GaussPost object or a PostTanSpace Object
%       stim - full stimulus - spike history and stimulus part
%           - also stimulus after any input transformations
%       obsrv- observation
%
%       mobj - model object
%       sobj - Simulation object
%               get all data from this object
%       trial - latest trial
%             -i.e the time index of the data point that is being added to
%             the dataset
%             - 10-28-2008 - this is obsolete since we pass in the
%             simulation object
function [post]=update(uobj,postlast,stim,mobj,obsrv,simobj,trial)



if isa(stim,'GLMInput')
    stim=getData(stim);
end


%get the full posterior on theta space from the previous iteration
if isa(postlast,'PostTanSpace')
    fullplast=getfullpost(postlast);
else
    fullplast=postlast;
end

%1. We update the full posterior using Newton 1d object
%   this just requires the most recent observation
unewt=Newton1d('optim',getoptim(uobj),'compeig',uobj.compeig);


%stim=projinp(mobj,getstim(simobj,trial),getsr(simobj,[max((trial-getshistlen(mobj)),1):trial-1]));
fpnew=update(unewt,fullplast,stim,mobj,obsrv,simobj);

if (uobj.comptanpost)
    [tanpoint,fptan]= comptanpost(uobj,fpnew,mobj);
else
    tanpoint=[];
    fptan=[];
end

post=PostTanSpace(fpnew,fptan,tanpoint);