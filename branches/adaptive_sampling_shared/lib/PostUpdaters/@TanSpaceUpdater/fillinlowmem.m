%funciton simobj=fillinlowmem(nobj, pdata,mobj,sr,trials)
%   newton1d - object
%   simobj - simulation object containing the data
%   trials - trials on which to compute the covariance matrix
%            -leave blank to compute it for all trials.
%
%Explanation: This computes the covariance matrices for tials
function [simobj]=fillinlowmem(nobj, simobj,trials)

if ~exist('trials','var')
    trials=[];
end

if isempty(trials)
    trials=1:getniter(simobj);
end

nnewt=Newton1d();

%determine which trials we have the posteior for allready
%thave post does not include the posterior
%thavepost =1 we have the posterior 
%thavepost =0 we do not have the posterior
thavepost=zeros(1,max(trials));
for ind=1:max(trials)
    if ~isempty(getpostc(simobj,ind))
        thavepost(ind)=1;
    end
end
mobj=getmobj(simobj);
glm=getglm(mobj);
%plast will store the posterior from previous iteration
plast=[];
tlast=0;    %trial for which plast represents the posterior
for trial=trials;       
        if (mod(trial,10)==0)
            fprintf('Trial = %d \n',trial);
        end
        %find most recent trial for which we have post
        tlast=find(thavepost(1:trial)==1);
        if isempty(tlast)
           %set it to the prior
            tlast=0;
        else
            tlast=tlast(end);
        end
        
        if (tlast==trial)
            %we already have this trial
            %make sure covariance matrix doesn't exist for this trial.
           fprintf('Skipping trial covariance already exists  for trial %d \n ',trial);
        else
            %get the posterior of trial
            plast=getfullpost(getpost(simobj,tlast));
           
            %to compute the covariance matrix we need to compute all
            %covariance matrices upto time trial
            for tup=(tlast+1):trial
                sr=getsr(simobj,tup);
                x=projinp(mobj,getInput(sr));
                obsrv=getobsrv(sr);
                
                
                %use gu-eisenstat code to compute the rank 1 update                
               [enew]=compceig(nnewt,plast,getm(getfullpost(getpost(simobj,tup))),x,obsrv,glm);
                
               %update plast
               tlast=tup;
               plast=getfullpost(getpost(simobj,tlast));
               plast=setc(plast,enew);
            end
            
            %set the posterior in simobj
            postobj=getpost(simobj,tlast);
            
            %set the full posterior
            postobj=setfullpost(postobj,plast);
          
              %compute the posterior on the tangent space
             tparams=getparam(gettanspace(postobj));
            tparams.dimsparam=getdimsparam(gettanspace(postobj));
            [tanspace,fptan]= comptanpost(nobj,plast,class(gettanspace(postobj)),tparams);
 
          
            postobj=settanpost(postobj,fptan);
            
          
            simobj=setpost(simobj,tlast,postobj);
            thavepost(tlast)=1;
                        
        end
    end
end




