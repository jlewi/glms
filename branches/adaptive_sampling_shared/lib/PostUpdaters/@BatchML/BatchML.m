%function sim=Batchll(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: This class updates the posterior by doing batch maximum
% likelihood on all observations so var.
%
% Revision 
%   01-11-2009 - version is now a structure defined in the base class
%
%   11-05-2008
%       added rmax.
%   11-03-2008
%   Converted to new OOP model
%   delete field bname
% $Revision$: 070625
%   added fields "bname", "version"
%   removed field optim. This is now a field of the base class
%
classdef (ConstructOnLoad=true) BatchML <PostUpdatersBase

    properties(SetAccess=private,GetAccess=public)
      
        
        %optional. If This is specified then we use this along with the
        %inputs already observed to impose linear constraints on the MAP
        %i.e for all inputs f(\inp^T\theta)<rmax
        %this helps speed up the search for the MAP
        rmax=[];
    end
    
    
    properties(SetAccess=private,GetAccess=public,Transient)
        %keep track of the inputs and observations so we don't
        %have to recompute the entire history on each update.
        %this could cause problems if we improperly access stored inputs
        %for an old simulation.
       inputs=[]; 
       obsrv=[];
       
       
       %lasttrrial
       %last trial for which we actually stored the inputs and obsrv
       %this may be less than length of inputs and obsrv
       lasttrial=[];
       
       %simidnum identifies the simulation so we can pake sure it hasn't
       %changed
       simidnum=[];
    end
    methods
        function obj=BatchML(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.

            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,[]);

            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    %all parameters are for base casse
                    bparams=params;
                otherwise
                    error('unexpected value for cind');
            end

            if isfield(bparams,'rmax')
                bparams=rmfield(bparams,'rmax');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@PostUpdatersBase(bparams);

%             switch cind
%                 case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
%                     %used by load object do nothing
%                     bparams=struct();
%                 case {Constructor.nomatch}
%                     %all parameters are for base casse
%                     bparams=params;
%                 otherwise
%                     error('unexpected value for cind');
%             end


            if ~isfield(params,'optim')
                %initialize default values of optim
                obj.optim = optimset(obj.optim,'GradObj','on');
                obj.optim = optimset(obj.optim,'Hessian','on');
            end
            
            if isfield(params,'rmax')
                obj.rmax=params.rmax;
            end
                
            %additional processing
            obj.compeig=false;

            obj.version.BatchML=090111;
        end


    end
    
    methods(Access=protected)
        %access past inputs and observations
       [inputs,obsrv]=getpastdata(uobj,simobj,trial); 
    end
end