%function [inputs,obsrv]=getpastdata(uobj,simobj,trial)
%   uobj- BatchML object
%   simobj - the simulation object
%   trial  - the trial to get the data for
%
%Explanation: Since batchML needs to acces all inputs and observations
%   we store these in transient variables. getpastdata takes care of
%   storing the values correctly and returning them
function [inputs,obsrv]=getpastdata(uobj,simobj,trial)

if (isempty(uobj.simidnum) || uobj.simidnum~=simobj.idnum)
    uobj.inputs=nan(getparamlen(simobj.mobj),simobj.lasttrial);
    uobj.obsrv=nan(getparamlen(simobj.mobj),simobj.lasttrial);
    uobj.simidnum=simobj.idnum;
    uobj.lasttrial=0;
end

if (uobj.lasttrial<trial)
    uobj.inputs(:,uobj.lasttrial+1:trial)=getinputs(simobj,uobj.lasttrial+1:trial);
end


uobj.obsrv(uobj.lasttrial+1:trial)=getobsrv(simobj,uobj.lasttrial+1:trial);

uobj.lasttrial=max(uobj.lasttrial,trial);

inputs=uobj.inputs(:,1:trial);
obsrv=uobj.obsrv(1:trial);