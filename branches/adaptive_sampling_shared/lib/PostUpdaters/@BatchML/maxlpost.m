%function [map,hessian]=maxlpost(uobj,thetainit,po,stim,obsrv,glm)
%   po - Gaussian prior at time 0
%
%Return value:
%   map the value of theta which maximizes the posterior
%   hessian - the hessian at the map
%Explanation:
%   Finds the maximum of the posterior using fminunc
%
%Revision
function [map,hessian]=maxlpost(uobj,thetainit,po,inp,obsrv,glm)

optim=getoptim(uobj);





%**************************************************************************
%Actual Optimization
%**************************************************************************
fun=@(theta)(objfun(theta,po,inp,obsrv,glm));
if isempty(uobj.rmax)
    [map, nlpost, exitflag, fsout,df,d2f]= fminunc(fun,thetainit,optim);

else
%constraints
%use the constraint A theta < b to constrain the value. 
%this can speed up the amount of time it takes to solve for the max value.
finv=getfinv(glm);
muprojmax=finv(uobj.rmax);
A=inp';
b=muprojmax*ones(size(inp,2),1);
Aeq=[];
beq=[];
lb=[];
ub=[];
nonlcon=[];
[map, nlpost, exitflag,fsout, lambda,df,d2f]=fmincon(fun,thetainit,A,b,Aeq,beq,lb,ub,nonlcon,optim);
end
lpost=-nlpost;
grad=-df;
hessian=-d2f;



%make sure we converged
%exitflag==1 it converged
switch (exitflag)
    case {-3,0}
        %potentially try changing initialization point
        fprintf('BSBatchFitGLM/maxlpost: fminunc did not converge after %2.2g Iterations. Exited with message: \n %s',fsout.iterations,fsout.message);
    case {1,2,3}
        %do nothing it terminated ok
    otherwise
        %fprintf('grad: fsolve terminated with message \n %s \n',fsout.message);
end

%error checking
%check derivative is zero
if ((grad'*grad)^.5>10^-7)
    warning('Derivative at MAP is not zero. Magnitude of derivative is %d',(grad'*grad)^.5);
end
%***********************************************************************************************************
%objective function
%**********************************************************************************************************
%we want to maximize the posterior
%but we use fminunc so we need to multiple the values by -1
%
% wmat - Linear mapping applied to x to create theta
function [fval,df,varargout]=objfun(theta,po,inp,obsrv,glm)

lpost=logpost(glm,theta,theta'*inp,obsrv,po);
if (nargout==3)
    [df d2lpost]=d2glm(glm,theta,po,obsrv,inp);

    %multiple by negative 1 because we will call fminunc
    d2f=-1*d2lpost;
    varargout{1}=d2f;
else

    [df]=d2glm(glm,theta,po,obsrv,inp);
end
fval=-1*lpost;
df=-1*df;