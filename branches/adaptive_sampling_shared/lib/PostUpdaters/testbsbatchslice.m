%**********************************************
%05-19-2008
%***************************************************
%Test bsbatchslice

clear variables;
%*******************************************************
%verify that BSBatchSlice/dlpostonslice.m is accurate. 
%*******************************************************
setpathvars;

datedir='080508';
setupfile='bsglmfit_setup_002.m';
xscript(fullfile(RESULTSDIR,'bird_song/fakedata',datedir,setupfile));
data=load(getpath(dsets.datafile));
rdata=load(getpath(dsets.rawdatafile));

fakemodel=rdata.fakemodel;
mobj=fakemodel.mobj;

%****************************************************
%create the upbdate object
bpost=BSlogpost('bdata',data.bdata,'windexes',2,'maxrepeats',2);
uobj=BSBatchSlice('bpost',bpost);

allpost=data.allpost;

prior=getpost(allpost,0);




%values at which to run the test
%w=zeros(getdim(prior),1);
%w(1,1)=1
w=normmag(randn(getparamlen(mobj),1));

%thetainit=zeros(getparamlen(mobj),1);
thetainit=normmag(randn(getparamlen(mobj),1));

info=compsliceprereq(uobj,thetainit,w,mobj,prior);

%********************************************
%BSBatchSlice uses dlpostonslice to compute the gradient along the slice.
%We check that this function returns the same value as compdlpost

%dw - how far of a step should we take to evaluate the gradient
dw=[-.1 0 .1];

for dind=1:length(dw)
    theta=thetainit+dw(dind)*w;

    %compute the value using dlpost
    [fd(dind).dlpost,fd(dind).d2lpost]=compd2lpost(bpost,mobj,theta,prior);
    
    %compute the value using dlpostonslice
    compderv=true;
    r=dlpostonslice(uobj,dw(dind),w,mobj,info,compderv);
    
    donslice(dind).dpost=r.dpost;
    
    if (abs(fd(dind).dlpost'*w-donslice(dind).dpost)>10^-8)
       error('derivative is not correct'); 
    end
end

fprintf('Derivative on the slice is computed correctly');