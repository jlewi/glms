%function [dtmin,dtmax]=lincon(uobj,postlast,inp)
%   mut - the current map
%   du  - C_t* inp/ || C_t * inp||
%   dumag - ||C_t * inp||
%   inp - the input
%
%Explanation: COmputes the maximum and minimum value of the step size to
%ensure that for the new MAP the maximimum firing rate constraint isn't
%violarted
function [dtmin,dtmax]=lincon(uobj,mut, du,dumag,mobj,bdata)

%maximum value of glmproj
finv=getfinv(mobj.glm);
if isempty(uobj.muprojmax)
    uobj.muprojmax=finv(uobj.rmax);
end

%compute the value of muproj for mu_t for all stimuli in uobj.windexes
%the following line is a bit of a hack to map theta into the same domain as
%the spectrogram
if isa(mobj,'MBSFTSep')
    theta=mobj.fullbasis*mut;
else
    error('Code needs to be updated to handle models other than MBSFTSep.');
end

if isempty(uobj.bpost)
    uobj.bpost=BSlogpost('bdata',bdata,'windexes',uobj.windexes,'maxrepeats',1);
end

glmproj=cell(1,length(uobj.windexes));

for wind=1:length(uobj.windexes)
    [glmproj{wind}]=compglmprojnoshist(uobj.bpost,uobj.windexes(wind),mobj,theta);
end

glmproj=cell2mat(glmproj);


%compute the cross product between the the stimuli used for the linear
%constraint and the actual input
if isa(mobj,'MBSFTSep')
    inprot=mobj.fullbasis*dumag*du;
else
    inprot=dumag*du;
end

inpproj=cell(1,length(uobj.windexes));

for wind=1:length(uobj.windexes)
    [inpproj{wind}]=compglmprojnoshist(uobj.bpost,uobj.windexes(wind),mobj,inprot);
end
inpproj=cell2mat(inpproj);

fbound=(uobj.muprojmax-glmproj)*dumag./abs(inpproj);

%upper boundary is determined by elements where inpproj is positive


dtmax=min(abs(inpproj));
dtmin=-dtmax;

if isinf(dtmax)
    error('dtmax and dtmin are infinite');
end

if (dtmax==0)
    warning('dtmax and dtmin are zero');
end
% if ~isempty(ind)
%     dtmax=min(fbound(ind));
% else
%     dtmax=inf;
%     fprintf('lincon: dtmax is inf \n');
% end
% 
% ind=find(inpproj<0);
% 
% if ~isempty(ind)
%     dtmin=max(fbound(ind));
% else
%     dtmin=-inf;
%     fprintf('lincon: dtmin is -inf \n');
% end




