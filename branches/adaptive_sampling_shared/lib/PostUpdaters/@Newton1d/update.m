%function [post,]=update(uobj,postlast,stim,mobj,obsrv)
%       postlast - the most poster after the previous observation
%       stim - full stimulus - spike history and stimulus part
%           - also stimulus after any input transformations
%       obsrv- observation
%
% Return value
%   post - the new posterior
%   post.uinfo - information about the update
%       .eigtime - time for the rank 1 updat of the eigendecomposition
%       .ndeflate - number of deflated eigenvalues if we computed the
%       eigendecomposition.
%
%   WARNING: post.c and einfo will not always match
%       This is because post.c is computed using sherman-morrison formula
%       einfo is computed if possible using Gu and Eisenstat
%       THis results in slight numerical differences between the post.c and
%       the covariance matrix constructed from the eigendecomposition. Over
%       many iterations these differences accumulate.
%
%
%%%Explanation:
%   This function can perform 1 step or multiple steps. If x is a matrix
%   and obsrv.n is a matrix then each column specifcies the stimuus and
%   observation on that trial
%
%10-30-2008:
%   A major revision of the update code. I simplified the update step in
%   order to make it more stable.
%04-09-2008
%   Do not set the options structure in the update function
%       this turns out to be really expensive. Instead we store the options
%       as part of the base class so we only have to set the options once.
%       Options has been a part of the base class for a while but for some
%       reason I'm still setting options here.
%02-25-2008
%   If the stimulus has no power then the posterior is unchanged.
%       b\c for all theta theta'*x= 0. I.e all models make the same
%       prediction so our uncertainty is unchanged
%11-26-2007: take simobj and trial as inputs
%11-18-2007:
%   fixed bug when we we take more than 1 step.
%
%Revision 1585: 10-19-2007
%       merged with code from postnewton1d.m
%       To handle one sided nonlinearities we need to make sure our initial
%
%7-23-2007:
%   Partial fix of numerical issue due to using Sherman-Morrison Formula
%   and the Gu-Eisenstat formula
% Revision: 1485 : save eigtime as part of einfo.
%           use EigObj -to handle the rank 1 update
%
% Revision: 1485 : 06-25-2007
%       Use the optim structure stored in the base class
%
%03-18-2007
%       1. GLM is no longer passed in as input
%       2. Test if mobj is derived from MGLMNonLinInp
%                   if so transform the input
%
%
%
%
%     guess for glmproj>=0 if obsrv is >0.
%
%Revision: 1485  - changed how we vary the initialization point when it doesn't
%   converge. setting ainit+aopt/2 - was a bad choice because this may not
%   move the initialization point in direction we need to go.
%
function [post]=update(uobj,postlast,stim,mobj,allobsrv,simobj,trial)
glm=getglm(mobj);

options=getoptim(uobj);

%observation is just the latest observation
%check if we need to nonlinearly transform the input
%10-29-2007: The input should already be transformed.
% if isa(mobj,'MGLMNonLinInp')
%     stim=projinp(mobj,stim);
% end


% iteration
uinfo=[];
uinfo.niter=[];
uinfo.ntime=[];

nsteps=size(allobsrv,2);            %how many steps to take
a=zeros(1,nsteps);
tic
for sind=1:nsteps
    %input vector
    inp=stim(:,sind);
    if isfield(allobsrv,'n')
        obsrv=allobsrv.n(:,sind);
    else
        obsrv=allobsrv(:,sind);
    end
    xmag=(inp'*inp)^.5;

    %*************************************************
    %special case: xmag=0;
    %**************************************************
    %if the stimulus has zero magnitude the posterior is unchanged
    if (xmag==0)
        pnew=postlast;
    else
        %***************************************************************
        %compute the quantities that we will need to solve for the step
        %size
        %*************************
        %du - is a unit vector in the direction of the update
        du=rotatebyc(postlast,inp);
        
        dumag=(du'*du)^.5;
        du=du/dumag;

        if(dumag<10^-12)
            warning('The magnitude of the direction of the update is very small this could potentially cause numerical issues.');
        end
            
        
        %sigmaeps is the constant which scales dstep when computing the
        %change in glmproj
        sigmaeps=dumag*inp'*du;
        
        %use the appropriate function of the model to compute mueps
        mueps=inp'*getm(postlast);        %only compute once
        
        aopt=[];
        %10-19-2007
        %       To handle one sided nonlinearities we need to make sure our initial
        %       guess for glmproj>=0 if obsrv is >0.
        % if post.m'*x<=0 and obsrv=0 there is no information in the responses
        %   we leave the mean unchanged. our uncertainty does not decrease
        %   because 2nd derivative is zero. Not sure if this is accurate
        %   because this trial should provide evidence that our estimate is
        %   correct.
        if (isonesided(glm))
            if ((mueps<0)&&(obsrv==0))
                %special case
                %post.m'*x<=0 and obsrv=0
                %how should we update?
                %the mean should be unchanged
                %leave our uncertainty unchanged

                %the mean is unchanged.
                %prevents loop.
                %leave the posterior unchanged
                % pnew=GaussPost('m',getm(postlast),'c',getc(postlast));

                ntime=toc;
                aopt=0;
                niter=0;
            else
                %do a binary search
                %avoid a newton search because we have to keep u positive which
                %is a pain
                abthresh=options.TolX;
                [aopt,aleft,aright,ferr,niter]=binarysearch(uobj,mueps,sigmaeps,dumag,obsrv,glm,abthresh);
            end
        else
            %its not a one-sided nonlinearity do regular newtonsearch
            [aopt,niter]=newtonsearch(uobj,mueps,sigmaeps,dumag,obsrv,glm,simobj.statusfid,simobj.niter+1);
        end

        %error checking
        if isnan(aopt)
            error('Newton1d update: aopt is nan');
        end
        if isinf(aopt)
            error('Newton1d: aopt is inf');
        end
        %*************************************************************
        %having solved the 1-d eqn we can compute the updated mean
        %********************************************************************
        %for stability limit the magnitude of aopt to 1
%         maxstep=10^-1;
%         if (abs(aopt)>10^-1)
%            fprintf('aopt is >%d will shrink aopt to %d. \n',maxstep,maxstep); 
%            aopt=sign(aopt)*maxstep;
%         end
        %compute the updated mean
        mnew=getm(postlast)+aopt*du;

      
        %make sure mnew is a double
        %mnew might not be a double because aopt could be an arbitrary
        %precision number
        mnew=double(mnew);

        %error checking
        if isnan(mnew)
            error('new mean is nan');
        end
        %******************************************************************
        %compute the updated covariance matrix
        %******************************************************************
        %If we're computing the eigenvectors
        %then we don't use the woodbury lemma because the numerical errors
        %build up

        %indicates whether to use woodbury
        if (~uobj.compeig)
            [gd1, gd2]=d2glmeps(glm,mnew'*inp,obsrv);
            v=rotatebyc(postlast,inp);
            q=compquad(postlast,inp);

            cnew=getc(postlast)-v*(-gd2/(1-gd2*q))*v';
            
            %add a small amount to the diagonal to ensure covariance matrix
            %stays valid
            %cnew=cnew+diag(eps*1000);
            
            a(sind)=aopt;


            %check if the new covariance matrix is no longer positive along
            %the diagonal. If thats the case then we use the
            %eigendecomposition to get better accuracy
            if any(diag(cnew)<0)
                error('Woodbury lemma produces covariance matrix  which is no longer positive definite. Use eigen update.');
            end
            
                %make sure mnew and cnew are doubles
                %They could potentially be mp objects and that will slow down
                %subseqeunt operations and possible cause errors
                pnew=GaussPost('m',double(mnew),'c',double(cnew));

            

        else
            %******************************************************************
            %Compute the Eigenvectors and the covariance matrix
            %****************************************
            %compute the covariance matrix from the eigendecomposition

            %*******************************************************
            %efficiently compute the eigenvectors
            %*******************************************************
            [einfo oinfo]=compceig(uobj,postlast,mnew,inp,obsrv,glm,simobj.statusfid);
                       
            if ~isempty(oinfo)
                uinfo.eigtime=oinfo.eigtime;
                uinfo.ndeflate=oinfo.ndeflate;
            end

             %new posterior
            pnew=GaussPost('m',mnew,'c',einfo);

            pnew=setuinfo(pnew,'uinfo');

            %compute the entropy
            entropy=compentropy(uobj,pnew);
            pnew=setentropy(pnew,entropy);
            %*************************************************************
            %error checking
            %*********************************************************
            if (getdebug(uobj))
                %print the max difference between the reconstructed covariance and actual covariance
%                 mdiff=max(max(abs(getc(postlast)+rho*u*u'-getmatrix(einfo))));
%                 fprintf('Postnewton update: max difference eigen reconstruct %d \n',mdiff);
% 
%                 xmax=stim(1:getstimlen(mobj));

                if ~isempty(find(isnan(einfo.evecs)))
                    warning('Eigenvectors not defined');
                    warning('Computing Eigenvectors via SVD');
                    c=getc(pnew);
                    [einfo.evecs einfo.eigd]=svd(c(1:getstimlen(mobj),1:getstimlen(mobj)));
                    einfo.eigd=diag(einfo.eigd);
                    einfo.esorted=-1;   %eigenvalues are in descending order
                end
            end

            %**************************************************************
            %compute the eigendecomposition of just the submatrix for the
            %stimuli
            %************************************************************
            if (getstimlen(mobj)>0)
                if ((getslen(postlast))<=0)
                    %we cannot compute the eigendecomposition efficiently
                    %because w don't have the eigendecomposition from the last
                    %iteration.
                else
                    %we only need to do this if we are trying to maximize
                    %with spike history under a power constraint
                    xmax=stim(1:getstimlen(mobj));
                    shist=stim(getstimlen(mobj)+1:end);
                    %10-19-2007
                    %We just want the eigendecomposition
                    %of the stimulus terms
                    %To avoid numerical issues we need to compute the
                    %covariance matrix from the
                    %eigendecomposition of the full matrix instead of using
                    %woodbury lemmma
                    c=getc(postlast);

                    error('Code to compute eigendecomposition of just the stimulus terms is not implemented');
                    u=c(1:getstimlen(mobj),1:getstimlen(mobj))*xmax+c(1:getstimlen(mobj),getstimlen(mobj)+1:end)*shist;
                end
            end
        end %compute c via woodbury or eigdecomposition.
    end %check if xmag=0
    post=pnew;
    ntime=toc;
    %update the posterior so on next iteration we take step from current
    %posterior
    if (nsteps>1)
        %            if (wopts.all~=0)
        %               pall(sind)=pnew;
        %          end
        postlast=pnew;

    end
end %loop over all steps