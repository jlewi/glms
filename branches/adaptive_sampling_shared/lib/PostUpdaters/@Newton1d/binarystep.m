%function [aleft,aright,ferr]=binarystep(obj,aleft,right,muproj,sigma,dumag,obsrv,glm)
%   muproj- projection of the old mean on the input
%   sigma - inp' C_t * inp/ ||C_t*inp||
%   dumag -||C_t*inp||
%Explanation: Takes one binary step
%
%Return value:
%   anew - the new value of the gain term
%   aerr - the change in a
%   ferr - the value of the function whose root we are trying to find
function [aleft,aright,anew,la]=binarystep(obj,aleft,aright,muproj,sigma,dumag,obsrv,glm)

%initialize the left and right boundaries if they aren't provided
if (isinf(aleft) || isinf(aright))
    [aleft,aright]=binaryinit(obj,muproj,sigma,dumag,obsrv,glm);
end

  anew=(aleft+aright)/2;
    la=stepsizefun(obj,anew,muproj,sigma,dumag,obsrv,glm);
    %assume la is monotonically increasing 
    if (la<0)
        aleft=anew;
    else
        aright=anew;
    end
    
