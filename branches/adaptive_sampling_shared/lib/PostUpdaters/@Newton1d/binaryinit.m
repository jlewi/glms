%function [aleft,aright]=binaryinit(obj,muproj,sigma,dumag,obsrv,glm)
%
%Return value:
%   aleft, aright - initial guesses of aleft and aright to ensure the zero
%       occurs on the interval [aleft, aright];
%
%Revisions:
% 10-30-2008: Use the new version of stepsizefun. One of the boundaries
%   should always be a=0. The objective function should always be
%   increasing with a.
%
function [aleft,aright]=binaryinit(obj,muproj,sigma,dumag,obsrv,glm)

ainit=0;


%one sided glm's are initialized differently b\c left boundary is always
%zero
if (isonesided(glm))
    error('I need to update the code to initialize the search for one sided glms. 10-30-2008');
    %*********************************************************
    %one sided glm's
    %*******************************************************
    %aleft is value such that unew is zero.
    aleft=-d*xmag/c;

    %sign of la here
    u=d+aleft*c/xmag;  %should be zero

    %u=0 is always left boundary

    %We need to initialize the right boundary to a point for which
    %la >0.
    %our objective function, binaryobjpost, is expressed
    %so that as a-->to infinity la becomes positive.
    %i.e la is monotonically increasing with a
    %therefore to get the right boundary we always increase 
    %a until la its positive
    %see my notes on Newton1d-->binarysearch
    aright=aleft+1*abs(aleft);

    saright=-1;
    while (saright<0)
        aright=max(aright+abs(aleft),aright*10);

        u=d+aright*c/xmag;  %should be zero
        [laright]=binaryobjpost(obj,aright,d,c,xmag,obsrv,glm);
%        [gd1]=d2glmeps(glm,u,obsrv);
 %       laright=-aright/xmag+gd1;
        saright=sign(laright);
    end


else

    %check if obsrv>fglmmu. if it is then left boundary is 0
    if (obsrv>fglmmu(glm,muproj))
        aleft=0;
       
        %to get a tight boundary we could find the the step size needed to
        %make
        %obsrv=fglmmu(glm,muproj+aright*sigma/dumag);
        %However, since fglmmu is at least linear we can solve a linear
        %equation directoly to get an upper bound
        aright=(obsrv-muproj)*dumag/sigma;
    else
        aright=0;
        
        %two cases obsrv=0 and not 
        if (obsrv==0)
           aleft=min([-1.001,(-log(dumag)-muproj)*dumag/sigma]);
        else
            %use fact that fglmmu is at most exponential to derive lower
            %boundary
            if ~isinf(fglmmu(glm,muproj))
                aleft=(log(obsrv)-fglmmu(glm,muproj))*dumag/sigma;
            else
               %if the exponential function blows up because muproj is large
               %then we set aleft such that muproj+aleft*sigma/dumag==0
               %we then multiple by 10 until aleft is negative
               aleft=-muproj*dumag/sigma;
               while(stepsizefun(obj,aleft,muproj,sigma,dumag,obsrv,glm)>0)
                aleft=aleft*10;
               end
            end
        end
    end
    %make sure the sign is correct
    saleft=stepsizefun(obj,aleft,muproj,sigma,dumag,obsrv,glm);
    
    if (saleft>0)
        error('Left most boundary does not map to a negative value of our objective function');
    end
    saright=stepsizefun(obj,aright,muproj,sigma,dumag,obsrv,glm);
    if (saright<0)
        error('right most boundary does not map to a positive value of our objective function');
    end

end