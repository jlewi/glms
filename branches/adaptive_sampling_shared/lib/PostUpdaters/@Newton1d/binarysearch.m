%function [anew,aleft,aright,dlda]=binarysearch(d,c,xmag,obsrv,glm,abthresh,aleft,aright)
%
%   abthresh - threshold for stopping the binary search
%   aleft,aright  - initial boundaries for the search
%                 - if these are unknown set to -inf and inf and we will set them
%                 appropriately
%
%
% Explanation:
%       Do a binary search to find the root of the 1-d equation
%
%
% Warnings:
%    Function assumes nonlinearity is log-concave. If its not and its one
%    sided the initialization will fail.
% Bug fix:
%   11-18-2007
%       fixed initialization for non-one-sided glm. Takes advantage of the
%       fact that our objective function is always increasing with a.
%   10-19-2007: I was searching for dlda=0. In reality we want to find
%   la=0. Right?
function [anew,aleft,aright,la,nbiter]=binarysearch(obj,muproj,sigma,dumag,obsrv,glm,abthresh,aleft,aright)

%initialize the left and right boundaries if they aren't provided
% if (isinf(aleft) || isinf(aright))
%     [aleft,aright]=binaryinit(obj,muproj,sigma,dumag,obsrv,glm);
% end
% 
% if (abs(aleft-aright)<abthresh)

%always take at least one step to compute anew even if
%aright-aleft<abthresh
     [aleft,aright,anew,la]=binarystep(obj,aleft,aright,muproj,sigma,dumag,obsrv,glm);
% end
%     

nbiter=0;
while ((abs(aleft-aright)>abthresh) && (nbiter <obj.maxiter))
    nbiter=nbiter+1;
  
    [aleft,aright,anew,la]=binarystep(obj,aleft,aright,muproj,sigma,dumag,obsrv,glm);
end


if (nbiter==obj.maxiter)
    warning('Newton1d BinarySearch: Max Iterations Reached, \n \t aerr=%d',abs(aleft-aright));
end

