%function[pnew,niter,ntime,a]=postnewton1d(obj,post,allx,allobsrv,glm,optim,wopts)
%       post - structure representing the gaussian posterior from the
%              previous time step or newton step
%               post.m is point at which we take a newton step from
%               post.c determines how large a step to take
%                       -this is the covariance matrix which is taken to be
%                       the negative inverse of the hessian.
%               .invc - this is the inverse of post.c
%                       -if this exists then we don't need to compute it
%       x     - column vector representing the stimulus
%               dimstimxt
%               -observations on each trial upto this time point
%       obsrv - struture representing the observations
%              .n - observation (number of spikes if glm is poisson)
%       `           1xt - (THIS IS NO LONGER NECESSARY BUT ITS MAINTAINED
%       FOR BACKWARDS COMPATIBILITY 12-27)
%              .twindow -window in which spikes are occured (THIS IS NO
%              LONGER USED TWINDOW SHOULD BE HANDLED BY THE GLMCF
%              STRUCTURE)
%
%       glmcf -  structure
%               .fglmnc - pointer to function which computes the
%                               normalizing value as a function of the canonical parameter
%                               this is determined by the choice of[pnew,niter,ntime]=postnewton1d(post,allx,allobsrv,glm,tolerance,wopts)
%                               distribution
%               .fglmmu  -pointer to function which computes the canonical
%                                    parameter as a function of the input
%                               input - x^t\theta
%                               output - mean
%                               this is the link function
%                               it is not determined by the choice of
%                               distribution
%               .fglmetamu - computes the canonical parameter as function
%                                      of mean
%                               input - mean
%                               output -canonical parameter
%                                this is determined by the choice of
%                                distribution
%       optim - optimization parameter structure to pass to fsolve
%       maxdm - this is the max amount the posterior mean can change along
%               any dimension in a single update. This is meant to prevent
%               instability due to a horizontal asymptote.
%
%      wopts - options about warnings
%           .warnings =0 don't print warning messages
%           .all             = 1 if this is one then pnew is a cell array
%                                 with the posterior otherwise pnew is just
%                                 the last poster
%Return value:
%   pnew - gaussian representing the updated posterior
%       .m - mean
%       .c - covariance
%            - this is an array of the posteriors after each time step if
%            wopts.all=1;
%   niter  - number of iterations
%   ntime - time for newton update
%   a        - the step size on each iteration
%Explanation:
%   when the posterior is just a rank one modification of the posterior
%   from the previous time step we can solve a 1-d optimization problem
%   using Newton's method rather than take a full newton step
%
%   This function can perform 1 step or multiple steps. If x is a matrix
%   and obsrv.n is a matrix then each column specifcies the stimuus and
%   observation on that trial
%
%
%Revision 1585: 10-19-2007
%       To handle one sided nonlinearities we need to make sure our initial
%       guess for glmproj>=0 if obsrv is >0.
%
%Revision: 1485  - changed how we vary the initialization point when it doesn't
%   converge. setting ainit+aopt/2 - was a bad choice because this may not
%   move the initialization point in direction we need to go.
%
%Revision: 1455  - Changed tolerance to an optim structure which gets passed to
%               tolerance (06-18-2007) changes not complete.
%           -handle exitflag=-2
function [pnew,niter,ntime,a]=postnewton1d(obj,post,allx,allobsrv,glm)

options=getoptim(obj);

if ~exist('wopts','var');
    wopts=[];
end
if ~isfield(wopts,'all')
    wopts.all=0;
end
%set default options if it doesn't exist
if ~exist('options','var')
    options=[];
    options =optimset(options,'Jacobian','on');
    options=optimset(options,'Display','off');
end

if isempty(optimget(options,'TolX'))
    options=optimset(options,'TolX',10^-9);
end

if isempty(optimget(options,'TolFun'))
    options=optimset(options,'TolFun',10^-9);
end

