%function nobj=Newton1d(varargin)
%
%Explanation: base class for the newton1d updater.
%
%Constructors
%   Newton1d()
%   Newton1d('compeig',val)
%           compeig - indicates whether or not to compute the
%           eigendecomposition of the covariance matrix
%
%Revisions
%   08-01-2009- version is now a structure defined in PostUpdaters base
%   07-30-2008: us Matlab new OOP model.
classdef (ConstructOnLoad=true) Newton1d <PostUpdatersBase


    properties (SetAccess=private,GetAccess=public)

        maxiter=1000;

    end
    methods
        function nobj=Newton1d(varargin)
            con.rparams={};
            con.cfun=1;
            %




            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
                    params=[];

             
                otherwise
                    [cind,params]=constructid(varargin,con);
            end


            %*************************************************
            %construct the base class
            %************************************************
            bparams=params;
            if isfield(bparams,'maxiter')
                bparams=rmfield(bparams,'maxiter');
            end


            nobj=nobj@PostUpdatersBase(bparams);
            if isfield(params,'maxiter')
                nobj.maxiter=params.maxiter;
            end
            
            %10-30-2008
            %We are now optimizing the size the change in the mean
            %thus we can set the tolerance more liberally because 
            %if the tolerance is less than 10^-8 we know we are taking
            %almost no step.
            nobj.optim=optimset(nobj.optim,'TolFun',10^-8);
            nobj.optim=optimset(nobj.optim,'TolX',10^-12);
            
            obj.version.Newton1d=090108;
        end

 
    end
end