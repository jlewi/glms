%function getobsrv=obsrv(obj)
%	 obj=SRObj object
% 
%Return value: 
%	 obsrv=obj.obsrv 
%
function obsrv=getobsrv(obj)
	 obsrv=[obj.obsrv];
