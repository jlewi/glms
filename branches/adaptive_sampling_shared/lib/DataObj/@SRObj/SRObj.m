%function sim=SRObj
%
%function SRObj(input,obsrv)
%   you don't need to specify strings identifying the parameters
%   this should speed things up when constructing lots of them
%
%01-08-2008
%   make verison a structure
%12-31-2008
%   derived this class from handle
%
classdef (ConstructOnLoad=true) SRObj <handle

    properties(SetAccess=public,GetAccess=public)
        version=struct();
        input=[];
        obsrv=[];
        stiminfo=[];

    end

    methods
        function obj=SRObj(varargin)
            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call


            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.

            con(1).rparams={'sr'};


            con(2).rparams={'input','obsrv'};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case {1,2}
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    
                %convert an sr structure
             
                       sr=params.sr;
                obj.input=sr.y;
                obj.obsrv=sr.nspikes;
                obj.stiminfo=sr.stiminfo;
                case 2
                    obj.input=params.input;
                    obj.obsrv=params.obsrv;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %set the version to be a structure
            obj.version.SRObj=090108;
        end
       

        %
    end
end




