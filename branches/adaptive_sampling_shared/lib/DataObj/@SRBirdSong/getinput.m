%function getinput=input(obj)
%	 obj=SRObj object
% 
%Return value: 
%	 input=obj.input 
%
function data=getinput(obj)
    %for each input we create a GLMMatrix object representing the stimulus
    for index=1:length(obj)
        sindex=getsindex(obj(index));
        
      
       %get the spectrogram
       %because we use pointers if the spectrogram is computed it will be
       %saved
       [obj(index).ptrbsdata.bsdata, stimspec]=getstimspec(obj(index).ptrbsdata.bsdata,getsindex(obj(index)));
       
       [obj(index).ptrbsdata.bsdata,nincrements]=getnincrements(obj(index).ptrbsdata.bsdata);
       
       m=zeros(size(stimspec,1),nincrements);
       
       %if startframe <=0 then the first entries in m correspond to
       %the pre sound silence
       mstart=1;
       startframe=obj(index).startframe;
       if (startframe<=0)
          mstart=mstart-startframe+1;
          startframe=1;       
       end
       
       endframe=obj(index).startframe+nincrements-1;
       mend=nincrements;
       
       %if endframe is longer than size(stimspec,2) then final entries
       %correspond to the silence following the stimulus
       if (endframe>size(stimspec,2))
           endframe=size(stimspec,2);
           mend=mstart+(endframe-startframe);           
       end
       m(:,mstart:mend)=stimspec(:,startframe:endframe);
       data(index)=GLMMatrix('matrix',m);
     
    end
