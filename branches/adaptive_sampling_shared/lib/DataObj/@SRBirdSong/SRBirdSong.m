%function sim=SRBirdSong('sindex','startframe','ptrbsdata')
function obj=SRBirdSong(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'sindex','startframe','ptrbsdata'};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
% sindex    - this is the index of the sound in bsdata to which the
%              stimulus corresponds to
%startframe -This is the first frame in the spectrogram of the stimulus on this trial
% This can be negative or greater than the number of frames in the spectrogram of the stimulus. These values correspond to silences before and after the stimulus.
%ptrbsdata - Pointer to the bsdata object
% bsdatainfo -this is a structure which we use to make sure the bsdata assighned to the SRObject is the correct one (see below on saving and loading the object)
%            - 03-26-2008 THis structure is not currently being used
%declare the structure
obj=struct('version',080326,'sindex',[],'startframe',[],'ptrbsdata',[],'bsdatainfo',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'SRBirdSong',SRObj());
        return
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        %create an array of objects if params is an array
        version=obj.version;

        %remove fields which belong to base class
        %obsrv
        obsrv=[params.obsrv];
        params=rmfield(params,'obsrv');

        s=params;
        %obj=repmat(params,size(params));
        %obj.sindex=params.sindex;
        %obj.startframe=params.startframe;
        %obj.ptrbsdata=params.ptrbsdata;
        for index=1:numel(params)
            s(index).version;
        end

        %order the fields in the same order as the structure declaration
        %otherwise we will get error about fieldnames changing
        s=orderfields(s,obj);
        %*********************************************************
        %Create the object
        %****************************************
        %instantiate a base class if there is one
        %if no base object
        obj=class(s,'SRBirdSong',SRObj());

        %set the observation if one is specified
        obj=setobsrv(obj,obsrv);

    otherwise
        error('Constructor not implemented')
end


%optional parameters
%if isfield(params,'bsdatainfo')
%    obj.bsdatainfo=params.bsdatainfo;
%end



