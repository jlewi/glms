%11-11-2005
%A test script for the function ll1dtemp

%A sample of data
%generate multiple filters
%each column of k is a different model
k=[1:5;5:-1:1];
%k=[1 2;5 2];
%k=[2;2];

x=[1 1];
dt=.1;
y=[1,0,1,0,1,0];

%number of different observations we are computing likelihood of 
nobsrv=size(x,1);
%number of models to consider each data point under
nmodel=size(k,2);

lltrue=zeros(nobsrv,nmodel);
llpoisson=zeros(nobsrv,nmodel);

%************************************************************************
%Compute by brute force
%*************************************************************************
%compute the likelihood and verify its correct for each trial
%The purpose of this test is to make sure all the fancy indexing
%to simultaneously compute multiple trials under multiple models is correct
for iobsrv=1:nobsrv
    for imodel=1:nmodel

        r=exp(x(iobsrv,:)*k(:,imodel));
        r=r*ones(1,size(y,2));
        %probability of getting spikes 
        sind=find(y(iobsrv,:)==1);        %indexes of spikes
        numspikes=size(sind,2);
        nosind=find(y(iobsrv,:)==0);      %indexes when spikes did not occur
        
        %probability of a spike at each time pt
        pspike=dt*r;
        %check if pspike is >1
        ind=find(pspike>=1);
        if (~isempty(ind))
            fprintf('Warning: probability of spike >1 \n');
            %set the probability = 1 -eps to avoid log of 0 errors
            pspike(ind)=1-eps;
        end
        ps=sum(log(pspike(sind)),2);
        pno=sum(log(1-pspike(:,nosind)),2);
        
        lltrue(iobsrv,imodel)=1/(factorial(numspikes))+ps+pno;
        
        llpoisson(iobsrv,imodel)=poissonll(y(iobsrv,:),r,dt);
    end
end
%*************************************************************************
%compute it using the function
%*************************************************************************
%now compute it using the function
llfunc=ll1dtemp(y,x,k,dt);

%fprintf('lltrue=%d \t llfunc=%d',lltrue,llfunc);



