%function refinemu(dotmu)
%       dotmu - additional values of mu at which to precompute the dot
%       product
%
% Explanation: allows new values of mu to be added to the grid 
function [pobj]=refinesigmasq(pobj,sigmasq)

   sigmasq=sort(sigmasq,'ascend');
   
   %remove any values from sigmasq if we've already computed them
   for i=1:length(sigmasq)
       if ~isempty(find(pobj.sigmasq==sigmasq(i)))
           sigmasq(i)=NaN;
       end
   end
   ind=find(~isnan(sigmasq));
   sigmasq=sigmasq(ind);
   
   
   %compute the values of the objective for all values of dotmu
   nmi=zeros(length(pobj.dotmu),length(sigmasq));
   nmi=compmi(pobj,pobj.dotmu,sigmasq);
   
   
   olen=length(pobj.sigmasq);        %original length
   ntoadd=length(sigmasq);        %number to add
   newfmi=zeros(length(pobj.dotmu),olen+ntoadd);
   newsigmasq=zeros(olen+ntoadd,1);
   %now we need to merge the arrays
   %we do this as a merge sort
   indold=1;
   indnew=1;
   
   fullind=1;
   while (indold<=olen && indnew <=ntoadd)
    if (sigmasq(indnew)<pobj.sigmasq(indold))
        newsigmasq(fullind)=sigmasq(indnew);
        newfmi(:,fullind)=nmi(:,indnew);
        indnew=indnew+1;
    else
         newsigmasq(fullind)=pobj.sigmasq(indold);
        newfmi(:,fullind)=pobj.mi(:,indold);
        indold=indold+1;
    end
        fullind=fullind+1;
   end
    
   if (indold<=olen)
       newsigmasq(fullind:end)=pobj.sigmasq(indold:end);
       newfmi(:,fullind:end)=pobj.mi(:,indold:end);
   end
   if (indnew<=ntoadd)
       newsigmasq(fullind:end)=sigmasq(indnew:end);
       newfmi(:,fullind:end)=nmi(:,indnew:end);
   end
   
   pobj.sigmasq=newsigmasq;
   pobj.mi=newfmi;