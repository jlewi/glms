
%function refinemu(dotmu)
%       dotmu - additional values of mu at which to precompute the dot
%       product
%
% Explanation: allows new values of mu to be added to the grid 
function [pobj]=refinedotmu(pobj,dotmu)

   dotmu=sort(dotmu,'ascend');
   
    
   %remove any values from sigmasq if we've already computed them
   for i=1:length(dotmu)
       if ~isempty(find(pobj.dotmu==dotmu(i)))
           dotmu(i)=NaN;
       end
   end
   ind=find(~isnan(dotmu));
   dotmu=dotmu(ind);
   
   
   %compute the values of the objective for all values of sigma
   nmi=zeros(length(dotmu),length(pobj.sigmasq));
   nmi=compmi(pobj,dotmu,pobj.sigmasq);
   
   
   oldmu=length(pobj.dotmu);        %original length
   ntoadd=length(dotmu);        %number to add
   newfmi=zeros(oldmu+ntoadd,length(pobj.sigmasq));
   newdotmu=zeros(oldmu+ntoadd,1);
   %now we need to merge the arrays
   %we do this as a merge sort
   indold=1;
   indnew=1;
   
   fullind=1;
   while (indold<=oldmu && indnew <=ntoadd)
    if (dotmu(indnew)<pobj.dotmu(indold))
        newdotmu(fullind)=dotmu(indnew);
        newfmi(fullind,:)=nmi(indnew,:);
        indnew=indnew+1;
    else
         newdotmu(fullind)=pobj.dotmu(indold);
        newfmi(fullind,:)=pobj.mi(indold,:);
        indold=indold+1;
    end
        fullind=fullind+1;
   end
    
   if (indold<=oldmu)
       newdotmu(fullind:end)=pobj.dotmu(indold:end);
       newfmi(fullind:end,:)=pobj.mi(indold:end,:);
   end
   if (indnew<=ntoadd)
       newdotmu(fullind:end)=dotmu(indnew:end);
       newfmi(fullind:end,:)=nmi(indnew:end,:);
   end
   
   pobj.dotmu=newdotmu;
   pobj.mi=newfmi;