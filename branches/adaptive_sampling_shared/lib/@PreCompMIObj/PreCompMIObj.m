%function pobj=PreCompMIObj('filename',fname)
%   -load the object from the specified file
%
%function pobj=PreCompMIObj('filename',fname,'dotmu',mu,'sigmasq',sigma,'glm',glm,'mifunc')
%       fname - name of file to save object to
%      dotmu      - value of mu'*stim at which to compute objective
%      function
%      sigmasq - value of \sigma^2 at which to compute objective function
%      glm          - general linear model for which we compute the objective function
%      mifunc     - pointer to function to compute mutual information
%                         should have signature
%                          mifunc(dotmu,sigmasq,glm,mifopts);
%      mifopts  - optional paramters to pass to mifunc
%                           these are stored so that later on we know what
%                           parameters were used to compute the function
%     Precomputes the objective function on the specified values of
%     (dotmu,sigmasq) and then saves it to a file.
%
%    Parameters are specified as name value pairs so they can be specified
%    in any order
function pobj=PreCompMIObj(varargin)


%ver internal object to keep track of class version so when we load the
%object we can handle any conversion
%debug - turns on some extra error code
%numtol - numerical tolerance for deciding things are equal
%ocases  - a structure to assign numeric values to different strings (the
%           fieldnames)
%               - these numbers describe the different ways the stimulus
%               might have been chosen. The corresponding number is
%               returned by choose stim
%               this is intended as a bit field
%               -numeric values therefore correspond to the bit to set to
%               true
ocases.normal=2^0;       %did optimization over precomputed region and then refined it
                                     %using fmincon
ocases.randstim=2^1;    %random stimulus was used
ocases.nooverlap=2^2;   %indicates there was no overlap
ocases.upperbound=2^3;  %indicates we ended up doing a search over the upper boundary
                                              %of the feasible region.
ocases.alongmean=2^4;      %set stim to be paralel to mean
ocases.belowqmin=2^5;    %optimal value was below feasible region
pobj=struct('ver',1,'filename',[],'glm',[],'dotmu',[],'sigmasq',[],'mi',[],'mifunc',[],'mifopts',[],'options',[],'debug',1,'numtol',10^-8,'ocases',ocases);

%check for no input arguments create an empty structure
%I think this gets used when we load a saved object
if (nargin==0)

    %create the object and register it as subclass of StimChooserObj
    pobj=class(pobj,'PreCompMIObj',StimChooserObj);

else
    %*************************************************
    %which constructor
    if (nargin==2)
        %load from file
        switch varargin{1}
            case 'filename'
                d=load(varargin{2});
                if ~isfield(d,'pobj')
                    error('file doesnt contain a PreCompMIObj named pobj');
                end

                if ~(isa(d.pobj,'PreCompMIObj'))
                    error('file doesnt contain a PreCompMIObj named pobj');
                end
                pobj=d.pobj;


            otherwise
                error('If only 1 argument is supplied it must be the filename');
        end
    else
        %list of required parameters
        required.glm=0;
        required.fname=0;
        required.dotmu=0;
        required.sigmasq=0;
        required.mifunc=0;
        for j=1:2:nargin
            switch varargin{j}
                case 'filename'
                    pobj.filename=varargin{j+1};
                    required.fname=1;
                case 'dotmu'
                    pobj.dotmu=varargin{j+1};
                    if isempty(pobj.dotmu)
                        error('dotmu cannot be empty');
                    end
                    %make sure its a vector check number of dimensions
                    if (isvector(pobj.dotmu)==0)
                        error('dotmu must be vector');
                    end

                    %sort them in ascending order;
                    pobj.dotmu=sort(pobj.dotmu,'ascend');
                    
                     %remove any duplicats
                    v=pobj.dotmu(1);
                    for i=2:length(pobj.dotmu)
                       if (pobj.dotmu(i)==v)
                           pobj.dotmu(i)=NaN;
                       else
                           v=pobj.dotmu(i);
                       end
                    end
                   ind=find(~isnan(pobj.dotmu));
                   pobj.dotmu=pobj.dotmu(ind);
                   
                    required.dotmu=1;
                case 'sigmasq'
                    pobj.sigmasq=varargin{j+1};
                    %make sure they are greater than zero
                    if ~isempty(find(pobj.sigmasq<=0))
                        error('sigmasq must be >=0');
                    end
                    if (isvector(pobj.sigmasq)==0)
                        error('sigmasq must be vector');
                    end
                    %sort in ascending order
                    pobj.sigmasq=sort(pobj.sigmasq,'ascend')
                    %remove any duplicats
                    v=pobj.sigmasq(1);
                    for i=2:length(pobj.sigmasq)
                       if (pobj.sigmasq(i)==v)
                           pobj.sigmasq(i)=NaN;
                       else
                           v=pobj.sigmasq(i);
                       end
                    end
                   ind=find(~isnan(pobj.sigmasq));
                   pobj.sigmasq=pobj.sigmasq(ind);
   
                    required.sigmasq=1;
                case 'mifunc'
                    pobj.mifunc=varargin{j+1};
                    if ~isa(pobj.mifunc,'function_handle');
                        error('mifunc must be a function handle');
                    end
                    required.mifunc=1;
                case 'mifopts'
                    pobj.mifopts=varargin{j+1};
                case 'glm'

                    pobj.glm=varargin{j+1};
                    if ~isa(pobj.glm,'GLMModel')
                        error('GLM must be an object of class GLMModel');
                    end
                    required.glm=1;
                otherwise
                    error(sprintf('%s unrecognized parameter \n',varargin{j}));
            end
        end

        %check if all required parameters were supplied
        freq=fieldnames(required);
        for k=1:length(freq)
            if (required.(freq{k})==0)
                error(sprintf('Required parameter %s not specified \n',freq{k}));
            end
        end

        %precompute the objective function
        fprintf('Precomputing the objective function \n');
        [pobj.mi]=compmi(pobj,pobj.dotmu,pobj.sigmasq);


    %create the object and register it as subclass of StimChooserObj
    pobj=class(pobj,'PreCompMIObj',StimChooserObj);


    %save it to a file
    %if an error occurs still return the object so that we don't lose all
    %the pecomputed values
    try
        saveopts.append=1;  %append results so that we don't create multiple files
        %each time we save data.
        saveopts.cdir =1; %create directory if it doesn't exist
        vtosave.pobj='';
        savedata(pobj.filename,vtosave,saveopts);
        fprintf('Saved to %s \n',pobj.filename);
    catch
        fprintf('Error occured while trying to save object. Try to save it manually \n');
        fprintf('Error msg %s \n',lasterror.message);
    end
    end %whether load from file or create new
end


%validate
%check sigmasq is greater than zero
if ~isempty(find(pobj.sigmasq<0))
    error('sigmasq cannot be < zero');
end
%check its sorted
if ~issorted(pobj.sigmasq)
    error('sigmasq not sorted in ascending order');
end
if ~issorted(pobj.dotmu)
    error('dotmu not sorted in ascending order');
end