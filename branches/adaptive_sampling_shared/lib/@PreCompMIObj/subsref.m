%allow up to three return arguments because that is how many arguments
%some of the functions we call may return
function [varargout] = subsref(obj,index)
% SUBSREF Define field name indexing for GLModel objects
switch index(1).type
    case '.'
        %for backwards compatibility with my old structure
        %allow user to specify data as obj.fname
        if (size(index,2)==1)
            varargout{1}=obj.(index(1).subs);
        elseif (size(index,2)==2)
            %check if the field we are referencing is a function handle
            if isa(obj.(index(1).subs),'function_handle')
                %we are derefencing a function handle
                %therefore evaluate the function if the user also specified
                %arguments
                if (strcmp(index(2).type,'()')==1)
                    %evaluate the funciton
                    %to allow for variable number of output arguments
                    %we construct a syntax for the calling function based
                    %on how many output arguments are supplied
                    if (nargout>0)
                        fcall='[';
                        for j=1:nargout
                            fcall=sprintf('%s varargout{%d}',fcall,j);
                            if (j<narargout)
                              fcall=sprintf('%s,',fcall);
                            end
                        end
                        fcall=sprintf('%s]=f(index(2).subs{1});');
                        eval(fcall);
                    else
                        %no return arguments so just evaluate the function
                        f(index(2).subs{1});
                    end
                end
            else
                %we are dereferencing a field
                 f=obj.(index(1).subs);
                 %2-15-07
                 %problems with the following code
%                 [varargout{1}]=f.(index(2).subs{:});
                 %recursively process any further dereferencing
                  varargout{1}=subsref(f,index(2:end));
            end

        else
            error('Code did not anticipate more than 2 levels of referencing');
        end
end

