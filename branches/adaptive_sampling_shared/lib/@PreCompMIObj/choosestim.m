%function stim=choosestim(finf,post,shist,einfo,mparam,glm)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters

% Return value
%       stim - the stimulus for the next trial
%       opreturn    - optional return information about the procedure
%               .ocase - a number giving information on how the stimulus was
%               selected.
%               .dotmuopt - optimal dotmu 
%               .sigmaopt  - optimal value of sigma
%               .miopt       - optimal value of mutual information
%       pobj    
%       quad  - the modified quadratic form. Return it so that subclasses
%                 can access it without recomputing it
% Explanation: Chooses the stimulus by doing a 2-d optimization over the
% precomputed region.
function [optstim, oreturn,pobj, quad]=choosestim(pobj,post,shist,einfo,mparam,varargin)
%initialize the oreturn structure
%we want the fields to always be the same becase otherwise we will have
%problems when we try to convert the cell array of oreturn to a structure
%array
%oreturn.ocase=0;
%sigmalim, dotmulim - store max and min of the largest bounding rectangle
%on each trial
oreturn=struct('ocase',0,'dotmuopt',[],'sigmaopt',[],'dotmulim',[],'miopt',[]);

glm=mparam.glm;

%check if einfo is suppled
if isempty(einfo)
    einfo=[];
end
if ~isfield(einfo,'eigd')
    einfo.eigd=[];
end
if ~isfield(einfo,'evecs')
    einfo.evecs=[];
end

%**************************************************************************
%Setup/Initialization for computing sigmaopt
%*************************************************************************
%compute the values we will need in order to comput max and min of sigma as
%function of mu
% quadmod requires eigendecomposition of just stimulus terms
if (length(einfo.eigd)==mparam.klength)
    eigck=einfo;
else
    warning('eigendecomposition decomposition of just stim terms Ck is required \n computing it via SVD');
    [eigck.evecs eigck.eigd]=svd(post.c(1:mparam.klength,1:mparam.klength));
    eigck.eigd=diag(eigck.eigd);
    eigck.esorted=-1;
end
muk=post.m(1:mparam.klength);
%quad=quadmodshist(post.c,eigck,muk,shist);
%keyboard;
quad=QuadModObj('C',post.c,'eigck',eigck,'muk',muk,'shist',shist);
optim=optimset('TolX',10^-12,'TolF',10^-12);

%******************************************************************
%compute the minimum and maximum value of dotmu

%lb =[min(mu); min(sigmasq)];
%ub =[max(mu); max(sigmasq)];
lb=[];
ub=[];
%bounds in mu
%magnitude of mean
mumag=(post.m'*post.m)^.5;
%if magnitude of mean is zero than mu_epsilon is always zero (so just do
%line search)
if (mumag~=0)
    lb.dotmu=-mparam.mmag*mumag;
    ub.dotmu=mparam.mmag*mumag;
    oreturn.dotmulim=[lb.dotmu ub.dotmu];   %save limits in dotmu space
end


%if einfo is empty compute it
if isempty(einfo.evecs)
    warning('einfo not supplied computing svd');
    [einfo.evecs einfo.eigd]=svd(post.c);
    einfo.esorted=-1;
    einfo.eigd=diag(einfo.eigd);
end


%**************************************************************************
%convert the bounding box to values of the indexes into dotmu and sigmasq
%for which we have precomputed the boundary
%**************************************************************************
%bind stores the indexes of the elemnts in pobj.dotmu, pobj.sigmasq
%which form a bounding box of the feasible region and for which we have
%precomputed the fisher info.
bind.dmind=[0 0];

%nooverlap variable indicating feasible region and precomputed region don't
%overlap
nooverlap=0;
%lower bound on dot mu
%check if its in the feasible region
if (pobj.dotmu(1)<lb.dotmu && lb.dotmu <=pobj.dotmu(end))
    bind.dmind(1)=binarysubdivide(pobj.dotmu,lb.dotmu);
else
    %check if there is any overlap
    if (pobj.dotmu(end)< lb.dotmu)
        nooverlap=1;
    else
        warning(sprintf('Left edge of feasible region is outside precomputed area. Left edge=%d',lb.dotmu));
        bind.dmind(1)=1;
    end
end

%upper bound on dotmu
if (pobj.dotmu(1)<=ub.dotmu && ub.dotmu <=pobj.dotmu(end))
    bind.dmind(2)=binarysubdivide(pobj.dotmu,ub.dotmu);
    if (pobj.dotmu(bind.dmind(2))>ub.dotmu)
        bind.dmind(2)= bind.dmind(2)-1;
    end
else
    if (ub.dotmu<pobj.dotmu(1))
        nooverlap=1;
    else
        warning(sprintf('Right edge of feasible region is outside precomputed area. Right edge=%d',ub.dotmu));
        bind.dmind(2)=length(pobj.dotmu);
    end
end

%**************************************************************************
%the following code is only executed if thereis overlap in the feasible
%dotmu and the precomputed range.
%Note: Its still possible that for all the precomputed values of dotmu
%which are feasible, none of the feasible sigmas fall within the
%precomputed values. 
%i.e its possible all the feasible values fall inbetween two adjacent
%values of sigmasq for which the objective function is precomputed.
if (nooverlap==0)
    %************************************************************************
    %optimize over the bounding box two steps
    %1. for each value of dotmu on our grid, comput sigmamax and sigmamin
    %2. find the maximum value for this value of dotmu
    %**************************************************************************

    optdmu.soptind=zeros(1,bind.dmind(2)-bind.dmind(1)+1);          %index of optimal sigmasq value for this dotmu
    optdmu.sigind=zeros(2,bind.dmind(2)-bind.dmind(1)+1);           %range of indexes of sigmasq which are within feasible region
    optdmu.fopt=zeros(1,bind.dmind(2)-bind.dmind(1)+1);
    optdmu.qmax=zeros(1,bind.dmind(2)-bind.dmind(1)+1);
    optdmu.qmin=zeros(1,bind.dmind(2)-bind.dmind(1)+1);
    optdmu.xmax=zeros(mparam.klength,bind.dmind(2)-bind.dmind(1)+1);
    optdmu.xmin=zeros(mparam.klength,bind.dmind(2)-bind.dmind(1)+1);
    optdmu.overlap=zeros(1,bind.dmind(2)-bind.dmind(1)+1);          %indicates that for this value of dotmu
                                                                                                                  %some values of sigmasq fall within the precomputed range

    %set noverlap to true
    %we set noverlap to false only if we find that for some of the feasible
    %precomputed values of dotmu there are some feasible values of sigmasq
    %on which we have precomputed the objective function
    nooverlap=1;
    for dmuind=bind.dmind(1):bind.dmind(2)
        ind=dmuind-bind.dmind(1)+1;
        dotmu=pobj.dotmu(dmuind);

        %dotmu is dotpduct stimulus and mean
        %stimulus we have to divide this by the magnitude of the mean
        %because we want the projeciton along the mean of the stimulus
        muproj=dotmu/mumag;
        %compute qmax and qmin
        [optdmu.qmax(:,ind),optdmu.qmin(:,ind),optdmu.xmax(:,ind),optdmu.xmin(:,ind)]=maxminquad(quad,muproj,mparam.mmag,optim);

        qmax=optdmu.qmax(:,ind);
        qmin=optdmu.qmin(:,ind);
        %now get the indexes for sigma
        %find the largest values which we know to be feasible
        %check to make sure feasible region and precomputed region overlap
        if (qmax<pobj.sigmasq(1) || qmin > pobj.sigmasq(end))
            %no overlap between the precomptued region and dotmu for this
            %value of dotmumu
        else
            
            if (pobj.sigmasq(1)<=optdmu.qmin(ind) && optdmu.qmin(ind)<=pobj.sigmasq(end))
                optdmu.sigind(1,ind)=binarysubdivide(pobj.sigmasq,optdmu.qmin(ind));
            else
                %qmin is smaller than pobj.sigmasq(1)
                optdmu.sigind(1,ind)=1;
            end

            if (pobj.sigmasq(1)<=optdmu.qmax(ind) && optdmu.qmax(ind) <=pobj.sigmasq(end))
                optdmu.sigind(2,ind)=binarysubdivide(pobj.sigmasq,optdmu.qmax(ind));
                if (pobj.sigmasq(optdmu.sigind(2,ind))>optdmu.qmax(ind))
                    optdmu.sigind(2,ind)=optdmu.sigind(2,ind)-1;
                end                 
            else
                optdmu.sigind(2,ind)=length(pobj.sigmasq);
            end
            if (optdmu.sigind(1,ind)>0 && optdmu.sigind(2,ind)>0)
                optdmu.overlap(ind)=1;
            end
        end

        %check if region qmin-qmax falls in between 2 grid points
        if (optdmu.sigind(1,ind)>optdmu.sigind(2,ind))
            optdmu.overlap(ind)=0;
        end
        %compute the maximum on this region
        if (optdmu.overlap(ind)~=0)
            nooverlap=0;
        [optdmu.fopt(1,ind) optdmu.soptind(1,ind)]=max(pobj.mi(dmuind,optdmu.sigind(1,ind):optdmu.sigind(2,ind)));
            %this is the index of sigmaopt corresponding to the optimium value
            %that is the index into pobj.sigmaopt
            optdmu.soptind(1,ind)=optdmu.sigind(1,ind)+optdmu.soptind(1,ind)-1; 
        else
            opdmu.fopt(1,ind)=-inf;
        end
    end%end for
    
end %end check for overlap in dotmu

%**************************************************************************
%Find the optimium value over the precomputed region
%We only search over the precomputed region which overlaps with the
%feasible region
%**************************************************************************

if (nooverlap==0)
    %now find optimium value over the precomputed region
    [optpreg.fopt optpreg.ind]=max(optdmu.fopt);
    optpreg.dmuopt=pobj.dotmu(bind.dmind(1)-1+optpreg.ind);
    optpreg.sigmaopt=pobj.sigmasq(optdmu.soptind(1,optpreg.ind));
    optpreg.xmax=optdmu.xmax(:,optpreg.ind);
    optpreg.xmin=optdmu.xmin(:,optpreg.ind);


    %solve for the actual stimulus
    %so we need to find a linear combination of xmax, xmin which gives
    %sigmaopt
    A=post.c(1:mparam.klength,1:mparam.klength);
    if (mparam.alength>0)
        b=shist'*post.c(mparam.klength+1:end,1:mparam.klength);
        d=shist'*post.c(mparam.klength+1:end,mparam.klength+1:end)*shist;
    else
        b=zeros(mparam.klength,1);
        d=0;
    end
    options = optimset('Jacobian','on','tolfun',10^-12);
    fs=@(s)stimsigma(s,optpreg.sigmaopt,A,b,d,optpreg.xmin,optpreg.xmax);
    [sopt,fzero]=fsolve(fs,.5,options);

    optstim=(1-sopt)*optpreg.xmin+sopt*optpreg.xmax;
    oreturn.ocase=oreturn.ocase+pobj.ocases.normal;
    %return these values to get some idea of the region and how big the
    %grid should be.
    oreturn.dotmuopt=optpreg.dmuopt;
    oreturn.sigmaopt=optpreg.sigmaopt;
    oreturn.miopt=optpreg.fopt;
    if (sopt<0 || sopt>1)
        error('Could not find linear combo xmin and xmax to yield sigmaopt');
    end
    %check it if debuggin is turned on
    if (pobj.debug~=0)
        if ((optstim'*post.m-optpreg.dmuopt)>pobj.numtol)
            error('Stimulus doesnt have optimal projection on mean');
        end
        if (([optstim;shist]'*post.c*[optstim;shist]-optpreg.sigmaopt)>pobj.numtol)
            error('Stimulus doesnt yield optimal value of sigma');
        end
    end%debug
else
    %no overlap 
    %generate random stimuli
    oreturn.ocase=oreturn.ocase+pobj.ocases.nooverlap+pobj.ocases.randstim;
    oreturn.dotmuopt=NaN;
    oreturn.sigmaopt=NaN;
    %feasible and precomputed region don't overlap
    %select a random stimulus
    obj=RandStimBall('mmag',mparam.mmag, 'klength',mparam.klength);
    optstim=choosestim(obj);
    warning(sprintf('Precomputed region and feasible region dont overlap. \n Selecting random stimulus'));
end


%***************************************
%function we need to solve to find stimulus which gives (muopt,
%sigma^2_opt)
%see aistat eqn 24.
function [d,jac]=stimsigma(s,sigmasqopt,A,b,d,xmin,xmax)
x=(1-s)*xmin+s*xmax;

sigmasq=x'*(A*x)+b'*x+d;
d=sigmasq-sigmasqopt;
%return the jacobian
jac=(2*x'*A+b')*(-xmin+xmax);