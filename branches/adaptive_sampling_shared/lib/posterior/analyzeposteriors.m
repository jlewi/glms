%function analyzeposteriors=(pmethods,sr,mparam,opts,simparam)
%       pmethods - data describing the posteriors
%       sr       - stimulus and response
%       mparam   - model parameters
%       opts     - options which specify which results/analyses to compute
%           .getstats - compute the mean and variance of discrete
%           representations
%           .getsamps - discretize continuous 
%           .mckl     - the kl divergence computed using monte carlo
%                   sampling
%           .bkl      -kl computed by brute force
%           .fname   - filename to save data to 
%      simparam
%Explanation:
%   Computes the statistics/results on the output of updateposteriors
%   For discrete representations of the posterior it computes the mean
%       and variance of the output
%   For parametric representations of the output it computes discrete
%   representations for the purpose of plotting
function [pmethods]=analyzeposteriors(pmethods,sr,mparam,opts,simparam)
%declare the default options
defopts.getstats=0;
defopts.getsamps=0;
defopts.mckl=0;
defopts.bkl=0;
defopts.fname='';
defopts.pfrac=.97;
defopts.nstd=5;             %when gaussians are sampled
                            %we sample in a range of +- nstd*std of the
                            %mean
defopts.nsamp=1000;          %number of sample points when sampling densities

if ~exist('opts','var')
    opts=[];
end
%initialize to defaults
opts=checkfields(opts,defopts);

%to compute the kl distance by brute force we need to get sampled
%representations of the data
if (opts.bkl~=0)
    opts.getsamps=1;
end


%construct a list of enabled methods
mon={};
mnames=fieldnames(pmethods);
for index=1:length(fieldnames(pmethods))
    if pmethods.(mnames{index}).on >0
        mon{length(mon)+1}=mnames{index};
    end
end

numiter=simparam.niter;

%duplicate the sr structure if neccessary
srtmp=sr;

%parameters for KL divergence
%this avoids needing to resample
klparam=[];
klparam.nsamp=10000;
klparam.z=randn(mparam.tlength,klparam.nsamp);

for index=1:length(mon)
            if ~isfield(sr,mon{index})
            sr.(mon{index})=srtmp;
        end
    
end

    
%**************************************************************************
%Compute Sampled versions of the variables
%**************************************************************************
if (opts.getsamps~=0)
    fprintf('Getting samples of posterior distributions \n');
    
    %following methods all get proced the same way 
    mnames={'g','ekf','ekf1step','ekfmax'};
    for index=1:length(fieldnames(pmethods))
        numiter=size(sr.(mon{index}).y,2);
        
        if (pmethods.(mnames{index}).on >0)
            mon{length(mon)+1}=mnames{index};            
            for tindex=1:numiter
                %for each dimension get std of that dimension
                indexes=indexoffset([1:mparam.tlength]'*ones(1,mparam.tlength),mparam.tlength*ones(1,mparam.tlength));
                vdims=(pmethods.(mnames{index}).c{tindex}(indexes))';
                gstd=vdims.^.5;
                krange=pmethods.(mnames{index}).m(:,tindex)+[-opts.nstd*gstd,opts.nstd*gstd];
                dk=(krange(:,2)-krange(:,1))/(opts.nsamp-1);
                [pmethods.(mnames{index}).p{tindex},pmethods.(mnames{index}).theta{tindex}]=mvgaussmatrix(krange,dk,pmethods.(mnames{index}).m(:,tindex),pmethods.(mnames{index}).c{tindex}); 
                pmethods.(mnames{index}).p{tindex}=pmethods.(mnames{index}).p{tindex}*dk;
            end     
        end
    end
    
    
    if (pmethods.batchg.on >0)
        for tindex=1:numiter
            %for each dimension get std of that dimension
            indexes=indexoffset([1:mparam.tlength]'*ones(1,mparam.tlength),mparam.tlength*ones(1,mparam.tlength));
            vdims=(pmethods.batchg.c{tindex}(indexes))';
            gstd=vdims.^.5;
            krange=pmethods.batchg.m(:,tindex)+[-opts.nstd*gstd,opts.nstd*gstd];
            dk=(krange(:,2)-krange(:,1))/(opts.nsamp-1);
            [pmethods.batchg.p{tindex},pmethods.batchg.theta{tindex}]=mvgaussmatrix(krange,dk,pmethods.batchg.m(:,tindex),pmethods.batchg.c{tindex}); 
            pmethods.batchg.p{tindex}=pmethods.batchg.p{tindex}*dk;
        end      
    end
    
end
%**************************************************************************
%Compute Statistics/Results
%**************************************************************************

%**************************************************************************
% Compute KL Divergence using Monte carlo techniques
%**************************************************************************
if (opts.mckl~=0)
        fprintf('Computing KL Divergence \n');
for tr=1:numiter+1
    if (pmethods.g.on >0)
        %multiply by mparam.dk to handle continuous to discrete approximation.
        %compute the KL Divergence if tr>1
        if (tr>1)
  
        q.mu=pmethods.g.m(:,tr);
        q.c=pmethods.g.c{tr};
        
        %We are computing the KL distance of our approximation from the
        %true posterior. To do this, the KL divergence needs to consider
        %the likelihood of all observations so far.
        %So the llhood function is the batch update function
        %To compute the KL Divergence the KL divergence
        %needs to be able to compute the likelihood of the observation
        %under the different models
        %so we pass the information needed to compute the likelihood
        %in the structure ll
        
        ll.func=@batchll;
        ll.obsrv=sr.(mon{index}).nspikes(tr-1);     %observation on this trial
        ll.input=(sr.(mon{index}).y(:,tr-1))';           %stimulus on this trial
        ll.param={@ll1dtemp,mparam.tresponse};         % cell array of parameters for the batchllupdate fcn
        %function to compute prior likleihood of model
        po.func=@logmvgauss;
        po.param={pmethods.g.m(:,1), pmethods.g.c{1}};
        pmethods.g.dk(tr)=dkl(q,po,ll,klparam);
        end
    end
    
    
    if (pmethods.batchg.on >0)
        %multiply by mparam.dk to handle continuous to discrete approximation.
        %compute the KL Divergence if tr>1
        if (tr>1)
    
        q.mu=pmethods.batchg.m(:,tr);
        q.c=pmethods.batchg.c{tr};
        
        %We are computing the KL distance of our approximation from the
        %true posterior. To do this, the KL divergence needs to consider
        %the likelihood of all observations so far.
        %So the llhood function is the batch update function
        %To compute the KL Divergence the KL divergence
        %needs to be able to compute the likelihood of the observation
        %under the different models
        %so we pass the information needed to compute the likelihood
        %in the structure ll
        
        ll.func=@batchll;
        ll.obsrv=sr.(mon{index}).nspikes(tr-1);     %observation on this trial
        ll.input=(sr.(mon{index}).y(:,tr-1))';           %stimulus on this trial
        ll.param={@ll1dtemp,mparam.tresponse};         % cell array of parameters for the batchllupdate fcn
        %function to compute prior likleihood of model
        po.func=@logmvgauss;
        po.param={pmethods.batchg.m(:,1), pmethods.batchg.c{1}};
        pmethods.batchg.dk(tr)=dkl(q,po,ll,klparam);
        end
    end
    
    if (pmethods.ekf.on >0)
        %multiply by mparam.dk to handle continuous to discrete approximation.
        %compute the KL Divergence if tr>1
        if (tr>1)

        q.mu=pmethods.ekf.m(:,tr);
        q.c=pmethods.ekf.c{tr};
        
        %We are computing the KL distance of our approximation from the
        %true posterior. To do this, the KL divergence needs to consider
        %the likelihood of all observations so far.
        %So the llhood function is the batch update function
        %To compute the KL Divergence the KL divergence
        %needs to be able to compute the likelihood of the observation
        %under the different models
        %so we pass the information needed to compute the likelihood
        %in the structure ll
        
        ll.func=@batchll;
        ll.obsrv=sr.ekf.nspikes(tr-1);     %observation on this trial
        ll.input=(sr.ekf.y(:,tr-1))';           %stimulus on this trial
        ll.param={@ll1dtemp,mparam.tresponse};         % cell array of parameters for the batchllupdate fcn
        
        %function to compute prior likleihood of model
        po.func=@logmvgauss;
         
        po.param={pmethods.ekf.m(:,1),pmethods.ekf.c{1}};
        
        pmethods.ekf.dk(tr)=dkl(q,po,ll,klparam);
        end
    end
    
    if (pmethods.ekfmax.on >0)
        %multiply by mparam.dk to handle continuous to discrete approximation.
        %compute the KL Divergence if tr>1
        if (tr>1)
   
        q.mu=pmethods.ekfmax.m(:,tr);
        q.c=pmethods.ekfmax.c{tr};
        
        %We are computing the KL distance of our approximation from the
        %true posterior. To do this, the KL divergence needs to consider
        %the likelihood of all observations so far.
        %So the llhood function is the batch update function
        %To compute the KL Divergence the KL divergence
        %needs to be able to compute the likelihood of the observation
        %under the different models
        %so we pass the information needed to compute the likelihood
        %in the structure ll
        
        ll.func=@batchll;
        ll.obsrv=sr.ekfmax.nspikes(tr-1);     %observation on this trial
        ll.input=(sr.ekfmax.y(:,tr-1))';           %stimulus on this trial
        ll.param={@ll1dtemp,mparam.tresponse};         % cell array of parameters for the batchllupdate fcn
        
        %function to compute prior likleihood of model
        po.func=@logmvgauss;
         
        po.param={pmethods.ekfmax.m(:,1),pmethods.ekfmax.c{1}};
        
        pmethods.ekfmax.dk(tr)=dkl(q,po,ll,klparam);
        end
    end
end

    if (pmethods.b.on >0)
        %kpts - is d x n
        %   d- = mparam.tlength
        %   n - n different models
        pmethods.b.m(:,tr)=sum((ones(mparam.tlength,1)*(pmethods.b.p{tr})').*kpts,2);
        pmethods.b.c{tr}=zeros(mparam.tlength,mparam.tlength);
        for kind=1:numkpts
            pmethods.b.c{tr}=pmethods.b.c{tr}+(pmethods.b.p{tr}(kind))'*(kpts(:,kind)-pmethods.b.m(:,tr))*(kpts(:,kind)-pmethods.b.m(:,tr));
        end
    end
    
    if (pmethods.bg.on >0)    
        error('Fix update for brute force gaussian');
     %   pmethods.bg.mbg(:,tr)=sum(pmethods.bg.p{tr}.*kpts');
    %    pmethods.bg.cbg{tr}=zeros(mparam.tlength,mparam.tlength);
   %     for kind=1:numkpts
   %         pmethods.bg.cbg{tr}=pmethods.bg.cbg{tr}+pmethods.bg.p{tr}(kind)*(kpts(:,kind)-pmethods.bg.mbg(tr,:)')*(kpts(:,kind)-pmethods.bg.mbg(tr,:)')';        
   %     end
   end
  
end


%**************************************************************************
% Compute Mean and variance
%**************************************************************************
%compute the gaussian and account for discretization error
%construct the data for slideshow
if (opts.getstats ~=0)
    for tr=1:numiter+1
    
    if (pmethods.b.on >0)
        %kpts - is d x n
        %   d- = mparam.tlength
        %   n - n different models
        pmethods.b.m(:,tr)=sum((ones(mparam.tlength,1)*(pmethods.b.p{tr})').*kpts,2);
        pmethods.b.c{tr}=zeros(mparam.tlength,mparam.tlength);
        for kind=1:numkpts
            pmethods.b.c{tr}=pmethods.b.c{tr}+(pmethods.b.p{tr}(kind))'*(kpts(:,kind)-pmethods.b.m(:,tr))*(kpts(:,kind)-pmethods.b.m(:,tr));
        end
    end
    
   if (pmethods.bg.on >0)    
        pmethods.bg.m(:,tr)=sum((ones(mparam.tlength,1)*(pmethods.bg.p{tr})').*kpts,2);
        pmethods.bg.c{tr}=zeros(mparam.tlength,mparam.tlength);
        for kind=1:numkpts
            pmethods.bg.c{tr}=pmethods.bg.c{tr}+(pmethods.bg.p{tr}(kind))'*(kpts(:,kind)-pmethods.bg.m(:,tr))*(kpts(:,kind)-pmethods.bg.m(:,tr));
        end
   end
end
end

%*************************************************************************
%compute the kl divergence by brute force
%**************************************************************************
if (opts.bkl~=0)
    %compute kl divergence if both methods are present
    %and we want to compute it by brute force
    if (pmethods.b.on >0 & pmethods.g.on>0 & opts.klbrute>0)
        pmethods.g.bkldist(1,tr)=kldiv(pmethods.g.p{tr},(pmethods.b.p{tr}));
    end
    if (pmethods.b.on>0 & pmethods.ekf.on>0 & opts.klbrute>0)
        pmethods.ekf.bkldist(1,tr)=kldiv(pmethods.ekf.p{tr},(pmethods.b.p{tr}));
    end
end
%save data
vtosave.pmethods='';
saveopts.append=1;

if (~(isempty(opts.fname)))
    savedata(opts.fname,vtosave,saveopts);
end

%***********************************************************************
%analyze batch update
%*************************************************************************
if (pmethods.batchg.on~=0)
    %compute the most likely theta after each iteration
    [v,ind]=max(pmethods.batchg.lpobsrv,[],2);
    for tindex=1:numiter
        pmethods.batchg.mll(1,tindex)=pmethods.batchg.thetapts(ind(tindex));
    end
    
end