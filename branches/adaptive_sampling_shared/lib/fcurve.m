%function fx=fcurve(pg, param)
%       pg - gaussian for p(theta)
%           .m - d*1 = mean
%           .c       = covariance
%
%       param -
%           .xpts - matrix dxn
%                   of pts at which to compute fx
%                   reusing pts will make it faster
%                   -of course you might also want to change the pts
%                   as it converges
%           .xrange - specify range for pts
%                   function will determine the pts
%                   make sure xpts not also specified
%           .plots 
%                   .image = 0 or 1 makes a colored image to represent
%                   fisher info at every point in a 2-d grid
%Explanation: computes the function f(x) which is the function we need to optimize 
%   to choose the optimal stimulus
%
%Return Value:
%   fx - if pg.m is 2x1
%        fx is 2x2 matrix representing the values
function [fx,xpts]=fcurve(pg,param)
pind=0;
numpts=500;

%dimensionality of x
d=size(pg.m,1);
if ~(exist('param','var'))
    param=[];
end

if ~isfield(param,'plots')
    param.plots.image=0;
end
if ~isfield(param.plots,'image')
        param.plots.image=0;
end
if ~(isfield(param,'xpts'))
    plots.on=1;
    %if xrange is specified get the points
    if ~isfield(param,'xrange');
        %if xrange not specified automatically determine
        %range from std
        nstd=10;       %how many std to take in getting the pts
         %xpts currently contains indexes set them to appropriate numbers
   %for that we need the variances along each dimension
   %indexes of diagonal elements
        ind=[1:d]'*ones(1,2);
        %std along each dimension
        vstd=indexoffset(ind,size(pg.c));
        vstd=pg.c(vstd);
        vstd=vstd.^.5;
        
        param.xrange(:,1)=(pg.m-nstd*vstd);
        param.xrange(:,2)=(pg.m+nstd*vstd);
    end
   xrange=param.xrange;
   %compute dx along each dimension
   dx=(xrange(:,2)-xrange(:,1))./(numpts-1);

   xpts=matrixindexes(numpts*ones(1,size(pg.m,1)));
   xpts=compmatrix(xpts);
   %xpts is dxn matrix
   %each column is a different pt at which fx will be calculated.
   xpts=xpts';
   
  
  
  
   
   %we subtract 1 to get the number of times to multiply dk
   onesmat=ones(1,size(xpts,2));
   xpts=(xpts-1).*(dx*onesmat)+xrange(:,1)*onesmat;
    
else
    xpts=param.xpts;
    numpts=size(xpts,2);
end


%compute pg.m*mu
%will be row vector
xproj=pg.m'*xpts;

%compute 1/2 x^t pg.c x
%step 1. Co x
%   this gives a matrix 
com=pg.c*xpts;
%step 2. compute x'*(Cox)
%We want to compute the dot prodcut
%so we do term by term multiplication and then sum across rows
%yielding a row vectors
com=xpts.*com;
com=sum(com,1);

fx=exp(xproj+.5*com).*com;

%**************************************************************************
%Plots
%********************************************************************
%if its 2d reshape fx to be matrix
if (param.plots.image~=0)
    %in my notes 12-12 I explain the indexing
    fxmat=reshape(fx,numpts,numpts);
    figure;
    %graph ptsr
    gpts=zeros(size(pg.m,1),numpts);
    for index=1:size(pg.m,1)
        gpts(index,:)=[xrange(index,1):dx(index):xrange(index,2)];
    end
    %transpose the image before plotting
    %b\c rows correspond to x values but image takes rows as yvalues.
    clims=[0 40];  %range to scale colors to
 
    figure;    
    imagesc(gpts(1,:),gpts(2,:),log(fxmat),clims);
    set(gca,'YDir','normal');    %make increasing y go up
    
    colorbar;
    title('ln(f(x))');
    hold on;
    %make a circle indicating ||x||=1
    %x=[-1:.05:1];
    %yp=(1-x.^2).^.5;
    %yn=-(1-x.^2).^.5;
    %plot(x,yp,'k.');
    theta=[0:2*pi/90:2*pi];
    x=1*cos(theta);
    y=1*sin(theta);
    pind=pind+1;
    h_p(pind)=plot(x,y,'k--');
    lgnd{pind}='||x||_2=1';

    %plot an ellipse to represent the gaussian. 
    [v,eigval]=eig(pg.c);
    ind=[1:size(pg.m,1)]'*ones(1,2);
    vind=indexoffset(ind,size(pg.c));
    vstd=eigval(vind);
    vstd=vstd.^.5;
    rx=vstd(1);
    ry=vstd(2);
    %compute ellipse in terms of coordinates along eigen vectors
    r=rx*ry./(((ry*cos(theta)).^2)+(rx*(sin(theta))).^2).^.5;
    x=r.*cos(theta);
    y=r.*sin(theta);
   %now rotate it
    cpts=v*[x;y];
    %now shift it to mean

    cpts(1,:)=cpts(1,:)+pg.m(1);
    cpts(2,:)=cpts(2,:)+pg.m(2);
    
    pind=pind+1;
    h_p(pind)=plot(cpts(1,:),cpts(2,:),'m.');
    lgnd{pind}='p_t(\theta)';
    xlabel('x_1');
    ylabel('x_2');
    legend(h_p,lgnd);
    axis square;
end

