%function sim=GLMInputVec(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%   data - data for the vectors
%
% Explanation: Represents the input when the input is just a vector
%
%Revisions
%   01-19-2009 - Converted to the new object model
%       deleted field bname
%       field version - now  a structure defined in base class
classdef (ConstructOnLoad=true) GLMInputVec<GLMInput

    
    properties(SetAccess=private, GetAccess=public)

        %  data   - the data
        data=[];

    end

    methods
        function obj=GLMInputVec(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'data'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=rmfield(params,'data');
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@GLMInput(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     obj.data=params.data;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end


            %set the version to be a structure
            obj.version.GLMInputVec=090119;
        end
    end
end

