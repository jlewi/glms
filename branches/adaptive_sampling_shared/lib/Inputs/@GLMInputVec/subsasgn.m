%function obj = subsasgn(obj,index,val)
%3-20-2007
%       first template. Probably needs work. Not sure how it will handle
%       multiple levels of indexing.
function obj = subsasgn(obj,index,val)
% SUBSASGN Define index assignment for ASResults objects
switch index(1).type
    case '()'
          %if the object to which we are assigning it is empty
        %i.e array hasn't been initialized then we just set it
        if isempty(obj)
            obj=val;
        else
            %apply recursively
            if (size(index,2)==1)
                obj(index(1).subs{1})=val;
            else
                oel=obj(index(1).subs{1});
                oel=subsasgn(oel,index(2:end),val);
                obj(index(1).subs{1})=oel;
            end
        end
    case '.'       
        switch index(1).subs
            %***********************************
            %add case statements for special handling
            %****************************************
            %will generate an error if field doesn't exist
            %not sure its worth checking for the fieldname explicitly
            %because that would just take time and result in the same
            %outcome (an error)
            otherwise
                obj.(index(1).subs)=val;
        end
end

