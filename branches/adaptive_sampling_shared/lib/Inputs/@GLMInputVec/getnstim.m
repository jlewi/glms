%function getnstim=nstim(obj)
%	 obj=GLMInputVec object
% 
%Return value: 
%	 nstim=obj.nstim 
%
function nstim=getnstim(obj)
    error('02 25 2008 why am I returning the number of columns in Data? should each stimulus be its own GLMINputVec?');
	 nstim=size(obj.data,2);
