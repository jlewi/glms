%function getmag=mag(obj)
%	 obj=GLMInputVec object
% 
%Return value: 
%	 mag=obj.mag 
%
function mag=getmag(obj)
	 mag=(obj.data'*obj.data)^.5;
