%function p=compp(obj,subind)
%   %   subind - multiple subscripts to identify the entry in p
%           ktlengthx1 vector
%           e.g
%         p(sub2ind(subind))=p(x_1=stim(:,subind(1)),x_2=stim(:,subind(2),.
%         ..);
%          - if an entry is nan then that means marginalize over that entry
%
function p=compp(obj,subind)
    
    %find the number of entries we need to "marginalize" over.
    indmarg=find(isnan(subind));
    nmarg=length(indmarg);
    
   
    %is using ':' faster than calling linindexes to get the linear indexes
    %and then just summing over those.
%     cmd='prob=obj.p(';
%     for i=1:obj.ktlength
%        if isnan(subind(i))
%            cmd=sprintf('%s:',cmd);
%        else
%            cmd=sprintf('%s%d',cmd,subind(i));
%        end
%        if (i<obj.ktlength)
%           cmd=sprintf('%s,',cmd);
%        end
%     end
%     cmd=sprintf('%s);',cmd);
%     eval(cmd);
    
    p=sum(prob(Indexing.lind([obj.nx,obj.ktlength],subind)));
    