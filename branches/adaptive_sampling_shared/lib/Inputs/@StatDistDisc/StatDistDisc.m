%function StatDistDisc('stim','ktlength')
%   stim - a matrix each column is a different possible stimulus
%   ktlength - the number of stimuli in our distribution
%            i.e p(s)=p(x_1,x_2,...,x_ktlength)
%
%StatDistDisc('ps')
%   ps - StaDistDisc object
%Explanation: Represent a stationary distribution on the inputs using a
%finite set of possible stimuli
%
classdef (ConstructOnLoad=true) StatDistDisc < handle
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %stim       - dim(x_t)xn_x matrix
    %           - each column is a different possible value for the
    %           stimulus
    %p          - a matrix containing the probability
    %             s=(x_1=a_1,x_2=a_2,....);
    %           - dim(x_t)xdim(x_t)x....=(dim(x_t)^ktlength) matrix
    %
    properties(SetAccess=private, GetAccess=public)
        version=struct();
        stim=[];

        ktlength;
    end

    %make p settable this way we can change the distribution
    %without recomputing the transient variables
    properties(SetAccess=public,GetAccess=public)
        p=[];
    end
    properties(SetAccess=private,GetAccess=public,Transient)
        %number of discretization points
        nx=[];

        %statcon
        %StatCon is a matrix of the constraints needed to satisfy
        %stationarity of the distribution
        statcon=[];


    end


    properties(SetAccess=private,GetAccess=public,Transient)
        % ss - a set of matrices corresponding ss' for each possible
        %      input in our stationary distrubtion
        %      we store this to avoid having to recompute
        %    - (dim(s)^2+dim(s))/2 x size(stim,2) matrix
        %
        ss=[];

        %s - matrix of all the inputs usefule for computing
        %    s'theta
        s=[];

        %a cell array of s(:,i)*s(:,i)'
        %each inp is a different possible sequence
        %this is essentially the same ass ss except we store the entire
        %matrix
        ssmat=[];
        
        
        %lpmat is the matrix we multiply p to project it onto the space
        %satisfying the stationarity constraints and normalization constraints.
        lpmat=[];
        
        %matrix to project p onto space just satisfying stationarity
        %constraints
        lpstatmat=[];
    end

    properties(SetAccess=private,GetAccess=public,Dependent)
        %number
        nstim;

        ns;
        dims;
        dimx;

    end
    methods
        function lpstatmat=get.lpstatmat(obj)
            if isempty (obj.lpstatmat)

                %compute the null space of the constraints to enforce stationarity
                %and normalization
                nstatcon=null([obj.statcon;]);

                %obj.lpmat is the matrix we multiply p to project it onto the space
                %satisfying the stationarity constraints.

                obj.lpstatmat=nstatcon*inv(nstatcon'*nstatcon)*nstatcon';
            end
            lpstatmat=obj.lpstatmat;
        end
        function lpmat=get.lpmat(obj)
            if isempty (obj.lpmat)

                %compute the null space of the constraints to enforce stationarity
                %and normalization
                nstatcon=null([obj.statcon;ones(1,obj.ns)]);

                %obj.lpmat is the matrix we multiply p to project it onto the space
                %satisfying the stationarity constraints.

                obj.lpmat=nstatcon*inv(nstatcon'*nstatcon)*nstatcon';
            end
            lpmat=obj.lpmat;
        end
        function ssmat=get.ssmat(obj)
            if isempty(obj.ssmat)

                pdim=obj.ns;
                obj.ssmat=cell(1,pdim);
                for i=1:pdim
                    obj.ssmat{i}=obj.s(: ,i)*obj.s(:,i)';
                end
            end
            ssmat=obj.ssmat;
        end

        function nstim=get.nstim(obj)
            nstim=size(obj.stim,2);
        end

        function ns=get.ns(obj)
            ns=obj.nstim^obj.ktlength;
        end
        function dimx=get.dimx(obj)
            dimx=size(obj.stim,1);
        end

        function dims=get.dims(obj)
            dims=size(obj.stim,1)*obj.ktlength;
        end
        function s=get.s(obj)
            if isempty(obj.s)
                dxt=size(obj.stim,1);

                %dimensionality of ds
                ds=dxt*obj.ktlength;

                nstim=size(obj.stim,2);
                %number of nss
                nss=nstim^obj.ktlength;

                s=zeros(obj.dims,obj.nstim^obj.ktlength);

                msize=ones(1,obj.ktlength)*obj.nstim;
                for j=1:nss
                    %form s
                    subind=Indexing.subind(msize,j);

                    smat=obj.stim(:,subind);


                    %put the diagonal entries first
                    s(:,j)=smat(:);
                end
                obj.s=s;
            end
            s=obj.s;
        end
        function ss=get.ss(obj)
            if isempty(obj.ss)
                dxt=size(obj.stim,1);
                nstim=size(obj.stim,2);

                %number of nss
                nss=nstim^obj.ktlength;

                %dimensionality of ds
                ds=dxt*obj.ktlength;

                ss=zeros((ds^2+ds)/2,nss);

                %get the linear indexes corresponding to the upper
                %triangular part
                ltriu=triu(reshape(1:ds*ds,ds,ds));
                ltriu=ltriu(ltriu~=0);

                msize=ones(1,obj.ktlength)*nstim;
                for j=1:nss
                    %form s

                    smat=obj.s(:,j)*obj.s(:,j)';

                    %put the diagonal entries first
                    ss(:,j)=smat(ltriu);
                end
                obj.ss=ss;
            end
            ss=obj.ss;
        end
        function iss=isstationary(obj)

            %check if the distribution is stationary
            %its possible that numerical error will cause sum not to be
            %perfectly zero
            if (any(abs(obj.statcon*obj.p(:))>=10^-13))
                %not stationary
                iss=false;
            else
                iss=true;
            end
        end
        
          function iss=isnormalized(obj)

            %check if the distribution is stationary
            %its possible that numerical error will cause sum not to be
            %perfectly zero
            if (abs(sum(obj.p(:))-1)>=10^-13)
                %not stationary
                iss=false;
            else
                iss=true;
            end
        end

        function obj=set.p(obj,p)
            %force p to be a matrix of the proper dimensions
            %we need support for p=[] to handle loading the object.
            if ~isempty(p)
                obj.p=reshape(p,ones(1,obj.ktlength)*obj.nx);
            else
                obj.p=[];
            end
        end
        function statcon=get.statcon(obj)
            if isempty(obj.statcon)
                nrows=sum(obj.nx.^[1:obj.ktlength-1]);

                statcon=zeros(nrows,obj.nx^obj.ktlength);

                msize=obj.nx*ones(1,obj.ktlength);
                rstart=1;
                for i=1:obj.ktlength-1
                    %loop over all possible values for s^{1:i}
                    for j=1:obj.nx^i

                        stimsubind=ind2sub(ones(1,i)*obj.nx,j);

                        cpos=Indexing.lind(msize,[stimsubind nan(1,obj.ktlength-i)]);

                        cneg=Indexing.lind(msize,[nan stimsubind nan(1,obj.ktlength-i-1)]);

                        pvec=zeros(1,obj.nx^obj.ktlength);
                        nvec=zeros(1,obj.nx^obj.ktlength);

                        pvec(:,cpos)=1;
                        nvec(:,cneg)=-1;
                        statcon(rstart+j-1,:)=pvec+nvec;
                    end
                    rstart=rstart+obj.nx^i;
                end

                obj.statcon=statcon;
            end

            statcon=obj.statcon;
        end
        function nx=get.nx(obj)
            if isempty(obj.nx)
                obj.nx=size(obj.stim,2);
            end
            nx=obj.nx;
        end
        function obj=StatDistDisc(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={'stim','ktlength'};
            con(2).rparams={'ps'};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                case 2
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    obj.stim=params.stim;
                    obj.ktlength=params.ktlength;

                    obj.p=ones(ones(1,obj.ktlength)*obj.nstim)*1/(obj.nstim.^obj.ktlength);

                    if isfield(params,'p')
                        obj.p=params.p;
                    else
                        %if no p provided make it a uniform distribution
                        obj.p(:)=1/(obj.nx^obj.ktlength);
                    end

                    %make sure its normalized
                    if (abs(sum(obj.p(:))-1)>10^-8)
                        error('distribution is not normalized.');
                    end
                case 2
                    obj.stim=params.ps.stim;
                    obj.ktlength=params.ps.ktlength;
                    obj.p=params.ps.p;

                    %make sure its normalized
                    if (abs(sum(obj.p(:))-1)>10^-8)
                        error('distribution is not normalized.');
                    end
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end



            %set the version to be a structure
            obj.version.StatDistDisc=090102;
        end
    end

    methods(Static)
        %function to test that linindexes is correct
        pass=teststatcon();


    end
end


