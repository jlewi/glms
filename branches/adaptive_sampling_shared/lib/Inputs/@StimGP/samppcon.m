%function samppcon(gp,spast)
%   pstim - matrix each column is a past stimulus
%
%Explanation: picks the next stimulus by sampling the conditional
%distribution for the Gaussian process.
%
function stim=samppcon(gp,spast)

    %convert to a vector;
    spast=spast(:);
    
    
    %compute the conditional distribution
    a=gp.mus(1:end-gp.klength);
    b=gp.mus(end-gp.klength+1:end);
    
    A=gp.cs(1:end-gp.klength,1:end-gp.klength);
    B=gp.cs(end-gp.klength+1:end,end-gp.klength+1:end);
    C=gp.cs(1:end-gp.klength,end-gp.klength+1:end);

    
    invA=inv(A);
    smu=b+C'*invA*(spast-a);
    svar=B-C'*invA*C;
    
    stim=mvgausssamp(smu,svar);