%function s=getinput(obj,trial)
%
%Return value:
%   s - a matrix of inputs on the specified trials
function s=getinput(obj,trial)

if ~exist('trial','var')
    s=cell2mat(readdata(obj));
else
    s=zeros(obj.dim,length(trial));

    %find out which trials are in the buffer
    tbuffind=[];
    if ~isempty(obj.buffer)
        tbuffind=find(trial>=obj.tstart & trial<=obj.tend);

        if ~isempty(tbuffind)
            s(:,tbuffind)=obj.buffer(:,trial(tbuffind)-obj.tstart+1);
        end

    end

    if (length(tbuffind)<length(trial))
        tnotinbuff=ones(1,length(trial));
        tnotinbuff(tbuffind)=0;

        mat=readdata(obj,trial(logical(tnotinbuff)));
        s(:,logical(tnotinbuff))=cell2mat(mat);

    end
end