%function getmatrix=matrix(obj)
%	 obj=GLMMatrix object
% 
%Return value: 
%	 matrix=obj.matrix 
%   The data reshaped as a matrix
%
function m=getmatrix(obj)
    m=reshape(obj.data,obj.matdim);
