%function sim=GLMInput(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Template for the constructor of a new class. This template
% shows how we can identify which constructor was called based on the
% parameters that were passed in.
%
% Revisions
%   01-11-2009
%       converted to the new object model
%       delete field bname
classdef (ConstructOnLoad=true) GLMInput
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %declare the structure
    properties(SetAccess=protected,GetAccess=public)
        version=struct();
    end

    methods
        function obj=GLMInput(varargin)
            con=[];



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %set the version to be a structure
            obj.version.GLMInput=090111;
        end
    end
end


