%function [eigd, evecs]=rank3mod(eigd,evecs,mu)
%   eigd - diagonal elements. Current eigenvalues
%         These should be sorted in ascending order!!!!!!
%         nx1
%   evecs - existing eigenvectors
%   mu - vector in direction of mean
%          - this is direction to which we want the quadratic form to be
%          othroghonal
%           -should be unit vector
%
%
% Explanation: 
%       Create the diagnalization of our n-1 quadratric form
%       which is due to components orthogonal to the mean.
%       We compute this by performing the rank one modifications
%1st rank1 mod
function [eigd3, evecs3]=rank3mod(eigd,evecs,mu)
mu=mu/(mu'*mu)^.5;
%project mu onto eigenvectors
emean=evecs'*mu;
z=(1+eigd).*emean;
rho=z'*z;
z=z/rho^.5;
rho=-rho;       %because we subtract the rank 1 mod
[eigd1 evecs1]=rankOneEigUpdate(eigd,z,evecs,rho);

%error checking
%C1=C+rho*(evecs*z)*(evecs*z)'

%2nd rank1 mod
%oldevecs=drank3.evecs;
%oldeigd=drank3.eigd;
%z=evecs1'*(C*mu);
z=evecs1'*(evecs*(eigd.*emean));
rho=z'*z;
z=z/rho^.5;
rho=rho;       %because we subtract the rank 1 mod
[eigd2 evecs2]=rankOneEigUpdate(eigd1,z,evecs1,rho);
%C2=C1+rho*(oldevecs*z)*(oldevecs*z)'

%3nd rank1 mod
%oldevecs=drank3.evecs;
%oldeigd=drank3.eigd;
z=evecs2'*mu;
rho=z'*z;
z=z/rho^.5;
%rho=(mu'*C*mu+1)*rho;       %because we subtract the rank 1 mod
rho=(eigd'*(emean.^2)+1)*rho;       %because we subtract the rank 1 mod
[eigd3 evecs3]=rankOneEigUpdate(eigd2,z,evecs2,rho);
%C3=C2+rho*(oldevecs*z)*(oldevecs*z)';