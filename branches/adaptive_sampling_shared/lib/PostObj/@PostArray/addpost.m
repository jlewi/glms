%function parray=addpost(parray,npost,id)
%   parray - an array of GaussPostFile objects
%   npost  - a GaussPost object to add to the array
%   trial     - id to associate with this trial
% Explanation: Expands the array of PostObjects by npost
%   This was added because I need it for GaussPostFile
function parray=addpost(parray,npost,trial)

    %we add 1 to trial because trial=0 is the prior
    parray=setat(parray,npost,trial+1);