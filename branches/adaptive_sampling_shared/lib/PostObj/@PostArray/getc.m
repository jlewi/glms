%function getc=c(obj,trial)
%	 obj=GaussPost object
%    trial = trial on which to get the 
%Return value: 
%	 c=obj.c 
%
%Explanation: If numlabs is >1 we automatically create a distributed
%representation of the covariance matrix
function c=getc(obj,trial)

c=getc(getpost(obj,trial));

    

