%04-09-2008
%   updated for new version of GaussPostFile
%Test harness for GaussPostFile object
clear variables;
setpathvars;

global TMPDIR;
TMPDIR='/tmp/';
fmean=FilePath('bcmd','TMPDIR','rpath','maccessdata.mat');
fmean=seqfname(fmean);
fmean=labBroadcast(1,fmean);


fcovar=FilePath('bcmd','TMPDIR','rpath','caccessdata.mat');
fcovar=seqfname(fcovar);
fcovar=labBroadcast(1,fcovar);

%dimensionality
%mdim=ceil(rand(1)*100));
mdim=50;

%create a new GaussPost object
gobj=GaussPostFile('mfile',fmean,'cfile',fcovar,'m',zeros(mdim,1),'c',eye(mdim),'id',1);


%**************************************************************
%generate some sample posteriors
%**************************************************************
%create an array of GaussPostObjects
%npost=ceil(rand(1)*100);
npost=50;

for index=1:npost
    %randomly generate the covariance matrix
    %set half of them to empty matrices
    if (binornd(1,.5))
        c=randn(mdim,mdim);
        c=c*c';
    else
        c=[];
    end
   gp(index)=GaussPost('m',randn(mdim,1),'c',c); 
end


%*****************************************************************
%test assighnment of a GaussPost object to a GaussPostFile array
%***************************************************************
gp(1)=setc(gp(1),[]);

gobj=setm(gobj,getm(gp(1)),1);
gobj=setc(gobj,getc(gp(1)),1);

for index=2:npost
   gobj=addpost(gobj,gp(index),index); 
end


%check the data matches
for index=1:npost
   if any(getm(gobj,index)~=getm(gp(index)))
       error('m on trial %d is wrong', index);
   end
   if isempty(getc(gp(index)))
       if ~isempty(getc(gobj,index))
            error('c on trial %d should be empty', index);
       end
   elseif any(getc(gobj,index)~=getc(gp(index)))
       error('c on trial %d is wrong', index);
   end
   
end

%test getlasttrial
if (getlasttrial(gobj)~=npost)
    error('getlasttrial is not valid');
end
%******************************************************
%Test: Loading/saving of the object
%***********************************
%%
fprintf('Testing: Loading/saving of the object \n');

fgauss=FilePath('bcmd','TMPDIR','rpath','gaussobj.mat');
fgauss=seqfname(fgauss);

save(getpath(fgauss),'gobj');
lobj=load(getpath(fgauss),'gobj');
lobj=lobj.gobj;

for index=1:length(gobj)
   if any(getm(gobj,index)~=getm(lobj,index))
       error('m on trial %d is wrong', index);
   end
   if isempty(getc(gobj,index))
       if ~isempty(getc(lobj,index))
            error('c on trial %d should be empty', index);
       end
   elseif any(getc(gobj,index)~=getc(lobj,index))
       error('c on trial %d is wrong', index);
   end
   
end

fprintf('Test Successful: Loading/saving of the object \n');

%*******************************************************************
%Test: Create a GaussPostFile object from an mfile and cfile containing
%   lots of matrices
%*********************************************************************
%%
fprintf('Testing: creating GaussPostfile object from an mfile and cfile with data \n');

lobj=GaussPostFile('mfile',fmean,'cfile',fcovar);

for index=1:length(gobj)
   if any(getm(gobj,index)~=getm(lobj,index))
       error('m on trial %d is wrong', index);
   end
   if isempty(getc(gobj,index))
       if ~isempty(getc(lobj,index))
            error('c on trial %d should be empty', index);
       end
   elseif any(getc(gobj,index)~=getc(lobj,index))
       error('c on trial %d is wrong', index);
   end
   
end
fprintf('Test succsessful. \n');

fprintf('Success: GaussPostFile works \n');
