%function getsubm=subm(obj)
%	 obj=LRMixturePost object
% 
%Return value: 
%	 subm=obj.subm 
%
function subm=getsubm(obj,ind)
    if exist('ind','var')
        subm=obj.subm(ind);
    else
        subm=obj.subm;
    end
        
