%function LRMixturePost('fpost',fpost,'mobj',mobj,'stim',stim,'obsrv')
%   fpost - Gaussian representation of the posterior over the full model
%      space
%   mobj  - model object
%   stim  - the inputs should be a matrix of the actual inputs
%   obsrv - the observations
%
%function LRMixturePost('fpost',fpost,'subm',subm,'submpost',submpost)
%   fpost- full posterior
%   subm - array of submodels
%   submpost - posterior on the submodel
% Explanation : This constructure takes fpost the Gaussian posterior on the full space of
%   models and represents it as a mixture. The number of submodels is
%   determined by the alogrithm for building the mixture
%
% Explanation: Template for the constructor of a new class. This template
% shows how we can identify which constructor was called based on the
% parameters that were passed in.
%
function obj=LRMixturePost(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
% con(1).rparams={'nmodels','m','apost','pmix','fpost'};
% con(1).cfun=1;
%
con(1).rparams={'fpost','mobj','stim','obsrv','nmodels'};
con(1).cfun=1;

con(2).rparams={'fpost','subm','submpost'}
con(2).cfun=2;
%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%nmodels - number of models in this mixture
%		 subm -1xnmodels structure array describing each model in the mixture
%			 .basis - a set of rank dxd rank 1 matrices
%				 This matrices form a vector space on which we represent the posterior
%		 submpost -  1xnmodels array of Gaussian posterior
%			- Represents the conditional poster 	p(\sigma|\M,\covar[t],\mu[t])%
%			- This is our posterior for that model in the mixture.
%
%		 Pmix - 1xnmodels array of probabilities.
%			 This is the probability of \theta being in a given mixture.
%		 Fpost - Gauss post the posterior represented on all matrices

%declare the structure
obj=struct('version',071116,'nmodels',[],'subm',[],'submpost',[],'pmix',[],'fpost',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'LRMixturePost',PostObjBase());
        return
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        %instantiate a base class if there is one
        obj.fpost=params.fpost;
        %number of models 1+dim(\theta)
        %1 model for the mean and each eigenvector
%        obj.nmodels=prod(getmatdim(params.mobj))+1;
        obj.nmodels=params.nmodels;
        params=rmfield(params,'nmodels');
        if (obj.nmodels > (getmatdim(params.mobj)+1))
            error('nmodels exceeds maximum value');
        end
        mobj=params.mobj;
        params=rmfield(params,'fpost');
        params=rmfield(params,'mobj');
        
        stim=params.stim;
        params=rmfield(params,'stim');
        
        obsrv=params.obsrv;
        params=rmfield(params,'obsrv');
        
        %pass all arguments to base class
        %pbase=PostUpdatersBase(params);

        
        %if no base object
        obj=class(obj,'LRMixturePost',PostObjBase());

        %call build mixture to build the mixture
        obj=buildmixture(obj,mobj,stim,obsrv);
    case 2
        obj.subm=params.subm;

        %set the submodel post 
        obj.submpost=params.submpost;       
        obj.nmodels=length(obj.subm);
        obj.fpost=params.fpost;
        %remove fields
        for fi=1:length(con(2).rparams)
            params=rmfield(params,con(2).rparams{fi});
        end
        
        %pass all arguments to base class
      %

        %if no base object
        obj=class(obj,'LRMixturePost',PostObjBase());

    otherwise
        error('Constructor not implemented')
end


%*********************************************************
%Create the object
%****************************************





