%function obj=buildmixture(obj,mobj)
%   obj - LRMixturePost
%   mobj - MBilinear obj
%   stim - the stimuli on each trial
%   obsrv - the observations on each trial
%
%Explanation : builds a mixture model of the posterior.
%   This is meant as an internal function.
%   It should only be called by the appropriate constructor of
%   LRMixturePost. The constructor sets all the relevant properties of obj.
%
%   We need the stimuli and observations to estimate the posteriors for
%   each model.
function obj=buildmixture(obj,mobj,stim,obsrv)
%get the full posterior
fpost=obj.fpost;



%what rank do we use to approximate the matrix
rank=getrank(mobj);

%get the dimensions of the matrix
mdim=getmatdim(mobj);

%currently only support rank 1
if (rank>min(getmatdim(mobj)))
    error('rank cannot exceed smallest dimension of matrix.')
end

% if (obj.nmodels~=prod(getmatdim(mobj))+1)
%     error('Algorithm for building mixture model assumes number of models = dim(\theta)+1');
% end

%create a newton1d object to update the posterior for each model
unewt=Newton1d();
optim=getoptim(unewt);
optim=optimset(optim,'TolFun',10^-8);
unewt=setoptim(unewt,optim);

%set the prior for each submodel
obj.pmix=zeros(obj.nmodels,1);
basis=zeros(mdim(1),mdim(2),rank);

%********************************************************************
%Construct the model for the mean
mi=1;
%change the mean into a matrix
tmat=formmat(mobj,getm(fpost));

%compute the svd of tmat
[u,s,v]=svd(tmat);
for ri=1:rank
    basis(:,:,ri)=u(:,ri)*v(:,ri)';
end

obj.subm=MBiLinearSub('basis',basis,'glm',getglm(mobj),'mmag',getmag(mobj),'pinit',getsubmprior(mobj));

%we need to
%create the posterior on this model
%we need to project the inputs into this model space
inpsub=projinp(obj.subm(mi),stim,obsrv);

obj.submpost=update(unewt,getprior(obj.subm),inpsub,obj.subm(mi),obsrv);

%**************************************************************************
%set the mixing component
%now we set the mixing components
%we do this by setting the mixture probablities proportional to the
%probability of the posterior evaluated at the mean of each mixture

%get the mean matrix from each submodel
mat=formmat(obj.subm(mi),getm(obj.submpost( mi)));

%convert the matrix to a vector
muvec=formvec(mobj,mat);

%evaluate the log posterior for this model.
obj.pmix(mi)=logpost(getglm(mobj),muvec,muvec'*getData(stim),obsrv,getfpost(getprior(mobj)))

%*****************************************************************
%construct model for each eigenvector
%*************************************************************
efull=getefull(fpost);

%for eigenvectors we take the smallest and largest eigenvectors
klength=getklength(mobj);
eindm=[1:floor((obj.nmodels-1)/2) (klength-ceil((obj.nmodels-1)/2)+1):klength];
for mi=2:(length(eindm)+1)
    %which eigenvector we are using
    eind=eindm(mi-1);
    evec=getevecs(efull,eind);

    %change evec into a matrix
    emat=formmat(mobj,evec);

    %compute the svd of emat
    [u,s,v]=svd(emat);

    %build the model
    for ri=1:rank
        basis(:,:,ri)=u(:,ri)*v(:,ri)';
    end


obj.subm(mi)=MBiLinearSub('basis',basis,'glm',getglm(mobj),'mmag',getmag(mobj),'pinit',getsubmprior(mobj));

    %we need to
    %create the posterior on this model
    %we need to project the inputs into this model space
    inpsub=projinp(obj.subm(mi),stim,obsrv);


obj.submpost(mi)=update(unewt,getprior(obj.subm(mi)),inpsub,obj.subm(mi),obsrv);


    %**************************************************************************
    %set the mixing component
    %now we set the mixing components
    %we do this by setting the mixture probablities proportional to the
    %probability of the posterior evaluated at the mean of each mixture

    %get the mean matrix from each submodel
    mat=formmat(obj.subm(mi),getm(obj.submpost( mi)));

    %convert the matrix to a vector
    muvec=formvec(mobj,mat);

    %evaluate the log posterior for this model.

    obj.pmix(mi)=logpost(getglm(mobj),muvec,muvec'*getData(stim),obsrv,getfpost(getprior(mobj)));

end

%pmix is currently set to logposterior so we take exponential and then
%normalize
obj.pmix=exp(obj.pmix);
obj.pmix=obj.pmix/(sum(obj.pmix));

if any(isnan(obj.pmix))
    error('mixing coefficients are nan');
end
