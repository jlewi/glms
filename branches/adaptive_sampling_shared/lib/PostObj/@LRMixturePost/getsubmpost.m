%function getsubpost=subpost(obj)
%	 obj=LRMixturePost object
% 
%Return value: 
%	 subpost=obj.subpost 
%
function subpost=getsubmpost(obj,ind)
	
    if exist('ind','var')
        subpost=obj.submpost(ind);
    else
        subpost=obj.submpost;
    end