%function map=getmap(obj,mi)
%	 obj-LRMixturePost object
%    mi - which submodel 
%       0 - means for the full model
%       otherwise its the index of the submodel to compute it for
%Return value: 
%	 submmap=obj.submmap 
%
%Explanation: Computes the MAP for a submodel and returns it a as a matrix.
function m=getmap(obj,mi)
	 
if (mi==0)
    %get the dimensions from one of the submodels
    basis=getbasis(obj.subm(1));
    matdim=size(basis);
    m=reshape(getm(obj.fpost),matdim(1:2));
else
    subm=obj.subm(mi);
    m=formmat(obj.subm(mi),getm(obj.submpost(mi)));
end
