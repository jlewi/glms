%function obj=loadobj(lobj)
%
%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
%
%Revisions:
%   080108 - cleaned up the revision code.
function obj=loadobj(lobj)

%check if lobj is a structure
%this indicates the class structure has changed and we need to handle
%the conversion
fprintf('PostTanSpace loadobj.m\n');
if isstruct(lobj)
    %create a blank object

    obj=PostTanSpace();
    latestver=obj.version;
    obj.version=lobj.version;
    %******************************************************************
    %Sequentially convert one version of the object to the next
    %until we get to the latest version
    %******************************************************************
    if (any([obj.version] <080128))
        warning ('Version of PostTanSpace was old messed up version. I.e PostTanSpace did not represent the posterior in theta space after setting variance orthogonal to tangent space to zero');
    end

    if (any([obj.version]<080730))
        warning('Loss of information converting old  version of PostTanSpace to new object model for the tangent space. Only the full posterior is copied. Additional info is lost.');
       
          fskip={'tanspace','PostObjBase'};    %fields not to copy      
        obj=copyfields(lobj,obj,fskip);
         obj.version=080730;
    end

    if (obj.version<080801)
        %we added infopost in this version
        %we don't need to do any conversion
        obj.version=080801;
    end
 
    %check if converted all the way to latest version
    if (obj.version <latestver)
        error('Object has not been fully converted to latest version. \n');
    end


else
    obj=lobj;
end

%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
function dest=copyfields(source,dest,skip)
%we just need to copy the fields
%and handle any special cases if required
fnames=fieldnames(source);

%struct for object
sobj=struct(dest);
for j=1:length(fnames)
    switch fnames{j}
        case skip
            %do nothing we skip this field
        otherwise
            %set the new field this will cause an error
            %if the field isn't a member of the new object
            dest.(fnames{j})=source.(fnames{j});
    end
end
