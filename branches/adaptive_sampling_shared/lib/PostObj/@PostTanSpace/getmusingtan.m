%function getm=m(obj)
%	 obj=PostTanSpace object
% 
%Return value: 
%	 m=obj.m 
%       -mean of the full posterior
%
%Explanation: We compute the mean in theta space but using information
%about the tangent space.
%  The mean in theta space
%   m=ptheta + B mu_b
%      ptheta- projection of mean of full posterior onto manifold
%      B   - basis of the tangent space
%      mu_b - mean on the tangent space parameters
function m=getmusingtan(obj)

    m=zeros(getdim(obj(1).fullpost),length(obj));
    
    for ind=1:length(obj)
        ptheta=gettheta(obj(ind).tanspace);
    
        B=getbasis(obj(ind).tanspace);
        mub=getm(obj(ind).tanpost);
    
        m(:,ind)=ptheta+B*mub;
    end