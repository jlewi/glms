%PostTanSpace(fullpost,tanpost,tanpoint)
%   fullpost - posterior on the full theta space
%   tanpost  - posterior on the tangent space
%   tanpoint - the point at which the tangent space is computed
%            - The tangent point should be the projeciton of the MAP of the
%            full posterior onto the manifold and correspond to the MAP of
%            tanpost
%
%PostTanSpace(fullpost,mobj)
%   fullpost - fullposter
%   mobj - the MTanSpace model
%   This constructor automatically computes the tanspace object from the fullpost.
%
%
%Explanation: Represent the posterior on some tangent space.
%
%
%Revisions:
%   08-01-2008
%       Add the Transient, property info. post.
%   07-30-2008
%       Start recoding this object using my new tangent space model.
%       In this new model, the parameters defining the tangent space are
%       stored as part of the model object. The model object should be
%       passed in as the input for the appropriate methods.
%
%       Deleted fields:
%           tanspace
%
%       New Fields
%           tanpoint- Keeps track of the point at which we construct the
%               tangent space
%
%       Also convert to Matlab's new OOP model.
%
%       Since we have to construct the Posterior on every object
%       we don't want to parse the constructor using string parsing,
%       because it will be slow.
%
%   03-11-2008:
%   The way I was computing the posterior on the tangent space was wrong.
%	The new way to compute the posterior is described here 14. Derviation
%	Tanpost is a Gaussian whose dimensionality= dim (tan space)
%	New code will be revision >1854 (1854 is old version)
%
classdef (ConstructOnLoad=true) PostTanSpace < PostObjBase
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %  fullpost    - GaussObj - full posterior on \Theta Space
    %  tanpost  - GaussObj - Posterior on the tangent space
    %  tanpoint - object represecting the point in theta space
    %              where the tangent space is computed
    %declare the structure

    properties(SetAccess=private,GetAccess=public)
        version=080801;
        fullpost=[];
        tanpost=[];
        tanpoint=[];

    end

    %*************************************************************
    %Transient Properties
    %*****************************************************************
    %   infopost - this posterior lives on the full theta space
    %              but its computed using the posterior on the tangent
    %              space
    %              this is the posterior used to optimize the stimulus
    properties(Transient=true,SetAccess=private,GetAccess=public)
        infopost=[];
    end
    methods
        function obj=PostTanSpace(varargin)




            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
                    return

                case 2
                    obj.fullpost=varargin{1};

                    mobj=varargin{2};




                    %to compute the posterior on the tangent space use
                    %TanSpaceUpdater/comptanpost
                    uobj=TanSpaceUpdater();
                    [obj.tanpoint,obj.tanpost]= comptanpost(uobj,obj.fullpost,mobj);


                case 3

                    obj.fullpost=varargin{1};
                    obj.tanpost=varargin{2};
                    obj.tanpoint=varargin{3};


                otherwise
                    error('Constructor not implemented')
            end

        end
        function infopost=get.infopost(post)
            %compute infopost if we haven't already
            if isempty(post.infopost)

                fpost=post.fullpost;
                tanpost=post.tanpost;
                tanpt=post.tanpoint;
                if ~isempty(tanpt)
                basis=tanpt.basis;
                else
                    basis=[];
                end
                
                if (~isempty(fpost) && ~isempty(tanpost) && ~isempty(basis))
                minfo=getm(fpost)+basis*getm(tanpost);
                cinfo=basis*getc(tanpost)*basis';
                %compute eigendecomp of cinfo because we will need it for the stimulus
                %optimization.
                post.infopost=GaussPost('m',minfo,'c',cinfo);
                end
            end
            infopost=post.infopost;
        end
    end
end






