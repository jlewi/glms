%function p=rotatebysubmat(post,v)
%       post - GaussPost object
%       v    - vector
%
% Result:
%   p=esub.evecs*esub.eigd*(esub.evecs'*v);
%   
% Explanation: This function computes the product of a submatrix and a
% vector
%   It does this efficiently using the eigenvectors of the sub matrix if
%   they are represented or using the covariance matrix if thats whats
%   stored.
%
function p=rotatebysubmat(post,v)

if (post.s==0)
    error('No submatrix is represented');
end
evecs=getevecs(post.esub);
eigd=geteigd(post.esub);
p=evecs*(eigd.*(evecs'*v));

