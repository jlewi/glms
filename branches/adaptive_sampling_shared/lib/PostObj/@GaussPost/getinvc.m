%function getinvc=invc(obj)
%	 obj=GaussPost object
% 
%Return value: 
%	 invc=obj.invc 
%    Inverse of the covariance matrix.
function invc=getinvc(obj)

if ~isempty(obj.efull)
   invc=getinv(obj.efull);
else
    invc=inv(getc(obj));
end
