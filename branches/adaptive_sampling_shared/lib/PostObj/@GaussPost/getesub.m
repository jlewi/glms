%function getesub=esub(obj)
%	 obj=GaussPost object
% 
%Return value: 
%	 esub=obj.esub 
%
function esub=getesub(obj)
	 esub=obj.esub;
