%function [p, dl, d2l]=logpdf(gobj, val)
%   gobj - GaussPost object
%   val  - slen x n - values whose probability we want to evaluate under
%   the Gaussian Posterior
%
%Explanation: Evaluates the log probability of the values in val.
%   I could speed this up because logmvgauss does an svd and we might
%   already have the svd stored.
%
%   dl- first derivative
%   d2l- 2nd derivative
function varargout=logpdf(gobj,val)

lp=logmvgauss(val,getm(gobj),getc(gobj));

varargout{1}=lp;

if (nargout>=2)
    invc=getinvc(gobj);
   dl=-invc*(val-getm(gobj)); 
   
   varargout{2}=dl;
end

if (nargout>=3)
    if (size(val,2)>1)
        error('need to modify code if want to return more than 1 second derivative.');
    end
    varargout{3}=-invc;
end
% nvals=size(val,2);
% dm=(val-getm(gobj)*ones(1,val));
% 
% cinv=getcinv(gobj);
% 
% lp=-.5*qprod(val,cinv);
% 
% lp=lp-log (2*pi)