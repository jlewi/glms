%function getm=m(obj,ind)
%	 obj=GaussPost object
%    ind = indexes when we have an array of objes
%Return value: 
%	 m=obj.m 
%
function m=getm(obj,ind)
if ~exist('ind','var')
	 m=[obj.m];
else
    m=[obj(ind).m];
end