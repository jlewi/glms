%function obj=setc(obj,m)
%	 obj-GaussPostFile object
%      m - posterior mean
%
%Return value: 
%	 obj= the modified object 
%
%Explanation: Write the value of c to the file 
% load the new value of c into inmemptr
function obj=setm(obj,m,trial)
    
overwrite=true;
maccess=getmaccess(obj);
   
maccess=writematrix(maccess,m,trial,overwrite);

obj=setmaccess(obj,maccess);
%update the value in memory
obj=setinmemptr(obj,trial,m,nan);

