%function lasttrial=getlasttrial(obj)
%	 obj=GaussPostFile object
% 
%Return value: 
%	 lasttrial=obj.lasttrial 
%
%Explanation get the id/trial number associated with the last mean
function lasttrial=getlasttrial(obj)
	 lasttrial=getlastid(getmaccess(obj));
