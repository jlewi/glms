%function obj=setc(obj,c,trial)
%	 obj-GaussPostFile object
%      c - covariance matrix
%
%Return value: 
%	 obj= the modified object 
%
%Explanation: Write the value of c to the file 
% load the new value of c into inmemptr
function obj=setc(obj,c,trial)
    
overwrite=true;
caccess=getcaccess(obj);
   
%if c is empty then we delete the matrix from the file if it exists
if isempty(c)
   %make sure covariance matrix is in file
if ~isnan(hasid(caccess,trial))
   caccess=deletematrix(caccess,trial);
end 
else
caccess=writematrix(caccess,c,trial,overwrite);
end

%update cacces
obj=setcaccess(obj,caccess);

%update the value in memory
obj=setinmemptr(obj,trial,nan,c);

