%function getcaccess=caccess(obj)
%	 obj=GaussPostFile object
% 
%Return value: 
%	 caccess=obj.caccess 
%
function caccess=getcaccess(obj)
	 caccess=obj.cinfo.caccess;
