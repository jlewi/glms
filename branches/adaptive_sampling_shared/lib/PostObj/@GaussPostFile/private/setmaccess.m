%function obj=setmaccess(obj,val)
%	 obj=GaussPostFile object
% 
%Return value: 
%	 obj= Sets the pointer for the RAccessFile object for the mean
%
function obj=setmaccess(obj,val)
	 obj.minfo.maccess=val;
