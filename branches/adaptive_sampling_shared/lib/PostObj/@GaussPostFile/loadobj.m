%function obj=loadobj(lobj)
%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
%
%Revisions: 08-25-2008
%   rewrite the conversion methods in a more structured fashion
%
function obj=loadobj(lobj)

%get the latest version
%save the latest version so we can check to make sure all
%conversions have been completed.
newobj=GaussPostFile();
latestver=newobj.version;

if isstruct(lobj)
    obj=newobj;

    obj.version=lobj.version;
else
    %we don't need to create a new object
    obj=lobj;
end


%check if lobj is a structure
%this indicates the class structure has changed and we need to handle
%the conversion
if (obj.version<latestver)
    %create a blank object



    %******************************************************************
    %Sequentially convert one version of the object to the next
    %until we get to the latest version
    %******************************************************************
    if (obj.version < 080630)
        %           The following fields have been replaced
        %           (inmemptr, caccessptr, maccessptr)
        %            with the fields
        %            (inmem,caccess, maccess)
        %do conversion
        %copy fields from old version to new version
        error('Conversion method for objects older than 080630 has not been written');
        fskip={};    %fields not to copy
        obj=copyfields(lobj,obj,fskip);
        obj.version=080630;
    end

    if (obj.version < 080825)
        %do nothing all we did was declare inmem to be a transient
        %property
        obj.version=080825;
    end
    %check if converted all the way to latest version
    if (obj.version <latestver)
        error('Object has not been fully converted to latest version. \n');
    end


else
    obj=lobj;
end

%we need to initialize inmem
obj.inmem=struct('m',[],'c',[],'id',[]); 


