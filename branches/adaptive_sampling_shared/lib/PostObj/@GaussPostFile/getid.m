%function getid=id(obj)
%	 obj=GaussPostFile object
% 
%Return value: 
%	 id=obj.id 
%
function id=getid(obj)
        warning('id is obsolete since version 080409');
	 id=obj.id;
