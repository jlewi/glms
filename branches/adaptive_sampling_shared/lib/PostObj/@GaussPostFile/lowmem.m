%function lowmem(pobj,trial)
%
%Explanation: COnvert this Posterior object into a low memory version.
%   i.e zero out the covariance matrix.
function pobj=lowmem(obj,trial)

%delete the covariance matrix in the file
caccess=getcaccess(obj);
   
%make sure covariance matrix is in file
if hasid(caccess,trial)
    obj.cinfo.caccess=deletematrix(caccess,trial);
end

%update the value in memory
obj=setinmemptr(obj,trial,nan,[]);