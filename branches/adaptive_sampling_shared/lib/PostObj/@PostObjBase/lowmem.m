%function obj=lowmem(obj)
%   obj - PostObjBase
%
%Explanation: Creates a low memory representation of the posterior. Usually
%this means zeroing out the covariance matrix. This function should be
%overwritten by the child class.
function obj=lowmem(obj)

warning('Lowmem called for PostObjBase. This should be overwritten by the child class.');
