%function sim=PostObjBase(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Base class for objects representing the posterior.
%
%
%Revisions:
%   07-30-2008
%       Convert to Matlab's new object model
%%
%       Rename version bversion otherwise we will get name conflict
%       b\c subclasses also define version
%
%08-04-2008: I added this line to test merging in svn
classdef (ConstructOnLoad=true) PostObjBase

    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %  bname  - name of base class. This allows us to refer to it templates
    %declare the structure
    properties (SetAccess=private,GetAccess=public)
        bversion=080730;
    end
    methods

        function obj=PostObjBase(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};
            con(1).cfun=1;




            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    return
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1

                otherwise
                    error('Constructor not implemented')
            end


            %*********************************************************
            %Create the object
            %****************************************
            %instantiate a base class if there is one

        end
    end
end






