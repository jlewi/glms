%function oinfo=objinfo(obj)
%   obj - object
%
%oinfo - 2d cell array which describes this object
%
%Explanation: idea is to populate a cell array with information about this
%object. We can then pass this cell array to oneNoteTable to create a table
%which can be imported into matlab.
%
%$Revision: 1474 $ - use structinfo to create a cell array which contains
% information about the fields of this object. This is useful for creating 
% tables to send to OneNote.
%
function oinfo=objinfo(obj)

oinfo={'class',class(obj),'field','value'};

s=struct(obj);

%remove fields we don't want to grab info for
fdell={};
for f=fdell
try
s=rmfield(s,f);
catch    
    warning('%s.objinfo: %s \n',class(obj),lasterr);
end
end
oinfo=[oinfo;structinfo(s)];


    
