%function getm=m(obj)
%	 obj=PostObjBase object
% 
%Return value: 
%	 m=obj.m 
%
function m=getm(obj)
	 m=[];
     Warning('getm called on PostObjBase. This function should be overwritten by child class.');
