%function [eigd, evecs]=rank2mod(eigd,evecs,mu)
%   eigd - diagonal elements. Current eigenvalues
%         These should be sorted in ascending order!!!!!!
%         nx1
%   evecs - existing eigenvectors
%   mu -nxk 
%           - k is the number of eigenvectors we orthogonalize to
%           vectors in direction of mean
%          - this is direction to which we want the quadratic form to be
%          othroghonal
%           -should be unit vector
%
%
% Explanation: 
%       Create the diagnalization of our n-1 quadratric form
%       which is due to components orthogonal to the mean.
%       We compute this by performing the rank one modifications
%1st rank1 mod
function [eigd, evecs]=rank2mod(eigdfull,evecsfull,mu)
d=size(mu,1);
nmu=size(mu,2);
%normalize mu
%make sure mu is unit vectors
mu=mu./(ones(d,1)*sum(mu.^2,1).^.5);



eigd=eigdfull;
evecs=evecsfull;
for mind=1:nmu
    
    %project mu onto eigenvectors
    emean=evecs'*mu(:,mind);
    %to compute rank one modiffications we need two quantities
    %mu'Cmu
    qmu=-eigd'*(emean.^2);
    erotate=evecs*(eigd.*(emean));
    
    %compute our rank1 modification vector
    z1=(qmu+2)/2*mu(:,mind)+erotate;
    z1=evecs'*z1;
    rho1=z1'*z1;
    z1=z1/rho1^.5;
    %since the rank one shoud be subtract it we multiply rho by -
    rho1=-1/2*rho1;       %because we subtract it
    [eigd1 evecs1]=rankOneEigUpdate(eigd,z1,evecs,rho1);

    %compute our 2nd rank 1 modification vector
    z=(qmu-2)/2*mu(:,mind)+erotate;
    z=evecs1'*z;
    rho=z'*z;
    z=z/rho^.5;
    rho=1/2*rho;
    [eigd evecs]=rankOneEigUpdate(eigd1,z,evecs1,rho);

 %debugging
 if (mind==1)
    Cfull=evecsfull*diag(eigdfull)*evecsfull';
    Clast=Cfull;
 else
     Clast=Cmod;
 end
  b=-mu(:,mind)'*Clast*mu(:,mind);
  v1=((b/2+1)*mu(:,mind)+Clast*mu(:,mind));
 v2=((b/2-1)*mu(:,mind)+Clast*mu(:,mind));
  Cmod=Clast-1/2*v1*v1'+1/2*v2*v2';
    fprintf('for break pt\n');

end