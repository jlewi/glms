%function [pmethods]=initmethods(mon)
%   mon - structure specifying which methods to turn on 
%           methods.bg  - brute force with gaussian posterior
%           methods.b   - brute force
%           methods.ekf - ekf 
%           methods.g   - gaussian update based on moments
%           methods.batchg -batch update using gaussian 1-d integration
%                           -only works if stimulus is 1d
%                  .ekf1step
%                       .on  - on or off
%                       .ekffunc - pointer to function to compute ekf
%                                  update
%                       .onestep - number of iterations between doing one
%                                 step updates
%           If these fields do not exist that method is not computed.
%           Otherwise non-zero means its off and 1 means on;
function [pmethods]=initmethods(methods)
pmethods=[];
%*************************************************************************
%setup pmethods structure
%************************************************************************
%methods specifies which methods to use
%turn appropriate methods on or off
if isfield(methods,'bg')
    pmethods.bg.on=methods.bg;
else
    pmethods.bg.on=0;
end

if isfield(methods,'ekf')
    pmethods.ekf.on=methods.ekf;
else
    pmethods.ekf.on=0;
end

if isfield(methods,'ekfmax')
    pmethods.ekfmax.on=methods.ekfmax;
else
    pmethods.ekfmax.on=0;
end

if isfield(methods,'ekf1step')
    pmethods.ekf1step=methods.ekf1step;
else
    pmethods.ekf1step.on=0;
end

%default ekf function to use
if ~isfield(pmethods.ekf1step,'ekffunc')
    pmethods.ekf1step.ekffunc=@ekfposteriormax;
end

%default values
if ~isfield(pmethods.ekf1step,'onestep')
    pmethods.ekf1step.onestep=5;
end

if isfield(methods,'g')
    pmethods.g.on=methods.g;
else
    pmethods.g.on=0;
end

if isfield(methods,'b')
    pmethods.b.on=methods.b;
else
    pmethods.b.on=0;
end

if isfield(methods,'batchg')
    pmethods.batchg.on=methods.batchg;
else
    pmethods.batchg.on=0;
end

if isfield(methods,'gasc')
    pmethods.gasc=methods.gasc;
else
    pmethods.gasc.on=0;
end

if isfield(methods,'newton')
    pmethods.newton=methods.newton;
else
    pmethods.newton=[];
end
if ~isfield(pmethods.newton,'on')
    pmethods.newton.on=0;
end

if ~isfield(pmethods.newton,'nsteps')
    pmethods.newton.nsteps=1;
end

if ~isfield(methods.newton,'tolerance')
    pmethods.newton.tolerance=10^-10;
else
    pmethods.newton.tolerance=methods.newton.tolerance;
end

if ~isfield(methods,'allobsrv')
    pmethods.allobsrv.on=0;
else
    if isfield(methods.allobsrv,'on')
        pmethods.allobsrv.on=methods.allobsrv.on;
    else
         pmethods.allobsrv.on=1;
    end
end
%************************************************************
%newton1d
%***************************************************************
if ~isfield(methods,'newton1d')
    pmethods.newton1d.on=0;
else
    if ~isfield(methods.newton1d,'on')
        pmethods.newton1d.on=methods.newton1d;
    else
        pmethods.newton1d.on=methods.newton1d.on;
    end
end

if ~isfield(methods.newton1d,'tolerance')
    pmethods.newton1d.tolerance=10^-10;
else
    pmethods.newton1d.tolerance=methods.newton1d.tolerance
end

if ~isfield(methods.newton1d,'maxdm')
    pmethods.newton1d.maxdm=1;
else
    pmethods.newton1d.maxdm=methods.newton1d.maxdm;
end
%labels for methods
pmethods.bg.label='Brute Force with Gaussian';
pmethods.g.label='Gaussian';
pmethods.ekf.label='EKF';
pmethods.ekfmax.label='EKF Max';
pmethods.ekf1step.label='EKF 1 step';
pmethods.b.label='Brute Force';
pmethods.batchg.label='Batch Update, Moment Based';
pmethods.gasc.label='Gradient Ascent';
pmethods.newton.label='Newton';
pmethods.newton1d.label='Newton 1d';
