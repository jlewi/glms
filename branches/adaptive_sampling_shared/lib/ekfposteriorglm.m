%function [pekf,pll]=ekfposteriorglm(prior,x,obsrv,fu)
%       prior - structure representing the gaussian prior
%              .m - mean
%              .c - covariance matrix
%       x     - column vector representing the stimulus
%       f    - pointer to the nonlinear function and its derivatives which computes
%               the rate as a fcn of u=k'x
%       obsrv - struture representing the observations
%              .n - number of spikes
%              .twindow -window in which spikes are occured
%       k     - structure providing information about
%               the model parameters
%             .d      -dimensionality of the k vector
%Return value:
%   pekf - gaussian representing the updated posterior
%       .m - mean
%       .c - covariance
%
%   pll - gaussian approximation of the likelihood function
%Explanation:
%   Updates the posterior for a glm by matching its 1st and 2nd moments
%   assuming posterior is represented as a gaussian. This update
%   is based on the EKF idea.
% 
%   That is we do a taylor expansion of the log likelihood. By taking just
%   the first and 2nd order terms we approximate the likelihood function as
%   a gaussian. The posterior is therefore the product of 2 gaussians.
%
%   We use the woodbury inversion lemma to efficently calculate the
%   covariance matrix of the new posterior.
%
%   The response/observations (obsrv) is the number of spikes produced by a
%   homogenous proisson process in the window of length twindow.
%
%   This assumes batch processing. That is we the response observed in the
%   window of length twindow is due entirely to the stimulus presented at
%   x;
%   
%   Assumes response is homogenous during window in which spikes observed
function [pekf,pll]=ekfposteriorglm(prior,x,obsrv,fu)

%whether or not to plot debugging graphs
plotson=0;
lpost.on=0;
lprior.on=0;
llike.on=1;
ltlike.on=1;        %true log likelihood
lekfpost.on=0;

%get the derivatives of fu. and the expected rate
%r0, r1, and r2 are the rate and its 1st and 2nd derivtives
%that is r0 is the average number of spikes per unit time.
%it is the evluation of the nonlinear function in the glm
[r0,r1,r2]=fu(prior.m'*x);

%We need to scale the terms by obsrv.twindow
%to get the expected number of spikes in the observed window
twin=obsrv.twindow;
r0s=r0*obsrv.twindow;
r1s=r1*obsrv.twindow;
r2s=r2*obsrv.twindow;

%g is the log likelihood up to a scaling factor
g=-r0s+ obsrv.n*log(r0s);
gd1=-r1s+obsrv.n*1/(r0s)*r1s;
gd2=-r2s+obsrv.n*(-1/(r0s^2)*(r1s)^2+1/r0s*r2s);


%compute lambda the inverse of the covariance matrix of the 
%gaussian approximating the log likelihood function (this will be singular)
lambda=-gd2*x*x';

%mean of the gaussian approximation of the likelihood
if (nargout>1)
pll.eta=(gd1*x+lambda*prior.m); %canonical parameter
pll.lambda=lambda;
pll.c=inv(lambda);      %this is only needed for debugging for higher d it will be singular
pll.m=pll.c*pll.eta;
end
%check 
%pekf.c=prior.c-(-1/gd2+sigma)*(prior.c*x)*(prior.c*x)';
%Compute the new covariance matrix without using the woodbury lemma
%cnew=prior.c-prior.c*x*inv((-1/gd2+x'*prior.c*x))*x'*prior.c
%use the woodbury.
pekf.c=prior.c-prior.c*x*(-gd2/(1-gd2*x'*prior.c*x))*x'*prior.c;

%This is the correct formula for combining the covariances
%of two gaussians so use it to check the evaluation using the
%woodbury formula.
%pekf.c=inv(inv(prior.c)+lambda);

%the update rule I think is correct
%we weight prior.m by pekf.c*inv(prior.c)
%Below is the equation to calculate new mean when multiply two gaussians
%it is inefficent because we calculate the inverse
%pekf.m=pekf.c*inv(prior.c)*prior.m+pekf.c*-gd2*x*x'*mll;
%
%We can use the expression for the new covariance matrix to
%simplify the calculation of the new mean so we avoid having to compute
%any new inverses.
%pekf.m=prior.m-prior.c*x*(-gd2/(1-gd2*x'*prior.c*x))*x'*prior.m+pekf.c*lambda*mll;
%pekf.m=prior.m-prior.c*x*(-gd2/(1-gd2*x'*prior.c*x))*x'*prior.m+pekf.c*(lambda*prior.m+gd1*x);
%pekf.m=pekf.c*inv(prior.c)*prior.m+pekf.c*(gd1*x+prior.m*lambda);
pekf.m=prior.m-prior.c*x*(-gd2/(1-gd2*x'*prior.c*x))*x'*prior.m+pekf.c*(gd1*x+lambda*prior.m);

pekf.m=prior.m-prior.c*x*(-gd2/(1-gd2*x'*prior.c*x))*x'*prior.m+pekf.c*(pll.eta);

%************************************************************
%Debugging:
%**********************************************************
if (plotson>0)
    
%make a plot of the two gaussians
figure;
y=[0:.05:6];
%truep=normpdf(y,prior.m,prior.c^(.5)).*normpdf(y,pll.m,pll.c^(.5));
%use mvgauss if statistics toolbox not available
truep=mvgauss(y,prior.m,prior.c^(.5)).*mvgauss(y,pll.m,pll.c^(.5));
cnew=inv(inv(prior.c)+inv(pll.c));
%gaussp=normpdf(y,cnew*inv(prior.c)*prior.m+cnew*inv(pll.c)*pll.m,(cnew)^.5);
gaussp=mvgauss(y,cnew*inv(prior.c)*prior.m+cnew*inv(pll.c)*pll.m,(cnew)^.5);
hold on;
plot(y,log(truep));
plot(y,log(gaussp),'r-');
legend('True Posterior','Updated Posterior');



%make a plot of the likelihood as a function of theta
%theta is models under which to compute the likelihod
theta=[-1:.05:pll.m+2];
%compute the predicted rates for these models
[rmodels]=fu(theta,x);
[ll]= hpoissonll(obsrv.n, rmodels, obsrv.twindow);

%compute the 2nd order taylor expansion of the log likelihood
[rmu]=fu(prior.m,x);
[lmu]=hpoissonll(obsrv.n, rmu, obsrv.twindow);


figure;
hold on;
pind=0;
if (ltlike.on~=0)
pind=pind+1;
h_p(pind)=plot(theta,ll,getptype(pind,1));
plot(theta,ll,getptype(pind,2));
lgndtxt{pind}='Log Likelihood';
end


%check canonical representation of the likelihood but in terms of theta
%lct=lmu-(gd1*x)*prior.m-prior.m'*lambda*prior.m/2+(gd1*x+prior.m*lambda)*theta-.5*lambda*theta.^2
if (llike.on~=0)

lct=lmu-(gd1*x)*prior.m-prior.m'*lambda*prior.m/2+pll.eta*theta-.5*pll.lambda*theta.^2;


%Below I compute the likelihood just by using the canonical parameters
%and then normalizing it 
%when I did this it was not the same as ll 
%there was an offset
%this is becaust it has the appropriate normalization for a gaussian
n=size(prior.m,1);
lct1=pll.eta*theta-.5*pll.lambda*theta.^2
logz=-1/2*(n*log(2*pi)-log(det(pll.lambda)));
%normalizing constant
logz=logz-1/2*pll.eta'*inv(pll.lambda)*pll.eta;  %compute the normalization constant
lct=lct1+logz;


pind=pind+1;
h_p(pind)=plot(theta,lct,getptype(pind,1));
plot(theta,lct,getptype(pind,2));
lgndtxt{pind}='Likelihood (Gaussian Approx)';
end

%check that lambda and mll are correct by plotting the exponent
%of a gaussian
%for 1-d case covariance is just reciprocal of lambda
%lgauss=-1/2*(theta'-mll)'*lambda;
%lgauss=lgauss.*(theta'-mll)';
%lgauss=sum(lgauss,1);
%pind=pind+1;
%h_p(pind)=plot(theta,lgauss,getptype(pind,1));
%plot(theta,lgauss,getptype(pind,2));
%lgndtxt{pind}='Gaussian exponent';


%plot log likelihood of the prior

if (lprior.on~=0)
    
lp=-1/2*(theta'-prior.m)'*inv(prior.c);
lp=lp.*(theta'-prior.m)';
lp=sum(lp,1);
%normalizing constant
z=(2*pi)^(-size(prior.m,1)/2)*(det(prior.c))^(-1/2);
lp=lp+log(z);
pind=pind+1;
h_p(pind)=plot(theta,lp,getptype(pind,1));
plot(theta,lp,getptype(pind,2));
lgndtxt{pind}='log of prior ';

%make a dot at L(prior.m) as the curves should be equal at this point
pind=pind+1;
h_p(pind)=plot(prior.m,lmu,'k+');
set(h_p(pind),'MarkerSize',15)
lgndtxt{pind}='L(p_t_-_1(\mu_0))';
xlabel('\theta');
end

%plot log likelihood of the posterior
if (lpost.on~=0 & lprior.on~=0 & llike.on~=0)
    %plot log likelihood of the posterior
    %plot the true posterior that is Log(prior) + Log(Likelihood)
    %lpost=-1/2*(theta'-pekf.m)'*inv(pekf.c);
%lpost=lpost.*(theta'-pekf.m)';
    lpost=lp+lct;
    pind=pind+1;
    h_p(pind)=plot(theta,lpost,getptype(pind,1));
    plot(theta,lpost,getptype(pind,2));
    lgndtxt{pind}='Log of posterior: L(p_t(\theta))';

%make a dot at L(posterior.m) to show mean of posterior
%likelihood is just the normalization constant
pind=pind+1;
z=(2*pi)^(-size(pekf.m,1)/2)*(det(pekf.c))^(-1/2);
lmu=log(z);
h_p(pind)=plot(pekf.m,lmu,'rd');
set(h_p(pind),'MarkerSize',15)
lgndtxt{pind}='L(\mu_1)';

end

%plot log likelihood of the posterior using its parameters
if (lekfpost.on~=0 )
    lekf=-1/2*(theta'-pekf.m)'*inv(pekf.c);
    lekf=lekf.*(theta'-pekf.m)';
    lekf=sum(lekf,1);
    %normalizing constant
	z=(2*pi)^(-size(pekf.m,1)/2)*(det(pekf.c))^(-1/2);
	lekf=lekf+log(z);
    %plot log likelihood of the posterior
    %plot the true posterior that is Log(prior) + Log(Likelihood)
    %lpost=-1/2*(theta'-pekf.m)'*inv(pekf.c);
%lpost=lpost.*(theta'-pekf.m)';
    
    pind=pind+1;
    h_p(pind)=plot(theta,lekf,getptype(pind,1));
    plot(theta,lekf,getptype(pind,2));
    lgndtxt{pind}='Log of ekf posterior';


end

legend(h_p,lgndtxt);
ylabel('Log Likelihood');
xlim([1 5]);

end

%**************************************************************