%function param=defaultparam(param)
%   param - structure containing the parameters passed in by user
%
%Return value
%   param - structure in which all values not set by user are set to
%   default values.
function param=defaultparam(param)

%check for outdated fields
if isfield(param,'pcutoff')
    error('pcutoff has been renamed prior');
end

if isfield(param,'compeig')
    error('compeig should now be specified as a field of updater');
end


%*************************************************************
%initialize param to default parameters for any missing parameters
%**************************************************************
fields={'windexes','alength','freqsubsample','glm','stimnobsrvwind','obsrvwindowxfs'};
fields=[fields, {'stimtype','stimparam','micomp','tanparam','model','savecint','mparam','prior','updater','maxiter'}];

for fname=fields
    if (~isfield(param,fname{1}) || isempty(param.(fname{1})))
        switch fname{1}
            case 'stimparam'
                param.stimparam=[];
            case 'windexes'
                param.windexes=[2:14 16:30];
            case 'alength'
                param.alength=8;
            case 'freqsubsample';
                param.freqsubsample=2;
            case 'glm'
                param.glm=GLMPoisson('canon');
            case 'stimnobsrvwind'
                param.stimnobsrvwind=10;
            case 'obsrvwindowxfs'
                param.obsrvwindowxfs=250;
            case 'stimtype'
                param.stimtype='BSBatchPoissLB';
            case 'micomp'
                param.micomp='numerical';
            case 'tanparam'
                param.tanparam=[];

            case 'model'
                param.model='MParamObj';
            case 'mparam'
                param.mparam=[];
            case 'savecint'
                param.savecint=50;
            case 'prior'
                param.prior=[];
            case 'updater'
                param.updater=[];
            case 'maxiter'
                %maximum number of iterations for the updater
                param.maxiter=100;
            otherwise
                error('unrecognized field');
        end
    end

    %fields which are structures with more than one value
    %which require additional processing
    switch fname{1}
        case 'tanparam'
            if (~isfield(param.tanparam,'startfullmax') || isempty(param.tanparam.startfullmax))
                param.tanparam.startfullmax=25;

            end
            if (~isfield(param.tanparam,'rank') || isempty(param.tanparam.rank))
                param.tanparam.rank=2;
            end

    end
end


%check for unrecognized parameters
fnames=fieldnames(param);
for f=fnames
    switch lower(f{1})
        case fields
        otherwise
            error('unrecognized parameter %s',f{1});
    end
end
        
