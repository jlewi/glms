%function setupinfomax(param)
%   param - structure containing optional parameters
%
%       mparam - additional model specific parameters
%function setupinfomax(fname,val)
%   enter the options in field name value pairs
%08-06-2008
%
%
%Explanation: This script sets up an infomax simulation
%
%Revisions:
%   08-11-2008
%      make it  a function
function [allfiles,otbl,ofile,bssimobj]=setupinfomax(varargin)

switch nargin
    case 0
        param=struct();
    case 1
        param=varargin{1};
    otherwise
        %load the fields passed in into a structure
        for j=1:2:nargin
            param.(varargin{j})=varargin{j+1};
        end
end

if ~exist('param','var')
    param=struct();
end


setpathvars;
fprintf('\n\n');

%directory where we will store the data, relative to RESULTSDIR
odir=fullfile('bird_song',datestr(now,'yymmdd'));

%info to check is a string containing the information we want the user to
%verify
itocheck='';

param=BSSetup.defaultparam(param);


%**********************************************************************
%Parameters for the BSData object
%***********************************************************************


%lenght of the window in which we obsrve the responses
%in seconds
bparam.obsrvwindowxfs=param.obsrvwindowxfs;
bparam.rawdatafile=param.rawdatafile;
bparam.stimnobsrvwind=param.stimnobsrvwind;
bparam.freqsubsample=param.freqsubsample;


%keep track of files whos directories we want to verify exist
files=[];


%structure describing the stimulus
%stiminfo.type='BSBatchPCanonTaylor';

%stiminfo.type='BSBatchPoissLB';
%use numerical or approxiamte computation of the mutual information for
%canonical poisson
%stiminfo.micomp='approx';

%stiminfo.type='batchshuffle';
%stiminfo.type='infomax';

%how many terms to use in our taylor approximation of the objective
%function
stiminfo.nterms=3;





%***********************************************************************
%create the BSData object
%**********************************************************************
bdata=BSSetup.initbsdata(bparam);

%**************************************************************************
%Create the model object
%**************************************************************************
mobj=BSSetup.initmodel(param,bdata);





updater=BSSetup.initupdater(param);

%***********************************************************************
%create the names of the files to store the results
%**********************************************************************
[datafiles,fnum]=BSSetup.initfiles(odir,'bsinfomax');

%*************************************************************************
%Create the status file
%***********************************************************************
fid=fopen(getpath(datafiles.statusfile),'a');
fprintf(fid,'Status file. \n');
fprintf(fid,'stimnobsrvwind=%d\n',bparam.stimnobsrvwind);
fprintf(fid,'obsrvwindowxfs=%d\n',bparam.obsrvwindowxfs);
fprintf(fid,'freqsubsample=%d\n',bparam.freqsubsample);
fclose(fid);

%**************************************************************************
%Wave files
%************************************************************************
%we create a wavefile field of dsets with the path to each file
%we do this so that when we run on the cluster we copy the wave files to
%the cluster
wavefilesdir=getstimdir(bdata);


%************************************************************************
%create an array of all the files we need to copy to the cluster
%***********************************************************************
allfiles=datafiles;
allfiles.wavefilesdir=wavefilesdir;
allfiles.rawdatafile=bparam.rawdatafile;


%************************************************************************
%Create the GaussPostfile object using the prior and save it to gpostfile
%************************************************************************
allpost=BSSetup.initgausspostfile(param,allfiles,mobj);

%**************************************************************************
%Setup the stimulus object
%**************************************************************************
%initiliaze the stimulus object
stimobj=BSSetup.initstimobj(param,bdata);


%************************************************************************
%Create the Simulation Object
%************************************************************************
bssimobj=BSSim('allpost',allpost,'updater',updater,'mobj',mobj,'stimchooser',stimobj,'savecint',param.savecint);


bssimobj.extra.param=param;
%bssimobj.extra.cinitscale=cinitscale;
%bssimobj.extra.cfinalscale=cfinalscale;
save(getpath(allfiles.datafile),'bssimobj');


fields={'windexes','alength','freqsubsample','glm','stimnobsrvwind','obsrvwindowxfs'};
fields=[fields, {'stimtype','micomp','tanparam'}];

v=load(getpath(allfiles.datafile),'bssimobj');

bssimobj=v.bssimobj;
stimobj=v.bssimobj.stimobj;
bdata=v.bssimobj.stimobj.bdata;
mobj=v.bssimobj.mobj;

%************************************************************************
%Have user verify that parameters are correct
%***********************************************************************
itocheck=sprintf('%s ************birdsong data******** \n',itocheck);
itocheck=sprintf('%s neuron=%s\n',itocheck,getfilename(bdata.fname));
itocheck=sprintf('%s freqsubsample=%d\n',itocheck,bdata.stimparam.freqsubsample);
itocheck=sprintf('%s windexes=%s \n',itocheck, num2str(stimobj.windexes));
itocheck=sprintf('%s stimnobsrvwind=%d \n',itocheck, bdata.stimparam.nobsrvwind);
itocheck=sprintf('%s obsrvwindowxfs=%d \n',itocheck, bdata.obsrvparam.tnsamps);


itocheck=sprintf('%s ************Model      **********\n',itocheck);
itocheck=sprintf('%s alength=%d \n',itocheck,mobj.alength);
itocheck=sprintf('%s klength=%d \n',itocheck,mobj.klength);
itocheck=sprintf('%s ktength=%d \n',itocheck,mobj.ktlength);


itocheck=sprintf('%s ************prior      **********\n',itocheck);
%print out the variance for each component
priorvar=diag(getc(bssimobj.allpost,0));
if (length(unique(priorvar(mobj.indstim(1):mobj.indstim(2))))==1)
    stimvar=priorvar(mobj.indstim(1));
else
    stimvar=priorvar(mobj.indstim(1):mobj.indstim(2));
end

if (length(unique(priorvar(mobj.indshist(1):mobj.indshist(2))))==1)
    shistvar=priorvar(mobj.indshist(1));
else
    shistvar=priorvar(mobj.indshist(1):mobj.indshist(2));
end

biasvar=priorvar(mobj.indbias);
itocheck=sprintf('%s variance of stim. coeff. \t=%s \n',itocheck,num2str(stimvar));
itocheck=sprintf('%s variance of shist. coeff. \t=%s \n',itocheck,num2str(shistvar));
itocheck=sprintf('%s variance of bias coeff. \t=%s \n',itocheck,num2str(biasvar));




itocheck=sprintf('%s ************Updater      **********\n',itocheck);
itocheck=sprintf('%s class=%s \n',itocheck,class(bssimobj.updater));
itocheck=sprintf('%s compeig=%d \n',itocheck,bssimobj.updater.compeig);


itocheck=sprintf('%s ************Stimulus Chooser      **********\n',itocheck);
itocheck=sprintf('%s Stimulus chooser = %s \n',itocheck, class(v.bssimobj.stimobj));
itocheck=sprintf('%s nrepeats = %d \n',itocheck, v.bssimobj.stimobj.nrepeats);

%chech any variables specific to this stim chooser
switch class(v.bssimobj.stimobj)
    case 'BSBatchPoissLBTan'
        itocheck=sprintf('%s Rank of tangent space = %d \n',itocheck, mobj.rank);
        itocheck=sprintf('%s For tangent space startfullmax = %d \n',itocheck, v.bssimobj.stimobj.bstan.startfullmax);
    otherwise
end

%************************************************************************
%Check that the parameters are correct
%***********************************************************************
for fname=fields
    switch fname{1}
        case 'windexes'
            if (length(param.windexes)~=length(stimobj.windexes) || any(param.windexes~=stimobj.windexes))
                itocheck=sprintf('%s ERROR: windexes does not appear to have been set correctly \n',itocheck);
            end
        case 'alength'
            if (param.alength~=mobj.alength)
                itocheck=sprintf('%s ERROR: alength not set correctly \n',itocheck) ;
            end
        case 'freqsubsample';
            if (bdata.stimparam.freqsubsample~=param.freqsubsample);
                itocheck=sprintf('%s ERROR: freqsubsample not set correctly \n',itocheck) ;
            end
        case 'glm'
            if ~isequal(param.glm,mobj.glm)
                itocheck=sprintf('%s ERROR: glm not set correctly \n',itocheck) ;
            end
        case 'stimnobsrvwind'
            if (param.stimnobsrvwind~=bdata.stimparam.nobsrvwind)
                itocheck=sprintf('%s ERROR: bdata nobsrvwind not set correctly \n',itocheck) ;
            end
        case 'obsrvwindowxfs'
            if (param.obsrvwindowxfs~=bdata.obsrvparam.tnsamps)
                itocheck=sprintf('%s ERROR: bdata obsrvwindowxfs not set correctly \n',itocheck) ;
            end
        case 'stimtype'
            if ~isa(stimobj,param.stimtype)

                itocheck=sprintf('%s ERROR: Stimulus chooser type is not set correctly \n',itocheck) ;

            end

        case 'rawdatafile'
            %file containing the actual bird song data
            if ~strcmp(getpath(param.rawdatafile),getpath(bdata.fname))

                itocheck=sprintf('%s ERROR: bdata rawdatafile not set correctly \n',itocheck) ;

            end
        case 'micomp'
                            props=properties(stimobj);
                            
            if ~isempty(strmatch('miobj',props,'exact'))
            switch lower(param.micomp)

                case 'numerical'
                    if ~isa(stimobj.miobj,'CompMINum')
                        itocheck=sprintf('%s ERROR: micomp not set correctly \n',itocheck) ;

                    end
                otherwise
                    itocheck=sprintf('%s Warning: did not check whether miobj was set correctly \n',itocheck) ;

            end
            end
        case 'tanparam'
            if (isa(mobj,'MTLowRankInt') || isa (mobj,'MTLowRank'))
                if (mobj.rank~=param.tanparam.rank)
                    itocheck=sprintf('%s ERROR: rank of model object not set correctly \n',itocheck) ;

                end

            end
        otherwise
            error('unrecognized field');
    end
end



%print out the object governing how we compute the mutual info
%IS THERE A WAY WE CAN TEST FOR PROPERTY MIOBJ directly?
switch class(v.bssimobj.stimobj)
    case {'BSBatchShuffle', 'BSStimReplay'}
    otherwise
        itocheck=sprintf('%s Class computing mutual info: %s \n', itocheck, class(v.bssimobj.stimobj.miobj));
end
fprintf('%s\n',itocheck);

% r=input('Is this correct(y/n)?','s');
% switch lower(r)
%     case {'y','yes'}
%         %continue
%     otherwise
%         fprintf('Setup aborted on user request. Files will be deleted. \n');
% 
%         ftodelete=[allfiles.datafile allfiles.mfile allfiles.cfile allfiles.statusfile];
% 
%         for ftod=ftodelete
%             delete(getpath(ftod));
%         end
%         return;
% end

%*************************************************************
%add to a our setup file the number of windexes so that
% we know how many nodes to use
%********************************************************
%allfiles.nwindexes=length(windexes);

%information
info.scriptname=getpath(allfiles.setupfile);
info.neuron=getfilename(bdata.fname);
info.stimnobsrvwind=bparam.stimnobsrvwind;
info.freqsubsample=bparam.freqsubsample;
info.strfdim=getstrfdim(bdata);
info.obsrvwindowxfs=bparam.obsrvwindowxfs;
info.klength=getklength(mobj);
info.ktlength=getktlength(mobj);
info.alength=param.alength;



%save the number of windexes so that when we kick off the job we no how
%many workers to use
allfiles.nwindexes=length(bssimobj.stimobj.windexes);
%output the information to an mfile
writeminit(allfiles.setupfile,allfiles,info);

%**********************************************************
%create an xml file to describe this experiment
%***********************************************************
scriptname=mfilename();
%make the first text in the table the label so that the title of the page
%will default to ti

otbl={bssimobj.label; itocheck};
otbl=[{'setup script', 'bssetupinfomax';'setupfile',getpath(allfiles.setupfile); 'datafile',getpath(allfiles.datafile);}];
otbl=[otbl;{'BSSimObj', bssimobj}];
otbl=[{bssimobj.label; itocheck};{otbl};{bssimobj}];



ofile=seqfname(FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,'bsinfomax.xml')),[],fnum);
onenotetable(otbl,getpath(ofile));


