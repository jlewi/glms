%function allpost=initgausspostfile(param,allfiles,mobj)
%   allfiles
%       .mfile
%       .cfile
function allpost=initgausspostfile(param,allfiles,mobj)
    switch lower(param.model)
    case {'mbsftsep','mbsftseplowrank','modbssinewaves'}

    prior=BSSetup.priorfourier(mobj,param.prior);
    otherwise

    prior=BSSetup.priornormal(mobj,param.prior);
end


allpost=GaussPostFile('mfile',allfiles.mfile,'cfile',allfiles.cfile,'m',getm(prior),'c',getc(prior),'id',0);
