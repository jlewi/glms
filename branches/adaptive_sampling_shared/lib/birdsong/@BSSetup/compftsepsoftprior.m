%function compfouriersoftprior(bdata,cinitrange,pcutoff)
%   bdata - bdata object
%   cinitrange - range for the covariance
%   pcutoff - optional, file to load songstats from
%Explanation:
%   computes the mean and prior covariance to use as the prior
%   Uses the stimulus statistics to do this.
%   Saves the prior to a file so it can be loaded later
%
%   This just computes the mean and prior of the stimulus coefficients
%
%   we then save it to a file
%
%   This function computes a prior for an MBSFT model which applies the FFT
%   to time and frequency separatly
%
function fname=compftsepsoftprior(bdata,cinitrange,pcutoff)

if ~exist('cinitrange','var')
   cinitrange=[]; 
end
if isempty(cinitrange)  
cinitrange=[10^-7 10^1]; 
end

if ~exist('pcutoff','var')
   pcutoff=[]; 
end
%create an mobj
glm=GLMPoisson('canon');
strfdim=getstrfdim(bdata);
alength=0;
maxf=MBSFTSep.maxn(strfdim);
mobj=MBSFTSep('nfreq',maxf(1),'ntime',maxf(2),'glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',0);


if ~isempty(pcutoff)
    v=load(getpath(pcutoff));
   
    songstats=v.songstats;
    
    if (size(songstats.mean,1)~=getparamlen(mobj))
       error('The stimulus statistics loaded from the file do not have the same number of terms as bdata.'); 
    end
else
    
windexes=find(waveissong(bdata,1:getnwavefiles(bdata)));

%comp the stimulus statistics for this neuron
songstats=BSStimStatsFTSep.comp(bdata,windexes);
end


dim=getparamlen(mobj);
minit=zeros(dim,1);

%map the log of the amplitude  of the mean to a linear range between
%cinit(1) and cinit(2)
lmean=log10(abs(songstats.mean));
lmean=lmean-min(lmean);
lmean=lmean/(max(lmean));

%cinit is the diagonal entries for our covariance matrix
cstiminit=lmean*(log10(cinitrange(2))-log10(cinitrange(1)))+log10(cinitrange(1));
cstiminit=10.^cstiminit;


fname=FilePath('RESULTSDIR',fullfile('bird_song',datestr(now,'yymmdd'),sprintf('bscinit_nfreq%g_ntime%g.mat',mobj.nfreq,mobj.ntime)));
fname=seqfname(fname);
[fdir]=fileparts(getpath(fname));
recmkdir(fdir);
save(getpath(fname),'cstiminit','cinitrange','songstats');
