%function stimobj=initstimobj(param,bdata)
%   param - structure array of parameters
%       windexes - which wave files to use 
%       tanparam  - paramaters for the tangent space when need
function stimobj=initstimobj(param,bdata)

%structure which could contain additional stimulus parameters
stimparam=param.stimparam;

stimparam.windexes=param.windexes;
stimparam.bdata=bdata;

%************************************************************************
%Create the stimulus chooser object
%**************************************************************************
switch param.stimtype
    case {'shuffled','BSUniformStim'}
        stimobj=BSUniformStim('bdata',bdata,'windexes',windexes);
    case {'infomax','BSOptStimNoHeur'}
        data.poolfile=[];

        stimparam.miobj=PoissExpCompMI();
        stimparam.poolfile=data.poolfile;
        stimobj=BSOptStimNoHeur(stimparam);

    case {'sequential','BSSeqStim'}

        stimobj=BSSeqStim(stimparam);
    case {'BSBatchPoissLB'}
        stimparam.bsize=param.stimnobsrvwind;
        stimparam.micomp=param.micomp;
        
        stimobj=BSBatchPoissLB(stimparam);
    case {'batchshuffle','BSBatchShuffle'}
         stimparam.bsize=param.stimnobsrvwind;
        stimobj=BSBatchShuffle(stimparam);
    case {'BSBatchPoissLBTan'}
        
        stimparam.bsize=param.stimnobsrvwind;
        stimparam.micomp=param.micomp;
        stimparam.startfullmax=param.tanparam.startfullmax;
        
        stimobj=BSBatchPoissLBTan(stimparam);
    case {'BSBatchPCanonTaylor'}
        
        stimobj=BSBatchPCanonTaylor('bdata',bdata,'windexes',windexes,'bsize',stiminfo.bsize,'nterms',stiminfo.nterms);
    case {'BSStimReplay'}
        stimobj=BSStimReplay('simfile',stimparam.simfile,'bdata',bdata);
    otherwise
       cmd=sprintf('stimobj=%s(stimparam);',param.stimtype);
       eval(cmd);
        %error('stiminfo.type is not  a valid stimulus object.');
end