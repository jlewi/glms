%function prior=priorfourier(mobj,cfile)
%   cfile - a file containing the covariance matrix for the stimulus
%         - coefficients computed using compfouriersoftprior.m
%
%function prior=priorfourier(mobj,param)
%   mobj - ModBSSineWaves object
%   param
%        stimvar - the variance for the stimulus coefficients
%        shistvar - the variance for the spike history coefficients
%        biasvar  - the variance for the bias terms
%
%Explanation:
%   this function assumes the file stores the following variables
%       cinitrange - the range of variances for the elemens
%       cinit - inital variance for the stimulus coefficients
%Revisions:
%  11-06-2008 - Revised this function so you specify the variance for the
%  stimulus coefficients, the bias, and the spike history coefficients
%  10-26-2008 - The output of function compfouriersoftprior is no longer compatible with
%               this function
%
function prior=priorfourier(mobj,param)
debug=true;

if ~exist('param','var')
    param=[];
end

if isa(mobj,'ModBSSinewaves')
    error('compfouriersoftprior needs to be modified for this function to work.');
end
%we add 1 for the bias term
dim=getparamlen(mobj);
minit=zeros(dim,1);

if isa(param,'FilePath')
    v=load(getpath(param));


    %variances for each element
    cinit=zeros(getparamlen(mobj),1);

    cstiminit=v.cstiminit;

    if (length(cstiminit)~=mobj.maxbvecs)
        error('cinit in the file doesn''t have enough terms for this model');
    end


    cinitrange=v.cinitrange;

    %set the stimulus coeffcieents
    cinit(mobj.indstim(1):mobj.indstim(2))=cstiminit(mobj.btouse);

    if (mobj.alength>0)
        cinit(mobj.indshist(1):mobj.indshist(2))=cinitrange(2);
    end
    if (mobj.hasbias)
        cinit(mobj.indbias)=cinitrange(2);
    end

elseif isempty(param)
    error('You need to specify stimvar, shistvar, and biasvar');
else
    cinit=zeros(getparamlen(mobj),1);
    cinit(mobj.indstim(1):mobj.indstim(2))=param.stimvar;

    if (mobj.alength>0)
        cinit(mobj.indshist(1):mobj.indshist(2))=param.shistvar;
    end
    if (mobj.hasbias)
        cinit(mobj.indbias)=param.biasvar;
    end

end


cinit=diag(cinit);


if (debug)
    if (any(diag(cinit)<0))
        error('elements of cinit are less than zero');

    end

end
prior=GaussPost('m',minit,'c',cinit);
