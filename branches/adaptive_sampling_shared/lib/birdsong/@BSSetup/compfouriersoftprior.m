%function compfouriersoftprior(bdata)
%   bdata - bdata object
%
%Explanation:
%   computes the mean and prior covariance to use as the prior
%   Uses the stimulus statistics to do this.
%   Saves the prior to a file so it can be loaded later
%
%   This just computes the mean and prior of the stimulus coefficients
%
%   we then save it to a file
%
function fname=compfouriersoftprior(bdata)

%create an mobj
glm=GLMPoisson('canon');
strfdim=getstrfdim(bdata);
alength=0;
maxf=ModBSSinewaves.maxn(strfdim);
mobj=ModBSSinewaves('nfreq',maxf(1),'ntime',maxf(2),'glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',0);

windexes=find(waveissong(bdata,1:getnwavefiles(bdata)));



%comp the stimulus statistics for this neuron
songstats=BSStimStats.comp(bdata,windexes);

dim=getparamlen(mobj);
minit=zeros(dim,1);
cinitscale=10;

%start by initializing the covariance matrix to a matrix with cinitscale
%along the diagonal
cinit=cinitscale*eye(dim);


%we set the variance for the stimulus coefficients based on the mean amplitude
%of the different frequencies in the stimulus statistics
%This should just be equivalent to just computing the ratio without
%normalizing by the energy first.
se=songstats.amp.mean/sum(songstats.amp.mean(:).^2);
stimvar=10.^(se/(max(se(:)))*7-6);


%now we need to set the variance for each element
for nf=0:mobj.nfreq
    for nt=0:mobj.ntime
        
        lindex=bindex(mobj,nf,nt);
        
        if ~isnan(lindex(1))
            cinit(lindex(1),lindex(1))=stimvar(nf+1,nt+1);
        end
        cinit(lindex(2),lindex(2))=stimvar(nf+1,nt+1);

    end
end
  
debug=true;

if (debug)
   %make sure matrix is diagonal
   %subtract out the diagonal
   if (any(any(triu(cinit,1)))~=0)
       error('cinit is not diagonal. This indicates our cutoff is imposed incorrectly');
   end


   %check to make sure the variance is set correctly
   %for all elements
    for index=1:mobj.nstimcoeff
        [ind]=bindexinv(mobj,index);
        
        if (cinit(index,index)~=stimvar(ind.nf+1,ind.nt+1))
            error('The variance isn''t set correctly \n');
        end
    end
end
cstimcoeff=cinit;

fname=FilePath('RESULTSDIR',fullfile('bird_song',datestr(now,'yymmdd'),sprintf('bscinit_nfreq%g_ntime%g.mat',mobj.nfreq,mobj.ntime)));
fname=seqfname(fname);
[fdir]=fileparts(getpath(fname));
recmkdir(fdir);
save(getpath(fname),'cstiminit','cinitscale','songstats');
