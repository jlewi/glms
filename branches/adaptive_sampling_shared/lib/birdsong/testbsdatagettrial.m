%04-06-2008
%
%A test script for verifying that BSData correctly chunks the data
%Currently this just tests that chunkdata and get trial return the same
%data.


%*******************************************************************
%Create a bsdata object
%*********************************************************************

dfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1_corrected.mat'));



%how many columns should be in the STRF 
stimnobsrvwind=10;
%subsample the frequencies of the spectrogram to reduce the dimensionality
%of the stimulus
freqsubsample=4;
obsrvwindowxfs=250;
bdata=BSData('fname',dfile,'stimnobsrvwind',stimnobsrvwind,'freqsubsample',freqsubsample,'obsrvwindowxfs',obsrvwindowxfs);


%*************************************************************************
%Check all trials returned by gettrialswithspikes actually have spikes
%*************************************************************************
twithspikes=gettrialswithspikes(bdata);

for tind=1:length(twithspikes)
   [bdata stim shist obsrv]=gettrial(bdata,twithspikes(tind));
   if (obsrv==0)
       error('gettrialswithspikes says there is a spike on this trial but no spike is returned by gettrial');
   end
end
%**************************************************************************
%Check the first and last trials
%*************************************************************************
fprintf('Checking first trial \n');
[bdata]=checktrial(bdata,1);
fprintf('First trial is good.\n');

fprintf('Checking last trial \n');
[bdata,ntrials]=getntrials(bdata);
[bdata]=checktrial(bdata,ntrials);
fprintf('Last trial is good.\n');


%************************************************************************
%check inputs corresponding to the transition between the silence preceding
%%the actual stimulus and the actual stimulus
%*************************************************************************
%%
%npre and npost are the number of frames in the silence preceding and
%following the stimulus
%get the length of the silences before and after the stimulus;
 [bdata,npre,npost]=nframesinsilence(bdata); 
 
[strfdim]=getstrfdim(bdata);
nincrements=strfdim(2);
fprintf('Checking the inputs around the transition from the pre stimulus silence to the post stimulus silence. \n');

ntocheck=100;
nspikes=0;
for trial=(npre-nincrements+1-ntocheck/2):(npre-nincrements+1+ntocheck/2)
    
   [bdata]=checktrial(bdata,trial);
end

fprintf('Trials were correct. \n %d of the trials had spikes. \n',nspikes);

%************************************************************************
%check inputs corresponding to the transition between the actual
% sound file and the silence following the stimulus 
%*************************************************************************
%%

%npre and npost are the number of frames in the silence preceding and
%following the stimulus
%get the length of the silences before and after the stimulus;
 [bdata,npre,npost]=nframesinsilence(bdata);
 
%which presentation of a stimulus to do this for
sindex=1;
windex=getwindexonwavetrial(bdata,sindex);

[bdata,finfo]=getstiminfo(bdata,windex);    


fprintf('Checking the inputs around the transition from the stimulus to the post stimulus silence. \n');

ntocheck=100;
nspikes=0;
tstart=finfo.laststart-nincrements+1-ntocheck/2;
for trial=tstart:tstart+ntocheck/2
    
   [bdata]=checktrial(bdata,trial);
end

fprintf('Trials were correct. \n %d of the trials had spikes. \n',nspikes);

%************************************************************************
%Randomly check that some number of trials is accurate
%*************************************************************************
%%
ntocheck=1000;

%keep track of how many of these random trials involved spikes
nspikes=0;
fprintf('Selecting %d trials randomly to verify the inputs and observations. \n', ntocheck)
for j=1:ntocheck
    if (mod(j,100)==0)
        fprintf('# tested= %d \n',j);
    end
   trial=ceil(rand(1,1)*ntrials); 
   [bdata]=checktrial(bdata,trial);
   [bdata,stim,shist,obsrv]=gettrial(bdata,trial);
   if (obsrv==1)
      nspikes=nspikes+1; 
   else
       %make sure there is no spike on this trial
       bindex=binarysubdivide(twithspikes,trial);
       if (bindex>0 && twithspikes(bindex)==trial)
           error('Get trial did not return a spike on trial %d but gettrialswithspikes did.',trial);
       end
   end
end
fprintf('Trials were correct. \n %d of the trials had spikes. \n',nspikes);

    






%%
return
%Compare the functions chunkdata and gettrial
%make sure they return the same stimuli

fprintf('Testing to make sure chunk data and getrial return the same data. \n');

%chunk the data into trials
numtrials=1000;


clear tdata;
clear cdata;

for j=1:numtrials
    if (mod(j,100)==0)
        fprintf('Gettrial trial=%d \n',j);
    end
   [bdata,tdata(j)]=gettrial(bdata,j); 
end

[bdata,cdata]=chunkdata(bdata,numtrials);


%check the match
for j=1:numtrials
   if (getData(getinput(tdata(j)))~=getData(getinput(cdata(j))))
       error('Stimulus does not match on trial %d \n',j);
   end
      if (getobsrv(tdata(j))~=getobsrv(cdata(j)))
       error('Observation does not match on trial %d \n',j);
   end
end

fprintf('Success: chunkdata and gettrial match.');

