%test the bsbatchfit object
%
%
%
function testbsbatchfit
%lenght of spike history
alength=5;

%what tests to run
tests.compglmproj=false;
tests.d2glm=true;
%*******************************************************************
%Create a bsdata object
%*********************************************************************

dfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1.mat'));



%how long to make the stimulus in time on each trial
%good idea to make this at least 2 time points wide
stimdur=.010;
%subsample the frequencies of the spectrogram to reduce the dimensionality
%of the stimulus
freqsubsample=64;
obsrvwindow=.005;
bdata=BSData('fname',dfile,'stimdur',stimdur,'freqsubsample',freqsubsample,'obsrvwindow',obsrvwindow);

uobj=BSBatchFitGLM('bdata',bdata);

%********************************************************************
%Create a logistic model no spike history
%********************************************************************
stimdim=getstrfdim(bdata);
klength=stimdim(1);
ktlength=stimdim(2);
mobj=MLogistic('alength',alength,'klength',klength,'mmag',1,'ktlength',ktlength);
%**************************************************************************
%test: compglmproj
if (tests.compglmproj)
    iscorrect=testcompglmproj(uobj,bdata,mobj);


    if (iscorrect)
        fprintf('BSBatchFitGLM/compglmproj successfull \n');
    else
        error('compglmproj not correct');
    end

end

if (tests.d2glm)
    iscorrect=testcompd2glm(uobj,bdata,mobj);


    if (iscorrect)
        fprintf('BSBatchFitGLM/compgd2glmp was successfull \n');
    else
        error('compd2loglike not correct');
    end

end
%*********************************************************************
%test computation of the derivatives
%**********************************************************************
function iscorrect=testcompd2glm(uobj,bdata,mobj)

iscorrect=true;


%index of the waves file to compute derivatives for
windexes=ceil(rand(1)*getnwavefiles(bdata));

theta=floor(randn(getparamlen(mobj),1)*10);

%compute glmproj for all presentations of this stimulus

[batchfit.dglm, batchfit.d2glm, bfitdebug]=compd2loglike(uobj,mobj,theta,windexes);


strfdim=getstrfdim(bdata);
%now manually check we get the same result if we use gettrial
%get the trials corresponding to this windex

direct.dglm=zeros(getparamlen(mobj),1);
direct.d2glm=zeros(getparamlen(mobj),getparamlen(mobj));

shist=[];


for w=1:length(windexes)
    
    windex=windexes(w);
    [bdata,trials]=gettrialindforwave(bdata,windex);
    
    
    direct.alldglmeps{w}=zeros(size(trials,1),trials(1,2)-trials(1,1)+1);
    direct.alld2glmproj{w}=zeros(size(trials,1),trials(1,2)-trials(1,1)+1);;
    for r=1:size(trials,1)
    %warning('need to set r=1:size(trials,1)');
    %for r=1
        for trial=trials(r,1):trials(r,2)
            if (mod(trial-trials(r,1),1000)==0)
                fprintf('Trial repeat %d of %d: %0.4g of %0.4g \n',r,size(trials,1),trial-trials(r,1),trials(r,2)-trials(r,1));
            end



                [bdata,sr,shist]=gettrial(bdata,trial,getshistlen(mobj));

                glmproj=compglmproj(mobj,getinput(sr),theta,shist);

                [dglmeps, d2glmproj]=d2glmeps(getglm(mobj),glmproj,getobsrv(sr));
                
                direct.allglmproj{w}(r,trial-trials(r,1)+1)=glmproj;                
                direct.alldglmeps{w}(r,trial-trials(r,1)+1)=dglmeps;
                direct.alld2glmproj{w}(r,trial-trials(r,1)+1)=d2glmproj;
                
                inpvec=projinp(mobj,getdata(getinput(sr)),shist);
                direct.dglm=direct.dglm+dglmeps*inpvec;

                direct.d2glm=direct.d2glm+d2glmproj*inpvec*inpvec';
        end
    end
end
%check the stimulus and observations match
%NOte there will be some numerical error because the order we  perform the
%operations matters
threshold=10^-8;
if (any(abs(direct.dglm-batchfit.dglm)>10^-8))
    fprintf('Error: 1st derivative computed directly and using BatchFitGLM did not return the same value.\n');
    %check which terms it is
    if any(abs(direct.dglm(1:getklength(mobj))-batchfit.dglm(1:getklength(mobj)))>threshold)
        fprintf('ERROR: 1st derivative: terms corresponding to stimulus are not correct \n');
    else
        fprintf('1st derivative: terms corresponding to stimulus are accurate\n');
    end
    
    dshist=direct.dglm(getklength(mobj)+1:getklength(mobj)+getshistlen(mobj))-batchfit.dglm(getklength(mobj)+1:getklength(mobj)+getshistlen(mobj));
    dshist=abs(dshist);
    if (any(dshist>threshold))
        fprintf('ERROR: 1st derivative: terms corresponding to spike history are not correct \n');
    else
        fprintf('1st derivative: terms corresponding to spike history are accurate\n');
    end
    if any(abs(direct.dglm(end)-batchfit.dglm(end))>threshold)
        fprintf('ERROR: 1st derivative: terms corresponding to bias not correct \n');
    else
        fprintf('1st derivative: terms corresponding to bias are accurate\n');
    end
    
    %***********************************************************
    %check the actual variables 
    for wind=1:length(windexes)
        d=abs(direct.allglmproj{wind}-bfitdebug.glmproj{wind});
       if any(d(:)>threshold)
           fprintf('Error: glmproj computation is not the same for both methods \n');
       end
    end
    iscorrect=false;
    error('Error: 1st derivative not correct');
end

%check second derivative
dd2=abs(direct.d2glm-batchfit.d2glm);

if (any(dd2(:)>threshold))
        fprintf('Error: 2nd derivative computed directly and using BatchFitGLM did not return the same value.');
    for wind=1:length(windexes)
    d2ndderv=abs(bfitdebug.d2glmproj{wind}-direct.alld2glmproj{wind});
        
    if (any(d2ndderv(:)>threshold))
       fprintf('Error: d2glmproj for both methods does not match. \n'); 
    else
        fprintf('d2glmproj for both methods does not match. \n'); 
    end
    end
            iscorrect=false;
    error('Error: 2nd derivative not correct');
end

%*********************************************************************
%test computation of glmproj
%**********************************************************************
function iscorrect=testcompglmproj(uobj,bdata,mobj)

iscorrect=true;

%index of the wave file to test
windex=ceil(rand(1)*getnwavefiles(bdata));

theta=floor(randn(getparamlen(mobj),1)*10);

%compute glmproj for all presentations of this stimulus
batchfit.glmproj=compglmproj(uobj,windex,mobj,theta);


strfdim=getstrfdim(bdata);
%now manually check we get the same result if we use gettrial
%get the trials corresponding to this windex
[bdata,trials]=gettrialindforwave(bdata,windex);

direct.glmproj=zeros(size(trials,1),trials(1,2)-trials(1,1)+1);

%we need to get allobsrv so we can properly compute the spike history for
%the first elements
[bdata,fullspec,allobsrv]=getallwaverepeats(bdata,windex);
for r=1:size(trials,1)
    fprintf('Testing %0.3g presentation of the %g wave file \n', r,windex);


    shist=[];

    %if the length of the spike history is > strfdim(2)-1 then
    %the spike history isn't fully known on the first trial so we pad
    %with zeros
    %     if (getshistlen(mobj)>(strfdim(2)-1))
    %         shist(end-(strfdim(:,2)-1)+1:end)=allobsrv(r,1:strfdim(2)-1);
    %     else
    %         %length of spike history is less than or equal to strfdim(2)-1
    %         shist=allobsrv(r,(strfdim(2)-1)-getshistlen(mobj)+1);
    %     end

    tr=1;
    for trial=trials(r,1):trials(r,2)
        [bdata,sr,shist]=gettrial(bdata,trial,getshistlen(mobj));

        direct.glmproj(r,tr)=compglmproj(mobj,getinput(sr),theta,shist);
        tr=tr+1;
        %update shist
        shist=[shist(2:end);getobsrv(sr)];
    end



end

%check the stimulus and observations match
d=abs(direct.glmproj-batchfit.glmproj);

if (any(d(:)>10^-8))
    fprintf('Error: glmproj computed directly and using BatchFitGLM compglmproj did not return the same value.');
    iscorrect=false;
    error('Error: compglmproj not correct');
end