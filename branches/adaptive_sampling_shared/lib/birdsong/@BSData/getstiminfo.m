%function [obj,stiminfo]=stiminfo(obj,index)
%	 obj=BSData object
%    index = index of the stimulus
%Return value:
%	 info=Returns obj.stimspec(index). The reason we use an accessor method
%       is so thought we can check whether stimulus is loaded.
%       .duration - duration in sec
%    obj - we return the object as well because if we compute the
%    spectrogram of the stimuli we want to save it so we don't have to
%    recompute it..
function [obj,stiminfo]=getstiminfo(obj,index)

%if we haven't computed the sound spectrum yet
%then do so because that function computes many of these parameters
%     if isempty(obj.stimspec(index).spec)
%         [obj]=getstimspec(obj,index);
%     end


if (isempty(obj.stimspec(index).stimsize) || isempty(obj.stimspec(index).fs))
    %****************************************
    % Load the stimulus and calculate necessary parameters
    %****************************************

    filename1 = fullfile(getpath(obj.stimdir),obj.stimspec(index).fname);
    
    %check if extension is missing
    if ~strcmp(filename1(end-3:end),'.wav')
        fwext=[filename1 '.wav'];
    else
        fwext=filename1;
    end
    if ~exist(fwext,'file')
      error('File does not exist: %s',fwext);
    end

    [stimulus fs nbits] = wavread(filename1);

    obj.stimspec(index).stimsize = length(stimulus);
    %save the sampling rate in hz
    obj.stimspec(index).fs=fs;
end

stiminfo=obj.stimspec(index);

%compute the duration of the stimulus as well in sec
stiminfo.duration=obj.stimspec(index).stimsize/obj.stimspec(index).fs;