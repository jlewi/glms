%function [obj,info]=compchunkinfo(obj,sindex)
%   obj - BSData object
%   sindex - stimulus for which we compute chunk info
%Return value:
%   obj  - the object we return the object so we can save internal values
%          that we compute: i.e info about the stimulus
%   info - structure containing info/parameters about how we divide the
%          the stimulus into frames.
%        - a frame is a time window in which we compute the fft
%        - The stimulus on any trial is made up of multiple frames
%       .framecount - number of frames we divide stimulus into
%       .increment  - number of samples between successive frames
%       .binsize - duration in samples of each frame
%                  This is the number of time samples used to compute
%                   the FFT
%       .bintime - duration of each frame in seconds
%
%05-29-2008
%       Since we specify the length of the observation window
%       as the number of samples we can use this to compute the other
%       quantities and therby avoid roundoff error.
function [obj,info]=frameinfo(obj,sindex)

    %****************************************
    % Define some constants
    % values taken from David Schneider's display_stim code
    %****************************************
    
    %in David's code
    fwidth = 125; %This is input by the user in strfpak
    

    
    DBNOISE = 80; %Not sure where this # came from
    initialFreq = 250; %define frequency range that i care about
    endFreq = 8000;
%    psth_smooth = 20; %This is input by the user in strfpak
 %   ftype = 2; %Filter for processing PSTH
%    smooth_rt = 41; %Used in autocorrelation, user input in strfpak
%    window = 200; %in msec
%    timeLag = round(0.5*window); %msec, used in autocorrelation, input in strfpak
%    pre = round(0.5*window); %amount of stim collected before each spike
%    post = round(window-pre); %amount of stim collected after each spike
 %   nt = 2*round(timeLag) +1; %size of strf in msec
    
    %we need the sampling rate of the stimulus
    %if we've already computed the spectrogram then the sampling rate was
    %stored in the object. If not we have to read the wave file.
    if ~isempty(obj.stimspec(sindex).fs)
        fs=obj.stimspec(sindex).fs;
        stimsize=obj.stimspec(sindex).stimsize;
    else
         filename1 = fullfile(getpath(obj.stimdir),obj.stimspec(sindex).fname);
        [stimulus fs nbits] = wavread(filename1);
        %save the sampling rate.
        obj.stimspec(sindex).fs=fs;
        obj.stimspec(sindex).stimsize=length(stimulus);
        stimsize=length(stimulus);
    end

    %why is the binsize set to 6/(2pi)* fs/(pi*fwidth)
    %fs/fwidth - This is the number of samples we need to take of signal
    %          sampled at rate fs, to ensure the frequencies in our fft
    %          are separated by fwidth hz.
    %6/(2pi) ~ 1 - so this is most likely some type of numerical
    %              correction.
    %
    info.binsize = floor(1/(2*pi*fwidth)*6*fs); %size of time bin over which fft is computed (in samples)
    info.bintime=info.binsize*1/fs; %time duration in seconds

    
    %we need bintime and corressponding binsize to be a multiple of 
    %obj.obsrvparam.tlength so round bintime up to the next multiple
    %once we fix binsize we reset bintime
    info.binsize=ceil(info.bintime/obj.obsrvparam.tlength)*obj.obsrvparam.tnsamps;
    info.bintime=info.binsize*1/fs;
    
    info.nFTfreqs = info.binsize; %# of frequencies at which fft is computed
    %
    %The following code sets the increment to always be the number
    %of samples corresponding to the size of the bin in which we bin the
    %response. If this is not an integer multiple through an error because
    %I think we will have alighnment problems
    %ampsamprate = 1000;
   % info.increment = fs*obj.obsrvparam.tlength; %# samples to shift for each new bin start point
   info.increment=obj.obsrvparam.tnsamps;
    if (mod(info.increment,1)~=0)
        error('increment is not an integer');
    end
    
    info.frameCount = floor((stimsize-info.binsize)/info.increment)+1; %# of bins
    info.DBNOISE=80;
    info.initialFreq=initialFreq;
    info.endFreq=endFreq;