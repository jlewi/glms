%function [varargout]=getntrials(obj)
%   
% Return value
%   [obj, ntrials] or
%
%   [ntrials]= The total number of trials resulting from chunking the data
%       as specified by the parameters
%
%Revisions:
%    07-15-2008 - stop returning obj if only 1 output argument
function [varargout]=getntrials(obj)

if (~isfield(obj.data,'ntrialssofar') || isempty(obj.data.ntrialssofar))
    %we need to find out which stimulus is presented on trial
    %laststart is the number of trials we divide the presentation of a
    %stimulus into. This includes the trials involving the silences
    %preceding and following the stimuli
    obj.data.ntrialssofar=cumsum([obj.stimspec(obj.data.stimindex).laststart]);
end
ntrials=obj.data.ntrialssofar(end);

switch nargout
    case 1
        varargout{1}=ntrials;
    case 2
    varargout{1}=obj;
    varargout{2}=ntrials;
    otherwise
    error('invalid number of output arguments');
end
