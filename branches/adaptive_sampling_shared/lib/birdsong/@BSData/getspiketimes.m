%function spiketimes=getspiketimes(obj,windex)
%	 obj=BSData object
%    windex - which presentation of a wave file to get the spike times for
%           -this is the wave trial 
%           
%Return value: 
%	 spiketimes=obj.spiketimes 
%
function spiketimes=getspiketimes(obj,windex)
    obj=loaddata(obj);
	 spiketimes=obj.data.spiketimes{windex};
