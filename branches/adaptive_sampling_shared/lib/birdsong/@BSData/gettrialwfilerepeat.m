%function [stim,shist,obsrv]=gettrialwfilerepeat(bdata,windex,repeat,trials,mobj)
%   windex - wave file
%   repeat - which repeat of this file
%           - this must have the same length as trial
%   trial - trial number (i.e column of the spectrogram)
%         - trial is a number between 1 and ntrialsinwave of windex
%         - this can be an array of trials in which case we return
%           multiple
%   mobj - model object - used to determine how many spike history terms to
%     return
%Return value
%   stim - dxlength(trials)the spectrogram for this trial
%           as a vector
%   shist - dxlength(trials) vector of spike history
%   obsrv - dxlength(trials) the response on this trial
%
%Revisions
%   07-24-2008 - no longer return  BSData
function [stim,shist,obsrv]=gettrialwfilerepeat(bdata,windex,repeat,trial,mobj)

[ntrials,cstart]=getntrialsinwave(bdata,windex);
[bdata,stimspec,allobsrv]=getallwaverepeats(bdata,windex);

if (length(repeat)~=length(trial))
    error('repeat and trial must have the same number of elements');
end
ntrials=length(trial);

trial=rowvector(trial);
repeat=rowvector(repeat);
stimdim=getstrfdim(bdata);

stim=zeros(prod(stimdim),length(trial));

if (getshistlen(mobj)>0)
    shist=zeros(getshistlen(mobj),length(trial));
else
    shist=[];
end

obsrv=zeros(1,length(trial));


%obsrvindex - the index into stimspec of the last column
%for this stimulus
obsrvindex=cstart+trial-1;


sdim=prod(stimdim);
for tind=1:ntrials
    stimmat= stimspec(:,obsrvindex(tind)-getktlength(mobj)+1:obsrvindex(tind));
    stim(:,tind)=stimmat(:);

end

%spike history

if (getshistlen(mobj)>0)
    %the start column for each spike history
    startind=(obsrvindex-1)-getshistlen(mobj)+1;
    startind=max([startind;ones(1,ntrials)]);

    for tind=1:ntrials


        shistvec=allobsrv(repeat(tind),startind(tind):obsrvindex(tind)-1);
    %    shistvec=colvector(shistvec);
        shist(:,tind)=[zeros(getshistlen(mobj)-length(shistvec),1); shistvec'];
    end
end

lindex=sub2ind(size(allobsrv),repeat,obsrvindex);
obsrv=allobsrv(lindex);
obsrv=rowvector(obsrv);