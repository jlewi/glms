%function loaddata(obj)
%   obj - BSData
%
%Explanation: loads the data from the file containing the bird song data
function obj=loaddata(obj)

%check data isn't already loaded
if isempty(obj.data)

   if isa(obj.fname,'FilePath')
       obj.filedata=load(getpath(obj.fname));
   else
        obj.filedata=load(obj.fname);
   end
   
   %initialize stimspec for this dataset
   obj.stimspec=repmat(struct('fname',[],'spec',[],'freqs',[],'fs',[],'stimsize',[]),[1 length(obj.filedata.waves)]);
   
   for ind=1:length(obj.filedata.waves)
      obj.stimspec(ind).fname=obj.filedata.waves{ind}; 
      
      %make sure it has the .wav file extension
      [fdir, fname,fext]=fileparts( obj.stimspec(ind).fname);
      
      if isempty(fext)
          obj.stimspec(ind).fname=[obj.stimspec(ind).fname '.wav'];
      end
   end 
   
   %**********************************************************
   %compute the length of the stimulus and observation windows
   %*************************************************************
   wfile=1;
   [obj, sinfo]=getstiminfo(obj,wfile);
   fs=sinfo.fs;
   
   obj.obsrvparam.tlength=obj.obsrvparam.tnsamps*1/fs;
   obj.stimparam.tlength=obj.stimparam.nobsrvwind*obj.obsrvparam.tlength;
   
   %create the .data fields by reprocessing the data in in filedata
   obj.data.stimindex=obj.filedata.mStimOrder(:,1);
   obj.data.stimrepeat=zeros(length(obj.data.stimindex),1);
   
   %get the order of the wave files
   mstimorder=obj.filedata.mStimOrder;
   
   %filedata.spikeIndex is a cell array of cellarrays
   %spikeindex{i}{k} - is an array. Each array is the indexes
   %      of the spiketimes in obj.filedata.spikeTimes corresponding
   %      to the spikes during the k'th presentation of the i'th wave file
   %waventrial will keep track of the presentation of the stimulus
   %as we loop through the data
   waventrial=zeros(1,length(obj.stimspec));
   
   %set data.spiketimes{j} - to the spiketimes on the jth trial
   obj.data.spiketimes=cell(size(mstimorder,1),1);
   for trial=1:size(mstimorder,1);
      waventrial(mstimorder(trial,1))= waventrial(mstimorder(trial,1))+1;
      
      %get the indexes in spikeTimes for the spikes of the 
      % k'th presentation of the j'th stimuli
      %j=mstimorder(trial,1)
      %k=waventrial(mstimorder(trial,1))
      %
      %mstimorder stores the intended presentation order of each 
      %wavefile. Its possible the experiment is stopped before all inputs
      %were presented
      nrepeats=getnrepeats(obj,mstimorder(trial,1));
      if nrepeats >=waventrial(mstimorder(trial,1))
          spikeindex=obj.filedata.spikeIndex{mstimorder(trial,1)}{waventrial(mstimorder(trial,1))};
          obj.data.spiketimes{trial,1}=obj.filedata.spikeTimes(spikeindex)';

        %keep track of which repeat of wave file this is on this trial        
          obj.data.stimrepeat(trial,1)=waventrial(mstimorder(trial,1));
      else
         %no spikes were recorded for this presentation
         obj.data.stimrepeat(trial,1)=nan;
      end
   end
   %create a cell array of the spike times for each stimulus
%    ntrials=length(obj.data.stimindex);
%    spikeindex=[obj.filedata.spikeIndex{:}];
%    %now get the spike times
%    obj.data.spiketimes=cell(size(spikeindex));
%    
%    for j=1:length(spikeindex)
%       obj.data.spiketimes{j}=obj.filedata.spikeTimes(spikeindex{j})';
%    end
   
   %get the length of the silence preceding the stimulus
   %and following the stimulus in seconds
   obj.data.presilence=obj.filedata.stimStaticCh.Delay;
   obj.data.postsilence=obj.filedata.stimStatic.PostStimRecordTime;
   
   %compute last trial
   obj=wavechunkinfo(obj);
end