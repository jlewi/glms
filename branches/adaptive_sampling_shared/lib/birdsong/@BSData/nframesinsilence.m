%function [obj,npre,npost]=nframesinsilence(obj)
%   
%Return value:
%   npre - number of frames in the silence preceding the stimulus
%   npost - number of frames in the silence following the stimulus
function [obj,npre,npost]=nframesinsilence(obj) 

%npre and npost are the number of frames in the silence preceding and
%following the stimulus
%
%we take the floor because the silences may not consist of an integer
%number of obseravtion windows
npre=floor(obj.data.presilence/obj.obsrvparam.tlength);
npost=floor(obj.data.postsilence/obj.obsrvparam.tlength);