%function strfdim=getstrfdim(obj)
%	 obj=BSData object
% 
%Return value: 
%	 strfdim=obj.stimdim 
%       [nrows ncols] - dimenions of the spectrogram for each input
function strfdim=getstrfdim(obj)

strfdim=[0 0];

if isnan(obj.stimparam.nfreqs)  
   [obj,spec,freqs]=getwavespec(obj,1);
    obj.stimparam.nfreqs=length(freqs);
end
 strfdim(1)=obj.stimparam.nfreqs;
strfdim(2)=obj.stimparam.nobsrvwind;
