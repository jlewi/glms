%function nrepeats=getnrepeats(obj,windex)
%	 obj=BSData object
% 
%Return value: 
%	 nrepeats=obj.nrepeats 
%       number of times this wave file is repeated
function nrepeats=getnrepeats(obj,windex)

    if isempty(windex)
        windex=1:getnwavefiles(obj);
    end
    nrepeats=zeros(length(windex),1);
    
    for j=1:length(windex)
     nrepeats(j)=length(obj.filedata.spikeIndex{windex(j)});
    end
