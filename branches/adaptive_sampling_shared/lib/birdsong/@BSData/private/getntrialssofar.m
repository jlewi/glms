%function bdata =getntrialssofar(bdata)
%
%Return value
%   ntrialsofar - an array with its length the number
%                of presentations of wave files
%           ntrialsofar(j)= the number of trials into which we 
%           divide the first j' wave files into.
%           This includes the pre and post stimulus silences
function [obj,ntrialssofar]=getntrialssofar(obj)

if (~isfield(obj.data,'ntrialssofar') || isempty(obj.data.ntrialssofar))
    %we need to find out which stimulus is presented on trial
    %laststart is the number of trials we divide the presentation of a
    %stimulus into. This includes the trials involving the silences
    %preceding and following the stimuli
    obj.data.ntrialssofar=cumsum([obj.stimspec(obj.data.stimindex).laststart]);
end
ntrialssofar=obj.data.ntrialssofar;