%function obj=saveobj(obj)
%   
%Explanation: Prepare the object for saving. 
%   This means zeroing out the spectrograms of the stimuli 
%
%   Since the object is a handle we need to make a copy of the object 
%       before zeroing out its fields. Otherwise we will end up zeroing out
%       the data in our current copy.
function nobj=saveobj(obj)

%make a copy of this object which has its 
%filedata, stimdir, and stimspec fields set to [];
nobj=BSData(obj);
    