%function obsrv - getobsrvmat(obj,sindex,repeat)
%   obj - the bsdata object
%   sindex - the index of the wave file to get the observations for
%
%   repeat - which repeate of the stimulus to get the observation for
%       if this isn't specified we return an matrix with each row
%       being the observation on a different repeat
%
%Return:
%   obsrv - a matrix. Each row is  a vector with each column
%       containing the observation on a different trial
%
%Explanation: This function converts the spike times in a vector of
%   responses
function obsrv=getobsrvmat(obj,sindex,repeat)

if ~exist('repeat','var')
    repeat=[];
end


%there's probably a more efficient way to compute the length
%number of trials is size of spectrogram of stimulus - size of strf
[obj,fullspec]=getfullspec(obj,sindex);

[spiketimes]=getstimresp(obj,sindex);

if isempty(repeat)
   repeat=1:length(spiketimes); 
end

obsrv=zeros(length(repeat),size(fullspec,2));
	 
ntrials=size(fullspec,2);

%set the appropriate entries to zero

for rind=1:length(repeat)
    %map each spiketime to the index of the appropriate observation
    %ind=floor(spiketimes{row}/getobsrvtlength(obj))+1;
  
    %compute the edges of the time bin for each observation
    stimes=[0:ntrials]*getobsrvtlength(obj);
    %count the number of spikes that fall in each bin
    counts=histc(spiketimes{repeat(rind)},stimes);
    obsrv(rind,:)=counts(1:end-1);
end
