%function [obj,spec,freqs]=getstimspec(obj,sindex)
%   obj    - bsdata object
%   sindex - the index of the stimulus to get the spectrogram for
%
%Return value:
%   obj - return the object so we store the spectrogram if it hasn't been
%   computed yet.
%   spec - spectrogram of the sound
%   outfreqs - the output frequencies
%   t - the time 
%Explanation: Gets the spectrogram of the stimulus; that is the spectrogram
%of the wave file plus any pre and post silences.
%
%
%%Revisions
%
%       02-10-2009 -Bug fix. The time for each spectrogram was negative. I
%           think because I was originally looking at time of the strf and
%           not for the spectrogram
function [obj,spec,outfreqs,t]=getfullspec(obj,sindex)      
%get the length of the silences before and after the stimulus;
[obj,npre,npost]=nframesinsilence(obj); 
 
[obj,spec,outfreqs]=getwavespec(obj,sindex);

nfreqs=size(outfreqs,1);
pre=zeros(nfreqs,npre);
post=zeros(nfreqs,npost);

spec=[pre,spec,post];

nincrements=size(spec,2);
t=[0:1:(nincrements-1)]*obj.obsrvparam.tlength;

