%function [t,freqs]=strftimefreq=getstrftimefreq(obj)
%	 obj=BSData object
% 
%Return value: 
%	 [t, freqs] - the times and frequencies at which the strf is computed
%       These are the times corresponding to the frame used to compute the 
%       response. 
function [t,freqs]=getstrftimefreq(obj)

sindex=1;
%[obj,info]=frameinfo(obj,sindex);
[obj,spec,freqs]=getwavespec(obj,1);

t=[-(obj.stimparam.nobsrvwind-1):1:0]*obj.obsrvparam.tlength;
     
     