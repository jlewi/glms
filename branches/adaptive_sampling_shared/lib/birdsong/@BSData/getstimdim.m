%function stimdim=getstimdim(obj)
%	 obj=BSData object
% 
%Return value: 
%	 stimdim=obj.stimdim 
%       [nrows ncols] - dimenions of the spectrogram for each input
function stimdim=getstimdim(obj)

error('08-22-2008. Obsolete use getstrfdim instead.');



