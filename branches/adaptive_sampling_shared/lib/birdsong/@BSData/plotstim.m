%function hf=plotstim(obj,sindex)
%   obj    - object representing the bird song data
%   sindex - index of the stimulus to plot
%
%Return value:
%   hf - handle to the figure
%Explanation: makes a plot of the sound - its intentisty and its
%spectrogram
%
%Code is based on David Schneider's display_stim code
function hf=plotstim(obj,sindex)

    %****************************************
    % Load the stimulus and calculate necessary parameters
    %****************************************
    %filenames as stored in the .mat file are usually already missing the
    %.wav extension
    filename1 = fullfile(getpath(obj.stimdir),obj.stimspec(sindex).fname);
    %filename1 = filename1(1:end-4);  %chop off the .wav
    [stimulus fs nbits] = wavread(filename1);
    
%****************************************
% Plot the stimulus envelope and the spectrogram
%****************************************
hf=figure;
subplot(3,1,1);
envelope_time = (1/fs:1/fs:length(stimulus)/fs)*1000;
plot(envelope_time,stimulus)
axis tight
title(filename1,'Interpreter','none')
subplot(3,1,2:3)
[obj,spec]=getstimspec(obj,sindex);
imagesc(spec);axis xy
set(gca,'YTick',[])
xlabel('Time (msec)')
ylabel('Frequency')