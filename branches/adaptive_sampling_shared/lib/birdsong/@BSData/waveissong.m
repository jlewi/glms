%function issong=waveissong(bdata,windex)
%   windex - the index of the wave file to check if its song
%
%Explanation: determine if this wave file is song or noise
%
%revision 
%   10-08-2008 - operate on arrays of windex
%
function issong=waveissong(bdata,windex)

%to check if its noise just check the first three letters of the wave file
%name
issong=zeros(1,length(windex));

for wi=1:length(windex)
[fdir fname]=fileparts(bdata.stimspec(windex(wi)).fname);
if (strcmp(fname(1:3),'rip'))
    issong(wi)=false;
else
    issong(wi)=true;
end
end