%function nwavefiles=getnwavefiles(obj)
%	 obj=BSData object
% 
%Return value: 
%   return the number of wavefiles
%
function nwavefiles=getnwavefiles(obj)
	 nwavefiles=length(obj.stimspec);
