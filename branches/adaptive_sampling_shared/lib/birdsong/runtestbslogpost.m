%05-28-2008
%
%Explantion: This script calls testbslogpost for a number
%   of different tests that we should run
%
%
% You should use really long alengths
%   b\c initial responses contain main zeros so its easy to obscure off by
%   1 errors
% Other tests I should run:
%   Models other than canonical poisson
%
%**************************************************************
%No spike history
%**************************************************************

windexes=[2];
%freqsubsample=64;
freqsubsample=64;
obsrvwindowxfs=250;
stimnobsrvwind=5;
alength=0;
windexes=[1];

testbslogpost(freqsubsample,obsrvwindowxfs,stimnobsrvwind,alength,windexes);
fprintf('Test Passed: No spike history. \n');



%**************************************************************
%Spike history - shorter than duration of stimulus
%   (this verifies that alighnment is ok)
%**************************************************************
freqsubsample=64;
obsrvwindowxfs=250;
stimnobsrvwind=1;
alength=1;
windexes=[1];


testbslogpost(freqsubsample,obsrvwindowxfs,stimnobsrvwind,alength,windexes);
fprintf('Test Passed: spike history shorter than STRF. \n');


%**************************************************************
%Spike history - longer than duration of stimulus
%   (this verifies that alighnment is ok)
%**************************************************************
freqsubsample=64;
obsrvwindowxfs=250;
stimnobsrvwind=5;
alength=10;

windexes=[1];

testbslogpost(freqsubsample,obsrvwindowxfs,stimnobsrvwind,alength,windexes);
fprintf('Test Passed: spike history longer than STRF. \n');
%%
%***************************************************************
%spike history just as long as strf
%*****************************************************************
freqsubsample=64;
obsrvwindowxfs=250;
stimnobsrvwind=5;
alength=5;
windexes=[1];

testbslogpost(freqsubsample,obsrvwindowxfs,stimnobsrvwind,alength,windexes);
fprintf('Test Passed: spike history same length as STRF. \n');