%function trials=gettrialswithspikes(bdata)
%   bdata - object
%
%Return value:
%   trials - an array of the trial numbers on which spikes occured
%
%Explanation we loop across the spike times of all wave presentations
%   and determine the trial on which each spike occured
%
%   This is useful for computing the spike trigerred average
%
%  Some spikes will be thrown out. This can happen if the spike occured
%  early enough in the trial that we have not been recording long enough to
%  the full input given the length of the window used.
%
function trials=gettrialswithspikes(bdata)

error('This function has not been overwritten yet.');