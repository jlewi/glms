%function timebslogpost(outfile,
%   outfile - file where we should save the output data to
%Explantion: This function times the running of bslogpost.
%  The purpose is to see not only how fast it is but how much speedup from
%  parallelization we get.
%
% 
function runtime=timebslogpost(outfile,freqsubsample,obsrvwindowxfs,stimnobsrvwind,alength,windexes)

dfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1_corrected.mat'));



%how long to make the stimulus in time on each trial
%subtract 1 because the way bdata works we end up adding one
%stimdursamps=stimdursamps-1;
%subsample the frequencies of the spectrogram to reduce the dimensionality
%of the stimulus
bdata=BSData('fname',dfile,'stimnobsrvwind',stimnobsrvwind,'freqsubsample',freqsubsample,'obsrvwindowxfs',obsrvwindowxfs);


uobj=BSlogpost('bdata',bdata,'windexes',windexes);


%create a model
%poisson model
stimdim=getstrfdim(bdata);
klength=stimdim(1);
ktlength=stimdim(2);
glm=GLMModel('poisson','canon');
mobj=MParamObj('alength',alength,'klength',klength,'mmag',1,'ktlength',ktlength,'glm',glm,'hasbias',true);


%for the poisson model we need to make the magnitude small enough that the
%derivatives don't blow up
theta=floor(randn(getparamlen(mobj),1)*10^-2);

tic;
startclock=clock;
%compute glmproj for all presentations of this stimulus
[ll,dglm,d2glm]=compllike(uobj,mobj,theta);

timetoc=toc;
endclock=clock;

%create a structure to store the results
runtime.toc=timetoc;
runtime.etime=etime(endclock,startclock);

[stat runtime.svnrevision]=system('getsvnrevision');

param.freqsubsample=freqsubsample;
param.stimnobsrvwind=stimnobsrvwind;
param.windexes=windexes;
param.alength=alength;
runtime.param=param;

runtime.numlabs=numlabs;

ecode=0;
emsg='';

fprintf('Code to load previous results \n');
try 
   %check if file exists
   if exist(getpath(outfile),'file')
       fprintf('Opening: %s \n',getpath(outfile));
       olddata=load(getpath(outfile));
       runtime = [olddata.runtime runtime];
   else
     fprintf('file %s does not exist \n',getpath(outfile));
   end
   fprintf('Saving the data \n');
   save(getpath(outfile),'runtime');
  catch e
       %we create a cell array containing the error messages from all nodes
       emsg=sprintf('lab %d: error %s \n', labindex,e.message);
   end


labBarrier;
ecode=gplus(ecode);

if (ecode>0)
emsg=gcat({emsg});
errmsg='';
   for j=1:length(emsg)
            if ~isempty(emsg{j})
                errmsg=sprintf('%s%s',errmsg,emsg{j});
            end
        end
        %throw an error
        error(errmsg);
        
        end