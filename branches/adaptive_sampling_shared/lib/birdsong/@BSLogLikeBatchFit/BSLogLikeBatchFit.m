%function sim=BSLogLikeBatchFit('bssim')
%       bssim - array of BSBatchMLSim objects
%
%function sim=BSLogLikeBatchFit('dfiles')
%       dfiles - array of datafiles containing the simulations
% Explanation: For BSBatchMLSim objects make plots of the log likelihood
%    on the test set
%
%
%Revisions:
%   12-27-2008 - Make version a structure with each field the name of a
%             different class.
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) BSLogLikeBatchFit < BSAnalyze
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %data       - the computed log likelihood
    %           - this is an n x nwave structure array
    %           - each row corresponds to a different set of results
    %              stored in the BSBatchMLSim object
    %           - each column corresponds to a different wave file in
    %               the data
    %           .rind - index into BSBatchMLSim of results correponding to
    %           this log-likelihood
    %           .llike
    %
    %wavinfo    - structure array containing information about the wave
    %               files in the test set
    properties(SetAccess=private, GetAccess=public)
        data=[];

        wavinfo=[];
    end

    properties(SetAccess=private,GetAccess=public,Transient)
        bssimobj=[];
    end

    properties(SetAccess=private,GetAccess=public,Dependent)
        %number of wave files in the test set
        ntest;
    end
    methods(Static)
        obj=loadobj(lobj);
    end
    methods
        function wavinfo=get.wavinfo(obj)
            if (isempty(obj.wavinfo) && ~isempty(obj.bssimobj))
                bssim=obj.bssimobj;

                if isa(bssim,'BSBatchMLSim')
                    bdata=bssim.updater.bpost.bdata;
                    windexes=bssim.updater.windexes;
                elseif isa(bssim, 'BSSim')
                    bdata=bssim.stimobj.bdata;
                    windexes=bssim.stimobj.windexes;
                end


                wavinfo=[];

                %determine which wave files we didn't train on
                testwind=ones(1,getnwavefiles(bdata));
                testwind(windexes)=0;
                testwind=find(testwind~=0);

                %check if we haven't stored info about the wavefile in wavinfo
                for ind=1:length(testwind)

                    wind=length(obj.wavinfo)+1;
                    obj.wavinfo(wind).wind=testwind(ind);
                    obj.wavinfo(wind).issong=waveissong(bdata,obj.wavinfo(wind).wind);

                end
            end
                wavinfo=obj.wavinfo;

        end
        function data=get.data(obj)
            if (isempty(obj.data) && ~isempty(obj.bssimobj))
                wavinfo=obj.wavinfo;
                data=struct('llike',[]);
                data=repmat(data,length(obj.bssimobj.results),length(wavinfo));
                obj.data=data;
            end
            data=obj.data;
        end
        function ntest=get.ntest(obj)

            ntest=length(obj.wavinfo);
        end

        function bssimobj=get.bssimobj(obj)
            if (isempty(obj.bssimobj) &&  ~isempty(obj.datafile))
                v=load(getpath(obj.datafile));
                obj.bssimobj=v.bssimobj;
            end
            bssimobj=obj.bssimobj;
        end
        function obj=BSLogLikeBatchFit(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={'bssim'};
            con(2).rparams={'dfiles'};


            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %number of objects to create
            nobj=1;
            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=struct();
                    nobj=length(params.bssim);
                case 2
                    bparams=struct();
                    nobj=length(params.dfiles);
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end

            if (nobj>1)
                error('To create an array of objects call the static method');
            end

            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %WE can't call the base class because this would make
            %each object a pointer to the same object.



            obj=obj@BSAnalyze(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    %create an object array and set bssimobj
                    for index=1:nobj
                        obj(index).bssimobj=params.bssim(index);
                    end
                case 2
                    for index=1:nobj
                        v=load(getpath(params.dfiles(index)));
                        fprintf('Loading %d of %d \n',index,nobj);
                        obj(index).bssimobj=v.bssimobj;
                        obj(index).datafile=params.dfiles(index);
                    end
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %set the version to be a structure

            obj.version.BSLogLikeBatchFit=090127;

        end
    end
    methods (Static)
        %create an array of objects
        function obj=create(varargin)
            con(1).rparams={'bssim'};
            con(2).rparams={'dfiles'};


            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);

            switch cind
                case 1
                    %create an object array and set bssimobj
                    nobj=length(params.bssimobj);
                    for index=1:nobj
                        obj(index).bssimobj=BSLogLikeBatchFit('bssim',params.bssimobj(index));
                    end
                case 2
                    nobj=length(params.dfiles);
                    for index=1:nobj
                        obj(index)=BSLogLikeBatchFit('dfiles',params.dfiles(index));
                    end
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end
        end
    end
end


