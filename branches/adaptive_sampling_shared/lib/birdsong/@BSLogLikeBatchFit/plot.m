%function [fh]=plot(obj)
%   obj - an array of BSLogLikeBatchFit objects to plot
%
function [fh]=plot(obj)

%find the number of test files 
%fprintf('Warning: We do not check that the test sets are the same for each object \n');

pstyles=PlotStyles();
ntest=max([obj.ntest]);
fh=FigObj('name','Log likelihood','naxes',[1,1],'xlabel','Test wave file','ylabel','Mean log likelihood');

for dind=1:length(obj)
    
    wavinfo=gettestset(obj(dind));
    rind=size(obj(dind).data,1);
    mlike=zeros(1,size(obj(dind).data,2));
    for tind=1:length(wavinfo)
        %compute the average log-likelihood across trials in the test set
         llike=getllike(obj(dind),rind,tind);
          
        
         mlike(tind)=llike;
         


    end

    hp=plot([obj(dind).wavinfo.wind], mlike,'.','MarkerSize',18);
    
    bssim=obj(dind).bssimobj;
    
    pstyle=plotstyle(pstyles,dind);
    pstyle.Line='none';
    %pstyle.Marker=pstyles.markers{dind+1};
    addplot(fh.a,'hp',hp,'lbl',sprintf('Sim %d',dind),'pstyle',pstyle);
end
lblgraph(fh);