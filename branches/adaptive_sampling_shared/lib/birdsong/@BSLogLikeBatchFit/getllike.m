%function llike=getllike(obj,rind,tind)
%   rind - which results in the object to use to compute the log likelihood
%   tind - which file in the test set to use
%
%Explanation: Compute the log-likelihood for one of the files in the test
%   set or return it ifs already there
function llike=getllike(obj,rind,tind)
        wavinfo=obj.wavinfo;
        
        
    if isempty(obj.data(rind,tind).llike)


        
        %compute the log-likelihood
        bssim=obj.bssimobj;
        
        %object to compute the log likelihood
        cname=class(bssim.updater.bpost);
        
        cmd=sprintf('bpost=%s(''bdata'',bssim.updater.bdata,''windexes'',%d);',cname,wavinfo(tind).wind);
        eval(cmd);

        %get the number of trials in the wavefile
        [ntrials,cstart]=getntrialsinwave(bssim.updater.bdata,wavinfo(tind).wind);
        obj.data(rind,tind).llike=1/ntrials*compllike(bpost,bssim.mobj,bssim.results(rind).theta);

    end

    llike=obj.data(rind,tind).llike;
        