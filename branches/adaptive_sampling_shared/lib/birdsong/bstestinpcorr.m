%function bstestinpcorr()
%
%Explanation: Test harness for the input correlation function
function bstestinpcorr()

%**************************************************************
%no spike history

if (false)
windexes=[2];
freqsubsample=128;
obsrvwindowxfs=250;
stimnobsrvwind=1;
alength=0;
windexes=[1:2];

fprintf('Testing  no spike history . \n');
[bdata,mobj,uobj]=newdataset(stimnobsrvwind,freqsubsample,obsrvwindowxfs,alength,windexes);

icorr=inpcorr(uobj,mobj);
direct=compdirect(bdata,mobj,uobj,windexes);



iscorrect=testmatch(direct,icorr,mobj,bdata);

if ~(iscorrect)
    error('Test failed');
else
    fprintf('Test Passed: no  spike history. \n');

end
end
%**************************************************************
%Spike history shorter than STRF
%**************************************************************
if (true)
freqsubsample=64;
obsrvwindowxfs=250;
stimnobsrvwind=10;
alength=5;
windexes=[1:2];

fprintf('Testing spike history shorter than STRF . \n');
[bdata,mobj,uobj]=newdataset(stimnobsrvwind,freqsubsample,obsrvwindowxfs,alength,windexes);

icorr=inpcorr(uobj,mobj);
direct=compdirect(bdata,mobj,uobj,windexes);



iscorrect=testmatch(direct,icorr,mobj,bdata);

if ~(iscorrect)
    error('Test failed');
else
    fprintf('Test Passed: spike history. \n');

end

end
%**************************************************************
%Spike history longer than STRF
%**************************************************************

freqsubsample=64;
obsrvwindowxfs=250;
stimnobsrvwind=7;
alength=11;
windexes=[1:2];

fprintf('Testing spike history longer than strf. \n');
[bdata,mobj,uobj]=newdataset(stimnobsrvwind,freqsubsample,obsrvwindowxfs,alength,windexes);

icorr=inpcorr(uobj,mobj);
direct=compdirect(bdata,mobj,uobj,windexes);



iscorrect=testmatch(direct,icorr,mobj,bdata);

if ~(iscorrect)
    error('Test failed');
else
    fprintf('Test Passed: spike history longer than strf. \n');

end


function iscorrect=testmatch(direct,icorr,mobj,bdata)

threshold=10^-8;

d=abs(direct.inpcorr-icorr)./abs(direct.inpcorr);

iscorrect=true;
if any(d(:)>threshold)
    iscorrect=false;
   fprintf('Test failed: Computations do not match \n');
   
   stimdim=getstrfdim(bdata);
   sdim=prod(stimdim);
   dx=abs(direct.inpcorr(1:sdim,1:sdim)-icorr(1:sdim,1:sdim));
   if (any(dx>threshold))
       fprintf('\t Stimulus coefficients are wrong \n');
   else
       fprintf('\t Stimulus coefficients are correct \n');
   end
   
   %stimulus bias
   dx=abs(direct.inpcorr(1:sdim,getparamlen(mobj))-icorr(1:sdim,getparamlen(mobj)));
   if (any(dx>threshold))
       fprintf('\t Stimulus bias are wrong \n');
   else
       fprintf('\t Stimulus bias are correct \n');
   end
   
 
    %stimulus cross corr
   dxr=abs(direct.inpcorr(1:sdim,sdim+1:end-1)-icorr(1:sdim,sdim+1:end-1));
   if (any(dxr>threshold))
       fprintf('\t Stimulus spike history correlation are wrong \n');
   else
       fprintf('\t Stimulus spike history correlation are correct \n');
   end
   
     %spike history
   drr=abs(direct.inpcorr(sdim+1:end-1,sdim+1:end-1)-icorr(sdim+1:end-1,sdim+1:end-1));
   if (any(drr>threshold))
       fprintf('\t spike history are wrong \n');
   else
       fprintf('\t spike history are correct \n');
   end
   
   %spike history bias
   dr=abs(direct.inpcorr(sdim+1:end-1,1)-icorr(sdim+1:end-1,1));
   if (any(dr>threshold))
       fprintf('\t spike history bias are wrong \n');
   else
       fprintf('\t spike history bias are correct \n');
   end
end

function [bdata,mobj,uobj]=newdataset(stimnobsrvwind,freqsubsample,obsrvwindowxfs,alength,windexes)
%*******************************************************************
%Create a bsdata object
%*********************************************************************

dfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1_corrected.mat'));

bdata=BSData('fname',dfile,'stimnobsrvwind',stimnobsrvwind,'freqsubsample',freqsubsample,'obsrvwindowxfs',obsrvwindowxfs);


uobj=BSlogpost('bdata',bdata,'windexes',windexes);

stimdim=getstrfdim(bdata);
klength=stimdim(1);
ktlength=stimdim(2);
glm=GLMModel('poisson','canon');
mobj=MParamObj('alength',alength,'klength',klength,'mmag',1,'ktlength',ktlength,'glm',glm,'hasbias',true);


%************************************************************
%function direct
%
%Explantion: Compute the input correlation directly
function [direct]=compdirect(bdata,mobj,uobj,windexes)

direct.inpcorr=zeros(getparamlen(mobj),getparamlen(mobj))'


for w=1:length(windexes)

    windex=windexes(w);
    [bdata,trials]=gettrialindforwave(bdata,windex);


    for r=1:size(trials,1)
        %warning('need to set r=1:size(trials,1)');
        %for r=1
        for trial=trials(r,1):trials(r,2)
            if (mod(trial-trials(r,1),1000)==0)
                fprintf('Trial repeat %d of %d: %0.4g of %0.4g \n',r,size(trials,1),trial-trials(r,1),trials(r,2)-trials(r,1));
            end



            [bdata,sr,shist]=gettrial(bdata,trial,getshistlen(mobj));

            inp=projinp(mobj,getinput(sr),shist);


            direct.inpcorr=direct.inpcorr+inp*inp';
        end
    end
end

