%function fh=plot(stats)
%   stats - the stimulus statistics as returned by comp
function fh=plot(stats,mobj)


[binv]=bindexinv(mobj,[1:mobj.nstimcoeff]);
nf=binv(:,1);
nt=binv(:,2);
bfun=binv(:,3);

fprior=FigObj('name','Stimulus Statistics','naxes',[2,2],'width',5,'height',6);

[maxnf,maxnt]=getmaxnfnt(mobj);


%plot each basis vector
for rind=1:2
    for cind=1:2
    setfocus(fprior.a,rind,cind);
        aind=(rind-1)*2+cind;
        
        ind=find(bfun==aind);
        
        pvar=nan(maxnf+1,maxnt+1);
        
        %add 1 to nf and nt for the 0 frequency
        linind=sub2ind([maxnf+1 maxnt+1],nf(ind)+1,nt(ind)+1);
        
        pvar(linind)=log10(abs(stats.mean(ind)));
    
        freqs=[0:maxnf];
        times=[0:maxnt];
        imagesc(times,freqs,pvar);
        
        ylim([freqs(1) freqs(end)]);
        xlim([times(1) times(end)]);
        switch aind
            case mobj.border.cc
                ttl='cos cos';
            case mobj.border.cs;
                ttl='cos sin';
            case mobj.border.ss;
                ttl='sin sin';
            case mobj.border.sc
                ttl='sin cos';
        end
        
        title(fprior.a(rind,cind),sprintf('log10(mean amplitude)\n basis vector %s',ttl));
      
        set(gca,'xtick',[]);
        set(gca,'ytick',[]);
        
    end
end

rind=2;
cind=1;

set(fprior.a(rind,cind).ha,'xtickmode','auto');
set(fprior.a(rind,cind).ha,'ytickmode','auto');
xlabel(fprior.a(rind,cind),'nt');
ylabel(fprior.a(rind,cind),'nt');


rind=1;
cind=2;
setfocus(fprior.a,rind,cind);
hc=colorbar;
sethc(fprior.a(rind,cind),hc);

cl=get([fprior.a.ha],'clim');
cl=cell2mat(cl);
cl=[min(cl(:,1)),max(cl(:,2))];
set([fprior.a.ha],'clim',cl);

lblgraph(fprior);
sizesubplots(fprior);



% height=6;
% width=5;
% fh=FigObj('name','Image statistics in fourier domain','naxes',[1 1],'width',width,'height',height);
% 
% hp=plot(abs(stats.mean));
% addplot(fh.a,'hp',hp);
% xlabel(fh.a,'i');
% ylabel(fh.a,'log (abs(Value of coefficient))');
% set(gca,'yscale','log');
% lblgraph(fh);


