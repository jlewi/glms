%function stats=comp(bdata,model);
%   bdata - BSdata object for which we want to compute the data
%
% Return value:
%   stats - the mean and covariance of each coefficient
%
%
%Explaantion:
%
function [stats,info]=comp(bdata,windexes)


strfdim=getstrfdim(bdata);

%no bias and no spike history terms
%because this weill allow us to use bslogpost to compute the projection
%of each wave file on each basis vector
alength=0;
hasbias=0;

glm=GLMPoisson('canon');

nmax=MBSFTSep.maxn([strfdim(1),strfdim(2)]);

mobj=MBSFTSep('nfreq',nmax(1),'ntime',nmax(2),'glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',hasbias);


%only use 1 repeat because the stimulus is the same on each repeat of a
%wafile
bspost= BSlogpost('bdata',bdata,'maxrepeats',1,'windexes',windexes);


%keep track of the sufficient statistics
wstats=struct('sum',[],'sum2',[],'ntrials',0);
wstats=repmat(wstats,1,length(windexes));

%stats across all wavefiles
stats=struct('sum',[],'sum2',[],'mean',[],'var',[],'ntrials',0);

%loop over the wavefiles
for wind=rowvector(windexes)
    fprintf('Processing wave file %d \n', wind);
    [wstats(wind)]=BSStimStatsFTSep.processwfile(mobj,bspost,wind);
end


stats.ntrials=sum([wstats(:).ntrials]);

stats.sum=sum([wstats.sum],2);
stats.sum2=sum([wstats.sum2],2);

stats.mean=stats.sum/stats.ntrials;

%use the unbiased estimator
stats.var=stats.sum2/(stats.ntrials-1)-stats.ntrials/(stats.ntrials-1)*stats.mean.^2;

info.mobj=mobj;
info.windexes=windexes;