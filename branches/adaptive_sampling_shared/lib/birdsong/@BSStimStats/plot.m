%function fh=plot(stats)
%   stats - the stimulus statistics as returned by comp
function fh=plot(stats)


height=6;
width=5;
fh=FigObj('name','Image statistics in fourier domain','naxes',[2 2],'width',width,'height',height);

x=0:size(stats.amp.mean,2)+1;
y=0:size(stats.amp.mean,1)+1;

nfreq=size(stats.amp.mean,1);
ntime=size(stats.amp.mean,2);
%**********************************************
%sin coefficients: mean
%***********************************************
setfocus(fh.a,1,1);

imagesc(x,y,log10(stats.amp.mean));
hc=colorbar;
sethc(fh.a(1,1),hc);

title(fh.a(1,1),'log10(Amplitude:mean)');
xlabel(fh.a(1,1),'ntime');
ylabel(fh.a(1,1),'nfreq');
xlim([0 ntime]);
ylim([0 nfreq]);


%**********************************************
%sin coefficients: var
%***********************************************
row=2;
col=1;
setfocus(fh.a,row,col);

imagesc(x,y,log10(stats.amp.var));
hc=colorbar;
sethc(fh.a(row,col),hc);

title(fh.a(row,col),'log10(Amplitude: variance)');

xlabel(fh.a(row,col),'ntime');
ylabel(fh.a(row,col),'nfreq');
xlim([0 ntime]);
ylim([0 nfreq]);



%**********************************************
%cos coefficients: mean
%***********************************************
row=1;
col=2;

setfocus(fh.a,row,col);

imagesc(x,y,stats.phase.mean);
hc=colorbar;
sethc(fh.a(row,col),hc);

title(fh.a(row,col),'Phase: mean');
xlabel(fh.a(row,col),'ntime');
xlim([0 ntime]);
ylim([0 nfreq]);


%**********************************************
%cos coefficients: std
%***********************************************

row=2;
col=2;

setfocus(fh.a,row,col);

imagesc(x,y,stats.phase.var);
hc=colorbar;
sethc(fh.a(row,col),hc);

title(fh.a(row,col),'Phase: variance');
xlabel(fh.a(row,col),'ntime');
xlim([0 ntime]);
ylim([0 nfreq]);

lblgraph(fh);
sizesubplots(fh);
