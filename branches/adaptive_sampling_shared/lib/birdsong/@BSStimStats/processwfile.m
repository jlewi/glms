%function [stats,bsproj,trig]=processwavefile(mobj,bspost,windex)
%   mobj - The model object defining the strf
%    bsdata - the bird song data
%    windex - which wavefile to process
%
%Explaantion:
%
%Return value
%   bsproj= the projection on the basis functions
%   trig  = structure
%       .amplitude - amplitude
%                  - (nfreq+1)*(ntime+1) matrix
%       .phase     - phase
function [stats,bproj,trig]=processwfile(mobj,bspost,windex)


%mobj should not have a bias or any spike history terms
if (hasbias(mobj) || mobj.alength>0)
    %we can recover by just creating a new mobj with no bias and no spike
    %history
    error('Mobj should not have a bias or any spike history terms');    
end

%matrix to store the projection on each basis
%each column is a different trial
bproj=zeros(getparamlen(mobj),getntrialsinwave(bspost.bdata,windex));

for bind=1:getparamlen(mobj)
   bvec=zeros(mobj.nstimcoeff,1);
   bvec(bind)=1;
   bproj(bind,:)=compglmproj(bspost,windex,mobj,bvec);
end

%computate the amplitude and phase
[trig]=compampphase(mobj,bproj);

amp=cat(3,trig.amp);
phase=cat(3,trig.phase);

stats.amp.sum=sum(amp,3);
stats.amp.sum2=sum(amp.^2,3);

stats.phase.sum=sum(phase,3);
stats.phase.sum2=sum(phase.^2,3);

stats.ntrials=length(trig);
