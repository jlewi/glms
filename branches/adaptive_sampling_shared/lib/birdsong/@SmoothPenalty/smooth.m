%function tsmooth=smooth(obj,tmat)
%   tmat - theta expressed as a matrix
%   
%
%Return value: 
%   The result of smoothing tmat with filter specified by this object.
function tmsmooth=smooth(obj,tmat)


tmsmooth=conv2(tmat,obj.filtcoeff,'same');