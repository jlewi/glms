%function p=comppenalty(tmat)
%   tmat - theta expressed as a matrix
%
%Return value
%   p - the penalty term for this tmat
%     -  These are negative numbers. Thus a worse penalty means the value
%     is more negative. 
function [p,tsmooth]=comppenalty(obj,tmat)

%set the dimensions of theta
if isempty(obj.thetadim)
   obj.thetadim=size(tmat); 
end
tsmooth=smooth(obj,tmat);

err=(tmat-tsmooth).^2;
err=sum(err(:));

p=-obj.errscale*err;