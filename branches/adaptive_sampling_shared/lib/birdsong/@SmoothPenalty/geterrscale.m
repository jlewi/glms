%function errscale=geterrscale(obj)
%	 obj=SmoothPenalty object
% 
%Return value: 
%	 errscale=obj.errscale 
%
function errscale=geterrscale(obj)
	 errscale=[obj.errscale];
