%function A =compquadterm(obj)
%
%Explanation: This function computes a matrix such that
%   the penalty = \vec{theta}^T A \vec{theta}
function A=compquadterm(obj)

A=zeros(obj.thetadim(1)*obj.thetadim(2),obj.thetadim(1)*obj.thetadim(2));

nfiltr=obj.size(1);
nfiltc=obj.size(2);

        %each row of the hessian is the result of convolving an impulse
        %matrix with obj.gradfilt. The result is just a matrix with zeros
        %except for a submatrix which equal obj.grad filt
        nrows=obj.thetadim(1);
        ncols=obj.thetadim(2);
        for e=1:nrows
            for h=1:ncols
                %for each e,h we computea matrix
                %the i,j element of this matrix is d (d penalty/ d \theta_e,h)(d a,
                %d b)| a=i, b=j
                d2mat=zeros(obj.thetadim(1),obj.thetadim(2));


         
                  %the following equations from smoothing.pdf
        %assume zero based indexing
        %so we subtract 1 from e and h to get the equivalent zero based
        %indexes
        ez=e-1;
        hz=h-1;
        rstartzb=max(0,ez-floor(nfiltr/2));
        rendzb=min(nrows-1,ez+floor(nfiltr/2));
        cstartzb=max(0,hz-floor(nfiltc/2));
        cendzb=min(ncols-1,hz+floor(nfiltc/2));
        
        %we now add 1 to get the zero based indexes
        rstart=rstartzb+1;
        rend=rendzb+1;
        cstart=cstartzb+1;
        cend=cendzb+1;
                if ((rstart<=rend) && (cstart<=cend))

                    %compute the corresponding start and end indexes
                    %of the matrix d2mat. This indexes are the submatrix
                    %of d2mat which gets set to obj.gradfilt
                    %rows
                 

           frstartzb=rstartzb+floor(nfiltr/2)-ez;
           frendzb=rendzb+floor(nfiltr/2)-ez;
           
           fcstartzb=cstartzb+floor(nfiltc/2)-hz;
           fcendzb=cendzb+floor(nfiltc/2)-hz;
           
           %compute the 1 based indexing
           frstart=frstartzb+1;
           frend=frendzb+1;
           fcstart=fcstartzb+1;
           fcend=fcendzb+1;
                    d2mat(rstart:rend,cstart:cend)=obj.filtcoeff(frstart:frend,fcstart:fcend);
                end



                %now element (a,b) of d2mat is d^2 penalty / d\theta_a,b
                %d\theta_e,h
                %therefore this matrix yields the column of the hessian
                %corresponding to partials with respect to e,h
                col=sub2ind([nrows, ncols], e,h);
                A(:,col)=d2mat(:);

            end
        end
