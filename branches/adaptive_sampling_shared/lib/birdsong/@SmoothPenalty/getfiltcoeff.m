%function filtcoeff=getfiltcoeff(obj)
%	 obj=SmoothPenalty object
% 
%Return value: 
%	 filtcoeff=obj.filtcoeff 
%
function filtcoeff=getfiltcoeff(obj)
	 filtcoeff=obj.filtcoeff;
