%function m=bsspiketriggeredaverage(bdata)
%   bdata - A file containing the bird song data object
%   compvar - whether or not to compute covariance
%             default is true
%
%Return value:
%   m- The spike triggered average of the STRF
%
%   we save the spike triggered average to the file
%
%   If the strf already exists then we just plot
function [theta,thetavar]=bsspiketriggeredaverage(datafile,compvar)

if isa(datafile,'FilePath')
    bdata=load(getpath(datafile),'bdata');
    bdata=bdata.bdata;
end

if ~exist('compvar','var')
    compvar=true;
end
[bdata,sr]=gettrial(bdata,1);

m=zeros(size(getmatrix(getinput(sr))));


theta=zeros(prod(getmatdim(getinput(sr))),1);
if (compvar)
thetasquare=zeros(prod(getmatdim(getinput(sr))),prod(getmatdim(getinput(sr))));
else
    thetasquare=[];
    thetavar=[];;
end
trials=gettrialswithspikes(bdata);

%only compute the spike triggered average if its not in the file
if ~(varinfile(datafile,'spiketrgavg'))

    for j=1:length(trials)
        trial=trials(j);
        [bdata,sr]=gettrial(bdata,trial);
        
        if (mod(j,100)==0)
            fprintf('Spike: %d of %d\n',j,length(trials));
        end
        %     m=getmatrix(getinput(sr))/length(trials)+m;
        if ~(getobsrv(sr)==1)
            error('No spike on try even though trial is returned by gettrialswithspikes');

        end
        stim=getData(getinput(sr));
        theta=stim/length(trials)+theta;
        if (compvar)
        thetasquare=stim*stim'/length(trials)+thetasquare;
        end
    end

    if (compvar)
    thetavar=thetasquare-theta*theta';
    end

    %add a zero bias term to theta
    theta=[theta;0];
    %save it to a file
    spiketrgavg.theta=theta;
    spiketrgavg.thetavar=thetavar;
    if (labindex==1)
        if (varinfile(datafile,'spiketrgavg'))
            v=load(getpath(datafile),'spiketrgavg');
            spiketrgavg=v.spiketrgavg;

        end

       save(getpath(datafile),'-append','spiketrgavg');
    end
else
    v=load(getpath(datafile),'spiketrgavg');
    spiketrgavg=v.spiketrgavg;
    theta=spiketrgavg.theta;
    thetavar=spiketrgavg.thetavar;

end

ntoplot=1;
if (compvar)
    ntoplot=2;
end
[bdata,spec,outfreqs]=getfullspec(bdata,1);

[t,f]=getstrftimefreq(bdata)

fh=FigObj('name','Spike Triggered Average');
fh.a=AxesObj('xlabel','time(ms)','ylabel','Frequency(hz)','nrows',ntoplot,'ncols',1)

fh.a=setfocus(fh.a,1,1);
imagesc([t(1) t(end)],outfreqs,reshape(theta(1:end-1,1),getmatdim(getinput(sr))));
fh.a(1,1)=title(fh.a(1,1),'Spike Triggered Average');
xlim([t(1) t(end)]);
ylim([outfreqs(1) outfreqs(end)]);
hc=colorbar;
fh.a(1,1)=sethc(fh.a(1,1),hc);

if (compvar)
fh.a=setfocus(fh.a,2,1);
imagesc(thetavar);
fh.a(2,1)=title(fh.a(2,1),'Spike Triggered covariance');



xlim([1 length(theta)-1]);
ylim([1 length(theta)-1]);
hc=colorbar;
fh.a(2,1)=sethc(fh.a(2,1),hc);
end
lblgraph(fh);
fh=sizesubplots(fh);
tbl={fh;{'function','bsspiketriggeredaverage'; 'Data File',getpath(datafile);'Explanation','The spike triggered average.'}};
onenotetable(tbl,seqfname('~/svn_trunk/notes/bsspikeaverage.xml'));