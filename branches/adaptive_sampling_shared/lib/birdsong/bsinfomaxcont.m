%function bssimobj=bsinfomaxcont(datafile,ntorun,psize,datafile,statusfile)
%
%Explanation: This function loads a bssimobj from datafile
%   and calls iterate
%   The purpose of this function is to use with parallel jobs on the
%   cluster
%   On the cluster we don't want to actually load the simobj because we
%   haven't copied the file from the cluster yet
%
function bssimobj=bsinfomaxcont(datafile,ntorun,statusfile)


fid=fopen(getpath(statusfile),'a');
fprintf('lab %d: calling load \n', labindex);
fprintf(fid,'lab %d: calling load \n', labindex);
v=load(getpath(datafile),'bssimobj');
fprintf('lab %d: finished load \n', labindex);
fprintf(fid,'lab %d: finished load \n', labindex);
bssimobj=v.bssimobj;
fprintf(fid,'lab %d: calling iterate \n', labindex);
[bssimobj]=iterate(bssimobj,ntorun,datafile,statusfile);
fprintf(fid,'lab %d: finished iterate \n', labindex);
fclose(fid);