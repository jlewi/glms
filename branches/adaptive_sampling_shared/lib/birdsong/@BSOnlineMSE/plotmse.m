function [fmse]=plotmse(obj)

mobj=obj.bmlsim.mobj;

%make a plot of the mse
nplots=1;

if (mobj.alength>0)
    nplots=nplots+1;
end

if (mobj.hasbias)
    nplots=nplots+1;
end

fmse=FigObj('name','mse','width',7,'height',7,'naxes',[nplots 1]);

rind=1;

%loop over the simulations because we only want to read the maps once

thetatrue=obj.bmlsim.results(end).theta;
for sind=1:length(obj.seqsims)

    rind=1;
    
    %get theta for this simulation
    theta=getm(obj.seqsims(sind).allpost);
    theta=theta(:,1:obj.seqsims(sind).niter+1);
    
    niter=obj.seqsims(sind).niter;
    lbl=obj.seqsims(sind).label;
    %*********************************************************************
    %Plot the mse of the stimulus coefficients
    %*****************************************************************
    setfocus(fmse.a,rind,1);
    
    smse=theta(mobj.indstim(1):mobj.indstim(2),:)-thetatrue(mobj.indstim(1):mobj.indstim(2))*ones(1,niter+1);
    smse=sum(smse.^2,1).^.5;
    
    hp=plot(0:niter,smse);
    
    addplot(fmse.a(rind,1),'hp',hp,'lbl',lbl);

    %*********************************************************************
    %Plot the mse of the spike history coefficients
    %*****************************************************************
    if (mobj.alength>0)
        rind=rind+1;
        setfocus(fmse.a,rind,1);
    
        hmse=theta(mobj.indshist(1):mobj.indshist(2),:)-thetatrue(mobj.indshist(1):mobj.indshist(2))*ones(1,niter+1);
        hmse=sum(hmse.^2,1).^.5;
    
        hp=plot(0:niter,hmse);
    
        addplot(fmse.a(rind,1),'hp',hp);
    end
    
    %*********************************************************************
    %Plot the mse of the bias
    %*****************************************************************
    if (mobj.alength>0)
        rind=rind+1;
        setfocus(fmse.a,rind,1);
    
        bmse=theta(mobj.indbias(1),:)-thetatrue(mobj.indbias(1))*ones(1,niter+1);
        bmse=sum(hmse.^2,1).^.5;
    
        hp=plot(0:niter,bmse);
    
        addplot(fmse.a(rind,1),'hp',hp);
    end
    
end


%*********************************************************************
%add labels
%*********************************************************************
%%
rind=1;
title(fmse.a(rind,1),'Stimulus coefficients');
ylabel(fmse.a(rind,1),'||\mu_t - \theta||');
    set(fmse.a(rind,1),'xscale','log');
    
if (mobj.alength)
    rind=rind+1;
    title(fmse.a(rind,1),'Spike history coefficients');
    ylabel(fmse.a(rind,1),'||\mu_t - \theta||');
    set(fmse.a(rind,1),'xscale','log');    
end

if (mobj.hasbias)
    rind=rind+1;
    title(fmse.a(rind,1),'Bias');
    ylabel(fmse.a(rind,1),'||\mu_t - \theta||');  
    set(fmse.a(rind,1),'xscale','log');
end

xlabel(fmse.a(rind,1),'trial');

lblgraph(fmse);

setposition(fmse.a(1,1).hlgnd,.5,.82,[],[]);

otbl={fmse, {'class',mfilename('class');'function',mfilename('full')}};
onenotetable(otbl,seqfname('~/svn_trunk/notes/compmse.xml'));
