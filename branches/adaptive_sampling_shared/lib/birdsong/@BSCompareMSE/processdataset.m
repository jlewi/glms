%function [mse,dset]=processdataset(msefile,dind,trials)
%   dind   - the element in the msefile to process
%   trials   - which trials to process the mse for
%            - Leave trials empty to do all trials for which we have the
%            covariance matrix.
%
%Return Value
%   data - structure 
%
%   dset
%       .msefile - name of the msefile
%Explanation: Computes the MSE for the specified trials in a dataset
%function processdataset(setupfile)
function [d,dset]=processdataset(dind,msefile,trials)

if ~exist('trials','var')
   trials=[]; 
end
%check if it exists
if ~exist(getpath(msefile),'file')
    error('The file storing the mse data does not exist');
end

%load the data
v=load(getpath(msefile));

if (length(v.data)~=length(v.dsets))
    error('MSE file appears to be corrupt the length of data is not the same as dsets');
end

if (dind>length(v.dsets))
    error('dind exceeds the number of datasets stored in the file');
end

[dset,d]=BSCompareMSE.compmse(v.dsets(dind),v.data(dind),trials);

dsets=copystruct(v.dsets,dind,dset);
data=copystruct(v.data,dind,d);
%save('~/tmp/data.mat');
save(getpath(msefile),'dsets','data','-append');

        