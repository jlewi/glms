%function [dset,data]=compmse(dset,data,trials)
%   dset    - structure describing this dataset
%   data    - structure containing the data already calculated
%   trials  - which trials to compute the mse on
%           - Leave trials empty to do all trials for which we have the
%            covariance matrix.
%
%
%Return value
%   mse - structue array
%       .trial - trial on which we computed the mse
%       .wind  - the index into wavinfo of the wave file for which this the
%                 mse
%       .simind - index into dsets of the simulation for which this is the
%                 mse
%
%Explanation:
% This script looks at a bunch of simulations in which we did not train
% on at least some of the wavefiles. We use those wavefiles as a test set
% and compute the MSE. For each file in the test set, we plot the MSE of
% the fitted model after different numbers of trials. Each curve
% corresponds to a different design.
%
%Revisions:
%   09-22-2008 - Turned it into a function
%              - Allow us to compute the MSE with the posterior projected
%              on the tangent space even for models in which the tangent
%              space wasn't used
%06-03-2008
%
%Cross validation
%
%
% 7-29-2008
%   This script started causing a segmentation fault unless you pause
%   execution
%   by putting a breakpoint before we make the plots
function [dset,data]=compmse(dset,data,trials)

bssim=[];

if isempty(data.wavinfo)
    v=load(getpath(dset.datafile));
    bssim=v.bssimobj;
    data.wavinfo=BSCompareMSE.gettestset(bssim);
end

if (~exist('trials','var') || isempty(trials))
   %set trials to all trials for which we have the covariance matrix 
    v=load(getpath(dset.datafile));
    bssim=v.bssimobj;
    
    trials=readmatids(bssim.allpost.cinfo.caccess);
end
%get the wavefiles in the test set if we haven't already processed them
testwind=rowvector([data.wavinfo.wind]);
trials=BSCompareMSE.trialsleft(data.mse,trials);


%%
%**************************************************************************
%Process the trials
%**************************************************************************
if ~(isempty(trials))

    if (isempty(bssim))
        %haven't loadded bssim yet
        v=load(getpath(dset.datafile));
        bssim=v.bssimobj;
    end

    bdata=bssim.stimobj.bdata;
    mobj=bssim.mobj;


    %            dset.lbl=bssim.label;
    dset.niter=bssim.niter;

    ntest=length(data.mse);

    %loop over all the trials we do the cross validation for
    trials=rowvector(trials);
    for trial=trials
        fprintf('trial:%d \n', trial);
        %terminate if we exceed the number of trials
        if (bssim.niter<trial)
            break;
        end
        theta=getm(bssim.allpost,trial);
        [stimcoeff, shistcoeff, bias]=parsetheta(bssim.mobj,theta);

        strf=getstrf(bssim.mobj,theta);

        %make sure the covariance matrix was saved
        post=getpost(bssim.allpost,trial);


        if isempty(getc(post))
            fprintf('Skipping trial %d for simulation %d label=%s. Covariance matrix was not saved \n',trial,sind,bssim.label);
        else

            if (dset.usetanpost)
                if isa(bssim.mobj,'MTanSpace')
             
                    mobj=bssim.mobj;
                else
                    %construct a tan space object;
                    params.klength=bssim.mobj.klength;
                    params.ktlength=bssim.mobj.ktlength;
                    params.alength=bssim.mobj.alength;
                    params.rank=dset.taninfo.rank;
                    params.glm=bssim.mobj.glm;
                    params.hasbias=bssim.mobj.hasbias;
                    params.mmag=bssim.mobj.mmag;
                    eval(sprintf('mobj=%s(params);',dset.taninfo.class));

                end
                pb=PostTanSpace(post,mobj);


                minfo=pb.fullpost.m+pb.tanpoint.basis*pb.tanpost.m;
                cinfo=pb.tanpoint.basis*pb.tanpost.c*pb.tanpoint.basis';
                %compute eigendecomp of cinfo because we will need it for the stimulus
                %optimization.
                post=GaussPost('m',minfo,'c',cinfo);



            end
            %************************************************************
            %apply smoothing to the MAP
            %******************************************************
            if (isfield(dset,'smooth') && ~isempty(dset.smooth))
                
                m=getm(post);
                c=getc(post);
                %we need to set amplitudes for coefficients greater than or
                %equal to the cutoff frequency to zero
                subind=bindexinv(mobj,1:mobj.nstimcoeff);
                
                [stimcoeff,strfcoeff,bias]=parsetheta(mobj,m);
                
                %find the linear indexes of those elements to set to zero
                lind=[find(subind.nf>=dset.smooth.nfcutoff) ;find(subind.nt>=dset.smooth.ntcutoff)];
                lind=sort(unique(lind));
                
                for index=rowvector(lind)
                    %set the coefficient to zero                    
                    stimcoeff(index)=0;

                    %set the covariance to zero
                    %WARNING: THIS ASSUMES THE STIMULUS COEFFICIENTS ARE
                    %THE FIRST ENTRIES IN THE COVARIANCE MATRIX
                    c(:,index)=0;
                    c(index,:)=0;
                end
                m=packtheta(mobj,stimcoeff,strfcoeff,bias);
                post=GaussPost(m,c);
            end
            for wind=testwind
                ntest=ntest+1;

                %        testr(ntest).fmapind=fmapind;
                data.mse(ntest).wavefile=wind;

                data.mse(ntest).trial=trial;

                [data.mse(ntest).mse data.mse(ntest).exprate data.mse(ntest).emprate]=bspsthexpmse(bssim,post,data.mse(ntest).wavefile);


            end
        end

    end

end
