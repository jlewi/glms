%function sim=BSCompareMSE
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: This class provides static methods for computing the MSE of
% a fitted model to the bird song data on a test set.
%
classdef (ConstructOnLoad=true) BSCompareMSE
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        version=080923;

    end

    methods (Static=true,Access=public)
       [msefile,dind]=createmseset(dnew,msefile);
       [d,dset]=processdataset(dind,msefile,trials);
       msefile=msefname(datafile);
       [dind,msefile]=findsetinfile(dset,msefile);

       deletedset(msefile,dind);
       
       %make the plot
       [fmse,otbl]=plot(dsets,pstyles);
       
       [mse,dset]=process(datasets);
    end
    
    methods(Static=true,Access=private)
         %compute the mse for the data in a wave file.
        [dset,data]=compmse(dset,data,trials);
        tleft=trialsleft(mse,trials);
        wavinfo=gettestset(bssim);
    end
    methods
        function obj=BSCompareMSE(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};
            con(1).cfun=1;


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=[];
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=[];
                otherwise
                    %remove fields which are for this class
                    try
                        bparams=rmfield(params,'field');
                    catch
                    end
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 0
                    %do nothing used by load object

                otherwise
                    error('Constructor not implemented')
            end



        end
    end
end


