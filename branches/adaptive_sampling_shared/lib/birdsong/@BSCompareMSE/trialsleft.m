%function tleft=trialsleft(mse,trials)
%   mse   - a structure array containing the MSE already computed on some
%           trials
%   trials- trials on which we want to compute the mse
%
%Return value:
%   tleft - an array indicating which of the trials in trials for which the
%   MSE isn't stored in MSE.
%
function tleft=trialsleft(mse,trials)
     
if isempty(mse)
    tleft=trials;
else
    
        tprocessed=[mse.trial];
        
        ttoprocess=zeros(1,length(trials));


        for tind=1:length(trials)
            if isempty(find(tprocessed== trials(tind),1))
                ttoprocess(tind)=1;
            end
        end

        tleft=trials(logical(ttoprocess));
end