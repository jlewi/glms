%function [mse,dset]=process(datasets)
%  datasets - structure array of datasets to process
%
%Explanation: Wrapper function to process many datasets
function [mse,dset]=process(datasets)

for dind=1:length(datasets)
   [msefile,dinfile]=BSCompareMSE.createmseset(datasets(dind)); 
   [mse(dind)]=BSCompareMSE.processdataset(dinfile,msefile);
end