%06-14-2008
%
%Test the computations of smooth penalty by comparing to the values
%computed in Mathematica

nrows=5;
ncols=5;

%penalty=SmoothPenalty('errscale',1,'size',[3,3],'sigma',eye(2),'thetadim',[nrows ncols]);
penalty=SmoothPenalty('errscale',1,'size',[3,3],'sigma',eye(2));
theta=reshape([1:nrows*ncols],[nrows,ncols]);

lpass=smooth(penalty,theta)

%compute the derivatives
[p,dp,d2p]=dpenaltydtheta(penalty,theta);

dp=reshape(dp,nrows,ncols)


%%
%some related testing for figuring out how to write the derivatives
%compactly
% fcoeff=getfiltcoeff(penalty)
% 
% 
% impulse=zeros(nrows,ncols);
% impulse(1,1)=1;
% 
% limp=conv2(impulse,fcoeff,'same');
% 
% l2=conv2(limp,conv2(impulse,fcoeff,'same'),'same')
% 
% fdouble=conv2(fcoeff,fcoeff)
% 
% 
% 


% lvec=impulse(:)'*compquadterm(penalty);
% lmat=reshape(lvec,nrows,ncols);