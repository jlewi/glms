%function [mu]=bsfitglmbatch(datafile,statusfile,opt)
%       datafile - file contains 2 objects
%       allpost - a GaussPostFile object contianing the posteriors
%       bdata   - a BSData object used to get the actual data)
%
%
%       
%Return value:
%   mu - peak of posterior
%
%   The return value is saved to datafile
%
%Explanation:
%   Finds the maximum of the posterior using gradient ascent
%
%Revision:
%   5-06-2008. obsolete
function [batchfit]=bsfitglmbatch(datafile)

error('this is obsolete use BSBatchFitGLM object instead');


%**************************************************************************
%load the allpost and bdata
%*************************************************************************
data=load(getpath(datafile));
allpost=data.allpost;
bdata=data.bdata;
clear data;

[bdata,sr]=gettrial(bdata,1);
dim=prod(getmatdim(getinput(sr)));

%force bdata to compute the spectrograms of all stimuli so that we only do
%this once. 
for index=1:getnwavefiles(bdata)
   [bdata, spec,outfreqs]=getstimspec(bdata,index);
end

%***********************************************************
%get the prior
%*************************************************************
prior=GaussPost('m',getm(allpost,0),'c',EigObj('matrix',getc(allpost,0)));
%compute the inverse of the prior covariance matrix so that we only do it
%once

mobj=MLogistic('dim',dim,'mmag',1);

%location of zeros of derivative
%initialization:
%thetainit=getm(allpost,getlasttrial(allpost));
thetainit=getm(allpost,0);

batchfit=[];
[batchfit.theta, batchfit.gradient, batchfit.exitflag, batchfit.fsout]= fsolve(@(theta)(gradient(theta,bdata,mobj,prior)),thetainit,optim);

if (labindex==1)
    if (varinfile(datafile,'batchfit'))
        v=load(getpath(datafile),'batchfit');
        batchfit=[v.batchfit batchfit]; 
    end
end
save(getpath(datafile),'-append','batchfit');

%*************************************************
%save results to the data file
%*****************************************************

%make sure we converged
%exitflag==1 it converged
switch (batchfit.exitflag)
    case {-3,0}
        %potentially try changing initialization point
    error('postgradasc: fsolve did not converge after %2.2g Iterations. Exited with message: \n %s',fsout.iterations,fsout.message);
    case {1,2,3}
        %do nothing it terminated ok
    otherwise
    %fprintf('grad: fsolve terminated with message \n %s \n',fsout.message);
end

%error checking
%check derivative is zero
if any(abs(batchfit.gradient)>10^-5)
    warning('Derivative at MAP is not zero');
end

function [d1total,d2total]=gradient(theta,bdata,mobj,prior)

%we need to compute the gradient of the log-likelihood
%we want to parallelize this
[bdata,ntrials]=getntrials(bdata);

d1total=zeros(1,getparamlen(mobj));
d2total=zeros(getparamlen(mobj),getparamlen(mobj));


ind=dcolon(1,ntrials);
trialind=local(ind);


for trial=trialind(1):trialind(end)
    if (mod(trial,500)==0)
        fprintf('trial=%d\n',trial);
    end
    [bdata,sr]=gettrial(bdata,trial);
    inp=projinp(mobj,getinput(sr),[]);
    [d1 d2]=d2glm(getglm(mobj),theta,prior,getobsrv(sr),inp);
    
    d1total=d1+d1total;
    d2total=d2+d2total;
end

%sum across all the labs
d1total=gplus(d1total);
d2total=gplus(d2total);

