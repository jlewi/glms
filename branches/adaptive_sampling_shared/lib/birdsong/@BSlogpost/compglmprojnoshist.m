%function glmproj=compglmproj(uobj,windex,mobj,theta)
%   windex - the index of the wave file for which we compute glmproj(t)
%   strf   - the STRF to use to compute glmproj(t)
%
%Return:
%   glmproj- each row is a vector containing glmproj[t] as a function of
%   time. Each row is a different wave trial. Each wave trial consists of
%   presenting the stimulus corresponding to windex and measuring the
%   response
%
%   fullstimspec
%   allobsrv - same dimensions as fullstimspec
%            - so could include observations at times for which we don't
%            count as a GLM trial because the input is not fully specified
%
%
%Note: the value of uobj.maxrepeats controls how many repeats of the 
%   wave file we compute the projection for. 
%
%Explanation:
%    Compute glmproj but set the spike history to zero.
%
%    This function just does some preprocessing corresponding to getting
%    the data. The actual computation takes place in compglmprojimp.
%
%Revision:
%   06-01-2008: Call compglmprojimp to do the actuall computation
%   
function [glmproj,varargout]=compglmprojnoshist(uobj,windex,mobj,theta)

bdata=uobj.bdata;


%************************************************
%get the full spectrogram of the stimulus

[bdata,stimspec]=getfullspec(uobj.bdata,windex);

%set the observations to [] so that spike history is ignored
obsrv=[];

[ntrials,cstart]=getntrialsinwave(bdata,windex);

%optional return arguments
switch nargout
    case 1
    [glmproj]=compglmprojimp(uobj,mobj,theta,stimspec,obsrv,ntrials);
    case 2
    [glmproj varargout{1}]=compglmprojimp(uobj,mobj,theta,stimspec,obsrv,ntrials);
    case 3 
        [glmproj varargout{1} varargout{2}]=compglmprojimp(uobj,mobj,theta,stimspec,obsrv,ntrials);
    otherwise
        error('Incorrect number of return arguments');
end

