%function sim=BSExpLLike('datafile',datafile)
%   datafile - the name of the file containing the simulation data
%  
%
%function sim=BSExpLLike('newdata',datafile)
%   datafile - datafile to process
%            - this always creates a new datafile
%
%   optional parameters
%       usetanpost - use the posterior on the tangent space 
%
%function sim=BSExpLLike('exfile',exfile)
%   exfile   - name of the file containing the BSExpLLike file
%
% Explanation: This class provides static methods for evaluating a
% posterior by computing the expected log likelihood of the spike trains in
% the test set. The expectation is with respect to our posterior.
%
%
% Revisions:
%   02-25-2009
%       no longer automatically compute the exfile from the datafile name
%       b\c we might want to compute expllike multiple ways for each object
%
%   01-27-2009 -
%       Class is derived from BSAnalyze
%       Save an instance of BSExpLLike to the file
%       old version is 081010
%       -add field data to store the results
classdef (ConstructOnLoad=true) BSExpLLike < BSAnalyze
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %exllike -  (nwavefilesxntrials) - structure array
    %          - rows correspond to different wave files in the test set
    %          - columns correspond to posteriors after different trials
    %          used to compute the expected likelihood.
    %          - fields the data to store
    %              .stats - statistics (mean and variance) across trials in this wave
    %                   file of the expected log likelikhood
    %          - .isdouble - indicates mean is a double not an mp object
    %
    %trials     -trial of the posterior used to compute the expected llike
    %
    %
    %
    %
    %dinfo     - stores information about the dataset
    %          -obsolete in 02-02-2009
    %wavinfo   - information about the wave files in the test set
    %            each element in the array matches the value stored in
    %            results.wind
    %
    %datafile  - the datafile from which we load the fitted model
    %outfile   - the file where we store the computations of the expected
    %            log-likelihood
    %label    - label for the data
    %         - storing the label should make it unnecessary to actual load
    %           the BSSimobjects if we just want to plot the results
    %taninfo  - use posterior projected onto the tangent space to compute
    %         .use - true/false indicates whether or not to use the posterior on
    %                the tangent space
    %          .tobj - If the datafile did not use the tangent space 
    %                   then we need to specify which tangent space object
    %                   to use
    properties(SetAccess=private, GetAccess=public)
        exllike=[];
        %dinfo=struct();

        wavinfo=[];

        outfile=[];
        
        trials=[];

        label=[];

        %use tan post
        taninfo=struct('use',[],'mtan',[]);
    end

    %bssim - store the bssimobj for this simulation. Do this so we only
    %        have to load it once.
    properties(SetAccess=private,GetAccess=public,Transient)
        bssimobj=[];
    end



    methods (Access=public)
        %compute the expected log likelihood for one of the wave files in
        %the test set
        compellike(obj);

        wavinfo=gettestset(bssim);


        %make the plot
        [fmse,otbl]=plot(dsets,pstyles,errorbars,subint);


        [fmse,otbl]=plotavg(dsets,pstyles);


    end

    methods(Access=private)
        [stats,ellike]=compellikeforwavefile(obj,post,windex,mobj);

    end

    methods
        
           function label=get.label(obj)
           if (isempty(obj.label) && ~isempty(obj.bssimobj)) 
               obj.label=obj.bssimobj.label;
           end
           label=obj.label;
           end
        
        function wavinfo=get.wavinfo(obj)
            if isempty(obj.wavinfo)
                bssim=obj.bssimobj;

                if isa(bssim,'BSBatchMLSim')
                    bdata=bssim.updater.bpost.bdata;
                    windexes=bssim.updater.windexes;
                elseif isa(bssim, 'BSSim')
                    bdata=bssim.stimobj.bdata;
                    windexes=bssim.stimobj.windexes;
                end


                wavinfo=[];

                %determine which wave files we didn't train on
                testwind=ones(1,getnwavefiles(bdata));
                testwind(windexes)=0;
                testwind=find(testwind~=0);

                %check if we haven't stored info about the wavefile in wavinfo
                for ind=1:length(testwind)

                    wind=length(wavinfo)+1;
                    wavinfo(wind).wind=testwind(ind);
                    wavinfo(wind).issong=waveissong(bdata,wavinfo(wind).wind);

                end
                obj.wavinfo=wavinfo;
            end
            wavinfo=obj.wavinfo;
        end
        function outfile=get.outfile(obj)
            if (isempty(obj.outfile)  && ~isempty(obj.datafile))
                 %hard code the suffix to avoid problems
                %obj.outfile=BSAnalyze.filename(obj.datafile(1),'expllike');
                error('02-25-2009 how should the filename be set');
            end
            outfile=obj.outfile;
        end

        function bssimobj=get.bssimobj(obj)
            if isempty(obj.bssimobj)
                if isa(obj.datafile,'FilePath');
                    v=load(getpath(obj.datafile));
                    obj.bssimobj=v.bssimobj;
                end
            end
            bssimobj=obj.bssimobj;
        end

        function bssimobj=getbssim(obj)
            if isempty(obj.bssimobj)
                v=load(getpath(obj.datafile));
                obj.bssimobj=v.bssimobj;
            end
            bssimobj=obj.bssimobj;
        end

        function obj=BSExpLLike(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'datafile'};
            con(1).cfun=1;


            con(2).rparams={'exfile'};
            
            con(3).rparams={'newdata'};
            
            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                case 2
                case 3
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

           
            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    obj.datafile=params.datafile;
                    
                    warning('This constructor is obsolete. You should specify actual exppllike object containing the file.');
                     if exist(getpath(obj.outfile),'file')
                        %load the object from the file
                        fprintf('file containing bsexpllike object exists. loading object from file \n');
                        v=load(getpath(obj.outfile));
                        obj=v.bsexllike;
                        
                        if (obj.version.BSExpLLike<090203)
                            %force the outfile to empty so it gets
                            %recomputed
                            %because the saved outfile is incorrect
                            obj.outfile=[];
                        end
                     end
                case 2
                    v=load(getpath(params.exfile));
                    obj=v.bsexllike;
                    obj.outfile=params.exfile;
                case 3
                    %process new dataset
                    obj.datafile=params.newdata;
                    obj.outfile=seqfname(BSAnalyze.filename(obj.datafile(1),'expllike'));
                    
                    if isfield(params,'usetanpost')
                       obj.taninfo.use=params.usetanpost;
                       
                       if ~isa(obj.bssimobj.mobj,'MTanSpaceInt')
                            if ~isfield(params,'mtan')
                               error('To use the tangent space with an object with a model which is not an MTanSpaceInt object you must pass in a tangent space model.');
                            else
                               obj.taninfo.mtan=params.mtan; 
                            end
                       end
                    end
                    %don't call compellike for objects when we load the
                    %object from a file because it will cause problems when
                    %we are just calling BSExpLLike with an empty
                    %constructor to laod the object.
                    compellike(obj);
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


           obj.version.BSExpLLike=090225;

        end
    end
end


