%function plot(dsets,pstyles)
%   dsets - A structure array of the datasets to process
%   pstyles - optional a PlotStyles object defining the styles to use
%   errorbars - true or false indicating we want to plot errorbars
%   subint - the interval at which we should plot the errorbars if any
%            If subint is 1 (default) often times the errrorbars will be
%            spaced to close together along the xaxis to be legible
%Explanation: Make a plot of the MSE
function [fexp,otbl]=plot(obj,pstyles,errorbars,subint)
if ~exist('pstyles','var')
    pstyles=[];
end
if isempty(pstyles)
    pstyles=PlotStyles();
         pstyles.linewidth=pstyles.linewidth;
        pstyles.markersize=4;
end

if ~exist('errorbars','var')
    errorbars=false;
end

if ~exist('subint','var')
   subint=1; 
end



%**************************************************************
%Make the plots
%************************************************************
%%
%wavefiles in the test set
wavinfo=[obj.wavinfo];

[wind,index]=unique([wavinfo.wind],'first');
wavinfo=wavinfo(index);

nplots=length(wavinfo);
clear fexp;


for wind=1:length(wavinfo)
    %plot the mse for al the files in the test set
    fexp(wind)=FigObj('name','Expected log likelihood','width',6,'height',6,'xlabel','trial','ylabel', 'E_{\theta}log p(r|s_t,\theta_t)', 'title', ['Wave file=' num2str(wavinfo(wind).wind)] );

    %loop over the simulations
    pind=0;
    for dind=1:length(obj)
        pind=pind+1;
        setfocus(fexp(wind).a,1,1);

       
        rind=find([obj(dind).wavinfo.wind]==wavinfo(wind).wind);
        
        %throw out any points for the log likelihood is close to zero
        %that we needed to use a multi precision object to store it
        keep=zeros(1,rind);
        

        if isfield(obj(dind).exllike(rind),'isdouble')      
            isd=[obj(dind).exllike(rind,:).isdouble];
        else
            isd=[];
        end
        if (length(isd)~=length(obj(dind).trials))
            %isdouble was not computed so compute it
            fprintf('isdouble was not saved. Computing and saving it. \n');
        for tind=1:size(obj(dind).exllike,2)
               obj(dind).exllike(rind,tind).isdouble=isa(obj(dind).exllike(rind,tind).stats.mean,'double');

        end
             bsexllike=obj(dind);
                save(getpath(bsexllike.outfile),'bsexllike','-append');
        end
        
        isd=logical([obj(dind).exllike(rind,:).isdouble]);
        
        
        stats=[obj(dind).exllike(rind,isd).stats];
        mean=[stats.mean];
        var=[stats.var];
        
        if isfield(stats,'meantvar')
            meantvar=[stats.meantvar];
        else
            meantvar=[];
        end
        
       
        %mse as a function of trial
        ttrial=[obj(dind).trials(isd)];

        [ttrial ind]=sort(ttrial);
        mean=mean(ind);
        var=var(ind);
        
        if ~isempty(meantvar)
           meantvar=meantvar(ind); 
        end

        %throw out any trials for which mean <-10^8
        %otherwise the plot will show an asymptote that will make the plot
        %look bad
       ind=find(mean>-10^8);
        mean=mean(ind);
        var=var(ind);
        if ~isempty(meantvar)
           meantvar=meantvar(ind); 
           meantvar=double(meantvar);
        end
        ttrial=ttrial(ind);
        
        
        %hp=plot(ttrial,mean);
       % if (errorbars && ~isempty(meantvar))
       %     heb=errorbar(ttrial(1:subint:end),mean(1:subint:end),meantvar(1:subint:end).^.5,meantvar(1:subint:end).^.5);
       if (errorbars && ~isempty(meantvar))
             heb=errorbar(ttrial(1:subint:end),mean(1:subint:end),var(1:subint:end).^.5,var(1:subint:end).^.5);
        else
            hp=plot(ttrial,mean);
        end
        %             [c m]=getptype(pind,2);
        %             pstyle.marker='o';
        %             pstyle.markerfacecolor=c;
        %             pstyle.linestyle=m;
        %             pstyle.color=c;
        %             pstyle.markersize=4;
        %             pstyle.linewidth=5;
        ps=pstyles.plotstyle(pind);
   
        addplot(fexp(wind).a,'hp',hp,'lbl',obj(dind).label,'pstyle',ps);
    end


    set(gca,'xscale','log');
    lblgraph(fexp(wind));
   set(fexp(wind).a,'ylim',[-1 0]);
end
    


%%
otbl={};
% 
 %create the table of output information
 %info about the fields
 scriptname=mfilename('fullpath');
info={'class',mfilename('class');'function',mfilename('full');};
explain={'wave file', 'A number identifying the wave file';};
 describe=sprintf('This plot compares the expected log-likelihood of the trials in the test as a function of the posterior\n');
 describe=sprintf('%sWe don''t plot the log likelihood for any trial where the log likleihood was so negative that we needed a multi-precision object \n',describe);
 describe=sprintf('%sOnly 1 repeat for each file in the test set is used\n',describe);
 explain=[explain;{'explanation',describe}];
% for sind=1:length(dsets)
%     if isfield(dsets(sind),'explain')
%         explain=[explain; {dsets(sind).lbl, {'setup',dsets(sind).setupfile;'mse data', getpath(fnames(sind));'explain',dsets(sind).explain}}];
%     else
%         explain=[explain; {dsets(sind).lbl, {'setup',dsets(sind).setupfile;'mse data', getpath(fnames(sind))}}];
%     end
% end
% 
 for wind=1:length(wavinfo)
     oinfo={'wave file', wavinfo(wind).wind; 'isbirdsong', wavinfo(wind).issong;};
     odata={fexp(wind) oinfo};
 
     if (wind==1)
         odata={fexp(wind) [info; oinfo;explain]};
     end
     otbl=[otbl;odata];
 end
% 



onenotetable(otbl,seqfname('~/svn_trunk/notes/bsexpllike.xml'));

return;