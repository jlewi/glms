%function [stimes]=samp1dglmh(fglm,k,x,tinfo)
%   fglm - pointer to function representing the glm
%   k    - vector representing linear stage of the glm
%   x    - stimului
%           dxn
%           d- dimensionality of the data 
%           n - number of trials to sample for
%
%   tinfo - information about the response
%           .twindow - length of window for response
%  Return value
%       stimes - cell array of spike times
%              - each elemet is array containing spike times for that trial
%
%Explanation:
%   -samples the glm assuming rate is constant during the response window
%
function stimes=samp1dglmh(fglm,k,x,tinfo)
%compute the rates for these stimuli
r=glm1dexp(k,x);
numtrials=size(x,2);
%sample the poisson model 
stimes=cell(1,numtrials);
for index=1:numtrials
    stimes{index}=shpoisson(r(index),tinfo.twindow);

end

