%$Revision: 1455 $: 06-14-2007
%   switch to using GLM object
%10-17-06
%
% Test glm functions for the logistic function with canonical
% parameterization
glm=GLMModel('binom','canon');
%generate a random number to represent epsilon=theta'*stim
epsvar=rand(1);

%check link function and function to canonical eta up giving eta=epsvar
eta=glm.fglmetamu(glm.fglmmu(epsvar));

%for canonical parameterization eta and epsvar should be the same
if (abs(eta-epsvar)>10^-10)
    error('Eta~=0 epsvar : not canonical parameterization');
   
end