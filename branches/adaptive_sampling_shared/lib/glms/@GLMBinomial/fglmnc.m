%function nc=fglmnc(glm,eta)
%    eta - the value of eta
%
%Return value
%    A - the normalizing constant
%    dA - derivative of A w.r.t to eta
%    d2A -2nd derivative A. w.r.t to eta
%Explanation: The normalizing constant for the distribution
%Explanation the normalizing function A(eta) for the logistic GLM
%
%Revision History:
%07-08-2008: Made function a member of class GLMBinomial
%08-29-2007: needed a period to do squaring term by term
%08-15-2007: Numerical issue
%   My formula for dA2 was unstable. When eta has a large magnitude we end
%   up computing the difference between two nearly identical numbers. This
%   is unstable. In particular it can give the wrong sign for dA2.
%   dA2 should always be positive. By simplifying this formula we can
%   guarantee dA2 is always positive.
%$Revision$: 06-14-2007
%   Numerical issue: if eta is large, we get Inf/Inf for dA2 which leads to
%   Nan. Fixed this.
%        
%06-14-2007: Numerical issue. If eta is large log(1+exp(eta)) ends up
%   being inf. In reality its approximatly log(1+exp(eta))\approx eta when
%   eta is large
function [A, dA1, dA2]=fglmnc(glm,eta)   
    A=log(1+exp(eta));    
    dA1=exp(eta)./(1+exp(eta));
    
    %dA2=-exp(2*eta)./(1+exp(eta)).^2+exp(eta)./(1+exp(eta));
    dA2=exp(eta)./(1+exp(eta)).^2;
    %check for numerical issues
    ind=find(isinf(A));
    if ~isempty(ind)        
        %use the approximation
        %log(1+exp(eta))~eta for large eta
        A(ind)=eta(ind);
        dA1(ind)=1;
        dA2(ind)=0;
    end
    
    %we also need to check if dA2 is nan
    %this can result because exp(eta)=inf so we get inf/inf
    %dA2=exp(\eta)/(1+exp(eta))^2. THerefore if it exp(\eta) is infinity
    %then dA2=0;
    ind=find(isnan(dA2));
    if ~isempty(ind)
        dA2(ind)=0;
    end