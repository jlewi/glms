%function [mu,dmu,d2mu]=fglmmu(glm,glmproj)
%   glmproj - input to the nonlinearity
%
%Explanation: Compute the mean (output of the nonlinearity) and its
%derivatives
%
%
function [mu,dmu,d2mu]=fglmmu(glm,glmproj)

%call the link function
[mu,dmu,d2mu]=glm.fglmmuhandle(glmproj);


