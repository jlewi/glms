%function expjobs(glmproj,tglm,aglm,confint)
%       glmproj - values of \stim^t \theta for which we want to compute the
%             expectation
%       tglm      - true glm. This glm controls the distribution
%                       of r given glmporj which is the distribution over
%                       which the expectation is computed
%      aglm     - the assumed glm
%                       this is the glm which gives us the observed fisher
%                       information whose expectation we want to evaluate
%       confint - interval used to compute bounds for integration
%
%Return value:
%   expjo - value of the integral for each value of glmrpoj
%   Computes the expectation of the observed Fisher information
function expjo=expjobs(glmproj,tglm,aglm,confint)

    if ~isvector(glmproj)
        error('glmproj should be row vector');
    end
    if (size(glmproj,1)>1)
         error('glmproj should be row vector');
    end
    
    nvals=length(glmproj);      %how many values we need to evaluate it for
    expjo=zeros(1,nvals);
    %compute the mean of the sampling distribution.
    mu= tglm.fglmmu(glmproj);      
    rbounds=zeros(nvals,2);

     %get the bounds for integration
    tail=(1-confint)/2;
    rbounds(:,1)=tglm.cdfinv(tail,mu');
    rbounds(:,2)=tglm.cdfinv(1-tail,mu');
    
    %evaluate the integral for each value
if (tglm.isdiscrete==1)
%evaluate the integral for each value
for k=1:nvals
      %the responses over which we sum to compute the expectation.
      r=floor(rbounds(k,1)):1:ceil(rbounds(k,2));
      r=r';         %needs to a be a column vector for when we call d2glmeps
      %1. compute d^2p(r|epsilon)de^2 sigma
      [dlde, dlldde]=d2glmeps(glmproj(k),r,aglm);
      clear('dlde');

      %2. multiply each by the pdf
      p=tglm.pdf(r,mu(k));
      %normalize the pdf
      p=p/(sum(p));
      s=-dlldde.*p;
      s=sum(s);
      
      if isnan(s)
          warning('expjobs: sum is NaN');
      end
      expjo(k)=s;
      
      
end
else
      error('Need to write code to compute expectation when distribution is continuous');
              
end