%function ll=loglike(glm,obsrv,epsvar)
%   glm   - glmmodel object
%   obsrv - observations
%   epsvar - stim'*param
%   
%Return value:
%   ll - log likelihood up to an additive constant
%
%Explanation: computes the log likelihood.
function ll=loglike(glm,obsrv,epsvar)


% To help avoid numerical issues, we check if the nonlinearity
%is the canonical function. If it is then eta=epsvar;
if isequal(glm.fglmmuhandle,glm.linkcanon)
    eta=epsvar;
else
    [mu]=fglmmu(glm,epsvar);
    %compute the derivatives of the canonical parameter
    [eta]=fglmetamu(glm,mu);
end

%compute normalizing function
[A]=fglmnc(glm,eta);

ll=obsrv.*eta-A;