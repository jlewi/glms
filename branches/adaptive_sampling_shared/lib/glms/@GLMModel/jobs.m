%function j=jobs(glm,r,glmproj)
%   r -nx1 column vector indicating the different observations for which we
%   want to comput the observed fisher information
%   glmproj-1xm - number of values at which to evaluate the observed fisher
%   information
%
%Explanation:computes the observed fisher information as a function of the
%projection of the stimulus on the true parameter. 
function j=jobs(glm,r,glmproj)
    nvals=length(glmproj);      %how many values we need to evaluate it for
    nobsrv=length(r);
    j=zeros(nobsrv,nvals);


      %1. compute d^2p(r|epsilon)de^2 sigma
      %This is the 2nd derivative of the log likelihood w.r.t to \glmproj
      %we need to multiply by xx' to get the observed likelihood
     [dlde, j]=d2glmeps(glm,glmproj,r);
      
     j=-j;
%evaluate the integral for each value
%for k=1:nvals           
      %1. compute d^2p(r|epsilon)de^2 sigma
      %This is the 2nd derivative of the log likelihood w.r.t to \glmproj
      %we need to multiply by xx' to get the observed likelihood
 %     [dlde, j(:,k)]=d2glmeps(glmproj(k),r,glm);
  %    j(k)=-j(k);
%end
