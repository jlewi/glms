%function [er,dmu,d2mu]=compmu(obj,glmproj)
%	 obj=GLMModel object
% 
%Return value: 
%	 The expected firing rate.
%   
function varargout=compmu(obj,glmproj)
    switch nargout
        case 1
            [varargout{1}]=fglmmu(obj,glmproj);
        case 2
            [varargout{1} varargout{2}]=fglmmu(obj,glmproj);
        case 3
            
            [varargout{1} varargout{2} varargout{3}]=fglmmu(obj,glmproj);
        otherwise
            error('too many output arguments');
    end
    
