%**************************************************
%r=isequal(glm1, glm2)
%
%Explanation: returns 1 if two glm's represent the same model
function r=isequal(glm1, glm2)

    r=1;
    
    fnames=fieldnames(glm1);
    for j=1:length(fnames)
        if ~isequal(glm1.(fnames{j}),glm2.(fnames{j}))
            r=0;
        end
    end