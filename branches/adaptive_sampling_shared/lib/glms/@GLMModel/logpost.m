%function ll=logpost(glm,theta,epsvar,obsrv,prior)
%   glm   - glmmodel object
%   obsrv - observations
%   epsvar - stim'*param
%   
%Return value:
%   lpost - log posterior up to an additive constant
%       
%Explanation: computes the true log-posterior assuming a Gaussian prior
%
function lpost=logpost(glm,theta,epsvar,obsrv,prior)

%evaluate the log-likelihood
ll=loglike(glm,obsrv,epsvar);


%evaluate the log prior
dt=(theta-getm(prior));
lprior=-1/2*dt'*getinvc(prior)*dt;

lpost=sum(ll)+lprior;