%function GLMPoisson(fnonlin)
%   fnonlin - the nonlinarity or 'canon' for canonical nonlinarity
%
% Explanation:
%   instead of defining properties which store pointers to the appropriate
%   functions we define the actual functions.
%
% Revisions:
%   080921 - Add the field nbits.
classdef (ConstructOnLoad=true) GLMPoisson < GLMModel

    %**********************************************************
    %properties
    %
    %************************************************************

    properties(SetAccess=private, GetAccess=public)
  
        %we have to give a different name to version because verison is
        %already defined in the super class
        poissversion=080921;
        
        %nbits number of pits to use for arbitrary precision objects to
        %avoid overflow
        nbits=150;
    end

    methods
        function glm=GLMPoisson(varargin)
            if (nargin==0)
                return;
            end

        

            %set the base class properties
     
            glm.linkcanon=@glm1dexp;
            glm.isdiscrete=true;
         
            switch nargin
                case 1
                    if isa(varargin{1},'function_handle')
                        glm.fglmmuhandle=varargin{1};
                    elseif (ischar(varargin{1}) && strcmp(varargin{1},'canon'))
                        glm.fglmmuhandle=glm.linkcanon;
                    else
                        error('incorrect type for fglmmu');
                    end
                otherwise
                    error('too many input arguments');
            end
            
            %call the base constructor
            %this will test whether nonlinearity is onesided
            fonesided=glm.fglmonesided;
        end




    end
end