%function p=pdf(glm,obsrv,rexp)
%   obsrv- value of the observations to evaluate the probability at
%   rexp - the expected value for othe observations
%
% Return value:
%   p -probability of these observations
function p=pdf(glm,obsrv,rexp)

 p=poisspdf(obsrv,rexp);

