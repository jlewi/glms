%function obsrv=cdfinv(glm,p,rexp)
%   p - the probability
%   rexp -the expected value
%Return value
%   obsrv - value of the observations with probability p given the expected
%   value for the observations is rexp
%
%Explanation:   this allows us to compute the bounds for
%                        integration when numerically computing
%                        expectations.
function obsrv=cdfinv(glm,p,rexp)
obsrv=poissinv(p,rexp);