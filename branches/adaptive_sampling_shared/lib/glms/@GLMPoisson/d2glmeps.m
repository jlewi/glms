%function [deps, d2eps]=d2glmeps(glm, epsvar,obsrv,dwhat)
%   glm    - GLModel object
%   epsvar -post.m'*x or mu'*x
%               -this is the value of the input times theta
%               -i.e it is the input to the link funciton of the glm
%               -1xn where n is number of epsvar to compute
%       obsrv -observation
%               -mx1 where m is number of observations for which we want to
%               evaluate the derivative if dwhat='m'
%             -dwhat ='v'
%               1xm - where epsvar is 1xm as well
%
%       dwhat - 'm' or 'v' controls whether return value is a matrix or vector
%               default: 'm'
%               'v'
%                   - obsrv and epsvar must have the same dimensions
% Computes the 1st and 2nd derivative of the the glm log likelihood (deta.*dmu.*(obsrv-dA))
% w.r.t to x'*theta= input to the link function of the glm
%
%Return Value:
%   if dwhat='v'
%   if dwhat='m'
%   dfdl - derivative
%           mxn
%   [df2dl2] - 2nd derivative
%           mxn
%   the (i,j)th entry of the return matrices give the value of the second
%   derivative for obsrv(i) and epsvar(j)
%   Optimization: If dlp2dt is not required (only 1 output argument) than
%       dlp2dt is not computed
%   Note the order of derivatives is to allow this function to be used with
%   matlab's fsolve routines to determine the optimal lambda
%
%
%Revision History:
%    09-21-2008 - Created d2glmeps.m in GLMPoisson to override d2glmeps.m
%    in GLMModel. 
function [varargout]=d2glmeps(glm,epsvar,obsrv,dwhat)

if ~exist('dwhat','var')
    dwhat='m';
end
nepsvar=length(epsvar);


%************************************************************
%Special processing based on w.*(deta).^2.*(dmu).^2;hether we are computing a vector or a matrix
switch lower(dwhat)
    case 'm'
        nobsrv=size(obsrv,1);
        %make sure epsvar is row vector;
        %obsrv is col vector
        if (size(epsvar,1)>1)
            error('epsvar must be row vector');
        end
        %create a row and column of ones to use to create proper dimensions
        row1s=ones(1,nepsvar);
        col1s=ones(nobsrv,1);
        if (size(obsrv,2)>1)
            error('obsrv must be a col vector');
        end


    case 'v'
        row1s=1;
        col1s=1;
        nobsrv=length(obsrv);
        %obsrv and epsvar should both be vectors
        if ~isvector(obsrv)
            error('obsrv should be a vector');
        end
        if ~isvector(epsvar)
            error('epsvar should be a vector');
        end

        if (nobsrv~=nepsvar)
            error('to return a vector number of observations must equal number of glmproj');
        end
        %make obsrv and epsvar both row vectors
        obsrv=rowvector(obsrv);
        epsvar=rowvector(epsvar);
    otherwise
        error('unkown value for dwhat \n');
end
%******************************************
%special code to handle one sided nonlinearities for poisson model
%***********************************************
%if nonlinearity is one sided that is f(u)=0 for u<0
%then theta'*x<0  should only be zero for observations with r=0
%these points should be excluded from the computation of the gradient and
%jacobian because the likelihood for them is not changing
%so gradient is zero as  is 2nd derivative
%
%So we don't want to compute eta,A, etc.. for these values
%
if (glm.fglmonesided==1)
    %check theta'*x<0 iff  r=0
    %4-18-07
    %following code was changed (see notes on one-sided-nonlinearity)
    eind=find((epsvar<0)) ;
    oind=find((obsrv>0));
    if (~isempty(oind) & ~isempty(eind))
        %so we have some observations of r=0 when glmproj<0
        %the gradient is not well defined for these locations
        %perhaps we should return NaN instead
        error('Current estimate yields theta such that glmproj<0 for observations>0');
    end

    %remove points for which theta'*x<0 and they don't influence
    %the gradient.
    %the gradient
    ind=find(epsvar>0);
    epsvar=epsvar(ind);
    nepsvar=size(epsvar,2);
    row1s=ones(1,nepsvar);
    %4-18-2007 commented out the following code
    %obsrv=obsrv(ind);

    %if removing these entries makes epsvar empty then we just set the
    %derivatives to zero
    if isempty(epsvar)
        varargout{1}=0;
        if (nargout==2)
            varargout{2}=0;
        end
        return;
    end
    
else
    %call d2glmeps for the base class
   if (nargout==1)
      [varargout{1}]=d2glmeps@GLMModel(glm,epsvar,obsrv,dwhat);       
   elseif (nargout==2)
      [varargout{1} varargout{2}]=d2glmeps@GLMModel(glm,epsvar,obsrv,dwhat);
   end
end

