%function obsrv=sampdist(glm,rexp)
%   rexp - Expected value of the response (i.e the output of the
%   nonlinearity)
%Return value
%   obsrv - sample the distribution
%
%Explanation
function obsrv=sampdist(glm,rexp)
    obsrv=normrnd(rexp,glm.variance*ones(1,length(rexp)));