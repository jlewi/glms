%function sim=MLogistic(dim,mag)
%   dim - the length of the stimulus
%   mag - magnitude constraint on the stimulus
%
%   Explanation: No spike history terms
%
%function sim=MLogistic(klength,alength,mag)
%   klength - length of stimulus dependence
%   alength - length of spike-history dependence
%   mag     - magnitude 
%
%   ktlength - the length in time of the stimulus dependence
%              a value of 1 means it depends only on the current stimulus
%
%Revisions:
%   080502 - allow the ktlength property to be set
%   080425 - allow spike history terms
function obj=MLogistic(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'dim','mmag'};
con(1).cfun=1;

con(2).rparams={'klength','alength','mmag'};
con(2).cfun=2;
%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%

%declare the structure
obj=struct('version',080502);

alength=0;
switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'MLogistic',MParamObj());
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

glm=GLMModel('binomial','canon');
%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
if isfield(params,'ktlength')
   ktlength=params.ktlength;
else
    ktlength=1;
end
switch cind
    case 1
        pbase=MParamObj('glm',glm,'klength',params.dim,'mmag',params.mmag,'ktlength',ktlength);
    case 2
        pbase=MParamObj('glm',glm,'klength',params.klength,'alength',params.alength,'mmag',params.mmag,'ktlength',ktlength);
      otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************

%instantiate a base class if there is one

obj=class(obj,'MLogistic',pbase);





    