%function [stimcoeff shistcoeff bias]= parsetheta(mobj,theta)
%   theta - vector of parameers
%
%Return value:
%   stimcoeff - the components of theta corresponding to the stimulus
%   shistcoeff - the components of theta corresponding to the spike history
%   bias     - the bias coefficient
%
%Explanation: Divides theta into its component parts
function [stimcoeff,shistcoeff,bias]= parsetheta(mobj,theta)

nstimcoeff=getklength(mobj)*getktlength(mobj);
stimcoeff=theta(1:nstimcoeff,1);
shistcoeff=theta(nstimcoeff+1:nstimcoeff+getshistlen(mobj));
bias=theta(end);