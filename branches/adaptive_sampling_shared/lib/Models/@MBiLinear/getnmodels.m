%function getnmodels=nmodels(obj)
%	 obj=MBiLinear object
% 
%Return value: 
%	 nmodels=obj.nmodels 
%
function nmodels=getnmodels(obj)
	 nmodels=obj.nmodels;
