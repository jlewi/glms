%function formmat(obj,mat)
%   mat - parameters represented as a matrix
%
%Explanation: reshapes the parameters as a vector
function vec=formmat(obj,mat)
	vec=mat(:);
    