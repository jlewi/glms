%Constructor 1
%   glm   - general linear model
%   matdim - dimensions of the matrix
%   mmag   - stimulus magnitude
%   nmodels - how many submodels to use
%   pinit - prior on the full space of all matrices 
%   rank - what rank approximation to use of the matrix
%   submprior - prior to put on each submodel
%
%
function obj=MBiLinear(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'rank','matdim','submprior'};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%  bname  - name of base class. This allows us to refer to it templates
%declare the structure
obj=struct('version',071117,'rank',[],'matdim',[],'submprior',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'MBiLinear');
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
    
    otherwise
        error('Constructor not implemented')
end
    
%extract the relevant parameters and pass the rest onto the base class
obj.rank=params.rank;
params=rmfield(params,'rank');

obj.matdim=params.matdim;
params=rmfield(params,'matdim');

% obj.nmodels=params.nmodels;
% params=rmfield(params,'nmodels');

obj.submprior=params.submprior;
params=rmfield(params,'submprior');

%*******************************************************
%validation
%*******************************************************
if ~isa(obj.submprior,'GaussPost')
    error('submprior must be GaussPost');
end

%check dimensionality of submprior matches rank.
if (obj.rank~=getdim(obj.submprior))
    error('Dimensionality of submprior must = rank');
end

if (numel(obj.matdim)~=2)
    error('matdim must be vector of length 2');
end


%*********************************************************
%set the relevant parameters for the base class
%********************************************************
params.alength=0;
params.klength=prod(obj.matdim);
params.ktlength=1;

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=MParamObj(params);
obj=class(obj,'MBiLinear',pbase);





    