%function formmat(obj,vec)
%   vec - parameters represented as a vector
%
%Explanation: reshapes the parameters as a matrix
function mat=formmat(obj,vec)
    mat=reshape(vec,obj.matdim);
    