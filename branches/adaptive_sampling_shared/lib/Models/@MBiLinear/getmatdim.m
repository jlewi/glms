%function getmatdim=matdim(obj)
%	 obj=MBiLinear object
% 
%Return value: 
%	 matdim=obj.matdim 
%
function matdim=getmatdim(obj)
	 matdim=obj.matdim;
