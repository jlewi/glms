%function mat=formmat(obj,vec)
%   vec - vector in the vector space represented by the basis vectors
%       - i.e a vector of length obj.rank
%
%Return value:
%   mat - the matrix corresponding to vec defined in the basis
%        obj.basis
function mat=formmat(obj,vec)
    mat=zeros(obj.matdim);
    
    if (length(vec)~=getrank(obj))
        error('Vec has incorrect dimension');
    end
    for bi=1:obj.rank
       mat=mat+obj.basis(:,:,bi)*vec(bi);
    end