%function getbasis=basis(obj)
%	 obj=MBiLinearSub object
% 
%Return value: 
%	 basis=obj.basis 
%
function basis=getbasis(obj)
	 basis=obj.basis;
