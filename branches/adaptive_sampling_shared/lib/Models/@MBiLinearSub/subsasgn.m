%function obj = subsasgn(obj,index,val)

function obj = subsasgn(obj,index,val)
% SUBSASGN Define index assignment for ASResults objects
switch index(1).type
    case '()'
          %if the object to which we are assigning it is empty
        %i.e array hasn't been initialized then we just set it
        if isempty(obj)
            obj=val;
        else
            %apply recursively
            if (size(index,2)==1)
                obj(index(1).subs{1})=val;
            else
                oel=obj(index(1).subs{1});
                oel=subsasgn(oel,index(2:end),val);
                obj(index(1).subs{1})=oel;
            end
        end
   
end

