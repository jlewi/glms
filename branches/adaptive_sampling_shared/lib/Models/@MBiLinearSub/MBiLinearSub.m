%Constructor 1
%   basis - kxdxr matrix
%         -basis vectors
%   glm   - general linear model
%   mmag   - stimulus magnitude
%   pinit - prior on the models
function obj=MBLinearSub(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'basis','pinit'};
con(1).cfun=1;


%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%  matdim - 1x2 matrix gives the dimensions of the matrices
%           in this vector space
%  basis  - matdim(1)xmatdim(2)xrank - matrix
%         - these are the rank 1 matrices which form a basis of this vector
%         space
%declare the structure
obj=struct('version',071117,'basis',[],'rank',[],'matdim',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        pbase=MParamObj();
        obj=class(obj,'MBiLinearSub',pbase);
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        obj.basis=params.basis;
        obj.rank=size(obj.basis,3);
        obj.matdim=[size(obj.basis,1),size(obj.basis,2)];    
        params=rmfield(params,'basis');
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
params.alength=0;
params.klength=obj.rank;
params.ktlength=1;

pbase=MParamObj(params);
%if no base object
obj=class(obj,'MBiLinearSub',pbase);




    