%function getrank=rank(obj)
%	 obj=MBiLinearSub object
% 
%Return value: 
%	 rank=obj.rank 
%
function rank=getrank(obj)
	 rank=obj.rank;
