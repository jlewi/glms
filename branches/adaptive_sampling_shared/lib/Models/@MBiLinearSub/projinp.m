%function tinp=projinp(mobj,inp,sr)
%   mobj - mobj
%   inp  - glm input vector
%        - can be an array of inputs if want to project more than 1
%   sr   - spike response
%        - these aren't used so they can be null
%
%Return value:
%   tinp - dxlength(inp)- each column is the input
function tinp=projinp(mobj,inp,sr)

tinp=zeros(getrank(mobj),length(inp));

%reshape the basis to form vectors
bvec=reshape(mobj.basis,[prod(getmatdim(mobj)),getrank(mobj)]);

for j=1:length(inp)
tinp(:,j)=bvec'*getData(inp(j));
end

