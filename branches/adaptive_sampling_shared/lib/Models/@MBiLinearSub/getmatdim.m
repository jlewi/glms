%function getmatdim=matdim(obj)
%	 obj=MBiLinearSub object
% 
%Return value: 
%	 matdim=obj.matdim 
%
function matdim=getmatdim(obj)
	 matdim=obj.matdim;
