%function theta=smooth(mobj,theta,nfcutoff,ntcutoff)
%   theta - model parameters
%   nfcutoff - cutoff in frequency
%   ntcutoff - cutoff in time
%Return value
%   theta - a smoothed version of theta obtained by setting all components
%           of theta corresponding to frequencies greater than nfcutoff or
%           ntcutoff to zero.
%
%

function theta=smooth(mobj,theta,nfcutoff,ntcutoff)
%WARNING: This method makes assumptions about how the coefficients are
%arranged
%   A better way to do this would be to create a new ModBSSineWaves object
%   with nfreq=nfcutoff-1 and ntime=ntcutoff-1
%   We could then just use mobj.fullbasis to project this strf onto this
%   domain
%
%we need to set amplitudes for coefficients greater than or
%equal to the cutoff frequency to zero
subind=bindexinv(mobj,1:mobj.nstimcoeff);

[stimcoeff,shistcoeff,bias]=parsetheta(mobj,theta);

%find the linear indexes of those elements to set to zero
lind=[find(subind.nf>=nfcutoff) ;find(subind.nt>=ntcutoff)];
lind=sort(unique(lind));

for index=rowvector(lind)
    %set the coefficient to zero
    stimcoeff(index)=0;

    %set the covariance to zero
    %WARNING: THIS ASSUMES THE STIMULUS COEFFICIENTS ARE
    %THE FIRST ENTRIES IN THE COVARIANCE MATRIX
    %We could probably adjust this to use indstim and
    %indshist
    c(:,index)=0;
    c(index,:)=0;
end


theta=packtheta(mobj,stimcoeff,shistcoeff,bias);