%function success=testbasis()
%
%Explanation: Test the basis functions of ModBSSinewaves are orthonormal
%
function [success,obj]=testbasis(strfdim)

if ~exist('strfdim','var')
    strfdim=[8 8];
end

klength=strfdim(1);
ktlength=strfdim(2);
success=true;
nmax=ModBSSinewaves.maxn([klength ktlength]);

nfreq=nmax(1);
ntime=nmax(2);

obj=ModBSSinewaves('nfreq',nfreq,'ntime',ntime,'klength',klength,'ktlength',ktlength,'mmag',1);


%***********************************************************************
%Check that bindex correctly maps the basis vectors to the proper column of
%basis
%***********************************************************************
% fprintf('Testing bindex.m \n');
% 
% for nf=0:nfreq
%     for nt=0:ntime
%         %compute the appropriate basis vector
%     end
% end

%***********************************************************************
%Sum to zero
%***********************************************************************
%all of the basis vectors except the first basis vector corresponding to
%the constant term should sum to zero
%determine which vector is the constant term
iconst=bindex(obj,0,0);
iconst=iconst(2);
s=sum(obj.basis(:,[1:iconst-1 iconst+1:obj.nstimcoeff]),1);
s=abs(s);
if (any(s>10^-8))
    error('Not all basis vectors integrate to 1.');
end
%check basis is orthonormal
mags=obj.basis'*obj.basis;

trueval=eye(obj.nstimcoeff);
d=abs(mags-trueval);

if (any(d(:)>10^-8))
    error('basis is not orthonormal');
end

%********************************************************************
%make sure tosindom and tospecdom return analgous answers
%********************************************************************

% %handle the zero frequency
% bf1=obj.basis.cos{1,1};
% mag=(bf1).^2;
% mag=sum(mag(:))^.5;
% 
% if (abs(mag-1)>10^-8)
%     error('Magnitude of the 0 frequency cosine term is not normalized.');
%     success=false;
% end
% 
% 
% for nf2=2:nfreq
%     for nt2=2:ntime
% 
%         for b2=1:2
% 
%             if (b2==1)
% 
%                 bf2=obj.basis.sin{nf2,nt2};
%             else
%                 bf2=obj.basis.cos{nf2,nt2};
%             end
%             mag=(bf1.*bf2);
%             mag=sum(mag(:));
%             mag=(mag^2)^.5;
% 
% 
%             if (abs(mag)>10^-8)
%                 error('The basis vectors are not orthonormal');
%                 success=false;
%             end
% 
%         end
%     end
% end
% %skip the zero frequency
% for nf1=2:nfreq
%     for nt1=2:ntime
%         for b1=1:2
%             if (b1==1)
% 
%                 bf1=obj.basis.sin{nf1,nt1};
%             else
%                 bf1=obj.basis.cos{nf1,nt1};
%             end
% 
%             %make sure its normalized
%             mag=(bf1).^2;
%             mag=sum(mag(:));
% 
%             if (abs(mag-1)>10^-8)
%                 error('The basis vector is not normalized');
%                 success=false;
%             end
% 
%             for b2=1:2
%                 if (b2==b1)
%                     startf=nf1+1;
%                     startt=nt1+1;
%                 else
%                     startf=nf1;
%                     startt=nt1;
%                 end
% 
%                 for nf2=startf:nfreq
%                     for nt2=startt:ntime
% 
% 
%                         if (b2==1)
% 
%                             bf2=obj.basis.sin{nf2,nt2};
%                         else
%                             bf2=obj.basis.cos{nf2,nt2};
%                         end
% 
% 
%                         mag=(bf1.*bf2);
%                         mag=sum(mag(:));
%                         mag=(mag^2)^.5;
% 
% 
%                         if (abs(mag)>10^-8)
%                             error('The basis vectors are not orthonormal');
%                             success=false;
%                         end
%                     end
%                 end
%             end
%         end
%     end
% end

if (success)
    fprintf('Success: Basis vectors are orthonormal.\n') ;
end
