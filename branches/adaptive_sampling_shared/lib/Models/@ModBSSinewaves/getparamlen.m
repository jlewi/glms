%function d=getparamlen(mobj)
%   mobj -MParamObj
%
%Return value
%   d - length of the actual parameters
%
%Explanation: Returns the number of parameters used to actually represent
%theta
function d=getparamlen(mobj)

    %we multiply by 2 because for each frequency we have a sinewave and a
    %cosine function.    
    %We add 1 for the constant term
    d=mobj.nstimcoeff;
    if (hasbias(mobj))
        d=d+1;
    end
    
    d=d+mobj.alength;