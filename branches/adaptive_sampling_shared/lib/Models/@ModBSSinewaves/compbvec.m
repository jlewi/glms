%function [bmat]=compbvec(mobj,nf,nt)
%   nf - value of nf
%   nt - value of nt
%Return value:
%   bmat - matrix of the basis vectors for this frequency
%        - if nf=0 and nt=0 then there is only 1 column
%        -otherwise the first column corresponds to the sinewave and the
%        second column to the cosine
%
%Revisions:
%   10-24-2008:
%         The rows and columns should really start at 0 and go to
%         ktlength-1
%         and klengt-1
function [bmat]=compbvec(obj,nf,nt)

 [col,row]=meshgrid([0:obj.ktlength-1],[0:obj.klength-1]);
 fr=obj.fffreq*nf*row;
 fc=obj.fftime*nt*col;
 
 
 
 bsin=sin(2*pi*(fr+fc));
 bcos=cos(2*pi*(fr+fc));
 
 if (nf==0 && nt==0)
     bmat=[bcos(:)];
 else
     bmat=[bsin(:) bcos(:)];
 end
 
 bmat=normmag(bmat);
 