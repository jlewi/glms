%function nstimcoeff=getnstimcoeff(mobj)
%   
%Return value:
%   nstimcoeff - the number of terms in theta which correspond to the
%   stimulus (as opposed to bias and spike history)
%   
%   Using a function allows us to override how it is computed in derived
%   classes
%
function [nstimcoeff, varargout]=getnstimcoeff(mobj)


%we have 2 coefficients 1 for the sin and cosine terms
            %we subtract 1 because we there is no bassi function for the
            %sine wave for the zero frequency
             nstimcoeff=2*(mobj.nfreq+1)*(mobj.ntime+1)-1;
 if (nargout==2)
       varargout{1}=getnstimcoeff@MParamObj(mobj); 
    end