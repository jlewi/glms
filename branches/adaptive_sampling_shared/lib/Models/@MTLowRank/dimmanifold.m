%function mdim=dimmanifold(mobj)
%   mobj - the MTLowRank object
%
%Return Value:
%   The dimensionality of the tangent space, (i.e the low rank matrix
%   approximation)
function mdim=dimmanifold(mobj)

%the dimensionality of the manifold is however many
 %terms we need to describe the matrix + the number of components in
%theta which are not in the matrix

dlowrank=(mobj.ktlength+mobj.klength)*mobj.rank+mobj.rank;

dother=mobj.alength+mobj.hasbias;
mdim=dlowrank+dother;