%function ptheta=proj(obj,theta)
%   theta- value of theta
%
%Return value:
%   ptheta - projection of theta onto the submanifold of rank r matrices
%          - this is a vector, use vectouv to convert vector to matrices
%          u,v
%           - the projection is returned in the space of the submanifold
function ptheta=proj(mobj,theta)

[stimcoeff, shistcoeff,bias]=parsetheta(mobj,theta);
m=reshape(stimcoeff,[mobj.klength,mobj.ktlength]);

[u,s,v]=svd(m);
s=diag(s);

%select first rank components 
u=u(:,1:mobj.rank);
s=s(1:mobj.rank);
v=v(:,1:mobj.rank);

%now convert this tp a vector
ptheta=uvtovec(mobj,u,s,v,shistcoeff,bias);