%function MTanSpace('rank')
%   rank - what rank to use for the tangent space approximation
%
% Additional Required parameters: see constructor for MParamObj
%
%Explanation: This constructs a model which uses a tangent space
%   to reduce the dimensionality of the parameter space
%
%   This model only constructs a low rank approximation of the
%   STRF not of the spike history or bias terms.
classdef (ConstructOnLoad=true) MTLowRank < MTanSpace
    %****************************************************
    %version - version of the class definition
    %rank  - What rank to use for the tangent space approximation
    %*********************************************
    properties(SetAccess=private,GetAccess=public)
        tversion=080730;
        rank=[];
    end

    methods

        function mobj=MTLowRank(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};
      
            
            con(2).rparams={'rank'};
      

            
            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    %we don't return because  we can't issue return befor
                    %the call to the base class constructor
                    params=[];
                    cind=1;
                otherwise
                    
                %determine the constructor given the input parameters
                [cind,params]=constructid(varargin,con);
            end


            %**************************************************************
            %Base Class: Constructor
            %*************************************************************
            %remove paramters for this object and construct the base class
            if ~isempty(params)
            sparams=rmfield(params,'rank');
            else
                sparams=[];
            end
            mobj=mobj@MTanSpace(sparams);

            
            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************

            switch cind
                case 1
                    %use by loadobj
                    return;
                case 2
                    mobj.rank=params.rank;
                otherwise
                                          error('Constructor not implemented')
                  
            end

        end
    end

    methods(Access=public)
        %function m=uvtovec(obj,u,s,v,shist,bias)
        %   u,s,v - are the matrices making up the rank r approximation of m
        %         we reshape these a vector, so that they can be stored as vector
        %         parameters of the manifold
        %   shist - the spike history terms
        %   bias  - bias term
        function m=uvtovec(obj,u,s,v,shist,bias)

            m=[s(:);u(:);v(:);shist;bias];

        end

        %function [u,s,v,bias]=vectouv(obj,sp)
        %   sp - is a vector representing the matrices u,s,v, the spike
        %   history terms and bias
        %       we return the matrices u,v
        %   s is a vector of singular values. These are stored in first rank elements of sp
        %   then come the matrices u and v
        function [u,s,v,shist,bias]=vectouv(obj,sp)

            rank=obj.rank;
            mdim=[obj.klength obj.ktlength];

            s=sp(1:rank);

            sind=rank+1;
            eind=sind+mdim(1)*rank-1;
            u=reshape(sp(sind:eind),[mdim(1),rank]);

            sind=eind+1;
            eind=sind+mdim(2)*rank-1;
            v=reshape(sp(sind:eind),[mdim(2),rank]);

            %get the spike history terms
            if (obj.alength>0)
                sind=eind+1;
                eind=sind+obj.alength-1;
                shist=sp(sind:eind);
            else
                shist=[];
            end
            if (obj.hasbias)
                bias=sp(end);
            else
                bias=[];
            end
        end
    end
end