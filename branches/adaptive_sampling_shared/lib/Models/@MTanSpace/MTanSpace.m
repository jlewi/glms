%function ModelTanSpace()
%
%
% Additional Required parameters: see constructor for MParamObj
%
%Explanation: This constructs a model which uses a tangent space
%   to reduce the dimensionality of the parameter space
%
%Revisions
classdef (ConstructOnLoad=true) MTanSpace < MParamObj
    %****************************************************
    %version - version of the class definition
    %tspace  - An object defining the tangent space for this model
    %*********************************************
    properties(SetAccess=private,GetAccess=public)
       version=080730; 
    end
    
    methods (Abstract)
       mdim=dimmanifold(obj); 
       %comptanpoint not only projects theta onto the manifold but also
       %computes the basis
       tspace=comptanpoint(mobj,theta);
       basis=gradsubmanifold(mobj,manifoldp);
       theta=mantotheta(mobj,manifoldp);
       manifoldp=proj(mobj,theta);
       
    end
   methods
       
       function mobj=MTanSpace(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};
            con(1).cfun=1;


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    params=[];
                    cind=0;
                otherwise
                    
               %determine the constructor given the input parameters
               [cind,params]=constructid(varargin,con);
            end


            %**************************************************************
            %Base Class: Constructor
            %*************************************************************
            %remove paramters for this object and construct the base class

            mobj=mobj@MParamObj(params);
            
            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************

            switch cind
                case 0
                    return;
                case 1
                    return;              
                otherwise
%                    error('Constructor not implemented')
            end

        end 
   end
end