%function r=compexpr(mobj, stim,theta,sr)
%       stim - the input - vector of stimulus and spike history
%                    concatenated together
%       theta - parameters for the model
%       sr     - past responses
%
% Return value:
%       r - the expected value of the observations computed under the model
%
% Explanation: Computes the expected value of the observation for input
%   stim and model parameters theta.
%
% Revision History
%   10-29-2007 - take past spikes as input as well.
function r=compexpr(mobj,stim,theta,sr)
    %transform the input via the nonlinear transformation
   % ninp=mobj.ninpfunc(stim,mobj.fparam);
 ninp=projinp(getninpobj(mobj),stim);
    %3-19-07
    %syntax mobj.glm isn't working I don't know why
    %work around:
    glm=getglm(mobj);
    r=compmu(glm,ninp'*theta);
