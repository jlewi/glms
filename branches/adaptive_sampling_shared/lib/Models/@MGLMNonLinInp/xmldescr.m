%function node =xmldescr(sobj,docnode)
%   docnode - document object 
%
% Return value
%   node - node used to represent this object
%Explanation create an xml description for this object
function simparnode=xmldescr(sobj,docnode)
    bname='MParamObj';
    %call it on base class
    simparnode=xmldescr(sobj.(bname),docnode);
    
    %we need to get the child node associated with the object
    %simparnode.getch
    %add additional fields
    onode=simparnode.getLastChild;
    
    %make sure this onode is the right one
    %check Name is PoolStimQuad
    oStruct = makeStructFromNode(onode);

    %class will be set to the base class
    %we need to change that
    if ~(strcmp(oStruct.Attributes.Value,bname))       
        error('could not find appropriate node for this object');
    end
    
    %change the name of the class
    
    onode.setAttribute('name',class(sobj));



    %*************************************************
    %additional parameters
    oparam=docnode.createElement('simparam');
    oparam.setAttribute('name','stimlen');
    oparam.setTextContent(num2str(sobj.stimlen));
    
    onode.appendChild(oparam);
    
%     
%     if ~isempty(sobj.Poolstim.srescale);
%     oparam=docnode.createElement('simparam');
%     oparam.setAttribute('name','sfactor');
%     oparam.setTextContent(num2str(sobj.Poolstim.srescale.sfactor));
%     onode.appendChild(oparam);
%     end
%      oparam=docnode.createElement('simparam');
%     oparam.setAttribute('name','resample');
%     oparam.setTextContent(num2str(sobj.Poolstim.resample));
%     onode.appendChild(oparam);
%     
%      oparam=docnode.createElement('simparam');
%     oparam.setAttribute('name','infomax');
%     if (sobj.Poolstim.infomax)
%         oparam.setTextContent('true');
%     else
%         oparam.setTextContent('false');
%     end
%     onode.appendChild(oparam);
    
    