%function nstimcoeff=getnstimcoeff(mobj)
%   
%Return value:
%   nstimcoeff - the number of terms in theta which correspond to the
%   stimulus (as opposed to bias and spike history)
%   
%   Using a function allows us to override how it is computed in derived
%   classes
%
function nstimcoeff=getnstimcoeff(mobj)

nstimcoeff=mobj.klength*mobj.ktlength;