%%function glmproj=compglmproj(mobj, stim,theta,sr)
%       stim - the input - vector of stimulus and spike history
%                    concatenated together
%       theta - parameters for the model
%       sr    - stimulus and spike history
% Return value:
%   glmproj - this is the output of the linearity in the GLM
%
% Explanation: Computes the expected value of the observation for input
%   stim and model parameters theta.
%
function glmproj=compglmproj(mobj,stim,theta,sr)


    glmproj=projinp(mobj,stim,sr)'*theta;
