%function m=getmag(mobj)
%   
%Explanation: gets the magnitude. This is faster than using subsref
function m=getmag(mobj)
m=mobj.mmag;
