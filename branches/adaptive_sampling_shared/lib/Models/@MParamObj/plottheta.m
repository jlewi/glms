%function fh=plottheta(mobj,theta)
%   theta - fourier coefficients or a structure containing the amplitudes
%   x     - lbls for the columns
%   y     - lbls for the rows
%   twidth - the amount of time associated with each time bin
%          - allows us to compute the appropriate interval for the spike
%          history
%
%Explanation: Plots the fourier coefficinets
function fstrf=plottheta(mobj,theta,x,y,twidth)


if (mobj.alength>0)
nrows=2;
else
    nrows=1;
end

if ~exist('x','var')
    x=[];
end
if ~exist('y','var')
    y=[];
end

if ~exist('twidth','var')
    twidth=1;
end

fstrf=FigObj('name','STRF','width',5,'height',8,'xlabel','time(s)','ylabel','Frequenzy(hz)','title','STRF','naxes',[nrows,1]);



    %*************************************************************
    %plot the STRF
    %***************************************************************
        %get rid of the bias term
        [stimcoeff shistcoeff bias]=parsetheta(mobj,theta);
        strf=getstrf(mobj,theta);


        if isempty(x)
            x=1:size(strf,2);
        end
        
        if isempty(y)
            y=1:size(strf,1);
        end
        
        setfocus(fstrf.a,1,1);
        
        imagesc(x,y,strf);
        hc=colorbar;
        sethc(fstrf.a(1,1),hc);
        title(fstrf.a(1,1),'STRF');
        xlim([x(1) x(end)]);
        ylim([y(1) y(end)]);


        %spike history coefficients
        row=nrows;
        if (getshistlen(mobj)>0)
            setfocus(fstrf.a,row,1);
            t=-twidth*[getshistlen(mobj):-1:1];
            hp=plot(t,shistcoeff);
            pstyle.marker='.';
            pstyle.markerfacecolor='b';
            addplot(fstrf.a(row,1),'hp',hp,'pstyle',pstyle);
            xlabel(fstrf.a(row,1),'time(s)');
            title(fstrf.a(row,1),'spike history coefficients');
            xlim([t(1) t(end)]);
        end

        lblgraph(fstrf);

        %set the heights of the plot
        %make spike history 1/4 the height of the strf plots
        heights=ones(1,nrows);

        if (getshistlen(mobj)>0)
            heights(end)=heights(1)/4;
            heights=heights/sum(heights);
        end
        sizesubplots(fstrf,[],[],heights);

    end
    