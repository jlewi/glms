%function convertoldver(mobj,'glm',glm)
%   mobj
%   glm - glm to assign to this object
%
%Explanation: This function is intended to be used to handle conversion of
%old versions of MParamObj to the new version.
%Thus it allows the GLM field to be set as in the old version this was not
%a field of mparamobj.
function mobj=convertoldver(mobj,varargin)

for j=1:2:length(varargin)
    switch lower(varargin{j})
        case {'glm'}
            mobj.glm=varargin{j+1};
    end
end
