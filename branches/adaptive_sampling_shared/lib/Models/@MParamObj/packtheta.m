%function [theta= parsetheta(mobj,stimcoeff shistcoeff bias)
%   theta - vector of parameers
%
%Return value:
%   theta
%Explanation: Packs the different terms of theta into a vector
%
%05-26-2008
%   take into account ktlength when returning theta
function theta= packtheta(mobj,stimcoeff,shistcoeff,bias)

%use the packinput function.
theta=packinput(mobj,stimcoeff,shistcoeff,bias);