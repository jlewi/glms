%function [theta= parsetheta(mobj,stimcoeff shistcoeff bias)
%   theta - vector of parameers
%
%Return value:
%   theta
%Explanation: Packs the different terms of theta into a vector
%  This function just calls pack theta.
%   However we create a sepearte function because for derived objects
%   the input and theta may not be in the same spaces
%05-26-2008
%   take into account ktlength when returning theta
function ivec= packinput(mobj,stimcoeff,shistcoeff,bias)

ivec=[stimcoeff;shistcoeff;bias];

