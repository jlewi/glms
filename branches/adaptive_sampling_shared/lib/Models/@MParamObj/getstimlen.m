%function d=getstimlen(mobj)
%   mobj -MParamObj
%
%Return value
%   d - length of the actual stimulus
%
%Explanation: Returns the length of the actual stimulus. Because of input
%nonlinearities this can be different from the actual length of the input
%to the GLM.
function d=getstimlen(mobj)
 d=mobj.klength*mobj.ktlength+mobj.alength;

    if (hasbias(mobj))
        d=d+1;
    end