%function klength=getklength(obj)
%	 obj=MParamObj object
% 
%Return value: 
%	 klength=obj.klength 
%
function klength=getklength(obj)
	 klength=obj.klength;
