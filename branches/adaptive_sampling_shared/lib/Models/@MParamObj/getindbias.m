%function indstim=getindbias(mobj)
%
%Return value
%   indstim=1x1 matrix containing the element of theta corresponding to the
%   bias
%
function indbias=getindbias(mobj)
        %use nstimcoeff for this function not that of any derived class
        if mobj.hasbias
        indbias=getparamlen(mobj);
        else
            indbias=nan;
        end


 end