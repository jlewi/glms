%function hb=hasbias(mobj)
%
% Return value
%   hb - true if model includes a bias term otherwise false
function hb=hasbias(mobj)
hb=mobj.hasbias;