%function strf=getstrf(mobj,theta)
%   theta - vector of parameters
%Return value:
%   strf - return the strf shaped as a matrix
function strf=getstrf(mobj,theta)

[stimcoeff]=parsetheta(mobj,theta);

strf=reshape(stimcoeff,mobj.klength,mobj.ktlength);