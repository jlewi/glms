%function [indbias, bindbias]=getindbias(mobj)
%
%Return value
%   indstim=1x1 matrix containing the element of theta corresponding to the
%   bias
%
%   bindbias - the bias from the base class
function [indbias]=getindbias(mobj)

%use nstimcoeff for this function not that of any derived class
indbias=getparamlen(mobj);

end