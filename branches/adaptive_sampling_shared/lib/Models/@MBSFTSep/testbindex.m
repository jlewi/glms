%function testbindex()
%
%Explanation: Test function to verify bindex and bindexinv are consistent
function success=testbindex()
success=true;
%test bindex and bindex inv when we use all possible basis vectors
tsuccess=testuseall();

if ~(tsuccess)
    success=false;
end


%test bindex and bindex inv when only a subset of the basis vectors are
%included
tsuccess=testusesubset();

if ~(tsuccess)
    success=false;
end

%test bindex and bindex inv when we use all possible basis vectors
function success=testusesubset()
success=true;
klength=40;
ktlength=10;

strfdim=[klength ktlength];
alength=0;
hasbias=0;

glm=GLMPoisson('canon');

nmax=MBSFTSep.maxn([klength,ktlength]);

maxbvecs=4*nmax(1)*nmax(2)+2*nmax(2)+2*nmax(1)+1;

%randomly decide which basis vectors use
nvecs=ceil(rand(1,1)*maxbvecs);
btouse=randperm(maxbvecs);
btouse=btouse(1:nvecs);
mobj=MBSFTSep('btouse',btouse,'glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',hasbias);


%generate the linear indexes
lind=1:mobj.nstimcoeff;

%generate mutliple indexes
subind=bindexinv(mobj,lind);

%run some checks on the results of subind
if any(subind(:,1)>mobj.nfreq)
    error('bindexinv returned nf > mobj.nfreq ');
    success=false;
end


if any(subind(:,2)>mobj.ntime)
    error('bindexinv returned nf > mobj.ntime ');
    success=false;
end

if (any(subind(:,3)<0) || any (subind(:,3)>4))
    error('bindexinv returned basis function that is out of bounds ');
    success=false;
end

%do some initial check on bindex
bsuccess=checkbindex(mobj);

if ~(bsuccess)
    success=false;
end

%verify bindexinv
for l=1:mobj.nstimcoeff
    %map this mupltiple indexes to a linear index
    %lincheck is 1x4 array with each column corresponding to a different
    %basis function
    lincheck=bindex(mobj,subind(l,1),subind(l,2));

    %subind(:,3) tells us which basis vector and hence column of lincheck
    %that we want
    ltocheck=lincheck(subind(l,3));

    if (ltocheck~=lind(l));
        error('bindexinv and bindex are not consistent');
        success=false;
    end

end



if (success)
    fprintf('Success: bindex and bindexinv correctly handling the mappings when we use a random subset of all possible basis vectors. \n');
else
    error('bindexinv and bindex are not consistent');
end


%test bindex and bindex inv when we use all possible basis vectors
function success=testuseall()
success=true;
klength=40;
ktlength=10;

strfdim=[klength ktlength];
alength=0;
hasbias=0;

glm=GLMPoisson('canon');

nmax=MBSFTSep.maxn([klength,ktlength]);

mobj=MBSFTSep('nfreq',nmax(1),'ntime',nmax(2),'glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',hasbias);


%generate the linear indexes
lind=1:mobj.nstimcoeff;

%generate mutliple indexes
subind=bindexinv(mobj,lind);

%run some checks on the results of subind
if any(subind(:,1)>mobj.nfreq)
    error('bindexinv returned nf > mobj.nfreq ');
    success=false;
end


if any(subind(:,2)>mobj.ntime)
    error('bindexinv returned nf > mobj.ntime ');
    success=false;
end

if (any(subind(:,3)<0) || any (subind(:,3)>4))
    error('bindexinv returned basis function that is out of bounds ');
    success=false;
end

%do some initial check on bindex
bsuccess=checkbindex(mobj);

if ~(bsuccess)
    success=false;
end

%verify bindexinv
for l=1:mobj.nstimcoeff
    %map this mupltiple indexes to a linear index
    %lincheck is 1x4 array with each column corresponding to a different
    %basis function
    lincheck=bindex(mobj,subind(l,1),subind(l,2));

    %subind(:,3) tells us which basis vector and hence column of lincheck
    %that we want
    ltocheck=lincheck(subind(l,3));

    if (ltocheck~=lind(l));
        error('bindexinv and bindex are not consistent');
        success=false;
    end

end



if (success)
    fprintf('Success: bindex and bindexinv correctly handling the mappings when all basis vectors are used. \n');
else
    error('bindexinv and bindex are not consistent');
end

function success=checkbindex(mobj)
success=true;
%************************************************************
%additional checks
%map all pairs i=0:nfreq and j=0:ntime to the corresponding linear index
%make sure the linear indexes return are unique and there nstimcoeff of
%them
%**************************************************************
[nf, nt]=ind2sub([mobj.nfreq+1 mobj.ntime+1],1:(mobj.nfreq+1)*(mobj.ntime+1));
nf=nf-1;
nt=nt-1;


allind=bindex(mobj,nf,nt);
allind=allind(:);
allind=allind(~isnan(allind));

if (length(allind)~=mobj.nstimcoeff)
    error('when we applied bind to all (nf,nt) pairs the correct number of stimulus coefficients was not returned') ;
    success=false;
end

if (length(allind)~=length(unique(allind)))
    error('The indexes returned by bindex for all pairs (nf,nt) are not unique');
    success=true;
end






