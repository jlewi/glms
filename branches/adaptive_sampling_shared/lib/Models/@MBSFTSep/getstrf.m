%function strf=getstrf(mobj,theta)
%   theta - vector of parameters
%Return value:
%   strf - return the strf shaped as a matrix
function strf=getstrf(mobj,theta)

[bcoeff]=parsetheta(mobj,theta);

strf=tospecdom(mobj,bcoeff);