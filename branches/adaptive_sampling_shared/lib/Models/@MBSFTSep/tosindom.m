%function bcoeff=tosindom(mobj,strf)
%
%       strf - STRF in the spectral domain
%
%Explanation: Compute the projections of the strf on the basis
%   This is mainly a helper function. The only time we probably need to
%   use this is when deciding how many frequencies to include to allow us
%   to represent an STRF well.
%
% Revisions
%   10-11-2008 - Allow STRF to be a vector
%
function bcoeff=tosindom(mobj,strf)


if isvector(strf)
    if (numel(strf)~=mobj.klength*mobj.ktlength)
        error('STRF does not have enough elements');
    end
else
    if (any(size(strf)~=[mobj.klength, mobj.ktlength]))
        error('Dimensions of STRF are not correct');
    end
end

bcoeff=mobj.basis'*strf(:);

