%function mdim=dimmanifold(mobj)
%   mobj - the MTLowRank object
%
%Return Value:
%   The dimensionality of the tangent space, (i.e the low rank matrix
%   approximation)
function mdim=dimmanifold(mobj)



dlowrank=mobj.rank;

dother=mobj.alength+mobj.hasbias;
mdim=dlowrank+dother;