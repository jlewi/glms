%function ptheta=proj(obj,theta)
%   theta- value of theta
%        - theta is the fourier coefficients as well as the spike history
%        and bias
%
%
%Return value:
%   ptheta - projection of theta onto the submanifold of rank r matrices
%          - this is a vector, use vectouv to convert vector to matrices
%          u,v
%           - the projection is returned in the space of the submanifold
function ptheta=proj(mobj,theta)

[stimcoeff, shistcoeff,bias]=parsetheta(mobj,theta);
m=reshape(stimcoeff,[mobj.klength,mobj.ktlength]);

strf=getstrf(mobj,theta);

%*********************************************************
%compute the eigenvectors of mobj.tbasis strf' strf tbasis'
[u,s,v]=svd(mobj.tbasis'*strf'*strf*mobj.tbasis);
s=diag(s).^.5;


%now we compute u
u=mobj.fbasis'*strf*mobj.tbasis*v*diag(s);

%select first rank components 
u=u(:,1:mobj.rank);
s=s(1:mobj.rank);
v=v(:,1:mobj.rank);

%now convert this tp a vector
ptheta=uvtovec(mobj,u,s,v,shistcoeff,bias);