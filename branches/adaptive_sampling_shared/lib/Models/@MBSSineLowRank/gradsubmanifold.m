%function grad=gradsubmanifold(obj,manifoldp)
%   obj - child of TanSpaceBase 
%   sparam - parameters of the submanifold on which to evaluate the
%   gradient
%   
%Return value:
%   grad = dim(theta) x nb
%       gradient of theta with respect to the submanifold
%       nb is the number of vectors needed to form an orthonormal basis
%Explantion: Computes the jacobian of theta w.r.t to the parameters of the
%submanifold (i.e of the gabor function).
%   
%
%Revisions:
function grad=gradsubmanifold(obj,gp)

%******************************************************************
%compute the basis ignoring spike history terms and bias terms
%
%We start by computing the derivatives of the STRF w.r.t to each parameter
%of the manifold. We then compute the derivative of each fourier coefficent
%by taking the dot product of d SRTF/ dManifold param with the basis
%function for each fourier coefficient
%******************************************************************
%dimension of the matrix
mdim=[obj.klength obj.ktlength];

%rank of the matrix
rank=obj.rank;

%form the matrices of the derivative
%number of partial derivatives
npartial=dimmanifold(obj)-obj.alength-obj.hasbias;
dmat=zeros(obj.nstimcoeff,npartial);

[u,s,v,shist,bias]=vectouv(obj,gp);
%derivatives with respect to elements of u
dind=0;

diags=diag(s);

du=zeros(size(obj.fbasis,2),rank);
for i=1:size(obj.fbasis,2)
    for j=1:rank       
        dind=dind+1;
        
        %put a 1 in entry (i,j) of du
        du(i,j)=1;
        dstrf=obj.fbasis*(du*diags*v')*obj.tbasis';
        
        %project theta into the sin domain
        dmat(:,dind)=tosindom(obj,dstrf);
        
        %zero out the entry in du
        %we do this because we want to avoid allocating memory for du on
        %each trial
        du(i,j)=0;
    end
end

%derivatives with respect to elements of v
dv=zeros(size(obj.tbasis,2),rank);
for i=1:size(obj.tbasis,2);
    for j=1:rank       
        dind=dind+1;
        
        %put a 1 in entry (i,j) of dv
        dv(i,j)=1;
        dstrf=obj.fbasis*(u*diags*v')*obj.tbasis';

        %project theta into the sin domain
        dmat(:,dind)=tosindom(obj,dstrf);
        
        %zero out the entry in du
        %we do this because we want to avoid allocating memory for du on
        %each trial
        dv(i,j)=0;
    end
end

%derivatives with respect to elements of s
for j=1:rank
   dind=dind+1;
    dstrf=obj.fbasis*u(:,j)*v(:,j)'*obj.tbasis';
   
    %project theta into the sin domain
    dmat(:,dind)=tosindom(obj,dstrf);
end


%**************************************************************************
%incoporate bias and spike history
%*************************************************************************
%for the spike history and bias terms the gradient is just the identity
%matrix because the manifold preserves those dimensions without any
%reduction
nextra=obj.alength+obj.hasbias;
dextra=eye(nextra);

dmat=[dmat zeros(dimstim,nextra); zeros(nextra,npartial) dextra];

%form an orthonormal basis
grad=orth(dmat);