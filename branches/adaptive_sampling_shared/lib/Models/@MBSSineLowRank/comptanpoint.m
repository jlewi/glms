%function tp=comptanspace(mobj,theta)
%   mobj - model object
%   theta - theta parameters
%
%Return value:
%   tp - the point on the tangent space evaluated at theta
%
function tp=comptanpoint(mobj,theta)


%project theta onto the manifold
manifoldp=proj(mobj,theta);

%compute the value of theta at this point
tonman=mantotheta(mobj,manifoldp);

%compute the basis at this point
basis=gradsubmanifold(mobj,tonman);


tp=TangentPoint(tonman,manifoldp,basis);

