%function x=projinpquad(stim)
%   stim=dxn
%            -each column is a different stimulus
%   param - not used by this input but used so that signature matches that
%             of other functions
%Explanation: Projects the input into the higher dimensional space in which
%a quadratic function (no linear term) is linear with the input
%
%6-01-2007: modify the projection so as to enforce symmetry of the Qmatrix
%
%Warning: If you change how x is represented you will interfere with other
%code
function x=projinpquad(stim,param)

error('06-01-2007: depreciated use QuadNonLinInp instead');
d=size(stim,1);
nstim=size(stim,2);


%x=zeros(d^2,nstim);
x=zeros(d/2+d^2/2,nstim);

%first d components are just the stimulus terms squared
x(1:d,:)=stim.^2;

%the rest of the components are the upper triangular matrix of stim*stim'
% for j=1:nstim
%     s=stim(:,j)*stim(:,j)';
%     x(d+1:end,j)=s(:);
% end

% %to do this quickly for large numbers of stim
% %we take the matrix nstim and multiply by a shifted version of itself
% sind=d+1;
% for dshift=1:d-1
% %    dshift
% %    stim(dshift+1:end,:)
% %    stim(1:(end-dshift),:)
%    x(sind:sind+(d-dshift)-1,:)=2*stim(dshift+1:end,:).*stim(1:(end-dshift),:);
%    sind=sind+(d-dshift);
% end

%construct the rest of the terms 
%we basically need the upper triangular matrix
%of x*x'
%we multiply by 2 because we are only taking the upper triangular part
sind=d+1;
for r=1:d
    %number of terms
    nt=d-r;
    lind=sind+nt-1;
   x(sind:lind,:)=2*(ones(nt,1)*stim(r,:)).*stim(r+1:end,:);
   sind=lind+1;
end