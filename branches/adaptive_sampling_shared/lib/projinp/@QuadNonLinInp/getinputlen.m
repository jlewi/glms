%function inputlen=getinputlen(obj)
%	 obj=QuadNonLinInp object
% 
%Return value: 
%	 stimlen=this is the dimensionality of the input that is being
%	 projected
%
function stimlen=getinputlen(obj)
	 stimlen=obj.d;
