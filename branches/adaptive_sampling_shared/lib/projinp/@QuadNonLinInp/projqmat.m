%function theta=projqmat(nobj,qmat)
%   nobj = the object responsible for projecting the input into the higher
%                dimension
%  qmat - the quadratic matrix
%Explanation: This returns theta such that
%   theta'*projinp(x)=x'*qmat*x;
%
%Therefore theta are the parameters our algorithm will try to estimate.
%
%Warning: If you change how x is represented you need to change the 
%   other functions of this class: qmatquad, projparam
%   use the test function to make sure they are all consistent
%
%   1-20-2007 - The TanSpace object QuadInpTanSpace also makes assumptions
%   about how this function is rehasping things so becareful.
function theta=projqmat(nobj,qmat)
d=nobj.d;
if (d~=size(qmat,1))
    error('This QuadNonLin was initialized with a size that does not match stim');
end



%x=zeros(d^2,nstim);
theta=zeros(d/2+d^2/2,1);

%first d components are just the diagonal terms
theta(1:d,:)=diag(qmat);

%construct the rest of the terms 
%we basically need the upper triangular matrix
%of q
%we take the elements going rowise but only taking those elements above the
%diagonal
%we do not need to multiply by 2 because 
%the factor of 2 is included in projinp 
sind=d+1;
for r=1:d
    %number of terms
    nt=d-r;
    lind=sind+nt-1;
   theta(sind:lind,:)=qmat(r,r+1:end);
   sind=lind+1;
end