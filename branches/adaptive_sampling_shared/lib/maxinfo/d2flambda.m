%function [dfdl, df2dl2]=d2flambda(lambda1,v,c,mmag)
%   lambda1 - current value of lambda
%   v - mean of posterior expressed in basis using eigenvectors of
%        covariance matrix
%   c - eigenvalues of covariance matrix
%   mmag - the constraint on the magnitude of x
%         i.e ||x||^2=mmag^2
%
% Computes the 2nd derivative of flambda
%
%Return Value:
%   dfdl - derivative of F(lambda) w.r.t lambda
%   [df2dl2] - 2nd derivative of F(lambda) w.r.t lambda
%
%   Note the order of derivatives is to allow this function to be used with
%   matlab's fsolve routines to determine the optimal lambda
function [dfdl,df2dl2]=d2flambda(lambda,v,c,mmag)

%compute the relevant values at this point
y=v./(c-lambda);
z=(y'*y)^.5/mmag;
y=1/z*y;

g=exp(v'*y);

%use true h not simplification as C->0
j=sum(c.*y.^2);
h=exp(.5*j)*j;

%**********************************************************************
%compute 1st derivatives
%*********************************************************************
%dz/dlambda 
dzdl=(1/mmag^2*sum(v.^2./(c-lambda).^2))^-.5*(1/mmag^2*sum(v.^2./(c-lambda).^3));

%dyi/dl - this is column vector
dyidl=v.*(z)^-1.*(c-lambda).^-2-(v*(z^-2)).*(c-lambda).^-1*dzdl;

%dgdlamda
dgdl=g*v'*dyidl;

%dh/dlambda
djdl=sum(2*c.*y.*dyidl);
dhdl=j*exp(.5*j)*.5*djdl+exp(.5*j)*djdl;

%full derivative
%df/dl
dfdl=h*dgdl+g*dhdl;

%*********************************************************************
%compute 2nd derivatives
%*********************************************************************
dz2dl2=-1/4*sum(2/mmag^2*v.^2./(c-lambda).^3)*sum(1/mmag^2*v.^2./(c-lambda).^2).^(-3/2)*sum(2*v.^2/mmag./(c-lambda).^3);
dz2dl2=dz2dl2+1/2*sum(1/mmag^2*v.^2./(c-lambda).^2)^(-1/2)*sum(6/mmag^2*v.^2./(c-lambda).^4);

%dy2dl2- column vector
dy2dl2=-v.*(c-lambda).^-2*z^-2*dzdl+2*v*z^-1.*(c-lambda).^-3;
dy2dl2=dy2dl2+2*v*z^-3*(dzdl)^2.*(c-lambda).^-1;
dy2dl2=dy2dl2-v*(z^-2).*(c-lambda).^-2*dzdl-v*z^-2.*(c-lambda).^-1*dz2dl2;

dg2dl2=(v'*dyidl)^2*exp(v'*y)+exp(v'*y)*v'*dy2dl2;

dj2dl2=sum(2*c.*((dyidl).^2+y.*dy2dl2));
dh2dl2=exp(j/2)*(djdl)^2+j/4*exp(j/2)*(djdl)^2+j/2*exp(j/2)*(dj2dl2)+exp(j/2)*dj2dl2;


%full 2nd derivative
df2dl2=dgdl*dhdl+dg2dl2*h+g*dh2dl2+dgdl*dhdl;