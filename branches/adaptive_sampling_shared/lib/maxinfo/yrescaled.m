%function [finfo y]=yrescaled(w, edist u,mmag)
%  w       - parameter should be between 0 and 1 (noninclusive) over which we are conducting search
%               w=0 corresponds to mean? or max eigenvector
%               w=1 corresponds to max eigenvector? or mean
%           -can be a vector of values
% eigd   - the DISTINCT EIGENVALUES
%  emmean    - This is the mean after projecting into the eigenspace and
%                   accounting for multiplicities >1.
%                   thus its dimensionality is kx1 
%                           k - number of distinct eigenvalues
%   mmag  - constraint on stimulus magnitude
%               -that  is (y'y)^.5 = mmag

% Explanation: This function computes the optimal stimulus as a function of
% W. W is a rescaled parameter of y. Thus this function maps the interval
% (0 to 1) to lambda >c_0 and computes the optimal stimulus on this range
% given w
%
% NOTE: This assumes you want the stimulus to have the same sign as the
% mean.
% For more information about the rescaling see my notes 3_13.m
function [eta]=yrescaled(w, eigd, emean,mmag)

if (w==1)
    error('Yresecaled: w=0 causes divide by zero error. Should rewrite yscale to explicitly compute limiting value in this case.')
end

dim=length(emean);


%distinct eigenvalues

ndist=length(eigd);

threshold=eps^.5;
%if the mean is orthogonal to max eigenvector
%then we do the following which ensures that all energy
%in the eigenspace of the max eigenvalue will be evenly distrubted among
%the eigenvectors
if (emean(1)<threshold)
    emean(1)=1/2^.5;            %needs to be normalized
end

%the negative sign should make all components negative
v=-1*sign(emean(1))*w./(1-w);

%now compute xs the stimulus in terms of v
eta=zeros(dim,length(w));

%ediff is the difference between the ith eigenvalue 
%and c0.
%it has dimensionality d-1
ediff=eigd(2:end)-eigd(1);
ediff=reshape(ediff,length(ediff),1); %make it columnvector
%ediff=ediff*ones(1,length(v));

%now we compute componnets of eta
%emmag=[edist.emmag]';
eta(1,:)=v;
if length(eigd)>1
eta(2:end,:)=(emean(2:end,:)*v)./(ediff*v+emean(1));
end

%now we rescale eta to satisfy the magnitude constraint
%we also have to choose the correct sign for the solution
%in this case the correct sign is -1 b\c right now
%every entry has opposite sign of emean
mag=sum(eta.^2,1);
mag=-mmag/mag.^.5;
mag=ones(ndist,1)*mag; %make its dimensions same as xs
eta=mag.*eta;
