%function [finfo,lambda,xpts]=fisherlambda(pg)
%       pg  - specifies the gaussian describing the posterior
%       lambda - optional pts at which to compute lambda
%               if not specified will be automatically generated
%Explanation: Computes the fisher information as a function of lambda
%   Implicitly assumes that constraint on X is that stimulus power is 1
%Return Value:
%   xpts- vectors at which it was computed.
%
%Optimization: Can increase the speed with which I search lambda1 for
%eigenvalues
%
%Warnings:
%   Function not reliable when lambda1=eigenvalue.
%   issue will not perform well when have repeated eigenvalue
function [finfo,lambda1,xpts]=fisherlambda(pg,lambda1,param)

mmag=1;

%plot options
if ~exist('param','var')
    param=[];
    param.plots.on=0;
 
end

%*************************************************************************
%Make Plot of F with respect to Lambda
%*************************************************************************

%*******************************
%Generate Lambda if not supplied
%********************************
[evec eigd]=svd(pg.c);

%get an erray of eigen values 
dimstim=size(pg.c,1);
%ind=[1:dimstim]'*ones(1,2);
%diagind=indexoffset(ind,size(pg.c));
%evals=evals(diagind);
eigd=diag(eigd);
%evals=evals/min(evals);

%order will be in descending order
%generate the lambda we want to plot F at
%We know its singular when lambda1 =eval so avoid those points

if ~(exist('lambda1','var'))
    lambda1=[];
end
if (isempty(lambda1))
%to make this efficent
%use cell arrays so we only concatenate once
dl=-min(eigd)/1000;
lmax=min(2*max(eigd));

%range for sampling lambda
%find eigenvalue with max absolute value
maval=max(abs(eigd));
lb=min(eigd)-maval;
ub=max(eigd)+maval;
lambda1=linspace(lb,ub,1000);
end

%lambdaurange=linspace(ub,100*ub,1000);
%lambda1=[lambda1 lambdaurange];
numl=length(lambda1);

finfo=zeros(1,numl);
y=zeros(length(pg.m),numl);
%project mean onto eigen vectors
mmag=1;
v=evec'*pg.m;
warning('Not sure I choose the correct sign');
ysign=1;
for lindex=1:numl
    [finfo(lindex) y(:,lindex)]=flambda(lambda1(lindex),ysign,eigd,v,mmag);
end

%plot the x coordinate associated with each lambda
xpts=evec*y;


%************************************************************************
%plots
if (param.plots.on~=0)
figure;
subplot(3,1,1);
plot(lambda1,finfo,'.');
xlabel('Lambda 1');
ylabel('F(x)');


subplot(3,1,2);
plot(lambda1,xpts(1,:),'.');
ylabel('x');

subplot(3,1,3);
plot(lambda1,xpts(2,:),'.');
ylabel('y');
end
