%function fmaxlambda(pg,mmag)
%   pg - gaussian approximation of the posterior
%   mmag - the constraint on the magnitude of x
%         i.e ||x||^2
%   plots - optional turn plots on
%       .on = 1 plots on
%Explanation: Performs 1 dimensional linesearch to maximize F(lambda).
%We reparameterize lambda with respect to alpha, lambda=exp(alpha).
%This requires considering two cases
% lambda=exp(alpha)
% lambda=-exp(alpha)
%
%Potential Issues: If interval has min and max might find min as opposed to
%max. Haven't full handled the case where there is more than 1 local min or
%max on an interval
function [xopt,lambdaopt,fopt]=fmaxlambda(pg,mmag,plots)

%meantol=
if ~(exist('plots','var'))
    plots=[];
end
if ~(isfield(plots,'on'))
    plots.on=0;
end

dimstim=size(pg.m,1);

%gradient descent parameters
%step should be 1 meaning we take full jump to begin with
%we will begin to decrease it in order to settle
gparam.step=1;
gparam.sfrac=.9;
gparam.tol=.001;
gparam.maxiter=20;
gparam.lambdainit=2;

iter=1;

%get the eigen values of the covariance matrix
%[eigv eigd]=eig(pg.c);
%To improve numerical stability use the svd to get eigen
%values and vectors. Without this I have a problem with imaginary
%eigenvectors and values
[eigv eigd v]=svd(pg.c);
ind=[1:dimstim]'*ones(1,2);
diagind=indexoffset(ind,size(pg.c));
%put the eigenvalues in a vector
%these will be in asscending order
eigd=eigd(diagind);
eigd=eigd/min(eigd);
%check non of the eigenvalues are imaginary
%if ~isempty(find(abs(imag(eigd))>10^-10))
%    warning('Imaginary part of eigenvalue in fmaxlambda');
%else
%    eigd=real(eigd);
%end
%check v and eigv don't differ by more than 10^-10
%I do this to check its really symetric
if (max(abs(eigv-v))>10^-10)
    warning('u and v of svd differ by more than tolerance amount');
end

%reparameterize the mean in terms of the eigenvectors
v=eigv'*pg.m;


%**************************************************************************
%Get Seeds for gradient descent.
%**************************************************************************
%we compute lambda at a number of points
%these points should include at least 1 pt in each interval between
%eigenvalues
%we compute F(lambda) at these points and then take the max value and
%perform  gradient ascent.

%we might want to perform gradient ascent on multiple points

%in our range of points take two outliers order of magnitude larger
%than largest and smallest eigen value
%don't yet have good way of automatically choosing these points.
outliers=max(abs(eigd))*100;
%dlpts=min(diff(eigd));
%dlpts=dlpts/100;
%lambdainit=[-outliers eigd(1)+dlpts:dlpts:eigd(end)-dlpts outliers];

%[finfo,lambdainit,xpts]=fisherlambda(pg,lambdainit);

%get the seed by taking the maximum
%[fmax,ind]=max(finfo);
%lambdainit=lambdainit(ind);


%*******************************************
%get the seeds
%*******************************************
%on each interval between eigenvalues take a discrete number of evenly
%spaced points. Compute the fisher information at these points 
%take the maximum. This will serve as the seed for that interval
npts=1000;       %number of pts to use
lambdainit=zeros(1,length(eigd)+1);

seigd=sort(eigd);
for eindex=1:length(eigd)-1
    %make sure eigenvalues are not the same
    pts=linspace(seigd(eindex),seigd(eindex)+1,npts);
    %if ((abs(eigd(eindex+1))-abs(eigd(eindex)))>10^-8)
        %eigen values can sometimes be nearly imaginary
    %dlpts=real((eigd(eindex+1)-eigd(eindex))/(npts+1));
    %leave out the eigen value b\c its singular there
    %pts=[real(eigd(eindex))+dlpts:dlpts:real(eigd(eindex+1))-dlpts];
    pts=pts(2:end-1);
    [fisherinfo]=fisherlambda(pg,pts);
    [fmax,ind]=max(fisherinfo);
    
    %maxe sure the maximum is a single point and store it as an
    %initialization pt
    lambdainit(eindex)=pts(ind(1));
    %else
        %set lambdainit(eindex) to Nan so we can remove it later
    %    lambdainit(eindex)=NaN;
    %end
end

lambdainit(end-1)=-outliers;
lambdainit(end)=outliers;

%only select the valid initial points
ind=find(~isnan(lambdainit));
lambdainit=lambdainit(ind);

%reparameterize with respect to alpha
%alpha=log(abs(lambdainit));
%we need to keep track of the sign of alpha independently
%sjump=sign(lambdainit);

%number of seeds for gradient descent
%for now just choose 1.
%most of the code should be able to handle more than that
%so should be easy to add more
nseeds=length(lambdainit);

%lmax stores lambda max on each interval
lmax=zeros(1,nseeds+length(eigd)+1);
%fmax stores flambda at these pts.
%we compute this at the locations of our seeds, the eigenvalues
%and at the mean as well which is why its nseeds+length(eigd)+1
fmax=zeros(1,nseeds+length(eigd)+1);

%x stores the stimulus associated with each point we're checking
xmax=zeros(length(pg.m),nseeds+length(eigd)+1);

%**************************************************************************
%using Matlab's optimization toolbox
%numerically find the locations where the derivative=0.
%**************************************************************************
options = optimset('Jacobian','on');
options = optimset(options,'Display','off');
options = optimset(options,'TolFun',1e-20);
options = optimset(options,'TolX',1e-15);
v=eigv'*pg.m;
for sindex=1:nseeds
    %location of zeros of derivative
  
    dzero = fsolve(@(lambdap)(d2flambda(lambdap,v,eigd,mmag)),lambdainit(sindex),options);
    
    %check if its a minimum or maximum by looking at sign of 2nd derivative
    %[dfdl, df2dl2]=d2flambda(dzero,v,eigd,mmag);
    
    %if (df2dl2>0)
        lmax(sindex)=dzero;
        %if dzero is eigen value set fmax(fisherinfo) to zero
        %b\c fisherlambda will give error b\c trying to divide by zero
        %we explicitly compute the fisher information at the eigen values
        %later on
        if (~isempty(find(eigd==dzero)))
            fmax(sindex)=0;
        else
            fmax(sindex)=fisherlambda(pg,dzero);
        end
        
    %else
        %set values to Nan to indicate its not a local max
        
    %compute the vector associated with this point
    y=v./(eigd-dzero);
    y=y*(mmag/(y'*y))^.5;

    %renormalize xmax to fit the constraint
    x=eigv*y;
    x=x*(mmag/(x'*x))^.5;
    xmax(:,sindex)=x;
end

%**************************************************************************
%Compute fmax when lambda=eigen values
%Singularities occur when lambda=eigenvalues
%At these locations derivative is non-zero therefore we have to explicitly
%check if its max location or not
%
%This should properly handle case of repeated eigenvalues but unique
%eigenvectors
%**************************************************************************
%this corresponds to letting x be parrallel or anti-parallel (rotated 180
%degrees) to each of the eigen vectors
%one of these points will be local max the other will be local min
for eindex=1:length(eigd)
    %stimulus in this case is
    xpos=eigv(:,eindex)*mmag;
    x=xpos;
    %compute flambda of these points
    fpos=exp(x'*pg.m+.5*x'*pg.c*x)*x'*pg.c*x;
    xneg=(-1)*xpos;
    x=xneg;
    fneg=exp(x'*pg.m+.5*x'*pg.c*x)*x'*pg.c*x;

    lmax(nseeds+eindex)=eigd(eindex);
    %take the max of these two values
    if (fpos>fneg)
        fmax(nseeds+eindex)=fpos;
        xmax(:,nseeds+eindex)=xpos;
    else
        fmax(nseeds+eindex)=fneg;
        xmax(:,nseeds+eindex)=xneg;
    end

end

%*************************************************************************
%compute the fisherinfo at the mean
%normalize the mean to mmag
xmean=pg.m/(pg.m'*pg.m)^.5 * mmag;
fmax(nseeds+length(eigd)+1)=exp(xmean'*pg.m+.5*xmean'*pg.c*xmean)*xmean'*pg.c*xmean;
xmax(:,nseeds+length(eigd)+1)=xmean;

%*************************************************************************
%get the max of all the points returned
[fopt indfmax]=max(fmax);

xopt=xmax(:,indfmax);
lambdaopt=lmax(indfmax);

%**************************************************************************
%Plotting
%**************************************************************************
if (plots.on ~=0)
   %plot flambda
   figure;
   hold on;
   lambda=-30:.05:30;
    [finfo,lambda,xpts]=fisherlambda(pg,lambda);
    
    h1=plot(lambda,finfo);
    
    h2=plot(lambdaopt,fopt,'rx');
    
    legend('F(\lambda)','F max');
end




