%function [finfo,y]=finfoestim(y,eigd,emean)
%   y - stimulus projected onto eigenvectors
%  
%   eigd - eigenvalues
%   emean - mean projected onto eigenvectors
%
% Explanation: Computes the log of fisher information for a vector expressed in
%   computes the log because exponential often blows up
% the eigenspace
function [finfo,y]=logfinfoestim(y,eigd,emean)
   % finfo=zeros(1,size(y,2));
    finfo=y'*emean+.5*y'*diag(eigd)*y +log(y'*diag(eigd)*y);