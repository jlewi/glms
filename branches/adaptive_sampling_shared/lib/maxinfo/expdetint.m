%function [expval]=expdetint(mueps,sigma,glm)
%         mueps - value of the projection of the stimulus along the mean
%         sigma - covariance =x^t covar_t x
%         glm  - structure representing th glm
%
%Return Value:
%       expval - the expected value.
%
%Explanation: Computes the expected determinant of the new covariance
%matrix by numerically integrating it as a function of mueps and sigma.
%This requires computing an expectation over r which is distributed according 
%to some distribution in the exponential distribution given by the glm.
%Distribution is paramterized by glmproj which is a gaussian r.v with mean
%mueps and variance sigma.
%
%Limitations: Currently this only works for responses which are poisson
%distributed. Extending it to other distributions shouldn't it be
%difficult. The only real issue is in computing the bounds of integration
%and we can do this by using the cdf function.
%
%Currently it assumes the likelihood is a discrete function but it would be
%easy to extend this
function [expval]=expdetint(mueps,sigma,glm)

error('Obsolete. Use CompMINum object instead');

mbounds=zeros(1,2);     %bounds for integration over the gaussian variable.

confint=.97;            %the bounds for integration over the responses
                        %and mueps are determined by finding the interval 
                        %in which confint % of the probability mass occurs
isdiscrete=1;           %determines whether r is discrete or continuous variable

inttol=10^-6;           %tolerance for integration function
%*****************************
%compute the bounds for mu
stdnorm=norminv(1-(1-confint)/2,0,sigma^.5);
mbounds=[mueps-stdnorm,mueps+stdnorm];

%*****************************************************************
%Distribution of specific code
%****************************************************************
%this code depends on the distribution for the responses
%determine the bounds for integration and a pointer to the function
%which computes the pdf.
if isequal(glm.sampdist,@poissrnd)
    isdiscrete=1;
    pdfdist=@poisspdf;
elseif isequal(glm.sampdist, @binornd)
    isdiscreate=1;
    pdfdist=@(r,p)(binopdf(r,1,p));
else
    error('Cannot handle the specified sampling distribution');
end

%****************************************************************
%integrate by calling quad if its discrete distribution
%integrate by calling dblquad if its continuous likelihood funciton
%*************************************************************
if (isdiscrete==1)
    intfun=@(g)(rexpdisc(g,sigma,glm,pdfdist,confint));
    expval=quad(intfun,mbounds(1),mbounds(2),inttol);
else
    error('Code to handle continuous distributions is not implemented');
end
%****************************************************************
%
%******************************************************************
%evaluate the expecatation over r for a particular value of glmproj
%for a discrete r.v
%       glmproj - values of epsilon for which we compute the inner
%               expectation
%                     -need to be able to evaluate for a vector 
%       sigma   - variance of glmproj
%       glm     - pointer to the glm
%       pdfdist - function which computes the pdf 
%       confint - interval used to compute bounds for integration
%
%Return value:
%   expvals - value of the integral for each value of glmrpoj
function expvals=rexpdisc(glmproj,sigma,glm,pdfdist,confint)
    nvals=length(glmproj);      %how many values we need to evaluate it for
    expvals=zeros(1,nvals);
    %compute the mean of the sampling distribution.
    mu= glm.fglmmu(glmproj);      
    rbounds=zeros(nvals,2);
%*****************************************************************
%Distribution of specific code
%****************************************************************
%this code depends on the distribution for the responses
%determine the bounds for integration and a pointer to the function
%which computes the pdf.
%
%pdfdist(r,mu) - pointer to function to compute pdf 
%                       r - value of observation
%                       mu - mean parameter
if isequal(glm.sampdist,@poissrnd)
    rbounds(:,1)=0;
    rbounds(:,2)=poissinv(confint,mu);
    pdfdist=@poisspdf;
elseif isequal(glm.sampdist, @binornd)
    isdiscreate=1;
    rbounds(:,1)=0;
    rbounds(:,2)=1;
    pdfdist=@(r,p)(binopdf(r,1,p));
else
    error('Cannot handle the specified sampling distribution');
end

%evaluate the integral for each value
for k=1:nvals
      %the responses over which we sum to compute the expectation.
      r=floor(rbounds(1)):1:ceil(rbounds(2));
      %for each response we need to compute 
      %log(1-d^2p(r|epsilon)de^2 sigma)
      
      %1. compute d^2p(r|epsilon)de^2 sigma
      [dlde, dlldde]=d2glmeps(glmproj,r,glm);
      clear('dlde');
%      error('Need to modify d2glmeps to ensure it computes the derivative correctly when obsrv is a matrix');
      
      s=1-dlldde*sigma;
      s=log(s);
      %2. multiply each by the pdf
      s=s.*pdfdist(r,mu(k));
      s=sum(s);
      
      expvals(k)=s;
end