%function [finfo y]=flambda(lambda,ysign,eigd,v,mmag)
%   lambda - lambda at which to compute it
%   ysign - what sign to assign to y
%           - can be 1 or -1
%   eigd - eigen values
%   v    - mean projected onto eigenvalues
%   mmag  - constraint on stimulus power

% Explanation: For a given lambda1 (lagrange multiplier constraint)
% computes the fisher information and the stimulus projected onto
% eigenvectors
function [finfo,y]=flambda(lambda,ysign,eigd,v,mmag)
    
    %if we divide by zero
    %set it = to eigenvalue
    denom=eigd-lambda;
    zind=find(denom==0);
    
    if ~(isempty(zind))
       y=zeros(length(v),1);
       y(zind)=mmag;
    else
        y=v./(eigd-lambda);
    end
    y=y*(mmag^2)^.5/(y'*y)^.5;
    y=sign(ysign)*y;
    
    finfo=exp(y'*v+.5*y'*diag(eigd)*y)*y'*diag(eigd)*y;