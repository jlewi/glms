function [finfo,y]=fisherinfo(y,c,v,mmag)
    
    y(2:end)=y(2:end)*(mmag^2-y(1)^2)^.5/(y(2:end)'*y(2:end))^.5;
    finfo=exp(y'*v+.5*y'*diag(c)*y)*y'*diag(c)*y;