%function [f]=fquadmin(x,h,d)
%    x - vector where we evaluate the function
%    h - hessian or matrix which parameterizes our quad funciton
%    d - linear parameter of our quad function
%
%Explanation fquadmin is the quadratic problem that we want to minimize
%   When you call fmin you will need to use an anonymous function handle 
%   in order to specify the parameters h,d
%
%Note: This minimization function its assumed that d and x use the
%eigenvectors as a basis and h is diagonal. 
function [f]=fquadmin(x,h,d)

f=-d'*x-x'*h*x;