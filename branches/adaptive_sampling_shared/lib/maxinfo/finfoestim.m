%function [finfo,y]=finfoestim(y,eigd,emean)
%   y - stimulus projected onto eigenvectors
%  
%   eigd - eigenvalues
%   emean - mean projected onto eigenvectors
%
% Explanation: Computes the fisher information for a vector expressed in
% the eigenspace
function [finfo,y]=finfoestim(y,eigd,emean)
   % finfo=zeros(1,size(y,2));
    finfo=exp(y'*emean+.5*y'*diag(eigd)*y)*y'*diag(eigd)*y;