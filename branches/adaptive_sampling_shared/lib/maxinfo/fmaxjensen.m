%function fmaxjensen(pg)
%   pg - gaussian approximation of the posterior
%   mmag - the constraint on the magnitude of x
%         i.e ||x||^2
%   plots - optional turn plots on
%       .on = 1 plots on
%Explanation: Uses jensen's inequality to find lower bound on F(x)
%maximizes this using lagrange multipliers. Solves for the lagrange
%multiplier using gradient descent.
function [xopt,lambdaopt]=fmaxjensen(pg,mmag,plots)

if ~(exist('plots',var))
    plots=[];
end
if ~(isfield(plots,'on'))
    plots.on=0;
end

dimstim=size(pg.m,1);

%gradient descent parameters
%step should be 1 meaning we take full jump to begin with
%we will begin to decrease it in order to settle
gparam.step=1;
gparam.sfrac=.9;
gparam.tol=.001;
gparam.maxiter=20;
gparam.lambdainit=2;

iter=1;

%get the eigen values of the covariance matrix
[eigv eigd]=eig(pg.c);
ind=[1:dimstim]'*ones(1,2);
diagind=indexoffset(ind,size(pg.c));
eigd=eigd(diagind);
v=eigv'*pg.m;
g=eigd/2+log(eigd)/mmag;

%*****************************************
%debug
%****************************************
%make plot of function for lagrange multiplier
lambda=[-4:.05:4];
flambda=g*ones(1,length(lambda))-ones(dimstim,1)*lambda;
flambda=sum((1/4)*(v*ones(1,length(lambda))).^2./flambda.^2,1);
%flambda=(1/4)*v.^2./(g+lambda).^2;
%figure;
%plot(lambda,flambda);


%**************************************************************************
%We have to find the optimal points on each interval.
%**************************************************************************
dg=mean(diff(g));
lambdainit=[(g(1)-dg);g(1:end-1)+diff(g)/2;g(end)+dg];

if (dg==0);
    dg=.1*g(1);
    lambdainit=[(g(1)-dg); g+dg];
end

%number of intervals 
nintervals=length(lambdainit);

%store the sign of the jump on each iteration
%will use this to induce settinling
slambda=zeros(length(lambdainit),gparam.maxiter);

%declare arrays to store lambda on each trial
%each row corresponds to a different interval
lambda=zeros(length(lambdainit),gparam.maxiter);
lambda(:,1)=lambdainit';

%numiterations executed for each interval
numiter=zeros(length(lambdainit),1);

%store fmax on each interval
fmax=zeros(nintervals,1);

%xmax and ymax store maximum y and x values for each interval
%these are colvectors, each column is for a different interval
xmax=zeros(dimstim,nintervals);
ymax=zeros(dimstim,nintervals);

%**************************************************************************
%compute maximum on each interval
%**************************************************************************
for numinit=1:length(lambdainit)

%we start at the second iteration
iter=1;
%lam stores the value of lambda for the current iteration of the current
%interval
lam=lambda(numinit,iter);

%this is what fraction of the step size to take
lstep=gparam.step;
%lmax and lmin are the bounds of the interval for this iteration
if (numinit==1)
    lmax=g(1)-gparam.tol;
    lmin=-10^10;
elseif (numinit==nintervals)
    lmax=10^10;
    lmin=g(end)+gparam.tol;
else
    lmax=g(numinit)-gparam.tol;
    lmin=g(numinit-1)+gparam.tol;
end
%compute flambda at this point
%flambda value we evaluate. want it bo mmag
%flambda=mmag+10*gparam.tol;                 %ensures we enter while loop
flambda=(1/4)*v.^2./(g-lam).^2;
flambda=sum(flambda);




while (iter<gparam.maxiter)
    
   fprintf('MaxJensen: Init  %0.2g \t Iteration %0.2g \t \t lambda=%0.2g flambda=%0.2g \n',numinit,iter,lam,flambda);
    
   %compute the gradient
   grad=1/4*v.^2./(g-lam).^3;
   grad=sum(grad);
   
   if (grad==0)
       fprintf('warning gradient is zero \n');
       %compute gradient at poisition slight to the right
        rshift=.001;
        grad=1/4*v.^2./(g-(lam+rshift)).^3;
        grad=sum(grad);
   
   end
   %if (flambda>mmag)
   %    lam=lam-gparam.step*grad;
  % else
  %     lam=lam+gparam.step*grad;
   %end
   %set the sign of the jump
   newlambda=(mmag-flambda)/grad;
   slambda(numinit,iter)=sign(newlambda);
   
   %check if we're oscillating 
   if (iter >=2)
       if (sum(slambda(numinit,iter-1:iter))==0)          
           lstep=lstep*gparam.sfrac;
       end
   end
   newlambda=lam+lstep*newlambda;
   
   %don't allow lambda to jump the interval it started in
   if (newlambda > lmax)
       newlambda=lmax;
   elseif (newlambda <lmin)
       newlambda=lmin;
   end
       
   %don't allow lam to exceed max or min gi
   %if (newlambda > g(end))
       
   %    newlambda=lam+.9*(g(end)-lam);
   %elseif (newlambda < g(1))
   %    
   %    newlambda=lam-.9*(lam-g(1));
   %end
   lam=newlambda;
   
   flambda=(1/4)*v.^2./(g-lam).^2;
   flambda=sum(flambda);
  
  
    
   iter=iter+1;
   lambda(numinit,iter)=lam;
   
    
     %stop if within tolerance
   if (abs(flambda-mmag)<gparam.tol)
        break;
  end
end

%for this interval compute fmax
%compute xmax using lambda
warning('Need to check the formula below');
y=-v./(2*(g-lam));
y=y*(mmag/(y'*y))^.5;


%renormalize xmax to fit the constraint
x=eigv*y;
x=x*(mmag/(x'*x))^.5;

xmax(:,numinit)=x;
ymax(:,numinit)=y;

%compute fisher information
%and the lower bound on the fisher information at these points
fmax(numinit,1)=exp(pg.m'*x+.5*x'*pg.c*x)*x'*pg.c*x;
jlbound(numinit,1)=exp(pg.m'*x+.5*x'*pg.c*x+ sum(y.*log(eigd).*y));
numiter(numinit,1)=iter;
end

%Compute which interval actually gave the maximum x.
[fval,ind]=max(fmax);

%set the optimal return values
xopt=xmax(:,ind);
lambdaopt=lambda(ind);

%**************************************************************************
%Plotting
%**************************************************************************
if (plots.on ~=0)
    if (dimstim==2)
   %only plot those items which came out be numbers
    ind=find(~isnan(sum(xmax,1)));
    
    %get theta for these values
    for index=1:nintervals
        if ( xmax(1,index)<0 & xmax(2,index)>0)
            %2nd quadrant
            theta(index)=180-acos(-xmax(1,index)/(xmax(:,index)'*xmax(:,index)))*180/pi;
        elseif (xmax(1,index)<0 & xmax(2,index)<0)
            theta(index)=180+acos(-xmax(1,index)/(xmax(:,index)'*xmax(:,index)))*180/pi;
        elseif (xmax(1,index)>0 & xmax(2,index) <0)
        theta(index)=360-acos(xmax(1,index)/(xmax(:,index)'*xmax(:,index)))*180/pi;
        else
            theta(index)=acos(xmax(1,index)/(xmax(:,index)'*xmax(:,index)))*180/pi;
        end
    end
   
    uparam.plots.on=0;
    [ficirc,jlcirc,thetacirc,xpts,hf]=fisherucirc(pg,uparam);

    %plot a separate figure as well
    figure;
    hold on;
    plot(thetacirc,ficirc);
    h_p(1)=plot(thetacirc,ficirc,'b.');
    lgnd{1}='F(x)';

    plot(thetacirc,jlcirc,'r');
    h_p(2)=plot(thetacirc,jlcirc,'rx');
    lgnd{2}='Lower Bound';
    hold on
    h_p(3)=plot(theta(ind)*pi/180,jlbound(ind),'k*');
    plot(theta(ind)*pi/180,fmax(ind),'k*');
    lgnd{3}='Max values';
    xlabel('\theta');
    ylabel('F(x)');
    xlim([0 2*pi]);

    lgnd{3}='Max solution';
    legend(h_p,lgnd);
    else
        warning('fmaxjensen: Plotting is only for 2-d case');
    end
end




