%test fsolve
options = optimset('Jacobian','on')
x = fsolve('ffsolve',-2)

%test use of fsolve to maximize flambda
options = optimset('Jacobian','on');
[eigv eigd]=eig(pg.c);
dimstim=length(pg.m);
ind=[1:dimstim]'*ones(1,2);
diagind=indexoffset(ind,size(pg.c));
%put the eigenvalues in a vector
%these will be in asscending order
mmag=1;
eigd=eigd(diagind);
x = fsolve(@(x)(d2flambda(x,eigv'*pg.m,eigd,mmag)),.5)
