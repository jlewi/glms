%1-19-2005
%test gradflambda.m to make sure it computes the correct derivative
%do this by computing flambda at a whole bunch of closely spaced points and
%comparing it to the return value

pg.m=[1;0];
pg.c=[.9 .3;.3 .1];
mmag=1;                 %constraint on x has to be 1 to compare to fisherlambda

%get the eigen values of the covariance matrix
[eigv eigd]=eig(pg.c);
dimstim=length(pg.m);
ind=[1:dimstim]'*ones(1,2);
diagind=indexoffset(ind,size(pg.c));
%put the eigenvalues in a vector
%these will be in asscending order
eigd=eigd(diagind);

dlpts=min(diff(eigd));
dlpts=dlpts/1000;
lambda=[eigd(1)+dlpts:dlpts:eigd(end)-dlpts];

%compute flambda
[finfo]=fisherlambda(pg,lambda);

%compute the gradient at these points
grad=zeros(1,length(lambda));
grad2=zeros(1,length(lambda));      %2nd deerivative
for index=1:length(grad)
    %[grad(index)]=gradflambda(lambda(index),eigv'*pg.m,eigd,mmag);
    [grad(index),grad2(index)]=d2flambda(lambda(index),eigv'*pg.m,eigd,mmag);
end

%estimated graded
gradest=diff(finfo)/dlpts;

%estimated 2nd derivative
gradest2=diff(gradest)/dlpts;

figure;
hold on;
h=plot(lambda, grad,getptype(1,1));
plot(lambda, grad,getptype(1,2));

hest=plot(lambda(2:end),gradest,getptype(2,1));
plot(lambda(2:end),gradest,getptype(2,2));

legend([h hest],{'True','Estimated'});


%***********************************************************
%2nd derivatives
figure;
hold on;
h=plot(lambda, grad2,getptype(1,1));
plot(lambda, grad2,getptype(1,2));

hest=plot(lambda(3:end),gradest2,getptype(2,1));
plot(lambda(3:end),gradest2,getptype(2,2));

legend([h hest],{'True','Estimated'});
