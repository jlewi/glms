%function PlotPredictionsGLM
% Make predictions of neural responses for each of the three STRFs
% Prediction are for song stimuli
% Plot the resutls at the end

%modified to be used with Joe's GLM STRF analysis

%Set up some parameters
%dataFile = 'C:\Spike\Data\Data_David.xls';
dataFile = 'C:\Users\Joe Schumacher\Desktop\Projects\svn_glms\Data_Joe.xls';
%strfDir = 'C:\Spike\Data\STRFs\';
strfDir = 'L:\Data\SpikeBackup\STRFs\';
GLMstrfDir = 'C:\Users\Joe Schumacher\Desktop\Projects\svn_glms\GLM_STRFs\';
stimDir = 'C:\Spike\WaveFiles\wrong_sampling_rate';
%toneDir = 'C:\Spike\Data\Sorted';
toneDir = 'L:\Data\SpikeBackup\Sorted';
readSheet = 'GLM_STRFS';

cellsToAnalyze = [1:8];

% Read excel spreadsheet
[nums,yy] = xlsread(dataFile,readSheet);
GLMstrfFile = yy(2:end,2);
strfFile = yy(2:end,3);
%toneFile = yy(2:end,2);

for i = 1:length(cellsToAnalyze)
    disp(i)
    if isempty(strfFile{cellsToAnalyze(i)})%||isempty(toneFile{cellsToAnalyze(i)}),
        %Set any recorded parameters to NaN if not enough strfs
        sCC(i) = NaN;
        nCC(i) = NaN;
        tCC(i) = NaN;
        strfCC(i) = NaN;
    else
        %Get the song STRF
        cd([strfDir, strfFile{cellsToAnalyze(i)}, '\song']);
        load best_CC_strf2;
        load stim_avg
        songBase = constmeanrate;
        toneBase = songBase;
        best_song_std = best_std;
        best_song_tol = best_tol;
        songSTRF = (strf(:,101:501));
        songCC = cc;
        clear cc strf
        
        %Get the noise STRF
        cd([strfDir, strfFile{cellsToAnalyze(i)}, '\noise']);
        load best_CC_strf2;
        load stim_avg
        noiseBase = constmeanrate;
        noiseSTRF = (strf(:,101:501));
        noiseCC = cc;
        clear cc strf
        
%         %Get the tone STRF
%         cd([strfDir, strfFile{cellsToAnalyze(i)}, '\NStone']);
%         load best_CC_strf2;
%         toneSTRF = ([zeros(60,100),strf(:,1:301)]);
%         clear cc strf
        
        %Get the GLM song STRF
        cd([GLMstrfDir, GLMstrfFile{cellsToAnalyze(i)}, '\Song']);
        gs=load('GLM_strf');
        gSTRF = gs.strf;
        [m n]=size(gSTRF);
        gSTRF=fliplr(gSTRF);
        glmSTRF = ([zeros(m,n-1),gSTRF]); 
        glmSTRF = adjustGLMSTRF(glmSTRF);
        
%         %%%%%%%%%%%%%%%%%%%%%%
%         % Normalize the tone STRF by the RMS of the song STRF
%         %%%%%%%%%%%%%%%%%%%%%%
%         toneSTRF = toneSTRF/max(songSTRF(:));
        
        %Find all of the song wave files
        cd(toneDir)
        load(strfFile{cellsToAnalyze(i)})
        songs = [1:14,25:30];
        
        %%%%%%%%%%%%%%%%%%%%%%
        % Run through each song, make predictions with each STRF
        %%%%%%%%%%%%%%%%%%%%%%
        cd(stimDir)
        ampsamprate = 1000; %This is input by the user in strfpak
        fwidth = 125; %This is input by the user in strfpak
        DBNOISE = 80; %Not sure where this # came from
        initialFreq = 250; %define frequency range that i care about
        endFreq = 8000;
        count = 1;
        for j = songs
            [stimulus,fs,nbits] = wavread(waves{j});
            stimsize = length(stimulus);
            stimduration(count) = round(length(stimulus)*1000/fs)+1; %duration of stim in msec
            binsize = floor(1/(2*pi*fwidth)*6*fs); %size of time bin over which fft is computed (in samples)
            nFTfreqs = binsize; %# of frequencies at which fft is computed
            increment = floor(fs/ampsamprate); %# samples to shift for each new bin start point
            frameCount = floor((stimsize-binsize)/increment)+1; %# of bins

            %****************************************
            % Compute the spectrogram
            %****************************************
            [outSpectrum outfreqs] = spectrogram(stimulus,binsize,frameCount,...
                increment,nFTfreqs,DBNOISE,fs,initialFreq,endFreq);
            
            %****************************************
            % Make the predictions
            %****************************************
            clear psthRow
            for k = 1:size(songSTRF,1),
                psthRow(k,:) = conv(outSpectrum(k,:),songSTRF(k,:));
            end
            songPred{count} = sum(psthRow) + songBase;
            songPred{count}(find(songPred{count}<0)) = 0;
            
            clear psthRow
            for k = 1:size(noiseSTRF,1),
                psthRow(k,:) = conv(outSpectrum(k,:),noiseSTRF(k,:));
            end
            noisePred{count} = sum(psthRow) + noiseBase;
            noisePred{count}(find(noisePred{count}<0)) = 0;
            
%             clear psthRow
%             for k = 1:size(toneSTRF,1),
%                 psthRow(k,:) = conv(outSpectrum(k,:),toneSTRF(k,:));
%             end
%             tonePred{count} = sum(psthRow) + toneBase;
%             tonePred{count}(find(tonePred{count}<0)) = 0;
            
            clear psthRow
                        for k = 1:size(glmSTRF,1),
                psthRow(k,:) = conv(outSpectrum(k,:),glmSTRF(k,:));
            end
            glmPred{count} = sum(psthRow) ;%+ toneBase;
            glmPred{count}(find(glmPred{count}<0)) = 0;
            
            
            count = count+1;
        end
        
        %****************************************
        % Now get the actual responses
        %****************************************
        cd([strfDir, strfFile{cellsToAnalyze(i)}, '\song']);
        SI = load('best_strf');
        load Global_Variables
        loaded1 = load('predResult_avgSpike1.mat');
        loaded2 = load('predResult_avgSpike2.mat');
        cc = [];
        smooth = [SI.psth_smoothconst SI.smoothVect(1):SI.smoothVect(2):SI.smoothVect(3)];
        PSTH_smoothed = {};
        
        %Get the jack-knifed predictions from STRFpak
        %Compare these to my predictions to make sure i'm doing this right
        best_std_num = find(Std_val==best_song_std);
        best_tol_num = find(Tol_val==best_song_tol);
        JN_Pred = {};
        loaded = load(['predResult_EstSpike_Tol' num2str(best_tol_num) '.mat']);
        for j = 1:length(songs)
            JN_Pred{j} = loaded.est_spike{j}{best_std_num} + songBase;
        end

        for j = 1:length(songs)
            clear PSTH
            PSTH = .5 *(loaded1.avgspike1{j} + loaded2.avgspike2{j});
            
            %Set the predicted and real responses to the same length
            strfLen = (size(songSTRF,2)-1)/2;
            glmstrfLen = (size(glmSTRF,2)-1)/2;
            lDiff = length(JN_Pred{j})-(length(songPred{j})-2*strfLen);
            glmlDiff = length(JN_Pred{j})-(length(glmPred{j})-2*glmstrfLen);
            songPred{j} = songPred{j}(strfLen:end-(strfLen-lDiff+1));
            noisePred{j} = noisePred{j}(strfLen:end-(strfLen-lDiff+1));
            %tonePred{j} = tonePred{j}(strfLen:end-(strfLen-lDiff+1));
            glmPred{j} = glmPred{j}(glmstrfLen:end-(glmstrfLen-glmlDiff+1));
            
            for k = 1:length(smooth),
                smoother = hanning(smooth(k))';
                smoother = smoother/sum(smoother);
                PSTH_smoothed = convn(PSTH',smoother,'same');
                psth = PSTH_smoothed/max(PSTH_smoothed);
                
                %Calculate the rms value for each time series.
                rmsPsth = sqrt(mean(psth.*psth));
                rmsSong = sqrt(mean(songPred{j}.*songPred{j}));
                rmsNoise = sqrt(mean(noisePred{j}.*noisePred{j}));
                %rmsTone = sqrt(mean(tonePred{j}.*tonePred{j}));
                rmsSTRFpak = sqrt(mean(JN_Pred{j}.*JN_Pred{j}));
                rmsGLM = sqrt(mean(glmPred{j}.*glmPred{j}));
                
                %Normalize the pred and psth to rms values
                %SONG
                pred = [songPred{j}]*(rmsPsth/rmsSong);
                len = min(length(pred),length(psth));
                r = corrcoef(pred(1:len),psth(1:len));
                songCC(j,k) = r(2);
                
                %GLM Song
                pred = [glmPred{j}]*(rmsPsth/rmsGLM);
                len = min(length(pred),length(psth));
                r = corrcoef(pred(1:len),psth(1:len));
                glmSongCC(j,k) = r(2);
%                 figure;
%                 plot(pred,'b')
%                 hold on;
%                 plot(psth,'r')
%                 close
%                 length(pred)-length(psth);

                
                %NOISE
                pred = [noisePred{j}]*(rmsPsth/rmsNoise);
                len = min(length(pred),length(psth));
                r = corrcoef(pred(1:len),psth(1:len));
                noiseCC(j,k) = r(2);
                
%                 %TONE
%                 pred = [tonePred{j}]*(rmsPsth/rmsTone);
%                 len = min(length(pred),length(psth));
%                 r = corrcoef(pred(1:len),psth(1:len));
%                 toneCC(j,k) = r(2);
                
                %STRFpak
                pred = [JN_Pred{j}]*(rmsPsth/rmsSTRFpak);
                len = min(length(pred),length(psth));
                r = corrcoef(pred(1:len),psth(1:len));
                STRFpakCC(j,k) = r(2);
            end
        end
        
        scc = max(songCC');
        sCC(i) = mean(scc(~isnan(scc)));
        gcc = max(glmSongCC');
        gCC(i) = mean(gcc(~isnan(gcc)));
        ncc = max(noiseCC');
        nCC(i) = mean(ncc(~isnan(ncc)));
%         tcc = max(toneCC');
%         tCC(i) = mean(tcc(~isnan(tcc)));
        strfcc = max(STRFpakCC');
        strfCC(i) = mean(strfcc(~isnan(strfcc)));
    end
end

figure;
plot(nCC,sCC,'ko')
hold on
plot([0 1],[0 1],'k-')
axis([0 1 0 1])
xlabel('Noise predicting song')
ylabel('Song predicting song')

figure;
plot(gCC,sCC,'ko')
hold on
plot([0 1],[0 1],'k-')
axis([0 1 0 1])
xlabel('GLM predicting song')
ylabel('Song predicting song')

% figure;
% plot(tCC,sCC,'ko')
% hold on
% plot([0 1],[0 1],'k-')
% axis([0 1 0 1])
% xlabel('Tone predicting song')
% ylabel('Song predicting song')

figure;
plot(strfCC,sCC,'ko')
hold on
plot([0 1],[0 1],'k-')
axis([0 1 0 1])
xlabel('STRFpak predicting song')
ylabel('Song predicting song')