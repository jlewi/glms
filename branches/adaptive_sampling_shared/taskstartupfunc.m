%this is a function I want to call at the end of taskStartup for parallel
%or distributed jobs. This function performs any tasks I want to do
%before starting the actual task.
%
%I use it to set the path
%
function taskstartupfunc()
  fprintf('TaskStartupfunc.m \n');
  setpaths;

  %set our environment path variable so taht we can find "svn" because
  %we need the svn command to be on our path in order to get the svn revision
  setenv('PATH',[getenv('PATH') ':~/programs/bin']);
  
  fprintf('Setting maximum number of computational threads to 2\n');
  maxNumCompThreads(2);