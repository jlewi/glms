%**************************************************************************
%
%Explanation: Setup the info. max. simulations using the tangent space
%   Sets up the following simulations
%   1. shuffled
%   2. info. max.
%   3. info. max. tan with rank
%       rank =2
%       rank =4
%       rank =8
%
%Theta is represented in the fourier domain
%
%Revision
%   11-10-2008: Revised script to change the cutoff frequencies, and the
%   prior
%
function phdsetup_bsftsep_infomax()

rfiles=phdbsdata_neurons_corrected();

dsets=struct();
dind=0;
%table to describe these datasets
%extra row for header
odinfo=cell(length(rfiles)+1,2);
odinfo{1,1}='neuron';
odinfo{1,2}='simulations';


for rind=1:length(rfiles)
    %try decreasing the bin size and increasing the number of time bins
    allparam.obsrvwindowxfs=124;
    allparam.stimnobsrvwind=20;
    allparam.freqsubsample=1;

    allparam.model='MBSFTSep';
    allparam.rawdatafile=rfiles(rind);






    scale=1;
    allparam.prior.stimvar=10^-2*scale;
    allparam.prior.shistvar=1*scale;
    allparam.prior.biasvar=10*scale^2;

    %create a bdata an mobject to use to decide which frequency components to
    %include
    bdata=BSSetup.initbsdata(allparam);

    strfdim=getstrfdim(bdata);


    %  nfreq=20;
    % ntime=4;
    nfreq=10;
    ntime=4;
    allparam.mparam.btouse=MBSFTSep.btouseforcutoff(strfdim,nfreq,ntime);


    %how many repeats of the data to use
    allparam.stimparam.nrepeats=5;

    allparam.updater.compeig=false;

    
    [nname]=getfilename(allparam.rawdatafile);
    %************************************************************
    %Shuffled
    %************************************************************
    shparam=allparam;
    shparam.stimtype='BSBatchShuffle';
    shparam.updater.type='Newton1d';
    [dshuffle,otbl,ofile,bssimobj]=BSSetup.setupinfomax(shparam);

    dind=dind+1;
    
    dshuffle.comment=sprintf('neuron: %s \nshuffled design\n',nname);
    dsets=copystruct(dsets,dind,dshuffle);
    %
    % %************************************************************
    % %Info. Max.
    % %************************************************************
    imparam=allparam;
    imparam.stimtype='BSBatchPoissLB';
    imparam.updater.type='Newton1d';
    [dimax,otbl,ofile,bssimobj]=BSSetup.setupinfomax(imparam);
    
    dimax.comment=sprintf('neuron: %s \nInfo. Max. design\n',nname);
    dind=dind+1;
    dsets=copystruct(dsets,dind,dimax);
    
    odinfo{rind+1,1}=nname;
    odinfo(rind+1,2)={{'Info. Max.', getrpath(dimax.datafile); 'Shuffle',getrpath(dshuffle.datafile)}};

    %**************************************************************
    %setup the infomax with the tangent space
    %***************************************************************
    % tanparam=allparam;
    %
    % tanparam.stimtype='BSBatchPoissLBTan';
    % tanparam.updater.type='TanSpaceUpdater';
    % for rank=[2];
    %     tanparam.tanparam.rank=rank;
    %     BSSetup.setupinfomax(tanparam);
    % end
end

opath=getpath(dsets(1).datafile);
opath=fileparts(opath);

fname=getfilename(dsets(1).datafile);

%get the startnum of the output files
%assume file is name 000.mat
startnum=fname(end-6:end-4);


fname=getfilename(dsets(end).datafile);
endnum=fname(end-6:end-4);




%**************************************************************
%create a datasetfile
%**************************************************************
setupfile=seqfname(sprintf('~/svn_glms/adaptive_sampling/scripts/phd/datasets/phdbsdata_bsinfomax_%s.m',datestr(now,'yymmdd')));
writeminit(setupfile,dsets,[]);

oinfo=[{{'setupfile',setupfile}};{odinfo}];
oname=sprintf('%s_%s-%s.xml',mfilename(),startnum,endnum);
onenotetable(oinfo,fullfile(opath,oname));