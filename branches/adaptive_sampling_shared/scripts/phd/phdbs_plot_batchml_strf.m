%02-12-2009
%Make a plot of the BatchML estimates of the strf and spike history
%to include in my phd thesis
%
%Here we make plots of the STRF when we use the fourier repersentation to
%smooth the STRF in time.
setpathvars;


neuron='thesis';

mname=mfilename();
%set the parameters appropriately
switch neuron
    case 'br2523'
        [seqfiles bfile]=phdbsdata_br2523();


    case 'all'
        [seqfiles bfile]=phdbsdata_all_neurons();

    case 'thesis'
        [seqfiles bfile]=phdbsdata_all_neurons();
        bfile=bfile(1:2);
    otherwise
        error('unrecognized neuron');

end

%**************************************************************************
%Make the plots
%*************************************************************************
%%
%load the plots
for sind=1:length(bfile)
    v=load(getpath(bfile(sind).datafile));

    bssim(sind)=v.bssimobj;
end


%%
width=3;
height=3;
for sind=1:length(bfile)


    %**************************************************************************
    %plot the strf
    %**************************************************************************
    fh(sind,1)=FigObj('name','Fourier Coefficents','width',width,'height',height);

    mobj=bssim(sind).mobj;
    [t,freqs]=getstrftimefreq(bssim(sind).bdata);

    %convert to ms and hz
    t=t*1000;
    freqs=freqs/1000;
    title(fh(sind,1).a,'STRF');


    [stimcoeff,shistcoeff,bias]=parsetheta(mobj,bssim(sind).results(end).theta);

    row=sind;
    col=1;
    imagesc(t,freqs,tospecdom(mobj,stimcoeff));
    xlabel(fh(row,col).a,'Time(ms)');
    ylabel(fh(row,col).a,'Frequencies(kHz)');
    hc=colorbar;
    xlim([t(1) t(end)]);
    ylim([freqs(1) freqs(end)]);


    lblgraph(fh(sind,1));

    %************************************************************************
    %Plot the spike history
    %***********************************************************************
    fh(sind,2)=FigObj('name','Fourier Coefficents','width',width,'height',height);


    row=sind;
    col=2;
    
    t=-1*[getshistlen(mobj):-1:1];
    hp=plot(t,shistcoeff);
    pstyle.marker='o';
    pstyle.markerfacecolor='b';
    pstyle.linewidth=3;
    addplot(fh(row,col).a,'hp',hp,'pstyle',pstyle);
    xlabel(fh(row,col).a,'Time (ms)');
    title(fh(row,col).a,'Spike History Filter');
    xlim([t(1) t(end)]);


    lblgraph(fh(sind,2));
end


%************************************************************************
%Compuct the expected log-likelihood
%************************************************************************
ntest=2;
for bind=1:length(bfile)
   bll(bind)=BSLogLikeBatchFit('bssim',bssim(bind));
   
   for wind=1:ntest
      ll(bind,wind)=getllike(bll(bind),length(bssim(bind).results),wind); 
   end
end
%***********************************************************************
%Save the plots
%***********************************************************************
savedata=false;

if (savedata)
for sind=1:size(fh,1)
    gdir='~/svn_trunk/publications/adaptive_sampling/phdthesis/figs';
    
    fstrf=sprintf('bs_batch_strf_n%02g.eps',sind);
    fshist=sprintf('bs_batch_shist_n%02g.eps',sind);
    saveas(fh(sind,1).hf,fullfile(gdir,fstrf),'epsc2');
    saveas(fh(sind,2).hf,fullfile(gdir,fshist),'epsc2');    
end

end