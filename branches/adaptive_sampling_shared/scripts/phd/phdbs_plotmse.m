%02-12-2009
%
%Explanation: Make a plot showing the MSE error between theta estimated
%using the infomax designs and theta using the batch designs.
%
%The point of this plot is to show that the online stochastic updater is
%consistent.

setpathvars

[seqfiles,bfiles]=phdbsdata_all_neurons();
seqfiles=seqfiles(1:2,:);
bfiles=bfiles(1:2);

nsims=size(seqfiles,1);
%**************************************************************************
%Compute the mse
%************************************************************************
%%

for rind=1:size(seqfiles,1)
    v=load(getpath(bfiles(rind).datafile));

    bssims(rind)=v.bssimobj;

   
    for cind=1:size(seqfiles,2)
        v=load(getpath(seqfiles(rind,cind).datafile));
        seqsims(rind,cind)=v.bssimobj;

    end
end

maxiter=min([seqsims.niter])+1;

for cind=1:size(seqfiles,2);

    data(cind).smse=nan(nsims,maxiter);
    data(cind).hmse=nan(nsims,maxiter);
    data(cind).bmse=nan(nsims,maxiter);

end
%%
for rind=1:size(seqfiles,1)
 thetatrue=bssims(rind).results(end).theta;

 mobj=bssims(rind).mobj;
    for cind=1:size(seqfiles,2);
        %get theta for this simulation
        theta=getm(seqsims(rind,cind).allpost);
        theta=theta(:,1:seqsims(rind,cind).niter+1);

        niter=seqsims(rind,cind).niter;
        smse=theta(mobj.indstim(1):mobj.indstim(2),:)-thetatrue(mobj.indstim(1):mobj.indstim(2))*ones(1,niter+1);
        smse=sum(smse.^2,1).^.5;
        
        %normalize by the magnitude of the stimulus coefficients
        strue=thetatrue(mobj.indstim(1):mobj.indstim(2));
        smse=smse/(strue'*strue)^.5;
        data(cind).smse(rind,:)=smse(1:maxiter);

        hmse=theta(mobj.indshist(1):mobj.indshist(2),:)-thetatrue(mobj.indshist(1):mobj.indshist(2))*ones(1,niter+1);
        hmse=sum(hmse.^2,1).^.5;
        htrue=thetatrue(mobj.indshist(1):mobj.indshist(2));
        hmse=hmse/(htrue'*htrue)^.5;
        data(cind).hmse(rind,:)=hmse(1:maxiter);

        bmse=theta(mobj.indbias(1),:)-thetatrue(mobj.indbias(1))*ones(1,niter+1);
        bmse=sum(hmse.^2,1).^.5;
        
        btrue=thetatrue(mobj.indbias(1));
        bmse=bmse/(btrue*btrue)^.5;
        data(cind).bmse(rind,:)=bmse(1:maxiter);
    end
end
%%
%**************************************************************************
%Plot of the stimulus error
%*************************************************************************
width=2.1;
height=2.1;

fsmse=FigObj('name','Stim MSE','width',width,'height',height);
lbls={'info. max.'; 'shuffled'};

clear pstyle;
pstyle(1,1).linewidth=4;
pstyle(1,1).color='r';
pstyle(1).linestyle='-';

r=1;
c=2;
pstyle(r,c).linewidth=3;
pstyle(r,c).color='r';
pstyle(r,c).linestyle='--';

r=2;
c=1;
pstyle(r,c)=pstyle(1,1);
pstyle(2,2)=pstyle(1,2);

pstyle(2,1).color='g';
pstyle(2,2).color='g';

for dind=1:length(data)
    mmse=mean(data(dind).smse,1);
    smse=std(data(dind).smse);

    hp=plot(0:maxiter-1,mmse);
     plot(0:maxiter-1,mmse+smse,'linewidth',pstyle(dind,2).linewidth,'color',pstyle(dind,2).color,'linestyle',pstyle(dind,2).linestyle);
     plot(0:maxiter-1,mmse-smse,'linewidth',pstyle(dind,2).linewidth,'color',pstyle(dind,2).color,'linestyle',pstyle(dind,2).linestyle);

    addplot(fsmse.a,'hp',hp,'pstyle',pstyle(dind,1),'lbl',lbls{dind});
end


xlabel(fsmse.a,'Trial');
ylabel(fsmse.a,'Normalized MSE');
title(fsmse.a,'STRF');

lblgraph(fsmse);


%*********************************************************************
%Spike history coefficients
%**************************************************************
%%
fhmse=FigObj('name','Spike history','width',width,'height',height);

for dind=1:length(data)
    mmse=mean(data(dind).hmse,1);
    smse=std(data(dind).hmse);

    hp=plot(0:maxiter-1,mmse);
    plot(0:maxiter-1,mmse+smse,'linewidth',pstyle(dind,2).linewidth,'color',pstyle(dind,2).color,'linestyle',pstyle(dind,2).linestyle);
    plot(0:maxiter-1,mmse-smse,'linewidth',pstyle(dind,2).linewidth,'color',pstyle(dind,2).color,'linestyle',pstyle(dind,2).linestyle);

    addplot(fhmse.a,'hp',hp,'pstyle',pstyle(dind,1));
end


xlabel(fhmse.a,'Trial');
ylabel(fhmse.a,'Normalized MSE');
title(fhmse.a,'Spike History');

lblgraph(fhmse);


%*********************************************************************
%Bias coefficients
%**************************************************************
%%
fbmse=FigObj('name','bias','width',width,'height',height);

for dind=1:length(data)
    mmse=mean(data(dind).bmse,1);
    smse=std(data(dind).bmse);

    hp=plot(0:maxiter-1,mmse);
    plot(0:maxiter-1,mmse+smse,'linewidth',pstyle(dind,2).linewidth,'color',pstyle(dind,2).color,'linestyle',pstyle(dind,2).linestyle);
    plot(0:maxiter-1,mmse-smse,'linewidth',pstyle(dind,2).linewidth,'color',pstyle(dind,2).color,'linestyle',pstyle(dind,2).linestyle);

    addplot(fbmse.a,'hp',hp,'pstyle',pstyle(dind,1));
end


xlabel(fbmse.a,'Trial');
ylabel(fbmse.a,'Normalized MSE');
title(fbmse.a,'Bias');

lblgraph(fbmse);

