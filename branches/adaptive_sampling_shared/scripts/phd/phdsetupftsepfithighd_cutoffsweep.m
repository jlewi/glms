%**************************************************************************
%
%Explanation: Setup a bunch of simulations for doing a sweep of the best
%cutoff frequencies
%
function [dsets,setupfile]=phdsetupftsepfithighd_cutoffsweep()


dsets=struct;
dind=0;
    
nfreqs=[10 2 4 20 39];
ntimes=[4 1 7 9];


%maxiter is the maximium number of iterations we want to allow in call to
%fminunc
allparam.maxiter=25;

%try decreasing the bin size and increasing the number of time bins
allparam.obsrvwindowxfs=124;
allparam.stimnobsrvwind=20;
allparam.freqsubsample=1;
allparam.model='MBSFTSep';
allparam.rawdatafile=FilePath('RESULTSDIR','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat');
allparam.windexes=[2:14 16:30];

allparam.alength=0;


scale=10;
allparam.prior.stimvar=10^-2*scale;
allparam.prior.shistvar=1*scale;
allparam.prior.biasvar=10*scale;
%create a bdata an mobject to use to decide which frequency components to
%include. Note that setting these parameters does not affect the 
%model actual used other than the frequencies used. 
bdata=BSSetup.initbsdata(allparam);

for i=1:length(nfreqs)
    for j=1:length(ntimes)
        
strfdim=getstrfdim(bdata);
mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.alength=0;
mparam.hasbias=false;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mobj=MBSFTSep(mparam);


%  nfreq=20;
% ntime=4;
 nfreq=nfreqs(i);
 ntime=ntimes(j);

subind=bindexinv(mobj,1:mobj.nstimcoeff);
btouse=subind(:,1)<=nfreq & subind(:,2)<=ntime;
btouse=find(btouse==1);

allparam.mparam.btouse=btouse;


%how many repeats of the data to use
allparam.stimparam.nrepeats=5;

    %************************************************************
    %Shuffled
    %************************************************************
    dind=dind+1;
    dnew=BSSetup.setupfitpoiss(allparam);
    dsets=copystruct(dsets,dind,dnew);
    
    
    end
end

%****************************************************************
%create a single file describing all these datasets.
%****************************************************************
%output the information to an mfile

info.nfreqs=nfreqs;
info.ntimes=ntimes;
info.neuron=getrpath(allparam.rawdatafile);

sdir='~/svn_glms/adaptive_sampling/scripts/phd/datasets';
sname=sprintf('phddata_bscutoffsweep_%s.m',datestr(now,'yymmdd'));
setupfile=seqfname(fullfile(sdir,sname));
writeminit(setupfile,dsets,info);

