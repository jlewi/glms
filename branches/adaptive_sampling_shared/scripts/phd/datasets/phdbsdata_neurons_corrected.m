%function dfiles=phdbsdata_neuron_uncorrected(check)
%   check - check files exist
%           default false
%create an array of the raw neuron datafiles
function dfiles=phdbsdata_neurons_uncorrected(check)

dind=0;

rdir='bird_song/sorted/';

if ~exist('check','var')
    check=false;
end
% 

dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'k302_06_001-Ch2-neuron1_corrected.mat']);

dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'br2523_06_003-Ch1-neuron1_corrected.mat']);



dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'aa0708_04_001-Ch1-neuron1_corrected.mat']);

dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'bb2728_02_001-Ch2-neuron1_corrected.mat']);


dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'k306_03_013-Ch1-neuron1_corrected.mat']);

dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'k313_02_001-Ch2-neuron1_corrected.mat']);


dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'k313_05_001-Ch1-neuron1_corrected.mat']);

dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'k313_06_001-Ch2-neuron1_corrected.mat']);


dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'k313_07_001-Ch1-neuron1_corrected.mat']);

dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'k319_04_001-Ch2-neuron1_corrected.mat']);


dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'ps10454_10_001-Ch2-neuron1_corrected.mat']);


%skip this neuron because it seems to be missing some of the spike for its
%repeats.
dind=dind+1;
dfiles(dind)=FilePath('RESULTSDIR',[rdir 'k313_04_001-Ch1-neuron1_corrected.mat']);

if (check)
 for dind=1:length(dfiles)
    if ~exist(getpath(dfiles(dind)),'file')
        fprintf('Missing: %s \n',getpath(dfiles(dind)));
    end
 end
end

