function [dsets,varargout]= phdbs_data_infomax_tspace_090227_001() 
%********************************


dsets=[];
%************************************************
% neuron: k302_06_001-Ch2-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=1;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_001.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_001.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_001.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_001.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_001.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: br2523_06_003-Ch1-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=2;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_002.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_002.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_002.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_002.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_002.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: aa0708_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=3;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_003.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_003.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_003.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_003.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_003.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: bb2728_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=4;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_004.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_004.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_004.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_004.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_004.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k306_03_013-Ch1-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=5;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_005.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_005.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_005.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_005.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_005.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=6;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_006.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_006.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_006.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_006.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_006.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_05_001-Ch1-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=7;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_007.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_007.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_007.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_007.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_007.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_06_001-Ch2-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=8;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_008.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_008.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_008.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_008.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_008.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_07_001-Ch1-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=9;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_009.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_009.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_009.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_009.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_009.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k319_04_001-Ch2-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=10;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_010.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_010.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_010.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_010.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_010.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: ps10454_10_001-Ch2-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=11;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_011.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_011.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_011.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_011.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_011.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=12;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_012.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_012.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_012.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_012.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_012.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];

if (nargout>=2)
    for dind=1:size(dsets,2)
        v=load(getpath(dsets(dind).datafile));
        simobj(dind)=v.bssimobj;
    end
    varargout{1}=simobj;
end

