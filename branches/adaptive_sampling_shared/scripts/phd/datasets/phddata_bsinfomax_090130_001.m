function [dsets,varargout]= phddata_bsinfomax_090130_001() 
%********************************


dsets=[];
%************************************************
% neuron: aa0708_04_001-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=1;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_005.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_005.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_005.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_005.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_005.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: aa0708_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=2;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_006.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_006.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_006.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_006.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_006.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: bb2728_02_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=3;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_007.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_007.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_007.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_007.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_007.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: bb2728_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=4;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_008.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_008.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_008.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_008.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_008.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k306_03_013-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=5;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_009.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_009.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_009.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_009.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_009.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k306_03_013-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=6;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_010.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_010.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_010.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_010.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_010.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_02_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=7;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_011.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_011.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_011.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_011.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_011.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=8;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_012.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_012.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_012.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_012.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_012.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_05_001-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=9;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_013.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_013.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_013.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_013.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_013.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_05_001-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=10;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_014.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_014.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_014.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_014.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_014.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_06_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=11;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_015.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_015.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_015.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_015.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_015.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_06_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=12;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_016.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_016.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_016.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_016.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_016.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_07_001-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=13;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_017.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_017.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_017.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_017.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_017.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_07_001-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=14;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_018.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_018.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_018.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_018.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_018.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k319_04_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=15;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_019.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_019.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_019.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_019.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_019.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k319_04_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=16;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_020.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_020.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_020.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_020.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_020.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: ps10454_10_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=17;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_021.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_021.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_021.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_021.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_021.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: ps10454_10_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=18;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_022.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_022.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_022.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_022.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_022.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_04_001-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
dind=19;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_023.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_023.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_023.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_023.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_023.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
dind=20;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_024.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_024.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_024.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_024.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_024.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];

if (nargout>=2)
    for dind=1:length(dsets)
        v=load(getpath(dsets(dind).datafile));
        simobj(dind)=v.bssimobj;
    end
    varargout{1}=simobj;
end