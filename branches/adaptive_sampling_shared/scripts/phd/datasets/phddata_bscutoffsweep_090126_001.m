function [dsets]= phddata_bscutoffsweep_090126_001() 
%********************************
%********************************
%Data set info
%********************************
% nfreqs=10   2   4  20  39 
% ntimes=4  1  7  9 
% neuron=bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat 


dsets=[];
dind=1;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_002.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_002.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_002.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_002.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_002.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=2;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_003.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_003.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_003.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_003.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_003.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=3;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_004.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_004.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_004.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_004.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_004.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=4;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_005.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_005.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_005.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_005.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_005.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=5;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_006.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_006.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_006.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_006.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_006.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=6;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_007.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_007.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_007.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_007.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_007.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=7;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_008.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_008.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_008.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_008.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_008.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=8;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_009.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_009.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_009.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_009.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_009.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=9;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_010.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_010.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_010.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_010.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_010.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=10;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_011.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_011.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_011.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_011.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_011.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=11;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_012.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_012.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_012.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_012.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_012.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=12;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_013.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_013.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_013.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_013.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_013.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=13;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_014.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_014.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_014.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_014.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_014.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=14;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_015.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_015.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_015.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_015.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_015.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=15;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_016.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_016.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_016.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_016.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_016.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=16;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_017.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_017.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_017.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_017.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_017.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=17;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_018.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_018.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_018.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_018.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_018.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=18;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_019.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_019.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_019.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_019.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_019.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=19;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_020.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_020.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_020.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_020.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_020.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=20;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_setup_021.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_data_021.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_mfile_021.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_cfile_021.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090126/bsglmfit_status_021.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


