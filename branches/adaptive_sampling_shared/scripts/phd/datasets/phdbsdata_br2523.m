%function [sefiles, bfiles]=phdbsdata_br2523()
%
%Explanation:
%   The datasets for these files
function [seqfiles, bfiles]=phdbsdata_br2523()



seqfiles=[];


dind=1;
seqfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_001.m','isdir',0);
seqfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_001.mat','isdir',0);
seqfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_001.dat','isdir',0);
seqfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_001.dat','isdir',0);
seqfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_001.txt','isdir',0);
seqfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(dind).nwindexes=28;
seqfiles(dind).setupfile=[mfilename('fullpath') '.m'];

dind=dind+1;
seqfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_002.m','isdir',0);
seqfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_002.mat','isdir',0);
seqfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_002.dat','isdir',0);
seqfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_002.dat','isdir',0);
seqfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_002.txt','isdir',0);
seqfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(dind).nwindexes=28;
seqfiles(dind).setupfile=[mfilename('fullpath') '.m'];

%*************************************************************************
%batch ml simulation
%**************************************************************************

bfiles=[];
dind=1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_setup_002.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_data_002.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_mfile_002.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_cfile_002.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_status_002.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];

