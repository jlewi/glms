%function [sefiles, bfiles]=phdbsdata_k302()
%
%Explanation:
%   The datasets for these files
function [seqfiles, bfiles]=phdbsdata_k302()



seqfiles=[];


dind=1;
seqfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_003.m','isdir',0);
seqfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_003.mat','isdir',0);
seqfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_003.dat','isdir',0);
seqfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_003.dat','isdir',0);
seqfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_003.txt','isdir',0);
seqfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(dind).nwindexes=28;
seqfiles(dind).setupfile=[mfilename('fullpath') '.m'];

dind=dind+1;
seqfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_004.m','isdir',0);
seqfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_004.mat','isdir',0);
seqfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_004.dat','isdir',0);
seqfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_004.dat','isdir',0);
seqfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_004.txt','isdir',0);
seqfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(dind).nwindexes=28;
seqfiles(dind).setupfile=[mfilename('fullpath') '.m'];
seqfiles(dind).exllfile=FilePath('RESULTSDIR','bird_song/090130/bsinfomax_data_004_expllike_001.mat');

%************************************************
% neuron: k302_06_001-Ch2-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
dind=dind+1;
seqfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_001.m','isdir',0);
seqfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_001.mat','isdir',0);
seqfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_001.dat','isdir',0);
seqfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_001.dat','isdir',0);
seqfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_001.txt','isdir',0);
seqfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(dind).nwindexes=28;
seqfiles(dind).setupfile=[mfilename('fullpath') '.m'];
seqfiles(dind).exllfile=FilePath('RESULTSDIR','bird_song/090227/bsinfomax_data_001_expllike_001.mat');
%**********************************************************************
%Batch ML
%**********************************************************************

bfiles=[];
dind=1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_setup_001.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_data_001.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_mfile_001.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_cfile_001.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_status_001.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];
