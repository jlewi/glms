%date: 01-24-2009
%
%Explanation: Make a plot of the mean squared error of the estimated STRF
%  for my phd.
clear variables;

[dsets]=gp_data_infomax_090208_001();

dsets=dsets(5:6);
dfiles=dsets;
for dind=1:length(dsets)
   v=load(getpath(dsets(dind).datafile));
   simobj(dind)=v.simobj;
end


fprintf('Changing simulation labels make sure they are correct !!!\n');
setlabel(simobj(1),'opt. gp.');
setlabel(simobj(2),'white noise');

%%

width=6.2;
height=3;

%how often to subsample the data before plotting
subsamp=500;

%throw out early trials for which MSE is increasing
starttrial=1000;



scriptfile=mfilename();
otble=[];
for dind=1:2:length(simobj)
    pind=ceil(dind/2);
   fh(pind)=plotmse([simobj(dind:dind+1)],subsamp);
  %set(fh(pind).a.ha,'xscale','log');
   otble{ceil(dind/2),1}=fh(pind);
   otble{ceil(dind/2),2}=[{simobj(dind).label, getrpath(dfiles(dind).datafile);simobj(dind+1).label, getrpath(dfiles(dind+1).datafile); 'mmag', simobj(dind).mobj.mmag}];
end

fh.width=width;
fh.height=height;
sizesubplots(fh)

%move the legend
setposition(fh.a(3).hlgnd,.22,.7,.18,.16);

for aind=1:3
   set(fh.a(aind).ha,'xlim',[starttrial max([simobj.niter])]); 
   set(fh.a(aind).ha,'xtick',10.^[1:5]); 
   
end


saveas(fh.hf,'~/svn_trunk/publications/adaptive_sampling/batch_optimization/figs/gp_mse.eps','epsc2');


% otble=[{'script',mfilename()};otble];
% onenotetable(otble,seqfname('~/svn_trunk/notes/mse.xml'));