%******************************************************************
%Date: 02-18-2009
%
%Explanation: Make figures for a schematic of how we compute speedup
%
%
% We make 4 figures
%   1. A plot of the Expected log likelihood vs. trial
%   2. A plot of the % Converged vs. trial
%   3. A plot of the trial vs. % converged
%   4. speedup vs. % Converged
clear variables;
setpathvars

[seqfiles]=phdbsdata_all_neurons();

seqfiles=seqfiles(3,:);

%create one file per dataset
for sind=1:size(seqfiles,1)
    for dind=1:2
        fprintf('Design %d of %d \n',(sind-1)*2+dind, numel(seqfiles));
        
        bsllike(sind,dind)=BSExpLLike('datafile',[seqfiles(sind,dind).datafile]);


    %compute the expected mse 
     % compellike(bsllike(sind,dind));
    end
end

pind=0;



%************************************************************************
%Create the speedup object
%*************************************************************************
tcutoff=2*10^4;
%We partition the fraction of the max predictive accuracy into bins
%bwidth- the width of the bins
bwidth=.005;

bsspeed=BSSpeedup('seqfiles',seqfiles,'tcutoff',tcutoff,'bwidth',bwidth);

data=bsspeed.data;
bcent=bsspeed.bcent;
%%
%************************************************************
%Parameters for the graphs
%*************************************************************

width=2;
height=2;

%font size
fsize=10;
%which wave file in the test set to use
tind =1;

lbls={'Shuffled','Info. Max.'};

%define the plot styles 
%for the shuffled and info. max designs
pstyles=PlotStyles();
ps(1)=pstyles.plotstyle(1);
ps(2)=pstyles.plotstyle(2);
ps(2).color=[0 1 0];
ps(2).markerfacecolor=[0 1 0];

%***********************************************************************
%Make figure of Expected log-likelihood
%************************************************************************
%%


    [fllike]=plot(bsspeed.bsllike);




for rind=1:size(fllike,1)
    for cind=1:size(fllike,2)
        %change the labels
        fllike(rind,cind).a.hlgnd.lbls={'Shuffled','Info. Max.'};
        
        fllike(rind,cind).width=width;
        fllike(rind,cind).height=height;
        fllike(rind,cind).fontsize=fsize;
        setfsize(fllike(rind,cind));
        plotlegend(fllike(rind,cind).a.hlgnd);
        autosize(fllike(rind,cind).a.hlgnd);
        setfontsize(fllike(rind,cind).a.hlgnd,9);
        setvoffset(fllike(rind,cind).a.hlgnd,-.1);

        setposition(fllike(rind,cind).a.hlgnd,.59,.32,.39,.18);
        
        if (cind==1)
           title(fllike(rind,cind).a,'Bird song'); 
           ttl='birdsong';
        else
           title(fllike(rind,cind).a,'ML-noise');
           ttl='mlnoise';
        end

        %fname=sprintf('~/svn_trunk/publications/adaptive_sampling/phdthesis/figs/bs_expllike_n%02g_%s.eps',rind,ttl);
        %saveas(fllike(rind,cind).hf,fname,'epsc2');
    end
end

xlabel(fllike(tind).a,'Trial');
title(fllike(tind).a,'');
lblgraph(fllike(tind));

pind=pind+1;
figs(pind)=fllike(tind);

%************************************************************************
%Make a plot of the expected log-likelihood in the linear domain
%*************************************************************************
%%
%wavefiles in the test set
bslike=bsspeed.bsllike;

wavinfo=[bsllike.wavinfo];

[wind,index]=unique([wavinfo.wind],'first');
wavinfo=wavinfo(index);

nplots=length(wavinfo);
clear fexp;

wind=tind;
    %plot the mse for al the files in the test set
    fexp(wind)=FigObj('name','Expected log likelihood in linear domain','width',width,'height',height,'xlabel','Trial','ylabel', 'exp(E_{\theta}log p(r|s_t,\theta_t))','fontsize',fsize );

    %loop over the simulations
    pind=0;
    for dind=1:length(bsllike)
        pind=pind+1;
        setfocus(fexp(wind).a);

       
        rind=find([bsllike(dind).wavinfo.wind]==wavinfo(wind).wind);
        
        %throw out any points for the log likelihood is close to zero
        %that we needed to use a multi precision object to store it
        keep=zeros(1,rind);
        

        if isfield(bsllike(dind).exllike(rind),'isdouble')      
            isd=[bsllike(dind).exllike(rind,:).isdouble];
        else
            isd=[];
        end
       
        
        isd=logical([bsllike(dind).exllike(rind,:).isdouble]);
        
        
        stats=[bsllike(dind).exllike(rind,isd).stats];
        mean=[stats.mean];
      
        
       
        %mse as a function of trial
        ttrial=[bsllike(dind).trials(isd)];

        [ttrial ind]=sort(ttrial);
        mean=mean(ind);
      

        %throw out any trials for which mean <-10^8
        %otherwise the plot will show an asymptote that will make the plot
        %look bad
       ind=find(mean>-10^8);
        mean=mean(ind);
       
        ttrial=ttrial(ind);
        
        
      
        hp=plot(ttrial,exp(mean));
      
        %ps=pstyles.plotstyle(pind);
   
        addplot(fexp(wind).a,'hp',hp,'pstyle',ps(dind));
    end


    set(fexp(wind).a,'xscale','log');
    set(fexp(wind).a,'xlim',[10^3 10^4]);
    lblgraph(fexp(wind));
%   set(fexp(wind).a,'ylim',[0]);

    

pind=pind+1;
figs(pind)=fexp;
%**************************************************************************
%Make a plot of % Converged vs. expected log-likelihood
%**************************************************************************
%%
fpc=FigObj('name','% Converged vs. trial','width',width,'height',height,'fontsize',fsize);

trialvacc=data(1,tind).trialvacc;

for dind=1:size(trialvacc)
   hp=plot(trialvacc(dind,:),bcent*100);
   
   addplot(fpc.a,'hp',hp,'pstyle',ps(dind));
end

xlabel(fpc.a,'Trial');
ylabel(fpc.a,'% Converged');

set(fpc.a.ha,'xscale','log')
set(fpc.a.ha,'xlim',[10^3 10^4])

lblgraph(fpc)

pind=pind+1;
figs(pind)=fpc;


%**************************************************************************
%Make a plot of trial vs. % converged
%**************************************************************************
%%
ftv=FigObj('name','trial vs % Converged','width',width,'height',height,'fontsize',fsize);

trialvacc=data(1,tind).trialvacc;

for dind=1:size(trialvacc)
   hp=plot(bcent*100,trialvacc(dind,:));
   
   addplot(ftv.a,'hp',hp,'pstyle',ps(dind));
end

xlabel(ftv.a,'% Converged');
ylabel(ftv.a,'Trial');

set(ftv.a.ha,'yscale','log')
%set(ftv.a.ha,'ylim',[10^3 10^4])

lblgraph(ftv)

pind=pind+1;
figs(pind)=ftv;


%**************************************************************************
%Make a plot of speedup 
%**************************************************************************
%%
fs=FigObj('name','speedup','width',width,'height',height,'fontsize',fsize);


   hp=plot(bcent*100,data(1,1).speedup*100,'LineWidth',4);
   


xlabel(fs.a,'% Converged');
ylabel(fs.a,'% Speedup');



lblgraph(fs)

pind=pind+1;
figs(pind)=fs;

%**************************************************************************
%save figures
%**************************************************************************
%*
savefigs=false;

if (savefigs)
   gdir='~/svn_trunk/publications/adaptive_sampling/phdthesis/figs/';
   fname='bs_speedschem_expllike.eps';
   saveas(fllike(1).hf,fullfile(gdir,fname),'epsc2'); 

   fname='bs_speedschem_p.eps';
   saveas(fexp.hf,fullfile(gdir,fname),'epsc2'); 
   
   fname='bs_speedschem_pc.eps';
   saveas(fpc.hf,fullfile(gdir,fname),'epsc2'); 
   
   fname='bs_speedschem_tv.eps';     
   saveas(ftv.hf,fullfile(gdir,fname),'epsc2');

   fname='bs_speedschem_speedup.eps';
   saveas(fs.hf,fullfile(gdir,fname),'epsc2');
end
