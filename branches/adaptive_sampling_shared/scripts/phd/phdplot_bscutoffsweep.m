%make plots of the log likeilhood on the test sets for our sweeps of the
%cutoff frequencies.

setpathvars
dfiles=phddata_bscutoffsweep_090126_001();


bsllike=BSLogLikeBatchFit.create('dfiles',[dfiles.datafile]);

[fmaps,omaps]=plottheta([bsllike.bssimobj]);

[fllike]=plot(bsllike);

%adjust the lbls to equal the frequencies
for bind=1:length(bsllike)
    mobj=bsllike(bind).bssimobj.mobj;

    %determine which frequencies we are using
    [ind]=bindexinv(mobj,1:mobj.nstimcoeff);

    nf=max(ind(:,1));
    nt=max(ind(:,2));

    setplabel(fllike.a,bind,sprintf('nf=%d   nt=%d',nf,nt));
end
plotlegend(fllike.a.hlgnd);
autosizetight(fllike.a.hlgnd);

%create a table of the frequencies and the log likelihoods
oinfo=cell(length(bsllike)+1,bsllike(1).ntest);

oinfo{1,1}='nf';
oinfo{1,2}='nt';
if (bsllike(1).wavinfo(1).issong)
    issong='true';
else
    issong='false';
end
oinfo{1,3}=sprintf('Log Likelihood wind=%d\n is wavesong=%s',bsllike(1).wavinfo(1).wind,issong);

if (bsllike(1).wavinfo(2).issong)
    issong='true';
else
    issong='false';
end
oinfo{1,4}=sprintf('Log Likelihood wind=%d\n is wavesong=%s',bsllike(1).wavinfo(2).wind,issong);

for bind=1:length(bsllike)
    mobj=bsllike(bind).bssimobj.mobj;

    %determine which frequencies we are using
    [ind]=bindexinv(mobj,1:mobj.nstimcoeff);

    nf=max(ind(:,1));
    nt=max(ind(:,2));

    oinfo{bind+1,1}=nf;
    oinfo{bind+1,2}=nt;
    oinfo{bind+1,3}=bsllike(bind).data(1,1).llike;
    oinfo{bind+1,4}=bsllike(bind).data(1,2).llike;

end

oinfo=[oinfo(1,:); sortrows(oinfo(2:end,:),-3)];

%***************************************************************
%create a string to represent the contents of a latex table
%**************************************************************

%%


data=cell2mat(oinfo(2:end,:));
freq=unique(data(:,1));
time=unique(data(:,2));

%an array which maps the frequencies to their indexes
ftoind=nan(1,max(freq));
ftoind(freq)=1:length(freq);

ttoind=nan(1,max(time));
ttoind(time)=1:length(time);

lind=sub2ind([length(freq) length(time)],ftoind(data(:,1)),ttoind(data(:,2)));


%which column contains the likelihood
for wind=1:2

    %tdata should be a matrix where the i^th j^th element stores
    %the log-likelihood of frequency i and time j
    tdata=zeros(length(freq),length(time));

    
    tdata(lind)=data(:,2+wind);

    ltbl='';

    %print the times

    for ti=1:length(time)
        ltbl=sprintf('%s & %d', ltbl,time(ti));
    end
    ltbl=sprintf('%s \\\\ \n \\hline \n',ltbl);


    for fi=1:length(freq)
        ltbl=sprintf('%s %d ',ltbl,freq(fi));

        %now print each value
        for ti=1:length(time)
            ltbl=sprintf('%s & %0.3g ',ltbl,tdata(fi,ti));
        end

        ltbl=sprintf('%s \\\\ \n \\hline \n',ltbl);
    end


tdataall{wind}=tdata;
texstr{wind}=ltbl;
end
%%



onenotetable(oinfo,seqfname('~/svn_trunk/notes/lliketble.xml'));