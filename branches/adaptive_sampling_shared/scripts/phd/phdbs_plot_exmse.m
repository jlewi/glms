%for the nips data plot the mse between the sequential designs and 
%the STRF estimated with batch methods


setpathvars

[seqfiles,bfiles]=phdbsdata_other_neurons();

%create one file per dataset
for sind=1:size(seqfiles,1)
    for dind=1:2
    bsmse(sind,dind)=BSOnlineMSE('seqfiles',[seqfiles(sind,dind).datafile],'bfile',bfiles(sind).datafile);


%compute the expected mse 
compexmse(bsmse(sind,dind));
    end
end


plotavgexpmse(bsmse);

otbl=cell(size(seqfiles,1),2);
for sind=1:size(seqfiles,1)
    [fmse(sind),o]=plotexpmse(bsmse(sind,:));
    otbl{sind,1}=fmse(sind);
    otbl{sind,2}=[{'neuron', getfilename(seqfiles(sind,1).rawdatafile)};o{1,2}];
end



onenotetable(otbl,seqfname('~/svn_trunk/notes/mseplots.xml'));