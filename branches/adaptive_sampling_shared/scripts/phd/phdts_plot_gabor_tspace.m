%02-24-2009
%
%Explanation: To illustrate the tangent space,
%   for our Gabor function make plots of the Gabor, perturbations of the
%   parameters, and the resulting approximation using the tangent space.
clear variables;

dimtheta=40;
A=3;
sigmasq=(dimtheta/2/3)^2;
center=0;
omega=3*pi/(dimtheta-1);

gp= [A; center;];
tobj=GaborTanSpaceAC('sparam',gp,'dimtheta',dimtheta,'sigmasq',sigmasq,'omega',omega);

ttrue=gettheta(tobj);
tspace=gradsubmanifold(tobj,[gp(1);gp(2)]);
    

%***********************************************************************
%Make a plot of the changes in A
%*******************************************************************
da=[-1.5 3 6];
dc=[1 2 5];

fa=FigObj('name','Amplitude','naxes',[length(da) 2],'width',6,'height',4);



pstyles=PlotStyles();
pstyles.order={'colors' 'lstyles' 'markers'};

pind=0;

ptrue.LineStyle='-';
ptrue.color=[0 0 1];

pman.LineStyle='-';
pman.color=[0 1 0];
pman.LineWidth=4;

ptan.LineStyle='--';
ptan.color=[0 1 0];
ptan.LineWidth=3;


for dind=1:length(da)
    aind=1;
    setfocus(fa.a,dind,aind);
    
    aind=1;
    %plot the true value
    setfocus(fa.a,dind,aind);
    hp=plot(ttrue);      
    pind=pind+1;
    
    if (dind==1)
        addplot(fa.a(dind,aind),'hp',hp,'pstyle',ptrue,'lbl','\theta');
    else
        addplot(fa.a(dind,aind),'hp',hp,'pstyle',ptrue);        
    end


    pind=pind+1;
    pstyle=plotstyle(pstyles,floor(pind/2)+1);
    tmod=submanifold(tobj,[gp(1)+da(dind);gp(2)]);
    hp=plot(tmod);      
   
 if (dind==1)
        addplot(fa.a(dind,aind),'hp',hp,'pstyle',pman,'lbl','manifold');
 else
      addplot(fa.a(dind,aind),'hp',hp,'pstyle',pman);
    
    end
        
    pind=pind+1;
    pstyle.LineStyle='--';
    tl=ttrue+tspace*[da(dind);0];
     hp=plot(tl);      

     if (dind==1)
        addplot(fa.a(dind,aind),'hp',hp,'pstyle',ptan,'lbl','tan. space');
     else
      addplot(fa.a(dind,aind),'hp',hp,'pstyle',ptan);
    
    end
end


%******************************************************
%Derivative with respect to the center
%****************************************************
%%
aind=2;
pind=0;


dc=[1 2 5];
for cind=1:length(dc)
    
    setfocus(fa.a,dind,aind);
    hp=plot(ttrue);      
    pind=pind+1;
    pstyle=plotstyle(pstyles,pind);
    addplot(fa.a(dind,aind),'hp',hp,'pstyle',ptrue);

    aind=2;
    setfocus(fa.a,cind,aind);
    
    pind=pind+1;
    pstyle=plotstyle(pstyles,floor(pind/2)+1);
    tmod=submanifold(tobj,[gp(1);gp(2)+dc(cind)]);
    hp=plot(tmod);      
    addplot(fa.a(cind,aind),'hp',hp,'pstyle',pman);
    
        
    pind=pind+1;
    pstyle.LineStyle='--';
    tl=ttrue+tspace*[0;dc(cind)];
     hp=plot(tl);      
    addplot(fa.a(cind,aind),'hp',hp,'pstyle',ptan);
end

lblgraph(fa);
sizesubplots(fa);


