%for the nips data plot the mse between the sequential designs and 
%the STRF estimated with batch methods
setpathvars

[seqfiles]=phdbsdata_all_neurons();

seqfiles=seqfiles(1:2,:);

%create one file per dataset
for sind=1:size(seqfiles,1)
    for dind=1:2
        fprintf('Design %d of %d \n',(sind-1)*2+dind, numel(seqfiles));
        
        bsllike(sind,dind)=BSExpLLike('datafile',[seqfiles(sind,dind).datafile]);


    %compute the expected mse 
     % compellike(bsllike(sind,dind));
    end
end

%***********************************************************************
%Make figures
%************************************************************************
%%

otbl=cell(size(seqfiles,1),1);
for sind=1:size(seqfiles,1)
    [fl,o]=plot(bsllike(sind,:));
    fllike(sind,:)=fl;

    otbl(sind,1)={[{'neuron', getfilename(seqfiles(sind,1).rawdatafile)};o]};
end

%***********************************************************************
%Adjust and save figures
%************************************************************************
%%
width=3.2;
height=3.2;

for rind=1:size(fllike,1)
    for cind=1:size(fllike,2)
        %change the labels
        fllike(rind,cind).a.hlgnd.lbls={'Shuffled','Info. Max.'};
        
        fllike(rind,cind).width=width;
        fllike(rind,cind).height=height;
        setfsize(fllike(rind,cind));
        plotlegend(fllike(rind,cind).a.hlgnd);
        autosize(fllike(rind,cind).a.hlgnd);
        setposition(fllike(rind,cind).a.hlgnd,.5,.2,.33,[]);
        
        
        if (cind==1)
           title(fllike(rind,cind).a,'Bird song'); 
           ttl='birdsong';
        else
           title(fllike(rind,cind).a,'ML-noise');
           ttl='mlnoise';
        end

        fname=sprintf('~/svn_trunk/publications/adaptive_sampling/phdthesis/figs/bs_expllike_n%02g_%s.eps',rind,ttl);
        saveas(fllike(rind,cind).hf,fname,'epsc2');
    end
end


onenotetable(otbl,seqfname('~/svn_trunk/notes/expllike.xml'));