%date: 01-24-2009
%
%Explanation: Plot the strf on
%several trials
clear variables;

[dsets]=gp_data_infomax_090208_001();

dsets=dsets(5:6);

for dind=1:length(dsets)
   v=load(getpath(dsets(dind).datafile));
   simobj(dind)=v.simobj;
end

fprintf('Changing simulation labels make sure they are correct !!!\n');
setlabel(simobj(1),'opt. gp.');
setlabel(simobj(2),'white noise');

scriptfile=mfilename();
%%

%width for the plot of the strf's
pstrf.width=6.2;
pstrf.height=4;

%widht and height for plot of stimulus coefficients
pshist.width=3;
pshist.height=3;

pbias.width=pshist.width;
pbias.height=pshist.height;


%extension for graphics
fext='.png';

%trials=[1000 5000 25000 50000 100000];
trials=[10*10^3 50*10^3 100*10^3 200*10^3 300*10^3 400*10^3];
fsize=12;


nrows=length(simobj);

oinfo=cell(ceil(length(simobj)/2),4);

%limits for the colorbars
clim.stim=[-2.5 2.5]*10^-3;
clim.shist=[-2 .5];

%number of sims per set
nset=2;
%**************************************************************************
%loop over the datesets and plot the strfs
%*********************************************************
%for sall=1:2:length(simobj)
for sall=1:2:1
    ncols=length(trials)+1;
    fh=FigObj('name','STRFs','width',pstrf.width,'height',pstrf.height,'naxes',[2 ncols],'fontsize',fsize);

    for sind=1:length(simobj);
        aind=sind-sall+1;

        for tind=1:length(trials)
            trial= trials(tind);


            t=simobj(sind).extra.t;
            f=simobj(sind).extra.f;
            %multiply t by 1000 so its in ms
            t=t*1000;

            %divide freqs by 1000 so its in khz
            f=f/1000;

            if (trial<=simobj(sind).niter)
                setfocus(fh.a,aind,tind);
                strf=getm(simobj(sind).allpost,trial);
                strf=strf(simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2));
                strf=tospecdom(simobj(sind).mobj,strf);
                %            strf=reshape(strf,[simobj(sind).mobj.klength simobj(sind).mobj.ktlength]);



                imagesc(t,f,strf);
            end



        end

    setfocus(fh.a,aind,ncols);
    %plot the true strf
    theta=simobj(sind).observer.theta;
    theta=theta(simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2));
    theta=tospecdom(simobj(sind).mobj,theta);

    imagesc(t,f,theta);
    

    end


    %add a colorbar to the final image in the first row
    dind=1;
    tind=ncols;

    
    
    setfocus(fh.a,dind,tind);
    %add a colorbar
    fh.a(dind,tind).hc=colorbar;
    title(fh.a(dind,tind),'true');


    addtext(fh,[.5,.95],'STRF');
    %%
    %*****************************************************************
    %adjust the labels
    %************************************************************
    %turn off all tickmars
    set(fh.a,'xtick',[]);
    set(fh.a,'ytick',[]);

    %set x and y limits
    set(fh.a,'xlim',[t(1) t(end)]);
    set(fh.a,'ylim',[f(1) f(end)]);

    %turn on xtick,ytick for appropriate graphs
    rind=2;
    set([fh.a(rind,1)],'ytickmode','auto');
    set(fh.a(rind,1),'xtickmode','auto');

    %********************************************************


    set([fh.a],'clim',clim.stim);


    %add ylabels
    for dind=1:2
        ylbl='';
        lbl=simobj(dind).label;
        %split the lbl based on :
        sind=strfind(lbl,':');
        if isempty(sind)
            ylbl=lbl;
        else
            while ~isempty(sind)
                ylbl=sprintf('%s\n%s',ylbl,lbl(1:sind(1)-1));
                lbl=lbl(sind(1)+1:end);
                sind=sind(2:end);
            end
            ylbl=sprintf('%s\n%s',ylbl,lbl);
        end

        if (dind==length(dsets))
            ylbl=sprintf('%s\nFrequency (KHz)',ylbl);
        else
            %don't add ticklables
            set(fh.a(dind,1),'YTickLabel',[]);
        end

        ylabel(fh.a(dind,1),ylbl);

    end



    %add titles
    for tind=1:length(trials)
        if (trials(tind)>=1000)
            tl=sprintf('Trial %2gk',trials(tind)/1000);
        else
            tl=sprintf('Trial %3g',trials(tind));
        end
        title(fh.a(1,tind),tl);
    end

    %add xlabels
    for tind=1:1
        xlabel(fh.a(2,tind),sprintf('Time(ms)'));
    end


    lblgraph(fh);
    space.cbwidth=.15;
    sizesubplots(fh,space,[],[],[0 0 0 .1]);

    %%
    %**************************************************************************
    %make plots of the spike history and bias coefficients
    %**************************************************************************
    fshist=FigObj('name','Spike history','width',pshist.width,'height',pshist.height,'naxes',[2 2]);

    %make a plot of the spike history on each trial

    ntrials=min([simobj(sall:sall+1).niter]);
    for sind=sall:sall+nset-1;
        aind=sind-sall+1;
        setfocus(fshist.a,1,aind);

        theta=getm(simobj(sind).allpost);
        theta=theta(:,1:ntrials);

        shistcoeff=theta(simobj(sind).mobj.indshist(1):simobj(sind).mobj.indshist(2),:);

        imagesc([1:simobj(sind).mobj.alength],1:ntrials,shistcoeff');
        title(fshist.a(1,aind),simobj(sind).label);

        set(gca,'ylim',[1 ntrials]);
        set(gca,'xlim',[.5 simobj(sind).mobj.alength+.5]);
        set(gca,'ydir','reverse');
        set(gca,'yscale','log');
        set(gca,'xtick',[]);

        %for the second row plot the true value
        setfocus(fshist.a,2,aind);
        theta=simobj(sind).observer.theta;
        theta=theta(simobj(sind).mobj.indshist(1):simobj(sind).mobj.indshist(2));
        imagesc([1:simobj(sind).mobj.alength],[0:1],ones(2,1)*theta');
        set(gca,'ytick',[]);
        set(gca,'xlim',[1 simobj(sind).mobj.alength]);
        set(gca,'ylim',[0 1]);

        set(fshist.a(2,aind).ha,'xlim',[.5 simobj(sind).mobj.alength+.5])
       
        xlabel(fshist.a(2,aind),'i');
    end
    ylabel(fshist.a(1,1),'Trial');
   ylabel(fshist.a(2,1),'True');
    set(fshist.a(1,1).ha,'ytick',10.^[1:2:5]);
    set(fshist.a(1,2).ha,'ytick',[]);
    %
    % clim=get([fshist.a.ha],'clim');
    % clim=cell2mat(clim);
    % clim=[min(clim(:,1)) max(clim(:,2))];
   
    set([fshist.a.ha],'clim',clim.shist);

    %add a colorbar
    fshist.a(1,2).hc=colorbar;


    lblgraph(fshist);

    addtext(fshist,[.5 .95], 'Spike History Coeff');
    space.cbwidth=.1;
    sizesubplots(fshist,space,[],[.8 .2],[0 0 0 .06]);

    %%
    %**************************************************************************
    %plot the bias
    %**************************************************************************
    fbias=FigObj('name','Bias','width',pbias.width,'height',pbias.height,'title','bias','xlabel','trial','ylabel','bias','naxes',[1 1]);

    for sind=sall:sall+1;
        theta=getm(simobj(sind).allpost);

        bias=theta(simobj(sind).mobj.indbias,1:simobj(sind).niter+1);

        hp=plot(0:simobj(sind).niter,bias);
        addplot(fbias.a,'hp',hp,'lbl',simobj(sind).label);
    end

    %plot the true bias
    truebias=simobj(1).observer.theta(simobj(1).mobj.indbias);
    ltrial=max([simobj(sall:sall+1).niter]);
    hp=plot(0:ltrial,truebias*ones(1,1+ltrial));
    addplot(fbias.a,'hp',hp,'lbl','true');
    set(fbias.a.ha,'xscale','log');
    lblgraph(fbias);
    set(fbias.a.ha,'xlim',[1 ltrial]);
   
    set(fbias.a.ha,'xtick',10.^[1:5]);
     autosizetight(fbias.a.hlgnd)
    setposition(fbias.a.hlgnd,.21,.75,.39,[]);



%%
%**************************************************************
%Create a onenote table of the results
%**************************************************************

oinfo{ceil(sall/2),1}=fh;
oinfo{ceil(sall/2),2}=fshist;
oinfo{ceil(sall/2),3}=fbias;

oinfo{ceil(sall/2),4}=[{simobj(sall).label, getrpath(dsets(sall).datafile);simobj(sall+1).label, getrpath(dsets(sall+1).datafile); 'mmag', simobj(sall).mobj.mmag;'maxrate', simobj(sall).extra.maxrate}];
end


oinfo=[{'script',scriptfile,'',''}; oinfo];

onenotetable(oinfo,seqfname('~/svn_trunk/notes/optgp.xml'));

savedata=false;
if (savedata)
saveas(fh.hf,'~/svn_trunk/publications/adaptive_sampling/batch_optimization/figs/gpopt_strf.eps','epsc2');
saveas(fshist.hf,'~/svn_trunk/publications/adaptive_sampling/batch_optimization/figs/gpopt_shist.eps','epsc2');
saveas(fbias.hf,'~/svn_trunk/publications/adaptive_sampling/batch_optimization/figs/gpopt_bias.eps','epsc2');
end

