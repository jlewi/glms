%1-18-2005
%
% Numerically compute the expected value of the mean estimated using
% the EKF rule and expaning around the single obsrvation maximum
%   y - value of the stimulus
%       we are integrating over the joint probability of p(y,nspikes)
%       nspikes is the number of spikes in response to y
%   mlest - the estimated likelihood
%   ml    - true max likelihood
%   mldiff - the difference between the likelihood 
%
%
%We do not consider zero spikes because this causes error when take log 0.
%True theta parameter
theta=3;
% Since we are take the expectation over a poisson process
% we need to sum over the number of observed events
% smax is the maximum value of our integral
smax=70;
%smax=70;
%length of the time window in which the response is observed
twindow=1; 

%pt at which to compute expected error
thetaest=theta;

%to compute the expectation we construct 2d matrices to store the joint
%distribution of p(y,nspikes)
%we start by constructing 2 2d matrices y and nspikes
%which store the associated value 
%range of y
ymin=.02;
ymax=2.02;
dy=.001;
y=(ymin:dy:ymax)';
y=y*ones(1,smax);

nspikes=1:smax;
nspikes=ones(size(y,1),1)*nspikes;

%construct the joint distribution
%we need a matrix to represent factorial of nspikes
factn=[1:smax];
for index=2:smax
    factn(index)=index*factn(index-1);
end
factn=ones(size(y,1),1)*factn;

%compute it for a range of theta
thetapts=.1:.25:3;
thetaest=zeros(1,length(thetapts));
for index=1:length(thetapts)
    theta=thetapts(index);
%joint probability is p(y)*p(nspikes|y)
%p(y) is uniform distribution
%p(nspikes|y) is poisson with rate lambda=twindow*e^(theta*y)
lambda=twindow*exp(theta*y);
pj=1/(size(y,1))*exp(-lambda).*(lambda).^nspikes./factn;

%compute the mle estimate from single observations
thetas=log(nspikes/twindow).*y./(y.*y);

lambda=twindow*exp(thetas.*y).*y.*y;
eta=lambda.*thetas;

exlambda=lambda.*pj;
exlambda=sum(exlambda(:));

%compute expected value
%exinvlambda=1./lambda.*pj;
%exinvlambda=sum(exinvlambda(:));

exeta=eta.*pj;
exeta=sum(exeta(:));

%expected mean
%exmean=exinvlambda*exeta
exmean=inv(exlambda)*exeta

thetaest(index)=exmean;
end

figure;
hold on;
plot(thetapts,thetaest);
h1=plot(thetapts,thetaest,'b.');
h2=plot(thetapts,thetapts,'--');
xlabel('\theta');
ylabel('E(\mu^t)');

        