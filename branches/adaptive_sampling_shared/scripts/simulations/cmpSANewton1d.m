%10-01-07
%   Script to compare SAUpdater and Newton1d updater.

clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/maxnumint.out';
diary(dfile);


niter=1000;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'cmpupdates', datestr(datetime,'yymmdd'));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('cmpupdates.mat'));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.lowmem=1; %how often to save the covariance matrix;

%****************************************************************
%Stimulus features
%****************************************************************
mparam.alength=0;
mparam.klength=10;
mparam.ktrue=cos(((1:mparam.klength)-(mparam.klength+1)/2)*(pi)/(mparam.klength-1))';
%mparam.ktrue=[10;0];
mparam.atrue=[];
%mparam.ktrue(:,1)=2*mparam.ktrue/max(mparam.ktrue);
%mparam.ktrue=[1;0];


%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
%mparam.pinit.m=[.1; .1];
mparam.pinit.c=.03*eye(mparam.klength+mparam.alength);    

%set mparam.mmag so that when 50% of energy is along the true parameter
%the avg number of spikes is 200
optim=optimset('TolX',10^-12,'TolF',10^-12);
mparam.avgspike=20;
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(.5*m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.avgspike);
mmag=fsolve(fsetmmag,1,optim);
mparam.mmag=mmag;

mobj=MParamObj('glm',glm,'klength',mparam.klength,'pinit',mparam.pinit,'mmag',mmag);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);
%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d();
%updater=BatchML();
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************


%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='newton1d';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimrand=RandStimNorm('mmag',mparam.mmag,'klength',mparam.klength);

simnewt=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',updater,'simid',simid,'lowmem',mparam.lowmem,'mobj',mobj);
simnewt=update(simnewt,niter);
savesim(simnewt,fname);
fprintf('Saved to %s \n',fname);

%%
%*********************************************
%now reupdate the posterior using same stim and observations but using
%SAupdater
simsa=simnewt;
saupdate=SAUpdater('compeig',0);
simsa=changesim(simsa,'updater',saupdate);
simsa=changesim(simsa,'simid','sa');

%compute the posterior
for iter=1:niter
   post=update(saupdate,getpost(simsa,iter-1),getstim(simsa,iter),getmobj(simsa),getobsrv(simsa,iter),[],iter); 
   post.entropy=[];
   post.uinfo=[];
   simsa=setpost(simsa,iter,post);
end
%turn off diary
diary('off');
%open the file
%%
edit(dfile);
return;

