
%7-27-07
%   Script for timing the simulations.
%   Sets up a 2-d Gabor simulation suited for timing the operations of
%   various dimenisons
clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/simtime.out';
diary(dfile);

%the different dimensions for the simulations we want to set up
%[width,height
dims=[]
%dims(end+1).d=[3 2];
%dims(end+1).d=[10 10];

% %dims(end+1).d=[15 10];
%dims(end+1).d=[20 10];
%dims(end+1).d=[20 15];
% %dims(end+1).d=[25 12];
% %dims(end+1).d=[25 14]
% %dims(end+1).d=[25 18];
% %dims(end+1).d=[25 22];
% dims(end+1).d=[20 20];
% dims(end+1).d=[25 20];
% %dims(end+1).d=[25 25];
% %dims(end+1).d=[30 25];
% %dims(end+1).d=[40 25];
% dims(end+1).d=[25 24];
% dims(end+1).d=[28 25];
% dims(end+1).d=[32 25];
dims(end+1).d=[30 30];
%dims(end+1).d=[40 25];

for dind=1:length(dims)
    

%set niter to 0 because we just want to setup but not run the simulations
niter=0;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'poissexp', datestr(clock,'yymmdd'));
RDIR=fullfile(RESULTSDIR,'poissexp', '070813');

%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save

%************************************************************************
%to speedup the simulation we adjust the optim structure
%**********************************************************************
optupdate=optimset('MaxIter',1000,'TolFun',10^-6, 'TolX',10^-6,'Display','off');
optstim=optimset('MaxIter',1000,'TolFun',10^-5, 'TolX',10^-3,'Display','off');
%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.lowmem=500; %how often to save the covariance matrix;

%****************************************************************
%Stimulus features
%****************************************************************
%mparam.gwidth=10; mparam.gheight=10;
%mparam.gwidth=25; mparam.gheight=10;
%mparam.gwidth=25; mparam.gheight=20;
%mparam.gwidth=25; mparam.gheight=25;
%mparam.gwidth=30; mparam.gheight=25;
%mparam.gwidth=40; mparam.gheight=25;
mparam.gwidth=dims(dind).d(1);   
mparam.gheight=dims(dind).d(2);
mparam.alength=0;
mparam.ktrue=gabortheta(mparam.gwidth,mparam.gheight,[0 0],10);
mparam.klength=mparam.gwidth*mparam.gheight;
mparam.atrue=[];
%mparam.ktrue(:,1)=2*mparam.ktrue/max(mparam.ktrue);
%mparam.ktrue=[1;0];

fname=fullfile(RDIR, sprintf('simtime_%dd_%s.mat',mparam.klength,datestr(clock,'yymmdd')));
fname=fullfile(RDIR, sprintf('simtime_%dd_%s.mat',mparam.klength,'070813'));
[fname, trialindex]=seqfname(fname);



%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=100*eye(mparam.klength+mparam.alength);    

%set mparam.mmag so that when 50% of energy is along the true parameter
%the avg number of spikes is 200
optim=optimset('TolX',10^-12,'TolF',10^-12);
mparam.avgspike=200;
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(.5*m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.avgspike);
mmag=fsolve(fsetmmag,1,optim);
mparam.mmag=mmag;

mobj=MParamObj('glm',glm,'klength',mparam.klength,'pinit',mparam.pinit,'mmag',mmag);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);
%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d('optim',optupdate);
%updater=BatchML();
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimmax=PoissExpMaxMI('searchmethod','varylagrange','optim',optstim);
%stimmax=ChooseMean;
%stimmax=StimMaxEval();
extra.mparam=mparam;
simid=sprintf('max%d',length(mparam.ktrue));
simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid,'lowmem',mparam.lowmem,'mobj',mobj,'extra',extra);
if (niter>0)
simmax=update(simmax,niter);
end
savesim(simmax,fname);
fprintf('Saved to %s \n',fname);
end
return;
