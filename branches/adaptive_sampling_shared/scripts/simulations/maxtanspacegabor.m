%11-18-2007
%
%Run a simulation using a theta which is a matrix but which is low rank
%
clear variables
%close all;

setpathvars;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile=seqfname('/tmp/maxnumint.out');
%diary(dfile);


niter=50;

%***********************************************************
%should replace with code for GLM object.
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'tanspace', datestr(datetime,'yymmdd'));


%data.fname
%to save data to file
%specify where to save data
%leave blank not to save
fbase=fullfile(RDIR, sprintf('gabor1d.mat'));


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.lowmem=1; %how often to save the covariance matrix;

%****************************************************************
%Stimulus features
%****************************************************************
%create theta according to the poly nomial object. 
%ptrue=PolyTanSpace('dimtheta',10,'sparam',[.1 1 1]);
dimtheta=10;
A=3;
sigmasq=(dimtheta/2/3)^2;
center=0;
omega=3*pi/(dimtheta-1);

gp= [A; sigmasq; center; omega];
ttrue=GaborTanSpace('sparam',gp,'dimtheta',dimtheta);

mparam.ktrue=gettheta(ttrue);
mparam.gp=gp;

%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);

%choose some random sinewave as mean of our prior
Ainit=rand(1,1)*10;
sinit=rand(1,1)*dimtheta^2;
cinit=rand(1,1)*dimtheta;
oinit=rand(1,1)*3*pi/dimtheta*10;
gp= [Ainit; sinit; cinit; oinit];
tinit=GaborTanSpace('sparam',gp,'dimtheta',dimtheta);
fprior=GaussPost('m',gettheta(tinit),'c',eye(getdimtheta(ttrue)));
tparam.dimsparam=getdimsparam(ttrue);
mparam.pinit=PostTanSpace('fullpost',fprior,'tanspacetype','GaborTanSpace','tanparam',tparam);


%set mparam.mmag so that when 100% of energy is along the true parameter
%the avg number of spikes is 1000
optim=optimset('TolX',10^-12,'TolF',10^-12);
mparam.avgspike=500;
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.avgspike);
[mmag, fmag, exitflag]=fsolve(fsetmmag,.1,optim);
mparam.mmag=mmag;

extra.mparam=mparam;

if (exitflag<=0)
    error('Magnitude is not properly normalized');
end

mobj=MParamObj('glm',glm,'klength',getdimtheta(ttrue),'pinit',mparam.pinit,'mmag',mparam.mmag);

%**************************************************************************
%observer
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);
%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
updater=TanSpaceUpdater();

slist={};
%**************************************************************************
%Simulations: we create 1 simulation object for each simulation we want to
%**************************************************************************
%which simulations to initialize
infomax=false;
simrand=true;
infomaxfull=false;
sgabor=false;
%**********************************************************************
%InfoMax simulation
%********************************************************************
%run the trials but when we optimize the stimulus
if (infomax)
simid='infomax';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
nstart=25;
nfull=25;
stimmax=TanSpaceInfoMaxCanon('iterfullmax',nfull,'startfullmax',nstart);
lbl=sprintf('Info. Max. Every %d iterations do full infomax. First %d trials do full infomax',nfull,nstart);

simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem, 'label',lbl);

fname=seqfname(fbase);
[simmax,slist(end+1,:)]=savesim(simmax,fname);

end
%%**********************************************************
%Simulation: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus
if (simrand)
simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
lbl='iid design.';
stimrand=RandStimNorm('mmag',mparam.mmag,'klength',getklength(mobj));
urand=TanSpaceUpdater('comptanpost',false);
simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',urand,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'label',lbl,'extra',extra);;
fname=seqfname(fbase);
[simrand,slist(end+1,:)]=savesim(simrand,fname);

end
%********************************************************************
%InfoMax: but infomax on full theta space not submanifold
%*****************************************************
if (infomaxfull)
simid='infomaxfull';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimfull=PoissExpMaxMI();
ufull=Newton1d();
lbl='Info. max. with full posterior.';
mobjfull=MParamObj('glm',glm,'klength',getdimtheta(ttrue),'pinit',getfullpost(mparam.pinit),'mmag',mparam.mmag);
simfull=SimulationBase('stimchooser',stimfull,'observer',observer,'updater',ufull,'mobj',mobjfull,'simid',simid,'lowmem',mparam.lowmem,'label',lbl);
fname=seqfname(fbase);
[simfull,slist(end+1,:)]=savesim(simfull,fname);
end

%****************************************************************
%Random Gabor functions
%************************************************************
%*************************************************************
%Update: using 1-d gabor functions
%********************************************************
if (sgabor)
simid='gabor';
%[stimtones]=PureTones('dim',getstimlen(mobj),'period',[.1 2*getklength(mobj)]);

simgabor=SimulationBase('stimchooser',stimgabor,'observer',observer,'mobj',mobj,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'label','Gabors');
savesim(simgabor,fname);
fprintf('Saved to %s \n',fname);
end


%create an xml file listing the saved simulations
onenotetable(slist,seqfname(fullfile(RDIR,'simlist.xml')));

return;

