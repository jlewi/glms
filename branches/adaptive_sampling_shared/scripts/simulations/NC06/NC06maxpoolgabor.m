%4-17-2007
%   select optimal stimuli from a pool
%   use the same gabor we used in our NIPS paper so we can compare
%   we want to draw stimuli from a pool with the same magnitude constraint.
%   as our data in the nips case
%
clear all
%close all;

setpathvars;

%file with the old data
oldfile=fullfile(RESULTSDIR, 'tracking','06_03','06_03_gabor_001.mat');
olddata=load(oldfile);
rmfield(olddata,'pmax');    %free up memory;
rmfield(olddata,'prand');   
%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/maxgabor2d.out';
diary(dfile);


niter=10;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'poolbased', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('poolgabor2d_%2.2d_%2.2d.mat',datetime(2),datetime(3)));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************
mparam.gwidth=olddata.mparam.gwidth;
mparam.gheight=olddata.mparam.gheight;
mparam.alength=0;
%mparam.ktrue=gabortheta(mparam.gwidth,mparam.gheight,[0 0],1000);
mparam.ktrue=olddata.mparam.ktrue;
mparam.klength=mparam.gwidth*mparam.gheight;

%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
%mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
%mparam.pinit.c=3*eye(mparam.klength+mparam.alength);   
mparam.pinit=olddata.mparam.pinit;

%mparam.pinit.c=[0.0482    0.0438;0.0438    0.0663];
%mparam.pinit.m=[2; -2];
mparam.lowmem=1000;

%max firing rate
%mparam.maxrate=50;
mparam.nstim=100;           %number of stimuli to use in pool
mobj=MParamObj('glm',glm,'klength',mparam.klength,'pinit',mparam.pinit,'mmag',olddata.mparam.mmag);

%*******************************************************************
%extra: is a structure belonging to the Simulation object which can save
%   any extra structures we want. i.e it can save helper variables used to
%   construct the parameters for the model
extra.mparam=mparam;
%*************************

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d();
%updater=BatchML();
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
%use pool based stimuli
%stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d10_03_14_001.mat');
stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d825_04_17_001.mat');

srescale=[];
%srescale.maxrate=100;
%srescale.theta=gettheta(observer);
%srescale.glm=mobj.glm;
%rescale the stimuli to have same magnitude as the data under the power
%constraint
srescale.sfactor=olddata.mparam.mmag;

%stimuli are selected randomly from a pool
[stimmax sfactor]=Poolstim('fname',stimfile,'miobj',PoissExpCompMI(),'stimrescale',srescale,'nstim',mparam.nstim,'resample','true');

simmax=SimulationBase('glm',glm,'stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid,'extra',extra,'lowmem',mparam.lowmem);
[pmax,srmax,simparammax,mparammax,timemax]=maxupdateobj('mparam',mobj,'simparam',simmax,'ntorun',niter);
savesim('simfile',fname,'pdata',pmax,'sr',srmax,'simparam',simparammax,'mparam',mparammax);

%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);

%specify the parameters for autmoatically rescaling the magnitudes of the
%stimuli
%srescale.avgrate=5; %median firing rate for stimuli
srescale=[];
srescale.sfactor=sfactor;

%stimuli are selected randomly from a pool
[stimrand sfactor]=Poolstim('fname',stimfile,'stimrescale',srescale,'nstim',mparam.nstim,'resample','true','lowmem',mparam.lowmem);
simrand=SimulationBase('glm',glm,'stimchooser',stimrand,'observer',observer,'updater',updater,'simid',simid,'extra',extra);
[prand,srrand,simparamrand,mparamrand]=maxupdateobj('mparam',mobj,'simparam',simrand,'ntorun',niter);
savesim('simfile',fname,'pdata',prand,'sr',srrand,'simparam',simparamrand,'mparam',mparamrand);


%turn off diary
diary('off');
%open the file
%edit(dfile);
return;

