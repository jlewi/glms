%logdetjexp (p,cmat)
%   p - nstimx1
%       probabilities to assign to the stimuli
%   cmat - values needed to compute it
%
%Computes -log|ExJexp| and the derivative
%   cmat- are the matrices produced by pooldetmat
function [ldjexp dljexpdp]=logdetjexp(p,cmat)
    djexp=0;
    dstim=size(cmat.v,2);
    nstim=length(p);

    numderv=zeros(1,nstim);
   
    for wind=1:factorial(dstim)
       dprod=p'*cmat.v(:,:,wind);
       proddp=prod(dprod);
       
       %compute contribution to log determinant
       djexp=djexp+cmat.sperm(wind)*proddp; 
       
       %numerator for derivative
       numderv=numderv+cmat.sperm(wind)*proddp*(sum(cmat.v(:,:,wind)./(ones(nstim,1)*dprod),2))';
    end
    
    ldjexp=log(djexp);
    dljexpdp=numderv/djexp;
    