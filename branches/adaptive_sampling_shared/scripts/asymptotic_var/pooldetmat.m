%function pooldetmat(stim,jeps)
%   stim - dxn matrix
%           each column is a vector x
%   jeps - 1xn vector of constants 1 for each 
%
% Explanation:
%   This is a helper function.
%
%   We want to compute:
%   log |Ex Jexp|
%   where 
%   Ex Jexp=\sum_i p(\stim_i)Jexp(\stim_i^t\theta) \stim_i \stim_i^t
%
%   This function computes an array of matrices which makes it easier  
%   and more efficient to compute the derivative of the determinant.
function cmat=pooldetmat(stim,jeps)
%*****************************************************
%create the matrices used to compute log |ExpJexp|
%and its derivative
%create a matrix where each row is a permutation of n #s
klength=size(stim,1);
nstim=size(stim,2);
cmat.perm=perms(1:klength);
cmat.v=zeros(nstim,klength,size(cmat.perm,1));
cmat.sperm=zeros(nstim,1);
im=eye(klength*[1 1]); %identity matrix used for computing determinant of permutation matrixes


%jeps=jexp(x_i't\theta) - column vector
%replicate it across rows so that 
%jeps  is nstimxdstim
%each row of jeps is jexp(x_i't\theta)*ones(1,nstim);
jeps=jeps'*ones(1,klength);

%to create matrices of c.v
for wind=1:size(cmat.perm,1)
   %determine sign of the permutation matrix
   cmat.sperm(wind)=det(im(:,cmat.perm(wind,:)));  
   %cmat.m=cmat.m+sperm*jeps.*stim'.*(stim(cmat.perm(wind,:),:)');
   cmat.v(:,:,wind)=jeps.*stim'.*(stim(cmat.perm(wind,:),:)');
end
