%03-22-07
%
%Return Value:
%   imax - results for info. max. design
%       .asymvar
%       .asymevals - eigenvalues of the asymptotic covariance matrix
%   irand - results for i.i.d. design
%       .asymvar
%       .asymevals - eigenvalues of the asymptotic covariance matrix
%
% Explanation:
%   %Compute the asymptotic variance in the pool based setting.
%
% 12-17-2007
%   updated code. Object data must be accessed via get/set methods
% $Revision$ I copied the code from the othermethod branch of my cvs
%   repository.
%   I was still having problems getting it to converge. So I tried
%   initializing the stimulus distribution with the empirical stimulus
%   distirubtion and this worked.
function [imax,irand,cmat]=asymvarpool(fname)
imax=[];
irand=[];
%setpaths;
%fname=fullfile(,'poolbased','03_22','03_22_poolstim_001.mat');

[pmax simmax, mobjmax,srmax]=loadsim('simfile',fname,'simvar','simmax');
[prand simrand, mobjrand,srrand]=loadsim('simfile',fname,'simvar','simrand');

%get the expeted fisher information
stim=getstimpool(getstimchooser(simmax));
nstim=size(stim,2);

%compute the fisher information for each stimulus
jeps=jexp(getglm(mobjmax),gettheta(getobserver(simmax))'*stim);

%*****************************************************
%create the matrices used to compute log |ExpJexp|
%and its derivative
%create a matrix where each row is a permutation of n #s
cmat=pooldetmat(stim,jeps);

%**************************************************
%compute the optimal asymptotic distribution
%imax.pop
Aeq=ones(1,nstim);
beq=1;
lb=zeros(nstim,1);
ub=ones(nstim,1);
A=[];
B=[];
%initialize by putting all probability on the stimuli actually chosen
pinit=zeros(nstim,1);
%stiminfo=[srmax.stiminfo{:}];
%sind=[stiminfo(:).sind];
%pinit=1/nstim*ones(nstim,1);
%put all probability on stimuli maximally correlated with theta
[mcorr mind]=max(gettheta(getobserver(simmax))'*stim);

%put the weight on at least 2 points otherwise determinant
%is zero and log|A| is undefined
%This is because Jexp is rank 1 
%pick a vector orthogonal to mind
[mcorr mminind]=min(normmag(stim(:,mind))'*normmag(stim));
pinit(mind)=.5;
pinit(mminind)=.5;

stiminfo=[srmax.stiminfo{:}];
sind=[stiminfo.sind];
stimcounts=zeros(size(stim,2),length(sind));
stimcounts(sub2ind([size(stim,2),length(sind)],sind,1:length(sind)))=1;
stimcounts=sum(stimcounts,2);
pinit=stimcounts/sum(stimcounts);
%initialize using the empirical distribution
%objfun=@(p)(negelogjexp(p,jmat));
optim=[];
optim = optimset(optim,'GradObj','on');
optim=optimset(optim,'Display','on');
optim=optimset(optim,'TolX',10^-16);
optim=optimset(optim,'TolFun',10^-16);
optim=optimset(optim,'Maxiter',100000);

objfun=@(p)(neglogdetjexp(p,cmat));
fprintf('Finished computing expected fisher information matrices \n');
[imax.popt,fval,exitflag,output]=fmincon(objfun,pinit,A,B,Aeq,beq,lb,ub,[],optim);

%compute the asymptotic variance;
ind=find(imax.popt>0);
imax.asymjexp=zeros(getklength(mobjmax),getklength(mobjmax));

for t=ind'
    imax.asymjexp= imax.asymjexp+imax.popt(t)*jeps(t)*stim(:,t)*stim(:,t)';
end
imax.asymvar=inv(imax.asymjexp);


%***********************************************************
%compute iid asymptotic variance
irand.popt=ones(nstim,1)/nstim; %uniform distribution
irand.asymjexp=zeros(getklength(mobjmax),getklength(mobjmax));
for t=ind'
    irand.asymjexp= irand.asymjexp+irand.popt(t)*jeps(t)*stim(:,t)*stim(:,t)';
end
irand.asymvar=inv(irand.asymjexp);


%compute the eigenvalues
[imax.asymevecs imax.asymevals]=svd(imax.asymvar);
[irand.asymevecs irand.asymevals]=svd(irand.asymvar);
imax.asymevals=diag(imax.asymevals);
irand.asymevals=diag(irand.asymevals);

%compute the negative of the log determinant and the derivative
%we need the negative because we will use fmincon to maximize our objective
%function
function [ldjexp dljexpdp]=neglogdetjexp(p,cmat)
    [ldjexp dljexpdp]=poollogdetjexp(p,cmat);
    ldjexp=-ldjexp;
    dljexpdp=-dljexpdp;

