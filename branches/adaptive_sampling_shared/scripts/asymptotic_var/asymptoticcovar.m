%function [avar]=asymptoticcovar(tglm, aglm,px1,n,mmag)
%           tglm    - true glm
%           aglm    - the assumed glm
%           px1     - pointer to function which computes probability
%                        stimulus has value x1 in direction theta.
%                         can also be string 'uniformsphere' in which case
%                         px1 is set to uniform distribution on n
%                         dimensional hypersphere
%           n      - dimensionality of stimulus
%           mmag - magnitude constriant.
%           thetamag - magnitude of theta
% Return value
%   avar.theta - variance in direction of posterior mean
%   avar.orth - variance in directions orthogonal to theta
%
% Explanation computes the asymptotic covariance matrix 
function [avar]=asymptoticcovar(tglm,aglm,px,n,mmag,thetamag)

confint=.99;    %confidence interval for computing expected fisher information
if (strcmp(px,'uniformasphere')==1)
    px=@pxuniformball;
end

%integrate the expected fisher information over the distribution on x1
%multiplied by x1^2.
fint=@(x1)(px(x1).*expjobs(x1*thetamag,tglm,aglm,confint).*(x1.^2));
avar.theta=quad(fint,-mmag,mmag);

%integrare to get variance in direction orthogonal to theta
fint=@(x1)(px(x1).*expjobs(x1*thetamag,tglm,aglm,confint).*(mmag^2-x1.^2)/(n-1));
avar.orth=quad(fint,-mmag,mmag);

%we need to take receiprocal
avar.theta=1/avar.theta;
avar.orth=1/avar.orth;
