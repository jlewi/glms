%11-05-2005
%The purpose of this script is to look at the probability along slices of a
% a multivariate gaussian and convince myself they are not gaussian.
mu=[0;0];
%std deviation expressed as eigenvectors and eigenvalues of
%covariance matrix
%these must be orthogonal
s=[1 -1; 1 1];
l=[8 0 ; 0 1];
sigma=s*l*s';

%generate the sample points
n=400;
xl=[-5 5];
x=ones(n,1)*linspace(xl(1),xl(2),n);
y=(linspace(xl(1),xl(2),n))'*ones(1,n);

v=[x(1:end); y(1:end)];

pdf=mvgauss(v,mu,sigma);
plot3(v(1,:),v(2,:),pdf);

%make a contour plod
figure;
pdfm=reshape(pdf,n,n);
contour(x,y,pdfm);

%*********************************************************************
% Get the probability along some vector u centered
% at the origin. Probability should have shape of a gaussian.
%********************************************************************
%get a slice some vector
u=[.5;.5];
u=u/(u'*u)^.5
a=linspace(-5,5,n);
v=u*a;

py=mvgauss(v,mu,sigma);
figure;
hold on;
plot(a,py,'.');


uvar=u'*inv(sigma)*u;
ps=1/(((2*pi)^(size(mu,1)/2)*det(sigma)^.5))*exp(-a.^2/2*uvar);
plot(a,ps,'r');

%fit a guassian with mean y*mu
%and variance u' Sigma u
%we need to scale the normalization constant appropriately
%c=(2*pi*u'*sigma*u)^.5/((2*pi)^(size(mu,1)/2)*det(sigma)^.5);
%uvar=u'*sigma*u;
%plot(a,c*mvgauss(a,u'*mu,u'*sigma*u),'r');


%*********************************************************************
% Get the probability along a vector w, parallel to u but not centered at
% the origin
% at the origin. Probability should have shape of a gaussian.
%********************************************************************
u=[0;.5];
u=u/(u'*u)^.5
a=linspace(-5,5,n);
wc=[1;0];
w=u*a+wc*ones(1,n);

figure;
%plot the contour of the multi-variate gaussian
%and the vector along which we are computing the probability
subplot(1,2,1);
hold on;
contour(x,y,pdfm);
plot(wc,wc+u);

subplot(1,2,2);
hold on;
py=mvgauss(w,mu,sigma);
plot(a,py,'.');

%by my calculations see my notes
%the slice should be gaussian with respect to a, the scaling along vector
%u
ww=wc'*inv(sigma)*wc;
uw=u'*inv(sigma)*wc;
uu=u'*inv(sigma)*u;
pa=(2*pi)^(-size(u,1)/2)*det(sigma)^(-.5)*exp(-.5*(ww-uw^2/uu))*exp(-.5*(uu*(a+uw/uu).^2));

plot(a,pa,'r');
