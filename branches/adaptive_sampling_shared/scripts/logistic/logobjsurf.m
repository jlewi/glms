%1-03-07
% Numerically compute the 2-d surface (muepsilon,sigma) over which 
%we do the optimization in the case of logistic regression.
clear all
%close all;
setpaths;

%******************************************
%file to save results to 
datetime=clock;
RDIR=fullfile(RESULTSDIR,'logistic', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));
ropts.fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_logobjsurf.mat',datetime(2),datetime(3)));
ropts.savedata=1;

%*******************************************
%set up the numerical integration
%*******************************************
glm=GLMModel('binomial','canon');
opts.confint=.999;               %how much confidence for integrating the inner expectation
opts.inttol=10^-6;        % tolerance for numerical integration
muglm=[-3:.25:3];
sigmasq=10.^[-3:.5:0];

lobjsurf=zeros(length(muglm),length(sigmasq));
for j=1:length(muglm)
    for k=1:length(sigmasq)
        [lobjsurf(j,k)]=expdetint(muglm(j),sigmasq(k),glm,opts);
    end
end
%********************************************************
%Post Process
%********************************************************
%save the file
vtosave.lobjsurf='';
vtosave.muglm='';
vtosave.sigmasq='';
vtosave.opts='';


fobj.hf=figure;
fobj.xlabel='\mu_\epsilon';
fobj.ylabel='\sigma^2';
fobj.name='Numerical Comp. of Obj. Func';
contourf(muglm,sigmasq,lobjsurf');
lblgraph(fobj);
colorbar;

if (ropts.savedata~=0)
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end

%********************************************************************
%compute the approximation of the expected value
fapp.hf=figure;
fapp.xlabel='\mu_\epsilon';
fapp.ylabel='\sigma^2';
fapp.name='Approximation. of Obj. Func';
fapp.title=fapp.name;
fapprox.lobjsurf=zeros(length(muglm),length(sigmasq));
for j=1:length(muglm)
    for k=1:length(sigmasq)
        [fapprox.lobjsurf(j,k)]=finfologistic(muglm(j),sigmasq(k));
    end
end
contourf(muglm,sigmasq,fapprox.lobjsurf');
lblgraph(fapp);
colorbar;

figure(fobj.hf);