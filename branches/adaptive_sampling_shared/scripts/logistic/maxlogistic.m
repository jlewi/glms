%1-28-07
%   revised to use new object model
%Script to run optimization using numerical integration
clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%reseed the random number generator
simparam.rstate=10;
randn('state',simparam.rstate);
rand('state',simparam.rstate);


niter=2;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMModel('binom','canon');


%**************************************************************************
%Optimization:
%crate the object which specifies how we will we optimize the stimulus
%*********************************************************************
%parameters for numerical integration
numint.confint=.999;               %how much confidence for integrating the inner expectation
numint.inttol=10^-6;        % tolerance for numerical integration
%simparam.finfo=FisherInfoObj('procedure','boundbox','pproc',pproc);
%pname=seqfname(fullfile(RESULTSDIR,'precompmi/01_25_logisticobj.mat'));
%simparam.finfo=PreCompMIObj('filename',pname,'glm',simparam.glm,'mifunc',@expdetint,'mifopts',numint,'dotmu',[-2:.5:2],'sigmasq',[.5 1:1:5 10:10:100]);
%pname=fullfile(RESULTSDIR,'precompmi/01_23_logisticobj_001.mat');
%simparam.finfo=PreCompMIObj('filename',pname);

%refine the grid
%pname=seqfname(fullfile(RESULTSDIR,'precompmi/01_25_logisticobj.mat'));
%simparam.finfo=PreCompMIObj('filename',fullfile(RESULTSDIR,'precompmi/01_25_logisticobj_001.mat'));
stimchooser=PreCompMIRand('filename',fullfile(RESULTSDIR,'precompmi/01_25_logisticobj_002.mat'),'nrand',100);
%save(pname,'pobj');
%pobj=refinesigmasq(pobj,[.1:.1:2 2:.25:10 ]);
%save(pobj.filename,'pobj');

%to use analytical approximation
%simparam.finfofunc=[];




%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;
RDIR=fullfile(RESULTSDIR,'logistic', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_numlog.mat',datetime(2),datetime(3)));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************
mparam.klength=10;
mparam.alength=0;
mparam.ktrue=zeros(mparam.klength,1);
%set ktrue to be a sine wave
mparam.ktrue=[cos([0:mparam.klength-1]*pi/(mparam.klength-1))]';
%mparam.ktrue(:,1)=2*mparam.ktrue/max(mparam.ktrue);
%mparam.ktrue=[1;0];


%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);    

%set mparam.mmag so that when 50% of energy is along the true parameter
%the probability of 1 is .75
fsetmmag=@(m)(glm.fglmmu(.5*m*(mparam.ktrue'*mparam.ktrue)^.5)-.75);
mparam.mmag=fsolve(fsetmmag,1);
mparam=MParamObj('ktrue',mparam.ktrue,'pinit',mparam.pinit,'mmag',mparam.mmag);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('glm',glm,'theta', [mparam.ktrue;mparam.atrue]);

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d();

%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
simmax=SimulationBase('glm',glm,'stimchooser',stimchooser,'observer',observer,'updater',updater,'simid',simid);
[pmax,srmax,simparammax,mparammax]=maxupdateobj('mparam',mparam,'simparam',simmax,'ntorun',niter);
savesim('simfile',fname,'pdata',pmax,'sr',srmax,'simparam',simparammax,'mparam',mparammax);
fprintf('Saved to %s \n',fname);

%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
stimchooser=RandStimBall('mmag',mparam.mmag,'klength',mparam.klength);
simrand=SimulationBase('glm',glm,'stimchooser',stimchooser,'observer',observer,'updater',updater,'simid',simid);
[prand,srrand,simparamrand,mparamrand]=maxupdateobj('mparam',mparam,'simparam',simrand,'ntorun',niter);
savesim('simfile',fname,'pdata',prand,'sr',srrand,'simparam',simparamrand,'mparam',mparamrand);
fprintf('Saved to %s \n',fname);

return;

