%06-14-2007
%
%Make a plot of the running time of the Seeger's code for the cholesky
%decomposition as a function of the number of disting eigenvales and
%dimensionality
%mdims=10.^[2:4];    %the dimensionalitys of the matrices to compute results for
mdims=[50 100:50:500];
%ndist=[10:50:100 200:100:mdims];    %number of distinct eigenvalues
%ndist=[10 25:25:150 165];
ndist=50;
einfo=[];
for mind=1:length(mdims)
    %randomly generate the eigenvectors
    evecs=normrnd(zeros(mdims(mind),mdims(mind)),ones(mdims(mind),mdims(mind)));
    evecs=orth(evecs);
    %make sure its full rank
    %null(evecs');
    for nind=1:length(ndist)
        if (ndist(nind)>mdims(mind))
            break;
            warning('Skipping. number of distinct eigenvalues exceeds dimensionality');
        end
        %randomly generate the number of distinct eigenvalues
        %eigenvalues must be positive because we can only take cholesky
        %decomposition of positive definite matrix
        einfo(mind,nind).edist=abs(normrnd(zeros(ndist(nind),1),10*ones(ndist(nind),1)));
        einfo(mind,nind).ndist=ndist(nind);
        einfo(mind,nind).dim=mdims(mind);
        %create the eigenvalues
        %randomly select eigenvalue to repeat
        eind=ceil(rand(1)*ndist(nind));
        einfo(mind,nind).eigd=[einfo(mind,nind).edist(:); einfo(mind,nind).edist(eind)*ones(mdims(mind)-ndist(nind),1)];

        %sort the eigenvalues
        einfo(mind,nind).eigd=sort(einfo(mind,nind).eigd);
        %create eobj
        eold=EigObj('evecs',evecs,'eigd',einfo(mind,nind).eigd);
        %create the cholesky object
        cold=CholObj('matrix',evecs*diag(einfo(mind,nind).eigd)*evecs');
        
        %random update 
        %random update
        vup=normrnd(zeros(mdims(mind),1),5*ones(mdims(mind),1));
        %random value for rho
        rho=normrnd(0,1);
        
        [cup eopt]=rankOneUpdate(cold,vup,rho);
        einfo(mind,nind).choltime=eopt.choltime;
    end
end

otable=[];

%make a plot of timing as we vary dimensionality but leave the number of
%distinct eigenvalues intact
fdims=[];
fdims.hf=figure;
fdims.xlabel='Dimensionality';
fdims.ylabel='Time(s)';
fdims.name='dim time';

hold on;
pind=1;
fdims.hp(pind)=plot([einfo.dim],[einfo.choltime],getptype(pind));
fdims.lbls{pind}='Total';

lblgraph(fdims);


[tchol.p,tchol.s]=polyfit([einfo.dim],[einfo.choltime],2);

pdiminfo={'step','degree','R norm'; 'total',2,tchol.s.normr};
otable=[];
otable=[otable;{fdims,{'script','rank1cholspeedup';'# distinct.',ndist;'fitted poly.',pdiminfo}}];


%make a plot of the running time as we vary the number of distinct
%eigenvalues but leaving the dimensionality constant
if (length(ndist)>1)
fetime.hf=figure;
fetime.xlabel='Number distinct eigenvalues';
fetime.ylabel='Time(s)';
fetime.name='eigtime';
hold on;
pind=1;
fetime.hp(pind)=plot([einfo.ndist],[einfo.eigtime],getptype(pind));
fetime.lbls{pind}='Total';

pind=pind+1;
fetime.hp(pind)=plot([einfo.ndist],[einfo.tpreproc],getptype(pind));
fetime.lbls{pind}='Pre-process';

pind=pind+1;
fetime.hp(pind)=plot([einfo.ndist],[einfo.tdeflate],getptype(pind));
fetime.lbls{pind}='Deflation';

pind=pind+1;
fetime.hp(pind)=plot([einfo.ndist],[einfo.tsecular],getptype(pind));
fetime.lbls{pind}='Secular';

pind=pind+1;
fetime.hp(pind)=plot([einfo.ndist],[einfo.tundeflate],getptype(pind));
fetime.lbls{pind}='Un Deflate';

lblgraph(fetime);

otable=[otable;{fetime,{'script','rank1eigspeedup';'Dim.',mdims}}];
end
onenotetable(otable,seqfname('../notes/eigrank1_0704.xml'));

