%Datasets to compare the effect of online smoothing 
%to smoothing after the experiment is over. 
%
%The experiments without any smoothing represent theta during the
%experiment using the spectral domain (i.e MParamObj).
%
%The experiments which use online smoothing use an ModBSSinewaves object
%with nfreq and ntime < than the maximum values.
%
%This data is nfcutoff=10
function [dfiles,pstyles]=fbdataonlinesmooth()

dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'lstyles','colors','markers'};
pstyles.linewidth=pstyles.linewidth;

smooth.nfcutoff=10;
smooth.ntcutoff=3;

%********************************************************
%shuffled: apply the smoothing online
%*********************************************************
bdir='081010';
bfile='bsinfomax_setup_003';
nscale=1/2;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
explain='Batch shuffled design';
explain=sprintf('%sUse frequency domain.\n',explain);
explain=sprintf('%snscale=%g',explain,nscale);
explain=sprintf('%snscale depetermines what frequencies to use. We use frequencies (0:nscale*nmax)(f_fundamental)  where nmax is the largest value possible without aliasing.',explain,nscale);
dfiles(dind).explain=explain;
dfiles(dind).lbl=sprintf('shuffled: online');
dfiles(dind).usetanpost=false;

%*************************************************************************
%Shuffled: Apply the smoothing offline
%*************************************************************************
%use the posterior on the tangent space
bdir='080828';
bfile='bsinfomax_setup_003';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).taninfo=[];
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Shuffled: offline';

explain={'Method:', 'Shuffled'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Use tanpost:', dfiles(dind).usetanpost}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;

%**************************************************************************
%Info. Max. only use 1/2 the frequencies as our basis
%**************************************************************************
bdir='081007';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='STRF represented as fourier series. Use 1/2  the maximum # of frequencies. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max.: online';
dfiles(dind).usetanpost=false;

%**************************************************************************
%Info. Max. offline smoothing
%**************************************************************************
%full posterior + smoothing
bdir='080921';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).taninfo=[];
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Info. Max.:offline';
explain={'Method:', 'Infomax'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Use tanpost:', dfiles(dind).usetanpost}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;