%**************************************************************************
%
%Explanation: Setup the info. max. simulations when we represent theta in
%the fourier space and we apply the fourier transform to time and frequency
%separatly
%Theta is represented in the fourier domain
function dsets=fbsetupftseplowd()


dsets=struct;
dind=0;
    
pind=3;

%try decreasing the bin size and increasing the number of time bins
allparam.obsrvwindowxfs=250;
allparam.stimnobsrvwind=10;
allparam.freqsubsample=2;
allparam.model='MBSFTSep';

%create a bdata an mobject to use to decide which frequency components to
%include
bdata=BSSetup.initbsdata(allparam);


allparam.updater.type='Newton1d';
allparam.updater.compeig=false;

scale=100;
allparam.prior.stimvar=10^-2*scale;
allparam.prior.shistvar=1*scale;
allparam.prior.biasvar=10*scale;

strfdim=getstrfdim(bdata);
mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.alength=0;
mparam.hasbias=0;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mobj=MBSFTSep(mparam);


nfreq=10;
ntime=2;
subind=bindexinv(mobj,1:mobj.nstimcoeff);
btouse=subind(:,1)<=nfreq & subind(:,2)<=ntime;
btouse=find(btouse==1);

allparam.mparam.btouse=btouse;


%how many repeats of the data to use
allparam.stimparam.nrepeats=1;

%************************************************************
%Shuffled
%************************************************************
shparam=allparam;
shparam.stimtype='BSBatchShuffle';

dind=dind+1;
dnew=BSSetup.setupinfomax(shparam);
dsets=copystruct(dsets,dind,dnew);

%************************************************************
%Info. Max.
%************************************************************
dind=dind+1;
imparam=allparam;
imparam.stimtype='BSBatchPoissLB';
dnew=BSSetup.setupinfomax(imparam);
dsets=copystruct(dsets,dind,dnew);


% %************************************************************
% %Info. Max. Tan
% %************************************************************
dind=dind+1;
imtan=allparam;
imtan.stimtype='BSBatchPoissLBTan';
imtan.model='MBSFTSepLowRank';
imtan.tanparam.rank=2;
imtan.tanparam.startfullmax=25;
dnew=BSSetup.setupinfomax(imtan);
dsets=copystruct(dsets,dind,dnew);



%************************************************************
%Shuffled - but use only a subset of the basis vectors
%************************************************************
% shsubparam=allparam;
% shsubparam.stimtype='BSBatchShuffle';
% shsubparam.mparam.btouse=1:47;
% BSSetup.setupinfomax(shsubparam);
