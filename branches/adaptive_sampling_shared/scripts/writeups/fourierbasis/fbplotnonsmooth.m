%10-11-2008
%
%Make plots illustrating the overfitting that results when we don't
%smooth the strf

clear all;
setpathvars;

%WARNING: index into dsets are likely to change you want the following
% Shuffled - use full posterior without smoothing
% Info. max using full posterior without smoothing

bdir='~/svn_trunk/publications/adaptive_sampling/fourierbasis';
[dsets,pstyles]=fbdatanosmooth();
dtoplot=1:length(dsets);
width=3.5;
height=3.5;
%%
%select the data sets corresponding to shuffled designs
%and info. max.


%plot the expected log likelihood
fh=BSExpLLike.plot(dsets(dtoplot),pstyles);

bname='expllikenosmoothing';

fname1=fullfile('~/svn_trunk/publications/adaptive_sampling/fourierbasis',[bname '_birdsong.eps']);
fname2=fullfile('~/svn_trunk/publications/adaptive_sampling/fourierbasis',[bname '_noise.eps']);
saveas(fh(1).hf,fname1,'epsc2');
saveas(fh(2).hf,fname2,'epsc2');
%%
%make a plot of the original STRF's in this case 
pind=0;
for dind=dtoplot
    v=load(getpath(dsets(dind).datafile));
    
    [t,f]=getstrftimefreq(v.bssimobj.stimobj.bdata);
    twidth=getobsrvtlength(v.bssimobj.stimobj.bdata);
    
    pind=pind+1;
    fstrf(pind)=plottheta(v.bssimobj.mobj,getm(v.bssimobj.allpost,v.bssimobj.niter),t,f,twidth);

    
    fstrf(pind).width=width;
    fstrf(pind).height=height;
end

for p=1:length(dtoplot)
   fname=fullfile(bdir,sprintf('strfnosmoothing_%03g.eps',p));
   saveas(fstrf(p).hf,fname,'epsc2');
end

%%
%******************************************************************
%Make a plot of the STRF's in the fourier domain and spectral domain
%*******************************************************************
pind=0;
for dind=dtoplot
    v=load(getpath(dsets(dind).datafile));
    
    nmax=ModBSSinewaves.maxn([v.bssimobj.mobj.klength, v.bssimobj.mobj.ktlength]);   
    msin=ModBSSinewaves('nfreq',nmax(1),'ntime',nmax(2),'mobj',v.bssimobj.mobj);
    
    
    [t,f]=getstrftimefreq(v.bssimobj.stimobj.bdata);
    twidth=getobsrvtlength(v.bssimobj.stimobj.bdata);
    
    pind=pind+1;
    
    fstrf(pind)=plottheta(msin,msin.fullbasis'*getm(v.bssimobj.allpost,v.bssimobj.niter));

    title(fstrf(pind).a(1,1),sprintf('STRF %s', dsets(dind).lbl));
    
    fstrf(pind).width=width;
    fstrf(pind).height=height;
end


 for p=1:length(dtoplot)
    fname=fullfile(bdir,sprintf('strffreq_%03g.eps',p));
    saveas(fstrf(p).hf,fname,'epsc2');
 end
