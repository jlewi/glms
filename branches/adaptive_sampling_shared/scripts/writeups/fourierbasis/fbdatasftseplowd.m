%These datasets all used infomax. But we varied the variance for the high
%frequencies. The cutoff frequency was the same for all models
%i.e for nf>4 and nt>3 we set the variance of our prior to a different
%value
function [dfiles,pstyles]=fbdatasoftcutoff()

dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'colors','lstyles','markers'};
pstyles.linewidth=pstyles.linewidth-.25;

tanparam.rank=2;
tanparam.class='MBSFTSepLowRank';


%*************************************************************************
%Shuffled.: 
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_004';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Shuffled: ');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Shuffled '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max.: use tangent posterior
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_004';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Shuffled: tan post.');
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo=tanparam;
explain={'Method:', 'Shuffled '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max.: 
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_005';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: ');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max.: use tangent posterior
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_005';
cvar=10^0;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: tan post.');
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo=tanparam;
explain={'Method:', 'Infomax '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max. tan: 
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_006';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: rank=2');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax. Tan '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;


%*************************************************************************
%info. max. tan: 
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_006';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max. Tan: tan. post.');
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo=tanparam;
explain={'Method:', 'Infomax. Tan '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;


%*************************************************************************
%info. max. tan: startfullmax=50;
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_007';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max. Tan: startfull=50');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax. Tan '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;


%*************************************************************************
%info. max. tan: startfullmax=50;
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_007';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max. Tan: tan. post. startfull=50');
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo=tanparam;
explain={'Method:', 'Infomax. Tan '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;


%*************************************************************************
%info. max. tan: rank 5 tangent space;
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_008';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: rank=5');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax. rank=5 '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max. tan: rank 5 tangent space; use posterior on tanget space
%************************************************************************
bdir='081028';
bfile='bsinfomax_setup_008';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max. Tan: tan rank=5');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax. tangent post rank=5 '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;
