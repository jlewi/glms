%make a plot of the image statistics for low d stimuli
%
%low d
%%
bdir='~/svn_trunk/publications/adaptive_sampling/fourierbasis';
width=2.5;
height=2.5;
param.freqsubsample=2;
bdata=BSSetup.initbsdata(param);

indsong=find(waveissong(bdata,[1:getnwavefiles(bdata)]));
indnoise=find(~waveissong(bdata,[1:getnwavefiles(bdata)]));


songstats=BSStimStats.comp(bdata,indsong);
fsong=BSStimStats.plot(songstats);

noisestats=BSStimStats.comp(bdata,indnoise);
fnoise=BSStimStats.plot(noisestats);

fnoise.width=width;
fnoise.height=height;
fsong.width=width;
fsong.height=height;
%%
switch param.freqsubsample
    case 1
    fname1=fullfile(bdir,'songstatshighd.eps');
fname2=fullfile(bdir,'noisestatsghighd.eps');
    case 2
fname1=fullfile(bdir,'songstatslowd.eps');
fname2=fullfile(bdir,'noisestatslowd.eps');
end
saveas(fsong.hf,fname1,'epsc2');
saveas(fnoise.hf,fname2,'epsc2');

