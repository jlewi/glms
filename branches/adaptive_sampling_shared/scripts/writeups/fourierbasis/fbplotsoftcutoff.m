%10-19-2008
%
%Make plots illustrating that we can control the effct of the bias
%by setting our prior covariance to zero

clear all;
setpathvars;

%WARNING: index into dsets are likely to change you want the following
% Shuffled - use full posterior without smoothing
% Info. max using full posterior without smoothing

bdir='~/svn_trunk/publications/adaptive_sampling/fourierbasis';
[dsets,pstyles]=fbdatasoftcutoff();
dtoplot=1:length(dsets)-1;
width=3.5;
height=3.5;


%*****************************************************************
%make a plot of the STRF on the final trial for each simulation
%make a plot of the original STRF's in this case 
pind=0;
for dind=dtoplot
    v=load(getpath(dsets(dind).datafile));
    
    [t,f]=getstrftimefreq(v.bssimobj.stimobj.bdata);
    twidth=getobsrvtlength(v.bssimobj.stimobj.bdata);
    
    pind=pind+1;
    fstrf(pind)=plottheta(v.bssimobj.mobj,getm(v.bssimobj.allpost,v.bssimobj.niter));

    
    fstrf(pind).width=width;
    fstrf(pind).height=height;
end

for p=1:length(dtoplot)
   fname=fullfile(bdir,sprintf('strfprior_%03g.eps',p));
   saveas(fstrf(p).hf,fname,'epsc2');
end


%%
%**************************************************************************
%
fh=BSExpLLike.plot(dsets,pstyles);

p=1;
fname=fullfile(bdir,sprintf('softcutoffllike_%03g.eps',p));
saveas(fh(p).hf,fname,'epsc2');

p=2;
fname=fullfile(bdir,sprintf('softcutoffllike_%03g.eps',p));
saveas(fh(p).hf,fname,'epsc2');