%Datasets to compare the effect of smoothing 
%after the experiment is over. 
%
%We ran the experiments using MParamObj objects i.e. during the experiment
%the STRF is represented in the spectral domain (not the fourier domain).
%
%After the experiment, we regularize the posterior by representing the
%posterior in the fourier space, and throwing out higher frequencies.
%
function [dfiles,pstyles]=fbdatasmooth()

dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'lstyles','colors','markers'};
pstyles.linewidth=pstyles.linewidth;

smooth.nfcutoff=15;
smooth.ntcutoff=3;


%**********************************************************
%Batch Shuffle: no smoothing
%**************************************************************
dind=dind+1;
bdir='080828';
bfile='bsinfomax_setup_003';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).explain='Shuffled. Use full posterior.';
dfiles(dind).lbl='Shuffled: no smoothing';

%*************************************************************
%batch shuffle smoothing
%*************************************************************
%use the posterior on the tangent space
bdir='080828';
bfile='bsinfomax_setup_003';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).taninfo=[];
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Shuffled: smoothing';
explain={'Method:', 'Shuffled'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Use tanpost:', dfiles(dind).usetanpost}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max.: no smoothing 
%************************************************************************
bdir='080921';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max:no smoothing';
dfiles(dind).usetanpost=false;


%*************************************************************************
%info. max.: no smoothing 
%************************************************************************
%full posterior + smoothing
bdir='080921';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).taninfo=[];
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Info. Max: smoothing';
explain={'Method:', 'Infomax'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Use tanpost:', dfiles(dind).usetanpost}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;
