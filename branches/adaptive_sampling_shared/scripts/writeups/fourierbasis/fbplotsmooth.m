%10-11-2008
%
%Make plots illustrating the overfitting that results when we don't
%smooth the strf

clear all;
setpathvars;

%WARNING: index into dsets are likely to change you want the following
% Shuffled - use full posterior with smoothing
% Info. max using full posterior with smoothing

bdir='~/svn_trunk/publications/adaptive_sampling/fourierbasis';
[dsets,pstyles]=fbdatasmooth();

%width and height for the plots
width=4;
height=4;
%%
%select the data sets corresponding to shuffled designs
%and info. max.
dtoplot=[1:length(dsets)];

%plot the expected log likelihood
fh=BSExpLLike.plot(dsets(dtoplot),pstyles);
fh(1).width=width;
fh(2).width=width;
fh(1).height=height;
fh(2).height=height;
bname='expllikesmoothing';

fname1=fullfile('~/svn_trunk/publications/adaptive_sampling/fourierbasis',[bname '_birdsong.eps']);
fname2=fullfile('~/svn_trunk/publications/adaptive_sampling/fourierbasis',[bname '_noise.eps']);
saveas(fh(1).hf,fname1,'epsc2');
saveas(fh(2).hf,fname2,'epsc2');
%%
%******************************************************************
%Make a plot of the smoothed STRF's in the fourier domain and spectral domain
%*******************************************************************
pind=0;

for dind=dtoplot
    v=load(getpath(dsets(dind).datafile));
    
    %only make a plot if we applied smoothing
    %the full strfs are plotted elsewhere
    if ~(isempty(dsets(dind).smooth))
        
    nmax=ModBSSinewaves.maxn([v.bssimobj.mobj.klength, v.bssimobj.mobj.ktlength]);   
    msin=ModBSSinewaves('nfreq',nmax(1),'ntime',nmax(2),'mobj',v.bssimobj.mobj);
    theta=msin.fullbasis'*getm(v.bssimobj.allpost,v.bssimobj.niter);
    
     theta=smooth(msin,theta,dsets(dind).smooth.nfcutoff,dsets(dind).smooth.ntcutoff);
    
    
    [t,f]=getstrftimefreq(v.bssimobj.stimobj.bdata);
    twidth=getobsrvtlength(v.bssimobj.stimobj.bdata);
    
    pind=pind+1;
    
   
    fstrf(pind)=plottheta(msin,theta);

    fstrf(pind).width=width;
    fstrf(pind).height=height;
end
end

 for p=1:pind
    fname=fullfile(bdir,sprintf('strffreqsmooth_%03g.eps',p));
    saveas(fstrf(p).hf,fname,'epsc2');
 end
