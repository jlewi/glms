%**************************************************************************
%
%Explanation: Setup the info. max. simulations when we represent theta in
%the fourier space and we apply the fourier transform to time and frequency
%separatly
%
% Updater uses batch methods on the first n trials
%Theta is represented in the fourier domain
function dsets=fbsetupftsephighdreplaybatch()

setpathvars;
dsets=[];
dind=0;

%this is the previous simulation which we want to redo with a different
%updater
simfile=FilePath('RESULTSDIR',fullfile('bird_song','081111','bsinfomax_data_001.mat'));

%load the params used to establish that simulation
v=load(getpath(simfile));
allparam=v.bssimobj.extra.param;


%change the stimulus object so we replay the simulation
allparam.stimtype='BSStimReplay';

allparam.stimparam.simfile=simfile;

%change the updater.
allparam.updater.type='BSNewtonBatchInit';
allparam.updater.ninit=1500;
allparam.updater.online=Newton1d('compeig',false);
allparam.updater.batch=BatchML();

% %************************************************************
% replay using batch init
% %************************************************************
dind=dind+1;
dnew=BSSetup.setupinfomax(allparam);
dsets=copystruct(dsets,dind,dnew);

