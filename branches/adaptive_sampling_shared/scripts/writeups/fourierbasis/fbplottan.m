%10-17-2008
%
%Make plots evaluating the tangent space results

clear all;
setpathvars;

%WARNING: index into dsets are likely to change you want the following
% Shuffled - use full posterior with smoothing
% Info. max using full posterior with smoothing

bdir='~/svn_trunk/publications/adaptive_sampling/fourierbasis';
[dsets,pstyles]=fbdatatan();


%width and height for the plots
width=4;
height=4;
%%
%select the data sets corresponding to shuffled designs
%and info. max.
dtoplot=[1:length(dsets)];

%plot the expected log likelihood
fh=BSExpLLike.plot(dsets(dtoplot),pstyles);
fh(1).width=width;
fh(2).width=width;
fh(1).height=height;
fh(2).height=height;
bname='explliketan';

fname1=fullfile('~/svn_trunk/publications/adaptive_sampling/fourierbasis',[bname '_birdsong.eps']);
fname2=fullfile('~/svn_trunk/publications/adaptive_sampling/fourierbasis',[bname '_noise.eps']);
%saveas(fh(1).hf,fname1,'epsc2');
%saveas(fh(2).hf,fname2,'epsc2');

%%
%***********************************************************************
%plot the STRF
%***********************************************************************
for dind=1:length(dsets)

    v=load(getpath(dsets(dind).datafile));
plottheta(v.bssimobj.mobj,getm(v.bssimobj.allpost,v.bssimobj.niter));

end
