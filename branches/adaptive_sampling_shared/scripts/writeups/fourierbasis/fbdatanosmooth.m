%Datasets without smoothing
%
%The modle object is MParamObj - i.e the STRF is represented in the
%spectral domain not the fourier domain.
%
function [dfiles,pstyles]=fbdatanosmooth()

dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'lstyles','colors','markers'};
pstyles.linewidth=pstyles.linewidth;

smooth.nfcutoff=10;
smooth.ntcutoff=3;


%**********************************************************
%Batch Shuffle
%**************************************************************
dind=dind+1;
bdir='080828';
bfile='bsinfomax_setup_003';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).explain='Shuffled. Use full posterior.';
dfiles(dind).lbl='Shuffled';



%*************************************************************************
%info. max.
%************************************************************************
bdir='080921';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max';
dfiles(dind).usetanpost=false;