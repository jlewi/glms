%**************************************************************************
%
%Explanation: Setup the simulation for fitting a low d model using batch
%updates
function dsets=fbsetupftsepfitlowd()


dsets=struct;
dind=0;
    

%try decreasing the bin size and increasing the number of time bins
allparam.obsrvwindowxfs=124;
allparam.stimnobsrvwind=20;
allparam.freqsubsample=1;
allparam.model='MBSFTSep';



scale=10;
allparam.prior.stimvar=10^-2*scale;
allparam.prior.shistvar=1*scale;
allparam.prior.biasvar=10*scale;
%create a bdata an mobject to use to decide which frequency components to
%include
bdata=BSSetup.initbsdata(allparam);

strfdim=getstrfdim(bdata);
mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.alength=0;
mparam.hasbias=0;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mobj=MBSFTSep(mparam);


%  nfreq=20;
% ntime=4;
nfreq=10;
 ntime=4;
subind=bindexinv(mobj,1:mobj.nstimcoeff);
btouse=subind(:,1)<=nfreq & subind(:,2)<=ntime;
btouse=find(btouse==1);

allparam.mparam.btouse=btouse;


%how many repeats of the data to use
allparam.stimparam.nrepeats=10;

%************************************************************
%Shuffled
%************************************************************
dind=dind+1;
dnew=BSSetup.setupfitpoiss(allparam);
dsets=copystruct(dsets,dind,dnew);
