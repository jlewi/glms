%These datasets all used infomax. But we varied the variance for the high
%frequencies. The cutoff frequency was the same for all models
%i.e for nf>4 and nt>3 we set the variance of our prior to a different
%value
function [dfiles,pstyles]=fbdatasoftcutoff()

dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'colors','lstyles','markers'};
pstyles.linewidth=pstyles.linewidth-.25;

tanparam.rank=2;
tanparam.class='MBSFTSepLowRank';


%*************************************************************************
%Shuffled.: 
%************************************************************************
bdir='081107';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Shuffled: ');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Shuffled '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;


%*************************************************************************
%info. max.: 
%************************************************************************
bdir='081107';
bfile='bsinfomax_setup_003';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.:');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max. tan: 
%************************************************************************
bdir='081107';
bfile='bsinfomax_setup_004';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: rank=2');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax. Tan '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{}];
dfiles(dind).explain=explain;


