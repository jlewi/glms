%12-05
%Script to plot the variance of the ekf prior and the gaussian
%approximating the likelihood after each iteration
%the goal is to illustrate that the small variance of the prior compared to
%the likelihood explains the slow rollof in bias.

obsrv.twindow=mparam.tresponse;
%for exach observation compute the inverse of the variance of the likelihood function
lambdall=zeros(1,simparam.niter);
for tindex=1:simparam.niter
    %x is the stimulus
    x=sr.y(:,tindex);
    [r0,r1,r2]=glm1dexp(pmethods.ekf.m(:,tindex)'*x);

    twin=obsrv.twindow;
    r0s=r0*obsrv.twindow;
    r1s=r1*obsrv.twindow;
    r2s=r2*obsrv.twindow;

    %g is the log likelihood up to a scaling factor
    obsrv.n=sr.nspikes(tindex);
    g=-r0s+ obsrv.n*log(r0s);
    gd1=-r1s+obsrv.n*1/(r0s)*r1s;
    gd2=-r2s+obsrv.n*(-1/(r0s^2)*(r1s)^2+1/r0s*r2s);

    %sigma=x'*prior.c*x;

    %compute the mean of the likelihood function 
    mll=-gd1/gd2*x/(x'*x)+pmethods.ekf.m(:,tindex);

    %compute lambda the inverse of the covariance matrix of the 
    %gaussian approximating the log likelihood function (this will be singular)
    lambdall(tindex)=-gd2*x*x';
end

%make a plot
fcvar.hf=figure;
fcvar.hp=[];
figure;hold on;
fcvar.h_p(1)=plot(1:simparam.niter,log(1./lambdall),getptype(1,1));
plot(1:simparam.niter,log(1./lambdall),getptype(1,2));
pc=cell2mat(pmethods.ekf.c);
pc=pc(1:simparam.niter);
fcvar.h_p(2)=plot(1:simparam.niter,log(pc),getptype(2,1));
plot(1:simparam.niter,log(pc),getptype(2,2));
xlabel('Iteration');
ylabel('Log_1_0(Variance)');
legend(fcvar.h_p,{'Likelihood','Prior'});
xlim([0 500])


