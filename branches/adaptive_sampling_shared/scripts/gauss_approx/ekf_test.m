%Paninski's code to compute ekf in 1-d using EKF and using newton
clear;
N=2000;
k_true=2;
prior_weight=1; %inverse of prior variance
N_steps=.1;
xs=randn(N,1);
is=poissrnd(exp(xs*k_true));

%L=-exp(k*x)+i*k*x;
ds=zeros(size(is));
d2s=zeros(size(is));
ekf_ks=zeros(size(is));
newton_ks=zeros(size(is));

%EKF
k=0; %prior mean
A=-prior_weight;
for(j=1:N)
	ds(j)=is(j)*xs(j)-xs(j)*exp(k*xs(j));
	d2s(j)=-exp(k*xs(j))*xs(j)^2;
	A=A+d2s(j);
	k=k-A\ds(j); %this notation generalizes to the vector case [and is done in O(d^2) time, instead of O(d^3) required of inv(A)*ds(j)]
	ekf_ks(j)=k;
end;

%Newton
k=0;
for(j=1:N)
	if(N_steps>=1) %run N_steps Newton steps each time
		for(jj=1:N_steps)
			ds(1:j)=is(1:j).*xs(1:j)-xs(1:j).*exp(k*xs(1:j));
			d2s(1:j)=-exp(k*xs(1:j)).*xs(1:j).^2;
			A=-prior_weight+sum(d2s(1:j));
			k=k-A\sum(ds(1:j));
		end;
	else %do Newton with probability N_steps
		if(rand<N_steps) %do Newton
			ds(1:j)=is(1:j).*xs(1:j)-xs(1:j).*exp(k*xs(1:j));
			d2s(1:j)=-exp(k*xs(1:j)).*xs(1:j).^2;
			A=-prior_weight+sum(d2s(1:j));
			k=k-A\sum(ds(1:j));
		else %do EKF, assuming A is the current inverse covariance and the current mean is k
			ds(j)=is(j)*xs(j)-xs(j)*exp(k*xs(j));
			d2s(j)=-exp(k*xs(j))*xs(j)^2;
			A=A+d2s(j);
			k=k-A\ds(j);
		end;
	end;
	newton_ks(j)=k;
end;

%full newton optim given all the data
k=0; k_old=1;
while(norm(k_old-k)>1e-6)
	k_old=k;
	ds=is.*xs-xs.*exp(k*xs);
	d2s=-exp(k*xs).*xs.^2;
	k=k-(-prior_weight+sum(d2s))\sum(ds);
end;
disp(sprintf('k_MAP = %2.3f; k_EKF = %2.3f; k_EKFN = %2.3f; norm err = %2.3f;',k,ekf_ks(end),newton_ks(end),(newton_ks(end)-k)*sqrt(N)));

figure(1); 
subplot(211);
plot(1:N,ekf_ks,'r',1:N,newton_ks,'b',1:N,k_true*ones(N,1),'k');
legend('ekf','newton','true',0);

subplot(212);
NN=(ceil(N/2):N)';
plot(NN,sqrt(NN).*(newton_ks(NN)-k_true),'k');
legend('root-n * error of newton',0);
