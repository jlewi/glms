%script to test object POptAsymGPLB
%
%Compute the optimal GP for a specific theta. 
%Then compare that design to an i.i.d gp

mp.glm=GLMPoisson('canon');
mp.mmag=1;
mp.klength=1;
mp.ktlength=3;
mobj=MParamObj(mp);

dimtheta=getparamlen(mobj);

ttrue=[1;-1;1];
truedir=normmag(ttrue);
trueproj=ttrue'*truedir;

mu=0;
c={[1] [0] [0]};
gpiid=StimGP('mu',mu,'c',c);

% mu=0;
% evec=normmag([[1;-1] null([1;-1]')]);
% cs=evec*diag([1.8 .1])*evec';
% c={[cs(1,1)] [cs(1,2)]};
% gpinit=StimGP('mu',mu,'c',c);
gpinit=gpiid;

if (any(eig(gpinit.cs)<0))
    error('cs is not positive definite');
end

sobj=POptAsymGPPCanon('klength',mp.klength,'ktlength',mp.ktlength);


pcon=1;
[gpopt,negffun,exitflag,output,thetadir,thetaproj]=optgp(sobj,[],pcon,truedir,trueproj,gpinit);

finit=exlogdetexss(sobj,[],gpinit, thetadir, thetaproj);
fiid=exlogdetexss(sobj,[],gpiid, thetadir, thetaproj);
fopt=exlogdetexss(sobj,[],gpopt, thetadir, thetaproj);