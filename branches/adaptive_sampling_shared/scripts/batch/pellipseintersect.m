%08-07-2008
%
%
%To compute the probability that some value is the 
%max eigenvalue of |Ib +UCtB| we need to determine
%the domain of rho over which to integrate p(rho|mu_t,c_t)
%
%The domain is defined by two quadratic eqns.
%
%This script plots the domain in rho in 2-d for the canonical poisson so
%that we can get some intuition into the region.

clear variables;
%1. Make a random, symmetric positive definite matrix which defines our
%ellipse
vec=normmag([1 -1; 1 1]);
emat=vec*diag([2 1]) *vec';

%lambda max is the max eigenvalue whose probability we are computing
%it must be greater than 1
lmax=1.2;

%grid points to compute rho on
rho1=[-5:.1:5];
rho2=[-5:.1:5];



%drho is 0 or 1 indicating whether each point is in the domain
drho=zeros(length(rho1),length(rho2));

for r1=1:length(rho1)
    for r2=1:length(rho2);
        jd=diag([exp(.5*rho1(r1)) exp(.5*rho2(r2))]);
        
        m=jd*emat*jd;
        
        emv=eig(m);
        
        if ((min(emv)<= (lmax-1)) && ((lmax-1)<max(emv)))
           drho(r2,r1)=1;        
        else
           drho(r2,r1)=0; 
        end
    end
end



%*****************************************************
%make a plot
%****************************************************
%%
frho=FigObj('name','Domain of rho','xlabel','\rho_1','ylabel','\rho_2');

[x,y]=meshgrid(rho1,rho2);

ind=find(drho==1);
hp=plot(x(ind),y(ind),'o');
set(hp,'MarkerFaceColor','b');
set(hp,'MarkerSize',6);

lblgraph(frho);
