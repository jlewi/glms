function [dsets,varargout]= gp_data_infomax_090204_002() 
%********************************


dsets=[];
dind=1;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_015.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_015.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_015.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_015.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_015.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_015.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_015.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=2;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_016.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_016.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_016.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_016.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_016.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_016.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_016.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=3;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_017.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_017.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_017.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_017.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_017.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_017.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_017.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=4;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_018.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_018.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_018.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_018.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_018.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_018.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_018.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=5;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_019.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_019.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_019.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_019.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_019.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_019.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_019.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=6;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_020.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_020.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_020.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_020.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_020.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_020.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_020.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=7;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_021.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_021.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_021.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_021.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_021.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_021.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_021.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=8;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_022.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_022.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_022.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_022.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_022.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_022.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_022.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=9;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_023.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_023.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_023.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_023.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_023.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_023.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_023.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=10;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_024.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_024.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_024.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_024.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_024.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_024.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_024.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=11;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_025.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_025.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_025.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_025.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_025.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_025.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_025.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=12;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_026.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_026.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_026.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_026.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_026.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_026.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_026.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=13;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_027.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_027.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_027.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_027.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_027.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_027.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_027.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=14;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_028.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_028.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_028.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_028.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_028.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_028.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_028.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];

if (nargout>=2)
   for dind=1:length(dsets)
      fprintf('Loading %d of %d \n', dind,length(dsets));
v=load(getpath(dsets(dind).datafile));
      simobj(dind)=v.simobj;
   end
   varargout{1}=simobj;
end
