function [dsets]= gp_data_infomax_090203_001() 
%********************************


dsets=[];
dind=1;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_001.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_001.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_001.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_001.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_001.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_001.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_001.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=2;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_002.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_002.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_002.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_002.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_002.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_002.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_002.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=3;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_003.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_003.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_003.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_003.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_003.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_003.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_003.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=4;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_004.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_004.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_004.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_004.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_004.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_004.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_004.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=5;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_005.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_005.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_005.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_005.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_005.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_005.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_005.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=6;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_006.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_006.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_006.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_006.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_006.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_006.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_006.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=7;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_007.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_007.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_007.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_007.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_007.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_007.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_007.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=8;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_008.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_008.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_008.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_008.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_008.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_008.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_008.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=9;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_009.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_009.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_009.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_009.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_009.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_009.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_009.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=10;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_setup_010.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_data_010.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_mfile_010.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_cfile_010.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_status_010.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_inputs_010.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090203/gpsims_obsrv_010.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


