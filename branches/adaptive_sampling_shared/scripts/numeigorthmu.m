%5-20-07
%
%make a plot of the number of eigenvectors not orthogonal to the mean
%we count this as the number of eigenvectors which account for sum
%percentage of the energy 

dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001.mat');
dsets(end).lbl='power';
dsets(end).simvar='simmax';


percente=.99;           %what percentage of the energy we want count
subsamp=100;

%*****
%%
for dind=1:length(dsets)
    %[pdata, sim, mobj, sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);

    %we will need to fill in the covariance matrices
    %[pdata]=fillinlowmem(dsets(dind).fname,dsets(dind).simvar,[]);
   dsets(dind).ncorr=zeros(1,sim.niter+1);
  dsets(dind).ncorr(:)=nan;
for j=1:subsamp:sim.niter+1
    if(mod(j,100)==0)
        fprintf('j=%d\n',j);
    end
    if ~isempty(pdata.c{j})
            [dsets(dind).eig(j).evecs dsets(dind).eig(j).eigd]=svd(pdata.c{j});
            %normalized energy
            nenergy=(normmag(pdata.m(:,j))'*dsets(dind).eig(j).evecs).^2;
    nenergy=sort(nenergy,'descend');        
    ind=find(cumsum(nenergy)>=percente);
            dsets(dind).ncorr(j)=ind(1);
    else
        dsets(dind).ncorr(j)=nan;
    end
end
end

fncorr.hf=figure;
fncorr.xlabel='trial';
fncorr.ylabel='number';
fncorr.name='ncorr';

hold on;
for dind=1:length(dsets)
    plot(0:subsamp:(length(dsets(dind).ncorr)-1),dsets(dind).ncorr(1:subsamp:end));
end
lblgraph(fncorr);
