clear variables;
setpathvars;

datedir='080604';
setupfile='bsglmfit_setup_001.m';
xscript(fullfile(RESULTSDIR,'bird_song',datedir,setupfile));

data=load(getpath(dsets.datafile));

bdata=data.bdata;
mobj=data.mobj;

%prior=getpost(data.allpost,0);
prior=GaussPost('m',data.fmap(end).theta,'c',getc(data.allpost,0));
%postobj=BSlogpost('windexes',1,'bdata',bdata);

bsopt=BSOptStim('data',dsets.datafile,'windexes',1,'miobj',PoissExpCompMI());

poolstim=compmutualinfo(postobj,prior,mobj);


