%date: 10-29-2008
%
%Explanation: For the bird song FFT applied separatly, plot the strf on
%several trials
setpathvars;

[dsets,pstyles]=nips08databstanhighd;
dsets(2).lbl=sprintf('Info. Max.\n full');
%[dsets,pstyles]=fbdatasftseplowd;
%[dsets,pstyles]=fbdatasftsephighd;

%make plots for poster or paper
poster=false;
if (poster)
width=7.5;
height=3;

gdir='~/svn_trunk/publications/adaptive_sampling/nips08/poster_figs';
%extension for graphics
fext='.png';

trials=[1000 2500 5000 7500 10000 20000 50000 100000];
gtype='png';
fsize=9;
else
    %width and height for the
width=5.3;
height=3;


trials=[1000 2500 5000 7500 10000 20000 50000];
gdir='~/svn_trunk/publications/adaptive_sampling/nips08/figs';
fext='.eps';
gtype='epsc2';

fsize=8;

%colormap('gray');
end
%select the datasets corresponding to
%1. shuffled
%2. info. max.
%3. info. max. tan rank=2
%4. info. max. tan rank=5
%dsets=dsets([1:2:5]);


%trials=[1000:250:2000];
%trials=[500 750 1000 1500 5000 10000];

%for high d
%trials=[100 1000 5000 10000];
%trials=[1500:500:4500];
%trials=[1000:250:2000];
%trials=[500 1000 1250 1500 1750 10000];

nrows=length(dsets);

fh=FigObj('name','STRFs','width',width,'height',height,'naxes',[nrows length(trials)],'fontsize',fsize);

%**************************************************************************
%loop over the datesets and plot the strfs
%*********************************************************
for dind=1:length(dsets)
   for tind=1:length(trials)
      trial= trials(tind);
      
      v=load(getpath(dsets(dind).datafile));
      
       [t,freqs]=getstrftimefreq(v.bssimobj.stimobj.bdata);
      if (trial<=v.bssimobj.niter)
         setfocus(fh.a,dind,tind);
         strf=getstrf(v.bssimobj.mobj,getm(v.bssimobj.allpost,trial));
          
         %multiply t by 1000 so its in ms
         t=t*1000;
         
         %divide freqs by 1000 so its in khz
         freqs=freqs/1000;
         imagesc(t,freqs,strf);
      end
      
   
    
end
end
%add a colorbar to the final image in the first row
dind=1;
tind=length(trials);

         setfocus(fh.a,dind,tind);
          %add a colorbar
         fh.a(dind,tind).hc=colorbar;

      

%%
%turn off all tickmars
set(fh.a,'xtick',[]);
set(fh.a,'ytick',[]);

%set x and y limits
set(fh.a,'xlim',[t(1) t(end)]);
set(fh.a,'ylim',[freqs(1) freqs(end)]);

%turn on xtick,ytick for appropriate graphs
set([fh.a(length(dsets),:)],'xtickmode','auto');
set([fh.a(:,1)],'ytickmode','auto');
set([fh.a(length(dsets),:)],'xticklabel',[]);
%********************************************************
%adjust labels

%make the color limits the same for all axes
% clim=get(fh.a,'clim');
% clim=cell2mat(clim);
% clim=[min(clim(:,1)) max(clim(:,2))];

%clim=[-5*10^-3 10^-2];
clim=[-2*10^-3 2*10^-3];
set(fh.a,'clim',clim);


%add ylabels
for dind=1:length(dsets)
    ylbl='';
    lbl=dsets(dind).lbl;
    %split the lbl based on :
    sind=strfind(lbl,':');
    if isempty(sind)
        ylbl=lbl;
    else
        while ~isempty(sind)
           ylbl=sprintf('%s\n%s',ylbl,lbl(1:sind(1)-1));
           lbl=lbl(sind(1)+1:end);
           sind=sind(2:end);
        end
        ylbl=sprintf('%s\n%s',ylbl,lbl);
    end

    if (dind==length(dsets))
       ylbl=sprintf('%s\nFrequency (KHz)',ylbl);
    else
        %don't add ticklables
        set(fh.a(dind,1),'YTickLabel',[]);
    end
    
       ylabel(fh.a(dind,1),ylbl);
    
end

%manually align the ylabels
pos=get(fh.a(length(dsets),1).ylbl.h,'position');
leftpos=-79;
for rind=1:length(dsets)-1
   lpos= get(fh.a(rind,1).ylbl.h,'position');
   lpos(1)=leftpos;
   set(fh.a(rind,1).ylbl.h,'position',lpos);
end

%add titles 
for tind=1:length(trials)
    if (trials(tind)>=10000)
        tl=sprintf('Trial %02gk',trials(tind)/1000);
    else
       tl=sprintf('Trial %03g',trials(tind));
    end
    title(fh.a(1,tind),tl);
end

%add xlabels
for tind=1:1
%add xlabels
    set(fh.a(nrows,tind),'xtick',[-40 -20 0]);
    set(fh.a(nrows,tind),'xticklabel',[-40 -20 0]);
   xlabel(fh.a(nrows,tind),sprintf('Time(ms)'));
end


lblgraph(fh);
space.cbwidth=.15;
sizesubplots(fh,space);

bname='bsstrf';

fname1=fullfile(gdir,[bname fext]);
saveas(fh(1).hf,fname1,gtype);
