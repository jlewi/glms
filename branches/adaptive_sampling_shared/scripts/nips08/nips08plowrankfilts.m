%make plots of the true filter components for the auditory example.


%new data set
dsets=[];
setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080311','dataset_lowrank_080311_001.m');
xscript(setupfile);




[simobj]=SimulationBase('fname',getpath(dsets(dind).fname),'simvar',dsets(dind).simvar);
extra=getextra(simobj);
%%
gparam=[];
gparam.height=1;
gparam.width=2.2;

%iwidth - what % of the width is for the image of the combined filter
%fwidth - what % of the width is for the plot of filter component
gparam.iwidth=.2;
gparam.fwidth=.05;

%iheight - what % of the height is for the image of the combined filter
%fheight - what % of the height is for the plot of time component
gparam.iheight=.5;
gparam.theight=.2;

gparam.ibottom=.2;

%spacing between the components of the rank-1 matrices
%and the plots of the actual rank 1 matrix 
%we scale the spacing based on the dimensions of the figure so that
%the vertical and horizontal spacing are the same.
gparam.stofiltshorz=.0177;
gparam.stofiltsvert=gparam.stofiltshorz*gparam.width/gparam.height;

%how much of a border to leave on left and right side
gparam.bhorz=.0177;
%spacing between the plots of the matrices
gparam.mspace=(1-(gparam.fwidth)*2-gparam.iwidth*3-gparam.stofiltshorz*2-gparam.bhorz*2)/2;


%set the left position of each matrix 
gparam.ileft(1)=gparam.fwidth+gparam.stofiltshorz+gparam.bhorz;
gparam.ileft(2)=gparam.ileft(1)+gparam.iwidth+gparam.fwidth+gparam.stofiltshorz+gparam.mspace;
gparam.ileft(3)=gparam.ileft(2)+gparam.iwidth+gparam.mspace;
  
gparam.clim=[-1 1];
gparam.linewidth=2;



hf=[];

    hf=figure;
    setfsize(hf,gparam);
    
for fi=1:2
    if (fi==1)
        filts.f=extra.f1;
        filts.t=extra.t1;
    else
        filts.f=extra.f2;
        filts.t=extra.t2;
    end
    filts.theta=filts.f*filts.t';


    %plot the main filter
    ha(fi).main=axes;
    imagesc(filts.theta,gparam.clim);
    set(gca,'ydir','reverse');
    set(gca,'yaxislocation','right');
    mpos=get(ha(fi).main,'position');
    

    
    
    mpos=[gparam.ileft(fi) gparam.ibottom gparam.iwidth gparam.iheight];
    set(gca,'position',mpos);
    
    
set(gca,'xtick',[]);
set(gca,'ytick',[]);
    %***************************************************
    %plot the temporal filter
    %**************************************************
    ha(fi).tfilt=axes;
    hp=plot(filts.t);
    set(hp,'linewidth',gparam.linewidth);

    set(gca,'xtick',[]);
    set(gca,'ytick',[-1 1]);
    
    tpos=get(gca,'position');
    tpos=[mpos(1) mpos(2)+gparam.iheight+gparam.stofiltsvert gparam.iwidth gparam.theight];
    set(gca,'position',tpos);
    xlim([1 ,length(filts.t)]);
    
   %***************************************************
    %plot the freq filter
    %***************************************************
    ha(fi).ffilt=axes;
   

    %we want this axes to be oriented vertically
    %so we need to effectively switch the x and y axes
    hp=plot(filts.f,1:length(filts.f));
    set(hp,'linewidth',gparam.linewidth);

    set(gca,'ytick',[]);
    set(gca,'xtick',[-1 1]);
    %put the y-axis on the right side
    set(gca,'YAxisLocation','right');
    set(gca,'xdir','reverse');
    set(gca,'ydir','reverse');

    fpos=get(gca,'position');
    fpos=[mpos(1)-gparam.fwidth-gparam.stofiltshorz mpos(2) gparam.fwidth gparam.iheight];
    set(gca,'position',fpos);
    ylim([1 ,length(filts.f)]);


    
end

%plot the true filter
fi=fi+1;
ha(fi).ffilt=axes;
mdim=length(filts.t);
imagesc(reshape(extra.ktrue,[mdim,mdim]),gparam.clim);
set(gca,'ydir','reverse');

ktpos=[gparam.ileft(fi) gparam.ibottom gparam.iwidth gparam.iheight];
set(gca,'position',ktpos);

set(gca,'xtick',[]);
set(gca,'ytick',[]);

%**************************************************************************
%add the text symbols
%**************************************************************************
%create an axes which fills the screen
fi=fi+1;
ha(fi).ffilt=axes;
%scale the axes to the full window and set its coordinates to 0,1
xlim([0 1]);
ylim([0 1]);
set(gca,'position',[0 0 1 1]);
axis off;

%+ symbol
txt.x=gparam.ileft(2)-gparam.fwidth-gparam.stofiltshorz-gparam.mspace/2;
txt.y=gparam.ibottom+gparam.iheight/2;
ht=text(txt.x,txt.y,'+');
set(ht,'fontsize',12);

%+ symbol
txte.x=gparam.ileft(3)-gparam.mspace/2;
txte.y=gparam.ibottom+gparam.iheight/2;
hequal=text(txte.x,txte.y,'=');
set(hequal,'fontsize',12);

foutfile=sprintf('~/svn_trunk/adaptive_sampling/writeup/nips08/figs/lowrank_true.eps');
%saveas(hf,foutfile,'epsc2');