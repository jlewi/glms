%**************************************************************************
%
%Explanation: Setup the info. max. simulations using the tangent space
%   Sets up the following simulations
%   1. shuffled
%   2. info. max.
%   3. info. max. tan with rank
%       rank =2
%       rank =4
%       rank =8
%
%Theta is represented in the fourier domain
function nips08setupbstaninfomax()

allparam.dataset='lowdfourier';
%allparam.pcutoff=FilePath('RESULTSDIR','bird_song/081019/bscinit_nfreq19_ntime4_001.mat');            
allparam.pcutoff.ntime=2;
allparam.pcutoff.nfreq=4;
allparam.pcutoff.var=10^-6;

%**************************************************************
%setup the infomax with the tangent space
%***************************************************************
tanparam=allparam;

tanparam.stimtype='BSBatchPoissLBTan';
for rank=[2 4 8];
    tanparam.tanparam.rank=rank;
    BSSetup.setupinfomax(tanparam);    
end

%************************************************************
%Info. Max.
%************************************************************
imparam=allparam;
imparam.stimtype='BSBatchPoissLB';
BSSetup.setupinfomax(imparam);


%************************************************************
%Shuffled
%************************************************************
shparam=allparam;
shparam.stimtype='BSBatchShuffle';

BSSetup.setupinfomax(shparam);
