%11-2008-
%To illustrate the batch optimization make plots of how each observation
%elimnates a subset of the possible logistic functions


fwidth=4;
fheight=2;
fsize=12;   %font size

%leave labels blank to add labels on the slide
xlbl='';    
ylbl='';


%**************************************************************************
%Plot several different logistic functions to illustrate the possible
%**************************************************************************
%%
param(1).centers=[-8:4:8];
param(1).ms=[.5, 1,2];
param(2).centers=[-8 0:4:8];
param(2).ms=param(1).ms;
param(3).centers=[-8 0 8];
param(3).ms=param(1).ms;
param(4).centers=[0 8];
param(4).ms=param(1).ms;
for pind=1:length(param)

    centers=param(pind).centers;
    ms=param(pind).ms;
mcolor={'r' 'g' 'b'};
dx=10;
x=[centers(1)-dx:.5:centers(end)+dx];

fmany=FigObj('width',2,'height',1,'fontsize',18);
hold on;

pstyle=[];
for cind=1:length(centers)
    for mind=1:length(ms)
    c=centers(cind);
    m=ms(mind);
        yv=1./(1+exp(-(x-c)/m));
       
     pstyle.linewidth=1;
     pstyle.linestyle='--';
     pstyle.color=mcolor{mind};
     
    hp=plot(x,yv);
    fmany.a=addplot(fmany.a,'hp',hp,'pstyle',pstyle);
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
%    xlim([centers(1)-ds centers(end)+dx]);
    end
end
xlim([-dx dx])
lblgraph(fmany);

gdir='~/svn_trunk/publications/adaptive_sampling/nips08/poster_figs/';
fname=sprintf('logisticobsrv_%03g.png',pind);
saveas(gethf(fmany),fullfile(gdir,fname));

end