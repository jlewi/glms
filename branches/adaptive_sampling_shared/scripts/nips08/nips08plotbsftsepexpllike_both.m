%date: 10-29-2008
%
%Explanation: For the bird song FFT applied separatly, plot the expected
%log likelihood comparing the info. max. and info. max. tangent spaces. 
%
%Here we make separate plots for both wave files
%

[dsets,pstyles]=nips08databstanhighd;




%make plots for poster or paper
poster=false;
if (poster)
%width and height for the plots
width=5;
height=1.5;
gdir='~/svn_trunk/publications/adaptive_sampling/nips08/poster_figs';
%extension for graphics
fext='.png';
gtype='png';

%titles for the figures
tl{1}='bird song';
tl{2}='noise';
else
    %width and height for the
width=6;
height=2;

gdir='~/svn_trunk/publications/adaptive_sampling/nips08/figs';
fext='.eps';
gtype='epsc2';

errorbars=false;

%titles for the figures
tl{1}='';
tl{2}='';
end


%change colors to be red green blue
pstyles.colors=eye(3);
fh=BSExpLLike.plot(dsets,pstyles,errorbars,100);

%************************************************************************
%adjust the plots
%************************************************************************
%%
%fontsize
fsize=10;

for ind=1:length(fh)
fh(ind).width=width;
fh(ind).height=height;
setfsize(fh(ind));
setfontsize(fh(ind).a.hlgnd,fsize);
autosizetight(fh(ind).a.hlgnd);
set(fh(ind).a,'xlim',[10^3,15000]);
set(fh(ind).a,'xtick',[10^3 10^4]);
end

ind=1;
set(fh(1).a,'ylim',[-1 0]);
%add a little space to prevent it being cut off when we save it
setposition(fh(ind).a.hlgnd,.5,.4,.35,[]);






 
 
%adjust the position of the axes
ind=2;
setposition(fh(ind).a.hlgnd,.58,.42,.40,[]);



%change the linestyles so that all traces are visible
for ind=1:2
pind=1;
 pstyle=fh(ind).a.p(pind).pstyle;
pstyle.LineWidth=2;
 pstyle.color= [1 0 0];
 pstyle.LineStyle='-';
 pstyle.Marker='+';
 pstyle.markersize=7.5;
 setpstyle(fh(ind).a,pind,pstyle);
% 
 pind=2;
 pstyle=fh(ind).a.p(pind).pstyle;
% pstyle.LineWidth=3;
pstyle.marker='none';
 pstyle.LineStyle='--';
 pstyle.Marker='none';
 pstyle.color= [0 .6 0]; 
 setpstyle(fh(ind).a,pind,pstyle);

 pind=3;
 pstyle=fh(ind).a.p(pind).pstyle;
 pstyle.color= [0 0 1];
 pstyle.LineStyle='-';
 setpstyle(fh(ind).a,pind,pstyle);
 
  %redo the labels of the legend so that the legend colors are accurate
 plotlegend(fh(ind).a.hlgnd)
 
 title(fh(ind).a,tl{ind});
 end
bname='bsexpllike';

fname1=fullfile(gdir,[bname '_birdsong' fext]);
fname2=fullfile(gdir,[bname '_noise' fext]);
saveas(fh(1).hf,fname1,gtype);
saveas(fh(2).hf,fname2,gtype);