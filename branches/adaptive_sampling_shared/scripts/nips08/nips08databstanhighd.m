%These datasets all used infomax. But we varied the variance for the high
%frequencies. The cutoff frequency was the same for all models
%i.e for nf>4 and nt>3 we set the variance of our prior to a different
%value
function [dfiles,pstyles]=nips08databstanhighd()

dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'colors','lstyles','markers'};
pstyles.linewidth=pstyles.linewidth-.25;




%*************************************************************************
%shuffled
%************************************************************************
bdir='081125';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('shuffled:');
dfiles(dind).usetanpost=false;
explain={'Method:', 'shuffled'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('set prior using stimulus statistics.')}];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max. full:
%************************************************************************
bdir='081125';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max. full:');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax  Full'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('set prior using stimulus statistics.')}];
dfiles(dind).explain=explain;


%*************************************************************************
%info. max. tan rank 2:
%************************************************************************
bdir='081125';
bfile='bsinfomax_setup_003';
rank=2;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max. Tan: rank=%d',rank);
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax  Tan'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('set prior using stimulus statistics.')}];
dfiles(dind).explain=explain;




