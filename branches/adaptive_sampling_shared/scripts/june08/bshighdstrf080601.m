%08-06-01
%
%Analyze the high-dimensional strf. See if its lowrank


bdir='080530';
bfile='bsglmfit_setup_001';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
xscript(setupfile);


data=load(getpath(dsets.datafile));

theta=data.fmap(end).theta;

mobj=data.mobj;

%noise floor force all elements of strf with value less than this to zeros
strfnfloor=.001;

%**********************************************************************
%post processing
%**********************************************************************
[strf shistcoeff bias]=parsetheta(mobj,theta);
strf=reshape(strf,getklength(mobj),getktlength(mobj));

ind=abs(strf)<strfnfloor;
strf(ind)=0;

%*****************************************************************
%compute the svd of the strf

[u s v]=svd(strf);

%%
fsvd=FigObj('name','singular values','width',5,'height',3);
fsvd.a=AxesObj('xlabel','i','ylabel','\sigma^2_i','title','Singular values of the strf');

svals=diag(s);
hp=plot(1:length(svals),svals);
fsvd.a=addplot(fsvd.a,'hp',hp);
xlim([1 length(svals)]);
lblgraph(fsvd);

odata={'script','bshighdstrf080601'};
odata=[odata; {'setfupfile', setupfile}];
odata=[odata;{'STRF noise floor', strfnfloor}];
odata=[odata; {'explanation', 'The singular values of the STRF'}];
otbl={fsvd;odata};

onenotetable(otbl,seqfname('~/svn_trunk/notes/strf_singularvalues.xml'));

