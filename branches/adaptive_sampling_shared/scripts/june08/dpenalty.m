%08-06-04
%
%Script to play with computing the derivative until I get it right.
clear variables
dimtheta=[4,4];
theta=floor(rand(dimtheta)*100)/10;

dimfilt=[3 3];
smoother=SmoothPenalty('errscale',1,'size',dimfilt,'sigma',eye(2));
filt=getfiltcoeff(smoother);

%test 0: Check that d theta/dtheta_m,n * f = f(i-m,j-n);
m=2;
n=2;

dtdmn=zeros(dimtheta);

dmn=zeros(dimtheta);
dmn(m,n)=1;

dtdmn=conv2(dmn,filt,'same');

filtshift=zeros(dimtheta);

%use zero based indexing
for r=0:dimtheta(1)-1;
    for c=0:dimtheta(2)-1;
        %since matlab indexing is 1 based
        if ((r-m)>=0 && (r-m)<dimfilt(1))
           if ((c-n)>=0 &&  (c-n)<dimfilt(2))
               %add 1 to r-m and c-n
               %because indexing in matlab is 1 based
              filtshift(r+1,c+1)=filt(r-m+1,c-n+1); 
           end
        end
        
    end
end

%%
%Test 1: The derivative of theta'x(theta * f) = (theta*f)-(theta*f(-i,-j))
%where *=convolution

thetafilt=conv2(theta,filt,'same');
true.dt=zeros(dimtheta);

for r=1:dimtheta(1);
    for c=1:dimtheta(2)
        true.dt(r,c)=thetafilt(r,c);
    
        drc=zeros(dimtheta);
        drc(r,c)=1;
        
        filtdrc=conv2(drc,filt,'same');
        true.dt(r,c)=true.dt(r,c)+theta(:)'*filtdrc(:);
    end
end

%now use the quickmethod
%q.dt=thetafilt+conv2(theta,f(end:-1:1,end:-1:1),'same');
%q.dt=thetafilt+conv2(theta,f,'same');