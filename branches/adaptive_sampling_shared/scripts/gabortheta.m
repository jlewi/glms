%function ktrue=gabortheta(width,height,pos, amp)
%       width -of gabor patch.
%       height - height
%       pos - position for the center of the gabor filter
%                 corresponds to the coordinates in the middle of the graph
%           - leave empty if you want center in the middle of the graph
%       amp - scaling constant for the function
%
% Return value
%       ktrue - is rasterization of a matrix that is
%               heightxwidth 
%               i.e consistent with Matlab conventions for drawing images
%               
%7-27-2007
%     dimensions do not neet to be odd
%***************************************
%function to generate filter coefficients which correspond to a gabor patch
function ktrue=gabortheta(width,height,pos,amp)
%length of gabor patch
%best if its odd

if ~exist('amp','var')
    amp=1;
end

if isempty(pos)
    pos(1)=0;
    pos(2)=0;
end

%we need to create a grid of x,y points which contain the actual values 
%of x, y which we plug into the gabor
%this ensures that the center of the gabor is in the middle of filter 
%we could now shift it if necessary
x=(1:width)-(1+width)/2;
y=(1:height)-(1+height)/2;
%shift by pos
x=x-pos(1);
y=y-pos(2);
[x,y]=meshgrid(x,y);
sigmax=width/2-1/2;
sigmay=sigmax;


%omega=pi/floor(len/2);
%we want half the width to be equal to 3/4 of the period
%omega=3/4*2*pi/floor(len/2);
%we want half the width be pi
omega=pi/(width/2-1/2);


r=(x.^2+y.^2).^.5;


%ktrue=normpdf(r,0,sigma).*cos((x-pos(1))*omega);
ktrue=amp/(2*pi*sigmax*sigmay)*exp(-(x).^2/(2*sigmax^2)-(y).^2/(2*sigmay^2)).*sin((x)*omega);
%zero out any positions greater then len/2
%ind=find(r>len/2);
%ktrue(ind)=0;

%rescale 
%ktrue=1/(2*pi*sigmax*sigmay)*exp(-(x-pos(1)).^2/(2*sigmax^2)-(y-pos(2)).^2/(2*sigmay^2)).*sin((x-pos(1))*omega);
%rscale=mval/max(ktrue(:));
%ktrue=ktrue*rscale;

%reshape ktrue
figure;imagesc(ktrue);colorbar;
ktrue=reshape(ktrue,[width*height,1]);