%04-18-2008

%load the strf
%
%Explanation: This function plots the STRF for my sims using fminunc to
%find the MAP.
clear all;
setpathvars;
% setupfile='/home/jlewi/svn_trunk/allresults/bird_song/080721/bsglmfit_setup_001.m';
% xscript(setupfile);

alldsets=nips08bsbatchlowd;
dsets=alldsets(5);

v=load(getpath(dsets.datafile));

bdata=v.bdata;


%nranks - plot an nrank approximation of the strf
%nranks=[5 10 20 40];
nranks=[];
%*************************************************************
%%
%which method to use
methods.newton=1;
methods.batchfit=2;
methods.spikeavg=3;
methods.fmap=4;

method=methods.fmap;

windexes=[];
%select theta
switch method
    case methods.batchfit
        theta=v.batchfit.theta;
        mname='batch update';

    case methods.newton
        allpost=v.allpost;
        theta=getm(allpost,getlasttrial(allpost));
        mname='online update';
    case methods.spikeavg
        %use the spike triggered average

        %        if (~exist(spikeavgisempty(spikeavg))
        %           spikeavg=bsspiketriggeredaverage(bdata);
        %       end

        %add a bias term of zero
        %   theta=[spikeavg(:);0];
        %        theta=[spikeavg(:);-2.8788];   %use the spike triggered average

        theta=v.spiketrgavg.theta;
        mname='Spike Triggered Average';

    case methods.fmap
        ind=length(v.fmap);
        fmap=v.fmap(ind);
        theta=v.fmap(ind).theta;
        mname='Find MAP using fminunc';

        if isfield(v.fmap,'windexes')
            windexes=v.fmap(ind).windexes;
        else
            windexes='all files';
        end
    otherwise
        error('unknown method');
end

mobj=v.mobj;
%get rid of the bias term
%strf=reshape(theta(1:getklength(mobj)),getstimdim(bdata));
%shistcoeff = theta(getklength(mobj)+1:getklength(mobj)+getshistlen(mobj),1);
[strf shistcoeff bias]=parsetheta(mobj,theta);
strf=reshape(strf,getklength(mobj),getktlength(mobj));

nrows=1;

if ~isempty(nranks)
    nrows=nrows+length(nranks);
end
if (getshistlen(mobj)>0)
    nrows=nrows+1;
end
%plot the strf
fstrf=FigObj('name','STRF','width',5,'height',8);
fstrf.a=AxesObj('xlabel','time(s)','ylabel','Frequenzy(hz)','title','STRF','nrows',nrows,'ncols',1);

setfocus(fstrf.a,1,1);
[t,f]=getstrftimefreq(bdata);
imagesc(t,f,strf);
hc=colorbar;
fstrf.a(1,1)=sethc(fstrf.a(1,1),hc);
fstrf.a(1,1)=title(fstrf.a(1,1),'STRF');
xlim([t(1) t(end)]);
ylim([f(1) f(end)]);

%plot low rank approximations of the strf
if ~isempty(nranks)
    [u s v]=svd(strf);
    row=1;
    for r=nranks
        lapprox=u(:,1:r)*s(1:r,1:r)*v(:,1:r)';

        row =row+1;

        setfocus(fstrf.a,row,1);
        imagesc(t,f,lapprox);
        hc=colorbar;
        fstrf.a(row,1)=sethc(fstrf.a(row,1),hc);
        fstrf.a(row,1)=title(fstrf.a(row,1),sprintf('STRF rank %d approx',r));
        xlim([t(1) t(end)]);
        ylim([f(1) f(end)]);
    end
end
%spike history coefficients
row=nrows;
if (getshistlen(mobj)>0)
    setfocus(fstrf.a,row,1);
    t=-getobsrvtlength(bdata)*[getshistlen(mobj):-1:1];
    hp=plot(t,shistcoeff);
    pstyle.marker='.';
    pstyle.markerfacecolor='b';
    fstrf.a(row,1)=addplot(fstrf.a(row,1),'hp',hp,'pstyle',pstyle);
    fstrf.a(row,1)=xlabel(fstrf.a(row,1),'time(s)');
    fstrf.a(row,1)=ylabel(fstrf.a(row,1),'spike history coefficient');
    xlim([t(1) t(end)]);

end


fstrf=lblgraph(fstrf);

%set the heights of the plot
%make spike history 1/4 the height of the strf plots
heights=ones(1,nrows);

if ~isempty(nranks)
   heights=heights/length(nranks); 
end
if (getshistlen(mobj)>0)
    heights(end)=heights(1)/4;
    heights=heights/sum(heights);
end
fstrf=sizesubplots(fstrf,[],[],heights);

finfo=rmfield(fmap,{'gradient','wmat','y','thetainit','theta','windexes','hessian'});

ginfo=objinfo(getglm(v.mobj));   

scriptname=mfilename;
otbl={'script',scriptname;'Sim File', setupfile; 'Bias term', sprintf('%0.3g',theta(end)); 'windexes',windexes};
otbl=[otbl; structinfo(finfo);{'GLM',ginfo};{'Explanation', mname}];
odata={fstrf;otbl}
onenotetable(odata,seqfname('~/svn_trunk/notes/strf.xml'));