%06-03-2008
%
%Cross validation
%
% This script looks at a bunch of simulations in which we did not train
% on at least some of the wavefiles. We use those wavefiles as a test set
% and compute the results
%
clear variables;
setpathvars;

bdir='080612';
bfile='bsglmfit_setup_001';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
xscript(setupfile);

%how many repeats to plot for the simulated raster plot
nrepeats=10;

data=load(getpath(dsets.datafile));
bdata=data.bdata;
allpost=data.allpost;
fmap=data.fmap;

%select just the moset recent results
fmap=data.fmap(end);

mobj=data.mobj;

%structure one for each wave file in the test set
testr=struct('fmapind',[],'mse',[],'raster',[],'spiketimes',[],'exprate',[],'wavefile',[]);



ntest=0;

%***************************************************************
%loop over all entries in fmap. Because each entry in fmap
% is for a different simulation/test set
%*****************************************************************
for fmapind=1:length(fmap)
    %determine which wave files we didn't train on
    testwind=ones(1,getnwavefiles(bdata));
    testwind(fmap(fmapind).windexes)=0;
    testwind=find(testwind~=0);

    theta=fmap(fmapind).theta;
    [strf, shistcoeff, bias]=parsetheta(mobj,theta);

    strf=reshape(strf,getstrfdim(bdata));
    for wind=testwind
        ntest=ntest+1;

        testr(ntest).fmapind=fmapind;
        testr(ntest).wavefile=wind;

        [testr(ntest).mse testr(ntest).exprate testr(ntest).emprate]=bspsthmse(dsets.datafile,theta,testr(ntest).wavefile);

        %generate a fake raster plot
        testr(ntest).raster=sampdist(getglm(mobj),ones(nrepeats,1)*testr(ntest).exprate);

        %from the raster plot we need to generate the spike times
        testr(ntest).spiketimes=cell(1,nrepeats);

        %the start time for each bin
        %compute the edges of the time bin for each observation
        [ntrials,cstart]=getntrialsinwave(bdata,testr(ntest).wavefile);
        tstart=([0:ntrials]+cstart-1)*getobsrvtlength(bdata);

        for rind=1:nrepeats
            nzind=find(testr(ntest).raster(rind,:)>0);
            spiketimes=zeros(1,sum(testr(ntest).raster(rind,:)));
            scount=0;
            for cind=nzind
                %linearly space these spikes out in the response bin
                scount=scount+1;
                nspikes=testr(ntest).raster(rind,cind);
                spiketimes(1,scount:scount+nspikes-1)=tstart(cind)+getobsrvtlength(bdata)/(nspikes+1)*[1:nspikes];
            end
            testr(ntest).spiketimes{rind}=spiketimes;
        end
    end
end
%%
%**************************************************************
%Make the plots
%************************************************************
%plot the mse for al the files in the test set
fmse=FigObj('name','mse','width',5,'height',3);
fmse.a=AxesObj('xlabel','Wave file','ylabel', '(M.S.E)^.5');

pstyle.marker='o';
pstyle.markerfacecolor='b';
pstyle.linestyle='none';
hp=plot([testr.wavefile],[testr.mse],'bo');
fmse.a=addplot(fmse.a,'hp',hp,'pstyle',pstyle);
fmse=lblgraph(fmse);


%****************************************************************
%for each file plot the psth and the raster
%**********************************************************
clear ftest;
for tind=1:length(testr)
    ftest(tind)=FigObj('name','Cross valid wavefile','width',5,'height',5);
    ftest(tind).a=AxesObj('nrows',3,'ncols',1);

    %*********************************************************
    %Plot the psth
    %**********************************************************

    row=1;
    col=1;

    [ntrials,cstart]=getntrialsinwave(bdata,testr(tind).wavefile);
    t=[cstart:(cstart+ntrials-1)]*getobsrvtlength(bdata);

    ftest(tind).a=setfocus(ftest(tind).a,row,col);

    hp=plot(t,testr(tind).emprate);
    pstyle=struct('linestyle','-','color','r','linewidth',3);
    ftest(tind).a(row,col)=addplot(ftest(tind).a(row,col),'hp',hp,'pstyle',pstyle,'lbl','empirical');

    hp=plot(t,testr(tind).exprate);
    pstyle=struct('linestyle','-','color','b','linewidth',2);
    ftest(tind).a(row,col)=addplot(ftest(tind).a(row,col),'hp',hp,'pstyle',pstyle,'lbl','model');

    xlim([t(1) t(end)]);

    ftest(tind).a(row,col)=ylabel(ftest(tind).a(row,col),'E(r)');
    ftest(tind).a(row,col)=xlabel(ftest(tind).a(row,col),'t(s)');
    ftest(tind).a(row,col)=title(ftest(tind).a(row,col),['PSTH: Wave file ' num2str(testr(tind).wavefile)]);


    %******************************************************************
    %Raster plot for model
    %******************************************************************
    row=2;
    col=1;



    ftest(tind).a=setfocus(ftest(tind).a,row,1);
    hold on;
    for rep=1:nrepeats
        %each cell array is the spike times on a different trial
        nspikes=length(testr(tind).spiketimes{rep});
        hp=plot(testr(tind).spiketimes{rep},rep*ones(1,nspikes),'b.');
        ftest(tind).a(row,1)=addplot(ftest(tind).a(row,1),'hp',hp);
    end
    ftest(tind).a(row,1)=title(ftest(tind).a(row,1),'Model Raster Plot');
    ftest(tind).a(row,1)=xlabel(ftest(tind).a(row,1),'time(s)');
    ftest(tind).a(row,1)=ylabel(ftest(tind).a(row,1),'trial');

    xlim([t(1) t(end)]);
    ylim([1 nrepeats]);


    %**********************************************************************
    %empirical raster plot
    %******************************************************
    row=3;
    col=1;



    ftest(tind).a=setfocus(ftest(tind).a,row,1);
    hold on;

    spiketimes=getstimresp(bdata,testr(tind).wavefile);
    for rep=1:nrepeats
        %each cell array is the spike times on a different trial
        nspikes=length(spiketimes{rep});
        hp=plot(spiketimes{rep},rep*ones(1,nspikes),'r.');
        ftest(tind).a(row,1)=addplot(ftest(tind).a(row,1),'hp',hp);
    end
    ftest(tind).a(row,1)=title(ftest(tind).a(row,1),'Empirical Raster Plot');
    ftest(tind).a(row,1)=xlabel(ftest(tind).a(row,1),'time(s)');
    ftest(tind).a(row,1)=ylabel(ftest(tind).a(row,1),'trial');

    xlim([t(1) t(end)]);
    ylim([1 nrepeats]);
end


%**************************************************************************
%Plot the actual STRFS
%*************************************************************************
%%
clear fstrf;
clear hc;
clear hi;
cl=[];
%cl=[-.0095 .015];
%limits for
for fmapind=1:length(fmap)
    theta=fmap(fmapind).theta;
    [strf shistcoeff bias]=parsetheta(mobj,theta);
    strf=reshape(strf,getklength(mobj),getktlength(mobj));

    nrows=1;

    if (getshistlen(mobj)>0)
        nrows=nrows+1;
    end
    %plot the strf
    fstrf(fmapind)=FigObj('name','STRF','width',5,'height',8);
    fstrf(fmapind).a=AxesObj('xlabel','time(s)','ylabel','Frequenzy(hz)','title','STRF','nrows',nrows,'ncols',1);

    setfocus(fstrf(fmapind).a,1,1);
    [t,f]=getstrftimefreq(bdata);
    hi(fmapind)=imagesc(t,f,strf);
    hc(fmapind)=colorbar;
    fstrf(fmapind).a(1,1)=sethc(fstrf(fmapind).a(1,1),hc(fmapind));
    fstrf(fmapind).a(1,1)=title(fstrf(fmapind).a(1,1),'STRF');
    xlim([t(1) t(end)]);
    ylim([f(1) f(end)]);

    if ~isempty(cl)
       set(gca,'clim',cl); 
    end
    %spike history coefficients
    row=nrows;
    if (getshistlen(mobj)>0)
        setfocus(fstrf(fmapind).a,row,1);
        t=-getobsrvtlength(bdata)*[getshistlen(mobj):-1:1];
        hp=plot(t,shistcoeff);
        pstyle.marker='.';
        pstyle.markerfacecolor='b';
        fstrf(fmapind).a(row,1)=addplot(fstrf(fmapind).a(row,1),'hp',hp,'pstyle',pstyle);
        fstrf(fmapind).a(row,1)=xlabel(fstrf(fmapind).a(row,1),'time(s)');
        fstrf(fmapind).a(row,1)=ylabel(fstrf(fmapind).a(row,1),'spike history coefficient');
        xlim([t(1) t(end)]);

    end


    fstrf(fmapind)=lblgraph(fstrf(fmapind));

    heights=[.75 .25];
    fstrf(fmapind)=sizesubplots(fstrf(fmapind),[],[],heights);

end
%make the limits of the colorbar uniform
% aobj=[fstrf(:).a];
% ha=getha(aobj);
% yl=cell2mat(get(ha,'clim'));
% yl=[min(yl(:,1)) max(yl(:,2))];
% set(ha,'clim',yl);
% 
% %set limits of the colorbar
% yl=cell2mat(get(hc,'ylim'));
% yl=[min(yl(:,1)) max(yl(:,2))];
% set(hc,'ylim',yl);

%%
otbl={};

%create the table of output information
%info about the fields
explain={'wave file', 'A number identifying the wave file'};
explain=[explain; {'isbirdsong', 'Indicates whether stimulus is bird song or a noise file.'}];
explain=[explain; {'fmapind', 'A number identifying the model used. This number can be matched to the results below to see the STRF for this model as well as the trainingset'}];
explain=[explain;{'explanation','This plot compares the empirical psth and raster plots to the predicted values using the model. The results are for  the wave file identified by wavefile.'}];

for testind=1:length(testr)
    oinfo={'wave file', testr(testind).wavefile; 'isbirdsong', waveissong(bdata,testr(testind).wavefile);'fmapind',testr(testind).fmapind};
    odata={ftest(testind) oinfo}; 
    
    if (testind==1)
        odata={ftest(testind) {oinfo;explain}};
    end
    otbl=[otbl;odata];
end

%create the table of output information

explain=[{'fmapind', 'A number identifying the model used. This number can be matched to the results below to see the STRF for this model as well as the trainingset'}];
explain=[explain;{'Training set', 'The indexes of the wave files used to train the model'}];
explain=[explain;{'explanation', 'This plot shows the strf computed for this model.'}];

for fmapind=1:length(fmap)
    oinfo={'fmapind', fmapind;'Training set',fmap(fmapind).windexes};
    odata={fstrf(fmapind) oinfo}; 
    
      if (fmapind==1)
        odata={fstrf(fmapind) {oinfo;explain}};
      end
    otbl=[otbl;odata];
end

otbl=[otbl;{fmse,{'explain', 'M.S.E for test sets'}}];
oinfo={'script','bscrossvalid.m';'setupfile', setupfile};
otbl=[otbl;oinfo];

onenotetable(otbl,seqfname('~/svn_trunk/notes/bscrossvalid.xml'));

return;

