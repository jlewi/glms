%********************************************************************
%08-07-2008
%********************************************************************
%Compute the running time of the log-likelihood of the bird-song data
%for different dimensions with and without the hessian
clear variables;


glm=GLMPoisson('canon');
rawdatafile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1_corrected.mat'));

%parameters for the birdsong
stimnobsrvwind=[1 5 10 20];
stimnobsrvwind=[1 5 10 20];
freqsubsample=[1 2];
obsrvwindowxfs=250;

alength=8;
%which wave files to use
windexes=1;

rnohess.time=zeros(length(freqsubsample),length(stimnobsrvwind));
rhess.time=zeros(length(freqsubsample),length(stimnobsrvwind));

nfreqs=zeros(1,length(freqsubsample));

for nsub=1:length(freqsubsample)
    for nwind=1:length(stimnobsrvwind)
        %***********************************************************************
        %create the BSData object
        %**********************************************************************
        bdata=BSData('fname',rawdatafile,'stimnobsrvwind',stimnobsrvwind(nwind),'freqsubsample',freqsubsample(nsub),'obsrvwindowxfs',obsrvwindowxfs);

        bpost=BSlogpost('bdata',bdata,'windexes',windexes);


        [strfdim]=getstrfdim(bdata);
        nfreqs(nsub)=strfdim(1);
        
        mobj=MParamObj('glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',1);

        theta=ones(getparamlen(mobj),1);
        theta=theta/(theta'*theta)^.5;

        %get the spectrogram so that computing the spectrogram doesn't figure into
        %the running
        for wind=windexes
            [bdata,fullspec]=getfullspec(bdata,wind);
        end

        %compute the time no hessian
        tic;
        [ll,dll]=compllike(bpost,mobj,theta);
        rnohess.time(nsub,nwind)=toc;

       % compute the time with hessian
        tic;
        [ll,dll,d2ll]=compllike(bpost,mobj,theta);
        rhess.time(nsub,nwind)=toc;
    end
end

%**************************************************************************
%make a plot
%***********************************************************************

sname=mfilename();
%%
fh=FigObj('name','timing','height',6,'width',5);
fh.a=AxesObj('nrows',2, 'ncols',1);

hold on;

%add a line for each subsampling
for nsub=1:length(freqsubsample)

    %no hessian
   

    for pind=1:2
     fh.a=setfocus(fh.a,pind,1);

        if (pind==1)
            data=rnohess.time(nsub,:);
        else
            data=rhess.time(nsub,:);
        end
    
       hp=plot(stimnobsrvwind,data);

        pstyle=plotstyle(nsub);
        pstyle.LineWidth=pstyle.LineWidth*3;
       fh.a(pind,1)=addplot(fh.a(pind,1),'hp',hp,'pstyle',pstyle,'lbl',sprintf('nfreqs=%d',nfreqs(nsub)));
    end
end

%set the titles
fh.a(1,1)=title(fh.a(1,1),'Running time (hessian not computed)');
fh.a(2,1)=title(fh.a(2,1),'Running time (hessian is computed)');
fh.a(2,1)=xlabel(fh.a(2,1),'Number time bins in STRF');
fh.a(1,1)=ylabel(fh.a(1,1),'time(s)');
fh.a(2,1)=ylabel(fh.a(2,1),'time(s)');



  

fh=lblgraph(fh);


[sys svnrevision]=system('~/svn_trunk/scripts/getsvnrevision');
svnrevision=deblank(svnrevision);
otbl={{fh};{'script',sname;'svnrevision',svnrevision;'glm',glm; 'alength', alength; 'explanation', 'The running time of BSlogpost/compllike.m'}};

onenotetable(otbl,seqfname('~/svn_trunk/notes/timecomparison.xml'));