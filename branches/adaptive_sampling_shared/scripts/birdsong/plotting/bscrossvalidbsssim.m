%06-03-2008
%
%Cross validation
%
% This script looks at a bunch of simulations in which we did not train
% on at least some of the wavefiles. We use those wavefiles as a test set
% and compute the results
%
clear variables;
setpathvars;

bdir='080729';
bfile='bsinfomax_setup_005';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
xscript(setupfile);

%how many repeats to plot for the simulated raster plot
nrepeats=10;

%which trials to do the cross validation for
trials=[1000 5000 10000:10000:50000];
data=load(getpath(dsets.datafile));

bssim=data.bssimobj;

%structure one for each wave file in the test set
testr=struct('fmapind',[],'mse',[],'raster',[],'spiketimes',[],'exprate',[],'wavefile',[]);


bdata=getbdata(bssim.stimobj);
mobj=bssim.mobj;

ntest=0;


%determine which wave files we didn't train on
testwind=ones(1,getnwavefiles(bdata));
testwind(bssim.stimobj.windexes)=0;
testwind=find(testwind~=0);

%loop over all the trials we do the cross validation for
for trial=trials

    theta=getm(bssim.allpost,trial);
    [strf, shistcoeff, bias]=parsetheta(bssim.mobj,theta);

    strf=reshape(strf,getstrfdim(bdata));
    for wind=testwind
        ntest=ntest+1;

        %        testr(ntest).fmapind=fmapind;
        testr(ntest).wavefile=wind;

        testr(ntest).trial=trial;

        [testr(ntest).mse testr(ntest).exprate testr(ntest).emprate]=bspsthmse(bssim,theta,testr(ntest).wavefile);

        %generate a fake raster plot
        testr(ntest).raster=sampdist(getglm(mobj),ones(nrepeats,1)*testr(ntest).exprate);

        %from the raster plot we need to generate the spike times
        testr(ntest).spiketimes=cell(1,nrepeats);

        %the start time for each bin
        %compute the edges of the time bin for each observation
        [ntrials,cstart]=getntrialsinwave(bdata,testr(ntest).wavefile);
        tstart=([0:ntrials]+cstart-1)*getobsrvtlength(bdata);

        for rind=1:nrepeats
            nzind=find(testr(ntest).raster(rind,:)>0);
            spiketimes=zeros(1,sum(testr(ntest).raster(rind,:)));
            scount=0;
            for cind=nzind
                %linearly space these spikes out in the response bin
                scount=scount+1;
                nspikes=testr(ntest).raster(rind,cind);
                spiketimes(1,scount:scount+nspikes-1)=tstart(cind)+getobsrvtlength(bdata)/(nspikes+1)*[1:nspikes];
            end
            testr(ntest).spiketimes{rind}=spiketimes;
        end
    end

end
scriptname=mfilename();
%%
%**************************************************************
%Make the plots
%************************************************************
%plot the mse for al the files in the test set
fmse=FigObj('name','mse','width',5,'height',3,'xlabel','Wave file','ylabel', '(M.S.E)^.5');

pstyle.marker='o';
%pstyle.markerfacecolor='b';
%pstyle.linestyle='none';

%for each trial plot the MSE for all test files using a different marker
%for tind=1:length(trials)
for tind=1:length(trials)
    trial=trials(tind);
    ind=find([testr.trial]==trial);
    hp=plot([testr(ind).wavefile],[testr(ind).mse]);
    
    [c m]=getptype(tind);
    pstyle.markerfacecolor=c;
    pstyle.color=c;
    fmse.a=addplot(fmse.a,'hp',hp,'lbl',num2str(trial),'pstyle',pstyle);

end
lblgraph(fmse);
%%
%****************************************************************
%for each file plot the psth and the raster
%**********************************************************
clear ftest;
for tind=1:length(testr)
    ftest(tind)=FigObj('name','Cross valid wavefile','width',5,'height',5,'naxes',[3 1]);


    %*********************************************************
    %Plot the psth
    %**********************************************************

    row=1;
    col=1;

    [ntrials,cstart]=getntrialsinwave(bdata,testr(tind).wavefile);
    t=[cstart:(cstart+ntrials-1)]*getobsrvtlength(bdata);

    ftest(tind).a=setfocus(ftest(tind).a,row,col);

    hp=plot(t,testr(tind).emprate);
    pstyle=struct('linestyle','-','color','r','linewidth',3);
    addplot(ftest(tind).a(row,col),'hp',hp,'pstyle',pstyle,'lbl','empirical');

    hp=plot(t,testr(tind).exprate);
    pstyle=struct('linestyle','-','color','b','linewidth',2);
    addplot(ftest(tind).a(row,col),'hp',hp,'pstyle',pstyle,'lbl','model');

    xlim([t(1) t(end)]);

    ylabel(ftest(tind).a(row,col),'E(r)');
    xlabel(ftest(tind).a(row,col),'t(s)');
    title(ftest(tind).a(row,col),['PSTH: Wave file ' num2str(testr(tind).wavefile)]);


    %******************************************************************
    %Raster plot for model
    %******************************************************************
    row=2;
    col=1;



    ftest(tind).a=setfocus(ftest(tind).a,row,1);
    hold on;
    for rep=1:nrepeats
        %each cell array is the spike times on a different trial
        nspikes=length(testr(tind).spiketimes{rep});
        hp=plot(testr(tind).spiketimes{rep},rep*ones(1,nspikes),'b.');
        ftest(tind).a(row,1)=addplot(ftest(tind).a(row,1),'hp',hp);
    end
    ftest(tind).a(row,1)=title(ftest(tind).a(row,1),'Model Raster Plot');
    ftest(tind).a(row,1)=xlabel(ftest(tind).a(row,1),'time(s)');
    ftest(tind).a(row,1)=ylabel(ftest(tind).a(row,1),'trial');

    xlim([t(1) t(end)]);
    ylim([1 nrepeats]);


    %**********************************************************************
    %empirical raster plot
    %******************************************************
    row=3;
    col=1;



    ftest(tind).a=setfocus(ftest(tind).a,row,1);
    hold on;

    spiketimes=getstimresp(bdata,testr(tind).wavefile);
    for rep=1:nrepeats
        %each cell array is the spike times on a different trial
        nspikes=length(spiketimes{rep});
        hp=plot(spiketimes{rep},rep*ones(1,nspikes),'r.');
        ftest(tind).a(row,1)=addplot(ftest(tind).a(row,1),'hp',hp);
    end
    ftest(tind).a(row,1)=title(ftest(tind).a(row,1),'Empirical Raster Plot');
    ftest(tind).a(row,1)=xlabel(ftest(tind).a(row,1),'time(s)');
    ftest(tind).a(row,1)=ylabel(ftest(tind).a(row,1),'trial');

    xlim([t(1) t(end)]);
    ylim([1 nrepeats]);
end


%**************************************************************************
%Plot the actual STRFS
%*************************************************************************
%%
clear fstrf;
clear hc;
clear hi;
cl=[];

fmapind=0;

%loop over all the trials for which we used the posterior to perform
%cross-validation.
%
%plot the strf for each trial
for trial=trials

    theta=getm(bssim.allpost,trial);
    fmapind=fmapind+1;
    [strf shistcoeff bias]=parsetheta(mobj,theta);
    strf=reshape(strf,getklength(mobj),getktlength(mobj));

    nrows=1;

    if (getshistlen(mobj)>0)
        nrows=nrows+1;
    end
    %plot the strf
    fstrf(fmapind)=FigObj('name','STRF','width',5,'height',8,'naxes',[nrows, 1],'xlabel','time(s)','ylabel','Frequenzy(hz)','title','STRF');

    setfocus(fstrf(fmapind).a,1,1);
    [t,f]=getstrftimefreq(bdata);
    hi(fmapind)=imagesc(t,f,strf);
    hc(fmapind)=colorbar;
    fstrf(fmapind).a(1,1)=sethc(fstrf(fmapind).a(1,1),hc(fmapind));
    fstrf(fmapind).a(1,1)=title(fstrf(fmapind).a(1,1),'STRF');
    xlim([t(1) t(end)]);
    ylim([f(1) f(end)]);

    if ~isempty(cl)
        set(gca,'clim',cl);
    end
    %spike history coefficients
    row=nrows;
    if (getshistlen(mobj)>0)
        setfocus(fstrf(fmapind).a,row,1);
        t=-getobsrvtlength(bdata)*[getshistlen(mobj):-1:1];
        hp=plot(t,shistcoeff);
        pstyle.marker='.';
        pstyle.markerfacecolor='b';
        fstrf(fmapind).a(row,1)=addplot(fstrf(fmapind).a(row,1),'hp',hp,'pstyle',pstyle);
        fstrf(fmapind).a(row,1)=xlabel(fstrf(fmapind).a(row,1),'time(s)');
        fstrf(fmapind).a(row,1)=ylabel(fstrf(fmapind).a(row,1),'spike history coefficient');
        xlim([t(1) t(end)]);

    end


    fstrf(fmapind)=lblgraph(fstrf(fmapind));

    heights=[.75 .25];
    fstrf(fmapind)=sizesubplots(fstrf(fmapind),[],[],heights);

end

%%
otbl={};

%create the table of output information
%info about the fields
explain={'wave file', 'A number identifying the wave file'};
explain=[explain; {'isbirdsong', 'Indicates whether stimulus is bird song or a noise file.'}];
explain=[explain;{'explanation','This plot compares the empirical psth and raster plots to the predicted values using the model. The results are for  the wave file identified by wavefile.'}];

for testind=1:length(testr)
    oinfo={'wave file', testr(testind).wavefile; 'isbirdsong', waveissong(bdata,testr(testind).wavefile);'trial',testr(testind).trial};
    odata={ftest(testind) oinfo};

    if (testind==1)
        odata={ftest(testind) {oinfo;explain}};
    end
    otbl=[otbl;odata];
end

%create the table of output information
%exaplain what each of the variables means
explain=[{'trial', 'A number identifying the posterior after which trial is used to compute. You can matchi this number to the STRF illustrated below. '}];
explain=[explain;{'Training set', 'The indexes of the wave files used to train the model'}];
explain=[explain;{'explanation', 'This plot shows the strf computed for this model.'}];

for tind=1:length(trials)
oinfo={'trial', trials(tind);'Training set',bssim.stimobj.windexes};
odata={fstrf(tind) oinfo};
otbl=[otbl;odata];
end

otbl=[{fmse,{'explain', 'M.S.E for test sets'}};otbl];
oinfo={'script',scriptname;'setupfile', setupfile};
otbl=[otbl;oinfo];

onenotetable(otbl,seqfname('~/svn_trunk/notes/bscrossvalid.xml'));

return;

