%07-209-2008
%
%Plot the magnitude of the change in mu between trials

clear variables;
setpathvars;

dind=0;
%**************************************************************************
%Setupfiles for the simulations we want to look at
%**************************************************************************

dind=dind+1;
bdir='080729';
bfile='bsinfomax_setup_003';
dfiles(dind).setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);


bdir='080729';
bfile='bsinfomax_setup_004';
dind=dind+1;
dfiles(dind).setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);

 bdir='080729';
  bfile='bsinfomax_setup_005';
  dind=dind+1;
  dfiles(dind).setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);

%**************************************************************************

%How many trials to do plot for
maxtrials=300;


%structure one for each simulation in the test set
results=struct('dmu',zeros(1,maxtrials));

ntest=0;

for sind=1:length(dfiles)
    %clear dsets and data
    clear dsets;
    clear data;
    clear bssim;
    clear mobj;
    clear bdata;
    xscript(dfiles(sind).setupfile);

    data=load(getpath(dsets.datafile));
    bssimobj=data.bssimobj;


    mu=readdata(bssimobj.allpost.minfo.maccess,[0:maxtrials]);
    mu=cell2mat(mu);
    dmu=diff(mu,1,2);
    
    results(sind).dmu=sum(dmu.^2,1).^.5;

    dfiles(sind).lbl=bssimobj.label;

end
scriptname=mfilename();


%%
%**************************************************************
%Make the plots
%************************************************************
fdmu=FigObj('name','mse','width',5,'height',3,'xlabel','trial','ylabel', '||\mu_t-\mu_{t-1}||_2', 'title', 'Magnitude of the change in \mu' );

    %loop over the simulations
    for sind=1:length(dfiles)

        hp=plot(1:maxtrials,results(sind).dmu);

        [c m]=getptype(sind);
        pstyle.marker='o';
        pstyle.markerfacecolor=c;
        pstyle.color=c;
        pstyle.markersize=4;
        addplot(fdmu.a,'hp',hp,'lbl',dfiles(sind).lbl,'pstyle',pstyle);

    end
    
set(gca,'yscale','log')
lblgraph(fdmu);

%%
%%
otbl={};

%create the table of output information
%info about the fields

explain={'script',scriptname;'explanation','This plots the magnitude of the difference between the MAP at time t and time t-1.'};
for sind=1:length(dfiles)
explain=[explain; {dfiles(sind).lbl, dfiles(sind).setupfile}];
end


otbl={fdmu, explain};

onenotetable(otbl,seqfname('~/svn_trunk/notes/bsdmu.xml'));

return;

