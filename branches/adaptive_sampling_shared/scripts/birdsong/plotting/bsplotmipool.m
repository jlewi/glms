%07-25-2008
%
%Plot The mutual information for a simulation which uses BSOptStimNoHeur
%and saved the pool of stimuli on all trials.

clear variables;
setpathvars;

dind=0;
%**************************************************************************
%Setupfiles for the simulations we want to look at
%**************************************************************************


bdir='080725';
bfile='bsinfomax_setup_001';
dind=dind+1;
dfiles(dind).setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
xscript(dfiles(dind).setupfile);

data=load(getpath(dsets.datafile));
bssim=data.bssimobj;

bdata=getbdata(bssim.stimobj);
mobj=bssim.mobj;

if ~isa(bssim.stimobj,'BSOptStimNoHeur')
    error('This script can only work if stimulus chooser is BSOptStimNoHeur.');
end

%which trials to plot the pool mutual information for
%
trials=[100:250:1000];

%indicates whether we throw out stimuli presented on past trials, or if we
%include them in our pool
excludepast=true;
%****************************************************************
%Make the plots
%****************************************************************

for tind=1:length(trials)

    trial=trials(tind);

    freg(tind)=FigObj('width',6,'height',6,'xlabel','\mu_\rho','ylabel','\sigma_\rho^2','title',sprintf('trial=%d',trial));

    %we get the posteri from the infomax set
    cmap=colormap();
    %reverse the colormap so darker colors are more informative
    cmap=cmap(end:-1:1,:);
    colormap(cmap);

    stimpool=readmatrix(bssim.stimobj.poolfile,trial);


    if (excludepast)
        %we map the indexes of paststim into the linear indexes
       pastind=stimindtopoolind(bssim.stimobj,bssim.paststim(:,1:trial-1));
       
       ind=ones(1,size(stimpool,1));
       ind(pastind)=0;
       
       stimpool=stimpool(logical(ind),:);
    end
    
    
    mi=stimpool(:,3);
    mu=stimpool(:,1);
    sigma=stimpool(:,2);
    %get the limits for the mutual info colorbar
    %scale the mi for each point to map it to a color to show the
    %information
    mimin=min([stimpool(:,3)]);
    mimax=max([stimpool(:,3)]);

    mumin=min([stimpool(:,1)]);
    mumax=max([stimpool(:,1)]);

    sigmin=min([stimpool(:,2)]);
    sigmax=max([stimpool(:,2)]);



    hold on;

    cscale=(size(cmap,1)-1)/(mimax-mimin);
    micind=(mi-mimin)*cscale;

    hold on;
    %plot each point for this graph and set the color approriately
    for pind=1:length(micind);
        hp=plot(mu(pind),sigma(pind),'.');
        set(hp,'MarkerFaceColor',cmap(floor(micind(pind))+1,:));
        set(hp,'MarkerEdgeColor',cmap(floor(micind(pind))+1,:));
    end

    xlim([mumin,mumax]);
    ylim([sigmin,sigmax]);


    %add a colorbar
    hc=colorbar;
    sethc(freg(tind).a,hc);



end

scriptname=mfilename();
%**************************************************************************
%Create the table in onenote
%***********************************************************************
%%
explain={'explain','A plot of the stimulus pool for various trials. "exclude past inputs" indicates whether the stimulus pool includes inputs which have already been presented on past trials.'};
explain=[{'setupfile',dfiles(1).setupfile;'scriptname',scriptname;'exclude past inputs',excludepast};explain];

odata=[];
oinfo=[];

oinfo=[{'trial',trials(1)}; explain];
odata={freg(1) oinfo};
for tind=2:length(trials)
    odata=[odata; {freg(tind) {'trial',trials(tind)}}];
end
        
onenotetable(odata,seqfname('~/svn_trunk/notes/mipool.xml'));