%make a plot of the running time as a function of the number of worker
%threads

clear all;
setpathvars;
datafile=FilePath('RESULTSDIR','bird_song/runtime_080627.mat');

runtime=load(getpath(datafile));
runtime=runtime.runtime;

%sort runtime by numlabs
[nlab sindex]=sort([runtime.numlabs]);
runtime=runtime(sindex);
ftime=FigObj('name','time','height',6,'width',4);

ftime.a=AxesObj('nrows',2,'ncols',1);

setfocus(ftime.a,1,1);

pstyle.Marker='o';
pstyle.MarkerFaceColor='b';
pstyle.LineStyle='none';
hp=plot([runtime.numlabs],[runtime.toc]);
ftime.a(1,1)=addplot(ftime.a(1,1),'hp',hp,'pstyle',pstyle);
ftime.a(1,1)=xlabel(ftime.a(1,1),'Number Labs');
ftime.a(1,1)=ylabel(ftime.a(1,1),'Time(s)');

setfocus(ftime.a,2,1);
%find the smallest number of nodes
[mval,mindex]=min([runtime.numlabs]);
t1=runtime(mindex).toc/runtime(mindex).numlabs;

hp=plot([runtime.numlabs],t1./[runtime.toc]);
ftime.a(2,1)=addplot(ftime.a(2,1),'hp',hp,'pstyle',pstyle);
ftime.a(2,1)=xlabel(ftime.a(2,1),'Number Labs');
ftime.a(2,1)=ylabel(ftime.a(2,1),'Speedup');

ftime=lblgraph(ftime);

odata={ftime;{'Script',mfilename();'Datafile',getpath(datafile);'svn Revision', runtime(1).svnrevision}};

onenotetable(odata,seqfname('~/svn_trunk/notes/timeinfo.xml'));

