%06-08-2008
%
%Make a plot of the mutual information as a function of muporj/sigma
%   for a pool of stimuli.
%This pool is eather selected by randomly selecting all possible inputs
%   or by using a heuristic

clear variables;
setpathvars;

datedir='080721';
setupfile='bsinfomax_setup_002.m';
sfile=fullfile(RESULTSDIR,'bird_song',datedir,setupfile);
xscript(sfile);

data=load(getpath(dsets.datafile));

bssim=data.bssimobj;
trial=bssim.niter;

prior=getpost(bssim.allpost,trial);

windex=1;
bssim.stimobj.mpool=@heurmumax;
[optstim,hpool]=optstiminwav(bssim.stimobj,bssim,windex,prior)

bssim.stimobj.mpool=@randpool;
[optstim,rpool]=optstiminwav(bssim.stimobj,bssim,windex,prior)
scriptname=mfilename();
%%
%****************************************************************
%Make the plots
%****************************************************************
freg=FigObj('width',6,'height',6);

%we get the posteri from the infomax set
cmap=colormap('gray');
%reverse the colormap so darker colors are more informative
cmap=cmap(end:-1:1,:);
colormap(cmap);
%get the limits for the mutual info colorbar
%scale the mi for each point to map it to a color to show the
%information
mimin=min([hpool.mi(:);rpool.mi(:)]);
mimax=max([hpool.mi(:);rpool.mi(:)]);

mumin=min([hpool.mu(:);rpool.mu(:)]);
mumax=max([hpool.mu(:);rpool.mu(:)]);

sigmin=min([hpool.sigma(:);rpool.sigma(:)]);
sigmax=max([hpool.sigma(:);rpool.sigma(:)]);
%create the axes object
nmethods=2;
nfmap=1;
freg.a=AxesObj('nrows',nfmap,'ncols',nmethods);

for tind=1:nfmap
    
    for col=1:2
        if (col==1)
            mi=hpool.mi;
            mu=hpool.mu;
            sigma=hpool.sigma;
            lbl='Heuristic';
        else
             mi=rpool.mi;
            mu=rpool.mu;
            sigma=rpool.sigma;
            lbl='Random';
        end

            setfocus(freg.a,tind,col);
            hold on;

            cscale=(size(cmap,1)-1)/(mimax-mimin);
            micind=(mi-mimin)*cscale;

            hold on;
            %plot each point for this graph and set the color approriately
            for pind=1:length(micind);
                hp=plot(mu(pind),sigma(pind),'.');
                set(hp,'MarkerFaceColor',cmap(floor(micind(pind))+1,:));
                set(hp,'MarkerEdgeColor',cmap(floor(micind(pind))+1,:));
            end
            freg.a(tind,col)=xlabel(freg.a(tind,col),'\mu_\rho');
            freg.a(tind,col)=ylabel(freg.a(tind,col),'\sigma_\rho^2');
            freg.a(tind,col)=title(freg.a(tind,col),lbl);
            set(gca,'ytick',[]);
             xlim([mumin,mumax]);
              ylim([sigmin,sigmax]);


        end
        %add a colorbar
        hc=colorbar;
        freg.a(tind,col)=sethc(freg.a(tind,col),hc);
        %adjust the ticklabels of the colorbar
        %mitick=linspace(mimin,mimax,10);
%         set(hc,'ytick',(mitick-mimin)*cscale);
%         set(hc,'yticklabel',mitick);
        

end

%set the ylabels for the first graph
tind=1;
col=1;
       setfocus(freg.a,tind,col);
       set(gca,'ytickmode','auto');
%**********************************************
%adjust labels
freg=lblgraph(freg);
freg=sizesubplots(freg);

explain='The mutual information in the stimulus pools constructed from a wave file. Note stimuli which had been picked on previous trials were not included in the pool';
oinfo={'script',scriptname;'datafile',sfile;'trial',trial;'windex',windex;'explanation',explain};
odata={freg;oinfo};
onenotetable(odata,seqfname('~/svn_trunk/notes/compmi.xml'));