%04-17-2008
%
%for each spike. Plot the predicted probability of a spike
%on that trial


bdir='080612';
bfile='bsglmfit_setup_001';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
xscript(setupfile);


v=load(getpath(dsets.datafile));
bdata=v.bdata;
allpost=v.allpost;

%which method to use
methods.newton=1;
methods.batchfit=2;
methods.spikeavg=3;
methods.fmap=4;

method=methods.fmap;

%select theta
switch method
    case methods.batchfit
        theta=v.batchfit.theta;
        mname='batch fit';

        
        mobj=v.mobj;
    case methods.newton

        theta=getm(allpost,getlasttrial(allpost));
        mname='Newtons method';
        
        mobj=v.mobj;
    case methods.spikeavg
        %use the spike triggered average

        if isempty(spikeavg)
            spikeavg=bsspiketriggeredaverage(bdata);
        end

        %add a bias term of zero
        %   theta=[spikeavg(:);0];
        theta=[spikeavg(:);-2.8788];   %use the spike triggered average

        
        %use the linear model.
        mobj=v.mobj;
    case methods.fmap
        theta=v.fmap(end).theta;
        mname='Find MAP using fminunc';
        
        mobj=v.mobj;
    otherwise
        error('unknown method');
end

%if ~(strcmp(getdist(getglm(mobj)),'bernoulli'))
%    error('This function assumes bernoulli model');
%end

%get those trials with spikes
trials=gettrialswithspikes(bdata);


%probability of a spike on trials with spike
spikeprob=zeros(1,length(trials));

%probability of no spike on trials with spikes
pnospikeprob=zeros(1,length(trials));

shist=zeros(getshistlen(mobj),1);

for j=1:length(trials)
    trial=trials(j);
    [bdata,sr]=gettrial(bdata,trial);
    if (mod(j,100)==0)
        fprintf('Spike: %d of %d\n',j,length(trials));
    end

nobsrv=trial-1;
for ind=1:min(getshistlen(mobj),nobsrv)
    
    [bdata,sr]=gettrial(bdata,trial-ind);
    shist(ind)=getobsrv(sr);
end
%    spikeprob(j)=compexpr(mobj,getinput(sr),theta,shist);
%compute the log-likelihood of the observation
    spikeprob(j)=exp(loglike(getglm(mobj),getobsrv(sr),compglmproj(mobj, getinput(sr),theta,sr)));
    
    %probability of no spikes
    onospike=0;
    pnospike(j)=exp(loglike(getglm(mobj),onospike,compglmproj(mobj, getinput(sr),theta,sr)));
end

%%
%how many trials with no spikes to analyze
nnospikes=2000;
%randomly select a set of trials in which we didn't get any spikes and
%compute the probability of no spike
[bdata, ntrials]=getntrials(bdata);
tnospikes=ones(1,ntrials);
tnospikes(trials)=0;
tnospikes=find(tnospikes==1);

tnospikes=tnospikes(ceil(rand(1,nnospikes)*length(tnospikes)));



for j=1:nnospikes
    trial=tnospikes(j);
    [bdata,sr]=gettrial(bdata,trial);
    if (mod(j,100)==0)
        fprintf('Trials no spikes: %d of %d\n',j,nnospikes);
    end

    pspikenospike(j)=compexpr(mobj,getinput(sr),theta,[]);
end
%*****************************************************
%make a plot
%**************************************************
%%
fpred=FigObj('name','predicted spike probability');
fpred.a=AxesObj('nrows',2,'ncols',1);

row=1;
col=1;
fpred.a=setfocus(fpred.a,row,col);
hp=plot(1:length(trials),spikeprob,'.');
%fpred.a=addplot(fpred.a,hp);
fpred=lblgraph(fpred);
xlim([1 length(trials)]);
fpred.a(row,col)=ylabel(fpred.a(row,col),'p(r|x,\mu_t)');
fpred.a(row,col)=title(fpred.a(row,col),'trials with spikes');

xlim([1 length(trials)]);

%trials without spikes
row=2;
col=1;
fpred.a=setfocus(fpred.a,row,col);
hp=plot(1:length(pspikenospike),pspikenospike,'.');
%fpred.a=addplot(fpred.a,hp);
xlim([1 length(pspikenospike)]);

fpred.a(row,col)=ylabel(fpred.a(row,col),'p(r=1|x,\mu_t)');
fpred.a(row,col)=title(fpred.a(row,col),'subset of trials without spikes');

%trials without s

%compute the fraction correct
fspikecorrect=length(find(spikeprob>.5))/length(spikeprob)*100;
fnospikecorrect=length(find(pspikenospike<.5))/length(pspikenospike)*100;
otbl={'script','bsplotspikepred';'Data file',setupfile};
otbl=[otbl;{'Spikes (%correct)', sprintf('%0.3g%%',fspikecorrect)}];
otbl=[otbl;{'No Spikes (%correct)', sprintf('%0.3g%%',fnospikecorrect)}];
otbl=[otbl;{'Method', mname}];
otbl=[otbl;{'Description', 'For each trial on which we observed a spike, we compute the probability of a spike on that trial under our fitted, logistic model.'}];
odata={fpred;otbl};
onenotetable(odata,seqfname('~/svn_trunk/notes/bsspikeprob.xml'));

