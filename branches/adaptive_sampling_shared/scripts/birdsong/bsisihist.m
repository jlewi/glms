%4-24-2008
%Make a historgram of the inter-spike interval

setupfile='/home/jlewi/svn_trunk/allresults/bird_song/080422/bsglmfit_setup_002.m';
xscript(setupfile);

%index of the file to plot
wavindex=1;

bdata=load(getpath(dsets.datafile),'bdata');
bdata=bdata.bdata;


%make a historgram of the ISI in terms of trials
trials=gettrialswithspikes(bdata);

%subtract 1 so an isi of - means we get spiking on consequetive trials
isi=diff(trials)-1;
isiedges=[-.5:1:100.5];
isival=isiedges(1:end-1)+.5;

%hist c sets isicounts(end) = to the number of elements that equal
%(isiedges(end). So we can throw this out
[isicounts]=histc(isi,isiedges);
isicounts=isicounts(1:end-1);

%normalize by number of trials
isicounts=isicounts/(length(trials)-1);
fisi=FigObj('name','isi');

fisi.a=AxesObj('xlabel','ISI (in trials)','ylabel','fraction','nrows',2,'ncols',1);
setfocus(fisi.a,1,1);

bar(isival,isicounts);

    lblgraph(fisi);
    xlim([isival(1) isival(end)]);
    
    %plot the cumulative sum in reverse i.e as the isi decrease
    setfocus(fisi.a,2,1);
    hp=plot(isival(end:-1:1),cumsum(isicounts(end:-1:1)));
    fisi.a(2,1)=addplot(fisi.a(2,1),'hp',hp);
    set(gca,'xdir','reverse');
    fisi.a(2,1)=xlabel(fisi.a(2,1),'ISI (in trials)');
    fisi.a(2,1)=ylabel(fisi.a(2,1),'Cumulative Sum');
    
    %compute the fraction which have isi's greater than 0 and less than or
    %equal to the duration of the stimulus on each trial.
    sdim=getstimdim(bdata);
    frac=sum(isicounts(1:sdim(2)-1));

    lblgraph(fisi);
    otbl={'function','bsisihist'};
    otbl=[otbl;{'setup File',getpath(dsets.datafile)}];
    otbl=[otbl;{'Data File',getpath(dsets.datafile)}]; 
    otbl=[otbl;{'Explanation:',sprintf('Top plot histogram of the ISIs. Bottom plot the cumulative sum of the ISI as the ISI decreases. The ISI is measured in terms of the # of trials not time. An ISI of 0 corresponds to spiking on consequctive trials.')}];
    tbl={fisi;otbl};
    onenotetable(tbl,seqfname('~/svn_trunk/notes/isihist.xml'));
