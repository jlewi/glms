%function  bsfindMAPparalleljob(dfiles)
%   dfiles - a structure array of the jobs to load
%
%****************************************************************************
%Date:07-21-2008
%Author: Jeremy Lewi
%Explanation: Submit a paralleljob which finds the MAP of the
%posterior using batch methods.
%
%$Revision$
%
%01-26-2009
%       take as input a structure array containing the names of the files
%       for each simulation.
%       
%
%10-10-2008
%   Create a loop so we can submit multiple jobs
%
%08-07-2008
%   Make it a function so that we don't have to worry about workspace variables
%   having the wrong value.
%
%**************************************************************************
%specify the setup script
%**************************************************************************
function [pjob,dsets,dsetfile]=bsfindMAPparalleljob(dfiles)

%**************************************************************************
%specify the setup script
%**************************************************************************
siminfo=struct();


datedir='081110';

sind=0;
for sind=1:length(dfiles)
    siminfo(sind).dsetfile=dfiles(sind).setupfile;
end


%jdep=5914;
%A string containing a comma separated list of jobs which must complete before this one
%jdep='5970,5971,5972,5973';
jdep=[];

%**************************************************************************
%parameters
%
%The values below should be customized for your setup
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/svn_glms/adaptive_sampling';

%whether or not to capture command window output
capcmdout=false;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=true;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
svndirs={'~/svn_glms'};


%an array of FilePath objects the input files
filesin={};

%an array of FilePath objects specfying the output files
filesout={};

%name to use for the job
jname='template';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='/home/jlewi/svn_trunk/allresults';

%if there's some code you want to execute before starting
%the function (i.e setting the path) you can specify a handle to this
%function
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=@taskstartupfunc;





%**************************************************************************
%update the node on the cluster
%*************************************************************************
if (svnupdate)
    wd=pwd;
    for j=1:length(svndirs)
        if ~exist(svndirs{j},'dir')
            error(['Cannot issue svn update in %s, directory does not ' ...
                'exist.'],svndirs{j});
        end
        cd(svndirs{j});
        fprintf('Issueing svn update in %s \n',svndirs{j});

        system('svn update');

    end
    cd(wd);
end


%****************************************************
%load the data set
%*****************************************************
for sindex=1:length(siminfo)


    %we use rsync to copy the dataset from bayes to hear
    src.host='bayes.neuro.gatech.edu';
    src.fname=siminfo(sindex).dsetfile;

    %copy the dataset to the current directory
    dest.host='';
    dest.fname=fullfile(pwd,'datasettorun.m');

    rsyncfile(src,dest);
    dsets=datasettorun;

    %***********************************************************
    %Determine the files to copy to the cluster
    %**********************************************************
    filesin=[];
    fnames=fieldnames(dsets);
    for j=1:length(fnames)
        fname=fnames{j};
        if isa(dsets.(fname),'FilePath');
            filesin=[filesin dsets.(fname)];
        end
    end


    %***********************************************************
    %Determine the files to copy from the cluster to the host
    %**********************************************************
    filesout=[dsets.datafile dsets.mfile dsets.cfile dsets.statusfile];

    if isfield(dsets,'poolfile')
        filesout=[filesout dsets.poolfile];
    end

    %**************************************************************************
    %Setup the scheduler
    sched=distsched();

    psubmit=get(sched,'ParallelSubmitFcn');

    if iscell(psubmit)
        psubmit=[psubmit; {'jdep';jdep}];
    else
        psubmit={psubmit, 'jdep',jdep};
    end

    set(sched,'ParallelSubmitFcn',psubmit);

    %**************************************************************************
    %create the parallel job
    %************************************************************************
    [jobdata]=initjobdata(jname,filesin,filesout,startdir,startupfunc,localhost,localdatadir);
    pjob=createParallelJob(sched);
    pjob.jobData=jobdata;



    %set the pathe dependencies
    %include the path for matlab (i.e path containing setmatlabpath) because
    %my setpaths script isn't correcly adding the directory containing matlab.
    %set(pjob,'PathDependencies',{'/Users/jlewi/svn_trunk/matlab/neuro_cluster/paralleljob','/Users/jlewi/svn_trunk/matlab'});


    nworkers=dsets.nwindexes;
    set(pjob,'MaximumNumberOfWorkers',nworkers);
    set(pjob,'MinimumNumberOfWorkers',nworkers);


    %********************************************************************************************
    %create the task
    %*************************************************************************************************
    nargout=1;
    ptask=createTask(pjob,@BSBatchMLSim.runonfile,nargout,{dsets.datafile,dsets.statusfile});
    set(ptask,'CaptureCommandWindowOutput', capcmdout);
    jobdata.jobname=jname;
    set(pjob,'JobData',jobdata);

    %submit the job
    submit(pjob);

    %***************************************************************************
    %Print sumarry information
    %******************************************************************************
    fprintf('\nJob Name: \t %s\n',get(pjob,'name'));
    fprintf('datafile: \t %s \n',getpath(dsets.datafile));
end
