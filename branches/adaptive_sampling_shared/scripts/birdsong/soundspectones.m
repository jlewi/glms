%Test understanding of spectrogram by creating 
%   some pure tones of different frequencies.

%length of the pulse (S)
tlen=1;

%frequency of the pulse (Hz)
freq=1000;

%sampling frequency (Hz)
fs=4000;

%create the pulse
data=sin(2*pi*freq*(0:1/fs:tlen));


%we want to sample the temporal axis of the spectrom at tsamp intervals
%tsamp should should probably be longer than 2 times the freq;
%tsamp=.1; %sample at 1ms
%set tsamp to multiple of freq
tsamp=1/(freq)*10;
if ((tsamp)<(1/(2*freq)))
    warning('tsamp is shorter than 1/(2*frequency) of the pulse');
end

%noverlap is amount of overlap between adjacent windows in which we compute
%the frequency decomposition
noverlap=0;

%number of frequency points used to compute the dft
%set this to length of each time window
nfft=max(256,2^(floor(log2(fs*tsamp))+1));


fh=figure;
%set the window to 1 period

spectrogram(data,fs*tsamp,noverlap,nfft,fs,'yaxis');
title(sprintf('nfft=%d',nfft));
set(gca,'yscale','log')

%%
%**************************************************************
%**************************************
%compute and plot the fft
%subtract 1 from number of samples b\c we have to include the zero frequency
N=(length(data));
%radians per sample
%sampling period
dt=1/fs;
dw=2 *pi/(N-1)/dt;
wmax=pi/dt;
w=-wmax:dw:wmax;

%now compute 
figure
 yfft=fft(data,N);
 freq=w./(2*pi);
 plot(freq,abs(fftshift(yfft))/N);