%04-17-2008
%
% Create an image with three rows and several columns
% each row shows plots of the MAP estimate of a 2-d receptive field for a
%       different method



dsets=[];
dind=0;
bdir=fullfile('bird_song','080417');
bfile='bsglmfit_data_001.mat';
dind=dind+1;
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,bfile));
dsets(dind).lbl=sprintf('');

%how many trials to plot
ntrials=5;




%************************************************************************
%Plot the results
%************************************************************************


%dorder is the order in which we want to plot the graphs
dorder=[1];

row=1;

v=load(getpath(dsets(dind).datafile));
bdata=v.bdata;
allpost=v.allpost;

%%
fstrf=FigObj('name','STRF MAPs','width',6.25,'height',5);
fstrf.a=AxesObj('nrows',length(dsets),'ncols',ntrials);

% row=row+1;
[bdata lasttrial]=getntrials(bdata);
trials=ceil(linspace(1,lasttrial,ntrials));
for tind=1:ntrials
    fstrf.a=setfocus(fstrf.a,row,tind);
    %get rid of bias term
    m=getm(allpost,trials(tind));
    m=reshape(m(1:end-1),getstimdim(bdata));
    [t,freqs]=getstrftimefreq(bdata);
    imagesc(t,freqs,m);
    %         set(gca,'ydir','reverse');
    set(gca,'xlim',[t(1) t(end)],'ylim',[freqs(1) freqs(end)]);




    %add a ylabel to each row

    fstrf.a(row,tind)=title(fstrf.a(row,tind),sprintf('trial=%d',trials(tind)));

    
    if (tind>1)
        set(gca,'ytick',[]);
    end
    fstrf.a(row,tind)=xlabel(fstrf.a(row,tind),'time(ms)');
    
end


%make the color limits all the same
ha=getha(fstrf.a);
cl=cell2mat(get(ha,'clim'));
cl=[min(cl(:,1)), max(cl(:,2))];

%add a colorbar
row=1;
tind=ntrials;
fstrf.a=setfocus(fstrf.a,row,tind);
hc=colorbar;
fstrf.a(row,tind)=sethc(fstrf.a(row,tind),hc);
    

fstrf.a=setfocus(fstrf.a,1);
fstrf.a(1,1)=ylabel(fstrf.a(1,1),'Frequency(hz)');

%label the graphs
fstrf=lblgraph(fstrf);
fstrf=sizesubplots(fstrf);



%previewfig(fstrf.hf,'bounds','tight','FontMode','Fixed','FontSize',fstrf.FontSize,'Width',gparam.width,'Height',gparam.height)
%fgoutfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/gabormaps.eps';
%saveas(gethf(fstrf),fgoutfile,'epsc2');

odata={fstrf;{'function','bsplotmseries'; 'Data File',getpath(dsets(1).datafile);'Explanation','The MAPS.'}};
onenotetable(odata,seqfname('~/svn_trunk/notes/bsmaps.xml'));