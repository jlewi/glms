%04-05-2008
%
%This is a script for setting up distributed job on the cluster to
%compute the spike triggered average


dsets=[];



setpaths;

%*********************************************************************
%data set
%************************************************************************
suffix=001;
dsets.bdatafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/080407/bsglmfit_data_001.mat');
statusfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(getrpathdir(dsets.bdatafile),'spikeavg_status.txt'));

%file to save a plot of the mean to
xmlfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(getrpathdir(dsets.bdatafile)),sprintf('spikeavg_%s.xml',suffix));

%mat file to save the spike triggered average to
savgfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(getrpathdir(dsets.bdatafile)),sprintf('spikeavg_%s.mat',suffix));

%**************************************************************
%define the hosts
%****************************************************************
bayes.hostid='bayes';
bayes.host='bayes.neuro.gatech.edu';
bayes.RESULTSDIR='/home/jlewi/svn_trunk/allresults';

cluster.hostid='cluster';
cluster.host='';
cluster.RESULTSDIR='/Users/jlewi/datafiles';



bcmd.(bayes.hostid)=bayes.RESULTSDIR;
bcmd.(cluster.hostid)=cluster.RESULTSDIR;



%*******************************************************************
%use rsync to copy data files from bayes to cluster
%************************************************************

dfields=fieldnames(dsets);

for ind=1:length(dfields)
    if isa(dsets.(dfields{ind}),'FilePath')

        %this field stores a file
        %so we create a RemoteFilePath object and then sync the file
        rfile=RemoteFilePath('bcmd',bcmd,'rpath',getrpath(dsets.(dfields{ind})));

        src.host=bayes.host;
        src.fname=getpath(rfile,bayes.hostid);


        dest.host=cluster.host;
        dest.fname=getpath(rfile,cluster.hostid);

        rsyncfile(src,dest);
    end
end

%**********************************************************************
%run svn update - but don't do it on bayes.
[s,hname]=system('hostname -s');

if ~(strcmp(deblank(hname),'bayes'))
    fprintf('Issueing svn update \n');
    system('svn update');
    wd=pwd;
    fprintf('svn update matlab path \n');
    cd(MATLABPATH);
    system('svn update');
    cd(wd);
end
%**************************************************************************
%create the jobs
%**************************************************************************
%set the startup directory
startdir='/Users/jlewi/svn_trunk/adaptive_sampling';
sched=findResource('scheduler','type','generic')
%specify the directory where the Job Data is stored
%use a directory different from our project directory.
set(sched,'DataLocation','/Users/jlewi/jobs');
set(sched,'HasSharedFilesystem',true);
set(sched,'ClusterMatlabRoot','/Applications/MATLAB_R2007b');
set(sched,'SubmitFcn',{@sgeSubmitFcn,startdir});

task=cell(1,length(dsets));
%create the job
job=createJob(sched);
for dind=1:length(dsets)
    %number of output arguments
    naout=0;


    %input arguments
    iparam={dsets.bdatafile,};

    task{dind}=createTask(job,@bsspiketriggeredaverage,naout,iparam);

    %task properties
    set(task{dind},'CaptureCommandWindowOutput',0);

    %to specify a nonstandard task name we use the userdata property of
    %the task
    %we set the name to fsetoutname and the index of the dataset
    [fpath fdata]=fileparts(getrpath(dsets.bdatafile));

    udata.taskname=sprintf('spikeavg.%s',fdata);
    task{dind}.UserData=udata;

end

submit(job);


%**************************************************************************
%create a task to rsync the files back to bayes
%**************************************************************************
ftotransfer=[xmlfile savgfile];
for dind=1:length(dsets)


    %if isempty(task{dind}.UserData)
    %  fjdep=sprintf('%sJob%d.%d',fjdep,jnum,tid);
    %else
    fjdep=task{dind}.UserData.taskname;
    %end


    set(sched,'SubmitFcn',{@sgeSubmitFcn,startdir,fjdep});



    fjob=createJob(sched);



    dfields=fieldnames(dsets);

    for ind=1:length(ftotransfer)

            %this field stores a file
            %so we create a RemoteFilePath object and then sync the file
            rfile=RemoteFilePath('bcmd',bcmd,'rpath',getrpath(ftotransfer(ind)));

            src.host=cluster.host;
            src.fname=getpath(rfile,cluster.hostid);


            dest.host=bayes.host;
            dest.fname=getpath(rfile,bayes.hostid);

            %create a task for copying this file
            naout=0;
            %parameters for the task
            iparam={src,dest};
            rfiletask{dind}=createTask(fjob,@rsyncfile,naout,iparam);
            udata.taskname=sprintf('rsync.%s',dfields{ind});
            rfiletask{dind}.UserData=udata;

    end

    submit(fjob);
end



