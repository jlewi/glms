%05-27-2008
%
%for a particular wave file we compute the average across repeats of the
%wave file of the number of observations
%
%we comapre this average to the expected value of the firing rate


bdir='080602';
bfile='bsglmfit_setup_001';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
xscript(setupfile);


data=load(getpath(dsets.datafile));
bdata=data.bdata;
allpost=data.allpost;

%which method to use
methods.newton=1;
methods.batchfit=2;
methods.spikeavg=3;
methods.fmap=4;
method=methods.fmap;

%use a low rank approximation of the strf
lrank=[];

%noise floor force all elements of strf with value less than this to zeros
%set to zero for no forcing to zero.
strfnfloor=0;

%if true then we don't plot the results for individual wave files
%just the M.S.D for each file
pjusttotal=false;

%select theta
switch method
    case methods.batchfit
        theta=data.batchfit.theta;
        mname='batch fit';

        
        mobj=data.mobj;
    case methods.newton

        theta=getm(allpost,getlasttrial(allpost));
        mname='Newtons method';
        
        mobj=data.mobj;
    case methods.spikeavg
        %use the spike triggered average

        if isempty(spikeavg)
            spikeavg=bsspiketriggeredaverage(bdata);
        end

        %add a bias term of zero
        %   theta=[spikeavg(:);0];
        theta=[spikeavg(:);-2.8788];   %use the spike triggered average

        
        %use the linear model.
        mobj=data.mobj;
    case methods.fmap
        theta=data.fmap(end).theta;
        mname='Find MAP using fminunc';
        
        mobj=data.mobj;
    otherwise
        error('unknown method');
end



wfiles=[1 15];
mseerr=zeros(1,length(wfiles));

%**************************************************************
%Post processing of the stf
%************************************************************
%compute low rank approximation if told to do so
[strf shistcoeff bias]=parsetheta(mobj,theta);
strf=reshape(strf,getstrfdim(data.bdata));
%enforce noise floor of the strf
ind=abs(strf)<strfnfloor;
strf(ind)=0;

if ~isempty(lrank)
    
    [u s v]=svd(strf);
     strf=u(:,1:lrank)*s(1:lrank,1:lrank)*v(:,1:lrank)';

end

theta=packtheta(mobj,strf(:),shistcoeff,bias);

%********************************************************
%end post processing
%************************************************************
wcount=0;
for wavfile=wfiles
wcount=wcount+1;

%get the stimulus and responses for this wavefile
[stimspec,obsrv]=getallwaverepeats(bdata,wavfile);

[ntrials,cstart]=getntrialsinwave(bdata,wavfile);
meanobsrv=mean(obsrv,1);
avgspike=meanobsrv(:,cstart:end);


bspost=BSlogpost('bdata',bdata);

%compute the expected firing rate
[bdata,stimspec]=getfullspec(bdata,wavfile);

glmproj=compglmprojimp(bspost,mobj,theta,stimspec,meanobsrv,ntrials);
    exprate=compmu(getglm(mobj),glmproj(1,:));


%compute the mse
mdist=(sum((exprate-avgspike).^2)/length(exprate))^.5;
mseerr(wcount)=mdist;


%*****************************************************
%make a plot
%**************************************************
%%
if ~(pjusttotal)
fpred=FigObj('name','predicted spike probability');
fpred.a=AxesObj('nrows',1,'ncols',1);

t=[cstart:(cstart+ntrials-1)]*getobsrvtlength(bdata);
row=1;
col=1;
fpred.a=setfocus(fpred.a,row,col);

hp=plot(t,avgspike);
pstyle=struct('linestyle','-','color','r','linewidth',3);
fpred.a(row,col)=addplot(fpred.a(row,col),'hp',hp,'pstyle',pstyle,'lbl','empirical');

hp=plot(t,exprate);
pstyle=struct('linestyle','-','color','b','linewidth',2);
fpred.a(row,col)=addplot(fpred.a(row,col),'hp',hp,'pstyle',pstyle,'lbl','model');


fpred=lblgraph(fpred);
xlim([1 length(exprate)]);
fpred.a(row,col)=ylabel(fpred.a(row,col),'E(r)');
fpred.a(row,col)=xlabel(fpred.a(row,col),'t(s)');
fpred.a(row,col)=title(fpred.a(row,col),'Predicted firing rate');

xlim([t(1) t(end)]);


%compute the fraction correct
otbl={'script','bsplotspikerate';'Data file',setupfile};
otbl=[otbl;{'Wavefile', wavfile}];
otbl=[otbl;{'Wavefile is bird song', waveissong(bdata,wavfile)}];
otbl=[otbl;{'Method', mname}];
if isempty(lrank)
   otbl=[otbl; {'Rank', sprintf('strf is approximated as rank %d matrix',lrank)}]; 
end
otbl=[otbl;{'M.S.D.',mseerr(wcount)}];
otbl=[otbl;{'Description', 'For each of the trials we compute from this wave file, we compute the expected firing rate and compare it to the empirical firing rate (i.e the average of the observations with respect to repeated presentations of each wave file.). M.S.D is the mean squared distance between the avg spike rate and the predicted spike rate.'}];
odata={fpred;otbl};
onenotetable(odata,seqfname('~/svn_trunk/notes/bsexprate.xml'));
end
end

%%
%make a plot of the mse of all wavefiles
fmse=FigObj('name','mse','width',5,'height',3);
fmse.a=AxesObj('xlabel','Wave file','ylabel', 'M.S.E');

hp=plot(wfiles,mseerr);
fmse.a=addplot(fmse.a,'hp',hp);

fmse=lblgraph(fmse);

ot={'script','bsplotspikerate';'Datafile',setupfile};
ot=[ot;{'STRF noise floor', strfnfloor}];
ot=[ot;{'Total M.S.D (all files)', sum(mseerr)}];
ot=[ot;{'Explanation', 'All coefficients of the STRF with absolute value less than STRF noise floor are forced to zero. The MSD is the mean squared distance between the predicted and empirical firing rate for each wavefile'}];
if ~isempty(lrank)
   ot=[ot; {'Rank', sprintf('strf is approximated as rank %d matrix',lrank)}]; 
end
odata={fmse;ot};

onenotetable(odata,seqfname('~/svn_trunk/notes/bsexpratemse.xml'));

