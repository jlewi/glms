%07-09-2008
%
%Suppose we we want to comput sum(x(t)y(tau-t)) for several values of tau
%is it faster to use convolution to compute the dot product for all taus
%or to use custom code to compute it for just that taus we want

%length of the signals
npts=[10 100 1000 2500 5000 7500 10000];
npts=[100 1000 10000];
%how many tau to compute it at
%the actual values of tau are tau=0:ntau-1
%ntau=[10 100::1000];
ntau=[10 100 1000 2500 10000];
ntau=[10 100 1000];



dcomp.time=zeros(length(npts), length(ntau));
dcomp.time=nan*dcomp.time;
cresults.time=zeros(length(npts), length(ntau));
cresults.time=nan*cresults.time;
cresults.v=cell(length(npts), length(ntau));
dcomp.v=cell(length(npts), length(ntau));
for pind=1:length(npts)
    x=rand(1,npts(pind));
    y=rand(1,npts(pind));


    for tind=1:length(ntau);
        %only go up to ntau <npts
        if (ntau(tind)<=npts(pind))
            %************************************************************
            %results using convolution
            %***********************************************************

            
            tic
            yrev=y(1,end:-1:1);
            xyconv=conv(x,yrev);
            cresults.v{pind,tind}=xyconv(1,npts(pind):npts(pind)+ntau(tind)-1);
            cresults.time(pind,tind)=toc;



            %**************************************************************
            %direct computation
            %*************************************************************

            tic;
            v=zeros(1,ntau(tind));


            for tau=0:ntau-1
                v(tau+1)=x(1,tau+1:end)*y(1,1:end-tau)';
            end
            dcomp.v{pind,tind}=v;
            dcomp.time(pind,tind)=toc;
        end
    end
end

%**************************************************************************
%Make a plot
%************************************************************************
%%
fh=FigObj('name','timing');
fh.a=AxesObj('xlabel','ntau','ylabel','log10 time(s)');

hold on;
% [xmat,ymat]=meshgrid(npts,ntau);
%
%
% hdirect=plot3(xmat(:),ymat(:),log10(dcomp.time(:)),'bo');
% set(hdirect,'markerfacecolor','b');
% set(hdirect,'markersize',5);
% hconv=plot3(xmat(:),ymat(:),log10(cresults.time(:)),'ro');
% set(hconv,'markersize',5);
% set(hconv,'markerfacecolor','r');

%add a line for each ntau
for pind=length(npts):-1:1

    %find the last ntau less than npts
    lind=find(~isnan(cresults.time(pind,:)),1,'last');

    if (lind>=1)
    xpts=npts(pind)*ones(1,lind);
    ypts=ntau(1:lind);

     hconv=plot(ypts,log10(cresults.time(pind,1:lind)));
%     set(hconv,'markersize',5);
%     set(hconv,'markerfacecolor','r');
%     set(hconv,'linestyle','-');

%     pstyle.markersize=5;
%     pstyle.markerfacecolor='r';
%     pstyle.linestyle='-';
    pstyle=plotstyle((pind-1)*2+1);
    pstyle.LineWidth=    pstyle.LineWidth*3;
    fh.a=addplot(fh.a,'hp',hconv,'pstyle',pstyle,'lbl',sprintf('conv: npts=%d',npts(pind)));
    
    hdirect=plot(ypts,log10(dcomp.time(pind,1:lind)),'bo');
    
    pstyle=plotstyle((pind)*2);
        pstyle.LineWidth=    pstyle.LineWidth*3;
    fh.a=addplot(fh.a,'hp',hdirect,'pstyle',pstyle,'lbl',sprintf('direct: npts=%d',npts(pind)));
    
%     set(hdirect,'markerfacecolor','b');
%     set(hdirect,'markersize',5);
%     set(hdirect,'linestyle','-');
%     pstyle.markersize=5;
%     pstyle.markerfacecolor='bo';
%     pstyle.linestyle='-';
   
    end
end

fh=lblgraph(fh);

sname=mfilename();
otbl={{fh};{'script',sname;'explanation', 'Compare the running time of computing \sum x(t) y(tau-t) for several taus using convolution vs. using a for loop over tau'}};

onenotetable(otbl,seqfname('~/svn_trunk/notes/timecomparison.xml'));