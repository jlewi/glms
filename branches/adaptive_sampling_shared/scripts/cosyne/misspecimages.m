%make plots of the model misspecification results
fname=fullfile(RESULTSDIR,'misspec/02_09/02_09_poisslogcanon_001.mat');
fontsize=30;
yticks=[1 1000 2000];
xticks=[1 50];
postimgfix('fname',fname,'width',6,'height',4,'xticks',xticks,'fsize',fontsize,'yticks',yticks,'nmag',1,'trials',[1:2000])


fname=fullfile(RESULTSDIR,'misspec/02_15/02_15_poisspowercanon_001.mat')
fontsize=30;
yticks=[1 1000];
xticks=[1 50];
postimgfix('fname',fname,'width',6,'height',4,'xticks',xticks,'fsize',fontsize,'yticks',yticks,'nmag',1)
