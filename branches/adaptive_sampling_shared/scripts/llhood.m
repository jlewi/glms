%function [p]= 1dllhood(y,x,theta,binsize)
%       y - value of the output (1 or 0)
%           should be single number
%       x - value of the input
%       theta - the model parameter
%       binsize - number of bins for continuous to discrete approximation

%Return Value:
%    p =size(x) by size(theta) pdf table 
%       p[x,theta]=p[y|x,theta]
%Explanation: The conditional likelihood p(y|x,theta) for the 1-d example 
%given in Paninski05. 
%
%The number of bins for x and theta is used to handle the conversion froma
%continuous approximation to a discrete approximation. This ensures that
%p(y|x,theta) sums to 1 when summed over x,theta
%
%maxp sets the probability of a 1 when x=truetheta
function p= llhood(y,x,theta,sigma,binsize,maxp)
    if (max(size(y))>1)
        error('llhood: y should be a scalar');
    end
    
    if (min(size(x))<1)
        error('llhood:x needs to be a vector');
    end
    x=rowvector(x);
    
    if (min(size(theta))<1)
        error('llhood:theta needs to be a vector');
    end
   theta=rowvector(theta);
    
    %create a matrix 
    p=zeros(length(x),length(theta));
    
    %this is unecessary
    %Integral p(y|x,theta) over y = 1. It doesn't need to integrate
    %to 1 over x,theta
    
    %To handle the discrete approximation of a continuous distribution we 
    %create a lookup table parameterized by abs(x-thetha)
    %mindist=-1;
    %maxdist=1;
    %diffp=normcdf(mindist:binsize:maxdist,0,sigma);
    %diffp=[diffp(1) diffp(2:end)-diffp(1:end-1)];
    
    %normalize it by putting all remaining probability in last bin
    %diffp(end)=diffp(end)+1-sum(diffp);
    
    %we now compute a matrix of distances
    %dist=x'*ones(1,length(theta))-ones(length(x),1)*theta;
    
    %we find the indexes into dist for each of these 
    %distances
    %dist=dist-mindist;  
    %dist(find(dist<=0))=binsize; %this way it goes to bin 1
    %dist=ceil(dist/binsize);
    %dist(find(dist>length(diffp)))=length(diffp);
    %p=diffp(dist);
    
    %maxp is probability y=1 when x=theta
    
    p=normpdf(x'*ones(1,length(theta))-ones(length(x),1)*theta,0,sigma);
    
    %compute the value of p when x=theta
    pxtheta=normpdf(0,0,sigma);
    %p/pxtheta expresses probability relative to pxtheta
    %multiplying by maxp then scales it so that maxp occurs when
    %x=truetheta
    p=maxp*p/pxtheta;
    %8-30 following line was wrong when do just a scalar
    %p ends up being= to maxp which is totatlly wrong
    %p=maxp*max(max(p))/*p;
    
    %p is the probability the output is 1. It can never be larger than 1.
    %b\c 1-p is probability it = 0, which also must be less than 1. 
    ind=find(p>=1);
    p(ind)=1-eps;
    
    ind=find(p==0);
    p(ind)=eps;
    
    %the above computes the probability if y=1 so we need to handle
    %case y=0;
    if(y==0)
        p=1-p;
    end