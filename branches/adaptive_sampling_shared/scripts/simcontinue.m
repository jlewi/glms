% simcontinue(fname,simvar,niter)
%   fname - file containing object
%   simvar - name of variable
%   niter  - number of iterations to run
%   finfo  - file where we want to print messages to
%Revision 
%   11-12-07 - reweritten using new object model
%
%   If an error occurs save the simulation 
function  [simobj]=simcontinue(fname,simvar,niter,finfo)

if ~exist('finfo','var')
    finfo=[];
end

simobj=SimulationBase('fname',fname,'simvar',simvar);
%try
simobj=update(simobj,niter,finfo);
savesim(simobj,fname);
%catch
   %if an error occured save the simulation to a file named
   %fname_aborted
   %This doesn't help because simobj won't have been updated 
   %I need to catch the error in update.
   %if isa(fname,'FilePath')
    %   fname=getpath(fname);
   %end
   %[fdir fn fext]=fileparts(fname);
  % ferr=seqfname(fullfile(fdir,sprintf('%s_aborted.mat',fn)));
 %  savesim(simobj,ferr);
%   fprintf('Simulation aborted due to error. \n Saved to %s \n',ferr);
       
%end