%******************************************************************
%define the files of 40x40 gabor files for my NC06 results
%These results use my new heuristic
%*******************************************************************
dsets=[];





dsets=[];
dind=0;
bdir=fullfile('gabor','071217');
bfile='gabor2d';
niter=5000;

%name of the simulation variables
simvars={'maxheur','maxheur'}

%start index for the sequential indexing of the files
startind=3;


for sind=1:length(simvars)
  dind=dind+1;
dsets(dind).fname=fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,sind-1+startind));
dsets(dind).simvar=simvars{sind};
dsets(dind).niter=niter;

end


%**********************************************************
%second heuristic
dsets=[];
dind=0;
bdir=fullfile('gabor','071218');
bfile='gabor2d';
niter=5000;

%name of the simulation variables
simvars={'maxheur'}

%start index for the sequential indexing of the files
startind=1;


for sind=1:length(simvars)
  dind=dind+1;
dsets(dind).fname=fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,sind-1+startind));
dsets(dind).simvar=simvars{sind};
dsets(dind).niter=niter;

end
