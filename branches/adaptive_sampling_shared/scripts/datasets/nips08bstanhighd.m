%function dfiles=nips08bstanhighd()
%   
%Explanation: The birdsong datasets for nips08.
%
function [dfiles,pstyles]=nips08bstanhighd()
dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'lstyles','colors','markers'};
pstyles.linewidth=pstyles.linewidth;


taninfo.class='MTLowRank';
taninfo.rank=2;
%**********************************************************************
%shuffled design
%**************************************************************************
bdir='080730';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Shuffled design. Use full posterior.';
dfiles(dind).lbl='Shuffled: Full post.';

bdir='080730';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Shuffled design. Use posterior on tangent space.';
dfiles(dind).lbl='Shuffled: Tan post.';
dfiles(dind).taninfo=taninfo;

%*************************************************************************
%info. max.
%************************************************************************
bdir='080801';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. use posterior on full theta space.';
dfiles(dind).lbl='Info. Max.: Full post.';

%use full posterior
bdir='080801';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. use posterior on tangent space.';
dfiles(dind).lbl='Info. Max.: Tan post.';
dfiles(dind).taninfo=taninfo;

%**************************************************************************
%Info. Max. Tangent space: use full posterior
%**************************************************************************
dind=dind+1;
bdir='080731';
bfile='bsinfomax_setup_006';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;

%use this dataset to define the tangent space for other models. 
dfiles(dind).deftanspace=true;
dfiles(dind).explain='Info. Max using tangent space. Use posterior on full space.';
dfiles(dind).lbl='Info. Max. Tan: Full post.';

%use posterior on the tangent space
dind=dind+1;
bdir='080731';
bfile='bsinfomax_setup_006';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max using tangent space. Use posterior on tangent space.';
dfiles(dind).lbl='Info. Max. Tan: Tan post.';

