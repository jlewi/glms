%******************************************************************
%define these data files are used to illustrate enforcing of the low rank
%prior by using the tangent space, for a space-time separable receptive
%field.
%*******************************************************************
dsets=[];









dsets=[];
dind=0;
bdir=fullfile('tanspace','080124');
bfile='gammatone';
niter=2000;


%************************************************************
%infomax using tangent space
%*************************************************************
dind=dind+1;
findex=7;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='infomax10';
dsets(dind).niter=niter;
dsets(dind).lbl='Info. Max: 10';

dind=dind+1;
findex=8;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='infomax100';
dsets(dind).niter=niter;
dsets(dind).lbl='Info. Max:100';

dind=dind+1;
findex=9;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='infomax300';
dsets(dind).niter=niter;
dsets(dind).lbl='Info. Max:300';

dind=dind+1;
findex=10;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='infomax400';
dsets(dind).niter=niter;
dsets(dind).lbl='Info. Max:400';


%****************************************************************
%info max on full posterior
%****************************************************************
dind=dind+1;
findex=11;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='infomaxfull';
dsets(dind).niter=niter;
dsets(dind).lbl='Info. Max. Full';



%**************************************************
%random design
dind=dind+1;
findex=12;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='rand';
dsets(dind).niter=niter;
dsets(dind).lbl='i.i.d.';

%****************************************
%create a structure whose fieldnames are simvar and whose values
%are the corresponding index into dsets
 dnames=[];
 for dind=1:length(dsets)
     dnames.(dsets(dind).simvar)=dind;
 end

