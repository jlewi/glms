%function [dfiles,pstyles]=nips08bstanhighd()
%   
%return value:
%   dfiles - array of dsets
%   pstyles - object defining the plot styles for this class
%
%Explanation: The birdsong datasets for nips08.
%
function [dfiles,pstyles]=nips08bsbatchlowd()
dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'lstyles','colors','markers'};
pstyles.order={'colors','lstyles','markers'};
pstyles.linewidth=pstyles.linewidth;

smooth.nfcutoff=15;
smooth.ntcutoff=3;

%**********************************************************
%Batch Shuffle
%**************************************************************
dind=dind+1;
bdir='080828';
bfile='bsinfomax_setup_003';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).explain='Shuffled. Use full posterior.';
dfiles(dind).lbl='Shuffled:';

%use the posterior on the tangent space
bdir='080828';
bfile='bsinfomax_setup_003';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).taninfo=[];
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Shuffled full';
if (dfiles(dind).usetanpost)
    dfiles(dind).lbl=sprintf('%s:Tan. Post',dfiles(dind).lbl);
end
if ~isempty(dfiles(dind).smooth)
     dfiles(dind).lbl=sprintf('%s:nf<%g nt<%g',dfiles(dind).lbl,smooth.nfcutoff,smooth.ntcutoff);
end
explain={'Method:', 'Shuffled'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Use tanpost:', dfiles(dind).usetanpost}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;


dind=dind+1;
bdir='080828';
bfile='bsinfomax_setup_003';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='shuffle';
dfiles(dind).lbl='Shuffled: Tan. post';
dfiles(dind).taninfo.class='MTLowRank';
dfiles(dind).taninfo.rank=2;

bdir='080828';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
startfull='n/a';

[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo.class='MTLowRank';
dfiles(dind).taninfo.rank=2;
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Shuffled';
if (dfiles(dind).usetanpost)
    dfiles(dind).lbl=sprintf('%s:Tan. Post',dfiles(dind).lbl);
end
if ~isempty(dfiles(dind).smooth)
     dfiles(dind).lbl=sprintf('%s:nf<%g nt<%g',dfiles(dind).lbl,smooth.nfcutoff,smooth.ntcutoff);
end
explain={'Method:', 'Shuffled'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Tangent space:', {'on',dfiles(dind).usetanpost; 'startfull (#trials at start which use full posterior)', startfull}}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;

%**********************************************************************
%BatchPoissLB Tan Space with Numerical Integration
%**************************************************************************
% bdir='080922';
% bfile='bsinfomax_setup_001';
% dind=dind+1;
% setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
% [data]=xfunc(setupfile);
% dfiles=copystruct(dfiles,dind,data);
% dfiles(dind).explain='Info. Max. using tangent space and lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
% dfiles(dind).lbl='Info. Max. Tan: Full post.';
% dfiles(dind).usetanpost=false;
% 
% %use the posterior on the tangent space
% bdir='080922';
% bfile='bsinfomax_setup_001';
% dind=dind+1;
% setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
% [data]=xfunc(setupfile);
% dfiles=copystruct(dfiles,dind,data);
% dfiles(dind).usetanpost=true;
% dfiles(dind).explain='Info. Max. using tangent space and lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration. Use the posterior on the tangent space.';
% dfiles(dind).lbl='Info. Max. Tan: Tan post.';
% dfiles(dind).usetanpost=true;

%*************************************************************************
%info. max.
%************************************************************************
bdir='080921';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max. full: Full post.';
dfiles(dind).usetanpost=false;

%full posterior + smoothing
bdir='080921';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).taninfo=[];
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Info. Max. full';
if (dfiles(dind).usetanpost)
    dfiles(dind).lbl=sprintf('%s:Tan. Post',dfiles(dind).lbl);
end
if ~isempty(dfiles(dind).smooth)
     dfiles(dind).lbl=sprintf('%s:nf<%g nt<%g',dfiles(dind).lbl,smooth.nfcutoff,smooth.ntcutoff);
end
explain={'Method:', 'Infomax'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Use tanpost:', dfiles(dind).usetanpost}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;

%use the posterior on the tangent space
bdir='080921';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration. Use the posterior on the tangent space.';
dfiles(dind).lbl='Info. Max. full: Tan post.';
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo.class='MTLowRank';
dfiles(dind).taninfo.rank=2;


%use the posterior on the tangent space
bdir='080921';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo.class='MTLowRank';
dfiles(dind).taninfo.rank=2;
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Info. Max. full';
if (dfiles(dind).usetanpost)
    dfiles(dind).lbl=sprintf('%s:Tan. Post',dfiles(dind).lbl);
end
if ~isempty(dfiles(dind).smooth)
     dfiles(dind).lbl=sprintf('%s:nf<%g nt<%g',dfiles(dind).lbl,smooth.nfcutoff,smooth.ntcutoff);
end
explain={'Method:', 'Infomax'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Use tanpost:', dfiles(dind).usetanpost}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;


%**************************************************************************
%info. max. stronger prior
%***********************************************************************
% bdir='080924';
% bfile='bsinfomax_setup_004';
% dind=dind+1;
% setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
% [data]=xfunc(setupfile);
% dfiles=copystruct(dfiles,dind,data);
% dfiles(dind).explain='Info. Max. using lower bound for the mutual info of the batch. Use a larger, stronger prior.';
% dfiles(dind).lbl='IM Prior: Full post.';
% dfiles(dind).usetanpost=false;
% 
% %use the posterior on the tangent space
% bdir='080921';
% bfile='bsinfomax_setup_001';
% dind=dind+1;
% setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
% [data]=xfunc(setupfile);
% dfiles=copystruct(dfiles,dind,data);
% dfiles(dind).usetanpost=true;
% dfiles(dind).explain='Info. Max. using lower bound for the mutual info of the batch. Use a larger stronger prior.';
% dfiles(dind).lbl='IM Prior: Tan post.';
% dfiles(dind).usetanpost=true;
% dfiles(dind).taninfo.class='MTLowRank';
% dfiles(dind).taninfo.rank=2;


%**************************************************************************
%info. max. use full info.max on the first 50 trials
%***********************************************************************
bdir='080925';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. Iterfullmax=25, startfullmax=50.';
dfiles(dind).lbl='Info. Max. Tan: Full post sf=50';
dfiles(dind).usetanpost=false;

%use the posterior on the tangent space
bdir='080925';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. Iterfullmax=25, startfullmax=50.';
dfiles(dind).lbl='Info. Max. Tan: tan post sf=50';
dfiles(dind).usetanpost=true;

%use the posterior on the tangent space
bdir='080925';
bfile='bsinfomax_setup_001';
startfull=50;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo.class='MTLowRank';
dfiles(dind).taninfo.rank=2;
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Info. Max. Tan';
if (dfiles(dind).usetanpost)
    dfiles(dind).lbl=sprintf('%s:Tan. Post sf=%g',dfiles(dind).lbl,startfull);
end
if ~isempty(dfiles(dind).smooth)
     dfiles(dind).lbl=sprintf('%s:nf<%g nt<%g',dfiles(dind).lbl,smooth.nfcutoff,smooth.ntcutoff);
end
explain={'Method:', 'Infomax'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Tangent space:', {'on',dfiles(dind).usetanpost; 'startfull (#trials at start which use full posterior)', startfull}}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;

%**************************************************************************
%info. max. use full info.max on the first 25 trials
%***********************************************************************
bdir='080925';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. Iterfullmax=25, startfullmax=25.';
dfiles(dind).lbl='Info. Max. Tan: tan post sf=25';
dfiles(dind).usetanpost=false;

%use the posterior on the tangent space
bdir='080925';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. Iterfullmax=25, startfullmax=25.';
dfiles(dind).lbl='Info. Max. Tan: tan post sf=25';
dfiles(dind).usetanpost=true;

bdir='080925';
bfile='bsinfomax_setup_002';
startfull=25;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo.class='MTLowRank';
dfiles(dind).taninfo.rank=2;
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Info. Max. Tan';
if (dfiles(dind).usetanpost)
    dfiles(dind).lbl=sprintf('%s:Tan. Post sf=%g',dfiles(dind).lbl,startfull);
end
if ~isempty(dfiles(dind).smooth)
     dfiles(dind).lbl=sprintf('%s:nf<%g nt<%g',dfiles(dind).lbl,smooth.nfcutoff,smooth.ntcutoff);
end
explain={'Method:', 'Infomax'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Tangent space:', {'on',dfiles(dind).usetanpost; 'startfull (#trials at start which use full posterior)', startfull}}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;

%**************************************************************************
%info. max. use full info.max on the first 100 trials
%***********************************************************************
bdir='080925';
bfile='bsinfomax_setup_003';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. Iterfullmax=25, startfullmax=100.';
dfiles(dind).lbl='Info. Max. Tan: full post sf=100';
dfiles(dind).usetanpost=false;

%use the posterior on the tangent space
bdir='080925';
bfile='bsinfomax_setup_003';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. Iterfullmax=25, startfullmax=100.';
dfiles(dind).lbl='Info. Max. Tan: Tan. Post sf=100';
dfiles(dind).usetanpost=true;

bdir='080925';
bfile='bsinfomax_setup_003';
startfull=100;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo.class='MTLowRank';
dfiles(dind).taninfo.rank=2;
dfiles(dind).smooth=smooth;
dfiles(dind).lbl='Info. Max. Tan:';
if (dfiles(dind).usetanpost)
    dfiles(dind).lbl=sprintf('%s:Tan. Post sf=%g',dfiles(dind).lbl,startfull);
end
if ~isempty(dfiles(dind).smooth)
     dfiles(dind).lbl=sprintf('%s:nf<%g nt<%g',dfiles(dind).lbl,smooth.nfcutoff,smooth.ntcutoff);
end
explain={'Method:', 'Infomax using the tangent space.'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Tangent space:', {'on',dfiles(dind).usetanpost; 'startfull (#trials at start which use full posterior)', startfull}}];
explain=[explain;{'Smoothing:', sprintf('Smooth using nf<%g nt<%g',smooth.nfcutoff,smooth.ntcutoff) }];
dfiles(dind).explain=explain;
