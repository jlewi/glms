%******************************************************************
%define differet data sets that I'm working with
%*******************************************************************
d20dfeat=[];









d20dfeat=[];
dind=0;
bdir=fullfile('tanspace','080129');
bfile='gammatone';
niter=500;

%************************************************************
%infomax in feature space 20d
%*************************************************************
dind=dind+1;
findex=1;
d20dfeat(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
d20dfeat(dind).simvar='infomaxfeat10';
d20dfeat(dind).niter=niter;
d20dfeat(dind).lbl='Info. Max: Feature';

dind=dind+1;
findex=2;
d20dfeat(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
d20dfeat(dind).simvar='infomaxfeatfull';
d20dfeat(dind).niter=niter;
d20dfeat(dind).lbl='Info. Max Full: Feature';



%************************************************************
%infomax in feature space 30d
%*************************************************************
clear d30dfeat;
dind=1;
findex=3;
d30dfeat(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
d30dfeat(dind).simvar='infomaxfeat10';
d30dfeat(dind).niter=niter;
d30dfeat(dind).lbl='Info. Max: Feature';

dind=dind+1;
findex=4;
d30dfeat(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
d30dfeat(dind).simvar='infomaxfeatfull';
d30dfeat(dind).niter=niter;
d30dfeat(dind).lbl='Info. Max Full: Feature';


%************************************************************
%infomax in feature space 40d
%*************************************************************
clear d40dfeat;
dind=1;
findex=5;
d40dfeat(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
d40dfeat(dind).simvar='infomaxfeat10';
d40dfeat(dind).niter=niter;
d40dfeat(dind).lbl='Info. Max: Feature';

dind=dind+1;
findex=6;
d40dfeat(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
d40dfeat(dind).simvar='infomaxfeatfull';
d40dfeat(dind).niter=niter;
d40dfeat(dind).lbl='Info. Max Full: Feat';

