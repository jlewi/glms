%******************************************************************
%define the files for 60d auditory gammatone filters
%*******************************************************************
dsets=[];





dsets=[];
dind=0;
bdir=fullfile('inpnonlin','071212');
bfile='audnonlin';
niter=5000;

%name of the simulation variables
simvars={'max','max','tones','unif','rand'}
%start index for the sequential indexing of the files
startind=16;


for sind=1:length(simvars);
  dind=dind+1;
dsets(dind).fname=fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,sind-1+startind));
dsets(dind).simvar=simvars{sind};
dsets(dind).niter=niter;

end
