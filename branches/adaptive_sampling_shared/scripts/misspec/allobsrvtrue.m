%11-17
%Go back into a data set and compute the gaussian approximation of the data
%on specified trials using all the observations.
%Also use the true nonlinearity not the approximate nonlinearity
clear('data');

%**************************************************************************
dind=1;
alldata{dind}.file=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_003.mat');
alldata{dind}.varsuffix={'max',};
alldata{dind}.umeth='allobsrv';         %this is the method used to generate the data from which we now 
                                                            %go back and
                                                            %resestimate
                                                            %the parameters
                                                            %under the true
                                                            %model                                                            
alldata{dind}.trials=[];             %which trials to compute posterior for or [] for all trials

dind=dind+1;
alldata{dind}.file=fullfile(RESULTSDIR,'nips','11_20','11_20_misspecified_001.mat');
alldata{dind}.varsuffix={'max','rand'};
alldata{dind}.umeth='allobsrv';         %this is the method used to generate the data from which we now 
                                                            %go back and
                                                            %resestimate
                                                            %the parameters
                                                            %under the true
                                                            %model
alldata{dind}.trials=[];                                                            
savec=0;                                            %whether or not to compute the covariance matrices

dind=2;

%clear the data structures to prevent them from interfering
data=[];

for vind=1:length(alldata{dind}.varsuffix) %index the suffix
    pname=sprintf('p%s',alldata{dind}.varsuffix{vind});
    data=load(alldata{dind}.file,pname);

    srname=sprintf('sr%s',alldata{dind}.varsuffix{vind});
    sr=load(alldata{dind}.file,srname);

    spname=sprintf('simparam%s',alldata{dind}.varsuffix{vind});
    simparam=load(alldata{dind}.file,spname);
    mparam=load(alldata{dind}.file,'mparam');
    mparam=mparam.mparam;

    umeth=alldata{dind}.umeth;
    %create the structure to store the results if it doesn't exist
    if ~isfield(data.(pname),'allobsrvtrue')
        data.(pname).allobsrvtrue.m=zeros(size(data.(pname).(umeth).m));
        data.(pname).allobsrvtrue.c=cell(size(data.(pname).(umeth).c));
    else
        warning('Field allobsrvtrue already exists possibly overwriting data');
    end
   
     %************************************************
     %generate x which is not only stimulus but spike history
    x=zeros(mparam.klength+mparam.alength,simparam.(spname).niter);
    x(1:mparam.klength,:)=sr.(srname).(umeth).y;
    obsrv=sr.(srname).(umeth).nspikes;

    if isempty(alldata{dind}.trials)
        alldata{dind}.trials=1:simparam.(spname).niter;
    end
    for tind=1:length(alldata{dind}.trials) %index the trials matrix
        trial=alldata{dind}.trials(tind);
        fprintf('Trial %d \n',trial);
       
        if (mparam.alength>0)
            shist=zeros(mparam.alength,1);
            for titer=1:alldata{dind}.trials(tind)
                nobsrv=titer-1;
                %only compute shist if we have spike history terms
                if (mparam.alength >0)
                    if isequal(simparam.(spname).(umeth).glm.fglmnc,@logisticA)
                        shist(1)=1;
                    else
                        if (mparam.alength <= (nobsrv))
                            shist(:,1)=sr.(srname).(umeth).shist(nobsrv:-1:nobsrv-mparam.alength+1);
                        else
                            
                            shist(1:nobsrv,1)=sr.(srname).(umeth).shist(nobsrv:-1:1);
                        end
                    end
                end
                x(mparam.klength+1:end,titer)=shist;
            end
        end
        %to initialize gradient ascent choose the estimate of the peak of the
        %posterior computed with our gaussian aproximation
        thetainit=data.(pname).(umeth).m(:,alldata{dind}.trials(tind)+1);

        %prior
        po.m=mparam.pinit.m(:,1);
        po.c=mparam.pinit.c;

        %change simparam so we use the truenonlinearity
        glm=simparam.(spname).glm;
        glm.fglmmu=simparam.(spname).truenonlinf;
        %compute the peak of the posterior using all observations
        [post.m]=postgradasc(thetainit,po,x(:,1:trial),obsrv(1:trial),glm);

        if (savec~=0)
        %compute the covariance matrix  using all the observations
        [dldpt dlp2dt]=d2glm(post.m,po,obsrv(1:trial),x(1:trial),glm);
        post.c=-inv(dlp2dt);
            data.(pname).allobsrvtrue.c{:,alldata{dind}.trials(tind)+1}=post.c;

        end
        data.(pname).allobsrvtrue.m(:,alldata{dind}.trials(tind)+1)=post.m;

        %

    end

%also save the prior
        data.(pname).allobsrvtrue.m(:,1)=po.m;
        data.(pname).allobsrvtrue.c{:,1}=po.c;
        
    fprintf('Save the results \n');
    %resave the results
    %copy the variables to the propernames
    eval(sprintf('%s=data.%s;',pname,pname));

    %we need to create copies of the sr results as well otherwise we will
    %have problems
     if isfield(sr.(srname),'allobsrvtrue')
        warning('Field allobsrvtrue of sr already exists possibly overwriting data');
     end
    sr.(srname).allobsrvtrue=sr.(srname).(umeth);
   eval(sprintf('%s=sr.%s;',srname,srname));

    %save the data
    saveopts.append=1;  %append results so that we don't create multiple files
    vtosave=[];
    vtosave.(pname)='';
    vtosave.(srname)='';
    
    savedata(alldata{dind}.file,vtosave,saveopts);
end