%11-25-2007
%
%Compare the results of simulations by plotting the MSE.
clear variables
setpathvars
dsets=[];
dind=0;
%
% dind=1;
% dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071125','lowrank_009.mat');
% dsets(dind).simvar='rand';
%
% dind=2;
% dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071125','lowrank_009.mat');
% dsets(dind).simvar='infomax';

dind=dind+1;
dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071206','lowrank_002.mat');
dsets(dind).simvar='rand';
dsets(dind).descr='random stimuli';

dind=dind+1;
dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071206','lowrank_002.mat');
dsets(dind).simvar='infomaxfull';
dsets(dind).descr='Select info. max. stimuli using full posterior, i.e without using assumption that theta is a low rank matrix.';


dind=dind+1;
dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071206','lowrank_002.mat');
dsets(dind).simvar='infomax';
dsets(dind).descr='Select info. max. stimuli using constraint that theta is a low rank matrix.';

for dind=1:length(dsets)
    simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

    %compute the mse
    post=getpost(simobj);
    m=getm(post);

    ktrue=gettheta(getobserver(simobj));

    data(dind).mse=m-ktrue*ones(1,size(m,2));
    data(dind).mse=(sum(data(dind).mse.^2,1)).^.5;
    if isa(post,'PostTanSpace')

        %compute mse of estimate on the subspace
        tanspace=gettanspace(post);
        tanpost=gettanpost(post);
        tsubm=getm(tanpost);
        tm=submanifold(tanspace(1),tsubm);

        data(dind).tanmse=tm-ktrue*ones(1,size(m,2));
        data(dind).tanmse=(sum(data(dind).tanmse.^2,1)).^.5;
    else
        data(dind).fullm=getm(post);

        %we need to project the full stimulus onto the submanifold
        tm=zeros(size(m));
        for j=1:size(m,2)
            tm(:,j)=submanifold(tanspace(1),proj(tanspace(1),m(:,j)));
        end

        data(dind).tanmse=tm-ktrue*ones(1,size(m,2));
        data(dind).tanmse=(sum(data(dind).tanmse.^2,1)).^.5;
    end
end


%*******************************************************************
%Plot it
%*****************************************************************

fig=FigObj('name','MSE of Full Posterior','width',4,'height',4);
fig.a=AxesObj('xlabel','Trial','Ylabel','MSE');
hold on;
for dind=1:length(dsets)
    hp=plot(0:size(data(dind).mse,2)-1,data(dind).mse);
    fig.a=addplot(fig.a,'hp',hp,'lbl',dsets(dind).simvar);


    hp=plot(0:size(data(dind).mse,2)-1,data(dind).tanmse);
    fig.a=addplot(fig.a,'hp',hp,'lbl',sprintf('%s: TanSpace',dsets(dind).simvar));
end

lblgraph(fig);

%data set descriptions
ddescr=cell(length(dsets),2);
for dind=1:length(dsets)
    ddescr{dind,1}= dsets(dind).simvar;
    ddescr{dind,2}= dsets(dind).descr;
end
odata={fig,[{'script:', 'cmptanresults.m'; 'Data File:', dsets(1).fname; 'ktrue:', ktrue}; {'datasets' ddescr}]};

onenotetable(odata,seqfname('~/svn_trunk/notes/cmptanresults.xml'))