%01-28-2008
%
%Try to get some sense of the tangent space by taking random combinations
%of the basis vectors of the tangent space

co08datagammatones30d;

simobj=SimulationBase('fname',dsets(1).fname,'simvar',dsets(1).simvar);

%%
%magnitude for randomcombinations of basis vectors
mag=1;
nvecs=1000;

%trial=getniter(simobj);
trial=20;
tanspace=gettanspace(getpost(simobj,trial));

basis=getbasis(tanspace);
bdim=size(basis,2);

bproj=normrnd(zeros(bdim,nvecs),ones(bdim,nvecs));


%normalize each column
bproj=normmag(bproj);

bdirs=basis*bproj;


ftan=FigObj('width',4,'height',4,'name','tanspace');
ftan.a=AxesObj('xlabel','vector','ylabel','index');

%clim=[-.05,.05];
imagesc(bdirs',clim);
colorbar;
lblgraph(ftan);