%test the tan space for a gabor function
%
%Try constructing the tanspace for a theta which is really a gabor.
%Make sure we can recover its parameters;

%gtrue=GaborTanSpace('center',0,'A',.1,'sigmasq',2,'width',10,'omega',.2);
A=2;
sigmasq=2;
center=0;

dimtheta=20;
omega=3*pi/dimtheta;

gp= [A; sigmasq; center; omega];


gtrue=GaborTanSpace('sparam',gp,'dimtheta',dimtheta);
tn=gettheta(gtrue)+0*randn(getdimtheta(gtrue),1);
gfit=GaborTanSpace('theta',tn);

figure;
t=[1:getdimtheta(gtrue)]';
hp=plot(t,gettheta(gtrue),'r',t,tn,'b',t,gettheta(gfit),'g');
legend(hp,{'true','noisy','fitted'});
