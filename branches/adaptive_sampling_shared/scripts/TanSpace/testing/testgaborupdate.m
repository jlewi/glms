%11-28-2007

%load the data from a random simulation
fname=fullfile(RESULTSDIR,'tanspace','071128','tanspace_001.mat');

simrand=SimulationBase('fname',fname,'simvar','rand');

%get the stimuli



%*********************************************************************
%create the prior for the tangent space updater
prior=getpost(simrand,0);
tparam.dimsparam=4;
%mparam.pinit=PostTanSpace('fullpost',prior,'tanspacetype','GaborTanSpace','tanparam',tparam);
mparam.pinit=PostTanSpace('fullpost',prior,'tanpost',[],'tanspace',[]);

uobj=TanSpaceUpdater();

postlast=mparam.pinit;
mobj=getmobj(simrand);
for ind=1:getniter(simrand)
    if (mod(ind,10)==0)
        fprintf('Trial %d \n',ind);
    end
    stim=getinput(getsr(simrand));
    obsrv=getobsrv(getsr(simrand));
    post(ind)=update(uobj,postlast,stim,mobj,obsrv); 
    postlast=post(ind);
end