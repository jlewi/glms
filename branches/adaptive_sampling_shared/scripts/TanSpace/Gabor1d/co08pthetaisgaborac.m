% For a range of A=amplitude of Gabor and C=center of Gabor
%We compute theta corresponding to this (A,C). We then compute the probability of this Theta under our posterior.
% We plot this posterior probability as a function of A,C
%
%
%what trials to do computation on
%
%2-09-2008
%   This script never really worked except for the infomax results 
%   However I didn't like the way the results looked so I decided to pretty
%   much abandon it.
xscript('/home/jlewi/svn_trunk/allresults/tanspace/080218/co08gabors1dAC_080218_001.m');

trials=[30 40 60];


%range of amplitude and center to consider for the gabors
arange=[0:.5:6];
crange=[-20:.5:20];

%Results is a 1x#dsets structure array with fields storing the results
%pgabor - is the probability of gabor with the row corresponding to the
%value of the amplitude, the column corresponding to the center, and the
%final value entry corresponding to the trial.
%
%pfullgabor - probability of the gabor under the full posterior
%ptangabor - probability under the posterior projected into the tangent
%space
results=struct('pfullgabor',zeros(length(arange),length(crange),length(trials)),'ptangabor',zeros(length(arange),length(crange),length(trials)));

results=repmat(results,[1 length(dsets)]);

for dind=1
    simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

    glm=getglm(getmobj(simobj));
    extra=getextra(simobj);
    if ~isempty(extra)
        mparam=extra.mparam;
        gptrue=mparam.gp;
        omega=mparam.omega;
        sigmasq=mparam.sigmasq;
    else
        post=getpost(simobj,0);
        tspace=gettanspace(post);
        param=getparam(tspace);
        sigmasq=param.sigmasq;
        omega=param.omega;

    end
    %we need a tan space object to handle computing theta
    tspace=gettanspace(mparam.pinit);
    dimtheta=length(mparam.ktrue);




    tup=TanSpaceUpdater();
    for trialind=1:length(trials)

        trial=trials(trialind);
        %form a posterior on the full theta space
        %and the tangent space
        post=getpost(simobj,trial);
        fpost=getfullpost(post);
        params.gpstart=mparam.gp;
        params.omega=mparam.omega;
        params.sigmasq=mparam.sigmasq;
        [tanspace,tanpost]= comptanpost(tup,post,'GaborTanSpaceAC',params);

        for aind=1:length(arange);
            for cind=1:length(crange);

                %compute the gabor corresponding to this amplitude and
                theta=submanifold(tspace,[arange(aind); crange(cind)]);


                %compute the probability under the full posterior
                results(dind).pfullgabor(aind,cind,trialind)=logpdf(fpost,theta);

                %compute the probability under the tangent space
                results(dind).ptangabor(aind,cind,trialind)=logpdf(tanpost,theta);


            end
        end
    end
end

%
%%
%plot the probability
for dind=1:length(results)
    fpost=FigObj('name',sprintf('Comparing Posteriors: %s',dsets(dind).simvar),'width',6,'height',8);
    fpost.a=AxesObj('nrows',length(trials),'ncols',2);

    ncountours=30;
    clim=[0 10^-3];

    %limits of colorbar are set to account for fraction of cfrac of the data;
    cfrac=.9;
    for trialind=1:length(trials)
        trial=trials(trialind);
        %plot the true posterior
        fpost.a=setfocus(fpost.a,trialind, 1);


        %plot it on log scale
        cind=1;

        for cind=1:2
            %select the appropriate data for the column
            if(cind==1)
                data=results(dind).pfullgabor(:,:,trialind);
                ttl=sprintf('p(\\theta_i|\\mu_t,C_t) for t=%d',trial);
            else
                data=results(dind).ptangabor(:,:,trialind);
                ttl=sprintf('p(\\theta_i|T(\\mu_t,C_t)) for t=%d',trial);

            end

        

            fpost.a=setfocus(fpost.a,trialind, cind);
            imagesc(crange,arange,data);
            %        [c hcon]=contourf(x,thetavals,ldata,ncountours);
            %       set(hcon,'Edgecolor','none');
            set(gca,'ydir','normal');

            fpost.a(trialind,cind)=title(fpost.a(trialind,cind),ttl);
           % cl=detclim(data,cfrac);
            %if ((cl(2)-cl(1))==0)
            %    cl=clim;
            %end
            %set(gca,'clim',cl);
            %set(gca,'clim',clim);

            axis tight;

            hc= colorbar;
            fpost.a(trialind,cind)=sethc(fpost.a(trialind,cind),hc);


            set(gca,'ytick',[]);
            set(gca,'xtick',[]);

            %add titles

            fpost.a(trialind,cind)=title(fpost.a(trialind,cind),ttl);

        end

    end

    %add xlabels
    for cind=1:2
        rind=length(trials);
        fpost.a(rind,cind)=xlabel(fpost.a(rind,cind),'center');
        setfocus(fpost.a,rind,cind);
        set(gca,'xtickmode','auto');
    end

    %add ylabels
    for rind=1:3
        cind=1;
        fpost.a(rind,cind)=ylabel(fpost.a(rind,cind),'amplitude');
        setfocus(fpost.a,rind,cind);
        set(gca,'ytickmode','auto');
    end
    %adjust the color limits
    % for rind=1:length(trials)
    %     for cind=1:3
    %         fpost.a=setfocus(fpost.a,rind, cind);
    %         set(gca,'clim',clim);
    %     end
    % end

    fpost=lblgraph(fpost);
    fpost=sizesubplots(fpost);

    fname=fullfile('~/svn_trunk/adaptive_sampling/writeup/nips08',sprintf('pgaborac_%s.png',dsets(dind).simvar));
    %saveas(gethf(fpost),fname,'png');
    explain='True theta is gabor with unknown amplitude and center. The left plot shows the probability under the full posterior of a gabor with that value for the amplitude and center. Right plot shows probability of the gabor under Gaussian posterior on tangent space.';
    onenotetable({fpost;{'script','pthetaisgaborac'; 'data file',getpath(dsets(dind).fname);'simvar',dsets(dind).simvar;'explanation',explain}},seqfname('~/svn_trunk/notes/truegaborpost.xml'));
end