%Illustration for how we compute p(theta_i=\omega)
%
%Basically we plot the log likelihood as a function of A,C and show that
%p(theta_i=\omega) is the value of this line integral.
%
%decrease the number of theta vals to make it run faster decrease the
%number of theta vals
%simulation to process
xscript('/home/jlewi/svn_trunk/allresults/tanspace/080218/co08gabors1dAC_080218_001.m');
%co08gabors1dAC;

%what trials to do computation on
trials=[30 40 60];
%results=[];

%pick the infomax data set
dind=1;
simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

glm=getglm(getmobj(simobj));
extra=getextra(simobj);
if ~isempty(extra)
    mparam=extra.mparam;
    gptrue=mparam.gp;
    omega=mparam.omega;
    sigmasq=mparam.sigmasq;
else
    post=getpost(simobj,0);
    tspace=gettanspace(post);
    param=getparam(tspace);
    sigmasq=param.sigmasq;
    omega=param.omega;

end
dimtheta=length(mparam.ktrue);



obsrv=getobsrv(simobj,1:getniter(simobj));
stim=getData(getstim(simobj,1:getniter(simobj)));

%compute the constants for the log likelihood i.e log h(r)
%because otherwise log-likelihoods become very sharply peaked which
%creates problems when we numerically integrate
%for poisson distribution
%h(r)=1/r!
r=log([1:max(obsrv)]);
r=cumsum(r);
lognc=sum(-r(obsrv(find(obsrv>0))));


%*******************************************************
%pick a value of theta to compute probability for
x=[1:dimtheta]-(1+dimtheta)/2;

%Compute p(theta_thetaind=thetaval);
thetaval=3;
thetaind=0;

%c is the values of the center for which we compute the probability
%c,A are the values of the parameters for which we compute the
%log-likelihood
c=[-3:.05:3];
a=zeros(1,length(c));
ll=zeros(1,length(c));


dim=size(stim,1);
ind=([1:dim]-(1+dim)/2)';

theta=zeros(dim,length(c));
for cind=1:length(c)

    %for this value of c we need to compute a
    a(cind)=thetaval./(cos((thetaind-c(cind))*omega).*exp(-.5*1/sigmasq*(thetaind-c(cind)).^2));

    %now compute the probability
    %compute theta
    theta(:,cind)=a(cind)*(cos((ind-c(cind))*omega).*exp(-.5*1/sigmasq*(ind-c(cind)).^2));

    ll(cind)=sum(loglike(glm,obsrv,theta(:,cind)'*stim))+lognc;


end



%
%%
%plot the probability
fig=FigObj('width',1.75,'height',1.75,'name','schematic for computing the true log likelihood');
fig.a=AxesObj('nrows',1,'ncols',1);
hold on;

%make a line plot of the a,c corresponding to this thetaval
hinv=plot3(a,c,ones(1,size(c,2))*min(ll(:)),'Linewidth',4,'Color','k','Marker','none');

%plot the log likelihood
hll=plot3(a,c,ll,'Linewidth',4,'Color','b','Marker','O','Marker','none');
%hx=xlabel('A');
%hy=ylabel('c');

%zlabel is added in impress because matlab screws up the fonts when we save
%to png and we can't insert eps images in open office
%hz=zlabel('$\log p(x_{1:t},r_{1:t}|\theta_i)$');
%set(hz,'interpreter','latex')

set(gca,'CameraPosition',[10.6705 -71.0438 182080]);
set(gca,'CameraTarget',[3.75 0 -70000]);
xlim([min(a(:)),max(a(:))]);
ylim([c(1),c(end)]);
zlim([min(ll(:)) max(ll(:))]);

%shrink the Axes to create room for the labels
ha=getha(fig.a);
%position=get(ha,'position');
%position=[.23 .25 .65 .55];
%set(ha,'position',position);

xposition=get(hx,'position');

set(gca,'ygrid','on')
set(gca,'xgrid','on')
set(gca,'zgrid','on')

%get rid of the tick labels
set(gca,'xticklabel',[]);
set(gca,'yticklabel',[]);
set(gca,'zticklabel',[]);
%add text labeling the inverse of \theta_i=gamma
%[amin, aind]=min(a);
%tinvpos=[amin c(aind) min(ll(:))+10^4];

%htinv=text(tinvpos(1),tinvpos(2),tinvpos(3),'$\Psi^{-1}(\theta_i=\gamma)$');
%set(htinv,'interpreter','latex');

    
%lblgraph(fig)
fname=fullfile('~/svn_trunk/adaptive_sampling/writeup/cosyne08',sprintf('gaborac_loglike_schematic%s.png',dsets(dind).simvar));
%saveas(gethf(fig),fname,'png');
%fname=fullfile('~/svn_trunk/adaptive_sampling/writeup/cosyne08',sprintf('gaborac_loglike_schematic%s.eps',dsets(dind).simvar));
%saveas(gethf(fig),fname,'epsc');
%explain='True theta is gabor with unknown amplitude and center. The left plot shows the true log-likleihood. Middle plot shows Gaussian posterior on full theta space. Last column shows posterior projected onto tangent space.';
%onenotetable({fpost;{'script','truegaborpost'; 'data file',getpath(dsets(dind).fname);'simvar',dsets(dind).simvar;'explanation',explain}},seqfname('~/svn_trunk/notes/truegaborpost.xml'));
