%Illustration for how we compute p(theta_i=\omega |\pmap[t],\covar[t])
%
%
%We plot a 2-d Gaussian. We then take a slice and integrate over this
%slice.

fpost=FigObj('width',1.75,'height',1.75);

%*******************************************************************
%plot a gaussian
%****************************************************************
%options

x=[-4:.05:4];
y=[-4:.05:4];

count=1;
sigma=[1 0;0 1];
mu=[0;0];
nclines=20;  %number of contour lines
zval=zeros(length(x),length(y));
for xind=1:length(x)
    for yind=1:length(y)
        %xval(count)=x(xind);
       %yval(count)=y(yind);
        zval(xind,yind)=mvgauss([x(xind);y(yind);],mu,sigma);
        
        count=count+1;
    end
end

%draw a surface plot
hsurf=surf(x,y,zval)

%get rid of the edge colors
set(hsurf,'EdgeColor','none');


lzval=log(zval);
%ind=find(lzval<mcont);
%lzval(ind)=mcont;
%colormap hsv
%colorbar

%set(gca,'FontSize',fg.axisfontsize);
opts.labels=0;
if (opts.labels~=0)
xlabel('\theta_1');
ylabel('\theta_2');
hz=zlabel('$P(\vec{\theta{}}\,\,|\vec{x}_{t-1},r_{t-1}$)');
set(hz,'Rotation',0);
set(hz,'interpreter','latex');

end
set(gca,'xticklabel',[]);
set(gca,'yticklabel',[]);
set(gca,'zticklabel',[]);
%set(gca,'Visible','off');


%plot a line in the plane to indicate the line integral over which we
%integrate
%yval=-1*ones(1,length(y));
%ztrough=zeros(1,length(y));
%hold on;
%plot3(x,yval,ztrough,'Linewidth',2,'Color','k','Marker','none');

%create a patch
ypts=[-1 -1 -1 -1];
xpts=[x(1) x(end) x(end) x(1)];
zpts=[0 0 max(zval(:)) max(zval(:))];

hp=patch(xpts',ypts',zpts','k');
set(hp,'FaceAlpha',.35)

%this prevents any border around the figure
%it might prevent the axes labels from showing up if there are any
set(gca,'Position',[0 0 1 1]);
%set(gca,'CameraPosition',[0 5 .5]);
%set(gca,'CamerPosition',[0 21,.22]);
%set(gca,'CameraViewAngle',26);

fname=fullfile('~/svn_trunk/adaptive_sampling/writeup/cosyne08',sprintf('gaborac_fullpost_schematic%s.png',dsets(dind).simvar));
saveas(gethf(fpost),fname);