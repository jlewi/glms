%1-22-2008
%
%To analyze the tangent space compute the difference in entropy of the
%posterior on the full posterior and the posterior on the tangent space.
%
%The posterior on the tangent space is computed in the lower dimensional
%space of the tangent space. B\c if we computed it in the full space it
%would be undefined because the variance in some directions is zero.

dsets=[];
dind=1;
%
%info. max. on 40 trials at start 
dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','080120','audnonline_003.mat');
simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

%get the entropy of the fullposterior
data(dind).fentropy=getentropy(getfullpost(getpost(simobj)));


%get the entropy of the tangent space