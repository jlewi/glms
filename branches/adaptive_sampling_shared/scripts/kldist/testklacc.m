%10_27
%Point of this script is to estimate how well the accuracy of a Gaussian
%can be approximated using monte carlo methods
%The point is to see whether the bias I'm seeing in my code is due to a
%constant
d=2;            %number of dimensions for our gaussian
nsamps=10^5;
xvar=10^-10; %variance

x=randn(d,nsamps)*xvar^.5;

%approximate the entropy
lpx=sum(log(normpdf(x,0,xvar^.5)),1);
happrox=1/nsamps*sum(lpx);


htrue=  1/2*log((2*pi*exp(1))^d*det(xvar*eye(d)));

%try using my code
lpmine=logmvgauss(x,zeros(d,1),xvar*eye(d));