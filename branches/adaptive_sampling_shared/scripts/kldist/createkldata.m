%10-27
%script to generate the data needed to run kl simulations
%outfile=fullfile(RESULTSDIR,'aistat','10_15','10_15_kldata_013.mat');
%simfile='/home/jlewi/cvs_ece/allresults/aistat/10_15/10_15_maxtrack_013.mat';

%outfile=fullfile(RESULTSDIR,'nips','11_15','11_15_kldata_001.mat');
%simfile=fullfile(RESULTSDIR,'nips','11_15','11_15_lowdcos_001.mat');
%outfile=fullfile(RESULTSDIR,'poissexp','03_13','03_13_kldata_002.mat');
%simfile=fullfile(RESULTSDIR,'poissexp','03_13','03_13_poissexp_002.mat');

%outfile=fullfile(RESULTSDIR,'poissexp','04_19','kldata_04_19_001.mat');
%simfile=fullfile(RESULTSDIR,'poissexp','04_19','poissexp_04_19_001.mat');

%outfile=fullfile(RESULTSDIR,'poissexp','04_19','kldata_04_19_001.mat');
%simfile=fullfile(RESULTSDIR,'poissexp','04_19','poissexp_04_19_001_batch.mat');

%outfile=fullfile(RESULTSDIR,'poissexp','04_19','kldata_04_19_002.mat');
%simfile=fullfile(RESULTSDIR,'poissexp','04_19','poissexp_04_19_002.mat');


%klfile=fullfile(RESULTSDIR,'poissexp','04_23','kldata_04_23_001.mat');
%simfile=fullfile(RESULTSDIR,'poissexp','04_23','poissexp_04_23_001.mat');

%file which has just the trials for datasimbatch1000
klfile=fullfile(RESULTSDIR,'poissexp','04_23','kldata_04_23_001_simbatch1000.mat');
simfile=fullfile(RESULTSDIR,'poissexp','04_23','poissexp_04_23_001.mat');

%file which has just the trials for datasimbatch100000
klfile=fullfile(RESULTSDIR,'poissexp','04_23','kldata_04_23_001_simbatch100000.mat');
simfile=fullfile(RESULTSDIR,'poissexp','04_23','poissexp_04_23_001.mat');


%msamps=10^5*ones(1,9);            %number of monte carlo samples to generate
msamps=[5*10^5*ones(1,25)];            %number of monte carlo samples to generate
%simvar={'s};
simvar={'simbatch'};
%umeth={'newton1d','allobsrv'}

%trials=[100 500 1000 1500 2000];                      %what trial to evaluate the kl distance on;
%trials=[200:100:400];                      %what trial to evaluate the kl distance on;
%trials=[100 500 1000];
%trials=[100 500 1000];
%trials=[3*10^4:10^4:10^5];
%trials=[10^4:10^4:10^5];
%trials=[1*10^3 10^4:10^4:10^5];
%trials=[30000 100000]
trials=[100 1000 10000 50000 100000];
trials=[];
trials=[10 50 100 500 1000 10000 100000];
trials=[100000];
      for k=1:length(msamps)
for sind=1:length(simvar)    
        for tind=1:length(trials)
            trial=trials(tind);
            try
                %outfile,simfile need to be absolute paths;
                fprintf('Generating data for simvar: %s \t  trial: %d \t msamps %d \n',simvar{sind},trials(tind),msamps(k));
                genkldata(klfile,msamps(k),trials(tind),simvar{sind},simfile);
            catch
                fprintf('error occured for triial %d \n', trial);
            end
            end
        end
end