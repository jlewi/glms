%make plots of the true filter components for the auditory example.


%new data set
dsets=[];
co08separable;




[simobj]=SimulationBase('fname',getpath(dsets(dind).fname),'simvar',dsets(dind).simvar);
extra=getextra(simobj);
%%
gparam.height=2;
gparam.width=2;

%iwidth - what % of the width is for the image of the combined filter
%fwidth - what % of the width is for the plot of filter component
gparam.iwidth=.6;
gparam.fwidth=.2;

%iheight - what % of the height is for the image of the combined filter
%fheight - what % of the height is for the plot of time component
gparam.iheight=.6;
gparam.theight=.2;

%left position of main image
gparam.ileft=.27;
gparam.ibottom=.1;

%spacing between filters
gparam.stofilts=.05;

gparam.clim=[-1 1];
gparam.linewidth=4;

hf=[];
for fi=1:2
    if (fi==1)
        filts.f=extra.f1;
        filts.t=extra.t1;
    else
        filts.f=extra.f2;
        filts.t=extra.t2;
    end
    filts.theta=filts.f*filts.t';

    hf(fi)=figure;
    setfsize(hf(fi),gparam);

    %plot the main filter
    ha(fi).main=axes;
    imagesc(filts.theta,gparam.clim);
    set(gca,'ydir','reverse');
    set(gca,'yaxislocation','right');
    mpos=get(ha(fi).main,'position');
    mpos=[gparam.ileft gparam.ibottom gparam.iwidth gparam.iheight];
    set(gca,'position',mpos);
    
    %reverse the tick marks 
%     tylbls=get(gca,'yticklabel');
%     set(gca,'yticklabel',tylbls(end:-1:1,:));
    
    %plot the temporal filter
    ha(fi).tfilt=axes;
    hp=plot(filts.t);
    set(hp,'linewidth',gparam.linewidth);

    set(gca,'xtick',[]);
    tpos=get(gca,'position');
    tpos=[mpos(1) mpos(2)+gparam.iheight+gparam.stofilts gparam.iwidth gparam.theight];
    set(gca,'position',tpos);
    xlim([1 ,length(filts.t)]);

    %plot the freq filter
    ha(fi).ffilt=axes;
   

    %we want this axes to be oriented vertically
    %so we need to effectively switch the x and y axes
    hp=plot(filts.f,1:length(filts.f));
    set(hp,'linewidth',gparam.linewidth);

    set(gca,'ytick',[]);
    %put the y-axis on the right side
    set(gca,'YAxisLocation','right');
    set(gca,'xdir','reverse');
    set(gca,'ydir','reverse');

    fpos=get(gca,'position');
    fpos=[mpos(1)-gparam.fwidth-gparam.stofilts mpos(2) gparam.fwidth gparam.iheight];
    set(gca,'position',fpos);
    ylim([1 ,length(filts.f)]);

    foutfile=sprintf('~/svn_trunk/adaptive_sampling/writeup/cosyne08/lowrankfilt%d.png',fi);
%    saveas(hf(fi),foutfile);
end

%%
%plot the true filter
fi=fi+1;
hf(fi)=figure;
setfsize(hf(fi),gparam);
mdim=length(filts.t);
imagesc(reshape(extra.ktrue,[mdim,mdim]),gparam.clim);
set(gca,'ydir','reverse');
ktpos=[gparam.ileft gparam.ibottom gparam.iwidth gparam.iheight];
set(gca,'position',ktpos);
foutfile=sprintf('~/svn_trunk/adaptive_sampling/writeup/cosyne08/lowrankfilt_true.png');
%saveas(hf(fi),foutfile);