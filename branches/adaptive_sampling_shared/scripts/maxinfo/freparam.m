%12-02
%script to make plots of F(lambda)- which is quantity we want to maximize
%we reparamterize with respect to lambda to try to deal efficently with the
%long tails
%as we rotate the mean 
%
%This can plot it for multible values of the mean of the posterior
%
%Since we reparameterice with lambda=exp(alpha)
%we need to make two graphs
%   lambda=exp(alpha)
%   lambda=-exp(alpha)

close all;
clear all;
setpaths;
m=[1;0];
pg.c=[.9 .3;.3 .1];

%alpha
alpha=[-2:.05:2];



%angles for mean
dangle=90/5*pi/180;
%avoid an angle of 0 and pi/2 so that projection 
%of mean onto eigen vectors is never 0.
%mangle=[dangle:dangle:pi/2-dangle];
mangle=[pi/4];

for tindex=1:length(mangle);
 pg.m=[cos(mangle(tindex));sin(mangle(tindex))]*(m'*m)^.5;


%********************************************************
%make a plot of fisher information as function of lambda
%**********************************************************
%lambda=exp(alpha)
lambda=exp(alpha);
[finfo,lambda,xpts]=fisherlambda(pg,lambda);
flambdap.finfo{tindex}=finfo;
flambdap.xpts{tindex}=xpts;
flambdap.lambda{tindex}=lambda;
flambdap.lbl{tindex}=sprintf('<\\mu=%0.2g',mangle(tindex)*180/pi);
flambdap.alpha{tindex}=alpha;

%lambda=exp(alpha)
%reverse the order
%so that finfo will correspond to going from -infty to 0
alpha=alpha(end:-1:1);
lambda=-exp(alpha);
[finfop,lambda,xpts]=fisherlambda(pg,lambda);
flambdan.finfo{tindex}=finfo;
flambdan.xptsp{tindex}=xpts;
flambdan.lambda{tindex}=lambda;
flambdan.alpha{tindex}=alpha;

end

%*******************************************************************
%plot all curves of lambda 
%*******************************************************************
flambda.hf=figure;
flambda.hp=[];

for tindex=1:length(mangle)
    
     subplot(2,2,1);
    
    %hold on
    %when we plot concatenate the positive and negative together
    plot(flambdan.alpha{tindex}, flambdan.finfo{tindex},getptype(tindex,1));   
    title('\lambda=-exp(\alpha)');
    xlabel('\alpha');
    set(gca,'xdir','reverse');
    
    subplot(2,2,2);
    
    %hold on
    %when we plot concatenate the positive and negative together
    plot(flambdap.alpha{tindex}, flambdap.finfo{tindex},getptype(tindex,1));   
    title('\lambda=exp(\alpha)');
    xlabel('\alpha');
    
   
    
    %reverse the direction of the x-axis.
    %subplot(3,1,1);
    %hold on
    %when we plot concatenate the positive and negative together
    %plot([flambdan.alpha{tindex} flambdap.alpha{tindex}], [flambdan.finfo{tindex} flambdap.finfo{tindex}],getptype(tindex,1));   
    %plot([flambdan.alpha{tindex}-flambdan.alpha{tindex}(1) flambdap.alpha{tindex}-flambdap.alpha{tindex}(1)], [flambdan.finfo{tindex} flambdap.finfo{tindex}],getptype(tindex,1));   
    %alpha=[flambdan.alpha{tindex} flambdap.alpha{tindex}];
    %xtick=get(gca,'xtick');
    %set(gca,'xticklabel',alpha(xtick));
    %plot(flambda.lambda{tindex},flambda.finfo{tindex},getptype(tindex,2)); 
    %xlabel('Alpha (Positive and Negative)');
    %ylabel('Fisher Info');
    
    %plot w.r.t lambda
    subplot(2,2,3);
    plot([flambdan.lambda{tindex} flambdap.lambda{tindex}], [flambdan.finfo{tindex} flambdap.finfo{tindex}],getptype(tindex,2)); 
    xlabel('\lambda');
    ylabel('Fisher Info');
    
    %hold on;
    %plot x and y
    %subplot(3,1,2);
    
    %hold on;
    %plot(flambda.lambda{tindex},flambda.xpts{tindex}(1,:),getptype(tindex,1));
    %plot the location of the maximum
    %[val,ind]=max(flambda.finfo{tindex});
    %plot(flambda.lambda{tindex}(ind), flambda.xpts{tindex}(1,ind),getptype(tindex,1));
    %ylabel('x');
    %ylim([-1.25 1.25]);

    %subplot(3,1,3);
    %hold on;
    %plot(flambda.lambda{tindex},flambda.xpts{tindex}(2,:),getptype(tindex,1));
    %plot(flambda.lambda{tindex}(ind), flambda.xpts{tindex}(2,ind),getptype(tindex,1));
    %ylabel('y');
end

%legend(flambda.hp,flambda.lbl);
