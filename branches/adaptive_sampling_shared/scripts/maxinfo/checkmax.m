%To check the maximization
%After running max2d. This function computes the fisher info for each trial
%at the location of the optimized stimulus as well as at the mean
%at the location of the max eigenvector.
%the point is to verify that the optimial stimulus really maximizes the
%fisher info

m=pmax.newton.m;
c=pmax.newton.c;
xopt=srmax.newton.y;
niter=simparam.niter;

for index=1:niter;
    fopt(1,index)=exp(xopt(:,index)'*m(:,index)+.5*xopt(:,index)'*c{index}*xopt(:,index))*xopt(:,index)'*c{index}*xopt(:,index);
    
    %normalize the mean to be consistent
    mnorm=m(:,index)/((m(:,index)'*m(:,index))^.5);
    fmean(1,index)=exp(mnorm'*m(:,index)+.5*mnorm'*c{index}*mnorm)*mnorm'*c{index}*mnorm;
    [uvec,s,v]=svd(c{index});
    %
    u=uvec(:,1);
    feigmax(1,index)=exp(u'*m(:,index)+.5*u'*c{index}*u)*u'*c{index}*u;
    u=uvec(:,2);
    feigmax(2,index)=exp(u'*m(:,index)+.5*u'*c{index}*u)*u'*c{index}*u;
    
end

figure;
hold on;
h1=plot(1:niter,fopt,getptype(1,1));
plot(1:niter,fopt,getptype(1,2));
h2=plot(1:niter,fmean,getptype(2,1));
plot(1:niter,fmean,getptype(2,2));
h3=plot(1:niter,feigmax(1,:),getptype(3,1));
plot(1:niter,feigmax(1,:),getptype(3,2));

h4=plot(1:niter,feigmax(2,:),getptype(4,1));
plot(1:niter,feigmax(2,:),getptype(4,2));
legend([h1 h2 h3 h4],{'OPtimal','Mean', 'Max Eigen','2nd Largest Eigen'});
xlabel('Iteration');
ylabel('Fisher Info');
set(gca,'YScale','Log')

%************************************************************************