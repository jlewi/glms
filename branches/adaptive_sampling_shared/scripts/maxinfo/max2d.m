%11-18
%
%maximize the information in 2-d
%clear all
%close all;

setpaths;

%which methods to turn on
mon.g=0;
mon.ekf=0;
mon.batchg=0;
mon.ekf1step.on=0;
mon.ekf1step.ekffunc=@ekfposteriormax;
mon.ekf1step.onestep=1;                 %how many steps between 1 step corrections.
mon.ekfmax=1;
mon.newton.on=0;
mon.newton.nsteps=10;
%**************************************************************************
%simulation parameters
%*************************************************************************
%reseed the random number generator
simparam.rstate=25;
randn('state',simparam.rstate);
rand('state',simparam.rstate);


simparam.niter=1000;

%************************************************************************
%analysis what results/analysis to collect. 
%see analyzeposteriors.m
%***********************************************************************
aopts.getstats=1;
aopts.mckl=0;       %compute kl divergence using monte carlo

%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;
RDIR=fullfile(RESULTSDIR,'maxinfo', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));

%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
ropts.fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_max2d.mat',datetime(2),datetime(3)));
[ropts.fname,trialindex]=seqfname(ropts.fname);
ropts.savedata=1;   %nonzero save data


%*************************************************************************
%save
%************************************************************************
%specify which variables get saved
%vtosave
%name of variables to save
%Save everything we need to potentially restart the sim
vtosave.pmax='';
vtosave.srmax='';
vtosave.srrand='';
vtosave.prand='';
vtosave.kldist='';
vtosave.simparam='';
vtosave.ropts='';
vtosave.mparam='';
vtosave.popts='';
vtosave.vtosave='';
vtosave.saveopts='';
%options whether to compute kl divergence by brute force
popts.klbrute=0;


%options for plotting functions
if (ropts.savedata~=0)
    popts.sgraphs=1;    %save graphs
else
    popts.sgraphs=0;
end

popts.sdir=RDIR;    %directory where to save results



%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window

%krange specifies the minimum and maximum value for the coefficents
%of k in the glm. 
%dk says what stepsize to use for storing k in the posterior
%krange is used to plot the results of the gaussians
mparam.tresponse=1;
mparam.krange=[-3 3];
mparam.dk=.0005;
mparam.tlength=500;
%mparam.ktrue=[1;1.1;.5;1;.9;.2;.2;.4;.7;.8];
mparam.ktrue=(round(rand(mparam.tlength,1)*15)+1)/10;
mparam.dt =.005;            %used for sampling the spike trains
                            %I should switch to homogenous sampling so I
                            %don't need dt.
%make the initial mean slightly larger than zero
mparam.pinit.m=(round(rand(mparam.tlength,1)*10)+1)/1000;;
%mparam.pinit.m=[1;0];

mparam.pinit.c=3*eye(mparam.tlength);
%we set the magnitude constraint so that the maximum firing rate is never
%larger than a 1000 this is because the bottleneck is sampling the poison
%process so we want to limit how many spikes we have to generate
mparam.maxrate=1000;
mparam.mmag= (log(mparam.maxrate/mparam.tresponse)/(mparam.ktrue'*mparam.ktrue)^.5)^2;
simparam.normalize=mparam.mmag; %we will normalize the stimuli to be same as magnitude
                                                            %constraint
                                                            %imposed on the
                                                            %stimuli  when
                                                            %optimizing the
                                                            %stimulus
                                                           
                                                            
%**************************************************************************
%Update the posteriors
%we do this twice once using maxupdate and 1 using regular update so we can
%compare
%**************************************************************************
%set ropts.fname='' so we never call save from maxupdate or
%updateposteriors
%we will call save from this script
fropts=ropts;
fropts.fname='';
tstart=clock;
[pmax,srmax,simparammax]=maxupdate(mparam,simparam,mon,fropts);
tstop=clock
pmax.newton.timing.totaltime= etime(tstop,tstart); 

%get the current state of the random number generator
%make this cell arrays in case we start and stop many times
simparammax.rstart{1}=simparammax.rstate;
simparammax.rfinish{1}=randn('state');
simparammax=rmfield(simparammax,'rstate');

%save data before performing random sampling
if (ropts.savedata~=0)
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end

[prand,srrand,simparamrand]=updateposteriors(mparam,simparam,mon,fropts);
%save the state of the random number generator
simparamrand.rstart{1}=simparamrand.rstate;
simparamrand.rfinish{1}=randn('state');
simparamrand=rmfield(simparamrand,'rstate');

%save data
if (ropts.savedata~=0)
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end

%*************************************************************************
%analysis
%**************************************************************************
%if we intend to make a movie we need discrete representations
%if ~isempty(popts.movie.mname)
%    aopts.getsamps=1;
%end
%save the analysis to whatever we save results to 
aopts.fname=ropts.fname;
%[pmethods]=analyzeposteriors(pmax,sr,mparam,aopts,simparam);

%**************************************************************************
%Plotting
%*************************************************************************
%Call the script which makes the plots
%script called will depend on the dimensionality
maxplots;
    

