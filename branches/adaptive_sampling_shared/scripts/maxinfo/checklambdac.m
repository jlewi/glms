%check lambda constraint
%right now this is for 2dimensions
figure;
hold on;
meths={pmax.newton}
for mindex=1:length(meths)
    c=meths{mindex}.c;
    m=meths{mindex}.m;
    eigvalues=zeros(mparam.tlength,simparam.niter+1);
    %this stores the index of the eigenvector most correlated with the
    %mean;
    emind=zeros(1,simparam.niter);
    for index=1:size(srmax.newton.y,2)
        [curru s]=svd(c{index});
        [val,emind(index)]=max(curru'*m(:,1));
        eigvalues(:,index)=diag(s);
    end    



    %(mparam.tlength,1,1);
    for index=1:mparam.tlength
        pind=(mindex-1)*mparam.tlength+index;
        h(pind)=plot(1:simparam.niter+1,eigvalues(index,:),getptype(pind,1));
        plot(1:simparam.niter+1,eigvalues(index,:),getptype(pind,2));
        lgnd{pind}=(sprintf('%0.2g Eigenvalue (%s)',index,lbls{mindex}));    
    end
end
pind=length(h)+1;
hold on
%plot the boundary
c=mparam.ktrue'*mparam.ktrue/(mparam.ktrue'*mparam.ktrue)^.5;
%h(pind)=plot(1:simparam.niter+1,eigvalues(2,:)+2*c,getptype(pind,1));
%plot(1:simparam.niter+1,eigvalues(2,:)+2*c,getptype(pind,2));
%lgnd{pind}='Limit';

set(gca,'YScale','Log');
xlabel('Iteration');
ylabel('Eigenvalue');
legend(h,lgnd);

%plot ratio of eigenvalues
figure;
plot(0:simparam.niter,eigvalues(1,:)./eigvalues(2,:))