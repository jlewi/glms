%1-24-08
%
%Test how much faster using eigs to get the maximal eigenvector is
%than using eig

dim=40^2;

mat=normrnd(zeros(dim,dim),ones(dim,dim));
mat=mat*mat';

tic;
[meigs.v,meigs.d]=eigs(mat,1);
meigs.time=toc;

fprintf('Eigs \t %d \n',meigs.time);

tic;
[u,s]=svd(mat);
msvd.time=toc;
msvd.v=u(:,1);
msvd.d=s(1);

fprintf('svd \t %d \n',msvd.time);
tic;
[evecs,eigd]=eig(mat);
me.time=toc;
[mval, ind]=max(diag(eigd));
me.v=evecs(:,ind(1));
me.d=mval;



fprintf('Method: \t time \n');
fprintf('Eigs \t %d \n',meigs.time);
fprintf('svd \t %d \n',msvd.time);
fprintf('eig \t %d \n',me.time);

if (any(abs(msvd.v-meigs.v)>10^-8))
    error('eigenvectors do not match');
end

data={'script','testeigspeed.m'; 'Dimensionality' sprintf('%d x %d',dim,dim)};
data=[data; {'Method','Time(s)';'Eigs', sprintf('%d',meigs.time)}];
data=[data;{'SVD', sprintf('%d',msvd.time)}];
data=[data;{'eig', sprintf('%d',me.time)}];

onenotetable(data,seqfname('~/svn_trunk/notes/eigspeed.xml'));


