%2-24-2006
%Look at the mean squared distance between successive eigenvectors
%on subsequent trials

%load data
load('results\maxinfo\02_17\02_17_max2d_003.mat');

%***************************************
%compute mean squared distance between eigenvectors on succesive trials
c=pmax.newton.c;
edist=zeros(mparam.tlength,simparam.niter-1);
[lastu s]=svd(c{1});
for index=2:size(srmax.newton.y,2)
    [curru s]=svd(c{index});
    edist(:,index-1)=(sum((curru-lastu).^2,1))';
    
end
figure;
hold on
heigs=zeros(1,mparam.tlength);
leigs=cell(1,mparam.tlength);
%for eindex=1:mparam.tlength
for eindex=1:2
    heigs(eindex)=plot(1:simparam.niter-1,edist(eindex,:),getptype(eindex,2));
    plot(1:simparam.niter-1,edist(eindex,:),getptype(eindex,1));
    leigs{eindex}=sprintf('S_%0.1g',eindex-1);
end
set(gca,'Xscale','Log');

xlabel('Iteration');
ylabel('Distance');
legend(heigs,leigs);


%*************************************************************
%random matrices
%*************************************************************
%generate a bunch of mparam.tlength matrices.
rnddist=zeros(mparam.tlength,simparam.niter-1);
rndmat=rand(mparam.tlength,mparam.tlength);
[lastu eigv]=svd(rndmat*rndmat');
for index=2:simparam.niter-1
     [curru s]=svd(c{index});
     rnddist(:,index-1)=(sum((curru-lastu).^2,1))';
end

figure;

hold on
heigs=zeros(1,mparam.tlength);
leigs=cell(1,mparam.tlength);
%for eindex=1:mparam.tlength
for eindex=1:2
    heigs(eindex)=plot(1:simparam.niter-1,rnddist(eindex,:),getptype(eindex,2));
    plot(1:simparam.niter-1,rnddist(eindex,:),getptype(eindex,1));
    leigs{eindex}=sprintf('S_%0.1g',eindex-1);
end
set(gca,'Xscale','Log');

xlabel('Iteration');
ylabel('Distance');
legend(heigs,leigs);