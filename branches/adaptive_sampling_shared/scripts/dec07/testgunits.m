%try to better understand the position properties of text objects
figure;
xlim([0 1]);
ylim([0 1]);
axis square;

h1=text(0,.5,'1 Line');
h2=text(.5,.5,sprintf('1 line \n2line'));

e1beforerotate=get(h1,'Extent');
set(h1,'Rotation',90);
