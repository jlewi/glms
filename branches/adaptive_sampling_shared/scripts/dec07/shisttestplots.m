dsets=[];

for fi=[25 27:28]
    dsets=[];
    fname=fullfile(RESULTSDIR,'shist','071211',sprintf('expnonlin_%03.3g.mat',fi));
dsets(end+1).fname=fname;
dsets(end).lbl=sprintf('i.i.d.');
dsets(end).simvar='rand';


dsets(end+1).fname=fname;
%dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
dsets(end).lbl=sprintf('info. max.');
dsets(end).simvar='max';

nc06shistimg
nc06shisterrors
end


for fi=18:22
    dsets=[];
    fname=fullfile(RESULTSDIR,'shist','071211',sprintf('expnonlin_%03.3g.mat',fi));

    createxmldescr(fname);
end