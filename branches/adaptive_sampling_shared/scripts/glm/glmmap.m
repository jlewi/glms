%10-10
%Find Maximum a priori estimate for a bunch of data
%using gradient ascent
clear('data');

%*************************************************
%a poison model with exponential link function
%load(fullfile(RESULTSDIR,'../../adaptive_sampling/results/lognonlin/10_10/10_10_maxtrack_003.mat'));
dind=1;
alldata{dind}.file=fullfile(RESULTSDIR,'../../adaptive_sampling/results/lognonlin/10_10/10_10_maxtrack_003.mat');
alldata{dind}.varsuffix='rand';
alldata{dind}.npts=1000;


%**************************************************************************
%load(fullfile(RESULTSDIR,'../../adaptive_sampling/results/lognonlin/10_10/10_10_maxtrack_003.mat'));
dind=2;
alldata{dind}.file=fullfile(RESULTSDIR,'../../adaptive_sampling/results/lognonlin/10_10/10_10_maxtrack_003.mat');
alldata{dind}.varsuffix='rand';

alldata{dind}.npts=1000;


%**************************************************************************
%load(fullfile(RESULTSDIR,'../../adaptive_sampling/results/lognonlin/10_11/10_11_maxtrack_003.mat'));
dind=3;
alldata{dind}.file=fullfile(RESULTSDIR,'../../adaptive_sampling/results/lognonlin/10_11/10_11_maxtrack_003.mat');
alldata{dind}.varsuffix='rand';
alldata{dind}.npts=100;

%**************************************************************************
%load(fullfile(RESULTSDIR,'../../adaptive_sampling/results/lognonlin/10_11/10_11_maxtrack_003.mat'));
dind=4;
alldata{dind}.file=fullfile(RESULTSDIR,'nips','11_15','11_15_test_001.mat');
alldata{dind}.varsuffix='max';
alldata{dind}.npts=100;             %how many trials to use

%**************************************************************************
dind=5;
alldata{dind}.file=fullfile(RESULTSDIR,'numint','01_13','01_13_numint_001.mat');
alldata{dind}.varsuffix='rand';
alldata{dind}.npts=250;             %how many trials to use


dind=5;

    %clear the data structures to prevent them from interfering
    datall={};
    data=[];
if (dind==-1)
    %load from workspace
    data.thetainit=mparam.pinit.m;
    data.glm=simparam.glm;
    data.varsuffix='max';
        data.npts=100;
    if (strcmp(data.varsuffix,'rand'))
        
    data.obsrv=srrand.newton1d.nspikes;
    %data.npts=simparam.niter;

    data.x=srrand.newton1d.y(:,1:data.npts);
    data.obsrv=srrand.newton1d.nspikes(:,1:data.npts);
    data.po=mparamrand.pinit;
    else
    data.obsrv=srmax.newton1d.nspikes;
    %data.npts=simparam.niter;
    data.x=srmax.newton1d.y(:,1:data.npts);
    data.obsrv=srmax.newton1d.nspikes(:,1:data.npts);
    data.po=mparammax.pinit;
    end
else
    load(alldata{dind}.file);
    if (strcmp(alldata{dind}.varsuffix,'rand'))
        alldata{dind}.x=srrand.newton1d.y(:,1:alldata{dind}.npts);
alldata{dind}.obsrv=srrand.newton1d.nspikes(1,1:alldata{dind}.npts);
alldata{dind}.thetainit=prand.newton1d.m(:,1);
alldata{dind}.po.m=prand.newton1d.m(:,1);
alldata{dind}.po.c=prand.newton1d.c{1};
alldata{dind}.po.invc=[];
alldata{dind}.shist=srrand.newton1d.nspikes;

alldata{dind}.glm=simparamrand.glm;
    else
        alldata{dind}.x=srmax.newton1d.y(:,1:alldata{dind}.npts);
alldata{dind}.obsrv=srmax.newton1d.nspikes(1,1:alldata{dind}.npts);
alldata{dind}.thetainit=pmax.newton1d.m(:,1);
alldata{dind}.po.m=pmax.newton1d.m(:,1);
alldata{dind}.po.c=pmax.newton1d.c{1};
alldata{dind}.po.invc=[];
alldata{dind}.shist=srmax.newton1d.nspikes;

alldata{dind}.glm=simparammax.glm;
    end
    data=alldata{dind};
    data.npts=alldata{dind}.npts;
end
%************************************************
%generate x which is not only stimulus but spike history
x=zeros(mparam.klength+mparam.alength,data.npts);
x(1:mparam.klength,:)=data.x;

if (mparam.alength>0)
    shist=zeros(mparam.alength,1);
for tind=1:data.npts
    nobsrv=tind-1;
     %only compute shist if we have spike history terms
        if (mparam.alength >0)
            if isequal(data.glm.fglmnc,@logisticA)
                shist(1)=1;
            else
            if (mparam.alength <= (nobsrv))
                shist(:,1)=data.shist(nobsrv:-1:nobsrv-mparam.alength+1);
            else
                shist(1:nobsrv,1)=data.shist(nobsrv:-1:1);
            end
            end
        end
        x(mparam.klength+1:end,tind)=shist;
end
end

[pmap]=postgradasc(data.thetainit,data.po,x,data.obsrv,data.glm);

%compute the covariance matrix  using all the observations
[dldpt dlp2dt]=d2glm(pmap,data.po,data.obsrv,x,data.glm);

