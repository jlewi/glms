%4-18-2007
%   modified it so it computes the covariance matrix without refinding the
%   peak of the posterior
%   turned it into a function
%
%   got rid of loop over trials and simvars.
%2-16-2007
%   modified it so that I can use files in old format using the new object
%   oriented model.
%10_27_06
%This is a script to compute missing covariance matrices for the newton1d update.
%I.e find the posterior covariance matrices which we did not save
%clear all;
%setpathvars;
function [pdata]=fillinlowmem(simfiles,simvar,trials);

error('5-28-07 code is obsolete. Use fillinlowmem for updater class \n');

%sort the trials in ascending order

%repeat for each file
%for findex=1:length(simfiles)
%    simparam.simfile=simfiles{findex};
%repeat for each method
%for index=1:length(varsuffix)
    %load the simulation. This assumes the file is one of our old file
    %formats
    %[pdata, simparam, mparam,sr]=loadsim('simfile',simfiles{findex},'varsuffix',varsuffix{index},'oldver',1,'method','newton1d');
    [pdata, simparam, mparam,sr]=loadsim('simfile',simfiles,'simvar',simvar);
    
    if ~isa(simparam.updater,'Newton1d')
       error('fillinlowmem only works with Newton1d method'); 
    end
    if isempty(trials)
        trials=1:simparam.niter;
    end
    
    for tind=1:length(trials);
        trial=trials(tind);
        
        %make sure covariance matrix doesn't exist for this trial.
        if ~isempty(pdata.c{trial+1})
           fprintf('Skipping trial covariance already exists  for trial %d \n ',trial);
        else
            %find previous trial for which posterior exists
            %to be consistent in our
            %representation
            %ptrial represents how many stimuli have been presented not
            %its index
            ptrial=trial-1;
            while isempty(pdata.c{ptrial+1})
                ptrial=ptrial-1;
            end            
            %get the posterior on this trial this will act as our prior
            prior.m=pdata.m(:,ptrial+1);
            prior.c=pdata.c{ptrial+1};
            
            %now get all the observations and stim for trials in between
            %ptrial and trial
            %05-28-07 we only need these quantities if we are recomputing
            %the mean
    %        x=sr.y(:,ptrial+1:trial);
    %        obsrv.n=sr.nspikes(:,ptrial+1:trial);
            
            %we already have the mean
            %so don't bother calling newton1d to find the means as
            %thats expensive.
            %instead just compute the second derivative
            %if (pdata.m
            %[post]=postnewton1d(prior,x,obsrv,simparam.glm,[],[]);
  
            x=sr.y(:,trial);
            obsrv.n=sr.nspikes(trial);
            [gd1, gd2]=d2glmeps(pdata.m(:,trial+1)'*x,obsrv.n,mparam.glm);
            post.c=prior.c-prior.c*x*(-gd2/(1-gd2*x'*prior.c*x))*x'*prior.c;

            
            %error check. Check means match
%            if (sum((post.m-pdata.m(:,trial+1)).^2,1)>10^-10)
 %               error('computed posterior mean did not match existing mean');
  %              return;
   %         end
            pdata.c{trial+1}=post.c;
            
                        
        end
        %fprintf('Save the results \n');
        %resave the results
        %copy the variables to the propernames
     %   eval(sprintf('%s=data.%s;',pname,pname));

        %save the data
%        saveopts.append=1;  %append results so that we don't create multiple files 
  %      vtosave=[];
    %    vtosave.(pname)='';
      %  savedata(simfiles{findex},vtosave,saveopts);
    end


