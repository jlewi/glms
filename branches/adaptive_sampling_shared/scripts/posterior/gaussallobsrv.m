%11-15
%Go back into a data set and compute the gaussian approximation of the data
%on specified trials using all the observations.

%3-13-07
%code is out dated. It doesn't use my new object model
%some code which might help for object model is
%
%

clear('data');

%**************************************************************************
dind=1;
alldata{dind}.file=fullfile(RESULTSDIR,'poissexp','03_13','03_13_poissexp_002.mat');
alldata{dind}.simvar={'simonline'};
alldata{dind}.trials=[500 1000 1500 2000];             %which trials to compute posterior for
alldata{dind}.cont=0;



%clear the data structures to prevent them from interfering
data=[];

[pdata simparam mparam sr]=loadsim('simfile',alldata{dind}.file,'simvar',alldata{dind}.simvar);
prior.m=pdata.m(:,1);
prior.c=pdata.c{1};

%if we are continuing a simulation we load some results already computed
%from the file
if (alldata{dind}.cont~=0)
    [pbatch]=loadsim('simfile',alldata{dind}.file,'simvar','simbatchrecomp');
    tstart=length(pbatch.m)-1;
    m=pbatch.m;
    c=pbatch.c;
    n=size(m,2);
    pbatch.m=zeros(size(pdata.m));
    pbatch.c=cell(size(pdata.c));
    pbatch.m(:,1:n)=m;
    pbatch.c(1:n)=c;
else

    pbatch.m=zeros(size(pdata.m));
    pbatch.c=cell(size(pdata.c));
    pbatch.m(:,1)=prior.m;
    pbatch.c{1}=prior.c;
    tstart=1;
end
for vind=1:length(alldata{dind}.simvar) %index the suffix

    %create the structure to store the results if it doesn't exist



    %initialize

    bupdater=BatchML;

    post=prior;


    for tind=1:length(alldata{dind}.trials) %index the trials matrix
        trial=alldata{dind}.trials(tind);
        if (mod(tind,10)==0)
            fprintf('Trial \t %d \n',trial);
        end

        %************************************************
        %generate x which is not only stimulus but spike history
        x=zeros(mparam.klength+mparam.alength,alldata{dind}.trials(tind));
        x(1:mparam.klength,:)=sr.y(:,1:trial);

        if (mparam.alength>0)
            shist=zeros(mparam.alength,1);
            for titer=1:trial
                nobsrv=titer-1;
                %only compute shist if we have spike history terms
                if (mparam.alength >0)
                    if isequal(simparam.glm.fglmnc,@logisticA)
                        shist(1)=1;
                    else
                        if (mparam.alength <= (nobsrv))
                            shist(:,1)=sr.shist(nobsrv:-1:nobsrv-mparam.alength+1);
                        else
                            shist(1:nobsrv,1)=sr.shist(nobsrv:-1:1);
                        end
                    end
                end
                x(mparam.klength+1:end,titer)=shist;
            end
        end
        %to initialize gradient ascent choose the estimate of the peak of the
        %posterior computed with our gaussian aproximation
        thetainit=pdata.m(:,alldata{dind}.trials(tind)+1);


        [post,uinfo,einfo]=update(bupdater,post,x,simparam.glm,mparam,sr.nspikes(1,1:trial),[]);
        pbatch.m(:,trial+1)=post.m;
        pbatch.c{trial+1}=post.c;

    end

    %modify simparam
    %  simparam.updater=bupdater;
    simparam.simid='batchrecomp';

    fprintf('Save the results \n');
    savesim('simfile',alldata{dind}.file,'simparam',simparam,'pdata',pbatch,'mparam',mparam,'sr',sr);
end