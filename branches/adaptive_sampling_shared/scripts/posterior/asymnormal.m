%12-27
%
%Check the asymptotic normality and root n consistency of our online update of the posterior mean
% Compute n^.5 (theta_map - theta)
% Compute the mean and covariance.
% Compare the covariance to the inverse of the Fisher information

%2000 trials
%25d
dfiles{1}.infile=fullfile(RESULTSDIR,'nips','11_15', '11_15_lowdcos_001.mat');
dfiles{1}.strial=100;       %how many trials to throw out before computing sample averages 
dind=length(dfiles);

load(dfiles{dind}.infile);
niter=simparammax.niter;

%implicit function to compute the  mean squared difference multiplied by
%the square root
%comptue the difference between the mean and the true parameter
%and multiply by the square root of the trial
%skip the prior
diffmap=pmax.newton1d.m(:,2:end)-[mparam.ktrue;mparam.atrue]*ones(1,niter);
mse=sum(diffmap.^2,1);
mse=mse.^.5;
rnmse=(1:niter).^.5.*mse;

%compute the mean
mudiff=mean(diffmap(:,dfiles{1}.strial:end),2);

%compute the covariance matrix
covdiff=zeros(mparam.klength+mparam.alength,mparam.klength+mparam.alength);
ntrials=simparam.niter-dfiles{1}.strial+1;
for j=dfiles{1}.strial:simparam.niter
    covdiff=covdiff+1/ntrials*(diffmap(:,j)-mudiff)*(diffmap(:,j)-mudiff)'
end
fig=[];
fig.hf=figure;
fig.name='Root N Consistency';
fig.ylabel='$n^{.5}||\theta_{MAP}$-$\theta{}||$';
fig.xlabel='Trial';
fig.semilog=1;
fig.linewidth=4;
fig.hl(1)=plot(rnmse);
fig.lbl{1}='Online Estimate';
hold on;
%plot the error of the true ML estimate
if isfield(pmax,'allobsrv')
    mseall=pmax.allobsrv.m(:,2:end) -mparam.ktrue*ones(1,niter);
    mseall=sum(mseall.^2,1);
    mseall=mseall.^.5;
    rnmseall=(1:niter).^.5.*mseall;
    
    fig.hl(2)=plot(rnmseall,getptype(2,2));
    fig.lbl{2}='True MLE';
end


fig=lblgraph(fig);
legend(fig.hl,fig.lbl);
set(fig.hylabel,'interpreter','latex');

%saveas(fig.hf,'~/figs/12_27_consistency.png')


%compute the step sizes
fss.hf=figure;
fss.xlabel='Trial';
fss.ylabel='Magnitude';
fss.name='Step Size';
plot(sum(diff(pmax.newton1d.m(:,2:end),[],2).^2,1).^.5);
lblgraph(fss);