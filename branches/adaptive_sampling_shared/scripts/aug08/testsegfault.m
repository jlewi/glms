dsets=xfunc(fullfile(RESULTSDIR,'bird_song','datasets','bstanhighd.m'));


for sind=1:length(dsets)
    data=load(getpath(dsets(sind).datafile));  
    
    bssim=data.bssimobj;

    fprintf('dataset %d \n',sind);
      testwind=ones(1,getnwavefiles(bssim.stimobj.bdata));
    testwind(bssim.stimobj.windexes)=0;
    testwind=find(testwind~=0);
end

