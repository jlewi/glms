%02-05-2009
%
%Some of my simulations did not save the mean, covariance matrix, 
%or observations for all trials up to niter because we ran out
%of disk space. 
%This script rolls the simulation back to the last trial
function feb09_rollback(simall,dfiles)
for dind=1:length(simall)
   
    %check if we properly saved the data for all trials
    %ids could be negative indicating data has been deleted
    mids=readmatids(simall(dind).allpost.minfo.maccess);
    cids=readmatids(simall(dind).allpost.cinfo.caccess);
    oids=readmatids(simall(dind).obsrv);
    iids=readmatids(simall(dind).inputs);
    niter=simall(dind).niter;
%     if (mids(end)~=niter)
%         fprintf('Simulation %d: mean: %d of %d \n', dind,mids(end),niter);
%     end
% 
%         if (cids(end)~=niter)
%         fprintf('Simulation %d: covar: %d of %d \n', dind, cids(end),niter);
%         end
%         if (oids(end)~=niter)
%         fprintf('Simulation %d: obsrv: %d of %d \n', dind, oids(end),niter);
%         end
        
     %determine the last trial on which all data was saved
     tlast=cids(end);
     cind=length(cids);
     while(isempty(find(mids==tlast)) || isempty(find(oids==tlast)) || isempty(find(iids==tlast)))
         cind=cind-1;
         tlast= cids(cind);
     end
     
     fprintf('Simulation %d: last: %d of %d \n', dind, tlast,niter);
     
     if (tlast<niter)
        %rollback the simulation
        tstart=tlast+1;
        rollback(simall(dind),tstart);
        simobj=simall(dind);
       
       save(getpath(dfiles(dind).datafile),'simobj','-append');
     end
    fprintf('\n');
end