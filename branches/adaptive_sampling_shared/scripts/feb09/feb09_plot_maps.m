%date: 01-24-2009
%
%Explanation: Plot the strf on
%several trials

scriptfile=mfilename();
%%
width=10;
height=4;


%extension for graphics
fext='.png';

trials=[1000 5000 10000:10000:50000];
trials=[60000:10000:120000];
%trials=[5000:5000:60000];
%trials=[65000:5000:120000];
%trials=[250:250:2000];
trials=175000;

trials=[120*10^3:20*10^3:320*10^3];
gtype='png';
fsize=12;


nrows=length(simobj);

oinfo=cell(ceil(length(simobj)/2),4);

%limits for the colorbars
clim.stim=[-2.5 2.5]*10^-3;
clim.shist=[-2 .5];

%number of sims per set
nset=2;
%**************************************************************************
%loop over the datesets and plot the strfs
%*********************************************************
for sall=1:2:length(simobj)

    ncols=length(trials)+1;
    fh=FigObj('name','STRFs','width',width,'height',height,'naxes',[2 ncols],'fontsize',fsize);

    for sind=sall:sall+1;
        aind=sind-sall+1;

        for tind=1:length(trials)
            trial= trials(tind);


            t=simobj(sind).extra.t;
            f=simobj(sind).extra.f;
            %multiply t by 1000 so its in ms
            t=t*1000;

            %divide freqs by 1000 so its in khz
            f=f/1000;

            if (trial<=simobj(sind).niter)
                setfocus(fh.a,aind,tind);
                strf=getm(simobj(sind).allpost,trial);
                strf=strf(simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2));
                strf=tospecdom(simobj(sind).mobj,strf);
                %            strf=reshape(strf,[simobj(sind).mobj.klength simobj(sind).mobj.ktlength]);



                imagesc(t,f,strf);
            end



        end

    setfocus(fh.a,aind,ncols);
    %plot the true strf
    theta=simobj(sind).observer.theta;
    theta=theta(simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2));
    theta=tospecdom(simobj(sind).mobj,theta);

    imagesc(t,f,theta);


    end


    %add a colorbar to the final image in the first row
    dind=1;
    tind=ncols;

    setfocus(fh.a,dind,tind);
    %add a colorbar
    fh.a(dind,tind).hc=colorbar;



    addtext(fh,[.5,.95],'STRF');
    %%
    %*****************************************************************
    %adjust the labels
    %************************************************************
    %turn off all tickmars
    set(fh.a,'xtick',[]);
    set(fh.a,'ytick',[]);

    %set x and y limits
    set(fh.a,'xlim',[t(1) t(end)]);
    set(fh.a,'ylim',[f(1) f(end)]);

    %turn on xtick,ytick for appropriate graphs
    rind=2;
    set([fh.a(rind,1)],'ytickmode','auto');
    set(fh.a(rind,1),'xtickmode','auto');

    %********************************************************


    set([fh.a],'clim',clim.stim);


    %add ylabels
    for dind=1:2
        ylbl='';
        lbl=simobj(dind).label;
        %split the lbl based on :
        sind=strfind(lbl,':');
        if isempty(sind)
            ylbl=lbl;
        else
            while ~isempty(sind)
                ylbl=sprintf('%s\n%s',ylbl,lbl(1:sind(1)-1));
                lbl=lbl(sind(1)+1:end);
                sind=sind(2:end);
            end
            ylbl=sprintf('%s\n%s',ylbl,lbl);
        end

        if (dind==length(dsets))
            ylbl=sprintf('%s\nFrequency (KHz)',ylbl);
        else
            %don't add ticklables
            set(fh.a(dind,1),'YTickLabel',[]);
        end

        ylabel(fh.a(dind,1),ylbl);

    end



    %add titles
    for tind=1:length(trials)
        if (trials(tind)>=1000)
            tl=sprintf('Trial %02gk',trials(tind)/1000);
        else
            tl=sprintf('Trial %03g',trials(tind));
        end
        title(fh.a(1,tind),tl);
    end

    %add xlabels
    for tind=1:1
        xlabel(fh.a(2,tind),sprintf('Time(ms)'));
    end


    lblgraph(fh);
    space.cbwidth=.15;
    sizesubplots(fh,space,[],[],[0 0 0 .1]);

    %%
    %**************************************************************************
    %make plots of the spike history and bias coefficients
    %**************************************************************************
    fshist=FigObj('name','Spike history','xlabel','i','ylabel','trial','naxes',[2 2]);

    %make a plot of the spike history on each trial

    ntrials=min([simobj(sall:sall+1).niter]);
    for sind=sall:sall+nset-1;
        aind=sind-sall+1;
        setfocus(fshist.a,1,aind);

        theta=getm(simobj(sind).allpost);
        theta=theta(:,1:ntrials);

        shistcoeff=theta(simobj(sind).mobj.indshist(1):simobj(sind).mobj.indshist(2),:);

        imagesc([1:simobj(sind).mobj.alength],1:ntrials,shistcoeff');
        title(fshist.a(1,aind),simobj(sind).label);

        set(gca,'ylim',[1 ntrials]);
        set(gca,'xlim',[1 simobj(sind).mobj.alength]);
        set(gca,'ydir','reverse');
        set(gca,'yscale','log');

        %for the second row plot the true value
        setfocus(fshist.a,2,aind);
        theta=simobj(sind).observer.theta;
        theta=theta(simobj(sind).mobj.indshist(1):simobj(sind).mobj.indshist(2));
        imagesc(theta');
        set(gca,'ytick',[]);
        set(gca,'xlim',[1 simobj(sind).mobj.alength]);
        ylabel(fshist.a(2,aind),'true');

    end
    %
    % clim=get([fshist.a.ha],'clim');
    % clim=cell2mat(clim);
    % clim=[min(clim(:,1)) max(clim(:,2))];
   
    set([fshist.a.ha],'clim',clim.shist);

    %add a colorbar
    fshist.a(1,2).hc=colorbar;


    lblgraph(fshist);

    addtext(fshist,[.5 .95], 'Spike History Coeff');
    sizesubplots(fshist,[],[],[.75 .25],[0 0 0 .1]);

    %%
    %**************************************************************************
    %plot the bias
    %**************************************************************************
    fbias=FigObj('name','Bias','title','bias','xlabel','trial','ylabel','bias','naxes',[1 1]);

    for sind=sall:sall+1;
        theta=getm(simobj(sind).allpost);

        bias=theta(simobj(sind).mobj.indbias,1:simobj(sind).niter+1);

        hp=plot(0:simobj(sind).niter,bias);
        addplot(fbias.a,'hp',hp,'lbl',simobj(sind).label);
    end

    %plot the true bias
    truebias=simobj(1).observer.theta(simobj(1).mobj.indbias);
    ltrial=max([simobj(sall:sall+1).niter]);
    hp=plot(0:ltrial,truebias*ones(1,1+ltrial));
    addplot(fbias.a,'hp',hp,'lbl','true');
    set(fbias.a.ha,'xscale','log');
    lblgraph(fbias);





%%
%**************************************************************
%Create a onenote table of the results
%**************************************************************

oinfo{ceil(sall/2),1}=fh;
oinfo{ceil(sall/2),2}=fshist;
oinfo{ceil(sall/2),3}=fbias;

oinfo{ceil(sall/2),4}=[{simobj(sall).label, getrpath(dfiles(sall).datafile);simobj(sall+1).label, getrpath(dfiles(sall+1).datafile); 'mmag', simobj(sall).mobj.mmag;'maxrate', simobj(sall).extra.maxrate}];
end

oinfo=[{'script',scriptfile,'',''}; oinfo];

onenotetable(oinfo,seqfname('~/svn_trunk/notes/optgp.xml'));

