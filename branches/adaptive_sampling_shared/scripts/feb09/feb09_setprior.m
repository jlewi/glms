%script to play around with how we should compute the prior for 
%my simulations
v=load('/home/jlewi/svn_glms/allresults/bird_song/090130/bsglmfit_data_002.mat');

bssimobj=v.bssimobj;

%compute the stimulus statistics to use for setting the priro
strfdim=getstrfdim(bssimobj.bdata);
maxf=MBSFTSep.maxn(strfdim);

windexes=find(waveissong(bssimobj.bdata,1:getnwavefiles(bssimobj.bdata)));

%comp the stimulus statistics for this neuron
%the following code computes the stimulus statistics
%otherwise we load them from a file

compstats=false;
if (compstats)
    songstats=BSStimStatsFTSep.comp(bssimobj.bdata,windexes);

    fstats=FilePath('RESULTSDIR',fullfile('asympopt',datastr(now,'yymmdd'),'birdsong_stim_stats.mat'));
    fname=seqfname(fstats);
    save(getpath(fname),'songstats');
else
   fstats=FilePath('RESULTSDIR',fullfile('asympopt','090203','birdsong_stim_stats_001.mat'));
   v=load(getpath(fstats));
   songstats=v.songstats;
end


%set the hardcutoff frequency
%setting a hardcutoff reduces the number of parameters in the model
nfcutoff=20;
ntcutoff=8;

%create a model object
mp.glm=GLMPoisson('canon');
initmag=.05; %initial guess
mp.mmag=1;
    mp.klength=strfdim(1);
    mp.ktlength=strfdim(2);
    mp.alength=3;
    mp.hasbias=true;

%We need to create a model object to identify the prob
[btouse]=MBSFTSep.btouseforcutoff(strfdim,nfcutoff,ntcutoff);

mp.btouse=btouse;
mobj=MBSFTSep(mp);

%**************************************************************************
%compute the variance for the stimulus coefficients
%**************************************************************************

%map the log of the amplitude  of the mean to a linear range between
%cinit(1) and cinit(2)
%cinitrange=[10^-7 10^1]; 
cinitrange=[10^-3 10^1]; 
lmean=log10(abs(songstats.mean(btouse)));
lmean=lmean-min(lmean);
lmean=lmean/(max(lmean));

%cinit is the diagonal entries for our covariance matrix
cstiminit=lmean*(log10(cinitrange(2))-log10(cinitrange(1)))+log10(cinitrange(1));
cstiminit=10.^cstiminit;

%set variance of bias and stimulus to be one order of magnitude 
%greater than order of magnitude of terms
shistvar=10*ones(mobj.alength,1);

biasvar=100;


cprior=zeros(getparamlen(mobj),1);
cprior(mobj.indstim(1):mobj.indstim(2))=cstiminit;
cprior(mobj.indshist(1):mobj.indshist(2))=shistvar;
cprior(mobj.indbias)=biasvar;


% fprior=FilePath('RESULTSDIR',fullfile('asympopt',datestr(now,'yymmdd'),'ftsep_prior.mat'));
% fprior=seqfname(fprior);
% 
% save(getpath(fprior),'cprior','mobj');
