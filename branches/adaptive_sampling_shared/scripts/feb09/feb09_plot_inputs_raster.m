%02-10-2009
%
%Make a plot of some of the wavefiles and a raster plot of the responses
%to that wavefile.
%
%
%Make synthetic raster plots as well
%
%These are for the batch results
%
clear variables;

%indicates the test set should be just the noise files
testset='noise';

savefigs=false;
gdir='~/tmp/rasters';

%threshold below which we force coefficients of STRF to zero
strfthresh=1*10^-3;


switch testset
    case 'noise'
        %trained on just the noise stimuli
        [dfiles]=feb09_data_fitsep_noise_090225_001;
        dfiles=dfiles';

        fsuffix='noise';
    case 'song'
        [dfiles]=feb09_data_fitsep_birdsong_090225_001;
        dfiles=dfiles';

        fsuffix='song';
end

%seqfiles=seqfiles(1,:);

sname=mfilename('full');

%***********************************************************************
%Compute data for fake raster plots
%***********************************************************************
%%
%rdata= size(seqfiles(1,))xsize of test set
nrepeats=10;
for dind=1:size(dfiles,1)
    %use the info. max. design
    v=load(getpath(dfiles(dind).datafile));
    bssimobj(dind)=v.bssimobj;

    %get the final estimate of theta
    if ~isempty(bssimobj(dind).results)
        theta=bssimobj(dind).results(end).theta;
        shistfilt=theta(bssimobj(dind).mobj.indshist(1):bssimobj(dind).mobj.indshist(2));
        bspost=bssimobj(dind).updater.bpost;
        
        if isa(bspost,'BSlogpostlin')
           bspost=BSlogpost('bdata',bspost.bdata); 
        end
                    
                    
        glm=bssimobj(dind).mobj.glm;

        %determine the test set
        windexes=ones(1,getnwavefiles(bssimobj(dind).bdata));


        windexes(bssimobj(dind).updater.windexes)=false;
        wtest=find(windexes==1);

        switch lower(testset)
            case 'noise'
                %select just the noise files in the test set
                wtest=wtest(~logical(waveissong(bssimobj(dind).bdata,wtest)));
            case 'song'
                %select just the bird song
                wtest=wtest(logical(waveissong(bssimobj(dind).bdata,wtest)));
        end


        %***************************************************************
        %filter the strf
        %***************************************************************
        theta=bssimobj(dind).results(end).theta;        
        [stimcoeff,shistcoeff,bias]=parsetheta(bssimobj(dind).mobj,bssimobj(dind).results(end).theta);
    
        if isa(bssimobj(dind).mobj,'MBSFTSep')
           %project the stimulus coefficients into the spectral domain
           [stimcoeff]=tospecdom(bssimobj(dind).mobj,stimcoeff);
            mobj=MParamObj(struct(bssimobj(dind).mobj));
        else
            mobj=bssimobj(dind).mobj;
        end
        ind=find(abs(stimcoeff)<strfthresh);
        stimcoeff(ind)=0;
        siminfo(dind).stimcoeff=stimcoeff;
        siminfo(dind).shistcoeff=shistcoeff;
        siminfo(dind).bias=bias;
        theta=[stimcoeff(:);shistcoeff;bias];
        for wind=1:length(wtest)

            [ntrials,cstart]=getntrialsinwave(bspost.bdata,wtest(wind));
            data(dind,wind).raster=zeros(nrepeats,ntrials);
            data(dind,wind).wind=wtest(wind);

            %compute glmproj ignoring shist

                [glmproj,varargout]=compglmprojnoshist(bspost,wtest(wind),mobj,theta);


            for rind=1:nrepeats
                %initialize the spike history
                shist=zeros(bssimobj(dind).mobj.alength,1);

                for tind=1:ntrials
                    %compute the expected firing rate and sample it
                    rexp=fglmmu(glm,glmproj(tind)+shist'*shistfilt);
                    obsrv=sampdist(glm,rexp);
                    data(dind,wind).raster(rind,tind)=obsrv;

                    %update shist
                    shist=[shist(1:end-1); obsrv];
                end
            end
        end
    end
end


%%


width=6.1;
height=4;

for rind=1:numel(bssimobj)

    %plot the inputs for the test inputs which are noise

    bdata=bssimobj(rind).bdata;



    if ~isempty(bssimobj(rind).results)
        for ind=1:size(data(rind),2);
            waveind=[data(rind,ind).wind];



            fh(rind,ind)=FigObj('name','BS Input and Raster', 'naxes',[3,1],'width',width,'height',height);
            figure(fh(rind,ind).hf);

            %plot the wave file

            [ntrials,cstart]=getntrialsinwave(bdata,waveind);
            [obj,spec,outfreqs,t]=getfullspec(bdata,waveind);

            %truncate spec and t by cstart
            spec=spec(:,cstart:end);
            t=t(1:ntrials);

            %convert to Khz
            outfreqs=outfreqs/1000;
            setfocus(fh(rind,ind).a,1,1);
            imagesc(t,outfreqs,spec);
            ylabel(fh(rind,ind).a(1,1),'Frequency (Khz)');



            hc=colorbar;
            sethc(fh(rind,ind).a(1,1),hc);

            set(fh(rind,ind).a(1,1).ha,'xlim',[t(1) t(end)]);

            set(fh(rind,ind).a(1,1).ha,'xtick',[]);
            set(fh(rind,ind).a(1,1).ha,'ylim',[outfreqs(1) outfreqs(end)]);

            %**********************************************************************
            %make a raster plot
            setfocus(fh(rind,ind).a,2,1);
            obsrv=getobsrvmat(obj,waveind);
            obsrv=obsrv(:,cstart:end);

            hold on;
            for r=1:size(obsrv,1);
                sind=find(obsrv(r,:)>0);
                plot(t(sind),r*ones(1,length(sind)),'MarkerSize',4,'Marker','o','MarkerFaceColor','b','line','none');
            end

            ylabel(fh(rind,ind).a(2,1),'True');
            set(fh(rind,ind).a(2,1).ha,'ytick',[]);
            set(fh(rind,ind).a(2,1).ha,'xtick',[]);
            set(fh(rind,ind).a(2,1).ha,'xlim',[t(1) t(end)]);


            %*******************************************************************
            %Make a raster plot using the model
            %**********************************************************************
            setfocus(fh(rind,ind).a,3,1);
            obsrv=data(rind,ind).raster;


            hold on;
            for r=1:size(obsrv,1);
                sind=find(obsrv(r,:)>0);
                plot(t(sind),r*ones(1,length(sind)),'MarkerSize',4,'Marker','o','MarkerFaceColor','b','line','none');
            end

            xlabel(fh(rind,ind).a(3,1),'Time(s)');
            ylabel(fh(rind,ind).a(3,1),'Predicted');
            set(fh(rind,ind).a(3,1).ha,'ytick',[]);

            set(fh(rind,ind).a(3,1).ha,'xlim',[t(1) t(end)]);


            lblgraph(fh(rind,ind));
            sizesubplots(fh(rind,ind),[],[],[.6 .2 .2]);

           

            pfile{rind,ind}=sprintf('rasters_%02g_%s.png',rind,fsuffix);
            fname=fullfile('~/tmp/rasters/',pfile{rind,ind});
            %    fname=seqfname(fname);
           
            if (savefigs)
            saveas(fh(rind,ind).hf,fname,'png');
            end

        end
    end
end


%*********************************************************************
%Plot the strf
width=3;
height=3;
for sind=1:length(bssimobj)

    if ~isempty(bssimobj(sind).results)

    %**************************************************************************
    %plot the strf
    %**************************************************************************
    fstrf(sind,1)=FigObj('name','STRF','width',width,'height',height);
    figure(fstrf(sind,1).hf);
    mobj=bssimobj(sind).mobj;
    [t,freqs]=getstrftimefreq(bssimobj(sind).bdata);

    %convert to ms and hz
    t=t*1000;
    freqs=freqs/1000;
    title(fstrf(sind,1).a,'STRF');




    stimcoeff=siminfo(sind).stimcoeff;
    shistcoeff=siminfo(sind).shistcoeff;

    row=sind;
    col=1;
%    imagesc(t,freqs,tospecdom(mobj,stimcoeff));
    imagesc(t,freqs,reshape(stimcoeff,mobj.klength,mobj.ktlength));
    xlabel(fstrf(row,col).a,'Time(ms)');
    ylabel(fstrf(row,col).a,'Frequencies(kHz)');
    hc=colorbar;
    xlim([t(1) t(end)]);
    ylim([freqs(1) freqs(end)]);


    lblgraph(fstrf(sind,1));

    %************************************************************************
    %Plot the spike history
    %***********************************************************************
    fstrf(sind,2)=FigObj('name','Spike History','width',width,'height',height);
    figure(fstrf(sind,2).hf);

    row=sind;
    col=2;
    
    t=-1*[getshistlen(mobj):-1:1];
    hp=plot(t,shistcoeff);
    pstyle.marker='o';
    pstyle.markerfacecolor='b';
    pstyle.linewidth=3;
    addplot(fstrf(row,col).a,'hp',hp,'pstyle',pstyle);
    xlabel(fstrf(row,col).a,'Time (ms)');
    title(fstrf(row,col).a,'Spike History Filter');
    xlim([t(1) t(end)]);


    lblgraph(fstrf(sind,2));
    
    
    strffile{sind}=sprintf('strf_%02g_%s.png',sind,fsuffix);
    shistfile{sind}=sprintf('shist_%02g_%s.png',sind,fsuffix);
    
    if (savefigs)
    saveas(fstrf(sind,1).hf,fullfile(gdir,strffile{sind}),'png');
    saveas(fstrf(sind,2).hf,fullfile(gdir,shistfile{sind}),'png');
    end
    end
end


%%
%*************************************************************************
%one note table
ncols=size(data,2)+3;
oinfo=cell(1,ncols);
oinfo{1,1}={'Script',sname;'Explanation','Plots of the true raster and predicted raster for the test set. GLM fitted with batch methdods'};

odata=cell(size(data,1),ncols);
for rind=1:size(data,1)
    for cind=1:size(data,2)
        odata{rind,cind}=fh(rind,cind);
    end

    cind=size(data,2)+1;
    odata{rind,cind}=fstrf(rind,1);
    cind=cind+1;
    odata{rind,cind}=fstrf(rind,2);
    
    if (savefigs)
    dinfo={'Datafile',getpath(dfiles(rind).datafile); 'rawdatafile',getpath(dfiles(rind).rawdatafile); 'raster image',pfile{rind,1};'strf image',strffile{rind};'shist image',shistfile{rind};'bias',siminfo(rind).bias};
    else
        dinfo={'Datafile',getpath(dfiles(rind).datafile); 'rawdatafile',getpath(dfiles(rind).rawdatafile); 'raster image',pfile{rind,1};'bias',siminfo(rind).bias};        
    end
    cind=cind+1;
    odata{rind,cind}=dinfo;
end

otbl=[oinfo;odata];
fname=sprintf('rasters_%s.xml',testset);
onenotetable(otbl,seqfname(fullfile('~/svn_trunk/notes/',fname)));

