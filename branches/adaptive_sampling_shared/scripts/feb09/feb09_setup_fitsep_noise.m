%**************************************************************************
%
%Explanation: Setup the simulation for fitting a low d model using batch
%updates
function dsets=phdsetupftsepfithighd()


rfiles=phdbsdata_neurons_corrected();

dsets=struct;
dind=0;

%table to describe these datasets
%extra row for header
odinfo=cell(length(rfiles)+1,2);
odinfo{1,1}='neuron';
odinfo{1,2}='simulations';


for rind=1:length(rfiles)


    %try decreasing the bin size and increasing the number of time bins
    allparam.obsrvwindowxfs=124;
    allparam.stimnobsrvwind=20;
    allparam.freqsubsample=1;
    allparam.model='MBSFTSep';


    allparam.rawdatafile=rfiles(rind);

    scale=10;
    allparam.prior.stimvar=10^-2*scale;
    allparam.prior.shistvar=1*scale;
    allparam.prior.biasvar=10*scale;


    %  nfreq=20;
    % ntime=4;
    nfreq=10;
    ntime=4;

    %create a bdata an mobject to use to decide which frequency components to
    %include
    bdata=BSSetup.initbsdata(allparam);
    strfdim=getstrfdim(bdata);


    btouse=MBSFTSep.btouseforcutoff(strfdim,nfreq,ntime);
    allparam.mparam.btouse=btouse;


    %how many repeats of the data to use
    allparam.stimparam.nrepeats=10;

    %train on just the noise files
    allparam.windexes=16:24;
    %************************************************************
    %Fit GLM
    %************************************************************
    dind=dind+1;
    dnew=BSSetup.setupfitpoiss(allparam);
    dsets=copystruct(dsets,dind,dnew);


    [nname]=getfilename(allparam.rawdatafile);
    odinfo{rind+1,1}=nname;
    odinfo(rind+1,2)={{'BatchML Fit GLM', getrpath(dnew.datafile); 'nfreq',nfreq;'ntime',ntime}};

end


opath=getpath(dsets(1).datafile);
opath=fileparts(opath);

fname=getfilename(dsets(1).datafile);

%get the startnum of the output files
%assume file is name 000.mat
startnum=fname(end-6:end-4);

fname=getfilename(dsets(end).datafile);
endnum=fname(end-6:end-4);




%**************************************************************
%create a datasetfile
%**************************************************************
setupfile=seqfname(sprintf('~/svn_glms/adaptive_sampling/scripts/feb09/feb09_data_fitsep_noise_%s.m',datestr(now,'yymmdd')));
writeminit(setupfile,dsets,[]);

oinfo=[{{'setupfile',setupfile}};{odinfo}];
oname=sprintf('%s_%s-%s.xml',mfilename(),startnum,endnum);
onenotetable(oinfo,fullfile(opath,oname));