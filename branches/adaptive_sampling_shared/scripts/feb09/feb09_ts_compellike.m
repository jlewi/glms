%For the tangent space data from nips
%
%Compute the expected log-likelihood using the tangent space
clear variables;
setpathvars;

[dsets]=nips08databstanhighd;

%speedup of rank 2 vs. infomax full
seqfiles=[dsets(2) dsets(3)];

%this creates a new datafile to store the expllikelihood
%don't do this if you want to continue processing something thats already
%been created
dind=3;
sim=BSExpLLike('newdata',dsets(dind).datafile,'usetanpost',true);