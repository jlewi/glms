%1-14-2005
%
% Explanation: The purpose of this function is to be used with the matlab
% solver to numerically calculate the expected difference between the EKF
% estimate of the peak of the likelihood funciton and the true ML estimate
% for a single observation in 1-d
%
%   y - value of the stimulus
%       we are integrating over the joint probability of p(y,nspikes)
%       nspikes is the number of spikes in response to y
%   mlest - the estimated likelihood
%   ml    - true max likelihood
%   mldiff - the difference between the likelihood 
%
%
%We do not consider zero spikes because this causes error when take log 0.
%True theta parameter
theta=1;
% Since we are take the expectation over a poisson process
% we need to sum over the number of observed events
% smax is the maximum value of our integral
smax=130;
smax=70;
%length of the time window in which the response is observed
twindow=20; 

%pt about which we take taylor expansion
tpt=2;

%to compute the expectation we construct 2d matrices to store the joint
%distribution of p(y,nspikes)
%we start by constructing 2 2d matrices y and nspikes
%which store the associated value 
%range of y
ymin=.02;
ymax=2.02;
dy=.001;
y=(ymin:dy:ymax)';
y=y*ones(1,smax);

nspikes=1:smax;
nspikes=ones(size(y,1),1)*nspikes;

%construct the joint distribution
%we need a matrix to represent factorial of nspikes
factn=[1:smax];
for index=2:smax
    factn(index)=index*factn(index-1);
end
factn=ones(size(y,1),1)*factn;

%joint probability is p(y)*p(nspikes|y)
%p(y) is uniform distribution
%p(nspikes|y) is poisson with rate lambda=twindow*e^(theta*y)
lambda=twindow*exp(theta*y);
pj=1/(size(y,1))*exp(-lambda).*(lambda).^nspikes./factn;


%compute the taylor approximation of the likelihood
mlest=((nspikes/twindow).*exp(-tpt*y)-1)*1./y+tpt;

%true ml estimate
ml=log(nspikes/twindow)*1./y;

%compute the difference
mldiff=mlest-ml;

%comput the expected value
exmldiff=mldiff.*pj;
exmldiff=sum(exmldiff(:))








        