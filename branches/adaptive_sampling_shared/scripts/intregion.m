%9-29-06
%Compute the 2-d region for the integration

%Input:
%1) Mean of the posterior
%2) Covariance of the posterior
%3) Magnitude constraint
d=4;

%generate a random d-dimensional covariance matrix
evecs=orth(rand(d,d));
eigd=ceil(rand(d,1)*10)/10;
%C=[3 2 1 4 ; 2 2 3 3; 4 1 3 1];
C=evecs*diag(eigd)*evecs';
mu=floor(rand(d,1)*10^4)/10^3;
mag=1;
