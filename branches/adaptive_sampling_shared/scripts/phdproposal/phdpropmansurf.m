%make a 3d plot of the manifold, 

%fontsize
fsize=28;

%********************************************************************
%Plot the manifold
%********************************************************************
%manifold is a sinewave in direction sdir
sdir=normmag([1;1]);

%generate the x, y points
freq=1;

pts=[-1:.05:1];
[x,y]=meshgrid(pts,pts);

%compute the projection on sidr
proj=x*sdir(1)+sdir(2)*y;
z=sin(2*pi*freq*proj);

%find those points with proj >.5 or <.5 and set to nan so they don't get
%plotted
ind=find(abs(proj)>.5);
z(ind)=nan;


fman=FigObj('name','Manifold','width',2.5,'height',2.5);
fman.a=axesobj('nrows',1,'ncols',1);
hold on;

hs=surf(x,y,z);
set(hs,'linestyle','none');

zlim([-2 2])

%set the view point
set(gca,'view',[14 18]);

set(gca,'xtick',[]);
set(gca,'ytick',[]);
set(gca,'ztick',[]);


%draw some arrows for the axes
lw=4;
o=[-1;-1;-1]; %origin
da=1;
arrow(o,o+da*[1;0;0],'LineWidth',lw);
arrow(o,o+da*[0;2;0],'LineWidth',lw);
arrow(o,o+da*[0;0;2],'LineWidth',lw);
xlim([-1 1]);
ylim([-1 1]);
zlim([-1 2]);

pos=o+da*[1;0;-.25];
hx=text(pos(1),pos(2),pos(3),'\theta_1','FontSize',fsize);
pos=o+da*[0;-.85;0];
hy=text(pos(1),pos(2),pos(3),'\theta_2','FontSize',fsize);
pos=o+da*[0;0;2];
hz=text(pos(1),pos(2),pos(3),'\theta_3','FontSize',fsize);

%saveas(gethf(fman),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/manifold.png');
saveas(gethf(fman),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/manifold.eps','epsc2');
% xlabel('\theta_1');
% ylabel('\theta_2');
% zlabel('\theta_3');
