%05-21-2008
%   modified the timing figure for my proposal.
%3-18-2008
%   modified for new object model so I can redo plots using helvetica fonts
%7-31-2007
%   use new object oriented model and new data.
%   


DAYDIR='070813';

dfile=[];
diary('~/tmp/runsimtiming.txt');


dims=[100 200 250 300 400 500 625 750];

dims=[100:100:800];
sims=[];
for dind=1:length(dims)
    
sims(dind).dim=dims(dind);
sims(dind).fname=fullfile(RESULTSDIR,'poissexp',DAYDIR,sprintf('simtime_%dd_%s_001.mat',sims(dind).dim,DAYDIR));
sims(dind).simvar=sprintf('max%d',sims(dind).dim);
if ~exist(sims(dind).fname,'file')
    error('file %s doesnot exist will not be able to do sim',sims(dind).fname);
    
else
    %check simulation variable exists
    if ~(varinfile(sims(dind).fname,sims(dind).simvar))
       warning('variable %s does not exist in file %s', sims(dind).simvar, sims(dind).fname);
    end
end
end

%sims(1).fname=fullfile(RESULTSDIR,'poissexp','070807',sprintf('simtime_070807_%d_002.mat',sims(1).dim));

%%
%start trial
%specfies how many trials at the beginning to throw out
%which dimensionality to use for fitting the data
dstart=200;

% loop through the data and compute the time as a function of the
% dimensionality of the different steps
stime=[];
stime.totalt=zeros(1,length(sims));
for sind=1:length(sims)

sim=SimulationBase('fname',sims(sind).fname,'simvar',sims(sind).simvar);

stime.dim(sind)=getklength(getmobj(sim));
mobj=getmobj(sim);
tstart=(getklength(mobj)+2);


post=getpost(sim);
uinfo=[post(tstart+1:end).uinfo];
%plot the optimization time
sr=getsr(sim);
stiminfo=[sr(tstart:end).stiminfo];
searchtime=[stiminfo.searchtime];
stime.fishermax(sind)=median([searchtime.fishermax]);
stime.tquadmod(sind)=median([searchtime.tquadmod]);

stime.eigtime(sind)=median([uinfo.eigtime]);
stime.ntime(sind)=median([uinfo.ntime]);

stime.totalt(sind)=stime.fishermax(sind)+stime.tquadmod(sind)+stime.ntime(sind)+stime.eigtime(sind);

stime.stdtotalt(sind)=std([searchtime.fishermax]+[searchtime.tquadmod]+[uinfo.ntime]+[uinfo.eigtime]);
end


%**************************************************************************
%*******************************************************************
%Plot it
%%
ftiming=FigObj('name','timing','width',6,'height',3,'fontsize',18);

ftiming.a=AxesObj('xlabel','dim(\theta)','ylabel','time(seconds)');


%ftiming.hf=figure;
hold on;

%index of first pt to use for fitting the data
istart=find(stime.dim>=dstart,1);
istart=istart(1);


%x pts to use for fitted x
x=dstart:stime.dim(end);
%********************************************************
%total time
%*****************************************************



hp=plot(stime.dim,stime.totalt);

pstyle=[];
pstyle.marker='o';
pstyle.linestyle='none';
pstyle.markerfacecolor='b';
pstyle.markersize=12;
pstyle.color='b';


ftiming.a=addplot(ftiming.a,'hp',hp,'pstyle',pstyle);

%***************************************************
%fitted polynomial
%**************************************************
%fit total time to to a 2nd degree
%polynomial eventhough it really is O(n^3)
%do this to illustrate the speedp do to the rank 1 step
[ptotalt.p,ptotalt.s]=polyfit(stime.dim(istart:end),stime.totalt(istart:end),2);
y=ptotalt.p(1)*x.^2+ptotalt.p(2)*x+ptotalt.p(3);



hp=plot(x,y);

pstyle=[];
pstyle.linestyle='-';
pstyle.color='k';
pstyle.linewidth=4;

ftiming.a=addplot(ftiming.a,'hp',hp,'pstyle',pstyle);












%**************************************
%adjust the graph
%**********************************************
set(gca,'yscale','log');
xlim([195 800]);
ftiming=lblgraph(ftiming);
set(gca,'ytick',10.^[-3:-0]);

lblgraph(ftiming);


%ylim([min([updatetime searchtime eigtime totaltime])-.001 max([updatetime searchtime eigtime totaltime])]+.001);
%We use save fig because we want the tick labels to be in base 10
%outfile='~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/timing.eps';
%print -dpng -r600 '~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/timing.png'
saveas(gethf(ftiming),outfile,'epsc2')
