%4-20-2008
%Make a plot of the stimulus spectrogram and below it a raster plot of
%spikes

xscript('/home/jlewi/svn_trunk/allresults/bird_song/080423/bsglmfit_setup_001.m');

%index of the file to plot
wavindex=1;

bdata=load(getpath(dsets.datafile),'bdata');
bdata=bdata.bdata;


if ~exist('compvar','var')
    compvar=true;
end

%size of bins for psth in seconds
%this is also the binsize used for constructing the firing rate
psthbinsize=.001;

%length of the hanning window in seconds
%hwinlen=.015;
hwinlen=2.5
%for wavindex=1:getnwavefiles(bdata)
for wavindex=1
    %for wavindex=1:1
    [bdata,spec,outfreqs]=getfullspec(bdata,wavindex);

    [bdata,stiminfo]=getstiminfo(bdata,wavindex);
 spiketimes=getstimresp(bdata,wavindex);
    
    %time of the last spike
    %this might be after the post-synaptic silence
    tlastspike=max(vertcat(spiketimes{:}));
   
    fh=FigObj('name','Spike Triggered Average','height',6,'width',5);
    fh.a=AxesObj('xlabel','time(s)','ylabel','Frequency(hz)','nrows',3,'ncols',1)

    fh.a=setfocus(fh.a,1,1);
    warning('Im not sure the times are computed correctly. It does not match my original spectrogram. Shouldnt I be scaling by binsize or increment?');
    t=[0:(size(spec,2)-1)]*getobsrvtlength(bdata);

    %add the pre and post silence to the spectrogram
   % [bdata,npre,npost]=nframesinsilence(bdata);
    imagesc(t,outfreqs, spec);
    fh.a(1,1)=title(fh.a(1,1),'Spectrogram');
    xlim([t(1) tlastspike]);
    ylim([outfreqs(1) outfreqs(end)]);
    hc=colorbar;
    fh.a(1,1)=sethc(fh.a(1,1),hc);

    %add a point and text to indicate end of pre and post
    % hold on
    % hp=plot(0,npre*getobsrvtlength(bdata),'k.');
    % pstyle.MarkerFaceColor='k';
    % pstyle.MarkerSize=8;
    % fh.a(1,1)=addplot(fh.a(1,1),'hp',hp,'pstyle',pstyle);

    %**************************************************************************
    %make a raster plot of the spike
    %**************************************************************************
   

    fh.a=setfocus(fh.a,2,1);
    hold on;
    for rep=1:length(spiketimes)
        %each cell array is the spike times on a different trial
        hp=plot(spiketimes{rep},rep*ones(1,length(spiketimes{rep})),'b.');
        fh.a(2,1)=addplot(fh.a(2,1),'hp',hp);
    end
    fh.a(2,1)=title(fh.a(2,1),'Raster Plot');
    fh.a(2,1)=xlabel(fh.a(2,1),'time(s)');
    fh.a(2,1)=ylabel(fh.a(2,1),'trial');

    xlim([t(1) tlastspike]);
    ylim([1 length(spiketimes)]);

    %******************************************************************
    %bin the responses and a plot the fraction of trials on which we get
    %a spike in that bin
    fh.a=setfocus(fh.a,3,1);

    bc=[0:psthbinsize:(stiminfo.duration+(npre+npost)*getobsrvtlength(bdata))];
    scounts=histc(vertcat(spiketimes{:}),bc);
    bar(bc,scounts/length(spiketimes));
    fh.a(3,1)=title(fh.a(3,1),'Fraction trials with spike');
    fh.a(3,1)=xlabel(fh.a(3,1),'time(s)');

    xlim([t(1) tlastspike]);
    ylim([0 max(scounts/length(spiketimes))]);
    %fh.a(3,1)=addplot(fh.a(3,1),'hp',hp);


    
    lblgraph(fh);
    fh=sizesubplots(fh);
%    tbl={fh;{'function','bsplotstimraster'; 'Data File',getpath(dsets.datafile); 'Bin size for PSTH',psthbinsize; 'Wave File', stiminfo.fname; 'Explanation','The stimulus and the spiking response. Stimulus includes pre+post silences.'}};
 %   onenotetable(tbl,seqfname('~/svn_trunk/notes/bsstimraster.xml'));
    %saveas(gethf(fh),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/bsspectrogram.png');
end