%*****************************************************************
%make some schematics for illustrating
%the simplifcations that take place in the batch setting
%**********************************************************


%make a contour plot which indicates the information as a
%function of the sequences. The sequences being 2-d in this case

fwidth=8;
fheight=4;
fsize=12;   %font size

xlbl='';
ylbl='';

ftrue=FigObj('width',fwidth,'height',fheight,'fontsize',fsize,'name',true');
ftrue.a=AxesObj('xlabel',xlbl,'ylabel',ylbl);

%contours will be lines with slope -1
%value of the contours will be a quadratic function of the 
%intercept

m=-1;

%b=[0:5];

x=[0:.01:.5];
y=x;

[x,y]=meshgrid(x,y);

b=(y-m*x);

z=-.5*(b-.5).^2+1;
%z=b;

hold on;
[c,hc]=contourf(x,y,z,30);
set(hc,'linestyle','none')
set(gca,'xtick',[]);
set(gca,'ytick',[]);


%add an arrow to indicate the direction along which we have to optimize
xpts=[.25 .4];
ypts=-1/m*xpts;
ha=arrow([xpts(1);ypts(1)],[xpts(2);ypts(2)]);
set(ha,'LineWidth',4);


hcb=colorbar;
set(hcb,'ytick',[]);
saveas(gethf(ftrue),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/batchcontours.png');


%**************************************************************************
%make a 1-d plot
%**************************************************************************
f1d=FigObj('width',fwidth,'height',fheight,'fontsize',fsize,'name',true');
f1d.a=AxesObj('xlabel',xlbl,'ylabel',ylbl);

%contours will be lines with slope -1
%value of the contours will be a quadratic function of the 
%intercept



%b=[0:5];
b=[0:.01:1];
fb=-.5*(b-.5).^2+1;


hp=plot(b,fb,'b-','LineWidth',4);

set(gca,'ytick',[]);
set(gca,'xtick',[]);

saveas(gethf(f1d),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/batch1d.png');
