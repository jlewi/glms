%Explanation: This script plots the efficiency measured as the ratio of the
%asymptotic variance for the iid to infomax design as a function of the
%dimensionality.
%
%for my proposal only plot it in direction of theta

clear all;
setpathvars;


%the asymptotic variances depend on magnitude of theta and magnitude of
%stimuli
thetamag=1;
magcon=1;

%dimensionality to compute ratios for. 
dims=[20:10:100 200:100:1000];

%compute the efficiency
for dind=1:length(dims)
   %compute asymptotic covariance for infomax
   [imax(dind)]=compimaxasymvar(dims(dind),thetamag, magcon);
   %compute iid asymptotic variance   
   [iidvar(dind)]=compiidasymvar(dims(dind),thetamag, magcon)
   
   %compute the ratios
   %%*************************************************************************
    %compute the ratio of the variance in the iid and infomax case
    r.parallel(dind)=iidvar(dind).asymevals(1)/imax(dind).asymevals(1);
    r.orthog(dind)=iidvar(dind).asymevals(2)/imax(dind).asymevals(2);
    
    %compute the ratio of the determinants
    r.det(dind)=sum(log10(iidvar(dind).asymevals))-sum(log10(imax(dind).asymevals));
end

%******************************************************************
%%
%plot it
clear lstyle

lstyle(1).color=[0 0 0];
lstyle(1).linestyle='-';
lstyle(1).linewidth=3;
lstyle(1).marker='.';


lstyle(2).color=[0 0 0];
lstyle(2).linestyle='--';
lstyle(2).linewidth=3;
lstyle(2).marker='none';

feff=FigObj('name','efficiency','width',6,'height',2.5,'fontsize',18);
feff.a=AxesObj('xlabel','$\dim(\vec{\theta})$','ylabel','$\frac{\sigma^2_{iid}}{\sigma^2_{info}}$');
hold on;
%parallel
hp=plot(dims,r.parallel,'k.')
feff.a=addplot(feff.a,'hp',hp,'pstyle',lstyle(1));


feff=lblgraph(feff)
hx=getxlabel(feff.a)
set(hx.h,'interpreter','latex')
feff=sizesubplots(feff,[],[],[],[.04 .08, 0 0]);

hy=getylabel(feff.a)
set(hy.h,'interpreter','latex')

%saveas(gcf,'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/asymefficiency.eps','epsc2');


% fdet=FigObj('name','Ratio of determinants','width',3,'height',3);
% fdet.a=AxesObj('xlabel','$\dim(\vec{\theta})$','ylabel','log(Ratio)');
% hp=plot(dims,r.det,'k.')