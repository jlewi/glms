%4-18-2007
%
%Plot the observed fisher information with respect to epsilon as a function
%of r, glmproj
%
%I want to see how well this function can be fit by a polynomial in r and
%glmproj

%***********************************************************
%don't use the canonical poisson b\c jobs its independent of r
%***********************************************************
alpha=2;
glm=GLMModel('poisson',@(glmproj)(nonlin_power(glmproj,alpha)));

r=[0:.5:100]';
glmproj=[1:.25:10];
j=jobs(glm,r,glmproj);
%test
%j=r.^2*glmproj;

fobj.hf=figure;
fobj.xlabel='\epsilon';
fobj.ylabel='r';
fobj.name='observed fisher info';
fobj.title=fobj.name;
contourf(r,glmproj,j');
lblgraph(fobj);
colorbar;


%fit a polynomial to the function
degree=5;
rs=RSMutualInfo('dotmu',r,'sigmasq',glmproj,'imutual',j,'degree',degree);
fmi=miimg(rs,r,glmproj);
fmi.xlabel='\epsilon';
fmi.ylabel='r';
lblgraph(fmi);


break

%test data
x=[1:10];
y=[21:30];

jxy=ones(length(y),1)*x+y'*ones(1,length(x));

rsxy=RSMutualInfo('dotmu',y,'sigmasq',x,'imutual',jxy,'degree',degree);
miimg(rsxy,y,x); colorbar
figure;
contourf(y,x,jxy);
colorbar;