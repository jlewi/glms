%1-07-07
% Numerically compute the 2-d surface (muepsilon,sigma) over which 
%we do the optimization
%can use this for non-poisson and non-canonical responses
%
%1-12-07
%modified it so it can be called as script or function
function [fobj]=objsurf(varargin)

if (nargin==0)
clear all
%close all;
setpaths;

%******************************************
%file to save results to 
datetime=clock;
RDIR=fullfile(RESULTSDIR,'numint', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));
ropts.fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_poissobjsurf.mat',datetime(2),datetime(3)));
ropts.savedata=0;
else
    ropts.savedata=0;
end

%*******************************************************
%default values
%*******************************************************
%muglm=[-1.4142:.25:1.4142];
muglm=[-10:1:10];
sigmasq=[1:1:40];

%*******************************************************
%overwrite any defaults passed in
%*******************************************************
for j=1:2:nargin
    eval(sprintf('%s=varargin{j+1};',varargin{j}))
end
%*******************************************
%set up the numerical integration
%*******************************************
glm=GLMModel('poisson',@glm1dlog);
opts.confint=.999;               %how much confidence for integrating the inner expectation
opts.inttol=10^-6;        % tolerance for numerical integration
%muglm=[-100 -50 -10 -5 0 5 10 50 100];
%sigmasq=10.^[-3:.5:0];
%sigmasq=[.1 1 10 20 30 40];

lobjsurf=zeros(length(muglm),length(sigmasq));
for j=1:length(muglm)
    for k=1:length(sigmasq)
        fprintf('mu= %g\t sigmasq=%g \n',muglm(j),sigmasq(k));
        [lobjsurf(j,k)]=expdetint(muglm(j),sigmasq(k),glm,opts);
    end
end

%********************************************************
%Post Process
%********************************************************
%save the file
vtosave.lobjsurf='';
vtosave.muglm='';
vtosave.sigmasq='';
vtosave.opts='';


fobj.hf=figure;
fobj.xlabel='\mu_\epsilon';
fobj.ylabel='\sigma^2';
fobj.name='Numerical Comp. of Obj. Func';
fobj.title=fobj.name;
%contourf(muglm,sigmasq,log10(lobjsurf'));
contourf(muglm,sigmasq,lobjsurf');
lblgraph(fobj);
colorbar;

if (ropts.savedata~=0)
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end



%saveas(fobj.hf,'~/cvs_ece/allresults/figs/01_07_numlogobj_001.png')
%saveas(fapp.hf,'~/cvs_ece/allresults/figs/01_04_numpoissapp_002.png')