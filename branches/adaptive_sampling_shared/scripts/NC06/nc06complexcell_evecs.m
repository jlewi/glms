%4-29-2007
%
% Create an image rows showing the principal eigenvectors
%   of an svd of the estimate on different trials
%
% each row shows a different principal eigenvector
%
% Last column displays the true receptive field



dsets=[];

%each entry describes the dataset for a different a row
dsets(1).fname=fullfile(RESULTSDIR,'poolbased','05_24','05_24_poissexp_004.mat');
dsets(1).simvar='simmax';
dsets(1).lbl=sprintf('info. max \n heuristic');

dind=length(dsets)+1;
dsets(dind).fname=fullfile(RESULTSDIR,'poolbased','05_24','05_24_poissexp_004.mat');
dsets(dind).simvar='simunif';
dsets(dind).lbl=sprintf('info. max \n uniform');

dind=length(dsets)+1;
dsets(dind).fname=fullfile(RESULTSDIR,'poolbased','05_24','05_24_poissexp_004.mat');
dsets(dind).simvar='simrand';
dsets(dind).lbl='i.i.d.';



gparam.trials=[200:200:1000];    %trials on which to plot the principal components

%specify the order in which to plot the principal components
%the point of this is to aline them with the true
%initialize the defaults
%for j=1:lendgth(dsets)
%    dsets(1).pind=[1;2]*[ones(1,length(gparam.trials))];

%**************************************************************************
%compute the svd
%**************************************************************************



for dind=1:length(dsets)
    fprintf('Dataset dind=%d \n',dind);
    fsv.a{dind}.ha=subplot(1,length(dsets),dind);

    %load the data and compute
    %the singular values
    [pdata, sim, mp, sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);

    if ~isequal(mp.ninpfunc,@projinpquad)
        error('Script only works for quadratic nonlinearities');
    end

    thetalength=length(gettheta(sim.observer));
    dsets(dind).sv=zeros(thetalength.^.5,length(gparam.trials));

    dsets(dind).u=cell(1,length(gparam.trials));
    %    dsets(dind).v=cell(1,length(gparam.trials));
    %on each trial we reshape the map to be a matrix and find its singualr
    %values
    for j=1:length(gparam.trials)
        t=gparam.trials(j);
        if (mod(j,10)==0)
            fprintf('Trial %d \n', gparam.trials(t));
        end
        map=reshape(pdata.m(:,t+1),thetalength^.5,thetalength^.5);
        [dsets(dind).u{j} sv]= svd(map);
        dsets(dind).sv(:,j)=diag(sv);
    end
end

%%

%***************************************************
%parameters for the graph
%***************************************************
%%


gparam.nprinc=2;                %how many of the principal components to plot
fsvd=[];
fsvd.hf=figure;
fsvd.a={};
fsvd.name='SVD';
fsvd.FontSize=12;         %force all fonts to this size
hold on;

%how many columns and rows of axes we will need
gparam.ncols=length(gparam.trials)+1;
gparam.nrows=(length(dsets))*gparam.nprinc;
gparam.fontsize=12;
gparam.width=6.25;          %width of the image in inches
gparam.height=5;            %height of the image in inches
gparam.clim=[-.05 .05];       %limits for color mapping
colormap(gray);

%an inline function which automatically computes the linear index
%of a subplot from its row and column
%subplot selects in row order so to use  sub2ind we transpose rows and
%columns
spind=@(row,col)(sub2ind([gparam.ncols gparam.nrows],col,row));
%%
%**************************************************************************
%Make plots of mean
%************************************************************************
%loop over the data sets
for dind=1:length(dsets)
    [pdata, sim, mp, sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
    for tind=1:length(gparam.trials)
        trial=gparam.trials(tind);
        for pind=1:gparam.nprinc
            %convert indexes to a linear index
            aind=spind((length(dsets))*(pind-1)+dind,tind);
            fsvd.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);

            %check mean exists
            if (trial>sim.niter)
                fprintf('Data set %s: trial %d exceeds number of iterations \n;',dsets(dind).lbl,trial);
                break;   %stop plotting for this dataset
            else
                if pind==1
                     k=sim.extra.mparam.k1;
                else
                    k=sim.extra.mparam.k2;
                end
                u=dsets(dind).u{tind};
                %determine which principal component is actually most
                %correlated with the true
                %[mc, pi]=max([k'*u(:,1),k'*u(:,2)]);

                mparam=sim.extra.mparam;
                imagesc(dsets(dind).sv(pind,tind)*reshape(u(:,pind),mparam.gheight,mparam.gwidth));
            end
            %turn off tick marks
            set(fsvd.a{aind}.ha,'xtick',[]);
            set(fsvd.a{aind}.ha,'ytick',[]);
        end
    end %end loop over trials

end %end loop over datasets

%plot the true components
%there are only 2
hc=[];
for dind=1:length(dsets)
    for pind=1:2
        aind=spind((length(dsets))*(pind-1)+dind,length(gparam.trials)+1);
        fsvd.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);
        if pind==1
            k=sim.extra.mparam.k1;
        else
            k=sim.extra.mparam.k2;
        end
        imagesc(reshape(k,mparam.gheight,mparam.gwidth));
        %turn off tick marks
        set(fsvd.a{aind}.ha,'xtick',[]);
        set(fsvd.a{aind}.ha,'ytick',[]);
        hc(end+1)=colorbar;
    end
end

%**************************************************************************
%Set axes labels and titles
%*************************************************************************
%add a title to each graph in the top row
for col=1:(gparam.ncols-1)
    aind=spind(1,col);
    fsvd.a{aind}.title=sprintf('trial %d',gparam.trials(col));
end

aind=spind(1,gparam.ncols);
fsvd.a{aind}.title=sprintf('true');

%add data set label to each row
for dind=1:length(dsets)
    for pind=1:gparam.nprinc
        aind=spind(length(dsets)*(pind-1)+dind,1);
        fsvd.a{aind}.ylabel=sprintf('%s \n %d',dsets(dind).lbl,pind);
    end
end

%label the graphs
lblgraph(fsvd);


%**************************************************************************
%Position the figure
%**************************************************************************
%space a structure to describe spacing settings
%this should be expressed as %
space.hspace=.025; %horizontal spacing
space.vspace=.05; %vertical spacing
space.cbwidth=.025;  %width of colorbar

%set the height and width of the figure
set(fsvd.hf,'units','inches');
fpos=get(fsvd.hf,'paperposition');
fpos(3)=gparam.width;
fpos(4)=gparam.height;
set(fsvd.hf,'paperposition',fpos);    %controls the position when we printout
%control its size on screen as well
fpos=get(fsvd.hf,'position');
fpos(3)=gparam.width;
fpos(4)=gparam.height;
set(fsvd.hf,'position',fpos);

%*********************************************************************
%determine how much of a border we need to leave on left bottom right top
%so that we don't cut off figure labels
ha=length(fsvd.a);
for i=1:length(fsvd.a)
    ha(i)=fsvd.a{i}.ha;
end
ha=[ha hc];

%ha has handles to all the axes
tinset=get(ha,'tightinset');
tinset=cell2mat(tinset);

border.left=max(tinset(:,1));
border.bottom=max(tinset(:,2));
border.right=max(tinset(:,3));
border.top=max(tinset(:,4));

%determine the size for each of the axes
asize.width=(1-border.left-border.right-space.hspace*gparam.ncols-space.cbwidth)/(gparam.ncols);
asize.height=(1-border.top-border.bottom-space.vspace*(gparam.nrows-1))/(gparam.nrows);

%loop through the axes and poisition them
for row=1:gparam.nrows
    for col=1:gparam.ncols
        aind=spind(row,col);
        pos=get(fsvd.a{aind}.ha,'position');
        pos(1)=border.left+(col-1)*(asize.width+space.hspace);
        pos(2)=border.bottom+(gparam.nrows-row)*(asize.height+space.vspace);
        pos(3)=asize.width;
        pos(4)=asize.height;
        set(fsvd.a{aind}.ha,'position',pos);
    end
end

%size the color bars
for j=1:length(hc)
    pos=get(hc(j),'position');
    pos(1)=border.left+(gparam.ncols)*(asize.width+space.hspace);
    pos(2)=border.bottom+(gparam.nrows-j)*(asize.height+space.vspace);
    pos(3)=space.cbwidth;
    pos(4)=asize.height;
    set(hc(j),'position',pos);
end
refresh;

%previewfig(fsvd.hf,'bounds','tight','FontMode','Fixed','FontSize',fsvd.FontSize,'Width',gparam.width,'Height',gparam.height)
%fsvd.outfile='~/cvs_ece/writeup/NC06/complexcell_evecs.eps';
%exportfig(fsvd.hf,fsvd.outfile,'bounds','tight','FontMode','Fixed','FontSize',fsvd.FontSize,'Width',gparam.width,'Height',gparam.height)