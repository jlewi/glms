%7-16-2007
%   Plot it on a log scale. We do this manually because we compute the
%   filters on a log increment
%5-30-2007
%
%Make an image of the best recovered auditory filter on each trial
%To do this we reshape the mean as a matrix and get the first two principal
%eigen vectors. We then project the true filters onto these vectors
%This represents our MSE guess of the best filters
%******************************************

%60 d gammatone
% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_simmax.mat');
% dsets(end).lbl='info. max. heuristic';
% dsets(end).simvar='simmax';
% %dsets(end).maxtrial=10000; %max trial to plot
%
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001.mat');
% dsets(end).lbl='info. max. i.i.d.';
% dsets(end).simvar='simunif';
% %dsets(end).maxtrial=10000;
%
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_simrand.mat');
% dsets(end).lbl='i.i.d.';
% dsets(end).simvar='simrand';

dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','071030','audnonlin_001.mat');
% dsets(end).lbl='info. max. heuristic';
% dsets(end).simvar='max';
% %dsets(end).maxtrial=10000; %max trial to plot
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','071029','audnonlin_001.mat');
% dsets(end).lbl='tones';
% dsets(end).simvar='tones';
% dsets(end).maxtrial=10000; %max trial to plot

ind=16;
 dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
 dsets(end).lbl=sprintf('info. max. \n $\\hat{\\mathcal{X}}_{heur,t+1}$') ;
 dsets(end).simvar='max';
dsets(end).maxtrial=10000; %max trial to plot

ind=ind+1;
%  dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
%  dsets(end).lbl='max: heur muorig';
%  dsets(end).simvar='max';
% dsets(end).maxtrial=10000; %max trial to plot

ind=ind+1;
 dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
 dsets(end).lbl='tones';
 dsets(end).simvar='tones';
dsets(end).maxtrial=10000; %max trial to plot


ind=ind+1;
 dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
 dsets(end).lbl=sprintf('info. max. \n $\\hat{\\mathcal{X}}_{iid,t+1}$') ;
 dsets(end).simvar='unif';
dsets(end).maxtrial=10000; %max trial to plot

ind=ind+1;
 dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
 dsets(end).lbl='i.i.d.';
 dsets(end).simvar='rand';
dsets(end).maxtrial=10000; %max trial to plot



gparam.maxtrial=10000;

%********************************************************************
%compute the subspace angle
for dind=1:length(dsets)
    
    simdata=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);
    extra=getextra(simdata);
    mparam=extra.mparam;
    kl=mparam.kl;
    k1=mparam.k1';
    k2=mparam.k2';
    ktrue=[k1 k2];
    if (size(k1,2)>1)
        k1=k1';
    end
    if (size(k2,2)>1)
        k2=k2';
    end

    dsets(dind).niter=getniter(simdata);

    %how many rows do we want to have in our image
    nrows=200;
    %we need to create a mapping from rows to trials
    %we want this mapping to follow a log scale.
    slope=log10(min(gparam.maxtrial,dsets(dind).niter))/(nrows-1);

    trials=10.^(slope*((1:nrows)-1));

    %take ceiling so that trials is valid
    trials=floor(trials);
    %compute the data on a log scale
    %how many points to plot in between each power
    %i.e logres 20 means we have 20 pts between successive powers
    %    logres=20;
    %    %mpower is the largest power of 10 for this data et
    %    lscale.mpower=floor(log10(dsets(dind).niter));
    %    %lscale .npts is how many at pts we have for the last power
    %    lscale.nlast=floor(mod(dsets(dind).niter,10^lscale.mpower)/(10^(lscale.mpower-1)/logres);
    %    lscale.npts=(lscale.mpower-1)*logres+lscale.nlast;
    %     dsets(dind).k1est=zeros(kl,lscale.npts);
    %     dsets(dind).k2est=zeros(kl,lscale.npts);
    % %   dsets(dind).k1est=zeros(kl,ceil(simdata.niter/gparam.subsample));
    %  %  dsets(dind).k2est=zeros(kl,ceil(simdata.niter/gparam.subsample));

    ninpobj=getninpobj(getmobj(simdata));
    for ind=1:length(trials)
        %trial=1:gparam.subsample:simdata.niter;
        % trial=10^power+j*10^(power-1);
        %ind=10*power+j;
        %       ind=floor(trial/gparam.subsample)+1;
        trial=trials(ind);
        [evecs, eigval]=svd(qmatquad(ninpobj,getpostm(simdata,trial)));
        %project the truth onto each eigenvector
        kproj=evecs(:,1:2)'*[k1 k2];
        dsets(dind).k1est(:,ind)=evecs(:,1:2)*kproj(:,1);
        dsets(dind).k2est(:,ind)=evecs(:,1:2)*kproj(:,2);
    end
    dsets(dind).trials=trials;
    dsets(dind).slope=slope;
end
%**************************************************************************
%plot the Recovered values
%*************************************************************
%%
gparam.nfilt=2;
gparam.nrows=gparam.nfilt*2;

gparam.ncols=length(dsets);
%an inline function which automatically computes the linear index
%of a subplot from its row and column
%subplot selects in row order so to use  sub2ind we transpose rows and
%columns
spind=@(row,col)(sub2ind([gparam.ncols gparam.nrows],col,row));
%%

ffilt=FigObj('width',6.3,'height',4,'name','Auditory Filters');
ffilt.a=AxesObj('nrows',gparam.nrows,'ncols',gparam.ncols);
 colormap(gray);

%*********************************************
%plot the estimated filters
for dind=1:length(dsets)
    for nf=1:gparam.nfilt
        %plot the filter
        setfocus(ffilt.a,2*nf-1,dind);
       
        %        aind=spind(2*nf-1,dind);
        tlast=min(gparam.maxtrial,dsets(dind).niter);
        %  ffilt.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);
        fname=sprintf('k%dest',nf);

        %indlast=ceil(tlast/gparam.subsample);
        imagesc(1:kl,1:nrows,dsets(dind).(fname)');
        %turn off xtick and ytick
        set(gca,'xtick',[]);
        set(gca,'ytick',[]);
        set(gca,'ydir','reverse');
        xlim([1 kl]);
        ylim([1 nrows]);
        %plot the truth
        %plot the filter
        %        aind=spind(2*nf,dind);
        %   ffilt.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);

        setfocus(ffilt.a,2*nf,dind);
        fname=sprintf('k%dest',nf);
        imagesc(ktrue(:,nf)');
        xlim([1 kl]);
        set(gca,'xtick',[]);
        set(gca,'ytick',[]);

    end
end

%make the color limits the same
cl=get(ffilt.a,'clim');
cl=cell2mat(cl);
cl=[min(cl(:,1)) max(cl(:,2))];
set(ffilt.a,'clim',cl);

%****************************************************************
%label graphs
%*******************************************************************
%left column
for nf=1:gparam.nfilt
    aind=spind(2*nf-1,1);
   ffilt.a(2*nf-1,1)=ylabel(ffilt.a(2*nf-1,1),sprintf('   $\\vec{\\phi}^%d$ \ntrial',nf));   
    setfocus(ffilt.a,2*nf-1,1);
    %adjust the tick labels
    %set the tick marks
    ytlbls=10.^[1:floor(log10(gparam.maxtrial))];
    ytick=floor(log10(ytlbls)/dsets(1).slope+1);
    set(gca,'ytick',ytick);
    %ytick=get(ffilt.a{aind}.ha,'ytick');
    set(gca,'yticklabel',ytlbls);



    ffilt.a(2*nf,1)=ylabel(ffilt.a(2*nf,1),'true');

end

%bottom row
for dind=1:length(dsets)
    aind=spind(gparam.nrows,dind);

    setfocus(ffilt.a,gparam.nrows,dind);
%    ffilt.a(gparam.nrows,dind)=xlabel(ffilt.a(gparam.nrows,dind),sprintf('\\phi_i'));
    ffilt.a(gparam.nrows,dind)=xlabel(ffilt.a(gparam.nrows,dind),sprintf('i'));
   set(gca,'xtickmode','auto');
  
end

%top row
for dind=1:length(dsets)
    setfocus(ffilt.a,1,dind);
    ffilt.a(1,dind)=title(ffilt.a(1,dind),dsets(dind).lbl);
    
ht=gettitle(ffilt.a(1,dind));
set(ht.h,'interpreter','latex');
end

%add colorbars to the last column
%right column
hc=[];
for nf=1:gparam.nfilt

    setfocus(ffilt.a,2*nf-1,gparam.ncols);
    hc=colorbar;
    ffilt.a(2*nf-1,gparam.ncols)=sethc(    ffilt.a(2*nf-1,gparam.ncols),hc);
end

% 
hy=getylabel(ffilt.a(1,1));
set(hy.h,'interpreter','latex');

hy=getylabel(ffilt.a(3,1));
set(hy.h,'interpreter','latex');


ffilt=lblgraph(ffilt);


ffilt=sizesubplots(ffilt,[],.25*ones(1,4),[.4 .1 .4 .1]',[.03 0 0 .08]);
ffiltoutfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/auditory_filters_mod.eps';
%exportfig(ffilt.hf,ffilt.outfile,'bounds','tight','FontMode','Fixed','FontSize',ffilt.fontsize,'Width',gparam.width,'Height',gparam.height)
%saveas(gethf(ffilt),ffiltoutfile,'epsc2');
odata={ffilt,[{'label'},{'variable'},{'file'};{dsets.lbl}' {dsets.simvar}' {dsets.fname}'];};
onenotetable(odata,seqfname('~/svn_trunk/notes/nc06audfilts.xml'));
return;
