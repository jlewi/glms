%12-17-2007
%   updated - Use get/set methods to access the object data
%04-23-07
%
%make a plot of the asymptotic variance
dsets=[];
dind=1;

fname=fullfile(RESULTSDIR,'poolbased','04_23','04_23_poolstim_002.mat');

%=fullfile(RESULTSDIR,'poolbased','04_23','04_23_poolstim_002.mat');
%dsets(dind).simvar='simrand';
%dsets(dind).lbl='i.i.d';

%run the optimization
[imax,irand,cmat]=asymvarpool(fname);

%load the data
[pmax, simmax, mmax, srmax]=loadsim('simfile',fname,'simvar','simmax');
[prand, simrand, mrand, srrand]=loadsim('simfile',fname,'simvar','simrand');

imax.trials=1:getniter(simmax);
irand.trials=1:getniter(simrand);

%%
%************************************************************************
%Compute the empirical variance in the direction of each eigenvector
%   of the asymptotic variance matrix.
%**************************************************************************
theta=gettheta(getobserver(simmax));
theta=normmag(theta);

imaxemp.evals=zeros(getklength(mmax),getniter(simmax));
imaxemp.trials=zeros(1,getniter(simmax));
imaxemp.trials(:)=nan;
for j=1:getniter(simmax)
    if ~isempty(pmax.c{j+1})
        varemp=pmax.c{j+1}*imax.asymevecs;
        varemp=imax.asymevecs.*varemp;
        varemp=sum(varemp,1);
        imaxemp.evals(:,j)=varemp;
        imaxemp.trials(j)=j;
    end
end
ind=find(~isnan(imaxemp.trials));
imaxemp.trials=ind;
imaxemp.evals=imaxemp.evals(:,ind);
%imaxemp.thetavar=imaxemp.thetavar(ind);



irandemp.evals=zeros(getklength(mrand),getniter(simrand));
irandemp.trials=zeros(1,getniter(simmax));
irandemp.trials(:)=nan;
for j=1:getniter(simrand)
    if ~isempty(prand.c{j+1})
        varemp=prand.c{j+1}*irand.asymevecs;
        varemp=irand.asymevecs.*varemp;
        varemp=sum(varemp,1);
        irandemp.evals(:,j)=varemp;
        irandemp.trials(j)=j;
    end
end
ind=find(~isnan(irandemp.trials));
irandemp.trials=ind;
irandemp.evals=irandemp.evals(:,ind);
%irandemp.thetavar=irandemp.thetavar(ind);


%%
%***********************************************************************
%compute the M.S.E
%***********************************************************************
imaxemp.mse=zeros(1,getniter(simmax));
for t=1:getniter(simmax)
    me=(t*pmax.c{t+1}-imax.asymvar).^2;
    imaxemp.mse(t)=sum(me(:)).^.5;
end
irandemp.mse=zeros(1,getniter(simrand));
for t=1:getniter(simrand)
    me=(t*prand.c{t+1}-irand.asymvar).^2;
    irandemp.mse(t)=sum(me(:)).^.5;    
end

%*******************************************************
%plot it
%*******************************************************
%%
fcovar=[];
fcovar.hf=figure;
fcovar.name='Asymptotic Covariance MSE';
fcovar.fname='dircovar';
fcovar.fontsize=12;
fcovar.semilogx=0;
fcovar.semilog=1;



%*****************************************************
%Graph parameters
%********************************************************
%structure to specify size and witdth of image

gparam.nrows=1;
gparam.width=2.25;
gparam.height=2.25;
setfsize(fcovar.hf, gparam);

fcovar.a{1}.ha=gca;
fcovar.a{1}.ylabel='M.S.E.';
fcovar.a{1}.xlabel='trial';
fcovar.width=2.25;
fcovar.height=2.25;

pind=0;


hold on;

pind=1;
fcovar.a{1}.hp(pind)=plot(1:getniter(simrand),irandemp.mse);
fcovar.a{1}.lbls{pind}='i.i.d.';
setlstyle(fcovar.a{1}.hp(pind),getbwlstyle(2));

pind=pind+1;
fcovar.a{1}.hp(pind)=plot(1:getniter(simmax),imaxemp.mse);
setlstyle(fcovar.a{1}.hp(pind),getbwlstyle(1));
fcovar.a{1}.lbls{pind}='info. max.';



lblgraph(fcovar);
set(gca,'yscale','log');
xlim([0 max([getniter(simmax),getniter(simrand)])]);
ylim([10^-5 10]);
set(gca,'ytick',10.^[-5:1]);

%set(gca,'yticklabel',[-5:1]);
fcovar.outfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/pool_asymmse.eps';
%saveas(fcovar.hf,fcovar.outfile,'epsc2')
%previewfig(fcovar.hf,'bounds','tight','FontMode','Fixed','FontSize',fcovar.fontsize,'Width',gparam.width,'Height',gparam.height)
%fcovar.outfile='~/cvs_ece/writeup/NC06/poolasymvar.eps';

%exportfig(fcovar.hf,fcovar.outfile,'bounds','tight','FontMode','Fixed','FontSize',fcovar.fontsize,'Width',gparam.width,'Height',gparam.height)

