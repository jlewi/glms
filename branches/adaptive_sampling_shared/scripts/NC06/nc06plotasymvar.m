%04-23-07
%
%make a plot of the asymptotic variance
clear all;
setpathvars;
dsets=[];
dind=1;

fname=fullfile(RESULTSDIR,'poolbased','04_23','04_23_poolstim_002.mat');

%=fullfile(RESULTSDIR,'poolbased','04_23','04_23_poolstim_002.mat');
%dsets(dind).simvar='simrand';
%dsets(dind).lbl='i.i.d';

%run the optimization
[imax,irand,cmat]=asymvarpool(fname);
imax.asymeig=EigObj('evecs',imax.asymevecs,'eigd',imax.asymevals);
irand.asymeig=EigObj('evecs',irand.asymevecs,'eigd',irand.asymevals);

%load the data
[pmax, simmax, mmax, srmax]=loadsim('simfile',fname,'simvar','simmax');
[prand, simrand, mrand, srrand]=loadsim('simfile',fname,'simvar','simrand');

imax.trials=1:getniter(simmax);
irand.trials=1:getniter(simrand);

%%
%************************************************************************
%Compute the empirical variance in the direction of each eigenvector
%   of the asymptotic variance matrix.
%**************************************************************************
theta=gettheta(getobserver(simmax));
theta=normmag(theta);

imaxemp.evals=zeros(getklength(mmax),getniter(simmax));
imaxemp.trials=zeros(1,getniter(simmax));
imaxemp.trials(:)=nan;
for j=1:getniter(simmax)
    if ~isempty(pmax.c{j+1})
        varemp=pmax.c{j+1}*imax.asymeig.evecs;
        varemp=imax.asymeig.evecs.*varemp;
        varemp=sum(varemp,1);
        imaxemp.evals(:,j)=varemp;
        imaxemp.trials(j)=j;
    end
end
ind=find(~isnan(imaxemp.trials));
imaxemp.trials=ind;
imaxemp.evals=imaxemp.evals(:,ind);
%imaxemp.thetavar=imaxemp.thetavar(ind);



irandemp.evals=zeros(getklength(mrand),getniter(simrand));
irandemp.trials=zeros(1,getniter(simmax));
irandemp.trials(:)=nan;
for j=1:getniter(simrand)
    if ~isempty(prand.c{j+1})
        varemp=prand.c{j+1}*irand.asymeig.evecs;
        varemp=irand.asymeig.evecs.*varemp;
        varemp=sum(varemp,1);
        irandemp.evals(:,j)=varemp;
        irandemp.trials(j)=j;
    end
end
ind=find(~isnan(irandemp.trials));
irandemp.trials=ind;
irandemp.evals=irandemp.evals(:,ind);
%irandemp.thetavar=irandemp.thetavar(ind);


%%
%compute the ratio of the variance in the iid and infomax case
rmin=irand.asymevals(end)/imax.asymevals(end);
rmax=irand.asymevals(1)/imax.asymevals(1);
fprintf('Ratio minimum (iid/imax): \t %d \n',rmin);
fprintf('Ratio maxmimum (iid/imax): \t %d \n',rmax);
%*******************************************************
%plot it
%*******************************************************
%%
fcovar=[];
fcovar.hf=figure;
fcovar.name='Asymptotic Covariance';
fcovar.fname='dircovar';
fcovar.fontsize=12;
fcovar.semilogx=0;
fcovar.semilog=1;
fcovar.width=5;
fcovar.height=3.5;


%*****************************************************
%Graph parameters
%********************************************************
%structure to specify size and witdth of image
gparam.nplot=[getklength(mmax) 1];     %which eigenvalues to plot
gparam.nrows=length(gparam.nplot);
gparam.ncols=1;
gparam.ylim=[10^-7 10];
% for j=1:gparam.ncols
%     fcovar.a{j}.hp=[];
%     fcovar.a{j}.ha=[];
%     fcovar.a{j}.lbls={};
% end



%an inline function which automatically computes the linear index
%of a subplot from its row and column
%subplot selects in row order so to use  sub2ind we transpose rows and
%columns
spind=@(row,col)(sub2ind([gparam.ncols gparam.nrows],col,row));
%**************************************************************************
%two panels
%1. panel components parallel to mean
%2. other panel orthogonal to mean


%*****************************************************************
%loop over the eigenvalues
%go in reverse order so smallest eigenvalue is first
for sind=1:length(gparam.nplot)
    eind=gparam.nplot(sind);
    fcovar.a{sind}.ha=subplot(gparam.nrows,gparam.ncols,sind);
    hold on;
    pind=0;


    %************************************************
    %iid:
    %empirical
    %****************************************************
    %pind=length(fcovar.a{sind}.hp)+1;
    pind=1;    
    fcovar.a{sind}.hp(pind)=plot(irandemp.trials,irandemp.evals(eind,:));
    setlstyle(fcovar.a{sind}.hp(pind),getbwlstyle(2));
    fcovar.a{sind}.lbls{pind}='iid. emp.';

    %************************************************
    %I.I.D:
    %Predicted
    %****************************************************
    pind=length(fcovar.a{sind}.hp)+1;
    fcovar.a{sind}.hp(pind)=plot(irand.trials,irand.asymeig.eigd(eind)*(1./(irand.trials)));
    setlstyle(fcovar.a{sind}.hp(pind),getbwlstyle(4));
    fcovar.a{sind}.lbls{pind}='i.i.d. pred.';



    %************************************************
    %imax:
    %empirical
    %****************************************************
    hold on;
    pind=length(fcovar.a{sind}.hp)+1;
    fcovar.a{sind}.hp(pind)=plot(imaxemp.trials,imaxemp.evals(eind,:));
    setlstyle(fcovar.a{sind}.hp(pind),getbwlstyle(1));
    fcovar.a{sind}.lbls{pind}='info. max. emp.';

    %************************************************
    %imax:
    %Predicted
    %****************************************************
    pind=length(fcovar.a{sind}.hp)+1;
    fcovar.a{sind}.hp(pind)=plot(imax.trials,imax.asymeig.eigd(eind)*(1./imax.trials));
    setlstyle(fcovar.a{sind}.hp(pind),getbwlstyle(3));
    fcovar.a{sind}.lbls{pind}='info. max. pred.';





end


%************************************************************
%adjust the axes
fcovar.a{1}.ylabel='variance';
fcovar.a{end}.title='max';
fcovar.a{1}.title='min';
ha=[];
for a=1:gparam.nrows
    %**************************************************************************
    %set the labels
    %**************************************************************************
    fcovar.a{a}.semilogx=1;
    fcovar.a{a}.semilog=1;
    fcovar.a{a}.xlabel='trial';
    fcovar.a{a}.ylabel='variance';  
    ha=[ha fcovar.a{a}.ha];

    %************************
    %set xtick labels in decimal format
    %so that we don't just print the exponents
    %set(fcovar.a{a}.ha,'xticklabel',get(fcovar.a{a}.ha,'xtick'));
end

%turn off xticks on first plot
set(fcovar.a{1}.ha,'xtick',[]);

%turn off labels. 
for a=1:gparam.ncols
    %**************************************************************************
    %set the labels
    %**************************************************************************
    if isfield(fcovar.a{a},'lbls')
    fcovar.a{a}=rmfield(fcovar.a{a},'lbls');
    end
end

for a=2:gparam.ncols
    %**************************************************************************
    %set the labels
    %**************************************************************************  
    set(fcovar.a{a}.ha,'ytick',[]);
end

%*************************
%adjust the ytick labels to decimal format
%set(fcovar.a{1}.ha,'yticklabel',get(fcovar.a{1}.ha,'ytick'));


fcovar=lblgraph(fcovar);
set(fcovar.a{2}.hlgn,'fontsize',10);
%adjust the axes limits of each graph
%yl=get(ha,'ylim');
%yl=cell2mat(yl);
%yl=[min(yl(:,1)) max(yl(:,2))];

set(ha,'xlim',[0 max(getniter(simmax),getniter(simrand))]);
set(ha,'ylim',gparam.ylim);
set(ha,'ytick',[10^-5 10^0]);


%********************************************************************
%adjust the width of the subplots so that we use all available space
%********************************************************************
%leave some extra horizontal space for the legend

hspace=.05; %horizontal space between graphs
vspace=0; %vertical space
tightinset=get([fcovar.a{1}.ha,fcovar.a{2}.ha],'tightinset');
tightinset=cell2mat(tightinset);

%compute width of legend
plgn=get(fcovar.a{2}.hlgn,'position');
%wlgn is a heuristic for how much space we need to fit legend on the side
wlgn=plgn(3);

ncols=gparam.ncols;
nrows=gparam.nrows;
if gparam.nrows==2

    %plot figures below each other

    width=(1-tightinset(1,1)-tightinset(1,3)-hspace*(ncols-1)-wlgn)/ncols;
    height=(1-sum(tightinset(:,2))-sum(tightinset(:,4))-vspace*(nrows-1))/nrows;

    pos=get(fcovar.a{1}.ha,'position');
    pos(1)=tightinset(1);
    pos(2)=tightinset(2,2)+tightinset(2,4)+height+vspace+tightinset(1,2);
    pos(3)=width;
    pos(4)=height;
    set(fcovar.a{1}.ha,'position',pos);

    %this is figure on bottom
    pos2=get(fcovar.a{2}.ha,'position');
    pos2(1)=tightinset(1);
    pos2(2)=tightinset(2,2);
    pos2(3)=width;
    pos2(4)=height;
    set(fcovar.a{2}.ha,'position',pos2);
else
    %plot figures side by side
    %width of axes
    width=(1-tightinset(1,1)-tightinset(1,3)-hspace*(ncols-1)-wlgn)/ncols;

    height=(1-max(tightinset(:,2))-max(tightinset(:,4))-vspace*(nrows-1))/nrows;

    pos=get(fcovar.a{1}.ha,'position');
    pos(1)=tightinset(1);
    pos(2)=max(tightinset(:,2));
    pos(3)=width;
    pos(4)=height;
    set(fcovar.a{1}.ha,'position',pos);

    pos2=get(fcovar.a{2}.ha,'position');
    pos2(1)=pos(1)+width+hspace;
    pos2(2)=max(tightinset(:,2));
    pos2(3)=width;
    pos2(4)=height;
    set(fcovar.a{2}.ha,'position',pos2);
end

%previewfig(fcovar.hf,'bounds','tight','FontMode','Fixed','FontSize',fcovar.fontsize,'Width',gparam.width,'Height',gparam.height)
fcovar.outfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/poolasymvar.eps';
%saveas(fcovar.hf,fcovar.outfile,'epsc2')
%exportfig(fcovar.hf,fcovar.outfile,'bounds','tight','FontMode','Fixed','FontSize',fcovar.fontsize,'Width',gparam.width,'Height',gparam.height)

