%Explanation: This script plots the efficiency measured as the ratio of the
%asymptotic variance for the iid to infomax design as a function of the
%dimensionality.

clear all;
setpathvars;


%the asymptotic variances depend on magnitude of theta and magnitude of
%stimuli

magcon=1;

%dimensionality to compute ratios for. 
dims=1000;
mind=1;
thetamags=[.1 .5 1:25:100];

%compute the efficiency
for mind=1:length(thetamags)
   %compute asymptotic covariance for infomax
   [imax(mind)]=compimaxasymvar(dims,thetamags(mind), magcon);
   %compute iid asymptotic variance   
   [iidvar(mind)]=compiidasymvar(dims,thetamags(mind), magcon);
   
   %compute the ratios
   %%*************************************************************************
    %compute the ratio of the variance in the iid and infomax case
    r.parallel(mind)=iidvar(mind).asymevals(1)/imax(mind).asymevals(1);
    r.orthog(mind)=iidvar(mind).asymevals(2)/imax(mind).asymevals(2);
    
    %compute the ratio of the determinants
    r.det(mind)=sum(log10(iidvar(mind).asymevals))-sum(log10(imax(mind).asymevals));
end

%******************************************************************
%%
%plot it
clear lstyle

lstyle(1).color=[0 0 0];
lstyle(1).linestyle='-';
lstyle(1).linewidth=3;
lstyle(1).marker='.';


lstyle(2).color=[0 0 0];
lstyle(2).linestyle='--';
lstyle(2).linewidth=3;
lstyle(2).marker='none';

feff=FigObj('name','efficiency','width',3,'height',3);
feff.a=AxesObj('xlabel','$||\vec{\theta}||_2$','ylabel','$\frac{\sigma^2_{iid}}{\sigma^2_{info}}$');
hold on;
%parallel
hp=plot(thetamags,r.parallel,'k.')
feff.a=addplot(feff.a,'hp',hp,'pstyle',lstyle(1),'lbl','$\vec{\omega}_{\parallel}$');

%fit a line
% [pcoeff]=polyfit(dims,r.parallel,1)
% x=[0 dims(end)];
% y=pcoeff(1)*x+pcoeff(2);
% plot(x,y,'k-');

%orthogonal
hp=plot(thetamags,r.orthog)
feff.a=addplot(feff.a,'hp',hp,'pstyle',lstyle(2),'lbl','$\vec{\omega}_{\perp}$');
set(gca,'yscale','log');
feff=lblgraph(feff)
hx=getxlabel(feff.a)
set(hx.h,'interpreter','latex')
hy=getylabel(feff.a)
set(hy.h,'interpreter','latex')

hl=gethlgnd(feff.a);
set(hl,'interpreter','latex');
%add some extra border
feff=sizesubplots(feff,[],[],[],[.05 .07, 0 0]);

%saveas(gcf,'~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/asymefficiencyvstheta.eps','epsc2');


% fdet=FigObj('name','Ratio of determinants','width',3,'height',3);
% fdet.a=AxesObj('xlabel','$\dim(\vec{\theta})$','ylabel','log(Ratio)');
% hp=plot(dims,r.det,'k.')