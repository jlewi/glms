%10_30_2007
%Make plots of f(glmproj) for the misspecified models.
clear all
setpathvars
%simfiles{1}=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_001.mat');

dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisspowercanon_001_newver.mat');
dsets(end).simvar='max';
dsets(end).obname='powercanon_rho.eps';
dsets(end+1).fname=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisslogcanon_001_newver.mat');
dsets(end).simvar='rand';
dsets(end).obname='logcanon_rho.eps';
gparam.width=3;
gparam.height=3;

outdir='/home/jlewi/svn_trunk/adaptive_sampling/writeup/NC06/images_eps';

lstyle(1).linewidth=4;
lstyle(1).lcolor=[.75 .75 .75];
lstyle(1).lstyle='--'

lstyle(2).linewidth=6;
lstyle(2).lcolor=[0 0 0];
lstyle(2).lstyle='-';

for dind=1:length(dsets)





  ff(dind)=FigObj('name','Rho','width',gparam.width,'height',gparam.height);
  ff(dind).a=AxesObj('xlabel','\rho_{t+1}','ylabel','f(\rho_{t+1})');
  [simobj]=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);
 
   %name for output file
%    [pathstr,sfname,EXT,VERSN] = fileparts(dsets(dind).fname);
%    outfile=seqfname(fullfile(pathstr,sprintf('%s_%s.eps',outbname,simparam.trialindex)));
    outfile{dind}=fullfile(outdir,dsets(dind).obname);

    
    theta=gettheta(getobserver(simobj));
hold on;
    massume=getmobj(simobj);
    mmag=getmag(massume);
    
    %get the range of rhow allowed under the experiment
    
    rho=[-mmag*(theta'*theta)^.5:.1:mmag*(theta'*theta)^.5];
    
    
    
    aglm=getglm(massume);
    nfun=getfglmmu(aglm);
    hp=plot(rho,nfun(rho));
    ff(dind).a=addplot(ff(dind).a,'hp',hp,'lbl','assumed','lstyle',lstyle(2));   
    
    %get the true glm
    mtrue=getmodel(getobserver(simobj));
    tglm=getglm(mtrue);
    nfun=getfglmmu(tglm);
    hp=plot(rho,nfun(rho));
    ff(dind).a=addplot(ff(dind).a,'hp',hp,'lbl','true','lstyle',lstyle(1));
    
    %assumed model
   
    %get the true glm
    
    ff(dind)=lblgraph(ff(dind));
    set(gca,'yscale','log');
end

%dind=1;saveas(gethf(ff(dind)),outfile{dind},'epsc2');
%dind=2;saveas(gethf(ff(dind)),outfile{dind},'epsc2');

