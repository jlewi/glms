%5-17-2007
%   make a plot of the distance between the MAPS of the two estimates
setpathvars
dsets=[];
%25-d results
% dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_19','poissexp_04_19_001.mat');
% dsets(end).lbl='online';
% dsets(end).simvar='simmax';
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_19','poissexp_04_19_001_batch.mat');
% dsets(end).lbl='batch';
% dsets(end).simvar='simbatch';

%5-d results04_23_001
dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_23','poissexp_04_23_001.mat');
dsets(end).lbl='online';
dsets(end).simvar='simmax';

dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_23','poissexp_04_23_001.mat');
dsets(end).lbl='batch';
dsets(end).simvar='simbatch';
%***************************************************
%parameters for the graph
%***************************************************
gparam.trials=[1:10];    %trials on which to plot the means
gparam.clim=[-1 1];       %values we scale color limits to



hold on;

%how columns and rows of axes we will need

gparam.fontsize=12;
gparam.width=3;          %width of the image in inches
gparam.height=3;            %height of the image in inches
colormap(gray);

%an inline function which automatically computes the linear index 
%of a subplot from its row and column
%subplot selects in row order so to use  sub2ind we transpose rows and
%columns
spind=@(row,col)(sub2ind([gparam.ncols gparam.nrows],col,row));


%************************************************************************
%loop over the data sets
for dind=1:length(dsets)
    [pdata{dind}, sim{dind}, mp{dind}, sr{dind}]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
end

%**************************************************
%recompute the means for the batch data since we don't have the means on
%all trials
prior=mp{2}.pinit;
post=prior;
for t=[1:100 100:50:1000 1000:500:10000 10000:5000:50000 500000:10000:100000]
    if (mod(t,10)==0)
        fprintf('trial %d \n',t);
    end
    if (sum(pdata{2}.m(:,t+1),1)==0)
        [post,uinfo,einfo]=update(sim{2}.updater,post,sr{2}.y(:,1:t),mp{2},sr{2}.nspikes(1:t),[]);
        pdata{2}.m(:,t+1)=post.m;
        pdata{2}.c{t+1}=post.c;
    end
end
%%
%********************************************

fmean=[];

fmean.hf=figure;
fmean.fontsize=12;
fmean.hp=[];
fmean.name='MAP';
%fmean.title='MAP';

gparam.xtick=10.^[1:5];
%gparam.ytick=[1 25:25:100];

setfsize(fmean.hf,gparam);

niter=min([sim{1}.niter sim{2}.niter]);
mdist=zeros(1,niter);

%find those points where we computed the mean in the batch case
ind=find(sum(pdata{2}.m,1)~=0)

%exclude initial trials for which distance is small because both start with
%same prior
ind=ind(find(ind>4));
mdist=pdata{1}.m(:,ind)-pdata{2}.m(:,ind);
mdist=sum(mdist.^2,1).^.5;

%normalize the distance by length of the true parameter
%plot the mdist
ktrue=gettheta(sim{1}.observer);
%skip the prior because they are same
fmean.hp=plot(ind,mdist/(ktrue'*ktrue)^.5,'k-');
%set(fmean.hp,'markerfacecolor','k');
%set(fmean.hp,'markeredgecolor','k');
fmean.xlabel='trial';
fmean.ylabel='normalized distance';

%don't make a box around the plot
box off;
lblgraph(fmean);
set(gca,'yscale','log');
set(gca,'xscale','log');
xlim([4 10^5])
set(gca,'xtick',gparam.xtick);
set(gca,'ytick',10.^[-6:0]);
%previewfig(fmean.hf,'bounds','tight','FontMode','Fixed','FontSize',fmean.fontsize,'Width',gparam.width,'Height',gparam.height)
fmean.outfile='~/cvs_ece/writeup/NC06/distmaps.eps';
%saveas(fmean.hf,fmean.outfile,'epsc2');
%exportfig(fmean.hf,fmean.outfile,'bounds','tight','FontMode','Fixed','FontSize',fmean.fontsize,'Width',gparam.width,'Height',gparam.height)