%12-19-2007
%Changed this to use our new data for the higher 40x40 gabor example
%
%Make a plot comparing the posterior entropy for optimization under the
%power constraint and using pool based stimuli for different numbers of
%stimuli in the pool
setpathvars;

dsets=[];
data=[];
nc06gabors40x40;

hold on;

%structure to specify size and witdth of image
gparam.xlim=[0 5000];
gparam.ylim=[-1900 500];

%******************************************************************
%Load the data
%**********************************************************
%order the plots in terms of which plot we want to be above others
%i.e think 'send to back' bring to front for graphics
for dind=1:length(dsets);
    %[pmax, simmax, mmax, srmax]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
    [simobj]=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);
    post=getpost(simobj);

    data(dind).entropy=getentropy(post);

end


%*************************************************************
%Plot  the entropy
%%

fentropy=FigObj('name','entropy','width',4,'height',4);
fentropy.a=AxesObj('xlabel','trial','ylabel','entropy');
hold on;

cmap=colormap(gray);
colors.grey=cmap(50,:);
c=[colors.grey;cmap(1,:)];

clear lstyle;
lind=1;
lstyle(1).marker='o';
lstyle(1).markerfacecolor=colors.grey;
lstyle(1).markeredgecolor=colors.grey;
lstyle(1).markersize=4;

lind=lind+1;
lstyle(lind).marker='o';
lstyle(lind).markerfacecolor=c(2,:);
lstyle(lind).markeredgecolor=c(2,:);
lstyle(lind).markersize=5;

lind=lind+1;
lstyle(lind).marker='s';
lstyle(lind).markerfacecolor=colors.grey;
lstyle(lind).markeredgecolor=colors.grey;
lstyle(lind).markersize=5.5;

lind=lind+1;
lstyle(lind).marker='^';
lstyle(lind).markerfacecolor=c(2,:);
lstyle(lind).markeredgecolor=c(2,:);
lstyle(lind).markersize=5;


lind=lind+1;
lstyle(lind).linewidth=2;
lstyle(lind).color=colors.grey;
lstyle(lind).marker='none';
lstyle(lind).linestyle='-';

lind=lind+1;
lstyle(lind).linewidth=2;
lstyle(lind).color=[0 0 0];
lstyle(lind).linestyle='--';
lstyle(lind).marker='none';

lind=lind+1;
lstyle(lind).linewidth=4;
lstyle(lind).color=[0 0 0];
lstyle(lind).marker='none';
lstyle(lind).linestyle='-';



%order the plots
dorder=[dnames.maxmean dnames.maxpool dnames.rand dnames.maxevec dnames.grad  dnames.maxheur  dnames.max];

dind=dorder(1);
ind=floor(linspace(1,gparam.xlim(2),20));
%plot the first two plots not as lines but as markers of dots

for oind=1:length(dorder)
    dind=dorder(oind);
    %subsample the entropy otherwise the dots merge together
    hp=plot(ind,data(dind).entropy(ind),'o');
    fentropy.a=addplot(fentropy.a,'hp',hp,'pstyle',lstyle(oind));
end

% for oind=5:length(dorder)
%     dind=dorder(oind);
%     %subsample the entropy otherwise the dots merge together
%     hp=plot(1:length(data(dind).entropy),data(dind).entropy(1:end),'o');
%     fentropy.a=addplot(fentropy.a,'hp',hp,'lbl',dsets(dind).slbl,'pstyle',lstyle(oind));
% end


xlim(gparam.xlim);
%ylim(gparam.ylim);

fentropy=lblgraph(fentropy);

%create a custom legend
hp=gethp(fentropy.a);
lbls={dsets(dorder).slbl};

hlgn=LegendObj('hlines',hp,'lbls',lbls);
hlgn=setlatex(hlgn);
hlgn=setposition(hlgn,.20,.13,.23,.5);
hlgn=settleft(hlgn,.32);
%hlgn=gethlgnd(fentropy.a);
%change the legend to latex
%set(hlgn,'interpreter','latex');

%previewfig(fentropy.hf,'bounds','loose','FontMode','Fixed','FontSize',fentropy.fontsize,'Width',gparam.width,'Height',gparam.height)
fentoutfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/gaborentropy.eps';
%saveas(gethf(fentropy),fentoutfile,'epsc2');
