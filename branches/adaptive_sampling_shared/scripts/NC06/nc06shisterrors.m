%11-09-07:
%   make plot of the MSE on each trial
%Revisions
%   11-09-2007: use new object model

dind=0;

% data=[];
% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071109','powernonlin_001.mat');
% dsets(end).lbl=sprintf('i.i.d.');
% dsets(end).simvar='rand';
% 
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071109','powernonlin_001.mat');
% %dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
% dsets(end).lbl=sprintf('info. max.');
% dsets(end).simvar='max';
% 
% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071111','powernonlin_001.mat');
% dsets(end).lbl=sprintf('i.i.d.');
% dsets(end).simvar='rand';
% 
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071111','powernonlin_001.mat');
% %dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
% dsets(end).lbl=sprintf('info. max.');
% dsets(end).simvar='max';
% 
% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071210','powernonlin_005.mat');
% dsets(end).lbl=sprintf('i.i.d.');
% dsets(end).simvar='rand';
% 
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071210','powernonlin_005.mat');
% %dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
% dsets(end).lbl=sprintf('info. max.');
% dsets(end).simvar='max';
% 
dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071211','expnonlin_021.mat');
dsets(end).lbl=sprintf('i.i.d.');
dsets(end).simvar='rand';


dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071211','expnonlin_021.mat');
%dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
dsets(end).lbl=sprintf('info. max.');
dsets(end).simvar='max';


lstyle=[];
lstyle=getbwlstyle(3);
lstyle(1).LineWidth=lstyle(1).LineWidth*.5;
lstyle(1).linestyle='-';
lstyle(1).color= [0.7778 0.7778 0.7778];
lstyle(2)=getbwlstyle(1);
lstyle(2).linestyle='-';
%subsample to prevent dashed lines from not showing up
subsamp=25;

%**************************************************************************
%Grapha Parameters
%**************************************************************************

fshist(1)=FigObj('name','Stimulus filter: MSE','width',2,'height',3);
fshist(1).a=AxesObj('nrows',1,'ncols',1);
fshist(2)=FigObj('name','Spike history filterMSE','width',2,'height',3);
fshist(2).a=AxesObj('nrows',1,'ncols',1);


yl=[10^0 2*10^1;9*10^-3 5*10^0];
xl=[1 10^4];

%loop over the datasets
clear simobj;
for dind=1:length(dsets)
    %load this simulation
   simobj(dind)=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

       
    %get true value
    theta=gettheta(getobserver(simobj(dind)));
    kfilt=theta(1:getstimlen(getmobj(simobj(dind))));
    afilt=theta(getstimlen(getmobj(simobj(dind)))+1:end);
    
    %get the estimates
    allpost=getpost(simobj(dind),1:getniter(simobj(dind)));
    mfull=getm(allpost,1:getniter(simobj(dind)));
    mstim=mfull(1:getstimlen(getmobj(simobj(dind))),:);
    mshist=mfull(getstimlen(getmobj(simobj(dind)))+1:end,:);


  
    stimmse=mstim-kfilt*ones(1,getniter(simobj(dind)));
    stimmse=sum(stimmse.^2,1).^.5;
    
    shistmse=mshist-afilt*ones(1,getniter(simobj(dind)));
    shistmse=sum(shistmse.^2,1).^.5;
    %fmse.a{dind}.lbls{pind}='i.i.d.'

    %*************************************
    %stimulus coefficients
    pind=1;
    tind=1;
    setfocus(fshist(pind).a,1,1);
    hold on;
    hp=plot(1:subsamp:getniter(simobj(dind)),stimmse(1:subsamp:getniter(simobj(dind))));
    fshist(1).a(tind,1)=addplot(fshist(1).a(tind,1),'hp',hp,'lstyle',lstyle(dind));
    fshist(pind).a(tind,1)=xlabel(fshist(pind).a(tind,1),'trial');
     
     xlim(xl);
     ylim(yl(1,:));
     
    %set ylabel
    fshist(pind).a(tind,1)=ylabel(fshist(pind).a(tind,1),'M.S.E.');
    
     fshist(pind).a(tind,1)=xlabel(fshist(pind).a(tind,1),'trial');
    set(gca,'yscale','log');
    set(gca,'xscale','log');
    xlim(xl);
    set(gca,'ytick',10.^[ceil(log10(yl(1,1))):floor(log10(yl(1,2)))]);
    %*************************************
    %spike history coefficients
    pind=2;
    tind=1;
    setfocus(fshist(2).a,1,1);
    hold on;
    hp=plot(1:subsamp:getniter(simobj(dind)),shistmse(1:subsamp:getniter(simobj(dind))));
    fshist(2).a(tind,1)=addplot(fshist(2).a(tind,1),'hp',hp,'lstyle',lstyle(dind));
     xlim([1 10^(ceil(log10(getniter(simobj(dind)))))]);
     
    %set ylabel
    fshist(pind).a(tind,1)=ylabel(fshist(pind).a(tind,1),'M.S.E.');
     fshist(pind).a(tind,1)=xlabel(fshist(pind).a(tind,1),'trial');
    set(gca,'yscale','log');
  %  xlim([1 getniter(simobj(dind))]);
     %xlim([1 10^(ceil(log10(getniter(simobj(dind)))))]);
     xlim(xl);
          ylim(yl(2,:));
       set(gca,'xscale','log');
       set(gca,'ytick',10.^[ceil(log10(yl(2,1))):floor(log10(yl(2,2)))]);
end

%adjust the ylimits
setfocus(fshist(1).a,1,1);

%**********************************************
%adjust labels
for pind=1:2
    
    %add xlabel to last plot
    if (pind==1)
        %title
    fshist(pind).a(1,1)=title(fshist(pind).a(1,1),'$\vec{\theta}_x$');        

 %   fshist(pind).a(length(trials),1)=xlabel(fshist(pind).a(length(trials),1),'\theta_{k_i}');
 
    else
            fshist(pind).a(1,1)=title(fshist(pind).a(1,1),'$\vec{\theta}_f$');        
%            fshist(pind).a(length(trials),1)=xlabel(fshist(pind).a(length(trials),1),'\theta_{a_i}');
    end
    ht=gettitle(fshist(pind).a(1,1));  
    set(ht.h,'interpreter','latex');
end


%call lblgraph first to adjust fontsizes
fshist(1)=lblgraph(fshist(1));
fshist(2)=lblgraph(fshist(2));

%turn off legend on all but first plot
% % for pind=1:2
% %     for tind=1:length(trials)
% %         hl=gethlgnd(fshist(pind).a(tind,1));
% %         set(hl,'visible','off');
% %     end
% %     end

figure(gethf(fshist(1)));
xlim(xl);


sizesubplots(fshist(1),[],[],[],[0 0 0 .05]);
%ha=plotlegend(gethp(fshist(1).a(1,1)),{dsets.lbl});

sizesubplots(fshist(2),[],[],[],[0 0 0 .05]);

% hlgnd=gethlgnd(fshist(1).a(1));
% set(hlgnd,'Location','south');

odata={'data sets',[{'label'},{'variable'},{'file'};{dsets.lbl}' {dsets.simvar}' {dsets.fname}'];fshist(1),'stimulus coefficients';fshist(2),'spike history coefficients'};
onenotetable(odata,seqfname('~/svn_trunk/notes/nc06shisterrors.xml'));
return;

%saveas(gethf(fshist(1)),'~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/kfilterrors.eps','epsc2')
%saveas(gethf(fshist(2)),'~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/shisterrors.eps','epsc2')