%5-20-07
%
%make a plot of the number of eigenvectors not orthogonal to the mean
%
%
%Revision History:
%   06-14-2007: Instead of measuring the energy measure the projection of
%   the mean because thats what the rank 1 code does. Deflation occurs if
%   the the projections is less than eps(or 10^-10)
%       Also count the number of eigenvectors for which projection is <eps;
%       Do not normalize the mean. We want to take the magnitude of the projection into account
%       because if the projection is small then we can't vary the linear
%       term very much by varying mglmproj so its unlikely we wast energy
%       on this dimension. 
%   Also plot the number of distinct eigenvectors
%   05-20-2007: we count the number of eigenvectors which account for sum
%   percentage of the energy 
clear all;
setpathvars;
dloaded=false;  %indicate data not loaded
dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001.mat');
dsets(end).lbl='power';
dsets(end).simvar='simmax';


%percente=1-10^-5;           %what percentage of the energy we want count
%threshold for determining if projection is zero or if eigenvalues are
%repated
threshold=10^-8;
subsamp=1;
tmax=1000;           %max trial;
msglast=0;
%*****
%%
for dind=1:length(dsets)
    if ~(dloaded)
        [pdata, sim, mobj, sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);

        tlast=min(sim.niter+1,tmax);
        %we will need to fill in the covariance matrices
        [pdata]=fillinlowmem(sim.updater,pdata,mobj,sr,1:tlast);
        dloaded=true;
    end
    dsets(dind).ncorr=zeros(1,tlast);
    dsets(dind).ncorr(:)=nan;
    for j=1:subsamp:tlast
        if(mod(msglast,100)==0)
            fprintf('j=%d\n',j);
            msglast=1;
        else
            msglast=msglast+1;
        end
        if ~isempty(pdata.c{j})
            [dsets(dind).eig(j).evecs dsets(dind).eig(j).eigd]=svd(pdata.c{j});
            %normalized energy
            %nenergy=(normmag(pdata.m(:,j))'*dsets(dind).eig(j).evecs).^2;
            %nenergy=sort(nenergy,'descend');
            %ind=find(cumsum(nenergy)>=percente);
            %dsets(dind).ncorr(j)=ind(1);            
            %count #of eigenvectors for which projection of mean is > eps
            eigproj=pdata.m(:,j)'*dsets(dind).eig(j).evecs;
            ind=find(abs(eigproj)>threshold);
      
            dsets(dind).ncorr(j)=length(ind);
            dsets(dind).neigdist(j)=length(find(abs(diff(diag(dsets(dind).eig(j).eigd)))>threshold==1));
            %compute the number of distinct eigenvalues
            
        else
            dsets(dind).ncorr(j)=nan;
        end
    end
end

%%
fncorr.hf=figure;
fncorr.xlabel='trial';
fncorr.ylabel='number';
fncorr.name='ncorr';
fncorr.semilogx=1;
fncorr.fontsize=12;
fncorr.name='Number eig. vec. orthogonal to mean';
gparam.width=3;
gparam.height=2.5;
setfsize(fncorr.hf,gparam);
hold on;
for dind=1:length(dsets)
    plot(1:subsamp:(length(dsets(dind).ncorr)-1),dsets(dind).ncorr(2:subsamp:end),'k-');
end
lblgraph(fncorr);
set(gca,'xtick',10.^[0:3])
xlim([1 10^3]);

fndist.hf=figure;
fndist.xlabel='trial';
fndist.ylabel='number';
fndist.name='ncorr';
fndist.semilogx=1;
fndist.fontsize=12;
fndist.name='Number distinct eigenvalues';
gparam.width=3;
gparam.height=2.5;
setfsize(fndist.hf,gparam);
hold on;
for dind=1:length(dsets)
    plot(1:subsamp:(length(dsets(dind).ncorr)-1),dsets(dind).neigdist(1:subsamp:end-1),'k-');
end
lblgraph(fndist);
set(gca,'xtick',10.^[0:3])
xlim([1 10^3]);

fncorr.outfile='~/cvs_ece/writeup/NC06/nmu.eps';
%saveas(fncorr.hf,fncorr.outfile,'epsc2')