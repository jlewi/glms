%03-17-2008
%
% This script redoes the plots of the MAPS for the case of a time varying 
% theta.
% I had to rewrite this script to remake the plots for the accepted version
% of our neural comp paper.

%Revisions:
%  10-01-2008
%       Rescaled the image for the final proofs
%       changed location of data
%       updated script to newest plotting objects
%
NDIR='/mnt/ext_harddrive/adaptive_sampling_results_06_13_2008/allresults/nips';

fdata=fullfile(NDIR,'11_27','11_27_gabortrack_001.mat');
alldata=load(fdata);
%%
fmaps=FigObj('name','time varying: MAPS','width',2.05,'height',1.4,'fontsize',5,'naxes',[1,4]);

%fmaps.a=AxesObj('nrows',1,'ncols',4,'xlabel','\theta_i');

yl=[1 800];
cl=[-.6 1];
%*****************************************************************
%Plot the Mean with random stimuli
%********************************************************************
cind=1;
fmaps.a=setfocus(fmaps.a,1,1);
data=alldata.prand.newton1d;
colormap('gray');
imagesc(data.m',cl);
fmaps.a(1,cind)=title(fmaps.a(1,cind),sprintf('random'));

xlim([1 100]);
ylim(yl);
%*****************************************************************
%Plot the true Mean 
%********************************************************************
cind=2;
fmaps.a=setfocus(fmaps.a,1,2);
data=alldata.mparam;
colormap('gray');
imagesc(data.ktrue',cl);
fmaps.a(1,cind)=title(fmaps.a(1,cind),('true \theta'));

xlim([1 100]);
ylim(yl);
%*****************************************************************
%Plot the Mean with infomax
%********************************************************************
cind=3;
fmaps.a=setfocus(fmaps.a,1,3);
data=alldata.pmax.newton1d;
colormap('gray');
imagesc(data.m',cl);
fmaps.a(1,cind)=title(fmaps.a(1,cind),sprintf('info. max.'));

xlim([1 100]);
ylim(yl);
%*****************************************************************
%Plot the Mean no diffusion
%********************************************************************
cind=4;
fmaps.a=setfocus(fmaps.a,1,4);

data=alldata.pmaxnodiff.newton1d;
colormap('gray');
imagesc(data.m',cl);
fmaps.a(1,cind)=title(fmaps.a(1,cind),sprintf('info. max.\n  no diffusion'));

xlim([1 100]);
ylim(yl);

hc=colorbar;
fmaps.a(1,4)=sethc(fmaps.a(1,4),hc);

%set the xlabels
for cind=1:4
   setfocus(fmaps.a,1,cind);
   fmaps.a(1,cind)=xlabel(fmaps.a(1,cind),'\theta_i'); 
   set(gca,'xtick',[1 100]);
end

%set the ylabels
setfocus(fmaps.a,1,1);
fmaps.a(1,1)=ylabel(fmaps.a(1,1),'trial');
set(gca,'ytick',[1 400 800]);

for cind=2:4
   setfocus(fmaps.a,1,cind);
   set(gca,'ytick',[]);
end

%reverse the direction 
for cind=1:4
   set(getha(fmaps.a(1,cind)),'ydir','reverse'); 
end

fmaps=lblgraph(fmaps);
space.cbwidth=.10;
fmaps=sizesubplots(fmaps,space);

outfile='~/svn_trunk/publications/adaptive_sampling/NC06/images_eps/gabortrack_mean_001.eps';
%saveas(gethf(fmaps),outfile,'epsc2');