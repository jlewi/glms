%12-16-2007
%   only plot the KL distance for the online method
%4-19-2007
%
%Compute the kl distance and make a plot of it
%outfile=fullfile(RESULTSDIR,'poissexp','04_19','kldata_04_19_001.mat');
%outfile=fullfile(RESULTSDIR,'poissexp','04_19','kldata_04_19_002.mat');

%List all the files containing the kldata
%we will select the variable which has the most data as measured by bytes
%KLdata should all be for the same simulation
klfile(1).fname=fullfile(RESULTSDIR,'poissexp','04_23','kldata_04_23_001.mat');
klfile(1).lbl='online'
%klfile(2).fname=fullfile(RESULTSDIR,'poissexp','04_23','kldata_04_23_001_simbatch1000.mat');
%klfile(2).lbl='batch';
%
%outfile=fullfile(RESULTSDIR,'poissexp','04_23','kldata_04_23_001_simbatch1000.mat');



simvars={'simmax','simbatch'};
simvars={'simmax'};
    
%simvars={'simbatch'};
%trials on which to compute the kl distance
%trials=[100 500 1000:500:9500 10000:10000:100000];
trials=[10 50 100 500 1000 10000 100000];
%trials=1000;
%trials=1000;
%load the results
%compute the kl distance

%trials=[100 500:500:2000];

%dk=zeros(length(simvars),length(trials));
dk=[];

%array to store estimate of variance
vsum=zeros(length(simvars),length(trials));

%********************************************************************
%get a list of the variables in each file
for j=1:length(klfile)
    klfile(j).vars=whos('-file',klfile(j).fname);
end
%********************************************************************
%compute the KL distance
%********************************************************************
for vind=1:length(simvars)
    dk(vind).trials=trials;
    dk(vind).dk=zeros(1,length(trials));
    dk(vind).dk(:)=nan;
    dk(vind).vsum=zeros(1,length(trials));
    dk(vind).nsamps=zeros(1,length(trials));
    dk(vind).fname=cell(1,length(trials));
    for tind=1:length(trials)

        trial=trials(tind);

        vname=sprintf('dkdata%s%d',simvars{vind},trial);

        %determine which files this variable is in
        ffile=[];
        for j=1:length(klfile)
            sind=strmatch(vname,{klfile(j).vars.name},'exact');
            if ~isempty(sind)
                ffile(end+1).fname=klfile(j).fname;
                ffile(end).bytes=klfile(j).vars(sind).bytes;
            end
            %warning('File did not contain variable %s \n',vname)
            %vname=sprintf('dkdata%s%d',simvars{vind},trial);
            %warning('Checking for %s \n',vname);
        end

        %select the file which has the most samples
        if isempty(ffile)
            warning('File did not contain variable %s \n',vname)

        else
            [n fi]=max([ffile.bytes]);
            dk(vind).fname{tind}=ffile(fi).fname;
            load(dk(vind).fname{tind},vname);
            eval(sprintf('data=%s;',vname));

            %the lpost is sorted so if we use less than all the samples we need to
            %randomly select them
            [dk(vind).dk(tind),dk(vind).vsum(tind)]=dkcompute(data.lpost,data.lpapprox,data.gpost);
            dk(vind).nsamps(tind)=size(data.lpost,2);
        end
    end
    ind=find(~isnan(dk(vind).dk));
    dk(vind).trials=dk(vind).trials(ind);
    dk(vind).dk=dk(vind).dk(ind);
    dk(vind).vsum=dk(vind).vsum(ind);
    dk(vind).nsamps=dk(vind).nsamps(ind);
end

%************************************************************************
%graph parameters for presentation figure
%*************************************************************************
%size in inches
%%
gparam.width=3.2;
gparam.height=3;


fkl=[];
fkl.hf=figure;
fkl.xlabel='trial';
fkl.ylabel='KL distance';
fkl.markersize=25;
fkl.fontsize=12;
fkl.name='KL distance';

setfsize(fkl,gparam);

hold on;
for v=1:length(simvars)
    fkl.hp(v)=plot(dk(v).trials,dk(v).dk,'.');
    fkl.lbls{v}=klfile(v).lbl;
end
fkl=lblgraph(fkl);
ls=getbwlstyle(1);
%ls.markersize=fkl.markersize+4;
setlstyle(fkl.hp(1),ls);
%setlstyle(fkl.hp(2),getbwlstyle(2));
set(gca,'xscale','log');
%set(gca,'yscale','log');

%we need to set the tick labels to be the full numbers
%otherwise only the exponents will be printed
set(gca,'xtick',10.^[1:5]);

%turn off the legend
delete(fkl.hlgn);

xlim([min(dk(1).trials) 10^4]);
%We use save fig because we want the tick labels to be in base 10
fkl.outfile='~/svn_trunk/adaptive_sampling/writeup/NC06/klpost.eps';
%saveas(gcf,fkl.outfile,'epsc2')

%previewfig(fkl.hf,'bounds','tight','FontMode','Fixed','FontSize',fkl.fontsize,'Width',gparam.width,'Height',gparam.height,'color','bw')
%outfile='~/cvs_ece/writeup/NC06/klpost.eps';
%exportfig(fkl.hf,fkl.outfile,'bounds','tight','FontMode','Fixed','FontSize',fkl.fontsize,'Width',gparam.width,'Height',gparam.height,'color','rgb')