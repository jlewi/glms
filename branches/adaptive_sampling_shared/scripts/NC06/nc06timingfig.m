%3-18-2008
%   modified for new object model so I can redo plots using helvetica fonts
%7-31-2007
%   use new object oriented model and new data.
%   


DAYDIR='070813';

dfile=[];
diary('~/tmp/runsimtiming.txt');


dims=[100 200 250 300 400 500 625 750];

dims=[100:100:800];
sims=[];
for dind=1:length(dims)
    
sims(dind).dim=dims(dind);
sims(dind).fname=fullfile(RESULTSDIR,'poissexp',DAYDIR,sprintf('simtime_%dd_%s_001.mat',sims(dind).dim,DAYDIR));
sims(dind).simvar=sprintf('max%d',sims(dind).dim);
if ~exist(sims(dind).fname,'file')
    error('file %s doesnot exist will not be able to do sim',sims(dind).fname);
    
else
    %check simulation variable exists
    if ~(varinfile(sims(dind).fname,sims(dind).simvar))
       warning('variable %s does not exist in file %s', sims(dind).simvar, sims(dind).fname);
    end
end
end

%sims(1).fname=fullfile(RESULTSDIR,'poissexp','070807',sprintf('simtime_070807_%d_002.mat',sims(1).dim));

%%
%start trial
%specfies how many trials at the beginning to throw out
%which dimensionality to use for fitting the data
dstart=200;

% loop through the data and compute the time as a function of the
% dimensionality of the different steps
stime=[];
stime.totalt=zeros(1,length(sims));
for sind=1:length(sims)

sim=SimulationBase('fname',sims(sind).fname,'simvar',sims(sind).simvar);

stime.dim(sind)=getklength(getmobj(sim));
mobj=getmobj(sim);
tstart=(getklength(mobj)+2);


post=getpost(sim);
uinfo=[post(tstart+1:end).uinfo];
%plot the optimization time
sr=getsr(sim);
stiminfo=[sr(tstart:end).stiminfo];
searchtime=[stiminfo.searchtime];
stime.fishermax(sind)=median([searchtime.fishermax]);
stime.tquadmod(sind)=median([searchtime.tquadmod]);

stime.eigtime(sind)=median([uinfo.eigtime]);
stime.ntime(sind)=median([uinfo.ntime]);

stime.totalt(sind)=stime.fishermax(sind)+stime.tquadmod(sind)+stime.ntime(sind)+stime.eigtime(sind);

stime.stdtotalt(sind)=std([searchtime.fishermax]+[searchtime.tquadmod]+[uinfo.ntime]+[uinfo.eigtime]);
end


%**************************************************************************
%define the marks to use
marks=cell(1,4);
marks{1}='o';
marks{2}='d';
marks{3}='^';
marks{4}='s';
marks{5}='x';

%ftiming.hf=figure;
ftiming=[];
ftiming.hf=figure;
ftiming.hp=[];
ftiming.x=[];
ftiming.xlabel='dimensionality';
ftiming.ylabel='time(seconds)';
ftiming.title='';
ftiming.name='Timing';
ftiming.on=1;
ftiming.fname='timing';
ftiming.semilog=1;
ftiming.linewidth=3;
ftiming.markersize=8;
ftiming.semilogx=0;
ftiming.fontsize=12;
ftiming.width=6;
ftiming.height=3;


%%
%*******************************************************************
%Plot it
%ftiming.hf=figure;
figure(ftiming.hf);
hold on;

%index of first pt to use for fitting the data
istart=find(stime.dim>=dstart,1);
istart=istart(1);


%x pts to use for fitted x
x=dstart:stime.dim(end);
%********************************************************
%total time
%*****************************************************
pind=1;


%fit total time to to a 2nd degree
%polynomial eventhough it really is O(n^3)
%do this to illustrate the speedp do to the rank 1 step
[ptotalt.p,ptotalt.s]=polyfit(stime.dim(istart:end),stime.totalt(istart:end),2);
y=ptotalt.p(1)*x.^2+ptotalt.p(2)*x+ptotalt.p(3);

%table describing the fits
ofits={'Step', 'Fitted Polynomial'};
ofits=[ofits; {'Total',{'degree', 2; 'coefficients', {'x^2', ptotalt.p(1); 'x', ptotalt.p(2); '1', ptotalt.p(3)}}}];
    
fprintf('Total time fitted to 2nd degree polynomial\n');
%fprintf('p(1)=%0.3g \t p(2)=%0.3g \t p(3)=%0.3g \n',p(1),p(2),p(3)); 

ftiming.hp(pind)=plot(stime.dim,stime.totalt,'k');

ftiming.hpline(pind)=plot(x,y,getptype(pind,2));
ftiming.lbls{pind}=sprintf('total');

%**************************************************************************
%Eigen decomposition of covariance matrix
%**************************************************************************
pind=pind+1;


%**********************************
%try fitting the diagonalization to a 2nd degree
%polynomial eventhough it really is O(n^3)
%to illustrate average running time
[peig.p,peig.s]=polyfit(stime.dim(istart:end),stime.eigtime(istart:end),2);
y=peig.p(1)*x.^2+peig.p(2)*x+peig.p(3);
ofits=[ofits; {'eigen.',{'degree', 2; 'coefficients', {'x^2', peig.p(1); 'x', peig.p(2); '1', peig.p(3)}}}];
   
fprintf('Diagonalization fitted to 2nd degree polynomial \n');
%fprintf('p(1)=%0.3g \t p(2)=%0.3g \t p(3)=%0.3g \n',p(1),p(2),p(3)); 

ftiming.hp(pind)=plot(stime.dim,stime.eigtime,marks{pind});

ftiming.hpline(pind)=plot(x,y);
ftiming.lbls{pind}=sprintf('eigen.');

%**************************************************************************
%Modify the quadratic form: time of the two rank 1 perturbations
%**************************************************************************
pind=pind+1;

%fit a degree 2 polynomial
[pqmod.p,pqmod.s]=polyfit(stime.dim(istart:end),stime.tquadmod(istart:end),2);
y=pqmod.p(1)*x.^2+pqmod.p(2)*x+pqmod.p(3);
ofits=[ofits; {'modifying the quadratic form.',{'degree', 2; 'coefficients', {'x^2', pqmod.p(1); 'x', pqmod.p(2); '1', pqmod.p(3)}}}];
fprintf('Diagonalization fitted to 2nd degree polynomial \n');

ftiming.hpline(pind)=plot(x,y);

ftiming.hp(pind)=plot(stime.dim,stime.tquadmod,marks{pind});

ftiming.lbls{pind}=sprintf('quad. mod.');

%*******************************************************************
%update time
pind=pind+1;

%fit 2nd degree
[pup.p,pup.s] = POLYFIT(stime.dim(istart:end),stime.ntime(istart:end),2);
y=pup.p(1)*x.^2+pup.p(2)*x+pup.p(3);
ftiming.hpline(pind)=plot(x,y,getptype(pind,2));
ftiming.lbls{pind}=sprintf('posterior');
ls=getbwlstyle(pind);
setlstyle(ftiming.hpline(pind),ls);


ftiming.hp(pind)=plot(stime.dim(istart:end),stime.ntime(istart:end),marks{pind});
%ftiming.hp(pind)=errorbar(d,updatetime,updatetimestd);


%*************************************************************************
%Optimization step
pind=pind+1;

%fit a linear polynomial
[pf.p,pf.s] = POLYFIT(stime.dim(istart:end),stime.fishermax(istart:end),1);
y=pf.p(1)*x+pf.p(2);
ftiming.hpline(pind)=plot(x,y,marks{pind});
ftiming.lbls{pind}=sprintf('optimize');

ftiming.hp(pind)=plot(stime.dim,stime.fishermax);
%ftiming.hp(pind)=errorbar(d,searchtime,searchtimestd);






%**********************************************
%loop and set the line style and marker style
for pind=1:5

    ls=getbwlstyle(pind);
    setlstyle(ftiming.hpline(pind),ls);
    set(ftiming.hpline(pind),'Marker','none');
    set(ftiming.hp(pind),'Marker',marks{pind});
    set(ftiming.hp(pind),'MarkerEdgeColor',ls.color);
    set(ftiming.hp(pind),'MarkerFaceColor',ls.color);
    set(ftiming.hp(pind),'MarkerSize',ftiming.markersize);

    set(ftiming.hp(pind),'LineStyle','none');
end

%%

%**************************************
%adjust 

ftiming=lblgraph(ftiming);
set(gca,'ytick',10.^[-3:-1]);


%
%for the legend we want to not only indicate the marker but the line style
%as well
%therefore we make a plot of each combined style of marker 
%and line style and then make the plot invisible
%hl=findall(ftiming.hlgn,'type','line')
for pind=1:5
    ftiming.hcomb(pind)=plot(0,0);
    ls=getbwlstyle(pind);
    setlstyle(ftiming.hcomb(pind),ls);
    set(ftiming.hcomb(pind),'Marker','none');
    set(ftiming.hcomb(pind),'Marker',marks{pind});
    set(ftiming.hcomb(pind),'MarkerEdgeColor',ls.color);
    set(ftiming.hcomb(pind),'MarkerFaceColor',ls.color);
    set(ftiming.hcomb(pind),'MarkerSize',ftiming.markersize);
end
lblgraph(ftiming);

ftiming.hlgn=legend(ftiming.hcomb,ftiming.lbls);
%shrink the font size of the legend
set(ftiming.hlgn,'fontsize',10);

%widen the graph a little bit
pos=get(gca,'position');
pos(3)=.84;
set(gca,'position',pos);

%ylim([min([updatetime searchtime eigtime totaltime])-.001 max([updatetime searchtime eigtime totaltime])]+.001);
%We use save fig because we want the tick labels to be in base 10
ftiming.outfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/timing.eps';
%saveas(ftiming.hf,ftiming.outfile,'epsc2')

%exportfig(ftiming.hf,ftiming.outfile,'bounds','tight','Color','bw');
%exportfig(ftiming.hf,ftiming.outfile,'bounds','tight','Color','rgb');


