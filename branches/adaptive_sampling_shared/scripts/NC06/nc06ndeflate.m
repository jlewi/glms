% plot the number of non-deflated eigenvalues as a function of the trial.
% This is the number of eigenvectors/eigenvalues which change
%
% my new simulations automatically save this information.
%
%Revisions:
%   3-13-2008: I made some modifications so that I could run this
%       script because I think my object model changed.
clear all;
setpathvars;
dsets=[];

%****************************************************
%Parameters
%******************************************************
tmax=5000;
tmax=600;

%each entry describes the dataset for a different a row
dim=200;
DAYDIR='070806';
dind=1;
sims(dind).fname=fullfile(RESULTSDIR,'poissexp',DAYDIR,sprintf('simtime_%dd_%s_001.mat',dim,DAYDIR));
sims.simvar=sprintf('max%d',dim);

einfo=[];


sim=SimulationBase('fname',sims.fname,'simvar',sims.simvar);

post=getpost(sim);
einfo=[post(:).uinfo];


%%
%**************************************************************************
%Figures
%**********************************************************************

ntrials=min(getniter(sim),tmax);

fdeft.hf=figure;
fdeft.ylabel=sprintf('number of \nmodified eigenvectors');
fdeft.xlabel='trial';
fdeft.name='deflate';
fdeft.hp(1)=plot(getklength(getmobj(sim))-[einfo.ndeflate],'k.');
fdeft.width=3;
fdeft.height=2.9;
fdeft.fontsize=12;
lblgraph(fdeft);

fdeft.outfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/ndeflate.eps';
%saveas(fdeft.hf,fdeft.outfile,'epsc2')
