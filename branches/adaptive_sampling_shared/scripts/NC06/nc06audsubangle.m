%5-29-07
%
%For the quadratic input nonlinearity simulations
%we compute the first and second principal components of the estimate
%reshaped as a matrix and then plot the angle between the subspace spanned
%by these vectors and the true eigenspace. 

 dsets=[];

%*******************************************
%60 d gammatone
% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_simmax.mat');
% dsets(end).lbl='info. max. heuristic';
% dsets(end).simvar='simmax';
% 
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_simrand.mat');
% dsets(end).lbl='i.i.d.';
% dsets(end).simvar='simrand';
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_simunif.mat');
% dsets(end).lbl='info. max. i.i.d.';
% dsets(end).simvar='simunif';

dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','071030','audnonlin_001.mat');
dsets(end).lbl='info. max. heuristic';
dsets(end).simvar='max';
%dsets(end).maxtrial=10000; %max trial to plot

dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','071029','audnonlin_001.mat');
dsets(end).lbl='tones';
dsets(end).simvar='tones';
%****************************
gparam.height=3.25;
gparam.width=3.25;
gparam.subsample=250;   %how often to compute and plot the angle


%********************************************************************
%compute the subspace angle
for dind=1:length(dsets)
    fprintf('Dataset # %d \n',dind);
  % [pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
   simobj{dind}=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);
    extra=getextra(simobj{dind});
   k1=extra.mparam.k1;
   k2=extra.mparam.k2;
   if (size(k1,2)>1)
       k1=k1';
   end
    if (size(k2,2)>1)
       k2=k2';
   end
   dsets(dind).angle=zeros(1,ceil(getniter(simobj{dind})/gparam.subsample));
   dsets(dind).niter=getniter(simobj{dind});
   ninp=getninpobj(getmobj(simobj{dind}));
   for trial=1:gparam.subsample:getniter(simobj{dind})
       ind=floor(trial/gparam.subsample)+1;
       [evecs, eigval]=svd(qmatquad(ninp,getpostm(simobj{dind},trial)));
       dsets(dind).angle(ind)=subspace(evecs(:,1:2),[k1 k2]);
   end
end

%**************************************************************************
%plot the angle
%*************************************************************
%%
fangle=[];
fangle.hf=figure;
fangle.fontsize=12;
fangle.xlabel='Trial';
fangle.ylabel='Angle (degrees)';
fangle.name='Auditory Subspace Angle';
setfsize(fangle,gparam);
hold on
for dind=1:length(dsets)
    fangle.hp(dind)=plot(1:gparam.subsample:dsets(dind).niter,dsets(dind).angle*180/pi);
    fangle.lbls{dind}=dsets(dind).lbl;
    ls=getbwlstyle(dind);
    setlstyle(fangle.hp(dind),ls);
end
lblgraph(fangle);

%set(gca,'xscale','log');
%set(gca,'yscale','log');
%ylim([.1 100]);
%set(gca,'xtick',10.^[0:4]);
fangle.outfile='~/cvs_ece/writeup/NC06/auditory_angle.eps';
%saveas(fangle.hf,fangle.outfile,'epsc2');