%verify the likelihoods returned by gposteriorglm
%and using brute force.
clear all
setpaths
%reseed the random number generator
rstate=5;
randn('state',rstate);
rand('state',rstate);

%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
dt=.005;
tresponse=20;

%**************************************************************************
%True Model: 1-d temporal gaussian
%**************************************************************************
ktrue=2;

%*************************************************************************
%Generate Sample data
%************************************************************************
%generate random stimuli
numiter=1;                  %how many iterations to try
x=rand(1,numiter)*2-1;
%compute the rates for these stimuli
r=glm1dexp(ktrue,x);


%array to store spike trains. Each row is a different trial
spikes=zeros(numiter,ceil(tresponse/dt));

for index=1:numiter
    stimes=shpoisson(r(index),tresponse);
    spikes(index,floor(stimes./dt)+1)=1;
end

%**************************************************************************

%**********************************
%compute the log liklihood over k
%*********************************
kvals=[-5:.01:5];
llobsrv=ll1dtemp(spikes,x',kvals,dt);

%********************************
%compute the log likelihood over u
%********************************
%compute the likelihood under all the different values of u
du=.01;
upts=[-10/2:du:10/2];
r=glm1dexp(upts);

lpobsrv=hpoissonll(sum(spikes,2),r,size(spikes,2)*dt);

figure;
hold on;
plot(kvals,llobsrv(1,:),'r-')
plot(kvals,llobsrv(1,:),'rx')
plot(upts/x(:,1),lpobsrv,'g-');
plot(upts/x(:,1),lpobsrv,'go');
