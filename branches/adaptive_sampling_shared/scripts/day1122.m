%make a plot of the least squared error between
%the mean of the gaussian and the true mean after every iteration
lse=pmethods.g.m-ktrue*ones(1,numiter+1);
lse=lse.^2;
lse=sum(lse,1);
figure;
hold on;
h=[];
pind=1;
h(pind)=plot(1:numiter+1,log(lse),getptype(pind,1));
plot(1:numiter+1,log(lse),getptype(pind,2));
xlabel('Iteration');
ylabel('log(Distance)');


%plot the stimulus
figure;
hold on;
subplot(2,1,1);
pind=1;
h(pind)=plot(1:numiter,x(1,:),getptype(pind,1));
plot(1:numiter,x(1,:),getptype(pind,2));
ylabel('x1');
subplot(2,1,2);
pind=2;
h(pind)=plot(1:numiter,x(2,:),getptype(pind,1));
plot(1:numiter,x(2,:),getptype(pind,2));
ylabel('x2');

xlabel('Iteration');
%ylabel('Value');
%legend(h,{'x_1','x_2'});