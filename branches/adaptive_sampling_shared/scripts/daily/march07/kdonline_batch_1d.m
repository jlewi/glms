%03-08-2007
%
%Compute the KL distance between the batch and online approximations of the
%kl distance.

%fname='/home/jlewi/cvs_ece/results/adaptive_sampling/results/nips/11_15/11_15_lowdcos_001.mat';
%fname='/home/jlewi/cvs_ece/allresults/poissexp/03_08/03_08_poissexp_001.mat'
fname='/home/jlewi/cvs_ece/allresults/poissexp/03_12/03_12_poissexp_001.mat'
%load(fname);

[pbatch simbatch mparam srbatch]=loadsim('simfile',fname,'simvar','simbatch');
[ponline simonline mparam sronline]=loadsim('simfile',fname,'simvar','simonline');
kl=zeros(1,size(pbatch,2));
trials=zeros(1,length(kl));

cmse=zeros(1,length(kl));
mmse=zeros(1,length(kl));

mbias=zeros(1,length(kl));
trcovar=zeros(1,length(kl));
d=mparam.klength;
for j=1:size(pbatch.m,2)
%for j=1:50
%    if (~isempty(pmax.newton1d.c{j}) && ~isempty(pmax.newton1d.c{j}))
       gbatch.m=pbatch.m(:,j);
      gbatch.c=pbatch.c{j};
      
      gonline.m=ponline.m(:,j);
      gonline.c=ponline.c{j};
%      gonline.m=gbatch.m;
      
     kl(j)=kldistgauss(gbatch,gonline);
%      kl(j)=kldistgauss(gonline,gbatch);
      trials(j)=j-1;
  %  end
  
    cmse(j)=j^.5*sum((gonline.c(:)-gbatch.c(:)).^2)^.5;
    mmse(j)=j^.5*sum((gonline.m(:)-gbatch.m(:)).^2)^.5;
    
    %mbias
%    mbias(j)=.5*trace(inv(gonline.c)*(gbatch.m*gbatch.m'+gonline.m*gonline.m'-2*gonline.m*gonline.m'));
  iq=inv(gonline.c);
    mbias(j)=.5*gbatch.m'*iq*(gbatch.m-gonline.m)+.5*(-gbatch.m+gonline.m)'*iq*gonline.m;
    trcovar(j)=.5*log(det(gonline.c))+.5*trace(inv(gonline.c)*gbatch.c)-(.5*d+.5*log(det(gbatch.c)));

end



fkl.hf=figure;
fkl.xlabel='Trial';
fkl.ylabel='KL distance';
fkl.name='Kl Distance';
fkl.title=sprintf('KL distance batch vs online updates \n');
fkl.axisfontsize=20;
fkl.hp=plot(trials,kl,getptype(2));
lblgraph(fkl);

%plot the mse of the means and the eigenvalues
%multiply by root n
fmse.hf=figure;
fmse.xlabel='Trial';
fmse.ylabel='n^.5*||C_{batch}-C_{online}||_2';
fmse.title='MSE of Covariance matrices';
fmse.hp=plot(trials,cmse,getptype(2));
fmse.name='c mse';
lblgraph(fmse);

fmmse.hf=figure;
fmmse.xlabel='Trial';
fmmse.ylabel='\sqrt{n}*||m_{batch}-m_{online}||_2';
fmmse.title='MSE of Means';
fmmse.hp=plot(trials,mmse,getptype(2));
fmmse.name='mean mse';
lblgraph(fmmse);

fmbias.hf=figure;
fmbias.xlabel='Trial';
fmbias.title='.5 Tr(C_o^{-1}(\mu_b\mu_b^t+\mu_o\mu_o^t-2*\mu_o\mu_b))';
%fmbias.title='Relative Bias';
fmbias.hp=plot(trials,mbias,getptype(2));
fmbias.name='mean mbias';
lblgraph(fmbias);

ftrcovar.hf=figure;
ftrcovar.xlabel='Trial';
ftrcovar.title='n^{.5}*(.5log|C_o|+Tr(C_o^{-1}C_b)-.5d-.5log|C_b|)';
%ftrcovar.title='Trace Covar';
ftrcovar.hp=plot(trials,trials.^.5.*trcovar,getptype(2));
ftrcovar.name='Trace covar';
lblgraph(ftrcovar);




%test whether the covariance converges to the predicted asymptotic values
fasym.hf=figure;
fasym.xlabel='trial';
fasym.ylabel='n \sigma^2';
fasym.name='asymptotic variance';
hold on;
pind=1;
fasym.hp(pind)=plot(trials,trials.*[pbatch.c{:}],getptype(pind));
fasym.lbls{pind}='batch';

pind=pind+1;
fasym.hp(pind)=plot(trials,trials.*[ponline.c{:}],getptype(pind));
fasym.lbls{pind}='online';



pind=pind+1;
iidvar=2*(mparam.mmag^2*(exp(-mparam.mmag*mparam.ktrue)+exp(mparam.mmag*mparam.ktrue)))^-1;
fasym.hp(pind)=plot([trials(1) trials(end)],iidvar*[1 1],getptype(pind));
fasym.lbls{pind}='True';

%evaluate the asymptotic variance at the estimated mean
pind=pind+1;
varbatch=2*(mparam.mmag^2*(exp(-mparam.mmag*pbatch.m)+exp(mparam.mmag*pbatch.m))).^-1;
fasym.hp(pind)=plot(trials,varbatch,getptype(pind));
fasym.lbls{pind}='Asym. at batch';

lblgraph(fasym);

%test whether the covariance converges to the predicted asymptotic values
fratio.hf=figure;
fratio.xlabel='trial';
fratio.ylabel='C_{batch}/C_{online}';
fratio.name='variance ratio';
plot(trials,[pbatch.c{:}]./[ponline.c{:}]);
lblgraph(fratio);

frratio.hf=figure;
frratio.xlabel='trial';
frratio.ylabel='n^{.5}|C_{batch}/C_{online}-1|';
frratio.name='rate variance ratio';
plot(trials,trials.^.5.*abs([pbatch.c{:}]./[ponline.c{:}]-1));
lblgraph(frratio);

fmap.hf=figure;
fmap.xlabel='trial';
fmap.ylabel='MAP';
fmap.name='MAP';
hold on;
fmap.hp(1)=plot(trials,ponline.m,getptype(1));
fmap.lbls{1}='online';

fmap.hp(2)=plot(trials,pbatch.m,getptype(2));
fmap.lbls{2}='batch';
fmap.hp(3)=plot([trials(1) trials(end)],[mparam.ktrue, mparam.ktrue],getptype(3));
fmap.lbls{3}='Truth';

lblgraph(fmap);


%plot the contribution to the variance in the online case on each iteration
fjobs.hf=figure;
fjobs.xlabel='trial';
fjobs.ylabel='J_{obs}';
fjobs.name='J_obs';
hold on;
plot(trials(2:end),exp(sronline.y.*ponline.m(2:end)).*sronline.y.^2,getptype(1));
lblgraph(fjobs);

%recompute the online covariance using only the most recent observations
nrecent=length(sronline.y);
conline=ponline.c{1}^-1+exp(sronline.y(end-nrecent+1:end).*ponline.m(end-nrecent+1:end)).*sronline.y(end-nrecent+1:end).^2;
conline=[ponline.c{1}^-1 conline];
conline=1./cumsum(conline);

fadj.hf=figure;
fadj.xlabel='Trial';
%plot((1:nrecent).*conline./(trials(end-nrecent+1:end).*[pbatch.c{end-nrecent+1:end}]));
hold on;
plot(conline,getptype(1));
plot([ponline.c{:}],getptype(2));


%**************************************************************************
%plot the different components of the ratio of the covariances to see the
%rate at which they go to zero.

ninitial=500;   %how many terms to include in the first component which I think decreases at n^.5 rate
for t=1:simonline.niter
    num1=ponline.c{1}^-1;
    tend=min(t,ninitial);
    numts=exp(sronline.y(1:t).*ponline.m(2:t+1)).*sronline.y(1:t).^2;
    numts=numts-exp(srbatch.y(1:t).*pbatch.m(t+1)).*sronline.y(1:t).^2;
%    num1=num1+exp(sronline.y(1:tend).*ponline.m(2:tend+1)).*sronline.y(1:tend).^2;
  %  num1=num1-exp(srbatch.y(1:tend).*pbatch.m(t+1)).*sronline.y(1:tend).^2;
    denom=sum(exp(sronline.y(1:t).*ponline.m(2:t+1)).*sronline.y(1:t).^2);
    rd1.t1(t)=sum(numts(1:tend))./denom;
    rd1.t2(t)=sum(numts(tend+1:end))./denom;
    
end

ft.hf=figure;
ft.xlabel='trial';
ft.name='terms';
hold on;
ft.hp(1)=plot(1:simonline.niter,(1:simonline.niter).^.5.*rd1.t1,getptype(1));
ft.hp(2)=plot(1:simonline.niter,(1:simonline.niter).*rd1.t2,getptype(2));
ft.lbls{1}='Initial Terms';
ft.lbls{2}='Growing Terms';
lblgraph(ft);
return;
