%03-08-2007
%
%Compute the KL distance between the batch and online approximations of the
%kl distance.

%fname='/home/jlewi/cvs_ece/results/adaptive_sampling/results/nips/11_15/11_15_lowdcos_001.mat';
%fname='/home/jlewi/cvs_ece/allresults/poissexp/03_08/03_08_poissexp_001.mat';
%fname='/home/jlewi/cvs_ece/allresults/poissexp/03_12/03_12_poissexp_001.mat'
fname='/home/jlewi/cvs_ece/allresults/poissexp/03_13/03_13_poissexp_002.mat';
%load(fname);

%[pbatch simbatch mparam sr]=loadsim('simfile',fname,'simvar','simbatch');
[ponline simonline mparam sr]=loadsim('simfile',fname,'simvar','simonline');

%restimate the mean and covariance using all observations
bupdater=BatchML();
%%

post.m=ponline.m(:,1);
post.c{1}=ponline.c{1};
pbatch.m=zeros(mparam.klength,simonline.niter+1);
pbatch.c=cell(1,simonline.niter+1);
pbatch.c{1}=ponline.c{1};
for t=1:simonline.niter
    [post,uinfo,einfo]=update(bupdater,post,sr.y(:,1:t),simonline.glm,mparam,sr.nspikes(1:t),[]);
    pbatch.m(:,t+1)=post.m;
    pbatch.c{t+1}=post.c;
end
%%


%%
kl=zeros(1,size(pbatch,2));
trials=zeros(1,length(kl));

cmse=zeros(1,length(kl));
mmse=zeros(1,length(kl));

mbias=zeros(1,length(kl));
trcovar=zeros(1,length(kl));
d=mparam.klength;
for j=1:size(pbatch.m,2)
%for j=1:50
%    if (~isempty(pmax.newton1d.c{j}) && ~isempty(pmax.newton1d.c{j}))
       gbatch.m=pbatch.m(:,j);
      gbatch.c=pbatch.c{j};
      
      gonline.m=ponline.m(:,j);
      gonline.c=ponline.c{j};
%      gonline.m=gbatch.m;
      
     kl(j)=kldistgauss(gbatch,gonline);
%      kl(j)=kldistgauss(gonline,gbatch);
      trials(j)=j-1;
  %  end
  
    cmse(j)=j^.5*sum((gonline.c(:)-gbatch.c(:)).^2)^.5;
    mmse(j)=j^.5*sum((gonline.m(:)-gbatch.m(:)).^2)^.5;
    
    %mbias
%    mbias(j)=.5*trace(inv(gonline.c)*(gbatch.m*gbatch.m'+gonline.m*gonline.m'-2*gonline.m*gonline.m'));
  iq=inv(gonline.c);
    mbias(j)=.5*gbatch.m'*iq*(gbatch.m-gonline.m)+.5*(-gbatch.m+gonline.m)'*iq*gonline.m;
    trcovar(j)=.5*log(det(gonline.c))+.5*trace(inv(gonline.c)*gbatch.c)-(.5*d+.5*log(det(gbatch.c)));

end



fkl.hf=figure;
fkl.xlabel='Trial';
fkl.ylabel='KL distance';
fkl.name='Kl Distance';
fkl.title='KL distance batch vs. online';
fkl.axisfontsize=20;
fkl.hp=plot(trials,kl,getptype(2));
lblgraph(fkl);

%plot the mse of the means and the eigenvalues
%multiply by root n
fmse.hf=figure;
fmse.xlabel='Trial';
fmse.ylabel='n^.5*||C_{batch}-C_{online}||_2';
fmse.title='MSE of Covariance matrices';
fmse.hp=plot(trials,cmse,getptype(2));
fmse.name='c mse';
lblgraph(fmse);

fmmse.hf=figure;
fmmse.xlabel='Trial';
fmmse.ylabel='n^.5*||m_{batch}-m_{online}||_2';
fmmse.title='MSE of Means';
fmmse.hp=plot(trials,mmse,getptype(2));
fmmse.name='mean mse';
lblgraph(fmmse);

fmbias.hf=figure;
fmbias.xlabel='Trial';
fmbias.title='n^{.5}(.5 Tr(C_o^{-1}(\mu_b\mu_b^t+\mu_o\mu_o^t-2*\mu_o\mu_b)))';
%fmbias.title='Relative Bias';
fmbias.hp=plot(trials,trials.^.5.*mbias,getptype(2));
fmbias.name='mean mbias';
lblgraph(fmbias);

ftrcovar.hf=figure;
ftrcovar.xlabel='Trial';
ftrcovar.title='n^{.5}(.5log|C_o|+Tr(C_o^{-1}C_b)-.5d-.5log|C_b|)';
%ftrcovar.title='Trace Covar';
ftrcovar.hp=plot(trials,trials.^.5.*trcovar,getptype(2));
ftrcovar.name='Trace covar';
lblgraph(ftrcovar);
return;
%%

%use a discrete appoximation to estimate the kl distance
%pts on which to evaluate the pdfs
theta=[];
theta.t1=[(mparam.ktrue(1)-5:.01:mparam.ktrue+5)]';
theta.t2=[(mparam.ktrue(1)-5:.01:mparam.ktrue+5)];

theta.t1=theta.t1*ones(1,length(theta.t2));
theta.t2=ones(length(theta.t1),1)*theta.t2;

theta.t1=reshape(theta.t1,1,numel(theta.t1)); 
theta.t2=reshape(theta.t2,1,numel(theta.t2)); 

theta=[theta.t1;theta.t2];

klapprox=zeros(1,size(pbatch,2));
trials=zeros(1,length(klapprox));
for j=1:size(pbatch.m,2)
%for j=1:50
    fprintf('Trial %d \n',j);
    %compute the pdf for gbatch 2
    pdfbatch=mvgauss([theta],pbatch.m(:,j),pbatch.c{j});
    pdfonline=mvgauss([theta],ponline.m(:,j),ponline.c{j});
      
    %normalize
    pdfbatch=pdfbatch/(sum(pdfbatch));
    pdfonline=pdfonline/(sum(pdfonline));
      klapprox(j)=kldiv(pdfbatch,pdfonline);
      trials(j)=j-1;
  %  end
end

%return;
fklapp.hf=figure;
fklapp.xlabel='Trial';
fklapp.ylabel='KL distance';
fklapp.name='Kl Distance';
fklapp.title='Approximate KL distance batch and online gaussian approximations';
fklapp.hp=plot(trials,klapprox,getptype(2));
lblgraph(fklapp);