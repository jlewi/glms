%look at numerical instability in two dimensions
pg.c=[1 0; 0 2];
pg.m=[1;1];
param.plots.on=1;
param.lbound.on=0;
fisherucirc(pg,param);

%compute corresponding lambda to a certain y
[evec eigd]=svd(pg.c);
eigd=diag(eigd);
v=evec'*pg.m;
xopt=[0.30901699437495;0.95105651629515];
y=evec'*xopt;

c0=eigd(1);
c1=eigd(2);
v0=v(1);
v1=v(2);
y0=y(1);
y1=y(2);
lambda=(v0/v1*c1*y1-c0*y0)/(v0*y1/v1-y0);

exp(y'*v+.5*y'*diag(eigd)*y)*y'*diag(eigd)*y;

xopt=evec*y;


%***************
%plot x' pg.m = b as function of lambda 1
lambda=[-4:.01:4];
b=zeros(1,length(lambda));
y=zeros(length(pg.m),length(lambda));
for lindex=1:length(lambda)
    [finfo,y(:,lindex)]=flambda(lambda(lindex),eigd,v,1);
    b(lindex)=y(:,lindex)'*v;
end

figure;
plot(lambda,b);
xlabel('\lambda_1');
ylabel('b');