%9-29-06
%some testing for the rank 3 eigen update stuff
%
%We want to see if we can create a n-d quadratic form which gives
%the same result as our n-1 quadratic form which is orthogonal to the mean
d=4;

%generate a random d-dimensional covariance matrix
evecs=orth(rand(d,d));
eigd=ceil(rand(d,1)*10)/10;
%C=[3 2 1 4 ; 2 2 3 3; 4 1 3 1];
C=evecs*diag(eigd)*evecs';
%C=evecs*diag([1,1,1,1])*evecs';
[dquad.evecs, dquad.eigd]= svd(C);

%pick a direction which will define our n-1 quadratic form
mu=[1;1;1;1]
mu=mu/(mu'*mu)^.5;

%********************************************
%we need an orthonormal basis
%first directorion parralel to theta 
%basis=eye(d);
%for initial vectors select the n eigenvectors
%the basis matrix starts with n+1 columns
%because one of the eigenvectors may be parallel to mu but we don't know
%which one
basis=zeros(d,d+1);
basis(:,1)=mu/(mu'*mu)^.5;
basis(:,2:end)=evecs;
for j=2:d
    for k=1:j-1
        %project out the vectors
        basis(:,j)=basis(:,j)-(basis(:,k)'*basis(:,j))*basis(:,k);
    end
    %normalize
    basis(:,j)=basis(:,j)/(basis(:,j)'*basis(:,j))^.5;
end

%******************
%d-1 dimensional quadratic form
%basis ortogonal to mu
lquad.basis=basis(:,2:end-1);
[lquad.evecs lquad.eigd]=svd(morth'*C*morth);

%define an implicit function to evaluate the quad form
lquad.quad=@(x)((morth'*x)'*lquad.evecs*lquad.eigd*lquad.evecs'*(morth'*x));

%define our rank3 quadratic form
%drank3.quad=@(x)(x'*C*x-(x'*basis(:,1))*(basis(:,1)'*C)*x-x'*(C*basis(:,1))*(basis(:,1)'*x)+x'*(basis(:,1)'*C*basis(:,1))^2*basis(:,1)*(basis(:,1)'*x));
drank3.quad=@(x)(x'*C*x-(x'*basis(:,1))*(basis(:,1)'*C)*x-x'*(C*basis(:,1))*(basis(:,1)'*x)+x'*(basis(:,1)'*C*basis(:,1))*basis(:,1)*(basis(:,1)'*x));
%alternate form for quad 3 where rankone modifications are in symetric form
k=(basis(:,1))'*C*basis(:,1)/2*eye(d)+C;
w=k*basis(:,1);
%drank3.quadmat=C-(basis(:,1)+C*basis(:,1))*(basis(:,1)+C*basis(:,1))'+C*basis(:,1)*basis(:,1)'*C+(1+(basis(:,1)'*C*basis(:,1)))*basis(:,1)*(basis(:,1)');
drank3.quadmat=(w+basis(:,1))(w+basis(:,1))'/2-(w-basis(:,1))(w-basis(:,1))'/2;
drank3.quad2=@(x)(x'*drank3.quadmat*x);


%compute the diagnolization of our rank 3 quad form using Gu and Eisenstat
eigdsorted=eigd(end:-1:1);
evecs=evecs(:,end:-1:1);

%1st rank1 mod
z=evecs'*(basis(:,1)+C*basis(:,1));
rho=z'*z;
z=z/rho^.5;
rho=-rho;       %because we subtract the rank 1 mod
[drank3.eigd drank3.evecs]=rankOneEigUpdate(eigdsorted,z,evecs,rho);

%error checking
C1=C+rho*(evecs*z)*(evecs*z)'

%2nd rank1 mod
oldevecs=drank3.evecs;
oldeigd=drank3.eigd;
z=drank3.evecs'*(C*basis(:,1));
rho=z'*z;
z=z/rho^.5;
rho=rho;       %because we subtract the rank 1 mod
[drank3.eigd drank3.evecs]=rankOneEigUpdate(drank3.eigd,z,drank3.evecs,rho);
C2=C1+rho*(oldevecs*z)*(oldevecs*z)'

%3nd rank1 mod
oldevecs=drank3.evecs;
oldeigd=drank3.eigd;
z=drank3.evecs'*(basis(:,1)'*C*basis(:,1)+1)^.5*basis(:,1);
rho=z'*z;
z=z/rho^.5;
rho=rho;       %because we subtract the rank 1 mod
[drank3.eigd drank3.evecs]=rankOneEigUpdate(drank3.eigd,z,drank3.evecs,rho);
C3=C2+rho*(oldevecs*z)*(oldevecs*z)';