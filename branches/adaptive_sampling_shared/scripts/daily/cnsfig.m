%load('results\maxinfo\02_02\02_02_max2d_003.mat')
load('results\maxinfo\02_05\max3d_001.mat')
fentropy.hf=figure;
subsamp=10;

for index=0:simparam.niter
     mentropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(pmax.newton.c{index+1})));
 end

 
 for index=0:simparam.niter
     rentropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(prand.newton.c{index+1})));
 end
popts.sgraphs=0;


figure(fentropy.hf);
hold on;
ind=[1:10,10:10:100, 100:100:length(fentropy.x)];
fentropy.hp(1)=plot(fentropy.x(ind),mentropy(ind),'k.');
fentropy.hp(2)=plot(fentropy.x(ind),rentropy(ind),'kx');

hg=gca;
set(hg,'FontSize',20)
ylabel(fentropy.ylabel);
xlabel('Trial');
xlim([0 fentropy.x(end)]);

set(hg,'Xscale','Log');

set(fentropy.hp(1),'Marker','x');
set(fentropy.hp(1),'LineWidth',2);
set(fentropy.hp(1),'MarkerSize',10);
set(fentropy.hp(1),'Color',[0 0 0])

set(fentropy.hp(2),'Marker','.');
set(fentropy.hp(2),'LineWidth',2);
set(fentropy.hp(2),'MarkerSize',20);
set(fentropy.hp(2),'Color',[0 0 0])
hl(1)=plot(fentropy.x,mentropy,'k-');
set(hl(1),'LineWidth',2);


hl(2)=plot(fentropy.x,rentropy,'k:');
set(hl(2),'LineWidth',2);

fentropy.lbls{1}='Optimized';
fentropy.lbls{2}='Random';
legend(fentropy.hp, fentropy.lbls);

set(hg,'XTick',[1 100 1000])
