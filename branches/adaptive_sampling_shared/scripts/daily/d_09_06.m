subsamp=1;
simparam.niter=20000;
iidvar.truevar=zeros(mparam.klength,simparam.niter);
for trial=0:subsamp:simparam.niter
    %we need to rotate the covariance matrix so that x1 
    %is in the direction of theta
    %do an eigendecomposition of the matrix

    %compute the idd variance if it exists
    if exist('prand','var')
        [u,s]=svd(prand.newton1d.c{trial+1});
        %rotate it;
        rcovar=basis'*u*s*u'*basis;
        iidvar.truevar(:,trial/subsamp+1)=diag(rcovar);
    end
end