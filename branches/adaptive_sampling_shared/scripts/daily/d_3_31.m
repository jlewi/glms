%03_31
ffilt.hf=figure;
ffilt.xlabel='iteration';
ffilt.name='Output linear filtering';

figure(ffilt.hf)
hold on;
pind=1;
ffilt.lbls{pind}='Random Sampling';
ffilt.hp(pind)=plot(1:simparam.niter,sum(mparam.ktrue.*srrand.newton1d.y,1), getptype(pind,1));
plot(1:simparam.niter,sum(mparam.ktrue.*srrand.newton1d.y,1),getptype(pind,2));


pind=2;
ffilt.lbls{pind}='InfoMax';
ffilt.hp(pind)=plot(1:simparam.niter,sum(mparam.ktrue.*srmax.newton1d.y,1),getptype(pind,1));
plot(1:simparam.niter,sum(mparam.ktrue.*srmax.newton1d.y,1),getptype(pind,2));

lblgraph(ffilt);

ct=pmax.newton1d.c{1200}+mparam.diffusion;
y=srmax.newton1d.y(:,1200);
ct1201=ct-ct*y*y'*ct/(y'*ct*y);
sum(sum((ct1201-pmax.newton1d.c{1201}).^2))

ct=pmax.newton1d.c{1200};
y=srmax.newton1d.y(:,1200);
ct1201nd=ct-ct*y*y'*ct/(y'*ct*y);
sum(sum((ct1201-pmax.newton1d.c{1201}).^2))