%4-22-2008
%
%DO some analysis to try to figure out whats going on with the updater

clear all;
setpathvars;
setupfile='/home/jlewi/svn_trunk/allresults/bird_song/080422/bsglmfit_setup_002.m';
xscript(setupfile);


%plot the change in theta every thousand trials
v=load(getpath(dsets.datafile));
allpost=v.allpost;

interval=1000;

mlast=getm(allpost,0);

trials=[interval:interval:getlasttrial(allpost)];

dmu=zeros(1,length(trials));
for tind=1:length(trials)
   trial=trials(tind);
   
   m=getm(allpost,trials(tind));
   %compute the change
   dmu(tind)=sum((m-mlast).^2)^.5;
   
   mlast=m;
end
%%
fdmu=FigObj('name','Change in mu');
fdmu.a=AxesObj('nrows',1,'ncols',1, 'xlabel', 'trial','ylabel','change in mu');
hp=plot(trials,dmu,'.');
fdmu.a=addplot(fdmu.a,'hp',hp);

lblgraph(fdmu);

otbl={fdmu;{'script','donlineupdate.m';'Explanation',sprintf('The distance between the mean every %0.4g trials', interval')}};
onenotetable(otbl,seqfname('~/svn_trunk/notes/dmu.xml'));


%**************************************************************************
%evaluate the derivative at the strf
%**************************************************************************
%%
clear all;
setpathvars;
setupfile='/home/jlewi/svn_trunk/allresults/bird_song/080422/bsglmfit_setup_002.m';
xscript(setupfile);

%what bias term to use when computing the errors
bias=-4;
load(getpath(dsets.datafile));
%compute the derivative using the first 10000 trials evaluated at the STRF
dlpost=zeros(length(spiketrgavg.theta),1);

ntrials=10000;
obsrv=zeros(1,ntrials);
glmproj=zeros(1,ntrials);


stim=zeros(prod(getstimdim(bdata)),ntrials);
for trial=1:ntrials
    if (mod(trial,100)==0)
        fprintf('trial %d \n',trial);
    end
    
   [bdata sr]=gettrial(bdata,trial);
   stim(:,trial)=getdata(getinput(sr));
   glmproj(trial)=[getdata(getinput(sr));1]'*spiketrgavg.theta;
   obsrv(trial)=getobsrv(sr);
   dlpost=dlpost+(getobsrv(sr)-1/(1+exp(-glmproj(trial))))*[getdata(getinput(sr));1];
    
end

fdmu=FigObj('name','d log posterior');
fdmu.a=AxesObj('nrows',1,'ncols',1, 'xlabel', 'time(ms)','ylabel','frequency');

[t,f]=getstrftimefreq(bdata);
imagesc(t,f,reshape(dlpost(1:end-1),getstimdim(bdata)));
xlim([t(1) t(end)]);
ylim([f(1) f(end)]);
hc=colorbar;
lblgraph(fdmu);


%how many trials do we correctly predict
nerrors=obsrv-1./(1+exp(-(glmproj+bias)));
nerrors=round(abs(nerrors));
nerrors=sum(nerrors);

otbl={fdmu;{'script','donlineupdate.m';'derivative of bias term', dlpost(end);'Explanation',sprintf('The derivative of the log-likelihood for %04g trials, evaluated at the STA',ntrials);'Fraction of errors',sprintf('%0.3g with bias set to %0.3g',nerrors/ntrials,bias)}};
onenotetable(otbl,seqfname('~/svn_trunk/notes/dlogpost.xml'));

%%
%compute the average of the spike trigered inputs
ind=find(obsrv==1);
sta=sum(stim(:,ind),2)/length(ind);
fsta=FigObj('name','sta','height',8,'width',5)
fsta.a=AxesObj('xlabel','time(s)','ylabel','Frequency(hz)','nrows',2,'ncols',1);
setfocus(fsta.a,1,1);
imagesc(t,f,reshape(sta,getstimdim(bdata)));
lblgraph(fsta);
xlim([t(1) t(end)]);
ylim([f(1) f(end)]);
hc=colorbar;
fsta.a(1,1)=sethc(fsta.a(1,1),hc);
fsta.a(1,1)=title(fsta.a(1,1),'STA');
fsta.a(1,1)=ylabel(fsta.a(1,1),'Frequency');
fsta.a(1,1)=xlabel(fsta.a(1,1),'Frequency');

%compute and plot the component wise variance
vsta=var(stim(:,ind)');
setfocus(fsta.a,2,1);
imagesc(t,f,reshape(vsta,getstimdim(bdata)));

xlim([t(1) t(end)]);
ylim([f(1) f(end)]);
fsta.a(2,1)=title(fsta.a(2,1),'Variance of STA');
hc=colorbar;
fsta.a(2,1)=sethc(fsta.a(2,1),hc);
fsta.a(2,1)=ylabel(fsta.a(2,1),'Frequency');
fsta.a(2,1)=xlabel(fsta.a(2,1),'Frequency');

lblgraph(fsta);
sizesubplots(fsta);

otbl={fsta;{'script','donlineupdata.m';'explanation',sprintf('Top plot the STA of just the first %0.4g trials. Bottom plot the variance of each component of the STA.',ntrials)}};
onenotetable(otbl,seqfname('~/svn_trunk/notes/sta.xml'));

%%
%plot the stimuli on spikes
clear fstim;
allspikeind=find(obsrv==1);
nstim=min(10,length(spikeind));
ind=[];
while (length(ind)<nstim)
   ind=unique([ind ceil(rand(nstim)*length(allspikeind))]);
   ind=ind(1:nstim);
end
spikeind=allspikeind(ind);

otbl=[];
for j=1:nstim
   fstim(j)= FigObj('name','spike stim','height',5,'width',5);
   fstim(j).a=AxesObj('xlabel','time(s)','ylabel','Frequency(hz)','nrows',1,'ncols',1);

   imagesc(t,f,reshape(stim(:,spikeind(j)),getstimdim(bdata)));
xlim([t(1) t(end)]);
ylim([f(1) f(end)]);
   fstim(j).a=title(fstim(j).a,sprintf('Trial %0.3g',spikeind(j)));
        hc=colorbar;
        fstim(j).a=sethc(fstim(j).a,hc);
       lblgraph(fstim(j));
   
       %compute the number of trials since the previous spike and and the
       %number of trials to the next spike
       ind=find(allspikeind==spikeind(j));
       if (ind==1)
           ntoprevious=0;
       else
       ntoprevious=allspikeind(ind)-allspikeind(ind-1);
       end
       ntonext=allspikeind(ind+1)-allspikeind(ind);
       
       otbl=[otbl;{fstim(j),{'#trials to next spike',sprintf('%0.3g',ntonext);'#trials to previous spike',sprintf('%0.3g',ntoprevious')}}];
end
otbl=[otbl;{'Script','donlineupdate';'explanation','The stimuli on sevaral trials with spikes. I also compute the number of trials preceding and following this trial which did not have spikes. A value of 1 means there is a spike on the trial preceding or following this trial.'}];
onenotetable(otbl,seqfname('~/svn_trunk/notes/spikestim.xml'));
