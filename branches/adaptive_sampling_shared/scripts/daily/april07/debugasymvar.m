popt=imax.popt;
%compute the expected value
pjexp=zeros(size(jmat));
for j=1:length(popt)
    pjexp(:,:,j)=popt(j)*jmat(:,:,j);
end
ejexp=sum(pjexp,3);
ldetjexp=log(det(ejexp));