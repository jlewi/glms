%1-19-2007
%
% Take a look at the posterior by computing the posterior via brute force
% in 2d

clear all;
setpaths
simfiles{1}=fullfile(RESULTSDIR, 'numint/01_20/01_20_numint_002.mat'); %klength = 200d
load(simfiles{1});

mname='newton1d';
dset='rand';
eval(sprintf('pdata=p%s.%s;',dset,mname));
eval(sprintf('sr=sr%s.%s;',dset,mname));
eval(sprintf('simparam=simparam%s;',dset));
niter=simparam.niter;
%points on which to compute the posterior
tpts.theta1=[-4:.1:8];
tpts.n1=length(tpts.theta1);
tpts.theta2=[-8:.1:4];
tpts.n2=length(tpts.theta2);

%compute an 2xn array of all the thetapoints at which to compute it 
%this will speed things up
t1=tpts.theta1'*ones(1,tpts.n2);
t2=ones(tpts.n1,1)*tpts.theta2;

tpts.theta=zeros(2,tpts.n1*tpts.n2);
tpts.theta(1,:)=reshape(t1,[tpts.n1*tpts.n2,1]);
tpts.theta(2,:)=reshape(t2,[tpts.n1*tpts.n2,1]);

%compute the log likelihood of all of the observations
%trial(j).ll(i,k) - is the likelihood of the j'th trial under theta1(i),
%thetai(k)

trials=[];

for tr=1:niter
    fprintf('tr %d \n',tr);
    trials(tr).ll=zeros(tpts.n1,tpts.n2);
    %for j=1:tpts.n1
    %    for k=1:tpts.n2
    %        mu=simparam.glm.fglmmu(sr.y(:,tr)'*[tpts.theta1(j);tpts.theta2(k)]);
   %     trials(tr).ll(j,k)=log(simparam.glm.pdf(sr.nspikes(tr),mu)+eps);
   %     end
    %end
    mu=simparam.glm.fglmmu(sr.y(:,tr)'*tpts.theta);
    trials(tr).ll=reshape(log(simparam.glm.pdf(sr.nspikes(tr),mu)+eps),tpts.n1,tpts.n2);
end


ntosum=niter;


pll=0;
for tr=1:ntosum
pll=pll+trials(tr).ll;
end

fll.hf=figure;
fll.xlabel='\theta_1';
fll.ylabel='\theta_2';
fll.name='Log Likelihood';
fll.title=sprintf('%s: Log Likelihood %d trials',dset,ntosum);
contourf(tpts.theta1,tpts.theta2,pll',30);
colorbar;
lblgraph(fll);



%plot the angle of the stimuli
fangle.hf=figure;
fangle.name='Angle';
subplot(2,1,1);
plot(atan2(sr.y(1,:),sr.y(2,:)))
title(sprintf('%s: Stimuli',dset));

ylabel('angle');
subplot(2,1,2);
plot((sum(sr.y.^2,1)).^.5);
ylabel('magnitude');
xlabel('trial');
hline=findall(fangle.hf,'type','line');
set(hline,'linewidth',4);

hline=findall(fangle.hf,'type','Line');
set(hline,'LineWidth',5);
htext=findall(fangle.hf,'type','text');
set(htext,'FontSize',15);

fmean.hf=figure;
fmean.name='Mean';
subplot(2,1,1);
plot(atan2(pdata.m(1,:),pdata.m(2,:)))
ylabel('angle');
xlabel('trial');
title(sprintf('%s: Posterior Mean',dset));

subplot(2,1,2);
plot((sum(pdata.m.^2,1)).^.5);
ylabel('magnitude');

hline=findall(fmean.hf,'type','Line');
set(hline,'LineWidth',5);
htext=findall(fmean.hf,'type','text');
set(htext,'FontSize',15);

return

for tr=1:niter
fll.hf=figure;
fll.xlabel='\theta_1';
fll.ylabel='\theta_2';
fll.name='Log Likelihood';
contourf(tpts.theta1,tpts.theta2,trials(tr).ll');
colorbar;
lblgraph(fll);
end


x=zeros(2,500);
for j=1:500
    x(:,j)=choosestim(rstim);
end