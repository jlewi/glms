%1_12_07
%Check that infomax is producing stimuli with larger fisherinformations
%plot the expected fisher information for each trial
%This script requires the covariance matrix on each trial
fdata=fullfile(RESULTSDIR,'numint','01_10','01_10_numint_002.mat');
fdata=fullfile(RESULTSDIR,'numint','01_12','01_12_numint_001.mat');
fdata=fullfile(RESULTSDIR,'numint','01_13','01_13_numint_001.mat');
fdata=fullfile(RESULTSDIR,'numint','01_15','01_15_numint_003.mat');
load(fdata);

suffix={'max','rand'};
%eval(sprintf('data=p%s.newton1d;',suffix));
%eval(sprintf('sr=sr%s.newton1d;',suffix));
%trial=1;

%************************************************************************
%compute the predicted fisher information
%****************************************************************
glm=simparam.glm;
numint=simparammax.finfo.pproc.numint;
for sind=1:length(suffix)
    
eval(sprintf('p=p%s.newton1d',suffix{sind}));
eval(sprintf('sr=sr%s.newton1d',suffix{sind}));
eval(sprintf('simparam=simparam%s',suffix{sind}));

r=[];
r.pinfo=zeros(1,simparam.niter);
for tind=1:simparam.niter
    %compute the predicted fisher information
    %prior is the parameters used to choose the stim y
    %post is posterior after we measure the responses
    prior.m=p.m(:,tind);
    prior.c=p.c{tind};
    post.m=p.m(:,tind+1);
    post.c=p.c{tind+1};
    prior.entropy=p.entropy(tind);
    post.entropy=p.entropy(tind+1);
    
    nspikes=sr.nspikes(tind);
    stim=sr.y(:,tind);
    
    sigma=stim'*prior.c*stim;
    muproj=stim'*prior.m;
    r.pinfo(tind)=expdetint(muproj,sigma,glm,numint);   
    
    %compute the observed fisherinfo

    sigma=stim'*post.c*stim;
    muproj=stim'*post.m;
     %[dlde, dlldde]=d2glmeps(muproj,nspikes,glm);
     % clear('dlde');
      %s=1-dlldde*stim'*prior.c*stim;
      %r.oinfo(tind)=log(s);
      
      %change in entropy
      r.diffh(tind)=prior.entropy-post.entropy;
end
eval(sprintf('%s=r;',suffix{sind}));
clear('r');
end

ff.hf=figure;
ff.xlabel='trial';
ff.ylabel='Mutual Info';
ff.name='Mutual Information';
ff.hp=[];
ff.lbls={};
hold on;

pind=0;
for sind=1:length(suffix)
    eval(sprintf('r=%s',suffix{sind}));
    pind=pind+1;
    ff.hp(pind)=plot(1:length(r.pinfo),r.pinfo,getptype(pind));
    ff.lbls{pind}=sprintf('%s expected', suffix{sind});
    
    ptype=getptype(pind,1);
    ptype=[ptype,'--'];
    pind=pind+1;
    %ff.hp(pind)=plot(1:length(r.pinfo),r.oinfo,ptype);
    %ff.lbls{pind}=sprintf('%s observed', suffix{sind});
    ff.hp(pind)=plot(1:length(r.pinfo),r.diffh,ptype);
   ff.lbls{pind}=sprintf('%s observed', suffix{sind});
end
lblgraph(ff);
return
%************************************************************************
%load the various structures
%*********************************************************************
%finfo=simparammax.finfo;
glm=simparam.glm;
post.m=data.m(:,trial+1);
post.c=data.c{trial+1};
if isempty(post.c)
    %covariance matrix wasn't saved so compute it
    prior=mparam.pinit;
    [post,niter,ntime,a]=postnewton1d(prior,sr.y(:,1:trial),sr.nspikes,glm)
end
shist=[];
einfo=[];
mparam=mparam;
[xmax fopt]=choosestim(finfo,post,shist,einfo,mparam,glm);
sigma=xmax'*post.c*xmax;
muproj=xmax'*post.m/(post.m'*post.m)^.5;
finfoxmax=expdetint(muproj*(post.m'*post.m)^.5,sigma,glm,finfo.pproc.numint);   


%************************************************
%random stimulus   %randomly generate the stimulus
x=normrnd(0,1,mparam.klength,1);

%set the first d stimuli to be indepenent, orthogonal vectors (just choose unit vectors
xmag=x.*x;
xmag=sum(xmag,1);
normconst=mparam.mmag/xmag^.5;
xrand=normconst*x;
finforand=expdetint(xrand'*post.m,xrand'*post.c*xrand,glm,finfo.pproc.numint);   
muprand=xrand'*post.m/(post.m'*post.m)^.5;
srand=xrand'*post.c*xrand;
