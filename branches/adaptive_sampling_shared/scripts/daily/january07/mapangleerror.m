%plot the angle of the map estimate
function fangle=mapangle(fname)
[pmax,simparammax,mparam,sr]=loadsim('simfile',fname,'simvar','simmax');
[prand,simparamrand,mparam,sr]=loadsim('simfile',fname,'simvar','simrand');
%plot the angle of the stimuli
fangle.hf=figure;
fangle.name='Angle';
fangle.xlabel='trial';
fangle.ylabel='Error in Angle';
fangle.hp=[];
fangle.lbls={};
hold on;
%infomax
pind=1;
fangle.hp(pind)=plot(abs(atan2(pmax.m(2,:),pmax.m(1,:))*180/pi-atan2(mparam.ktrue(2),mparam.ktrue(1))*180/pi),getptype(pind));
fangle.lbls{pind}='Info. Max';

pind=pind+1;
fangle.hp(pind)=plot(abs(atan2(prand.m(2,:),prand.m(1,:))*180/pi-atan2(mparam.ktrue(2),mparam.ktrue(1))*180/pi),getptype(pind));
fangle.lbls{pind}='Rand.';

%pind=pind+1;
%fangle.hp(pind)=plot(ones(1,simparammax.niter)*atan2(mparam.ktrue(2),mparam.ktrue(1))*180/pi,getptype(pind));
%fangle.lbls{pind}='True';
lblgraph(fangle);

