
function [focase]=plotocases(varargin)

%default values
mname='newton1d';
dset='max';
%*****************************************
%default names
mname='newton1d';
data=[];
nsamps=1000;
minmag=1;           %minimum magnitude for the test stimuli
for j=1:2:nargin
switch varargin{j}
    case {'fname','filename'}
        fname=varargin{j+1};
    otherwise
        if exist(varargin{j},'var')
            eval(sprintf('%s=varargin{j+1};',varargin{j}));
        else
        fprintf('%s unrecognized parameter \n',varargin{j});
        end
end
end

load(fname);



eval(sprintf('pdata=p%s.%s;',dset,mname));
eval(sprintf('sr=sr%s.%s;',dset,mname));
eval(sprintf('simparam=simparam%s;',dset));
niter=simparam.niter;

focase.hf=figure;
focase.xlabel='trial';
focase.ylabel='stimulus';
focase.title='Stimulus method';
p=[sr.stiminfo{:}]
plot(log2([p.ocase]));
lblgraph(focase);

ocases=simparam.finfo.ocases;
fnames=fieldnames(ocases);
for j=1:length(fnames)
    text(0,log2(ocases.(fnames{j})),fnames{j});
end
htxt=findall(focase.hf,'type','text');
set(htxt,'fontsize',20);

ylim([-1 6]);