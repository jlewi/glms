%1-17-2007
%make a plot of the 2nd derivative of the log likelihood
%for the log(1+exp(epsilon)) nonlinearity

fddl.hf=figure;
fddl.xlabel='\epsilon';
fddl.ylabel='$-\frac{\partial^2 \log p(r|\epsilon)}{\partial \epsilon^2}$'
fddl.name='d2 log likelihood';
fddl.hp=[];
fddl.lbls={};
epsvar=[-10:.1:10];
obsrv=[0:1:10]';

glm=GLMModel('poisson',@glm1dlog);
%function to compute the true derivative
%this formula has been checked in my notes and mathematica
trueddf=@(epsilon,r)(exp(epsilon)*(r*(log(1+exp(epsilon))-exp(epsilon))-(log(1+exp(epsilon)))^2)/((log(1+exp(epsilon)))^2*(1+exp(epsilon))^2));

tdd=zeros(length(obsrv),length(epsvar));
for j=1:length(obsrv)
    for k=1:length(epsvar)
      tdd(j,k)=trueddf(epsvar(k),obsrv(j));
      [d ddg(j,k)]=d2glmeps(epsvar(k),obsrv(j),glm);
    end
end

[dl d2ll]=d2glmeps(epsvar,obsrv,glm);

%plot each observation on a separate trace
for j=1:length(obsrv)
    hold on;
    fddl.hp(j)=plot(epsvar,-d2ll(j,:),getptype(j));
    fddl.lbls{j}=sprintf('r=%d',obsrv(j));
end

fddl=lblgraph(fddl);
set(fddl.hylabel,'interpreter','latex');


