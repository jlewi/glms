
fstim.hf=figure;
fstim.xlabel='x_1';
fstim.ylabel='x_2';
fstim.hp=[];
fstim.lbls={};
fstim.title='Symettry about ktrue';
fstim.name='stim';
hold on;
trials=[(simparam.niter-200):simparam.niter];
fstim.title=sprintf('%s: \nTrials %d to %d',fstim.title,trials(1), trials(end));
fstim.hp(1)=plot(sr.y(1,trials),sr.y(2,trials),'.','MarkerSize',20);
fstim.lbls{1}='stim';
fstim.hp(2)=arrow([0;0],mparam.ktrue,'linewidth',2,'FaceColor','g','EdgeColor','g');
fstim.lbls{2}='ktrue';
%scale the map estimate to have same magnitude as true parameter
maps=normmag(pdata.m(:,end))*(mparam.ktrue'*mparam.ktrue)^.5;
fstim.hp(3)=arrow([0;0],maps,'linewidth',2,'FaceColor','r','EdgeColor','r');
fstim.lbls{3}='MAP(scaled)';

%orthonormal vector to mparam.ktrue
vorth=null(mparam.ktrue');
proj=vorth'*sr.y(:,trials);

ind=find(proj>0);
mpos=mean(proj(ind));
ind=find(proj<0);
npos=mean(proj(ind));

vpos=[mparam.ktrue(1)+mpos*vorth(1); mparam.ktrue(2)+mpos*vorth(2)];
%draw line indicating distance orthogonal to ktrue
plot([mparam.ktrue(1) vpos(1)], [mparam.ktrue(2) vpos(2)],'b--');
tpos=(vpos+mparam.ktrue)/2;
text(tpos(1),tpos(2), num2str(mpos),'FontSize',15);
%draw line indicating distance parallel to ktrue

vneg=[mparam.ktrue(1)+npos*vorth(1); mparam.ktrue(2)+npos*vorth(2)];
plot([mparam.ktrue(1) vneg(1)], [mparam.ktrue(2) vneg(2)],'c--');
tpos=(vneg+mparam.ktrue)/2;
text(tpos(1),tpos(2), num2str(npos),'FontSize',15);

axis square
lblgraph(fstim)

%*******************************************************************
%Symmetry about pmap
fsym.hf=figure;
fsym.xlabel='x_1';
fsym.ylabel='x_2';
fsym.hp=[];
fsym.lbls={};
fsym.title='Symettry about pmap';
fsym.name='sympmap';
hold on;
trials=[(simparam.niter-200):simparam.niter];
fsym.title=sprintf('%s: \nTrials %d to %d',fsym.title,trials(1), trials(end));
fsym.hp(1)=plot(sr.y(1,trials),sr.y(2,trials),'.','MarkerSize',20);
fsym.lbls{1}='stim';
fsym.hp(2)=arrow([0;0],mparam.ktrue,'linewidth',2,'FaceColor','g','EdgeColor','g');
fsym.lbls{2}='ktrue';
%scale the map estimate to have same magnitude as true parameter
pmap=normmag(pdata.m(:,end))*(mparam.ktrue'*mparam.ktrue)^.5;
fsym.hp(3)=arrow([0;0],pmap,'linewidth',2,'FaceColor','r','EdgeColor','r');
fsym.lbls{3}='MAP(scaled)';

%orthonormal vector to pmap
vorth=null(pmap');
proj=vorth'*sr.y(:,trials);

ind=find(proj>0);
mpos=mean(proj(ind));
ind=find(proj<0);
npos=mean(proj(ind));



vpos=[pmap(1)+mpos*vorth(1); pmap(2)+mpos*vorth(2)];
%draw line indicating distance orthogonal to ktrue
plot([pmap(1) vpos(1)], [pmap(2) vpos(2)],'b--');
txtpos=(vpos+pmap)/2;
text(txtpos(1),txtpos(2), num2str(mpos),'FontSize',15);
%draw line indicating distance parallel to ktrue

[pmap(1)+npos*vorth(1); pmap(2)+npos*vorth(2)];
plot([pmap(1) vneg(1)], [pmap(2) vneg(2)],'c--');
tpos=(vneg+pmap)/2;
text(tpos(1),tpos(2), num2str(npos),'FontSize',15);

axis square
lblgraph(fsym);