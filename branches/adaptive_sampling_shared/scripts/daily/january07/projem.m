%for the first klength stimuli
%compute what % of the stimulus energy is orthogonal of all previous stimuli

niter=10;
dset='rand';
eval(sprintf('sr=sr%s.newton1d;',dset));
oenergy=zeros(1,niter);
oenergy(1)=1;

ostim=sr.y(:,1:niter); %normalized stimuli
omag=sum(ostim.^2,1).^.5;
ostim=ones(mparam.klength,1)*(1./omag).*ostim;


for j=2:niter
    x=sr.y(:,j);
    %construct an orthogonal basis for stimuli already presented
%    B=orth(ostim(:,1:j-1));
B=orth(sr.y(:,1:j-1));
    %project vector onto it
    p=B'*x;
%    ee=(ones(mparam.klength,1)*x'*ostim(:,1:j-1)).*ostim(:,1:j-1);
  %  ee=sum(ee,2);
    %compute the vector orthogonal to the intial components
    ov=x-sum(B*p,2)
    oenergy(j)=ov'*ov/(x'*x);
%    oenergy(j)=oenergy(j)/(x'*x);
end

fpe.hf=figure;
fpe.xlabel='trial';
fpe.ylabel='Energy';
fpe.name='Stim Energy';
fpe.title=sprintf('%s stimuli',dset);
plot(oenergy);
lblgraph(fpe);