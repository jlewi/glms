%1_05_07
%
% Compare the results of info. max when we numerically integrate the
% expectation for the objective function compared to using our analytical
% approximation
fnum='/home/jlewi/cvs_ece/allresults/numint/01_04/01_04_numint_001.mat'; 
fanalytic='/home/jlewi/cvs_ece/allresults/numint/01_05/01_05_numint_001.mat';

dnum=load(fnum);
danal=load(fanalytic);

%******************************************************
%check the parameters are the sum
%************************************************
if (dnum.mparam.klength~=danal.mparam.klength)
    error('cant compare sims true parameters not the same');
end
if ~isempty(find((dnum.mparam.ktrue-danal.mparam.ktrue)~=0))
    error('cant compare sims true parameters not the same');
end
klength=dnum.mparam.klength;
niter=dnum.simparam.niter;
%****************************************************************
%PLot the M.S.E of the methods
fmse.hf=figure;
fmse.xlabel='Trial';
fmse.ylabel='M.S.E';
fmse.name='Mean Squared Error';
hold on;

nmse=dnum.pmax.newton1d.m-dnum.mparam.ktrue*ones(1,1+niter);
nmse=nmse.^2;
nmse=sum(nmse,1).^.5;
fmse.hp(1)=plot(0:niter,nmse,getptype(1,2));
fmse.lbls{1}='Numerical';



amse=danal.pmax.newton1d.m-dnum.mparam.ktrue*ones(1,1+niter);
amse=amse.^2;
amse=sum(amse,1).^.5;
fmse.hp(2)=plot(0:niter,amse,getptype(2,2));
fmse.lbls{2}='Analytical';

lblgraph(fmse);

%**************************************************************************
%plot the mean squared distance between the estimates
fdist.hf=figure;
fdist.xlabel='Trial';
fdist.ylabel='M.S.D';
fdist.name='Mean Squared Distance';
hold on;

d=dnum.pmax.newton1d.m-danal.pmax.newton1d.m;
d=d.^2;
d=sum(d,1).^.5;
fdist.hp(1)=plot(0:niter,d,getptype(1,2));
lblgraph(fdist)
