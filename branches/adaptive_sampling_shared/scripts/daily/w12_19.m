%12-19
%
%PLot the distance of the mean from true value using the posterior updates
%and the maximum updates
RDIR=fullfile(RESULTSDIR,'maxinfo','12_19');

postfname=fullfile(RDIR,'12_19_pdfs_002.mat');
maxfname=fullfile(RDIR,'12_19_max2d_003.mat');

postd=load(postfname);
maxd=load(maxfname);



fmdist.hf=figure;
fmdist.hp=[];
fmdist.xlabel='Iteration';
fmdist.ylabel='Distance';
fmdist.title='Distance From Mean to True Value';

fangle.hf=figure;
fangle.hp=[];

fangle.xlabel='Iteration';
fangle.ylabel='Angle';
fangle.title='Angle of Mean';
fangle.lbls={};

fkl.hf=figure;
fkl.hp=[];

fkl.xlabel='Iteration';
fkl.ylabel='Distance';
fkl.title='Kl Divergence';

pind=0;
for index=1:2
    pind=pind+1;
    switch index
        case 1
            pmethods=maxd.pmethods;
            mparam=maxd.mparam;
            simparam=maxd.simparam;
            pmethods.ekf.label='InfoMax';
            fangle.x=[0:simparam.niter];
            fmdist.x=[0:simparam.niter];
            fkl.x=[0:simparam.niter];
        case 2
            pmethods=postd.pmethods;
            mparam=postd.mparam;
            simparam=postd.simparam;
            pmethods.ekf.label='Random Inputs';
    end
    
        %***************************************************************
        %plot the kl distance
        %***************************************************************
        figure(fkl.hf);
        hold on;
        if isfield(pmethods.(mon{mindex}),'dk')
        fkl.hp(pind)=plot(fkl.x,pmethods.(mon{mindex}).dk(1,:),getptype(pind,1));
        plot(fkl.x,pmethods.(mon{mindex}).dk(1,:),getptype(pind,2));
        fkl.lbls{pind}=sprintf('Monte Carlo: %s', pmethods.(mon{mindex}).label);   
        end

        %***************************************************************
        %figure of the least mean squared distance of mean and true mean
        %*********************************************************
        mon={'ekf'};
        mindex=1;
        figure(fmdist.hf);
        hold on;
        
        lse=pmethods.(mon{mindex}).m-mparam.ktrue*ones(1,simparam.niter+1);
        lse=lse.^2;
        lse=sum(lse,1);
        lse=lse.^.5;
        fmdist.lbls{pind}=pmethods.(mon{mindex}).label;
        
        fmdist.hp(pind)=semilogy(fmdist.x,lse,getptype(pind,1));
        semilogy(fmdist.x,lse,getptype(pind,2));
        
        %***************************************************************
        %Plot the angle
        %*********************************************************
        if (mparam.tlength==2)
        mon={'ekf'};
        mindex=1;
        figure(fangle.hf);
        hold on;
        %angle with x-axis
        mag=pmethods.(mon{mindex}).m.^2;
        mag=sum(mag,1);
        mag=mag.^.5;

        angle=[pmethods.(mon{mindex}).m .* ([1;0]*ones(1,size(pmethods.(mon{mindex}).m,2)))];
        angle=sum(angle,1);
        angle=angle./(mag);
        angle=acos(angle)*180/pi;
        fangle.lbls{pind}=pmethods.(mon{mindex}).label;
        
        fangle.hp(pind)=plot(fangle.x,angle,getptype(pind,1));
        plot(fangle.x,angle,getptype(pind,2));
        end
end

%plot the true angle of the winner 
if (mparam.tlength==2)
        pind=length(fangle.hp)+1;
        
        mon={'ekf'};
        mindex=1;
        
        figure(fangle.hf);
        hold on;
        
        mag=mparam.ktrue'*mparam.ktrue;
        mag=sum(mag,1);
        mag=mag.^.5;
        angle=mparam.ktrue'*[1;0];
        angle=sum(angle,1);
        angle=angle./(mag);
        angle=acos(angle)*180/pi;
        fangle.lbls{pind}='True Value';
        
        fangle.hp(pind)=plot([fangle.x(1) fangle.x(end)],[angle angle],'k--');
        
end
 figure(fmdist.hf);
legend(fmdist.hp,fmdist.lbls);
xlabel(fmdist.xlabel);
ylabel(fmdist.ylabel);
title(fmdist.title);

figure(fangle.hf);
legend(fangle.hp,fangle.lbls);
xlabel(fangle.xlabel);
ylabel(fangle.ylabel);
title(fangle.title);

figure(fkl.hf);
legend(fkl.hp,fkl.lbls);
xlabel(fkl.xlabel);
ylabel(fkl.ylabel);
title(fkl.title);
