%make some plots for analyzing newton stability

d=138.2492;
c=
fs.hf=figure;
fs.xlabel='\Delta t';
fs.ylabel='f(\Delta t)';
x=[-10:.05:2];
plot(x,objpost(x,d,c,xmag,obsrv,glm));
lblgraph(fs)



%***********************************************************
%%
xmag=27.3180;
tw=1;
a=0;
anew=0;
niter=0;
newtonerr=inf;
tolerance=10^-6;
tic
while (newtonerr >tolerance);   
    niter=niter+1
    a=anew;
    unew=d+a*c/xmag;
    %evaluate the nonlinearity with respect to u    
    r=exp(unew);
    d1=r;
    d2=r;
    %non specific for u
    %la is the derivative of the log of the posterior as a function of a
    
    la=-a/xmag-tw*d1+obsrv/r*d1;
    dlda=-1/xmag+(-tw*d2-obsrv/(r^2)*d1^2+obsrv/r*d2)*c/xmag;
       
    anew=a-1/dlda*la;
    newtonerr=abs(anew-a);
    
end
toc