%the function whose value we want to compute via monte carlo methods.

glm=GLMModel('poisson','canon');
epsilon=[-3:.25:3];
r=[0:20];
sigma=10.^[-3:0];
fvals=zeros(length(r),length(epsilon),length(sigma));

%[x,y] = meshgrid(epsilon,r) ;


for sind=1:length(sigma)
for eind=1:length(epsilon)
    for rind=1:length(r)

        fvals(rind,eind,sind)=log2(jobs(glm,r(rind),epsilon(eind))^-1+sigma(sind));
    end
end
end


ff.hf=figure;
ff.width=6;
ff.height=3;

for sind=1:length(sigma)
   ff.a{sind}.ha=subplot(1,length(sigma),sind);
   contourf(epsilon,r,fvals(:,:,sind));
   ff.a{sind}.title=sprintf('\sigma=%d',sigma(sind));
   ff.a{sind}.xlabel='\epsilon';
   ff.a{sind}.ylabel='r';
end

lblgraph(ff);
        