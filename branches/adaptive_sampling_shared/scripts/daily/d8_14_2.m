%compute the Expected value of the fisher fisher
%information over the optimal distribution
%load './results/tracking/08_15/08_15_maxtrack_001.mat';
%load './results/tracking/06_03/06_03_gabor_001.mat';


%load ./results/tracking/08_15/08_15_maxtrack_002.mat
%load ../../adaptive_sampling/results/asym/08_15/08_15_maxtrack_002.mat
load ../../adaptive_sampling/results/tracking/08_24/08_24_maxtrack_001.mat
simparam.niter=simparammax.niter;
subsamp=100;
%starttrial=3*10^4;          %which trial to start with for computing 
%find the optimal p(x)
%assume its supported on a single point
em=mparam.mmag;

n=mparam.klength+mparam.alength;


%we need to definea new coordinate system 
%in which theta is the first axis
theta=[mparam.ktrue; mparam.atrue];
theta1=(theta'*theta)^.5;


%npts=200;
%xlrange=linspace(-em,em,npts);

%find the optimal support point for p
dhdx=@(x)(n*theta1+2/x+(n-1)*(-2*x)/(em^2-x^2));
xopt=fsolve(dhdx,.4);


%compute the fisher information matrix
jexp=eye(mparam.klength);
jexp(1,1)=mparam.tresponse*(exp(xopt*theta1)*xopt^2);
jexp(2:mparam.klength,2:mparam.klength)=mparam.tresponse*(mparam.mmag^2-xopt^2)/(mparam.klength-1)*exp(xopt*theta1)*eye(mparam.klength-1);

covarexp=inv(jexp);
%compute the expected log determinant of the fisher information
%over the optimal p
ljexp=mparam.tresponse*(log2(exp(xopt*theta1)*xopt^2)+(n-1)*log2(exp(xopt*theta1)*(em^2-xopt^2)/(n-1)));

%compute the asymptotic entropy
trials=1:simparam.niter;

entexp=1/2*n*(log2(2*pi*exp(1)))-1/2*ljexp-1/2*n*log2(trials);


fasym.hf=figure;
fasym.hp=[];

fasym.xlabel='Trial';
fasym.ylabel='Entropy';
fasym.title='';
fasym.name='asympcovar';
fasym.fname='Asymptotic Covariance';
fasym.axisfontsize=30;
fasym.lgndfontsize=30;
fasym.linewidth=2;
fasym.markersize=20;
fasym.semilogx=0;
hold on;
plot(0:simparam.niter,pmax.newton1d.entropy,getptype(1,1));
plot(1:simparam.niter,entexp,getptype(2,1));
legend('Actual','Predicted');
lblgraph(fasym);

fdiffent=fasym;
fdiffent.hf=figure;
fdiffent.xlabel='Trial';
fdiffent.ylabel='H(\theta)_{actual}-H(\theta)_{predicted}';
fdiffent.semilog=1;
hold on;

hl=plot(1:simparam.niter,pmax.newton1d.entropy(2:end)-entexp);
set(hl,'LineWidth',fasym.linewidth);
[fdir]=fileparts(ropts.fname);
fname=fullfile(fdir,sprintf('asymentropy_%03.g.eps',ropts.trialindex));
lblgraph(fdiffent);
%exportfig(fdiffent.hf,fname,'color','bw','bounds','tight');



figure;
covar=pmax.newton1d.entropy(2:end)+1/2*n*(log2(2*pi*exp(1)))+1/2*n*log2(trials);
plot(covar);
hold on;
plot([1 length(trials)],ones(1,2)*-ljexp,'r');



%********************************************
%we need an orthonormal basis
%first directorion parralel to theta 
basis=eye(mparam.klength);
basis(:,1)=mparam.ktrue/(mparam.ktrue'*mparam.ktrue)^.5;

for j=2:mparam.klength
    for k=1:j-1
        %project out the vectors
        basis(:,j)=basis(:,j)-(basis(:,k)'*basis(:,j))*basis(:,k);
    end
    %normalize
    basis(:,j)=basis(:,j)/(basis(:,j)'*basis(:,j))^.5;
end
%make a plot of the covariance norm

covarn=zeros(1,floor(simparam.niter/subsamp));
index=1;
for trial=0:subsamp:simparam.niter
    %we need to rotate the covariance matrix so that x1 
    %is in the direction of theta
    %do an eigendecomposition of the matrix
    [u,s]=svd(pmax.newton1d.c{trial+1});
    %rotate it;
    rcovar=basis'*u*s*u'*basis;
    %now construct an orthogonal basis the first of which is parallel to 
    diffc=(trial*rcovar-covarexp).^2;
    covarn(trial/subsamp+1)=sum(diffc(:))^.5;
end

fcovar.hf=figure;
fcovar.xlabel='Trial';
fcovar.ylabel='||nC_t-C_{opt}||_2';
fcovar.title='';
fcovar.name='Asymptotic Covariance';
fcovar.fname='normdcovar';
fcovar.axisfontsize=30;
fcovar.lgndfontsize=30;
fcovar.linewidth=8;
fcovar.markersize=20;
fcovar.semilogx=0;
fcovar.semilog=1;
fcovar.hp=[];
hold on;
fcovar.hp(1)=plot(0:subsamp:simparam.niter,covarn,getptype(1,2));
lblgraph(fcovar);
rdir=fileparts(ropts.fname);
exportfig(gcf,fullfile(rdir,sprintf('%s_%.3d.eps',fcovar.fname,ropts.trialindex)),'bounds','loose','color','bw');
