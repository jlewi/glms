%plot the mean firing as a function of \glmproj for various nonlinearities

ffreq.hf=figure;
ffreq.name='Mean Firing Rate';
ffreq.hp=[];
ffreq.lbls={};
ffreq.xlabel='\epsilon';
ffreq.ylabel='E(r|\epsilon)';
ffreq.semilog=1;
ffreq.axisfontsize=30;
%ffreq.lgndfontsize=20;
hold on;
glmproj=[-1:.1:10];

%log nonlinearity
pind=1;
ffreq.hp(pind)=plot(glmproj,glm1dlog(glmproj),getptype(pind));
%ffreq.lbls{pind}='log(1+exp(\epsilon))';
ffreq.lbls{pind}='log(1+exp(\epsilon))';

pind=pind+1;;
ffreq.hp(pind)=plot(glmproj,exp(glmproj),getptype(pind));
ffreq.lbls{pind}='exp(\epsilon)';


%pind=pind+1;;
%rpow=glmproj.^1;
%ind=find(glmproj<=0);
%rpow(ind)=0;
%ffreq.hp(pind)=plot(glmproj,rpow,getptype(pind));
%ffreq.lbls{pind}='\epsilon';
%ffreq.lbls{pind}='epsilon2';
ffreq=lblgraph(ffreq);

set(gca,'ytick',[1  100  10000]);

ylim([.1 10^5])
xlim([-1 10])
return;
%plot of glmproj on some trials
feps.hf=figure;
feps.name='glmproj on info. max';
feps.xlabel='glmproj';
feps.ylabel='counts';
hist(mparam.ktrue'*sr.y);
lblgraph(feps);