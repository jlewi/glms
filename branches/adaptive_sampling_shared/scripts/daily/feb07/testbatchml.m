%test the batch update for
%for non-exponential poisson models


 [ppoiss, simpoiss, mppoiss,srpoiss]=loadsim('simfile',fpoiss,'simvar','simrand');
 
%load the data
 [pdata, simparam, mparam,sr]=loadsim('simfile',fname,'simvar','simrand');
 
 
 post.m=zeros(mparam.klength,simparam.niter);
 post.c=cell(1,simparam.niter);
 post.dlpost=zeros(mparam.klength,simparam.niter);
 uobj=BatchML();
 
 
 prior.m=pdata.m(:,1);
 prior.c=pdata.c{1};
 pnew=prior;
 for j=51:100
     fprintf('Trial %d \n',j);
     postlast.m=mparam.ktrue;
   
     stim=sr.y(:,1:j);
     %obsrv=poissrnd(simparam.glm.fglmmu(mparam.ktrue'*stim)); %glm for the updater
     obsrv=sr.nspikes(1:j);
     %pnew=update(uobj,postlast,sr.y(:,1:j),simparam.observer.glm,mparam,sr.nspikes(1:j),[]);
    %pnew=update(uobj,postlast,sr.y(:,1:j),simparam.glm,mparam,sr.nspikes(1:j),[]);
    pnew=update(uobj,postlast,sr.y(:,1:j),simparam.glm,mparam,obsrv,[]);
     post.m(:,j)=pnew.m;
     post.c{j}=pnew.c;
     
     post.dlpost(:,j)=d2glm(post.m(:,j),mparam.pinit,obsrv,stim,simparam.glm);
     %check if the log likelihood is less than that of the true parameters
     post.ll(j)=computelogll(post.m(:,j),sr.y(:,1:j),obsrv,simparam.glm,mparam);
     post.truell(j)=computelogll(mparam.ktrue,sr.y(:,1:j),obsrv,simparam.glm,mparam);
     if (truell>ll)
        % error('not peak');
     end
 end
 
 llfun = @(mu)(-computelogll(mu,sr.y(:,1:j),sr.nspikes(1:j),simparam.glm,mparam));
optim=optimset('MaxFunEvals',50000);
 pmin.m=fminunc(llfun,mparam.ktrue,optim);
 pmin.dlpost=d2glm(pmin.m,mparam.pinit,sr.nspikes(1:j),sr.y(:,1:j),simparam.glm)

 
 %finite diff approximation of the gradient at ktrue
 fdiff.dt=.001;
 fdiff.lp=zeros(mparam.klength,1);
for k=1:mparam.klength
    d=zeros(mparam.klength,1);
    d(k)=fdiff.dt;
     fdiff.lp(k)=computelogll(mparam.ktrue+d,sr.y(:,1:j),sr.nspikes(1:j),simparam.observer.glm,mparam);
end
fdiff.dlpost=(fdiff.lp-truell')/fdiff.dt;

 return
 
 fnonlin.hf=figure;
 fnonlin.xlabel='\epsilon';
 fnonlin.ylabel='Spikes';
 fnonlin.name='Nonlinearity';
 
plot(mparam.ktrue'*sr.y,sr.nspikes,'.');
lblgraph(fnonlin);


%plot the actual spikes and the predicted spikes
fspikes.hf=figure;
fspikes.xlabel='\epsilon';
fspikes.ylabel='Spikes';
fspikes.name='Spikes';
hold on;

pind=1;
fspikes.hp(pind)=plot(simparam.glm.fglmmu(post.m(:,76)'*sr.y),getptype(pind));
fspikes.lbls{pind}='Predicted Mean';

%pind=pind+1;
%fspikes.hp(pind)=plot(simparam.glm.fglmmu(mparam.ktrue'*sr.y),getptype(pind));
%fspikes.lbls{pind}='True Mean';



pind=pind+1;
fspikes.hp(pind)=plot(sr.nspikes,getptype(pind));
fspikes.lbls{pind}='Observed';
lblgraph(fspikes);


fstim.hf=figure;
fstim.name='name';
fstim.xlabel='trial';
fstim.ylabel='magnitdue';
plot(sum(sr.y.^2,1).^.5);
lblgraph(fstim);

fcorr.hf=figure;
fcorr.name='corr';
fcorr.xlabel='trial';
fcorr.ylabel='correlation';
plot(normmag(post.m(:,60))'*normmag(sr.y))
lblgraph(fcorr)

return;
ll=computelogll(post.m(:,j),sr.y(:,1:j),sr.nspikes(1:j),simparam.observer.glm,mparam);
llfun=@(mu)(-computelogll(mu,sr.y(:,1:j),sr.nspikes(1:j),simparam.observer.glm,mparam));