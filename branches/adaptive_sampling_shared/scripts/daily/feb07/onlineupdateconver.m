%test whether the covariance matrix for the online update converges to the
%covariance matrix for the batch update.
%use the old data for which we measured the KL distance.

%fname='/home/jlewi/cvs_ece/results/adaptive_sampling/results/nips/11_15/11_15_lowdcos_001.mat';
%load(fname)

[pbatch, simparam, mparam,sr]=loadsim('simfile',fname,'varsuffix','max','oldver',1,'method','allobsrv');
%you will need to run fillinlowmem and then copy the results into variable
%pmonline

conline=pmonline.c;
cbatch=pbatch.c;


fdcovar.hf=figure;
fdcovar.xlabel='Trial';
fdcovar.ylabel='n^.5||C_{online}-C_{batch}||_2';
fdcovar.name='Online to Batch Convergence';
fdcovar.semilog=1;
mse=zeros(1,length(conline));
for t=1:length(conline)
    if (mod(t,10)==0)
        fprintf('t=%d \n',t);
    end
    dc=(conline{t}-cbatch{t}).^2;
    mse(t)=sum(dc(:))^.5;    
end

plot((1:t).^.5.*mse);
lblgraph(fdcovar);