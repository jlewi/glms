function lpost=computelogll(mu,stim,obsrv,glm,mparam)

   rmu=glm.fglmmu(mu'*stim);
% ll=sum(log(eps+glm.pdf(obsrv,rmu)))-(mu-mparam.pinit.m)'*inv(mparam.pinit.c)*(mu-mparam.pinit.m);
 %compute the log likelihood without using the pdf function
 ll=obsrv.*glm.fglmetamu(rmu)-glm.fglmnc(glm.fglmetamu(rmu));
 lprior=-(mu-mparam.pinit.m)'*inv(mparam.pinit.c)*(mu-mparam.pinit.m);
 lpost=sum(ll)+lprior;