%plot the values of sigmasq and muprojopt
%on each trial
fname='/home/jlewi/cvs_ece/allresults/20news/02_05/02_05_news_006.mat';
[pmax,simmax,mparam,srmax]=loadsim('simfile',fname,'simvar','simmax');

fopt=[];
fopt.hf=figure;
fopt.name='2dvalues';

fopt.a{1}.ha=subplot(2,1,1);
fopt.a{1}.ylabel='\mu^tx';
stiminfo=[srmax.stiminfo{:}];
fopt.a{1}.hp(1)=plot(1:simmax.niter,[stiminfo.dotmu]);

fopt.a{2}.ha=subplot(2,1,2);
fopt.a{2}.xlabel='iter';
fopt.a{2}.ylabel='\sigma^2';
stiminfo=[srmax.stiminfo{:}];
fopt.a{2}.hp(1)=plot(1:simmax.niter,[stiminfo.sigmasq]);

lblgraph(fopt);

