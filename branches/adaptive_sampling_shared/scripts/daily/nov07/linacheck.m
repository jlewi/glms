%check some linear algebra related to the bilinear model

%how man basis vectors
d=2;
%df - dimensionality in frequency space
df=3;
%dt - dimensionality in time
dt=5;

%randomly generate the basis vectors
kf=orth(randn(df,d));
kt=orth(randn(dt,d));

%randomly generate singular values
a=randn(d,1);

qtrue=kt*diag(a)*kf';

qtest=zeros(dt,df);
for i=1:d
    qtest=qtest+a(i)*kt(:,i)*kf(:,i)';
end