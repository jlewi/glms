%look at the pattern for the trace of A^n
%       n - power of trace to compute
%       d - imension of A matrix
%
%Explanation: This function checks whether the formula I worked out for
% tr((AJ)^n) where J is a diagonal matrix is correct
function trval=trpowpat(n,d)

%generate the sample matrix
A=rand(d,d);
A=A*A';

J=rand(d,1);

%since n is unknown we generate a string with the apppriate command
cmd='[i1';
for i=2:n
    cmd=sprintf('%s,i%d',cmd,i);
end
cmd=sprintf('%s]=ind2sub([%d',cmd,d);
for i=2:n
     cmd=sprintf('%s,%d',cmd,d);
end
cmd=sprintf('%s],[1:%d]);',cmd,d^n);

eval(cmd);

%command to concatenate indexes together
cmd='indexes=[i1''';
for i=2:n
   cmd=sprintf('%s i%d''',cmd,i) ;
end
cmd=sprintf('%s];',cmd);

eval(cmd);

%terms in the summation
terms=ones(size(indexes,1),1);

for p=1:n-1
   %convert to linear indexes
   lindex=sub2ind([d d],indexes(:,p),indexes(:,p+1));
   
   terms=terms.*A(lindex);
end

lindex=sub2ind([d d],indexes(:,n),indexes(:,1));
terms=terms.*A(lindex);

%compute the appropriate product of the J terms
terms=terms.*prod(J(indexes),2);

trval=sum(terms);

trueval=trace((A*diag(J))^n);
if (abs(trueval-trval)/trueval > 10^-10)
    error('Value for trace is not correct');
else
    fprintf('Success')
end
%we need to generate a string 
%n=20;
%f=@(x,n)(sum(((-1).^[0:n-1].*x.^[1:n]./[1:n])))