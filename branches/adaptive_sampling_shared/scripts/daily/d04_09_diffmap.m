%the purpose of this script 
%is to compare the accuracy of the MAP estimate
%using the full newtonian update or just using the gaussian posterior
%from our previous iteration
%to summarrize all previous results
%load './results/tracking/04_09/04_09_maxtrack_005.mat';

fmapdiff=[];
fmapdiff=[];
fmapdiff.title='Distance of Infomax MAPs newton1d and newton';
fmapdiff.name='Distance of MAPs Infomax';
fmapdiff.fname='distmap';
fmapdiff.hf=[];      %only create the figure if we have something to plot
fmapdiff.hp=[];
fmapdiff.x=[0:simparam.niter];
fmapdiff.xlabel='Iteration';
fmapdiff.ylabel='Distance';
fmapdiff.on=1;
fmapdiff.axisfontsize=16;
fmapdiff.lgndfontsize=16;

fmapdiff.hf=figure;
hold on
dist=(pmax.newton1d.m-pmax.newton.m).^2;
dist=sum(dist,1);
dist=dist^.5
plot(0:simparam.niter, ((0:simparam.niter).^.5).*dist,1));
lblgraph(fmapdiff);

%fit a 1/n plot
%x=1./(1:simparam.niter)^.5;
%y=sum(((pmax.newton1d.m-pmax.newton.m).^2)^.5,1);
%y=(2:end);
%p=polyfit(x,y,1);
%plot(
