%plot correlation between optimal stimulus and unit vector in direction of
%model parameter
figure;
uvec=mparam.ktrue/(mparam.ktrue'*mparam.ktrue)^.5;
c=(srmax.ekf1step.y).*(uvec*ones(1,simparam.niter));
c=sum(c,1);
r=(srrand.y).*(uvec*ones(1,simparam.niter));
r=sum(r,1);
hold on;
h1=plot(c);
h2=plot(r,'r-');

xlabel('Iteration');
ylabel('Projection of Stimulus');
legend([h1,h2],{'Optimal Stimulation','Random Stimulation'});

%double check stimulus magnitude is always 1
figure;

c=(srmax.ekf1step.y).^2;
c=sum(c,1);
r=(srrand.y).^2;
r=sum(r,1);
hold on;
h1=plot(c);
h2=plot(r,'r-');

xlabel('Iteration');
ylabel('Stim Magnitude');
legend([h1,h2],{'Optimal Stimulation','Random Stimulation'});


