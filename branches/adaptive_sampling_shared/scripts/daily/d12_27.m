%12_27_06
%script to play with the consistency of our online update method
clear all;
setpaths;
dfiles{1}.infile=fullfile(RESULTSDIR,'nips','11_15', '11_15_lowdcos_001.mat');
dind=length(dfiles);

load(dfiles{dind}.infile);
glm=simparammax.glm;
tolerance=10^-10;
wopts.all=1;
%restimate the posteriors on each trial
pinit=mparam.pinit;
%pinit.c=100*eye(mparam.klength,mparam.klength);
[post]=postnewton1d(pinit,srmax.newton1d.y,srmax.newton1d.nspikes,simparammax.glm,tolerance,wopts);

%make our update homogoneous by setting the observations to the mean on
%each trial
%nspikes=simparammax.glm.fglmmu(mparam.ktrue'*srmax.newton1d.y);
%[post,n,nn,a]=postnewton1d(pinit,srmax.newton1d.y,nspikes,simparammax.glm,tolerance,wopts);

%get the eigenvalues
evals=zeros(mparam.klength,simparammax.niter);
for i=1:(simparammax.niter)
    evals(:,i)=svd(post(i).c);
end


%**************************************************************************
%linearize the recursion for the error (mu_t-\param)
%do this so we can plot the error of the inhomogenous and the homogenous
%part.
% CODE below is specific for the exponential distribution with canonical
% parameter because I take dg/de to be 1
%**************************************************************************
nsteps=simparammax.niter;
strial=1000;    %trial at which we start
strial=1;
%1. Compute the matrices whose products we need to compute
for index=1:nsteps
    [mu,dmu]=glm.fglmmu(srmax.newton1d.y(:,index)'*mparam.ktrue);
    mats{index}=eye(mparam.klength)-dmu*post(index).c*srmax.newton1d.y(:,index)*srmax.newton1d.y(:,index)';
end

%compute the matrix products
for j=nsteps:-1:strial
    matprods{j,j}=mats{j};
    for k=j-1:-1:strial
        matprods{j,k}=matprods{j,k+1}*mats{k};
    end
end
%initial error
vinit=post(strial-1).m-mparam.ktrue;

%compute the homogeneous part
for t=strial:nsteps
    v(t).homo=matprods{t,strial}*vinit;
end
%compute the inhomogeneous part
for t=strial:nsteps
    v(t).inhomo=(srmax.newton1d.nspikes(t)-glm.fglmmu(srmax.newton1d.y(:,t)'*mparam.ktrue))*post(t).c*srmax.newton1d.y(:,t);
    for k=strial:t
        v(t).inhomo=v(t).inhomo+matprods{t,k}*(srmax.newton1d.nspikes(k)-glm.fglmmu(srmax.newton1d.y(:,k)'*mparam.ktrue))*post(k).c*srmax.newton1d.y(:,k);
    end
end

ferrs.hf=figure;
ferrs.hp=[];
ferrs.lbls={};
ferrs.xlabel='Trial';
ferrs.ylabel='Magnitude Squared';
hold on;

pind=1;
ferrs.hp(pind)=plot(strial:nsteps,sum((pmax.newton1d.m(:,strial+1:end)-mparam.ktrue*ones(1,nsteps-strial+1)).^2,1),getptype(pind,1));
plot(strial:nsteps,sum((pmax.newton1d.m(:,strial+1:end)-mparam.ktrue*ones(1,nsteps-strial+1)).^2,1),getptype(pind,2));
ferrs.lbls{pind}='True Error';

pind=pind+1;
ferrs.hp(pind)=plot(strial:nsteps,sum([v.homo].^2,1),getptype(pind,1));
plot(strial:nsteps,sum([v.homo].^2,1),getptype(pind,2));
ferrs.lbls{pind}='Homogenous Error';


pind=pind+1;
ferrs.hp(pind)=plot(strial:nsteps,sum([v.inhomo].^2,1),getptype(pind,1));
plot(strial:nsteps,sum([v.inhomo].^2,1),getptype(pind,2));
ferrs.lbls{pind}='Inhomogenous Error';


pind=pind+1;
ferrs.hp(pind)=plot(strial:nsteps,sum(([v.inhomo]+[v.homo]).^2,1),getptype(pind,1));
plot(strial:nsteps,sum(([v.inhomo]+[v.homo]).^2,1),getptype(pind,2));
ferrs.lbls{pind}='Total Error';

lblgraph(ferrs);
