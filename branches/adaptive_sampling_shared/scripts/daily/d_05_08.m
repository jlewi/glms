%**********************************************************************
% 5_08_2006
%************************************************************************
%testing of the rank 1 update problem.
n=100;

%generate some random values
d=sort(rand(n,1));
%truncate the eigenvalues at 3 pts of precision
d=floor(d*10^4)/1000;
evold=orth(rand(n,n));
m=evold*diag(d)*evold';
u=floor(rand(n,1)*10^4)/1000;

%try normalizing it so d and u are normal
%d=d/(d'*d)^.5;
%u=u/(u'*u)^.5;

%make 3 and 2 diagonal element the same
%do this to test deflation
d(3)=d(2);

%[eigd,delta]=mexRankOneEig(d,u);
[eigd,evecs]=rankOneEigUpdate(d,evold'*u,evold);

%compute the mse
mse=(evold*diag(d)*evold'+u*u')*evecs-evecs*diag(eigd);
mse=sum(mse.^2,1);

dmat=d*ones(1,n)-ones(n,1)*eigd';

%compare

%diffevals
diffevals=eigtrue-eigd;

[evecstrue eigdtrue]=eig(evold*diag(d)*evold'+u*u');
%in order to compare them we want to normalize and sort them.
mag=(sum(evtrue.^2,1)).^.5;
mag=ones(n,1)*mag;
evtrue=mag.*evtrue;
[eigtrue sind]=sort(diag(eigtrue));
evtrue=evtrue(:,sind);

[evtrue etrue]=svd(evold*diag(d)*evold'+u*u');
etrue=diag(etrue);
etrue=etrue(end:-1:1);
evtrue=evtrue(:,end:-1:1);




%try to figure out what delta is
dmat=zeros(n,n)
for i=1:n
    for j=1:i-1

        dmat(i,j)=(eigd(j)-d(i))/(d(j)-d(i))
    end
    for j=i:n-1
        dmat(i,j)=(eigd(j)-d(i))/(d(j+1)-d(i))
    end
    for j=n:n
        dmat(i,j)=eigd(j)-d(i)
    end
end