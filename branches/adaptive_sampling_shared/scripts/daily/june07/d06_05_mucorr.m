%to see why mean is blowing up
%compute the percentage of the energy in the direction of the mean
%we want to see if its blowing up in a direction orthogonal to the mean


dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_05','06_05_poissexp_001.mat');
dsets(end).lbl='info. max. heuristic';
dsets(end).simvar='simmax';


dind=1;[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);

mparam=simdata.extra.mparam;
ktrue=mparam.ktrue;

fmucorr.hf=figure;
fmucorr.xlabel='trial';
fmucorr.ylabel='\theta^T/||\theta||_2 \mu/||\mu||_2';
fmucorr.title='Fraction energy of mean along truth';
fmucorr.name='mucorr';
fmucorr.fontsize=20;

nktrue=normmag(ktrue);
mcorr=nktrue'*pdata.m;
mumag=sum(pdata.m.^2,1).^.5;
mcorr=mcorr./mumag;
fmucorr.hp(1)=plot(mcorr);
lblgraph(fmucorr);