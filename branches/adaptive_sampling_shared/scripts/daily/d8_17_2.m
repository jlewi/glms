%compute the expected fisher information using the parameter we've
%converged to
load ../../adaptive_sampling/results/tracking/08_17/08_17_maxtrack_001.mat

%find the optimal p(x)
%assume its supported on a single point
em=mparam.mmag;

n=mparam.klength+mparam.alength;


%we need to definea new coordinate system 
%in which theta is the first axis
theta=[pmax.newton1d.m(:,end)];
theta1=(theta'*theta)^.5;
theta1=mparam.ktrue'*pmax.newton1d.m(:,end)/(mparam.ktrue'*mparam.ktrue)^.5;
%npts=200;
%xlrange=linspace(-em,em,npts);

%find the optimal support point for p
dhdx=@(x)(n*theta1+2/x+(n-1)*(-2*x)/(em^2-x^2));
xopt=fsolve(dhdx,.4);

%compute the fisher information matrix
jexpap=eye(mparam.klength);
jexpap(1,1)=mparam.tresponse*(exp(xopt*theta1)*xopt^2);
jexpap(2:mparam.klength,2:mparam.klength)=mparam.tresponse*(mparam.mmag^2-xopt^2)/(mparam.klength-1)*exp(xopt*theta1)*eye(mparam.klength-1);

covarexpap=inv(jexpap);

%compare to asymptotic covariance using true mparam
diffc=(covarexpap-covarexp).^2;
ndiff=sum(diffc(:))^.5;