%
dsets=bsfourierhighd();
v=load(getpath(dsets(1).datafile));

trial=v.bssimobj.niter;

theta=getm(v.bssimobj.allpost,trial);

%filter out higher frequencies
nfcutoff=15;
ntcutoff=3;
theta=smooth(v.bssimobj.mobj,theta,nfcutoff,ntcutoff);

fh=plottheta(v.bssimobj.mobj,theta);