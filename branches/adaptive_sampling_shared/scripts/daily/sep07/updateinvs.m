%9-14-2007
%
%Regarding the proof of asymptotic consistency of our estimator,
%look at simple 2-d example to try to get some intution.

%Compute (pmap[t-1]-theta)*C(t-1)x[t](f(x'*theta)-f(x'*pmap[t-1])
simobj=simrand;
mobj=getmobj(simobj);
glm=getglm(mobj);
observer=getobserver(simobj);
def=zeros(1,getniter(simobj));

for t=1:getniter(simobj)
    
    stim=getstim(simobj,t);
    stimvec=getData(stim);
    theta=gettheta(observer);
   merr=(getpostm(simobj,t-1)- theta);
   ferr=(compexpr(mobj,stim,theta))-compexpr(mobj,stim,getpostm(simobj,t-1));
   def(t)=merr'*getpostc(simobj,t-1)*stimvec*ferr;
end

fsign.hf=figure;
fsign.xlabel='trial';
fsign.ylabel='sign';
plot(1:getniter(simobj),sign(def));
lblgraph(fsign);

%%
%numerically compute
%E_x x [f(x'theta)-f(x'pmap[t])] for each estimate
%expectation is with respect to x
%and multiply this by (pmap[t-1]-theta)*C(t-1)
x=getData(getstim(simobj,1:getniter(simobj)));

expx=zeros(size(x,1),getniter(simobj));
expdef=zeros(1,getniter(simobj));
for t=1:getniter(simobj)
    rmu=glm.fglmmu(getpostm(simobj,t-1)'*x);
    rtheta=glm.fglmmu(theta'*x);
    
    expx(:,t)=1/size(x,2)*sum(x.*(ones(size(x,1),1)*(rtheta-rmu)),2);
    
    expdef(t)=merr'*getpostc(simobj,t-1)*expx(:,t);
end