n=5;

mu=[1;zeros(n-1,1)];


eigd=sort(rand(1,2));
eobj=EigObj('eigd',[eigd(1) eigd(2)*ones(1,n-1)],'evecs',[mu null(mu')]);

nvals=1000;
mag=1000*rand(2,nvals);
mix=1000*rand(2,nvals);
theta=rand(n,nvals);
theta=normmag(theta);


c=zeros(1,nvals);

for j=1:nvals
    %make sure mix is sorted same as mag
    if (mag(1,j)<mag(2,j))
        mix(:,j)=sort(mix(:,j));
    else
        mix(:,j)=sort(mix(:,j),'descend');
    end

 c(j)=(mag(1,j)*theta(:,j)-mag(2,j)*mu)'*getmatrix(eobj)*(mix(1,j)*theta(:,j)-mix(2,j)*mu);
end
    

tms=zeros(5,nvals);

proju=mu'*theta;

tms(1,:)=eigd(2)*mix(1,:).*mag(1,:).*(1-proju.^2);
tms(2,:)=eigd(1)*mag(1,:).*mix(1,:).*proju.^2;

tms(3,:)=eigd(1)*mag(2,:).*mix(2,:);

tms(4,:)=-eigd(1)*mix(1,:).*mag(2,:).*proju;
tms(5,:)=-eigd(1)*mix(2,:).*mag(1,:).*proju;

stms=sum(tms,1);