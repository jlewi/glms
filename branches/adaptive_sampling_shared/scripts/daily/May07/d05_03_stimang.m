%5-30-2007
%TO see why info. max. i.i.d is worse than i.i.d. try plotting the
%information
%of the stimuli from the two methods
%plot the angle between successive stimuli
dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
% dsets(end).lbl='info. max. heuristic';
% dsets(end).simvar='simmax';
% 


 dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
 dsets(end).lbl='i.i.d.:10000';
 dsets(end).simvar='simrand';
 
 dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
 dsets(end).lbl='info. max. i.i.d:10000';
 dsets(end).simvar='simunif';

 


ndiff=5;
for dind=1:length(dsets)
[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
   dsets(dind).angle=zeros(ndiff,simdata.niter);
   dsets(dind).angle(:)=nan;
   normstim=normmag(sr.y);    
   for nd=1:ndiff

    ang=normstim(:,nd+1:end).*normstim(:,1:end-nd);
    dsets(dind).angle(nd,nd+1:end)=acos(sum(ang,1))*180/pi;
   end
end

fmi=[];
fmi.hf=figure;

hold on;

for nd=1:ndiff
    fmi.a{nd}.ha=subplot(ndiff,1,nd);
    fmi.a{nd}.title=sprintf('Difference %d ',nd);
    hold on;
for dind=1:length(dsets)
    
    fmi.a{nd}.hp(dind)=plot(dsets(dind).angle(nd,nd+1:end),getptype(dind));
    fmi.a{nd}.lbls{dind}=dsets(dind).lbl;
    
%    plot(trials,dsets(dind).mirand(:,trials));
end
end
lblgraph(fmi);