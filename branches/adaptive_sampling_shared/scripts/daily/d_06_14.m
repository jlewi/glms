post.m=[1;1];
post.c=[2 0; 0 1];
[finfo,xpts]=fonucirc(post,[],1);

%finfo=[finfo(50:end) finfo(1:49)];
figure;
hl=plot(finfo);
set(hl,'LineWidth',6);
set(gca,'FontSize',30);
set(gca,'xtick',[]);
set(gca,'ytick',[]);
%s
xlabel('\lambda_1')
hy=ylabel('F(x(\lambda_1))');
set(hy,'Rotation',0);
set(gca,'box','off');