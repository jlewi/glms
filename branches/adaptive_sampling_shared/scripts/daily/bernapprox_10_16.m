%bernoulli
%look at Guassian approximation of Bernoulli variance
truevar=@(x)(exp(-x)./(1+exp(-x)).^2);

sigma=1.5;
approxvar=@(x)(.25*(2*pi)^.5*sigma*normpdf(x,0,sigma));


fapprox.hf=figure;
hold on;
x=[-5:.05:5];
fapprox.hp(1)=plot(x,truevar(x),getptype(1,2));
fapprox.lbls{1}='True Variance';
fapprox.hp(2)=plot(x,approxvar(x),getptype(2,2));
fapprox.lbls{2}='Approximation';
lblgraph(fapprox);


