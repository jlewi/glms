%
em=1;
npts=100;
n=mparam.klength;
theta1=(mparam.ktrue'*mparam.ktrue)^.5;

%load parameters from files
em=mparam.mmag;
n=mparam.klength+mparam.alength;
theta1=(mparam.ktrue'*mparam.ktrue)^.5


xkrange=linspace(-em,em,npts);
xlrange=linspace(-em,em,npts);



%objective function we need to minimize
w=zeros(npts,npts);

%we set elements of fw to Inf
%because we want to ensure any points we don't compute
%don't accidently get chosen as the maximum
fw=ones(npts,npts)*Inf;

for k=1:npts
    %theres no reason to test the points twice
    %so l just ranges from k to npts
    for l=k:npts
        x=[xkrange(k);xlrange(l)];
      %compute the coefficients
      a=exp(x*theta1).*(x).^2;
      c=exp(x*theta1);
      d=em^2*c-a;

      fobj=@(w)(-(log(a'*[w;1-w])+(n-1)*log(d'*[w;1-w]/(n-1))));
      [w(k,l),fw(k,l)]=fminbnd(fobj,0,1);
    end
end
obj=-fw;
figure;
surf(xkrange,xlrange,obj);

%find the makimum
[v,kmaxind]=max(obj,[],1);
[v,lmax]=max(v,[],2);
kmax=kmaxind(lmax);

%make a color plot
%truncate values b\c we have log of 0
lbound=-2;
ind=find(obj<lbound);
objt=obj;
objt(ind)=lbound;
figure;
imagesc(xkrange,xlrange,objt);
colorbar;



