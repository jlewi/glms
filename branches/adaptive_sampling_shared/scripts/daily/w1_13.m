obsrv.twindow=100;
obsrv.n=99
theta=2;
x=log((obsrv.n/obsrv.twindow))/theta;
[pekf,pll]=ekfposteriorglm(prior,x,obsrv,@glm1dexp)

obsrv.twindow=100;
theta=2;
x=.0001;
obsrv.n=round(obsrv.twindow*exp(theta*x));
[pekf,pll]=ekfposteriorglm(prior,x,obsrv,@glm1dexp)

load('G:\jlewi\my_files\cvs\ece\adaptive_sampling\results\posteriors\01_12\01_12_pdfs_data_005.mat');
%make a plot of the error on each iteration of the true ML estimate on each
%trial and the approximate ML estimate on each trial using a single data
%point (this is just the mean of the log likeliood function from our 2nd
%order taylor approximation)
mle=log(sr.nspikes/mparam.tresponse).*(sr.y.^-1);
mlest=pmethods.ekf.ll.m;
indmle=find(isinf(mle));
mle(indmle)=0;

ind=find(isinf(mlest));
mlest(ind)=0;

err=mle-pmethods.ekf.ll.m;
ind=find(isinf(err));
err(ind)=0;
merr=sum(err)/(length(err)-length(ind));
figure;
plot(mle-pmethods.ekf.ll.m,'.')
title('Error of Approximate and True ML')
xlabel('Iteration')