d=2;
ux=normrnd(zeros(d,1),1);
theta=normrnd(zeros(d,1),1);
cx=rand(d,d);
cx=cx'*cx;


m=ux*ux'+cx*theta*ux'+(cx*theta*ux')'+cx*theta*theta'*cx;

u=[ux cx*theta ux cx*theta];
v=[ux ux cx*theta cx*theta];
