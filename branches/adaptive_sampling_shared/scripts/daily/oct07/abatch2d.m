%10-09-2007
%
%Try to solve a 2-d example for the optimal gaussian process
function [muopt,copt]=abatch2d
%first coordinate of x is the mean
%next 3 coordinates are the upper diagonal of the covariance matrix

ptheta=GaussPost('m',[1;0],'c',eye(2));
pmax=2; %maximum power
tk=2;
nparam=1+(tk^2+tk)/2
A=[];
b=[];
Aeq=zeros(1,nparam);
%indexes of the elements corresponding to the diagonal entries of the
%covariance
dind=([1:tk].^2+[1:tk])/2;
dind=1+dind;
Aeq(dind)=1;
beq=pmax;

lb=[-pmax^.5;-pmax*ones(nparam-1,1)];
lb(dind)=0;
ub=[pmax^.5;pmax*ones(nparam-1,1)];

%initial guess
minit=0;
cinit=eye(tk);
xinit=[minit; zeros(nparam-1,1)];


xind=2;
for cind=1:tk
    for rind=1:cind
        xinit(xind)=cinit(rind,cind);
        xind=xind+1;
    end
end

%to compute the objective function we evaluate an integral over ptheta
%we use monte carlo sampling. We only draw the samples once and reuse them
%on only calls to evaluate the functions
numsamples=10^6;

%generate samples of theta from a Gaussian
theta=mvgausssamp(getm(ptheta),getc(ptheta),numsamples);
    
optim=optimset('GradOBj','on');
%to check derivatives
optim=optimset(optim,'DerivativeCheck','on');

[xopt,fval,exitflag]= fmincon(@(vx)(-1*objfun(vx,ptheta,theta)),xinit,A,b,Aeq,beq,lb,ub,[],optim);
 oinfo=-1*fval;
 
muopt=xopt(1);
copt=zeros(tk,tk);
xind=2;
for cind=1:tk
    for rind=1:cind
        copt(rind,cind)=xopt(xind);
        xind=xind+1;
    end
end
%zero out the diagonal
cdiag=diag(copt);
copt=copt-diag(cdiag);
copt=copt+copt'+diag(cdiag);


%objective function
function [fval,df]=objfun(x,ptheta,theta)
tk=getdim(ptheta);
%unpack the parameters and call batchasymcanonobj
mu=x(1)*ones(tk,1);
cx=[x(2) x(3); x(3) x(4)];

px=GaussPost('m',mu,'c',cx);

[fval]=batchasymcanonobj(px,ptheta,theta);


%compute the gradients
[dmux, dcx]=gradbatchopt(px,ptheta,theta);

df=zeros(4);
df(1)=dmux;
df(2)=dcx(1,1);
df(3)=dcx(1,2);
df(4)=dcx(2,2);
