%create matrix of observations
%2nd derivative
dimstim=size(x,1);
d2ll=zeros(dimstim,dimstim);

for index=1:size(x,2);
    d2ll=d2ll-obsrv.twindow*exp(pekf.m'*x(:,index))*x(:,index)*x(:,index)';
end

for index=1:size(x,2);
    d2ll=d2ll-obsrv.twindow*x(:,index)*x(:,index)';
end

%measure correlation between successive stimuli
cstim=sum((srmax.ekf1step.y(:,2:end).*srmax.ekf1step.y(:,1:end-1)),1);

%measure correlation between successive stimuli
cstim=sum((srmax.newton.y(:,2:end).*srmax.newton.y(:,1:end-1)),1);