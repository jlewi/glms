%d_3_29_2
%look at results of optimization in 2-ds and compare plots of fisher
%information as function of f of the scaled parameter and compare it to
%plots as a funciton of the unit circle

%which iteration to look at
niter=10;

pmethods=pmax.newton;
sr=srmax.newton;
post.m=pmax.newton.m(:,niter+1);
post.c=pmax.newton.c{niter+1};

%construct the spike history
if (mparam.alength <= (niter))
            shist(:,1)=sr.nspikes(niter:-1:niter-mparam.alength+1);
        else
            shist(1:niter,1)=sr.nspikes(niter:-1:1);
end
        
%plot the fisher information on the unit circle for this trial
param.plots.on=1;
[fcirc,xcirc]=fonucirc(post,shist,mparam.mmag,param);

%get the optimium
[fcircopt,ind]=max(fcirc);
xcircopt=xcirc(:,ind);



%*************************************
%code fragment: you have to run rescalef.m
%*********************************************************
%take the points we got from rescale.m and compute fisher information at
%them
%then see if this matches result we get from rescaled computation
xpts=xpos;
finfox=zeros(1,size(xpts,2));
for xind=1:size(xpts,2)
    %full stim
    x=[xpts(:,xind);shist];
    g=exp([x'*post.m]);
    inner=x'*post.c*x;
    h=exp(.5*inner)*inner;
    
    finfox(1,xind)=g*h;

end