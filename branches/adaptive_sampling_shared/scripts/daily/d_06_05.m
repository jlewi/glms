


figure
nrows=2;
ncols=4;

outerpos=zeros(nrows*ncols,4);
pos=zeros(nrows*ncols,4);
tpos=zeros(nrows*ncols,4);
for row=1:nrows
    for col=1:ncols
          pindex=(row-1)*ncols+col;
        subplot(nrows,ncols,pindex)
      
        set(gca,'xtick',[]);
                set(gca,'ytick',[]);
                drawnow;
                refresh();
        outerpos(pindex,:)=get(gca,'OuterPosition');
        pos(pindex,:)=get(gca,'Position');
        tpos(pindex,:)=get(gca,'TightInset');
        set(gca,'OuterPosition',pos(pindex,:));
    
     if (col>1)
       %             opos(1)=pos(1)-tpos(pindex,1);
                    npos=pos(pindex,:);
                    npos(1)=pos(pindex-1,1)+pos(pindex-1,3);
                   set(gca,'Position',npos);
                     set(gca,'OuterPosition',npos);
                     get(gca,'TightInset')
           %         drawnow;
        %else
%        opos(1)=pos(1)-tpos(pindex,1);
        end
        %opos(2)=pos(2)-tpos(3);
        %opos(3)=pos(3)+tpos(pindex,1)+tpos(pindex,2);
        %opos(4)=pos(4)+tpos(pindex,3)+tpos(pindex,4);
        %set(gca,'OuterPosition',opos);
        %set(gca,'ActivePositionProperty','Position');

end
end
return;

figure;
hold on;
plot(pmax.newton1d.entropy,'g');
plot(pnohist.newton1d.entropy,'b');
plot(prand.newton1d.entropy,'r');

[pmax.newton1d.m(1:mparam.klength,end) mparam.ktrue pnohist.newton1d.m(1:mparam.klength,end) prand.newton1d.m(1:mparam.klength,end)]


subplot(nrows,ncols,1);