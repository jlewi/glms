n=100;
%x=linspace(-1,1,n);
x=linspace(.75,.85,n);
x=x';
dx=x(2)-x(1);

v1=dx*exp(x).*x.^2;
v2=dx*exp(x).*(1-x.^2);

%quadratic program
%multiply by -1 b\c quad prog finds minimum
H=-v1*v2';
f=zeros(n,1);

A=eye(n);
b=ones(n,1);
Aeq=dx*ones(1,n);
beq=1;
LB=zeros(n,1);
UB=ones(n,1);
popt=quadprog(H,f,A,b,Aeq,beq,LB,UB);


%see if the solution meets the criterion in Paninski's o4 paper
xvec=[x (1-x.^2).^.5]; %full stimulus

vi=zeros(n,1);
avgfinfo=zeros(2,2);
finfo=cell(n,1);
for i=1:n
    finfo{i}=exp(xvec(i,1))*xvec(i,:)'*xvec(i,:);
    avgfinfo=avgfinfo+finfo{i}*popt(i)*dx;
end

for ind=1:n
    vi(ind)=1/2*trace(inv(avgfinfo)*finfo{ind});
end


d=2;
b=1/d*ones(d,1);
lambda=[1;2];
m=50000;              %number of samples to draw

bexp=b;

for iter=1:100
%draw random x
%each column a different sample
xsamp=1+poissrnd(lambda*ones(1,m));

%compute the expectation via montecarlo
bexp=1/m*xsamp./(ones(d,1)*sum(xsamp.*(b*ones(1,m)),1));
bexp=b.*sum(bexp,2)
end