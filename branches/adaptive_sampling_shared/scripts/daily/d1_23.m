%make plot of sqrt * difference of means
figure;
n=1:simparam.niter;
plot(n.^.5.*(pmethods.ekf1step.m(2:end)-pmethods.batchg.m(2:end)));
xlabel('Iteration');
ylabel('n^.5(\mu_E_k_f_1_s_t_e_p-\mu_b_a_t_c_h)');
ylim([-.005 .005])

%make plot of sqrt * difference of means
figure;
n=1:simparam.niter;
plot(n.^.5.*(pmethods.ekf1step.m(2:end)-pmethods.gasc.m(2:end)));
xlabel('Iteration');
ylabel('n^.5(\mu_E_k_f_1_s_t_e_p-\mu_g_a_s_c)');

%
figure;
n=1:simparam.niter;
h1=plot(n.^.5.*(pmethods.ekfmax.m(2:end)-pmethods.gasc.m(2:end)));
set(h1,'LineWidth',4');

xlabel('Iteration');
ylabel('n^.5(\mu_E_k_f_m_a_x-\mu_g_a_s_c)');
%fit a function of n.^.5 to it
y=n.^.5.*(pmethods.ekfmax.m(2:end)-pmethods.gasc.m(2:end));
b=inv(((n.^.5)*(n.^.5)'))*(n.^.5)*y';
hold on;
h=plot(n,n.^.5*b,'k--')
set(h,'LineWidth',2);



%for multi di
figure;
n=1:simparam.niter;
msd=(pmethods.ekf1step.m(:,2:end)-pmethods.gasc.m(:,2:end)).^2;
msd=sum(msd,1);
plot(n.^.5.*msd);
xlabel('Iteration');
ylabel('n^.5||\mu_E_k_f_1_s_t_e_p-\mu_g_a_s_c)||^2');

%for multi di
figure;
n=1:simparam.niter;
msd=(pmethods.ekfmax.m(:,2:end)-pmethods.gasc.m(:,2:end)).^2;
msd=sum(msd,1);
plot(n.^.5.*msd);
xlabel('Iteration');
ylabel('n^.5||\mu_E_k_f_m_a_x-\mu_g_a_s_c)||^2');