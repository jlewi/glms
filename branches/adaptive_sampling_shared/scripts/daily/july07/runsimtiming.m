%having set up all the simulations for the timing
%we need to actually run the simulations

DAYDIR='070813';
niter=500*ones(1,1);
dfile=[];
diary('~/tmp/runsimtiming.txt');

dims=[100 250 500 625 750];
dims=[200 300 400];
dims=[150 350 450 550 1000];
dims=[100:100:500]
dims=[700:100:1000];
for dind=1:length(dims)

    dfile(dind).dim=dims(dind);
    dfile(dind).fname=fullfile(RESULTSDIR,'poissexp',DAYDIR,sprintf('simtime_%dd_%s_001.mat',dfile(dind).dim,DAYDIR));
    dfile(dind).niter=niter;
    dfile(dind).simvar=sprintf('max%d',dfile(dind).dim);
    if ~exist(dfile(dind).fname,'file')
        warning('file %s doesnot exist will not be able to do sim',dfile(dind).fname);

    else
        %check simulation variable exists
        if ~(varinfile(dfile(dind).fname,dfile(dind).simvar))
            warning('variable %s does not exist in file %s', dfile(dind).simvar, dfile(dind).fname);
        end
    end

end

%%
for ni=niter
    for dind=1:length(dfile)

        try
            simcontinue('fname',dfile(dind).fname,'simvar',dfile(dind).simvar,'ntorun',ni);
        catch
            fprintf('Error occured for dimensionality %d\n',dfile(dind).dim);
        end
    end

end
