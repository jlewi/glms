%daily script 1_15_2006
%
%Code below is used to check that formula for my estimate of the 
%likleihood using the taylor approximation is correct.
prior.m=2;
prior.c=1;

y=1;
obsrv.twindow=1;
obsrv.n=round(exp(2));

%compute the estimate;
[pekf,pll]=ekfposteriorglm(prior,y,obsrv,@glm1dexp);

%
mlest=(obsrv.n/obsrv.twindow).*exp(-prior.m*y)-1)*1./y+prior.m;



%***********************************************************************
%analyze/plot the maximum of the maximum likelihood estimate after each
%iteration. 
%compute the cumulative sum of the of the observations
clpobsrv=cumsum(pmethods.batchg.lpobsrv,1);
[lmax,lmind] = max(clpobsrv,[],2);
tmle=pmethods.batchg.thetapts(lmind);

figure;plot(tmle)
xlabel('Iteration');
ylabel('\theta=Max_\theta of Likelihood')