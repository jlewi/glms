
linfo=log(wopt*finfoy1*y1^2+(1-wopt)*finfoy2*y2^2);
linfo=linfo+(dims-1)*log(wopt*finfoy1*(magcon^2-y1^2)+(1-wopt)*finfoy2*(magcon^2-y2^2));

w=[0:.05:1];
linfo=zeros(1,length(w));

for wind=1:length(w)
    linfo(wind)=complinfo(w(wind),y1,y2,finfoy1,finfoy2,dims,magcon);
end


%*********************
%%
n=(finfoy1*y1^2-finfoy2*y2^2)*(w*(magcon^2-y1^2)*y1^2+(1-w)*finfoy2*(magcon^2-y1^2));
n=n+(dims-1)*(finfoy1*(magcon^2-y1^2)-finfoy2*(magcon^2-y2^2))*(w*finfoy1*y1^2+(1-w)*finfoy2*y2^2);


%%
%plot the suppport point as a function of \theta1
theta1=[1:20];
xopt=zeros(1,length(theta1));
magcon=1;
for xind=1:length(xopt)
       xopt(xind)=xoptasympower(100,theta1(xind),magcon);
end
figure;
plot(theta1,magcon^2-xopt.^2,'b.');
hold on;
%fit a acurve a/theta + b;

xlabel('theta1');