d=4;
mu=[1;1;1;1]
glmproj=.5;

magcon=1;

%C=[3 0 0 ; 0 2 0; 0 0 1];
evecs=orth(rand(d,d));
%C=[3 2 1 4 ; 2 2 3 3; 4 1 3 1];
C=evecs*diag([4,3,2,1])*evecs';
C=evecs*diag([1,1,1,1])*evecs';
[evecs, eigd]= svd(C);

%********************************************
%we need an orthonormal basis
%first directorion parralel to theta 
%basis=eye(d);
%for initial vectors select the n eigenvectors
%the basis matrix starts with n+1 columns
%because one of the eigenvectors may be parallel to mu but we don't know
%which one
basis=zeros(d,d+1);
basis(:,1)=mu/(mu'*mu)^.5;
basis(:,2:end)=evecs;
for j=2:d
    for k=1:j-1
        %project out the vectors
        basis(:,j)=basis(:,j)-(basis(:,k)'*basis(:,j))*basis(:,k);
    end
    %normalize
    basis(:,j)=basis(:,j)/(basis(:,j)'*basis(:,j))^.5;
end

%basis ortogonal to mu
morth=basis(:,2:end-1);

%***************************************************************
neig=diag(eigd)-(mu'*evecs).^2/(mu'*mu)^2.*(mu'*c*mu)

%remove from each eigenvector projection projeciton along mean
enom=evecs-(mu'*evecs)/(mu'*mu)*mu;
%scaling factor for the eigenvalues
sf=(1-(mu'*evecs).^2/(mu'*mu))^2;

seig=sf.*(diag(eigd))'

%*********************************************************************
[eorth eigdorth]=svd(morth'*C*morth);

[m sigma n]=svd(morth);
%xopt=basis(:,1)*glmproj;