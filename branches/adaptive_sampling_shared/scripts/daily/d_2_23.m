clear all
setpaths

pg.m=[1;1;1];
pg.c=[1 0 0; 0 2 0; 0 0 3]; 
[eigv,eigd]=svd(pg.c);
eigd=diag(eigd);

arrowidth=3;
ptypexpos=1;            %which plottype to use trajectories
ptypexneg=2;
lwidth=4;               %line widths
plotproj=0;             %wherther to plot plane indicating projection of a particular b
meshstyle='row';
view=[-1.1336    8.3962];

%compute grid points for computing circle
%
x=[];
y=[];

%for ibeta=1:length(beta);
%      y(beta)=r(ir)*cos(beta(ibeta))*cos(alpha(ialpha));
%end
%r=[0:.05:1];
%index=0;
x=[-1:.1:1];
y=[-1:.1:1];


%top half of severe
%note the way I plot it there are some points which are outside of the
%unit sphere and these get plotted on the zero axis.
for xind=1:length(x)
    for yind=1:length(y);
            if (x(xind)^2+y(yind)^2 <=1)
                z(yind,xind)=(1-x(xind)^2-y(yind)^2)^.5;
            else
                z(yind,xind)=0;
            end
    end
end

figure;
hold on;
axis square
hspherep=mesh(x,y,z);
%set(h1,'CDatamapping','Direct');
set(hspherep,'EdgeColor',[0 0 0]);
set(hspherep,'FaceColor','none');         %make grid transparent
set(hspherep,'MeshStyle',meshstyle);

hspheren=mesh(x,y,-z);
set(hspheren,'CDatamapping','Direct');
set(hspheren,'FaceColor','none');         %make grid transparent
set(hspheren,'EdgeColor',[0 0 0]);
set(hspheren,'MeshStyle',meshstyle);

set(gca,'view',view);
xlim([-3 3]);
ylim([-3 3]);
zlim([-3 3]);

%plot eigenvectors
for index=1:length(eigd)
    heig(index)=arrow(zeros(1,size(eigv,1)),eigd(index)*eigv(:,index));
    set(heig(index),'LineWidth',arrowidth);
    etext=sprintf('s_%0.1g',index-1);
    %scale the vector where we put text
    trad=(eigd(index)+.1);
    text(trad*eigv(1,index),trad*eigv(2,index),trad*eigv(3,index),etext);
end

%plot the mean
hma=arrow(zeros(1,size(eigv,1)),pg.m);
set(hma,'EdgeColor',[1 0 0]);
set(hma,'FaceColor',[1 0 0]);
set(hma,'LineWidth',arrowidth);
text(pg.m(1),pg.m(2),pg.m(3),'Mean');

%***********************************************
%plot the surface corresponding to a paritucular f
%that is projection onto the mean. 
b=.5;
xproj=x;
yproj=y;

%*************************************************************************
%get unit vector in direction of mean
if (plotproj~=0)
umean=pg.m/(pg.m'*pg.m)^.5;
for xind=1:length(xproj)
    for yind=1:length(yproj);
            %get z point by solving
            %[x;y;z]'*pg.m=0;
            %this gives us z assumping b=0
            %that is plane passes through origion
            zproj(yind,xind)=-pg.m(1:length(pg.m)-1)'*[x(xind);y(yind)]/pg.m(end);
    end
end
hproj=mesh(xproj,yproj,zproj)
set(hproj,'EdgeColor',[1 1 0]);
set(hproj,'FaceColor','none');  %make it transparent
end
%********************************************************
%**************************************************************************

%compute lambda
%and y(lambda)
%start at smallest eigenvalue
%build the domain of lambda
%we will plot each domain separately to avoid having matlab interpolate
%between singularities
lambda=[];
lambda=cell(1,length(eigd)+1)
index=0;
for eindex=length(eigd):-1:2
    index=index+1;
lambda{index}=[eigd(eindex)+.001:.01:eigd(eindex-1)-.001];
end
index=index+1;
lambda{index}=[eigd(1)+.001:.01:10*eigd(1)];
index=index+1;
lambda{index}=[eigd(end)-10*eigd(1):.01:eigd(end)-.001];

%plot each subdomain for lambda
for ldind=1:length(lambda)

ysign=1;
v=eigv'*pg.m;
lam=lambda{ldind};    
y=zeros(length(pg.m),length(lam));
for lindex=1:length(lam)    
    [finfo,y(:,lindex)]=flambda(lam(lindex),ysign,eigd,v,1);   
end
xpos=eigv*y;
xneg=eigv*-1*y;

%plot the trajectories

hxpos{ldind}=plot3(xpos(1,:),xpos(2,:),xpos(3,:),getptype(ptypexpos,2));
hxneg{ldind}=plot3(xneg(1,:),xneg(2,:),xneg(3,:),getptype(ptypexneg,2));
set(hxpos{ldind},'LineWidth',lwidth);
set(hxneg{ldind},'LineWidth',lwidth);

end
