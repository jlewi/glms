%load './results/tracking/08_03/08_03_maxtrack_001.mat'


load './results/tracking/08_03/08_03_maxtrack_002.mat'
trials=1+[1 500 1000 1500 2000]

fr.xlabel='i';
fr.ylabel='\theta_i';

for tr=trials
fr.hf=figure;
hold on;
fr.hp(1)=plot(1:mparam.klength,pmax.newton1d.m(:,tr),'g');
fr.hp(2)=plot(1:mparam.klength,prand.newton1d.m(:,tr),'b');
fr.hp(3)=plot(1:mparam.klength,mparam.ktrue,'r');

fr.lbls{1}='Optimal';
fr.lbls{2}='Random';
fr.lbls{3}='True'

fr.title=sprintf('Trial %g', tr-1);
lblgraph(fr);
ylim([-1.5 1.5]);
saveas(gcf,sprintf('~/tmp/08_04/%g.png',tr))
end
