%plot EKF max and Newton approach on same axes
RDIR=fullfile(RESULTSDIR,'maxinfo','03_22');
fnewton=fullfile(RDIR,'03_22_max2d_001.mat');
fekfmax=fullfile(RDIR,'03_22_max2d_002.mat');

load(fnewton);
dekf=load(fekfmax);

pmax.ekfmax=dekf.pmax.ekfmax;