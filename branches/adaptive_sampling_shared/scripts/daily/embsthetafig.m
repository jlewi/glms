%04-01-2006
%make  a plot of the distance of theta on each iteration from its initial value
%load('results\maxinfo\02_02\02_02_max2d_003.mat')
<<<<<<< embsthetafig.m
%load('results\maxinfo\02_05\max3d_001.mat')
fentropy.hf=figure;
subsamp=10;
if isfield(mparam,'klength')
mparam.tlength=mparam.klength;
end
for index=0:simparam.niter
     mentropy(index+1)=1/2*log2((2*pi*exp(1))^(mparam.tlength)*abs(det(pmax.newton.c{index+1})));
 end
=======
%load('results\tracking\03_31\03_31_maxtrack_003.mat')
>>>>>>> 1.2

<<<<<<< embsthetafig.m
 
 for index=0:simparam.niter
     rentropy(index+1)=1/2*log2((2*pi*exp(1))^(mparam.tlength)*abs(det(prand.newton.c{index+1})));
 end
popts.sgraphs=0;
=======
ftheta.hf=figure;
ftheta.hp=[];
ftheta.x=[0:simparam.niter];
ftheta.xlabel='Trial';
ftheta.ylabel='Distance';
ftheta.title='';
ftheta.on=1;
ftheta.fname='theta';
ftheta.name='Theta Distance';
ftheta.axisfontsize=30;

figure(ftheta.hf);
%tdist=zeros(1,simparam.niter);
theta0=mparam.ktrue(:,1)*ones(1,simparam.niter);
tdist=mparam.ktrue-theta0;
tdist=sum(tdist.^2,1).^.5;


hold on;
ftheta.hp(1)=plot(1:simparam.niter,tdist,'k-');
lblgraph(ftheta);

%plot(1:simparam.niter,tdist,'-');
set(ftheta.hp(1),'LineWidth',2)
%ftheta.hp(1)=plot(1:simparam.niter,tdist,'kx');
%set(ftheta.hp(2),'MarkerSize',10);
 

%set(hg,'XTick',[1 100 1000])
