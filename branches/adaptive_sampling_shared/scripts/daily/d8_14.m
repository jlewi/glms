em=1;

n=2;
theta1=1;
npts=200;
xlrange=linspace(-em,em,npts);

dhdx=@(x)(n*theta1+2/x+(n-1)*(-2*x)/(em^2-x^2));

xopt=fsolve(dhdx,.9);


%******************************
%now compute the derivative over xl
al=exp(xlrange*theta1).*xlrange.^2;
ak=exp(xopt*theta1).*xopt^2;
cl=exp(xlrange*theta1);
ck=exp(xopt*theta1);

dhdw=(ak-al)/ak+(n-1)*(em^2*ck-ak-em^2*cl+al)/(em^2*ck-ak);