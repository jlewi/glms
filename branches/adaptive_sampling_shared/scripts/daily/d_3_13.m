%3-13-2006
%try to solve scaling problem by reparemterizing the stimulus 
eigd=[3,.0098];
emean=[3*10^-16;1.62];

pg.c=diag(eigd);
pg.m=emean;
mmag=1;

%make a plot of the fisher information as a function of lambda
%so that we can see the singularity
param.plots.on=1;
fisherlambda(pg,[],param);

%compute the fisher information on the unit circle
fisherucirc(pg,param);

%now try the rescaling
%we vary the zero component
%each column a different vector
%To understand about the sign see notes 3-16 on scaling
%x will be the stimulus
%w is the sclaed version of v 
%mapping from w to v is [0,1] -> [-infty,0]
wstep=.01;
w=[0+wstep:wstep:1-wstep];

%compute the rescaled y as function of w
x=yrescaled(w, eigd,emean,mmag);

%compute the fisherinformation of the scaled points
param.plots.on=0;
param.xpts=x;
[fx,xpts]=fcurve(pg,param);

%compute theta for each xpt
theta=atan(abs(xpts(2,:)./xpts(1,:)));
%set the quadrant
%quadrant 2
ind=find(xpts(1,:)<0 & xpts(2,:)>0);
theta(ind)=pi-theta(ind);
%quadrant 3
ind=find(xpts(1,:)<0 & xpts(2,:)<0);
theta(ind)=pi+theta(ind);
%quadrant 4
ind=find(xpts(1,:)>0 & xpts(2,:)<0);
theta(ind)=2*pi-theta(ind);

%plot the fisher information
figure;
set(gcf,'Name','Scaled Fisher Info');
subplot(3,1,1);
plot(theta,fx);
xlabel('\theta');
ylabel('Fisher Info');

subplot(3,1,2);
plot(theta,xpts(1,:));
xlabel('\theta');
ylabel('x');

subplot(3,1,3);
plot(theta,xpts(2,:));
xlabel('\theta');
ylabel('y');
