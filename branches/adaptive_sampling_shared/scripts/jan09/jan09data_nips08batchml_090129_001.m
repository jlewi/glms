function [dsets]= jan09data_nips08batchml_090129_001() 
%********************************
%********************************
%Data set info
%********************************
% neuron=bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat 


dsets=[];
dind=1;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090129/bsglmfit_setup_001.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090129/bsglmfit_data_001.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090129/bsglmfit_mfile_001.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090129/bsglmfit_cfile_001.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090129/bsglmfit_status_001.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


