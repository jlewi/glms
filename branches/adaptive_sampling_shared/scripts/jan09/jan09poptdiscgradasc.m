%script to test object POptAsymDistDisc
%
%Compute the optimal StatDistDisc for a specific theta. 
%Then compare that design to an i.i.d gp
clear variables
setpathvars;

mp.glm=GLMPoisson('canon');
mp.mmag=1;
mp.klength=2;
mp.ktlength=2;
mobj=MParamObj(mp);

dimtheta=getparamlen(mobj);

%pick two orthonormal vectors for theta
thetamat=[[1;-1] [1;1]]/2^.5;
theta=thetamat(:);


mu=0;

%the simuli should have some magnitude 
%we generate the stimuli by sampling at some fixed angles along the circle
nstim=10;
%we add 1 to the nstim because the first 0 and 2*pi are the same so we
%throw out the last one
angles=linspace(0,2*pi,nstim+1);
angles=angles(1:end-1);

%rotate the angles by the angle of theta because we want to have one
%stimulus perfectly aligned with the true theta
tang=atan2(thetamat(2,1),thetamat(1,1));

smag=1;
stim=smag*[cos(angles+tang);sin(angles+tang)];



%choose the stimuli by sampling the ball
% nstim=3;
% stim=normrnd(0,1,1,nstim);
% 
% %append theta
% stim=[stim theta' null(theta')'];
% nstim=size(stim,2);

%make the stimuli evenly spaced out
% nstim=3;
% stim=linspace(-2,2,nstim);
sobj=POptAsymDistDisc('stim',stim,'ktlength',mp.ktlength);


post=GaussPost(zeros(getparamlen(mobj),1),eye(getparamlen(mobj)));

psuni=StatDistDisc('stim',stim,'ktlength',mp.ktlength);

%distribution with random marginal distribution
%and independence
pm=rand(nstim,1);
pm=pm/sum(pm);
psrind=StatDistDisc('stim',stim,'ktlength',mp.ktlength,'p',pm*pm');

%we want to make a copy of the distribution and not just a pointer
psinit=StatDistDisc('stim',stim,'ktlength',mp.ktlength,'p',psrind.p);


[funi]=exlogdetexss(sobj,post,psuni, mobj.glm, theta );
[frind]=exlogdetexss(sobj,post,psrind, mobj.glm, theta );

[funi frind]


%%

psopt=StatDistDisc('ps',psinit);

nsteps=100;
[fopt]=gradasc(sobj,psopt,nsteps,post,mobj.glm,theta);

[fopt funi frind]

savepopt=false;
if (savepopt)
%save it to a file
fname=FilePath('RESULTSDIR',fullfile('asympopt',datestr(now,'yymmdd'),sprintf('psopt_kl%g_kt%g.mobj',mobj.klength,mobj.ktlength)));
fname=seqfname(fname);

[fdir]=fileparts(getpath(fname));
recmkdir(fdir);
save(getpath(fname),'psopt','mobj','theta','nstim');
fprintf('Saved to: %s \n', getpath(fname));
end
