%function [dfiles, varargout]=jan09data2dgp
%
%Return value:
%   dfiles - the dataset files
%   simobj - an array of the simulation objects
function [dfiles, varargout]=jan09data2dgp
dsetfiles={};

simlist={'090119',1;'090119',2;'090119',3;'090119',4;'090119',5};

simlist={'090120',1;'090120',2};
sind=0;

%if we are running on one of the cluster nodes we
%need to specify the aboslute path
[s,hname]=system('hostname -s');

hname=deblank(hname);

l=min([7 length(hname)]);
if (strcmp(hname(1:l),'cluster'))
    dfiles=cell(1,size(simlist,1));
    for sind=1:size(simlist,1);
        fsetoutname=sprintf('gpsims_setup_%03g.m',simlist{sind,2});
        dfiles{sind}=fullfile('/home/jlewi/svn_trunk/allresults','asympopt',simlist{sind,1},fsetoutname);


    end

else
    dfiles=struct();
    for sind=1:size(simlist,1);
        fsetoutname=sprintf('gpsims_setup_%03g.m',simlist{sind,2});
        dsetfiles{sind}=fullfile('/home/jlewi/svn_trunk/allresults','asympopt',simlist{sind,1},fsetoutname);

        dnew=xfunc(dsetfiles{sind});
        dfiles=copystruct(dfiles,sind,dnew);
    end

    if (nargout>=2)
        %create an array of simobjects
        for dind=1:length(dfiles)
            v=load(getpath(dfiles(dind).datafile));
            simobj(dind)=v.simobj;
        end

        varargout{1}=simobj;
    end
end