%play around with block circulant matrices

k=5;

%indexes for a circulant matrix
cind=nan(k,k);
for j=0:k-1
    
   cind(j+1,:)=mod([0:k-1]-j,k)+1; 
end

%create a random symetric matrix m
m=randn(k,k);
%make m circulant
m=m(cind);

%create a second random symetric matrix
A=randn(k,k);
A=A*A';



%create circulant matrix with same sufficient statistic
s=zeros(1,k);
for j=1:k
   s(j)=sum(diag(A,-j+1)); 
end

a=sum(A,1);
Ac=a(cind);


%discrete fourier transform
%k roots of 1 raised to the 0:tk-1power
%each row is a different root
roots=ones(k,1)*exp(2*pi/k*[0:k-1]*(-1)^.5);
rpowers=[0:k-1]'*ones(1,k);
G=roots.^rpowers;


froots=ones(k,1)*exp(-2*pi/k*[0:k-1]*(-1)^.5);
frpowers=[0:k-1]'*ones(1,k);
F=froots.^frpowers;


