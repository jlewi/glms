%optimize the Gaussian process ignoring stationarity constraints
%using our lower bound.
%
%script to test object POptAsymDistDisc
%
%Compute the optimal StatDistDisc for a specific theta. 
%Then compare that design to an i.i.d gp

mp.glm=GLMPoisson('canon');
mp.mmag=1;
mp.klength=2;
mp.ktlength=1;
mobj=MParamObj(mp);

dimtheta=getparamlen(mobj);

%pick two orthonormal vectors for theta
theta=[1;-1];








post=GaussPost(theta,eye(getparamlen(mobj)));


sobj=POptAsymGPPCanonLB('powcon',2);


gp=optgp(sobj,post,mobj);