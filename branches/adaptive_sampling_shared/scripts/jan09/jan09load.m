setpathvars;

feature jitallow off
dbstop if caught
dbstop if error
setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080606','nips08_bslowd.m');
%setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080606','nips08_bsmod.m');
xscript(setupfile);
for dind=3:length(dsets)
    
    %load the simulation and then save it to a file
    [simobj]=SimulationBase('fname',getpath(dsets(dind).fname),'simvar',dsets(dind).simvar);
    
    [fdir,fbase]=fileparts(getpath(dsets(dind).fname));
    fnew=fullfile(fdir,[fbase '_converted.mat']);
    save(fnew,'simobj');
end