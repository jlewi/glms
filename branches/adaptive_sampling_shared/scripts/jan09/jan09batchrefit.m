%01-25-2009
%
%Try to figure out why the MAP on the batch estimates is blowing 

[dsets,simobj]=jan09data2dgpbsstrf();

%restimate the posterior on some trial
trial=2500;

sind=1;
postlast=getpost(simobj(sind).allpost,2137);
inp=[];
obsrv=[];
[post]=update(simobj(sind).updater,postlast,inp,simobj(sind).mobj,obsrv,simobj(sind),trial);