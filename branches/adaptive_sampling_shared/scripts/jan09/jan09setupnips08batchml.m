%01-27-2009
%Explanation:
%   Setup a BatchML simulation to fit an STRF to the nips08 data
%Use BatchML to 
%**************************************************************************
%
%Explanation: Setup a bunch of simulations for doing a sweep of the best
%cutoff frequencies
%
function [dsets,setupfile]=jan09setupnips08batchml()

dind=01;
[dnips]=nips08databstanhighd();

vnips=load(getpath(dnips(2).datafile));

%maxiter is the maximium number of iterations we want to allow in call to
%fminunc
allparam.maxiter=25;

%try decreasing the bin size and increasing the number of time bins
bdata=vnips.bssimobj.stimobj.bdata;

allparam.obsrvwindowxfs=bdata.obsrvparam.tnsamps;
allparam.stimnobsrvwind=bdata.stimparam.nobsrvwind;
allparam.freqsubsample=bdata.stimparam.freqsubsample;
allparam.model='MBSFTSep';
allparam.rawdatafile=FilePath('RESULTSDIR','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat');
allparam.windexes=vnips.bssimobj.stimobj.windexes;

allparam.alength=vnips.bssimobj.mobj.alength;

cprior=getc(vnips.bssimobj.allpost,0);
cprior=diag(cprior);
mobj=vnips.bssimobj.mobj;

allparam.prior.stimvar=cprior(mobj.indstim(1));
allparam.prior.shistvar=cprior(mobj.indshist(1));
allparam.prior.biasvar=cprior(mobj.indbias);



allparam.mparam.btouse=mobj.btouse;


%how many repeats of the data to use
%use all repeats
allparam.stimparam.nrepeats=10;

dind=dind+1;    
 dsets=BSSetup.setupfitpoiss(allparam);
%    dsets=copystruct(dsets,dind,dnew);
    
    
%****************************************************************
%create a single file describing all these datasets.
%****************************************************************
%output the information to an mfile

info.neuron=getrpath(allparam.rawdatafile);

sdir='~/svn_glms/adaptive_sampling/scripts/jan09';
sname=sprintf('jan09data_nips08batchml_%s.m',datestr(now,'yymmdd'));
setupfile=seqfname(fullfile(sdir,sname));
writeminit(setupfile,dsets,info);

