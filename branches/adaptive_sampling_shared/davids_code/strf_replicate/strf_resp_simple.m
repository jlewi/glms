%function [neuronpsth neuronmean nmat ntrials] =strf_resp_simple(current_cell,stimduration,psth_smooth,ftype,ampsamprate)
%	current_cell- this looks like a matrix of spike times. 
%       -Each row is the spike times on a different trial
%       -These are in msec.
%       -I.e the spike time should be the response bin in which we observe
%       the spike. I think each response bin is 1ms.
%   stimduration - The duration of the stimulus in msec.
%   psth_smooth - looks like the length of the filter to use for smoothing
%         the PSTH
%   ftype - controls the filter used for smoothing the PSTH
%   ampsamprate - how much to scale the PSTH
%Return value:
%   neuronpsth - PSTH of the responses computed from multiple trials
%   neuronmean - The average number of trials on which we observe a spike
%     in each response bin.
%   nmat - This is a matrix with the number of rows = number of trials,
%        number of columns = stimduration.
%        -Each row is vector of '0','1's containing the response on that
%        trial.

function [neuronpsth neuronmean nmat ntrials] = strf_resp_simple(current_cell,stimduration,psth_smooth,ftype,ampsamprate)

%****************************************
% Get rid of negative spikes and those after stimulus
%****************************************
neuron = reshape(current_cell,1,size(current_cell,1)*size(current_cell,2));
neuron = sort(neuron(find(neuron>0))); %sort and eliminate early spikes
neuron = round(neuron) + 1; %scale appropriately to match stimulus
neuron = neuron(find(neuron<stimduration)); %eliminate late spikes
ntrials = size(current_cell,1);

nmat = zeros(ntrials,stimduration);
ccell = round(current_cell);
ccell(find(ccell<0)) = 0;
ccell(find(ccell>stimduration)) = 0;
for i = 1:ntrials,
    nmat(i,ccell(i,find(ccell(i,:)~=0))+1) = 1;
end

%****************************************
% Create the binary spike train
%****************************************
neuron_binary = zeros(1,stimduration);
neurontemp = neuron;
dupe_flag = 1;
i = [];
dupes = 1:length(neurontemp);
while dupe_flag == 1,
    neurontemp = neurontemp(dupes);
    [xu i j] = unique(neurontemp);
    neuron_binary(xu) = neuron_binary(xu)+1;
    dupes = setdiff([1:length(neurontemp)],i);
    if isempty(dupes),
        dupe_flag = 0;
    end
end
neuronmean = neuron_binary/ntrials;

%****************************************
% Create the filter for convolution
% Calculate the PSTH
% Choose the appropriate smoothing filter
%****************************************
halfwinsize = floor(psth_smooth/2);
if ftype == 1,
    filt = ones(1,psth_smooth);
elseif ftype == 2,
    filtl = psth_smooth;
    xfilt = 1:filtl;
    mu = floor((filtl-1)/2)+1;
    sig = mu/3;
    filt = exp(-(xfilt-mu).^2./(2*sig^2))./(sig*sqrt(2*pi));
    filt = (filt-min(filt))/sum(filt-min(filt));
elseif ftype == 3,
    filt = hanning(psth_smooth)/sum(hanning(psth_smooth));
else
    filt = ones(1,psth_smooth);
end

neuronpsth = convn(neuronmean,filt,'same')*ampsamprate;