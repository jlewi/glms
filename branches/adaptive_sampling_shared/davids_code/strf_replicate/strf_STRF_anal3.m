function [strf] = strf_STRF_anal(CSR,nt,nband,Tol_val,CStime,CSfreq)

%****************************************
% Smooth cross-correlation by hanning window with size nt
%****************************************
w = hanning(nt);
CSRw = CSR.*repmat(w,1,nband)';

%****************************************
% Window and diagnalize CStime to tCStime
% This is effectively taking the autocorrelatio in time, and shifting it
% and centering it over all of the delays that i'm intereste in...300ms
% prior to 300ms after t=0
%****************************************
tCStime=zeros(nt);
for j=1:nt,
    tCStime(j,:)=CStime((nt+1-j):(nt*2-j));
end

%****************************************
% SVD of temporal AC matrix
%****************************************
[u1,s1,v1] = svd(tCStime);

%****************************************
% SVD of temporal AC matrix
%****************************************
[u2,s2,v2] = svd(CSfreq);

%****************************************
% Calculate the strf
% Flip so that the time course is now preceding
%****************************************
strf = v2*s2*(u2'*(v1*s1*(u1'*CSRw.')).');
strf = fliplr(strf);

%****************************************
% Implement a window to enforce causality
%****************************************
nt2 = (nt-1)/2;
xval = -nt2:nt2;
wcausal = (atan(xval)+pi/2)/pi;
strf = strf.*repmat(wcausal,nband,1);