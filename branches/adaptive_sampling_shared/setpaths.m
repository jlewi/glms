fprintf('\n\n setpaths.m \n\n');

%setpaths
PARENT=pwd;



%I should change this so you use genpath to get all the paths recursively
%based on the working directory.
%I should then remove any directories which don't have any m files.

%find the MATLAB directory
%by ascending up the hierarchy until we find it or we get to the root
%directory
MPARENT=pwd;


%if we are running on one of the cluster nodes we
%need to specify the aboslute path
[s,hname]=system('hostname -s');

hname=deblank(hname);

if (strcmp(hname(1:4),'node'))
  %we're on the cluster
  MATLABPATH='/Users/jlewi/svn_trunk/matlab';
else
    %change the matlab path for the code package for the Woolley group   
    %MATLABPATH will be replaced by setmatlabpath with an absolute path
   MATLABPATH=[pwd filesep '..' filesep 'matlab_shared'];
end

%cpaths=path;
%r=strmatch(MATLABPATH,cpaths,'exact');
   
%if isempty(r)

 %  addpath(MATLABPATH);
%end
  %we don't want to add relative paths to the MATLAB path because
  %then our MATLAB path will be dependent on us remaining the directory
  %from which set the path.
  %will depend on us not changing our working directory.
  %therefore we cd to MATLABPATH and call setmatlabpath
  %we then use abspath to 

  odir=pwd;
%add matlab paths
try
    cd(MATLABPATH)
    setmatlabpath
    cd(odir);
catch e
   cd(odir); 
end

%run script which sets that pathvariables
%this allows us to avoid resetting the path
setpathvars;
    
paths=[rmhiddendirs(TESTING), rmhiddendirs(GRAPHPATHS,pathsep), rmhiddendirs(SCRIPTPATHS,pathsep), rmhiddendirs(LIBPATHS,pathsep)];

%include the directory containing David's code
paths=[paths rmhiddendirs(genpath(fullfile(PARENT,'/davids_code')))];

%11-23-2007
%addpath already checks for duplicat paths so we don't have to
%turn warning about duplicat paths off
warning('off','MATLAB:dispatcher:pathWarning')
addpath(paths);
warning('off','MATLAB:dispatcher:pathWarning')






