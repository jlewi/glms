#include "mkl_vsl.h"
#include <stdlib.h>
#include "debug.h"
#include "poisson.h"     
#include <time.h>
#include <stdio.h>


spoissgen* createpoissongen(){
  unsigned long current_time = time(NULL);
  spoissgen* gen=malloc(sizeof(spoissgen));
  gen->stream=malloc(sizeof(VSLStreamStatePtr));
  vslNewStream(&(gen->stream), VSL_BRNG_MCG31, (unsigned int) current_time);

  return gen;
}
void closepoissongen(spoissgen* pgen){
  
 vslDeleteStream(&(pgen->stream));

}

int poissrnd(const spoissgen* gen, double lambda){
  isnullm(gen,"spoissgen* gen");
  int rndnum;
  int num=1;  //number to generate
  #ifdef DEBUG
  printf("lambda=%0.2g\n",lambda);
  #endif
  if(lambda <=0){
    lambda=1;
  }
  viRngPoisson(VSL_METHOD_IPOISSON_PTPE,gen->stream,num,&rndnum,lambda);

  //if (status !=VSL_ERROR_OK){
  //  emsg("poissrnd: error occured trying to generate poisson random number \n");
  //}
  return rndnum;
}
