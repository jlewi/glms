#include "model.h"
#include "optstruct.h"
#include "searchrange.h"
#include "matrixops.h"
#include "matrixmath.h"
#include "probability.h"
#include "simulation.h"
#
#ifndef OPTOVERLAP_INC
   #define OPTOVERLAP_INC 


//*****************************************************************
//decompose posterior
//Inpute:
//	sGauss* post - current posterior
//	sFullStim stim - st_FullStim - includes all information
//					 about the posterior most notably 
//Output Parameters:
//	sOptRegion* regions -an array of regions representing the
//				the parsing of the posterior. Note all values
//				are not computed here.
//
//The number of regions is = stim->nSubstims
//*****************************************************************************
sOptRegion* decompose(sGauss* post, ssubStim** stimregions, int nregions);


//******************************************************************************************
//finfo1neuron - 
//    yunique - stim for unique region - projected onto eigenvectors
//    sOptRegion* unique - pointer to structure with eigenvalues and mean of unique region
//    yshared - stim for shared region
double finfo1neuron(const sMatrix* yunique, const sOptRegion* unique, const sMatrix* yshared, const sOptRegion* shared);

#ifdef MATLAB
void fishermax(const sGauss* post, double mmag);
#endif


//void searchover_shared_energy(smParam* mparam, sharedrange, const sGauss* postu, const sGauss* posts, int nshared, double mmag, double* fopt)
//  input parameters:
//      mparam  - model parameters
//	smrange - specifies the range of magnitude constraints 
//			on the amount of energy in the shared vs. unique region
//			over which we conduct search
//             - this should actually be the square root
//             i.e ||x_uopt||_2 = erange
//	sOptRegion shared - pointer to the shared region of the stimulus space
//      sOptRegion unique - pointer to array of unique regions 
//      int        nunique- number of unique regions
//      double     mmag   - magnitude constraint on the stimulus
//  
//
//   output parameters
//      double*	 fopt  - fisher information at the optimium
//   Return value:
//      sFullStim* optStim - optimized stimulus   
sFullStim* searchover_unique_energy(smparam* mparam, sSearchRng* smrange, sOptRegion* shared, sOptRegion* unique, int nshared, int nunique,  double* fopt);

sFullStim* optimize(ssimparam* simparam, smparam* mparam, sGauss* post);

//****************************************************************************
// ystimrescale(
// Compute the rescaled stimulus
//  double     w - variable which parameterizes the rescaling

//   
// Return:
//    sMatrix* y - pointer to the rescaled stimulus
sMatrix* ystimrescale(double w, sMatrix* eigd, sMatrix* emean, double mmag);
//**************************************************************
//Testing
void testystimrescale(char* fname);


#endif
