#include <stdlib.h>
#include "matrixops.h"
#include "model.h"
#include "debug.h"
#include "datastruct.h"
#include "substim.h"
#include "stiminfo.h"
#include "neuron.h"

#ifdef MATLAB
#include "mat.h"
#include "matlabinter.h"
#endif
#ifndef MPARAM_INC
#define MPARAM_INC
#define freemparam freemParam

//****************************************************************
//structure to characterize the model
//**************************************************************************
typedef struct st_mParam{
  //no longer specify klength
  //klength can be returned by calling getklength
  //klength is implicitly stored as part of ktrue
  //int klength;		//length of stimulus coefficents
  //ktru is obsolete
  sMatrix* ktrue;      	// this will be a 2d array of true stimulus
			// coefficents
  // each column is stimulus coefficents for a different neuron.
  //stiminfo is obsolete
  sstimInfo* stiminfo;

  sneuron** neurons;
  int numneurons;
  double mmag;         //magnitude constraint

  double twindow;
} smParam, smparam;

//****************************************************************
//Contains structures and functions
//for our model
//******************************************************************
//  mmag - magnitude constraint
//smParam* createmParam(int klength, ssubStim** sregions, int nregions, double mmag);
smParam* createmParam(sneuron** neurons, int nneurons,double mmag, double twindow);

#ifdef MATLAB
smParam* mparamfrommxarray(mxArray* mxparam);
#endif
//set ktrue

void freemParam(smParam* mparam);

void printmparam(smParam* mparam);
const sstimInfo* conststiminforef(smParam* mparam);

//get the length of the spatial filter
int getmparamstimlength(smParam* mparam);

double getmmag(smParam* mparam);
sneuron* getneuron(smparam* mparam, int nindex);
int getnumneurons(smparam* mparam);

//Explanation:
//      This constructs a full stimulus object from an array of subStim objects
//      which describe each region of the stimulus, and an array of vectors which specify
//      the value of the stimulus at these reigion
//
//     All data is copied to the new location in memory so that no chance for someone else
//     deleting memory it depends on
sFullStim* constructFullStim(smParam* mparam, ssubStim** subStims, sMatrix** x,int nsubstim);
sFullStim* fullstimfromvec(smparam* mparam, smatrix* vec);
#endif
