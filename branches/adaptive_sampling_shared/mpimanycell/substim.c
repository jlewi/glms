#include <stdlib.h>
#include "matrixops.h"
#include "model.h"
#include "debug.h"
#include "datastruct.h"
#include "string.h"

#ifdef MATLAB
#include "mat.h"
#include "matlabinter.h"

#endif

ssubStim* createsubStim(int kstart, int klength, int* neurons, int nneurons){
  
  ssubStim* substim=malloc(sizeof(ssubStim));
  
  if (kstart <0){
    emsg("createsubStim: kstart must be > 0 \n");
  }
  if (klength <0){
    emsg("createsubsim:klength must > 0 \n");
  }
  if (neurons==NULL){
    emsg("createsubstim: neurons is null array \n");
  }

  if (nneurons <0){
    emsg("createsubstim: nneurons is <0 \n");
  }
  substim->kstart=kstart;
  substim->klength=klength;
  
  substim->nneurons=nneurons;
  substim->neurons=copyintarray(neurons,nneurons);

  return substim;
}


//*********************************************************************
//copy a SubStim object
//*********************************************************************
ssubStim* copysubStim(const ssubStim* substim){
  if (substim==NULL){
    emsg("copysubstim: tried to copy null object \n");
  }
  ssubStim* newstim=malloc(sizeof(ssubStim));

  newstim->kstart=substim->kstart;
  newstim->klength=substim->klength;

  if (substim->neurons==NULL){
    wmsg("copysubstim: neurons is null pointer \n");
    newstim->nneurons=0;
    newstim->neurons=NULL;
  }
  else{
    newstim->nneurons=substim->nneurons;
    newstim->neurons=copyintarray(substim->neurons,substim->nneurons);
    
  }

  return newstim;
}


//free a subStim object
void freesubStim(ssubStim* substim){
	if (substim==NULL){
		emsg("freesubstim: tried to free NULL pointer \n");
		return;
	}  	
	if (substim->neurons==NULL){
		emsg("freesubstim: tried to free NULL pointer substim->neurons \n");
	}
	free(substim->neurons);
	
	free(substim);
}	
	
//validate an array of substims
//that is make sure none of the regions overlap
//and that together all substims = the full stimulus
int validsubstims(ssubstim** substims , int nsubstims, int klength){
  int* iscovered=malloc(sizeof(int)*klength);
  int index=0;

  int isvalid=TRUE;
  for (index=0;index<klength;index++){
    iscovered[index]=FALSE;
  }

  //loop through all substims and set is covered
  int sindex;  ///index into stimulus
  int isub;    //index the substimulus
  for (isub=0; isub<nsubstims;isub++){
    for (sindex=substims[isub]->kstart; sindex<(substims[isub]->kstart+substims[isub]->klength);sindex++){
      if (iscovered[sindex]==TRUE){
	emsg("validsubstims: substimuli stimuli overlap \n");
	emsg("validsubstims: invalid set of substimuli \n");
	printf("validsubstims: overlap at element %d \n",sindex);
	isvalid=FALSE;
      }
      else{
	iscovered[sindex]=TRUE;
      }
    }
  }

  //make sure all elementes are covered
  for (index=0;index<klength;index++){
    if(iscovered[index]==FALSE){
	emsg("validsubstims: set of substimuli do not cover stimulus \n");
	printf("validsubstims: element %d  not covered\n",index);
    }
  }

  return isvalid;
}

#ifdef MATLAB
//    mxstimreg -a structure array of substimuli
//    sindex - which element of structure array we get
ssubstim* substimfrommxstruct(mxArray* mxstimreg,int sindex){
 //variables we need to load from mxparam
  int kstart;
  int klength;
  int* neurons;
  int  nneurons;

  //fieldnames which must be provided
  enum FNAMES {f_kstart, f_klength, f_neurons};

  //create an array to keep track of whether field was provided
  int fexists[3];
  int NNEEDED=3;   //how many elements in FNAMES are there
  fexists[0]=FALSE;
  fexists[1]=FALSE;
  fexists[2]=FALSE;


  bool isstruct=mxIsStruct(mxstimreg);
  if (isstruct==0){
    emsg("substimfrommxarray: mxstimreg is not a structure \n");
    return NULL;
  }

  int numfields=mxGetNumberOfFields(mxstimreg);

  //stor name of field
  const char* fname;
  //********************************************************************
  //loop through all the fields in the structure and proces them
  int findex;
  for (findex=0;findex<numfields;findex++){
      fname=mxGetFieldNameByNumber(mxstimreg, findex);

    if(strcmp(fname,"kstart")==0){
      fexists[f_kstart]=TRUE;
      mxArray* mxdata=mxGetField(mxstimreg,sindex,fname);
      kstart=*((double *)(mxGetData(mxdata)));      
    }
    
    if(strcmp(fname,"klength")==0){
      fexists[f_klength]=TRUE;
      mxArray* mxdata=mxGetField(mxstimreg,sindex,fname);
      klength=*((double *)(mxGetData(mxdata)));      
    }

     if(strcmp(fname,"neurons")==0){
      fexists[f_neurons]=TRUE;
      mxArray* mxdata=mxGetField(mxstimreg,sindex,fname);
      nneurons=mxGetNumberOfElements(mxdata);

      
      neurons=malloc(sizeof(int)*nneurons);
      
      double* data=mxGetPr(mxdata);
      for (int nindex=0;nindex<nneurons;nindex++){
	neurons[nindex]=(int)(*(data+nindex));
      }
      
    }

  }//end loop over all fields


  //check all fields were provided
  int isvalid=TRUE;

  //  fname=malloc(100);
  for (findex=0;findex<NNEEDED;findex++){
    
    switch (findex){
    case f_kstart:
      fname="kstart";
      break;
    case f_klength:
      fname="klength";
      break;
    case f_neurons:
      fname="neurons";
      break;
    }
    if (fexists[findex]!=TRUE){
      isvalid=FALSE;
      fprintf(stderr,"substimfrommxstruct: error mising field %s \n", fname);
    }
  }


  if (isvalid==TRUE){
    ssubstim* substim= createsubStim(kstart, klength, neurons, nneurons);
    free(neurons);
    return substim;
  }
  else{
    return NULL;
  }



}


#endif
