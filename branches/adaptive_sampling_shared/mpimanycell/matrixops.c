#include "matrixops.h"
#include <stdio.h>
#include <stdlib.h>
#include "datastruct.h"
#include <math.h>
#include "debug.h"

#ifdef MATLAB
#include "mat.h"
#include "matrix.h"
#ifdef MATLABMEX
//#include "mex.h"
#endif
#include "matlabinter.h"
#endif
//print matlab header file from redefining printf as mexprintf

//#define printf printf
void testfun(){

}

#ifdef MATLAB
//********************************************************
//Caveats:
//   1. all vectors are forced to be column vectors
sMatrix* mxArrayTosMatrix(mxArray* marray){
	if (marray==NULL){
		emsg("mxArrayTosMatrix tried to create sMatrix from null mxArray \n");	
		return NULL;
	}
	if (!(mxIsDouble(marray))){
		emsg("mxArrayTosMatrix only works if the matrix is a matrix of doubles \n");	
		return NULL;
	}
	
	
	char * errmsg=malloc(200);
	mxAssertS(marray,errmsg);
	const int *dims =mxGetDimensions(marray);
	int ndims =mxGetNumberOfDimensions(marray);
	double *data;
	data=mxGetData(marray);
       
	if (data==NULL){
	  emsg("mxArraytoSmatrix: data is null pointer \n");
	}
	if (ndims!=2){
		emsg("mxArrayTosMatrix requires a 2d matrix \n");
		return NULL;
	}

	sMatrix* smatrix=createMatrix(ndims,dims);
		
	double val=0;
	//matlab uses columnmajor ordering
	for (int row=0; row<dims[0];row++){
		for (int col=0; col<dims[1];col++){
			val=data[row+col*dims[0]];
			//#ifdef DEBUG
			// printf("read %g \n",val);
                        //#endif
			setij(smatrix,row,col,val);
		}	
	}
	
	//if this is a row vector take the transpose
	if (isrowvector(smatrix)){
	  //to transpose it we just change the dimensions
	  //b\c its a vector
	  int ncols=smatrix->dims[1];
	  smatrix->dims[0]=ncols;
	  smatrix->dims[1]=1;
	}
	return smatrix;
}

mxArray* smatrixtomxarray(sMatrix* mat){
  if (validmatrix(mat)==FALSE){
    emsgsl("smatrixtomxarray: invalid matrix", __FILE__, __LINE__);
    return NULL;
  }
   if (mat->ndims!=2){
     emsgsl("smatrixtomxarray: matrix must be 2 dimensional", __FILE__, __LINE__);
    return NULL;
  }
  mxArray* mxmat=mxCreateDoubleMatrix(mat->dims[0], mat->dims[1],mxREAL);

  //free the initial values of the data
  //is this step necessary
  double* data=mxGetPr(mxmat);
  //mxFree(data);

  //now allocate space for it
  //data=mxCalloc(sizeof(double)*numelements(mat));

  //now copy the data
  memcpy(data,mat->data,sizeof(double)*numelements(mat));

  //now set the pointer of mxmat
  //mxSetPr(mxmat,data);

  return mxmat;
}

#endif

sMatrix* identitymatrix(int dim){
  sMatrix* matrix=createMatrix2ds(dim);

  for (int row=0; row<dim;row++){
    
      setij(matrix,row,row,1);
    
  }

  return matrix;

}
/******************************************
//get the mat(index)=val
//
//Explanation: This does flat indexing
//   into the matrix
*/
double get(const sMatrix *mat, int index){
  //error checking
  validmatrix(mat);

  //check the index is valid
  if (index>=numelements(mat)){
    char *msg=malloc(100);
    emsg("get: index exceeded numelements. \n");
   sprintf(msg,"get: index=%d \t numelements= %d \n",index,numelements(mat));
    emsg(msg);
    return -1;
  }
  return mat->data[index];

}

void set(const sMatrix *mat, int index,double val){
  //error checking
  validmatrix(mat);

  //check the index is valid
  if (index>=numelements(mat)){
    char *msg=malloc(100);
    emsg("set: index exceeded numelements. \n");
   sprintf(msg,"set: index=%d \t numelements= %d \n",index,numelements(mat));
    emsg(msg);
      }
  mat->data[index]=val;

}
//****************************************
//int numelements(const sMatrix * mat)
//
//Explanation: the total number of elements in a matrix
int numelements(const sMatrix* mat){
  int num=1;
  
  int ERROR=FALSE;
  //error checking
  if (validmatrix(mat)==FALSE){
    return -1;
  }
  for (int index=0;index<mat->ndims;index++){
    num=num*mat->dims[index];
  }
  return num;

}
/******************************************
//set the mat[i,j]=val
//
//Explanation: Reason for having a function
//      is because for fortran functions
//      we need to store the elements in column major
//      format. That is all elements in same column are
      stored sucessively in memory
*/
double getij(const sMatrix *mat,int row, int col){
	if (validmatrix(mat)==FALSE){
    emsg("getij: invalid matrix \n");
  }
  
  if (validindex(mat,row,0)==FALSE){

    emsg("getij: invalid row index \n");
  }

  if (validindex(mat,col,1)==FALSE){

    emsg("getij: invalid col index \n");
  }
       
  //column major ordering
  #ifdef ROWMAJOR
    //row major ordering
  	return mat->data[row*(mat->dims[1])+col];
  #else
  //column major ordering
  return mat->data[row+col*(mat->dims[0])];
  	
  #endif
}

/******************************************
//setij(sMatrix* Mat, int row, int col, double val)
//    Mat - matrix whose element we want to set
//    row - row of matrix to set
//    col - column to set
//    val - value to set;
//
//Explanation: Reason for having a function
//	is because for fortran functions
//	we need to store the elements in column major
//	format. That is all elements in same column are 
//      stored sucessively in memory
*/
void setij( sMatrix *mat,int row, int col, double val){
  #ifdef DEBUG
  if (validmatrix(mat)==FALSE){
    emsg("setij: invalid matrix \n");
  }
  
  if (validindex(mat,row,0)==FALSE){

    emsg("setij: invalid row index \n");
  }

  if (validindex(mat,col,1)==FALSE){

    emsg("setij: invalid col index \n");
  }

 
  #endif

 
  
  //Row major code
  #ifdef ROWMAJOR
  mat->data[row*(mat->dims[1])+col]=val;
  #else
  mat->data[row+mat->dims[0]*col]=val;
  #endif
}
/******************************************
//sMatrix* subMatrix(const sMatrix* matrix, int[2] rows, int[2] cols)
//	Matrix - matrix we want submatrix of 
//	rows   - 2d integer array specifying what rows we want
//			-i.e start and stop
//	cols   - 2d integer array specifying what columns we want
// Explanation: return a submatrix
// we make a copy of the submatrix
*/
sMatrix* subMatrix(const sMatrix* matrix, int rows[2], int cols[2]){
	//error checking
	validmatrix(matrix);
	validindex(matrix,rows[0],0);
	validindex(matrix,rows[1],0);
	validindex(matrix,cols[0],1);
	validindex(matrix,cols[1],1);
	
	if (rows[1]<rows[0]){
		emsg("subMatrix: endrow can't be smaller than start row \n");
		return NULL;
	}
	if (cols[1]<cols[0]){
		emsg("subMatrix: endcol can't be smaller than start col \n");
		return NULL;
	}
	//dimensions of submatrix
	int  dims[2];
	dims[0]=rows[1]-rows[0]+1;
	dims[1]=cols[1]-cols[0]+1;
	
	sMatrix* sub=createMatrix(2,dims);
	double entry=0;						//used to store values of the returned entries
	//now copy the matrix
	//it might be faster to use memory copy reoutines but 
	//I want to use my interface to minimize the problems
	//rindex,cindex index the row to set
	for (int rindex=0;rindex<dims[0];rindex++){
		for (int cindex=0;cindex<dims[1];cindex++){
			entry=getij(matrix,rindex+rows[0],cindex+cols[0]);
			setij(sub,rindex,cindex,entry);
					
		}
	}
	return sub;

}
sMatrix* getSubMatrix(const sMatrix* matrix, int srow, int erow, int scol,int ecol){
	int rows[]={srow,erow};
	int cols[]={scol,ecol};
	
	
	return subMatrix(matrix,rows,cols);
	
}



/******************************************
/setSubMatrix(sMatrix target*, sMatrix* source,int srow, int erow, int scol, int ecol) 
/       target - matrix whose elements we want to set
/       source - matrix we want to copy
/       srow   - start row in target
/       scol   - start col in target
/
/Explanation sets the specified region of the 2d Matrix target
/to the elements given by the source
*/

void setSubMatrix(sMatrix* target, const sMatrix* source,int srow, int scol){
  
  //number of rows and columns to set in target
  //int nrows=erow-srow+1;
  //int ncols=ecol-scol+1;
  //*******************************************************
  //Error checking
  //*****************************************************
  //error checking make sure we don't exceed the bounds
  if(validindex(target,srow,0)==FALSE){
    emsg("setSubmatrix: start row is invalid \n");
    }

  //last row to set
  if(validindex(target,srow+source->dims[0]-1,RDIM)==FALSE){
  emsg("setSubmatrix: end row is invalid \n");
  }
 
 if(validindex(target,scol,RDIM)==FALSE){
    emsg("setSubmatrix: start col is invalid \n");
 }
 //last col to be set
  if(validindex(target, scol+source->dims[1]-1,CDIM)==FALSE){
    emsg("setSubmatrix: end col is invalid \n");
 }
 
  if (validmatrix(source)==FALSE){
    emsg("setSubmatrix: source is not valid matrix \n");
    
  }

  //if (srow>erow){
  //  emsg("setSubmatrix: start row is greater than end row\n");
  //} 
  // if (scol>ecol){
  //  emsg("setSubmatrix:  start column is greater than end column\n");
  // }
  

  //check indexes match the 
  //***************************************************
  //set the submatrix
  //***************************************************
  int rowi; //index into source
  int coli;
  double val;  //value to set it to
  
  for (rowi=0; rowi<source->dims[0];rowi++){
    for (coli=0;coli<source->dims[1];coli++){
      val=getij(source,rowi,coli);
      setij(target,srow+rowi,scol+coli,val);
    }
  }
}


/************************************************
//check dimension index is valid
************************************************/
//int validindex(const sMatrix*, int index, int dim)
//    matrix - matrix we want to check
//    index  - which index do we want to validate
//    dim    - along which dimension are we validating it
//             this is given as the index into matrix->dims
//             of the dimensionality of that index
int validindex(const sMatrix* matrix, int index, int dim){
  //validate matrix is not null
  if (matrix== NULL){
    emsg("validindex: matrix is null pointer \n");
    return FALSE;
  }
  if (matrix->data==NULL){
    emsg("validindex: matrix->data is null pointer \n");
    return FALSE;
  }

  if (index<0){
    emsg("validindex: index  is <0 \n");
    return FALSE;
    
  }
  if (dim >= matrix->ndims){
     emsg("validindex: dim - dimension of index  exceeds matrix dimensionality \n");
    return FALSE;
  }
  if (index>=matrix->dims[dim]){
    emsg("validindex: index exceeds the size of the matrix  \n");
    return FALSE;
    
  }
  
  
  return TRUE;

}

/************************************************
//check matrix structure is valid
************************************************/
//int validindex(const sMatrix*)
//    matrix - matrix we want to check
int validmatrix(const sMatrix* matrix){

  //validate matrix is not null
  if (matrix== NULL){
    emsg("validmatrix: matrix is null pointer \n");
    return FALSE;
  }
  if (matrix->data==NULL){
    emsg("validmatrix: matrix->data is null pointer \n");
    return FALSE;
  }

  if (matrix->dims==NULL){
    emsg("validmatrix: matrix->dims is null pointer\n");
    return FALSE;
    
  }
  
  return TRUE;

}
/******************************************/
//Explanation: Create a vector
//which is just a matrix with only a single dimension
//not = 1
//these are row vectors
//should check this doesn't create problems with fortran
sMatrix* createVector(int length){
  if (length<1){
    emsg("CreateVector: vector length must be >1 \n");
  }
        int dims[2]={length,1};
	return createMatrix(2,dims);

}

//******************************************/
//Explanation: Create a square matrix of the specified size
sMatrix* createMatrix2ds(int size){
	int dims[2]={size,size};
	return createMatrix(2,dims);

}

//*********************************************
//array to vector
//create a matrix from an array of doubles
sMatrix* array2vector(double* data, int length){
  sMatrix* vec=createVector(length);

  for (int index=0;index<length;index++){
    set(vec,index,data[index]);
  }

  return vec;
}
//*********************************************/
//createMatrix
//
//Explanation: Creates 2d matrices of the specified size
//
//Warning: THIS CREATES COLUMN MAJOR MATRICES for use with LAPLACK library 
//thus elements in same column are stored successevely in memory
//
// dims - element 0 = rows
sMatrix* createMatrix(int ndims,const int* dims){
        
  
	//allocate space for the matrix
	sMatrix *mat=(sMatrix*)(malloc(sizeof(sMatrix)));

	//set its dimensions
	mat->ndims=ndims;

	//to avoid any memory problems
        //allocate new space for the dims object
	mat->dims=copyIarray(dims,ndims);

	//compute the number of elements
	int numelements=1;
	for (int index=0;index<mat->ndims;index++){
	  numelements=numelements*mat->dims[index];
	}
	//allocate space for the data array
	if (ndims ==2){
		//we create a column major matrix
		//thats why dimensions are reversed when create the array
		//mat->data=create2d_darray(mat->dims[1], mat->dims[0]);
	  mat->data=malloc(sizeof(double)*numelements);
	        for (int index=0;index<numelements;index++){
		  mat->data[index]=0;
		}
        }	
	else{
		
		printf("Error: createMatrix has no code to create matrices of \n");
		printf("Error: size other than 2 \n");	
	}

	return mat;
}

//***************************************************/
//void initimatrix
//**************************************************/
//void initimatrix
//    matrix - matrix to initialize
//    ndims  - number of dimensions
//    dims   - dimensions
//initialize a matrix
//THis function intializes an sMatrix structure
//space already neeads to be allogcated for the sMatrix* pointer
//but not its elements

void initMatrix(sMatrix* matrix,int ndims,int*dims){
  matrix->ndims=ndims;
  matrix->dims=dims;
  
  
  
 double numelements=1;
 for (int index=0;index<ndims;index++){
   numelements=numelements*dims[index];

 }

  //allocate space for data
 matrix->data=(double *) (malloc(numelements*sizeof(double)));

  
 //loop over all elements and set to zero
 for(int index=0;index<numelements;numelements++){
   matrix->data[index]=0;
   
 }
}

//*********************************************************/
//sMatrix* copyMatrix(
//make a copy of a matrix
//Assumes space hasn't been allocated yet
//use coopyMatrixTo if you have already assigned space
//for a matrix
sMatrix* copyMatrix(const sMatrix* source){
 
   sMatrix* copy=createMatrix(source->ndims,source->dims);

  //copy the data
   int size=1;
   int index=0;
   for (index=0;index<source->ndims;index++){
     size=size*source->dims[index];
}
  copyDarrayTo(copy->data,source->data,size);

  return copy;
}

//use copyMatrixTo to copy a matrix to an existing
//Location. This function allows you to pass in a pointer
//to a target object which will then be set to match the source
//space has already been allocated for the sMatrix structure
//but not its elements.
void copyMatrixTo( sMatrix* target, const sMatrix* source){
  target->ndims=source->ndims;
  target->dims=copyIarray(source->dims,source->ndims);

  int size=1;
  for (int index=0; index<target->ndims;index++){
    size=size*target->dims[index];
  }

    //make a copy of the double array
    target->data=copyDarray(source->data,size);
  

}

//Copy one double array to another
//use this function 
//when space has not been allocated yet 
 double* copyDarray(const double* source,int size){

   //allocate space for it
   double *copy=(double *) (malloc(size*sizeof(double)));

   copyDarrayTo(copy,source,size);

   return copy;
 }

//Copy one double array to another
//use this function when space has already been allocated for the copy
 void copyDarrayTo(double* target, const double* source, int size){

	//check to make sure neither pointer is null
	if (target==NULL){
		printf("Error: copy2d_darrayto target is null \n");
	}
	if (source==NULL){
		printf("Error: copy2d_darrayto source is null \n");			
	}
	int index;
            
	int length=size;
	  for (index=0;index<length;index++){	    
	      target[index]=source[index];
	    }
 }
 


//make a copy of an integer array
//This function allocates space for the copy
int* copyIarray(const int* source,int length){
         int *target=(int *)(malloc(length*sizeof(int)));

	//check to make sure neither pointer is null
	if (target==NULL){
		printf("Error: copy_iarray target is null \n");
	}
	if (source==NULL){
		printf("Error: copy_iarray source is null \n");			
	}
	for (int index=0; index <length;index++){
		target[index]=source[index];
	}
	return target;
} 




//*****************************************************/
//create a diagonal matrix from a vector

sMatrix* diag(const sMatrix* vec){
  if (vec->dims[1]>1){
    printf("Error: diag requires a row vector \n");
  }

  int ndims=2;
  int dims[2];
  dims[0]=vec->dims[0];
  dims[1]=vec->dims[0];

  sMatrix* dmat=createMatrix(ndims,dims);
  int index=0;
  for (index=0;index<dmat->dims[1];index++){
    setij(dmat,index,index,getij(vec,index,0));
  }

  return dmat;
}

//*****************************************************************
//create an array of doubles
double* smatrixtodarray(const sMatrix* mat){
  if (validmatrix(mat)==FALSE){
    emsg("smatrixtodarray: not a valid matrix");
    return NULL;
  }
  int length=numelements(mat);
  double* data=malloc(sizeof(double)*length);

  memcpy(data,mat->data,sizeof(double)*length);
  return data;

}
//**************************************************************
// isvector
int isvector(sMatrix* vec){
  validmatrix(vec);
  if (isnullm(vec, "vec")==TRUE){
    return FALSE;
  }
  if (vec->ndims>2){
    return FALSE;
  }
  else{
    if(((vec->dims[0]==1)&& (vec->dims[1] !=1)) || ((vec->dims[0]!=1)&& (vec->dims[1] ==1))){
      return TRUE;
    }
    else{
      return FALSE;
    }
  }
  
}

int isrowvector(sMatrix* vec){
  //check its vector
  if ((isvector(vec))==FALSE){
  return FALSE;
}
if (vec->dims[0]==1){
  return TRUE;
}

return FALSE;

}

int iscolvector(sMatrix* vec){
  //check its vector
  if ((isvector(vec))==FALSE){
  return FALSE;
}
if (vec->dims[1]==1){
  return TRUE;
}

return FALSE;

}

void printmatrix(sMatrix* mat){
  //error checking
  if(validmatrix(mat)==FALSE){
    emsg("printmatrix: matrix not valid");
  }
  int BUFFSIZE=200;
  char* msg=malloc(BUFFSIZE);
 
  //if its 2d matrix print out a grid
  if (mat->ndims==2){
    for (int row=0;row<mat->dims[0];row++){
       sprintf(msg,"");
      for (int col=0;col<mat->dims[1];col++){
	sprintf(msg, "%s %g \t", msg, getij(mat,row,col));
	//printf("%g \t",geij(mat,row,col));
	//if we are close to edge of buffer break the inner loop
	if (strlen(msg)>=(BUFFSIZE-30)){
	  sprintf(msg, "%s \t truncated", msg);
	  //exit loop by violating loop condition
	  col=mat->dims[1]+10;
	}
      }
      //printf("\n");
      sprintf(msg,"%s \n",msg);
      fmsg(msg);
    }
  }
  free(msg);
  
}
//**********************************************************/
//freeMatrix(sMatrix*)
//********************************************************/
void freeMatrix(sMatrix* matrix){
  //error checking
  if (matrix==NULL){
    emsgsl("freeMatrix: tried to free a null pointer matrix  \n",__FILE__,__LINE__);
  
    return;
  }
  if (matrix->dims==NULL){
     emsg("freeMatrix: tried to free null pointer sMatrix->dimsx \n");
  }
  else{
  free(matrix->dims);
  }
  
  if(matrix->data==NULL){
    emsg("freeMatrix:tried to free null pointer sMatrix->data \n");
  }
  else{
    
  free (matrix->data);
  }

  free (matrix);

}
