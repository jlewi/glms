%Make a plot of the entropies to show optimization does better
clear all
setpaths
%file with data
fname='./data/entropy/simout_P01_001.mat';
subsamp=10;
fentropy.hf=figure;
fentropy.axisfontsize=15;
fentropy.lgndfontsize=15;
fentropy.xlabel='Trial'
fentropy.ylabel='Entropy'
fentropy.fname='postentropy.eps'
fentropy.name='entropy';
data=load(fname);

randdata=data.trials00(2,1);
optdata=data.trials00(1,1);


optpost=optdata.neurons.post;
randpost=randdata.neurons.post;

niter=length(optpost);
%***************************************************************
%figure of the posterior entropy
%*********************************************************

        figure(fentropy.hf);
        hold on;
        
        %need to know dimensionlaity of filter to compute entropy
        filtlength=size(randpost(1).m,1);
        for index=1:niter
	    %***********************************************
	    %entropy of optimized stimului
            eigd=eig(optpost(index).c);
            %should be base 2 for the log
            entropy(index).opt=1/2*filtlength*(log2(2*pi*exp(1)))+1/2*sum(log2(abs(eigd)));

            %random stimuli
	    eigd=eig(randpost(index).c);
            %should be base 2 for the log
            entropy(index).rand=1/2*filtlength*(log2(2*pi*exp(1)))+1/2*sum(log2(abs(eigd)));
        end

	    

pind=1;
   entvals=[entropy(:).rand];
   entvals=entvals(1:subsamp:end);
   fentropy.hp(pind)=plot(1:subsamp:niter,entvals,getptype(pind,1));
   set(fentropy.hp(pind),'Marker','d')
   set(fentropy.hp(pind),'MarkerSize',10)
        plot(1:subsamp:niter,entvals,getptype(pind,2));
fentropy.lbls{pind}=sprintf('Random stimuli');


pind=pind+1;
        

   entvals=[entropy(:).opt];
   entvals=entvals(1:subsamp:end);
        fentropy.hp(pind)=plot(1:subsamp:niter,entvals,getptype(pind,1));
       set(fentropy.hp(pind),'MarkerSize',10)
        set(fentropy.hp(pinf),'Color','k');
        h=plot(1:subsamp:niter,entvals,getptype(pind,2));
       set(fentropy.h,'Color','k');
fentropy.lbls{pind}=sprintf('Optimal stimuli');


    
lblgraph(fentropy);
        

outfile=fullfile(pwd,'writeup','postentropy.eps');
exportfig(fentropy.hf,outfile,'bounds','tight','Color','bw');
