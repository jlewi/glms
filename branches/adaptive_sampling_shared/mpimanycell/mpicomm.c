#include "simulation.h"
#include "mparam.h"
#include "matrixops.h"

#include "mpicomm.h"
#ifdef MPI
//broadcast the stimulus to all the nodes
//stim is Null except for the root node.
sMatrix* bcaststim(ssimparam* simparam, smparam* mparam, sMatrix* stim){
  int stimlength=getmparamstimlength(mparam);

  sMatrix* newstim=createVector(stimlength);
  //MPI buffers
  //each node sends to the root the max fisher information in its range
  //double *sendbuff=malloc(sizeof(double)*stimlength);
  //don't bother allocating space for sendbuff we will just point to the stimulus
 
  
//initialize the recv buffer
  if (getrank(simparam->mpiinfo)==0){
    if (isnullm(stim->data,"stim->data")==TRUE){
      emsgm("stimlus to broadcast has null pointer to data");
      emsgm("terminating");
      exit(FALSE);
    }
    //root node sends the stimulus
    //so copy data to it
    memcpy(newstim->data,stim->data, sizeof(double)*stimlength);
    
  }
      

  //do actual broadcast
  MPI_Bcast(newstim->data, stimlength, MPI_DOUBLE,0, MPI_COMM_WORLD);
  fmsg("Optimal Stimulus recieved \n");
      
  //now create the optimal stimulus
  return newstim;

}
#endif
