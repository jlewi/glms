%Matlab script to create structures of simulation parameters 
%which can be loaded into the simulation

%I need to increase the maximum number of allowed spikes by scaling ktrue
%this will lead to better convergence
clear all
setpaths 
ntrials=5;
klength=10;


maxrate=1000;
numbersofneurons=[1:10 20:10:100];
numbersofneurons=[1:2:10 20:20:100 500];
numbersofneurons=[500 1000];
numbersofneurons=[5000];
mparam.mmag=1; 
mparam.twindow=1;


%simulation parameters
simparam.ntrials=ntrials;
simparam.randstimuli=0;              % 0 - optimize the stimuli
                                     % 1 - use random stimuli

fname=fullfile(pwd,'data','simdata.mat');


%generate the neurons
for index=1:length(numbersofneurons)
     numneurons=numbersofneurons(index);
     neurons=[]




for nindex=1:numneurons
neurons(nindex).klength=klength;
neurons(nindex).ktrue=rand(klength,1);

%normalize ktrue
sf=(log(maxrate/mparam.twindow)*(neurons(nindex).ktrue'*neurons(nindex).ktrue)^.5);
neurons(nindex).ktrue=sf^.5*neurons(nindex).ktrue/(neurons(nindex).ktrue'*neurons(nindex).ktrue)^.5;

neurons(nindex).prior.m=rand(klength,1);
neurons(nindex).prior.c=eye(klength);
neurons(nindex).ntrials=ntrials;
if (nindex>1)
neurons(nindex).kstart=neurons(nindex-1).kstart+neurons(nindex-1).klength;
else
neurons(nindex).kstart=0;
end

end
mparam.neurons=neurons;





foutfile=seqfname(fname);
fprintf('saving %s \n',foutfile); 
save(foutfile);

end
