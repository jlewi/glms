#include "model.h"
#include "matrixops.h"
#include "matrixmath.h"
#include "probability.h"

#ifndef OPTSTRUCT_INC
   #define OPTSTRUCT_INC 
//*****************************************************************************
//Structures
//***************************************************************************
//declare a structure to hold all of the important
//structures needed in the optimization method 
//each region
//
typedef struct st_OptRegion {
  sGauss* post;		//posterior
  sMatrix* emean;	// projection of mean onto eigenvectors
  sMatrix* eigd;	//eigenvalues
  double mmag;		//magnitude constraint
  sMatrix* evecs;   //eigenvectors
  const ssubStim* sregion;	//structure representing information
  						//about what region of stim space we represent
} sOptRegion;






//*****************************************************************//
//const ssSubStim* substim - this is declared as constant because we will never
//                         alter the substimulus region
void initOptRegion(sOptRegion* cregion, const sGauss* post, double mmag,const ssubStim* substim);


//****************************************************
//functions to free memory
//**********************************************

void freeOptRegion(sOptRegion* optregion);

#endif
