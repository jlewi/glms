#include "probability.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "debug.h"
#include "matrixops.h"
#include "matrixmath.h"
#include "mpiinfo.h"
#include "output.h"
#include "optstruct.h"
#include "optimization.h"
#include "model.h"
#include "opttest.h"
#include "fullstim.h"
#include "searchresult.h"
#include "simulation.h"
#include "mparam.h"
#include "probability.h"
#include "neuronstim.h"

#ifdef MPI
#include "mpi.h"
#endif

//matlab headers
#ifdef MATLAB
#include "mat.h"

#include "matrix.h"
#ifdef MATLABMEX
#include "mex.h"
#endif
#include "engine.h"
#endif

sFullStim* optstim(ssimparam* simparam, smParam* mparam){
  sneuron** neurons;//which neurons we optimize for
  const srfield** rfields;
   
  sFullStim* fullstim=NULL;  //new full stimulus
  const sMatrix** stims=NULL;     //optimal stimulus for different regions

  int trial=gettrial(simparam);
#ifdef MATLABENG
    
  //**************************************************
  //  set up matlab eng
  //************************************************
  //we will call MATLAB to do the update
  Engine *ep;
  ep=getmateng(simparam);


  //create an output buffer to store output of matlab commands
  char * mBuffer;
  int buffSize=1000;
  mBuffer=(char*) malloc(sizeof(char)*buffSize);
  engOutputBuffer(ep, mBuffer, buffSize);  
   
  
  //*******************************************************
#ifdef MPI
  //*****
  //COde to paralellize the updates go heres
  // neeed to divide up which node does what
#else
  neurons=mparam->neurons;

  int numneurons=getnumneurons(mparam);
  stims=malloc(sizeof(sMatrix*)*numneurons);
  rfields=malloc(sizeof(srfield*)*numneurons);
  for(int nindex=0;nindex<numneurons;nindex++){
  
    rfields[nindex]=neurons[nindex]->rfield;
    //put relevant variables into matlab
    //get the last posterior
    sgauss*  post=getneuronpost(neurons[nindex], trial-1);
    mxArray* mxpost=createmxgauss(&post,1);
    mxArray* mxmmag=mxCreateDoubleScalar(getmmag(mparam));

    //put the variables into matlab
    //clear the workspace before doing anything
    engEvalString(ep, "clear all; setpaths;");
    //put the variables in the matlab workspace
    engPutVariable(ep, "post", mxpost); 
    matmsg(mBuffer);

    engPutVariable(ep, "mmag", mxmmag);
    matmsg(mBuffer);

    engEvalString(ep, "[xopt]=fishermax(post,mmag)");
    matmsg(mBuffer);

  
    mxArray *mxstim;
    mxstim=engGetVariable(ep, "xopt");

    stims[nindex]=mxArrayTosMatrix(mxstim);

    //cleanup
    freeGauss(post);
	       
    mxFree(mxpost);
    mxFree(mxmmag);
    mxFree(mxstim);

  }

  //compute full stimulus
  fullstim=createfullstim(mparam->stiminfo,rfields,stims,numneurons);

  if(isnullm(rfields,"rfields")==FALSE){
    //this shouldn't free the rfields
    //this should just free the array of rfield pointers
    free(rfields);
  }
#endif //end if for MPI



#else
  emsgm("No code for fishermax \n");
  exit(FALSE);
#endif //end if for MATALABENG

/*   return fullstim; */
/* } */
/* //\**************************************************************** */
/* //void optimize */
/* //\**************************************************************** */
/* //	Input: */
/* //		sharedrange - this is the range of values on the magintude */
/* //			constraint on the unique region that we will search */
/* //		double mmag - full constraint on the magnitude */
/* //					-using this and the current value of sharedrange */
/* //					 we can compute the constrain each unique region */
/* //		post		- current value of the posterior */
/* //		stim		- structure representing the full stimulus */
/* //					  includes information needed to parse */
/* //					  the posterior into shared and unique regions */
/* // */
/* //      Output the new ooptimized  */
/* //              stimOpt - the optimized stimulus */
/* //              fopt    - the fisher information at the maximal value */

/* sFullStim* optimize(ssimparam* simparam, smParam* mparam, sGauss* post){ */
	
/*   //1. decompose	the posterior into unique and shared regions */
/*   int nshared = mparam->stiminfo->nshared; */
/*   sOptRegion* regshared=decompose(post,mparam->stiminfo->subshared,nshared); */
/*   int nunique = mparam->stiminfo->nunique; */
/*   sOptRegion* regunique=decompose(post,mparam->stiminfo->subunique,nunique); */


/*   //\**************************************************************************** */
/*   //perform search over range of values  */
/*   //get the search range for this node */
/*   double minmag=0; */
/*   double nsteps=10;   //how many steps do we want to parse the range into */
/*   sSearchRng* sharedrng=parserng(minmag,getmmag(mparam),getrank(simparam->mpiinfo),getnprocs(simparam->mpiinfo)); */
/*   setinterval(sharedrng,nsteps); */

    
/*   //2. perform search over the range of energy constraints */
/*   //store results from search */
/*   sFullStim* optstim=NULL; // - optimium stimulus in range searched */
/*   double* foptrng=malloc(sizeof(double)); //fisher info at the optimium stimulus in this range */
 
    
/*   optstim=searchover_unique_energy(mparam, sharedrng, regshared, regunique, nshared, nunique,foptrng); */

/*   //\************************************************************************************************************************************* */
/*   //mpi code to select maxium of all the search ranges */
/*   //\************************************************************************************************************************************ */
/* #ifdef MPI */
/*   //each node sends to the root the max fisher information in its range */
/*   double *sendbuff=malloc(sizeof(double)); */
/*   int sendcount=1; */

/*   double *recvbuff=malloc(sizeof(double)*getnprocs(simparam->mpiinfo)); */
/*   int recvcount=1; */

/*   int root=0;  //identity of root in this case */

/*   //set the sendbuff to foptrng */
/*   *sendbuff=*foptrng; */
  
/*   //perform an all gather   */
/*   fmsg("optimization.c: Perform all gather to find node which has maximum value \n"); */
/*   MPI_Gather(sendbuff,sendcount,MPI_DOUBLE,recvbuff,sendcount,MPI_DOUBLE,root,MPI_COMM_WORLD); */

/*   //node wmax will store rank of node with maximum value */
/*   int nodewmax=0; */
  
/*   //root node finds the identity of the node with the maximum */
/*   if (getrank(simparam->mpiinfo)==root){ */
/*     //we are root node */
/*     //find the maximum */
/*     int nprocs=getnprocs(simparam->mpiinfo); */
    
/*     for(int index=0;index<nprocs;nprocs++){ */
/*       if (recvbuff[index]>recvbuff[nodewmax]){ */
/* 	nodewmax=index; */
/*       } */
/*     } */
/*   } */
/*   //now we do broadcast to tell all nodes which one has optimium */
/*   //explicitly free and reallocate the memory */
/*   free(sendbuff); */
/*   free(recvbuff); */
/*   sendbuff=malloc(sizeof(double)); */
/*   recvbuff=malloc(sizeof(double)); */
/*   sendcount=1; */
/*   recvcount=1; */

/*   sendbuff[0]=nodewmax; */
/*   recvbuff[0]=nodewmax; */

/*   fmsg("Broadcast node which has maximum value \n"); */
/*   MPI_Bcast(sendbuff, sendcount, MPI_DOUBLE,root, MPI_COMM_WORLD); */
   
/*   //now we all set nodewmax */
/*   nodewmax=(int)sendbuff[0]; */

/*   //free all the buffers to avoid problems */
/*   free(sendbuff); */
/*   free(recvbuff); */
/*   sendcount=0; */
/*   recvcount=0; */

/*   //now we do broadcast to send the optimal stimulus to all nodes */
/*   //initalize the buffer */
/*   recvcount=getklength(mparam); */
      
/*   fmsg("Broadcast optimal stimulus \n"); */

/*   //initialize the recv buffer */
/*   if (getrank(simparam->mpiinfo)==nodewmax){ */
/*     //this node has max */
/*     recvbuff=smatrixtodarray(optstim->stim); */
/*     isnull(recvbuff,"recbuff",__FILE__,__LINE__); */
/*   } */
/*   else{ */
/*     //initialize it to zero */
/*     int klength=getklength(mparam); */
/*     recvbuff=malloc(sizeof(double)*klength); */
/*     for(int index=0;index<klength;index++){ */
/*       recvbuff[index]=0; */
/*     } */
/*   } */
      

/*   //do actual broadcast */
/*   MPI_Bcast(recvbuff, recvcount, MPI_DOUBLE,nodewmax, MPI_COMM_WORLD); */
/*   fmsg("Optimal Stimulus recieved \n"); */
      
/*   //now create the optimal stimulus */
/*   sMatrix* mstim=array2vector(recvbuff, recvcount); */

/* #ifdef DEBUG */
/*   fmsg("Optimal stimulus recieved: \n"); */
/*   printmatrix(mstim); */
/* #endif */
/*   //set the optimal stimulus */
/*   setfullstim(optstim,mstim); */
/*   //clean up */
/*   isnull(recvbuff,"recbuff",__FILE__,__LINE__); */
/*   free(recvbuff); */
/*   freeMatrix(mstim); */

/* #endif */
/*   //\******************************************************************************************************************** */
/*   //clean up */
/*   //\************************************************************************************************************************* */
/*   //because I allocated memory for regshared and regunique with a single call i can use a single free call */
/*   free(sharedrng); */
/*   free(regshared); */
/*   free(regunique); */
/*   free (foptrng); */
/*   return optstim; */
	
/* } */
/* //Explanation: Provides a skeleton structure for the optimization routin */
/* //\***************************************************************** */
/* //decompose posterior */
/* //Inpute: */
/* //	sGauss* post - current posterior */
/* //	sFullStim stim - st_FullStim - includes all information */
/* //					 about the posterior most notably  */
/* //Output Parameters: */
/* //	sOptRegion* regions -an array of regions representing the */
/* //				the parsing of the posterior. Note all values */
/* //				are not computed here. */
/* // */
/* //The number of regions is = stim->nSubstims */
/* //\***************************************************************************** */
/* sOptRegion* decompose(sGauss* post, ssubStim** stimregions, int nregions){ */

/*   //\******************************************************** */
/*   //allocate space for the regions */
/*   //\******************************************************* */
       
/*   sOptRegion* regions=(sOptRegion*)malloc((nregions)*sizeof(sOptRegion)); */
	
/*   sOptRegion* cregion; //point to region currently being modifed */
       
	
/*   //some arrays used for copying submatrices */
/*   int rows[2]; */
/*   int cols[2]; */
/*   //\*********************************************************** */
/*   //now loop over each region */
/*   //\*********************************************************** */
/*   for (int rindex=0; rindex<nregions;rindex++){ */
/*     //get a pointer to the substimulus structure representing the stimulus */
/*     //corresponding to this region */
	  
/*     //get pointer to the region we are currently modifying */
/*     cregion=regions+rindex; */
/*     double initmmag=0; */
/*     initOptRegion(cregion,post,initmmag,stimregions[rindex]); */
		
		
/*   } */

/*   //return the array of parsed matrices */
/*   return regions; */

/* } */



/* //\**************************************************************** */
/* //void searchover_shared_energy(smParam* mparam, sharedrange, const sGauss* postu, const sGauss* posts, int nshared, double mmag, double* fopt) */
/* //  input parameters: */
/* //      mparam  - model parameters */
/* //	smrange - specifies the range of magnitude constraints  */
/* //			on the amount of energy in the shared vs. unique region */
/* //			over which we conduct search */
/* //             - this should actually be the square root */
/* //             i.e ||x_uopt||_2 = erange */
/* //	sOptRegion shared - pointer to the shared region of the stimulus space */
/* //      sOptRegion unique - pointer to array of unique regions  */
/* //      int        nunique- number of unique regions */
/* //      double     mmag   - magnitude constraint on the stimulus */
/* //   */
/* // */
/* //   output parameters */
/* //      double*	 fopt  - fisher information at the optimium */
/* //   Return value:const ssSubStim* substim */
/* //      sSearchResult - this contains the optimium result found in the range smrange */
/* sFullStim* searchover_unique_energy(smParam* mparam, sSearchRng* smrange, sOptRegion* shared, sOptRegion* unique, int nshared, int nunique, double* fopt){ */
	
/*   double mmag=getmmag(mparam); */

/*   //parameters which define search over w, which is rescaled lambda 1 */
/*   double MINVAL=.00001; //minimum value of w (0 causes problems) */
/*   //double dw=.001;      //step size */
/*   double dw=.2; */

/*   //sSearchResult* results =(sSearchResult*)malloc(sizeof(sSearchResult)*ceil((smrange->max-srange->min)/srange->dw)); */
/*   if( nshared!=1){ */
/*     emsg("searchover_unique_energy: don't know what to do when nshared !=1"); */
/*   } */
 
/*   //nshared=1; */
/*   //for initializing the xu and xsopt structures */
/*   int dims[2]; */
/*   int ndims=2; */

/*   double fumax[nunique];       //this will store the fisher information */
/*   //from each of the unique regions  */
/*   //for a particular value of smag and sproj */
/*   //\****************************************************************** */
/*   //allocate space */
/*   //keep a pointer to the sSearch Result which is the current maximum */
/*   //we also need to store our current maximum */
/*   sSearchResult* maxResult=initSearchResult(nshared,nunique); */
/*   sSearchResult* result=NULL; */
     
/*   //\********************************************************** */
/*   //Perform a search over smrange - the range of the relative amount of energy in the shared */
/*   //vs unique region */
/*   //\*********************************************************** */
/*   for (double smag=smrange->min; smag<=smrange ->max;smag=smag+smrange->dw){   */
   
    
/*     //result->smag=smag; */

/*     //\************************************************************************ */
/*     //search over projection of stimulus onto mean of unique region */
/*     //sprojrange - specifies range of projections of shared stimulus onto its mean */
/*     //this is the rescaled lambda1 which should be between 0 and 1 non inclusive */
/*     for (double sproj=MINVAL; sproj<1;sproj=sproj+dw){ */
/*       //initialize a new result structure */
/*       result=initSearchResult(nshared,nunique); */
/*       //result->finfo=0;  //should be set by init function */
/*       result->sproj=sproj; */
/*       result->smag=smag; */

/*       //compute the stimulus in the shared region in this case */
/*       //for each shared region we compute 	 */
/*       //compute the rescaled stimulus in terms of the eigenvectors */
/*       //adjust the magnitude constraints */
/*       result->ysopt[0]=ystimrescale(sproj,shared->eigd,shared->emean,shared->mmag); */
/*       //\************************************************************************** */
/*       //optimize each unique region for the following constraints: */
/*       //1. Magnitude contraint on shared region - determines */
/*       //   the relative amount of energy in the unique vs. shared region */
/*       //2. Projection constraint of shared stimulus */
/*       //\********************************************************************  */
/*       //now for each of the unique regions */
/*       for (int rindex=0;rindex<nunique; rindex++){ */
/* 	fumax[rindex]=0;  //keep track of the fisher information so we can see which value we should choose  */
/* 	//for that shared region */
     
/* 	double uprojopt=-1;  //optimal value for it */
/* 	//adjust magnitude constraint based on amount of energy */
/* 	//in shared region   */
/* 	unique[rindex].mmag=sqrt(1-pow(smag,2)); */

        
/* 	//\************************************************************************** */
/* 	//optimize the selected unique unique region */
/* 	//\******************************************************************** */
/* 	//for each of the unique regions we need to perform a search over the projection */
/* 	//of this stimulus into the mean of the unique region */
/* 	double uproj=0; */
        
/* 	sMatrix* yunique=NULL; */
/* 	for(uproj=MINVAL; uproj<1;uproj=uproj+dw){ */
/* 	  //compute the stimulus for this value */
/* 	  yunique=ystimrescale(uproj, unique->eigd,unique->emean,unique->mmag); */

/* 	  double finner=finfo1neuron(yunique, unique+rindex, result->ysopt[0], shared); */
	
/* 	  //check if this is greater than our currently found maximum */
/* 	  //if it is change our maximum */
/* 	  if (finner>fumax[rindex]){ */
/* 	    fumax[rindex]=finner; */
/* 	    uprojopt=uproj; */
/* 	  } */
/* 	  //free the memory for this stimulus		     */
/* 	  freeMatrix(yunique); */
/* 	} //end search over projection of unique region */
   
     
     
/* 	// */
/* 	//recompute the optimal value for this unique region */
/* 	//of the stimulus for this value of smag */
/* 	result->yuopt[rindex]=ystimrescale(uprojopt, (unique+rindex)->eigd,(unique+rindex)->emean,(unique+rindex)->mmag); */
/* 	//add the fisher information from this region to the total fisher information */
/* 	result->finfo=result->finfo+fumax[rindex]; */

/*       } //end search over all unique regions */

/*       //\************************************************************ */
/*       // result ->yuopt */
/*       //    -this contains the optimal stimulus for each unique region for: */
/*       //    1. this value of sproj - projection of shared stimulus onto mean */
/*       //    2. this value of smag  - constraint on magnitude of shared stimulus */
/*       // */
/*       // result->finfo  */
/*       //    -contains the maximum fisher infor for this value of sproj,smag */
/*       //\********************************************************************** */
/*       //\************************************************** */
/*       //now test whether this value of smag, sproj */
/*       //is the maximum so far */
/*       if (result->finfo > maxResult->finfo){ */
/* 	//it is greater so we free the current maxResult and */
/* 	//set maxResult to result */
/* 	freeSearchResult(maxResult); */
/* 	maxResult=result; */
/* 	//now free result */
/*       } */
/*       else{ */
/* 	//just free the result */
/* 	freeSearchResult(result); */
/*       } */
      
  
/*     }//end search over projection of shared stim onto mean of shared region. */
/*   }//end search over magnitude constraint  */

/*   //\********************************************************************* */

/*   //maxResult->yuopt */
/*   //maxResult->ysopt */
/*   //    contain the optimal stimulus in terms of the eigenvectors */
/*   //maxResult->finfo */
/*   //   expected fisherinformation of this value  */

/*   //\********************************************************************** */
/*   //compute actual stimuli by multiplying by eigenvectors */
/*   //allocate arrays of pointers to store the stimuli */
/*   sMatrix** xs=(sMatrix**)malloc(sizeof(sMatrix*)*(nshared+nunique)); */

/*   //create an array of ssubStim objects to go with xs  */
/*   ssubStim** subStims =(ssubStim**)malloc(sizeof(ssubStim*)*(nshared+nunique)); */
/*   //we will need this to create the full stim */
/*   int iunique; */
/*   for (iunique=0;iunique<nunique;iunique++){ */
/*     subStims[iunique]=copysubStim((unique+iunique)->sregion); */
/*     xs[iunique]=matrixProd((unique+iunique)->evecs,maxResult->yuopt[iunique]); */
/*   } */
  
/*   int ishared; */
/*   for (ishared=0;ishared<nshared;ishared++){ */
/*     subStims[ishared+nunique]=copysubStim((shared+ishared)->sregion); */
/*     xs[ishared+nunique]=matrixProd((shared+ishared)->evecs,maxResult->ysopt[ishared]); */
/*   } */

/*   //\******************************************************************** */
/*   //output */
/*   //\********************************************************************* */
/*   //construct the full stimulus */
/*   sFullStim* fullStim=constructFullStim(mparam,subStims,xs,nunique+nshared); */

/*   //set the fisher information of the optimium */
/*   (*fopt)=maxResult->finfo; */
/*   //clean up */
/*   freeSearchResult(maxResult); */

/*   //clean up the stimuli */
/*   /\*for (iunique=0;iunique<nunique;iunique++){ */
/*     freeMatrix(xuopt+iunique); */
/*     } */
/*     free(xuopt); */


/*     for (ishared=0;ishared<nshared;ishared++){ */
/*     freeMatrix(xsopt+ishared); */
/*     } */
/*     free(xsopt);*\/ */

/*   return fullStim; */

/* } */




/* //\**************************************************************************** */
/* // ystimrescale( */
/* // Compute the rescaled stimulus */
/* //  double     w - variable which parameterizes the rescaling */
/* //  sMatrix* eigd - eigenvalues in ascending order */
/* //    */
/* // Return: */
/* //    sMatrix* y - pointer to the rescaled stimulus */

/* sMatrix* ystimrescale(double w, sMatrix* eigd, sMatrix* emean, double mmag){ */
  
/*   //eigenvalues should in ascending order  */
/*   int klength=emean->dims[0]; */

/*   //if its a scalar provide a warning message and return */
/*   if (klength==1){ */
/*     //wmsg("ystimrescale: input is a scalar \n"); */
/*     //the rescaled y in this case is just  */
/*     //the magnitude */
/*     sMatrix* y=createVector(klength); */
/*     setij(y,0,0,mmag); */
/*     return y; */
/*   } */
/* #ifdef DEBUG */
/*   //check that eigd and emean are both vectors */
/*   if (isvector(emean)==FALSE){ */
/*     emsg("ystimrescale: emean should be a column vector \n"); */
/*   } */
/*   if (iscolvector(emean)==FALSE){ */
/*     emsg("ystimrescale:emean should be a column vector \n"); */
/*   } */
/*   if (isvector(eigd)==FALSE){ */
/*     emsg("ystimrescale: eigd should be a column vector \n"); */
/*   } */
/*   if (iscolvector(eigd)==FALSE){ */
/*     emsg("ystimrescale:eigd should be a column vector \n"); */
/*   } */
 
/*   //check lengths match */
/*   if (numelements(eigd) !=numelements(emean)){ */
/*     emsg("ystimrescale: eigd and emean should be same length\n"); */

/*   } */
/*   //check the eigenvalues are sorted in ascending order */
/*   int ERROR=FALSE; */
/*   for (int index=0;index<numelements(eigd)-1;index++){ */
/*     if (get(eigd,index)> get(eigd,index+1)){ */
/*       ERROR=TRUE; */
/*     } */
/*   } */
/*   if (ERROR==TRUE){ */
/*     emsg("ystimrescale: eigenvalues are not sorted in ascending order \n"); */
/*   } */
/* #endif */

/*   //error checking */
/* #ifdef DEBUG */
/*   if (klength!=emean->dims[0]){ */
/*     emsg("ystimrescale: number of eigenvalues does not match dimensionality of eigenvectors \n"); */
/*   } */
/*   //if (klength!=emean->dims[1]){ */
/*   //   emsg("ystimrescale: number of eigenvalues does not match number of eigenvectors \n"); */
/*   // } */
/* #endif */
/*   sMatrix* y=createVector(klength); */

/*   double emax=get(eigd,klength-1); */
/*   double emeanmax=get(emean,klength-1); */
/*   double v=0; */
/*   if (emax==0){ */
/*     flowmsg("ystimrescale: max eigenvalue is 0 \n"); */
/*     v=-1*w/(1-w); */
/*   } */
/*   else { */
/*     //v=-1*sign(emax)*w/(1-w); */
/*     v=-1*sign(getij(emean,klength-1,0))*w/(1-w); */
/*   } */
 
/*   //ediff is the difference between the i+1th eigenvalue  */
/*   //and c0. */
/*   //it has dimensionality d-1 */
/*   double ediff[klength-1]; */
/*   double mag=0;             //keep trag of sum of magnitude */
/*   // so we can normalize the stimulus */
/*   double eigval=0; */

/*   int index=0; */
/*   for (index=0; index<klength-1;index++){ */
/*     eigval=getij(eigd,index,0); */
/*     ediff[index]=eigval-emax;  */
/*     //compute all the components */
/*     //of the stimulus */
/*     double emeani =getij(emean,index,0); */
/*     double yi=emeani*v/(emeanmax+ediff[index]*v); */
/*     setij(y,index,0,yi); */

/*     mag=mag+pow(yi,2); */
/*   } */
 
/*   //set the component corresponding to the max eigenvector */
/*   setij(y,klength-1,0,v);  */
/*   mag=mag+pow(v,2); */

/*   //multiply by -1 to get signs right */
/*   mag=-mmag/sqrt(mag); */
  
/*   //now normalize the stimulus */
/*   for (index=0; index<klength;index++){ */
/*     double yi=getij(y,index,0); */
/*     yi=yi*mag; */
/*     setij(y,index,0,yi);     */
/*   } */


/*   return y; */

/* } */

/* //\***************************************************************************** */
/* //void fishermax(const sGauss* post, double mmag, sGauss* xopt, double *fopt){ */
/* //input:	 */
/* //	post	- structure representing the gaussian psterior */
/* //  mmag    - magnitude constraint on the stimulus *\/ */
/* //Return: */
/* //	xopt	- optimal stimulus */
/* //	fopt	- fisher information at optimal valu */
/* // */
/* //  Explanation: */
/* //  maximize the fisher information  */
/* //  for the case of a single non-overlapping neuronmaximizes the fisher information by calling the corresponding matlab function */
/* #ifdef MATLABENG */
/* void fishermax(const sGauss* post, double mmag){ */
/*   sMatrix* evecs=(sMatrix*) malloc(sizeof(sMatrix)); */
/*   sMatrix*  eigd=(sMatrix*) malloc(sizeof(sMatrix)); */
/*   Engine *ep; */
   
                  
/*   flowmsg("In fishermax. \n"); */

/*   //perform an eigendecomposition on the covariance matrix */
/*   int esuccess=0; */
/*   //esuccess=eig(post->c,  evecs, eigd); */
   
/* #ifdef EMSGS */
/*   if (esuccess==-1){ */
/*     fprintf(stderr,"Error: fishermax eigendecomposition threw error \n"); */
/*   } */
/* #endif */

/*   //project the mean onto the eigenvector */
/*   //sMatrix* emean=matrixProd(evecs,post->m); */
  
/*   if (!(ep = engOpen("matlab -nosplash -nojvm -nodesktop"))) { */
/*     fprintf(stderr, "\nCan't start MATLAB engine\n"); */
	 
/*   } */
/*   //create an output buffer to store output of matlab commands */
/*   char * mBuffer; */
/*   int buffSize=1000; */
/*   mBuffer=(char*) malloc(sizeof(char)*buffSize); */
/*   engOutputBuffer(ep, mBuffer, buffSize);   */
   

/*   //put the arrays intou matlab */
/*   //allocate space in matlab for the array */
/*   //not really sure why I have to allocate space for the matrices */
/*   //why I can't just use the matrices I already have */
/*   //but at least no worry about overriding my data */
/*   //the field is probably a more complicated structure than that */
/*   //I could however make all my arrays mxArrays */
/*   mxArray* mxCovar = mxCreateDoubleMatrix(post->c->dims[0],post->c->dims[1],mxREAL); */

/*   mxArray* mxMean= mxCreateDoubleMatrix(post->m->dims[0],post->m->dims[1],mxREAL); */
   
/*   //copy the actual data to the above arrays */
   
/*   //for the scalar magnitude its already copied to it */
/*   mxArray* mxMag=mxCreateDoubleScalar(mmag); */
   
    
/*   memcpy((void *)mxGetPr(mxCovar), (void *)(post->c->data), sizeof(double)*post->c->dims[0]*post->c->dims[1]); */
   
/*   memcpy((void *)mxGetPr(mxMean), (void *)(post->m->data), sizeof(double)*post->m->dims[0]*post->m->dims[1]);  */

/*   flowmsg("Run matlab commands \n"); */

/*   //put the variables in the matlab workspace */
/*   engPutVariable(ep, "post.c", mxCovar);  */
/*   engPutVariable(ep, "c", mxCovar);  */
/*   matmsg(mBuffer); */

/*   engPutVariable(ep, "post.m", mxMean); */
/*   engPutVariable(ep, "m", mxMean); */
/*   matmsg(mBuffer); */
   
/*   engPutVariable(ep, "mmag",mxMag); */
/*   matmsg(mBuffer); */

/*   //evaluate fishermax */
/*   engEvalString(ep, "setpaths;"); */
/*   //engEvalString(ep, "fishermax(post,mmag);"); */
/*   engEvalString(ep, "post.m=m;"); */
/*   engEvalString(ep, "post.c=c;"); */

/*   engEvalString(ep, "[xopt,fopt]=fishermax(post,mmag);"); */
/*   matmsg(mBuffer); */

/*   flowmsg("Done evaluating matlab commands \n"); */

/*   //get the results from matlab */
/*   mxArray *mxXopt; */
/*   mxXopt=engGetVariable(ep, "xopt"); */
/*   matmsg(mBuffer); */

/*   if (mxXopt==NULL){ */
/*     errmsg("Could not get xopt from matlab \n"); */
/*   } */
/*   mxArray *mxFopt; */
/*   mxFopt=engGetVariable(ep, "fopt"); */
/*   matmsg(mBuffer); */
/*   if (mxFopt==NULL){ */
/*     errmsg("Could not get fopt from matlab \n"); */
/*   } */

/*   //copy the results to sMatrices */
/*   //to facilitate printing */
/*   sMatrix *xOpt=createMatrix(2,post->m->dims); */
/*   double  *fopt; */
/*   fopt=mxGetPr(mxFopt); */
   
/*   memcpy((void *) xOpt->data,(void *)mxGetPr(mxXopt), sizeof(double)*post->m->dims[0]*post->m->dims[1]);  */
   
/*   printf("Optimal stimulus \n"); */
/*   print_mat(xOpt); */
    
/*   printf("fopt: %g \t \n",*fopt); */

/*   /\*create an anonymous function to get fisherinfo for a particular w */
/*   //we multiply by negative 1 b\c fminbnd finds minimum */
/*   //fofw=@(w)(-finfoestim(yrescaled(w,eigd, emean,mmag),eigd,emean)); */
/*   //Find the optimal stimulus */
/*   //we use the rescaled y of lambda and pass that to to finfoestim */
/*   //in the rescaled verison */
/*   //wopt should be between 0 and 1 */
/*   //[wopt]=fminbnd(fofw,0,1); */
        
/*   //compute the optimal stimulus */
/*   //fcase=-1;   %fcase is now obsolete */
/*   //yopt=yrescaled(wopt,eigd, emean,mmag); */
/*   //xopt=eigvec*yopt; */
/*   //fopt=finfoestim(yopt,eigd,emean)   *\/	 */
/* } */
/* #endif */

/* //\****************************************************************************************** */
/* //finfo1neuron -  */
/* //    yunique - stim for unique region - projected onto eigenvectors */
/* //    sOptRegion* unique - pointer to structure with eigenvalues and mean of unique region */
/* //    yshared - stim for shared region */
/* double finfo1neuron(const sMatrix* yunique, const sOptRegion* unique, const sMatrix* yshared, const sOptRegion* shared){ */
/*   //compute the fisherinformation for this unique region and the shared region */
/*   //compute the inner products */
/*   double innerys=0; */
/*   double inneryu=0; */

/*   double yuproj=0;  //projection of stimulus onto mean */
/*   double ysproj=0; */

/*   //compute inner product and dot product of unique region */
/*   int index=0; */
/*   for (index=0;index< unique->sregion->klength;index++){ */
/*     yuproj=yuproj+getij(yunique,index,0)*getij(unique->emean,index,0); */
/*     inneryu=pow(getij(yunique,index,0),2)*getij(unique->eigd,index,0); */
/*   } */

/*   //compute inner product and dot product of shared region       */
/*   for (index=0;index< shared->sregion->klength;index++){ */
/*     ysproj=ysproj+getij(yshared,index,0)*getij(shared->emean,index,0); */
/*     innerys=pow(getij(yshared,index,0),2)*getij(shared->eigd,index,0); */
/*   } */

/*   double finfo=0; */

/*   finfo=exp(ysproj)*exp(yuproj)*exp(1/2*(inneryu+innerys))*(inneryu+innerys); */
/*   return finfo; */

/* } */


