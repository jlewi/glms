#include "simulation.h"
#include "debug.h"
#include "mpiinfo.h"
#include "stdlib.h"
#include "poisson.h"

#ifdef MATLAB
#include "mat.h"
#include "matlabinter.h"
#endif
//initialize the simulation
ssimparam* initsim(int niter, int randstimuli){
  int ISVALID=TRUE;

  if (niter<1){
    emsg("initsim: number of iterations must be at least 1 \n");
    ISVALID=FALSE;
  }

  ssimparam* simparam=malloc(sizeof(ssimparam));
  
  simparam->niter=niter;
  simparam->trial=0;

  //*******************************************************
  if (randstimuli!=0){
    simparam->randstim=TRUE;
  }
  else{
    simparam->randstim=FALSE;
  }
 

  //create the poisson random number generator
  simparam->poissgen=createpoissongen();

  simparam->normgen=createnormalgen();

  //initialize the mpiinfo
  simparam->mpiinfo=creatempiinfo();
 
  //***************************************************************
  //initialize matlabengine to null
  simparam->matlabeng==NULL;


  
  //open the debug file
  opendebugfile(getrank(simparam->mpiinfo));

  if (ISVALID==FALSE){
    emsgm("Was unable to create valid simparam object terminating execution \n");
    exit(FALSE);
  }
  return simparam;
  
}

int getrandstimuli(ssimparam* simparam){
  if (isnullm(simparam,"simparam")==FALSE){
    return TRUE;
  }
  return simparam->randstim;
}

Engine* getmateng(ssimparam* simparam){
  if (isnullm(simparam,"simparam")==TRUE){
    return NULL;
  }
  return simparam->matlabeng;

}
void freesimparam(ssimparam* simparam){
  if (isnullm(simparam,"simparam")==TRUE){
    return;
  }

  //**************************************************************************
  //close the matlab engine
  //*************************************************************************
  #ifdef MATLABENG
  if (isnullm(simparam->matlabeng,"simparam->matlabeng")==FALSE){
    engClose(simparam->matlabeng);
  }
  #endif
  //*************************************************************************
  //clean up the simulation
   //***************************************************************
  //initialize the simulation  
  //create the poisson random number generator
  closepoissongen(simparam->poissgen);

  //don't need to close the normalgenerator b\c they both use the same VSL
  //streem as poissgen therefore it should already be closed.
  //closenormalgen(simparam->normgen);
  //initialize the mpiinfo
  freempiinfo(simparam->mpiinfo);

  //open the debug file
  closedebugfile();

}
int gettrial(ssimparam* smparam){
  return smparam->trial;
}

const spoissgen* getpoissgen(ssimparam* simparam){
  if (isnullm(simparam,"simparam")){
    return NULL;
  }
  if (isnullm(simparam->poissgen,"simparam->poissgen")){
    return NULL;
  }

  return simparam->poissgen;
}

#ifdef MATLAB
//create an mparam object from an mxArray structure stored in a mat file
ssimparam* simparamfrommxarray(mxArray* mxsim){
  //variables we need to load from mxparam
  ssimparam* simparam;

  int ntrials;
  int randstimuli;

  //fieldnames which must be provided
  enum FNAMES {f_ntrials,f_randstimuli};
  //create an array to keep track of whether field was provided
  int fexists[2];
  int NNEEDED=2;   //how many elements in FNAMES are there
  fexists[0]=FALSE;
  fexists[1]=FALSE;

  


  bool isstruct=mxIsStruct(mxsim);
  if (isstruct==0){
    emsgm("simmparamfrommxarray: mxparam is not a structure \n");
    return NULL;
  }

  int numfields=mxGetNumberOfFields(mxsim);

  //stor name of field
  const char* fname;
  //********************************************************************
  //loop through all the fields in the structure and proces them
  int findex;
  for (findex=0;findex<numfields;findex++){
    fname=mxGetFieldNameByNumber(mxsim, findex);


    if(strcmp(fname,"ntrials")==0){
     
      mxArray* mxdata=mxGetField(mxsim,0,fname);
      if (mxdata !=NULL){
	ntrials=getmxint(mxdata);
	fexists[f_ntrials]=TRUE;
      }
    }

 if(strcmp(fname,"randstimuli")==0){
     
      mxArray* mxdata=mxGetField(mxsim,0,fname);
      
      if (mxdata !=NULL){
      randstimuli=getmxint(mxdata);
 fexists[f_randstimuli]=TRUE;
      }
      
    }
}//end loop over all fields


//check all fields were provided
int isvalid=TRUE;

//  fname=malloc(100);
for (findex=0;findex<NNEEDED;findex++){
    
  switch (findex){
  case f_ntrials:
    fname="ntrials";
    break;
  case f_randstimuli:
    fname="randstimuli";
    break;
  }
  if (fexists[findex]!=TRUE){
    isvalid=FALSE;
    fprintf(stderr,"simmparamfrommxarray: error mising field %s \n", fname);
  }
}


if (isvalid==TRUE){
  simparam=initsim(ntrials,randstimuli);
  fmsg("Created simmparam: \n");
  char* msg=malloc(200);
  sprintf(msg,"simparam.ntrials=\t %d\n",ntrials);
  fmsg(msg);
  sprintf(msg,"simparam.randstimuli=\t %d\n",randstimuli);
  fmsg(msg);
  free(msg);

  #ifdef DEBUG
  fmsg("About to return from simparam from mxarray() \n");
  #endif
  return simparam;

}
else{
  return NULL;
}
//free(fname);
}//end mparam from mxarray

#endif
