/*define which element of sMatrix->dims 
  corresponds to rows and columns in a 2d matrix. */
#define RDIM 0
#define CDIM 1

/*define true and false*/
#define TRUE 1
#define FALSE 0

/*include the matlab header so that we can create mxArrays from this file*/
//matlab headers
#ifdef MATLAB
#include "mat.h"
#include "matrix.h"
#ifdef MATLABMEX
#include "mex.h"
#endif
#include "engine.h"
#endif

#ifndef MATRIXOPS_INC
#define MATRIXOPS_INC

//some alternate names of functions
#define copymatrix copyMatrix
#define smatrix    sMatrix
#define mxarraytosmatrix mxArrayTosMatrix
enum {ROW_MAJOR, COL_MAJOR} MATRIX_ORDER; 
/*
define an enumerated type to declare whether matrix is row or column major
*****************************************************
define all my matrix operations and structures
********************************************************
vectors will just be matrices with one dimension of length 1
define a structure to represent a matrix
*/

typedef struct st_Matrix {
  int ndims  ;                //number of dimensions
  int* dims;                  //dimensions
  double* data;              //2d matrix
} sMatrix;

sMatrix* diag(const sMatrix* vec);
/*****************************************************************************
Matrix Manipulation Operations
*****************************************************************************/
sMatrix* array2vector(double* data, int length);

//function to create a matrix
//which allocate space for the Matrix structure
sMatrix* createMatrix(int ndims,const int* dims);
sMatrix* createMatrix2ds(int size);

sMatrix* copyMatrix(const sMatrix* source);
sMatrix* createVector(int length);
int* copyIarray(const int* source,int length);


void copyDarrayTo(double* target, const double* source, int size);
double* copyDarray(const double* source,int size);
void copyMatrixTo(sMatrix* target, const sMatrix* source);


double get(const sMatrix *mat, int index);
double getij( const sMatrix *mat,int row, int col);

//create a square identity matrix of the appropriate size
sMatrix* identitymatrix(int dim);
//***************************************************
//void initimatrix
//    matrix - matrix to initialize
//    ndims  - number of dimensions
//    dims   - dimensions
void initMatrix(sMatrix* matrix,int ndims,int*dims);

#ifdef MATLAB
//********************************************
//sMatrix* mxArrayTosMatrix(mxArray*);
//	 mxArray -
//	 explanation create an sMatrix structure to represent the mxArray (which is matlab's implementation);
sMatrix* mxArrayTosMatrix(mxArray*);

mxArray* smatrixtomxarray(sMatrix* mat);
#endif
int numelements(const sMatrix* mat);

//******************************************
//setij(sMatrix* Mat, int row, int col, double val)
//    Mat - matrix whose element we want to set
//    row - row of matrix to set
//    col - column to set
//    val - value to set;
//
void setij(sMatrix *mat,int row, int col, double val);
void set(const sMatrix *mat, int index,double val);
//******************************************

//setSubMatrix(sMatrix target*, sMatrix* source,int srow, int erow, int scol, int ecol) 
//       target - matrix whose elements we want to set
//       source - matrix we want to copy
//       srow   - start row in target
//       scol   - start col in target
//
//
void setSubMatrix(sMatrix* target, const sMatrix* source,int srow, int scol);

//******************************************
//sMatrix* subMatrix(const sMatrix* matrix, int[2] rows, int[2] cols)
//	Matrix - matrix we want submatrix of 
//	rows   - 2d integer array specifying what rows we want
//			-i.e start and stop
//	cols   - 2d integer array specifying what columns we want

sMatrix* subMatrix(const sMatrix* matrix, int rows[2], int cols[2]);

sMatrix* getSubMatrix(const sMatrix* matrix, int srow, int erow, int scol,int ecol);

int isvector(sMatrix* vec);
int iscolvector(sMatrix* vec);
int isrowvector(sMatrix* vec);

void printmatrix(sMatrix* mat);

//************************************************
//int validindex(const sMatrix*, int index, int dim)
//    matrix - matrix we want to check
//    index  - which index do we want to validate
//    dim    - along which dimension are we validating it
//             this is given as the index into matrix->dims
//             of the dimensionality of that index
int validindex(const sMatrix* matrix, int index, int dim);

//************************************************
//check matrix structure is valid
//************************************************
//int validindex(const sMatrix*)
//    matrix - matrix we want to check
int validmatrix(const sMatrix* matrix);

//*****************************************************
//Free memory functions
void freeMatrix(sMatrix* matrix);

double* smatrixtodarray(const sMatrix* mat);
#endif


