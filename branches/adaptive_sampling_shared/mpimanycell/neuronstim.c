/*
File includes neuron operations that require knowledge of a stimulus or substim object.
To prevent circular dependencies these go in a separate file
*/
#include "neuron.h"
#include "fullstim.h"

//******************************************************
//return part of stimulus innervating this neuron
sFullStim* createfullstim(const sstiminfo* stiminfo, const srfield** rfields,  sMatrix** stims, int n){
  if (isnullm(stiminfo,"stiminfo")==TRUE){
    emsgm("stiminfo is null");
  }
  if (stiminfo->klength<1){
    emsgm("stiminfo->klength <1 \n");
  }

  if(n<1){
    emsgm("n - number of neurons from which to create stimulus must be greater than 0 \n");
  }

  if (isnullm(rfields,"rfields")==TRUE){
    emsgm("rfields is null");
  }

 if (isnullm(stims,"stims")==TRUE){
    emsgm("stims is null");
  }
  //create a matrix for the fullstimulus
  sMatrix* fullstim=createVector(stiminfo->klength);

  for (int index=0;index<n;index++){
    setSubMatrix(fullstim, stims[index],rfields[index]->kstart, 0);
  }

  

  sFullStim* newfull=malloc(sizeof(sFullStim));
  newfull->stim=fullstim;
  newfull->stiminfo=stiminfo;
  newfull->subStims=NULL;
  newfull->nsubStims=n;
  return newfull;
}

//******************************************************
//return part of stimulus innervating this neuron
sMatrix* getneuronstim(sneuron* neuron, sFullStim* fullstim){
  if(isnullm(neuron,"neuron")==TRUE){
    return NULL;
  }

  if (isnullm(fullstim,"fullstim")==TRUE){
    return NULL;
  }

  if (isnullm(fullstim->stim,"fullstim->stim")==TRUE){
    return NULL;
  }

  sMatrix* stim=getSubMatrix(fullstim->stim,neuron->rfield->kstart,neuron->rfield->kstart+neuron->rfield->klength-1,0,0);

  return stim;

}

//*******************************************************************
//create a stiminfo object
//     klength - length of stimulus
//     sregions - ssubStim** pointer to different regions
sstimInfo* createstiminfo(sneuron** neurons, int numneurons){
  sstimInfo* stimInfo=malloc(sizeof(sstimInfo));

  //****************************************************
  //determine klength
  //looop through all neurons and find the last stimulus entry reference
  int klength=0;
  for (int nindex=0;nindex<numneurons;nindex++){
    int length=neurons[nindex]->rfield->kstart+neurons[nindex]->rfield->klength;
    if (length>klength){
      klength=length;
    }
  }

  stimInfo->klength=klength;

  //*********************************************************************
  //create a unique region for each neuron b\c no overlap
  ssubstim** sregions=malloc(sizeof(ssubstim*)*numneurons);
  int nregions=numneurons;

  for (int nindex=0;nindex<numneurons;nindex++){

    sregions[nindex]=createsubStim(neurons[nindex]->rfield->kstart,neurons[nindex]->rfield->klength, &nindex,1);
  }
  //*********************************************************************
  stimInfo->nregions=nregions;

  
  //loop through all regions and count whether its shared or unique region
  stimInfo->nshared=0;
  stimInfo->nunique=0;

  for (int index=0;index<nregions;index++){
    if (sregions[index]->nneurons==1){
      stimInfo->nunique=stimInfo->nunique+1;
    }
    else {
      stimInfo->nshared=stimInfo->nshared+1;
    }
  }

  if (stimInfo->nregions != stimInfo->nshared+stimInfo->nunique){
    emsg("createStimInfo: number of unique and shared regions did not sum to the number of regions \n");
  }

  //now we set the pointers to the shared and unique regions
  stimInfo->substims=malloc(sizeof(ssubStim*)*nregions);
  stimInfo->subshared=stimInfo->substims;
  stimInfo->subunique=stimInfo->substims+stimInfo->nshared;

  //now copy the subregions into the stimulus info
  //keep track of the offset for the shared and unique stimuli
  int oshared=0;
  int ounique=0;;
  for (int index=0;index<nregions;index++){
    if (sregions[index]->nneurons==1){
      //its a unique region
      stimInfo->subunique[ounique]=copysubStim(sregions[index]);
      ounique++;
    }
    else {
      //its a shared region
      stimInfo->subshared[oshared]=copysubStim(sregions[index]);
      oshared++;
    }
  }

  if (stimInfo->nshared>1){
    emsg("createStiminfo: more than 1 shared region which code can't handle \n");
  }

  return stimInfo;
}


