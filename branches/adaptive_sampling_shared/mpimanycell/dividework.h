#ifndef DIVIDEWORK_INC
#define DIVIDE_WORK_INC
typedef struct st_sneuronrng {
  int nstart;
  int num;
  int maxnum;
  int minnum;
} sneuronrng;
char* rngtostring(sneuronrng* rng);
sneuronrng* divideneurons(int myrank, int numneurons, int nnodes);
#endif
