/* File contains all the optimization structures along with any initilizaiton routines
 */
#include "probability.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "debug.h"
#include "matrixops.h"
#include "matrixmath.h"
#include "output.h"
#include "optstruct.h"
#include "model.h"

//matlab headers
#ifdef MATLAB
#include "mat.h"
#include "matrix.h"
#ifdef MATLABMEX
#include "mex.h"
#endif
#include "engine.h"
#endif


//***************************************************************/
//void initOptRegion(sOptRegion* cregion, const Gauss* post, double mmag)
// initialize the cregion object
void initOptRegion(sOptRegion* cregion, const sGauss* post, double mmag,const ssubStim* substim){
  //error checking
  if (cregion==NULL){
    emsg("initOptRegion: cregion is null pointer \n");
}
//set all the pointers to null to begin with
		cregion->post=NULL;
		cregion->eigd=NULL;
		cregion->evecs=NULL;
		cregion->emean=NULL;	// projection of mean onto eigenvectors
		cregion->sregion=NULL;	
		cregion->mmag=0;       //this will be set during our search

		//set the posterior covariance
		cregion->post=(sGauss*)malloc(sizeof(sGauss));
	        
		//get a pointer to the substimulus structure representing the stimulus
		//corresponding to this region
		//we will set a const pointer because we never modify
		cregion->sregion=substim;

		//copy the submatrix representing the posterior
		//we need to get the rows and cols for this submatrix
		//we do that using 
		int rows[2];
		int cols[2];
		rows[0]=cregion->sregion->kstart;
		rows[1]=cregion->sregion->kstart+cregion->sregion->klength-1;
		cols[0]=rows[0];
		cols[1]=rows[1];
		cregion->post->c=subMatrix(post->c,rows,cols);

		//copy the mean
		cols[0]=0;
		cols[1]=0;
		cregion->post->m=subMatrix(post->m,rows,cols);


		//********************************************************************
		//perform an eigen decomposition of this region of the stimulus
		//******************************************************************
		//allocate space for the eigenvectors and eigenvalues
		cregion->evecs=(sMatrix*) malloc(sizeof(sMatrix));
		cregion->eigd=(sMatrix*) malloc(sizeof(sMatrix));

		//perform the eigendecomposition
		double esuccess=eig(cregion->post->c,  cregion->evecs, cregion->eigd);

		if (esuccess==-1){
		  emsg("Error: decompose: eigendecomposition of covariance of matrix for region \n");
		}
		//compute the projection
		cregion->emean=matrixProd(transpose(cregion->evecs),cregion->post->m);

}


 



//free opt region
void freeOptRegion(sOptRegion* optregion){
	if(optregion==NULL){
		emsg("freeOptReigion: attempted to free null pointer \n");	
	} 
	
	freeGauss(optregion->post);		//posterior
  freeMatrix(optregion->emean);	// projection of mean onto eigenvectors
  freeMatrix(optregion->eigd);	//eigenvalues

  freeMatrix(optregion->evecs);   //eigenvectors
  //we do not free the sregion because we have a constant pointer to it
  //we delete sregions when we delete the stimulus
  //freesubStim(optregion->sregion);	//structure representing information
 
 free(optregion);
  				
}
