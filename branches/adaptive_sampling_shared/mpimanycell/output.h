#include <stdio.h>
#include "matrixops.h"

void print_mat(sMatrix* mat);

void print_farray(float *, int);
void print2d_farray(float **, int, int );
void print2d_iarray(int **, int, int );

void print_darray(double *, int);
void print2d_darray(double **, int, int );

