#include <stdio.h>
#include "matrixops.h"

//*****************************************
//functions for handling/printing output 
//****************************************
//print a 2d double matrix
void print_mat(sMatrix* mat){
  int row;
  int col;
   
  for (row=0; row< mat->dims[0];row++){
  for (col=0; col< mat->dims[1];col++){
    printf("%0.2f \t",getij(mat,row,col));
  }
  printf("\n");
  }

}

void print_darray(double * array, int length){
  int index;
  for (index=0;index<length;index++){
    printf("%0.2g \t",array[index]);
  }
  printf("\n");
}
void print2d_darray(double ** array, int rows, int cols){
  int index;
  int row;
  int col;
  for (row=0; row<rows;row++){
  for (col=0; col<cols;col++){
    printf("%0.2f \t",array[row][col]);
  }

  printf("\n");
  }
}
void print_farray(float * array, int length){
  int index;
  for (index=0;index<length;index++){
    printf("%0.2g \t",array[index]);
  }
  printf("\n");
}
void print2d_farray(float ** array, int rows, int cols){
  int index;
  int row;
  int col;
  for (row=0; row<rows;row++){
  for (col=0; col<cols;col++){
    printf("%0.2g \t",array[row][col]);
  }

  printf("\n");
  }
}

void print2d_iarray(int ** array, int rows, int cols){
  int index;
  int row;
  int col;
  for (row=0; row<rows;row++){
  for (col=0; col<cols;col++){
    printf("%d \t",array[row][col]);
  }

  printf("\n");
  }
}

void print_iarray(int *array,  int cols){
  int index;
  int row;
  int col;
  
 for (col=0; col<cols;col++){
    printf("%d \t",array[col]);
  }

  printf("\n");
  
}
