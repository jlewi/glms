#include "probability.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "debug.h"
#include "matrixops.h"
#include "matrixmath.h"
#include "output.h"
#include "optstruct.h"
#include "optimization.h"
#include "model.h"
#include "opttest.h"
#include "substim.h"
#include "fullstim.h"
#include "stiminfo.h"
#include "mparam.h"


//matlab headers
#ifdef MATLAB
#include "mat.h"
#include "matrix.h"
#ifdef MATLABMEX
#include "mex.h"
#endif
#include "engine.h"
#include "matlabinter.h"
#endif

//*****************************************************************************
//main optimization function
//*****************************************************************************
//This is a test function for calling various routines to test the routines I wrote
//
// -mat <filename>  = this argument specifies the matlab file containing the test data
// -test <test name> - specifies which test routine to call
int main (int argc, char **argv){

  //defaults
  char* fname=(char*)malloc(200);
  char* test=(char*)malloc(200); //name of test to run
   
  int fileprovided=FALSE;


  //set up MPI
#ifdef MPI
  MPI_Init( &argc, &argv );  
#endif
  //***************************************************************
  //process the command line arguments
  //*************************************************************
  int argind=1;
  
  while(argind <argc ){
    if (strcmp(argv[argind],"-mat")==0){
      fname=argv[argind+1];   
      fileprovided=TRUE;
    }
    else if (strcmp(argv[argind],"-test")==0){
      test=argv[argind+1];
    
    }
    argind=argind+2;  
  }

  //***********************************************************
  //call the appropriate test function
  //**********************************************************
  if (strcmp(test,"yrescale")==0){
    testystimrescale(fname);	
		
  }
  else{
    //was a file provided
    if (fileprovided==TRUE){
      testoptimizefile(fname);
	     
    }
    else{
      //check if default file exists
      //do this by trying to read it
      FILE* fp;
      int defexists=FALSE;
      fp=fopen("simdata.mat","r");
      if (fp !=NULL){
	defexists=TRUE;
	fclose(fp);
      }
	   
      if (defexists==TRUE){
	testoptimizefile("simdata.mat");
      }
      else{
	testoptimize();
      }
    }
	  
	  
    //printf("Unkown test %s \n",test); 
	
  }

  //close the debugging
  closedebugfile();


}

//****************************************************************************
//test the optimization routine
//load the data from a file
//***********************************************************
void testoptimizefile(char* fname){
  void** vars=NULL;  //save variables loaded from file
  int nvars=0;
  int err;
  //read the data from the mat file
  vars= readsimparam(fname, &nvars, &err);

  //get the parameters
  smParam* mparam=(smParam*)(vars[0]);


  //free the variables
  freemParam(mparam);

  free(vars);
}
//********************************************************************************
//test the optimization 
//*******************************************************************************
void testoptimize(){
  //create an mparam structure
  int klength =3;              //length of full stimulus
  int nregions=3;             //how many regions are we creating
  double ktrue[]={.01, .02,.03};
  
  //simulation paramaters;
  int niter=1;

  //*********************************************************
  //Create the stimulus structure
  //*******************************************************
  ssubStim ** sregions=NULL;
  sregions=malloc(sizeof(ssubStim*)*nregions);

  //create 3 regions only one of which is shared
  //we will divide it upinto 3 equal sized regions
  //compute lengths of shared and unique region
  int slength=floor((double)klength)/nregions;
  int ulength=klength-2*slength;

  //shared region
  int sneurons[2];
  sneurons[0]=0; 
  sneurons[1]=1;
  sregions[0]=createsubStim(0,slength,sneurons, 2);

  int uneuron;
  uneuron=0; 
  sregions[1]=createsubStim(slength,ulength,&uneuron, 1);
  
  uneuron=1; 
  sregions[2]=createsubStim(slength+ulength,ulength,&uneuron, 1);

  //********************************************************************
  //create mparam
  //********************************************************************
  double mmag=1;
  smParam* mparam=createmParam(klength,sregions,nregions,mmag);

  //set ktrue
  sMatrix* ktruevec=array2vector(ktrue,klength);
  setktrue(mparam,ktruevec);  
  free(ktruevec);
  
  //*********************************************************************
  //create the simulation structure
  //****************************************************************************
  ssimparam* simparam=initsim(niter);
  

  //*****************************************************************************
  //create the prior
  //******************************************************************************
  sMatrix* mean=createVector(klength);
  sMatrix* covar=identitymatrix(klength);
  sGauss* prior=createGauss(mean, covar);

  sFullStim* optstim=NULL;
  optstim=optimize(simparam, mparam, prior);

  ///***************************************************************************  
  //save data to a mat file
  //cleanup
#ifdef MATLAB
  int nstim=1;
  sFullStim** allstim = malloc(sizeof(sFullStim* )*nstim);

  allstim[0]=optstim;

  int nvars=1; //number of variables to save
  smxsave* vtosave=malloc(sizeof(smxsave)*nvars);
  vtosave[0].data=createmxfullstim(allstim,nstim);
  vtosave[0].name="optstim";

  //try wrtiing a matrix
  //vtosave[0].data=smatrixtomxarray(mparam->ktrue);
  //vtosave[0].name="ktrue";
  char* outfile="opttestout.mat";

  writematfile(outfile,vtosave,nvars);

  
  
  //free the mxarrays
  int vindex;
  for (vindex=0;vindex<nvars;vindex++){
    mxDestroyArray(vtosave[vindex].data);
  }
  //free array of allstim
  free(allstim);
#endif

  //free the memory
  free(mean);
  free(covar);

  for (int index=0; index < nregions;index++){
    freesubStim(sregions[index]);
  }
  free(sregions);
}
//*****************************************************************************
//test the stimulus rescaling function
//*******************************************************************************
void testystimrescale(char* fname){
#ifdef MATLAB
  char* msg=malloc(200);
  int ERROR=FALSE; //indicates if error occured;
  //open the matlab file that contains the data
  printf("Test ystim rescale: data file %s \n", fname);
  MATFile *mfile=matOpen(fname, "r");
  if (mfile == NULL) {
    printf("Error opening file %s\n", fname);
    return;
  }

  //data inside the mat file will be a structure array called "trials"
  //trials will have the following fields
  //.klength - length of data
  //.c - klengthxklength matrix covariance matrix
  //.m - mean
  //.emean - mean projected onto stimulus
  //.evecs - eigenvectors
  //.eigd  - eigenvalues
  //.w     - an array of w values 1xnumw
  //.yrescaled - a klengthxnumw matrix of the appropriately rescaled y 
  //get the array get trials from the matfile
  mxArray* trials =NULL;
  trials=matGetVariable(mfile, "trials");	
  if (trials==NULL){
    emsg("testystimrescale: could not load trials from matlab file \n");        printf("could not load trials \n");

  }
  mxAssert(trials,msg);

  //get the number of trials
  //this should be a 1xn matrix
  const int *trialdims= mxGetDimensions(trials);
  int ndims =mxGetNumberOfDimensions(trials);  //get number of dimensions
  printf("There are %d dimensions for trial dims \n", ndims);
  printf("There are %d trials \n" , trialdims[1]);
  sOptRegion* region=NULL;
	
  //now we loop over the trials
  for (int tindex=0; tindex < trialdims[1]; tindex++){
    //first we get a point to the actual trial
    //I think the matrix is automatically indexed in row major form
    mxArray* klength=mxGetField(trials, tindex, "klength");
		
    mxArray*  c =mxGetField(trials,tindex,"c");
    mxArray*  m = mxGetField(trials,tindex,"m");
    mxArray* emean=mxGetField(trials,tindex,"emean");
    mxArray* evecs=mxGetField(trials,tindex,"evecs");
    mxArray* eigd=mxGetField(trials,tindex,"eigd");
    mxArray* w=mxGetField(trials,tindex,"w");
    mxArray* mxxstimrescale = mxGetField(trials,tindex,"xrescaled");
    mxArray* mmag=mxGetField(trials,tindex,"mmag");
    double dmmag=*((double *)(mxGetData(mmag)));
    //create sMatrix objects
    region=(sOptRegion*)malloc(sizeof(sOptRegion));
		
    //region->emean= mxArrayTosMatrix(emean);
    //region->evecs= mxArrayTosMatrix(evecs);
    //region->eigd=mxArrayTosMatrix(eigd);
    //region->mmag=*((double *)(mxGetData(mmag)));
    sMatrix* smean=mxArrayTosMatrix(m);
    sMatrix* scovar=mxArrayTosMatrix(c);

    //initialize the region
    //sGauss* post=createGauss(smean,scovar);

    //perform an eigendecomposition
    sMatrix* sevecs=malloc(sizeof(sMatrix));
    sMatrix* seigd=malloc(sizeof(sMatrix));
    eig(scovar, sevecs, seigd);
    sMatrix* semean=matrixProd(transpose(sevecs),smean);
    sMatrix* correctystim=mxArrayTosMatrix(mxxstimrescale);

    //now loop over all values of w
    const int* wdims=mxGetDimensions(w);
    double *wdata=mxGetData(w);
    for (int windex=0;windex<wdims[1];windex++){
      //get the w value
      double wval=wdata[windex];

      //we need to check the full stimulation
      //not in eigenspace because
      //matlab might have used different eigenvectors
      sMatrix* yrescaled=ystimrescale(wval,seigd,semean,dmmag);
      sMatrix* xrescaled=matrixProd(sevecs,yrescaled);
      sMatrix* xcorrect=getSubMatrix(correctystim,0,correctystim->dims[0]-1,windex,windex);

			
      //check if the two matrices are the same
      if (matrixEqual(xrescaled,xcorrect)==FALSE){
	ERROR=TRUE;
	printf("Error for w=%g \n", wval);
	printf("computed \t correct \n");
	for (int row=0;row< xrescaled->dims[0];row++){
	  printf("%g \t %g \n", getij(xrescaled,row,0),getij(xcorrect,row,0));
	}
					
      }
			
      freeMatrix(yrescaled);
      freeMatrix(xcorrect);
      freeMatrix(xrescaled);
    }
    freeMatrix(correctystim);
    freeMatrix(semean);
    freeMatrix(seigd);
    freeMatrix(sevecs);

		
    //free all the arrays
    mxFree(klength);
    mxFree(c);
    mxFree(m);
    mxFree(evecs);
    mxFree(eigd);
    mxFree(w);
    mxFree(mxxstimrescale);
  }
	
	
  //free all the arrays
  mxFree(trials);
  if (matClose(mfile) != 0) {
    printf("Error closing file %s\n",fname);
    
  }
	
  if (ERROR==TRUE){
    printf("An error was detected in the y rescaling function \n");
  }
  else {
    printf("No error detected in the y rescaling function \n");
  }
#endif
}
//***************************************************************************
//the code below is a test file for calling matlab from c
//*************************************************************************
void testmatlab(){
  sGauss* post=(sGauss*)(malloc(sizeof(sGauss)));

  //create the matrix
  post->c=createMatrix2ds(2);
  setij(post->c,0,0,2);
  setij(post->c,0,1,0);
  setij(post->c,1,0,0);
  setij(post->c,1,1,1);

  post->m=createVector(2);
  setij(post->m,0,0,1);
  setij(post->m,1,0,1);

#ifdef MATLAB
  fishermax(post, 1);
#endif
}
