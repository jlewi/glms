#include "searchresult.h"
#include "stdlib.h"
#include "debug.h"

//*************************************************************************************
//Functions to initialize/destroy structures free the memory used by the search result structure
//************************************************************************************
//sSearchResult* initSearchResult(int nshared, int nunique)
//      nshared - number of shared regions
//      nunique - number of unique regions
//Explanation: allocate space for the Search Result structure
//
sSearchResult* initSearchResult(int nshared, int nunique){

if (nshared <=0){
emsg("initSearchResult: nshared is negative \n");
}
if (nunique <=0){
emsg("initSearchResult: nunique is negative \n");
}

  sSearchResult* result=(sSearchResult*)malloc(sizeof(sSearchResult));

  //set values to zero
  result->finfo=0;
  result->nysopt=nshared;
  result->nyuopt=nunique;
result->smag =0;
result->sproj=0;

  //ysopt and yuopt are arrays of pointers
  //allocate space for the arrays  
  result->ysopt=(sMatrix**)malloc(sizeof(sMatrix*)*nshared);
  result->yuopt=(sMatrix**)malloc(sizeof(sMatrix*)*nunique);

  
//set the pointers to null
//set the vectors to scalars of length 0
//do this so that if we call freesearchresult we don't get error
for (int index=0;index<nshared;index++){
result->ysopt[index]=createVector(1);
}
for (int index=0;index<nunique;index++){
result->yuopt[index]=createVector(1);
}
return result;
  
}

//*********************************************************************************************
//Free memory functions
void freeSearchResult(sSearchResult* result){


 if (result==NULL){
   emsg("freeSearchResult: attempt to free null pointer SearchResult \n");
 }

//free all the elements
//loop through all the elements and free actual matrices
for (int index=0;index<result->nysopt;index++){
freeMatrix(result->ysopt[index]);
}
for (int index=0;index<result->nyuopt;index++){
freeMatrix(result->yuopt[index]);
}

// now free the pointer array
 if (result->ysopt==NULL){
   emsg("freeSearchResult: attempt to free null pointer result->ysopt \n");
 }
 else{
 free(result->ysopt);
 }
 if (result->yuopt==NULL){
  emsg("freeSearchResult: attempt to free null pointer result->yuopt \n");

}
 else{
free(result->yuopt);
 }

free(result);
}
