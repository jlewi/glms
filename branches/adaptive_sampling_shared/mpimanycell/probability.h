#include "matrixops.h"

/* file contains structures and functions related to 
 probability.
*/


#ifndef PROBABILITY_INC
   #define PROBABILITY_INC 1
   #define creategauss createGauss
typedef struct st_Gauss {
  sMatrix* c;
  sMatrix* m;
} sGauss,sgauss;


mxArray* createmxgauss(sgauss** post,int num);
sGauss* createGauss(sMatrix* mean, sMatrix* covar);
sGauss* copygauss(sgauss* source);
void printgauss(sgauss* gauss);

//funciton to free memory for gaussian
void freeGauss(sGauss *);
sgauss* sgaussfrommxarray(mxArray* mxgauss,int gindex);
#endif
