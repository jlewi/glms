#ifndef DEBUG_INC
#define DEBUG_INC
//shortcut to call the isnull macro
#define isnullm(ptr,msg)   isnull(ptr,msg,__FILE__,__LINE__)

#define emsgm(msg)         emsgsl(msg,__FILE__,__LINE__)
#define wmsgm(msg)         wmsgfl(msg,__FILE__,__LINE__)
#define matmsg(msg)        matmsgfl(msg,__FILE__,__LINE__)
#define fmsgm(msg)         fmsgfl(msg,__FILE__,__LINE__)
#ifdef TRACES
#define tmsgm         fmsgfl("",__FILE__,__LINE__)
#else
#define tmsgm  
#endif
/********************************************/
//includes functions for handling debugging
//*******************************************/
void flowmsg(char* );
void fmsgfl(char*, char* fname, int line);

void fmsg(char *);
void errmsg(char*);
void emsg(char*);
//fname - name of file were error occured
//line - line number of error
void emsgsl(char* msg,char* fname, int line);
void wmsg(char*);
void warning(char*);
void wmsgfl(char* msg, char* file, int line);
//print matlab output
void matmsgbase(char* msg);
void matmsgfl(char* msg, char * fname, int line);
int isnull(const void * ptr, char* ptrname, char* file, int line);
void opendebugfile(int rank);

void closedebugfile();
void openfps(char* fname);
void closefps();
int MATLABMSG;
#endif
