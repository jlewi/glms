#include "matrixops.h"
#include "matrixmath.h"
#include "math.h"


#ifndef NEWTON_INC
#define NEWTON_INC
void newton_update_all(ssimparam* simparam, smparam* mparam, sFullStim* stim);
void newton_update_neuron(sneuron* neuron, sFullStim* fullstim,ssimParam* simparam,smparam* mparam);
#endif
