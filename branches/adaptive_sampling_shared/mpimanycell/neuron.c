#include "neuron.h"
#include "probability.h"
#include "stdlib.h"
#include "debug.h"
#include "model.h"
#include <string.h>
#include "matrixops.h"

#ifdef MATLAB
#include "matlabinter.h"
#include "mat.h"
#endif
sneuron* createneuron(int ntrials,sgauss* prior,sMatrix* ktrue, int kstart, int klength){
  
  if (ntrials <1){
    emsg("createneuron: ntrials must be >= 1 \n");
  }
  
  if (isvector(ktrue)==FALSE){
    emsgm("ktrue must be a vector \n");
  }

  if (ktrue->dims[0] !=klength){
    emsgm("klength and length of ktrue should be the same \n");

  }
  sneuron* neuron=malloc(sizeof(sneuron));

  neuron->ntrials=ntrials;

  //create array to store results
  neuron->post=malloc(sizeof(sgauss*)*ntrials);
  for (int index=0;index<ntrials;index++){
    neuron->post[index]=NULL;
  }

  //if (isnullm(prior,"prior")==FALSE);
  isnullm(prior,"prior");
  neuron->prior=copygauss(prior);

  neuron->rfield=createrfield(kstart, klength);

  neuron->resp=malloc(sizeof(int)*ntrials);

  neuron->ktrue=copymatrix(ktrue);
  
  return neuron;
}




srfield* createrfield(int kstart, int klength){
  if (kstart<0){
    emsgm("kstart must be >=zero");
  }
  
  if (klength <1){
    emsgm("klength must be > 0");
  }
  
  srfield* rfield=malloc(sizeof(srfield));

  rfield->kstart=kstart;
  rfield->klength=klength;

  return rfield;
}

sneuron* copyneuron(sneuron* neuron){
  if (isnullm(neuron,"neuron")==TRUE){
    return NULL;
  }

  sneuron* copy;
  copy=createneuron(neuron->ntrials,neuron->prior,neuron->ktrue, neuron->rfield->kstart, neuron->rfield->klength);
  return copy;

}

//**********************************************************************************
//get klength for this neuron
//*********************************************************************************
int getneuronklength(sneuron* neuron){
  if(isnullm(neuron,"neuron")==TRUE){
    return 0;
  }

  if (isnullm(neuron->ktrue,"neuron->ktrue")==TRUE){
    return 0;
  }
  
  return neuron->ktrue->dims[0];
}



//**********************************************************************************
//get ktrue for this neuron
//*********************************************************************************
sMatrix* getktrue(sneuron* neuron){
  if(isnullm(neuron,"neuron")==TRUE){
    return NULL;
  }

  if (isnullm(neuron->ktrue,"neuron->ktrue")==TRUE){
    return NULL;
  }
  
  return copymatrix(neuron->ktrue);
}

//*********************************************************************************
//
sgauss* getneuronpost(sneuron* neuron, int trial){
  //we subtract 1 from the trial because c matrices 
  //start indexes at 0
  trial=trial-1;
  if (isnullm(neuron,"neuron")==TRUE){
    return NULL;
  }
  //validate the trial
  if ((trial<-1) || (trial >=neuron->ntrials)){
    emsgm("trial is not a valid trial number \n");
  }

  if (trial==-1){
    //return the prior
    if (isnullm(neuron->prior,"neuron->prior")==TRUE){
      return NULL;
    }
    else{
      return copygauss(neuron->prior);
    }
  }
  else{	
    if (isnullm(neuron->post[trial],"neuron->post[trial]")==FALSE){
      return copygauss(neuron->post[trial]);
    }
    else{
      return NULL;
    }
  }
}
//**********************************************************************************
//
void setobsrv(sneuron* neuron, int trial, int nspikes){
  //subtract 1 from trial
  //because of c indexing
  trial=trial-1;
  if (isnullm(neuron,"neuron")==TRUE){
    return;
  }
  if ((trial<0) || (trial>=neuron->ntrials)){
    emsgm("Invalid trial. Can't set observation \n");
    return;
  }
  neuron->resp[trial]=nspikes;
}	



void printrfield(srfield* rfield){
  if (isnullm(rfield,"rfield")==TRUE){
    return;
  }

  char* msg=malloc(200);
  sprintf(msg,"rfield.klength=%d \n",rfield->klength);
  fmsg(msg);
  
  sprintf(msg,"rfield.kstart=%d \n",rfield->kstart);
  fmsg(msg);

  free(msg);
}

void printneuron(sneuron* neuron){
  if (isnullm(neuron,"neuron")==TRUE){
    return;
  }

  char* msg=malloc(200);
 
  sprintf(msg,"neuron.rfield \n");
  fmsg(msg); 
  printrfield(neuron->rfield);

  sprintf(msg,"neuron.ktrue: \n");
  fmsg(msg);
  printmatrix(neuron->ktrue);

  sprintf(msg,"neuron.prior: \n");
  fmsg(msg);
  printgauss(neuron->prior);

  free(msg);
}
void freerfield(srfield* rfield){
  if (isnullm(rfield,"rfield")==TRUE){
    return;
  }
  free (rfield);
}	      
void freeneuron(sneuron* neuron){
  if (isnullm(neuron,"neuron")==TRUE){
    return;
  }

  freerfield(neuron->rfield);

  //free the posteriors
  for(int tindex=0;tindex<neuron->ntrials;tindex++){
    freeGauss(neuron->post[tindex]);
  }
  free(neuron->post);

  //free the prior
  freeGauss(neuron->prior);

  freeMatrix(neuron->ktrue);

  if (isnullm(neuron->resp,"neuron->resp")==FALSE){
    free(neuron->resp);
  }
  
  free(neuron);
}



#ifdef MATLAB
//    mxstimreg -a structure array of substimuli
//    sindex - which element of structure array we get
sneuron* neuronfrommxstruct(mxArray* mxneurons,int nindex){
  //variables we need to load from mxparam
  int kstart;
  int klength;
  int ntrials;
  sMatrix* ktrue;
  sgauss* prior;
  sneuron* neuron;

  //fieldnames which must be provided
  enum FNAMES {f_ntrials, f_klength, f_kstart, f_ktrue,f_prior};

  //create an array to keep track of whether field was provided
  int fexists[5];
  int NNEEDED=5;   //how many elements in FNAMES are there
  fexists[0]=FALSE;
  fexists[1]=FALSE;
  fexists[2]=FALSE;
  fexists[3]=FALSE;
  fexists[4]=FALSE;

  int isstruct=mxIsStruct(mxneurons);
  if (isstruct==0){
    emsg("neuronfrommxarray: mxneurons is not a structure \n");
    return NULL;
  }

  int numfields=mxGetNumberOfFields(mxneurons);

  //stor name of field
  const char* fname;
  //********************************************************************
  //loop through all the fields in the structure and proces them
  int findex;
  for (findex=0;findex<numfields;findex++){
    fname=mxGetFieldNameByNumber(mxneurons, findex);

    if(strcmp(fname,"kstart")==0){
      
      mxArray* mxdata=mxGetField(mxneurons,nindex,fname);
      if (mxdata!=NULL){
	fexists[f_kstart]=TRUE;
	kstart=getmxint(mxdata);
	mxFree(mxdata);
      }
    }
    
    if(strcmp(fname,"klength")==0){     
      mxArray* mxdata=mxGetField(mxneurons,nindex,fname);
      if (mxdata!=NULL){
	fexists[f_klength]=TRUE;
	klength=getmxint(mxdata);      
	mxFree(mxdata);
      }
    }

    if(strcmp(fname,"ntrials")==0){      
      mxArray* mxdata=mxGetField(mxneurons,nindex,fname);
      if (mxdata!=NULL){
	fexists[f_ntrials]=TRUE;
	ntrials=getmxint(mxdata);
	mxFree(mxdata);
      }
    }
    if(strcmp(fname,"ktrue")==0){
     
      mxArray* mxdata=mxGetField(mxneurons,nindex,fname);
      if (mxdata!=NULL){
	ktrue=mxarraytosmatrix(mxdata);      
	fexists[f_ktrue]=TRUE;
	mxFree(mxdata);
      }
    }
    if(strcmp(fname,"prior")==0){
     
      mxArray* mxdata=mxGetField(mxneurons,nindex,fname);
      if (mxdata!=NULL){
	fexists[f_prior]=TRUE;
	prior=sgaussfrommxarray(mxdata,0);   
	//will I get double free
	mxFree(mxdata);
      }
    }

  }//end loop over all fields


  //check all fields were provided
  int isvalid=TRUE;

  char* msg=malloc(200);
  //  fname=malloc(100);
  for (findex=0;findex<NNEEDED;findex++){
    
    switch (findex){
    case f_kstart:
      fname="kstart";
      break;
    case f_klength:
      fname="klength";
      break;
    case f_ktrue:
      fname="ktrue";
      break;
    case f_prior:
      fname="prior";
      break;
    case f_ntrials:
      fname="ntrials";
      break;
    }
    if (fexists[findex]!=TRUE){
      isvalid=FALSE;
      sprintf(msg,"neuronfrommxstruct: error mising field %s \n", fname);
      emsg(msg);
    }
  }

  
  free(msg);
  if (isvalid==TRUE){
    neuron=createneuron( ntrials,prior,ktrue, kstart, klength);
    freeMatrix(ktrue);
    freeGauss(prior);
    
    // ssubstim* substim= createsubStim(kstart, klength, neurons, nneurons);
    //free(neurons);
    return neuron;
  }
  else{
    return NULL;
  }



}
#endif

void setneuronpost(sneuron* neuron, int trial, sgauss* post){
  //subtract 1 from trial because of c indexing
  trial=trial-1;

  isnullm(neuron,"neuron");
  isnullm(post,"post");

  if ((trial<0)|| trial>=neuron->ntrials){
    char* msg=malloc(100);
    sprintf(msg,"Invalid trial: trial=%d \n",trial);
    emsgm(msg);
    free(msg);
  }

  //make sure we haven't already created this value
  if (neuron->post[trial]!=NULL){
    emsgm("Posterior for trial was not null. You are overwriting its value");
    freeGauss(neuron->post[trial]);
  }

  //copy the new post
  sgauss* cpost=copygauss(post);
  neuron->post[trial]=cpost;

}


#ifdef MATLAB

mxArray* mxstructfromneurons(sneuron** neurons, int num){
  mxArray * mxneurons;
  mxArray * mxpost;
  int ndim=1;
  int dims[1];
  dims[0]=num;
  
  int nfields=5;


  int CBUFFSIZE=200;
  const char** field_names=malloc(sizeof(char*)*nfields);
  field_names[0]="ktrue";
  field_names[1]="prior";
  field_names[2]="post";
  field_names[3]="rfield";
  field_names[4]="resp";
  

  mxneurons=mxCreateStructArray(ndim,dims,nfields,field_names);
  
  //we only loop over those neurons which we have

  //loop over all the neurons
#ifdef Debug
  fmsgm("");
#endif
  for (int nindex=0;nindex<num;nindex++){
   
    tmsgm;
    mxArray* mxprior=createmxgauss(&(neurons[nindex]->prior),1);
    isnullm(mxprior,"mxprior");
    mxArray* mxktrue=smatrixtomxarray(neurons[nindex]->ktrue);
    isnullm(mxktrue,"mxktrue");

   
    tmsgm;
    //we will not allways have the posteriors for all neurons
    //we only save them if we do
    //we only save the responses if we have the posteriors
    if ((neurons[nindex]->post[0] !=NULL) && ((neurons[nindex]->post[neurons[nindex]->ntrials-1])!=NULL)){
      tmsgm;

 #ifdef DEBUG
    char * msg=malloc(100);
    sprintf(msg, "nindex=%d num=%d \n",nindex,num);
    fmsgm(msg);
    free(msg);
    #endif
   
    //to avoid filling up the disk only save the last posterior when the number of neurons
    //is greater than 10
    if (num>1){
      sgauss** post=malloc(sizeof(sgauss*));
      post[0]=neurons[nindex]->post[neurons[nindex]->ntrials-1]; 
        mxpost =createmxgauss(post,1);
	free(post);

    }
 else{
      mxpost =createmxgauss(neurons[nindex]->post,neurons[nindex]->ntrials);

    }
      isnullm(mxpost,"mxpost");
      tmsgm;
      mxSetField(mxneurons, nindex,field_names[2], mxpost);
    

      tmsgm;
      mxArray* mxresp = mxCreateDoubleMatrix(1, neurons[nindex]->ntrials,mxREAL);
      isnullm(mxresp,"mxresp");
      double* resppt=mxGetPr(mxresp);
      //copy the responses to the double array
      for (int tindex=0;tindex<neurons[nindex]->ntrials;tindex++){
	resppt[tindex]=(double)(neurons[nindex]->resp[tindex]);

      }
      mxSetField(mxneurons, nindex,field_names[4], mxresp);
    
    tmsgm;
    mxSetField(mxneurons, nindex,field_names[0],mxktrue);
    mxSetField(mxneurons, nindex,field_names[1], mxprior);
  
    }
    //mxFree(m);
    //mxFree(c);
  }
  tmsgm;
  return mxneurons;

}
#endif
