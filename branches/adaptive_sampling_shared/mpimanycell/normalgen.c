#include "mkl_vsl.h"
#include <stdlib.h>
#include "debug.h"
#include "normalgen.h"
#include <time.h>
#include "matrixops.h"
#include "matrixmath.h"
#include <math.h>


snormalgen* createnormalgen(){
  unsigned long current_time = time(NULL);
  snormalgen* gen=malloc(sizeof(snormalgen));
  gen->stream=malloc(sizeof(VSLStreamStatePtr));
  vslNewStream(&(gen->stream), VSL_BRNG_MCG31, (unsigned int) current_time);

  return gen;
}
void closenormalgen(snormalgen* normalgen){
  
  if (isnullm(normalgen,"normalgen")==TRUE){
    return;
  }
  if (isnullm(normalgen->stream,"normalgen->stream")==FALSE){
    vslDeleteStream(normalgen->stream);
  }

  free(normalgen);
}

//length is the length of the stimulus
//mag is constraint on normalization
sMatrix* normrnd(snormalgen* gen, int length, double mmag){
  isnullm(gen,"snormalgen* gen");


  int num=1;  //number to generate
  double rndnums[length];
  double mean=0;
  double sigma=1;
  sMatrix* rmat;
  vdRngGaussian(VSL_METHOD_SGAUSSIAN_BOXMULLER,gen->stream,length,rndnums,mean,sigma);

  //normalize it
  rmat=array2vector(rndnums,length);

  //magnitude
  double rmmag=dotprod(rmat,rmat);
  double normconst=mmag/sqrt(rmmag);

  for (int index=0;index<length;index++){
    rndnums[index]=rndnums[index]*normconst;
  }
  
  freeMatrix(rmat);
  rmat=array2vector(rndnums,length);
  return rmat;
}
