#include "model.h"
#include "optstruct.h"
#include "searchrange.h"
#include "matrixops.h"
#include "matrixmath.h"
#include "probability.h"
#include "simulation.h"
#include "mparam.h"

#ifndef OPTIMIAZATION_INC
   #define OPTIMIZATION_INC 




sFullStim* optstim(ssimparam* simparam, smparam* mparam);

sMatrix** optstim_neurons(sneuron** neurons, int numneurons,int trial, ssimparam* simparam, smparam*  mparam);
#endif
