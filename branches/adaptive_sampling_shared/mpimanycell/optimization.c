#include "probability.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "debug.h"
#include "matrixops.h"
#include "matrixmath.h"
#include "mpiinfo.h"
#include "output.h"
#include "optstruct.h"
#include "optimization.h"
#include "model.h"
#include "opttest.h"
#include "fullstim.h"
#include "searchresult.h"
#include "simulation.h"
#include "mparam.h"
#include "probability.h"
#include "neuronstim.h"
#include "dividework.h"

#ifdef MPI
#include "mpi.h"
#endif

//matlab headers
#ifdef MATLAB
#include "mat.h"

#include "matrix.h"
#ifdef MATLABMEX
#include "mex.h"
#endif
#include "engine.h"
#endif

sFullStim* optstim(ssimparam* simparam, smParam* mparam){
  sneuron** neurons;//which neurons we optimize for

  
  const srfield** rfields;
   
  sFullStim* fullstim=NULL;  //new full stimulus
  sMatrix** stims=NULL;     //optimal stimulus for different regions

  int trial=gettrial(simparam);
#ifdef MATLABENG
    
  
   
  
  //*******************************************************
  //update all neurons in this range
  //********************************************************
  //parse the range of neurons to be processed by this node
  sneuronrng* rng=divideneurons(getrank(simparam->mpiinfo),getnumneurons(mparam),getnprocs(simparam->mpiinfo));


  neurons=mparam->neurons+rng->nstart;
  int numinrange=rng->num;
  rfields=malloc(sizeof(srfield*)*numinrange);

  #ifdef DEBUG
  char* msg=malloc(200);
  sprintf(msg,"rank %d: optimization numinrange=%d \n", getrank(simparam->mpiinfo),numinrange);
  fmsg(msg);
  free(msg);
  #endif
  //only call update if numinrange is>0
  //its possible its 0 if we have more processes then neurons
  if(numinrange>0){
  stims=optstim_neurons(neurons, numinrange,trial,simparam,mparam);
  }
    //**************************************************************************
    //compute full stimulus


    //**************************************************************************
    //All gather - construct full stimulus at root
    //*************************************************************************
    //do an all gather
    //each node sends rng->maxnum vectors
    //sum will be zero

    //allocate send buffers
    //this assumes all neurons have same length
    int klength=getneuronklength(mparam->neurons[0]);
  int root=0;
  int sendcount=rng->maxnum*klength;
  double *sendbuff=malloc(sizeof(double)*sendcount);

  double* recvbuff=NULL;
  if (getrank(simparam->mpiinfo)==0){
    recvbuff=malloc(sizeof(double)*sendcount*getnprocs(simparam->mpiinfo));
  }

  //now each node initializes its sendbuffer
  for (int nindex=0;nindex<rng->num;nindex++){
    memcpy(sendbuff+klength*nindex,stims[nindex]->data,sizeof(double)*klength);
  }

  //now do all gather
  #ifdef MPI
  //fmsg("optimization.c: Perform all gather to get all optimal stimuli \n");
  MPI_Gather(sendbuff,sendcount,MPI_DOUBLE,recvbuff,sendcount,MPI_DOUBLE,root,MPI_COMM_WORLD);
  #else
  //in nonmpi case just set recvbuff to sendbuff
  recvbuff=sendbuff;
  #endif

  //put all stimulus values into allstim
  double *allstim=malloc(sizeof(double)*getmparamstimlength(mparam));	
  if (getrank(simparam->mpiinfo)==0){
    //now root assembles the stimulus
    //loop over each node and add its stimuli to the fullsitmulus
   
    for (int pindex=0;pindex<getnprocs(simparam->mpiinfo);pindex++){
      sneuronrng* prng=divideneurons(pindex,getnumneurons(mparam),getnprocs(simparam->mpiinfo));
      
      //now loop over all neurons proceesed by this process
      for (int nindex=prng->nstart;nindex<(prng->nstart+prng->num);nindex++){
	//set ptr to point to location in recv buff of this neuron
	double* ptr=recvbuff+pindex*sendcount+(nindex-prng->nstart)*klength;      	
	//set stimptr to point to location in all ptr
	//corresponding to this receptive field
	double* stimptr=allstim+nindex*klength;
	memcpy(stimptr,ptr,sizeof(double)*klength);
      }
    }
  }
	

  //*********************************************************
  //broadcast distribute stimulus to all nodes now we do broadcast to send allstim to all nodes
  //**********************************************************
  #ifdef MPI
  MPI_Bcast(allstim,getmparamstimlength(mparam),MPI_DOUBLE,root,MPI_COMM_WORLD);
  #endif
  //allstim has data for the entire stimulus
  sMatrix* mallstim=array2vector(allstim,getmparamstimlength(mparam));
  fullstim=malloc(sizeof(fullstim));
  fullstim->stim=mallstim;
  fullstim->stiminfo=mparam->stiminfo;

  //#else
  //fullstim=createfullstim(mparam->stiminfo,rfields,stims,getnumneurons(mparam));
  //#endif //endif MPI


  if(isnullm(rfields,"rfields")==FALSE){
    //this shouldn't free the rfields
    //this should just free the array of rfield pointers
    free(rfields);
  }




#else
  emsgm("No code for fishermax \n");
  exit(FALSE);
#endif //end if for MATALABENG

  free(rng);
  return fullstim;
}


//******************************************************************************************
//   optimize stimuli for a number of neurons
// return an array matrix containing optimized stimulus for each neuron
sMatrix** optstim_neurons(sneuron** neurons, int numneurons,int trial,ssimparam* simparam,smparam* mparam){
  if(isnullm(neurons,"neurons")==TRUE){
    emsgm("terminating\n");
    exit(FALSE);
  }
  if (numneurons<1){
    emsgm("numneurons can't be <1 \n");
  }

  sMatrix** stims=malloc(sizeof(sMatrix*)*numneurons);

 //**************************************************
  //  set up matlab eng
  //************************************************
  //we will call MATLAB to do the update
  Engine *ep;
  ep=getmateng(simparam);


  //create an output buffer to store output of matlab commands
  char * mBuffer;
  int buffSize=1000;
  mBuffer=(char*) malloc(sizeof(char)*buffSize);
  engOutputBuffer(ep, mBuffer, buffSize); 

  for(int nindex=0;nindex<numneurons;nindex++){
  
    //rfields[nindex]=neurons[nindex]->rfield;
    //put relevant variables into matlab
    //get the last posterior
    sgauss*  post=getneuronpost(neurons[nindex], trial-1);
    mxArray* mxpost=createmxgauss(&post,1);
    mxArray* mxmmag=mxCreateDoubleScalar(getmmag(mparam));

    //put the variables into matlab
    //clear the workspace before doing anything
    engEvalString(ep, "clear all; setpaths;");
    //put the variables in the matlab workspace
    engPutVariable(ep, "post", mxpost); 
    matmsg(mBuffer);

#ifdef DEBUG
    engEvalString(ep, "post.m");
    engEvalString(ep, "post.c");
#endif
    engPutVariable(ep, "mmag", mxmmag);
    matmsg(mBuffer);

    engEvalString(ep, "[xopt]=fishermax(post,mmag);");

    /* #ifdef DEBUG
       fmsg("evaluate xopt in matlab \n");
       engEvalString(ep, "xopt");
       #endif
       matmsg(mBuffer);*/

  
    mxArray *mxstim;
    mxstim=engGetVariable(ep, "xopt");


    if (isnullm(mxstim,"mxstim")==TRUE){
      emsgm("Failed to execute fishermax in matlab \n Terminating");
      //to prevent it from terminating set stim to just be the mean
      #ifdef DEBUG
      printmatrix(post->m);
      #endif
      
      //exit(FALSE);
      
    }
    else{
    stims[nindex]=mxArrayTosMatrix(mxstim);
    }
    //cleanup
    freeGauss(post);
	       
    mxFree(mxpost);
    mxFree(mxmmag);
    mxFree(mxstim);

  }

  return stims;
}

