#include "probability.h"
#include "debug.h"
#include <stdlib.h>
#include "string.h"

//create an initial gaussian for somedimension
//var - variance in each dimension
sGauss* initgauss(int d, double var){
  if (d<1){
    emsg("initGauss: dimension needs to be at least 1 \n");
  }

  sMatrix* m =createVector(d);
  sMatrix* c=createMatrix2ds(d);

  //set diagonal elements to var
  for (int index=0;index<d;index++){
    setij(c,index,index,var);
  }

  sGauss* gauss=malloc(sizeof(sGauss));
  gauss->m=m;
  gauss->c=c;
  return gauss;
}

sGauss* copygauss(sgauss *source){
  if(isnullm(source,"source")==TRUE){
    return NULL;
  }

  sGauss* ngauss=malloc(sizeof(sgauss));

  ngauss->c=copymatrix(source->c);
  ngauss->m=copymatrix(source->m);

  return ngauss;
}
//this makex copies of m and c before initializing 
//the gaussian

sGauss* createGauss(sMatrix* m, sMatrix* c){
  sGauss* gauss=(sGauss*)malloc(sizeof(sGauss));

  //make sure m and c have same dimensions
  if (isvector(m)==FALSE){
    emsg("createGauss m is not a vector \n");
  }
  if (isrowvector(m)==TRUE){
    //take transpose
    m->dims[0]=m->dims[1];
    m->dims[1]=1;
  }

  //make sure c is square
  if(c->dims[0]!=c->dims[1]){
    emsg("createGauss c is not square \n");
  }
  if(c->dims[0]!=m->dims[0]){
    emsg("createGauss: meand and covariance matrix do not have appropriate dimensions");
  }

  //else copy the matrices
  gauss->m=copyMatrix(m);
  gauss->c=copyMatrix(c);
  return gauss;

}

void printgauss(sgauss* gauss){
  if (isnullm(gauss,"gauss")==TRUE){
    return;
  }
  char* msg=malloc(200);
  sprintf(msg,"gauss.m \n");
  fmsg(msg);
  printmatrix(gauss->m);

  sprintf(msg,"gauss.c \n");
  fmsg(msg);
  printmatrix(gauss->c);
  free(msg);
  
}

void freeGauss(sGauss* gauss){
	if (gauss==NULL){
	  //I often try to free a null gaussian because of how I distribute the
	  //posteriors across multiple nodes
	  //emsg("free gauss: attempted to free null pointer \n");	
		return;
}	
	freeMatrix(gauss->m);
	freeMatrix(gauss->c);
	
	free(gauss);
	
}


#ifdef MATLAB
//****************************************************************
//this function creates an MxStructure array to represent
//a full stim object so that we can write it to matlab
//don't forget to delete by call to mxDestroyStructArray
mxArray* createmxgauss(sgauss** post,int num){
  mxArray * mxgauss=NULL;
  int ndim=1;
  int dims[1];
  dims[0]=num;
  int nfields=2;
  char **fieldnames;

  char * msg=malloc(100);;
  int CBUFFSIZE=200;
  const char** field_names=malloc(sizeof(char*)*nfields);
  field_names[0]="m";
  field_names[1]="c";

  mxgauss=mxCreateStructArray(ndim,dims,nfields,field_names);

  //create an mxArray to represent the stimulus

  tmsgm;
  for (int gindex=0;gindex<num;gindex++){
    #ifdef DEBUG
    sprintf(msg,"create gauss index %d  num=%d \n",gindex,num

);
    fmsgm(msg);
    #endif
    if(isnullm(post[gindex],"postgindex")==TRUE){
    
      emsg("post[gindex] is null");
      exit(FALSE);
    }

   if(isnullm(post[gindex]->m,"postgindex->m")==TRUE){
      emsg("post[gindex]->m is null");
      exit(FALSE);
   }
 if(isnullm(post[gindex]->c,"postgindex->c")==TRUE){
      emsg("post[gindex]->c is null");
      exit(FALSE);
   }


    mxArray* m=smatrixtomxarray(post[gindex]->m);
    mxArray* c=smatrixtomxarray(post[gindex]->c);
    
    if(isnullm(m,"m")==FALSE){
    mxSetField(mxgauss, gindex,field_names[0], m);
    }
    if(isnullm(c,"c")==FALSE){
    mxSetField(mxgauss, gindex,field_names[1], c);
    }
    //mxFree(m);
    //mxFree(c);
   }
  tmsgm;
  free (msg);
  return mxgauss;
}



//    mxstimreg -a structure array of substimuli
//    sindex - which element of structure array we get
sgauss* sgaussfrommxarray(mxArray* mxgauss,int gindex){
 //variables we need to load from mxparam
  sgauss* gauss;
  sMatrix* m;
  sMatrix* c;

  //fieldnames which must be provided
  enum FNAMES {f_m, f_c};

  //create an array to keep track of whether field was provided
  int fexists[2];
  int NNEEDED=2;   //how many elements in FNAMES are there
  fexists[0]=FALSE;
  fexists[1]=FALSE;


  int isstruct=mxIsStruct(mxgauss);
  if (isstruct==0){
    emsg("sgaussfrommxarray: mxgauss is not a structure \n");
    return NULL;
  }

  int numfields=mxGetNumberOfFields(mxgauss);

  //stor name of field
  const char* fname;
  //********************************************************************
  //loop through all the fields in the structure and proces them
  int findex;
  for (findex=0;findex<numfields;findex++){
      fname=mxGetFieldNameByNumber(mxgauss, findex);

      if(strcmp(fname,"m")==0){
      fexists[f_m]=TRUE;
      mxArray* mxdata=mxGetField(mxgauss,gindex,fname);
      m=mxarraytosmatrix(mxdata);
      mxFree(mxdata);
    }
    
      
    if(strcmp(fname,"c")==0){
      fexists[f_c]=TRUE;
      mxArray* mxdata=mxGetField(mxgauss,gindex,fname);
      c=mxarraytosmatrix(mxdata);
      mxFree(mxdata);
      }

   
  }//end loop over all fields


  //check all fields were provided
  int isvalid=TRUE;

  //  fname=malloc(100);
  for (findex=0;findex<NNEEDED;findex++){
    
    switch (findex){
    case f_m:
      fname="m";
      break;
    case f_c:
      fname="c";
      break;
    }
    if (fexists[findex]!=TRUE){
      isvalid=FALSE;
      fprintf(stderr,"gaussfrommxstruct: error mising field %s \n", fname);
    }
  }


  if (isvalid==TRUE){
    gauss=creategauss(m,c);
      freeMatrix(m);
      freeMatrix(c);

    return gauss;
  }
  else{
    return NULL;
  }



}
#endif
