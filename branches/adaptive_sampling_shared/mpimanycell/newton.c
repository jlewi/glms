
#include "matrixops.h"
#include "matrixmath.h"
#include "math.h"
#include "simulation.h"
#include "mparam.h"
#include "neuron.h"
#include "newton.h"
#include "neuronstim.h"
#include "dividework.h"

#ifdef MATLAB
#include "mat.h"
#ifdef MATLABENG
#include "engine.h"
#endif
#endif
void newton_update_all(ssimparam* simparam, smparam* mparam, sFullStim* stim){
  //**MPI CODE to select neurons
  int nneurons;  //store number of neurons we need to process
  //increment the trial
  //siminctrial(simparam);
  //*****************************************************
  //Get a list of neurons we're updating
  //******************************************************
  //1. Non-mpi we get all neurons
  //2. Mpi we only update some neurons
  //we should get a constant reference to the neurons
  //object
  //
  #ifdef MPI
    //parse the range of neurons to be processed by this node
  sneuronrng* rng=divideneurons(getrank(simparam->mpiinfo),getnumneurons(mparam),getnprocs(simparam->mpiinfo));
  sneuron **neurons=mparam->neurons+rng->nstart;
  nneurons=rng->num;

  #else
  nneurons=getnumneurons(mparam);
  sneuron** neurons=mparam->neurons;  
  #endif
  //loop over the neurons we need to update
  for (int nindex=0;nindex<nneurons;nindex++){
    newton_update_neuron(neurons[nindex],stim,simparam,mparam);		
  }

	       
}

void newton_update_neuron(sneuron* neuron, sFullStim* fullstim,ssimParam* simparam, smparam* mparam){

  int trial=gettrial(simparam);
  //***********************************************
  //get the current posterior from the previous timestep which acts like prior
  sgauss* prior=getneuronpost(neuron,trial-1);

  //  ssubStim* rfield=neuron->rfield;   //get the receptive field
  //get the true k vector for just this neuron
  sMatrix* ktrue= getktrue(neuron);

  //get the stimulus for just this neuron
  sMatrix* stim=getneuronstim(neuron,fullstim);

  //simulate the response
  //compute mean firing rate of neuron

  double mrate=exp(dotprod(stim,ktrue));

  //sometimes the newton update can screw up leading to Nan
  //we don't want to cause problems with MKL so try setting 
  //mrate to zero
  if (mrate<0){
    mrate=0;
  }
  int nspikes=poissrnd(getpoissgen(simparam),mrate);

  //set the observations for the neuron
  setobsrv(neuron,trial,nspikes);

  #ifdef MATLABENG
  
  //**************************************************
  //  set up matlab eng
  //************************************************
    //we will call MATLAB to do the update
  Engine *ep;
  ep=getmateng(simparam);


  //create an output buffer to store output of matlab commands
  char * mBuffer;
  int buffSize=1000;
  mBuffer=(char*) malloc(sizeof(char)*buffSize);
  engOutputBuffer(ep, mBuffer, buffSize);  
   
  

  //**************************************************
  //compute new mean and covariance using matlab
  //*************************************************
  //***********************************************
  //put relevant variables into matlab
  //I could however make all my arrays mxArrays
  int klength=getneuronklength(neuron);             //get length of this neuron's receptive field

  mxArray* mxCovar = smatrixtomxarray(prior->c);
  mxArray* mxMean= smatrixtomxarray(prior->m);
  mxArray* mxStim=smatrixtomxarray(stim);
  mxArray* mxnspikes=mxCreateDoubleScalar(nspikes);
  mxArray* mxtwindow=mxCreateDoubleScalar(mparam->twindow);

  #ifdef DEBUG
  fmsg("newton.c: starting matlab calls to do update \n");
  #endif
  //flowmsg("Run matlab commands \n");
  //clear the workspace before doing anything
  engEvalString(ep, "clear all; setpaths;");
  //put the variables in the matlab workspace
  engPutVariable(ep, "c", mxCovar); 
  matmsg(mBuffer);

  engPutVariable(ep, "m", mxMean);
  matmsg(mBuffer);

  engPutVariable(ep, "stim", mxStim);
  matmsg(mBuffer);

  engPutVariable(ep, "nspikes", mxnspikes);
  matmsg(mBuffer);

   engPutVariable(ep, "twindow", mxtwindow);
  matmsg(mBuffer); 

  matmsg(mBuffer);
   
  //evaluate the update
  //evaluate fishermax
 
  engEvalString(ep, "prior=[];");
  engEvalString(ep, "prior.m=m;");
  engEvalString(ep, "prior.c=c;");

  
  engEvalString(ep, "[m,c]=newton1dupdate(prior, stim,nspikes,twindow);");
  matmsg(mBuffer);

  //flowmsg("Done evaluating matlab commands \n");


  //get the results from matlab
  mxArray *mxnmean;
  mxnmean=engGetVariable(ep, "m");
  matmsg(mBuffer);

  isnull(mxnmean,"mxnmean",__FILE__,__LINE__);
  if (mxnmean==NULL){
    errmsg("Could not get m from matlab \n");
  }

  mxArray *mxnc;


  mxnc=engGetVariable(ep, "c");
  matmsg(mBuffer);
  if (mxnc==NULL){
    errmsg("Could not get c from matlab \n");
  }

  //create new matrices
  sMatrix* newm=mxArrayTosMatrix(mxnmean);
  sMatrix* newc=mxArrayTosMatrix(mxnc);

  #ifdef DEBUG
  fmsg("new mean: \n");
  printmatrix(newm);
  #endif
  //create the new posterior
  sGauss* newpost=creategauss(newm,newc);


  //update the posterior
  setneuronpost(neuron, trial, newpost);


  //clean up
  //clean up mxarrays
  mxFree(mxCovar);
  mxFree(mxMean);
  mxFree(mxStim);
  mxFree(mxnspikes);
  mxFree(mxnmean);
  mxFree(mxnc);
  mxFree(mxtwindow);			      
  
  //free structures
  freeGauss(newpost);
  freeGauss(prior);

#endif
}


