// Purpose of this script is to test calling
// the intel Mkl Fortran svd function
// from C.

#include "datastruct.h"
#include "output.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "matrixops.h"
#include "matrixmath.h"

//#extern 
int main(){
  int ndims=2;
  printf("Starting testeigd.c \n");
 

  //create the matrix
  sMatrix* mat=createMatrix2ds(ndims);
  setij(mat,0,0,6);
  setij(mat,0,1,4);
  setij(mat,1,0,4);
  setij(mat,1,1,6);

  printf("Covariance matrix: \n");
  print_mat(mat);


  sMatrix evecs;
  sMatrix eigd;

 int info= eig(mat,&evecs,&eigd);


 
 if (info==-1){
   printf("Eigendecomposition error occured\n");
 }
 else{
   printf("Eigen vectors: \n");
   print_mat(&evecs);
  
   printf("Eigen values: \t");
   //print_mat(eigd);
   print_mat(&eigd);
 }

 //check eigendecomposition
 sMatrix* eigprod=matrixProd(mat,&evecs);
 sMatrix* eigmat=diag(&eigd);
 sMatrix* eigvecs=matrixProd(&evecs,eigmat);

 //check to make sure they are equal
 int row;
 int col;
 int err=0;
 for (row=0;row< mat->dims[0];row++){
   for (col=0;col <mat->dims[1];col++){
     err=abs(getij(eigprod,row,col)-getij(eigvecs,row,col))+err;
   }
 }

 if (err==0){
   printf("No problem with eigendecomposition or matrix multiplication \n");
 }
 else{
   printf("Problem with either eigenddecompositon or matrix multiplication \n");
 }

 //test the dot product
 sMatrix *vec1=createVector(3);
 setij(vec1,0,0,1);
 setij(vec1,1,0,2);
 setij(vec1,2,0,4);

 sMatrix *vec2=createVector(3);
 setij(vec2,0,0,4);
 setij(vec2,1,0,3);
 setij(vec2,2,0,2);

 double dp=dotprod(vec1,vec2);

 //compute the dot product
 double checkdp=0;
 for (int index=0;index<vec1->dims[0];index++){
   checkdp=getij(vec1,index,0)*getij(vec2,index,0)+checkdp;
 }
 
 if (checkdp==dp){
   printf("dotprod works!  \n");
 }
 else{
   printf("Dot product returned error! \n");
 }
}

