%Matlab script to create structures of simulation parameters 
%which can be loaded into the simulation

fname='simdata.mat';

mparam.ktrue=[.1;.2;.3]; %//needs to be columnvector
mparam.mmag=1;

%//stimulatin regions
mparam.stimregions(1).kstart=0;
mparam.stimregions(1).klength=1;
mparam.stimregions(1).neurons=[1 2];
mparam.stimregions(2).kstart=1;
mparam.stimregions(2).klength=1;
mparam.stimregions(2).neurons=[1];
mparam.stimregions(3).kstart=2;
mparam.stimregions(3).klength=1;
mparam.stimregions(3).neurons=[2];

save(fname);
