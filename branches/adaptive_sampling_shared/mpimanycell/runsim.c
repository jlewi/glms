#include "matlabinter.h"
#include "mparam.h"
#include <stdlib.h>
#include <stdio.h>
#include "newton.h"
#include "matlabinter.h"
#include "optimization.h"
#include "debug.h"
#include "mpicomm.h"
#include <string.h>

#ifdef MATLAB
#include "mat.h"
#endif

extern char* OUTFNAME;

char* basename(int nprocs);
int runsim(char** fname,int nfiles, int ntrials);
void mainloop(ssimparam* simparam, smparam* mparam);
void saveresults(char* outfile, char** fname,int findex, int nfiles, ssimparam** allsimparam, smparam** allmparam, int trial, int ntrials);
char* getoutfname();

#ifdef MATLABENG
Engine* startmatlab();
#endif
int main (int argc, char **argv){
#ifdef MPI
  MPI_Init( &argc, &argv );  
#endif
  //defaults
  int MAXFILES=100;
  int BLEN=100;
  int nfiles=0;
  int ntrials=1;
  char* flist=malloc(200);
  char** fnames=malloc(sizeof(char*)*MAXFILES);
  char* test=(char*)malloc(200); //name of test to run

  char* foutput=NULL;
  char* msg=malloc(BLEN);
  //default is matlab msgs are off
  MATLABMSG=FALSE;
  int fileprovided=FALSE;

  

  //***************************************************************
  //process the command line arguments
  //*************************************************************
  int argind=1;
  
  while(argind <argc ){
    if (strcmp(argv[argind],"-mat")==0){
      //we need to copy the filename
      //otherwise we get segmentation fault when we try to free pointer
      fnames[nfiles]=malloc(strlen(argv[argind+1])+1);
      memcpy(fnames[nfiles],argv[argind+1],strlen(argv[argind+1])+1);   
      nfiles++;
      fileprovided=TRUE;
    }
    else if (strcmp(argv[argind],"-test")==0){
      test=argv[argind+1];
    
    }
    else if (strcmp(argv[argind],"-matlabmsg")==0){
      MATLABMSG=TRUE;
      argind=argind-1; //b\c we add 2 below
    }
    else if (strcmp(argv[argind], "-trials")==0){
      ntrials=atoi(argv[argind+1]);
    }
    else if (strcmp(argv[argind], "-outfile")==0){
      foutput=argv[argind+1];
    }
    else if (strcmp(argv[argind], "-flist")==0){
      flist=argv[argind+1];
      fileprovided=TRUE;

      //open the file an read the filenames on each line
      FILE* fp=fopen(flist,"r");
      if (fp==NULL){
	sprintf(msg,"Could not read file %s to get list of mat files \n",flist);
	emsgm(msg);
	exit(FALSE);
      }
      //keep reading lines until we reach end of file or run out of files      
      while(nfiles<MAXFILES){
	fnames[nfiles]=malloc(BLEN);	
	fgets(fnames[nfiles],BLEN, fp);

	if ((fnames[nfiles]!=NULL) && (strlen(fnames[nfiles])>1)){
	  int len=strlen(fnames[nfiles]);

	  //remove any leading white space
	  while ((fnames[nfiles][0]==' ')){
	    fnames[nfiles]++;
	  }
	  
	  if (strlen(fnames[nfiles])>1){
	    //find the newline character if any and replace with `\0`
	    //also remove any trailing whitespace
	    for(int cind=0;cind<strlen(fnames[nfiles]);cind++){
	      if((fnames[nfiles][cind]=='\n') || (fnames[nfiles][cind]==' ')){
		fnames[nfiles][cind]='\0';
		break;
	      }
	    }
          } 
	  if (strlen(fnames[nfiles])>1){
	    nfiles++;
	  }
	  else{
	    free(fnames[nfiles]);

	  }
	}//   
	else{
	  //reached end of file or error occured
	  //terminate reading
	  break;
	}  //else goes with if null     
	
      }//end while loop
      fclose(fp);


    }
    argind=argind+2;  
  }

  //open the files for stdout and stderr
  openfps(foutput);
  //***********************************************************
  //call the appropriate test function
  //**********************************************************
  //was a file provided
  if (fileprovided==TRUE){
    runsim(fnames, nfiles,ntrials);

#ifdef DEBUG
    fmsg("Back in main()");
#endif
  }
  else{
    //check if default file exists
    //do this by trying to read it
    FILE* fp;
    int defexists=FALSE;
    fp=fopen("simdata.mat","r");
    if (fp !=NULL){
      defexists=TRUE;
      fclose(fp);
    }
	   
    if (defexists==TRUE){
      //runsim("simdata.mat");
      printf ("NO defaults");
      exit(FALSE);
    }
    else{
      printf("No file specified and default doesn't exist \n");
    }
  }
	  
	  
  //********************************************
  //cleanup
  for (int index=0;index<nfiles;index++){
    free(fnames[index]);
       
  }
  free(fnames);
	
 
  //close file pointers
  closefps();
  //close the debugging
  // closedebugfile();
#ifdef DEBUG
  fmsg("MPI finalize \n");
#endif
#ifdef MPI
 
  MPI_Finalize();
#endif
}

int runsim(char** files, int nfiles,int ntrials){

  //********************************************************
  //start matlab
  //*******************************************************
  Engine* matlabeng=startmatlab();

  //*********************************************************
  //get the name of the file to save it to
  //this uses MPI to sync the nodes
  char* foutname=getoutfname();
  //***********************************************************
  //sim setup
  smparam** allmparam=malloc(sizeof(smparam*)*nfiles*ntrials);
  ssimparam** allsimparam=malloc(sizeof(ssimparam*)*nfiles*ntrials);

  for (int index=0;index<nfiles*ntrials;index++){
    allsimparam[index]=NULL;
    allmparam[index]=NULL;
  }

  int allindex;
  char* fname=NULL; //name currently being processed

  char* msg=malloc(200);

  for(int findex=0;findex<nfiles;findex++){
    fname=files[findex];

    sprintf(msg,"Runsim: \t File =%s \t ntrials=%d \n \n \n", fname,ntrials);
    fmsg(msg);

    //*****************************************************
    //load the simulation
    //****************************************************
    smparam* mparam=NULL;
    ssimparam* simparam=NULL;
    //test opening and saving of a simulation file
    void** vars;
    int*  nvars=malloc(sizeof(int));
    int* err=malloc(sizeof(int));
    //set up MPI

  
    for (int trial=0;trial <ntrials;trial++){
      *err=FALSE;
      //store the index into allmparam and allsimparam
      allindex=nfiles*trial+findex;

      sprintf(msg,"Runsim: \t File =%s \t trial=%d \n \n \n", fname,trial);
      fmsg(msg);

      
      vars=readsimparam(fname, nvars, err);

      //if error occured while trying to read file skip trial
      if (vars==NULL){
	emsgm("Nothing read from file terminating \n");
	//return FALSE;
	*err=TRUE;
      }
      else{
	if (vars[0]==NULL){
	  emsgm("Was not able to read mparam from file. Terminating.");
	  //return FALSE;
	  *err=TRUE;
	}
	else {
	  if (vars[1]==NULL){
	    emsgm("was not able to read simparam from file. Terminating.");
	    //return FALSE;
	    *err=TRUE;
	  }
	}
      }

#ifdef DEBUG
      //printmparam(mparam);
#endif
    
     
  
      if (*err==TRUE){
	sprintf(msg,"Error: Could not load sim from  \t File =%s \t trial=%d \n", fname,trial);
	emsg(msg);
	emsg("Skipping trial \n \n");
      }
      else{
	mparam=(smparam*)vars[0];    
	allmparam[allindex]=mparam;
      
	simparam=(ssimparam*)vars[1];
	simparam->matlabeng=matlabeng;
	allsimparam[allindex]=simparam;

	//call the main loop
	mainloop(simparam,mparam);


   

	//*********************************************************
	//save results to 
	//we add 1 to files and trial
	//because saveresults wants to know the current sizes (i.e number of
	//elements which are not null
	//of the arrays allsimparam, allmparam
	saveresults(foutname, files, findex,nfiles, allsimparam, allmparam, trial, ntrials);


	//close and ropen the output file in order to flushit
	closefps();
	openfps(OUTFNAME);
      }
    }//end loop over tials
  }//end loop over files
  
  //*********************************************************
  //clean up
  //********************************************************
  for (int index=0;index<nfiles*ntrials;index++){
    freesimparam(allsimparam[index]);
    freemparam(allmparam[index]);
  }
  free(allmparam);
  free(allsimparam);
  free(msg);
  free(foutname);
#ifdef DEBUG
  fmsg("Finishing runsim() \n");
#endif
  return TRUE  ;
}


Engine* startmatlab(){

  smpiinfo* mpiinfo=creatempiinfo();
  Engine* matlabeng;
  //***************************************************************
  //initialize the matlabengine
  //we do this one at a time
  //root sends a broadcast message telling each node
  //when to start
#ifdef MATLABENG
  int sendbuff=1;
  int sendcount=1; 
  
  char* smatmsg=malloc(200);
  //do loop over broadcast
  for (int rindex=0;rindex<getnprocs(mpiinfo);rindex++){
    if (getrank(mpiinfo)==rindex){
      //root starts its matlab engine
      sprintf(smatmsg,"rank %d: starting matlab engine \n", rindex);
      fmsg(smatmsg);

      if (!(matlabeng = engOpen("matlab -nosplash -nojvm -nodesktop"))) {
	emsgm("\nCan't start MATLAB engine\n");
	//ISVALID=FALSE;	 
      } 
      sprintf(smatmsg,"rank %d: done starting matlab engine \n", rindex);
      fmsg(smatmsg);
    }
    //now do broadcast message to let all nodes no rindex finished startingmatlab
#ifdef MPI
    MPI_Bcast(&sendbuff, sendcount, MPI_INT,rindex, MPI_COMM_WORLD);
#endif
  }
  free(smatmsg);
#endif
  fmsg("Done starting matlb!\n");

  freempiinfo(mpiinfo);
  return matlabeng;
}

void mainloop(ssimparam* simparam, smparam* mparam){
  sFullStim* stim=NULL;
  char * msg=malloc(200);

  //trial indexing  effectively starts at 1
  simparam->trial=0;
  
  double starttime=recordtime(simparam->mpiinfo,"mainloopstart");

  for (int trial=1;trial<=simparam->niter;trial++){
    //increment the trial index
    simparam->trial=trial;

 

    //***************************************************
    //generate the stimulus
    //***************************************************
    if (simparam->randstim==TRUE){
      sMatrix* rndnums=NULL;
      stim=malloc(sizeof(sFullStim));
      stim->stiminfo=mparam->stiminfo;
      //root node only generates randomnumbers
      //generate random stimuli
      if(getrank(simparam->mpiinfo)==0){ 
	sprintf(msg,"Trial %d: generate Random stimulus \n",trial);
	fmsg(msg);
	rndnums=normrnd(simparam->normgen,getmparamstimlength(mparam), getmmag(mparam));
     
     
	
	//avoid copying memory in order to try to speed it up.
	//stim=fullstimfromvec( mparam,rndnums);
	//freeMatrix(rndnums);
    
	stim->stim=rndnums;
      }
      else{


	stim->stim=NULL;
      }
      

      //broadcast stimulus to all nodes if necessary
#ifdef MPI
      sMatrix* stimvals=NULL;

      stimvals=bcaststim(simparam,mparam, stim->stim);
      stim->stim=stimvals;
#endif

    }
    else{      
         if (getrank(simparam->mpiinfo)==0){
      //output message about trial
      sprintf(msg,"Trial %d: get optimal stimulus \n",trial);
      fmsg(msg);
    }
      stim=optstim(simparam,mparam);
      
    }


    
    //****************************************************
    //update
    if (getrank(simparam->mpiinfo)==0){
      //output message about trial
      sprintf(msg,"Trial %d: update posterior \n",trial);
      fmsg(msg);
    }
    newton_update_all(simparam, mparam, stim);

  }
  double endtime=recordtime(simparam->mpiinfo,"mainloopend");
  sprintf(msg,"Rank %0.2d RT %0.9g   NN %0.2d  NP %0.2d \n", getrank(simparam->mpiinfo),endtime-starttime, getnumneurons(mparam), getnprocs(simparam->mpiinfo));
  fmsg(msg);
  //fmsg("Completed all trials \n");


  free(msg);
  

  

#ifdef DEBUG
  fmsg("Finishing mainloop() \n");
#endif
}


//*********************************************************************************
//this function uses MPI to ensure all processes know which node to write
//to 
char* getoutfname(){
  int FLEN=100;
  char* fout=malloc(FLEN);
  char* msg=malloc(100);

  smpiinfo* mpiinfo=creatempiinfo();
  //root node will bcast the filename to all nodes
  if (getrank(mpiinfo)==0){
    char* fbase=basename(mpiinfo->nprocs);  
    sprintf(fout,"%s",fbase);

    //open the file in order to create it
    fmsg("Creating file: \n");
    fmsg(fout);
    MATFile* mfp=matOpen(fout,"w");
    matClose(mfp);
    free(fbase);
  }

  //broad cast the filename
#ifdef DEBUG
  fmsg("broadcasting the filename \n");
#endif
#ifdef MPI
  MPI_Bcast(fout, FLEN, MPI_CHAR,0, MPI_COMM_WORLD);
#endif
#ifdef DEBUG
  sprintf(msg,"rank %d: recieved filename= %s \n",getrank(mpiinfo),fout);
  fmsg(msg);
#endif
  //  sprintf(fout,"%s_P%0.2d.mat",fbase,getrank(simparam->mpiinfo));
  free(msg);
  freempiinfo(mpiinfo);
  return fout;
}

//nfiles and ntrials are the max number of files and trials 
//we need these to properly compute the index indexes into the array respectively

void saveresults(char* fout, char** fname, int flast, int nfiles, ssimparam** allsimparam, smparam** allmparam, int triallast, int ntrials){
  smpiinfo* mpiinfo=creatempiinfo(); //create this because sometimes simparam will be null

  ssimparam* simparam;
  smparam* mparam;
  //create a structure called trials
  //each trial contains the neurons and mpiinfo
  int nfields=8;
  const char** tfield_names=malloc(sizeof(char*)*nfields);
  tfield_names[0]="neurons";
  tfield_names[1]="mpiinfo";
  tfield_names[2]="mmag";
  tfield_names[3]="nprocs";
  tfield_names[4]="numneurons";
  tfield_names[5]="file";
  tfield_names[6]="klength";
  tfield_names[7]="niter";

  
  char* wfmsg=malloc(200);
  mxArray* mxtrials=mxCreateStructMatrix(nfiles,ntrials,nfields,tfield_names);

  for (int findex=0;findex<=flast;findex++){
    //the index of the last trial depends on the file
    //b\c we run all trials at once
    int lasttrial;
    if (findex<flast){
      lasttrial=ntrials-1;
    }
    else{
      lasttrial=triallast;
    }
    for (int trial=0;trial<=lasttrial;trial++){
      //set the field of trials appropriately
      //the index of the element in trials to set
      //matlab is column major
      //printf("findex=%d trial=%d \n",findex,trial);
      int trialindex=nfiles*trial+findex;
      simparam=allsimparam[trialindex];
      mparam=allmparam[trialindex];
      //output the neurons

      //only save it if simparam and mparam are not null
      //could be null if there was error loading this trial
      if ((simparam!=NULL) && (mparam !=NULL)){
#ifdef DEBUG
	fmsg("Creating mxneurons \n");
#endif
	mxArray* mxneurons=mxstructfromneurons(mparam->neurons,mparam->numneurons);
#ifdef DEBUG
	fmsg("Creating mxmpiinfo \n");
#endif

	mxArray* mxmpiinfo=createmxmpiinfo(simparam->mpiinfo);

 
	mxSetField(mxtrials,trialindex, tfield_names[0],mxneurons);
	mxSetField(mxtrials,trialindex, tfield_names[1],mxmpiinfo);
  

	mxArray* mxmag=mxCreateDoubleScalar(getmmag(mparam));
	mxArray* mxnprocs=mxCreateDoubleScalar(getnprocs(simparam->mpiinfo));
	mxArray* mxnumneurons=mxCreateDoubleScalar(getnumneurons(mparam));
	mxArray* mxfile=mxCreateString(fname[findex]);
	mxArray* mxklength=mxCreateDoubleScalar(mparam->neurons[0]->ktrue->dims[0]);
	mxArray* mxniter=mxCreateDoubleScalar(simparam->niter);

	mxSetField(mxtrials,trialindex, tfield_names[2],mxmag);
	mxSetField(mxtrials,trialindex, tfield_names[3],mxnprocs);
	mxSetField(mxtrials,trialindex, tfield_names[4],mxnumneurons);
	mxSetField(mxtrials,trialindex, tfield_names[5],mxfile);
	mxSetField(mxtrials,trialindex, tfield_names[6],mxklength);
	mxSetField(mxtrials,trialindex, tfield_names[7],mxniter);
      }
      else{
	sprintf(wfmsg,"No data saved for file=%s \n trial=%d \n",fname[findex],trial);
	fmsg(wfmsg);
      }
    
  }//endloop over trialindex
  }//end loop over fileindex
  
    int nvars=1;;
    //if (getrank(simparam->mpiinfo)==0){
    //  nvars=nvars+3;
    //}

    int FLEN=300;
    smxsave* saveneurons=malloc(sizeof(smxsave)*nvars);

    saveneurons->data=mxtrials;
    (saveneurons)->name=malloc(FLEN);
    sprintf(saveneurons->name,"trials%0.2d",getrank(mpiinfo));

    //saveneurons->data=mxneurons;
    //saveneurons->name=malloc(FLEN);
    //sprintf(saveneurons->name,"neurons%0.2d",getrank(simparam->mpiinfo));

    //(saveneurons+1)->data=mxmpiinfo;
    //(saveneurons+1)->name=malloc(FLEN);
    //sprintf((saveneurons+1)->name,"mpiinfo%0.2d",getrank(simparam->mpiinfo));

    //following variables only written by root
    //this is the number of variables which are written by all nodes not just root
    /* int offset=1;
       if (getrank(simparam->mpiinfo)==0){
       mxmag=mxCreateDoubleScalar(getmmag(mparam));
       (saveneurons+offset)->data=mxmag;
       (saveneurons+offset)->name="mmag";

       mxnprocs=mxCreateDoubleScalar(getnprocs(simparam->mpiinfo));
       (saveneurons+offset+1)->data=mxnprocs;
       (saveneurons+offset+1)->name="nprocs";
    
       mxnumneurons=mxCreateDoubleScalar(getnumneurons(mparam));
       (saveneurons+offset+2)->data=mxnumneurons;
       (saveneurons+offset+2)->name="numneurons";
       }*/

   

    //*******************************************************************
    //take turns writing the matlab file
    int sendbuff=1;
    int sendcount=1; 
  
    if (getrank(mpiinfo)==0){
      fmsg("\n");
    }
    //do loop over broadcast
    for (int rindex=0;rindex<getnprocs(mpiinfo);rindex++){
      if (getrank(mpiinfo)==rindex){

	sprintf(wfmsg,"rank %d: writing to file %s \n", rindex, fout);
	fmsg(wfmsg);

	writematfile(fout,saveneurons,nvars);
	sprintf(wfmsg,"rank %d: done starting matlab engine \n", rindex);
	fmsg(wfmsg);
      }
      //now do broadcast message to let all nodes no rindex finished startingmatlab
#ifdef MPI
      MPI_Bcast(&sendbuff, sendcount, MPI_INT,rindex, MPI_COMM_WORLD);
#endif
    }
    return;
    free(wfmsg);



    //**************************************************************

#ifdef DEBUG
    //  mxArray*
    //double* ptr=mxGet
#endif   
   
    //free the output fariables
#ifdef DEBUG
    fmsg("mainloop() cleanup variables \n");
#endif
    mxFree(mxtrials);
    //mxFree(mxmpiinfo);

  

#ifdef DEBUG
    fmsg("mainloop() cleanup non mxVariables \n");
#endif
    free(saveneurons->name);
  
    freempiinfo(mpiinfo);
#ifdef DEBUG
    fmsg("freed saveneurons->name \n");
#endif
    free(saveneurons);
#ifdef DEBUG
    fmsg("freed saveneurons \n");
#endif

  
}



















//****************************************************
//return a free number for file
char* basename(int nprocs){
  int fexists=TRUE;
  int filenum=1;
  char* fname=malloc(200);
  char* basef="simout";
  while (fexists==TRUE){
    //sprintf(fname,"./data/%s_%0.3d_P00.mat",basef,filenum);
    sprintf(fname,"./data/%s_P%0.2d_%0.3d.mat",basef,nprocs,filenum);
    //check if default file exists
    //do this by trying to read it
    FILE* fp;
    fp=fopen(fname,"r");
    if (fp ==NULL){
      fexists=FALSE;
      //fclose(fp);
    }
    else{
       
      fclose(fp);
      filenum++;
    }
  }

  //sprintf(fname,"./data/%s_%0.3d",basef,filenum);
  return fname;
}
