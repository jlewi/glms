#include "matrixops.h"
#ifndef MATRIXMATH_INC
  #define MATRIXMATH_INC
  double dotprod(sMatrix* vec1, sMatrix* vec2);
int eig(const sMatrix* data,  sMatrix* evecs, sMatrix* eigd);

  sMatrix* matrixProd(sMatrix* mat1, sMatrix* mat2);

   int sign(double n);
   
//matrixEqual(sMatrix* mat1, sMatrix* mat2)
//
int matrixEqual(sMatrix* mat1, sMatrix* mat2);

//matrixDiff(sMatrix* mat1, sMatrix* mat2)
//
//Compute difference between two matrices
sMatrix* matrixDiff(sMatrix* mat1, sMatrix* mat2);

sMatrix* transpose(sMatrix* mat);
#endif
