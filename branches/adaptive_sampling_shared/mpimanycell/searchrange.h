#ifndef SEARCHRANGE_INC

extern const int NSTEPS;   //default number of steps to devide interval into
  #define SEARCHRANGE_INC
//declare a structure which parameterizes the 
//1-d search we will conduct
typedef struct st_SearchRange{
  double min;  ///minimum value
  double max;  //maximum value
  double dw;   //step size
} sSearchRng, ssearchrng;



sSearchRng* createsearchrng(double min, double max, double dw);

//*****************************************************************************
//Parse a range of values
sSearchRng* parserng(double min, double max, int myrank, int nnodes);


void setinterval(sSearchRng* rng, int nsteps);

  void freesearchrng(sSearchRng* rng);
#endif
