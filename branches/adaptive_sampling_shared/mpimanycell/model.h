
//I think the ndef will prevent circular inclusion
#ifndef MODEL_INC
  #define MODEL_INC
  
#include "matrixops.h"
#include "datastruct.h"
#include "probability.h"
#include "substim.h"
#ifndef STIMINFO_INC
#include "stiminfo.h"
#endif
#ifndef FULLSTIM_INC
#include "fullstim.h"
#endif
#endif
