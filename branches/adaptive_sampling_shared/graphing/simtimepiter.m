%7-31-2007
%   use new object oriented model and new data.
%   plot the timing on each iteration


DAYDIR='070813';
niter=1000;
dfile=[];
diary('~/tmp/runsimtiming.txt');

dims=[100];

for dind=1:length(dims)

    sims(dind).dim=dims(dind);
    sims(dind).fname=fullfile(RESULTSDIR,'poissexp',DAYDIR,sprintf('simtime_%dd_%s_001.mat',sims(dind).dim,DAYDIR));
    sims(dind).niter=niter;
    sims(dind).simvar=sprintf('max%d',sims(dind).dim);
    if ~exist(sims(dind).fname,'file')
        error('file %s doesnot exist will not be able to do sim',sims(dind).fname);

    else
        %check simulation variable exists
        if ~(varinfile(sims(dind).fname,sims(dind).simvar))
            warning('variable %s does not exist in file %s', sims(dind).simvar, sims(dind).fname);
        end
    end
end



%%
%start trial
%specfies how many trials at the beginning to throw out
%clear tstart
%tstart=1;
%which dimensionality to use for fitting the data
dstart=1000;


ftiter=[];
for sind=1:length(sims)
    %plot timing on each iteration

    ftiter(sind).hf=figure;
    ftiter(sind).xlabel='iteration';
    ftiter(sind).ylabel='time(seconds)';
    ftiter(sind).name='Time per iter';
    ftiter(sind).title=sprintf('Dimensionality %d',sims(sind).dim);
    ftiter(sind).semilog=1;
    ftiter(sind).linewidth=2;
    sim=SimulationBase('fname',sims(sind).fname,'simvar',sims(sind).simvar);

    %%

    %plot the optimization time
    sr=sim.sr(:);
    stiminfo=[sr.stiminfo];

    searchtime=[stiminfo.searchtime];
    post=sim.post(:);
    uinfo=[post.uinfo]; %automatically ecludes prior

    pind=0;
    hold on;

    totalt=[searchtime.fishermax]+[searchtime.tquadmod]+[uinfo.ntime]+[uinfo.eigtime];
    pind=pind+1;
    ftiter(sind).hp(pind)=plot(totalt,getptype(pind));
    ftiter(sind).lbls{pind}='total';

    pind=pind+1;;
    ftiter(sind).hp(pind)=plot([searchtime.fishermax],getptype(pind));
    ftiter(sind).lbls{pind}='fishermax';

    pind=pind+1
    ftiter(sind).hp(pind)=plot([searchtime.tquadmod],getptype(pind));
    ftiter(sind).lbls{pind}='tquadmod';

    pind=pind+1;
    ftiter(sind).hp(pind)=plot([uinfo.ntime],getptype(pind));
    ftiter(sind).lbls{pind}='update';

    pind=pind+1;
    ftiter(sind).hp(pind)=plot([uinfo.eigtime],getptype(pind));
    ftiter(sind).lbls{pind}='eigtime.';


    lblgraph(ftiter(sind));

end

odata={ftiter,'script: simtimepiter'};
onenotetable(odata,seqfname(fullfile('~/svn_trunk/notes/',sprintf('tpiter_%s.xml',datestr(clock,'yymmdd')))));
return;

