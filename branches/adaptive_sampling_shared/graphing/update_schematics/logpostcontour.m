%11-28
%   modiefied it to run as script or function
%8-29-2006
% 
% This script is part of the scripts used to make the figures used to
% illustrate the newtonian update rule
%
% Usage:
%   1. Run the script ll1d first
%   2. run dgaussian first?
%           addpath('/home/jlewi/cvs_ece/research/figures/scripts');
%           should be in cvs_ece/research/figures/scripts
% Other Relevant scripts
%   1. dgaussian and dgaussiansurf - this will be in my scripts folder for
%   drawing diagrams
%           they can be used to make images of gaussians
function [fg,zpost,thetax,thetay, pts]=logpostcontour(varargin)

%**************************************************888
%default values for when called as script
%*********************************************
lpthreshold=-68;
lprior=[];
zll=[];
nclines=20;
%loop through the parameters and set any values
vind=1;
while (vind<nargin)
    eval(sprintf('%s=varargin{vind+1};',varargin{vind}));
    vind=vind+2;
end
%5-14-set log prior if not already set
if isempty(lprior)
    %[fg,lprior,thetax,thetay]=dgaussian('mu',[0; 0],'sigma',eye(2))
    %[fg,lprior,thetax,thetay]=dgaussian('mu',[0; 0],'sigma',eye(2))
    [fg,lprior,thetax,thetay]=dgaussian;
end
if isempty(zll)
    [fg,zll]=ll1d;
end

%lprior =log of prior
zpost=lprior+zll;
fg.hf=figure;
fg.axisfontsize=30;
%1. we want to truncate the values of zpost so that we don't end up
%    scaling the colors to the minimum of zpost which goes expoenentially
%    to infinity
%zpost(zneg)=-inf;
%l=find(isfinite(zpost));
%zpost(zneg)=min(zpost(l));
%2. Alternatively set those points to nan so they aren't plotted.
zneg=find(zpost<lpthreshold);
zpost(zneg)=nan;
contourf(thetax,thetay,zpost,nclines);
%axis square
%axis tight
axis equal
set(gca,'FontSize',fg.axisfontsize);
xlabel('\theta_1');
ylabel('\theta_2');
hz=zlabel('$P(r_t\,\,|\vec{x}_t,\vec{\theta{}}$)');
set(hz,'Rotation',0);
set(hz,'interpreter','latex');
set(gca,'xtick',[]);
set(gca,'ytick',[]);
set(gca,'ztick',[]);



hold on

ho=plot(0,0,'.');
set(ho,'MarkerSize',25);

[v, ind]=max(zpost(:));

[row,col]=ind2sub(size(zpost),ind);
%col=floor(ind/length(thetax));
%row=ind-col*length(thetax);
%pts=[thetax(row) thetay(col)];
pts=[thetax(col) thetay(row)];
hn=plot(thetax(col),thetay(row),'.');
set(hn,'MarkerSize',25);
fg.ha=gca;
xlim([-2 2])
ylim([-2 2]);
