%11-28-
%
%Script intended to draw schematics of all three steps of updating the
%posterior
%   1. the prior
%   2. the likelihood
%   3. the true posterior
%   4. gaussian approximation of the posterior

%**************************************************************************
%************************************************************************
%graphing parameters
%***********************************************************************
nclines=20;         %number of nullclines to use for plotting

%threshold for plotting
%points with a log likeilhood smaller then this value are not plotted
%this prevents the color scheme from looking flat
%lpthreshold=log(1/(2*pi))-
lpthreshold=-68;

%sampling at .05 is much faster but it leaves ragged edges
thetax=[-3:.01:3];
thetay=[-3:.01:3];

%**************************************************************************
%parameters for the likelihood, prior, and posterior
prior.m=[0;0];
prior.c=.05*[1 0;0 1];
stim=[2.3;2.3];
dt=1;
obsrv.n=10;
obsrv.twindow=1;
%specify what glm to use
glm.fglmnc=@glm1dexp;  %normalizing function for the distribution. 
glm.fglmetamu=@etamupoiss;   %function to convert mean into canonical parameter depends on the distribution
glm.fglmmu=@glm1dexp;          %link funciton - funciton to generate the mean
glm.sampdist=@poissrnd;
%**************************************************************************
%draw the prior
%**************************************************************************
[fprior,lprior]=dgaussian('mu',prior.m,'sigma',prior.c,'nclines',nclines,'x',thetax,'y',thetay,'lpthreshold',lpthreshold);


%**************************************************************************
%draw the likelihood
%**************************************************************************
[fll,logll]=ll1d('nclines',nclines,'thetax',thetax,'thetay',thetay,'lpthreshold',lpthreshold);


%**************************************************************************
%draw the true posterior
%**************************************************************************
%zll=log likelihood
%lprior=logprior
[fpost]=logpostcontour('nclines',nclines,'thetax',thetax,'thetay',thetay,'lpthreshold',lpthreshold,'lprior',lprior, 'zll',logll);

%*************************************************************************
%draw the gaussian aproximation
[fapost]=approxpost('stim',stim,'obsrv',obsrv,'prior',prior,'nclines',nclines,'thetax',thetax,'thetay',thetay,'lpthreshold',lpthreshold);


%*********************
%addjust graph properties
fgraphs={fprior,fll,fpost,fapost};
for ind=1:length(fgraphs)
    fg=fgraphs{ind};
    axes(fg.ha);
  %  xlim([thetax(1),thetax(end)]);
%    ylim([thetay(1),thetay(end)]);
    axis equal tight;    
    
    %to get rid of the ragged edges around the edges of the plots which is
    %due to truncating the values 
end
%for the log likelihood set the xlimits to the edges of the plot
axes(fll.ha)
xlim([thetax(1),thetax(end)]);
