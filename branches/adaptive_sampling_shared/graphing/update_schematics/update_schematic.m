%make a picture of the update schematic
%we will need to create a bunch of axes. 
%   bh - this will be a set of axes equal to the size of the image
%       this will allow us to place text objects anywhere in the image
%
%
% Notes:
%       axes position is specified in relative coordinates (i.e [0 to 1])
%       text objects are specified in data units of the corresponding axis
%               I scale the units of the background axes to be 0 to 1 so
%               that I don't have to keep scaling
%
%Revision
%   11-05-07 - changed the labels. get rid of panel showing gaussian approx
%   for batch. 
%           - changes for new object model
clear all;
setpathvars
gspath='/home/jlewi/svn_trunk/research/figures/scripts';
addpath(gspath);


%graph parameters
gparam.width=6.1;       %inches
gparam.height=3;        %inches
gparam.nclines=20;      %number of null clines for the graph




%**************************************************************************
%image data
%create an update object to compute the Gaussian approximation
opts=optimset('TolFun',10^-10,'TolX',10^-10);
uobj=Newton1d('optim',opts);
glm=GLMModel('poisson','canon');
prior=GaussPost('m',[0;0],'c',2*eye(2));

mobj=MParamObj('klength',2,'alength',0,'pinit',prior,'mmag',1,'glm',glm);


stim=[1 ;1];
obsrv=exp(stim'*[.4;.4]);
[post]=update(uobj,prior,stim,mobj,obsrv);

mintheta=2.5;
thetax=[-mintheta:.05:mintheta];
thetay=[-mintheta:.05:mintheta];

[f data.loglike]=ll1d('nspikes',obsrv,'stim',stim','thetax',thetax,'thetay',thetay,'lpthreshold',-20);
set(f.hf,'name','Likelihood');
[f data.logprior]=dgaussian('mu',getm(prior),'sigma',getc(prior),'x',thetax,'y',thetay);
set(f.hf,'name','Prior');
[fpost data.logpost data.lpostx data.lposty data.means]=logpostcontour('lprior',data.logprior,'zll',data.loglike,'thetax',thetax,'thetay',thetay,'lpthreshold',-10);
set(fpost.hf,'name','Posterior');
[fpgauss data.logpgauss]=dgaussian('mu',getm(post),'sigma',getc(post),'lpthreshold',-5,'x',thetax,'y',thetay); %gaussian approximation of the posterior
set(fpgauss.hf,'name','Posterior Gaussian');
%zero out the corners
threshval=max(data.logprior(:,1));
ind=find(data.logprior<threshval);
data.logprior(ind)=nan;

threshval=max(data.logpgauss(:,1));
ind=find(data.logpgauss<threshval);
data.logpgauss(ind)=nan;

%*********************************************************
%Graphs
%%
fupdate.hf=figure;
fupdate.fontsize=12;
setfsize(fupdate.hf,gparam);


%create the background axes and set it to occupy the entire figure
%using relative coordinates
ba.ha=axes('position',[0, 0, 1,1]);
axis off
%scale the axis so its units are relative 
%this way the units are the same as for the axes
xlim([0 1]);
ylim([0 1]);
figure(fupdate.hf);

%change the color map so the peak of the gaussian doesn't completely
%blend in with the background
%do this by removing white from the colormap

%***************************
%uncomment for black and white
cmap=colormap(gray);
cmap=cmap(1:end-5,:);
colormap(cmap);

%**********************************************************************
%toprow has 5 images
%   1. log prior
%   2. log likelihood
%   3. log likelihood
%   4. log posterior
%   5. gaussian approximation of log posterior
%
%indexes for the axes

tr.lprior=1;
tr.ll=2;
tr.llt=3;
tr.lpost=4;
tr.lpgauss=5;

br.lprior=6;
br.ll=7;
br.lpost=8;
br.lpgauss=9;

%1.log prior
fupdate.a{tr.lprior}.ha=axes;
[c fupdate.a{tr.lprior}.hc]=contourf(data.logprior,gparam.nclines);
fupdate.a{tr.lprior}.title='$\log p(\vec{\theta})$';
fupdate.a{tr.lprior}.ylabel='batch:';

%2.log likelihod for all but most recent terms
fupdate.a{tr.ll}.ha=axes;
[c fupdate.a{tr.ll}.hc]=contourf(data.loglike,gparam.nclines);
fupdate.a{tr.ll}.title='$\log p(r_i|\vec{s}_i,\vec{\theta})$';

%3. log likelihood for most recent term
fupdate.a{tr.llt}.ha=axes;
[c fupdate.a{tr.llt}.hc]=contourf(data.loglike,gparam.nclines);
fupdate.a{tr.llt}.title='$\log p(r_t|\vec{s}_t,\vec{\theta})$';

%4. log posterior
fupdate.a{tr.lpost}.ha=axes;
[c fupdate.a{tr.lpost}.hc]=contourf(data.lpostx,data.lposty,data.logpost,gparam.nclines);
fupdate.a{tr.lpost}.title='$\log p(\vec{\theta}|\vec{\underline{s}}_t,\vec{\underline{r}}_t)$';

%add dots to show the means
hold on
mprior=getm(prior);
mpost=getm(post);
ho=plot([mprior(1);mpost(1)],[mprior(2);mpost(2)],'ko');
set(ho,'MarkerFaceColor','k');
set(ho,'MarkerSize',5);

%5. Gaussian approximation of the posterior
fupdate.a{tr.lpgauss}.ha=axes;
[c fupdate.a{tr.lpgauss}.hc]=contourf(data.logpgauss,gparam.nclines);
fupdate.a{tr.lpgauss}.title='$\log p(\vec{\theta}|\vec{\mu}_t,C_t)$';


%**************************************************************************
%bottom row
%**************************************************************************
%1. Gaussian approximation of the posterior
fupdate.a{br.lprior}.ha=axes;
[c fupdate.a{br.lprior}.hc]=contourf(data.logprior,gparam.nclines);
fupdate.a{br.lprior}.title='$\log p(\vec{\theta}|\vec{\mu}_{t\frac{\,\,}{\,\,}1},C_{t\frac{\,\,}{\,\,}1})$'; 
fupdate.a{br.lprior}.ylabel='online:';

%2. log likelihood for most recent term
fupdate.a{br.ll}.ha=axes;
[c fupdate.a{br.ll}.hc]=contourf(data.loglike,gparam.nclines);
fupdate.a{br.ll}.title='$\log p(r_t|\vec{s}_t,\vec{\theta})$';

%3. log posterior
fupdate.a{br.lpost}.ha=axes;
[c fupdate.a{br.lpost}.hc]=contourf(thetax,thetay,data.logpost,gparam.nclines);
fupdate.a{br.lpost}.title='$\log p(\vec{\theta}|\vec{\underline{s}}_t,\vec{\underline{r}}_t)$';
%add dots to show the means
hold on
ho=plot([mprior(1);mpost(1)],[mprior(2);mpost(2)],'ko');
set(ho,'MarkerFaceColor','k');
set(ho,'MarkerSize',5);

%4. Gaussian approximation of the posterior
fupdate.a{br.lpgauss}.ha=axes;
[c fupdate.a{br.lpgauss}.hc]=contourf(data.logpgauss,gparam.nclines);
fupdate.a{br.lpgauss}.title='$\log p(\vec{\theta}|\vec{\mu}_t,C_t)$';

%turn off tick marks
for j=1:9
    if ~isempty(fupdate.a{j})
    axes(fupdate.a{j}.ha)
    axis square;
set(fupdate.a{j}.ha,'xtick',[]);
set(fupdate.a{j}.ha,'ytick',[]);

%get rid of the edge colors
set(fupdate.a{j}.hc,'linecolor','none');
    end

end

%********************************************************************
%text objects for the top row
%*********************************************************
%place them in middle of figure for now
axes(ba.ha);
trowtxt(1).h=text(1/2,.5,'$+\Sigma_i$','interpreter','latex');
trowtxt(2).h=text(1/2,.5,'$+$','interpreter','latex');
trowtxt(3).h=text(1/2,1/2,'$=$','interpreter','latex');
trowtxt(4).h=text(1/2,1/2,'$\approx$','interpreter','latex');

%********************************************************************
%text objects for the bottom row
%*********************************************************
%place them in middle of figure for now
axes(ba.ha);
browtxt(1).h=text(1/2,.5,'$+$','interpreter','latex');
browtxt(2).h=text(1/2,.5,'$\approx$','interpreter','latex');
browtxt(3).h=text(1/2,1/2,'$\approx$','interpreter','latex');


%*******************************************************
%label the graph so font sizes get set
fupdate=lblgraph(fupdate);

%set the interpreters to latex
alltext=findall(fupdate.hf,'type','text');
set(alltext,'interpreter','latex');
	
%******************************************************************
%how much horizontal space do we need for the txt object
txtext=get([trowtxt.h],'extent');
txtext=cell2mat(txtext);
twidth=sum(txtext(:,3));

%determine the width for the graphs
%relative widths
ssize.width=(1-twidth)/5;
ssize.vspace=.05;           %space between rows
%**************************************
%determine the height for the graphs
%handles to the figures
trha=[];
brha=[];
for j=1:5
    trha=[trha fupdate.a{j}.ha];
end
for j=6:9    
    brha=[brha fupdate.a{j}.ha];
end

%determine how much height of graphs
trtinset=get(trha,'tightinset');
trtinset=cell2mat(trtinset);

brtinset=get(brha,'tightinset');
brtinset=cell2mat(brtinset);

vpad=.06;   %padding in order to make sure figures not cut off
ssize.height=(1-vpad-ssize.vspace-max(trtinset(:,2))-max(trtinset(:,4))-max(brtinset(:,2))-max(brtinset(:,4)))/2

%position information
brow.bottom=max(brtinset(:,2))
trow.bottom=ssize.height+vpad/2+ssize.vspace+brow.bottom+max(brtinset(:,4))+max(trtinset(:,2));

%**************************************
%set the position of the top row
for j=1:5   
    pos=get(fupdate.a{j}.ha,'position');
    pos(1)=(j-1)*ssize.width+sum(txtext(1:j-1,3));
    pos(2)=trow.bottom;
    pos(3)=ssize.width;
    pos(4)=ssize.height;
    set(fupdate.a{j}.ha,'position',pos);
    fupdate.a{j}.pos=pos;
end

%****************************************
%position the text objects in the top row
for j=1:length(trowtxt)    
    pos=get(trowtxt(j).h,'position');
    set(trowtxt(j).h,'horizontalalignment','center');
    %center it in between adjacent graphs
    %left=(fupdate.a{j}.pos(1)+ssize.width+fupdate.a{j+1}.pos(1)-txtext(j,3))/2
    gap=(fupdate.a{j+1}.pos(1)-fupdate.a{j}.pos(1)-ssize.width);
    left=fupdate.a{j}.pos(1)+ssize.width+gap/2;
    pos(1)=left;
    
    %set height be same as titles
    %set the units of the title to normalized
    set(fupdate.a{j}.htitle,'units','normalized');
    titpos=get(fupdate.a{j}.htitle,'position');
    titext=get(fupdate.a{j}.htitle,'extent');
    
    %titpos is in normalized units of graph and relative to lower right corner
    %xscale=1/diff(xlim)*ssize.width;
%    yl=get(fupdate.a{j}.ha,'ylim');
 %   yscale=1/diff(yl)*ssize.height;
    yscale=ssize.height;
    %pos(2)=tpos(2)*yscale;
    %height of title
    titheight=fupdate.a{j}.pos(2)+yscale*titpos(2);
    %pos(2)=trow.bottom+ssize.height;%+max(trtinset(:,4))
    pos(2)=titheight;
    set(trowtxt(j).h,'position',pos);
end


%*************************************************
%position elements in the bottom row
%1. space the image in the center of the two above it
left=(fupdate.a{1}.pos(1)+fupdate.a{1}.pos(3)+fupdate.a{2}.pos(1))/2-ssize.width/2;
pos=get(fupdate.a{br.lprior}.ha,'position');
pos(1)=left;
pos(2)=brow.bottom;
pos(3)=ssize.width;
pos(4)=ssize.height;
set(fupdate.a{br.lprior}.ha,'position',pos);
fupdate.a{br.lprior}.pos=pos;

%2. log likelihood
pos=get(fupdate.a{br.ll}.ha,'position');
pos(1)=fupdate.a{tr.llt}.pos(1);
pos(2)=brow.bottom;
pos(3)=ssize.width;
pos(4)=ssize.height;
set(fupdate.a{br.ll}.ha,'position',pos);
fupdate.a{br.ll}.pos=pos;

%3. lpost
pos=get(fupdate.a{br.lpost}.ha,'position');
pos(1)=fupdate.a{tr.lpost}.pos(1);
pos(2)=brow.bottom;
pos(3)=ssize.width;
pos(4)=ssize.height;
set(fupdate.a{br.lpost}.ha,'position',pos);
fupdate.a{br.lpost}.pos=pos;
%add dots to show the means


%4. lpgauss
pos=get(fupdate.a{br.lpgauss}.ha,'position');
pos(1)=fupdate.a{tr.lpgauss}.pos(1);
pos(2)=brow.bottom;
pos(3)=ssize.width;
pos(4)=ssize.height;
set(fupdate.a{br.lpgauss}.ha,'position',pos);
fupdate.a{br.lpgauss}.pos=pos;

%****************************************
%position the text objects in the bottom row
%brtxtext=get([browtxt.h],'extent');
%brtxtext=cell2mat(brtxtext);
for ti=1:3    
    j=ti+5;
    pos=get(browtxt(ti).h,'position');
        set(browtxt(ti).h,'horizontalalignment','center');
        
    %center it in between adjacent graphs
%    left=(fupdate.a{j}.pos(1)+fupdate.a{j}.pos(3)+fupdate.a{j+1}.pos(1)-brtxtext(ti,3))/2;
    %center it in between adjacent graphs
    %left=(fupdate.a{j}.pos(1)+ssize.width+fupdate.a{j+1}.pos(1)-txtext(j,3))/2
    gap=(fupdate.a{j+1}.pos(1)-fupdate.a{j}.pos(1)-ssize.width);
    left=fupdate.a{j}.pos(1)+ssize.width+gap/2;
    pos(1)=left;
    
    %set height be same as titles    
    titpos=get(fupdate.a{j}.htitle,'position');
    
    %titpos is in units of graph and relative to lower right corner
    %so we need to scale and shift appropriately
    yl=get(fupdate.a{j}.ha,'ylim');
    yscale=1/diff(yl)*ssize.height;
    %height of title
%    titheight=fupdate.a{j}.pos(2)+yscale*titpos(2);   
    titheight=fupdate.a{j}.pos(2)+yscale*(titpos(2)-yl(1));
    pos(2)=titheight;
    set(browtxt(ti).h,'position',pos);
end

pos=get(fupdate.hf,'position');
pos(3)=gparam.width;
pos(4)=gparam.height;
set(fupdate.hf,'position',pos);

%***********************************************************************
%turn off the panel showing the Gaussian approximation of the batch mode"
%************************************************************
delete(fupdate.a{tr.lpgauss}.ha);
delete(trowtxt(4).h);
%previewfig(fupdate.hf,'bounds','tight','FontMode','Fixed','FontSize',fupdate.fontsize,'Width',gparam.width,'Height',gparam.height);
%saveas(fupdate.hf,'~/svn_trunk/research/figures/update_schematics.eps','epsc2');
%saveas(fupdate.hf,'~/cvs_ece/writeup/NC06/update_schematics.eps','epsc2');
%exportfig(fupdate.hf,'~/cvs_ece/writeup/NC06/update_schematics.eps','bounds','tight','FontMode','Fixed','FontSize',fupdate.fontsize,'Width',gparam.width,'Height',gparam.height);