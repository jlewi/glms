%%Can be called as function or script
%call as function
%   specify the values as named parameter pairs
%   i.e ('mu', value)
%make a 3d plot of a likelihood function that only varies along one
%dimension.
%clear all;
function [fg,zll,thetax,thetay]=ll1d(varargin)
%*******************************************
% default values
thetax=[-2:.05:2];
thetay=[-2:.05:2];
lpthreshold=-68;
dt=1;
nspikes=10;
nclines=20;  %number of contour lines

stim=[2.3 2.3];

%loop through the parameters and set any values
vind=1;
while (vind<nargin)
    eval(sprintf('%s=varargin{vind+1};',varargin{vind}));
    vind=vind+2;
end

%compute the mean
ev=log(nspikes)/2/stim(1);
zll=zeros(length(thetax),length(thetay));

for xind=1:length(thetax)
    for yind=1:length(thetay)
        %xval(count)=x(xind);
       %yval(count)=y(yind);
        %zval(xind,yind)=exp(ll1dtemp(nspikes,stim,[thetax(xind);thetay(yind)],dt));
        zll(xind,yind)=ll1dtemp(nspikes,stim,[thetax(xind);thetay(yind)],dt);
        %count=count+1;
    end
end

fg.hf=figure;
fg.axisfontsize=12;

%colormap('gray');

%draw a surface plot
%surf(thetax,thetay,zval)

zneg=find(zll<lpthreshold);
%zll(zneg)=lpthreshold;
zll(zneg)=nan;

%draw a contour plot
contourf(thetax,thetay,zll,nclines);
%contourf(thetax,thetay,zll,[linspace(-3.25,-2.08, 10)]);
%contourf(thetax,thetay,zll,[linspace(min(zll(:)),max(zll(:)), 10)]);
fg.ha=gca;
%colormap hsv
%colorbar

set(gca,'FontSize',fg.axisfontsize);
xlabel('\theta_1');
ylabel('\theta_2');
ht=title('$p(r|\vec{x},\vec{\theta})$');
set(ht,'interpreter','latex');
hz=zlabel('$P(r_t\,\,|\vec{x}_t,\vec{\theta{}}$)');
set(hz,'Rotation',0);
set(hz,'interpreter','latex');
set(gca,'xtick',[]);
set(gca,'ytick',[]);
set(gca,'ztick',[]);
fg=lblgraph(fg);
%axis equal;
%hz=zlabel('P(\vec\theta |\vec{k},\vec{a})');
%axis tight;
axis square;
axis tight;
%exportfig(gcf,fullfile('..','..','research','phd','presentations','figures','1dlike.eps'),'bounds','tight','Color','bw');