%11-28-06
%       updated the script to use the neew post method
%8-29-2006
%
% This script makes a plot of the updated gaussian used to approximate the
% posterior
%
% This script is part of the scripts used to make the figures used to
% illustrate the newtonian update rule
%
% Usage:
%   1. Run the script ll1d first
%
% Other Relevant scripts
%   1. dgaussian and dgaussiansurf - this will be in my scripts folder for
%   drawing diagrams
%           they can be used to make images of gaussians
function [fg]=approxpost(varargin)

%*******************************************************
%defaults for when run as script
prior.m=[0;0];
prior.c=.05*[1 0;0 1];
stim=[2.3;2.3];
dt=1;
obsrv.n=10;
obsrv.twindow=1;
fu=@glm1dexp;
%specify what glm to use
glm.fglmnc=@glm1dexp;  %normalizing function for the distribution. 
glm.fglmetamu=@etamupoiss;   %function to convert mean into canonical parameter depends on the distribution
glm.fglmmu=@glm1dexp;          %link funciton - funciton to generate the mean
glm.sampdist=@poissrnd;


%loop through the parameters and set any values
vind=1;
while (vind<nargin)
    eval(sprintf('%s=varargin{vind+1};',varargin{vind}));
    vind=vind+2;
end
post=postnewton1d(prior,stim,obsrv,glm,10^-5);

%call dgaussian to draw the gaussian
[fg]=dgaussian('mu',post.m,'sigma',post.c,'x',thetax,'y',thetay,'nclines',nclines,'lpthreshold',lpthreshold);