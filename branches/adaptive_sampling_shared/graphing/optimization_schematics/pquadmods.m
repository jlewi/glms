%11-20-06
%make plot to illustrate how we modify the quadratic form to eliminate the
%linear constraint.

%black and white or color
iscolor=false;
lwidth=2.5; %line widths for arrows
figure;

colormap(gray);
hold on;
[x,y,z] = ellipsoid(0,0,0,4,2,1,40);
he=surf(x,y,z);

%draw a plane;
%define the normal vector
m=2^.5;        %projeciton along normal vector
nvec=[1;0;.5];
nvec=nvec/(nvec'*nvec)^.5;
x=[1:.1:2];
y=[-2:.1:2];
z=zeros(length(y),length(x));
for k=1:length(x)
    for j=1:length(y)
        z(j,k)=(m-nvec(1)*x(k)-nvec(2)*y(j))/nvec(3);
    end
end

%now we shift the plane 
%m is distance along the nvector
%we compute the projection of m*nvec on the z vector and then add this to
%the coordinat
hp=surf(x,y,z);

%get rid of the edge colors
set(hp,'EdgeColor','none');     
set(he,'EdgeColor','none');     
%set transparency
set(he,'FaceAlpha',.5);

if (iscolor)
set(hp,'FaceColor','blue');
else
set(hp,'FaceColor','black');    
end


%this makes axis tick marks equal
axis equal
%axis square;
%draw an arrow to the plane;
ha=arrow([0;0;0],m*nvec);
set(ha,'Linewidth',lwidth);

if (iscolor)
set(ha,'FaceColor','red');
set(ha,'EdgeColor','red');
else
    acolor=.75*ones(1,3);
    set(ha,'FaceColor',acolor);
    set(ha,'EdgeColor',acolor);
end

CameraPosition = [-38.9501 -48.2024 13.9937];
CameraUpVector = [0.239347 0.296202 0.960137];
CameraViewAngle = [5.59969];
CameraTarget = [0 0 0.75];
set(gca,'CameraPosition',CameraPosition);
set(gca,'CameraUpVector',CameraUpVector);
set(gca,'CameraViewAngle',CameraViewAngle);
set(gca,'CameraTarget',CameraTarget);
xlabel('x');
ylabel('y');


%draw an arrow to define the x,y,z, axes
hax=arrow([0;0;0],[5;0;0]);
hay=arrow([0;0;0],[0;-6;0]);
haz=arrow([0;0;0],[0;0;3.5]);
set([hax,hay,haz],'LineWidth',lwidth);
axis off;

%exportfig(gcf,'~/cvs_ece/writeup/NC06/quadmod.eps');