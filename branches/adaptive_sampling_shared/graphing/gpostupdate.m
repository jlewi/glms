%11-19-2005
%Script to make plots illustrating whats happening in the 
%update of the posterior
close all;


%x and ylimits
dlim=[-10 10];
dd=.1;
%make po a gaussian
p0.m=[0;0];
p0.c=[1 0;0 5];

p0.p=mvgaussmatrix(dlim,dd,p0.m,p0.c);

figure;
%transpose the gaussian so that y is on the yaxis
imagesc([-10:dd:10],[-10:dd:10],(p0.p));
title('P_0(\theta)');
axis square;
hold on

%compute the variance in directions d1 and d2
d1=[2; 1];
d1=d1/(d1'*d1)^.5;
d2=[1;-1];
d2=d2/(d2'*d2)^.5;

vd1=d1'*p0.c*d1;
vd2=d2'*p0.c*d2;

%plot the vectors representing the variances
line([0;vd1*d1(1)],[0;vd1*d1(2)],'color','k');
line([0;vd2*d2(1)],[0;vd2*d2(2)]);

%make a plot of the gaussian which has these variances in these directions

papprox.m=p0.m;
papprox.c=[d1 d2]*[vd1 0; 0 vd2]*[d1 d2]';
papprox.p=mvgaussmatrix(dlim,dd,papprox.m,papprox.c);

figure;
%transpose the gaussian so that y is on the yaxis
imagesc([-10:dd:10],[-10:dd:10],(papprox.p)');
title('Variances to Match');
axis square;
hold on

%make a plot of the gaussian which has these variances in these directions
papprox.m=p0.m;
papprox.c=[d1 d2]*[vd1 0; 0 vd2]*[d1 d2]';
papprox.p=mvgaussmatrix(dlim,dd,papprox.m,papprox.c);

figure;
%transpose the gaussian so that y is on the yaxis
imagesc([-10:dd:10],[-10:dd:10],(papprox.p)');
title('Variances to Match');
axis square;
hold on


%make a plot of ct-1
prior.m=p0.m;
prior.c=p0.c-vd1*d1*d1';
%make diagonal elements positive
prior.c=prior.c.*sign(prior.c).*eye(2,2);

prior.p=mvgaussmatrix(dlim,dd,prior.m,prior.c);

figure;
imagesc([-10:dd:10],[-10:dd:10],prior.p')
title('C_t_1_1-vd1*d1*d1 with absolute value');