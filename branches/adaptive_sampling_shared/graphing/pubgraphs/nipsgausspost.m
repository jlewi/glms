%11-15-06
%Make a plot comparing the posterior estimate using our gaussian
%approximation to the estiamte using all the observations
data=[];
data(end+1).infile=fullfile(RESULTSDIR,'nips','11_15','11_15_lowdcos_001.mat');
data(end).outfile=fullfile(RESULTSDIR,'nips','11_15','11_15_cmp_mean_001.eps');

data(end).fsuffix='_img';
data(end).yticks=[1 25:25:100];
data(end).xticks=[1 10 20 25];
data(end).clim{1}=[-2 2];                
data(end).trials=[1:100];

dind=length(data);
%dind=3;
%**************************************************************************
%Select the data set
%**************************************************************************

    %load data from file
    load(data(dind).infile);
	outfile=data(dind).outfile;
    xticks=data(dind).xticks;
    yticks=data(dind).yticks;
    clim=data(dind).clim;
    fprintf('\n\n Plotting data from file: \n %s \n', data(dind).infile);
    trials=data(dind).trials;


%***************************************************
%Plot the mean
%*****************************************************
plog=0;
%index =1 : plot stimulus coefficients
%index =2 :plot spikehistory coefficients
if (mparam.alength>0)
    nfigs=2;
else
    nfigs=1;
end
figs=cell(1,nfigs);
for index=1:nfigs
    fmean=[];
    fmean.hf=figure;
    
    
    %**********************************************************************
    %Configure the image for aistat
    %***********************************
    fmean.font='Times'; %use a scalable font
set(fmean.hf,'units','inches');
fpos=get(fmean.hf,'position');
fpos(3)=3.5;
fpos(4)=2;
set(fmean.hf,'position',fpos);
fmean.axisfontsize=10;% was point 1
fmean.lgndfontsize=10; 
fmean.fontunits='Points';
fmean.font='Times'; %use a scalable font
fmean.hp=[];

%*********************************
%we plot the results for approximation using poster and then using all
%observations
%true values below them
nrows=2;
ncols=2;
%colormap(gray);


%*************************************
%Plot: infomax
%***************************************
ha(1)=subplot(nrows,ncols,1);
set(gca,'fontunits',fmean.fontunits);
set(gca,'fontsize',fmean.axisfontsize);

fmean.hp(end+1)=imagesc(pmax.newton1d.m(:,trials)',clim{1});
title(sprintf('Gaussian Approximation'));
%xlabel('i');
ylabel('Trial');
set(gca,'ytick',yticks);
set(gca,'YTickLabel',yticks);
set(gca,'xtick',[]);
%set(gca,'xtick',xticks);
%set(gca,'XTickLabel',xticks);

%*****************************************
%Plot: True below infomax
%****************************************
ha(3)=subplot(nrows,ncols,3);
set(gca,'fontunits',fmean.fontunits);
set(gca,'fontsize',fmean.axisfontsize);
imagesc(mparam.ktrue',clim{1});

%title('True \theta');
xlabel('i');
set(gca,'xtick',xticks);
set(gca,'XTickLabel',xticks);
set(gca,'ytick',[]);

%colorbar resizes this image so lets set its to be equal to other axes
%execute a drawnow command to force matlab to determine the sizes for the
%individual axes
%Too make each axis the same size we need to do the following
%   1. set the position of the last subplot appropriately (its size is not
%   the same because we add the colorbar)
%   2. switch the ActivePositionProperty to 'position' so it scales the
%   image based on Position not outerposition.
drawnow;
ha(1)=subplot(nrows,ncols,1);
ha(2)=subplot(nrows,ncols,2);
subplot(nrows,ncols,3);


ha(2)=subplot(nrows,ncols,2);
set(gca,'fontsize',fmean.axisfontsize);
imagesc(pmax.allobsrv.m(:,trials)',clim{1});
hc=colorbar;


%***************************************************
%Plot: Approximation using all observations
%**************************************************
title('All Observations');
%xlabel('i');
%ylabel('Trial');
set(fmean.hf,'name','Post Series');
set(gca,'fontunits',fmean.fontunits);
set(gca,'fontsize',fmean.axisfontsize);
set(gca,'xtick',[]);
%set(gca,'xtick',xticks);
%set(gca,'XTickLabel',xticks);
set(gca,'ytick',[]);
%set(gca,'ActivePositionProperty','Position');
%subplot(nrows,ncols,4);
%hc=colorbar;
%set(hc,'CLim',clim);

%***********************************************
%Plot: true beneath random
%**********************************************
ha(4)=subplot(nrows,ncols,4);
set(gca,'fontsize',fmean.axisfontsize);
set(gca,'fontunits',fmean.fontunits);
imagesc(mparam.ktrue',clim{1});

%title('True \theta');
xlabel('i');
set(gca,'xtick',xticks);
set(gca,'XTickLabel',xticks);
set(gca,'ytick',[]);

lblgraph(fmean);


%*********************************************************************
%Position the figures
%*******************************************************************
%set the figure size based on how big it will be in our pdf so we don't
%have to resize
%set(fmean.hf,'units','inches');
%fpos=get(fmean.hf,'position');
%fpos(3)=1.5;
%fpos(4)=1.75;
%set(fmean.hf,'Position',fpos);
%everthing is measured in relative screen coordinates
%determine how much space we need for the labels
%tinset=[left bottom right top]
tinset=zeros(nrows*ncols+1,4);
for hind=1:4
    tinset(hind,:)=get(ha(hind),'tightinset');
end
tinset(5,:)=get(hc,'tightinset');               %tightinset for colorbar
%we want to find the effective width and height which is how much room we
%have which isn't taken up by the labels
%effective width measure the left and right margins need
%for the infomax, random, and colorbar
effwidth=1-(tinset(1,1)+tinset(1,3))-(tinset(2,1)+tinset(2,3))-(tinset(5,1)+tinset(5,3));
%effective height depends on top bottom
%for infomax and true
effheight=1-(tinset(1,2)+tinset(1,4))-(tinset(3,2)+tinset(3,4));


%we need to position the figures
%we want the interiors of the figures to be aligned
%when positioning the interior of the figures we need to make sure we
%allowe room for the labels

wspace=.07;         %spacing between the two columns of figures
hspace=.05;         %horizontal spacing

htrue=.12;           %height for the true graph

hpost=effheight-hspace-htrue;   %height for graph of posteriors

wcb=.05;             %width for colorbar
wtocb=.01;          %distance from edge of random to colorbar
wpost=(effwidth-wspace-wcb-wtocb)/2;

bpost=1-tinset(1,4)-hpost; %bottom of the posterior results
                                               %its set relative to top of
                                               %figure
%set the position of the infomax results
linfo=tinset(1,1);                  %left coordinate of post info max
set(ha(1),'position',[linfo bpost wpost hpost]);

%set the position of ktrue
btrue=tinset(3,2);                  %bottom of true is set relative to bottom of figure

set(ha(3),'position',[linfo btrue wpost htrue]);

%set the position of the results for random
lrandom=linfo+wspace+wpost;     %left coordinate of posterior results for random
set(ha(2),'position',[lrandom bpost wpost hpost]);


%set the position of ktrue below random results
set(ha(4),'position',[lrandom btrue wpost htrue]);



%previewfig(fmean.hf,'bounds','tight','Color','bw');
%exportfig(fmean.hf,fmean.fname,'bounds','tight','Color','rgb');
figs{index}=fmean;
end
exportfig(fkl.hf,data(end).outfile,'bounds','tight','color','rgb');