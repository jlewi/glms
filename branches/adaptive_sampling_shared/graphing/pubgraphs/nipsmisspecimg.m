%11-16-2006
%Look at the results for a misspecified model
%Since the estimate will only be accurate up to a scaling constant
%normalize the true parameter vector and the estimates in order to
%eliminate effects of the scaling factor
clear all
setpaths
%simfiles{1}=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_001.mat');

simfiles{1}=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_001.mat');
simfiles{1}=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_003.mat');
simfiles{1}=fullfile(RESULTSDIR,'nips','11_20','11_20_misspecified_001.mat');

data(1).simfile=fullfile(RESULTSDIR,'nips','11_20','11_20_misspecified_001.mat');;
data(1).clim=[-.4 .4];
data(1).xticks=[1 10 20];
data(1).yticks=[250:250:1000];
dind=1;
outbname='misspec';
varsuffix={'rand','max'};
%umeth={'newton1d','allobsrv','allobsrvtrue'}
umeth={'allobsrv'}
%umeth={'newton1d','allobsrvtrue'}
%compute the angle
results=[];
rind=0;

nplots=length(varsuffix)*length(umeth);

%***********************************************************************
%compute information for spacing the subplots
%***********************************************************************
gspace.hspace=.05;         %vertical spacing between graphs)
gspace.wspace=.05;         %horizontal spacing
gspace.border=[.17 .13 .15 .10];    %border to leave on [left bottom right top];
gspace.htrue=.12;            %height for graph of true parameters
gspace.hpost=(1-gspace.hspace-gspace.htrue-gspace.border(2)-gspace.border(4));
gspace.wpost=(1-(nplots-1)*gspace.wspace-gspace.border(1)-gspace.border(3))/nplots;

gspace.wcb=.05;             %width for colorbar
gspace.wtocb=.01;          %distance from edge of random to colorbar


ferr=[];
ferr.hf=figure;
ferr.xlabel='Trials';
ferr.ylabel='Angle (degrees)';
ferr.hp=[];
ferr.lbls={};
ferr.markersize=10;
ferr.axisfontsize=20;
hold on;

%**************************************
%generate all the axes for the subplots
for i=1:nplots
    ferr.hpost(i)=subplot(2,nplots,i);
end
for i=1:nplots
    ferr.htrue(i)=subplot(2,nplots,i+nplots);
end
for vind=1:length(varsuffix)
   pdata=load(simfiles{1},sprintf('p%s',varsuffix{vind}));
   mparam=load(data(dind).simfile,'mparam');
   pdata=pdata.(sprintf('p%s',varsuffix{vind}));
   mparam=mparam.mparam;
   simparam=load(simfiles{1},sprintf('simparam%s',varsuffix{vind}));
   simparam=simparam.(sprintf('simparam%s',varsuffix{vind}));
   %name for output file
    [pathstr,NAME,EXT,VERSN] = fileparts(simfiles{1});
    outfile=seqfname(fullfile(pathstr,sprintf('%s_%0.3d.eps',outbname,simparam.trialindex)));
    for uind=1:length(umeth)
        rind=rind+1;

        %normalize the mean estimate
        mag=(sum((pdata.(umeth{uind}).m).^2,1)).^(-.5);
        mvecs=(ones(mparam.klength+mparam.alength,1)*mag).*pdata.(umeth{uind}).m;
        truevec=[mparam.ktrue;mparam.atrue];
        truevec=truevec/(truevec'*truevec)^.5;

        %label for graph
        if (strcmp(varsuffix{vind},'max')==1)
            results(rind).lbl='Info max:';
        else
            results(rind).lbl='I.I.D:';
        end
        if (strcmp(umeth{uind},'allobsrv')==1)
            results(rind).lbl=sprintf('%s All Observations',results(rind).lbl);
        elseif(strcmp(umeth{uind},'newton1d')==1)
         results(rind).lbl=sprintf('%s Gauss. Approx.',results(rind).lbl);
       elseif(strcmp(umeth{uind},'allobsrvtrue')==1)
         results(rind).lbl=sprintf('%s True Nonlinearity',results(rind).lbl);
        end
        
        %*********************************************
        %plot it
        %**********************************************
        pind=rind;
        subplot(ferr.hpost(rind));
        imagesc(mvecs',data(dind).clim);
          %if its the last axes add a colorbar
        if (rind==nplots)
            ferr.hc=colorbar;
        end
        %set the position
        %position=[left bottom width height]
        left=(rind-1)*(gspace.wspace+gspace.wpost)+gspace.border(1);
        bottom=1-gspace.border(4)-gspace.hpost;
        set(ferr.hpost(rind),'position',[left bottom gspace.wpost gspace.hpost]);
        set(ferr.hpost(rind),'fontsize',ferr.axisfontsize);
        drawnow;
        %turn of tick marks
        set(ferr.hpost(rind),'xtick',[]);
        set(ferr.hpost(rind),'ytick',[]);
if (strcmp(varsuffix{vind},'max')==1)
            title('Info max');
        else
            title('I.I.D');
        end
        
        %plot the true parameters below it            
        subplot(ferr.htrue(rind));
        imagesc(mparam.ktrue'/(mparam.ktrue'*mparam.ktrue)^.5,data(dind).clim);
       
      
        %position=[left bottom width height]
        left=(rind-1)*(gspace.wspace+gspace.wpost)+gspace.border(1);
        bottom=gspace.border(2);
        set(ferr.htrue(rind),'position',[left bottom gspace.wpost gspace.htrue]);
        set(ferr.htrue(rind),'fontsize',ferr.axisfontsize);
        drawnow;
        %turn off xticks
        xlabel('i');
        set(ferr.htrue(rind),'ytick',[]);
        set(ferr.htrue(rind),'xtick',data(dind).xticks);
        clear('mvecs','truevec','mag');
    end
end
drawnow;
%******************
%position the colorbar
%cpos=get(ferr.hc,'position');
%tin=get(ferr.hc,'tightinset');
 % bottom=1-gspace.border(4)-gspace.hpost;
%set(ferr.hc,'position',[1-tin(3)-cpos(3), bottom,  cpos(3) gspace.hpost]);
%***********************************
%label axes
subplot(ferr.hpost(1))
ylabel('Trial');
set(ferr.hpost(1),'ytick',data(dind).yticks);
subplot(ferr.hpost(nplots));

subplot(ferr.htrue(1));
ylabel('\theta');

%exportfig(ferr.hf,outfile,'bounds','tight','color','rgb');