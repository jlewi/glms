%11-16-2006
%Look at the results for a misspecified model
%Since the estimate will only be accurate up to a scaling constant plot the
%angle between the true parameter and the estimated parameter
clear all
setpaths
%simfiles{1}=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_001.mat');

simfiles{1}=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_001.mat');
simfiles{1}=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_003.mat');
simfiles{1}=fullfile(RESULTSDIR,'nips','11_20','11_20_misspecified_001.mat');
outbname='misspec';
varsuffix={'rand','max'};
%umeth={'newton1d','allobsrv','allobsrvtrue'}
umeth={'allobsrv','allobsrvtrue'}
%umeth={'newton1d','allobsrvtrue'}
%compute the angle
results=[];
rind=0;

lstyle(1).lw=3;
lstyle(1).lcolor=[0 0 0];
lstyle(1).lcolor=[1 0 0];
lstyle(1).lstyle='-'

lstyle(2).lw=2;
lstyle(2).lcolor=[.75 .75 .75];
lstyle(2).lcolor=[0 0 1];
lstyle(2).lstyle='-';


lstyle(3).lw=3;
lstyle(3).lcolor=[0 0 0];
lstyle(3).lcolor=lstyle(1).lcolor;
lstyle(3).lstyle='-.';


lstyle(4).lw=2;
lstyle(4).lcolor=[.75 .75 .75];
lstyle(4).lcolor=lstyle(2).lcolor;
lstyle(4).lstyle='-.';

for vind=1:length(varsuffix)
   pdata=load(simfiles{1},sprintf('p%s',varsuffix{vind}));
   mparam=load(simfiles{1},'mparam');
   pdata=pdata.(sprintf('p%s',varsuffix{vind}));
   mparam=mparam.mparam;
   simparam=load(simfiles{1},sprintf('simparam%s',varsuffix{vind}));
   simparam=simparam.(sprintf('simparam%s',varsuffix{vind}));
   %name for output file
    [pathstr,NAME,EXT,VERSN] = fileparts(simfiles{1});
    outfile=seqfname(fullfile(pathstr,sprintf('%s_%0.3d.eps',outbname,simparam.trialindex)));
    for uind=1:length(umeth)
        rind=rind+1;

        %normalize the mean estimate
        mag=(sum((pdata.(umeth{uind}).m).^2,1)).^(-.5);
        mvecs=(ones(mparam.klength+mparam.alength,1)*mag).*pdata.(umeth{uind}).m;
        truevec=[mparam.ktrue;mparam.atrue];
        truevec=truevec/(truevec'*truevec)^.5;
        results(rind).angle=acos(truevec'*mvecs)*180/pi;
        if (strcmp(varsuffix{vind},'max')==1)
            results(rind).lbl='Info max:';
        else
            results(rind).lbl='I.I.D:';
        end
        if (strcmp(umeth{uind},'allobsrv')==1)
            results(rind).lbl=sprintf('%s All Observations',results(rind).lbl);
        elseif(strcmp(umeth{uind},'newton1d')==1)
         results(rind).lbl=sprintf('%s Gauss. Approx.',results(rind).lbl);
       elseif(strcmp(umeth{uind},'allobsrvtrue')==1)
         results(rind).lbl=sprintf('%s True Nonlinearity',results(rind).lbl);
        end
        clear('mvecs','truevec','mag');
    end
end
ferr=[];
ferr.hf=figure;
ferr.xlabel='Trials';
ferr.ylabel='Angle (degrees)';
ferr.hp=[];
ferr.lbls={};
ferr.markersize=10;
%ferr.linewidth=2;
hold on;

for rind=1:length(results)
    %plot(ksamps,dk);
    %[c,m]=getptype(1,1)
%    ferr.hp(rind)=plot(0:(length(results(rind).angle)-1),results(rind).angle,getptype(rind,1));
    ferr.hl(rind)=plot(0:(length(results(rind).angle)-1),results(rind).angle,getptype(rind,2));
     set(ferr.hl(rind),'Color', lstyle(rind).lcolor);
  %  set(fkl.hp(mind),'Marker', mark);
    set(ferr.hl(rind),'LineStyle',lstyle(rind).lstyle);
    %set(fkl.hp(mind),'MarkerSize',fkl.markersize);
    set(ferr.hl(rind),'LineWidth',lstyle(rind).lw);
 
end

legend(ferr.hp,{results.lbl});
lblgraph(ferr);
xlim([0 (length(results(rind).angle)-1)]);
set(gca,'xscale','log');

%exportfig(ferr.hf,outfile,'bounds','tight','color','rgb');