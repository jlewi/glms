%10-06-06
%make plots of the 1d plots of the mean as a function of trial

%create a structure which contains the relevant fields for different data
%sets we then select which element of the structure we want to select the
%data. if we want to use data in the current workspace set dind=0;
dind=0;

data=[];
data(end+1).infile=fullfile('results', 'tracking','06_02','06_02_bumptrack_005.mat');
data(end).outfile=fullfile('writeup','nips','bumptrack_trials.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
data(end).trials=[0 200 400];
data(end).fsuffix='_bumptrack';
data(end).yticks=[1 200:200:800];
data(end).xticks=[1 50 100];
    


data(end+1).infile=fullfile('results', 'tracking','06_03','06_03_bumptrack_018.mat');
data(end).outfile=fullfile('writeup','nips','bumptrack_mean.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
data(end).trials=[0 200 400];
data(end).fsuffix='_bumptrack';
data(end).yticks=[1 200:200:800];
data(end).xticks=[1 50 100];
data(end).clim=[-.5 1];

data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/glmgen/10_08/10_08_maxtrack_003.mat');
data(end).outfile=fullfile(RESULTSDIR,'glmgen/10_08/postimg_003.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
%data(end).trials=[0 200 400];
data(end).fsuffix='_img';
data(end).yticks=[1 100:100:600];
data(end).xticks{1}=[1 25:25:100];
data(end).xticks{2}=[1 5 10];
data(end).clim{1}=[-2 2];                  %limits for stimulus coefficents
data(end).clim{2}=[-7 0];         %limits for fixed terms
data(end).shistlog=1;                        %plot spike history on log scale
dind=length(data)-1;

data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/glmgen/10_10/10_10_maxtrack_001.mat');
data(end).outfile=fullfile(RESULTSDIR,'tracking/10_10/postimg_001.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
%data(end).trials=[0 200 400];
data(end).fsuffix='_img';
data(end).yticks=[1 100:100:600];
data(end).xticks{1}=[1 25:25:100];
data(end).xticks{2}=[1 5 10];
data(end).clim{1}=[-2 2];                  %limits for stimulus coefficents
data(end).clim{2}=[-7 0];         %limits for fixed terms
data(end).shistlog=1;                        %plot spike history on log scale

data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/lognonlin/10_11/10_11_maxtrack_003.mat');
data(end).outfile=fullfile(RESULTSDIR,'lognonlin/10_11/postimg_001.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
%data(end).trials=[0 200 400];
data(end).fsuffix='_img';
data(end).yticks=[1 500:500:1500];
data(end).xticks{1}=[1 25:25:100];
data(end).xticks{2}=[1 5 10];
data(end).clim{1}=[-2 2];                  %limits for stimulus coefficents
data(end).clim{2}=[-7 0];         %limits for fixed terms
data(end).shistlog=1;                        %plot spike history on log scal

data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/lognonlin/10_11/10_11_maxtrack_005.mat');
data(end).outfile=fullfile(RESULTSDIR,'lognonlin/10_11/postimg_001.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
%data(end).trials=[0 200 400];
data(end).fsuffix='_img';
data(end).yticks=[1 10:10:100];
data(end).xticks{1}=[1 25:25:100];
data(end).xticks{2}=[1 5 10];
data(end).clim{1}=[-2 2];                  %limits for stimulus coefficents
data(end).clim{2}=[-7 0];         %limits for fixed terms
data(end).shistlog=1;                        %plot spike history on log scal

data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/glmgen/10_12/10_12_maxtrack_001.mat');
data(end).outfile=fullfile(RESULTSDIR,'glmgen/10_12/postimg_001.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
%data(end).trials=[0 200 400];
data(end).fsuffix='_img';
data(end).yticks=[1 100:100:400];
data(end).xticks{1}=[1 25:25:100];
data(end).xticks{2}=[1 5 10];
data(end).clim{1}=[-2 2];                  %limits for stimulus coefficents
data(end).clim{2}=[-7 0];         %limits for fixed terms
data(end).shistlog=1;                        %plot spike history on log scal

data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/aistat/10_15/10_15_maxtrack_013.mat');
data(end).outfile=fullfile(RESULTSDIR,'aistat/10_15/postimg_001.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
%data(end).trials=[0 200 400];
data(end).fsuffix='_img';
data(end).yticks=[1 500:500:2000];
data(end).xticks{1}=[1 100:100:200];
data(end).xticks{2}=[1 5 10];
data(end).clim{1}=[-2 2];                  %limits for stimulus coefficents
data(end).clim{2}=[-7 0];         %limits for fixed terms
data(end).shistlog=1;                        %plot spike history on log scal

data(end+1).infile=fullfile(RESULTSDIR,'nips','12_19','12_19_shist_002.mat');
data(end).outfile=fullfile(RESULTSDIR,'nips','12_19','12_19_shist_post.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
data(end).fsuffix='_shist';
data(end).yticks=[1 400 800];
data(end).xticks{1}=[1 50];
data(end).xticks{2}=[1 10];
data(end).clim{1}=[-2.5 2.5];
data(end).clim{2}=[-7 0];
data(end).shistlog=1;
data(end).width=1.8;                %width of image in inches width was 2.5
data(end).height=2;                 %height of images
data(end).fsize=10;                 %fontsize
dind=length(data);

%dind=3;
%**************************************************************************
%Select the data set
%**************************************************************************
if (dind==0)
    %use following parameters and data in the workspace
    %create suffix for file
    %get directory of current file
    if ~isempty(ropts.fname)
        [outdir,fname] =fileparts(ropts.fname);
        if isfield(ropts,'trialindex')
            trialindex=ropts.trialindex;
        else
            trialindex=str2num(fname(end-2:end));
        end
        trialdate=(fname(1:5));

        fsuffix=sprintf('_%s_%.3i.eps',trialdate,trialindex);
    else
     outdir=RESULTSDIR;
     fusffix='';
    end
    outfile=fullfile(outdir,sprintf('bumptrack_%s_%.3i.eps',trialdate,trialindex));
    yticks=[1 200:200:800];
    xticks=[1 50 100];
    clim=[-1 1];
    
else
    %load data from file
    load(data(dind).infile);
	outfile=data(dind).outfile;
    xticks=data(dind).xticks;
    yticks=data(dind).yticks;
    clim=data(dind).clim;
    fprintf('\n\n Plotting data from file: \n %s \n', data(dind).infile);
end


%***************************************************
%Plot the mean
%*****************************************************
plog=0;
%index =1 : plot stimulus coefficients
%index =2 :plot spikehistory coefficients
if (mparam.alength>0)
    nfigs=2;
else
    nfigs=1;
end
figs=cell(1,nfigs);
for index=1:nfigs
    fmean=[];
    fmean.hf=figure;
    
    
    %**********************************************************************
    %Configure the image for aistat
    %***********************************
    fmean.font='Times'; %use a scalable font
set(fmean.hf,'units','inches');
fpos=get(fmean.hf,'position');
fpos(3)=3.5;
fpos(4)=2;
set(fmean.hf,'position',fpos);
fmean.axisfontsize=10;% was point 1
fmean.lgndfontsize=10; 
fmean.fontunits='Points';
fmean.font='Times'; %use a scalable font

    %extract the data
    if index==1
        pdata.pmax=pmax.newton1d.m(1:mparam.klength,:)';
        pdata.prand=prand.newton1d.m(1:mparam.klength,:)';
        pdata.true=mparam.ktrue';
        fmean.name='Stimulus Coefficients'
        [pathstr, name,ext]=fileparts(outfile);
        fmean.fname=fullfile(pathstr,sprintf('%s_stim%s',name,ext));
    else
        %plot spike history on log scale
        %assume its all inhibitory
        plog=0;
        if isfield(data(dind),'shistlog')
            if (data(dind).shistlog~=0)
              plog=1;
              
            end
        end
        if (plog~=0)
                
        pdata.pmax=log10(abs(pmax.newton1d.m(mparam.klength+1:end,:)))';
       pdata.prand=log10(abs(prand.newton1d.m(mparam.klength+1:end,:)))';
               pdata.true=log10(abs(mparam.atrue))';
               
             
        else
                            
        pdata.pmax=pmax.newton1d.m(mparam.klength+1:end,:)';
       pdata.prand=prand.newton1d.m(mparam.klength+1:end,:)';
               pdata.true=mparam.atrue';
        end
     
        fmean.name='Spike History Coefficients';
        [pathstr, name,ext]=fileparts(outfile);
        fmean.fname=fullfile(pathstr,sprintf('%s_hist%s',name,ext));
        %clim(1)=min(pdata.true(:));
        %clim(2)=max(pdata.true(:));
    end
    if iscell(data(dind).clim)
        clim=data(dind).clim{index};
    end
    if iscell(data(dind).xticks)
        xticks=data(dind).xticks{index};
    end
    

    fmean.hp=[];
set(fmean.hf,'name',sprintf('postseries:'));

set(fmean.hf,'name',sprintf('postseries:'));

%*********************************
%we plot the results for infomax and random trials and them the
%true values below them
nrows=2;
ncols=2;
colormap(gray);


%*************************************
%Plot: infomax
%***************************************
ha(1)=subplot(nrows,ncols,1);
set(gca,'fontunits',fmean.fontunits);
set(gca,'fontsize',fmean.axisfontsize);

fmean.hp(end+1)=imagesc(pdata.pmax,clim);
title(sprintf('info. max.'));
%xlabel('i');
%*****************************
%remove the yaxis labels from the spike history plot
set(gca,'ytick',yticks);
if (index==1)
ylabel('Trial');
set(gca,'YTickLabel',yticks);
else
    set(gca,'YtickLabel','');
end;
set(gca,'xtick',[]);

%set(gca,'xtick',xticks);
%set(gca,'XTickLabel',xticks);

%*****************************************
%Plot: True below infomax
%****************************************
ha(3)=subplot(nrows,ncols,3);
set(gca,'fontunits',fmean.fontunits);
set(gca,'fontsize',fmean.axisfontsize);
imagesc(pdata.true,clim);

%title('True \theta');
xlabel('i');
set(gca,'xtick',xticks);
set(gca,'XTickLabel',xticks);
set(gca,'ytick',[]);

%colorbar resizes this image so lets set its to be equal to other axes
%execute a drawnow command to force matlab to determine the sizes for the
%individual axes
%Too make each axis the same size we need to do the following
%   1. set the position of the last subplot appropriately (its size is not
%   the same because we add the colorbar)
%   2. switch the ActivePositionProperty to 'position' so it scales the
%   image based on Position not outerposition.
drawnow;
ha(1)=subplot(nrows,ncols,1);
ha(2)=subplot(nrows,ncols,2);
subplot(nrows,ncols,3);


ha(2)=subplot(nrows,ncols,2);
set(gca,'fontsize',fmean.axisfontsize);
imagesc(pdata.prand,clim);
hc=colorbar;


%***************************************************
%Plot: Random stimuli
%**************************************************
title('i.i.d');
%xlabel('i');
%ylabel('Trial');
set(fmean.hf,'name','Post Series');
set(gca,'fontunits',fmean.fontunits);
set(gca,'fontsize',fmean.axisfontsize);
set(gca,'xtick',[]);
%set(gca,'xtick',xticks);
%set(gca,'XTickLabel',xticks);
set(gca,'ytick',[]);
%set(gca,'ActivePositionProperty','Position');
%subplot(nrows,ncols,4);
%hc=colorbar;
%set(hc,'CLim',clim);

%***********************************************
%Plot: true beneath random
%**********************************************
ha(4)=subplot(nrows,ncols,4);
set(gca,'fontsize',fmean.axisfontsize);
set(gca,'fontunits',fmean.fontunits);
imagesc(pdata.true,clim);

%title('True \theta');
xlabel('i');
set(gca,'xtick',xticks);
set(gca,'XTickLabel',xticks);
set(gca,'ytick',[]);

%tick labels  for the colorbar get added later after we've positioned
%everything
if (index==2)
    if (plog~=0)
      cytick=get(hc,'ytick');  
      cytick=[-7  -4 -1];
      set(hc,'ytick',cytick);
      cyticklbls=cell(1,length(cytick));
      for cind=1:length(cytick)
          cyticklbls{cind}=sprintf('-10^{%0.2d}',cytick(cind));
      end
%      cyticklbls{end}=1;
      set(hc,'yticklabel',cyticklbls);

    end
end
fmean=lblgraph(fmean);


%*********************************************************************
%Position the figures
%*******************************************************************
%findall text objects
%set the fontname and fontsize of all objects
allText   = findall(fmean.hf, 'type', 'text');
set(allText,'FontName','Times');
set(allText,'FontSize',data(dind).fsize);

allAxes   = findall(fmean.hf, 'type', 'axes');
set(allAxes,'FontName','Times');
set(allAxes,'FontSize',data(dind).fsize);

%set the figure size based on how big it will be in our pdf so we don't
%have to resize
%set(fmean.hf,'units','inches');
%fpos=get(fmean.hf,'position');
%fpos(3)=1.5;
%fpos(4)=1.75;
%set(fmean.hf,'Position',fpos);
%everthing is measured in relative screen coordinates
%determine how much space we need for the labels
%tinset=[left bottom right top]
tinset=zeros(nrows*ncols+1,4);

for hind=1:4
    tinset(hind,:)=get(ha(hind),'tightinset');
end
%text is being cuttoff on xlabel so add some padding
tinset(1)=tinset(1)+.09;
tinset(5,:)=get(hc,'tightinset');               %tightinset for colorbar
%we want to find the effective width and height which is how much room we
%have which isn't taken up by the labels
%effective width measure the left and right margins need
%for the infomax, random, and colorbar
effwidth=1-(tinset(1,1)+tinset(1,3))-(tinset(2,1)+tinset(2,3))-(tinset(5,1)+tinset(5,3));
%effective height depends on top bottom
%for infomax and true
effheight=1-(tinset(1,2)+tinset(1,4))-(tinset(3,2)+tinset(3,4));


%we need to position the figures
%we want the interiors of the figures to be aligned
%when positioning the interior of the figures we need to make sure we
%allowe room for the labels

wspace=.07;         %spacing between the two columns of figures
hspace=.05;         %horizontal spacing

htrue=.12;           %height for the true graph

hpost=effheight-hspace-htrue;   %height for graph of posteriors

wcb=.05;             %width for colorbar
wtocb=.03;          %distance from edge of random to colorbar
wpost=(effwidth-wspace-wcb-wtocb)/2;

bpost=1-tinset(1,4)-hpost; %bottom of the posterior results
                                               %its set relative to top of
                                               %figure
%set the position of the infomax results
linfo=tinset(1,1);                  %left coordinate of post info max
set(ha(1),'position',[linfo bpost wpost hpost]);

%set the position of ktrue
btrue=tinset(3,2);                  %bottom of true is set relative to bottom of figure

set(ha(3),'position',[linfo btrue wpost htrue]);

%set the position of the results for random
lrandom=linfo+wspace+wpost;     %left coordinate of posterior results for random
set(ha(2),'position',[lrandom bpost wpost hpost]);


%set the position of ktrue below random results
set(ha(4),'position',[lrandom btrue wpost htrue]);
refresh;
%adjust the width and bottom of the colorbar
hcpos=get(hc,'position')
hcpos(1)=lrandom+wpost+wtocb;
hcpos(2)=bpost;
hcpos(4)=hpost;
set(hc,'position',hcpos);

%readjust the position of the second plot because above code seems to
%affect it
lrandom=linfo+wspace+wpost;     %left coordinate of posterior results for random
set(ha(2),'position',[lrandom bpost wpost hpost]);




%modify the tick labels for the spike history terms to make them true
%exponentials
%everything
if (index==2)
    if (plog~=0)
      cytick=get(hc,'ytick');  
      cytick=[-7  -4 -1];
      set(hc,'ytick',cytick);
      cyticklbls=cell(1,length(cytick));
        %THis code is slightly unstable
               %point is to make the labels of the colorbar true
               %exponentials. First set the ticklabels to be strings in
               %tex format then call extyticklbls
      for cind=1:length(cytick)
          cyticklbls{cind}=sprintf('-10^{%0.1d}',cytick(cind));
      end
%      cyticklbls{end}=1;
      set(hc,'yticklabel',cyticklbls);
      %now call Extyticklabels
      hcytick=Extyticklbls(hc);
    end
end

%previewfig(fmean.hf,'bounds','tight','Color','bw');
%exportfig(fmean.hf,fmean.fname,'bounds','tight','Color','bw');



%exportfig 
%for nips specify the figure size and the fontsize
%k=1;previewfig(figs{k}.hf,'bounds','tight','FontMode','Fixed','FontSize',data(dind).fsize,'Width',data(dind).width,'Height',data(dind).height)
%k=1;exportfig(figs{k}.hf,figs{k}.fname,'bounds','tight','FontMode','Fixed','FontSize',data(dind).fsize,'Width',data(dind).width,'Height',data(dind).height)


figs{index}=fmean;
end