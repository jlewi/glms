%***********************************************************************
%plot the spike rates
%*************************************************************************
% can be called as function or script
function  [fnspikes]=plotmse(varargin)

%*****************************************************************
%default values
%****************************************************************
%override variables
vnames={'simmax','simrand'};
for j=1:2:nargin
    switch varargin{j}
        case {'fname','filename','file','simfile'}
            fname=varargin{j+1};
      otherwise
            if exist(varargin{j},'var')
                eval(sprintf('%s=varargin{j+1};',varargin{j}));
            else
                fprintf('%s unrecognized parameter. \n',varargin{j});
            end
    end
end

fprintf('\n***************************\n Spike Counts for file: %s \n***********************\n',fname);
% [pmax, simparammax, mparam, srmax]=loadsim('simfile',fname, 'simvar','simmax');
% [prand, simparamrand, mparam, srrand]=loadsim('simfile',fname, 'simvar','simrand');
 
 

fnspikes.hf=figure;
fnspikes.xlabel='Trial';
fnspikes.ylabel='n spikes';
fnspikes.hp=[];
fnspikes.lbls={};
fnspikes.name='Spike counts';
hold on;
pind=0;

for pind=1:length(vnames)
     [pdata, simparam, mparam, sr]=loadsim('simfile',fname, 'simvar',vnames{pind});
fnspikes.hp(pind)=plot(sr.nspikes,getptype(pind));
fnspikes.lbls{pind}=simparam.simid;
end
%pind=pind+1;
%fnspikes.hp(pind)=plot(srrand.nspikes,getptype(pind));
%fnspikes.lbls{pind}='I.I.D';

lblgraph(fnspikes);


