%$Revision$: 7-12-2007
%   redo gabor figure for book chapter
%   I initially based this on pgaborseries.m 
%   but I then copied all the code from nc06pgaborseries becasue the cdoe
%   for aligning the axes was better in that version
%FOr the case where the true k represents a gabor posterior or some other
%2d shape. We can make an image of the mean after iterations to see how
%good it looks.


%create a structure which contains the relevant fields for different data
%sets we then select which element of the structure we want to select the
%data. if we want to use data in the current workspace set dind=0;
dind=5;

data=[];

%tracking results for nips
%these results are for diffusion
data(end+1).infile=fullfile('..','..','cvs_ece','allresults', 'tracking','06_03','06_03_gabor_001.mat');
data(end).outdir=fullfile('writeup','nips');
data(end).trials=[0 100 500 2500 5000];
data(end).fsuffix=sprintf('');

if dind==0;
    fprintf('\n\n Plotting Data in Work Space \n\n');
    %get directory of current file
    if ~isempty(ropts.fname)
        [outdir,fname] =fileparts(ropts.fname);
        if isfield(ropts,'trialindex')
            trialindex=ropts.trialindex;
        else
            trialindex=str2num(fname(end-2:end));
        end
        trialdate=(fname(1:5));
    else
        outdir=RESULTSDIR;
    end
    %which trials to plot it on.
    %trial of 0 corresponds to prior
    %trials=[200:400:1200 1500];
    trials=[0 100 500:500:3000];
    %trials=[10:20:100];
    trials=[100:200:500];
    %trials=[100 250 300:100:400];
    %trials=[0:20:60];

    ntrials=length(trials);
    %color scaling for the images
    clim=[min(mparam.ktrue(:)) max(mparam.ktrue(:))];
    fsuffix=sprintf('_%s_%.3i',trialdate,trialindex);
else
    load(data(dind).infile);
    fprintf('\n\n Plotting Data in file: \n %s \n\n',data(dind).infile);
    trials=data(dind).trials;
    ntrials=length(trials);
    fsuffix=data(dind).fsuffix;
    outdir=data(dind).outdir;
    clim=[min(mparam.ktrue(:)) max(mparam.ktrue(:))];
end
%clim=[0 .5];


%if the number of trials exceeds the number of iterations then truncate it
ind=find((trials+1)>(simparam.niter+1));
if ~isempty(ind)
    trials=trials(1:ind(1)-1);
    ntrials=length(trials);
end

%methods structure is an array of structures
% each element describes a structure in which results are stored
% .rvar - name of the structure storing the results for the simulation
% .simvar-  name of variable containing the simulation parameters
% i.e methods(1).pname='pmax'
%     means workspace should contain a structure called pmax
%
methods=[];
methods(1).rvar='pmax';
methods(1).simvar='simparammax';
methods(1).srvar='srmax';

methods(1).label='info. max.';

methods(2).rvar='prand';
methods(2).simvar='simparamrand';
methods(2).srvar='srrand';
methods(2).label='random';


%construct a list of enabled methods
monf={};
mnames=fieldnames(pmax);
for index=1:length(fieldnames(pmax))
    if pmax.(mnames{index}).on >0
        monf{length(monf)+1}=mnames{index};
    end
end

%************************************************************************
%image parameters
%**************************************************************************
if isfield(mparam,'gwidth')
    gwidth=mparam.gwidth;
    gheight=mparam.gheight;
else
    gwidth=mparam.klength^.5;
    gheight=mparam.klength^.5;
end

%**************************************************************************
%obtain the data
%**************************************************************************
%we collect the data into an array of structures.
%each element of the array contains the data for the posterior on multiple
%trials it has following fields
%   .trials - a list of trials specifying the trial on which following
%               was collected
%   .m      - each col is mean after corresponding trial in trials
%   .var  - i,j th element is the varance of element i of theta on trial j
%   .lbl     -lbl for this trace
%   .ktrue - true parameters
%   .hp     - handle to plots
%number of data series = elements of pdata
nseries=0;

for index=1:length(methods)
    %extract the results parameters and simulation parameters
    eval(sprintf('presults=%s;',methods(index).rvar));
    eval(sprintf('sparam=%s;',methods(index).simvar));
    eval(sprintf('sr=%s;',methods(index).srvar));
    nseries=nseries+1;
    pdata(nseries).lbl=methods(index).label;

    pdata(nseries).m=zeros(size(presults.newton1d.m,1),length(trials));
    pdata(nseries).var=zeros(size(presults.newton1d.m,1),length(trials));

    pdata(nseries).mmat=cell(1,ntrials)
    for tindex=1:length(trials);

        %add 1 b\c a trial of 0 is prior
        %trial is the index into the array presults
        trial=trials(tindex)+1;

        pdata(nseries).trial(tindex)=trials(tindex);
        %add 1 so that trial of 0 corresponds to prior
        pdata(nseries).m(:,tindex)=presults.newton1d.m(:,trial);
        if ~isempty(presults.newton1d.c{trial})
            pdata(nseries).var(:,tindex)=diag(presults.newton1d.c{trial});
        end
        pdata(nseries).hp=zeros(1,ntrials);

        %reshape the matrix
        pdata(nseries).mmat{tindex}=reshape(pdata(nseries).m(:,tindex), [gheight gwidth]);

        %we subtract 1 from the trial before getting the stimulus
        %set the clim information
        if (trial-1)==0
            %there is no stimulus for the prior so just set it to zeros
            pdata(nseries).stim{tindex}=zeros(gheight,gwidth);
            pdata(nseries).sclimmin=[0];
            pdata(nseries).sclimmax=[0];
        else
            pdata(nseries).stim{tindex}=reshape(sr.newton1d.y(:,trial-1), [gheight gwidth]);
            pdata(nseries).sclimmin=[min(sr.newton1d.y(:,trial-1))];
            pdata(nseries).sclimmax=[max(sr.newton1d.y(:,trial-1))];
        end
    end
    %compute the squared error of each term. So if we were to sum across
    %rows and take the square root we would get the MSE.
    %   pdata(nseries).mse=(pdata(nseries).m-mparam.ktrue *ones(1,length(trials))).^2;
end

%%
%***************************************************
%parameters for the graph
%***************************************************
gparam.trials=[0 100 500 2500 5000];    %trials on which to plot the means
hold on;

%how columns and rows of axes we will need
gparam.ncols=length(gparam.trials)+1;
gparam.nrows=length(methods);
gparam.fontsize=12;
gparam.width=4.5;          %width of the image in inches
gparam.height=1.25;            %height of the image in inches
gparam.clim=[-1 1];       %limits for color mapping

%an inline function which automatically computes the linear index 
%of a subplot from its row and column
%subplot selects in row order so to use  sub2ind we transpose rows and
%columns
spind=@(row,col)(sub2ind([gparam.ncols gparam.nrows],col,row));


%%
%make the figure
fgabor=[];
fgabor.hf=figure;
fgabor.a={};
fgabor.name='Gabor MAPs';
fgabor.FontSize=10;         %force all fonts to this size
colormap(gray);

%info about the data sets
dsets(1).lbl=sprintf('  info.\n  max.');
dsets(2).lbl='random';
%**************************************************************************
%Make plots of mean
%************************************************************************
%loop over the data sets
for dind=1:length(methods)
%    [pdata, sim, mp, sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
    for tind=1:length(gparam.trials)
        trial=gparam.trials(tind);
        %convert indexes to a linear index
        aind=spind(dind,tind);
        fgabor.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);
        
        %check mean exists
           
        
        imagesc(pdata(dind).mmat{tind},gparam.clim);
      
        %turn off tick marks
        set(fgabor.a{aind}.ha,'xtick',[]);
        set(fgabor.a{aind}.ha,'ytick',[]);
    end %end loop over trials
    
    %plot the true parameter
 
    aind=spind(dind,gparam.ncols);
    %subplot goes rows first
    %so to use sub2ind transpose rows and colums
    fgabor.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,spind(dind,gparam.ncols));
     %imagesc(reshape(theta,sim.extra.mparam.gheight,sim.extra.mparam.gwidth),gparam.clim);     
     imagesc(reshape(mparam.ktrue(:,1),[mparam.gheight,mparam.gwidth]),gparam.clim);

     set(fgabor.a{aind}.ha,'xtick',[]);
     set(fgabor.a{aind}.ha,'ytick',[]);
     
     hc(dind)=colorbar;
end %end loop over datasets

%**************************************************************************
%Set axes labels and titles
%*************************************************************************
%add a title to each graph in the top row
for col=1:(gparam.ncols-1)
    aind=spind(1,col);
    fgabor.a{aind}.title=sprintf('trial %d',gparam.trials(col));
end
aind=spind(1,gparam.ncols);
fgabor.a{aind}.title='\theta true';

%add data set label to each row
for row=1:gparam.nrows
    aind=spind(row,1);
    fgabor.a{aind}.ylabel=dsets(row).lbl;
end

%label the graphs
fgabor=lblgraph(fgabor);
%make first ylable latex
%set(fgabor.a{1}.hylabel,'interpreter','latex');
%**************************************************************************
%Position the figure
%**************************************************************************
%space a structure to describe spacing settings
%this should be expressed as %
space.hspace=.035; %horizontal spacing
space.vspace=.1; %vertical spacing
space.cbwidth=.025;  %width of colorbar

%set the height and width of the figure
set(fgabor.hf,'units','inches');
fpos=get(fgabor.hf,'paperposition');
fpos(3)=gparam.width;
fpos(4)=gparam.height;
set(fgabor.hf,'paperposition',fpos);    %controls the position when we printout
%control its size on screen as well
fpos=get(fgabor.hf,'position');
fpos(3)=gparam.width;
fpos(4)=gparam.height;
set(fgabor.hf,'position',fpos);  

%*********************************************************************
%determine how much of a border we need to leave on left bottom right top
%so that we don't cut off figure labels
ha=length(fgabor.a);
for i=1:length(fgabor.a)
    ha(i)=fgabor.a{i}.ha;
end
ha=[ha hc];

%ha has handles to all the axes
tinset=get(ha,'tightinset');
tinset=cell2mat(tinset);

border.left=max(tinset(:,1));
border.bottom=max(tinset(:,2));
border.right=max(tinset(:,3));
border.top=max(tinset(:,4));

%determine the size for each of the axes
asize.width=(1-border.left-border.right-space.hspace*gparam.ncols-space.cbwidth)/(gparam.ncols);
asize.height=(1-border.top-border.bottom-space.vspace*(gparam.nrows-1))/(gparam.nrows);

%loop through the axes and poisition them
for row=1:gparam.nrows
    for col=1:gparam.ncols
        aind=spind(row,col);
        pos=get(fgabor.a{aind}.ha,'position');
        pos(1)=border.left+(col-1)*(asize.width+space.hspace);
        pos(2)=border.bottom+(gparam.nrows-row)*(asize.height+space.vspace);
        pos(3)=asize.width;
        pos(4)=asize.height;
        set(fgabor.a{aind}.ha,'position',pos);
    end
end

%size the color bars
for j=1:length(hc)
    pos=get(hc(j),'position');
    pos(1)=border.left+(gparam.ncols)*(asize.width+space.hspace);
    pos(2)=border.bottom+(gparam.nrows-j)*(asize.height+space.vspace);
    pos(3)=space.cbwidth;
    pos(4)=asize.height;
    set(hc(j),'position',pos);
end
refresh;

ppos=get(fgabor.hf,'paperposition');
ppos(1)=0;
ppos(2)=0;
set(fgabor.hf,'paperposition',ppos);
fgabor.outfile='writeup/book_chapter/gabormaps.eps';
%saveas(fgabor.hf,fgabor.outfile,'epsc2');
%exportfig(fgabor.hf,fgabor.outfile,'bounds','tight','color','rgb');