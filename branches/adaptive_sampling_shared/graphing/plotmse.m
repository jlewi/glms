%***********************************************************************
%plot the mean squared distance between the estimated parameters
%and the true theta 
%*************************************************************************
% can be called as function or script
function  [fmse]=plotmse(varargin)

%*****************************************************************
%default values
%****************************************************************
setpaths;
fname=fullfile(RESULTSDIR,'logistic','01_28','01_28_numlog_001.mat');
normalize=0; %if set to 1 we normalize the magintude of the estiamtes to be 1
                        %as well as the true parameters. This means when we
                        %measure MSE we include any error due to a scaling
                        %factor
rootnfactor=0  % multiple the mse by a factor of root n
%*******************************************************************
%override variables
for j=1:2:nargin
    switch varargin{j}
        case {'fname','filename','file','simfile'}
            fname=varargin{j+1};
      otherwise
            if exist(varargin{j},'var')
                eval(sprintf('%s=varargin{j+1};',varargin{j}));
            else
                fprintf('%s unrecognized parameter. \n',varargin{j});
            end
    end
end

fprintf('\n***************************\n MSE for file: %s \n***********************\n',fname);
 [pmax, simparammax, mparam, srmax]=loadsim('simfile',fname, 'simvar','simmax');
 [prand, simparamrand, mparam, srrand]=loadsim('simfile',fname, 'simvar','simrand');
 
 %3-18-07
 %new object model ktrue is field of the simulator
 if (simparammax.simver>=070318)
     ktrue=simparammax.observer.theta;
 else
     %backwards compatibility
     ktrue=mparam.ktrue;
 end
 
 if (normalize==1)
     mmag=sum(mparam.ktrue.^2,1);
     mmag=mmag^.5;
     ktrue=ktrue/mmag;
     
     mmag=sum(pmax.m.^2,1);
     mmag=1./(mmag.^.5);
     pmax.m=(ones(mparam.klength,1)*mmag).*pmax.m;
     
          mmag=sum(prand.m.^2,1);
     mmag=1./(mmag.^.5);
     prand.m=(ones(mparam.klength,1)*mmag).*prand.m;
 end
fmse.hf=figure;
fmse.xlabel='Trial';
if (normalize==0)
fmse.ylabel='M.S.E';
else
    fmse.ylabel=sprintf('M.S.E of \n normalized estimates');
end
if (rootnfactor==1)
    fmse.ylabel=sprintf('$n^{.5}||\\mu-\\theta||_2$');
end
fmse.name='Mean Squared Error';
hold on;
pind=0;
   
 
    pind=pind+1;
niter=simparammax.niter;
nmse=pmax.m-ktrue*ones(1,1+niter);
nmse=nmse.^2;
nmse=sum(nmse,1).^.5;
if (rootnfactor==1)
   nmse=(1:length(nmse)).^.5 .*nmse;
end
fmse.hp(1)=plot(0:niter,nmse,getptype(pind,2));
fmse.lbls{1}='Info. max.';

pind=pind+1;
niter=simparamrand.niter;
amse=prand.m-ktrue*ones(1,1+niter);
amse=amse.^2;
amse=sum(amse,1).^.5;

if (rootnfactor==1)
    amse=(1:length(amse)).^.5 .*amse;
end
fmse.hp(pind)=plot(0:niter,amse,getptype(pind,2));
fmse.lbls{pind}='random';

fmse=lblgraph(fmse);
set(fmse.hylabel,'interpreter','latex');