%$Revision$ 07-12-2007
%       I needed to redo the timing figure for the book chapter. So I went
%       back to revision 646 of timingfig.m and created this copy whichI
%       then modified.
%4_17_2006
%
% This script plots the running time for a variety of different sized
% unkown parameters
% it averages the time per iteration because the time per iteration should
% be constant
%
% an earlier version of this script was used to make the timing figure for
% the embs abstract.
%directory for files
tdir=fullfile('..','..','cvs_ece','allresults','tracking');
%data files used for embs abstract
%tfiles={'04_17/04_17_maxtrack_001.mat','04_17/04_17_maxtrack_002.mat','04_17/04_17_maxtrack_004.mat'};
%tfiles{end+1}='04_17/04_17_maxtrack_006.mat';
%tfiles{end+1}='04_17/04_17_maxtrack_007.mat';
%tfiles{end+1}='04_17/04_17_maxtrack_008.mat';
%tfiles{end+1}='04_17_maxtrack_009.mat';
%tfiles{end+1}='04_18/04_18_maxtrack_002.mat'
tfiles={};
%tfiles{end+1}='05_21/05_21_maxtrack_001.mat';
%tfiles{end+1}='05_21/05_21_maxtrack_002.mat';

%set 2
%tfiles{end+1}='05_22/05_22_maxtrack_001.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_002.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_003.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_004.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_005.mat';
%
%tfiles{end+1}='05_22/05_22_maxtrack_006.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_007.mat';
%
%set 3 for nips
tfiles{end+1}='05_30/05_30_maxtrack_004.mat';
tfiles{end+1}='05_30/05_30_maxtrack_005.mat';
tfiles{end+1}='05_30/05_30_maxtrack_006.mat';
tfiles{end+1}='05_30/05_30_maxtrack_007.mat';
tfiles{end+1}='05_30/05_30_maxtrack_008.mat';
tfiles{end+1}='05_30/05_30_maxtrack_009.mat';
tfiles{end+1}='05_30/05_30_maxtrack_010.mat';
tfiles{end+1}='06_01/06_01_maxtrack_001.mat';
%tfiles{end+1}='06_03/06_03_gabor_001.mat';
outfile=fullfile('writeup','book_chapter','simtiming.eps');

%outfile=fullfile(RESULTSDIR,'rank1eig','simtiming.eps');



%start trial
%specfies how many trials at the beginning to throw out
tstart=200;

%time for each step
nfiles=length(tfiles);
d=zeros(1,nfiles);              %dimensionality of the data for that trial
eigtime=zeros(1,nfiles);
eigtimestd=zeros(1,nfiles);
searchtime=zeros(1,nfiles);
searchtimestd=zeros(1,nfiles);
updatetime=zeros(1,nfiles);
updatetimestd=zeros(1,nfiles);

data={};

for findex=1:length(tfiles);
    %load the data
    data{findex}=load(fullfile(tdir,tfiles{findex}));
    
    d(findex)=data{findex}.mparam.klength;
    
    eigtime(findex)=mean(data{findex}.pmax.newton1d.timing.eigtime(tstart:end));
    eigtimestd(findex)=std(data{findex}.pmax.newton1d.timing.eigtime(tstart:end));
    
        searchtime(findex)=mean(data{findex}.pmax.newton1d.timing.searchtime(tstart:end));
            searchtimestd(findex)=std(data{findex}.pmax.newton1d.timing.searchtime(tstart:end));
            
            updatetime(findex)=mean(data{findex}.pmax.newton1d.timing.update(tstart:end));
            updatetimestd(findex)=std(data{findex}.pmax.newton1d.timing.update(tstart:end));

            totaltime(findex)=mean(data{findex}.pmax.newton1d.timing.update(tstart:end)+data{findex}.pmax.newton1d.timing.eigtime(tstart:end)+data{findex}.pmax.newton1d.timing.searchtime(tstart:end));
            totaltimestd(findex)=std(data{findex}.pmax.newton1d.timing.update(tstart:end)+data{findex}.pmax.newton1d.timing.eigtime(tstart:end)+data{findex}.pmax.newton1d.timing.searchtime(tstart:end));
            %clear data or we'll run out of memory
            clear data;
            
end

%sort the data
[d index]=sort(d);
eigtime=eigtime(index);
updatetime=updatetime(index);
searchtime=searchtime(index);
totaltime=totaltime(index);

eigtimestd=eigtimestd(index);
searchtimestd=searchtimestd(index);
updatetimestd=updatetimestd(index);
totaltimestd=totaltimestd(index);
%%
%*******************************************************************
%Plot it

%ftiming.hf=figure;
ftiming=[];
ftiming.hp=[];
ftiming.x=[];
ftiming.xlabel='Dimensionality';
ftiming.ylabel='Time(Seconds)';
ftiming.title='';
ftiming.name='Timing';
ftiming.on=1;
ftiming.fname='timing';
ftiming.semilog=1;
ftiming.fontsize=12;
ftiming.linewidth=2;
ftiming.semilogx=0;
ftiming.width=1.9;
ftiming.height=1.9;
ftiming.hf=figure;
ftiming.markersize=18;
figure(ftiming.hf);
hold on;

%index of first pt to use for fitting the data
istart=find(d>=200);
istart=istart(1);

%********************************************************
%total time
%*****************************************************
pind=1;
ftiming.hp(pind)=plot(d,totaltime,'k.');
set(ftiming.hp(pind),'markerfacecolor','k');


%fit total time to to a 2nd degree
%polynomial eventhough it really is O(n^3)
%do this to illustrate the speedp do to the rank 1 step
%[p,s] = POLYFIT(d,eigtime,3);
%x=[50:d(end)];
%y=p(1)*x.^3+p(2)*x.^2+p(3)*x + p(4);
[p,s]=polyfit(d(istart:end),totaltime(istart:end),2);
x=[50:d(end)];
y=p(1)*x.^2+p(2)*x+p(3);
fprintf('Total time fitted to 2nd degree polynomial\n');
fprintf('p(1)=%0.3g \t p(2)=%0.3g \t p(3)=%0.3g \n',p(1),p(2),p(3)); 
ftiming.hpline(pind)=plot(x,y,'k-');
%ftiming.lbls{pind}=sprintf('total time');
set(ftiming.hpline(pind),'LineWidth',3);





lblgraph(ftiming);


figure(ftiming.hf);
%set the ticklabels otherwise if its a log graph it only prints the
%exponent

%ylimits=[updatetime(1) max(totaltime(end))];
ylimits=[.01 .5];

ylim(ylimits);
ytick=get(gca,'YTick');
ytick=[.01 .1 .5];
set(gca,'ytick',ytick);
%yticklbl=['10^-4', '10^-2', '10^-1'];
yticklbl=ytick;
set(gca,'YTickLabel',yticklbl);
xlim([0 600]);
set(gca,'xtick',[0 200 400 600]);
%savegraph('entropy.eps',fentopy.hf)
%exportfig(ftiming.hf,outfile,'bounds','tight','Color','bw');
%exportfig(ftiming.hf,outfile,'bounds','tight','Color','rgb');


