%10-19-06
%Compute and plot the number of errors computed using various estimates
%
%Instructions:
%   1. load the appropriate data into the workspace
%testing datasets consists of a cell array of inputs and outputs
%we compute the number of errors for each of these sets
testsets={};
tind=length(testsets)+1;
testsets(1).stim=srmax.newton1d.y;
testsets(1).obsrv=srmax.newton1d.nspikes;
testsets(1).lbl='Max';

testsets(2).stim=srrand.newton1d.y;
testsets(2).obsrv=srrand.newton1d.nspikes;
testsets(2).lbl='Random';

testsets={};
tind=length(testsets)+1;
testsets(tind).stim=10*rand(mparam.klength,1000)-5;
testsets(tind).obsrv=binornd(1,logisticcanon(mparam.ktrue'*testsets(tind).stim+mparam.atrue));
testsets(tind).lbl='Test';

%lin filters to compute
filters(1).k=pmax.newton1d.m(1:mparam.klength,:);
filters(1).lbl='Info. Max';

filters(2).k=prand.newton1d.m(1:mparam.klength,:);
filters(2).lbl='Random';

filters(3).k=mparam.ktrue*ones(1,simparam.niter);
filters(3).lbl='True';


errorfrac=@(filt,stim,obsrv)(sum(abs(round(1./(1+exp(-[filt;1]'*[stim;ones(1,length(obsrv))])))-obsrv))/length(obsrv));

ferrors.hf=figure;
ferrors.xlabel='Trial';
ferrors.ylabel='Error Fraction';
ferrors.hp=[];
ferrors.lbls={};

fangle.hf=figure;
fangle.xlabel='Trial';
fangle.ylabel='Angle';
fangle.hp=[];
fangle.lbls={};

hold on;
pind=1;
apind=1;
%for each data set
for tind=1:length(testsets)
    %for each filter
    figure(ferrors.hf);
    hold on;
    for find=1:length(filters)
        %compute the number of errors
        testsets(tind).filters(find).errorfrac=zeros(1,size(filters(find).k,2))
        for index=1:size(filters(find).k,2)
                testsets(tind).filters(find).errorfrac(index)=errorfrac(filters(find).k(:,index),testsets(tind).stim,testsets(tind).obsrv);
        end
        ferrors.hp(pind)=plot(1:length(testsets(tind).filters(find).errorfrac), testsets(tind).filters(find).errorfrac,getptype(pind,'1'));
plot(1:length(testsets(tind).filters(find).errorfrac), testsets(tind).filters(find).errorfrac,getptype(pind,'2'));
      ferrors.lbls{pind}=sprintf('T:%s F:%s',testsets(tind).lbl,filters(find).lbl);
      pind=pind+1;
      
    end
    
    %if its 2d data plot the angle of each stimulus
    figure(fangle.hf);
    hold on;
    if (size(testsets(tind).stim,1)==2)
                %compute the angles
                angles=180/pi*atan2(testsets(tind).stim(2,:),testsets(tind).stim(1,:));
               fangle.hp(apind)=plot(1:length(angles),angles,getptype(apind,'1'));
                plot(1:length(angles), angles,getptype(apind,'2'));
              fangle.lbls{apind}=sprintf('%s',testsets(tind).lbl);
      apind=apind+1;
    end
end

lblgraph(ferrors);
lblgraph(fangle);

return;
%***********************
%extra plots
%plot the projection along the decision boundary for infomax
fbound.hf=figure;
fbound.hp=[];
fbound.lbls={};
hold on;
fbound.xlabel='Trial';
fbound.ylabel='Position on Boundary';
pind=1;
fbound.hp(pind)=plot(1:simparam.niter,[mparam.ktrue;mparam.atrue]'*[srmax.newton1d.y;ones(1,simparam.niter)],getptype(pind,1));
plot(1:simparam.niter,[mparam.ktrue;mparam.atrue]'*[srmax.newton1d.y;ones(1,simparam.niter)],getptype(pind,2));
fbound.lbls{pind}='Info. Max.';
pind=pind+1;
fbound.hp(pind)=plot(1:simparam.niter,[mparam.ktrue;mparam.atrue]'*[srrand.newton1d.y;ones(1,simparam.niter)],getptype(pind,1));
plot(1:simparam.niter,[mparam.ktrue;mparam.atrue]'*[srrand.newton1d.y;ones(1,simparam.niter)],getptype(pind,2));
fbound.lbls{pind}='Random';
lblgraph(fbound);
