%Return value
%   fpoly - handle to figure showing region as a polygon
%   freg  - handle to just the border of the region
%   data  - structure of the actual data
%
%1-09-07
%   modified it so can be called as function or script
%10-07-06
%
%plotquadreg('name','value')
%       post -
%                   .m - mean
%                   .c  - covariance matrix
%       mmag - magnitude constraint
%       klength
%       alength
function [fpoly,freg,data]=plotquadreg(varargin)
optim=optimset('MaxIter',1000,'TolFun',10^-5, 'TolX',10^-3,'Display','off');
%if no input arguments load from file
if (nargin==0)
data={}
%Make a plot of the 2-d feasible region on multiple iterations
%data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/tracking/10_07/10_07_maxtrack_001.mat');
%data(end).outfile=fullfile(RESULTSDIR,'tracking/10_07/intreg_001.eps');
%data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/glmgen/10_08/10_08_maxtrack_003.mat');
fdata(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/glmgen/10_12/10_12_maxtrack_001.mat');
fdata(end).outfile=fullfile(RESULTSDIR,'glmgen/10_12/quadreg_001.eps');
fdata(end).trials=[1];
fdata(end).fsuffix='_intreg';
dind=length(fdata);
load(fdata(dind).infile);

    error('need to rewrite this section of the code becaue I didnt do when I revised on 1-09-07');
    %load the data into a structure
    %1 field per trial
%    for tind=1:length(fdata(dind).
  %         trial=fdata(dind).trials(tind);
  %  trials=fdata(end).trials;
    
    m=pmax.newton1d.m(:,trial+1);
    % post.c=pmax.newton1d.c{trial+1}(1:mparam.klength,1:mparam.klength);
    post.c=pmax.newton1d.c{trial+1};
    srdata=srmax.newton1d;
    maxmu=mparam.mmag*(post.m'*post.m)^.5;
    [eigck.evecs eigck.eigd]=svd(post.c(1:mparam.klength,1:mparam.klength));
    eigck.eigd=diag(eigck.eigd);
    mu=pmax.newton1d.m(:,trial+1);

else
    trials=1;
    vind=1;
    data=[];
    while (vind<nargin)
        data.(varargin{vind})=varargin{vind+1};
        vind=vind+2;
    end
end
freg.hf=figure;
freg.xlabel='\mu_{\epsilon}';
freg.ylabel='\sigma';
freg.hp=[];
freg.lbls={};
freg.linewidth=1;
freg.name='Integration Region';
datetime=clock;
%freg.fname=data(dind).outfile;
freg.linewidth=6;
hold on;

saveout=0;
npts=1000;

%range of values for magnitude in direction of stimulus
alpharange=linspace(-data(1).mmag,data(1).mmag,npts);

%npts=2;
%alpharange=[.222 .23];
qmax=zeros(length(trials),npts);
qmin=zeros(length(trials),npts);
for tind=1:length(trials)
    trial=trials(tind);
    post=data(tind).post
    mmag=data(tind).mmag;
    % post.c=pmax.newton1d.c{trial+1}(1:mparam.klength,1:mparam.klength);
    %post.c=data(tind).m;
    %srdata=srmax.newton1d;
    maxmu=mmag*(post.m'*post.m)^.5;
    [eigck.evecs eigck.eigd]=svd(post.c(1:data(tind).klength,1:data(tind).klength));
    eigck.eigd=diag(eigck.eigd);
%    mu=pmax.newton1d.m(:,trial+1);

    %**********************************************************************
    %obtain the values needed to compute the feasible region
    %**********************************************
    %unit vector in direction of mean of just stimulus components
    %need its magnitude as well
    muk=post.m(1:data(tind).klength,1);
    mukmag=(muk'*muk)^.5;
    muk=muk/mukmag;

    %*****************************************************************
    %compute the spike history
    %******************************************************************
    %spike history is just the previous responses
    %we take them to be zero if they happened before the start of the
    %experiment
    %tr-2 is actual number of observations we have already made
    nobsrv=trial-2;    %how many observations have we made
    shist=[];
    %only compute shist if we have spike history terms
    if (data(tind).alength >0)
          shist=zeros(mparam.alength,1);
        if (mparam.alength <= (nobsrv))
            shist(:,1)=srdata.nspikes(nobsrv:-1:nobsrv-mparam.alength+1);
        else
            shist(1:nobsrv,1)=srdata.nspikes(nobsrv:-1:1);
        end
    end

    %projection onto mean of stimulus
    if isempty(shist)
        %no spike history terms
        muprojr=0;
    else
        muprojr=shist'*post.m(mparam.klength+1:end,1);
    end
    %*************************************************************************
    %Define the quadratic form
    %05-28-07: switch to using object
    %[quad]=quadmodshist(post.c,eigck,muk,shist);
    quad=QuadModObj('C',post.c,'eigck',eigck,'muk',muk,'shist',shist)
    for j=1:length(alpharange);
        [qmax(tind,j),qmin(tind,j),xmax,xmin]=maxminquad(quad,alpharange(j),data(tind).mmag,optim);
    end


    pind=length(freg.hp)+1;
    [c,m]=getptype(tind,1);
    freg.hp(pind)=plot([alpharange],[qmin(tind,:)],getptype(tind,2));
    set(freg.hp(pind),'linestyle', '.')
    hp=plot([alpharange(end:-1:1)],[qmax(tind,end:-1:1)],getptype(tind,1));
%    set(hp,'linestyle', '-')
  %  set(hp,'linewidth',freg.linewidth);

    %    plot([alpharange],[qmin(tind,:)],getptype(tind,2));
    freg.lbls{pind}=sprintf('Trial %d',trial);

    %h=fill([alpharange alpharange(end:-1:1)],[qmax(tind,:), qmin(tind,end:-1:1)],c);


        data.alpha=alpharange;
        data.qmax=qmax;
        data.qmin=qmin;
end
freg=lblgraph(freg);


fpoly.hf=figure;
fpoly.xlabel='Projection along unit vector in direction \mu';
fpoly.ylabel='\sigma^2';
fpoly.name='Integration Region';
%fpoly.fname=data(dind).outfile;
fpoly.axisfontsize=20;
hold on;
h=fill([alpharange alpharange(end:-1:1)],[qmax(tind,:), qmin(tind,end:-1:1)],[0 .8 1]);
hp(1)=plot([alpharange],qmax(tind,:),'k');
set(hp(1),'linewidth',5);
set(hp(1),'color',[0 0 0])
hp(2)=plot([alpharange],qmin(tind,:));
set(hp(2),'linewidth',5);
set(hp(2),'color',[.75 .75 .75]);



%draw lines to represent axes
xl=xlim;
yl=ylim;
hl(1)=line([0;0],yl')
set(hl(1),'linewidth',.05);
set(hl(1),'color','k');
%hl(2)=line(xl',[0;0]);
%set(hl(2),'linewidth',1);
%set(hl(2),'color','k');

%get rid of axes lines
set(gca,'LineWidth',0.0000001);
set(gca,'LineWidth',.05);
%set(gca,'YColor',[1 1 1]);
set(gca,'xColor',[0 0 0]);

lblgraph(fpoly);
%set(gca,'xtick',[]);
%set(gca,'ytick',[]);

return
%******************************************************************
%old code
%**********************************************************************
  %**************************************************************************
    %now plot the quadratic region by varying lambda instead
    %**********************************************************************
    %determine if there are any values of pcon for which max occurs with
    %lambda=emax
    [pconrmax]=pconsingmax(quad,mparam.mmag);
    npcpts=100;
    nlpts=100;
    [emax]=max(quad.eigd);

    if ~isempty(pconrmax)
        if (length(pconrmax)>1)
        %lam>=emax
        %compute the max at lam=emax
        pconpts=linspace(pconrmax(1),pconrmax(2));
        pqmax=zeros(1,npcpts);

        for ind=1:npcpts
            pqmax(ind)=sigmamaxsing(pconpts(ind), quad,mparam.mmag);
        end
        else
            pconpts=pconrmax;
            pqmax=sigmamaxsing(pconpts(1), quad,mparam.mmag);
        end
    else
        pconpts=[];
        pqmax=[];
    end

    %instead of searching over the unbounded range emax<lam1<infty
    %we use our rescaling
    lr=linspace(0,.01,nlpts);
    lr=10.^linspace(-8,-5,nlpts);
    %get rid of the first and lambda pt because we don't to evaluate it at the
    %singularity
    lr=lr(2:end-1);
    pclr=[];
    qmaxlr=[];

    for ind=1:length(lr)
        [smax,pcon,xmax,nsols]= sigmamax(lr(ind), quad,muk,mparam.mmag);
        for sind=1:nsols
            qmaxlr(end+1)=smax(sind);
            pclr(end+1)=pcon(sind);
        end
    end

    %now we concatenate the results of the two together
    qmaxlam(tind).pcon=[pconpts pclr];
    qmaxlam(tind).qmax=[pqmax qmaxlr];
    
    hold on;
    plot(qmaxlam(tind).pcon,qmaxlam(tind).qmax,'r');