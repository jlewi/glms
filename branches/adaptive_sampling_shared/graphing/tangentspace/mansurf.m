%make a 3d plot of the manifold, theta, and projection of theta onto
%manifold

%fontsize
fsize=18;

%********************************************************************
%Plot the manifold
%********************************************************************
%manifold is a sinewave in direction sdir
sdir=normmag([1;1]);

%generate the x, y points
freq=1;

pts=[-1:.05:1];
[x,y]=meshgrid(pts,pts);

%compute the projection on sidr
proj=x*sdir(1)+sdir(2)*y;
z=sin(2*pi*freq*proj);

%find those points with proj >.5 or <.5 and set to nan so they don't get
%plotted
ind=find(abs(proj)>.5);
z(ind)=nan;


fman=FigObj('name','Manifold','width',4.5,'height',4.5);
fman.a=axesobj('nrows',1,'ncols',1);
hold on;

hs=surf(x,y,z);

zlim([-2 2])

%set the view point
set(gca,'view',[14 18]);

%**************************************************************************
%plot the mean
%**************************************************************************
pmap=normmag([1;1])*.25;
zpmap=sin(2*pi*freq*pmap'*sdir)+1;
hpmap=plot3(pmap(1),pmap(2),zpmap,'O','MarkerSize',12,'MarkerFaceColor','k');
%add some text
hmaptxt=text(pmap(1)+.1,pmap(2),zpmap,'$\vec{\mu}_{t+1}$','interpreter','latex','FontSize',18);

%plot the projection onto the manifold.
zpproj=sin(2*pi*freq*pmap'*sdir);

hpproj=plot3(pmap(1),pmap(2),zpproj,'O','MarkerSize',12,'MarkerFaceColor','k');
%add some text
hprojtxt=text(pmap(1)+.05,pmap(2),zpproj+.7,'$P_M\vec{\mu}_{t+1}$','interpreter','latex','FontSize',18);

%draw an arrow
harrow=arrow([pmap;zpmap],[pmap; zpproj],'LineStyle',':');


%basis of the tangent space
basis=orth([[1;0; cos(2*pi*freq*pmap'*sdir)*sdir(1)] [0;1; cos(2*pi*freq*pmap'*sdir)*sdir(2)] ]);

%how far along the basis vector to go
maxb=.75;
db=.01;
phi=-maxb:db:maxb;
%define the pts bpts in a rectangle in the tangent space centered at the
%projeciton of mut
bx=zeros(length(phi),length(phi));
by=zeros(length(phi),length(phi));
%take linear combinations of the basis vectors
for b1ind=1:length(phi)
    for b2ind=1:length(phi)
        bx(b2ind,b1ind)=phi(b1ind)*basis(1,1)+phi(b2ind)*basis(1,2)+pmap(1);
        by(b2ind,b1ind)=phi(b1ind)*basis(2,1)+phi(b2ind)*basis(2,2)+pmap(2);
    end
end

%compute a Gaussian on these points
bprob=normpdf(((bx-pmap(1)).^2+(by-pmap(2)).^2).^.5, 0,1);

%for visualization make the region on which we plot bprob have changes
%parallel and anti paralle to sdir
%code below ends up causing the surface to be obscured 
thresh=.5;
proj=(bx-pmap(1))*sdir(1)+(by-pmap(2))*sdir(2);
ind=find(abs(proj)>thresh);
bprob(ind)=nan;

proj=(bx-pmap(1))*sdir(2)-(by-pmap(2))*sdir(1);
ind=find(abs(proj)>thresh);
bprob(ind)=nan;

%scale bprob so it uses the full colorange
%so we need to scale it to -1 1
 bprob=bprob-min(bprob(:));
bprob=bprob/(max(bprob(:)))*2-1;

%colormap(hot);
hm=mesh(bx,by,zpproj*ones(length(phi),length(phi)),bprob,'facecolor','flat');
set(hm,'linestyle','none');

set(gca,'xtick',[]);
set(gca,'ytick',[]);
set(gca,'ztick',[]);

%draw some arrows for the axes
lw=4;
o=[-1;-1;-1]; %origin
da=1;
arrow(o,o+da*[.5;0;0],'LineWidth',lw);
arrow(o,o+da*[0;1;0],'LineWidth',lw);
arrow(o,o+da*[0;0;1],'LineWidth',lw);
xlim([-1 1]);
ylim([-1 1]);
zlim([-1 2]);

pos=o+da*[.5;0;0];
hx=text(pos(1),pos(2),pos(3),'\theta_1','FontSize',fsize);
pos=o+da*[0;1;0];
hy=text(pos(1),pos(2),pos(3),'\theta_2','FontSize',fsize);
pos=o+da*[0;0;1];
hz=text(pos(1),pos(2),pos(3),'\theta_3','FontSize',fsize);

%add text for the manifold
hmtxt=text(-1.1,0,0,'M','FontSize',fsize);

axis off;


%we want the sinusoidal surface to be in black and white 
%while the pdf on the tangent space is in color
%So we need to create a new colormap by concatenating the black and weight
%colormap with a color colormap. We then set the clim property of the two
%surfaces so we only uses the rows of the colormaps that correspond to the
%correct colormap.
cmapbw=colormap('gray');
cmapc=colormap('jet');
cboth=[cmapbw;cmapc];
colormap(cboth);


%we set the colorscaling to direct so that CData contains indexes into
%cboth
set(hm,'CDAtaMapping','direct');
%map brob to 0 1
cdata=(bprob-min(bprob(:)));
cdata=cdata/max(cdata(:));
%now map it indexes
cdata=ceil(cdata*(size(cmapc,1)-1))+1+size(cmapbw,1);
set(hm,'CData',cdata);

set(hs,'CDAtaMapping','direct');
%map brob to 0 1
cdata=(z-min(z(:)));
cdata=cdata/max(cdata(:));
%now map it indexes
cdata=ceil(cdata*(size(cmapbw,1)-1))+1;
set(hs,'CData',cdata);



%saveas(gethf(fman),'~/svn_trunk/adaptive_sampling/writeup/cosyne08/tangentspacepost.eps');
% xlabel('\theta_1');
% ylabel('\theta_2');
% zlabel('\theta_3');
