%function plotmumag(fname,simvar)
%   fname - filename
%   simvar - cell array of simulation variables
%
% Explanation: makes aplot of the magnitude of the mean
function fmag=plotmumag(fname,simvar)

fmag.hf=figure;
fmag.xlabel='trial';
fmag.ylabel='||\mu||_2';
fmag.title='Magnitude of the posterior mean';
fmag.name='mumag';
fmag.fontsize=20;
if ~iscell(simvar)
    simvar={simvar};
end

hold on;
for vind=1:length(simvar)
    [pdata, simdata mobj sr]=loadsim('simfile',fname,'simvar',simvar{vind});
    fmag.hp(vind)=plot(sum(pdata.m.^2,1).^.5,getptype(vind));
    fmag.lbl{vind}=simdata.simid;
end

lblgraph(fmag);

