% $Revision$ : created
%
% Make plots showing its the multiplicity which causes speedup of
% eigendecomposition
clear all;
setpathvars;
dsets=[];

%****************************************************
%Parameters
%******************************************************
tmax=5000;
tmax=600;

%each entry describes the dataset for a different a row
dsets(end+1).dfile='gabor_04_24_001.mat';
dsets(end).fname=fullfile(RESULTSDIR,'poissexp','04_24',dsets(end).dfile);
dsets(end).lbl=sprintf('info. max. \n $||\\vec{x}||_2=e$') ;
dsets(end).simvar='simmax';


einfo=[];

dloaded=false;
for dind=1:length(dsets)
    if ~(dloaded)
        [pdata, sim, mparam, sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
        dloaded=true;
    end


        %turn debugging of the update on 
       updater=setdebug(sim.updater,false);
    
    %initalize einfolast to eigendecomposition of the prior
    elast=EigObj('matrix',pdata.c{1});
   
 
    postlast.m=pdata.m(:,1);
    postlast.c=pdata.c{1};
    for (trial=1:min(sim.niter,tmax))
        if (mod(trial,10)==0)
           fprintf('Trial %d \n',trial); 
        end
        stim=sr.y(:,trial);
        obsrv=sr.nspikes(trial);
        %7-23-2007
        %before I was just computing the eigendecomposition not the update
        %this should be fasterbut it looks like there is a bug in the code
        %because eigenvalues were coming out to be negative which doesn't
        %happen if I call the full update. 
        %[gd1, gd2]=d2glmeps(pdata.m(:,trial+1)'*stim,obsrv,mparam.glm);
        %rho=gd2/(1-gd2*stim'*elast.evecs*diag(elast.eigd)*elast.evecs'*stim);
    
        [post, uinfo,eup]=update(updater,postlast,stim,mparam,obsrv,elast);
    
        %         einfo(trial).eigd=eup.eigd;
        eup=EigObj('eigd',eup.eigd,'evecs',eup.evecs);
         einfo(trial).eigtime=uinfo.eigtime;
         %zero out the eigenvectors       
         %compute the number of distinct eigenvectors
         einfo(trial).neigdist=numdistinct(eup);


%         [eup, oinfo]=rankOneEigUpdate(elast,stim,rho);        
        einfo(trial).eigd=eup.eigd;
%        einfo(trial).eigtime=oinfo.eigtime;
%         %zero out the eigenvectors       
%         %compute the number of distinct eigenvectors
        einfo(trial).neigdist=numdistinct(eup);
%         einfo(trial).rho=rho;
         einfo(trial).ndeflate=uinfo.ndeflate;
         
           if any(einfo(trial).eigd<0)
                            error('eigenvalues are negative');
           end
% 
%         if any(einfo(trial).eigd<0)
%             %check if update is valid
%           %  iscorrect=verifyupdate(eup,elast,stim,rho);
%             if (iscorrect)
%                 error('eigenvalues are negative but eigenupdate appears valid');
%             else
%                 error('eigenvales are negative and eigenupdate is incorrect');
%                 
%                 %verify covariance update
%                 postlast.m=pdata.m(:,trial);
%                 postlast.c=pdata.c{trial};                
%                 [post,uinfo,einfo]=update(sim.updater,postlast,stim,mparam,obsrv,elast)
%             end
%         end
%         elast=eup;

postlast=post;
elast=eup;
    end
end

%%
%**************************************************************************
%Figures
%**********************************************************************

ntrials=min(sim.niter,tmax);

fdeft.hf=figure;
fdeft.xlabel='#deflated';
fdeft.ylabel='time(s)';
fdeft.name='deflate';
fdeft.hp(1)=plot([einfo.ndeflate],[einfo.eigtime],'.');
set(fdeft.hp(1),'MarkerFaceColor','b');
fdeft.width=3;
fdeft.height=3;
lblgraph(fdeft);


fdeflate.hf=figure;
fdeflate.xlabel='trial';
fdeflate.ylabel='# deflated';
fdeflate.name='deflate';
fdeflate.hp(1)=plot([einfo.ndeflate],'.');
set(fdeflate.hp(1),'MarkerFaceColor','b');
fdeflate.width=3;
fdeflate.height=3;
lblgraph(fdeflate);

ftt.hf=figure;
ftt.xlabel='trial';
ftt.ylabel='time(s)';
ftt.name='eigtime';
ftt.hp(1)=plot(1:ntrials,[einfo.eigtime])
set(ftt.hp(1),'MarkerFaceColor','b');
ftt.width=3;
ftt.height=3;
lblgraph(ftt);


fdist.hf=figure;
fdist.xlabel='# distinct eigenvalues';
fdist.ylabel='time(s)';
fdist.name='eigdistinct';
fdist.hp(1)=plot([einfo.neigdist]',[einfo.eigtime]','.');
fdist.width=3;
fdist.height=3;
lblgraph(fdist);

fdt.hf=figure;
fdt.xlabel='trial';
fdt.ylabel='# distinct eigenvalues';
fdt.name='eigdistinct';
fdt.hp(1)=plot([einfo.neigdist],'.');
fdt.width=3;
fdt.height=3;
lblgraph(fdt);





otable=[];
otable={fdeft,{'nc06eigmulttime.m';dsets(end).dfile; 'dimensionality: 165'}; fdeflate,''};
otable=[otable; {ftt,''; fdist,'';fdt,''}];

onenotetable(otable,seqfname('../notes/eignote_0704.xml'));
