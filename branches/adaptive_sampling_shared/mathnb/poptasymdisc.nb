(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 5.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[      5803,        175]*)
(*NotebookOutlinePosition[      6455,        198]*)
(*  CellTagsIndexPosition[      6411,        194]*)
(*WindowFrame->Normal*)



Notebook[{
Cell[BoxData[
    \(\(\( (*\ 
      Compute\ the\ Hessian\ of\ our\ objective\ function\ *) \)\(\
\[IndentingNewLine]\)\(Clear["\<@\>"]\[IndentingNewLine] (*\ 
      start\ by\ defining\ our\ objective\ function\ *) \[IndentingNewLine] \
(*\ dimensions\ of\ the\ inputs\ *) \[IndentingNewLine]
    \(dims = 2;\)\[IndentingNewLine] (*\ 
      dimension\ of\ the\ probability\ vector\ *) \[IndentingNewLine]
    \(dimp = 2;\)\[IndentingNewLine]
    svec[j_] := 
      Transpose[{Table[s\_\(k, j\), {k, 1, dims}]}]\[IndentingNewLine]
    smat[j_] := svec[j] . Transpose[svec[j]]\[IndentingNewLine]
    o[p_] := 
      Log[Det[\[Sum]\+\(j = 1\)\%dimp p[\([j]\)]\ \(jo\_j\) 
              smat[j]]]\[IndentingNewLine]
    pvec[start_, end_] := 
      Table[p\_i, {i, start, 
          end}]\[IndentingNewLine] (*compute\ the\ derivatives\ \
*) \[IndentingNewLine]
    dop[i_] := 
      Module[{df}, ov = o[pvec[1, dimp]]; df = D[ov, p\_i]; 
        df]\[IndentingNewLine]
    d2op[i_, j_] := 
      Module[{d2f}, of = dop[i]; d2f = D[of, p\_j]; d2f]\[IndentingNewLine]
    \)\)\)], "Input"],

Cell[BoxData[
    \(\(\( (*\ 
      now\ we\ write\ down\ the\ expressions\ in\ matrix\ form\ for\ the\ \
derivatives\ and\ see\ if\ the\ are\ equal\ to\ the\ above\ expressions\ \
*) \)\(\[IndentingNewLine]\)\(emat[
        p_] := \[Sum]\+\(j = 1\)\%\(Length[p]\)p[\([\)\(j\)\(]\)]\ \(jo\_j\) 
          smat[j]\[IndentingNewLine]
    dmat[i_] := 
      Module[{imat}, imat = Inverse[emat[pvec[1, dimp]]]; 
        simat = \((\(jo\_i\) smat[i])\); 
        mmat = imat*
            simat; \[IndentingNewLine]\[Sum]\+\(c = 1\)\%dims\(\[Sum]\+\(r = \
1\)\%dims mmat[\([c, r]\)]\)]\[IndentingNewLine]
    d2mat[i_, j_] := 
      Module[{imat}, imat = Inverse[emat[pvec[1, dimp]]]; 
        rmat = \(-imat . \((\(jo\_j\) smat[j])\)\); 
        simat = \(jo\_i\) smat[i]; \ 
        mmat = \((rmat . imat)\)*
            simat; \[IndentingNewLine]\[Sum]\+\(c = 1\)\%dims\(\[Sum]\+\(r = \
1\)\%dims mmat[\([c, r]\)]\)]\)\)\)], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
    \(\(\( (*Check\ the\ \ Hessian\ computed\ using\ both\ methods\ equals\ \
the\ same\ results\ derivatives\ *) \)\(\[IndentingNewLine]\)\(\(dimp = 
        3;\)\[IndentingNewLine]
    Table[
      FullSimplify[d2mat[i, j] - d2op[i, j]], {i, 1, dimp}, {j, 1, 
        dimp}]\)\)\)], "Input"],

Cell[BoxData[
    \({{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
    \(\(dimp = 3;\)\), "\[IndentingNewLine]", 
    \(\(dims = 2;\)\), "\[IndentingNewLine]", 
    \(Table[
      FullSimplify[dop[i] - dmat[i]], {i, 1, 
        dimp}]\[IndentingNewLine]\), "\[IndentingNewLine]", 
    \(\)}], "Input"],

Cell[BoxData[
    \({0, 0, 0}\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
    \(MatrixForm[
      Transpose[{pvec[1, 2]}] . {pvec[1, 2]}\ ]\), "\[IndentingNewLine]", 
    \(MatrixForm[{\ pvec[1, 2]}]\)}], "Input"],

Cell[BoxData[
    TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
            {\(p\_1\%2\), \(p\_1\ p\_2\)},
            {\(p\_1\ p\_2\), \(p\_2\%2\)}
            },
          RowSpacings->1,
          ColumnSpacings->1,
          ColumnAlignments->{Left}], "\[NoBreak]", ")"}],
      Function[ BoxForm`e$, 
        MatrixForm[ BoxForm`e$]]]], "Output"],

Cell[BoxData[
    TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
            {\(p\_1\), \(p\_2\)}
            },
          RowSpacings->1,
          ColumnSpacings->1,
          ColumnAlignments->{Left}], "\[NoBreak]", ")"}],
      Function[ BoxForm`e$, 
        MatrixForm[ BoxForm`e$]]]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(MatrixForm[svec[1]]\)], "Input"],

Cell[BoxData[
    TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
            {\(s\_\(1, 1\)\)},
            {\(s\_\(1, 2\)\)}
            },
          RowSpacings->1,
          ColumnSpacings->1,
          ColumnAlignments->{Left}], "\[NoBreak]", ")"}],
      Function[ BoxForm`e$, 
        MatrixForm[ BoxForm`e$]]]], "Output"]
}, Open  ]]
},
FrontEndVersion->"5.2 for X",
ScreenRectangle->{{0, 1052}, {0, 1680}},
WindowSize->{742, 600},
WindowMargins->{{Automatic, 63}, {107, Automatic}},
ShowSelection->True
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 1085, 24, 285, "Input"],
Cell[2842, 77, 930, 19, 259, "Input"],

Cell[CellGroupData[{
Cell[3797, 100, 307, 6, 59, "Input"],
Cell[4107, 108, 67, 1, 27, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[4211, 114, 249, 6, 91, "Input"],
Cell[4463, 122, 43, 1, 27, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[4543, 128, 154, 3, 43, "Input"],
Cell[4700, 133, 356, 10, 62, "Output"],
Cell[5059, 145, 304, 9, 41, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[5400, 159, 52, 1, 27, "Input"],
Cell[5455, 162, 332, 10, 58, "Output"]
}, Open  ]]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

