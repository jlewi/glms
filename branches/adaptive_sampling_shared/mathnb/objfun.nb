(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 5.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[      3286,        105]*)
(*NotebookOutlinePosition[      3917,        127]*)
(*  CellTagsIndexPosition[      3873,        123]*)
(*WindowFrame->Normal*)



Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
    \(\(\( (*\ 
      Numerically\ evaluate\ the\ expectations\ \
*) \)\(\[IndentingNewLine]\)\(\(\[Mu] =  .5;\)\[IndentingNewLine]
    \(\[Sigma] =  .05^ .5;\)\[IndentingNewLine]
    N[\[Integral]\_\(-\[Infinity]\)\%\[Infinity] Log[
            1 + \(\[ExponentialE]\^\[Epsilon]\) \[Sigma]\^2] \(1\/\(\(\@\(2  \
\[Pi]\)\) \[Sigma]\)\) 
          Exp[\(-\((\[Epsilon] - \[Mu])\)\^2\)\/\(2  \[Sigma]\^2\)] \
\[DifferentialD]\[Epsilon]]\)\)\)], "Input"],

Cell[BoxData[
    RowBox[{\(Integrate::"gener"\), \(\(:\)\(\ \)\), "\<\"Unable to check \
convergence. \\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\", \
ButtonStyle->\\\"RefGuideLinkText\\\", ButtonFrame->None, \
ButtonData:>\\\"Integrate::gener\\\"]\\)\"\>"}]], "Message"],

Cell[BoxData[
    \(0.08098567145346393`\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(N[Obj]\)], "Input"],

Cell[BoxData[
    \(\(-5.210340371976262`\) + 0.`\ Fail\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(N[\(1\/\(\(\@\(2  \[Pi]\)\) \[Sigma]\)\) 
        Exp[\(-\((2 - \[Mu])\)\^2\)\/\(2  \[Sigma]\^2\)]]\)], "Input"],

Cell[BoxData[
    \(0.3989422804014327`\)], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
    \(Obj\)], "Input"],

Cell[BoxData[
    \(\[Integral]\_\(-\[Infinity]\)\%\[Infinity]\(\( \
\[ExponentialE]\^\(\(-\(1\/2\)\)\ \((\(-2\) + \[Epsilon])\)\^2\)\ log[
                1 + \[ExponentialE]\^\[Epsilon]]\)\/\@\(2\ \[Pi]\)\) \
\[DifferentialD]\[Epsilon]\)], "Output"]
}, Open  ]]
},
FrontEndVersion->"5.2 for X",
ScreenRectangle->{{0, 1280}, {0, 1024}},
WindowSize->{518, 600},
WindowMargins->{{193, Automatic}, {Automatic, 56}}
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{

Cell[CellGroupData[{
Cell[1776, 53, 465, 9, 102, "Input"],
Cell[2244, 64, 269, 4, 20, "Message"],
Cell[2516, 70, 54, 1, 27, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[2607, 76, 39, 1, 27, "Input"],
Cell[2649, 79, 69, 1, 27, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[2755, 85, 132, 2, 51, "Input"],
Cell[2890, 89, 53, 1, 27, "Output"]
}, Open  ]],

Cell[CellGroupData[{
Cell[2980, 95, 36, 1, 27, "Input"],
Cell[3019, 98, 251, 4, 60, "Output"]
}, Open  ]]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

