/*
 *
 * $Header$
 *
 * Copyright (c) 1992, 1993 Cornell University.  All Rights Reserved.  
 *  
 * Use, reproduction, preparation of derivative works, and distribution
 * of this software is permitted.  Any copy of this software or of any
 * derivative work must include both the above copyright notice of
 * Cornell University and this paragraph.  Any distribution of this
 * software or derivative works must comply with all applicable United
 * States export control laws.  This software is made available AS IS,
 * and CORNELL UNIVERSITY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE, AND NOTWITHSTANDING ANY OTHER
 * PROVISION CONTAINED HEREIN, ANY LIABILITY FOR DAMAGES RESULTING FROM
 * THE SOFTWARE OR ITS USE IS EXPRESSLY DISCLAIMED, WHETHER ARISING IN
 * CONTRACT, TORT (INCLUDING NEGLIGENCE) OR STRICT LIABILITY, EVEN IF
 * CORNELL UNIVERSITY IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

static char rcsid[] = "@(#)$Header$";

#include <stdio.h>
#include "misc.h"
#include <math.h>
#include "image.h"
#include "scale-h.h"
#include "list.h"
#include "chunk.h"
#include "dbug.h"

#include "prof.h"

#ifdef	MP
#include <signal.h>
#include <sys/wait.h>
#endif

/* About how fine to slice each dimension at the lowest resolution */
#define	SLICE		4 /* 63 */

/* What the resolution refinement between steps is */
#define	REFINE		2

/* What the minimum step size at top level we're willing to accept is */
#define	MINTOPSTEP	4

/* The usual FP epsilon */
#define	EPSILON		1e-7

/* How much of the model to scan before deciding on early decision */
#define	EARLY_FRAC	0.2

/* How wide a beam in the beam search, initially */
#define	BEAMWIDTH	16

/* and how much bigger it gets on a backtrack */
#define	BEAMSPREAD	2

/* Instrumentation */
#ifdef	INSTRUMENT
static long nprobes = 0;
static long nabort = 0;
static long npick = 0;
static long nearly = 0;
static long nwrong = 0;
static long nfull = 0;

static long nprobes1 = 0;
static long nabort1 = 0;
static long npick1 = 0;
static long nearly1 = 0;
static long nwrong1 = 0;
static long nfull1 = 0;

static int level;

#ifdef INST2
#define MAXPT 10000

static boolean decision[MAXPT];

static int ntt[MAXPT];
static int ntf[MAXPT];
static int nft[MAXPT];
static int nff[MAXPT];

static int ntt1[MAXPT];
static int ntf1[MAXPT];
static int nft1[MAXPT];
static int nff1[MAXPT];

static int nbelow[MAXPT];
static int nbelow1[MAXPT];

#endif
#endif

/* Search and verification functions */
static void findSTrans(simage_info *image, smodel_info *model,
		       int forwstyle, int revstyle);
static void checkSTransMtoI(simage_info *image, smodel_info *model);
static void checkSTransItoM(simage_info *image, smodel_info *model,
			    int revstyle);
static boolean checkOneSTransMtoI(simage_info *image, smodel_info *model,
				  stransval *t);
static boolean evalOneSTransMtoI(simage_info *image, smodel_info *model,
				 int x, int y, int xs, int ys,
				 long *dist, double *frac, double *frac2);
static boolean checkOneSTransItoM(simage_info *image, smodel_info *model,
				  long reverse_thresh, double reverse_frac,
				  int revstyle,
				  int x, int y, int xs, int ys,
				  boolean *result);
extern void findSTransMtoI(simage_info *image, smodel_info *model);
static void findSTransOne(simage_info *image, smodel_info *model,
			  int revstyle);
static void findSTransBestDist(simage_info *image, smodel_info *model,
			       int revstyle);
static void findSTransBestFrac(simage_info *image, smodel_info *model,
			       int revstyle);

extern boolean findSTransMtoISampled(simage_info *image, smodel_info *model,
				     int lowX, int highX, int stepX,
				     int lowY, int highY, int stepY,
				     int lowScaleX, int highScaleX,
				     int stepScaleX,
				     int lowScaleY, int highScaleY,
				     int stepScaleY,
				     PriQ pq,
				     long thresh, boolean needdata);

static void BFS(simage_info *image, smodel_info *model, int revstyle,
		boolean (*cmpfunc)(void *pqh1, void *pqh2),
		boolean (*matchfunc)(simage_info *image, smodel_info *model,
				     int revstyle, spqHook *hook));
static boolean bfsrecur(simage_info *image, smodel_info *model,
			PriQ lowpq,
			boolean (*cmpfunc)(void *pqh1, void *pqh2),
			boolean (*matchfunc)(simage_info *image,
					     smodel_info *model,
					     int revstyle, spqHook *hook),
			int revstyle,
			int lowX, int highX, int stepX,
			int lowY, int highY, int stepY,
			int lowScaleX, int highScaleX, int stepScaleX,
			int lowScaleY, int highScaleY, int stepScaleY,
			int curbeam);

static void hillclimb(simage_info *image, smodel_info *model, int revstyle);

extern boolean probeRegions(simage_info *image, smodel_info *model,
			    PriQ lowpq, PriQ highpq,
			    int lowX, int highX, int stepX, int oldStepX,
			    int lowY, int highY, int stepY, int oldStepY,
			    int lowScaleX, int highScaleX, int stepScaleX,
			    int oldStepScaleX,
			    int lowScaleY, int highScaleY, int stepScaleY,
			    int oldStepScaleY,
			    long thresh, boolean needdata, int nexpand);

#ifdef	MP
static void mp_probeRegions(void);
#endif

/* Support functions */
static boolean compute_scaled_pts(smodel_info *model);

static void shuffle_model(smodel_info *model);

static void calcLowCells(simage_info *image, smodel_info *model,
			 int *lowX, int *highX, int *stepX,
			 int *lowY, int *highY, int *stepY,
			 int *lowScaleX, int *highScaleX, int *stepScaleX,
			 int *lowScaleY, int *highScaleY, int *stepScaleY);
static void updateCellSteps(smodel_info *model,
			    int stepX, int stepY,
			    int stepScaleX, int stepScaleY,
			    int *newStepX, int *newStepY,
			    int *newStepScaleX, int *newStepScaleY);
static void calcBoxes(smodel_info *model, int stepX, int stepY,
		      int stepScaleX, int stepScaleY,
		      unsigned *box_width, unsigned *box_height);
static void convertPtToBox(int x, int y, int scaleX, int scaleY,
			   int lowX, int stepX, int oldStepX,
			   int lowY, int stepY, int oldStepY,
			   int lowScaleX, int stepScaleX, int oldStepScaleX,
			   int lowScaleY, int stepScaleY, int oldStepScaleY,
			   int *botX, int *topX, int *botY, int *topY,
			   int *botSX, int *topSX,
			   int *botSY, int *topSY);

static boolean inrange(simage_info *image, smodel_info *model,
		       int x, int y, int xs, int ys);

static int roundlog(int x, int n, int m);

/* Comparison functions for priq sorting */
static boolean falseCompare(void *a, void *b);
static boolean findDistCompare(void *a, void *b);
static boolean findOneCompare(void *a, void *b);
static boolean findFracCompare(void *a, void *b);

/* Match functions for search */
static boolean findOneMatch(simage_info *image, smodel_info *model,
			    int revstyle, spqHook *hook);
static boolean findDistMatch(simage_info *image, smodel_info *model,
			     int revstyle, spqHook *hook);
static boolean findFracMatch(simage_info *image, smodel_info *model,
			     int revstyle, spqHook *hook);

#ifdef	MP
/* Multiprocess stuff */
static boolean mp_startup(simage_info *image, smodel_info *model, int forwstyle);
static void mp_shutdown(void);

static void killall(int sig, int code, struct sigcontext *scp, char *addr);

extern int getpid(void);
extern void (*signal(int sig, void (*func)()))();

static pid_t pid[NPROC];	/* Who is out there? */
static pid_t mypid;		/* who am I? */
static int mynum;		/* and what's my number? */

static void *shmem;		/* Shared mem stuff */
static int shmid;

static int semid;

/* Some semaphore numbers */
#define	MAIN_Q_LOCK	0		/* Access to main queue */
#define	SECOND_Q_LOCK	1		/* Access to second queue */
#define	MAIN_Q_SLEEP	2		/* Waiting for main queue non-empty */
#define	SECOND_Q_SLEEP	3		/* Waiting for second queue non-full */
#define	MAINPROC_WAIT	4		/* Main process waiting for everyone else */

#define	NSEMS		5

/* Some stuff to deal with the job queues */

/* Max length of each one */
#define	Q_SIZE		5000

/* How many things you should grab from a queue at once, max */
#define	JOBCHUNK	100

/* The sorts of things that can be stuck in a queue */
#define	Q_WORK		1
#define	Q_DIE		2

/* and an actual queue entry */
typedef struct {
    int whattodo;
    spqHook pqh;
    } qent;

/*
 * Queue pointers and the queues. The main process is the only writer on
 * the main queue and only reader on the secondary. Note that the queue
 * pointers are shared. Any access to this structure must be protected
 * by a lock.
 * The box sizes etc are basically the values passed to probeRegions. They
 * stay constant until the level of the search shifts. This will never
 * happen unless all the subsidiary processes are sitting around waiting
 * for jobs, and so the subsidiary processes can just look at them when they
 * pull a job chunk off the queue.
 *
 * Note that I'm passing pointers (specifically model and image) through
 * shared memory. This is fine as long as they point to the right thing
 * in every process's address space. This is true since they start out
 * as clones of each other, at some time *after* model and image are
 * generated.
 */
typedef struct {
    int main_first;
    int main_last;
    int main_waiters;
    int second_first;
    int second_last;
    int second_waiters;
    int jobsize;
    boolean (*cmpfunc)(void *pqh1, void *pqh2);
    unsigned box_width;
    unsigned box_height;
    int lowX, highX, stepX, oldStepX;
    int lowY, highY, stepY, oldStepY;
    int lowScaleX, highScaleX, stepScaleX, oldStepScaleX;
    int lowScaleY, highScaleY, stepScaleY, oldStepScaleY;
    boolean needdata;
    long thresh;
    smodel_info *model;
    simage_info *image;
    long model_thresh;
    double model_frac;
    qent main[Q_SIZE];
    qent second[Q_SIZE];
    } jobs;

#define	SHM_SIZE	(sizeof(jobs))

static volatile jobs *queues;

/* Some operations */

/* Lock and unlock the queues */
static struct sembuf lock_main_q_op = { MAIN_Q_LOCK, -1, 0 };
static struct sembuf unlock_main_q_op = { MAIN_Q_LOCK, 1, 0 };
static struct sembuf lock_second_q_op = { SECOND_Q_LOCK, -1, 0 };
static struct sembuf unlock_second_q_op = { SECOND_Q_LOCK, 1, 0 };

static union semun dummyarg = { 0 };

/*
 * This is using the semaphore-zero stuff of SunOS: you can wait
 * for a semaphore to become zero. These semaphores are normally 1,
 * and if the queue is empty (full), the processes start sleeping
 * on them (waiting for them to become zero). Once they are ready
 * again (non-empty, non-full resp), the main process decrements the
 * semaphore, everybody wakes up, and it increments it again.
 * Note that they have to unlock the main lock for the queue before they
 * go to sleep since otherwise nobody could get in to wake them up. I'm
 * assuming (since man semop isn't clear) that:
 * Given an array of sembufs, semop will process the operations in them
 * 1) in order and
 * 2) atomically.
 * Thus, we unlock the queue and go to sleep as one action - there is no
 * timing hole between unlocking and sleeping (i.e. nobody can sneak in
 * and remove the need for us to go to sleep, so we'll never get woken
 * up since that event won't happen again).
 *
 * Hum. These assumptions appear to be incorrect. Of course.
 * What it seems to do is more like wait until it can perform them all
 * at once, then do them. I guess this makes sense, since then it really
 * is an atomic action. I guess I get to do it as two operations and live
 * with the (tiny) timing hole until I decide to figure out how to fix it.
 * FIX.
 *
 * There's an added complication (of course) - the main process needs to
 * know when everyone has finished processing the jobs they took, so it
 * needs to wait for them. Of course, they might block partway through because
 * the secondary queue filled up, and so they'll need the main process to
 * come and clean it out for them. Thus, whenever a process goes to sleep
 * on either queue (waiting for main non-empty or secondary non-full), it
 * increments the MAINPROC_WAIT semaphore. The main process can therefore
 * safely sleep on this semaphore, and whenever anyone else increments
 * it, it will wake up and do what is necessary: either proceed because all
 * the work has been done, or clean out the secondary queue and go back to
 * sleep, or go back to sleep because someone is still working.
 */
static struct sembuf presleep_main_q_op[2] = { { MAINPROC_WAIT, 1, 0 },
					       { MAIN_Q_LOCK, 1, 0 } };
static struct sembuf sleep_main_q_op = { MAIN_Q_SLEEP, -1, 0 };
static struct sembuf wakeup_main_q_op = { MAIN_Q_SLEEP, 0, 0 };
static struct sembuf presleep_second_q_op[2] = { { MAINPROC_WAIT, 1, 0 },
						 { SECOND_Q_LOCK, 1, 0 } };
static struct sembuf sleep_second_q_op = { SECOND_Q_SLEEP, -1, 0 };
static struct sembuf wakeup_second_q_op = { SECOND_Q_SLEEP, 0, 0 };

static struct sembuf mainproc_do_wait_op = { MAINPROC_WAIT, -1, 0 };

/*
 * Initial values:
 * The lock semaphores should be 1, so one process can grab each lock.
 * The sleep semaphores should be 0, so processes initially sleep on them.
 * The mainproc semaphore should be 0, so the main process sleeps on it
 * until someone else sleeps on one of the others.
 */
static ushort semarr[NSEMS] = { 1, 1, 0, 0, 0 };

/* Some macros to deal with the queues */
#define	LOCK_MAIN_Q	semop(semid, &lock_main_q_op, 1)
#define	UNLOCK_MAIN_Q	semop(semid, &unlock_main_q_op, 1)
#define	LOCK_SECOND_Q	semop(semid, &lock_second_q_op, 1)
#define	UNLOCK_SECOND_Q	semop(semid, &unlock_second_q_op, 1)

#define	PRESLEEP_MAIN_Q	semop(semid, &presleep_main_q_op[0], 2)
#define	SLEEP_MAIN_Q	semop(semid, &sleep_main_q_op, 1)
#define	WAKEUP_MAIN_Q	semop(semid, &wakeup_main_q_op, 1)
#define	PRESLEEP_SECOND_Q	semop(semid, &presleep_second_q_op[0], 2)
#define	SLEEP_SECOND_Q	semop(semid, &sleep_second_q_op, 1)
#define	WAKEUP_SECOND_Q	semop(semid, &wakeup_second_q_op, 1)

#define	MAINPROC_DO_WAIT	semop(semid, &mainproc_do_wait_op, 1)

#endif

/*
 * forwstyle controls how the search is performed.
 * If it is FORWARD_ALL, we search for all transformations where
 * the specified model_frac of the model pixels are under model_thresh.
 * In this case, we can also pass in a list of transformations in model->trans;
 * these will be checked out and those that meet the criteria will be returned;
 * others will be deleted.
 * If it is FORWARD_ONE, we search for *any* transformation which
 * meets this requirement, and report it. It must also satisfy the reverse
 * criteria (unless revstyle is REVERSE_NONE).
 * If it is FORWARD_BESTDIST, we search for all transformations where
 * the model_frac'th distance is minimised: find a transformation (which must
 * satisfy the reverse criteria) and use its distance until a better one
 * is found etc. model->model_thresh is set to this best distance.
 * If it is FORWARD_BESTFRAC, we do a similar search, except that we want
 * to find transformations where the number of points less than model_thresh
 * are maximised. model->model_frac is set to this best fraction.
 *
 * FORWARD_ONE can be modified by FORWARD_HILLCLIMB: find a match, and
 * then hillclimb from there to a local minimum. Also, with FORWARD_ALL and
 * trans != NULLLIST (i.e. passing in a list of transformations to be
 * evaluated), if FORWARD_HILLCLIMB is set it will hillclimb out of all
 * transformations which meet the criteria.
 */
void
findSTransAllModels(simage_info *image, smodel_info *models, unsigned nmodels,
		    int forwstyle, int revstyle)
{
    int i;
    int max_leftborder;
    int max_topborder;
    int max_rightborder;
    int max_bottomborder;

    assert(models != (smodel_info *)NULL);
    assert(image != (simage_info *)NULL);

    if (nmodels == 0) {
	return;		/* What's to do? */
	}

    max_leftborder = models[0].leftborder;
    max_topborder = models[0].topborder;
    max_rightborder = models[0].rightborder;
    max_bottomborder = models[0].bottomborder;
    for (i = 1; i < nmodels; i++) {
	max_leftborder = MAX(max_leftborder, models[i].leftborder);
	max_topborder = MAX(max_topborder, models[i].topborder);
	max_rightborder = MAX(max_rightborder, models[i].rightborder);
	max_bottomborder = MAX(max_bottomborder, models[i].bottomborder);
	}

    if ((image->dtrans == (LongImage)NULL) ||
	(image->leftborder < max_leftborder) ||
	(image->topborder < max_topborder) ||
	(image->rightborder < max_rightborder) ||
	(image->bottomborder < max_bottomborder)) {

	/* Were they being very silly? */
	if ((max_leftborder + max_rightborder + (int)image->xsize <= 0) ||
	    (max_topborder + max_bottomborder + (int)image->ysize <= 0)) {
	    /*
	     * They asked for borders which leave no part of the image.
	     * I refuse to deal with this case properly (i.e. deal with the
	     * model->trans fields appropriately). FIX someday?
	     */
	    return;
	    }

	/* The image's distance transform is not present, or is too small */
	if (image->dtrans != (LongImage)NULL) {
	    imFree(image->dtrans);
	    image->dtrans = (LongImage)NULL;
	    }

	/* Any cached box dtranses are out of date */
	clear_sboxdtcache(image);

	image->leftborder = max_leftborder;
	image->topborder = max_topborder;
	image->rightborder = max_rightborder;
	image->bottomborder = max_bottomborder;

	image->dtrans =
	    dtrans_pts(image->leftborder + image->rightborder + image->xsize,
		       image->topborder + image->bottomborder + image->ysize,
		       -image->leftborder, -image->topborder,
		       image->npts, image->pts);
	if (image->dtrans == (LongImage)NULL) {
	    return;
	    }
	}

    for (i = 0; i < nmodels; i++) {
	/* We'll need the max values */
	/* and the scaled X and Y coordinates of all the points */
	if (!compute_scaled_pts(&models[i])) {
	    return;
	    }

	/* Do the match */
	findSTrans(image, &models[i], forwstyle, revstyle);

	if (models[i].trans == NULLLIST) {
	    return;
	    }

	}
#ifdef INSTRUMENT
    DEBUG("Total: %d probes\n", nprobes);
#endif
    }

/*
 * Perform the matching between this model and this image, using the
 * specified matching styles.
 *
 * Should handle trans!=NULL in cases other than FORWARD_ALL, even if only
 * to assertfail. FIX.
 */
static void
findSTrans(simage_info *image, smodel_info *model, int forwstyle, int revstyle)
{
    ListNode ln;
    stransval *t;

#ifdef	MP
    /* We're running in multiprocess mode. Start up the MP stuff */
    if (!mp_startup(image, model, forwstyle)) {
	return;
	}
#endif

    switch(forwstyle) {
      case FORWARD_ALL:
      case FORWARD_ALL | FORWARD_HILLCLIMB: {
	/*
	 * If we're searching for all matches, can do the two (forward and
	 * reverse) passes separately
	 */
	  
	  
	if (model->trans != NULLLIST) {
	    /* We've been handed a list of transformations to look at */
	    checkSTransMtoI(image, model);
	    }
	else {
	    /* Search for matches */
	    assert(forwstyle == FORWARD_ALL);	/* Can't hillclimb */
	    findSTransMtoI(image, model);
	    }
	
	/* Failed? */
	if (model->trans == NULLLIST) {
	    return;
	    }
	
	break;
	}

      case FORWARD_ONE:
      case FORWARD_ONE | FORWARD_HILLCLIMB: {
	/* Find a single match */
	findSTransOne(image, model, revstyle);
	break;
	}

      case FORWARD_BESTDIST: {
	/* Find the best distance */
	findSTransBestDist(image, model, revstyle);
	break;
	}

      case FORWARD_BESTFRAC: {
	/* Find the best distance */
	findSTransBestFrac(image, model, revstyle);
	break;
	}

      default: {
	panic("bad forwstyle");
	break;
	}

      }

#ifdef	MP
    mp_shutdown();
#endif

    if (forwstyle & FORWARD_HILLCLIMB) {
	/*
	 * We got some matches. Improve them.
	 */
	hillclimb(image, model, revstyle);
	}

    /*
     * OK - now we need to fill in the reverse values for the matches.
     * Some of these may already have been done, as matches used to generate
     * the parameter must pass the reverse test, but there's no way to
     * tell which, at the moment, so we do them all.
     */
    if (revstyle != REVERSE_NONE) {
	checkSTransItoM(image, model, revstyle);
	}
    else {
	/* Clear out all the reverse vals, so we don't report garbage */
	foreach (ln, model->trans) {
	    t = (stransval *)liGet(ln);
	    assert(t != (stransval *)NULL);
	    t->reverse_val = 0;
	    t->reverse_frac = t->reverse_frac2 = 0.;
	    t->reverse_num = 0;
	    }
	}
    
    }
    

/*
 * Find all transformations where the model->image distance is <= thresh.
 * Use pruning tricks. The tricks are complicated a little by the fact
 * that we're using a partial match. We therefore have to consider all the
 * model points.
 * Also, we can't simply abort out of a scan when we find a value that is
 * too large, though we can do something similar by counting. The rest of
 * the pruning is still possible, though.
 *
 * The space we search is the space of all translations, crossed with the
 * space of scales (indpendent scales in x and y) such that
 * maxtransx >= x >= mintransx
 * maxtransy >= y >= mintransy
 * MIN(1, maxscalex) >= scalex >= model->minscalex
 * MIN(1, maxscaley) >= scaley >= model->minscaley
 * scalex / scaley <= maxskew
 * scaley / scalex <= maxskew.
 * and the scaled model box fits inside the image borders.
 *
 * We discretise each space. We discretise the translation space at a
 * resolution of one pixel in x and y, and discretise the scale space in such
 * a way that one "tick" in the x-scale direction causes each model point to
 * move at most one pixel in x, and similarly for the y dimension.
 *
 * We do the whole mess in a multi-resolution manner:
 * the first pass is very coarse, with very high threshold. It tells us
 * what sections look interesting. Further passes refine these sections
 * at increasingly higher resolution.
 *
 */
void
findSTransMtoI(simage_info *image, smodel_info *model)
{
    PriQ lowpq = NULLPRIQ;
    PriQ highpq = NULLPRIQ;
    List trans = NULLLIST;
    PriQNode pqn;
    spqHook *hook;
    ListNode newln;
    stransval *newt;
    boolean needdata;

    int lowX, highX, stepX, oldStepX;
    int lowY, highY, stepY, oldStepY;
    int lowScaleX, highScaleX, stepScaleX, oldStepScaleX;
    int lowScaleY, highScaleY, stepScaleY, oldStepScaleY;
    unsigned box_width, box_height;
    
    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(model->pts != (point *)NULL);
    assert(image->dtrans != (LongImage)NULL);
    assert(model->stepx > 0);
    assert(model->stepy > 0);

    trans = liNew();
    if (trans == NULLLIST) {
	goto bailout;
	}

    /* Get rid of an annoying case */
    if (model->npts == 0) {
	model->trans = trans;
	return;		/* No transformations */
	}

    /* Now figure out what step size we're using at the lowest resolution */

    calcLowCells(image, model,
		 &lowX, &highX, &stepX, &lowY, &highY, &stepY,
		 &lowScaleX, &highScaleX, &stepScaleX,
		 &lowScaleY, &highScaleY, &stepScaleY);

    /* Allocate the lowest resolution priority queue */
    lowpq = pqNew(falseCompare);
    if (lowpq == NULLPRIQ) {
	goto bailout;
	}

    /* Get the top-level box sizes */
    calcBoxes(model, stepX, stepY, stepScaleX, stepScaleY,
	      &box_width, &box_height);

    /* and make the boxdtrans */
    if (!ensure_sdtrans(image, box_width, box_height, model->model_thresh)) {
	goto bailout;
	}

    DEBUG("box_width = %d, box_height = %d\n", box_width, box_height);

    /*
     * Determine if we're in highest res yet; store in needdata.
     */
    if ((stepX == model->stepx) && (stepY == model->stepy) &&
	(stepScaleX == model->stepx) && (stepScaleY == model->stepy)) {
	needdata = TRUE;
	}
    else {
	needdata = FALSE;
	}
    
#ifdef	INSTRUMENT
    level = 0;
    nprobes1 = nabort1 = npick1 = nearly1 = nwrong1 = nfull1 = 0;
    nprobes = nabort = npick = nearly = nwrong = nfull = 0;
#ifdef	INST2
    {
    int i;
    for (i = 0; i < MAXPT; i++) {
	ntt1[i] = nft1[i] = ntf1[i] = nff1[i] = 0;
	ntt[i] = nft[i] = ntf[i] = nff[i] = 0;
	nbelow1[i] = 0;
	nbelow[i] = 0;
	}
    }
#endif
#endif

    /* Get the coarsest-resolution scan */
    if (!findSTransMtoISampled(image, model,
			       lowX, highX, stepX,
			       lowY, highY, stepY,
			       lowScaleX, highScaleX, stepScaleX,
			       lowScaleY, highScaleY, stepScaleY,
			       lowpq, model->model_thresh, needdata)) {
	/* Failed. Bah. */
	goto bailout;
	}

#ifdef	INSTRUMENT

    DEBUG("after level %d:\n", level);
    DEBUG("%d probes, %d aborted, %d picked (%d early, of which %d wrong), %d full (this level)\n",
	  nprobes1, nabort1, npick1, nearly1, nwrong1, nfull1);
    DEBUG("%d probes, %d aborted, %d picked (%d early, of which %d wrong), %d full (total)\n",
	  nprobes, nabort, npick, nearly, nwrong, nfull);
#ifdef	INST2
    {
    int i;
    fprintf(stderr, "\"ntt%d\n", level);
    for (i = 0; i < model->npts; i++) {
	fprintf(stderr, "%d %d\n", i, ntt1[i]);
	}
    fprintf(stderr, "\n\"ntf%d\n", level);
    for (i = 0; i < model->npts; i++) {
	fprintf(stderr, "%d %d\n", i, ntf1[i]);
	}
    fprintf(stderr, "\n\"nft%d\n", level);
    for (i = 0; i < model->npts; i++) {
	fprintf(stderr, "%d %d\n", i, nft1[i]);
	}
    fprintf(stderr, "\n\"nff%d\n", level);
    for (i = 0; i < model->npts; i++) {
	fprintf(stderr, "%d %d\n", i, nff1[i]);
	}
    }
    {
    GrayImage m;
    int i;
    char name[100];
    m = (GrayImage)imNew(IMAGE_GRAY, model->xsize, model->ysize);
    for (i = 0; i < model->npts; i++) {
	imRef(m, model->pts[i].x, model->pts[i].y) =
	    nbelow1[i] * (COLRNG - 1) / nprobes1;
	}
    (void)sprintf(name, "/tmp/mod%d.pgm", level);
    (void)imSave(m, name);
    imFree(m);
    }
#endif
#endif

    /* We now have the lowest-resolution priority queue. Refine... */
    while ((stepX > model->stepx) || (stepY > model->stepy) ||
	   (stepScaleX > model->stepx) || (stepScaleY > model->stepy)) {
	/* update stepX etc. keep old ones in oldStepX etc */
	oldStepX = stepX;
	oldStepY = stepY;
	oldStepScaleX = stepScaleX;
	oldStepScaleY = stepScaleY;

	updateCellSteps(model,
			oldStepX, oldStepY, oldStepScaleX, oldStepScaleY,
			&stepX, &stepY, &stepScaleX, &stepScaleY);
	/* Update the threshold */
	calcBoxes(model, stepX, stepY, stepScaleX, stepScaleY,
		  &box_width, &box_height);

	if (!ensure_sdtrans(image, box_width, box_height, model->model_thresh)) {
	    goto bailout;
	    }
	
	DEBUG("box_width = %d, box_height = %d\n", box_width, box_height);
	
	/*
	 * Determine if we're in highest res yet; store in needdata.
	 */
	if ((stepX == model->stepx) && (stepY == model->stepy) &&
	    (stepScaleX == model->stepx) && (stepScaleY == model->stepy)) {
	    needdata = TRUE;
	    }
	else {
	    needdata = FALSE;
	    }

	/* Allocate the new priority queue */
	highpq = pqNew(falseCompare);
	if (highpq == NULLPRIQ) {
	    goto bailout;
	    }
	
	shuffle_model(model);
#ifdef	INSTRUMENT
	level++;
	nprobes1 = nabort1 = npick1 = nearly1 = nwrong1 = nfull1 = 0;
#ifdef	INST2
	{
	int i;
	for (i = 0; i < MAXPT; i++) {
	    ntt1[i] = nft1[i] = ntf1[i] = nff1[i] = 0;
	    nbelow1[i] = 0;
	    }
	}
#endif
#endif
	/* Pick out regions */
	if (!probeRegions(image, model, lowpq, highpq,
			  lowX, highX, stepX, oldStepX,
			  lowY, highY, stepY, oldStepY,
			  lowScaleX, highScaleX, stepScaleX, oldStepScaleX,
			  lowScaleY, highScaleY, stepScaleY, oldStepScaleY,
			  model->model_thresh, needdata, -1)) {
	    goto bailout;
	    }

#ifdef	INSTRUMENT
    DEBUG("after level %d:\n", level);
    DEBUG("%d probes, %d aborted, %d picked (%d early, of which %d wrong), %d full (this level)\n",
	  nprobes1, nabort1, npick1, nearly1, nwrong1, nfull1);
    DEBUG("%d probes, %d aborted, %d picked (%d early, of which %d wrong), %d full (total)\n",
	  nprobes, nabort, npick, nearly, nwrong, nfull);
#ifdef	INST2
	{
	int i;
	fprintf(stderr, "\"ntt%d\n", level);
	for (i = 0; i < model->npts; i++) {
	    fprintf(stderr, "%d %d\n", i, ntt1[i]);
	    }
	fprintf(stderr, "\n\"ntf%d\n", level);
	for (i = 0; i < model->npts; i++) {
	    fprintf(stderr, "%d %d\n", i, ntf1[i]);
	    }
	fprintf(stderr, "\n\"nft%d\n", level);
	for (i = 0; i < model->npts; i++) {
	    fprintf(stderr, "%d %d\n", i, nft1[i]);
	    }
	fprintf(stderr, "\n\"nff%d\n", level);
	for (i = 0; i < model->npts; i++) {
	    fprintf(stderr, "%d %d\n", i, nff1[i]);
	    }
	}
	{
	GrayImage m;
	int i;
	char name[100];
	m = (GrayImage)imNew(IMAGE_GRAY, model->xsize, model->ysize);
	for (i = 0; i < model->npts; i++) {
	    imRef(m, model->pts[i].x, model->pts[i].y) =
		nbelow1[i] * (COLRNG - 1) / nprobes1;
	    }
	(void)sprintf(name, "/tmp/mod%d.pgm", level);
	(void)imSave(m, name);
	imFree(m);
	}
#endif
#endif

	/* That generated highpq for us - make it the new lowpq */
	assert(pqFirst(lowpq) == NULLPRIQNODE);
	pqFree(lowpq);
	lowpq = highpq;
	highpq = NULLPRIQ;
	}

    /*
     * We now have lowpq - the priority queue at highest resolution.
     * Read it out into trans.
     */
    for (pqn = pqFirst(lowpq); pqn != NULLPRIQNODE; pqn = pqFirst(lowpq)) {

	/* Get the hook from the first thing in the pq */
	hook = (spqHook *)pqGet(pqn);
	assert(hook != (spqHook *)NULL);
	/* and unhook it */
	pqRem(pqn);

	/* The values must be valid */
	assert(hook->model_thresh == model->model_thresh);
	assert(hook->model_frac == model->model_frac);

	newt = malloc_strans();
	if (newt == (stransval *)NULL) {
	    free_spqHook(hook);		/* Free it - we own it now */
	    goto bailout;
	    }
	newt->transpos.x = hook->x;
	newt->transpos.y = hook->y;
	newt->scalex = hook->scaleX * model->scaleStepX;
	newt->scaley = hook->scaleY * model->scaleStepY;
	newt->forward_val = hook->forward_val;
	newt->forward_frac = hook->forward_frac;
	newt->forward_frac2 = hook->forward_frac2;
	/* The hook is no longer needed */
	free_spqHook(hook);

	newln = liAdd(trans, (void *)newt);
	if (newln == NULLLISTNODE) {
	    free_strans(newt);
	    goto bailout;
	    }

	}

    /* Free useless stuff */
    assert(pqFirst(lowpq) == NULLPRIQNODE);
    pqFree(lowpq);

    model->trans = trans;

    
#ifdef INST2
    {
    int i;
    fprintf(stderr, "\"ntt\n");
    for (i = 0; i < model->npts; i++) {
	fprintf(stderr, "%d %d\n", i, ntt[i]);
	}
    fprintf(stderr, "\n\"ntf\n");
    for (i = 0; i < model->npts; i++) {
	fprintf(stderr, "%d %d\n", i, ntf[i]);
	}
    fprintf(stderr, "\n\"nft\n");
    for (i = 0; i < model->npts; i++) {
	fprintf(stderr, "%d %d\n", i, nft[i]);
	}
    fprintf(stderr, "\n\"nff\n");
    for (i = 0; i < model->npts; i++) {
	fprintf(stderr, "%d %d\n", i, nff[i]);
	}
    }
    {
    GrayImage m;
    int i;
    char name[100];
    m = (GrayImage)imNew(IMAGE_GRAY, model->xsize, model->ysize);
    for (i = 0; i < model->npts; i++) {
	imRef(m, model->pts[i].x, model->pts[i].y) =
	    nbelow[i] * (COLRNG - 1) / nprobes;
	}
    (void)sprintf(name, "/tmp/modall.pgm");
    (void)imSave(m, name);
    imFree(m);
    }
#endif

    return;

  bailout:

    if (lowpq != NULLPRIQ) {
	free_spqHooklist(lowpq);
	}

    if (highpq != NULLPRIQ) {
	free_spqHooklist(highpq);
	}

    if (trans != NULLLIST) {
	free_stranslist(trans);
	}

    model->trans = NULLLIST;
    }

/*
 * Check the transformations that we were handed (in model->trans); remove
 * those where the thresholds aren't satisfied.
 */
static void
checkSTransMtoI(simage_info *image, smodel_info *model)
{
    ListNode ln;
    long pointval = 0;
    long *vals = (long *)NULL;
    int *curxvals;
    int *curyvals;
    stransval *t;
    int x, y;
    int xs, ys;
    int i;
    int model_required;

    int nover;
    int novermax;

    /* Stuff cached from image and model */
    unsigned nmodel_pts;
    LongImage image_dtrans;
    long model_thresh;
    double model_frac;
    double scalestepx, scalestepy;
    double minscalex, minscaley;
    double maxskew;
    
    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(image->dtrans != (LongImage)NULL);
    assert(model->trans != NULLLIST);

    nmodel_pts = model->npts;
    model_thresh = model->model_thresh;
    model_frac = model->model_frac;
    image_dtrans = image->dtrans;
    minscalex = model->minscalex;
    minscaley = model->minscaley;
    maxskew = model->maxskew;
    scalestepx = model->scaleStepX;
    scalestepy = model->scaleStepY;

    model_required = (int)(ceil(nmodel_pts * (model_frac - EPSILON)));
    novermax = nmodel_pts - model_required;

    if (model_frac != 1.) {
	vals = (long *)malloc(nmodel_pts * sizeof(long));
	if (vals == (long *)NULL) {
	    goto bailout;
	    }
	}

    for (ln = liFirst(model->trans); ln != NULLLISTNODE; ) {
	t = (stransval *)liGet(ln);
	assert(t != (stransval *)NULL);
	x = t->transpos.x;
	y = t->transpos.y;
	/* Convert the X and Y scale values to integers... */
	xs = (int)(t->scalex / scalestepx + 0.5);
	ys = (int)(t->scaley / scalestepy + 0.5);

	if (!ensure_xscales(model, xs)) {
	    goto bailout;
	    }
	if (!ensure_yscales(model, ys)) {
	    goto bailout;
	    }
	curxvals = model->scaled_x[xs];
	curyvals = model->scaled_y[ys];

	if (model_frac == 1.) {
	    pointval = 0;
	    for (i = 0; i < nmodel_pts; i++) {
		pointval = MAX(pointval, imRef(image_dtrans,
					       curxvals[i] + x,
					       curyvals[i] + y));
		if (pointval > model_thresh) {
		    break;
		    }
		}
	    }
	else {
	    nover = 0;
	    for (i = 0; i < nmodel_pts; i++) {
		/* Probe at this model point's transformed location */
		pointval = vals[i] = imRef(image_dtrans,
					   curxvals[i] + x,
					   curyvals[i] + y);
		if (pointval > model_thresh) {
		    nover++;
		    if (nover > novermax) {
			break;
			}
		    }

		}
	    
	    if (i == nmodel_pts) {
		pointval = pickNth(vals, (int)nmodel_pts, model_required - 1);
		assert(pointval <= model_thresh);
		}
	    }

	if (pointval <= model_thresh) {
	    t->forward_val = pointval;
	    if (model_frac == 1.) {
		t->forward_frac = 1.;

		if (pointval == model_thresh) {
		    t->forward_frac2 = 1.;
		    }
		else {
		    /* Great - we have to rescan */
		    nover = 0;
		    for (i = 0; i < nmodel_pts; i++) {
			if (imRef(image_dtrans,
				  curxvals[i] + x,
				  curyvals[i] + y) > pointval) {
			    nover++;
			    }
			}
		    t->forward_frac2 =
			(nmodel_pts - nover) / ((double)nmodel_pts);
		    }

		}
	    else {
		t->forward_frac = frac_lower(vals, (int)nmodel_pts,
					     model_thresh);
		assert(t->forward_frac >= model_frac - EPSILON);
		if (pointval == model_thresh) {
		    t->forward_frac2 = t->forward_frac;
		    }
		else {
		    t->forward_frac2 =
			frac_lower(vals, (int)nmodel_pts, pointval);
		    }
		
		}

	    ln = liNext(ln);
	    }
	else {
	    free_strans(t);
	    ln = liRemAndNext(ln);
	    }
	}

    if (model_frac != 1) {
	assert(vals != (long *)NULL);
	free((void *)vals);
	}

    return;

  bailout:
    if (vals) {
	free((void *)vals);
	}

    free_stranslist(model->trans);
    model->trans = NULLLIST;
    return;
    }

/*
 * This routine evaluates the forward distance for a single stransval.
 * It returns TRUE on success, FALSE on failure. It does no checking
 * of ranges etc - just evaluates the stransval wrt the forward match
 * parameters. It fills in the forward_val and forward_frac* fields.
 */
static boolean
checkOneSTransMtoI(simage_info *image, smodel_info *model, stransval *t)
{
    int x, y;
    int xs, ys;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(t != (stransval *)NULL);
    
    x = t->transpos.x;
    y = t->transpos.y;
    /* Convert the X and Y scale values to integers... */
    xs = (int)(t->scalex / model->scaleStepX + 0.5);
    ys = (int)(t->scaley / model->scaleStepY + 0.5);

    /* and evaluate, storing into t */
    return(evalOneSTransMtoI(image, model, x, y, xs, ys,
			     &(t->forward_val), &(t->forward_frac),
			     &(t->forward_frac2)));
    }

/*
 * This routine evaluates the forward distance for a single location.
 * It returns TRUE on success, FALSE on failure. It does no checking
 * of ranges etc - just evaluates the location wrt the forward match
 * parameters.
 */
static boolean
evalOneSTransMtoI(simage_info *image, smodel_info *model,
		  int x, int y, int xs, int ys,
		  long *dist, double *frac, double *frac2)
{
    static long *vals = (long *)NULL;
    static int nvals = 0;

    int *curxvals;
    int *curyvals;
    int i;
    int model_required;

    /* Stuff cached from image and model */
    LongImage image_dtrans;
    unsigned nmodel_pts;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(dist != (long *)NULL);
    assert(frac != (double *)NULL);
    assert(frac2 != (double *)NULL);
    
    assert(image->dtrans != (LongImage)NULL);
    image_dtrans = image->dtrans;
    nmodel_pts = model->npts;

    model_required = (int)(ceil(nmodel_pts * (model->model_frac - EPSILON)));

    /* Make sure our work area is valid */
    if ((vals == (long *)NULL) || (nvals < nmodel_pts)) {
	if (vals != (long *)NULL) {
	    free((void *)vals);
	    }
	vals = (long *)malloc(nmodel_pts * sizeof(*vals));
	if (vals == (long *)NULL) {
	    goto bailout;
	    }
	nvals = nmodel_pts;
	}

    /* Get the scaled pts */
    if (!ensure_xscales(model, xs)) {
	goto bailout;
	}
    if (!ensure_yscales(model, ys)) {
	goto bailout;
	}
    curxvals = model->scaled_x[xs];
    curyvals = model->scaled_y[ys];

    /* Probe at this model point's transformed location */
    for (i = 0; i < nmodel_pts; i++) {
	vals[i] = imRef(image_dtrans, curxvals[i] + x, curyvals[i] + y);
	}

    /* and evaluate */
    *dist = pickNth(vals, (int)nmodel_pts, model_required - 1);
    *frac = frac_lower(vals, (int)nmodel_pts, model->model_thresh);
    if (*dist == model->model_thresh) {
	*frac2 = *frac;
	}
    else {
	*frac2 = frac_lower(vals, (int)nmodel_pts, *dist);
	}

    return(TRUE);

  bailout:
    return(FALSE);

    }

/*
 * This routine scans the transformations listed in modeltrans, looking for
 * those where at least image_frac of the image points under the model lie
 * within image_thresh of model points.
 *
 * It sorts those transformations by their scale parameters, since each
 * distinct set of scale values requires a new model to be constructed and
 * dtransed; this is expensive if there are lots of transformations to be
 * checked. We therefore group them and reuse the dtranses wherever possible.
 *
 * Note that this means that modeltrans is actually rebuilt as a new list
 * in a possibly different order.
 */
static void
checkSTransItoM(simage_info *image, smodel_info *model, int revstyle)
{
    static long *vals = (long *)NULL;
    static int nstaticvals = 0;

    ListNode ln;
    ListNode newln;
    stransval *t;
    int x, y;
    int x2, y2;
    int xs, ys;
    int curw, curh;
    int minx, maxx;
    int miny, maxy;
    long pointval;
    int nvals;
    int whichval;
    LongImage image_dtrans;
    LongImage model_dtrans;
    PtrImage scaleim = (PtrImage)NULL;
    List slist;
    int nimmax;
    int nimage;
    int maxxscale;
    int maxyscale;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(model->trans != NULLLIST);
    assert(image->dtrans != (LongImage)NULL);

    assert((revstyle == REVERSE_BOX) || (revstyle == REVERSE_ALLIMAGE));

    /* Cache stuff */
    image_dtrans = image->dtrans;
    nimage = image->npts;

    /* Figure out how many image pixels might be visible */
    if (revstyle == REVERSE_BOX) {
	nimmax = MIN(nimage, model->xsize * model->ysize);
	}
    else {
	nimmax = nimage;
	}

    /* Figure out how big an array we need */
    maxxscale = floor(model->maxscalex / model->scaleStepX + EPSILON) + 1;
    maxyscale = floor(model->maxscaley / model->scaleStepY + EPSILON) + 1;

    if ((vals == (long *)NULL) || (nstaticvals < nimmax)) {
	if (vals != (long *)NULL) {
	    free((void *)vals);
	    }
	vals = (long *)malloc(nimmax * sizeof(long));
	if (vals == (long *)NULL) {
	    goto bailout;
	    }
	nstaticvals = nimmax;
	}

    /* Build scaleim - bucket sorter for the scales */
    scaleim = (PtrImage)imNew(IMAGE_PTR,
			      (unsigned)maxxscale + 1,
			      (unsigned)maxyscale + 1);
    if (scaleim  == (PtrImage)NULL) {
	goto bailout;
	}

    /*
     * Read the list of translations out into scaleim.
     * Destroy model->trans as you go, so that there are never two
     * ListNodes which hold the same stransval. This makes cleanup
     * in the case of failure (bailout) possible, since otherwise we
     * wouldn't be able to tell which bits of model->trans had been read
     * out and which hadn't.
     */
    for (ln = liFirst(model->trans);
	 ln != NULLLISTNODE;
	 ln = liRemAndNext(ln)) {
	t = (stransval *)liGet(ln);
	assert(t != (stransval *)NULL);

	/* Compute the scale space grid coordinates */
	xs = (int)(t->scalex / model->scaleStepX + 0.5);
	ys = (int)(t->scaley / model->scaleStepY + 0.5);
	assert(xs >= 0);
	assert(ys >= 0);
	assert(xs <= maxxscale);
	assert(ys <= maxyscale);
	    
	if (imRef(scaleim, xs, ys) == (void *)NULL) {
	    /* First one at this scale */
	    slist = liNew();
	    if (slist == NULLLIST) {
		goto bailout;
		}
	    imRef(scaleim, xs, ys) = (void *)slist;
	    }
	/* Hook it into the bin */
	newln = liAdd((List)imRef(scaleim, xs, ys), (void *)t);
	if (newln == NULLLISTNODE) {
	    goto bailout;
	    }
	}

    /*
     * We don't need model->trans any more - free it.
     * Note that we *do not* use free_stranslist, since that would free
     * all the stransvals, which we just hooked into scaleim. Actually,
     * at this point model->trans should be empty.
     */
    assert(liLen(model->trans) == 0);
    liFree(model->trans);
    model->trans = NULLLIST;

    /* Now work on the bins in scaleim. */
    for (ys = 0; ys <= maxyscale; ys++) {
	for (xs = 0; xs <= maxxscale; xs++) {
	    if (imRef(scaleim, xs, ys) != (void *)NULL) {
		slist = (List)imRef(scaleim, xs, ys);

		/* Build the dtrans for scalings of xs, ys */
		if (revstyle == REVERSE_BOX) {
		    model_dtrans = get_smodel_dtrans(model, xs, ys);
		    }
		else {
		    assert(revstyle == REVERSE_ALLIMAGE);
		    /*
		     * Put borders of model->image_thresh on the dtrans.
		     * This means that if an image pixel's value is below
		     * thresh, then we will get its real value; any image
		     * pixel outside the model trans would have been over
		     * thresh if we'd seen it; we just need to keep track
		     * of how many such pixels there are.
		     */
		    model_dtrans =
			get_smodel_dtrans_padded(model, xs, ys,
						 (int)ceil(model->image_thresh /
						      (double)DSCALE));
		    }
		if (model_dtrans == (LongImage)NULL) {
		    goto bailout;
		    }
		
		/* Find out the area we need to scan */
		curw = imGetWidth(model_dtrans);
		curh = imGetHeight(model_dtrans);

		/* Now work on the list */
		for (ln = liFirst(slist); ln != NULLLISTNODE; ) {
		    t = (stransval *)liGet(ln);
		    assert(t != (stransval *)NULL);
		    x = t->transpos.x;
		    y = t->transpos.y;
		    
		    /* Scan over the place in the image where this lands */
		    nvals = 0;
		    if (revstyle == REVERSE_BOX) {
			assert(imGetXBase(model_dtrans) == 0);
			assert(imGetYBase(model_dtrans) == 0);
			/* We know that this lies within borders */
			for (y2 = y; y2 < y + curh; y2++) {
			    for (x2 = x; x2 < x + curw; x2++) {
				if (imRef(image_dtrans, x2, y2) == 0) {
				    vals[nvals++] =
					imRef(model_dtrans, x2 - x, y2 - y);
				    }
				}
			    }
			}
		    else {
			/* We have to do border checks */
			minx = MAX(x + imGetXBase(model_dtrans),
				   imGetXBase(image_dtrans));
			miny = MAX(y + imGetYBase(model_dtrans),
				   imGetYBase(image_dtrans));
			maxx = MIN(x + imGetXMax(model_dtrans),
				   imGetXMax(image_dtrans));
			maxy = MIN(y + imGetYMax(model_dtrans),
				   imGetYMax(image_dtrans));
			
			for (y2 = miny; y2 <= maxy; y2++) {
			    for (x2 = minx; x2 <= maxx; x2++) {
				if (imRef(image_dtrans, x2, y2) == 0) {
				    vals[nvals++] =
					imRef(model_dtrans, x2 - x, y2 - y);
				    }
				}
			    }
			}

		    if (revstyle == REVERSE_BOX) {
			whichval =
			    (int)(ceil(model->image_frac * (nvals - EPSILON)));
			pointval = pickNth(vals, nvals, whichval - 1);
			}
		    else {
			/*
			 * OK - we have nvals values which have been probed
			 * and nimage - nvals which haven't been,
			 * but which would be large if they were.
			 */
			whichval = (int)(ceil(model->image_frac * nimage));
			if (whichval > nvals) {
			    /* Reject */
			    pointval = model->image_thresh * 2 + 1;
			    }
			else {
			    /* Give it a chance */
			    pointval = pickNth(vals, nvals, whichval - 1);
			    }
			}

		    if (pointval <= model->image_thresh) {
			/* A good point. Keep it. */
			t->reverse_val = pointval;
			if (revstyle == REVERSE_BOX) {
			    if (nvals == 0) {
				t->reverse_frac = 1.;
				t->reverse_frac2 = 1.;
				}
			    else {
				t->reverse_frac =
				    frac_lower(vals, nvals,
					       model->image_thresh);
				if (pointval == model->image_thresh) {
				    t->reverse_frac2 = t->reverse_frac;
				    }
				else {
				    t->reverse_frac2 =
					frac_lower(vals, nvals, pointval);
				    }
				}
			    t->reverse_num = nvals;
			    }
			else {
			    if (nvals == 0) {
				if (nimage == 0) {
				    t->reverse_frac = 1.;
				    t->reverse_frac2 = 1.;
				    }
				else {
				    t->reverse_frac = 0.;
				    t->reverse_frac2 = 0.;
				    }
				}
			    else {
				t->reverse_frac =
				    frac_lower(vals, nvals,
					       model->image_thresh) * nvals /
						   nimage;
				if (pointval == model->image_thresh) {
				    t->reverse_frac2 = t->reverse_frac;
				    }
				else {
				    t->reverse_frac2 =
					frac_lower(vals, nvals,
						   pointval) * nvals /
						       nimage;
				    }
				}
			    
			    t->reverse_num = nimage;
			    }

			assert(t->reverse_frac >= model->image_frac - EPSILON);
			
			/* and advance */
			ln = liNext(ln);
			}
		    else {
			/* Free the hook from slist */
			free_strans(t);
			ln = liRemAndNext(ln);
			}
		    
		    }
		}
	    }
	}

    /*
     * The stransvals which remain in scaleim have been validated
     * and filled out. Read them out into model->trans.
     */
    model->trans = liNew();
    if (model->trans == NULLLIST) {
	goto bailout;
	}

    for (ys = 0; ys <= maxyscale; ys++) {
	for (xs = 0; xs <= maxxscale; xs++) {
	    if (imRef(scaleim, xs, ys) != (void *)NULL) {
		slist = (List)imRef(scaleim, xs, ys);
		liApp(model->trans, slist);
		imRef(scaleim, xs, ys) = (void *)NULL;
		}
	    }
	}

    imFree(scaleim);
    return;

  bailout:
    /* Don't free model_dtrans - we don't own it */

    if (model->trans != NULLLIST) {
	free_stranslist(model->trans);
	model->trans = NULLLIST;
	}

    if (scaleim != (PtrImage)NULL) {
	for (ys = 0; ys <= maxyscale; ys++) {
	    for (xs = 0; xs <= maxxscale; xs++) {
		if (imRef(scaleim, xs, ys) != (void *)NULL) {
		    slist = (List)imRef(scaleim, xs, ys);
		    free_stranslist(slist);
		    }
		}
	    }
	imFree(scaleim);
	}

    return;
    }

/*
 * Evaluate the transformation (x,y,xs,ys) for model wrt image, using
 * the supplied matching parameters and revstyle.
 * Return TRUE in *result if it checks out, FALSE if not.
 * Return TRUE on success, FALSE on failure.
 */
static boolean
checkOneSTransItoM(simage_info *image, smodel_info *model,
		   long reverse_thresh, double reverse_frac, int revstyle,
		   int x, int y, int xs, int ys, boolean *result)
{
    int curw, curh;
    int minx, maxx;
    int miny, maxy;
    int nvals;
    int nunder;
    LongImage image_dtrans;
    LongImage model_dtrans;
    int x2, y2;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(image->dtrans != (LongImage)NULL);
    assert(result != (boolean *)NULL);

    assert(xs >= 0);
    assert(ys >= 0);

    if (revstyle == REVERSE_NONE) {
	/* All succeed */
	*result = TRUE;
	return(TRUE);
	}

    /* At the moment, it has to be this */
    assert((revstyle == REVERSE_BOX) || (revstyle == REVERSE_ALLIMAGE));

    /* Cache stuff */
    image_dtrans = image->dtrans;

    /* Get the model dtrans */
    if (revstyle == REVERSE_BOX) {
	model_dtrans = get_smodel_dtrans(model, xs, ys);
	}
    else {
	model_dtrans = get_smodel_dtrans_padded(model, xs, ys,
						(int)ceil(reverse_thresh /
							  (double)DSCALE));
	}

    if (model_dtrans == (LongImage)NULL) {
	goto bailout;
	}

    curw = imGetWidth(model_dtrans);
    curh = imGetHeight(model_dtrans);

    /* Scan over the place in the image where this lands */
    nvals = nunder = 0;
    if (revstyle == REVERSE_BOX) {
	/* We know that this lies within borders */
	for (y2 = y; y2 < y + curh; y2++) {
	    for (x2 = x; x2 < x + curw; x2++) {
		if (imRef(image_dtrans, x2, y2) == 0) {
		    nvals++;
		    if (imRef(model_dtrans, x2 - x, y2 - y) <= reverse_thresh) {
			nunder++;
			}
		    }
		}
	    }
	}
    else {
	/* We have to do border checks */
	minx = MAX(x + imGetXBase(model_dtrans),
		   imGetXBase(image_dtrans));
	miny = MAX(y + imGetYBase(model_dtrans),
		   imGetYBase(image_dtrans));
	maxx = MIN(x + imGetXMax(model_dtrans),
		   imGetXMax(image_dtrans));
	maxy = MIN(y + imGetYMax(model_dtrans),
		   imGetYMax(image_dtrans));
	
	for (y2 = miny; y2 <= maxy; y2++) {
	    for (x2 = minx; x2 <= maxx; x2++) {
		if (imRef(image_dtrans, x2, y2) == 0) {
		    if (imRef(model_dtrans, x2 - x, y2 - y) <= reverse_thresh) {
			nunder++;
			}
		    }
		}
	    }

	nvals = image->npts;
	}

    if (nunder >= nvals * reverse_frac) {
	*result = TRUE;
	}
    else {
	*result = FALSE;
	}

    return(TRUE);

  bailout:

    /* Don't free model_dtrans - we don't own it */

    return(FALSE);

    }

/* Do the forwstyle == FORWARD_ONE matching */
static void
findSTransOne(simage_info *image, smodel_info *model, int revstyle)
{
    BFS(image, model, revstyle, findOneCompare, findOneMatch);
    /* We only wanted one match, and it's been thoroughly checked out. */
    }

static void
findSTransBestDist(simage_info *image, smodel_info *model, int revstyle)
{
    ListNode ln;
    stransval *t;

    assert(model != (smodel_info *)NULL);
    assert(image != (simage_info *)NULL);

    /* Set the model distance to a known large value, if it is < 0 */
    if (model->model_thresh < 0) {
	model->model_thresh =
	    DSCALE * (image->xsize + image->ysize +
		      image->leftborder + image->topborder +
		      image->rightborder + image->bottomborder);
	}
    /* Otherwise, it's set to a good starting value */

    BFS(image, model, revstyle, findDistCompare, findDistMatch);

    if (model->trans == NULLLIST) {
	/* Failed */
	return;
	}

    /*
     * model->trans now has a bunch of matches, some of which may not
     * meet the specs, due to the search method: whenever we find a match
     * which is better than the previous best, we throw it on to model->trans,
     * and rely on this cleanup phase to remove the previous best.
     *
     * Also, the forward_frac values won't be right: they will be based on
     * whatever the threshold was set to when this match was generated; it
     * may have decreased since then. However, the frac2 will be exactly
     * what we want; they'll all be based on the right model_thresh, since
     * all the matches we want to keep have the same forward_val.
     */

    for (ln = liFirst(model->trans); ln != NULLLISTNODE; ) {
	t = (stransval *)liGet(ln);
	assert(t != (stransval *)NULL);

	assert(t->forward_val >= model->model_thresh);
	if (t->forward_val > model->model_thresh) {
	    /* This one didn't work */
	    free_strans(t);
	    ln = liRemAndNext(ln);
	    }
	else {
	    /* t->forward_frac may be out of date - clobber it */
	    t->forward_frac = t->forward_frac2;
	    ln = liNext(ln);
	    }
	}

    }

static void
findSTransBestFrac(simage_info *image, smodel_info *model, int revstyle)
{
    ListNode ln;
    stransval *t;

    assert(model != (smodel_info *)NULL);
    assert(image != (simage_info *)NULL);

    /* Set the model fraction to a known small value, if it is < 0 */
    if (model->model_frac < 0) {
	model->model_frac = 0.;
	}
    /* Otherwise, it's set to a good starting value */


    BFS(image, model, revstyle, findFracCompare, findFracMatch);

    if (model->trans == NULLLIST) {
	/* Failed */
	return;
	}

    /*
     * Similar to above, except check via the fracs.
     * 
     * We also have to think about the forward_val values: some of them may
     * have been computed based on a different model_frac than the one that
     * we ended up using, and so may need to be recomputed. Now, since
     * we're dealing with stransvals, and not spqHooks, there aren't any
     * fields for the model_frac and model_thresh entries. However, we
     * arranged for the match function to store these values in
     * t->reverse_frac and t->reverse_val, which is a bit of a hack.
     */
    for (ln = liFirst(model->trans); ln != NULLLISTNODE; ) {
	t = (stransval *)liGet(ln);
	assert(t != (stransval *)NULL);

	assert(t->reverse_val == model->model_thresh);
	assert(t->reverse_frac <= model->model_frac);

	if (t->reverse_frac != model->model_frac) {
	    /* This one was calculated with different parameters. */
	    if (t->forward_val == model->model_thresh) {
		/* No recalc needed - it happened to work */
		assert(t->forward_frac == t->forward_frac2);
		}
	    else {
		/* We need to recalc the values */
		if (!checkOneSTransMtoI(image, model, t)) {
		    goto bailout;
		    }
		}

	    if (t->forward_frac < model->model_frac) {
		/* Kill it */
		free_strans(t);
		ln = liRemAndNext(ln);
		}
	    else {
		ln = liNext(ln);
		}
	    }
	else if (t->forward_frac < model->model_frac) {
	    /* This one didn't work */
	    free_strans(t);
	    ln = liRemAndNext(ln);
	    }
	else {
	    /* This one gets kept */
	    ln = liNext(ln);
	    }
	}

    return;

  bailout:
    if (model->trans != NULLLIST) {
	free_stranslist(model->trans);
	model->trans = NULLLIST;
	}

    return;
    }

/*
 * This is where the search is actually done. How it works:
 * Start by subdividing the transformation space, exactly as in
 * findSTransMtoI. This gives the top level. Scan all the cells there.
 * This generates a list of matching cells, at the current parameter
 * settings. Put these onto a priority queue, using cmpfunc to make
 * sure the "best" cells at the current parameters are used. Now,
 * scan the first few cells of the priority queue; this gives us
 * a bunch of cells at the next level, and so on.
 * When we get to the bottom level, what we are finding are actual
 * matches; we call matchfunc on each of those. If matchfunc returns
 * TRUE any time, we abort the search. This will happen in the FORWARD_ONE
 * case when a match is found, and also in the other cases if something fails
 * (memory allocation etc) during the match.
 */
static void
BFS(simage_info *image, smodel_info *model, int revstyle,
    boolean (*cmpfunc)(void *pqh1, void *pqh2),
    boolean (*matchfunc)(simage_info *image, smodel_info *model,
			 int revstyle, spqHook *hook))
{
    int lowX, highX, stepX;
    int lowY, highY, stepY;
    int lowScaleX, highScaleX, stepScaleX;
    int lowScaleY, highScaleY, stepScaleY;
    List trans = NULLLIST;
    PriQ lowpq = NULLPRIQ;
    unsigned box_width, box_height;
    boolean needdata;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(model->pts != (point *)NULL);
    assert(image->dtrans != (LongImage)NULL);
    assert(model->stepx > 0);
    assert(model->stepy > 0);
    assert(cmpfunc != (boolean (*)(void *pqh1, void *pqh2))NULL);
    assert(matchfunc != (boolean (*)(simage_info *image, smodel_info *model,
				     int revstyle, spqHook *hook))NULL);

    trans = liNew();
    if (trans == NULLLIST) {
	goto bailout;
	}
    model->trans = trans;

    /* Get rid of an annoying case */
    if (model->npts == 0) {
	return;		/* No transformations */
	}

    /* Now figure out what step size we're using at the lowest resolution */

    calcLowCells(image, model,
		 &lowX, &highX, &stepX, &lowY, &highY, &stepY,
		 &lowScaleX, &highScaleX, &stepScaleX,
		 &lowScaleY, &highScaleY, &stepScaleY);

    /* Allocate the lowest resolution priority queue */
    lowpq = pqNew(cmpfunc);
    if (lowpq == NULLPRIQ) {
	goto bailout;
	}

    /* Get the top-level box sizes */
    calcBoxes(model, stepX, stepY, stepScaleX, stepScaleY,
	      &box_width, &box_height);

    /* and make the boxdtrans */
    if (!ensure_sdtrans(image, box_width, box_height, model->model_thresh)) {
	goto bailout;
	}

    /* Initially, expand the top level into lowpq */

    /* Determine if we're in highest res yet. */
    if ((stepX == model->stepx) && (stepY == model->stepy) &&
	(stepScaleX == model->stepx) && (stepScaleY == model->stepy)) {
	needdata = TRUE;
	}
    else {
	needdata = FALSE;
	}

    /* Get the coarsest-resolution scan */
    if (!findSTransMtoISampled(image, model,
			       lowX, highX, stepX,
			       lowY, highY, stepY,
			       lowScaleX, highScaleX, stepScaleX,
			       lowScaleY, highScaleY, stepScaleY,
			       lowpq, model->model_thresh, needdata)) {
	/* Failed. Bah. */
	goto bailout;
	}

    if (!needdata) {
	/* OK - we're set to begin the actual search */
	(void)bfsrecur(image, model, lowpq, cmpfunc, matchfunc, revstyle,
		       lowX, highX, stepX,
		       lowY, highY, stepY,
		       lowScaleX, highScaleX, stepScaleX,
		       lowScaleY, highScaleY, stepScaleY, BEAMWIDTH);
	}
    else {
	/*
	 * We're basically done. We haven't really done the search they
	 * wanted though. Pity. FIX?
	 */
	}
	   

    /* We're done - clean up and return */
    free_spqHooklist(lowpq);

    return;

  bailout:

    if (lowpq != NULLPRIQ) {
	free_spqHooklist(lowpq);
	}

    if (trans != NULLLIST) {
	free_stranslist(trans);
	}

    model->trans = NULLLIST;

    return;

    }

/*
 * The recursive part of the best-first (beam) search.
 *
 * We are called with an image, a model, and a list of cells to expand
 * (in lowpq). The cells sizes and range are given by the low, high, step
 * parameters (i.e. the cells represented in lowpq have those step sizes).
 *
 * We expand up to curbeam of the cells in lowpq into highpq. If we're
 * not at the lowest level, then we call ourselves recursively on highpq.
 * If that call returns TRUE (i.e. search successful), we clean up and
 * return TRUE.
 * If not, we expand curbeam*BEAMSPREAD and try again. Continued failures
 * make the beam width continue to expand. Our child starts their beam width
 * off at our current width.
 *
 * When we've cleaned lowpq out, we return FALSE.
 *
 * If we are at the lowest level, then we take the list of matches which
 * we have in highpq, and call matchfunc() on each of them. If it returns
 * TRUE on any, then we clean up and return TRUE. Otherwise, we keep trying
 * until we run out of entries in lowpq. matchfunc will do things like
 * updating model->model_thresh (in the case of ...BestDist) and so on. It
 * will also do reverse matching if required, and will hook good matches
 * onto model->trans. It doesn't free the hook it is passed, though.
 *
 * Note that it isn't possible to distinguish between matchfunc() returning
 * TRUE because the search has succeeded, and it returning TRUE because it
 * failed in some memory allocation. Our cleanup routine must take this into
 * account.
 */
static boolean
bfsrecur(simage_info *image, smodel_info *model,
	 PriQ lowpq,
	 boolean (*cmpfunc)(void *pqh1, void *pqh2),
	 boolean (*matchfunc)(simage_info *image, smodel_info *model,
			      int revstyle, spqHook *hook), int revstyle,
	 int lowX, int highX, int stepX,
	 int lowY, int highY, int stepY,
	 int lowScaleX, int highScaleX, int stepScaleX,
	 int lowScaleY, int highScaleY, int stepScaleY,
	 int curbeam)
{
    int newStepX, newStepY, newStepScaleX, newStepScaleY;    
    unsigned box_width, box_height;
    PriQ highpq = NULLPRIQ;
    PriQNode pqn;
    spqHook *hook;
    boolean bottom;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(lowpq != NULLPRIQ);
    assert(cmpfunc != (boolean (*)(void *pqh1, void *pqh2))NULL);
    assert(matchfunc != (boolean (*)(simage_info *image, smodel_info *model,
				     int revstyle, spqHook *hook))NULL);

    /* Find out how big cells we should be generating */
    updateCellSteps(model, stepX, stepY, stepScaleX, stepScaleY,
		    &newStepX, &newStepY,
		    &newStepScaleX, &newStepScaleY);

    /* Update the threshold */
    calcBoxes(model, newStepX, newStepY, newStepScaleX, newStepScaleY,
	      &box_width, &box_height);

    /* Are we at the bottom yet? */
    bottom = ((newStepX == model->stepx) && (newStepY == model->stepy) &&
	      (newStepScaleX == model->stepx) &&
	      (newStepScaleY == model->stepy));

    /* Allocate the new priority queue */
    highpq = pqNew(cmpfunc);
    if (highpq == NULLPRIQ) {
	goto bailout;
	}
    
    /* Expand everything in lowpq, a bit at a time */
    while (pqFirst(lowpq) != NULLPRIQNODE) {
	
	/* Make sure that the right dtrans is loaded */
	if (!ensure_sdtrans(image, box_width, box_height, model->model_thresh)) {
	    goto bailout;
	    }

	/* Pick out regions */
	if (!probeRegions(image, model, lowpq, highpq,
			  lowX, highX, newStepX, stepX,
			  lowY, highY, newStepY, stepY,
			  lowScaleX, highScaleX, newStepScaleX, stepScaleX,
			  lowScaleY, highScaleY, newStepScaleY, stepScaleY,
			  model->model_thresh, bottom, curbeam)) {
	    goto bailout;
	    }
	
	if (bottom) {
	    /*
	     * At the bottom level, we need to scan through all the
	     * matches that probeRegions generated, and call matchfunc
	     * on each one. matchfunc will know what to do...
	     */
	    for (pqn = pqFirst(highpq);
		 pqn != NULLPRIQNODE;
		 pqn = pqFirst(highpq)) {
		hook = (spqHook *)pqGet(pqn);
		assert(hook != (spqHook *)NULL);
		pqRem(pqn);

		if ((*matchfunc)(image, model, revstyle, hook)) {
		    /* A match detected - bomb out */
		    free_spqHook(hook);
		    goto bailout;
		    }
		free_spqHook(hook);
		}
	    }
	else {
	    /* Recurse down the search */
	    if (bfsrecur(image, model, highpq, cmpfunc, matchfunc, revstyle,
			 lowX, highX, newStepX,
			 lowY, highY, newStepY,
			 lowScaleX, highScaleX, newStepScaleX,
			 lowScaleY, highScaleY, newStepScaleY, curbeam)) {
		/* Something was found down the line - cleanup and return */
		goto bailout;
		}
	    else {
		/* They must have exhausted highpq */
		assert(pqFirst(highpq) == NULLPRIQNODE);
		}
	    }

	/* We didn't find it - expand the beam and try again. */
	curbeam *= BEAMSPREAD;
	}

    /* We exhausted lowpq - search failed */
    assert(pqFirst(highpq) == NULLPRIQNODE);	/* Nothing left for downsearch */
    pqFree(highpq);
    return(FALSE);

  bailout:

    /* The search is over... clean up */
    if (highpq != NULLPRIQ) {
	free_spqHooklist(highpq);
	}

    return(TRUE);

    }

/*
 * A direction array used for hillclimbing. There are NDIR directions.
 * This has the property that delta[x] is the opposite of
 * delta[(x + NDIR/2) % NDIR].
 */
static int delta[][4] =
{
    { 1, 0, 0, 0}, { 0, 1, 0, 0}, { 0, 0, 1, 0}, { 0, 0, 0, 1},
    {-1, 0, 0, 0}, { 0,-1, 0, 0}, { 0, 0,-1, 0}, { 0, 0, 0,-1}
    };

#define	NDIR	8

/*
 * This routine looks at the matches in model->trans and attempts to
 * improve on each of them. For each match, it evaluates it, and then
 * evaluates its neighbours. If it is "better" than all of its neighbours,
 * then it returns. Otherwise, it continues searching from the best
 * neighbour. Of course, neighbours must qualify in the reverse direction
 * as well.
 *
 * Once it has found a better neighbour, that establishes a search line
 * direction. It keeps going in that direction until the matches stop
 * improving, at which point it does a complete neighbour-scan again, etc.
 *
 * This guarantees that it will end up at *a* local maximum. Note that it
 * might not end up at the best connected local maximum: there may be
 * a local maximum which is reachable from the initial match by passage
 * through good-match-space which it will not reach.
 *
 * Neighbours are the 16-connected (i.e. +-1 in one of the 4 dimensions)
 * neighbours of the match. It takes steps of size model->step[xy],
 * of course.
 */
static void
hillclimb(simage_info *image, smodel_info *model, int revstyle)
{
    int x, y;
    int xs, ys;
    int nx, ny;
    int nxs, nys;
    boolean goodrev;
    long curdist;
    double curfrac;
    double curfrac2;
    long newdist;
    double newfrac;
    double newfrac2;
    int curdir;
    ListNode ln;
    stransval *t;
    boolean done;
    int i;

    int stepx;
    int stepy;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);

    assert(model->trans != NULLLIST);

    stepx = model->stepx;
    stepy = model->stepy;

    foreach (ln, model->trans) {
	t = (stransval *)liGet(ln);
	assert(t != (stransval *)NULL);

	x = t->transpos.x;
	y = t->transpos.y;
	/* Convert the X and Y scale values to integers... */
	xs = (int)(t->scalex / model->scaleStepX + 0.5);
	ys = (int)(t->scaley / model->scaleStepY + 0.5);

	/* Get the current values */
	curdist = t->forward_val;
	curfrac = t->forward_frac;
	curfrac2 = t->forward_frac2;

	/* OK. Start the search... */
	assert(inrange(image, model, x, y, xs, ys));

	while (TRUE) {
	    /* Do a full scan */
	    curdir = -1;
	    for (i = 0; i < NDIR; i++) {
		nx = x + delta[i][0] * stepx;
		ny = y + delta[i][1] * stepy;
		nxs = xs + delta[i][2] * stepx;
		nys = ys + delta[i][3] * stepy;

		if (inrange(image, model, nx, ny, nxs, nys)) {
		    if (!evalOneSTransMtoI(image, model, nx, ny, nxs, nys,
					   &newdist, &newfrac, &newfrac2)) {
			goto bailout;
			}

		    if ((newdist < curdist) ||
			((newdist == curdist) && (newfrac2 > curfrac2))) {
			/* A potentially better match - check reverse */
			if (!checkOneSTransItoM(image, model,
						model->image_thresh,
						model->image_frac, revstyle,
						nx, ny, nxs, nys, &goodrev)) {
			    goto bailout;
			    }

			if (goodrev) {
			    /* yay */
			    curdist = newdist;
			    curfrac = newfrac;
			    curfrac2 = newfrac2;
			    curdir = i;
			    }
			}
		    }
		}

	    if (curdir == -1) {
		/* Nothing beat this one - a local max. */
		break;
		}
	    else {
		/* OK - now scan in curdir until done */
		done = FALSE;

		/* nx etc are garbage here - make them right */
		nx = x + delta[curdir][0] * stepx;
		ny = y + delta[curdir][1] * stepy;
		nxs = xs + delta[curdir][2] * stepx;
		nys = ys + delta[curdir][3] * stepy;

		while (!done) {
		    /* Take the step */
		    x = nx;
		    y = ny;
		    xs = nxs;
		    ys = nys;
		    
		    /* and plan the next one */
		    nx = x + delta[curdir][0] * stepx;
		    ny = y + delta[curdir][1] * stepy;
		    nxs = xs + delta[curdir][2] * stepx;
		    nys = ys + delta[curdir][3] * stepy;

		    done = TRUE;

		    if (inrange(image, model, nx, ny, nxs, nys)) {
			if (!evalOneSTransMtoI(image, model, nx, ny, nxs, nys,
					       &newdist, &newfrac, &newfrac2)) {
			    goto bailout;
			    }

			if ((newdist < curdist) ||
			    ((newdist == curdist) && (newfrac2 > curfrac2))) {
			    /* A potentially better match - check reverse */
			    if (!checkOneSTransItoM(image, model,
						    model->image_thresh,
						    model->image_frac,
						    revstyle,
						    nx, ny, nxs, nys,
						    &goodrev)) {
				goto bailout;
				}
			    
			    if (goodrev) {
				/* yay */
				curdist = newdist;
				curfrac = newfrac;
				curfrac2 = newfrac2;
				done = FALSE;		/* Keep going */
				}
			    }
			}
		    }
		}
	    }

	/* OK - we climbed the hill. Update t. */
	t->transpos.x = x;
	t->transpos.y = y;
	t->scalex = xs * model->scaleStepX;
	t->scaley = ys * model->scaleStepY;
	t->forward_val = curdist;
	t->forward_frac = curfrac;
	t->forward_frac2 = curfrac2;
	}

    return;

  bailout:
    /* Should do some indication of failure, like trashing model->trans */
    free_stranslist(model->trans);
    model->trans = NULLLIST;

    }

/*
 * Scan a region in transformation space from lowX to highX (inclusive,
 * exclusive) in X, stepping by stepX in X, and similarly for the other
 * dimensions. Probe at locations which are cell top top left left corners:
 * lowX, lowX + stepX etc. Cells must contain at least one valid
 * transformation (within transformation search bounds) to be considered.
 *
 * Look for places where there may be something interesting within a cell
 * of size (stepX,stepY,...) starting at the current position. For
 * each interesting cell you find, add a flag to pq. If needdata is TRUE,
 * evaluate the match. Note that this is worked out based on
 * image_box_dtrans, not image_dtrans. In the simple search case, these should
 * have the same values, since needdata is only turned on at the bottom level,
 * when they should be the same. In the more complex search cases, they
 * might be different.
 *
 * In any case, we attach a rough estimate of cell quality to
 * hook->forward_frac (unless needdata is turned on, in which case that
 * field contains what you'd expect).
 *
 * Cells are represented by their top left corner position.
 *
 * A cell is interesting if the corner position's value (determined by
 * probing image->box_dtrans) is <= thresh.
 */
boolean
findSTransMtoISampled(simage_info *image, smodel_info *model,
		      int lowX, int highX, int stepX,
		      int lowY, int highY, int stepY,
		      int lowScaleX, int highScaleX, int stepScaleX,
		      int lowScaleY, int highScaleY, int stepScaleY,
		      PriQ pq,
		      long thresh, boolean needdata)
{
    static long *vals = (long *)NULL;
    static int nvals;

    int i;
    int x, y;
    int xs, ys;
    double scalex, scaley;
    int *curxvals;
    int *curyvals;
    spqHook *newhook;
    int model_required;
    int model_early;
    int model_early_required;
    int novermax;
    int valsover;
    long pointval;
    boolean keep;
#ifdef	INST2
    boolean keepearly;
#endif
    double quality = 0.;

    /* Stuff cached from image and model */
    point *model_pts;
    unsigned nmodel_pts;
    LongImage image_dtrans;
    LongImage image_box_dtrans;
    long model_thresh;
    int mintransx, mintransy;
    int maxtransx, maxtransy;
    double minscalex, minscaley;
    double maxscalex, maxscaley;
    double maxskew;
    double scalestepx, scalestepy;
    
    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(model->pts != (point *)NULL);
    assert(image->dtrans != (LongImage)NULL);
    assert(pq != NULLPRIQ);

/*    DEBUG("sampling(%d..%d by %d, %d..%d by %d, %d..%d by %d, %d..%d by %d)\n", lowX, highX, stepX, lowY, highY, stepY, lowScaleX, highScaleX, stepScaleX, lowScaleY, highScaleY, stepScaleY);*/

    model_pts = model->pts;
    nmodel_pts = model->npts;
    model_thresh = model->model_thresh;
    mintransx = model->mintransx;
    mintransy = model->mintransy;
    maxtransx = model->maxtransx;
    maxtransy = model->maxtransy;
    minscalex = model->minscalex;
    minscaley = model->minscaley;
    maxscalex = model->maxscalex;
    maxscaley = model->maxscaley;
    maxskew = model->maxskew;
    scalestepx = model->scaleStepX;
    scalestepy = model->scaleStepY;

    image_dtrans = image->dtrans;

    model_required = (int)(ceil(nmodel_pts * (model->model_frac - EPSILON)));
    novermax = nmodel_pts - model_required;

    model_early = (int)(ceil(nmodel_pts * (EARLY_FRAC - EPSILON)));
    model_early_required =
	(int)(ceil(model_early * (model->model_frac - EPSILON)));

    /* Alloc vals, if necessary */
    if ((vals == (long *)NULL) || (nvals < nmodel_pts)) {
	if (vals != (long *)NULL) {
	    free((void *)vals);
	    }
	vals = (long *)malloc(nmodel_pts * sizeof(*vals));
	if (vals == (long *)NULL) {
	    goto bailout;
	    }
	nvals = nmodel_pts;
	}
    
    assert(image->box_dtrans != (LongImage)NULL);
    image_box_dtrans = image->box_dtrans;

    /* What are the smallest scales we're actually going to consider here? */
    minscalex = MAX(minscalex, lowScaleX * scalestepx);
    minscaley = MAX(minscaley, lowScaleY * scalestepy);

    /* Perform the scan. */
    for (y = lowY; y < highY; y += stepY) {
	/* Check translation bounds */
	if (y + stepY - model->stepy < mintransy) {
	    continue;
	    }
	if (y > maxtransy) {
	    break;
	    }

	/* Is there a place in this cell where the model fits in the borders? */
	if ((int)(y + minscaley * (model->ysize - 1) + .5) >=
	    (int)image->ysize + model->bottomborder) {
	    /* No - even at min scale it goes too far */
	    break;
	    }

	for (x = lowX; x < highX; x += stepX) {
	    /* Check translation bounds */
	    if (x + stepX - model->stepx < mintransx) {
		continue;
		}
	    if (x > maxtransx) {
		break;
		}
	    
	    /* Can this fit in, even at minimum scale? */
	    if ((int)(x + minscalex * (model->xsize - 1) + .5) >=
		(int)image->xsize + model->rightborder) {
		/* No - even at min scale it goes too far */
		break;
		}
	    
	    /* Search the scale space */
	    for (ys = lowScaleY; ys < highScaleY; ys += stepScaleY) {
		
		scaley = ys * scalestepy;
		/*
		 * We need to check if any of the the scales within
		 * this cell are valid.
		 */
		if ((ys + stepScaleY - model->stepy) * scalestepy <
		    minscaley - EPSILON) {
		    /* Even the highest scale in the cell is too small */
		    continue;
		    }
		if ((scaley > maxscaley + EPSILON) ||
		    ((int)(scaley * (model->ysize - 1) + .5) + y >=
		     (int)image->ysize + model->bottomborder)) {
		    /* Even the lowest scale is too big */
		    break;
		    }
		
		/* Pick out the model scaled by scaley */
		/* Make sure the scaled points exist at the current scale */
		if (!ensure_yscales(model, ys)) {
		    goto bailout;
		    }
		curyvals = model->scaled_y[ys];
		assert(curyvals != (int *)NULL);
		
		for (xs = lowScaleX; xs < highScaleX; xs += stepScaleX) {
		    scalex = xs * scalestepx;
		    /* Check validity */

		    if ((xs + stepScaleX - model->stepx) * scalestepx <
			minscalex - EPSILON) {
			/* Even the highest scale in the cell is too small */
			continue;
			}
		    if ((scalex > maxscalex + EPSILON) ||
			((int)(scalex * (model->xsize - 1) + .5) + x >=
			 (int)image->xsize + model->rightborder)) {
			break;
			}

		    if (scalex > ((ys + stepScaleY - model->stepy) *
				  scalestepy * maxskew)) {
			/* All the scales in the cell are too skewed */
			break;
			}
		    if (scaley > ((xs + stepScaleX - model->stepx) *
				  scalestepx * maxskew)) {
			continue;
			}
			
		    valsover = 0;

		    /* Make sure the scaled points exist at the current scale */
		    if (!ensure_xscales(model, xs)) {
			goto bailout;
			}
		    curxvals = model->scaled_x[xs];
		    assert(curxvals != (int *)NULL);

#ifdef	INSTRUMENT
		    nprobes++;
		    nprobes1++;
#endif
		    if (needdata) {
			/*
			 * We won't be taking early acceptance or anything.
			 * Be simple.
			 */
			for (i = 0; i < nmodel_pts; i++) {
			    vals[i] = pointval =
				imRef(image_box_dtrans,
				      curxvals[i] + x, curyvals[i] + y);

			    if (pointval <= thresh) {
#ifdef	INST2
				nbelow[i]++;
				nbelow1[i]++;
#endif
				}
			    else {
#ifndef INST2
				valsover++;
				if (valsover > novermax) {
				    /* DIE YOU GRAVY-SUCKING PIG! */
#ifdef	INSTRUMENT
				    nabort++;
				    nabort1++;
#endif
				    break;
				    }
				}
#endif
#ifdef INST2
			    decision[i] = ((i + 1 - valsover) / (double)(i + 1)) >= model->model_frac;
#endif
			    }

#ifndef	INST2
			if (i == nmodel_pts) {
			    keep = TRUE;
			    }
			else {
			    keep = FALSE;
			    }
#else
			if (valsover <= novermax) {
			    keep = TRUE;
			    }
			else {
			    keep = FALSE;
			    }
#endif

			}
		    else {
			/* Do the full thing. */

			for (i = 0; i < model_early; i++) {
			    pointval = imRef(image_box_dtrans,
					     curxvals[i] + x, curyvals[i] + y);
			    if (pointval <= thresh) {
#ifdef	INST2
				nbelow[i]++;
				nbelow1[i]++;
#endif
				}
			    else {
#ifndef INST2
				valsover++;
				if (valsover > novermax) {
				    /* DIE YOU GRAVY-SUCKING PIG! */
#ifdef	INSTRUMENT
				    nabort++;
				    nabort1++;
#endif
				    break;
				    }
#endif
				}
#ifdef INST2
			    decision[i] = ((i + 1 - valsover) / (double)(i + 1)) >= model->model_frac;
#endif
			    }


			/*
			 * At this point, check valsover for early decision
			 */
#ifdef INST2
			if ((model_early - valsover) >= model_early_required) {
			    nearly++;
			    nearly1++;
			    keepearly = TRUE;
			    }
			else {
			    keepearly = FALSE;
			    }
			if (TRUE) {
#else
			if (((model_early - valsover) < model_early_required) &&
			    (valsover <= novermax)) {
#endif
			    /* No early decision - keep probing */
			    for (i = model_early; i < nmodel_pts; i++) {
				pointval =
				    imRef(image_box_dtrans,
					  curxvals[i] + x, curyvals[i] + y);
				if (pointval <= thresh) {
#ifdef	INST2
				    nbelow[i]++;
				    nbelow1[i]++;
#endif
				    }
				else {
#ifndef INST2
				    valsover++;
				    if (valsover > novermax) {
					/* DIE YOU GRAVY-SUCKING PIG! */
#ifdef	INSTRUMENT
					nabort++;
					nabort1++;
#endif
					break;
					}
#endif
				    }
#ifdef INST2
				decision[i] = ((i + 1 - valsover) / (double)(i + 1)) >= model->model_frac;
#endif
				}
#ifndef	INST2
			    if (i == nmodel_pts) {
				keep = TRUE;
				quality = (nmodel_pts - valsover) /
				    (double)nmodel_pts;
				}
			    else {
				keep = FALSE;
				}
#else
			    if (valsover <= novermax) {
				keep = TRUE;
				quality = (nmodel_pts - valsover) /
				    (double)nmodel_pts;
				}
			    else {
				keep = FALSE;
				}
#endif
			    }
			else {
			    if (valsover <= novermax) {
				/* Take early acceptance on this point */
#ifdef	INSTRUMENT
				nearly++;
				nearly1++;
#endif
				keep = TRUE;
				quality = (model_early - valsover) /
				    (double)model_early;
				}
			    else {
				/* It aborted out of the early scan */
				keep = FALSE;
				}
			    }
			}

#ifdef INST2
		    for (i = 0; i < nmodel_pts; i++) {
			if (valsover <= novermax) {
			    if (decision[i]) {
				ntt[i]++;
				ntt1[i]++;
				}
			    else {
				ntf[i]++;
				ntf1[i]++;
				}
			    }
			else {
			    if (decision[i]) {
				nft[i]++;
				nft1[i]++;
				}
			    else {
				nff[i]++;
				nff1[i]++;
				}
			    }
			}

		    if ((!needdata) && (keepearly) && (!keep)) {
			nwrong++;
			nwrong1++;
			}
#endif

#ifdef INST2
		    if (((!needdata) && keepearly) || keep) {
#else
		    if (keep) {
#endif

			VDEBUG("Found one at (%d ", x, 10);
			VDEBUG("%d) ", y, 10);
			VDEBUG("[%d ", xs, 10);
			VDEBUG("%d]\n", ys, 10);

#ifdef	INSTRUMENT
			npick++;
			npick1++;
#endif

			/* Make a mark in pq */
			newhook = malloc_spqHook();
			if (newhook == (spqHook *)NULL) {
			    goto bailout;
			    }
			newhook->x = x;
			newhook->y = y;
			newhook->scaleX = xs;
			newhook->scaleY = ys;

			if (needdata) {
			    /* vals contains the info we need */
			    pointval = pickNth(vals, (int)nmodel_pts,
					       model_required - 1);
			    newhook->model_thresh = thresh;
			    newhook->model_frac = model->model_frac;
			    newhook->forward_val = pointval;
			    newhook->forward_frac =
				frac_lower(vals, (int)nmodel_pts, thresh);

			    if (pointval == thresh) {
				newhook->forward_frac2 = newhook->forward_frac;
				}
			    else {
				newhook->forward_frac2 =
				    frac_lower(vals, (int)nmodel_pts,
					       pointval);
				}
			    }
			else {
			    /* Stash the quality estimate */
			    newhook->forward_frac = quality;
#ifdef	INSTRUMENT
			    /* Work out if the cell is full */
			    valsover = 0;
			    for (i = 0; i < nmodel_pts; i++) {
				if (imRef(image->box_maxdtrans,
					  curxvals[i] + x, curyvals[i] + y)
				    > thresh) {
				    /*
				     * This point is over thresh,
				     * somewhere inside this cell
				     */
				    valsover++;
				    if (valsover > novermax) {
					break;
					}
				    }
				}
			    if (valsover <= novermax) {
				/* Any transform in this cell is good */
				nfull1++;
				nfull++;
				}
#endif
			    }

			if (pqAdd(pq, (void *)newhook) == NULLPRIQNODE) {
			    free_spqHook(newhook);
			    goto bailout;
			    }
			}
		    else {
			/* We used to do skip-forward here. */
			}
		    }
		}

	    }
	}

    /* Here are some comments about how it should be done better */
	/*
	 * The order of the loops here is:
	 * for each y scale
	 *   for each y translation such that the model fits inside the
	 *   borders at that scale
	 *     for each x scale
	 *       for each x translation which fits
	 * The reasons for this order are:
	 *   The scale loops are outside the translation loops so that
	 *   the y- and x- coordinates do not need to be recomputed too often.
	 *   The x loops are inside the y loops due to a desire for locality
	 *   since two pixels which are adjactent in x are closer in memory
	 *   than two pixels adjacent in y.
	 * Inside the x loops, we can use pruning techniques based on the
	 * +x distance transform: when we probe at a given transformation,
	 * we get back a distance in pixels to the closest under-threshold
	 * pixel for each model point. We can convert each of these into
	 * "scale ticks" by using the distance of each point from the left
	 * edge of the model. These two values determine how much of the
	 * x-(translation,scale) space can be ruled out; the higher the
	 * product, the more area, since as we project rightwards by a pixel,
	 * we lose some number of ticks that we can move in the scale direction
	 * before losing our guarantee that we're not skipping anything.
	 * Note that we can project into the strict "future", and also
	 * into the semi-strict future as well: if the x scale and translation
	 * values are >= than the ones at which the probe was taken, we can
	 * see if they moved the largest-area-generating pixel past the
	 * guaranteed distance; if they don't, we can rule them out. If
	 * x translation is < probe location, but x scale is > probe scale
	 * by enough that the pixel will not have moved leftwards, we can
	 * also perform this ruling out.
	 *
	 * It's actually not clear how to pick the best-area point; we
	 * have to make sure that the required guarantees hold. They are
	 * known to hold if we pick the model_frac'th value of the
	 * raw +x-transform values, but there may be some better way to do
	 * this. We also can't use the inflation factor of that point, but
	 * have to use the worst inflation factor that we saw. This is messy,
	 * and probably isn't worth doing.
	 *
	 * Unfortunately, none of this carries
	 * over to the next y translation or scale value; it may be worth
	 * putting in pruning based on true distance transform values as well,
	 * which can be propagated in all directions, including "semi-future"
	 * ones, and ones in y.
	 */

    /* Don't free vals - might be re-used */

    /* and done. */
    return(TRUE);

  bailout:
    /* Don't free vals */

    return(FALSE);
    }

/*
 * Given lowpq, a priority queue containing interesting regions at one
 * resolution (lowX..highX by oldStepX etc), we want to compute
 * highpq, a priority queue containing interesting regions at another
 * resolution (lowX..highX by stepX etc).
 *
 * We do this by finding "rectangular" regions in lowpq which are
 * interesting, and calling findSTransMtoISampled() on each, telling
 * it to store the results in highpq. Once every region flagged as interesting
 * in lowpq has been processed, we're done.
 * Each region is represented by its top left corner.
 * thresh defines what the threshold of "interesting" is.
 * Expand at most nexpand of the entries in lowpq. If nexpand is -1, expand
 * them all. Also, if nexpand is not -1, then we know that we're in one
 * of the odd search modes. In that case, model_thresh and model_frac may
 * have changed since the entries in lowpq were generated. Check them as
 * they come off lowpq, and throw away any ones that no longer meet the
 * requirements. Note that this assumes that they have had their data
 * calculated.
 */
boolean
probeRegions(simage_info *image, smodel_info *model,
	     PriQ lowpq, PriQ highpq,
	     int lowX, int highX, int stepX, int oldStepX,
	     int lowY, int highY, int stepY, int oldStepY,
	     int lowScaleX, int highScaleX, int stepScaleX, int oldStepScaleX,
	     int lowScaleY, int highScaleY, int stepScaleY, int oldStepScaleY,
	     long thresh, boolean needdata, int nexpand)
{
    int x, y, scaleX, scaleY;
    int nProbes = 0;
    spqHook *hook;
    PriQNode pqn;
#ifdef	MP
    int nwaiters;
    int i;
    volatile spqHook *vhook;
    unsigned box_width, box_height;
#else
    int botX, topX, botY, topY;
    int botSX, topSX, botSY, topSY;
#endif

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(lowpq != NULLPRIQ);
    assert(highpq != NULLPRIQ);

#ifndef	MP
    /* Single process mode */
    for (pqn = pqFirst(lowpq);
	 (pqn != NULLPRIQNODE) && ((nexpand < 0) || (nProbes < nexpand));
	 pqn = pqFirst(lowpq)) {

	/* Get the hook from the first thing in the pq */
	hook = (spqHook *)pqGet(pqn);
	assert(hook != (spqHook *)NULL);

	/* and unhook it */
	pqRem(pqn);
	x = hook->x;
	y = hook->y;
	scaleX = hook->scaleX;
	scaleY = hook->scaleY;

	/* and get rid of it */
	free_spqHook(hook);
	    
	/* Make it into a region fit for ...Sampled */
	convertPtToBox(x, y, scaleX, scaleY,
		       lowX, stepX, oldStepX,
		       lowY, stepY, oldStepY,
		       lowScaleX, stepScaleX, oldStepScaleX,
		       lowScaleY, stepScaleY, oldStepScaleY,
		       &botX, &topX, &botY, &topY,
		       &botSX, &topSX, &botSY, &topSY);

	if (!findSTransMtoISampled(image, model,
				   botX, topX, stepX,
				   botY, topY, stepY,
				   botSX, topSX, stepScaleX,
				   botSY, topSY, stepScaleY,
				   highpq, thresh, needdata)) {
	    /* Something failed down the line */
	    goto bailout;
	    }
	nProbes++;

	}
    DEBUG("level covered %d cells\n", nProbes);
#else
    /* Handle this in a multi-process manner */

    /*
     * We're the main process. Our job:
     * 1) Read lowpq off into the main job queue
     * 2) Read the secondary job queue off into highpq
     * Note that we're never allowed to block (except waiting for access
     * to a queue) since that would deadlock the system: nobody else is
     * going to do these jobs... but see the stuff above about the
     * MAINPROC_WAIT semaphore.
     */
    assert (queues != (jobs *)NULL);

    /* Everyone should be waiting for this queue to fill up */
    nwaiters = queues->main_waiters;
    DEBUG("nwaiters = %d\n", nwaiters);
    /* Some of them might not have made it here yet - nwaiters could be != NPROC */
    assert(queues->main_first == queues->main_last);

    /* Set up the current parameters in the shared memory */
    queues->main_first = queues->main_last = 0;
    queues->lowX = lowX;
    queues->highX = highX;
    queues->stepX = stepX;
    queues->oldStepX = oldStepX;
    
    queues->lowY = lowY;
    queues->highY = highY;
    queues->stepY = stepY;
    queues->oldStepY = oldStepY;
    
    queues->lowScaleX = lowScaleX;
    queues->highScaleX = highScaleX;
    queues->stepScaleX = stepScaleX;
    queues->oldStepScaleX = oldStepScaleX;
    
    queues->lowScaleY = lowScaleY;
    queues->highScaleY = highScaleY;
    queues->stepScaleY = stepScaleY;
    queues->oldStepScaleY = oldStepScaleY;
    
    queues->needdata = needdata;
    queues->thresh = thresh;

    queues->model = model;
    queues->image = image;

    /* Note that these may change, and so need to be pushed through */
    queues->model_thresh = model->model_thresh;
    queues->model_frac = model->model_frac;

    if (nexpand > 0) {
	queues->jobsize = (nexpand + NPROC - 1) / NPROC;
	}
    else {
	queues->jobsize = JOBCHUNK;
	}

    calcBoxes(model, stepX, stepY, stepScaleX, stepScaleY,
	      &box_width, &box_height);
    queues->box_width = box_width;
    queues->box_height = box_height;

    /* Make sure that we have the right dtrans before starting processing */
    if (!ensure_sdtrans(image, queues->box_width, queues->box_height,
		       model->model_thresh)) {
	goto bailout;
	}
    

    /* Enter the processing loop */
    while (TRUE) {
	/* Grab a lock so I can check the queue */
	DEBUG("main locking main\n");
	if (LOCK_MAIN_Q < 0) {
	    goto bailout;
	    }
	DEBUG("main locked main\n");
	nwaiters = semctl(semid, MAINPROC_WAIT, GETVAL, dummyarg);
	DEBUG("mainproc_wait val = %d\n", nwaiters);

	if (queues->main_first == queues->main_last) {
	    queues->main_first = queues->main_last = 0;
	    /* It's empty. If we have anything, feed it down it. */
	    if ((pqFirst(lowpq) == NULLPRIQNODE) ||
		((nexpand >= 0) && (nProbes >= nexpand))) {
		/*
		 * Nothing left. Everything has been fed down it.
		 * Now we just have to wait for everyone to finish processing.
		 */
		if (queues->main_waiters == NPROC) {
		    /* Everyone has. */
		    DEBUG("main finished\n");
		    DEBUG("main unlocking main\n");
		    if (UNLOCK_MAIN_Q < 0) {
			goto bailout;
			}
		    break;
		    }
		}
	    else {
		/* Read stuff off lowpq and put it into the main job queue */
		DEBUG("main filling main\n");
		for (pqn = pqFirst(lowpq);
		     ((pqn != NULLPRIQNODE) &&
		      ((nexpand < 0) || (nProbes < nexpand)) &&
		      (queues->main_last < Q_SIZE));
		     pqn = pqFirst(lowpq)) {
		    /* Get the hook from the first thing in the pq */
		    hook = (spqHook *)pqGet(pqn);
		    assert(hook != (spqHook *)NULL);
		    
		    /* and unhook it */
		    pqRem(pqn);
		    x = hook->x;
		    y = hook->y;
		    scaleX = hook->scaleX;
		    scaleY = hook->scaleY;
		    
		    /* and get rid of it */
		    free_spqHook(hook);
		    
		    /* and stick it on the queue */
		    queues->main[queues->main_last].whattodo = Q_WORK;
		    vhook = &(queues->main[queues->main_last].pqh);
		    vhook->x = x;
		    vhook->y = y;
		    vhook->scaleX = scaleX;
		    vhook->scaleY = scaleY;
		    queues->main_last++;
		    nProbes++;
		    }
		
		/* Wake up the sleepers */
		wakeup_main_q_op.sem_op = queues->main_waiters;
		queues->main_waiters = 0;
		DEBUG("main waking up %d on main\n", wakeup_main_q_op.sem_op);
		if (wakeup_main_q_op.sem_op > 0) {
		    if (WAKEUP_MAIN_Q < 0) {
			goto bailout;
			}
		    }
		}

	    }

	/* Release the lock */
	DEBUG("main unlocking main\n");
	if (UNLOCK_MAIN_Q < 0) {
	    goto bailout;
	    }

	/* Check to see if the secondary queue is full */

	DEBUG("main locking second\n");
	if (LOCK_SECOND_Q < 0) {
	    goto bailout;
	    }
	DEBUG("main locked second\n");
	nwaiters = semctl(semid, MAINPROC_WAIT, GETVAL, dummyarg);
	DEBUG("mainproc_wait val = %d\n", nwaiters);

	if (queues->second_last > 0) {
	    DEBUG("main reading second\n");
	    /* It's not empty. Read it out. */
	    assert(queues->second_first == 0);
	    for (i = 0; i < queues->second_last; i++) {
		/*
		 * If one of the subsidiary processes ran into a problem,
		 * it will stick a Q_DIE onto the secondary queue.
		 */
		if (queues->second[i].whattodo == Q_DIE) {
		    /* I don't want to think about cleanup here... */
		    if (UNLOCK_SECOND_Q < 0) {
			goto bailout;
			}
		    DEBUG("main argh... they got me...\n");
		    goto bailout;
		    }

		hook = malloc_spqHook();
		if (hook == (spqHook *)NULL) {
		    goto bailout;
		    }

		*hook = queues->second[i].pqh;

		if (pqAdd(highpq, (void *)hook) == NULLPRIQNODE) {
		    free_spqHook(hook);
		    goto bailout;
		    }
		}
	    
	    /* It's empty now. */
	    queues->second_first = queues->second_last = 0;
	    
	    /* Wake up people sleeping on it */
	    wakeup_second_q_op.sem_op = queues->second_waiters;
	    queues->second_waiters = 0;
	    DEBUG("main waking up %d on second\n", wakeup_second_q_op.sem_op);
	    if (wakeup_second_q_op.sem_op > 0) {
		if (WAKEUP_SECOND_Q < 0) {
		    goto bailout;
		    }
		}
	    }

	DEBUG("main unlocking second\n");
	if (UNLOCK_SECOND_Q < 0) {
	    goto bailout;
	    }

	nwaiters = semctl(semid, MAINPROC_WAIT, GETVAL, dummyarg);
	DEBUG("mainproc_wait val = %d\n", nwaiters);
	DEBUG("main waiting\n");
	/* Now wait for someone to ask for more work */
	if (MAINPROC_DO_WAIT < 0) {
	    goto bailout;
	    }
	DEBUG("main done wait\n");
	}

    /* Make sure that we didn't miss anything on the secondary queue */
    /* No lock required. Really. */
    assert(queues->main_waiters == NPROC);

    if (queues->second_last > 0) {
	DEBUG("main finally reading second\n");
	/* It's not empty. Read it out. */
	assert(queues->second_first == 0);
	for (i = 0; i < queues->second_last; i++) {
	    /*
	     * If one of the subsidiary processes ran into a problem,
	     * it will stick a Q_DIE onto the secondary queue.
	     */
	    if (queues->second[i].whattodo == Q_DIE) {
		/* I don't want to think about cleanup here... */
		DEBUG("main argh... they got me...\n");
		goto bailout;
		}
	    
	    hook = malloc_spqHook();
	    if (hook == (spqHook *)NULL) {
		goto bailout;
		}
	    
	    *hook = queues->second[i].pqh;
	    
	    if (pqAdd(highpq, (void *)hook) == NULLPRIQNODE) {
		free_spqHook(hook);
		goto bailout;
		}
	    }
	
	/* It's empty now. */
	queues->second_first = queues->second_last = 0;
	
	/* Nobody is sleeping on it. */
	/* Wake up people sleeping on it */
	assert(queues->second_waiters == 0);
	}
    
#endif

    return(TRUE);

  bailout:
    /* I have no idea how to clean up after this... */
    return(FALSE);

    }

#ifdef	MP
/*
 * This does much the same job as probeRegions, but as a subsidiary
 * process.
 *
 * It gets jobs from the main queue, runs them through ...Sampled() and
 * puts the results on the secondary queue. Of course, this is complicated
 * by the fact that the main queue could be empty, or the secondary one full,
 * or we could just run into some problem and have to about out...
 */
static void
mp_probeRegions(void)
{
    smodel_info *model;
    simage_info *image;
    int botX, botY, botSX, botSY;
    int topX, topY, topSX, topSY;
    int x, y, scaleX, scaleY;
    int nProbes = 0;
    spqHook *hook;
    volatile spqHook *vhook;
    PriQNode pqn;
    spqHook work[JOBCHUNK];
    int i;
    PriQ lowpq = NULLPRIQ;
    PriQ highpq = NULLPRIQ;
    boolean needdata;
    long thresh;
    int lowX, highX, stepX, oldStepX;
    int lowY, highY, stepY, oldStepY;
    int lowScaleX, highScaleX, stepScaleX, oldStepScaleX;
    int lowScaleY, highScaleY, stepScaleY, oldStepScaleY;
    unsigned box_width, box_height;
    int jobsize;

    assert(queues != (jobs *)NULL);

    /* Get my internal work queues */
    lowpq = pqNew(queues->cmpfunc);
    if (lowpq == NULLPRIQ) {
	goto bailout;
	}

    highpq = pqNew(queues->cmpfunc);
    if (highpq == NULLPRIQ) {
	goto bailout;
	}

    /* Now go to work */
    while (TRUE) {
	/* Get some jobs off the main queue */
	DEBUG("pid %d trying to lock main\n", mypid);
	if (LOCK_MAIN_Q < 0) {
	    goto bailout;
	    }
	DEBUG("pid %d locked main\n", mypid);

	while (queues->main_first == queues->main_last) {
	    /* It's empty. Release the lock and go to sleep */
	    DEBUG("pid %d presleeping on main\n", mypid);
	    queues->main_waiters++;

	    if (PRESLEEP_MAIN_Q < 0) {
		goto bailout;
		}
	    DEBUG("pid %d sleeping on main\n", mypid);
	    if (SLEEP_MAIN_Q < 0) {
		goto bailout;
		}
	    DEBUG("pid %d awake from main\n", mypid);

	    /* We're back - gotta check the queue again */
	    DEBUG("pid %d trying to relock main\n", mypid);
	    if (LOCK_MAIN_Q < 0) {
		goto bailout;
		}
	    DEBUG("pid %d relocked main\n", mypid);
	    }
	
	jobsize = MIN(JOBCHUNK, queues->jobsize);

	/* OK - there's something there and we're in charge. */
	for (i = 0; i < jobsize; i++) {
	    if (queues->main_first == queues->main_last) {
		break;
		}
	    /*
	     * Have we been told to die?
	     */
	    if (queues->main[queues->main_first].whattodo == Q_DIE) {
		/* Yup. Bang. */
		if (UNLOCK_MAIN_Q < 0) {
		    goto bailout;
		    }
		DEBUG("pid %d argh...\n", mypid);
		return;
		}

	    assert(queues->main[queues->main_first].whattodo == Q_WORK);
	    vhook = &(queues->main[queues->main_first].pqh);
	    work[i].x = vhook->x;
	    work[i].y = vhook->y;
	    work[i].scaleX = vhook->scaleX;
	    work[i].scaleY = vhook->scaleY;
	    queues->main_first++;
	    }
	assert(i > 0);		/* We must have got something */
	nProbes = i;

	/* Get the new params */
	lowX = queues->lowX;
	highX = queues->highX;
	stepX = queues->stepX;
	oldStepX = queues->oldStepX;
	
	lowY = queues->lowY;
	highY = queues->highY;
	stepY = queues->stepY;
	oldStepY = queues->oldStepY;
	
	lowScaleX = queues->lowScaleX;
	highScaleX = queues->highScaleX;
	stepScaleX = queues->stepScaleX;
	oldStepScaleX = queues->oldStepScaleX;
	
	lowScaleY = queues->lowScaleY;
	highScaleY = queues->highScaleY;
	stepScaleY = queues->stepScaleY;
	oldStepScaleY = queues->oldStepScaleY;
	
	needdata = queues->needdata;
	thresh = queues->thresh;
	
	model = queues->model;
	image = queues->image;
	
	model->model_thresh = queues->model_thresh;
	model->model_frac = queues->model_frac;

	box_width = queues->box_width;
	box_height = queues->box_height;

	/* Got the work - release the lock */
	if (UNLOCK_MAIN_Q < 0) {
	    goto bailout;
	    }
	DEBUG("pid %d unlocked main\n", mypid);

	/* Make sure that we have the right dtrans */
	if (!ensure_sdtrans(image, box_width, box_height,
			   model->model_thresh)) {
	    goto bailout;
	    }
	
	/* Now do the work. */
	for (i = 0; i < nProbes; i++) {
	    /* Get a point */
	    x = work[i].x;
	    y = work[i].y;
	    scaleX = work[i].scaleX;
	    scaleY = work[i].scaleY;
	    
	    /* Make it into a region fit for ...Sampled */
	    convertPtToBox(x, y, scaleX, scaleY,
			   lowX, stepX, oldStepX,
			   lowY, stepY, oldStepY,
			   lowScaleX, stepScaleX, oldStepScaleX,
			   lowScaleY, stepScaleY, oldStepScaleY,
			   &botX, &topX, &botY, &topY,
			   &botSX, &topSX, &botSY, &topSY);
	    
	    /* and scan it */
	    if (!findSTransMtoISampled(image, model,
				       botX, topX, stepX,
				       botY, topY, stepY,
				       botSX, topSX, stepScaleX,
				       botSY, topSY, stepScaleY,
				       highpq, thresh, needdata)) {
		/* Something failed down the line */
		goto bailout;
		}
	    }

	/* We now have highpq. Read it out into the secondary queue */

	DEBUG("pid %d trying to lock second\n", mypid);
	if (LOCK_SECOND_Q < 0) {
	    goto bailout;
	    }
	DEBUG("pid %d locked second\n", mypid);

	while (pqFirst(highpq) != NULLPRIQNODE) {
	    DEBUG("pid %d writing to second\n", mypid);
	    for (pqn = pqFirst(highpq);
		 ((pqn != NULLPRIQNODE) &&
		  (queues->second_last < Q_SIZE));
		 pqn = pqFirst(highpq)) {
		
		/* Get the hook from the first thing in the pq */
		hook = (spqHook *)pqGet(pqn);
		assert(hook != (spqHook *)NULL);
		
		/* and unhook it */
		pqRem(pqn);
		
		/* Copy it onto the secondary queue */
		queues->second[queues->second_last].whattodo = Q_WORK;
		queues->second[queues->second_last].pqh = *hook;
		queues->second_last++;
		
		/* and get rid of it */
		free_spqHook(hook);
		}

	    if (pqFirst(highpq) != NULLPRIQNODE) {
		/* It's full and we have more to put on it. Take a nap. */
		while (queues->second_last == Q_SIZE) {
		    DEBUG("pid %d presleeping on second\n", mypid);
		    queues->second_waiters++;

		    if (PRESLEEP_SECOND_Q < 0) {
			goto bailout;
			}
		    
		    DEBUG("pid %d sleeping on second\n", mypid);
		    if (SLEEP_SECOND_Q < 0) {
			goto bailout;
			}
		    DEBUG("pid %d awake from second\n", mypid);

		    /* We're back - gotta check the queue again */
		    DEBUG("pid %d trying to relock second\n", mypid);
		    if (LOCK_SECOND_Q < 0) {
			goto bailout;
			}
		    DEBUG("pid %d relocked second\n", mypid);

		    }
		}
	    }

	/* Done with the readout */
	if (UNLOCK_SECOND_Q < 0) {
	    goto bailout;
	    }
	DEBUG("pid %d unlocked second\n", mypid);
	}

    DEBUG("pid %d returning normally\n", mypid);
    return;

  bailout:
    /* We should put a DIE message on the secondary queue. FIX. */

    DEBUG("pid %d returning abnormally\n", mypid);

    return;

    }
#endif


/*
 * Compute the scaled values for each point in a model, at each
 * relevant scale. Figure out what scales are relevant.
 *
 * Actually, we don't compute the scaled values; those get allocated
 * and computed as needed by ensure_xscales and ensure_yscales.
 */
static boolean
compute_scaled_pts(smodel_info *model)
{
    int i;

    assert(model != (smodel_info *)NULL);
    assert(model->pts != (point *)NULL);

    /* We need maxx and maxy */
    if (model->npts == 0) {
	/* People are so annoying - be arbitrary at them */
	model->maxx = model->maxy = 0;
	}
    else {
	/*
	 * Find the largest X and Y values.
	 * These determine how fine the rasterisation in scale will be.
	 */
	model->maxx = model->pts[0].x;
	model->maxy = model->pts[0].y;
	for (i = 1; i < model->npts; i++) {
	    model->maxx = MAX(model->maxx, model->pts[i].x);
	    model->maxy = MAX(model->maxy, model->pts[i].y);
	    }
	}

    /* Find the size of a high-res scale tick */
    model->scaleStepX = (1. - EPSILON / 10.) / (model->maxx);
    model->scaleStepY = (1. - EPSILON / 10.) / (model->maxy);

    /* Now figure out how many ticks there are in high-res, scale 0..1 */
    model->ntickX = (int)(ceil(1. / model->scaleStepX)) + 1;
    model->ntickY = (int)(ceil(1. / model->scaleStepY)) + 1;

    return(TRUE);
    }
    
/*
 * This routine shuffles the model's points, to make the initial sequence
 * a real random sampling.
 */
static void 
shuffle_model(smodel_info *model)
{
    int i;
    int j;
    int k;
    point p;
    int ii;

    for (i = 0; i < model->npts - 1; i++) {
	j = (random() % (model->npts - i)) + i;
	p = model->pts[i];
	model->pts[i] = model->pts[j];
	model->pts[j] = p;
	for (k = 0; k < model->nscaled_x; k++) {
	    if (model->scaled_x[k]) {
		ii = model->scaled_x[k][i];
		model->scaled_x[k][i] = model->scaled_x[k][j];
		model->scaled_x[k][j] = ii;
		}
	    }
	for (k = 0; k < model->nscaled_y; k++) {
	    if (model->scaled_y[k]) {
		ii = model->scaled_y[k][i];
		model->scaled_y[k][i] = model->scaled_y[k][j];
		model->scaled_y[k][j] = ii;
		}
	    }
	}
    }

/*
 * Here are the comparison functions which give various sorts of sorting to
 * the priority queues.
 */

/*
 * This one always returns FALSE, indicating that everything is equal. This
 * is used in the standard search.
 */
static boolean
falseCompare(void *a, void *b)
{
    return(FALSE);
    }

/*
 * This one is used in the find-any-match search. We want to search the
 * "most promising" areas first. These are defined by their quality estimates,
 * stored in hook->forward_frac.
 *
 * This should return TRUE if a is greater than (i.e. worse than) b.
 */
static boolean
findOneCompare(void *a, void *b)
{
    spqHook *ahook, *bhook;

    assert(a != (void *)NULL);
    assert(b != (void *)NULL);

    ahook = (spqHook *)a;
    bhook = (spqHook *)b;

    if (ahook->forward_frac < bhook->forward_frac) {
	return(TRUE);
	}
    else {
	return(FALSE);
	}
    }

/*
 * Same thing - use same estimate of quality. It's a separate function
 * because it might want to do something different sometime
 */
static boolean
findDistCompare(void *a, void *b)
{
    spqHook *ahook, *bhook;

    assert(a != (void *)NULL);
    assert(b != (void *)NULL);

    ahook = (spqHook *)a;
    bhook = (spqHook *)b;

    if (ahook->forward_frac < bhook->forward_frac) {
	return(TRUE);
	}
    else {
	return(FALSE);
	}
    }

/*
 * Same thing - use same estimate of quality. It's a separate function
 * because it might want to do something different sometime
 */
static boolean
findFracCompare(void *a, void *b)
{
    spqHook *ahook, *bhook;

    assert(a != (void *)NULL);
    assert(b != (void *)NULL);

    ahook = (spqHook *)a;
    bhook = (spqHook *)b;

    if (ahook->forward_frac < bhook->forward_frac) {
	return(TRUE);
	}
    else {
	return(FALSE);
	}
    }

/*
 * We wanted to find one match, and here it is. Actually, here it might be:
 * we have to verify it in the reverse distance. If it checks out, though,
 * we add it to model->trans and return TRUE: the search is over.
 */
static boolean
findOneMatch(simage_info *image, smodel_info *model, int revstyle, spqHook *hook)
{
    boolean checkrev;
    ListNode newln;
    stransval *newt;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(hook != (spqHook *)NULL);

    if (!checkOneSTransItoM(image, model,
			    model->image_thresh, model->image_frac, revstyle,
			    hook->x, hook->y, hook->scaleX, hook->scaleY,
			    &checkrev)) {
	/* Failed */
	return(TRUE);
	}

    if (checkrev) {
	/* It checked out in the reverse direction - we're done */
	assert(model->trans != NULLLIST);

	newt = malloc_strans();
	if (newt == (stransval *)NULL) {
	    /* On the verge of success... */
	    return(TRUE);
	    }

	newt->transpos.x = hook->x;
	newt->transpos.y = hook->y;
	newt->scalex = hook->scaleX * model->scaleStepX;
	newt->scaley = hook->scaleY * model->scaleStepY;
	newt->forward_val = hook->forward_val;
	newt->forward_frac = hook->forward_frac;
	newt->forward_frac2 = hook->forward_frac2;
	    
	newln = liAdd(model->trans, (void *)newt);
	if (newln == NULLLISTNODE) {
	    free_strans(newt);
	    return(TRUE);
	    }

	/* Done. */
	return(TRUE);
	}
    else {
	/* Didn't check out in the reverse direction - keep going */
	return(FALSE);
	}
    }

/*
 * If the match has a better forward distance than we had, and it
 * checks out in the reverse direction, then update model->model_thresh.
 * Otherwise, if it's as good in the forward distance, add it to model->trans.
 * Note that the parameters for the reverse distance depend on the results
 * for the forward distance
 */
static boolean
findDistMatch(simage_info *image, smodel_info *model, int revstyle, spqHook *hook)
{
    boolean checkrev;
    ListNode newln;
    stransval *newt;
    long reverse_thresh;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(hook != (spqHook *)NULL);

    /* Better than what we have? */
    if (hook->forward_val < model->model_thresh) {
	/* Verify in reverse direction */
	reverse_thresh = (long)(model->reverse_scale * hook->forward_val +
				model->reverse_offset);
	if (!checkOneSTransItoM(image, model,
				reverse_thresh, model->image_frac,
				revstyle,
				hook->x, hook->y, hook->scaleX, hook->scaleY,
				&checkrev)) {
	    /* Failed */
	    return(TRUE);
	    }

	if (checkrev) {
	    /* It checked out in the reverse direction - update */
	    model->model_thresh = hook->forward_val;
	    model->image_thresh = reverse_thresh;
	    }
	else {
	    /* Didn't check out - ignore it */
	    return(FALSE);
	    }
	}
    else if (hook->forward_val > model->model_thresh) {
	/*
	 * This can happen: a bunch of matches are generated at once, then
	 * fed one at a time into this function. Suppose one of the first ones
	 * lowers the threshold - later ones will then be above thresh.
	 * They should be ignored.
	 */
	return(FALSE);
	}
    
    /* Hook it onto model->trans */
    assert(model->trans != NULLLIST);
    
    newt = malloc_strans();
    if (newt == (stransval *)NULL) {
	/* On the verge of success... */
	return(TRUE);
	}
    
    newt->transpos.x = hook->x;
    newt->transpos.y = hook->y;
    newt->scalex = hook->scaleX * model->scaleStepX;
    newt->scaley = hook->scaleY * model->scaleStepY;
    newt->forward_val = hook->forward_val;
    newt->forward_frac = hook->forward_frac;
    newt->forward_frac2 = hook->forward_frac2;
    
    newln = liAdd(model->trans, (void *)newt);
    if (newln == NULLLISTNODE) {
	free_strans(newt);
	return(TRUE);
	}

    /* Done. Continue the search */
    return(FALSE);
    }

/*
 * Basically the same as above, but improve based on fraction.
 * Store the current parameters in t->reverse_val and t->reverse_frac since
 * the final match verification and evaluation needs to know them.
 */
static boolean
findFracMatch(simage_info *image, smodel_info *model, int revstyle, spqHook *hook)
{
    boolean checkrev;
    ListNode newln;
    stransval *newt;
    double reverse_frac;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);
    assert(hook != (spqHook *)NULL);

    /* Better than what we have? */
    if (hook->forward_frac > model->model_frac) {
	/* Verify in reverse direction */
	reverse_frac = model->reverse_scale * hook->forward_frac +
	    model->reverse_offset;
	if (!checkOneSTransItoM(image, model,
				model->image_thresh, reverse_frac,
				revstyle,
				hook->x, hook->y, hook->scaleX, hook->scaleY,
				&checkrev)) {
	    /* Failed */
	    return(TRUE);
	    }

	if (checkrev) {
	    /* It checked out in the reverse direction - update */
	    model->model_frac = hook->forward_frac;
	    model->image_frac = reverse_frac;
	    }
	else {
	    /* Didn't check out - kill it */
	    return(FALSE);
	    }
	}
    else if (hook->forward_frac < model->model_frac) {
	return(FALSE);
	}
    
    /* Hook it onto model->trans */
    assert(model->trans != NULLLIST);
    
    newt = malloc_strans();
    if (newt == (stransval *)NULL) {
	/* On the verge of success... */
	return(TRUE);
	}
    
    newt->transpos.x = hook->x;
    newt->transpos.y = hook->y;
    newt->scalex = hook->scaleX * model->scaleStepX;
    newt->scaley = hook->scaleY * model->scaleStepY;
    newt->forward_val = hook->forward_val;
    newt->forward_frac = hook->forward_frac;
    newt->forward_frac2 = hook->forward_frac2;
    
    newt->reverse_val = hook->model_thresh;
    newt->reverse_frac = hook->model_frac;

    newln = liAdd(model->trans, (void *)newt);
    if (newln == NULLLISTNODE) {
	free_strans(newt);
	return(TRUE);
	}

    /* Done. Continue the search */
    return(FALSE);
    }


/*
 * Find out how big the box we're working in is (borders are inclusive
 * on the low side, exclusive on the high side)
 * and calculate lowX, highX, stepX, ...Y, ...ScaleX, ...ScaleY for
 * the lowest resolution.
 *
 * What we are determining here:
 * The absolute bounds on X are lowX..highX-1 inclusive. Ditto Y etc.
 * The lowest resolution cell is stepX by stepY by stepScaleX by
 * stepScaleY.
 * The first cell has its corner at (lowX, lowY, lowScaleX, lowScaleY).
 *
 * stepX etc are rounded to multiples of model->stepx and model->stepy.
 * This isn't entirely right for the scale dimensions - maybe fix later.
 *
 * We want the box to be as "square" as possible, since that maximises
 * the actual amount of the search space ruled out for the minimum possible
 * rise in threshold. We'd also like each box to be a power of REFINE,
 * as that means that cells at one level should be entirely contained in
 * a single cell at the next higher level, rather than overlapping as
 * many as 16 of them.
 */
static void
calcLowCells(simage_info *image, smodel_info *model,
	     int *lowX, int *highX, int *stepX,
	     int *lowY, int *highY, int *stepY,
	     int *lowScaleX, int *highScaleX, int *stepScaleX,
	     int *lowScaleY, int *highScaleY, int *stepScaleY)
{
    int minStep;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);

    assert(lowX != (int *)NULL);
    assert(highX != (int *)NULL);
    assert(stepX != (int *)NULL);

    assert(lowY != (int *)NULL);
    assert(highY != (int *)NULL);
    assert(stepY != (int *)NULL);

    assert(lowScaleX != (int *)NULL);
    assert(highScaleX != (int *)NULL);
    assert(stepScaleX != (int *)NULL);

    assert(lowScaleY != (int *)NULL);
    assert(highScaleY != (int *)NULL);
    assert(stepScaleY != (int *)NULL);

    *lowX = -model->leftborder;
    *highX = (int)image->xsize + model->rightborder;
    *stepX = MAX(model->stepx, (*highX - *lowX) / SLICE);
    *stepX = MAX(MINTOPSTEP, *stepX);
    
    *lowY = -model->topborder;
    *highY = (int)image->ysize + model->bottomborder;
    *stepY = MAX(model->stepy, (*highY - *lowY) / SLICE);
    *stepY = MAX(MINTOPSTEP, *stepY);

    *lowScaleX = ceil(model->minscalex / model->scaleStepX - EPSILON);
    *highScaleX = floor(model->maxscalex / model->scaleStepX + EPSILON) + 1;
    *stepScaleX = MAX(model->stepx, (*highScaleX - *lowScaleX) / SLICE);
    *stepScaleX = MAX(MINTOPSTEP, *stepScaleX);

    *lowScaleY = ceil(model->minscaley / model->scaleStepY - EPSILON);
    *highScaleY = floor(model->maxscaley / model->scaleStepY + EPSILON) + 1;
    *stepScaleY = MAX(model->stepy, (*highScaleY - *lowScaleY) / SLICE);
    *stepScaleY = MAX(MINTOPSTEP, *stepScaleY);

    /* Make all the steps the same, if possible */
    minStep = MIN(*stepX, *stepY);
    minStep = MIN(minStep, *stepScaleX);
    minStep = MIN(minStep, *stepScaleY);

    *stepX = MAX(model->stepx, minStep);
    *stepY = MAX(model->stepy, minStep);
    *stepScaleX = MAX(model->stepx, minStep);
    *stepScaleY = MAX(model->stepy, minStep);

    /* Now, make each one a power of REFINE times model->stepx etc */
    *stepX = roundlog(*stepX, REFINE, model->stepx);
    *stepY = roundlog(*stepY, REFINE, model->stepy);
    *stepScaleX = roundlog(*stepScaleX, REFINE, model->stepx);
    *stepScaleY = roundlog(*stepScaleY, REFINE, model->stepy);

    }

/*
 * Given the cell steps at one level, compute the cell steps at
 * the next finer level.
 */
static void
updateCellSteps(smodel_info *model,
		int stepX, int stepY,
		int stepScaleX, int stepScaleY,
		int *newStepX, int *newStepY,
		int *newStepScaleX, int *newStepScaleY)
{
    assert(model != (smodel_info *)NULL);
    assert(newStepX != (int *)NULL);
    assert(newStepY != (int *)NULL);
    assert(newStepScaleX != (int *)NULL);
    assert(newStepScaleY != (int *)NULL);

    *newStepX = MAX(model->stepx, stepX / REFINE);
    *newStepX = (*newStepX / model->stepx) * model->stepx;
	
    *newStepY = MAX(model->stepy, stepY / REFINE);
    *newStepY = (*newStepY / model->stepy) * model->stepy;

    *newStepScaleX = MAX(model->stepx, stepScaleX / REFINE);
    *newStepScaleX = (*newStepScaleX / model->stepx) * model->stepx;

    *newStepScaleY = MAX(model->stepy, stepScaleY / REFINE);
    *newStepScaleY = (*newStepScaleY / model->stepy) * model->stepy;
    }

/*
 * We want to know: if the transformation at the top top left left
 * corner of a cell places a point at (x, y), where can the
 * transformation at the bottom bottom right right corner put the same
 * point? The answer is
 * (x + stepX + stepScaleX - 2 * model->stepx,
 *  y + stepY + stepScaleY - 2 * model->stepy).
 * We therefore use these numbers to build the box_dtrans.
 */
static void
calcBoxes(smodel_info *model, int stepX, int stepY,
	  int stepScaleX, int stepScaleY,
	  unsigned *box_width, unsigned *box_height)
{
    assert(model != (smodel_info *)NULL);
    assert(box_width != (unsigned *)NULL);
    assert(box_height != (unsigned *)NULL);

    *box_width = stepX + stepScaleX - 2 * model->stepx + 1;
    *box_height = stepY + stepScaleY - 2 * model->stepy + 1;
    }

/*
 * This routine takes a point in transformation space, representing a box
 * oldStepX by oldStepY by..., and converts it into a region in
 * transformation space, tiled by boxes of stepX by stepX by ...
 * which covers that box.
 */
static void
convertPtToBox(int x, int y, int scaleX, int scaleY,
	       int lowX, int stepX, int oldStepX,
	       int lowY, int stepY, int oldStepY,
	       int lowScaleX, int stepScaleX, int oldStepScaleX,
	       int lowScaleY, int stepScaleY, int oldStepScaleY,
	       int *botX, int *topX, int *botY, int *topY,
	       int *botSX, int *topSX,
	       int *botSY, int *topSY)
{
    assert(botX != (int *)NULL);
    assert(topX != (int *)NULL);
    assert(botY != (int *)NULL);
    assert(topY != (int *)NULL);
    assert(botSX != (int *)NULL);
    assert(topSX != (int *)NULL);
    assert(botSY != (int *)NULL);
    assert(topSY != (int *)NULL);

    /* The point is the top top left left corner of the box */
    *topX = *botX = x;
    *topY = *botY = y;
    *topSX = *botSX = scaleX;
    *topSY = *botSY = scaleY;
    
    /*
     * We now have a rectangular region, all of which was interesting,
     * in (botX..topX)(botY..topY)(botSX..topSX)(botSY..topSY). Now,
     * these are actually top-left-corner of cell coordinates. We
     * want to end up with botX being the lowest X coordinate in the
     * region that we want scanned, and topX being one more than the
     * highest one: we want to check transformations which have X values
     * in the botX..topX-1 range; find...Sampled will actually check
     * ones spaced every stepX apart, starting at botX.
     *
     * However, we must ensure that each resolution level has consistent
     * coordinates: no matter where the current region of interest is,
     * we want to have the higher-resolution cells we generate be
     * on spacings which are consistent with all other high-resolution
     * cells we may generate for other regions of interest. This is why
     * we need lowX etc: these are the "origin" of the multi-resolution grid.
     */
    
    /* Now compensate for the cell coordinate grid */
    /*
     * Round down botX to be a multiple of stepX above lowX, and
     * round topX to be a multiple of stepX above botX. This ensures
     * that everything will line up with cells generated for other regions
     * of interest.
     * We want topX to be the first translation *not* considered at higher
     * resolution, so we make it one cell higher than we would if
     * it were the inclusive limit.
     * We also must increase topX by oldStepX - 1, since that
     * is the highest actual translation considered in this region of
     * interest; we then round it down so that it is the left corner
     * of the cell containing that translation.
     */
    *botX -= (*botX - lowX) % stepX;
    *topX += oldStepX + stepX - 1;
    *topX -= (*topX - lowX) % stepX;
    
    *botY -= (*botY - lowY) % stepY;
    *topY += oldStepY + stepY - 1;
    *topY -= (*topY - lowY) % stepY;
    
    *botSX -= (*botSX - lowScaleX) % stepScaleX;
    *topSX += oldStepScaleX + stepScaleX - 1;
    *topSX -= (*topSX - lowScaleX) % stepScaleX;
    
    *botSY -= (*botSY - lowScaleY) % stepScaleY;
    *topSY += oldStepScaleY + stepScaleY - 1;
    *topSY -= (*topSY - lowScaleY) % stepScaleY;
    }

/*
 * Are these translation parameters in the valid range?
 */
static boolean
inrange(simage_info *image, smodel_info *model,
	int x, int y, int xs, int ys)
{
    double xscale, yscale;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);

    xscale = xs * model->scaleStepX;
    yscale = ys * model->scaleStepY;

    if ((y < -model->topborder) || (y < model->mintransy)) {
	return(FALSE);
	}

    if (((int)(y + yscale * (model->ysize - 1) + .5) >=
	 (int)image->ysize + model->bottomborder) ||
	(y > model->maxtransy)) {
	return(FALSE);
	}

    if ((x < -model->leftborder) || (x < model->mintransx)) {
	return(FALSE);
	}

    if (((int)(x + xscale * (model->xsize - 1) + .5) >=
	 (int)image->xsize + model->rightborder) ||
	(x > model->maxtransx)) {
	return(FALSE);
	}

    if ((yscale < model->minscaley - EPSILON) ||
	(yscale > model->maxscaley + EPSILON)) {
	return(FALSE);
	}

    if ((xscale < model->minscalex - EPSILON) ||
	(xscale > model->maxscalex + EPSILON)) {
	return(FALSE);
	}

    if ((xscale > model->maxskew * yscale + EPSILON) ||
	(yscale > model->maxskew * xscale + EPSILON)) {
	return(FALSE);
	}

    return(TRUE);
    }

/*
 * Round x down to the nearest number of the form n^k * m
 */
static int
roundlog(int x, int n, int m)
{
    int k = 0;
    int oldx = x;

    assert(n > 0);
    assert(m > 0);
    assert(x >= m);

    for (x = x / m; x; x /= n) {
	k++;
	}
    k--;

    for (x = m; k; x *= n, k--) {
	}

    assert(x % m == 0);
    assert(x <= oldx);

    return(x);
    }

#ifdef	MP

/*
 * Start up the multiprocessing system.
 *
 * Get the shared memory and semaphores. Initialise both.
 * Generate all the box dtranses we're likely to need (this is mainly to
 * save memory rather than time, as they're large and it would be nice
 * if they could be shared).
 * Fork. All the children go immediately into mp_probeRegions.
 * Set up a signal handler which will kill the children on receipt of
 * any number of different signals.
 */
static boolean
mp_startup(simage_info *image, smodel_info *model, int forwstyle)
{
    int lowX, highX, stepX, oldStepX;
    int lowY, highY, stepY, oldStepY;
    int lowScaleX, highScaleX, stepScaleX, oldStepScaleX;
    int lowScaleY, highScaleY, stepScaleY, oldStepScaleY;
    unsigned box_width, box_height;
    int i;
    union semun arg;

    assert(image != (simage_info *)NULL);
    assert(model != (smodel_info *)NULL);

    /* Make sure everything is in a known state */
    shmid = -1;
    shmem = (void *)NULL;

    semid = -1;

    for (i = 0; i < NPROC; i++) {
	pid[i] = -1;
	}

    /* Create the shared memory segment */
    shmid = shmget(IPC_PRIVATE, SHM_SIZE, IPC_CREAT | 0777);
    if (shmid < 0) {
	goto bailout;
	}
    DEBUG("Got shmid %d\n", shmid);

    /* and attach to it */
    shmem = shmat(shmid, (void *)0, 0);
    /* Is the man page wrong or is shmat **really** brain dead?? */
    if (shmem == (void *)-1) {
	shmem = (void *)NULL;
	goto bailout;
	}
    queues = (volatile jobs *)shmem;
    DEBUG("Queues mapped at %lx\n", (unsigned long)queues);

    /* Create the semaphores */
    semid = semget(IPC_PRIVATE, NSEMS, IPC_CREAT | 0777);
    if (semid < 0) {
	goto bailout;
	}
    DEBUG("Got semid %d\n", semid);

    /* Set the semaphores to the right values */
    arg.array = semarr;
    if (semctl(semid, 0, SETALL, arg) < 0) {
	goto bailout;
	}

    /* Generate a bunch of box dtranses. Ugly, but speed hacks are like that */
    calcLowCells(image, model,
		 &lowX, &highX, &stepX, &lowY, &highY, &stepY,
		 &lowScaleX, &highScaleX, &stepScaleX,
		 &lowScaleY, &highScaleY, &stepScaleY);

    /* Get the top-level box sizes */
    calcBoxes(model, stepX, stepY, stepScaleX, stepScaleY,
	      &box_width, &box_height);

    /* and make the boxdtrans */
    if (!ensure_sdtrans(image, box_width, box_height, model->model_thresh)) {
	goto bailout;
	}

    while ((stepX > model->stepx) || (stepY > model->stepy) ||
	   (stepScaleX > model->stepx) || (stepScaleY > model->stepy)) {
	/* update stepX etc. keep old ones in oldStepX etc */
	oldStepX = stepX;
	oldStepY = stepY;
	oldStepScaleX = stepScaleX;
	oldStepScaleY = stepScaleY;

	updateCellSteps(model,
			oldStepX, oldStepY, oldStepScaleX, oldStepScaleY,
			&stepX, &stepY, &stepScaleX, &stepScaleY);
	/* Update the threshold */
	calcBoxes(model, stepX, stepY, stepScaleX, stepScaleY,
		  &box_width, &box_height);

	if (!ensure_sdtrans(image, box_width, box_height, model->model_thresh)) {
	    goto bailout;
	    }
	}


    /* Initialise the queues and other params */
    queues->main_first = queues->main_last = 0;
    queues->main_waiters = 0;
    queues->second_first = queues->second_last = 0;
    queues->second_waiters = 0;
    queues->jobsize = 1;

    /* We need to set up cmpfunc. I don't like doing it this way. */
    switch(forwstyle) {
      case FORWARD_ALL:
      case FORWARD_ALL | FORWARD_HILLCLIMB: {
	queues->cmpfunc = falseCompare;
	break;
	}

      case FORWARD_ONE:
      case FORWARD_ONE | FORWARD_HILLCLIMB: {
	queues->cmpfunc = findOneCompare;
	break;
	}

      case FORWARD_BESTDIST: {
	queues->cmpfunc = findDistCompare;
	break;
	}

      case FORWARD_BESTFRAC: {
	queues->cmpfunc = findFracCompare;
	break;
	}

      default: {
	panic("bad forwstyle");
	break;
	}

      }

    /* Now fork. */
    for (i = 0; i < NPROC; i++) {
	mynum = i;
	pid[i] = fork();
	if (pid[i] == -1) {
	    goto bailout;
	    }

	if (pid[i] == 0) {
	    mypid = getpid();
	    DEBUG("pid %d starting\n", mypid);
	    /* We're a new child */
	    mp_probeRegions();

	    /* When this returns, we're done. */
	    DEBUG("pid %d exiting\n", mypid);
	    exit(0);
	    }

	DEBUG("Forked pid %d\n", pid[i]);
	}
    mynum = -1;

    /* Set up the signal handler */
    if (signal(SIGINT, killall) == (void (*)())-1) {
	goto bailout;
	}

    return(TRUE);

  bailout:

    /* Free shared mem stuff */
    if (semid != -1) {
	(void)semctl(semid, 0, IPC_RMID, dummyarg);
	}

    if (shmem != (void *)NULL) {
	assert(shmid != -1);
	(void)shmdt(shmem);
	}

    if (shmid != -1) {
	(void)shmctl(shmid, IPC_RMID, (struct shmid_ds *)NULL);
	}

    /* Shoot any children that exist */
    for (i = 0; i < NPROC; i++) {
	if (pid[i] > 0) {
	    kill(pid[i], SIGKILL);
	    }
	}

    return(FALSE);

    }

/*
 * Shut down the subprocesses, with extreme prejudice.
 * Get rid of persistent resources (semaphores, shared memory).
 * Make it all a closed case.
 *
 * Note that state at this point is pretty undefined, as we've taken
 * this signal async.
 */
static void
killall(int sig, int code, struct sigcontext *scp, char *addr)
{
    int i;

    /* Shoot any children that exist */
    for (i = 0; i < NPROC; i++) {
	if (pid[i] > 0) {
	    kill(pid[i], SIGKILL);
	    }
	}

    /* I'm not sure what to do about errors from these... */
    (void)semctl(semid, 0, IPC_RMID, dummyarg);
    (void)shmdt(shmem);
    (void)shmctl(shmid, IPC_RMID, (struct shmid_ds *)NULL);

    /*
     * I can't live with the consequences of what I've done. Goodbye, cruel
     * world...
     */
    exit(1);
    }

/*
 * Shut down the multiprocessing system.
 * Feed some Q_DIEs down the main queue, and wait for the children
 * to knock themselves off.
 * Delete the semaphores and the shared memory.
 */
static void
mp_shutdown(void)
{
    int i;
    int nwaiters;
    int status;
    int deadpid;

    assert(queues != (jobs *)NULL);

    /*
     * They must all be sleeping on the main queue. Sleeping like babies.
     * Little do they know what evil fate awaits them...
     */
    nwaiters = queues->main_waiters;
    assert(nwaiters == NPROC);
    assert(queues->main_first == queues->main_last);

    for (i = 0; i < NPROC; i++) {
	queues->main[i].whattodo = Q_DIE;
	}
    queues->main_last = NPROC;
    queues->main_first = 0;

    DEBUG("killer waking up %d on main\n", NPROC);
    wakeup_main_q_op.sem_op = NPROC;
    if (WAKEUP_MAIN_Q < 0) {
	/* Oh dear. Martha, where did we put that machine gun? */
	for (i = 1; i < NPROC; i++) {
	    kill(pid[i], SIGKILL);
	    }
	}

    for (i = 0; i < NPROC; i++) {
	if ((deadpid = wait(&status)) < 0) {
	    /* Funny, I thought we had children... */
	    break;
	    }
	DEBUG("child %d is dead with status %d\n", deadpid, status);
	/* Do something with status? */
	}

    /* OK - they're all dead. Dead, I tell you, Dead! */

    /* I'm not sure what to do about errors from these... */
    (void)semctl(semid, 0, IPC_RMID, dummyarg);
    (void)shmdt(shmem);
    (void)shmctl(shmid, IPC_RMID, (struct shmid_ds *)NULL);
    (void)signal(SIGINT, SIG_DFL);
    }

#endif
