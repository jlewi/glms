/*
 *
 * $Header$
 *
 * Copyright (c) 1992, 1993 Cornell University.  All Rights Reserved.  
 *  
 * Use, reproduction, preparation of derivative works, and distribution
 * of this software is permitted.  Any copy of this software or of any
 * derivative work must include both the above copyright notice of
 * Cornell University and this paragraph.  Any distribution of this
 * software or derivative works must comply with all applicable United
 * States export control laws.  This software is made available AS IS,
 * and CORNELL UNIVERSITY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE, AND NOTWITHSTANDING ANY OTHER
 * PROVISION CONTAINED HEREIN, ANY LIABILITY FOR DAMAGES RESULTING FROM
 * THE SOFTWARE OR ITS USE IS EXPRESSLY DISCLAIMED, WHETHER ARISING IN
 * CONTRACT, TORT (INCLUDING NEGLIGENCE) OR STRICT LIABILITY, EVEN IF
 * CORNELL UNIVERSITY IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

#ifndef	TRANSHASH_H

#define	TRANSHASH_H

#include "list.h"
#include "misc.h"

typedef struct {
    int lowX, highX, stepX;
    int lowY, highY, stepY;
    int nlist;			/* How many buckets there are */
    List *listarr;		/* The buckets themselves */
    int lowbucket;		/* and the lowest non-empty one */
    } TransHash;

extern TransHash *thNew(int lowX, int highX, int stepX,
			int lowY, int highY, int stepY);
extern void thFree(TransHash *th);
extern boolean thAdd(TransHash *th, int x, int y, void *userdata);
extern void thRem(TransHash *th, int x, int y);
extern boolean thIsIn(TransHash *th, int x, int y);
extern boolean thGetLowest(TransHash *th, int *x, int *y);
extern void *thGet(TransHash *th, int x, int y);

#endif
