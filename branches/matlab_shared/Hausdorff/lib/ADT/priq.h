/*
 *
 *	$Header$
 *
 * Copyright (c) 1990, 1991, 1992, 1993 Cornell University.  All Rights
 * Reserved.
 *
 * Copyright (c) 1991, 1992 Xerox Corporation.  All Rights Reserved.
 *
 * Use, reproduction and preparation of derivative works of this software is
 * permitted.  Any copy of this software or of any derivative work must
 * include both the above copyright notices of Cornell University and Xerox
 * Corporation and this paragraph.  This software is made available AS IS, and
 * XEROX CORPORATION DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
 * WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE, AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED
 * HEREIN, ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF XEROX CORPORATION IS ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGES.
 */
/*
 * priq.h - header file for the priority queue module
 */
#ifndef PRIQ_H
#define PRIQ_H

/*
 * Types. These types are opaque. You can't see these, really...
 */
typedef struct {
    struct _PriQIntNode *root, *last;
    boolean (*compareGT)(void *a, void *b);
    unsigned count;
    } *PriQ;

typedef struct _PriQIntNode {
    struct _PriQIntNode *left, *right, *parent, *next, *prev;
    struct _PriQNode *owner;
    void *userdata;
    } *PriQIntNode;

typedef struct _PriQNode {
    PriQIntNode val;
    PriQ owner;
    } *PriQNode;

/*
 * Constants
 */
#define	NULLPRIQ	((PriQ)NULL)
#define	NULLPRIQNODE	((PriQNode)NULL)

/*
 * Functions
 */
extern PriQ pqNew(boolean (*compareGT)(void *a, void *b));
extern void pqFree(PriQ p);
extern PriQNode pqFirst(PriQ p);
extern PriQNode pqAdd(PriQ p, void *userdata);
extern void pqRem(PriQNode pqn);
extern void *pqGet(PriQNode pqn);
extern unsigned int pqLen(PriQ p);

#endif
