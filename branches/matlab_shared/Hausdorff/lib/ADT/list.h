/*
 *
 *	$Header$
 *
 * Copyright (c) 1990, 1991, 1992, 1993 Cornell University.  All Rights
 * Reserved.
 *
 * Copyright (c) 1991, 1992 Xerox Corporation.  All Rights Reserved.
 *
 * Use, reproduction and preparation of derivative works of this software is
 * permitted.  Any copy of this software or of any derivative work must
 * include both the above copyright notices of Cornell University and Xerox
 * Corporation and this paragraph.  This software is made available AS IS, and
 * XEROX CORPORATION DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
 * WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE, AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED
 * HEREIN, ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF XEROX CORPORATION IS ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGES.
 */
/*
 * list.h - header file for the list module
 */
#ifndef LIST_H
#define LIST_H

/*
 * Types. These types are opaque. You can't see these, really...
 */
/*
 * Complain, complain: recursive types are messy in C.
 * Note that one structure has to be named, not anonymous.
 */
typedef struct {
    struct _ListNode *first, *last;
    } *List;

struct _ListNode {
    struct _ListNode *next, *prev;
    List owner;
    void *userdata;
    };
typedef struct _ListNode *ListNode;

/*
 * Constants
 */
#define	NULLLIST	((List)NULL)
#define	NULLLISTNODE	((ListNode)NULL)

/*
 * Functions
 */
extern List liNew(void);
extern void liFree(List l);
extern ListNode liFirst(List l);
extern ListNode liNext(ListNode ln);
extern ListNode liAdd(List l, void *userdata);
extern void liRem(ListNode ln);
extern ListNode liRemAndNext(ListNode ln);
extern void *liGet(ListNode ln);
extern ListNode liIsIn(List l, void *userdata);
extern unsigned int liLen(List l);
extern void liApp(List l1, List l2);

/*
 * Macros
 */
#define foreach(ln, l) for((ln) = liFirst((l)); (ln) != NULLLISTNODE; (ln) = liNext((ln)))
#define forafter(ln1, ln2) for ((ln1) = liNext(ln2); (ln1) != NULLLISTNODE; (ln1) = liNext((ln1)))

#endif
