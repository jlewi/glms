/*
 *
 *	$Header$
 *
 * Copyright (c) 1990, 1991, 1992, 1993 Cornell University.  All Rights
 * Reserved.
 *
 * Copyright (c) 1991, 1992 Xerox Corporation.  All Rights Reserved.
 *
 * Use, reproduction and preparation of derivative works of this software is
 * permitted.  Any copy of this software or of any derivative work must
 * include both the above copyright notices of Cornell University and Xerox
 * Corporation and this paragraph.  This software is made available AS IS, and
 * XEROX CORPORATION DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
 * WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE, AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED
 * HEREIN, ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF XEROX CORPORATION IS ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGES.
 */
/*
 * chunk.h - header file for the chunk module
 */
#ifndef CHUNK_H
#define CHUNK_H

/*
 * Types. These types are opaque. You can't see these, really...
 */
typedef struct {
    struct chunkBit *freelist;
    unsigned size;
    } *Chunk;

struct chunkBit {
    struct chunkBit *next;
    };

/*
 * Constants
 */
#define	NULLCHUNK	((Chunk)NULL)

/*
 * Functions
 */
extern Chunk chNew(unsigned size);
extern void *chAlloc(Chunk c);
extern void chFree(Chunk c, void *p);

/*
 * Macros
 */
#define foreach(ln, l) for((ln) = liFirst((l)); (ln) != NULLLISTNODE; (ln) = liNext((ln)))
#define forafter(ln1, ln2) for ((ln1) = liNext(ln2); (ln1) != NULLLISTNODE; (ln1) = liNext((ln1)))

#endif
