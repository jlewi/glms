/*
 *
 *	$Header$
 *
 */
/*
 * Xerox may use this code without restriction.
 */

/*
 * This file contains the _panic function.
 */

static char rcsid[] = "@(#)$Header$";

#include <stdio.h>
#include "misc.h"

void
_panic(char *why, char *rcs, int line, char *file)
{
    /*
     * "Burma!"
     * "Why did you say 'Burma!'?"
     * "I panicked."
     */
    fprintf(stderr, "Burma: %s in line %d of %s (%s)\n", why, line, file, rcs);
    exit(1);
    }
