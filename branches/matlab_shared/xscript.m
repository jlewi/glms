%function xscript(fname)
%   fname - name of a script whose path may not be on the matlab path
%
%Explanation: executes this script by switching to that directory and then
%cding back
function xscript(fname)

if isa(fname,'FilePath')
[fdir, fname, fext]=fileparts(getpath(fname));
else
[fdir, fname, fext]=fileparts(fname);
end
odir=pwd;

cd(fdir);

try
evalin('caller',[fname]);
catch e
    
    %if we threw an error still cd back to odir
        cd(odir);
%    if strcmp(e.identifier,'MATLAB:undefinedFunction')
        
    throw(e);
%    error(e);
end
    
cd(odir);
