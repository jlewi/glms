%rval=function fcrossing (x,y,z)
%   y - a vector of data
%       row vector
%   rval - pts at which y(x)=z
%
% Explanation:
%   to find y(x)=z, this function finds the zero crossings of y(x)-z
%   to find the zero crossings we look at where the sign(y(x)-z) 
%   changes from -1 to 1. We then linearly interpolate between 
%   these points
%
% Warnings: How it handles endpoints may not work so well
%           1-13-05 - doesn't it also need to look for points where sign
%           goes from 1 to -1 I think it does
% Changes:
%   2-28-05 - still not handling tangents correctly
%       Changed how I determine which points we are interpolating between 
%       in order to get find the zero.
%       I no longer convolve the sign with an averaging filter
%       the idea behind this was that a zero crossing would be represented
%       by a -1 1 for the sign. In reality you can have zeros in between
%       So what I do is find all points where sign(y-z)==0 and remove
%       these.
%       I keep track of the indexes to associate with these points
%       Now we only have 1 and -1 and after convolving with 1,-1
%       the zero points should indicate which points to use for
%       interpolation
%       THIS WILL NOT FIND POINTS WHICH ARE TANGENT TO Z
%       This also doesn't handle the case [-1 -1 -1 0 0 0 0 0 1 1 1 1 ]
%           not clear what you should return in this case
%   1-13-05 - smoothing was not using the padded version of ys
%          
function rval = fcrossing(x,y,z)
    
    dim=size(y);
    if (dim(1))>1;
        %its a row vector take transpose
        y=y';
    end
    if (length(x)~=length(y))
        fprintf('x and y need to be same length');
    end
    
    numones=2;                  %this should be 2 no m         
    filt=ones(1,numones);
    %subtract z and take the sign
    %therefore ys says whether we are above or below z
    %taking the sign means
    %   ys=1 if  y>z
    %   ys=0 if  y=0
    %   ys=-1 if y<z
    y=y-z;
    ys=sign(y);

    
    %2-28-05
    %shouldn't need to pad since ysconv does this by repeating the
    %values at the end of the string
    %yspad=[ones(1,numones-1)*ys(1), ys,ones(1,numones-1)*ys(end)];
    %ysconv=conv(yspad,filt);
    %average it to smooth out noise. I.e rapid flunctions around z
    %ysconv=movavg(ys,numones);   
    
    %remove points where ys is =0
    %we need to keep trac of the indexes associated with these points
    %so we can get the values for interpolation later on
    %before doing the convolution pad the ends of ys with whatever sign is
    %at the last value
    nzxpts=find(ys~=0); %stores the indexes to associate with each value in ys
    zc=ys(nzxpts);
    
    %find the zero points
    %zc points should indicate locations where sign changes from -1 to 1 or
    %1 to -1. so conv it with 1,1
    zc=movavg(zc,2);
    
    ind=find(zc==0);    %index is not the index for the original yvalues
    rval=zeros(1,length(ind));
    for j=1:length(ind)
        zcindex=ind(j);               %index into nzxpts             
        index=nzxpts(zcindex);      %index into original matrix
        %2-28-2005
        %not sure how to handle case where y(n)==0 
        %check if the point actually y(index)=z
        %if so just return x(index)
        %if y(index)==z
        %    rval(j)=x(index);
        %else
        %if its the first point
        %lindex= the index of the point on the left for interpolation
        %rindex=pt on the right for interpolation
        if index==1
            %interpolate using the slope from the next pt
            lindex=index;
            rindex=nzxpts(zcindex+1);
            m=(y(rindex)-y(lindex))/(x(rindex)-x(lindex));
           b=y(lindex)-m*x(lindex);
            rval(j)=-b/m;
        else
            %linearly interpolate
            %calculate the slop and intercept
            rindex=index;
            lindex=nzxpts(zcindex-1);
                m=(y(rindex)-y(lindex))/(x(rindex)-x(lindex));
            b=y(rindex)-m*x(rindex);
            rval(j)=-b/m;
        end
       
    end

 

    