%function kl=klgauss(p, q)
%   p -.m mean
%      .c covariance matrix
%   q -.m mean
%      .c covariance matrix
%
% Return value
%   D(p||q) kl distance in NATS between p and q
function kl=klgauss(p,q)

if ~isvector(p.m)
    error('p.m must be a vector');
end

if ~isvector(q.m)
    error('q.m must be a vector');
end

d=length(p.m);

if (size(p.c)~=[d d])
    error('p.c dimensions mismatch with p.m');
end

if (size(q.m)~=d)
    error('q.m not same dimension as p.m');
end

if (size(q.c)~=[d d])
    error('q.c dimensions mismatch with q.m');
end

%compute the entropy of p in nats
%hp=.5*d*(1+log(2*pi))+.5*log(det(p.c));
hp=.5*d+.5*log(det(p.c));
%the other term
iq=inv(q.c);
%hq=(d/2)*log(2*pi)+.5*log(det(q.c))+.5*trace(iq*p.c)+.5*p.m'*iq*p.m-p.m'*iq*q.m+.5*q.m'*iq*q.m;
hq=.5*log(det(q.c))+.5*trace(iq*p.c)+.5*p.m'*iq*(p.m-q.m)+.5*(-p.m+q.m)'*iq*q.m;
kl=hq-hp;