% function s= shpoisson(r,twin)
%   r - rate of the homogenous process (events/unit time)
%   twin - length of time window(unit time) in which to generate spikes
%   
%   Return value
%       s - vector of spike times
%
% Explanation: samples a homogenous poisson process with rate r.
%    The units of time for r and twin should be the same. 
%    The function only returns spikes which occur for time <=twin
%
function s=shpoisson (r,twin)

s=[];

%generate the spike times
%Eqn 1.38 in Dayan and Aboot (2001)

tspike=0;

while (tspike <twin)
    tspike=tspike-log(rand(1))/r;
    if (tspike <twin)
        s(length(s)+1)=tspike;
    end
end

