%function d=kldiv(p,q)
%   p - pdf 1
%   q - pdf 2
%
% Return value
%   d - kl divergence between the pdf's
function d=kldiv(p,q)

%add eps to avoid numerical error
lp=log(p+eps);
lq=log(q+eps);

d=p.*(lp-lq);

%we need to sum over all dimensions
d=sum(d(:));
