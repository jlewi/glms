%function clim=detclim(pvals,frac)
%   pvals - values to determine the range for
%   frac -  fraction of data that must be in this range
%
%Explanation: Finds the smallest range (clim) that contains
%   a fraction=frac of the data.
function clim=detclim(pvals,frac)
pvals=pvals(:);

%remove nan
ind=find(~isnan(pvals));
pvals=pvals(ind);

[spvals sind]=sort(pvals,'ascend');

dist=diff(spvals);

cdist=cumsum(dist);

wlen=ceil(frac*length(pvals));
%now we slide a window of wlen across wdist looking
%for window that has smallest range of values
wstart=1;
dlim=cdist(wlen);

for ind=2:(length(cdist)-wlen+1)
    %how big is the range in this window
    if ((cdist(ind+wlen-1)-cdist(ind))<dlim)
        wstart=ind;
        dlim=(cdist(ind+wlen-1)-cdist(ind));
    end
end

clim=spvals(1)+[cdist(wstart), cdist(wstart+wlen-1)];

% %get the center
% center=median(pvals);
% 
% %compute the absolute distance of each point from center
% dist=abs(pvals-center);
% 
% dist=sort(dist,'ascend');
% 
% 
% dv=dist(ceil(frac*length(pvals)));
% 
% clim=[center-dv; center+dv];