%function obj=setvoffset(obj,val)
%	 obj=LegendObj object
% 
%Return value: 
%	 obj= the modified object 
%
function obj=setvoffset(obj,val)
	 obj.linfo.voffset=val;
     obj=plotlegend(obj);
