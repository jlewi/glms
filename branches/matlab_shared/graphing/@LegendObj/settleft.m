%function obj=settleft(obj,val)
%	 obj=LegendObj object
% 
%Return value: 
%	 obj= the modified object 
%
%Explanation: tleft is the start coordinate in relative coordinates at
%which the text starts. 
function obj=settleft(obj,val)
	 obj.linfo.tleft=val;
     
     %replot the legend
     obj=plotlegend(obj);
