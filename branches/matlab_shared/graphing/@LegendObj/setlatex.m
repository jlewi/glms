%function obj=setlatex(obj,val)
%	 obj=LegendObj object
% 
%Use interpreter latex
function obj=setlatex(obj,val)
    obj.linfo.latex=true;
	for ind=1:length(obj.htxt)
       set(obj.htxt(ind),'interpreter','latex'); 
       
    end
