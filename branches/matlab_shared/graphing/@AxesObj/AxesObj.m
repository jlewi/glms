%function sim=ClassName(ha,fieldname, value,....)
%   ha - handle to the axes
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Template for the constructor of a new class. This template
% shows how we can identify which constructor was called based on the
% parameters that were passed in.
%
%Revisions:
%   07-23-2008 - use new object model and make it a handle
%       nrows, ncols are now stored as part of the figure object
%   03-12-2008 - added zlabel
classdef (ConstructOnLoad=true) AxesObj < handle

    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %  bname  - name of base class. This allows us to refer to it templates
    %
    %  ha     - handle to the object this represents.
    %  hc     - handle to the colorbar to associate with this axes
    %  hlgnd  - handle to the legend
    %  xlbl   - structures with info about xlabel
    %  ylbl   - structures with info about ylabel
    %  tlbl   - structure  with info about tlbl
    %
    %  p      - structure array identifying the different plots on this graph
    %           .hp  - handle to the plot
    %           .lbl - label for this plot
    %           .pstyle - structure array describing style for hp
    %declare the structure
    %p=struct('hp',[],'lbl',[],'pstyle',[]);

    properties(SetAccess=private,GetAccess=public)
        version=080723;
        ha=[];
        hlgnd=[];
        xlbl=[];
        ylbl=[];
        zlbl=[];
        tlbl=[];        
        p=[];
    end


    properties(SetAccess=public,GetAccess=public)        
        hc=[];
        
    end
    methods
        function obj=AxesObj(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            %match any constructor
            con(1).rparams={'ha'};
            con(1).cfun=1;




            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end

            %******************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 0  
                case 1
                    obj.ha=params.ha;
                otherwise
                    error('Constructor not implemented')
            end


          
            %assume only 1 axis
            fnames=fieldnames(params);
            for pind=1:length(fnames)
                switch lower(fnames{pind})
                    case {'xlabel','xlbl'}
                        obj=xlabel(obj,params.(fnames{pind}));
                    case {'ylabel','ylbl'}
                        obj=ylabel(obj,params.(fnames{pind}));
                    case {'title'}
                        obj=title(obj,params.(fnames{pind}));
                end
            end
        end
    end
end