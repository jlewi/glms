%function ylabel(obj,text,propname,val,probname,val,etc..)
%   propname - name of label property to set e.g. interpreter
%   val      - value for the property
%Explanation: create an xlabel
function obj=ylabel(obj,txt,varargin)
    obj.ylbl.h=ylabel(obj.ha,txt);
    obj.ylbl.txt=txt;
    
     if (nargin>1)
       for j=1:2:length(varargin)
          set(obj.ylbl.h,varargin{j},varargin{j+1}); 
       end
    end