%
function setpstyle(obj,pind,pstyle)

  obj.p(pind).pstyle=pstyle;
  
            fnames=fieldnames(pstyle);
            for f=1:length(fnames)
                if (~isempty(pstyle.(fnames{f})) && ~any(isnan(pstyle.(fnames{f}))))
                    set(obj.p(pind).hp,fnames{f},pstyle.(fnames{f}));
                end
            end