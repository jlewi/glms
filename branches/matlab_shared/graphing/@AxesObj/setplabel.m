%function setplbl(aobj,pind,lbl)
%   pind - index of the handle to the plot
%   lbl  - new lbl for the plot
%
%Explanation function to change the lbl of a plot
function setplabel(aobj,pind,lbl)
    aobj.p(pind).lbl=lbl;
    
    %update the legend. 
    if ~isempty(aobj.hlgnd)
       aobj.hlgnd.lbls{pind}=lbl;
       plotlegend(aobj.hlgnd);
    end