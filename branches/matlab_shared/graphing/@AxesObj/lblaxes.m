%function lblaxes(obj,dparam)
%   obj - AxesObj
%   dparam - structure specifying default parameter values
%       dparam will be overriden if the values of the axes 
%         have been specified as part of this axes.
%$Revision: 1492 $ - check for fields .width and .height use them to set
%   figure width if not use default values. This is useful for making fonts
%   show up correctly. This will cause problems with some of my other code
%
function [obj]=lblaxes(obj,dparam)

%if obj is an array of axes objects we call lbl axes on each object
if (numel(obj)>1)
   for aind=1:numel(obj)
      obj(aind)=lblaxes(obj(aind),dparam); 
   end
end
aparam=dparam;
%loop over the plots
for pind=1:length(obj.p)
    %set the font
    set(obj.ha,'FontName',aparam.fontname);
    set(obj.ha,'FontSize',aparam.fontsize);

    %adjust the marker if thereis one
    if ~isempty(obj.p(pind).hp)
        %override it if pstyle is specified
        if (isfield(obj.p(pind),'pstyle') && ~isempty(obj.p(pind).pstyle))
            %psyle is structure with whatever fields of the plot we want to
            %set
            fnames=fieldnames(obj.p(pind).pstyle);
            for f=1:length(fnames)
                if (~isempty(obj.p(pind).pstyle.(fnames{f})) && ~any(isnan(obj.p(pind).pstyle.(fnames{f}))))
                    set(obj.p(pind).hp,fnames{f},obj.p(pind).pstyle.(fnames{f}));
                end
            end
        else
            %use defaults
             set(obj.p(pind).hp,'MarkerSize',dparam.markersize);
             
             set(obj.p(pind).hp,'LineWidth',dparam.linewidth);
             [c m]=getptype(pind);
             try
                 %will throw error if not a lineseries and property color
                 %doesn't exist
            set(obj.p(pind).hp,'Color',c);
             catch
             end
        end        
    end
       
    %set the line style if one is specified
    if ~isempty(obj.p(pind).lstyle)
        setlstyle(obj.p(pind).hp,obj.p(pind).lstyle); 
    end
end


%*****************************************
%create a legend
%*****************************************

%get the handles for plots with legends
hpind=zeros(1,pind);
for pind=1:length(obj.p)
    if isfield(obj.p(pind),'lbl')
    if ~isempty(obj.p(pind).lbl)
       hpind(pind)=1; 
    end
    end
end
ind=find(hpind==1);

if ~isempty(ind)

hp=[obj.p(ind).hp];
lbls={obj.p(ind).lbl};

if (length(hp)==length(lbls))
%obj.hlgnd=legend(hp,lbls,'fontsize',aparam.fontsize,'location','best');
%create my type of legend
if isempty(obj.hlgnd)
    obj.hlgnd=LegendObj('hlines',hp,'lbls',lbls,'fontsize',aparam.fontsize);

end
else
   warning('Not displaying legend because number of lbls does not match number of handles.'); 
end
end
