%function obj=sethcposition(obj,left,bottom,width,height)
%
%
% Explanation: position the colorbars
%Return value: 
%	 obj= the modified object 
%
function obj=sethcposition(obj,left,bottom,width,height)


for rind=1:size(obj,1);
    for cind=1:size(obj,2)
        %posiiton the colorbar with there is one
         if ~isempty(obj(rind,cind).hc)
        pos=[left(cind) bottom(rind) width(cind) height(rind)];
        set(obj(rind,cind).hc,'position',pos);
         end
        
%         pos=[left(cind)+wplotact(cind)*(1+cb.stocb) bottom(rind) wplotact(cind)*cb.cbwidth hplot(rind)];
%         set(obj(rind,cind).hc,'position',pos);
%         end
    end
end