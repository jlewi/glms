%function getxlabel=xlabel(obj)
%	 obj=AxesObj object
% 
%Return value: 
%	 xlabel=obj.xlabel 
%
function xlabel=getxlabel(obj)
	 xlabel=obj.xlbl;
