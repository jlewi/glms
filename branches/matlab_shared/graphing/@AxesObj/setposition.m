%function obj=setposition(obj,left,bottom,wplotact,hplot)
%	 obj=AxesObj object
%    left - vector specifing left coordinate of each column
%    bottom - specifies bottom of each row
%    wplotact - width of each column
%    hplot  - height of each row
%Return value: 
%	 obj= the modified object 
%
function obj=setposition(obj,left,bottom,wplotact,hplot)


for rind=1:size(obj,1);
    for cind=1:size(obj,2)
        pos=[left(cind) bottom(rind) wplotact(cind) hplot(rind)];
        set(obj(rind,cind).ha,'position',pos);
        
        %posiiton the colorbar with there is one
%         if ~isempty(obj(rind,cind).hc)
%         pos=[left(cind)+wplotact(cind)*(1+cb.stocb) bottom(rind) wplotact(cind)*cb.cbwidth hplot(rind)];
%         set(obj(rind,cind).hc,'position',pos);
%         end
    end
end
