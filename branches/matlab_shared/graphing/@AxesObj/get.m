%function get=(obj,pname)
%	 obj=AxesObj object
% 
%Return value: 
%	 =obj. 
%
%   just call the get function for the axes
function p=get(obj,pname)
    
	 p=get([obj.ha],pname);
