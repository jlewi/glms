%function getborder=border(obj)
%	 obj=AxesObj object or array of objects
%Return value:
%	 border=structure specifying how much border we need to
%           to allow for text around this axes
%           - this is just the border for the inner position of the axes
%         .left
%         .right
%         .top
%         .bottom
function border=getborder(obj,cb)



%get the tight inset for this axes
%I'm assuming this doesn't include space for colorbar but I could
%be wrong. 
border=struct('left',0,'right',0,'bottom',0,'top',0);

for row=1:size(obj,1)
    for col=1:size(obj,2)
        tinset=get(obj(row,col).ha,'tightinset');

        border(row,col).left=tinset(:,1);
        %12-13-07 - Code below does not help

        %tight inset appears to sometimes cut off the ylabel
        %i.e if ylabel is multiline 
        %therefore we compute the leftmost edge of the ylabel from its
        %extent property.
        %Since the extent property is in the same units as the axes we have
        %to conver them
        %width of axis in axis units        
%         if ~isempty(obj(rind,cind).ylbl)
%        pos=get(obj(rind,cind).ha,'position');
%         xwidth=diff(get(obj(rind,cind).ha,'xlim'));
%         xscale=pos(3)/xwidth;
%         
%         ylblextent=get(obj(rind,cind).ylbl.h,'Extent');
%         
%         %convert the left position of the lbl 
%         %multiple by negative 1 because Left is negative as its outside the
%         %axes
%         ylblleftborder=-1*ylblextent(1)*xscale;
%         border(rind,cind).left=max([border(rind,cind).left, ylblleftborder]);
      %  end
        border(row,col).bottom=tinset(:,2);
        border(row,col).right=tinset(:,3);
        border(row,col).top=tinset(:,4);
    end
end