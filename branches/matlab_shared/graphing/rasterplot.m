%function rasterplot=(t,e);
%       t -m x t array of times
%          1 x t use same times for each events
%       e -m x t 
%           -spike train for each neuron
%
function rasterplot(t,e)
    
    if size(e,1)>size(t,1)
        t=ones(size(e,1),1)*t;
    end
    
    sh=1;       %height of the spike event
    sw=mean(diff(t,1,2))/2;       %width of the spike event
    figure;
    hold on
    
    %we plot each spike as a rectangle using the patch
    %command so we have to define for each spike a rectangle
    %specified by the 4 corners
    %we do this by constructing two matrices x and y
    %which specify the four corners of the rectangle
    %each column specifies a different spike
    %we handle each row separately so we can change the color
    for index=1:size(t,1);
        ind=find(e(index,:)>0);
        x=zeros(4,length(ind));
        x(1,:)=t(index,ind)-sw(index)/2;
        y=zeros(4,size(x,2));
        y(1,:)=(index-1)*sh;
        x(2,:)=x(1,:)+sw(index);
        y(2,:)=y(1,:);
        x(3,:)=x(2,:);
        y(3,:)=y(2,:)+sh;
        x(4,:)=x(1,:);
        y(4,:)=y(3,:);
        
        patch(x,y,'b');
    end
    
    
    
    
    
    
    
    
    
    
