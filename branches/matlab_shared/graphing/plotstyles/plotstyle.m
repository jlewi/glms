% function plotstyle(index)
%   index - number of the plot style
%
%Explanation- defines several linestyles for figures
%   return value is a structure containing field name value pairs
%
%Revisions:
%   08-29-2008
%       Double the line width to 4
%       swap ':' and '--' in lstyles
function [l]=plotstyle(index)

error('Use PlotStyles.plotstyle')
