%function setfsize(f,gparam)
%   f - structure for figure
%   gparam - presentation figure
%
% Explanation: make a figure ready for 
%   presentation. This sets the size of the figure
function f=setfsize(f,gparam)

if ishandle(f)
    hf=f;
else
    hf=f.hf;
end


%set the height and width of the figure
set(hf,'units','inches');
fpos=get(hf,'paperposition');
fpos(3)=gparam.width;
fpos(4)=gparam.height;
set(hf,'paperposition',fpos);    %controls the position when we printout
%control its size on screen as well
fpos=get(hf,'position');
fpos(3)=gparam.width;
fpos(4)=gparam.height;
set(hf,'position',fpos); 
%make sure it honors the paper position
set(hf,'Paperpositionmode','manual');