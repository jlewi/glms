%function addtext(fobj,pos,txt)
%  pos - position in relative coordinates
%        at which to add the text
%   txt - text to add
%
%Explanation:
%   Creates a blank axis the same size as the figure which allows us to add
%   text anywhere we like on the figure
function ht=addtext(fobj,pos,txt)

if isempty(fobj.tlabels)
    %ha - handle to the axes
    %ht - array of handles to the created text objects
    fobj.tlabels=struct('ha',[],'ht',[]);    
end

if isempty(fobj.tlabels.ha)
   fobj.tlabels.ha=axes('position',[0 0 1 1]);
   
   %set the x and y limits to 0 and 1 so that the x,y coordinates are in
   %relative coordinates
   xlim([0 1]);
   ylim([0 1]);
   
   set(gca,'visible','off');
end
set(fobj.hf,'CurrentAxes',fobj.tlabels.ha);
tind=length(fobj.tlabels.ht);
tind=tind+1;

ht=text(pos(1),pos(2),txt,'horizontalalignment','center');
fobj.tlabels.ht(tind)=ht;




