%function sim=FigObj(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Template for the constructor of a new class. This template
% shows how we can identify which constructor was called based on the
% parameters that were passed in.
%
%
% optional:
%   xlabel - xlabel for each axis
%   ylabel - ylabel for each axes
%
% Revisions:
% 12-04-2008 - Use new template for constructor
%
%  11-21-2008 - Add functionality to allow text labels to be put anywhere
%               on the figure. We added the structure tlabels 
%               -We add txt labels by creating an axes whose dimensions are
%                the same as the figure
%               - Add sizesub - to store information about resizing the
%               subplots
%  10-12-2008 - make widht and height settable
%
%   07-23-2008 - Use new OOP model and derive from class handle
classdef (ConstructOnLoad=true) FigObj < handle

    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %  bname  - name of base class. This allows us to refer to it templates
    %  hf     - handle to figure
    %  width  -width of figure in inches
    %  height - height of figure in inches
    %  gparam - structure of values to control plot display
    %  a      - array of axes objects that go with this figure
    %  name   - name of figure
    %  naxes  - number of rows and columns of axes
    %use helvetica as the default font because its a sans-serif font
    %which is required for neural comp and I think is more general in type
    %setting
    properties(SetAccess=private,GetAccess=public)
        version=081021;
        hf=[];
        name=[];
        gparam=struct('fontsize',12,'linewidth',4,'markersize',5,'fontname','helvetica');
        naxes=[1 1];
        
        %sizesubs
        %store information about how to size the subplots
        sizesub=[];
    end

    %for backwards compatability make the axes property assignable
    %we actually prevent the user from overloading it by accessing the set
    %method
    properties(SetAccess=public,GetAccess=public)
        a=[];
        
        width=4.5;
        height=3;
        
        %structure that allows us to put text labels at arbitrary positions
        %on the graph. 
        tlabels=struct('ha',[],'ht',[]);
    end
    properties(SetAccess=public,GetAccess=public,Dependent=true)
       fontsize; 
    end
    methods
        function fsize=get.fontsize(obj)
           fsize=obj.gparam.fontsize; 
        end
        function obj=set.fontsize(obj,val)
           obj.gparam.fontsize=val; 
           
           %lbl and resize the graph
           lblgraph(obj);
           sizesubplots(obj);
        end
        function obj=set.width(obj,val)
           obj.width=val;
          
           %I think calling setfsize might be slowing construction down
           %setfsize(obj);
        end
        
        function obj=set.height(obj,val)
           obj.height=val;
                      %I think calling setfsize might be slowing
                      %construction down
           %setfsize(obj);
        end
        function obj=FigObj(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con=[];
       



   %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    %default constructor no parameters are required
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    %default constructor no parameters are required
                otherwise
                 error('unexpected value for cind');
             end            



            %process optional fields
            if isfield(params,'fontsize')
                obj.gparam.fontsize=params.fontsize;
                params=rmfield(params,'fontsize');
            end

            %**************************************************
            %extract any parameters which are meant for the axes object
            %*****************************************************
            %structure to store parameters for the axes object
            aparam=[];
            afields={'xlabel','xlbl','ylabel','ylbl','title','ttl'};
            for aind=1:length(afields)

                if isfield(params,afields{aind})
                    aparam.(afields{aind})=params.(afields{aind});
                    params=rmfield(params,afields{aind});
                end
            end

            %**********************************************************
            %Create a new figure and adjust the size
            %*****************************************************
            obj.hf=figure;

            fnames=fieldnames(params);
            for fi=1:length(fnames)
                obj.(fnames{fi})=params.(fnames{fi});
            end
            obj=setfsize(obj);

            %*********************************************************
            %create the axes
            %*********************************************************
            if isfield(params,'naxes')
                obj.naxes=params.naxes;
            end

            %create the axes object
            for row=1:obj.naxes(1)
                for col=1:obj.naxes(2)
                    hind=sub2ind([obj.naxes(2) obj.naxes(1)],col,row);
                    ha=subplot(obj.naxes(1),obj.naxes(2),hind);
                    hold on;
                    aparam.ha=ha;
                    if isempty(obj.a)
                        obj.a=AxesObj(aparam);
                    else
                        obj.a(row,col)=AxesObj(aparam);
                    end
                end
            end
        end

        function obj=set.a(obj,a)
            % warning('Setting a no longer has any effect.');
            obj.a=a;
        end
    end
end




