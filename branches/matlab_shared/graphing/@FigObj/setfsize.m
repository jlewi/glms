%function setfsize(obj)
%	obj - figure object

% Explanation: make a figure ready for 
%   presentation. This sets the size of the figure
function obj=setfsize(obj)

%set the height and width of the figure
set(obj.hf,'units','inches');
fpos=get(obj.hf,'paperposition');
fpos(3)=obj.width;
fpos(4)=obj.height;
set(obj.hf,'paperposition',fpos);    %controls the position when we printout
%control its size on screen as well
fpos=get(obj.hf,'position');
fpos(3)=obj.width;
fpos(4)=obj.height;
set(obj.hf,'position',fpos); 
%make sure it honors the paper position
set(obj.hf,'Paperpositionmode','manual');