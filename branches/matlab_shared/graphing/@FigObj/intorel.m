%function [x,y]=intorel(fobj,xi,yi)
%   xi- position in figure in terms of inches
%       positive number = distance from left
%       negative = distance from right
%   yi - position in figure in terms of inches 
%       positive number = distance from bottom
%       negative number = distance from top
%
%Return value:
%   x,y - corresponding location in the figure in relative coordinates
%       0,0 = lower left corner
%       1,1 = upper right corner
function [x,y]=intorel(fobj,xi,yi)

if (xi>0)
    startx=0;
else
    startx=1;
end
x=startx+xi/fobj.width;

if (yi>0)
    starty=0;
else
    starty=1;
end
y=starty+yi/fobj.height;