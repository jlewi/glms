%Sig.get   Return value of a field
%
%   get(node,property_name)
%   Possible property names
%      
%   Note all values are returned by value (this is the way matlab does it)
function [val] = get(obj,prop_name)
% GET Get asset properties from the specified object
% and return the value
switch prop_name
case {'start','tstart'}
    val = obj.start;
case {'width','twidth'}
    val=obj.width;
case {'stop','tstop'}
    val = obj.stop;
otherwise
    error([prop_name,' Is not a valid TimeWindow property'])
end