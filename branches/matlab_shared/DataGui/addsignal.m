%function  addsignal(handles,s)
%       handles - handles object
%       S - sig object to add to the list of signals.
%       tw - optional - initial time window for the object

function [handles]=addsignal(handles,s,tw)

if (~(exist('tw','var')));
    tw=TimeWindow();
end

%increment the number of signals
handles.data.numsignals=handles.data.numsignals+1;

%add the element to the listbox
labels=get(handles.cbo_entitylist,'String');
labels{handles.data.numsignals}=get(s,'label');
set(handles.cbo_entitylist,'String',labels);

%add it to the signals 
handles.data.signals{handles.data.numsignals}=s;
handles.data.timeWindow{handles.data.numsignals}=tw;

guidata(handles.datagui_fig,handles);