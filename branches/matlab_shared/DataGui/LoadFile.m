%function LoadFile(handles)
%   handles - handles for the gui
%
% Explanation:
%   Gets a filename and loads the apropriate results.
%
% Author: Jeremy Lewi
% Date: 2-02-2005
function [handles]=LoadFile(handles)
nsLibFile=handles.ns.nsLibFile;   %set the library file
                                %this is the neuroshare api file to use
                                
%get the filename to open
currdir=pwd;

%switch to directory before opening dialog box if it exists
%startdir='Z:\work\graduate\hybrid_neural_microsystems_2\module_1\data';
%if exist(startdir,'dir')==7
%    cd(startdir);
%end

%remember the current directory
%ldir=cd;
if (exist(handles.param.startdir,'dir'))
    cd(handles.param.startdir);
end
[filename, pathname] = uigetfile({'*.abf','Axon ABF File (*.abf)';'*.mat', 'Matlab DataGui File (*.mat);'});

cd(currdir);

%if user hit cancel on the dialog box then exit 
if isequal(filename,0) | isequal(pathname,0)
      return;
end

handles.param.startdir=pathname;

set(handles.datagui_fig,'name', sprintf('dataegui: %s',filename));


handles.data.filename=filename;

%check if file is abf file
[pathstr,name,ext] = fileparts(filename);

if strcmpi(ext,'.abf')
    %create the signals from an abf file
    data=readabf([pathname filename]);
    units=data.data.units;
    t=data.data.time;
    starttime=t(1);
    samplerate=1/(t(2)-t(1));
    
    fnames=fieldnames(data.data);
    handles.data.numsignals=0;
    
    for index=1:length(fnames)
        if (strcmpi(fnames(index),'units')==0)
            if (strcmpi(fnames(index),'time')==0)
                handles.data.numsignals=handles.data.numsignals+1;
                u=units.(fnames{index});
                handles.data.signals{handles.data.numsignals}=Sig('data',data.data.(fnames{index}),'units',units.(fnames{index}),'label',fnames{index},'starttime',starttime,'samplerate',samplerate);
                %the size of segments to plot. Size should be in time (seconds)
                %twindow describes the time window we are looking at
                %there is one timewindow object for each signal
                handles.data.timeWindow{handles.data.numsignals}=TimeWindow();
                labels(handles.data.numsignals)=fnames(index);
            end
        end
    end
    
elseif (strcmpi(ext,'.dg'))
    sdata=load([pathname fname]);
    
    numsig=length(sdata.data.signals);
    %handles.data.signals=sdata.data.signals;
    %handles.data.numsignals=length(sdata.data.signals);
    
    %handles.param.index=sdata.param.index;
    
    %generate the labels
    %labels=cell(1,handles.data.numsignals);
    
    for index=1:numsig
        addsignal(handles,sdata.data.signals{index},sdata.data.timeWindow{index})        
    end
    
    set(handles.cbo_entitylist,'value',sdata.param.index);
    updateData(handles);
    
end

if (handles.data.numsignals>0)
set(handles.cbo_entitylist,'String',labels,'Value',1) % Load listbox
    
 %update the data displayed in the gui
 cla(handles.maxes);
set(handles.txt_info,'String','');
% handles=updateData(handles);
else
set(handles.cbo_entitylist,'String','','Value',1) % Load listbox
cla(handles.maxes);
set(handles.txt_info,'String','');
end
