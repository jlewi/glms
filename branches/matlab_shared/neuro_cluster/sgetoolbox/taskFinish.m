%function taskFinish(task)
%
%Explanation: Runs tasks on finish of the task
%   1) Lab 1 (and only lab 1 ) syncs the output files back to the client
%
%
%  02-05-2009
%       if the destination host is the cluster head node
%       then leave src blank
%  08-12-2008
%       Put the output file for taskfins.m in NL_JOBDIR and rename it taskfinish
function taskFinish(task)
% TASKFINISH Perform user-specific task cleanup actions.
%
%   taskFinish(task)
%
%   To define specific task cleanup actions on each worker
%   for each task, you can do any of the following:
%   1. Add m-code that performs those actions to this file for each worker.
%   2. Add to the job's PathDependencies property a directory that
%      contains a file named taskFinish.m.
%   3. Include a file named taskFinish.m in the job's FileDependencies
%      property.
%
%   The file in FileDependencies takes precendence over the
%   PathDependencies file, which takes precedence over this file on
%   the worker's installation.
%
%   The task parameter that is passed to this function is the task object
%   that the worker has just executed.
%
%   This function runs even if the jobStartup, taskStartup, or the
%   task itself threw an error.
%
%   If this function throws an error, the error information appears in the task's
%   ErrorMessage and ErrorIdentifier properties unless the jobStartup,
%   taskStartup, or the task itself threw an error.
%
%   Any path changes made here or during the execution of tasks will be
%   reverted by the MATLAB Distributed Computing Engine to their original
%   values before the next job runs, but preserved for subsequent tasks in
%   the same job.  Any data stored by this function or by the execution of
%   the job's tasks (for example, in the base workspace or in global or
%   persistent variables) will not be cleared by the MATLAB Distributed
%   Computing Engine before the next job runs, unless the RestartWorker
%   property of the next job is set to true.
%
%   See also jobStartup, taskStartup.
% Copyright 2004-2006 The MathWorks, Inc.


%keep track of errors in rsync
nerrors=0;
emsg='';

t=clock;

job=task.Parent;
udata=job.JobData;

global NL_JOBDIR;
%*******************************************************************************
%open the status file to print messages to

if (isfield(udata,'statusdir') && ~isempty(udata.statusdir))
    statusdir=udata.statusdir;
else
    statusdir=NL_JOBDIR;
end

statusfile=fullfile(statusdir,sprintf('taskfinish_lab%d.out',get(job,'ID'),labindex));

fprintf('statusfile=%s \n',statusfile);
try
    fid=fopen(statusfile,'a');
    if (fid==-1)
        fid=1;
    end
catch
    %if theres a problem opening the file redirect it to standard output
    fid=1;
end


fprintf(fid,'\n\n****************************************************************** \n');
fprintf(fid, 'taskFinish.m: started %s', datestr(t,'yy-mm-dd HH:MM:SS'));
fprintf(fid,'\n******************************************************************** \n');




%********************************************************
%copy all files

if isfield(udata,'filesout')
    filesin=udata.filesout;

    if ~isempty(filesin)
        if ~isfield(udata,'client')
            error('Cannot copy files. task.userdata is missing field client');
        end

        if ~isfield(udata,'cluster')
            error('Cannot copy files. task.userdata is missing field client');
        end

        if (labindex==1)
            for i=1:length(filesin)
                fprintf('taskFinish: file # %d \n', i);

                dest.fname=getpath(filesin(i),udata.client.hostid);


                switch lower(dest.host)
                    case {'','localhost'}
                        %set host blank so we do local copy
                        dest.host='';
                        src.host='';
                    otherwise
                        dest.host=udata.client.host;
                        src.host=udata.cluster.host;
                end
                src.fname=getpath(filesin(i),udata.cluster.hostid);
                src.isdir=isdir(filesin(i));


                ecode=rsyncfile(src,dest,[],statusfile);



                %ecode value other than zero indicates rsync failed
                switch ecode
                    case -1
                        %indicates a problem with sudo
                        emsg=sprintf(['%slab %d: rsync failed because user wasnot correct and we arent root so we cant su. \n'],emsg,labindex);
                        nerrors=nerros+1;
                    case 0

                        fprintf(fid,'taskstartup.m: rsync was successful \n');

                    case 30
                        fprintf(fid,'taskstartup.m: rsync error timedout \n');
                        nerrors=nerrors+1;
                        emsg=sprintf(['%slab %d: rsync timedout \n'],emsg,labindex);
                    otherwise
                        fprintf(fid,['taskstartup.m: rsync error exited with %d \n'],ecode);

                        emsg=sprintf(['%slab %d: rsync error exited with %d  \n'],emsg,labindex, ecode);
                        nerrors=nerrors+1;
                end
            end
        end
    end
else
    fprintf(fid,'no files to copy. \n');
end

t=clock;
fprintf(fid, '\n\n taskFinish.m: finished %s \n\n', datestr(t,['yy-mm-dd HH:MM:SS']));

%close the file
try
    if (fid~=1)
        fclose(fid);
    end
catch err
end

%check if any of the labs had problems copying the files
%if they did then we throw an error

%we do this by summing nerrors
%
%if any of these elements is non-zero that means an error occured
nerrors=gplus(nerrors);


if (nerrors>0)
    %we create a cell array containing the error messages from all nodes
    emsg=gcat({emsg});

    errmsg='';
    for j=1:length(emsg)
        if ~isempty(emsg{j})
            errmsg=sprintf('%s%s',errmsg,emsg{j});
        end
    end
    %throw an error
    error(errmsg);
end