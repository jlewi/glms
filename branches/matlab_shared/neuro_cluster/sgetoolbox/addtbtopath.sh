#!/bin/bash
#
#This is a script on each node that I want to execute to add the neurolab toolbox to each path
#I had to create a separate script because the quote marks made it difficult to get
#to work with dsh
#
#The function takes as input a list of paths to add to the matlab path
#

cmd="";

#loop over the arguments and build the appropriate matlab command
while [ $# -gt 0 ]
do
   MPATH=$1;
   cmd="$cmd;addpath('$MPATH')"
   shift 1;
done

#command to run in matlab
#adds the path
#rehashes the toolbox
#exits

cmd="$cmd;savepath;rehash toolboxcache;exit";
matlab -dmlworker -r "$cmd";
