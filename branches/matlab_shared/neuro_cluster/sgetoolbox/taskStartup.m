%function taskStartup(task)
%
%Explanation: Perform task initilization
%
%   i) Copy files listed specified in the filesin property of JobData
%      The files are first rsynced from the client machine to the head
%      node
%         The files are first copied to the directory in Scheduler data
%         location
%         They are then copied to a local filesystem on each node
%
%
%   ii) Print status trace information to file
%   JobData.statusdir/Job#_lab#.out
%      directory defaults to tmp
%
%$Revision$
%Revisions:
%   08-12-2008
%       Create the output file for task startup in NL_JOBDIR
%       Rename it taskstartup.out
%   06-27-2008
%      check if f is a handle
%   06-24-2008
%      Create the global variable NL_JOBDIR which contains the path
%      to the local directory which is specific for this
%      user/job/task/lab
%
%   05-30-2008
%       rsync was failing when I tried to rsync to multiple worker nodes
%       So instead I rsync once to a shared filesystem and then issue copy
%       commands on all the nodes.
function taskStartup(task)
% TASKSTARTUP Perform user-specific task startup actions.
%
%   taskStartup(task)
%
%   To define specific task initialization actions on each worker
%   for each task, you can do any of the following:
%   1. Add M-code that performs those actions to this file for each worker.
%   2. Add to the job's PathDependencies property a directory that
%      contains a file named taskStartup.m.
%   3. Include a file named taskStartup.m in the job's FileDependencies
%      property.
%
%   The file in FileDependencies takes precendence over the
%   PathDependencies file, which takes precedence over this file on
%   the worker's installation.
%
%   The task parameter that is passed to this function is the task object
%   that the worker is about to execute.
%
%   If this task throws an error, the error information appears in the task's
%   ErrorMessage and ErrorIdentifier properties, and the task will not be
%   executed.
%
%   Any path changes made here or during the execution of tasks will be
%   reverted by the MATLAB Distributed Computing Engine to their original
%   values before the next job runs, but preserved for subsequent tasks in
%   the same job.  Any data stored by this function or by the execution of
%   the job's tasks (for example, in the base workspace or in global or
%   persistent variables) will not be cleared by the MATLAB Distributed
%   Computing Engine before the next job runs, unless the RestartWorker
%   property of the next job is set to true.
%
%   See also jobStartup, taskFinish.

% Copyright 2004-2006 The MathWorks, Inc.

%******************************************************
%Rsync was causing all kinds of problems when I tried to
%rsync all the files to the worker nodes directly
%therefore I use rsync to copy the files from the client
%to a shared filesystem between the workers
%I then copy these files to a local directory for each worker
%************************************************************

%number of errors which occured in calls to rsync
%I use this to detect an error on any node if an error occurs on any node
%then all nodes throw an error because throwing an error on a single node
%doesn't appear to work.
nerrors=0;

%store the error message from each lab
emsg='';



%job=task.Parent;

job=getCurrentjob();
udata=job.JobData;

%sched=distsched();
%for the shared directory we use the one storing all the job data
%we get this from the environment variable MDCE_STORAGE_LOCATION
location=getstoragelocation();

%it would probably be better to use the job directory in MDCE_JOB_LOCATION
%rather than assuming its job
shareddir=fullfile(location.unix,sprintf('Job%d',get(job,'ID')));


%****************************************************************
%Create a directory on the local machine to store datafiles
%*****************************************************************
%the path of the directory contains the username, the job number,
%the task number, the lab and a time stamp
%This should guarantee that the path is always unique.
%It also ensures the path contains information that can be used
%for deciding when the directory can be removed.

jnum=sprintf('%d',get(job,'ID'));
tnum=sprintf('%d',get(task,'ID'));
[status,username]=system('whoami');
username=deblank(username);
t=clock;
tstamp=datestr(t,'yymmdd');
tstamp=sprintf('%s%02g%s',tstamp,t(4),datestr(t,'MMSS'));

global NL_JOBDIR;

%NL_JOBDIR=sprintf('/tmp/%s_job%s_task%s_lab%02.2g_%s',username,jnum,tnum,labindex,tstamp);
%Name the job using the sge JOB_ID so we can easily check when job is no longer running
JOBID=getenv('JOB_ID');

NL_JOBDIR=fullfile(filesep,'tmp',['sgejob_' num2str(JOBID)], sprintf('task%s',tnum));

ecode=recmkdir(NL_JOBDIR);

allecodes=gcat(ecode);
if (any(allecodes~=0))
    %one of the nodes couldn't create NL_JOBDIR throw an error
    if (ecode==0)
        emsg=sprintf('An error occurred on labs: %d ',find(allecodes~=0));
    else
        emsg=sprintf(['lab %d: recmkdir could not create directory %s. recmkdir exited with code %d  ' ...
            '\n'],labindex,NL_JOBDIR, ecode);
    end

    throw(emsg);
end


%*******************************************************************************
%open the status file to print messages to
%**************************************************************************
if (isfield(udata,'statusdir') && ~isempty(udata.statusdir))
    statusdir=udata.statusdir;
else
    statusdir=NL_JOBDIR;
end

statusfile=fullfile(statusdir,sprintf('taskStartup_lab%d.out',get(job,'ID'),labindex));

fprintf('statusfile=%s \n',statusfile);
try
    fid=fopen(statusfile,'a');
    if (fid==-1)
        fid=1;
    end
catch
    %if theres a problem opening the file redirect it to standard output
    fid=1;
end

fprintf(fid,'taskstartup.m for parallel jobs\n');


%********************************************************
%copy all files
%***************************************************************
if isfield(udata,'filesin')
    filesin=udata.filesin;

    if ~isfield(udata,'client')
        error('Cannot copy files. task.userdata is missing field client');
    end

    if ~isfield(udata,'cluster')
        error('Cannot copy files. task.userdata is missing field client');
    end

    fprintf(fid,'TaskStartup.m: Copying Files to shared dir \n');
    %    fprintf(fid,'Taskstartup.m: copying files to shared dir \n');

    %***************************************************************
    %rsync the files to a filesystem shared by all nodes
    %***************************************************************
    if (labindex==1)
        %its our turn
        try
            %check if files are stored on the local filesystem of the
            %cluster if they are then we don't need to transfer them
            %using rsync
            rsyncfiles=true;
            switch lower(udata.client.host)
                case{ '','cluster.neuro.gatech.edu','localhost'}
                    rsyncfiles=false;
                otherwise
                    rsyncfiles=true;
            end

            if (rsyncfiles)
                fprintf(fid,'taskStartup.m: tranfer %d files \n', length(filesin));
                for i=1:length(filesin)
                    src.host=udata.client.host;
                    src.fname=getpath(filesin(i),udata.client.hostid);
                    src.isdir=isdir(filesin(i));

                    dest.host=udata.cluster.host;
                    dest.fname=fullfile(shareddir,getrpath(filesin(i)));

                    fprintf(fid,'src file: %s \n',src.fname);
                    fprintf(fid,'dest file: %s \n',dest.fname);

                    ecode=rsyncfile(src,dest,[],statusfile);

                    fprintf(fid,'taskstartup.m: rsync ecode=%d \n',ecode);
                    %ecode value other than zero indicates rsync failed
                    switch ecode
                        case -1
                            %indicates a problem with sudo
                            emsg=sprintf(['%slab %d: rsync failed because user wasnot ' ...
                                'correct and we arent root so we cant su. \n'],emsg,labindex);
                            nerrors=nerrors+1;
                        case 0
                            % succes
                            fprintf(fid,'taskstartup.m: rsync was successful \n');

                        case 30
                            fprintf(fid,'taskstartup.m: rsync error timedout \n');
                            nerrors=nerrors+1;
                            %error('rsync timed out');
                            emsg=sprintf(['%slab %d: rsync timedout \n'],emsg,labindex);
                        otherwise
                            fprintf(fid,'taskstartup.m: rsync error exited with %\n',ecode);

                            emsg=sprintf('%slab %d: rsync error exited with %d \n',emsg,labindex, ecode);
                            %error('taskStartup.m: rsync error');
                            nerrors=nerrors+1;
                    end

                    %don't bother continuing with the copying
                    if (nerrors>1)
                        break;
                    end
                end
                fprintf(fid,'taskStartup.m: Finished copying files to shared director \n');
            else

                fprintf(fid,'taskStartup.m: Data files are stored on shared file system of cluster. No need to use rsync. \n');
            end

        catch (e)
            nerrors=nerrors+1;
            emsg=e.message;
        end

    end %wait lab 1 rsync to transfer the files
    %wait until all nodes get here
    labBarrier;

    %make sure rsync didn't cause any errors if it did throw an error
    %we do this by summing nerrors
    %
    %if any of these elements is non-zero that means an error occured
    nerrors=gplus(nerrors);


    if (nerrors>0)
        %we create a cell array containing the error messages from all nodes
        emsg=gcat({emsg});

        errmsg='';
        for j=1:length(emsg)
            if ~isempty(emsg{j})
                errmsg=sprintf('%s%s',errmsg,emsg{j});
            end
        end
        %throw an error
        error(errmsg);
    end

    fprintf(fid,'taskstartup.m: done copying files to shared dir. \n');

    %*********************************************************************
    %each file copies the files from the shared directory to a local
    %directory
    %****************************************************************
    try
        %fprintf(fid,'taskStartup.m: Copying files from shared directory to local director \n');


        fprintf(fid,'taskStartup.m: copying the files from the shared directory to NL_JOBDIR \n');
        %we can only continue if recmkdir was successful




        for i=1:length(filesin)
            src.host=udata.client.host;
            if (rsyncfiles)
                srcfile=fullfile(shareddir,getrpath(filesin(i)));
            else
                %in this case the files are stored on the head node already
                %we didn't have to copy them using rsync
                %so we use the full path
                srcfile=getpath(filesin(i),udata.client.hostid);

            end



            %destination file
            destfile=getpath(filesin(i),udata.cluster.hostid);

            %make sure the directory exist
            destdir=fileparts(destfile);


            ecode=recmkdir(destdir);


            %make sure no error occured

            fprintf(fid,'copying %s \n',srcfile);

            %make sure no error occured

            if (ecode~=0)
                nerrors=nerrors+1;
                msg=sprintf('%s error ocurred while trying to create the directory: %s\nto be the destination for file: %s. \n',emsg,destdir,srcfile);
                msg=sprintf('%s Some possible causes of this error are \n',msg);
                msg=sprintf('%s. 1. There is a problem with how RemoteFile Path objects evaluate paths on the cluster.\n',msg);
                msg=sprintf('%s \t hostid=%s \n',msg,udata.cluster.hostid);
                bcmd=getbcmd(filesin(i));
                msg=sprintf('%s \t bcmd isa %s \n',msg, class(bcmd.(udata.cluster.hostid)));
                if isa(bcmd.(udata.cluster.hostid),'PathGlobalVar')
                    gvar=getgvar(bcmd.(udata.cluster.hostid));
                    msg=sprintf('%s \t bcmd.gvar= %s \n',msg, gvar);
                    if ~(strcmp(gvar,'NL_JOBDIR'))
                        msg=sprintf(['%s \t Warning. The path does not use NL_JOBDIR. \n If you want to store the file in a directory that is on the worker node, then use NL_JOBDIR as the base command of the path. \n\n'], msg);
                    end
                end
                msg=sprintf('%s base path on cluster=%s \n',msg,getpath(bcmd.(udata.cluster.hostid)));
                msg=sprintf('%s 2. Check you have enough disk space to create the files. \n',msg);

                emsg=msg;
                break;
            end

            fprintf(fid,'taskstarup.m: copying %s \n',srcfile);



            cmd=sprintf('cp -r %s %s',srcfile, destfile);
            [cpstat]=system(cmd);


            fprintf(fid,'taskstartup.m: cp exit code=%d \n',cpstat);
            %ecode value other than zero indicates rsync failed
            switch cpstat
                case 0
                    % succes
                    fprintf(fid,'taskstartup.m: cp was successful \n');

                otherwise
                    fprintf(fid,'taskstartup.m: cp exited with %d. \n Possibly out of disk space. \n',cpstat);

                    emsg=sprintf('%slab %d: cp exited with %d. \n  Possibly out of disk space \n',emsg,labindex, cpstat);
                    %error('taskStartup.m: rsync error');
                    nerrors=nerrors+1;
            end

            %don't bother continuing with the copying
            if (nerrors>1)
                break;
            end
        end %loop over files

    catch err
        nerrors=nerrors+1;
        emsg=sprintf('%s %s\n',emsg,err.message);
    end

    %**************************************************************
    %make sure none of the nodes threw an error copying the files to
    %the local directory
    nerrors=gplus(nerrors);


    if (nerrors>0)
        %we create a cell array containing the error messages from all nodes
        emsg=gcat({emsg});

        errmsg='';
        for j=1:length(emsg)
            if ~isempty(emsg{j})
                errmsg=sprintf('%s%s',errmsg,emsg{j});
            end
        end
        %throw an error
        error(errmsg);
    end


    %call the user supplied taskstartup function
    if (isfield(udata,'taskstartupfunc') && ~isempty(udata.taskstartupfunc))
        %call the taskstartup function
        if isa(udata.taskstartupfunc,'function_handle')
            fprintf(fid, 'taskstartup.m: \n\n calling taskstartupfunc\n\n');
            udata.taskstartupfunc();
        else
            error('taskstrupfunc is not a function handle');
        end
    else
        fprintf(fid, 'no taskstartupfunc specified \n');
    end
end %if filesin exists

%global RESULTSDIR;
%fprintf(fid, '\n taskstartup.m RESULTSDIR=%s \n',RESULTSDIR);
fprintf(fid,'finished taskstartup.m\n');
try
    if (fid~=1)
        fclose(fid);
    end
catch
end

%this code was coppied from the makeFileStorageObject.m in toolbox/distcom/private
%
function xPlatformLocation=getstoragelocation()
location=getenv('MDCE_STORAGE_LOCATION');
pcStart = 'PC{';
pcEnd   = '}:';
unixStart = ':UNIX{';
unixEnd   = '}:';
% The location will be of the form PC{...}:UNIX{...} - thus use lazy (.*?)
% matching for the pc as :} cannot be part of the windows path but greedy
% matching for the unix as it could just be part of the path
pcLocation   = regexp(location, ['^' pcStart '.*?' pcEnd], 'match');
unixLocation = regexp(location, [ unixStart '.*' unixEnd], 'match');
% Was the input of this form? We also accept a simple string whic is just a
% directory
if isempty(pcLocation) && isempty(unixLocation)
    xPlatformLocation = location;
else
    % Strip start and end parts off the strings
    pcLocation   = pcLocation{1}(numel(pcStart)+1:end-numel(pcEnd));
    unixLocation = unixLocation{1}(numel(unixStart)+1:end-numel(unixEnd));
    % Make the structure to hold this information
    xPlatformLocation = struct('pc', pcLocation, 'unix', unixLocation);
end
