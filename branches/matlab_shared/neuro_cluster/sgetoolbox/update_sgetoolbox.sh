#!/bin/bash
#
#Script to update the neurolab toolbox
#
#modifying path requires write permissions to the appropriate mfiles
#easiest way to accomplish this to take ownership of matlab first.
#
#This script does the following
# 1. Copies the sgetoolbox to MATLAB/toolbox/sgetoolbox on each node
# 2. Adds the sgetoolbox to the Matlab path and rehashes the toolbox cache
# 3. Renames the file MATLAB/toolbox/sgetoolbox/paralleljob/mpiLibConf.m.template ../mpiLibConf.m"
#    This ensures that we actually override the default MPI configuration
# 
#The source and destination paths for the toolbox
MPATH="/Applications/MATLAB_R2008a/toolbox/sgetoolbox"
SRCPATH=~/svn_glms/matlab/neuro_cluster/sgetoolbox/

#the following is the command we will run to set the MATLAB path
#on each node to include the toolbox
#we need to rehash to the toolbox cache on all machines otherwise
#Matlab won't see any new files in the toolbox
setpathcmd="${SRCPATH}addtbtopath.sh $MPATH $MPATH/paralleljob $MPATH/distjob";


#options for rsync
#we use -L because we want to copy the files refereed to by symbolic links, not the links
#We exclude my svn directories
#
ropt='-avL --exclude=*.svn* --exclude=*~'

#*******************************************************************************************
#Worker Nodes
#*******************************************************************************************
#on all nodes do the following
#1. copy the latest code to SRCPATH
#2. rename mpiLibConf file
#3. set the matlab path on each worker
MPILIBFILE="$MPATH/paralleljob/mpiLibConf.m"
dsh -a -f -e "rsync $ropt $SRCPATH $MPATH; mv $MPILIBFILE.template $MPILIBFILE; $setpathcmd"


#*******************************************************************************************
#Head Node
#*******************************************************************************************
#do this on the head node as well
#for MATLAB 2007 we also have to do this for the gt director
#We write out the options for rsync because if we use $ropt we end up having problems
# The problem seems to be that if we use
#rsync $ropt $SRCPATH $MPATH
#the contents of $ropt is treated as one variable

rsync  $ropt $SRCPATH $MPATH

#rehash the toolbox cache on the head node
#must do this after syncinc the files
exec $setpathcmd

