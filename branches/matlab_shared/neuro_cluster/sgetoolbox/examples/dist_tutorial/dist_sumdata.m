%script: createdist.job_template.m
%author: jeremy lewi - jeremy@lewi.us
%
%Explanation: This is a script which provides a template/sample
%  of how to submit a distributed job to the Matlab cluster.
%
%  A distributed job is a serial code that we want to run on the cluster.
%
%
% How the cluster must be setup to use this script:
%     1. This script is intended to be run on the head node of your
%     cluster
%     2. You must have access to a shared filesystem with all the nodes
%          in the cluster. Matlab uses this shared filesystem to store
%          files which are used to pass data to the worker nodes.
%     3. We assume subversion is used to synchronize your code on your
%     development machine with your code on the cluster. This script will call
%     svn update to ensure we have the latest code.
%
%
% $Revision: 2215 $
%
%**************************************************************************
%parameters
%
%The values below should be customized for your setup
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/dist_tutorial';

%whether or not to capture command window output
capcmdout=true;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=false;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
%MATLABPATH='~/svn_trunk/matlab';
%svndirs={pwd,MATLABPATH};


%we loop to create a separate job for each datafile
for simnum=1:4
    
%an array of FilePath objects the input files
finfiles={FilePath('bcmd','RESULTSDIR','rpath',sprintf('data_%03g.mat',simnum))};

%an array of FilePath objects specfying the output files
foutfiles={FilePath('bcmd','RESULTSDIR','rpath',sprintf('output_%03g.mat',simnum))};

%name to use for the job
jname='jtemp';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='~/dist_tutorial';

%if there's some code you want to execute before starting
%the function (i.e setting the path) you can specify a handle to this
%function
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=@setupjob;
%**************************************************************************
%update the node on the cluster
%*************************************************************************
if (svnupdate)
    wd=pwd;
    for j=1:length(svndirs)
        if ~exist(svndirs{j},'dir')
            error(['Cannot issue svn update in %s, directory does not ' ...
                'exist.'],svndirs{j});
        end
        cd(svndirs{j});
        fprintf('Issueing svn update in %s \n',svndirs{j});

        system('svn update');

    end
    cd(wd);
end

%*********************************************************************
%setup the job
%************************************************************************

%get the scheduler
%distsched.m is a function which is part of the Neurolab Matlab toolbox
%which creates a scheduler for use with the Neurolab Matlab cluster.
sched=distsched();








%**************************************************************************
%create the jobs
%**************************************************************************
[jobdata]=initjobdata(jname,finfiles,foutfiles,startdir,startupfunc,localhost,localdatadir);



%create the job
job=createJob(sched);

%set the jobdata
job.JobData=jobdata;



%number of output arguments
naout=1;


%number of output arguments
naout=0;


%input arguments
iparam={finfiles{1},foutfiles{1}};

task=createTask(job,@process,naout,iparam);



%task properties
set(task,'CaptureCommandWindowOutput',capcmdout);


submit(job);

end