%function process(infile,outfile)
% infile - FilePath to the datafile to process
%outfile - where to save the results
%
function process(infile,outfile)
    load(getpath(infile));
    
    total=sum(data);
    fprintf('outfile=%s \n', getpath(outfile));
    save(getpath(outfile),'total');
    
    