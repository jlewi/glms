%*******************************************************
%this is a test function I created to test MPI programs to see if communication works
%*********************************************************
function mpitestfun()
    
    fprintf('%d: MPD_CON_EXT=%s \n',getenv('MPD_CON_EXT'));
    fprintf('%d: Before labBarrier \n', labindex);
    labBarrier;
    fprintf('%d: Passed labBarrier \n',labindex);
    
    
    %lab 1 broadcast a random number
    n=0;
    if (labindex==1)
        n=rand(1,1);
        fprintf('%d: will broad cast n=%d \n', labindex,n);
    end
    data=labBroadcast(1,n);
    
    fprintf('%d: Data recieved = %d \n',labindex,data);
    
        %lab 2 broadcast a random number
    n=0;
    if (labindex==2)
        n=rand(1,1);
        fprintf('%d: will broad cast n=%d \n', labindex,n);
    end
    data=labBroadcast(2,n);
    
    fprintf('%d: Data recieved = %d \n',labindex,data);