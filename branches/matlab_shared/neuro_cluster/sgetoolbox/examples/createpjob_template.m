%****************************************************************************
%Date:07-04-2008
%Author: Jeremy Lewi
%Explanation: A template script for creating and submitting parallel jobs
%
%$Revision$
%
%
%**************************************************************************
%Setup the scheduler
sched=distsched();

%**************************************************************************
%parameters
%
%The values below should be customized for your setup
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/svn_trunk/adaptive_sampling';

%whether or not to capture command window output
capcmdout=false;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=false;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
MATLABPATH='~/svn_trunk/matlab';
svndirs={pwd,MATLABPATH};

%an array of FilePath objects the input files
filesin={};

%an array of FilePath objects specfying the output files
filesout={};

%name to use for the job
jname='template';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='/home/jlewi/svn_trunk/allresults';

%if there's some code you want to execute before starting
%the function (i.e setting the path) you can specify a handle to this
%function
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=[];

%how many nodes do you want to run on
nworkers=2;
%**************************************************************************
%create the parallel job
%************************************************************************
[jobdata]=initjobdata(jname,filesin,filesout,startdir,startupfunc,localhost,localdatadir);
pjob=createParallelJob(sched);
pjob.jobData=jobdata;



%set the pathe dependencies
%include the path for matlab (i.e path containing setmatlabpath) because
%my setpaths script isn't correcly adding the directory containing matlab.
set(pjob,'PathDependencies',{'/Users/jlewi/svn_trunk/matlab/neuro_cluster/paralleljob'});




set(pjob,'MaximumNumberOfWorkers',nworkers);
set(pjob,'MinimumNumberOfWorkers',nworkers);


%********************************************************************************************
%create the task
%*************************************************************************************************
%ptask=createTask(pjob,@error,0,{'Throw an error in main code'});
%ptask=createTask(pjob,@fprintf,1,{'HELLO_WORLD'});
%ptask=createTask(pjob,@pause,0,{600});
ptask=createTask(pjob,@mpitestfun,0,{});

set(ptask,'CaptureCommandWindowOutput', capcmdout);
jobdata.jobname=jname;
set(pjob,'JobData',jobdata);

%submit the job
submit(pjob);
