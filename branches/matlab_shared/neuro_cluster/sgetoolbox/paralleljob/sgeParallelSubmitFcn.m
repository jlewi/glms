%function sgeSubmitFcn(scheduler,job,props)
%
%
%
%$Revision$
%Revisions:
%   09-25-2008
%       RRRRRRRRRRemove decode dir as an input argument
%   08-11-2008:
%       Don't set the MDCE_DECODE_DIR anymore
%       The directory containing the decode function should be part of the path
%       Directory containing sgeMpdWrapper.sh should be same as this mfile.
%   07-25-2008:
%        Use the new sgeMpdWrapper.sh which uses the MPD process daemon instead of the
%        The wrapper sgeMpdWrapper.sh should be in the neurolab toolbox
%        Removed code to MDCE_USER
%       
%   05-29-2008:
%        Create the environment variable MDCE_USER and set it to the
%        current username. The sgeParallelWrapper.sh su's to this user
%        before starting matlab.
%
%        
%   05-28-2008: got rid of startdir. To specify the startdir
%     we specify it as a field of the job data. The the jobstartup and
%     taskstartup
%     functions cd to the appropriate directory.
%   02-25-2008: added startdir to specify directory from which to run the code
%
function sgeParallelSubmitFcn( scheduler, job, props, varargin )
%sgeParallelSubmitFcn - parallel submission for SGE

% Copyright 2006 The MathWorks, Inc.
% $Revision$   $Date: 2006/12/27 20:40:57 $


jdep=[];
varargin

%process optional arguments
for index=1:2:length(varargin)
  fprintf('varargin(%d)=%s\n',index,varargin{index});
  switch varargin{index}
    case 'jdep'
    jdep=varargin{index+1};
    
   otherwise
     fprintf('Unrecognized option %s',varargin{index});
  end
  end


% Set up the environment for the decode function - the wrapper shell script
% will ensure that all these are forwarded to the MATLAB workers.
setenv( 'MDCE_DECODE_FUNCTION', 'sgeParallelDecode' );
setenv( 'MDCE_STORAGE_LOCATION', props.StorageLocation );
setenv( 'MDCE_STORAGE_CONSTRUCTOR',props.StorageConstructor );
setenv( 'MDCE_JOB_LOCATION', props.JobLocation );
% Ask the workers to print debug messages by default:
setenv('MDCE_DEBUG', 'true');

% Set this so that the script knows where to find MATLAB, MW_SMPD and MW_MPIEXEC
% on the cluster. This might be empty - the wrapper script will deal with that
% eventuality.
setenv( 'MDCE_CMR', scheduler.ClusterMatlabRoot );

% Set this so that the script knows where to find MATLAB, SMPD and MPIEXEC on
% the cluster. This might be empty - the wrapper script must deal with that.
setenv( 'MDCE_MATLAB_EXE', props.MatlabExecutable );
setenv( 'MDCE_MATLAB_ARGS', props.MatlabArguments);


% The wrapper script is in the same directory as this M-file
[dirpart] = fileparts( mfilename( 'fullpath' ) );

scriptName = fullfile( dirpart, 'sgeMpdWrapper.sh' );

% Forward the total number of tasks we're expecting to launch
setenv( 'MDCE_TOTAL_TASKS', num2str( props.NumberOfTasks ) );

% Choose a file for the output. Please note that currently, DataLocation refers
% to a directory on disk, but this may change in the future.
logFile = fullfile( scheduler.DataLocation, ...
                    sprintf( 'Job%d.mpiexec.out', job.ID ) );

% Choose a number of processors per node to use (you will need to customise
% this section to match your cluster).
nodesArg = sprintf( '-pe matlab %d', props.NumberOfTasks );

 if (isfield(job.UserData,'jobname') && ~isempty(job.UserData.jobname));
      jname=job.UserData.jobname;
    else
      jname=sprintf('Job%d',job.ID);
 end
      
% Finally, submit to SGE. note the following:
% "-N Job#" - specifies the job name
% "-j yes" joins together output and error streams
% "-o ..." specifies where standard output goes to
%cmdLine = sprintf( 'qsub -N %s -j yes -o "%s" %s "%s"', ...
%                   jname, logFile, nodesArg, scriptName );

cmdLine = sprintf( 'qsub -N %s -j yes -o "%s" ',jname, logFile);

%we need to specify how many licenses of Matlab we need
%to use
%cmdLine = sprintf('%s -l mlab=%d ',cmdLine,props.NumberOfTasks);
%
%mlab=1 because we need 1 license per worker started
cmdLine = sprintf('%s -l mlab=1 ',cmdLine);
 %if jdep is not empty then add the option -hold_jid
   % -hold_jid - list of job ids which must complete sucessfully
   %    before running this job
    if ~isempty(jdep)
        if isnumeric(jdep)
            jdep=num2str(jdep);
            end
       cmdLine=sprintf('%s -hold_jid %s',cmdLine,jdep); 
    end


       cmdLine=sprintf('%s  %s "%s" ',cmdLine,nodesArg,scriptName); 
       
       fprintf('Submit command:\n %s \n',cmdLine);
       
[s, w] = system( cmdLine );

% Report an error if the script did not execute correctly.
if s
    warning( 'distcompexamples:generic:SGE', ...
             'Submit failed with the following message:\n%s', w);
    fprintf('submit command: %s\n',cmdLine);
else
    % The output of successful submissions shows the SGE job identifier%
    fprintf( 1, 'Job output will be written to: %s\nQSUB output: %s\n', logFile, w );
end

