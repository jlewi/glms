%function sgeSubmitFcn(scheduler,job,props, cluster,clusterdatadir)
%
%   cluster  - hostname of the cluster. This the name of the host
%              where qsub is executed
%   clusterdatadir - the directory on the cluster where the data should be
%             stored
%   jdep     - a string which is a comma separated list of jobIDs of jobs which must
%              complete before scheduling this job
%
function sgeRemoteParallelSubmitFcn( scheduler, job, props,cluster,clusterdatadir, varargin )
%sgeParallelSubmitFcn - parallel submission for SGE

% Copyright 2006 The MathWorks, Inc.
% $Revision$   $Date: 2006/12/27 20:40:57 $

%**************************************************************
%Parameters
%
%the directory containing the toolbox for job submission.
%this directory contains scripts for starting Matlab as well as the decode
%function
toolboxdir=fullfile(get(scheduler,'ClusterMatlabRoot'),'toolbox','neurolab');
%
%the name of the script which starts Matlab and runs the actual job
mlabstartscript=fullfile(toolboxdir,'sgeWrapper.sh');
%    
%*********************************************************************



  
jdep=[];

%process optional arguments
for index=1:2:length(varargin)
  fprintf('varargin(%d)=%s\n',index,varargin{index});
  switch varargin{index}
    case 'jdep'
    jdep=varargin{index+1};
    
   otherwise
     fprintf('Unrecognized option %s',varargin{index});
  end
end

 %*******************************************************************
    %create the job script
    %   this is the script the grid engine will execute on the worke node
    %   to start the job.
    %This script does two things
    % 1. calls sgeWrapper.sh to start Matlab and run the actual job
    % 2. calls rsync to rsync the job directory back to the local machine
    %********************************************************************
    
    localexec=fullfile(localdatalocation,props.JobLocation,execscript);
    remoteexec=fullfile(clusterdatadir,props.JobLocation,execscript);
    fid=fopen(localexec,'w');

    fprintf(fid,'#!/bin/sh \n');
    fprintf(fid, '# Ensure that under SGE, were in /bin/sh too \n');
    fprintf(fid,'#$ -S /bin/sh \n');
    fprintf(fid, '#$ -v MDCE_DECODE_FUNCTION,MDCE_STORAGE_LOCATION,MDCE_STORAGE_CONSTRUCTOR,MDCE_JOB_LOCATION,MDCE_TASK_LOCATION,MDCE_MATLAB_EXE,MDCE_MATLAB_ARGS,MDCE_DEBUG,MDCE_START_DIR \n');

scheduler.UserData = { cluster ; clusterdatadir };
localdatalocation = scheduler.DataLocation;
jobdata=job.JobData;



    
% Set up the environment for the decode function - the wrapper shell script
% will ensure that all these are forwarded to the MATLAB workers.
setenv( 'MDCE_DECODE_FUNCTION', 'sgeParallelDecode' );
setenv( 'MDCE_STORAGE_LOCATION', props.StorageLocation );
setenv( 'MDCE_STORAGE_CONSTRUCTOR',props.StorageConstructor );
setenv( 'MDCE_JOB_LOCATION', props.JobLocation );
% Ask the workers to print debug messages by default:
setenv('MDCE_DEBUG', 'true');

% Set this so that the script knows where to find MATLAB, MW_SMPD and MW_MPIEXEC
% on the cluster. This might be empty - the wrapper script will deal with that
% eventuality.
setenv( 'MDCE_CMR', scheduler.ClusterMatlabRoot );

% Set this so that the script knows where to find MATLAB, SMPD and MPIEXEC on
% the cluster. This might be empty - the wrapper script must deal with that.
setenv( 'MDCE_MATLAB_EXE', props.MatlabExecutable );
setenv( 'MDCE_MATLAB_ARGS', props.MatlabArguments);


% The wrapper script is in the same directory as this M-file
[dirpart] = fileparts( mfilename( 'fullpath' ) );
scriptName = fullfile( dirpart, 'sgeParallelWrapper.sh' );

% Forward the total number of tasks we're expecting to launch
setenv( 'MDCE_TOTAL_TASKS', num2str( props.NumberOfTasks ) );


%**************************************************************
%create the appropriate submit scripts
%   the submit script is a script which gets executed on the submit host
%   for the SGE.
%   This script sets the appropriate environment variables and then calls
%   qsub.
%******************************************************

% Choose a file for the output. Please note that currently, DataLocation refers
% to a directory on disk, but this may change in the future.
logFile = fullfile( scheduler.DataLocation, ...
                    sprintf( 'Job%d.mpiexec.out', job.ID ) );

% Choose a number of processors per node to use (you will need to customise
% this section to match your cluster).
nodesArg = sprintf( '-pe matlab %d', props.NumberOfTasks );

 if (isfield(job.UserData,'jobname') && ~isempty(job.UserData.jobname));
      jname=job.UserData.jobname;
    else
      jname=sprintf('Job%d',job.ID);
 end
      
% Finally, submit to SGE. note the following:
% "-N Job#" - specifies the job name
% "-j yes" joins together output and error streams
% "-o ..." specifies where standard output goes to
%cmdLine = sprintf( 'qsub -N %s -j yes -o "%s" %s "%s"', ...
%                   jname, logFile, nodesArg, scriptName );

cmdLine = sprintf( 'qsub -N %s -j yes -o "%s" ',jname, logFile);

%we need to specify how many licenses of Matlab we need
%to use
%cmdLine = sprintf('%s -l mlab=%d ',cmdLine,props.NumberOfTasks);

 %if jdep is not empty then add the option -hold_jid
   % -hold_jid - list of job ids which must complete sucessfully
   %    before running this job
    if ~isempty(jdep)
       cmdLine=sprintf('%s -hold_jid %s',cmdLine,jdep); 
    end


       cmdLine=sprintf('%s  %s "%s" ',cmdLine,nodesArg,scriptName); 
       
[s, w] = system( cmdLine );

% Report an error if the script did not execute correctly.
if s
    warning( 'distcompexamples:generic:SGE', ...
             'Submit failed with the following message:\n%s', w);
    fprintf('submit command: %s\n',cmdLine);
else
    % The output of successful submissions shows the SGE job identifier%
    fprintf( 1, 'Job output will be written to: %s\nQSUB output: %s\n', logFile, w );
end

