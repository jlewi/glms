#!/bin/sh
# Ensure that under SGE, we're in /bin/sh too:
#$ -S /bin/sh
# Pass through the right environment:
#$ -v MDCE_DECODE_FUNCTION,MDCE_STORAGE_LOCATION,MDCE_STORAGE_CONSTRUCTOR,MDCE_JOB_LOCATION,MDCE_CMR,MDCE_MATLAB_EXE,MDCE_MATLAB_ARGS,MDCE_TOTAL_TASKS,MDCE_DEBUG
#
# The previous line ensures that all required environment variables are
# forwarded from the client MATLAB session through the scheduler to the cluster.
# 
# This script uses the following environment variables set by the submit M-code:
# MDCE_CMR            - the value of ClusterMatlabRoot (may be empty)
# MDCE_MATLAB_EXE     - the MATLAB executable to use
# MDCE_MATLAB_ARGS    - the MATLAB args to use
#
# The following environment variables are forwarded through mpiexec:
# MDCE_DECODE_FUNCTION     - the decode function to use
# MDCE_STORAGE_LOCATION    - used by decode function 
# MDCE_STORAGE_CONSTRUCTOR - used by decode function 
# MDCE_JOB_LOCATION        - used by decode function 
#
# TMP                      - we reset TMP to /tmp and pass it through 
#                            because distcomp_evaluate_filetask
#                            calls tempname which requires the temp directory to exist because it cd's to it
#
# MPD_CON_EXT             - we forward this so that other nodes can send communincate with the MPD ring
#                           i.e other nodes issue mpdallexit 
# Copyright 2006 The MathWorks, Inc
# $Revision: 1.1.4.2 $   $Date: 2006/12/07 10:14:16 $
#
#
# $Revision$
#  Create full paths to mw_smpd/mw_mpiexec if needed
#
#
# Revisions:
#   08-11-2008
#      Set the environment variable "TMP" to /tmp/sgejob_####
#      Then add add TMP to the genvlist. We do this because Matlab uses
#      the function tempname which chokes if TMP is not a valid directory

MPD_BIN=/Applications/MATLAB_R2008a/mpich_mpd/bin
MPIEXEC_CODE=0

#set MPD_CON_EXT. This needs to match value set in startmatlabpe.sh
#otherwise we won't be able to communicate with the mpd ring associated with this job
export MPD_CON_EXT="sgejob_${JOB_ID}"

WDIR=/tmp/sgejob_${JOB_ID}
machines="$WDIR/machines"


echo "********************************************************************************"
echo
echo "sgeMpdWrapper.sh: starting"
echo
echo "********************************************************************************"
echo "TMP=$TMP"
echo "Setting TMP=$WDIR"
#set tmp to a directory that exists otherwise Matlab won't work.
export TMP="/tmp"
echo "MPD_CON_EXT=${MPD_CON_EXT}"

# Assert that we can read $WDIR/machines
if [ ! -r ${WDIR}/machines ]; then
    echo "Couldn't read ${WDIR}/machines" >&2
    exit 1
fi


# Work out how many processes to launch - set MACHINE_ARG
chooseMachineArg() {    
    MACHINE_ARG="-machinefile ${machines} -n ${NSLOTS}  "
}

runMpiexec() {
    # As a debug stage: echo the command line...
    
    echo \"${MPD_BIN}/mpiexec\"   \
        -l  -genvlist \
        MDCE_DECODE_FUNCTION,MDCE_STORAGE_LOCATION,MDCE_STORAGE_CONSTRUCTOR,MDCE_JOB_LOCATION,MDCE_DEBUG,TMP,MPD_CON_EXT \
        ${MACHINE_ARG} \
        \"${MDCE_MATLAB_EXE}\" ${MDCE_MATLAB_ARGS}
    
    # ...and then execute it
    eval \"${MPD_BIN}/mpiexec\"  \
        -l -genvlist \
        MDCE_DECODE_FUNCTION,MDCE_STORAGE_LOCATION,MDCE_STORAGE_CONSTRUCTOR,MDCE_JOB_LOCATION,MDCE_DEBUG,TMP,MPD_CON_EXT \
        ${MACHINE_ARG} \
        \"${MDCE_MATLAB_EXE}\" ${MDCE_MATLAB_ARGS}
    MPIEXEC_CODE=${?}
}

# Define the order in which we execute the stages defined above
MAIN() {  
    chooseMachineArg
    runMpiexec
    exit ${MPIEXEC_CODE}
}

# Call the MAIN loop
MAIN
