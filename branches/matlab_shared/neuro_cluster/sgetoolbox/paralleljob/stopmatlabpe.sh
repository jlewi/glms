#!/bin/sh

# Copyright 2006 The MathWorks, Inc.

# $Revision$
#
# This script is called whenever the Job finishes or if we issue a qdel to delete the job
# Therefore we want to issue a kill to the mpd job to make sure it exits if issued qdel
# 
# I think Matlab also tries to cleanup the MPD daemons by issueing MPD allexit.
# Therefore, I output some diagnostic information so that we can keep track of whats going on
# And figure out exactly what Matlab is doing, and more importantly if and why jobs fail to get cleaned up
#properly
MPD_BIN=/Applications/MATLAB_R2008a/mpich_mpd/bin;

#boolean variable indiciating whether the MPD ring is still up
#it gets set by the checkmpdring function
MPDRING=0;

#set MPD_CON_EXT. This needs to match value set in startmatlabpe.sh
#otherwise we won't be able to communicate with the mpd ring
export MPD_CON_EXT=sgejob_${JOB_ID}

#status message
echo
echo "********************************************************************************"
echo
echo "stopmatlabpe.sh: Starting cleanup of up parallel environment"
echo
echo "********************************************************************************"
echo
echo "MPD_CON_EXT=${MPD_CON_EXT}"

#Determine if we still have mpd's running


#************************************************************************
#Function to test if MPD ring is still up
#**************************************************************************
checkmpdring(){

echo "Run MPD trace to see if MPD's are still running."
${MPD_BIN}/mpdtrace
TEXIT=${?}

if [ $TEXIT -eq 0 ] ; then
    MPDRING=1;
else
    MPDRING=0;
fi

}
#*********************************************************************************
#Kill the MPI job using mpikilljob if it is still running
#******************************************************************************

#call checkmpdring to make sure ring is up
checkmpdring

#if MPD is still running. 
if [ $MPDRING -eq 1 ] ; then
    echo "MPD ring is still up."
    
    #test if the job is still running
    jout=`${MPD_BIN}/mpdlistjobs`

    #check if jout is empty
    slen=${#jout}
    if [ "$slen" -eq 0 ]; then
	echo "No Jobs are running."	
    else
	echo "Jobs are still running. output of mpdlistjobs is:"
	${MPD_BIN}/mpdlistjobs

        #we need to get the job id so that we can kill the job
	jid=`${MPD_BIN}/mpdlistjobs | awk '/^jobid/ {print $3;}' | uniq`;
	echo "mpdkilljob $jid "
	${MPD_BIN}/mpdkilljob $jid
    fi
fi

   


#*****************************************************************************
#destroy the ring if it is still up
#*****************************************************************************
#call checkmpdring to make see if MPD ring is still up
checkmpdring
if [ $MPDRING -eq 1 ]; then
    echo "MPD Ring is still up. Destroy mpd ring using mpdallexit"
    ${MPD_BIN}/mpdallexit
else
    echo "MPD ring is alredy gone. Most likely"
    echo "   1.  Matlab killed it by calling MPDallexit"
    echo "   2.  The ring was destroyed when issued mpdkilljob."
fi

echo
echo "********************************************************************************"
echo
echo "stopmatlabpe.sh: Finished cleanup of up parallel environment"
echo
echo "********************************************************************************"
echo