#!/bin/sh
#
# Revision: 
#    06-22-2008
#    no longer cd to the startdirectory
#       Instead we assume the decode function is already on the path
#
# Ensure that under SGE, we're in /bin/sh too:
#$ -S /bin/sh
#$ -v MDCE_DECODE_FUNCTION,MDCE_STORAGE_LOCATION,MDCE_STORAGE_CONSTRUCTOR,MDCE_JOB_LOCATION,MDCE_TASK_LOCATION,MDCE_MATLAB_EXE,MDCE_MATLAB_ARGS,MDCE_DEBUG,MDCE_START_DIR
#
#
# Copyright 2006 The MathWorks, Inc.
# $Revision$   $Date: 2006/12/27 20:41:00 $
echo start of sgeWrapper.sh >> ~/sgewrapper.out
echo Queue=$QUEUE     
echo Host=$HOSTNAME
echo executing sgeWrapper.sh to start distributed matlab job
#echo "cd to $MDCE_START_DIR"
#CD to the directory containing the decode function
#cd "$MDCE_START_DIR"
echo current directory: `pwd`
echo "Executing: $MDCE_MATLAB_EXE $MDCE_MATLAB_ARGS"
exec "${MDCE_MATLAB_EXE}" ${MDCE_MATLAB_ARGS}