%function distched(dldir)
%       dldir - A directory on a shared file system where the
%       job data should be stored. We set the DataLocation to this path.
%             - default value is "/Users/username/jobs"
%             - If you are submitting a job from your local machine
%               This should be set to a directory on your local machine.
%
%Explanation: This function creates the scheduler object
%  with the appropriate properties set for submitting distributed jobs
%
%
%
% $Revision$
%
%06-27-2008
%   change the submit
%    function to sgeremoteSubmitFcn based on the hostname
%06-22-2008
%   do not use '/~/jobs' as the data location. This ends up
%   evaluating to /Volumes/Raid-stripe/Users/jlewi. This path exists
%  on the cluster but not on the worker nodes. Which causes problems.
%
function sched=distsched(dldir)

sched=findResource('scheduler','type','generic');
%specify the directory where the Job Data is stored
%use a directory different from our project directory.
if ~exist('dldir','var')
  dldir=[];
end

if isempty(dldir)
[sys, username]=system('whoami');

%do not use '~' to specify the home directory
%the problem with '~' is that it can evaluate to paths like
%/Volumes/RAID-STRIPE
%which may not be the same on all machines.
%
%so dldir should specify a path which is accessible on all nodes.
%which wont 
dldir=sprintf('/Users/%s/jobs',deblank(username));

end
if ~exist(dldir,'dir')
  
  %make sure its not a file
  if exist(dldir,'file')
    error(['The directory for the DataLocation %s cannot be created because a file of that name already exists. Please specify a different location'],dldir);
  end
  
  %create the directory
  success=mkdir(dldir);

  if (success~=1)
      error('The directory for the DataLocation %s cannot be created. Please specify a different location',dldir);
  end
end
set(sched,'DataLocation',dldir);
set(sched,'HasSharedFilesystem',true);
set(sched,'SubmitFcn',{@sgeSubmitFcn});
set(sched,'ClusterMatlabRoot','/Applications/MATLAB_R2008a');


[sys, hname]=system('hostname -s');
hname=deblank(hname);

switch hname
 case {'cluster','portal'}
set(sched,'SubmitFcn',{@sgeSubmitFcn});   
otherwise
set(sched,'SubmitFcn',{@sgeremoteSubmitFcn});       
end

%setting for parallel job
%the sgeParallelSubmitFcn takes 1 argument - the directory where the
%sgeParallelDecode function is stored on the worker nodes.
set(sched,'ParallelSubmitFcn',{@sgeParallelSubmitFcn});
