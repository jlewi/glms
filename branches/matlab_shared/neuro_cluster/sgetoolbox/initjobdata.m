%function initjobdata (name)
%   name - used for naming the task;
%   finfiles - cell array of FilePath objects specifying input or data files.
%              These are files which are copied to the worker nodes and
%              used by our simulations.
%
%            - Each of these FilePath objects must specify the path
%              using a global variable named "RESULTSDIR"
%
%            
%   foutfiles - cell array of FilePath objects specifying the outputfiles
%               which are any matlab files containing results which need
%               to be copied back to our host machine.
%
%             - Each of these FilePath objects must specify the path
%              using a global variable named "RESULTSDIR"
%   startdir  - the directory from which our job should be run
%               avoid using '~'
%
%   startupfunc - a function handle to a function toexecute at the end of taskStartup.m
%              - this is useful for setting the path
%   localhost - The local host is the host name of the machine
%               from which we want to copy any input datafiles and
%               to which we copy any results files when we're done
%
%   localdatadir - The path on our local machine to substitute for
%               "localmachine"
%
%   taskstartupfunc - this is a function that you want to call
%               after Matlab's taskstartup.m function executes
%              this can be used for setting the path and other repetitive
%              functions
%              we would like to perform before the start of each task
%
%   localjobdir - directory on your local machine for the jobs
%                 If you are submitting from the head node you can leave
%                 this empty
%   clusterjobdir - directory on the cluster where you are submitting your
%                 jobs
%
%Explanation: 
%   This function create the basic structure that I use to pass information
%into jobs such as information about the hosts 
%and the files to copy. These values are used in JobStartup.m,
%taskStartup.m, taskFinish.m to set the appropriate directory in Matlab
%and copy the appropriate datafiles to the cluster. 
%
%
%this is specific to my cluster setup on Bayes
%
%$Revision$
%Revisions
%  06-24 - To specify the base path for data files on the cluster we use
%          the global variable "NL_JOBDIR". This global variable should
%          be set in taskStartup
%
function [jobdata,bcmd]=initjobdata(name,finfiles,foutfiles,startdir,startupfunc,localhost,localdatadir,localjobdir,clusterjobdir)

if ~exist('startdir','var')
  startdir=[];
end
if ~exist('startupfunc','var')
  startupfunc=[];
end
if ~exist('localhost','var')
  localhost='';
end

if ~exist('localdatadir','var')
  localdatadir=[];
end

if ~exist('localjobdir','var')
    localjobdir=[];
end

if ~exist('clusterjobdir','var')
    clusterjobdir=[];
end


%get the hogname to determine if we are submitting from the ehad node of the cluster
[s hname]=system('hostname -s');

hname=deblank(hname);

onhead=false;

switch hname
  case 'cluster'
    onhead=true;
  case 'portal2net'
    onhead=true;
end


if (onhead)
    if (~isempty(localjobdir))
        warning('You appear to be on the head node. localjobdir will be ignored.');
        
    end
    if (~isempty(clusterjobdir))
        warning('You appear to be on the head node. clusterjobdir will be ignored.');
    end
    
else
    if (isempty(localjobdir))
        error('You are not submitting from the head node of the cluster. localjobdir should not be empty.');
    end
        if (isempty(clusterjobdir))
        error('You are not submitting from the head node of the cluster. localjobdir should not be empty.');
        end
    end
    

%**************************************************************
%create the jobdata structure 
%This contains all the info needed to customize the taks
%****************************************************************
%cluster - Structure identifying the cluster for the purposes of rsyncing
%          files
%client  - structure identifying the client for the purpose of rsyncing
%           files
%startdir - directory to set working directory to
%
%filesin  - An array of RFilePath objects to copy to node before starting
%job
%
%filesout - An array of RFilePathObjects to copy to client after task
%completes

jobdata=struct('cluster',[],'client',[],'startdir',[],'filesin',[],'filesout',[]);
jobdata.client.hostid='client';
jobdata.client.host=localhost;
jobdata.client.RESULTSDIR=PathABS(localdatadir);

jobdata.cluster.hostid='cluster';
jobdata.cluster.host='';
%each lab needs to have a unique directory. It is not enough
%for the directory to be unique for each machine because multiple labs
%might run on the same machine
%to handle the uniqueness setpathvars ensures that the global variable
%RESULTSDIR contains the lab number in the path
jobdata.cluster.hostid='cluster';
jobdata.cluster.host='';
jobdata.cluster.RESULTSDIR=PathGlobalVar('NL_JOBDIR');


%name for the job
jobdata.name=name;


bcmd.(jobdata.client.hostid)=jobdata.client.RESULTSDIR;
bcmd.(jobdata.cluster.hostid)=jobdata.cluster.RESULTSDIR;

   %***********************************************************
    %Determine the inputfiles files
    %**********************************************************
   
    for j=1:length(finfiles)
        if iscell(finfiles)
        fname=finfiles{j};
        else
            fname=finfiles(j);
            end

 
        if isa(fname,'FilePath');
            %create an rpath object
            %so we create a RemoteFilePath object and then sync the file
            rfile=RemoteFilePath('bcmd',bcmd,'rpath',getrpath(fname),'isdir',isdir(fname));

            jobdata.filesin=[jobdata.filesin rfile];
        end
    end %loop over input files

    %***********************************************************
    %Determine the output filesrequired files
    %**********************************************************


    for j=1:length(foutfiles)
        if iscell(foutfiles)
            fname=foutfiles{j};
        else
            fname=foutfiles(j);
            end

        if isa(fname,'FilePath');
            %create an rpath object
            %so we create a RemoteFilePath object and then sync the file
            rfile=RemoteFilePath('bcmd',bcmd,'rpath',getrpath(fname),'isdir',isdir(fname));

            jobdata.filesout=[jobdata.filesout rfile];
        end
    end %loop over output files

    %the base of our code
%avoid relative paths or ~
jobdata.startdir=startdir;

if (~isempty(startupfunc) && ~isa(startupfunc,'function_handle'))
  error('startupfunc should either be empty or a ahandle to a function.');
end
jobdata.taskstartupfunc=startupfunc;

if ~isempty(localjobdir)
   if ~exist(localjobdir,'dir')
      error('If localjobdir is not empty it must specify a directory which exsists');       
   end
end

jobdata.localjobdir=localjobdir;
jobdata.clusterjobdir=clusterjobdir;
