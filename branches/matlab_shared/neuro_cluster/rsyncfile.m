%function ecode=rsyncfile(src,dest,user,statusfile)
%   src - describes src host
%       .host - name of host
%       .fname - path to file
%       .isdir  - indicates the path is a directory
%          default= false
%   dest - describes dest host
%        .host
%        .fname - path to file
%
%   user - 
%        - 08-08-25  This is currently not used
%          We might need it again in the future.
%       - the user we need to be. The user is important
%         because we need to have ssh set up for passwordless login
%         so the user's home directory matters for access to the
%         appropriate ssh keys
%   statusfile - a file to write status/debug information to
%
%Return value:
%    ecode = 0 no error
%          > 0 the exit code of the rsync command indicates an error
%          = -1 Error indicating we are not the right user and we aren't
%          root so we can't sudo to appropriate user
%
%
%Explanation:
%
%   Parallel Considerations:
%      we never throw an error in rsync because if we are running a
%      parallel job we might end up throwing an error on only some of the
%      nodes. In which case we could end up deadlocked.
%
%      To handle errors we return an appropriate value of ecode
%      The calling function is then responsible for handling the error.
%
%      We do it this way because rsync may or may not be running on all
%      workers. Therefore, doing a blocking operation for each file would
%      also cause deadlock.
%
%
%
%uses rsync to sync a data file between the client and the
%cluster.
%
%  If we are running under root we try to run rsync using the appropriate
%  username user by using sudo. We do this because on the Neurolab
%  cluster
%  Matlab is currently being started under the root user when running a
%  parallel job.
%
%  If we have to use sudo to copy the file we also use sudo to make the
%  file world writeable so that we can modify it without having to do sudo.
%
% $Revision$
%Revisions
%08-25-2008:
%   User field is now obsolete.
%   We no longer need to specify the user because it is assumed you are
%   running as the appropriate user.
%   For parallel jobs, each user now runs the mpi daemons as himself.
%   Thus, there is no longer any reason to do an sudo in order to change
%   the permissions of the files.
%
%04-05-2008: make sure the destination directory exists
%
%11-30-2007: switch back to rsync
function ecode=rsyncfile(src,dest,user,statusfile)

ecode=0;

%indicates source and destination are on the same machine.
localmode=true;

if ~exist('user','var')
    user=[];
end


if ~exist('statusfile','var')
    statusfile=[];
end

if ~isempty(statusfile)
    try
        fid=fopen(statusfile,'a');
    catch
        %if error occurs redirect to standard output
        fid=1;
    end
else
    fid=1;
end

%        cmd=['scp ' src.host ':' src.fname ' ' dest.host ':' dest.fname];

if ~isempty(src.host)
    src.host=[src.host ':'];
    localmode=false;
end

if ~isempty(dest.host)
    %append the character ':' to it
    dest.host=[dest.host ':'];
    localmode=false;
end

if isa(src.fname,'FilePath')
    src.fname=getpath(src.fname);
end

if isa(dest.fname,'FilePath');
    dest.fname=getpath(dest.fname);
end

if ~isfield(src,'isdir')
    src.isdir=false;
end

%*************************************************************************
%if the src is a directory
%make sure the final character is a trailing slash
%This way we can copy the contents of one directory to another directory
%when the diretories don't have the same name
%***********************************************************************
if (src.isdir)
    if (src.fname(end)~=pathsep)
        src.fname=[src.fname filesep];
    end

    if (dest.fname(end)~=pathsep)
        dest.fname=[dest.fname filesep];
    end
end

%ensure the destination directory exists
%we can only do this if the destination is local i.e dest.host is empty
if isempty(dest.host)
    [fdir]=fileparts(dest.fname);
    fprintf(fid,'rsync: reckmkdir %s \n', fdir);
    recmkdir(fdir);
end

cmd=[];

%specify a timeout so that if rsync tries to prompt for a password or
%something it will timeout in 10 minutes. Timeout is specified in seconds
%change time out 2 two hours

if (localmode)
    opt='-ae';
else
    %copying remote files use ssh
    opt='-ae ssh';
end
cmd=[cmd 'rsync --timeout=6000 ' opt src.host  src.fname ' ' dest.host  dest.fname];


%fprintf(fid,'cmd=%s\n',cmd);
fprintf(fid,'issusing rsync command \n');
%sudo should return the status of the called command so we should be ok
[ecode,rout]=system(cmd);

switch ecode
    case 0
        %rsync was successfull do nothing
        fprintf(fid,'rsyncfile.m: rsync successful \n rsync cmd=%s\n',cmd);
    case 30
        %timeout error
        fprintf(fid,'rsyncfile.m: error rsync timeout \n rsync cmd=%s\n',cmd);
        return;
    otherwise
        fprintf(fid,'rsyncfile.m: error in rsync: rsync exited with %d \n rsync cmd=%s\n',ecode,cmd);
        return;
end


fprintf(fid,'rsync: cmd output \n');
fprintf(fid,'%s  \n',rout);


fprintf(fid, 'rsync: finsihed\n');

try
    if (fid~=1)
        fclose(fid);
    end
catch
end

