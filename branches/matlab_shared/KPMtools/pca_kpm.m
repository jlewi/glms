function [pc_vec]=pca_kpm(features,N);
% [pc_vec]=pca_kpm(features,N)
% 
% Compute top N principal components.
% features(:,i) is the i'th example
% pc_vec(:,j) is the j'th basis function onto which you should project the data

[d ncases] = size(features);
fm=features-repmat(mean(features,2), 1, ncases);

if d*d < d*ncases
  C = cov(data);
  options.disp = 0;
  pc_vec = eigs(C, N, 'LM', options);
else
  [U,D,V] = svds(fm', N);
  pc_vec = V';
end


