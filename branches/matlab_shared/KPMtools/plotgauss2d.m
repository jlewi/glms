function h=plotgauss2d(mu, Sigma)
% PLOTGAUSS2 Plot a 2D Gaussian as an ellipse with cross hairs
% function h=plotgauss2(mu, Sigma)

[V,D]=eig(Sigma);
lam1 = D(1,1);
lam2 = D(2,2);
v1 = V(:,1);
v2 = V(:,2);
if v1(1)==0
  theta = 0; % horizontal
else
  theta = atan(v1(2)/v1(1));
end
a = sqrt(lam1);
b = sqrt(lam2);
h=plot_ellipse(mu(1), mu(2), theta, a,b);
hold on
minor1 = mu-a*v1; minor2 = mu+a*v1;
line([minor1(1) minor2(1)], [minor1(2) minor2(2)])
major1 = mu-b*v2; major2 = mu+b*v2;
hl=line([major1(1) major2(1)], [major1(2) major2(2)]);
%set(hl,'color','r')

