%function [cid,params]=constructid(var,con)
%   var - a cell array of field value pairs
%   con - a structure array describing each possible constructor
%       .rparams - cell array containing names of required parameters
%           -EMPTY - if this is empty then all constructors match
%
%
%Return Value:
%   cid - array of cfun value of the element in con whose required parameters
%         were supplied
%   params - conversion of var to a structure field array
%
%Explanation: This function is used by my objects to implement multile
%constructors.
%
%Revisions:
%  11-03-2008
%       return -1 instead of nan to indicate no much (this is because
%           switch cannot handle -1)
%       return 0 - constructor is load obj constructor
%                - possibilities are:
%                 1. No input arguments
%                 2. A structure with no fields 
%       
function [cind,params]=id(var,con)
        params=struct();
        
switch numel(var)
    case 0
        cind=Constructor.noargs;

        return;
    case 1
        %two different cases empty matrix or structure
        var=var{1};
        if isstruct(var)
           %two cases an empty structure or a structure which contains
           %the field name value paris of the inputs to the class
           pnames=fieldnames(var);
           if isempty(pnames)
               cind=Constructor.emptystruct;
               return;
           else
               params=var;
           end
        else
            if isempty(var)
                cind=Constructor.emptyin;
                return;
            else
                error('The input should never be just an array.');
            end
        end
        
    otherwise
        %var should be a structure of name, value pairs
        params=parseinputs(var);
        pnames=fieldnames(params);
end

%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor
%   2. we can resolve the constructor

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    %only match this constructor if its not empty

        ismatch=1;
        for f=1:length(con(j).rparams)
            %check if  parameter was passed in
            if isempty(strmatch(con(j).rparams{f},pnames))
                ismatch=0;
                break;
            end
        end
        %if constructor is blank; i.e no parameters required then it matches
        if (ismatch==1)
            cind=[cind j];
        end
   
end

if isempty(cind)
   cind=Constructor.nomatch; 
end

%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    params.(vars{j})=vars{j+1};
end
