%function dsets=copystruct(dsets,dind,data)
%       dsets - a structure array
%       dind  - index into dsets
%       data  - structure 
%
%Explanation: sets dsets(dind)=data
%   We use this function because data may not have all the fields
%   in dsets or it may have new fields which we want to be added
function dsets=copystruct(dsets,dind,data)
    
    if isempty(data)
        return;
    end
    fnames=fieldnames(data);
    for j=1:length(fnames)
        dsets(dind).(fnames{j})=data.(fnames{j});
    end