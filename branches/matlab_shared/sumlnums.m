%
%Explanation: sums a bunch of numbers, the log of which are passed into the
%function
%. To avoid numerical issues
%       1. we sum the elements like in a binary tree we do this 
%           to avoid summing numbers of different magnitudes to try to
%           avoid numerical error which would happen if you had many
%           numbers and did a running sum
%       2. we use the logarithm of the numbers as much as possible
function tot=sumlnums(sdata)
num=length(sdata);

%number of passes we need to make through the data
nlevels=ceil(log2(num));

nelements=num;
for l=1:nlevels
    
    
    %number of summations we need to do
    nsums=floor(nelements/2);
    for sind=0:(nsums-1)
        %we sum the elements at 2*sind and 2*sind+1
        %and store at position sind
        s=add2nums(sdata(2*(sind)+1),sdata(2*(sind)+2));
        sdata(sind+1)=s;
    end
    %check if there is an odd element
    if (mod(nelements,2)==1)
        sdata(nsums+1)=sdata(nelements);
    end
    
    %update number of elements remaining to be summed
    nelements=floor(nelements/2)+mod(nelements,2);
end
tot=sdata(1);
function s=add2nums(n1,n2)
%n1 and n2 are the log of the numbers we want to add
%we want to know which is small b\c we're going to pull it out
if (n1<n2)
    n2=n2-n1;
    s=n1+log(exp(n2)+1);
else   
    n1=n1-n2;
    s=n2+log(exp(n1)+1);
end