function B = eval_pdf_cond_parzen(data, mu, Sigma, N, unit_norm)
% EVAL_PDF_COND_PARZEN Evaluate the pdf of a conditional Parzen window
% function B = eval_pdf_cond_parzen(data, mu, Sigma, N)
%
% B(q,t) = Pr(data(:,t) | Q=q) = (1/N(q)) sum_{m=1}^{N(q)} K(data(:,t) - mu(:,q,m); sigma)
% where K() is a Gaussian kernel with variance sigma
% and N(q) is the number of mxiture components for case q (defaults to size(mu,3) if omitted)
%
% This is optimized for the case Q << min(T,M)

[d Q M] = size(mu);
[d T] = size(data);

if nargin < 4, N = repmat(M, 1, Q); end
if nargin < 5, unit_norm = 0; end

B = zeros(T,Q);
const1 = (2*pi*Sigma)^(-d/2);
const2 = -(1/(2*Sigma));
for q=1:Q
  muq = permute(mu(:,q,1:N(1)), [1 3 2]);
  if unit_norm
    %fprintf('using unit norm in parzen\n')
    D = 2 - 2*(data'*muq);
  else
    D = sqdist(data, muq); % D(t,m)
  end
  B(:,q) = (1/N(q)) * const1 * sum(exp(const2*D), 2);
end
B = B';

