function [mu, Sigma, B] = Mstep_clg(varargin)
% MSTEP_CLG Compute ML/MAP estimates for a conditional linear Gaussian
% [mu, Sigma, B] = Mstep_clg(...)
%
% We fit P(Y|X,Q=i) = N(Y; B_i X + mu_i, Sigma_i) 
% and w(i,t) = p(M(t)=i|y(t)) = posterior responsibility
% See www.ai.mit.edu/~murphyk/Papers/learncg.pdf.
%
% All inputs must be passed as 'param_name', param_value pairs.
%
% INPUTS:
% w(i) = sum_t w(i,t) = responsibilities for each mixture component
%  If there is only one mixture component (i.e., Q does not exist),
%  then w(i) = N = nsamples,  and 
%  all references to i can be replaced by 1.
% YY(:,:,i) = sum_t w(i,t) y(:,t) y(:,t)' = weighted outer product
% Y(:,i) = sum_t w(i,t) y(:,t) = weighted observations
% YTY(i) = sum_t w(i,t) y(:,t)' y(:,t) = weighted inner product
%   You only need to pass in YTY if Sigma is to be estimated as spherical.
%
% In the regression context, we must also pass in the following
% XX(:,:,i) = sum_t w(i,t) x(:,t) x(:,t)' = weighted outer product
% XY(i) = sum_t w(i,t) x(:,t)' y(:,t) = weighted cross product
% X(:,i) = sum_t w(i,t) x(:,t) = weighted inputs
%
% Optional inputs (default values in [])
%
% 'cov_type' - 'full', 'diag' or 'spherical' ['full']
% 'tied_cov' - 1 (Sigma) or 0 (Sigma_i) [0]
% 'clamped_cov' - pass in clamped value, or [] if unclamped [ [] ]
% 'clamped_mean' - pass in clamped value, or [] if unclamped [ [] ]
% 'clamped_weights' - pass in clamped value, or [] if unclamped [ [] ]
% 'cov_prior' - Lambda_i, added to YY(:,:,i) [0.01*eye(d,d,Q)]
%
% If cov is tied, Sigma has size d*d.
% But diagonal and spherical covariances are represented in full size.

[w, YY, Y, YTY, XX, XY, X, cov_type, tied_cov, ...
clamped_cov, clamped_mean, clamped_weights,  cov_prior] = ...
    process_options(varargin, 'w', [], 'YY', [], 'Y', [], 'YTY', [], ...
		    'XX', [], 'XY', [], 'X', [], ...
		    'cov_type', 'full', 'tied_cov', 0,  'clamped_cov', [], 'clamped_mean', [], ...
		    'clamped_weights', [], 'cov_prior', []);

[Ysz Q] = size(Y);

if isempty(X) % no regression
  %B = [];
  B2 = zeros(Ysz, 1, Q);
  for i=1:Q
    B(:,:,i) = B2(:,1:0,i); % make an empty array of size Ysz x 0 x Q
  end
  [mu, Sigma] = Mstep_cond_gauss(w, Y, YY, YTY, varargin{:});
  return;
end


N = sum(w);
if isempty(cov_prior)
  cov_prior = 0.01*eye(Ysz,Ysz,Q); 
end
YY = YY + cov_prior; % regularize the scatter matrix

% Set any zero weights to one before dividing
% This is valid because w(i)=0 => Y(:,i)=0, etc
w = w + (w==0);

Xsz = size(X,1);
% Append 1 to X to get Z
ZZ = zeros(Xsz+1, Xsz+1, Q);
ZY = zeros(Xsz+1, Ysz, Q);
for i=1:Q
  ZZ(:,:,i) = [XX(:,:,i)  X(:,i);
	       X(:,i)'    w(i)];
  ZY(:,:,i) = [XY(:,:,i);
	       Y(:,i)'];
end


%%% Estimate mean and regression 

if ~isempty(clamped_weights) & ~isempty(clamped_mean)
  B = clamped_weights;
  mu = clamped_mean;
end
if ~isempty(clamped_weights) & isempty(clamped_mean)
  B = clamped_weights;
  % eqn 5
  mu = zeros(Ysz, Q);
  for i=1:Q
    mu(:,i) = (Y(:,i) - B(:,:,i)*X(:,i)) / w(i);
  end
end
if isempty(clamped_weights) & ~isempty(clamped_mean)
  mu = clamped_mean;
  % eqn 3
  B = zeros(Ysz, Xsz, Q);
  for i=1:Q
    tmp = XY(:,:,i)' - mu(:,i)*X(:,i)';
    %B(:,:,i) = tmp * inv(XX(:,:,i));
    B(:,:,i) = (XX(:,:,i) \ tmp')';
  end
end
if isempty(clamped_weights) & isempty(clamped_mean)
  mu = zeros(Ysz, Q);
  B = zeros(Ysz, Xsz, Q);
  % Nothing is clamped, so we must estimate B and mu jointly
  for i=1:Q
    % eqn 9
    %A = ZY(:,:,i)' * inv(ZZ(:,:,i));
    A = (ZZ(:,:,i) \ ZY(:,:,i))';
    B(:,:,i) = A(:, 1:Xsz);
    mu(:,i) = A(:, Xsz+1);
  end
end

if ~isempty(clamped_cov)
  Sigma = clamped_cov;
  return;
end


%%% Estimate covariance

% Spherical
if cov_type(1)=='s'
  if ~tied
    Sigma = zeros(Ysz, Ysz, Q);
    for i=1:Q
      % eqn 16
      A = [B(:,:,i) mu(:,i)];
      s = tr(YY(:,:,i) + A'*A*SZZ(:,:,i) - 2*A*ZY(:,:,i)) / (Ysz*w(i));
      Sigma(:,:,i) = s*eye(Ysz,Ysz);
    end
  else
    S = 0;
    for i=1:Q
      % eqn 18
      A = [B(:,:,i) mu(:,i)];
      S = S + tr(YY(:,:,i) + A'*A*SZZ(:,:,i) - 2*A*ZY(:,:,i));
    end
    Sigma = repmat(S / (N*Ysz), [1 1 Q]);
  end
  return;
end

% Full/diagonal
if ~tied_cov
  Sigma = zeros(Ysz, Ysz, Q);
  for i=1:Q
    A = [B(:,:,i) mu(:,i)];
    % eqn 10
    SS = (YY(:,:,i) - ZY(:,:,i)'*A' - A*ZY(:,:,i) + A*ZZ(:,:,i)*A') / w(i);
    if cov_type(1)=='d'
      Sigma(:,:,i) = diag(diag(SS));
    else
      Sigma(:,:,i) = SS;
    end
  end
else % tied
  SS = zeros(Ysz, Ysz);
  for i=1:Q
    A = [B(:,:,i) mu(:,i)];
    % eqn 13
    SS = SS + (YY(:,:,i) - ZY(:,:,i)'*A' - A*ZY(:,:,i) + A*ZZ(:,:,i)*A');
  end
  SS = SS / N;
  if cov_type(1)=='d'
    Sigma = diag(diag(SS));
  else
    Sigma = SS;
  end
  Sigma = repmat(Sigma, [1 1 Q]);
end

  
