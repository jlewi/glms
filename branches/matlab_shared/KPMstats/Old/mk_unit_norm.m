function S = mk_unit_norm(M)
% MK_UNIT_NORM Make each column be a unit norm vector
% function S = mk_unit_norm(M)
% 
% We divide each column by its magnitude
%
% If M has more than 2 dimensions, we regard all but the first dimension as one long column.

if ndims(M)>2
  sz = size(M);
  s1 = prod(sz(1:end-1));
  s2 = sz(end);
  M = reshape(M, s1, s2);
  do_reshape = 1;
else
  do_reshape = 0;
end

[nrows ncols] = size(M);
S = M ./ repmat(sqrt(sum(M.^2)), [nrows 1]);

if do_reshape
  S = reshape(S, sz);
end

