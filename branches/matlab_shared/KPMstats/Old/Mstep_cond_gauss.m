function [mu, Sigma] = Mstep_cond_gauss(weights, m, op, ip, varargin)
% MSTEP_MOG Compute MLEs for mixture of Gaussians given expected sufficient statistics
% function [mu, Sigma] = Mstep_mog(weights, m, op, ip, ...)
%
% Notation:
% Y(t) = t'th observation, M(t) = hidden value of mixture variable
% w(i,t) = p(M(t)=i|y(t)) = posterior 
%
% INPUTS:
% weights(i) = sum_t w(i,t) = responsibilities for each mixture component
% m(:,i) = sum_t w(i,t) y(:,t) = weighted observations
% op(:,:,i) = sum_t w(i,t) y(:,t) y(:,t)' = weighted outer product
% ip(i) = sum_t w(i,t) y(:,t)' y(:,t) = weighted inner product
%
% Optional parameters may be passed as 'param_name', param_value pairs.
% Parameter names are shown below; default values in [] - if none, argument is mandatory.
%
% 'cov_type' - 'full', 'diag' or 'spherical' ['full']

[cov_type] = process_options(varargin, 'cov_type', 'full');

[O Q] = size(m);

mu = zeros(O,Q);
Sigma = zeros(O,O,Q);
for i=1:Q
  mu(:,i) = m(:,i) / weights(i);
  if cov_type(1) == 's'
    s2 = (1/O)*( (ip(i)/weights(i)) - mu(:,i)'*mu(:,i) );
    Sigma(:,:,i) = s2 * eye(O);
  else
    SS = op(:,:,i)/weights(i) - mu(:,i)*mu(:,i)';
    if cov_type(1)=='d'
      SS = diag(diag(SS));
    end
    Sigma(:,:,i) = SS;
  end
end
