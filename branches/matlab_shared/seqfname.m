%function [fname,snum]=seqfname(basename,padding,start)
%       basename - base file name including extension i.e file.ext
%       padding  - number of digits to pad to
%                   default - 3
%       start    - number at which to start
%                   default - 0
%Return:
%   fname - a string with file_###.ext where ### is a sequential number
%       representing the next availble file number
%   num  - the number appended to the filename
%   snum - string representation with padding
function [fname,num]=seqfname(basename,padding,start)
    if ~exist('start','var')
        start=1;
    end
    if ~exist('padding','var')
        padding=3;
    end
    
    %separate out parts of file
    [pathstr, bname,ext]=fileparts(basename);
    num=start;
    
    %add padding to number
    %add padding if number is < 10^(padding-1)
    if num<10^(padding-1)
        %add 10^padding to the number
        %convert to an integer and take all but the last character
        snum=num2str(num+10^padding);
        snum=snum(2:end);
    else
        snum=num2str(num);
    end
    numbname=sprintf('%s_%s',bname,snum);
    fname=fullfile(pathstr,[numbname ext]);
    while exist(fname,'file')
        %increment num
        num=num+1;
    
    %add padding to number
    %add padding if number is < 10^(padding-1)
    if num<10^(padding-1)
        %add 10^padding to the number
        %convert to an integer and take all but the last character
        snum=num2str(num+10^padding);
        snum=snum(2:end);
    else
        snum=num2str(num);
    end
        numbname=sprintf('%s_%s',bname,snum);
        fname=fullfile(pathstr,[numbname ext]);
    end
    
    