%[z b]= fittanh (x,y)
%   x - the x data
%   y - the y data
%   pts - pts to use when estimating z i.e y(pts(1):pts(2))
%         if you use all the points then causes problems
%         because things blow up
%Explanation:
%   fits a tanh function: y=b*(e^-zx-1)/(e^-zx+1);
function [z,b]=fittanh(x,y,pts)
    sthreshold=.1   %threshold  for finding flat part of curve
    sm=2;
    
    
    %compute b by looking at the flat part of the curve
    %get the slope of y
    s=diff(abs(y));
    
    
    
    %find the flat part of the curve
    %do this by finding all points with slope <=5 times the minimum slope
    %then take the median of these points
    mins=min(abs(s));
    indexes=find(s<=sm*mins);
    
    b=abs(y(indexes));
    b=median(b);
    
    %now find the slope z
    xfit=x(pts(1):pts(2));
    yfit=y(pts(1):pts(2));
    %the next line blows up if yfit>b which can happen
    %which is why we don't use all the pts
    yfit=log((yfit+b)./(b-yfit));
    
    p=polyfit(xfit,real(yfit),1);
    z=1/2*p(1);
    