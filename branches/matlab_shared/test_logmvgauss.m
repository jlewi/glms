%test harness for logmvgauss

d=2;
nsamps=1000;

%generate a random mean and covariance matrix
gpdf.m=ceil(rand(d,1)*100)/100;
gpdf.eigd=ceil(rand(d,1)*100)/100;
gpdf.evecs=orth(ceil(rand(d,d)*100)/100);
gpdf.c=gpdf.evecs*diag(gpdf.eigd)*gpdf.evecs;

%generate the random samples
x=ceil(rand(d,nsamps)*100)/100;
%compute the probability using logmvgauss
lpxmvgauss=logmvgauss(x,gpdf.m,gpdf.c);

%compute the true probability by using matlabs normfunction
lptrue=zeros(1,nsamps);
%project the samples onto the eigenvectors
%so we can treat each component as i.i.d gaussian
y=gpdf.evecs'*x;
lpy=zeros(d,nsamps);
for xind=1:nsamps
%    for dind=1:d
        
  %  lpy(dind,xind)=log(normpdf(y(dind,xind),gpdf.evecs'*gpdf.m(dind),gpdf.eigd(dind).^.5));
   % end
    lptrue(xind)=sum(log(normpdf(y(:,xind),gpdf.evecs'*gpdf.m,gpdf.eigd.^.5)),1);
end

err=lpxmvgauss-lptrue;

ind=find(abs(err)>10^-13);
if ~isempty(ind)
    error('Results did not match');
else
    fprintf('logmvgauss returned same results as normpdf \n');
end
