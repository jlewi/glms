% IM = pgmRead( FILENAME )
%
% Load a pgm image into a MatLab matrix.  
%   This format is accessible from the XV image browsing utility.
%   Only works for 8bit gray images (raw or ascii)

% Hany Farid, Spring '96.  Modified by Eero Simoncelli, 6/96.
% Modified by Jeremy to read color P6 format files

function im = pgmRead( fname );

[fid,msg] = fopen( fname, 'r' );

if (fid == -1)
  error(msg);
end

%%% First line contains ID string:
%%% "P1" = ascii bitmap, "P2" = ascii greymap,
%%% "P3" = ascii pixmap, "P4" = raw bitmap, 
%%% "P5" = raw greymap, "P6" = raw pixmap
TheLine = fgetl(fid);
format  = TheLine;		

if ~((format(1:2) == 'P2') | (format(1:2) == 'P5'))
  error('PGM file must be of type P2 or P5');
end

%%% Any number of comment lines
TheLine  = fgetl(fid);
while TheLine(1) == '#' 
	TheLine = fgetl(fid);
end

%%% dimensions
sz = sscanf(TheLine,'%d',2);
xdim = sz(1);
ydim = sz(2);
sz = xdim * ydim;

%%is this color??
if (format(1:2)=='P6')
    sz=sz * 3;
end

%%% Maximum pixel value
TheLine  = fgetl(fid);
maxval = sscanf(TheLine, '%d',1);

%%im  = zeros(dim,1);
if (format(2) == '2')
  [im,count]  = fscanf(fid,'%d',sz);
else
  [im,count]  = fread(fid,sz,'uchar');
end

fclose(fid);

if (count == sz)
    if (format(1:2)=='P6')
        %im = reshape( im, ydim, xdim,3 );
        img=zeros(ydim,xdim,3);
        index=1;
        xp=1;
        yp=1;
        %I'v left the following code in to illustrate the layout of pixels 
        %while(yp <=ydim);
        %    while (xp <= xdim)
        %        img(yp,xp,1)=im(index);
        %        img(yp,xp,2)=im(index+1);
        %        img(yp,xp,3)=im(index+2);
        %        index=index+3;
        %        xp=xp+1;
        %    end
        %    xp=1;
        %    yp=yp+1;

        %end
        %reshape the array
        %the following creates an array with 3 rows which correspond to the 3 color channels
        %each column is a pixel going across the x-coordinates
        im=reshape(im,3,xdim*ydim);
        img(:,:,1)=reshape(im(1,:),xdim,ydim)';
        img(:,:,2)=reshape(im(2,:),xdim,ydim)';
        img(:,:,3)=reshape(im(3,:),xdim,ydim)';
        im=img;
    else
        im = reshape( im, xdim, ydim)';
    end
    
else
  fprintf(1,'Warning: File ended early!');
  im = reshape( [im ; zeros(sz-count,1)], xdim, ydim)';
end
	  
