%SLIDESHOW    display a series of jpegs
%   SLIDESHOW(basename,ext,numframes)   displays images with a basename=basename. To this basename the function 
%       appends 'd.ext' where d is a number corresponding to the frame number and ranges from 1 to numframes.
%       & .ext specifies the file extension of the image
%   SLIDESHOW(basenum,ext,startnum,endnum) display images in same way but d ranges from startnum to endnum
%   SLIDESHOW(basenum,ext,startnum,framerate) framerate -number of frames to display. I.e if framerate=5 displays every 5th frame.
%this creates a slide show out of stored images 
%it is assumed that all images have the same base name but have a number appended to the end of the file
%it also assumes jpegs
function slideshow(basename,ext,startnum,endnum,framerate)
    if (exist('endnum')~=1)
        endnum=startnum;
        startnum=1;
    end
    if(exist('framerate')~=1)
        framerate=1;
    end
    
    %draw a colorbar
    figure
    colorbar
    figure
    
    %set preferences for image display
    iptsetpref('ImShowBorder','loose');         %turn border on 'loose' or off 'tight;, without border can't have title 
    iptsetpref('ImShowTruesize','manual');       %so image won't be automatically resized to true size
   
    for iteration=startnum:framerate:endnum
   
        file=[basename num2str(iteration) '.' ext];
        if(strcmp(ext,'ppm')==1)
            im=pgmReadColor(file);
            imshow(uint8(im));
        else
            imshow(file);
        end
        fprintf('Press a key to continue: \n');
        pause;

    end
    