%this function adds a box to an image
%purpose of this is to indicate the position of the sample in the image
%
%inputs
%   xpos,ypos -position of upper left corner of box
%                -for matrix ypos is row
%                -xpos is column
%   width,height -height, width of box
%   border       -thickness of border of box
%                -if = -1 box occupies the entire box
%   image        -image to which to add box
%   bcolor      -optional [1 x 3] vector specifing color of border

function output=addbox(xpos,ypos,bwidth,bheight, border, output,bcolor)
    %verify xpos, ypos are valid numbers
    %if they aren't just return the current output
    if ( ~(isnan(xpos) | isnan(ypos)))
    
            
        if (exist('bcolor')~=1)
            bcolor=[0,0,0];
        end
        
		%create the box
		box=zeros(bheight,bwidth,3);
		
		%make middle =1 so we don't suppress the middle
        if border >0
    		box(border:bheight-border,border:bwidth-border,:)=ones(bheight-2*border+1,bwidth-2*border+1,3);
        end
		
        %boundaries of image
        width=size(output,2);
        height=size(output,1);
        
		%make sure boundries of box are integers and within image
		startx=floor(xpos);
		starty=floor(ypos);
		stopx=startx+bwidth-1;
		stopy=starty+bheight-1;
		if (startx <1)
              startx=1;
		end
		if(stopx <1);
          %can happen b\c start x might be 0 & then we subtract wwidth
          stopx=2;
		end
		if (stopx >width)
          stopx=width;
		end
		
		if (starty <1)
          starty=1;
		end
		if(stopy <1)
          stopy=2;
		end
		if (stopy > height)
          stopy=height;
		end
		
        %recreate the box to fit the image
		box=box(starty-starty+1:stopy-starty+1,startx-startx+1:stopx-startx+1,:);
        
        %add box to image
        %this creates black border 
      	output(starty:stopy,startx:stopx,:)=box .*     output(starty:stopy,startx:stopx,:);
        %if color is specified then create the border of the specified color
	
            %invert the box so middle is zeros and border is one
            box= -1 * box;
            box= 1 +box;
            box(:,:,1)=bcolor(1) *box(:,:,1);
            box(:,:,2)=bcolor(2) *box(:,:,2);
            box(:,:,3)=bcolor(3) *box(:,:,3);
          	output(starty:stopy,startx:stopx,:)=box + output(starty:stopy,startx:stopx,:);        
    end
    

