%function creategetfun(cname,prop)
%   cname - name of class to create set method for
%   prop - name of property to create access method for
%        - can be cell array of properties to create set methods for
%Explanation: Creates a set method (i.e setprop.m) for the specified class
%and property
function creategetfun(cname,prop)

if ~iscell(prop)
   pname=prop; 
   prop=cell(1,1);
   prop{1}=pname;
end
%we need to find the directory containing the class
%loop through the directories on the path looking for one named
%@cname
pdirs=sarraytocell(path,pathsep);

cpath=[];

for j=1:length(pdirs)
    if exist(fullfile(pdirs{j},['@' cname]),'dir')
        cpath=fullfile(pdirs{j},['@' cname]);
        break;
    end
end

if isempty(cpath)
   fprintf('Could not find directory for class %s \n',cpath);
   return;
end

for pind=1:length(prop)
    pname=prop{pind};
    %if pname begins with set then don't append set to function name
    if ((length(pname)>=3) && (strcmp(lower(pname(1:3)),'set')))
        pname=pname(4:end);
        mname=sprintf('%s',prop{pind});
        mfile=sprintf('%s.m',prop{pind})
    else
        mname=sprintf('set%s',prop{pind});
        mfile=sprintf('set%s.m',prop{pind});
    end
    
%check if get method already exists
if exist(fullfile(cpath,mfile),'file')
    fprintf('set method already exists for %s. \n',pname);
    return;
end

%create the get method
fid=fopen(fullfile(cpath,mfile),'w');

fprintf(fid,'%%function obj=%s(obj,val)\n',mname);
fprintf(fid,'%%\t obj=%s object\n', cname);
fprintf(fid,'%% \n');
fprintf(fid,'%%Return value: \n');
fprintf(fid,'%%\t obj= the modified object \n');
fprintf(fid,'%%\n');
fprintf(fid,'function obj=%s(obj,val)\n',mname);
fprintf(fid,'\t obj.%s=val;\n', pname);

fclose(fid);

edit(fullfile(cpath,mfile));

end



   