%function creategetfun(cname,prop)
%   cname - name of class to create get method for
%   prop - name of property to create access method for
%        - can be cell array of properties to create get methods for
%Explanation: Creates a get method (i.e getprop.m) for the specified class
%and property
function creategetfun(cname,prop)

if ~iscell(prop)
   pname=prop; 
   prop=cell(1,1);
   prop{1}=pname;
end
%we need to find the directory containing the class
%loop through the directories on the path looking for one named
%@cname
pdirs=sarraytocell(path,pathsep);

cpath=[];

for j=1:length(pdirs)
    if exist(fullfile(pdirs{j},['@' cname]),'dir')
        cpath=fullfile(pdirs{j},['@' cname]);
        break;
    end
end

if isempty(cpath)
   fprintf('Could not find directory for class %s \n',cpath);
   return;
end

for pind=1:length(prop)
       %if pname begins with get then don't pre-pend get to function name
    pname=prop{pind};

    if ((length(pname)>=3) && (strcmp(lower(pname(1:3)),'get')))
           pname=pname(4:end);
        mname=sprintf('%s',prop{pind});
        mfile=sprintf('%s.m',prop{pind})
    else
        mname=sprintf('get%s',prop{pind});
        mfile=sprintf('get%s.m',prop{pind});
    end
%check if get method already exists

if exist(fullfile(cpath,mfile),'file')
    fprintf('Get method already exists for %s. \n',pname);
    return;
end

%create the get method
fid=fopen(fullfile(cpath,mfile),'w');

fprintf(fid,'%%function %s=%s(obj)\n', pname,mname);
fprintf(fid,'%%\t obj=%s object\n', cname);
fprintf(fid,'%% \n');
fprintf(fid,'%%Return value: \n');
fprintf(fid,'%%\t %s=obj.%s \n',pname,pname);
fprintf(fid,'%%\n');
fprintf(fid,'function %s=%s(obj)\n', pname,mname);
fprintf(fid,'\t %s=obj.%s;\n', pname,pname);

fclose(fid);

edit(fullfile(cpath,mfile));

end



   