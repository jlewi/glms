%function [mval,mind]=maxeval(eobj) 
%       eobj - EigObj
%   
%Return:
%   mval    - maximum eigenvalue
%   mind    - indexes of maximum eigenvalue. Possibly more than 1 if
%             multiplicitiy is greater than 1
% Explanation: returns the maximum eigenvalue.
% Checks the eig structure to see if its sorted. If its sorted it returns
% the eigenvalue without having to do a search.
%
% Revision History:
% 7-28-2007 check for multiplicities greater than 1
function [mval mind]=maxeval(edec)

    mind=0;

    if (edec.esorted==1)
        mind=length(edec.eigd);
        
        %check for multiplicities greater than 1
        while (mind>1)
            %two eigenvalues are equal if their difference is less than the
            %relative error
           if (edec.eigd(end)-edec.eigd(mind-1))/(edec.eigd(end))< eps
               mind=mind-1;
           else
               break;
           end
        end
        mind=length(edec.eigd):-1:mind;
        
    elseif (edec.esorted==-1)
        mind=1;
         %check for multiplicities greater than 1
        while (mind<length(edec.eigd))
            %two eigenvalues are equal if their difference is less than the
            %relative error
           if (edec.eigd(1)-edec.eigd(mind+1))/(edec.eigd(1))< eps
               mind=mind+1;
           else
               break;
           end
        end
        mind=1:mind;
    else        
  
    %find the maximum eigenvalue and check for repeated eigenvalues
    %check for repeated eigenvalues 
    mval=edec.eigd(1);
    mind=zeros(1,length(edec.eigd)); %array of 1's indicating whether element is a maximum eigenvalue
    
    for j=2:length(edec.eigd)
       if (edec.eigd(j)>mval)
           mval=edec.eigd(j);
           mind(1:j-1)=0;
           mind(j)=1;
       elseif ((mval-edec.eig(j))/(mval)< eps)
           %values are the same
           mind(j)=1;
       end
    end
    mind=find(mind==1);
    end
    
    [mval]= edec.eigd(mind);
