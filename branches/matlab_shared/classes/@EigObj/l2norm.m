%function l2=l2norm(eobj)
%
%Return Value:
%  l2 - The l2 norm of this matrix
%
function l2=l2norm(eobj)

l2=sum(eobj.evecs.^2*eobj.eigd.^2);
l2=l2^.5;