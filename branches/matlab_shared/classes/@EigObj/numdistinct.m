%function n=numdis(eobj)
%   eobj - eigendecomposition
%   threshold - optinal value for determining values are unique
%Explanation: comput the number of distinct eigenvalues
%
% 7-23-2007
%    threshold - is now stored as part of the object
function n=numdistinct(eobj)

    if (eobj.esorted==0)
        eobj=sorteig(eobj,1);
    end
    
    %compute the number of distinct eigenvectors
    n=length(find(abs(diff(eobj.eigd))>eobj.threshold==1));
