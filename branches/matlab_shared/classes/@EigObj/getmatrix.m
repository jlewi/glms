%function getmatrix(eobj)
%   eobj -EigObj
%
%Return value:
%   m - matrix corresponding to this eigendecomposition
function m=getmatrix(eobj)
m=eobj.evecs*diag(eobj.eigd)*eobj.evecs';