%function getinv=inv(obj)
%	 obj=EigObj object
% 
%Return value: 
%	 inv=obj.inv 
%
%    Compute the ivnerse of matrix
function invm=getinv(obj)

    invm=obj.evecs*diag(1./obj.eigd)*obj.evecs';
    
