% EigObj('evecs',evecs,'eigd',eigd)
%   evecs - eigen vectors to construct object from
%   eigd  - eigen values to construct from
% EigObj('estruct',es)
%   es - structure with fields
%      .evecs
%      .eigd
% EigObj('matrix',m)
%   m - matrix to construct eigendecomposition for
%
% Explanation: This class encapsulates an eigendecomposition. In addition
%   to eigenvectors and eigenvalues it stores information about whether
%   the eigenvalues are sorted or not. This information can be used to
%   efficiently retrieve the maximum and minimum eigenvalues.
%
% Revisions
%   10-28-2008 - Convert to new object model
%                get rid of field bname
%               still need to convert the constructor to use my template
%
%   08-14-2008 - Remove threshold as a parameter because we use the
%   formula's in Dongarra to determine the proper threshold.
classdef (ConstructOnLoad=true) EigObj
    properties(SetAccess=private,GetAccess=public)
        %**********************************************************
        %Define Members of object
        %**************************************************************
        % version - version number for the object
        %           store this as yearmonthdate
        %           using 2 digit format. This way version numbers follow numerical
        %           order
        %           -version numbers make it easier to maintain compatibility
        %               if you add and remove fields
        %
        %  esorted - +1 - sorted in ascending order
        %           - -1 - sorted in descending order
        %  evecs   - eigenvectors are column vectors
        %
        %declare the structure
        version =081028;
        evecs=[];
        eigd=[];
        esorted=[];
    end
    methods

        function obj=EigObj(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'evecs','eigd'};
            con(1).cfun=1;

            con(2).rparams={'matrix'};
            con(2).cfun=2;

            con(3).rparams={'estruct'};
            con(3).cfun=3;

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                 
                    return              

                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end

            

            
            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    %call seteig on the eigenvectors and eigenvalues passed in                 
                    obj=seteig(obj,params.evecs,params.eigd);
                case 2
                    %compute eigendecomposition of the matrix
                    %if matrix is not square throw an error
                    if (size(params.matrix,1)~=size(params.matrix,2))
                        error('Matrix must be square');
                    end
                    [evecs,eigd]=svd(params.matrix);             
                    obj=seteig(obj,evecs,eigd);
                case 3
                    %call seteig on the eigenvectors and eigenvalues passed in        
                    obj=seteig(obj,params.estruct.evecs,params.estruct.eigd);
                otherwise
                    error('Constructor not implemented')
            end

            %make the eigenvalues a column vector
            if (size(obj.eigd,2)>1)
                obj.eigd=obj.eigd';
            end

        end



       
    end
    methods(Static)
        iscorrect=testrank1(eobj);
    end
end
