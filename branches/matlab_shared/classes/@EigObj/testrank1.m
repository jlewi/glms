%function iscorrect=testrank1(eobj)
%       
%
%Explanation: This is a test harness for the rank 1 gu-eisenstat code.
%   We generate a set of random matrices and random updates and verify rank
%   1 update is correct
%
%Revisions history:
%   08-11-2008 - Added some specific test cases:
%       add test cases for when the magnitude of the
%   perturbation and the eigenvalues of the existing matrices have really
%   small magnitudes
%
%       specifically test eigenvalues which are negative and eigenvalues
%       which are mixed
function iscorrect=testrank1()
iscorrect=true;

nmats=10;   %number of matrices to generate
d=100;       %dimensionality of matrices
sigma=10;

%scaling factor for the order of the magnitudes of the eigenvalues of the
%existing matrix
evmags=[10^-10 10^-3 10^1 10^100];

%outer loop determines whether eigenvalues are positive, negative, or mixed
for esign=[1:3]
    for emag=evmags
        fprintf('*******************************************\n');
        fprintf('Test: magnitude = exp(10,%d) \n',log10(evmags));

        switch esign
            case 1
                smsg='positive';
            case 2
                smsg='negative';
            case 3
                smsg='mixed';
        end
        fprintf('Test: eigenvalue sign is %s \n',smsg);

        for mi=1:nmats
            fprintf('Test: repeat %d of %d \n',mi,nmats');
            %generate a random orth normal matrix for the eigenvector matrix
            emat=orth(normrnd(zeros(d,d),sigma*ones(d,d)));

            %generate the eigenvalues by randomly sampling a uniform distribution
            eigd=rand(d,1);
            switch esign
                case 2
                    %we want eigenvalues to be negative
                    eigd=-1*eigd;
                case 3
                    %we want eigenvalues to be half and half
                    eigd(1:floor(d/2),1)=-1*eigd(1:floor(d/2),1);
            end

            %scale eigd by the magnitude
            eigd=emag*eigd;

            %**********************************
            %create the eigen decomposition
            eorig=EigObj('evecs',emat,'eigd',eigd);

            %****************************************
            %generate the perturbationn
            %*******************************************
            %we don't want vup to be normalized because we want to make sure the
            %code properly handles this
            %random update
            vup=normrnd(zeros(d,1),sigma*ones(d,1));
            %random value for rho
            rho=normrnd(0,sigma);

            %we want the magnitude of the perturbation to be emag so we scale rho
            %10^(log10(emag)-1/2log10(vup'*vup)- log10(rho));
            p=log10(emag)-1/2*log10(vup'*vup)- log10(rho);
            rho=rho*10^p;


            %srho=sign(binornd(1,.5)-.5);


            %compute rank 1 update using
            eup=rankOneEigUpdate(eorig,vup,rho);
            if ~(verifyupdate(eup,eorig,vup,rho))
                error('Rank 1 update is not correct');
            end
        end
    end
end

fprintf('\n Testing Special Cases \n');

%**************************************************************************
%Special Case: rho=0
%*********************************************************************
mdim=6;
eigd=[-.3664; -.84332; -8.435; -10*ones(mdim-3,1)];
vup=[0; 0; 0; rand(mdim-3,1)*2];
rho=0;
evecs=orth(normrnd(zeros(mdim,mdim),ones(mdim,mdim)));
eorig=EigObj('evecs',evecs,'eigd',eigd);

eup=rankOneEigUpdate(eorig,vup,rho);

if ~(verifyupdate(eup,eorig,vup,rho))
    error('Rank 1 update is not correct for special case rho=0');
end

if (iscorrect)
    fprintf('Success: Rank 1 updates correct when rho=0 \n');
end
%**************************************************************************
%Some Special Cases:
%These are some examples I found while using my code that revealed bugs
%in my deflation code
%*********************************************************************
%special case 1:
%   You have 3 eigenvalues which are distinct - but perturbation for these
%       eigenvalues is 0
%   Remaining eigenvalues are all -10
%
%   This is a hard case because the Dongarra Condition for deflation
%   is  (eigd[j]-eigd[i])*s*c
%   s=0 if z[j]=0. Therefore this condition can be satisified
%   even though eigd[j] is not equal to eigd[i] and we need to make sure we
%   handle this correctly.
mdim=6;
eigd=[-.3664; -.84332; -8.435; -10*ones(mdim-3,1)];
vup=[0; 0; 0; rand(mdim-3,1)*2];
rho=8.2264;
evecs=orth(normrnd(zeros(mdim,mdim),ones(mdim,mdim)));
eorig=EigObj('evecs',evecs,'eigd',eigd);

eup=rankOneEigUpdate(eorig,vup,rho);

if ~(verifyupdate(eup,eorig,vup,rho))
    error('Rank 1 update is not correct for special case');
end

if (iscorrect)
    fprintf('Success: All rank 1 updates were accurate \n');
end