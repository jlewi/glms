%function [obj, ind]=sorteig(obj,sdir)
%   obj -eigobj
%   sdir - sort direction
%           +1 - ascending order
%           -1 - descending order
%
% Return value
%   obj - sorted eigendecomposition
%   ind - rearrangement to generate the ordering
%       
% Explanation - sorts the object if not already sorted otherwise does
% nothing
function [obj ind]=sorteig(obj,sdir)
    sdir=sign(sdir);
    if (sdir==0)
        error('sort direction should +1 or -1');
    end
    
    if (obj.esorted==0)
            %sort eigenvalues and eigenvectors
            dnonsing=find(size(obj.eigd)>1);    %find non-singleton dimension
            if (sdir>0)
               [obj.eigd ind]=sort(obj.eigd,dnonsing,'ascend');
            else
               [obj.eigd ind]=sort(obj.eigd,dnonsing,'descend');
            end
            
         obj.evecs=obj.evecs(:,ind);
    else
        if (sdir==obj.esorted)
            %do nothing
            ind=1:length(obj.eigd);
        else
           %reverse the order
           ind=length(obj.eigd):-1:1;
           obj.eigd=obj.eigd(ind);
           obj.evecs=obj.evecs(:,ind);
        end
    end
    
    %update esorted
    obj.esorted=sdir;
