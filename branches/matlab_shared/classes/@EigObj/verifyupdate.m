%function verifyupdate(eup,eorig,vup,rho)
%   eup - the updated eigendecomposition
%   eorig - original eigendecomposition
%   vup - the rank one vector
%   rho - scaling factor of vup
%
%Explantion verifies
%   That eup is eigendecomposition of our original matrix eig(A)=eorig
%   eup should be eig(A+rho*vup*vup')
%
%Return value:
%true- if update is correct
%
%Revision History:
%   08-11-2008 - Use the relative error as our check instead of absolute
%                 error
%   7-23-2007 - properly handle multiplcities greater than 1 when checking
%   eigenvectors
function iscorrect=verifyupdate(eup,eorig,vup,rho)
iscorrect=true;

truemat=getmatrix(eorig)+rho*vup*vup';
upmat=getmatrix(eup);

%threshold for error
threshold=10^-10;

if (any(abs((upmat-truemat)./truemat)>threshold));
    iscorrect=false;
    fprintf('Update is not correct');
end


%******************************************************
%compute the true eigendecomposition of the updated matrix
%*****************************************************
%debugging compute the eigenvalues
%use svd not eig because eig seems to return imaginary eigenvalues
%sometimes
[u s v]=svd(truemat);
%for a symmetric matrix the column of u and v should
%be equal up to the sign. For the eigendecomposition, u and v must have the
%same sign. Therefore if the column in u and corresponding column in v are
%not in the same direction then we need to flip the sign of the
%corresponding eigenvalue to get a valid eigen decomposition
s=diag(s);

sdir=diag(u'*v);
ind=find(sdir<0);
s(ind)=-1*s(ind);

etrue=EigObj('evecs',u,'eigd',s);
evtrue=[];
eigd=[];


%sort in ascending order;
etrue=sorteig(etrue,1);
eup=sorteig(eup,1);

%*******************************************************
%Check eigenvalues
%***************************************************
%when I was using eig to compute the eigendecomposition
%of the true matrix it was sometimes returning eigenvalues
%with an imaginary part that was close to zero
%this seems to be fixed by using the svd instead of eig
if any(~isreal(etrue.eigd))
    %check if the imaginary part is zero
    if (any(abs(imag(etrue.eigd))>threshold))
               error('True eigenvalues have non zero imaginary part.');
    else
        %since imaginary part is zero just take real part
       fprintf('Warning the true eigenvalues have an imaginary part but it is zero. \n');
       etrue=EigObj('evecs',getevecs(etrue),'eigd',real(geteigd(etrue)));
    end
end

%eigderr=sum((etrue.eigd-eup.eigd).^2);
if any(abs((etrue.eigd-eup.eigd))./etrue.eigd>threshold)
    error('verifyupdate: eigenvalues dont match');
    iscorrect=false;
end

%     %eigenvectors may not match because we can rotate the eigenvectors
%     %within the eigenspace of each eigenvalue therefore we measure the
%     %angle between the subspaces of the associated eigenvectors
i=1;
while i<length(etrue.eigd)
    j=i+1;
    %this should handle multiplicities >1
    %i.e if di=di+1=di+2=...
    while ((etrue.eigd(j)-etrue.eigd(i))<threshold)
                j=j+1;
            if (j>length(etrue.eigd))
            break;
        end
    end
    %j is index of last eigenvalue=eigd(i)
    j=j-1;
    j=min(j,length(etrue.eigd));
    %check if eigenspaces for these eigenvectors match
    if(abs(subspace(etrue.evecs(:,i:j),eup.evecs(:,i:j)))>10^-6)
         iscorrect=false;
        error('Space spanned by eigenvectors do not match');       
    end
    %start with next eigenvector
    i=j+1;
end

if (iscorrect==false)
    error('update is incorrect');
end
