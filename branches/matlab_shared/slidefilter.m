% function y=filter(x,filtcoef)
%    x   - data to filter
%    filtcoef - filter coef
%
% Return
%    y   - the filtered data
% Explanation:
%       applies a moving average filter of length len to the data x
%       result is the same length as x.
%       NOTE: FCN pads end of X with the first and last value of X to
%       prevent boundary effects
%
%04-22-208
%   I defined xconv but did not change the algorithm.
%   I think there is bug with even filters
% 10-10-2005
%   fixed alighnment issue. Result was not aligned 
%   properly if used oddlength filter
function y=filter(x,filtcoef)
    len=length(filtcoef);
    dim=size(x);
    if (dim(1))>1;
        %its a row vector take transpose
        x=x';
    end
    %pad edges
    xfilt=[ones(1,len-1)*x(1) x ones(1,len-1)*x(end)];
    %fcoeff=ones(1,len)*1/len;
    
    %xconv(j)=xfilt(j-len(filtcoef)+1:j)'*filtcoef(end:-1:1)
    %conv zero pads xfilt so that this works
    %length(xconv)=length(xfilt)+length(filtcoef)-1
    xconv=conv(xfilt,filtcoef);
    
    %y=xfilt(1,len-1+floor(len/2):end-len-ceil(len/2)+1);
    
     %xfilt is padded with ones
    %y=xfilt(1,len+floor(len/2):end-len-floor(len/2)+1); %this works for od
    %d values for len but not even
    if (len/2==floor(len/2))
        %its even
        %code before 6-06-2005
        %y=xfilt(1,len:end-len);
        %after 6-06-2005
        y=xconv(1,len+floor(len/2)-1:end-len-floor(len/2)); %this works for od
    else
        %its odd
        y=xconv(1,len+floor(len/2):end-len-floor(len/2)+1); %this works for od
    end