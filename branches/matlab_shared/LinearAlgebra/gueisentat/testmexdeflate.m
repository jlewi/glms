%7-23-2007
%
%Test my c- code for deflation. 
%For repeated eigenvalues my c code rotates the eigenvectors and
%perturbation so that only 1 eigenvector for each repeated eigenvalue is
%correlated with the perturbation
%
function testmexdeflate()
%this is the value of threshold as determined by our deflation rule 

%mdims=10.^[2:4];    %the dimensionalitys of the matrices to compute results for
mdims=[2:10];
mdims=100;
%mdims=[50 100:50:500];
%ndist=[10:50:100 200:100:mdims];    %number of distinct eigenvalues
ndist=[3:10];
ndist=[1];
%ndist=[1:mdims];

rhovals=[10^-10 10^-3 10^1 10^10];

%ndist=50;
einfo=[];

for rho=rhovals
for mind=1:length(mdims)
    %randomly generate the eigenvectors
    evecs=normrnd(zeros(mdims(mind),mdims(mind)),ones(mdims(mind),mdims(mind)));
    evecs=orth(evecs);
    %make sure its full rank
    %null(evecs');
    for nind=1:length(ndist)
        if (ndist(nind)>mdims(mind))
            break;
            warning('Skipping. number of distinct eigenvalues exceeds dimensionality');
        end
        %randomly generate the number of distinct eigenvalues
        einfo(mind,nind).edist=normrnd(zeros(ndist(nind),1),10*ones(ndist(nind),1));
        %sort in ascending order
        einfo(mind,nind).edist=sort(einfo(mind,nind).edist,'ascend');
        
        einfo(mind,nind).ndist=ndist(nind);  % number of distinct eigenvalues
        einfo(mind,nind).dim=mdims(mind);    %  dimensionality
        
        %create the eigenvalues
        %randomly determine how many eigenvalues will have multiplicity
        %greater than 1
%         einfo(mind,nind).nmultg1=ceil(rand(1,1)*min(ndist(nind),mdims(mind)-ndist(nind)));
        
        %randomly generate the muliplicty of each distinct eigenvalue
        %start by setting the multiplicity for all repeated eigenvalues to
        %2 and all distinct eigenvalues to one
        einfo(mind,nind).mult=ones(1,ndist(nind));
        
%         for j=1:einfo(mind,nind).nmultg1
%           einfo(mind,nind).mult=2;
%         end
        
        %now randomly generate the number of remaining repeats        
        for j=1:ndist(nind)
           %number of repeats left
           nleft=mdims(mind)-sum(einfo(mind,nind).mult);                               
           einfo(mind,nind).mult(j)=einfo(mind,nind).mult(j)+floor(rand(1,1)*nleft);
        end        
        %assign remaining repeats to last repeated eigenvalue
       nleft=mdims(mind)-sum(einfo(mind,nind).mult);           
       einfo(mind,nind).mult(end)=einfo(mind,nind).mult(end)+nleft;          
        
        %generate the eigenvalues
        einfo(mind,nind).eigd=zeros(1,mdims(mind));
        for j=1:ndist(nind)
            if (j>1)
                sind=sum(einfo(mind,nind).mult(1:j-1))+1;
            else
                sind=1;
            end
            eind=sind+einfo(mind,nind).mult(j)-1;
            einfo(mind,nind).eigd(sind:eind)=einfo(mind,nind).edist(j);
        end
        
        %create eobj
        eold=EigObj('evecs',evecs,'eigd',einfo(mind,nind).eigd);
        
        %random update 
        %random update
        vup=normrnd(zeros(mdims(mind),1),5*ones(mdims(mind),1));
     
        %in Dongarra87
        threshold=1/2*eps(l2norm(eold));        
        [reig revecs rvup ndeflate]=mexDeflate(eold.eigd,eold.evecs,vup,rho,threshold,eold.esorted);
        
       checkdeflation(eold,rho,vup,revecs,reig,rvup,ndeflate);
        
        %check that only 1 entry of vup is nonzero for each repeated
        %eigenvalue
%         for j=1:ndist(nind)
%            if (einfo(mind,nind).mult(j)>1)
%             if (j>1)
%                 sind=sum(einfo(mind,nind).mult(1:j-1))+1;
%             else
%                 sind=1;
%             end
%             eind=sind+einfo(mind,nind).mult(j)-1;
%               if any(rvup(sind+1:eind)~=0)
%                   error('entry of z not set to zero for repeated eigenvalue');
%               end
%            end
%         end
    end
end
end

%**************************************************************************
%Some Special Cases: 
%These are some examples I found while using my code that revealed bugs
%in my deflation code
%*********************************************************************
%special case 1:
%   You have 3 eigenvalues which are distinct - but perturbation for these
%       eigenvalues is 0
%   Remaining eigenvalues are all -10
%
%   This is a hard case because the Dongarra Condition for deflation
%   is  (eigd[j]-eigd[i])*s*c
%   s=0 if z[j]=0. Therefore this condition can be satisified
%   even though eigd[j] is not equal to eigd[i] and we need to make sure we
%   handle this correctly.
mdim=6;
eigd=[-.3664; -.84332; -8.435; -10*ones(mdim-3,1)];
vup=[0; 0; 0; rand(mdim-3,1)*2];
rho=8.2264;
evecs=orth(normrnd(zeros(mdim,mdim),ones(mdim,mdim)));
eold=EigObj('evecs',evecs,'eigd',eigd);
%in Dongarra87
threshold=1/2*eps(l2norm(eold));        
[reig revecs rvup ndeflate]=mexDeflate(eold.eigd,eold.evecs,vup,rho,threshold,eold.esorted);
        
checkdeflation(eold,rho,vup,revecs,reig,rvup,ndeflate);
        

fprintf('Success mexdeflate works \n');

%**********************************************************************
%function to verify deflation worked correctly
%   eold - original eigendecomposition
%   rho - rho for the perturbation
%   vup - the perturbation vector
%   revecs - the eigenvectors after deflation
%   reig   - eigenvalues after deflation
%   rvup   - perturbation after deflation
%   ndeflate - number of deflated eigenvalues
function checkdeflation(eold,rho,vup,revecs,reig,rvup,ndeflate)

ethresh=10^-10;

mdim=size(revecs,1);
 %check eigenvectors are orthonormal
        if any(abs(revecs'*revecs-diag(ones(1,mdim)))>10^-8)
           error('Rotated eigenvectors are not orthonormal'); 
        end

        %check perturbation is unchanged
        %we check this by check the perturbed matrices
        pmatorig=getmatrix(eold)+rho*vup*vup';
        pmatrot=revecs*diag(reig)*revecs'+rho*vup*vup';
        dp=pmatorig-pmatrot;
        if any(abs(dp)>ethresh)
           error('Rotated perturbation does not match'); 
        end
        
        %first n-ndeflate eigenvalues of reig should be sorted in ascending
        %order
        for j=1:(mdim-ndeflate-1)
            if (reig(j+1)<reig(j))
                error('Non deflated eigenvalues are not sorted in ascending order');
            end
        end
        
        %check last ndeflated eigenvalues are in descending order
        %order
        for j=(mdim-ndeflate+1):(mdim-1)
            if (reig(j+1)>reig(j))
                error(' deflated eigenvalues are not sorted in descending order');
            end
        end
        
        %check that z entries are zero
        if any(rvup((mdim-ndeflate+1):end)~=0)
            error('deflated entries of z are not zero');
        end