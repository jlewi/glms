%Create a .mat file containing data for the testdeflate program

%threshold=10^-8;

%mdims=10.^[2:4];    %the dimensionalitys of the matrices to compute results for
mdims=10;
%mdims=[50 100:50:500];
%ndist=[10:50:100 200:100:mdims];    %number of distinct eigenvalues
ndist=[4:10];
%ndist=50;
einfo=[];

rho=1;
for mind=1:length(mdims)
    %randomly generate the eigenvectors
    evecs=normrnd(zeros(mdims(mind),mdims(mind)),ones(mdims(mind),mdims(mind)));
    evecs=orth(evecs);
    %make sure its full rank
    %null(evecs');
    for nind=1:length(ndist)
        if (ndist(nind)>mdims(mind))
            break;
            warning('Skipping. number of distinct eigenvalues exceeds dimensionality');
        end
        %randomly generate the number of distinct eigenvalues
        einfo(mind,nind).edist=normrnd(zeros(ndist(nind),1),10*ones(ndist(nind),1));
        %sort in ascending order
        einfo(mind,nind).edist=sort(einfo(mind,nind).edist,'ascend');
        
        einfo(mind,nind).ndist=ndist(nind);  % number of distinct eigenvalues
        einfo(mind,nind).dim=mdims(mind);    %  dimensionality
        
        %create the eigenvalues
        %randomly determine how many eigenvalues will have multiplicity
        %greater than 1
%         einfo(mind,nind).nmultg1=ceil(rand(1,1)*min(ndist(nind),mdims(mind)-ndist(nind)));
        
        %randomly generate the muliplicty of each distinct eigenvalue
        %start by setting the multiplicity for all repeated eigenvalues to
        %2 and all distinct eigenvalues to one
        einfo(mind,nind).mult=ones(1,ndist(nind));
        
%         for j=1:einfo(mind,nind).nmultg1
%           einfo(mind,nind).mult=2;
%         end
        
        %now randomly generate the number of remaining repeats        
        for j=1:ndist(nind)
           %number of repeats left
           nleft=mdims(mind)-sum(einfo(mind,nind).mult);                               
           einfo(mind,nind).mult(j)=einfo(mind,nind).mult(j)+floor(rand(1,1)*nleft);
        end        
        %assign remaining repeats to last repeated eigenvalue
       nleft=mdims(mind)-sum(einfo(mind,nind).mult);           
       einfo(mind,nind).mult(end)=einfo(mind,nind).mult(end)+nleft;          
        
        %generate the eigenvalues
        einfo(mind,nind).eigd=zeros(1,mdims(mind));
        for j=1:ndist(nind)
            if (j>1)
                sind=sum(einfo(mind,nind).mult(1:j-1))+1;
            else
                sind=1;
            end
            eind=sind+einfo(mind,nind).mult(j)-1;
            einfo(mind,nind).eigd(sind:eind)=einfo(mind,nind).edist(j);
        end
        
        %create eobj
        eold=EigObj('evecs',evecs,'eigd',einfo(mind,nind).eigd);
        
        %random update 
        %random update
        vup=normrnd(zeros(mdims(mind),1),5*ones(mdims(mind),1));
     
        j=length(mdims)*(nind-1)+nind;
        data(j).eigd=eold.eigd;
        data(j).evecs=eold.evecs;
        data(j).vup=vup;
        data(j).threshold=threshold;
        data(j).sdir=eold.esorted;
        data(j).rho=rho;
    end
end
save(fullfile(MATLABPATH,'LinearAlgebra','gueisentat','testdeflate.mat'),'data');

