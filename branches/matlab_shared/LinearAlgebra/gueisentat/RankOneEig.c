#include "RankOneEig.h"
#include "math.h"
#include "mex.h"

/*********************************************************************************************
Author: Jeremy Lewi
Explanation: Implement the Gu-Eisenstat algorithm for Rank One Updates of the eigendecomposition
************************************************************************************************/


/**********************************************************************
Pre-processing
***********************************************************************/
// z - this is where the projection of the perturbation on the eigenvectors    will be stored it must be allocated before calling this function.
// rho - a pointer to rho - its value will be changed
void
preprocess (int neig, double eigd[], double evecs[], double v[], double z[],
	    double *rho)
{


}


/*******************************************************************************
undeflate
**********************************************************************************/
/*******************************************************************************
deflation
**********************************************************************************/
//Inputs:
//   neig - number of eigen values
//   eigd - array of eigen values-must be sorted
//   evecs - array of eigen vectors
//    z   - vector corresponding to the perturbation
//    rho - magnitude of perturbation. We need this to check deflationc criterion
//   threshold - value for deciding two eigenvalues are distinct
//   sdir  - direction of sorting
//         +1 - sorted in ascending order
//         -1 - sorted in descending order
//Outputs:
//   deigd -output-deflated eigenvalues
//                 - first neig-ndeflate are the nondeflated eigenvalues sorted in ascending order
//         - last ndeflate values are deflated eigenvalues in descending order
//   devecs-output
///  dz    - output
//   ndeflate - output number of deflated eigenvalues
//
//
//Return value
//  The sorting happens in place
//  We pass in pointers (arrays) to evecs and z
//  After a call to deflate the elements of evecs and z have been adjusted so that for each 
//  repeated eigenvector at most 1 eigenvector is correlated with z
//
//Revisions:
//     08-11-2008
//           When checking whether two eigenvalues should be considered equal
//           use the relative difference (i.e the relative difference normalized by the eigenvalue with the smaller magnitude)
//           Before I was using the absolute difference and this causes errors because if
//           the eigenvalues are small i.e 10^-10 they will be considered to be equal.
void
deflate (int neig, double eigd[], double evecs[], double z[], double rho,
	 double threshold, int sdir, double deigd[], double devecs[],
	 double dz[], double *ndeflate)
{



  //used to store coefficients of given s rotations
  //values for givens rotation
  //c= cos (theta)
  //s= sin(theta) see pg. 122 of Demmel book on numerical linear 
  double c;
  double s;


  //find indexes which are greater than threshold
  //we add the first entry  as well
  //therefore the indexes are of those di for which di -di-1 > threshold
  //therefore number of givens rotations is 
  //n-length(ind); which is the number of entries di+1=di

  int ngivens = 0;

  //indexes for deflations
  //we need to rearrange the deflated eigenvectors and eigenvalues 
  //so that the first n-ndeflate entries are sorted in ascending order
  //and the last ndeflate entries are sorted in descending order
  //head
  int head = 0;			//index for the next non-deflated eigenvalue
  int tail = neig - 1;		//index for the next deflated eigenvalue


  //subtract 2 because indexing is zero based
  //we need to loop through the eigenvalues in ascending order
  //we teriminate when i=neig-1 if eigenvalues are in ascending order
  //we                 i=o if eigenvalues are in descending order;
  int i;
  if (sdir == 1)
    {
      i = 0;
    }
  else
    {
      i = neig - 1;
    }
  int icont = 1;
  while ((i >= 0) & (i < neig))
    {
#ifdef DEBUG
      mexPrintf ("Continue deflation i=%d \n", i);
#endif
      // test for deflation
      // 08-14-2008
      // Criterion in Dongarr87 for deflation is  
      // if rho*z[i] is < threshold then we deflate this eigenvalue
      if (fabs (rho * z[i]) < threshold)
	{
#ifdef DEBUG
	  mexPrintf ("deflation abs(z[i])=%f \n", fabs (z[i]));
#endif
	  *ndeflate = *ndeflate + 1;
	  //since we deflate we copy this eigenvector, and eigenvalue to the end of the output
	  //arrays
	  dz[tail] = 0;
	  deigd[tail] = eigd[i];

	  //copy the eigenvector          
	  for (int row = 0; row < neig; row++)
	    {
	      //matlab uses column major ordering             
	      devecs[row + tail * neig] = evecs[row + i * neig];
	    }
	  //increment i
	  i = i + sdir;
	  tail = tail - 1;
	}
      else
	{
	  //1. Since this eigenvector is not deflated we copy it to the head of our output arrays
	  dz[head] = z[i];
	  deigd[head] = eigd[i];

	  //copy the eigenvector          
	  for (int row = 0; row < neig; row++)
	    {
	      //matlab uses column major ordering             
	      devecs[row + head * neig] = evecs[row + i * neig];
	    }
	  //head points to the eigenvector we just copied
	  //we don't increment head yet

	  //************************************************************
	  //If this eigenvalue is repeated then we use givens rotations to zero-out 
	  //z[j] and deflate it
	  // j should be the index of the next eigenvector in ascending order
	  int j = i + sdir;
	  //this should handle multiplicities >1
	  //i.e if di=di+1=di+2=...




	  // #ifdef DEBUG
	  //    mexPrintf ("Eigenvalue relative difference %d \n", (eigd[j] - eigd[i])/eigd[i]);
//#endif

	  //indicates whether we continue loop to test eigenvalues for deflation
	  bool cont = 1;

	  if ((j < 0) || (j >= neig))
	    {
	      cont = 0;
	    }
	  //we keep incrementing j, until we find an eigenvalue such that eigd[j]!=eigd[i]
	  while (cont)
	    {

	      double zmag = sqrt (dz[head] * dz[head] + z[j] * z[j]);
	      //values for givens rotation
	      //c= cos (theta)
	      //s= sin(theta) see pg. 122 of Demmel book on numerical linear 
	      c = dz[head] / zmag;
	      s = -z[j] / zmag;

	      //Test if eigenvalues are the same using the criterion in Dongarra87
	      if (fabs ((eigd[j] - eigd[i]) * c * s) < threshold)
		{
			//This if statement could be triggered for two reasons
			// 1. (eigd[j]-eigd[i])= 0  ( but z[j] !=0)
			// 2. z[j]= 0  (but eigd[j] != eigd[i])
			
			//
		  //we deflate these eigenvalues        

#ifdef DEBUG
		  mexPrintf
		    ("Deflating eigenvalue j=%d eig[j]=%f eigd[i]=%f \n", j,
		     eigd[j], eigd[i]);
#endif

		  *ndeflate = *ndeflate + 1;
		  //since we deflate we copy this eigenvector, and eigenvalue to the end of the output
		  //arrays
		  //note we use eigd[j] and not eigd[i] because there is the possibility that eigd[j]!=eigd[i]
		  deigd[tail] = eigd[j];


		 
		  //compute updated eigenvectors after the rotations
          //if z[j]= 0 and eigd[i] != eigd[j] then c=1 and s=0
          //therefore nothing is actually rotated. 
          //so the following code can handle both cases which might trigger this if block
		  //Warning: I'm doing this in place unlike in my orginal matlab coode
		  for (int row = 0; row < neig; row++)
		    {
		      //matlab uses column major ordering
		      double icol =
			c * devecs[row + head * neig] - s * evecs[row +
								  j * neig];
		      double jcol =
			s * devecs[row + head * neig] + c * evecs[row +
								  j * neig];
		      devecs[row + head * neig] = icol;
		      devecs[row + tail * neig] = jcol;
		    }


		  //therefore
		  //We set the values of the deflated perturbation vector
		  //the entry at dz[head] is zmag
		  //the deflated entry gets pushed to the end of the vector 
		  dz[head] = zmag;
		  dz[tail] = 0;

		  tail = tail - 1;

		  //increment j in direction of ascending eigenvalues
		  j = j + sdir;
		}

	      else
		{
		  //we don't deflate these eigenvalues
		  //terminate the loop
		  cont = 0;
		}

	      //make sure j is still valid
	      if ((j < 0) || (j >= neig))
		{
		  cont = 0;
		}
	    }			//end loop over j
	  head = head + 1;
	  //set i=j. we will now continue our loop. Incrementing j to
	  //look for eigd[i]=eigd[j]
	  i = j;
	}			//end givens rotations and deflation for this eigenvalue
    }//end loop over i
}
