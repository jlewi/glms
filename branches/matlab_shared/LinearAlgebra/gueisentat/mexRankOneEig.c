#include "mex.h"
#include <stdlib.h>

extern void dlaed4_(int *dim, int *i, double *d, double *z, double *delta, double *rho, double* eval, int* info);

//my modified version which is in the recompiled lapack library
//extern void dlewi4_(int *dim, int *i, double *d, double *z, double *delta, double *rho, double* eval, int* info);

#define fmsg         flowmsg(__FILE__,__LINE__)


void flowmsg(char* fname, int line){
 #ifdef DEBUG
  mexPrintf("File %s \t Line %d \n",fname,line);
  #endif
}


/*    d - current eigenvalues these need to be sorted in ascending order */
//     
void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {

  #ifdef DEBUG
     mexPrintf("Starting MexFunction \n");
  #endif
  //input parameters
  double *d,*z;
  double rho;

  //output parameters
  double *evals;        //eigenvalues
  double *evecs;        //eigenvectors

  //dimensionality
  int dim=0;
 
  int nrows;
  int ncols;



  double *delta;  
  // stores output of dlaed4.f
  //right hand side
  if (nrhs <3){
    mexErrMsgTxt("Expected 3 arguments: D, rho, z");
  }

  #ifdef DEBUG
  mexPrintf("Number of arguments=%d \n",nrhs);
  #endif
  //*********************************************************************************
  //input parameters
  //**********************************************************************************
   #ifdef DEBUG
     mexPrintf("Get input Parameters  \n");
  #endif
     if (nrhs <2){
       mexPrintf("Error: Two input parameters are required \n");
       return;
     }

    

  //get pointer to the array containing elements of the diagonal matrix
  #ifdef DEBUG
     mexPrintf("Pointer to D \n");
  #endif
  d=mxGetPr(prhs[0]);
  if (d==NULL){
    mexPrintf("Error: unable to get pointer to d \n");
  }
  fmsg;
  
  //get rho
 if (prhs[1]==NULL){
    mexPrintf("Error: second argument is null mx pointer \n");
    return;
  }
  fmsg;
  rho=*mxGetPr(prhs[1]);    
  if (z==NULL){
    mexPrintf("Error: unable to get rho \n");
  }
  #ifdef DEBUG
  mexPrintf("rho= %g \n", rho);
  #endif
  fmsg;

  
  if (prhs[2]==NULL){
    mexPrintf("Error: second argument is null mx pointer \n");
    return;
  }
  fmsg;
  z=mxGetPr(prhs[2]);    //get pointer to vector specifing the rank 1 update
  if (z==NULL){
    mexPrintf("Error: unable to get pointer to z \n");
  }
  fmsg;

  fmsg;
  nrows=mxGetM(prhs[0]);
  ncols=mxGetN(prhs[0]);
 
  //should verify its a vector
  if((nrows >1) && (ncols==1)){
    dim=nrows;
  }
  if ((nrows==1)&&(ncols>1)){
    dim=ncols;

  }
  //if its a scalar which is possible then dim is just 1
  //scalar could happen because of deflation
  if((nrows==1) && (ncols==1)){
    dim=1;
  }
 
  if (dim==0){
    mexErrMsgTxt("Not able to get dimension of z and d");
  }
  #ifdef DEBUG
  mexPrintf("Dimensionality = %d \n", dim);
  #endif
  fmsg;
  //**********************************************************************************
  //Allocate space for the arrays
   #ifdef DEBUG
     mexPrintf("Allocate space for output \n");
  #endif
  plhs[0]=mxCreateDoubleMatrix(dim, 1, mxREAL);
  evals=mxGetPr(plhs[0]);

  mxArray* mxdelta=mxCreateDoubleMatrix(dim,dim,mxREAL);
  delta=mxGetPr(mxdelta);

  //initialize delta
  for (int i=0;i<dim*dim;i++){
    delta[i]=0;
  }
  //**********************************************************************************
  //call dlaed4 once for each eigenvalue
  //*********************************************************************************
  int info=1; //stores success of dlaed
  fmsg;
  for(int index=0;index<dim;index++){
    //dlaed4 is a fortran function so we need to pass everything by reference
    int i=index+1;  //which value to compute
    fmsg;
    
    //delta(i,j) = d_j -lambda_i
    //         d_j old eigenvalue 
    // lambda_i = new eigenvalue
    dlaed4_( &dim,&i, d, z, delta+index*dim, &rho, evals+index, &info);
    

    #ifdef DEBUG
    mexPrintf("dim==%d\n",dim);
    mexPrintf("delta: \n");
    for (int j=0;j<dim;j++){
      mexPrintf("%f \t", delta[index*dim+j]);
    }
    mexPrintf("\n New Eigen Value =%g \n", evals[index]);
    #endif
    if (info>0){
      mexPrintf("Call to dlaed4 failed \n"); 
      mexPrintf("Value returned was =%g \n",evals[index]);
    }
  }


  //*************************************
  //clean up
  //if a single output was given then free delta
  //otherwise assign it to the output
  if (nlhs >=2){
    plhs[1]=mxdelta;
  }
  else{
    mxFree(mxdelta);
  }
    
  //get d the vector of the eigenvalues of the current matrix
  //nrows = mxGetM(prhs[0]);
  //ncols = mxGetN(prhs[0]);
 

 
  //A= mxGetPr(prhs[1]);
 


  //compute the eigen decomposition


 
}
