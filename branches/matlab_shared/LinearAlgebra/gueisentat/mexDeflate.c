#include "mex.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "RankOneEig.h"

/*****************************************************************************
Author: Jeremy Lewi
Date: 7-23-2007
Explanation: A Matlab function to perform the rotation part of deflation. This function rotates 
the eigenvectors so that for any eigenvector of multiplicity greater than 1 at most 1 eigenvector
is parallel to the perturbation.

Matlab Syntax:
Deflate(eigd,evecs,z,threshold,sdir)
   eigd - array of eigenvalues sorted in ascending order
   evecs - matrix of eigenvectors
   threshold - This should be the threshold used for deflation according to the rule in Dongarra87
               Thus, it should equal 1/2 * eps(l2norm(eobj))
               where l2norm(eobj) is the l2 norm of the matrix represented by the eigendecomposition

Revision History:


*********************************************************************************/

#define fmsg         flowmsg(__FILE__,__LINE__)


void
flowmsg (char *fname, int line)
{
#ifdef DEBUG
  mexPrintf ("File %s \t Line %d \n", fname, line);
#endif
}





/*    d - current eigenvalues these need to be sorted in ascending order */
//     
void
mexFunction (int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
{

  //number of expected arguments
  int nrhsexp = 6;
  //integer indicating an error occured and abort execution
  //cont=1; continue
  //cont=0; failed
  int cont = 1;
#ifdef DEBUG
  mexPrintf ("Starting MexFunction \n");
#endif

  //input parameters
  double *eigd;			// pointer to array of eigenvalues
  double *evecs;		//pointer to eigenvectors
  double *z;			//pointer to pertubation
  int neig;			// number of eigenvlaues
  double rho;	       //magnitude of perturbation
  double threshold;		//numerical thershold (see Dongarra87) used for deflation

  int sdir;			//specifies whether eigenvalues are sorted in ascending or descending order
  //output parameters
  double *rz;			//rotated pertubation
  double *revecs;		//rotated eigenvectors
  double *reigd;		//rotated eigenvalues
  double *ndeflate;		//number of deflated eigenvalues

  //dimensionality
  int dim = 0;

  int nrows;
  int ncols;

#ifdef DEBUG
  mexPrintf ("Number of arguments=%d \n", nrhs);  
#endif

  double *delta;
  // stores output of dlaed4.f
  //right hand side
  
  if (nrhs != nrhsexp)
    {
    	
      mexPrintf ("Number of arguments is wrong \n", nrhs);
      mexErrMsgTxt("Incorrect number of input arguments. \n Expected 6 arguments: eigd, evecs, z, rho, threshold, sdir");
      cont = 0;
	  //return;
    }
else{
//we don't want to try to get the input arguments
//unles the correct number of arguments were specified
//otherwise we will crash because we will try to access areas of memory which aren't valid	

  //*********************************************************************************
  //input parameters
  //**********************************************************************************
#ifdef DEBUG
  mexPrintf ("Get input Parameters  \n");
#endif




  //get pointer to the array containing eigenvalues
#ifdef DEBUG
  mexPrintf ("Pointer to eigd \n");
#endif
  eigd = mxGetPr (prhs[0]);
  if (eigd == NULL)
    {
      mexPrintf ("Error: unable to get pointer to eigd \n");
      cont = 0;
    }
  fmsg;

//get pointer to the array containing eigenvector
#ifdef DEBUG
  mexPrintf ("Pointer to evecs \n");
#endif
  evecs = mxGetPr (prhs[1]);
  if (evecs == NULL)
    {
      mexPrintf ("Error: unable to get pointer to evecs \n");
      cont = 0;
    }
  fmsg;


//get pointer to the array containing z
#ifdef DEBUG
  mexPrintf ("Pointer to z \n");
#endif
  z = mxGetPr (prhs[2]);
  if (z == NULL)
    {
      mexErrMsgTxt ("Error: unable to get pointer to z \n");
      cont = 0;
    }
  fmsg;


//get the number of eigenvalues by counting the rows of the eigenvectors
  neig = mxGetM (prhs[1]);
//  ncols=mxGetN(prhs[0]);

  //should verify its a vector
#ifdef DEBUG
  mexPrintf ("Dimensionality = %d \n", neig);
#endif
  fmsg;


//index for the parameter
  int rind = 3;
  if (prhs[rind] == NULL)
    {
      mexErrMsgTxt ("Error unable to get pointer to rho \n");
      cont = 0;
    }

  rho = *mxGetPr (prhs[rind]);

  //get threshold
  rind = 4;
  if (prhs[rind] == NULL)
    {
      mexErrMsgTxt ("Error unable to get pointer to threshold \n");
      cont = 0;
    }

  threshold = *mxGetPr (prhs[rind]);

  //get sdir
  rind = 5;
  if (prhs[rind] == NULL)
    {
      mexErrMsgTxt ("Error unable to get pointer to sdir \n");
      cont = 0;
    }
  sdir = (int) *mxGetPr (prhs[rind]);
}
/*********************************************
/call deflate
/this produces the new eigenvectors and rotated perturbation in place
so we first copy the inputs to the outputs
***********************************************************/
/**********************************************************************************/
//Allocate space for the output arrays 

 
#ifdef DEBUG
      mexPrintf ("Allocate space for output \n");
#endif

//always allocate space for the output arguments
//to avoid an Output arguments not assigned error

int oind = 0;

if (oind<nlhs){
      plhs[oind] = mxCreateDoubleMatrix (neig, 1, mxREAL);
      reigd = mxGetPr (plhs[oind]);
}
 
      oind = oind + 1;
 if (oind<nlhs){
      plhs[oind] = mxCreateDoubleMatrix (neig, neig, mxREAL);
      revecs = mxGetPr (plhs[oind]);
 }
 
 
      oind = oind + 1;
 if (oind<nlhs){
      plhs[oind] = mxCreateDoubleMatrix (neig, 1, mxREAL);
      rz = mxGetPr (plhs[oind]);
 }
 
      oind = oind + 1;
 if (oind<nlhs){
      plhs[oind] = mxCreateDoubleMatrix (1, 1, mxREAL);
      ndeflate = mxGetPr (plhs[oind]);
 }
 

if (oind>=nlhs){
 mexErrMsgTxt("Insufficient number of output arguments. You must supply all output arguments because the mex function uses them to store data during defaltion. \n");
cont=0;
}

  //only continue if cont=true;
  if (cont == 1)
    {
      deflate (neig, eigd, evecs, z, rho, threshold, sdir, reigd, revecs, rz,
	       ndeflate);
    }
}
