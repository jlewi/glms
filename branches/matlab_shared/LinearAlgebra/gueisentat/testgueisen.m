%08-01-2007
%
%Script to test the numerical issues I was having with GU-Eisenstat
%In particular why is the sign of one of my eigenvalues being flipped
dk=[-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.3216;-0.0168;-0.012;-0.0058;-0.0042;-0.002;-0.0003;-0.0001;-0.0001;-0.0001;-0.0001;-0.0001;-0.0001;-0.0001;-0.0001;-0.0001;-0.0001;-0.0001;-0.0001;-0.0001;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;-0;]

%from the secular equation we know the largest eigenvalue should remain
%zero if the secular equation evaluated at 0 is >0
fsec=@(x)(1/rho+sum(zk.^2./(dk-x)));
fsec=@(x)(1/rho+sum(zk.^2./(dmod-x)));