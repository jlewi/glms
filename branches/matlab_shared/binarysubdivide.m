%function location=binarysubdivide(data,threshold)
%   data= a vector of numbers which are strictly increasing
%   threshold = a value which is >= min of data and <= max of data
%
%Purpose
%   use binary subdivision to find the smallest index for which 
%   index = arg min_{index} threshold<=data(index)
%
% Returns -1 if there is no element >=r
function location=binarysubdivide(data,threshold)
    if ~isvector(data)
        error('data must be vector');
    end
    %three pointers which define where to look next for the element
    left=1;
    right=length(data);

    
    %verify that smallest element is not left
    if (data(left) >= threshold)
        location=left;
    else
        %verify that right element is >= r 
        %if not return -1
        if (data(right) <threshold)
            location=-1;
        else
            
            %do binary search 
            %at the end right always points to the desired location
            %this is because left never points to a location that satifies data(left)>=r
            %& we terminate when left and right are adjacent
            while(right > left+1)
                middle=floor((right+left)/2);
                
                if (data(middle) >=threshold)
                    right=middle;
                else
                    left=middle;
                end
            end
            location=right;
        end
    end
    
    