%this file determines the comparative amount of time to concatenate a row to a matrix using 3 diffferent
% methods
% 1. the cat command
% 2. [oldmatrix; new row]
% 3. [new row; old matrix]


NUMLOOPS=2000
row=1:100
matrix=[]

startime=cputime
for i=1: NUMLOOPS
    matrix=cat(1,matrix,row);
end
time=cputime -startime


matrix2=[];
startime=cputime
matrix2=zeros(3000,100);
for i=1: NUMLOOPS
    matrix2(i,:)=row;
end
time=cputime -startime

