%function datalength=getdatalength(obj)
%	 obj=DynArray object
% 
%Return value: 
%	 datalength=obj.datalength 
%
%   datalength = the actual size of the structure currently storing the
%  data. Some of this space is the extra entries which allow for future
%  growth.
function datalength=getdatalength(obj)
	 datalength=length(obj.data);
