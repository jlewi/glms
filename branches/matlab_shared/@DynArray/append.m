%function append(obj,d)
%   d- a list of numbers to append to the end of the list
%
%Explanation: appends a list of numbers growing the array as needed
%
function obj=append(obj,d)


    %grow the array if necessary
    obj=grow(obj,getlength(obj)+length(d));
    
    obj.data(getlength(obj)+1:getlength(obj)+length(d))=d;
    obj.length=getlength(obj)+length(d);
    
    
    