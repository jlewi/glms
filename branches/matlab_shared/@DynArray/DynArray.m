%function sim=DynArray('length','grow')
%   length - initial size of the array
%   grow - when capacity is exceeded how much should we grow the array by
%         this is specified as a fraction relative to the current size of
%         the array
%       i.e grow = 1 means the array doubles in size every time the
%       capacity is exceeded
% Explanation: Template for the constructor of a new class. This template
% shows how we can identify which constructor was called based on the
% parameters that were passed in.
%
%function obj=Dynarray('data')
%   create an array from the data data
function obj=DynArray(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'length','grow'};
con(1).cfun=1;

con(2).rparams={'data'};
con(2).cfun=2;
%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%length       - how many elements we actually have
%             which could be less than the size of data
%            - i.e length is the largest value of index which has been
%            passsed into setat
%       
%declare the structure
obj=struct('version',080409,'length',0,'grow',1,'data',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'DynArray');
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        obj.data=nan*zeros(params.length,1);
        obj.length=0;
    case 2
        obj.length=length(params.data);
        obj.data=params.data;
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
%if no base object
obj=class(obj,'DynArray');




    