%function all=getall(obj)
%	 obj=DynArray object
% 
%Return value: 
%	 all=obj.all 
%   Returns all elements up to lengt
function all=getall(obj)
	 all=obj.data(1:obj.length);
