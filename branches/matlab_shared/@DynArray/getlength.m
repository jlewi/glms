%function length=getlength(obj)
%	 obj=DynArray object
% 
%Return value: 
%	 length=obj.length 
%    Returns the actual number of elements stored in the data
function length=getlength(obj)
	 length=obj.length;
