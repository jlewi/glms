%function at=getat(obj,index)
%	 obj=DynArray object
%    index - index of the element to get
%Return value: 
%	 at=obj.at 
%
function at=getat(obj,index)
    if (index>obj.length)
        error('index exceeds current length');
    end
    at=obj.data(index);
