%function p=mvgauss(X,MU,SIGMA)
%
%   X      - (dxn) vector
%   MU     - (dx1) vector
%   SIGMA  - (dxd) matrix
%
%   Explanation:
%       Computes the probability DENSITY at location of X assuming a multivariate gaussian
%       distribution
%       with mean=mu and covarance matrix specified by sigma
%   to compute log of the probability use
%   logmvgauss
%
%   WARNING: THIS DOES NOT HANDLE THE CONTINUOUS DISCRETE CONVERSION
%       it returns the pdf not the probability.
% 11-13-2005
%   To deal with nearly singular covariance matrices
%   I add a small number to the covarance matrix along the diagonal
% 11-05-2005
%       -changed it so X is now dxn as opposed to dx1
function p=mvgauss(x,mu,sigma)
    
    dx=x-mu*ones(1,size(x,2));
    p=(-1/2)*inv(sigma)*dx;
    
    p=p.*(dx);
    p=sum(p,1);
    p=exp(p);
    z=(det(sigma+eps*eye(size(sigma))));
    
    p=(2*pi)^(-size(x,1)/2)*(z)^(-1/2)*p;
    