%filtcoef=gabor(targetr,pcomp,imgwidth,imgheight)
%
%   targetr     -1x2 - radius of excitatory region measured as distance along principal component
%                i.e the width of the excitatory lobe will be twice this
%   pcomp       -2 x 2 matrix where each column is one of the principal components of the gaussian
%               -this is given as [x1 x1]
%                                 [y1 y2]
%               -the two eigen vectors should be orthogonal in order to get a gaussian not sure what it will be if there not
%               -these will be made into unit vectors
%               -NOTE: SEE BELOW FOR INFORMATION ON COORDINATE SYSTEM USED
%
%   Explanation: Build an on center gabor patch. To build an offcenter just multiply by -1.
%                This will only construct a circular gabor patch.
%
%                Note: The size of filtcoef will be imgwidth,imgheight. But all coefficents that vary by more than half a period
%                from center are zero. This is because moving out from center of target.
%                
%   return:
%       FiltCoef    - is the filtercoefficents for a gaussian modulated by a two dimensional sinusoid. 
%                     size of filter is chosen to allow for one excitatory region surrounded by 2 inhibitory regions
%
%   Warning: Image Coordinates: The resulting matrix filtcoef is parameterized consistent with imshow, image etc
%            which means that filtcoef(y,x) is coordinate (x,y). WARNING: when imagesc displays an image (0,0) is upper left
%            corner and y increases going down. Therefore when specifing the principal eigen vectors you need to take this into account
%           i.e the vector [1;1] actually looks like (1,-1) when displayed or when used as a filter.

function filtcoef=gabor(targetr,pcomp)

warning('I think this is outdated look at gabortheta');

%make pcomp unit vectors
pcomp(:,1)=pcomp(:,1)/(pcomp(:,1)'*pcomp(:,1))^.5;
pcomp(:,2)=pcomp(:,2)/(pcomp(:,2)'*pcomp(:,2))^.5;

%determine which dimension is major and minor axis
if targetr(1)>targetr(2)
    majoraxis=pcomp(:,1);
    minoraxis=pcomp(:,2);
    axisratio=targetr(1)/targetr(2);
    majorwidth=targetr(1);
    minorwidth=targetr(2);
else
    majoraxis=pcomp(:,2);
    minoraxis=pcomp(:,1);
    axisratio=targetr(2)/targetr(1);
    majorwidth=targetr(2);
    minorwidth=targetr(1);
end
%compute angle between major axis and x-axis
%acos is between 0 & pi
angle=acos(majoraxis'*[1;0]);
angle=angle;

%construct an
%centeral region is on
%center of gabor corresponds to center of matrix
%spatial period = 2 times width of target, b\c want 1/2 the period=width of target
%therefore spatial frequency in radians is pi/(width of target)
k1=pi/targetr(1);
k2=pi/targetr(2);
targetwidth=targetr(1);
targetheight=targetr(2);

%total distance of 1 excitatory region and 1 inhibitory region along each principal componnet
totr(1)=floor(1.5*pi/k1);
totr(2)=floor(1.5*pi/k2);

%want to express these distances in terms of x & y so know how big to make the filter

%width of filter: Want the main lobe and two side lobes (one on each side)=1.5 times the
%spatial period.
%therefore we want to go out 1.5 * majoraxiswidth along the major axis in each direction from mean and 1.5 along minor axis
%so filter needs to be big enough to accomadte 3*majoraxiswidth and 3*minorwidth. Therefore compute width and height
%associated with each of these measurements and take max.
width=max(ceil(3*majorwidth*majoraxis(1)),ceil(3*minorwidth*minoraxis(1)));
height=max(ceil(3*majorwidth*majoraxis(2)),ceil(3*minorwidth*minoraxis(2)));
%totr so far is expressed along each principal component, so multiply by the eigen vectors and take max value
%width=2*max(totr(1)*pcomp(1,1),totr(1)*pcomp(1,2));
%height=2*max(totr(2)*pcomp(1,2),totr(2)*pcomp(2,2));;
%width=imgwidth;
%height=imgheight;

%make height, width odd, so that location of center is well defined 
if (width/2)==floor(width/2)
    width=width+1
end
if (height/2)==floor(height/2)
   height=height+1
end

mu=[ceil(width/2); ceil(height/2)];

%std deviations: 2 std's within mean contains 95% of the data. 
%Therefore set 2 std = width and height of our filters.
sigma(1)=totr(1)/2;
sigma(2)=totr(2)/2;

%covariance matrix
co=pcomp*[sigma(1)^2 0; 0 sigma(2)^2]*inv(pcomp);

%calculate the filter
filtcoef=zeros(height,width);
%create the gaussian
for y=1:height
    for x=1:width
        filtcoef(y,x)=(-1/2)*([x;y]-mu)'*inv(co)*([x;y]-mu);
        filtcoef(y,x)=exp(filtcoef(y,x));
        filtcoef(y,x)=1/((2*pi)^.5 * (det(co))^.5)*filtcoef(y,x);
    end
end

%compute the distance of each point from the center
%but we don't use euclidean distance but the distance along an ellipse


%add pi/2 b\c its angle perpindicular to major axis
%subtract 1 from mu because ellipsedist starts counting positions at zero not 1
%therefore if we don't subtract by 1 we will be off by 1
dist=ellipsedist(mu-1,axisratio,angle,width,height);

%maxdist is the maximum distance we want to go out along the ellipse
%note this is not euclidean distance
%this is the distance assoicated with going out a distance =3/4 the spatial period of the sinusoid. 3/4 from 1/2 the excitatory 
%region and 1 full inhibtory region. But 1/2 period =target width. Therefore need to go out by 1.5 targetwidth along major axis and
%recall first column of dist is y pixels
%maxpos is the indexes of the maximum position
maxpos=[floor(1.5*majorwidth*majoraxis(2)+mu(2)),floor(1.5*majorwidth*majoraxis(1))+mu(1)];
maxdist=dist(maxpos(1),maxpos(2));


%therefore adjust period of sinusoid so that maxdist=1.5period
k=5*pi/4/maxdist;


%multiply by the sinusoid
wave=zeros(height,width);
for y=1:height
    for x=1:width
        r=dist(y,x);
        
        %because the gabor patch is circular but we store in square matrix
        %we need to set all elements with r>totr to have a coefficent of zero
        if r>maxdist
            wave(y,x)=0;
        else
            wave(y,x)=cos(k*r);
        end
        
    end
end
%figure;subplot(1,2,1);imagesc(filtcoef);title('gaussian');
%subplot(1,2,2);imagesc(dist);title('dist');colorbar

%multiply by the sinusoid
filtcoef=filtcoef.* wave;
% 
% %make sure the filter coefficents sum to zero
 ctot=sum(sum(filtcoef));
% 
 if ctot <0
     %find positive elements
     pcoef=filtcoef>0;
     pcoef=-ctot/sum(sum(pcoef))*pcoef;
     filtcoef=filtcoef+pcoef;
 else
      %find negative elements
    ncoef=filtcoef<0;
    ncoef=-ctot/sum(sum(ncoef))*ncoef;
    filtcoef=filtcoef+ncoef;
end
    
