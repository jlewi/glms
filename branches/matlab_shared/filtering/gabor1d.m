%function gabor1d(width,cent)
%   width - length of the array to return
%   cent  - center of the gabor
%           0  corresponds to the middle of the filter
%   period - The duration of the gabor is 3/2*period
%   tau    - decay constant
%            exp(-period^2/(4tau)) = the amplitude
%           of the negative lobe as a fraction of the amplitude(peak) of
%           the positive lobe of the Gabor
% f= A exp(-(x-cent)/tau)cos(2*pi/period(x-cent)) I((x-cent)< 3/4*period)
%
%       we force the value of f to zero for points father away than 3/2
%       period from the center
function k=gabor1d(width,cent,period,tau)

if ~exist('period','var')
    %set the period such that
    %width/2-1/2 is 3/4 of the period
    period=4/3*(width/2-1/2);
end

if ~exist('tau','var')
   %set tau so that at negative peak the amplitude is
   %1/2 the peak at the center (which is 1)
   dx=period/2;
   tau=dx^2/log(2);
end
x=(1:width)-(1+width)/2;

%shift by cent
x=x-cent;
%sigmax=len/4-1/2;

%we want half the width be 3/2pi
%omega=3*pi/2*1/(width/2-1/2);
%omega=3*pi/2*1/(width/2-1/2);


%k=normpdf(x,0,sigmax).*cos((x)*omega);
k=exp(-x.^2/tau).*cos(2*pi/period*x);
%ktrue=amp/(2*pi*sigmax*sigmay)*exp(-(x).^2/(2*sigmax^2)-(y).^2/(2*sigmay^2)).*sin((x)*omega);

%normalize it
%but use the value at the center point
%kcent=normpdf(cent,0,sigmax);
ind=find(abs(x)>(3/4)*period);
k(ind)=0;