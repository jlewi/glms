%function offsets=indexoffset(indexes,d)
%
%   indexes = nxd matrix
%             each row specifies the indexes for an element, thus each column corresponds to
%             dimension
%             i.e indexes=[1 2]
%                         [3 5]
%             Means reaturn the indexes for elements (1,2) & (3,5)
%   d       =dimensions of matrix = d1,d2,d3 ...dn
%
%   Return Value
%       offsets - n x 1 vector specifing the offset for each row specified in indexes
%
%  Explanation= suppose you have a 2-d matrix A and you want to return A(2,1);
%               if indexes=[2,1] and you say A(indexes) this would return A(2) & A(1)
%               therefore what this function does is calculate what the single value offset of indexes is and returns it.
%               i.e if A is 2x2. and Indexes=[1,2] this function returns 3 b\c A(3) & A(2,1) are the same element
%       
%               The formula that this function uses, comes from looking up indexing in matlab and help and looking under
%               advanced.
%In general, the offset formula for an array with dimensions [d1 d2 d3 ... dn] using any subscripts
% (s1 s2 s3 ... sn) is
% (sn-1)(dn-1)(dn-2)...(d1)+(s(n-1)-1)(dn-2)...(d1)+...+(s2-1)(d1)+s
%
%   
function offsets=indexoffset(indexes,d)

    offsets=zeros(size(indexes,1),1);
    
    for ioff=1:size(indexes,1)
        index=0;
        for j=size(indexes,2):-1:2;
            s=indexes(ioff,j)-1;
            index=index+s*prod(d(1:j-1));
        end        
        index=index+indexes(ioff,1);
        
        offsets(ioff,1)=index;
    end
        