%setpaths
%
%Revisions:
%   11-01-2007- cleaned it up
%setpaths
PARENT=pwd;
MATLABPATH=fullfile(PARENT,'..','..','..','matlab');

cpaths=path;
r=strfind(cpaths,MATLABPATH);
if isempty(r)
    addpath(MATLABPATH);
end
%add matlab paths
setmatlabpath

%inline function to generate a path all subdirectories but
%exclude hidden directories
%generates a colon separated list of paths
alldirs=@(topdir)(regexprep(genpath(topdir),'[^:]*/\.[^:]*:',':'));


paths=alldirs(pwd);

%cpaths=path;

%11-23-2007
%addpath already checks for duplicat paths so we don't have to
%turn warning about duplicat paths off
warning('off','MATLAB:dispatcher:pathWarning')
addpath(paths);
warning('off','MATLAB:dispatcher:pathWarning')
