%function onenotetable(data,outfile)
%   data  - 2d cell array
%   fout - output file
%        - leave blank to return the document node instead of outputting to
%        a file
%explanation: create xml for a onenote table from a 2-d cell array
%   each entry of the table contains the entry for a cell in the onenote
%   table. The entries can contain nested tables
%
%   Figures: To include a figure, specify a structure which has a field
%   "hf" or FigObj which is a handle to the figure.
%
%   Objects:
%
%
%Dependecies:
%   base64encode.m
%   abspath
%
%Author: Jeremy Lewi
%email: jlewi@gatech.edu
%url:
%Date: 08-16-2007
%
%
%Revisions:
%   11-19-07 - recognize FigObj's
function [outfile]=onenotetable(data,outfile)

if ~exist('outfile','var')
    outfile=[];
end

%XML document.
docNode = com.mathworks.xml.XMLUtils.createDocument('http://schemas.microsoft.com/office/onenote/2007/onenote','one:Outline');
%docNode.setDocumentURI('http://schemas.microsoft.com/office/onenote/2007/onenote');
docRootNode = docNode.getDocumentElement;

oechildren=docNode.createElement('one:OEChildren');
docRootNode.appendChild(oechildren);

oe=createTableNode(docNode,data);
oechildren.appendChild(oe);

if ~isempty(outfile)
    % Save the sample XML document.
    %WARNING THIS SEEMS TO CAUSE PROBLEMS IF you have relative paths
    xmlwrite(abspath(outfile),docNode);
    fprintf('Saved note to %s \n',outfile);
else
    outfile=docNode;
end

function oe=createTableNode(docNode,data)

if (length(size(data))>3)
    error('data must be 2-d cell array');
end

ncols=size(data,2);
nrows=size(data,1);

oe=docNode.createElement('one:OE');

table=docNode.createElement('one:Table');
table.setAttribute('bordersVisible','true');
oe.appendChild(table);

columns=docNode.createElement('one:Columns');
table.appendChild(columns);

for j=1:ncols
    column=docNode.createElement('one:Column');
    column.setAttribute('index',num2str(j-1));
    column.setAttribute('width',num2str(30));
    columns.appendChild(column);
end

for rind=1:nrows
    row=docNode.createElement('one:Row');
    table.appendChild(row);
    for cind=1:ncols
        cell=docNode.createElement('one:Cell');
        row.appendChild(cell);
        coechildren=docNode.createElement('one:OEChildren');
        cell.appendChild(coechildren);


        %determine if cell is array which we need to process recursively
        isarray=false;
        if iscell(data)
            %check if its a string
            if isstr(data{rind,cind})
                isarray=false;
            else
                %get the size of the element
                if (numel(data{rind,cind})>1)
                    isarray=true;
                elseif iscell(data{rind,cind})
                    isarray=true;
                end
            end
        else
            %data is a matrix (a string?)
            %so element is not a matrix
            isarray=false;
        end
        %if its a matrix recursively call onenotetable
        %get the data stored at the current entry
        if iscell(data)
            dentry=data{rind,cind};
        else
            dentry=data(rind,cind);
        end

        %if its an object
        %try to call objinfo
        %if objinfo is defined we create a table for it
        if isobject(dentry)
            if isa(dentry,'FigObj')

                coe=processfig(docNode,gethf(dentry));
            else
                %try to call objinfo on the object
                %if that fails create
                try
                    dentry=objinfo(dentry);
                    isarray=true;
                catch
                    %objinfo is probably not defined for this object
                    %default just print the class
                    dentry=['object type ' class(dentry)];
                end
                %make sure its not empty
                if ~isempty(dentry)
                    coe=createTableNode(docNode,dentry);
                else
                    coe=docNode.createElement('one:OE');

                    tcell=docNode.createElement('one:T');
                    coe.appendChild(tcell);
                    if iscell(dentry)
                        tcell.setTextContent('{}');
                    else
                        tcell.setTextContent('[]');
                    end
                end
            end
        elseif(isarray)
            %make sure its not empty
            if ~isempty(dentry)
                coe=createTableNode(docNode,dentry);
            else
                coe=docNode.createElement('one:OE');

                tcell=docNode.createElement('one:T');
                coe.appendChild(tcell);
                if iscell(dentry)
                    tcell.setTextContent('{}');
                else
                    tcell.setTextContent('[]');
                end
            end
        else
            %determine if its a figure
            if isfield(dentry,'hf')

                coe=processfig(docNode,dentry.hf);
                %the following will cause problems I think
                %because we process the element twice.
          %  elseif ishandle(dentry)
           %     coe=processfig(docNode,dentry);
            else

                coe=docNode.createElement('one:OE');

                tcell=docNode.createElement('one:T');
                coe.appendChild(tcell);

                if isnumeric(dentry)
                    tcell.setTextContent(num2str(dentry));
                elseif islogical(dentry)
                    if (dentry)
                        tcell.setTextContent('true');
                    else
                        tcell.setTextContent('false');
                    end
                else
                    tcell.setTextContent(dentry);

                end
            end
        end
        coechildren.appendChild(coe);

    end% loop over columns
end %loop over rows

%create an element to represent a figure
function coe=processfig(docNode,fh)
%its a figure
%save it as a png
if exist('/tmp','dir')

    coe=docNode.createElement('one:OE');

    %fname=fullfile('/tmp/','onenotetable.png');
    %set paper position to manual so it honors the values in paperposition
    set(fh,'PaperPositionMode','manual')
    %set(fh,'Paper
    % saveas(fh,fname);

    %fname=savepng(fh,fullfile('/tmp','onenotetable.png'));
    fname=fullfile('/tmp','onenotetable.png');
    saveas(fh,fname);

    %read the png file into a binary stream
    fid=fopen(fname,'r');
    pngbytes=fread(fid,inf,'uint8');
    fclose(fid);
    %create a java object to convert this to a base64
    %stream
    try
        bytes64=base64encode(pngbytes,'');
        inode=docNode.createElement('one:Image');
        inode.setAttribute('format','png');
        odata=docNode.createElement('one:Data');
        odata.setTextContent(char(bytes64'));
        inode.appendChild(odata);
        coe.appendChild(inode);
    catch

        warning('Could not convert image to base 64 stream. Most likely missing function base64encode.');
        fprintf('Error: %s \n', lasterror.message);
        tcell=docNode.createElement('one:T');
        coe.appendChild(tcell);
        tcell.setTextContent('missing image');
    end



else
    warning('Cannot save figure to png because /tmp doesnt exist');
end