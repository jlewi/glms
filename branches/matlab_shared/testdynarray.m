%04-09-2008
%
%Test harness for dynarray


%***************************************************************
%test creating an empty array
%****************************************************************
startsize=20;
da=DynArray('length',startsize,'grow',1);

%set the elements
for j=1:ceil(startsize*1.5)
   da=setat(da,j,j); 
end
    
%make sure the grow function caused the actual length to double
if (getdatalength(da)~=2*startsize)
    error('when the array grew it does not appear to have doubled in size');
end

%verify the elements

for j=1:ceil(startsize*1.5)
    if (getat(da,j)~=j)
        error('getat returned an incorrect value');
    end
end
    
%***********************************************************
%create it from an array
%***************************************************************
data=[1:100]';
da=DynArray('data',data);

   
if (getdatalength(da)~=length(data))
    error('initial size of data is wrong');
end

if (getlength(da)~=length(data))
    error('initial length is wrong');
end
%verify the elements

for j=1:length(data)
    if (getat(da,j)~=data(j))
        error('getat returned an incorrect value');
    end
end
    
%*****************************************************************
%try appending some elements to it
%*************************************************************
dtoappend=[2:25]';
data=[data;dtoappend];
da=append(da,[2:25]);


if (getlength(da)~=length(data))
   error('Length is wrong after append'); 
end

if any(getall(da)~=data)
    error('Data incorrect after append');
end

fprintf('Success: DynArray works \n');