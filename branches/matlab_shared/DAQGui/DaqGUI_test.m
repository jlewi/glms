function varargout = daqgui(varargin)
% DAQGUI M-file for daqgui.fig
%      DAQGUI, by itself, creates a new DAQGUI or raises the existing
%      singleton*.
%
%      H = DAQGUI returns the handle to a new DAQGUI or the handle to
%      the existing singleton*.
%
%      DAQGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DAQGUI.M with the given input arguments.
%
%      DAQGUI('Property','Value',...) creates a new DAQGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before daqgui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to daqgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help daqgui

% Last Modified by GUIDE v2.5 17-Jun-2004 11:17:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @daqgui_OpeningFcn, ...
                   'gui_OutputFcn',  @daqgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before daqgui is made visible.
function daqgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to daqgui (see VARARGIN)

% Choose default command line output for daqgui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes daqgui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

handles.user = struct('daq',[]);
handles.user.daq = IOTdaqInit;

handles.user.HoldingPotential = 0;
IOTdaqOutputSingle(handles.user.daq,0,handles.user.HoldingPotential);
handles.user.HoldingPotentialEnable = 0;
% calibration factor -> 20mV/V going into (-B) input of Brownlee
handles.user.HoldingPotentialFactor = -20;

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = daqgui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background, change
%       'usewhitebg' to 0 to use default.  See ISPC and COMPUTER.
usewhitebg = 1;
if usewhitebg
    set(hObject,'BackgroundColor',[.9 .9 .9]);
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.text2,'String',sprintf('%g mV',get(hObject,'Value')));
handles.user.HoldingPotential = get(hObject,'Value');
if handles.user.HoldingPotentialEnable == 1
    IOTdaqOutputSingle(handles.user.daq,0,handles.user.HoldingPotential/handles.user.HoldingPotentialFactor);
end

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

if get(hObject,'Value') == 0,
    set(handles.text2,'ForegroundColor',[.30 .30 .30]);
    handles.user.HoldingPotentialEnable = 0;
    % set holding potential to zero
    IOTdaqOutputSingle(handles.user.daq,0,0);
else
    set(handles.text2,'ForegroundColor',[0 0 0]);
    handles.user.HoldingPotentialEnable = 1;
    % set holding potential to current value
    IOTdaqOutputSingle(handles.user.daq,0,handles.user.HoldingPotential/handles.user.HoldingPotentialFactor);    
end
disp(sprintf('Checkbox value: %g\n', get(hObject,'Value')));    



% Update handles structure
guidata(hObject, handles);
