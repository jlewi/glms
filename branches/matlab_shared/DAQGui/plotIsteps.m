% function plotIsteps(CurOutput)
function plotIsteps(CurOutput)

t = CurOutput.StreamVars.t;
y = CurOutput.StreamVars.Vm*1000; %mV
A = CurOutput.StreamVars.scaled; %nA

% figure;
% subplot(2,1,1)
% plot(t,y);
% subplot(2,1,2)
% plot(t,A);

figure;
n = 10;
for i=1:CurOutput.StreamVars.Repeat
    startindex = (i-1)*CurOutput.StreamVars.SegmentSize+1;
    stopindex = (i)*CurOutput.StreamVars.SegmentSize;

    A1 = A(startindex:stopindex);
    % filter the signal
    A2 = conv(A1,ones(1,n)*1/n);
    A3 = abs(diff(A2));
    A3 = A3(10:length(A3)-10);
    thresh = (max(A3)-min(A3))*0.8;
    A4 = A3;
    A4(find(A3<thresh)) = 0;
    
    subplot(3,1,1)    
    plotyy(t(1:length(A4)),A4,t(1:CurOutput.StreamVars.SegmentSize),A(startindex:stopindex));
%    hold on;
    subplot(3,1,2)
    plot(t(1:CurOutput.StreamVars.SegmentSize),y(startindex:stopindex));
    hold on;
    subplot(3,1,3)
    plot(t(1:CurOutput.StreamVars.SegmentSize),A(startindex:stopindex));
    hold on;
end
hold off;
