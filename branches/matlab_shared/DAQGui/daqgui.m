function varargout = daqgui(varargin)
% DAQGUI M-file for daqgui.fig
%
%      Version Control
%      $Id$
%      $Source$
%
%      Author:
%      Randy Weinstein - Summer, 2004
%      Lee Group
%      Laboratory for Neuroengineering
%      Georgia Institute of Technology/Emory University
%
%      DAQGUI, by itself, creates a new DAQGUI or raises the existing
%      singleton*.
%
%      H = DAQGUI returns the handle to a new DAQGUI or the handle to
%      the existing singleton*.
%
%      DAQGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DAQGUI.M with the given input arguments.
%
%      DAQGUI('Property','Value',...) creates a new DAQGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before daqgui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to daqgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help daqgui

% Last Modified by GUIDE v2.5 21-Jul-2004 17:23:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @daqgui_OpeningFcn, ...
                   'gui_OutputFcn',  @daqgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before daqgui is made visible.
function daqgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to daqgui (see VARARGIN)

% Choose default command line output for daqgui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes daqgui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

daqreset;

handles.user = struct('daq',[]);
handles.user.daq = IOTdaqInit;
handles.user.fg = HPFunGen;

% define enumerations
handles.user.enum.filestate.new = 1; % on startup
handles.user.enum.filestate.unsaved = 2; % before saved for the first time
handles.user.enum.filestate.saved = 3; % up to date
handles.user.enum.filestate.edited = 4; % previously saved and then edited
handles.user.filestatus = handles.user.enum.filestate.new;
handles.user.enum.runstate.idle = 1;
handles.user.enum.runstate.running = 2;
handles.user.enum.runstate.interrupted = 3;
%handles.user.enum.runstate.stopped = 4;
handles.user.runstatus = handles.user.enum.runstate.idle;


handles.user.HoldingPotential = 0;
IOTdaqOutputSingle(handles.user.daq,0,handles.user.HoldingPotential);
handles.user.HoldingPotentialEnable = 0;
% calibration factor -> 20mV/V going into (-B) input of Brownlee
handles.user.HoldingPotentialFactor = -20;

% prepare output
handles.user.output = '';

% load in external protocols
protocols;

% Create Protocol List
FullProtocolList = fieldnames(handles.user.protocol);
ProtocolList = {};
for i=1:length(FullProtocolList)
    if handles.user.protocol.(FullProtocolList{i}).Visible
        ProtocolList{length(ProtocolList)+1} = FullProtocolList{i};
    end
end
set(handles.listbox2,'String',ProtocolList);

% Update properties
guidata(hObject,handles);
listbox2_Callback(get(handles.listbox2),eventdata,handles);
handles = guidata(hObject);

% Define IO structure
handles.user.DAQin.Vm = 0;
handles.user.DAQin.Im = 1;
handles.user.DAQin.scaled = 2;
handles.user.DAQin.gain = 3;
handles.user.DAQin.freq = 4;
handles.user.DAQin.temp = 5;

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = daqgui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background, change
%       'usewhitebg' to 0 to use default.  See ISPC and COMPUTER.
usewhitebg = 1;
if usewhitebg
    set(hObject,'BackgroundColor',[.9 .9 .9]);
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.text2,'String',sprintf('%g mV',get(hObject,'Value')));
handles.user.HoldingPotential = get(hObject,'Value');
if handles.user.HoldingPotentialEnable == 1
    IOTdaqOutputSingle(handles.user.daq,0,handles.user.HoldingPotential/handles.user.HoldingPotentialFactor);
%    disp(handles.user.HoldingPotential);
end

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

if get(hObject,'Value') == 0,
    set(handles.text2,'ForegroundColor',[.30 .30 .30]);
    handles.user.HoldingPotentialEnable = 0;
    % set holding potential to zero
    IOTdaqOutputSingle(handles.user.daq,0,0);
else
    set(hObject,'Value',0);
%     set(handles.text2,'ForegroundColor',[0 0 0]);
%     handles.user.HoldingPotentialEnable = 1;
%     % set holding potential to current value
%     IOTdaqOutputSingle(handles.user.daq,0,handles.user.HoldingPotential/handles.user.HoldingPotentialFactor);    
end
% disp(sprintf('Checkbox value: %g\n', get(hObject,'Value')));    


% Update handles structure
guidata(hObject, handles);


% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over pushbutton2.
% function ButtonRecord1s(hObject, eventdata, handles) % 1s recording
% % hObject    handle to pushbutton1 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% fs = 100000; %100k
% dur = 1; %1s
% 
% data = IOTdaqGetInput(handles.user.daq,[0 1],fs,dur);
% 
% CurCount = handles.user.protocol.onesecond.RunCount + 1;
% handles.user.protocol.onesecond.RunCount = CurCount;
% 
% CurOutput = sprintf('%s%g',handles.user.protocol.onesecond.ID,CurCount);
% 
% eval(sprintf('handles.user.output.%s = struct(''Protocol'',''1s Vm/Im recording'',''Time'',datestr(now),''Vm'',[],''Im'',[],''t'',[]);',CurOutput));
% eval(sprintf('handles.user.output.%s.t = 1/fs:1/fs:dur;',CurOutput));
% eval(sprintf('handles.user.output.%s.Vm = data(1,:)/10;',CurOutput));
% eval(sprintf('handles.user.output.%s.Im = data(2,:);',CurOutput));
% 
% handles.user.protocol.LastRun = CurOutput;
% 
% 
% % Update handles structure
% guidata(hObject, handles);
% 
% % --- If Enable == 'on', executes on mouse press in 5 pixel border.
% % --- Otherwise, executes on mouse press in 5 pixel border or over pushbutton2.
% function ButtonRecord30s(hObject, eventdata, handles) % 30s recording
% % hObject    handle to pushbutton2 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% fs = 100000; %100k
% dur = 30; %30s
% 
% data = IOTdaqGetInput(handles.user.daq,[0 1],fs,dur);
% 
% % index = length(handles.user.output)+1;
% % handles.user.output(index) = struct('Protocol','30s Vm/Im recording','Time',datestr(now),'Vm',[],'Im',[],'t',[]);
% % handles.user.output.t(index) = 1/fs:1/fs:dur;
% % handles.user.output.Vm(index) = data(1,:)/10;
% % handles.user.output.Im(index) = data(2,:);
% 
% CurCount = handles.user.protocol.thirtyseconds.RunCount + 1;
% handles.user.protocol.thirtyseconds.RunCount = CurCount;
% 
% CurOutput = sprintf('%s%g',handles.user.protocol.thirtyseconds.ID,CurCount);
% 
% eval(sprintf('handles.user.output.%s = struct(''Protocol'',''30s Vm/Im recording'',''Time'',datestr(now),''Vm'',[],''Im'',[],''t'',[]);',CurOutput));
% eval(sprintf('handles.user.output.%s.t = 1/fs:1/fs:dur;',CurOutput));
% eval(sprintf('handles.user.output.%s.Vm = data(1,:)/10;',CurOutput));
% eval(sprintf('handles.user.output.%s.Im = data(2,:);',CurOutput));
% 
% handles.user.protocol.LastRun = CurOutput;
% 
% 
% % Update handles structure
% guidata(hObject, handles);
% 

% --- Executes on button press in pushbutton4.
function PlotButton(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Determine selected output from list
[CurOutput,CurProtocol] = GetSelectedOutput(handles);
CurProtocolPlot = CurProtocol.plot;

% Generate new figure
figure;
% for each subplot
for i=1:CurProtocolPlot.numsubplots
    subplot(CurProtocolPlot.numsubplots,1,i);
    g = CurProtocolPlot.graph(i);
    plot(CurOutput.StreamVars.(g.xvar)*g.xscale,CurOutput.StreamVars.(g.yvar)*g.yscale);
    if i == 1
        title(sprintf('%s (%s %s)',g.title,CurOutput.ID,CurOutput.Time));
    else
        title(g.title);
    end
    xlabel(g.xlabel);
    ylabel(g.ylabel);
end

% output = getfield(handles.user.output,handles.user.protocol.LastRun);
% 
% 
% figure;
% subplot(2,1,1);
% if output.t(length(output.t)) > 1,
%     plot(output.t,output.Vm*1000);
%     xlabel('Time (seconds)');
% else
%     plot(output.t*1000,output.Vm*1000);
%     xlabel('Time (milliseconds)');
% end
% title(sprintf('Membrane Potential (%s %s)',handles.user.protocol.LastRun,datestr(now)));
% ylabel('Voltage (mV)');
% subplot(2,1,2);
% if output.t(length(output.t)) > 1,
%     plot(output.t,output.Im);
%     xlabel('Time (seconds)');
% else
%     plot(output.t*1000,output.Im);
%     xlabel('Time (milliseconds)');
% end
% title('Membrane Current');
% ylabel('Current (nA)');

% % --- Executes on button press in pushbutton4.
% function SaveToDiskButton(hObject, eventdata, handles)
% % hObject    handle to pushbutton5 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% 
% [file,path] = uiputfile('*.mat','Save Acquisition As');
% capture = handles.user;
% save(fullfile(path,file),'capture');
% 
% % update filestatus
% handles.user.filestate = handles.user.enum.filestatus.saved;
% 
% % Update handles structure
% guidata(hObject, handles); % save output to gui

% --- Saves to Disk
function SaveToDisk(hObject,handles)

% use saved filename
filename = handles.user.filename;

% update filestatus
handles.user.filestatus = handles.user.enum.filestate.saved;

data = handles.user;
data = rmfield(data,'daq');
data = rmfield(data,'fg');
save(filename,'data');

% Update handles structure
guidata(hObject, handles); % save output to gui


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

% save notes to output structure
if length(get(handles.listbox1,'String')) > 0,
    savepos = get(handles.listbox1,'Value');
    ProtocolList = get(handles.listbox1,'String');
    saveprotocol = char(ProtocolList(savepos));
    handles.user.output.(saveprotocol).Notes = get(handles.edit2,'String');
end

% Update handles structure
guidata(hObject, handles); % save output to gui


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
UpdateSessionLog(get(hObject,'Value'),hObject,handles);

% --- Executes on button press in pushbutton6.
function CalcRButton(hObject, eventdata, handles)
% hObject    handle to CalcRButton(see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- return single values
function y = getCurValues(strlist,handles)

if not(iscell(strlist))
    strlist = {strlist};
end

values = IOTdaqGetSingleAll(handles.user.daq);

y = zeros(1,length(strlist));

for i = 1:length(strlist)
    y(i) = values(handles.user.DAQin.(char(strlist(i))) + 1);
end

% --- return indices of input names
function y = getInputNumber(strlist,handles)

if not(iscell(strlist))
    strlist = {strlist};
end

y = zeros(1,length(strlist));

for i = 1:length(strlist)
    y(i) = handles.user.DAQin.(char(strlist(i)));
end

    
% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2

% Lookup Protocol selected in list box
ProtocolList = get(handles.listbox2,'String');
CurProtocolStr = char(ProtocolList(get(handles.listbox2,'Value')));
CurProtocol = getfield(handles.user.protocol,CurProtocolStr);

if length(CurProtocol.Properties) % if there are properties
    prop_ID = fieldnames(CurProtocol.Properties);
    num_prop = length(prop_ID);
    prop_list = cell(1,num_prop);
    for i=1:num_prop
        prop_list{i} = CurProtocol.Properties.(prop_ID{i}).Name;
%        disp(properties(i));
    end
    set(handles.popupmenu1,'String',prop_list);
    set(handles.popupmenu1,'Value',1);
    popupmenu1_Callback(get(handles.popupmenu1),eventdata,handles);
else
    set(handles.popupmenu1,'Value',1);
    set(handles.popupmenu1,'String','No Properties');
    set(handles.edit3,'Visible','Off');
    set(handles.popupmenu2,'Visible','Off');

end



% --- Executes on button press in pushbutton7.
function ExecProtocolButton(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Update filestatus state
filestatus = handles.user.enum.filestate;
if handles.user.filestatus == filestatus.saved
    handles.user.filestatus = filestatus.edited;
elseif handles.user.filestatus == filestatus.new
    handles.user.filestatus = filestatus.unsaved;
end

% Set run status
handles.user.runstatus = handles.user.enum.runstate.running;

% Lookup Protocol selected in list box
ProtocolList = get(handles.listbox2,'String');
CurProtocolStr = char(ProtocolList(get(handles.listbox2,'Value')));
CurProtocol = getfield(handles.user.protocol,CurProtocolStr);

% Prepare Output
CurCount = CurProtocol.RunCount + 1;
CurProtocol.RunCount = CurCount;
handles.user.protocol.(CurProtocolStr) = CurProtocol;
guidata(hObject, handles); % save output to gui
OutputID = sprintf('%s_%g',CurProtocolStr,CurCount); % example: onesecond_4
CurOutput.ID = OutputID;
CurOutput.Protocol = CurProtocol.Name;
CurOutput.Time = datestr(now);
CurOutput.Notes = 'Notes: ';
CurOutput.Behaviors = '';

% Read properties -> copy into output structure
CurOutput.Properties = '';
if isstruct(CurProtocol.Properties)
    field_names = fieldnames(CurProtocol.Properties);
    for i=1:length(field_names)
        prop_ID = char(field_names(i))
        feval(CurProtocol.Properties.(prop_ID).Callback,hObject,handles,CurProtocolStr,prop_ID,CurProtocol.Properties.(prop_ID).Value);
        handles = guidata(hObject);
        CurProtocol = handles.user.protocol.(CurProtocolStr);
        CurOutput.Properties.(prop_ID) = '';
        CurOutput.Properties.(prop_ID).Name = CurProtocol.Properties.(prop_ID).Name;
        CurOutput.Properties.(prop_ID).Value = CurProtocol.Properties.(prop_ID).Value;        
    end
end
     

% eval(sprintf('handles.user.output.%s = struct(''Protocol'',''1s Vm/Im recording'',''Time'',datestr(now),''Vm'',[],''Im'',[],''t'',[]);',CurOutput));
% eval(sprintf('handles.user.output.%s.t = 1/fs:1/fs:dur;',CurOutput));
% eval(sprintf('handles.user.output.%s.Vm = data(1,:)/10;',CurOutput));
% eval(sprintf('handles.user.output.%s.Im = data(2,:);',CurOutput));


% % Capture Single Values
% SingleValues = getCurValues(CurProtocol.SaveSingles,handles);
% for i=1:length(CurProtocol.SaveSingles);
%     CurOutput.SingleVars.(char(CurProtocol.SaveSingles(i))) = SingleValues(i);
% end

% Capture all Single Values
AllInputs = fieldnames(handles.user.DAQin);
SingleValues = getCurValues(AllInputs,handles);
[gain,beta] = SetGain(SingleValues(handles.user.DAQin.gain + 1),handles);
CurOutput.SingleVars.gain = gain;
CurOutput.SingleVars.beta = beta;
[freq,bypass] =  SetFreq(SingleValues(handles.user.DAQin.freq + 1),handles);
CurOutput.SingleVars.freq = freq;
CurOutput.SingleVars.LPF = bypass;
temp = SetTemp(SingleValues(handles.user.DAQin.temp + 1),handles);
CurOutput.SingleVars.temp = temp;
UpdateSettings(CurOutput,handles); % Update display settings

% Create index of streaming variables
StreamIndex = '';
for i=1:length(CurProtocol.CaptureStreams)
    StreamIndex.(char(CurProtocol.CaptureStreams(i))) = i;
end

% Capture Streaming and Single Values
segment = 0; % initialize for completeness
if CurProtocol.Repeat > 0
    % Capture settings only once during repeats, but prior to each acquisition when in
    % continunous mode
%     % Capture all Single Values
%     AllInputs = fieldnames(handles.user.DAQin);
%     SingleValues = getCurValues(AllInputs,handles);
%     [gain,beta] = SetGain(SingleValues(handles.user.DAQin.gain + 1),handles);
%     CurOutput.SingleVars.gain = gain;
%     CurOutput.SingleVars.beta = beta;
%     [freq,bypass] =  SetFreq(SingleValues(handles.user.DAQin.freq + 1),handles);
%     CurOutput.SingleVars.freq = freq;
%     CurOutput.SingleVars.LPF = bypass;
%     temp = SetTemp(SingleValues(handles.user.DAQin.temp + 1),handles);
%     CurOutput.SingleVars.temp = temp;
%     UpdateSettings(CurOutput,handles); % Update display settings

    % allocate space
    segment = CurProtocol.fs*CurProtocol.duration;
    data = zeros(length(CurProtocol.CaptureStreams),segment*CurProtocol.Repeat);
    startpos = 1;
    for count=1:CurProtocol.Repeat
        if handles.user.runstatus == handles.user.enum.runstate.running
            CurProtocol.RunIndex = count;
            if CurProtocol.FunGen.Enable;
                ConfigureFunGen(CurProtocol,CurOutput,hObject,handles); % configure HP Fun Gen and save info to output
                handles = guidata(hObject); % update user info following fungen configuration
                %            pause(1.4); % wait 1.4 seconds for function generator to be updated                
            end
            data(:,startpos:startpos+segment-1) = IOTdaqGetInput(handles.user.daq,getInputNumber(CurProtocol.CaptureStreams,handles),CurProtocol.fs,CurProtocol.duration);
            %        data2 = IOTdaqGetInput(handles.user.daq,getInputNumber(CurProtocol.CaptureStreams,handles),CurProtocol.fs,CurProtocol.duration);
            if CurProtocol.ContProcessFunction > 0,
                % save current state
                handles.user.protocol.(CurProtocolStr) = CurProtocol; % update protocol info (RunCount)
                handles.user.output.(OutputID) = CurOutput; % update output database
                guidata(hObject, handles); % save output to gui
                handles = feval(CurProtocol.ContProcessFunction,OutputID,data(:,startpos:startpos+segment-1),hObject,handles);
                guidata(hObject, handles); % save output to gui
                CurProtocol = handles.user.protocol.(CurProtocolStr);
                CurOutput = handles.user.output.(OutputID);    
            end
            startpos = startpos+segment;
            if CurProtocol.FunGen.Enable
                DisableFunGen(handles); %set output to rear to shut off
            end
        end % if runstatus == running
    end

else
    if CurProtocol.FunGen.Enable
        ConfigureFunGen(CurProtocol,CurOutput,hObject,handles); % configure HP Fun Gen and save info to output
        handles = guidata(hObject); % update user info following fungen configuration
        %         pause(1.4); % wait 1.4 seconds for function generator to be updated    
    end
    while(handles.user.runstatus == handles.user.enum.runstate.running)
        % Capture all Single Values
        AllInputs = fieldnames(handles.user.DAQin);
        SingleValues = getCurValues(AllInputs,handles);
        [gain,beta] = SetGain(SingleValues(handles.user.DAQin.gain + 1),handles);
        CurOutput.SingleVars.gain = gain;
        CurOutput.SingleVars.beta = beta;
        [freq,bypass] =  SetFreq(SingleValues(handles.user.DAQin.freq + 1),handles);
        CurOutput.SingleVars.freq = freq;
        CurOutput.SingleVars.LPF = bypass;
        temp = SetTemp(SingleValues(handles.user.DAQin.temp + 1),handles);
        CurOutput.SingleVars.temp = temp;
        UpdateSettings(CurOutput,handles); % Update display settings

        % Capture Streaming data
        data = IOTdaqGetInput(handles.user.daq,getInputNumber(CurProtocol.CaptureStreams,handles),CurProtocol.fs,CurProtocol.duration);
        % grab updated guidata
        handles = guidata(hObject);
        % run continuous processing
        if CurProtocol.ContProcessFunction > 0,
            % save current state
            handles.user.protocol.(CurProtocolStr) = CurProtocol; % update protocol info (RunCount)
            handles.user.output.(OutputID) = CurOutput; % update output database
            guidata(hObject, handles); % save output to gui
            handles = feval(CurProtocol.ContProcessFunction,OutputID,data,hObject,handles);
            guidata(hObject, handles); % save output to gui
            CurOutput = handles.user.output.(OutputID);    
        end
    end
    if CurProtocol.FunGen.Enable
        DisableFunGen(handles); %set output to rear to shut off
    end
end

% Save Streaming Variables
if length(CurProtocol.SaveStreams) > 0
    CurOutput.StreamVars.Repeat = CurProtocol.Repeat;
    CurOutput.StreamVars.SegmentSize = segment;
    CurOutput.StreamVars.SamplingFrequency = CurProtocol.fs;
    CurOutput.StreamVars.Duration = CurProtocol.duration;
    if CurProtocol.SaveTime,
        CurOutput.StreamVars.t = 1/CurProtocol.fs:1/CurProtocol.fs:CurProtocol.duration*CurProtocol.Repeat;
    end    
    for i=1:length(CurProtocol.SaveStreams)
        CurOutput.StreamVars.(char(CurProtocol.SaveStreams(StreamIndex.(char(CurProtocol.SaveStreams(i)))))) = data(i,:);
    end
end

% set run status to idle before handles.user is saved
handles.user.runstatus = handles.user.enum.runstate.idle;

% run post processing
if CurProtocol.PostProcessFunction > 0,
    % save current state
    handles.user.protocol.(CurProtocolStr) = CurProtocol; % update protocol info (RunCount)
    handles.user.output.(OutputID) = CurOutput; % update output database
    guidata(hObject, handles); % save output to gui
    handles = feval(CurProtocol.PostProcessFunction,OutputID,hObject,handles);
    guidata(hObject, handles); % save output to gui
else
    handles.user.protocol.(CurProtocolStr) = CurProtocol; % update protocol info (RunCount)
    handles.user.output.(OutputID) = CurOutput; % update output database
    guidata(hObject, handles); % save output to gui
end


% Update Session Listbox
UpdateSessionLog(0,hObject,handles); % update output listbox and move selection to this output





% --- Executes on button press in pushbutton8.
function StopButton(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%disp('stop');

%stop(handles.user.daq.ai);
if handles.user.runstatus == handles.user.enum.runstate.running
    handles.user.runstatus = handles.user.enum.runstate.interrupted;
end

% Update handles structure
guidata(hObject, handles); % save output to gui


% --- ConfigureFunGen(CurProtocol,CurOutput,hObject,handles)
function ConfigureFunGen(CurProtocol,CurOutput,hObject,handles)

%handles.user.fg
set(handles.user.fg,'WaveType',CurProtocol.FunGen.WaveType);
set(handles.user.fg,'Frequency',CurProtocol.FunGen.Frequency);
set(handles.user.fg,'Amplitude',CurProtocol.FunGen.Amplitude/CurProtocol.FunGen.ExtCmdSensitivity);
set(handles.user.fg,'Offset',CurProtocol.FunGen.Offset/CurProtocol.FunGen.ExtCmdSensitivity);
set(handles.user.fg,'Phase',CurProtocol.FunGen.Phase);
if CurProtocol.FunGen.Enable
    set(handles.user.fg,'Output','Front');
else
    set(handles.user.fg,'Output','Rear');
end

pause(1.4); % Pause to allow time for function generator to reset

% --- Disable FunGen
function DisableFunGen(handles);
set(handles.user.fg,'Output','Rear');



% --- UpdateSessionLog
function UpdateSessionLog(position,hObject,handles);
% position   set current position or 0 for last
% handles    structure with handles and user data (see GUIDATA)

% if length(get(handles.listbox1,'String')) > 0,
%     % save the values of notes
%     savepos = get(handles.listbox1,'Value');
%     ProtocolList = get(handles.listbox1,'String');
%     saveprotocol = char(ProtocolList(savepos));
%     handles.user.output.(saveprotocol).Notes = get(handles.edit2,'String');
% end

if length(fieldnames(handles.user.output)) == 0 % if there are no outputs
    set(handles.listbox1,'String','');
    set(handles.listbox1,'Value',0);
    set(handles.edit2,'String','Cleared outputs');
    set(handles.text4,'String','Gain:');
    set(handles.text5,'String','-3 dB:');
    set(handles.text11,'String','Temp:');
    set(handles.text9,'String','Resistance:');
    set(handles.text12,'String','Capacitance:');
    set(handles.text6,'String','');
else
    outputs = fieldnames(handles.user.output);
    set(handles.listbox1,'String',outputs);
    if position == 0,
        set(handles.listbox1,'Value',length(outputs));
        position = length(outputs);
    else
        set(handles.listbox1,'Value',position);
    end
    selectedOutput = handles.user.output.(char(outputs(position)));
    
    set(handles.text6,'String',sprintf('<%s> %s',selectedOutput.Protocol,selectedOutput.Time));
    set(handles.edit2,'String',selectedOutput.Notes);
    
    UpdateSettings(selectedOutput,handles);
    UpdateBehaviors(selectedOutput,handles);
end


% Update handles structure
guidata(hObject, handles); % save output to gui


% --- Continuous Processing Function for calculating R
function handles = CalcR(ID,data,hObject,handles);
Vm = data(1,:)/10;
Im = data(2,:);
% figure(1);
% subplot(2,1,1)
% plot(Vm)
% subplot(2,1,2)
% plot(Im)

gain = handles.user.output.(ID).SingleVars.gain;
beta = handles.user.output.(ID).SingleVars.beta;

Vamp = amplitude(Vm);
Iamp = amplitude(Im);
R = (Vamp*10e-3)/((Iamp*10e-12)/(gain*beta));

handles.user.output.(ID).Behaviors.impedance = R;

UpdateSettings(handles.user.output.(ID),handles);
UpdateBehaviors(handles.user.output.(ID),handles);
% 
% if R > 1e9
%     Rstr = sprintf('%0.3g GOhm',R/1e9);
% elseif R > 1e6
%     Rstr = sprintf('%0.3g MOhm',R/1e6);
% elseif R > 1e3
%     Rstr = sprintf('%0.3g kOhm',R/1e3);
% end
% 
% set(handles.text9,'String',sprintf('Imp: %s',Rstr));


%---- Continuous Processing Function for calculating C
function handles = CalcC(ID,data,hObject,handles);

dur = 1; %duration
fs = 100000; %frequency

gain = handles.user.output.(ID).SingleVars.gain;
beta = handles.user.output.(ID).SingleVars.beta;

Vm = data(1,:)/10;
Im = data(2,:);

% figure;
% subplot(2,1,1)
% plot(Vm)
% subplot(2,1,2)
% plot(Im)

Vamp = amplitude(Vm);
Iamp = amplitude(Im);
R = (Vamp*10e-3)/((Iamp*10e-12)/(gain*beta)) %calculate R

Vm = Vm; 
Im = Im/(gain*beta);
t = [1/fs:1/fs:dur];

desired_Vm = min(Vm)+0.632*amplitude(Vm); %determine 63.2 percent point
der_Im = diff(lpbutterfilt(Im, 1e5, 5e3)); %derivative of current pulse
max_der_Im = max(der_Im);
t_min = 1;
while max_der_Im ~= der_Im(t_min)
    t_min = t_min+1;
end
t_amp = t_min;
while Vm(t_amp) <= desired_Vm
    t_amp = t_amp+1;
end
tau = t(t_amp)-t(t_min); %time constant
C = tau/R; 

handles.user.output.(ID).Behaviors.capacitance = C;
handles.user.output.(ID).Behaviors.tau = tau;

UpdateBehaviors(handles.user.output.(ID),handles);

% --- Step Command - handles update of amplitude during step commands
function handles = StepCommand(ID,data,hObject,handles);

[s,f,t] =regexp(ID,'(.*)_\d+');
CurProtocolStr = ID(t{1}(1):t{1}(2));
CurProtocol = handles.user.protocol.(CurProtocolStr);
RunIndex = CurProtocol.RunIndex;

Amplitude = CurProtocol.Properties.StartAmplitude.Value + CurProtocol.Properties.Increment.Value*RunIndex;
CurProtocol.FunGen.Amplitude = Amplitude;

CurProtocol.FunGen.Offset = Amplitude/2;

% save user data to figure
handles.user.protocol.(CurProtocolStr) = CurProtocol;
guidata(hObject,handles);

% --- Post Adjust Gain
function handles = PostAdjustGain(ID,hObject,handles);

gain = handles.user.output.(ID).SingleVars.gain;
beta = handles.user.output.(ID).SingleVars.beta;

CurOutput = handles.user.output.(ID);
if isfield(CurOutput.StreamVars,'scaled')    
    Output = CurOutput.StreamVars.scaled;
    AdjustedOutput = Output/(gain*beta);
    handles.user.output.(ID).StreamVars.scaled = AdjustedOutput;
else
    Output = CurOutput.StreamVars.Im;
    AdjustedOutput = Output/(beta);
    handles.user.output.(ID).StreamVars.Im = AdjustedOutput;
end    
handles.user.output.(ID).StreamVars.Vm = handles.user.output.(ID).StreamVars.Vm/10;


% --- SetGain
function [gain,beta] = SetGain(value,handles);
gain_index = [0.5 1 2 5 10 20 50 100];
if value > 0
    beta = 1;    
else
    beta = 100;
    value = -value;
end
%handles.user.output.(ID).SingleVars.gain
gain = round(value/(0.4));
if gain < 1 | gain > length(gain_index)
    gain = 0;
else
    gain = gain_index(gain);
end

% --- SetFreq
function [freq,bypassStr] = SetFreq(value,handles);
freq_index = [20 50 100 200 500 1000 2000 5000 10000 20000 50000 100000];
if value < 0
    bypassStr = 'Bypass';
    value = -value;
else
    bypassStr = 'Active';
end
freq = round(value/(0.4));
if freq < 1 | freq > length(freq_index)
    freq = 0;
else
    freq = freq_index(freq);
end



% --- SetTemp
function [temp] = SetTemp(value,handles);
temp = value*10;

% --- Executes on button press in pushbutton9.
function SaveWorkspaceButton(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[CurOutput,CurProtocol] = GetSelectedOutput(handles);
assignin('base',CurOutput.ID,CurOutput);


% --- GetSelectedOutput
function [Output,Protocol] = GetSelectedOutput(handles);

% Get selected protocol
if length(get(handles.listbox1,'String')) > 0,
    savepos = get(handles.listbox1,'Value');
    ProtocolList = get(handles.listbox1,'String');
    saveprotocol = char(ProtocolList(savepos));
    Output = handles.user.output.(saveprotocol);
    [s,f,t] =regexp(saveprotocol,'(.*)_\d+');
    Protocol = handles.user.protocol.(saveprotocol(t{1}(1):t{1}(2)));
else
    Output = ''; Protocol = '';
end


% --- Update Properties and Settings
function UpdateSettings(Output,handles);

gain = Output.SingleVars.gain;
beta = Output.SingleVars.beta;
freq = Output.SingleVars.freq;
bypassStr = Output.SingleVars.LPF;
temp = Output.SingleVars.temp;

set(handles.text4,'String',sprintf('Gain: %g (beta=%g)',gain,beta));


if freq >= 1000
    freqStr = sprintf('%gk',freq/1000);
else
    freqStr = sprintf('%g',freq);
end


set(handles.text5,'String',sprintf('-3 dB: %s (%s)',freqStr,bypassStr));


tempStr = sprintf('%0.3g deg C',temp);
set(handles.text11,'String',sprintf('Temp: %s',tempStr));


% --- Update Behaviors
function UpdateBehaviors(Output,handles);

if isfield(Output.Behaviors,'impedance'),
    R = Output.Behaviors.impedance;
    if R > 1e9
        Rstr = sprintf('%0.3g GOhm',R/1e9);
    elseif R > 1e6
        Rstr = sprintf('%0.3g MOhm',R/1e6);
    elseif R > 1e3
        Rstr = sprintf('%0.3g kOhm',R/1e3);
    end    
    set(handles.text9,'String',sprintf('Impedance: %s',Rstr));
end

if isfield(Output.Behaviors,'capacitance'),
    C = Output.Behaviors.capacitance;
    if C > 1
        Cstr = sprintf('%0.3g F',C);
    elseif C > 1e-3
        Cstr = sprintf('%0.3g mF',C/1e-3);
    elseif C > 1e-6
        Cstr = sprintf('%0.3g uF',C/1e-6);
    elseif C > 1e-9
        Cstr = sprintf('%0.3g nF', C/1e-9);
    elseif C > 1e-12
        Cstr = sprintf('%0.3g pF', C/1e-12);
    elseif C > 1e-15
        Cstr = sprintf('%0.3g fF', C/1e-15);
    else
        Cstr = sprintf('%0.3g' , C);
    end  
    set(handles.text12,'String',sprintf('Cap: %s', Cstr));
end


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function NewMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to NewMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%disp('new');
% check to see if the data is edited
filestate = handles.user.enum.filestate;

if handles.user.filestatus == filestate.unsaved || handles.user.filestatus == filestate.edited
    % open save dialog box
    answer = savequestion;
    if strcmp(answer,'Yes')
        SaveAsMenuItem_Callback(hObject, eventdata, handles)
    end
end

handles.user.filestatus = filestate.new;

% update protocols
protocols;
handles.user.output = '';
%UpdateSettings;
%UpdateBehaviors;
UpdateSessionLog(0,hObject,handles);

% Update handles structure
guidata(hObject, handles); % save output to gui


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%disp('open');

% check to see if the data is edited
filestate = handles.user.enum.filestate;

if handles.user.filestatus == filestate.unsaved || handles.user.filestatus == filestate.edited
    % open save dialog box
    answer = savequestion;
    if strcmp(answer,'Yes')
        SaveAsMenuItem_Callback(hObject, eventdata, handles)
    end
end

[file,path] = uigetfile('*.mat','Choose Acquisition file');

if file ~= 0,
    filename = fullfile(path,file);

    % save DAQ,FG
    saveDAQ = handles.user.daq;
    saveFG = handles.user.fg;
    
    load(filename); % saves to 'data'
    
    handles.user = data;
    handles.user.daq = saveDAQ;
    handles.user.fg = saveFG;
    handles.user.filename = filename;

    handles.user.filestatus = filestate.saved; % treat as a saved state so that 'File-Save' works
    
    % Update handles structure
    guidata(hObject, handles); % save output to gui
end
UpdateSessionLog(1,hObject,handles);



% --------------------------------------------------------------------
function SaveMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%disp('save');
filestate = handles.user.enum.filestate;
if handles.user.filestatus == filestate.new || handles.user.filestatus == filestate.unsaved
    SaveAsMenuItem_Callback(hObject,eventdata,handles);
else
    SaveToDisk(hObject,handles);
end

% --------------------------------------------------------------------
function SaveAsMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to SaveAsMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%disp('save as');

[file,path] = uiputfile('*.mat','Save Acquisition As');

if file ~= 0,
    filename = fullfile(path,file);
    handles.user.filename = filename;

    % Update handles structure
    guidata(hObject, handles); % save output to gui

    SaveToDisk(hObject,handles);
end

% --------------------------------------------------------------------
function ExitMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to ExitMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%disp('exit');

filestate = handles.user.enum.filestate;
if handles.user.filestatus == filestate.edited || handles.user.filestatus == filestate.unsaved
    % open save dialog box
    answer = savequestion;
    if strcmp(answer,'Yes')
        SaveAsMenuItem_Callback(hObject, eventdata, handles)
    end
end
    
delete(handles.user.daq.ai);
delete(handles.user.daq.ao);
delete(handles.user.daq.dio);
delete(handles.user.fg);

delete(handles.figure1);


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from
%        popupmenu1

% Lookup Protocol selected in list box
ProtocolList = get(handles.listbox2,'String');
CurProtocolStr = char(ProtocolList(get(handles.listbox2,'Value')));
CurProtocol = getfield(handles.user.protocol,CurProtocolStr);

prop_names = get(handles.popupmenu1,'String');
num_prop_names = length(prop_names);
% if get(handles.popupmenu1,'Value') <= num_prop_names
    prop_name = prop_names{get(handles.popupmenu1,'Value')};
% else
%     prop_name = prop_names{1};
% end
field_names = fieldnames(CurProtocol.Properties);
for i=1:length(field_names)
    if strcmp(CurProtocol.Properties.(field_names{i}).Name,prop_name)
        prop = CurProtocol.Properties.(field_names{i});
        if strcmp(prop.Type,'Number')
            set(handles.edit3,'Visible','On');
            set(handles.popupmenu2,'Visible','Off');
            set(handles.edit3,'String',num2str(prop.Value));
        elseif strcmp(prop.Type,'Boolean')
            set(handles.edit3,'Visible','Off');
            set(handles.popupmenu2,'Visible','On');
            set(handles.popupmenu2,'String',{'true' 'false'});
            if prop.Value == true
                set(handles.popupmenu2,'Value',1);
            else
                set(handles.popupmenu2,'Value',2);
            end
        elseif strcmp(prop.Type,'List')
            set(handles.edit3,'Visible','Off');
            set(handles.popupmenu2,'Visible','On');
            set(handles.popupmenu2,'String',prop.Range);
            for j=1:length(prop.Range)
                if strcmp(char(prop.Range{j}),prop.Value)
                    set(handles.popupmenu2,'Value',j);
                end
            end
        end            
    end
end


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double

% determine current protocol
ProtocolList = get(handles.listbox2,'String');
CurProtocolStr = char(ProtocolList(get(handles.listbox2,'Value')));
CurProtocol = getfield(handles.user.protocol,CurProtocolStr);

% determine current property selected
prop_names = get(handles.popupmenu1,'String');
prop_name = prop_names{get(handles.popupmenu1,'Value')};
field_names = fieldnames(CurProtocol.Properties);
for i=1:length(field_names)
    if strcmp(CurProtocol.Properties.(field_names{i}).Name,prop_name)
        prop_ID = char(field_names(i));
        prop = CurProtocol.Properties.(prop_ID);
    end
end

value = str2num(get(hObject,'String'));

% if number is within range
if isfield(prop,'Range')
    if value < prop.Range(1) || value > prop.Range(2)
        error(sprintf('Property out of range [%g,%g]',prop.Range(1),prop.Range(2)));
    else
        prop.Value = value;
    end
end


% Update handles structure
handles.user.protocol.(CurProtocolStr).Properties.(prop_ID) = prop;
guidata(hObject, handles); % save output to gui



% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% determine current protocol
ProtocolList = get(handles.listbox2,'String');
CurProtocolStr = char(ProtocolList(get(handles.listbox2,'Value')));
CurProtocol = getfield(handles.user.protocol,CurProtocolStr);

% determine current property selected
prop_names = get(handles.popupmenu1,'String');
prop_name = prop_names{get(handles.popupmenu1,'Value')};
field_names = fieldnames(CurProtocol.Properties);
for i=1:length(field_names)
    if strcmp(CurProtocol.Properties.(field_names{i}).Name,prop_name)
        prop_ID = char(field_names(i));
        prop = CurProtocol.Properties.(prop_ID);
    end
end

if strcmp(CurProtocol.Properties.(prop_name).Type,'List')
    Range = get(hObject,'String');
    prop.Value = char(Range(get(hObject,'Value')));
elseif strcmp(CurProtocol.Properties.(prop_name).Type,'Boolean')
    Range = [true false];
    prop.Value = Range(get(hObject,'Value'));
end


% Update handles structure
handles.user.protocol.(CurProtocolStr).Properties.(prop_ID) = prop;
guidata(hObject, handles); % save output to gui

% --- UpdateFunGenProp
function UpdateFunGenProp(hObject,handles,protocol_ID,prop_ID,value)

%if handles.user.protocol.(protocol_ID).Properties.T
handles.user.protocol.(protocol_ID).FunGen.(prop_ID) = value;
%disp(sprintf('Prop_ID: %s; Value: %g',prop_ID,value));
%disp(sprintf('Prop_ID: %s; Value: %s',prop_ID,value));

guidata(hObject,handles);

% --- UpdateGeneralProp
function UpdateGeneralProp(hObject,handles,protocol_ID,prop_ID,value)

handles.user.protocol.(protocol_ID).(prop_ID) = value;

guidata(hObject,handles);

% --- UpdateFilterProperty
function UpdateFilterProp(hObject,handles,protocol_ID,prop_ID,value)

handles.user.protocol.(protocol_ID).LPFilter.(prop_ID) = value;

guidata(hObject,handles);

% --- UpdateStepProperty
function UpdateStepProp(hObject,handles,protocol_ID,prop_ID,value)

switch(prop_ID)
    case 'StartAmplitude'
        handles.user.protocol.(protocol_ID).FunGen.Amplitude = value;
        handles.user.protocol.(protocol_ID).FunGen.Offset = value/2;
    case 'Increment'
        handles.user.protocol.(protocol_ID).StepInc = value;
end


guidata(hObject,handles);



% --- Executes on button press in pushbutton10.
function DeleteProtocol_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Determine selected output from list
[CurOutput,CurProtocol] = GetSelectedOutput(handles);

if isstruct(CurOutput)
    handles.user.output = rmfield(handles.user.output,CurOutput.ID);
end

% Update handles structure
guidata(hObject, handles);

% Update Session Listbox
UpdateSessionLog(1,hObject,handles); % update output listbox and move selection to this output


