function times = findTransitions(t,x)

n = 10;
pad = 10;
x1 = conv(x,ones(1,n)*1/n);
x2 = abs(diff(x1));
x3 = x2(pad:length(x2)-pad);
plot(x3);
thresh = (max(x3)-min(x3))*0.8;
indices = find(x3 > thresh)-n/2-pad;
mask = zeros(1,length(t));
mask(indices) = 1;
figure;

subplot(2,1,1);
plot(t,x);
subplot(2,1,2);
stem(t,mask);

