%function y = IOTdaqGetSingleAll(daq)
function y = IOTdaqGetSingleAll(daq);

last_channel = 8;

% save the previous ai
ai_prev = daq.ai;

% reinitialize the inputs
daq.ai = eval(char(daq.hw.ObjectConstructorName(1)));
chnls = addchannel(daq.ai,0:last_channel-1);

% reset sample information
daq.ai.SampleRate = 1000;
daq.ai.SamplesPerTrigger = 10;

% start DAQ
start(daq.ai);

% wait for samples to be finished
% daq.ai.SamplesAvailable
% while daq.ai.SamplesAvailable < 10,
%     daq.ai.SamplesAvailable
% end

y = mean(getdata(daq.ai));

delete(daq.ai);
daq.ai = ai_prev;