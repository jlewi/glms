%function daq = IOTdaqInit
%   return value:
%       daq - a daq_hardware object which stores the daq structure
function daq = IOTdaqInit;
daq_hardware = daqhwinfo('iotdaq');

daq.hw = daq_hardware;
daq.ai = eval(char(daq_hardware.ObjectConstructorName(1)));
daq.ao = eval(char(daq_hardware.ObjectConstructorName(2)));
daq.dio = eval(char(daq_hardware.ObjectConstructorName(3)));

addchannel(daq.ao,[0 1]);
