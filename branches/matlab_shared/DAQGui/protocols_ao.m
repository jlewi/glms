% protocol listing

%handles.user.protocol = struct('LastRun','');
DefaultProtocol = struct(...
    'Name','Protocol',... % Descriptive name
    'Visible',true,... % Is visible
    'RunCount',0,... % Run count per session
    'CaptureStreams',{{'Vm' 'Im'}},... % Full streams to capture
    'SaveStreams',{{'Vm' 'Im'}},... % Full streams to save
    'SaveSingles',{{'temp'}},... % Single variables to save
    'SaveTime',true,... % Record time (as t)
    'ContProcessFunction','',... % Call function continuously after each iteration of streaming
    'PostProcessFunction','',... % Call function following streaming
    'Repeat',1,... % Number of times to repeat (if 0, repeats endlessly until a stop)
    'fs',100000,... % sampling rate in hertz
    'duration',1,... % duration of recording in seconds
    'Properties',''); % Property structure containing those properties that are adjustable
% Properties is a structure containing the following fields:
%   Name: Property name
%   Value: Default (and current) value
%   Type: can be 'Number','Boolean','List'
%   if Type = 'Number'
%       Range = [low high]
%   if Type = 'List'
%       Range = {{'cellstr1' 'cellstr2' 'cellstr3'}}
%   Callback: Function called with the following prototype
%   <Callback_function(hObject,handles,protocol_ID,prop_ID,value);
DefaultProtocol.AnalogOutput = struct(...
    'Enable',false,... % enable Analog output (front panel output)
    'OutputMode','Voltage',... % output in 'Voltage' or 'Current'
    'Frequency',1,... % Frequency (Hz)
    'Amplitude',1,... % Amplitude (mV/nA)
    'Offset',1,... % Offset (mV/nA)
    'Phase',0,... % Phase in degrees
    'WaveType','DC',.... % DC output, other options: 'Sine','Square','Triangle','Positive Ramp','Negative Ramp'
    'ExtCmdSensitivity',40); % units mV/V or nA/V
%DefaultProtocol.LPFilter = struct(...
%    'FilterVar','Vm',... % either Vm or Im
%    'CornerFreq','Wide Band'); % either 20,50,100,200,500,1k,2k,5k,10k,20k,50k,WB
% DefaultProtocol.Properties.CornerFreq = struct('Name','Butterworth LPF (Vm)','Value','Wide Band','Type','List',...
%     'Range',{{'20 Hz','50 Hz','100 Hz','200 Hz','500 Hz','1 kHz','2 kHz','5 kHz','10 kHz','20 kHz','50 kHz','Wide Band'}},...
%     'Callback','UpdateFilterProp');

handles.user.protocol.onesecond = DefaultProtocol;
handles.user.protocol.onesecond.Name = '1s Recording';
%handles.user.protocol.onesecond.Repeat = 3;
handles.user.protocol.onesecond.PostProcessFunction = 'PostAdjustGain';
handles.user.protocol.onesecond.plot.numsubplots = 2;
handles.user.protocol.onesecond.plot.graph = struct('title','','xvar','yvar','xlabel','','ylabel','','xscale',1,'yscale',1);
handles.user.protocol.onesecond.plot.graph(1).title = 'Membrane Potential';
handles.user.protocol.onesecond.plot.graph(1).xvar = 't';
handles.user.protocol.onesecond.plot.graph(1).yvar = 'Vm';
handles.user.protocol.onesecond.plot.graph(1).xlabel = 'Time (milliseconds)';
handles.user.protocol.onesecond.plot.graph(1).ylabel = 'Voltage (mV)';
handles.user.protocol.onesecond.plot.graph(1).xscale = 1000;
handles.user.protocol.onesecond.plot.graph(1).yscale = 1000;
handles.user.protocol.onesecond.plot.graph(2).title = 'Membrane Current';
handles.user.protocol.onesecond.plot.graph(2).xvar = 't';
handles.user.protocol.onesecond.plot.graph(2).yvar = 'Im';
handles.user.protocol.onesecond.plot.graph(2).xlabel = 'Time (milliseconds)';
handles.user.protocol.onesecond.plot.graph(2).ylabel = 'Current (nA)';
handles.user.protocol.onesecond.plot.graph(2).xscale = 1000;
handles.user.protocol.onesecond.plot.graph(2).yscale = 1;
handles.user.protocol.onesecond.Visible = false;

handles.user.protocol.TestProtocol = DefaultProtocol;
handles.user.protocol.TestProtocol.Name = 'Scaled Current Output';
handles.user.protocol.TestProtocol.CaptureStreams = {'Vm' 'scaled'};
handles.user.protocol.TestProtocol.SaveStreams = {'Vm' 'scaled'};
handles.user.protocol.TestProtocol.SaveSingles = {'temp' 'gain' 'freq'};
handles.user.protocol.TestProtocol.PostProcessFunction = 'PostAdjustGain';
handles.user.protocol.TestProtocol.plot = handles.user.protocol.onesecond.plot;
handles.user.protocol.TestProtocol.plot.graph(2).yvar = 'scaled';


handles.user.protocol.VClampSquare = handles.user.protocol.TestProtocol;
handles.user.protocol.VClampSquare.Name = 'V-Clamp Square Wave';
handles.user.protocol.VClampSquare.FunGen.Enable = true;
handles.user.protocol.VClampSquare.FunGen.OutputMode = 'Voltage';
handles.user.protocol.VClampSquare.FunGen.Frequency = 2; %Hz
handles.user.protocol.VClampSquare.FunGen.Amplitude = 20; %mV
handles.user.protocol.VClampSquare.FunGen.Offset = 10;
handles.user.protocol.VClampSquare.FunGen.Phase = 0;
handles.user.protocol.VClampSquare.FunGen.WaveType = 'Square';
handles.user.protocol.VClampSquare.Properties.Frequency = struct('Name','Frequency (Hz)','Value',2,'Type','Number','Range',[0.001 100000],'Callback','UpdateFunGenProp');
handles.user.protocol.VClampSquare.Properties.Amplitude = struct('Name','Amplitude (mV)','Value',20,'Type','Number','Range',[1 10000],'Callback','UpdateFunGenProp');
handles.user.protocol.VClampSquare.Properties.Offset = struct('Name','Offset (mV)','Value',10,'Type','Number','Range',[-10000 10000],'Callback','UpdateFunGenProp');
%handles.user.protocol.VClampSquare.Properties.WaveType = struct('Name','Waveform Type','Value','Square','Type','List','Range',{{'Sine', 'Square', 'Triangle'}},'Callback','UpdateFunGenProp');
%handles.user.protocol.VClampSquare.Properties.Enable = struct('Name','Enable','Value',true,'Type','Boolean','Callback','UpdateFunGenProp');


handles.user.protocol.VClampRamp = handles.user.protocol.VClampSquare;
handles.user.protocol.VClampRamp.Name = 'V-Clamp Ramp';
handles.user.protocol.VClampRamp.duration = 12; % seconds
handles.user.protocol.VClampRamp.FunGen.Frequency = 0.1; %Hz
handles.user.protocol.VClampRamp.FunGen.Amplitude = 100; %mV
handles.user.protocol.VClampRamp.FunGen.WaveType = 'Triangle';
handles.user.protocol.VClampRamp.Properties.Frequency = struct('Name','Frequency (Hz)','Value',0.1,'Type','Number','Range',[0.001 100000],'Callback','UpdateFunGenProp');
handles.user.protocol.VClampRamp.Properties.Amplitude = struct('Name','Amplitude (mV)','Value',100,'Type','Number','Range',[1 10000],'Callback','UpdateFunGenProp');
handles.user.protocol.VClampRamp.Properties.Offset = struct('Name','Offset (mV)','Value',10,'Type','Number','Range',[-10000 10000],'Callback','UpdateFunGenProp');

handles.user.protocol.IClampRamp = handles.user.protocol.VClampRamp;
handles.user.protocol.IClampRamp.Name = 'I-Clamp Ramp';
handles.user.protocol.IClampRamp.CaptureStreams = {'Vm' 'scaled'};
handles.user.protocol.IClampRamp.SaveStreams = {'Vm' 'scaled'};
handles.user.protocol.IClampRamp.PostProcessFunction = 'PostAdjustGain';
handles.user.protocol.IClampRamp.plot = handles.user.protocol.TestProtocol.plot;
handles.user.protocol.IClampRamp.FunGen.Amplitude = 0.1; %if Amp = 1, then 1 nA peak current (2nA P-P)
handles.user.protocol.IClampRamp.FunGen.Offset = 0.05; % (nA)
handles.user.protocol.IClampRamp.FunGen.ExtCmdSensitivity = 0.4; %nA/mV
handles.user.protocol.IClampRamp.Properties.Frequency = struct('Name','Frequency (Hz)','Value',0.1,'Type','Number','Range',[0.001 100000],'Callback','UpdateFunGenProp');
handles.user.protocol.IClampRamp.Properties.Amplitude = struct('Name','Amplitude (nA)','Value',0.1,'Type','Number','Range',[0.001 10],'Callback','UpdateFunGenProp');
handles.user.protocol.IClampRamp.Properties.Offset = struct('Name','Offset (nA)','Value',0.05,'Type','Number','Range',[-10 10],'Callback','UpdateFunGenProp');
%handles.user.protocol.IClampRamp.LPFilter.FilterVar = 'Im';
%handles.user.protocol.IClampRamp.Properties.CornerFreq.Name = 'Butterworth LPF (Im)';

handles.user.protocol.thirtyseconds = DefaultProtocol;
handles.user.protocol.thirtyseconds.Name = '30s Recording';
handles.user.protocol.thirtyseconds.duration = 30;
handles.user.protocol.thirtyseconds.PostProcessFunction = 'PostAdjustGain';
handles.user.protocol.thirtyseconds.plot.numsubplots = 2;
handles.user.protocol.thirtyseconds.plot.graph = struct('title','','xvar','yvar','xlabel','','ylabel','','xscale',1,'yscale',1);
handles.user.protocol.thirtyseconds.plot.graph(1).title = 'Membrane Potential';
handles.user.protocol.thirtyseconds.plot.graph(1).xvar = 't';
handles.user.protocol.thirtyseconds.plot.graph(1).yvar = 'Vm';
handles.user.protocol.thirtyseconds.plot.graph(1).xlabel = 'Time (seconds)';
handles.user.protocol.thirtyseconds.plot.graph(1).ylabel = 'Voltage (mV)';
handles.user.protocol.thirtyseconds.plot.graph(1).xscale = 1;
handles.user.protocol.thirtyseconds.plot.graph(1).yscale = 1000;
handles.user.protocol.thirtyseconds.plot.graph(2).title = 'Membrane Current';
handles.user.protocol.thirtyseconds.plot.graph(2).xvar = 't';
handles.user.protocol.thirtyseconds.plot.graph(2).yvar = 'Im';
handles.user.protocol.thirtyseconds.plot.graph(2).xlabel = 'Time (seconds)';
handles.user.protocol.thirtyseconds.plot.graph(2).ylabel = 'Current (nA)';
handles.user.protocol.thirtyseconds.plot.graph(2).xscale = 1;
handles.user.protocol.thirtyseconds.plot.graph(2).yscale = 1000;
handles.user.protocol.thirtyseconds.Visible = false;

handles.user.protocol.calcR = DefaultProtocol;
handles.user.protocol.calcR.Name = 'Calculate Resistance';
handles.user.protocol.calcR.CaptureStreams = {'Vm' 'scaled'};
handles.user.protocol.calcR.SaveStreams = '';
handles.user.protocol.calcR.SaveSingles = {'gain','freq','temp'};
handles.user.protocol.calcR.ContProcessFunction = 'CalcR';
handles.user.protocol.calcR.Repeat = 0;
handles.user.protocol.calcR.fs = 100000;
handles.user.protocol.calcR.duration = 0.5;
handles.user.protocol.calcR.FunGen.Enable = true;
%handles.user.protocol.calcR.FunGen.OutputMode = 'Voltage';
handles.user.protocol.calcR.FunGen.Frequency = 2; %Hz
handles.user.protocol.calcR.FunGen.Amplitude = 20; %mV
handles.user.protocol.calcR.FunGen.Offset = 0;
handles.user.protocol.calcR.FunGen.Phase = 0;
handles.user.protocol.calcR.FunGen.WaveType = 'Square';
handles.user.protocol.calcR.Properties.Frequency = struct('Name','Frequency (Hz)','Value',2,'Type','Number','Range',[0.001 100000],'Callback','UpdateFunGenProp');


handles.user.protocol.calcC = DefaultProtocol;
handles.user.protocol.calcC.Name = 'Calculate Capacitance';
handles.user.protocol.calcC.CaptureStreams = {'Vm' 'scaled'};
handles.user.protocol.calcC.SaveStreams = '';
handles.user.protocol.calcC.SaveSingles = {'gain','freq','temp'};
handles.user.protocol.calcC.ContProcessFunction = 'CalcC';
handles.user.protocol.calcC.Repeat = 1;
handles.user.protocol.calcC.fs = 100000;
handles.user.protocol.calcC.duration = 1;
handles.user.protocol.calcC.FunGen.Enable = true;
handles.user.protocol.calcC.FunGen.Frequency = 2; %Hz
handles.user.protocol.calcC.FunGen.Amplitude = 0.020; %nA
handles.user.protocol.calcC.FunGen.Offset = -0.01;
handles.user.protocol.calcC.FunGen.Phase = 0;
handles.user.protocol.calcC.FunGen.WaveType = 'Square';
handles.user.protocol.calcC.FunGen.ExtCmdSensitivity = 0.4; %nA/mV

handles.user.protocol.Vsteps = handles.user.protocol.VClampSquare;
handles.user.protocol.Vsteps.Name = 'Voltage steps';
handles.user.protocol.Vsteps.Properties.duration = struct('Name','Step Duration','Value',1,'Type','Number','Range',[0.1 100],'Callback','UpdateGeneralProp');
handles.user.protocol.Vsteps.Properties.Repeat = struct('Name','Repetitions','Value',15,'Type','Number','Range',[0 100],'Callback','UpdateGeneralProp');
handles.user.protocol.Vsteps.Properties = rmfield(handles.user.protocol.Vsteps.Properties,'Amplitude');
handles.user.protocol.Vsteps.Properties = rmfield(handles.user.protocol.Vsteps.Properties,'Offset');
handles.user.protocol.Vsteps.ContProcessFunction = 'StepCommand';
handles.user.protocol.Vsteps.StepVar = 'Vm';
handles.user.protocol.Vsteps.Properties.StartAmplitude = struct('Name','Starting Amplitude (mV)','Value',-30,'Type','Number','Range',[-200 200],'Callback','UpdateStepProp');
handles.user.protocol.Vsteps.Properties.Increment = struct('Name','Increment (mV)','Value',10,'Type','Number','Range',[-200 200],'Callback','UpdateStepProp');

handles.user.protocol.Isteps = handles.user.protocol.IClampRamp;
handles.user.protocol.Isteps.Name = 'Current steps';
handles.user.protocol.Isteps.FunGen.WaveType = 'Square';
handles.user.protocol.Isteps.ContProcessFunction = 'StepCommand';
handles.user.protocol.Isteps.Properties = rmfield(handles.user.protocol.Isteps.Properties,'Amplitude');
handles.user.protocol.Isteps.Properties = rmfield(handles.user.protocol.Isteps.Properties,'Offset');
handles.user.protocol.Isteps.Properties.Frequency.Value = 2;
handles.user.protocol.Isteps.Properties.duration = struct('Name','Step Duration','Value',1,'Type','Number','Range',[0.1 100],'Callback','UpdateGeneralProp');
handles.user.protocol.Isteps.Properties.Repeat = struct('Name','Repetitions','Value',11,'Type','Number','Range',[0 100],'Callback','UpdateGeneralProp');
handles.user.protocol.Isteps.StepVar = 'Im';
handles.user.protocol.Isteps.Properties.StartAmplitude = struct('Name','Starting Amplitude (nA)','Value',0,'Type','Number','Range',[-2 2],'Callback','UpdateStepProp');
handles.user.protocol.Isteps.Properties.Increment = struct('Name','Increment (nA)','Value',0.01,'Type','Number','Range',[-2 2],'Callback','UpdateStepProp');


% TestProtocol properties
handles.user.protocol.TestProtocol.Properties.duration = struct('Name','Duration (s)','Value',1,'Type','Number','Range',[0.1 100],'Callback','UpdateGeneralProp');
handles.user.protocol.TestProtocol.Properties.Frequency = struct('Name','Frequency (Hz)','Value',100,'Type','Number','Range',[0.001 100000],'Callback','UpdateFunGenProp');
handles.user.protocol.TestProtocol.Properties.Amplitude = struct('Name','Amplitude (mV)','Value',20,'Type','Number','Range',[1 10000],'Callback','UpdateFunGenProp');
handles.user.protocol.TestProtocol.Properties.Offset = struct('Name','Offset (mV)','Value',10,'Type','Number','Range',[-10000 10000],'Callback','UpdateFunGenProp');
handles.user.protocol.TestProtocol.Properties.WaveType = struct('Name','Waveform Type','Value','Square','Type','List','Range',{{'DC','Sine', 'Square', 'Triangle','Positive Ramp','Negative Ramp'}},'Callback','UpdateFunGenProp');
handles.user.protocol.TestProtocol.Properties.Enable = struct('Name','Enable','Value',false,'Type','Boolean','Callback','UpdateFunGenProp');
