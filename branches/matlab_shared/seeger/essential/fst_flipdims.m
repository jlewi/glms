%FST_FLIPDIMS Operator for flipping dimensions
%  FST_FLIPDIMS(A,N)
%
%  Operates on A, which is of size N*D1-by-D2. The flat buffer of
%  A is taken as N-by-D1-by-D2 tensor, and dimensions 2 and 3
%  are exchanged. The resulting matrix is reshaped to have D1
%  columns. In-place variant of
%    RESHAPE(PERMUTE(RESHAPE(A,N,D1,D2),[1 3 2]),N*D2,D1),
%  but Matlab PERMUTE is not in-place.
%
%  NOTE: This is not straightforward if D1~=D2. We use CACM Algorithm
%  467 which does transposition in place:
%    Brenner: Matrix Transposition in Place, CACM 16(11), 1973
%    Available: portal.acm.org
%  See also matrix/TransInPlace.h of STATSIM.

%  Copyright (C) 2005 Matthias Seeger
% 
%  This program is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
%  USA.
