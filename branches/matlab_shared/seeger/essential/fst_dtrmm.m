%FST_DTRMM Operator multiplication with triangular matrix
%  FST_DTRMM(B,A,LEFT,TRS,{ALPHA=1})
%
%  B = ALPHA*op(A)*B (LEFT==1), op(X) == X or X', dep. on TRS
%  B = ALPHA*B*op(A) (LEFT==0)
% 
%  Here, A must be triangular (UPLO str. code required, DIAG code
%  optional) and square. If B has str. codes, they are ignored.

%  Copyright (C) 2005 Matthias Seeger
% 
%  This program is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
%  USA.
