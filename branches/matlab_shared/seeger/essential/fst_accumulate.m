%FST_ACCUMULATE Special accumulation function
%  RET = FST_ACCUMULATE(A,IV,B,{BTR})
%
%  Specialized accumulation function. A is NN-by-NC matrix, IV an
%  NN index vector, B cell array of index vectors. For each i,
%  RET(i) is the sum of A(i,j), where j runs over B{IV(i)}.
%  If BTR is given, it is an additional index. In this case, RET(i)
%  is the sum of A(i,BTR(j)), where j runs over B{IV(i)}.

%  Copyright (C) 2005 Matthias Seeger
% 
%  This program is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
%  USA.
