%FST_DSPAMMT Operator multiplication by A*A', A sparse
%  FST_DSPAMMT(C,A,B,{ALPHA=1},{BETA=0})
%
%  C = ALPHA*A*A'*B + BETA*C
% 
%  Here, A is a sparse matrix, while C, B are dense matrices.
%  Structure codes are ignored. Intermed. storage of size COLS(B)
%  is used, but A'*B does not have to be stored.

%  Copyright (C) 2005 Matthias Seeger
% 
%  This program is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
%  USA.
