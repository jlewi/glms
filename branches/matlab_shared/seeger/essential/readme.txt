SOME ESSENTIAL MATLAB AND MEX FUNCTIONS
---------------------------------------

Copyright (C) 2005 Matthias Seeger

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor,
Boston, MA  02110-1301, USA.

Matthias Seeger
Max Planck Institute for Biological Cybernetics
P.O. Box: 21 69
72012 Tuebingen, Germany
seeger@tuebingen.mpg.de
www.kyb.tuebingen.mpg.de/bs/people/seeger/

1) Installation

- Untar essential.tar.gz:
    tar xzf essential.tar.gz
  Go to subdirectory essential.
- Compile MEX files:
    make
  Produces files XXX.<dll-ending> for each XXX.c, where <dll-ending> is s.th.
  like "mexglx", "mexa64" (Linux), or something else for your system.
  If this does not work, consult your Matlab docs for how to compile MEX
  files and edit the Makefile.
- Copy all files *.m, *.<dll-ending> somewhere into your Matlab path. The
  XXX.m files for MEX functions XXX simply contain the help text.

2) How to use it

The functions are explained in the help texts.
Part of the ESSENTIAL package is the FST package, which has some
documentation in fst_overview.txt.
