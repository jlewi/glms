/* -------------------------------------------------------------------
 * FST_SUMPOS
 *
 * Y is obtained by starting from 0 of size YN, then adding X(j)
 * to Y(IND(j)) for all j.
 *
 * Input:
 * - X:   Input vector
 * - IND: Index
 * - YN:  S.a.
 * Return:
 * - Y:   Return vector
 * -------------------------------------------------------------------
 * Matlab MEX Function
 * Author: Matthias Seeger
 * ------------------------------------------------------------------- */

/*
 * Copyright (C) 2005 Matthias Seeger
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#define MATLAB_VER65

#include <math.h>
#include "mex.h"
#include "mex_helper.h"

char errMsg[200];

/* Main function FST_SUMPOS */

void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
  int i,yn,n,pos;
  const double* xvec,*ind;
  double* yvec;

  /* Read arguments */
  if (nrhs<3)
    mexErrMsgTxt("Not enough input arguments");
  if (nlhs!=1)
    mexErrMsgTxt("Function returns 1 argument");
  if ((n=getVecLen(prhs[0],"X"))==0)
    mexErrMsgTxt("X is empty");
  xvec=mxGetPr(prhs[0]);
  if (getVecLen(prhs[1],"IND")!=n)
    mexErrMsgTxt("IND has wrong size");
  ind=mxGetPr(prhs[1]);
  yn=getScalInt(prhs[2],"YN");
  if (yn<1)
    mexErrMsgTxt("YN wrong");

  plhs[0]=mxCreateDoubleMatrix(yn,1,mxREAL);
  yvec=mxGetPr(plhs[0]);
  for (i=0; i<n; i++) {
    pos=((int) *(ind++))-1;
    if (pos<0 || pos>=yn)
      mexErrMsgTxt("IND or YN wrong");
    yvec[pos]+=(*(xvec++));
  }
}
