%function sim=PathABS(path)
%   path - a string for the path to represent
%
% Explanation: An FPathBaseCMD object where we actually specify the path
%   of the target.
%   This is useful when accessing files on remote machines.
%
%$Revision$
%Revisions:
%   080829- Convert to new OOP model
classdef (ConstructOnLoad=true) PathABS< FPathBaseCMD

    %*****************************************
    %properties
    %*******************************************
    %pname - the path this object represents
    properties(SetAccess=private,GetAccess=public)
        version=080829;
        pname=[];
    end

    methods
        function obj=PathABS(varargin)

            obj=obj@FPathBaseCMD();
            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required


                case 1
                    obj.pname=varargin{1};
            end

        end
    end
end