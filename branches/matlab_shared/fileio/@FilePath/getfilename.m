%function getrpath=getfilename(obj)
%	 obj=FilePath object
% 
%Return value: 
%	 fname- just the filename
%
function fname=getfilename(obj)
	 [rdir fname fext]=fileparts(getrpath(obj));
     fname=[fname fext];
