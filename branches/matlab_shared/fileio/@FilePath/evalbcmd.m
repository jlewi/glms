%function bcmd=bcmd(obj)
%	 obj=FilePath object
% 
%Return value: 
%	 bcmd= the output of evaluating the base command
%
%Revisions
%   10-01-2008
%       if obj is empty then set p=[]
%       Due this to avoid the disp method throwing an error when bcmd isn't
%       set
function bcmd=evalbcmd(obj)

if isempty(obj.basecmd)
    bcmd=[];
else
%    bcmd=evalin('base',obj.basecmd);
    %basecmd should be a global variable   
%get the base path 
     bcmd=getpath(obj.basecmd);
end
