%function [in,vinf]=varinfile(fname,vname)
%   fname - file
%   vname - variable name
%
% Return
%   in - 1 - variable exists in file
%       -0 variable does not exist in file
%   vinf - info about variable
% Explanation: 
%   checks if a variable is stored in a .mat file
function [in,vinf]=varinfile(fname,vname)
%I think issue might be whos won't work with relative file paths
%try using my function abspath to convert filename to absolute file
%error('for some files whos doesnt work I dont know why');
if isa(fname,'FilePath')
    fname=getpath(fname);
end
vinf=whos('-file',abspath(fname),'-regexp', ['^' vname '$']);

%in=strmatch(vname,{vinf.name});
if isempty(vinf)
    in=false;
else
    in=true;
end