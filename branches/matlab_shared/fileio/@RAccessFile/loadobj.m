%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
function obj=loadobj(lobj)

cname='RAccessFile';

%indicates whether we have already performed a copy
%copying the fields of lobj to obj when lobj is a structure
%because we only want to do the copying once
cstruct=false;

%get the latest version
%save the latest version so we can check to make sure all
%conversions have been completed.
eval(sprintf('newobj=%s();',cname));

latestver=newobj.version.(cname);

if isstruct(lobj)
    obj=newobj;
   
else
    %we don't need to create a new object
 
    obj=lobj;
end

 if ~isstruct(lobj.version)
        vnum=lobj.version;
        obj.version=struct();
        obj.version.(cname)=vnum;
    else
        obj.version.(cname)=lobj.version.(cname);
    end

%******************************************************************
%Sequentially convert one version of the object to the next
%until we get to the latest version
%
%Warning: If lobj is a structure then we haven't copied the data from lobj
% to obj yet.
%******************************************************************

%**********************************************************************
%Convert old objects to new object model
%*******************************************************************
if (obj.version.(cname)<080609)
    finfo=obj.finfo;

    %the new version no longer uses pointers to store the data
    finfo=rmfield(finfo,{'headptr','deletedptr','idsptr'});

    obj.finfo=finfo;
    obj.version.(cname)=080609;
end

if (obj.version.(cname)<080805)
    %we just added the field DELID so we don't have to do anything else
    obj.version.(cname)=080805;
end

if (obj.version.(cname) < 090120)
    %do conversion
    %just changed version to a structure
    obj.version.(cname)=090120;
end

%check if converted all the way to latest version
if (obj.version.(cname) <latestver)
    error('Object has not been fully converted to latest version. \n');
end

%**********************************************************************
%Initialization
%*********************************************************************

%initialize the object after loading it from file

initonload(obj);


%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
%
%Copy fields must be declared within loadobj because otherwise it won't
%have permssion to set the private fields of the object.
function dest=copyfields(source,dest,skip)
%we just need to copy the fields
%and handle any special cases if required
fnames=fieldnames(source);

%struct for object
sobj=struct(dest);
for j=1:length(fnames)
    switch fnames{j}
        case skip
            %do nothing we skip this field
        otherwise
            %set the new field this will cause an error
            %if the field isn't a member of the new object
            dest.(fnames{j})=source.(fnames{j});
    end
end








