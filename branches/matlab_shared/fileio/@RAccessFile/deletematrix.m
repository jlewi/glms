%function deletematrix(obj,id)
%   ids - id of the matrix to delete
%
%Explanation: To delete a matrix in the file we set its id
%   to a negative number. This way subsequent calls to writematrix will
%   cause the record in the file to be overwrriten when we write a new
%   matrix to the file.
%
%02-05-2009
%   can delete multiple matrices
function obj=deletematrix(obj,ids)

for ind=1:length(ids)
    id=ids(ind);
recnum=posofid(obj,id);

%id to assighn a deleted record
did=-1;

if isnan(recnum)
    error('cannot delete id=%d it is not in the file',id);
end

    head=getheader(obj);
    
if (labindex==1)

  
    %seek to the start of this
    %matrix
    %start of the record
    recstart=offset(obj,recnum,head.dim);
    fseek(obj.fid,recstart,'bof');

    fwrite(obj.fid,did,'double');


    %update the header
    head.nmat=head.nmat-1;
    writeheader(obj,head);

else
    
    head.nmat=head.nmat-1;
    %still update the header.
    writeheader(obj,head);
end
%update the deleted ptrs on all labs
obj.finfo.ids=setat(obj.finfo.ids,recnum,did);
obj.finfo.deletedrecnums=[obj.finfo.deletedrecnums recnum];

end
labBarrier;