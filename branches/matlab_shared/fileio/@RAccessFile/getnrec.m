%function nrec=getnrec(obj)
%	 obj=RAccessFile object
% 
%Return value: 
%	 nrec=obj.nrec 
%    get number of records in the file (some records may be deleted)
%
function nrec=getnrec(obj)
    head=getheader(obj);
	 nrec=head.nrec;
