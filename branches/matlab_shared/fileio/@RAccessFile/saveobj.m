%function obj=saveobj(obj)
%   obj -RAccessFile
%
% Explanation: prepare the object to be saved to a file
%
%Revisions:
%   08-11-2008: We no longer have to copy the object before saving
%       because we declare the fields we don't want to save as being
%       transient properties
function sobj=saveobj(obj)
    
    sobj=obj;
    
    