%Function [head,obj]=getheader(fobj)
%   obj - a CFile2d object
%
%Return value:
%   header - header for the file
%       header.nrec = number of records in the file
%       header.nmat = number of matrices stored in the file
%       header.dim     = dimensionality [#rows, #cols];
%       header.fver     = file format version
%get the header for the file
%
%WARNING: in order for the header to be saved af
function [header,obj]=getheader(obj)

%check if we've already read the header
if ~isempty(obj.finfo.header)
    header=obj.finfo.header;
else
    fname=getpath(obj.fname);
    if ~exist(fname,'file')
        error(sprintf('File %s doesnt exist',fname));
    end

    %goto zeroth byte
    fseek(obj.fid, obj.fwidth*0, 'bof');

    %read the header
    data = fread(obj.fid, obj.hsize, 'double');

    header.fver=data(1);
    header.nrec=data(2);
    header.nmat=data(3);
    header.dim=data(4:end);

    obj.finfo.header=header;

end

%add a lab barrier because if nodes are reading header from the file
%we don't want other nodes to be changing it.
labBarrier;