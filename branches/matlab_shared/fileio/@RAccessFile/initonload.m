%function initonload(obj)
%
%Explanation: Called by laodobj to perform initialization when loading an
%RACessFile object from a file
function initonload(obj)
%*read the file from the data
if ~exist(getpath(obj.fname),'file')
    error('File: %s does not exist', getpath(obj.fname));
end

obj.fid=fopen(getpath(obj.fname),'r+');

% create headptr before readmatids because readmatids will call
% getheader which tries to access finfo.headptr
obj.finfo.header=[];
%read the id's associated with each record
%create a pointer to an array to store them
obj.finfo.ids=DynArray('data',readmatids(obj));



obj.finfo.deletedrecnums=find(isdeleted(obj,1:getnrec(obj)));

%1. read the header and check the file id hasn't changed
head=getheader(obj);

if (head.fver~=obj.fileid)
    error('The fileid stored in the object does not match the fileid in the file.');
end
