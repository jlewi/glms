
%**************************************************************************
%test loading and saving an RAccessFile object
%*************************************************************************


function testloadsave(robj)
clear data;
clear dinfile;

fobj=FilePath('bcmd','TMPDIR','rpath','robj.mat');
fobj=seqfname(fobj);

fprintf('Testing: saving and loading an RAccessFile object.\n');
if (labindex==1)
    save(getpath(fobj),'robj');
end

fobj=labBroadcast(1,fobj);

%load the object and verify the data
lobj=load(getpath(fobj),'robj');
lobj=lobj.robj;


[dinfile.mat dinfile.ids]=readdata(lobj);
[data.mat data.ids]=readdata(robj);

%sort the data by the id's to facilitate the comparison
[dinfile.ids sindex]=sort(dinfile.ids);
dinfile.mat=dinfile.mat(sindex);

[data.ids sindex]=sort(data.ids);
data.mat=data.mat(sindex);
if ~(verifydata(data,dinfile))
    error('data is inaccurate after saving and loadding raccessfile object.');
end

fprintf('Success: Saving and loading of object works. \n');

fprintf('Success RAccessFile works. \n');
