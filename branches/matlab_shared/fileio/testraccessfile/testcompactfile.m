%function 
function testcompactfile(dim,nmat,fpath)

%how many to delete
ndelete=ceil(.5*nmat);

%generate some initial data
data=testraccessgendata(dim,nmat);

%shuffle the data so we test that compact file sorts it properly
ind=randperm(nmat);
data.ids=data.ids(ind);

robj=RAccessFile('fname',fpath,'dim',dim,'debug',false);

robj=writedata(robj,[data.mat],[data.ids]);

%verify the data
[dinfile.mat dinfile.ids]=readdata(robj);
vm=verifydata(data,dinfile);

if ~(vm)
    error('Verify data failed');
end

%randomly delete some matrices
idelete=randperm(nmat);
itokeep=idelete(ndelete+1:end);
idelete=idelete(1:ndelete);

for dind=idelete
    deletematrix(robj,data.ids(dind));
end



ddel=data;
ddel.mat=ddel.mat(itokeep);
ddel.ids=ddel.ids(itokeep);

[dinfile.mat dinfile.ids]=readdata(robj);
%the elements in ddel and dinfile won't be in the same order because
%of the shuffling to generate itokeep
%therefore we srt them before doing the check
[ddel.ids,ind]=sort(ddel.ids);
ddel.mat=ddel.mat(ind);

[dinfile.ids,ind]=sort(dinfile.ids);
dinfile.mat=dinfile.mat(ind);

%sort itokeep so that it will match order in the file

vm=verifydata(ddel,dinfile);

if ~(vm)
    error('Verify data failed');
end

%************************************
%compact the file
compactfile(robj);

%read and verify the data
[dinfile.mat dinfile.ids]=readdata(robj);
vm=verifydata(ddel,dinfile);


if ~(vm)
    error('Verify data failed');
end
fprintf('Success compactfile.m works\n');