%function getpath(robj,hostid)
%   robj - the RemoteFilePath object
%   hostid - the id of the host for which we evaluate the base command
function fname=getpath(robj,hostid)

bcmd=getbcmd(robj);

if ~isfield(bcmd,hostid)
    error('bcmd is not specified for hostid=%s',hostid);
end

fname=fullfile(getpath(bcmd.(hostid)),getrpath(robj));
