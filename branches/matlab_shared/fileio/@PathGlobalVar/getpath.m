%function path=getpath(obj)
%	 obj=FPathBaseCMD object
% 
%Return value: 
%   evaluate the path represented by this object
%	 path=obj.path 
%
function bcmd=getpath(obj)
	
    %the path is stored as the value of a global variable
   eval(['global ' obj.gvar]);  
   bcmd=eval(obj.gvar);