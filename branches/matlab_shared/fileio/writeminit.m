%function writedataset(fname,data,info)
%   fname - file to write dataset to
%   data  - a structure array of field name value paris
%         - comment - a comment to place before the dataset
%
%   info  - field structure array
%         - written as comments in header of file
%
%Explanation: This creates a matlab function which can be executed
%   to create an array which specifies the parameters for a simulation
%
%Revision:
%   08-01-2008
%       Create functions instead of scripts
function writeminit(fname,dsets,info)

if isa(fname,'FilePath')
    fname=getpath(fname);
end
if exist(fname,'file')
    error('File already exists');
end

fid=fopen(fname,'w');

[fdir,funcname,fext]=fileparts(fname);

fprintf(fid,'function [dsets]= %s() \n',funcname);
fprintf(fid,'%%********************************\n');

if ~isempty (info)
fprintf(fid,'%%********************************\n');
fprintf(fid,'%%Data set info\n');
fprintf(fid,'%%********************************\n');
fnames=fieldnames(info);
for ind=1:length(fnames)
    if ~isstr(info.(fnames{ind}))
        data=num2str(info.(fnames{ind}));
    else
        data=info.(fnames{ind})
    end
   fprintf(fid,'%% %s=%s \n',fnames{ind},data);
end
end

fprintf(fid,'\n');
fprintf(fid,'\n');


fnames=fieldnames(dsets);

fprintf(fid,'dsets=[];\n');
%loop through the data set
for ind=1:length(dsets)
    
    %print a comment if there is one
    if (isfield(dsets(ind),'comment') && ~isempty(dsets(ind).comment))
       fprintf(fid,'%%************************************************\n');
       comment=dsets(ind).comment;
       comment=regexp(comment,'\n','split');
       for j=1:length(comment)
          fprintf(fid,'%% %s \n',comment{j}); 
       end
       fprintf(fid,'%%********************************************\n');
    end
    fprintf(fid,'dind=%d;\n',ind);
    for k=1:length(fnames)
        if ~strcmpi(fnames(k),'comment')
        if isa(dsets(ind).(fnames{k}),'FilePath')
              fprintf(fid,'dsets(dind).%s=FilePath(''bcmd'',''%s'',''rpath'',''%s'',''isdir'',%d);\n',fnames{k},getgvar(getbcmd(dsets(ind).(fnames{k}))),getrpath(dsets(ind).(fnames{k})),isdir(dsets(ind).(fnames{k})));
        elseif isnumeric(dsets(ind).(fnames{k}))
             if ~isempty(dsets(ind).(fnames{k}))
               fprintf(fid,'dsets(dind).%s=%d;\n',fnames{k},dsets(ind).(fnames{k}));
             else
                              fprintf(fid,'dsets(dind).%s=[];\n',fnames{k});
             end
        elseif isempty(dsets(ind).(fnames{k}))
             fprintf(fid,'dsets(dind).%s=[];\n',fnames{k});
        else
               fprintf(fid,'dsets(dind).%s=''%s'';\n',fnames{k},dsets(ind).(fnames{k}));
        end
        end
    end
    
    %*************************************************
    %also print the setup file
    %************************************************
    fprintf(fid,'dsets(dind).setupfile=[mfilename(''fullpath'') ''.m''];\n');
    fprintf(fid,'\n\n');
end

fclose(fid);