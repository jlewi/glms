%function [fname]=savedata(fname,hf,opts)
%   fname  - name of file to save to
%   hf     - handle to figure
%   opts   - Options for saving
%          - .overwrite = 1
%          - .cdir   = 1  -create directory if it doesn't exist
% Explanation: Saves figure
%   EPS - files automatically saved as level 2 color eps
%   Performs error checking:
%       1. If filename allready exists it gets the next availble filename
%           by using seqfname (This could potentially cause weird
%           numbering if the filename already has a number such as _0001
%           overwriteed to it
%           unless opts.overwrite=1 in which case data is overwriteed
%       2. Checks the directory for the file exists. If it doesn't it will
%           save to the working directory
%  Return
%       fname - name to which it was saved
function [fname]=savegraph(fname,hf,opts)

optstr='';      %string of options
if ~(exist('opts','var'))
    opts=[];
end
if ~(isfield(opts,'overwrite'))
    opts.overwrite=0;
else
    if (opts.overwrite=='y' | opts.overwrite=='Y')
        opts.overwrite=1;
    end
    if (opts.overwrite=='n' | opts.overwrite=='N' | opts.overwrite=='No')
        opts.overwrite=0;
    end
end

if ~(isfield(opts,'cdir'))
    opts.cdir=0;
end

[pathstr,name,ext] = fileparts(fname);

%check if directory exists
if ~(exist(pathstr,'dir'))
    fprintf('Warning: Directory %s does not exist. \n', pathstr);
    if (opts.cdir~=0)
         fprintf('Creating: Directory \n');
         r=mkdir(pathstr);
         if (r==0)
             fprintf('Could not create directory %s. \n', pathstr);
             fprintf('Warning: Saving to working directory %s \n', pwd);
            pathstr=pwd;
         end
    else
        fprintf('Warning: Saving to working directory %s \n', pwd);
        pathstr=pwd;
    end
end

%fullfile path
%this takes care of pathsepartor charactor for the platform
fname=fullfile(pathstr, [name ext]);

if (exist(fname,'file'))
    fprintf('Warning: File %s exists \n',fname);
    if (opts.overwrite~=0)
        fprintf('Overwriting: File \n');
    else
    %get the next available filename
    fname=seqfname(fname);
    %make sure that file doesn't exist
    %that can happen 
    if exist(fname,'file')
        fname=seqfname(fname);
    end
    %check it again
    if exist(fname,'file')
        fprintf('Error: Could not save data. Could not find unused file \n');
        return;
    else
        fprintf('Saving to %s instead \n',fname);
    end
    end

end

%make sure its a valid graph handle
if isempty(hf)
    fprintf('Cannot save figure. No figure provided \n');
    return;
end


isvalid=ishandle(hf);

if isvalid==1
    if strcmpi(ext,'.eps')
        %if its eps file save it as color level 2 eps
        saveas(hf,fname,'epsc2');
    else
        %save the figure
        %set the paper position mode to auto
        %this way the figure is the same size as it appears on screen this
        %means when you import it into gobinder you shouldn't have to
        %resize
        set(hf,'PaperPositionMode','auto');
        set(hf,'PaperPosition',[0.25 2.5 4.0 3.0]);
        saveas(hf,fname);
    end
    
else
    fprintf('Cannot save figure. Handle not a valid figure \n');
end