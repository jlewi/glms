%function to convert a set of strings separated by ':' to a string array
%
%$Revision Histor$: remove hidden directories
%   i.e dirctories beginning with .
%Revision history
%   4-15-2009
%       Modifications to handle paths on windows machines
%   6-10-2007 Add 2nd parameter to sepecify the separator
%
%Explantion: Similar to Matlab's private function ParseDirs
function strcell=sarraytocell(sin,sep)
  
if isempty(sin)
    strcell={};
    return;
end

%default separator is ':'
if ~exist('sep','var')
   sep=pathsep; 
end
if (length(sep)>1)
    error('length of seperator must be 1');
end

%remove any leading ':'
if (sin(1)==sep)
    sin=sin(2:end);
end

%add a trailing ':'
if (sin(end)~=sep)
    sin=[sin sep];
end

%remove hidden directories
sin=rmhiddendirs(sin);

%regexprep(GRAPHPATHS,'[^:]*/\.[^:]*:',':')
% if (filesep=='\')
%     %windows machine
%     sin=regexprep(sin,['[^' sep ']*\\.[^' sep '\.]*' sep], sep);
% else
% sin=regexprep(sin,['[^' sep ']*/\.[^' sep '\.]*' sep], sep);
% end
% sin=regexprep(sin,[ sep '{2,1000}'],sep);
% 
% 

% find the path separations
 sepind=strfind(sin,sep);

strcell=cell(1,length(sepind));

for index=1:length(sepind)
    if index>1
        strcell{index}=sin((sepind(index-1)+1):(sepind(index)-1));
    else
        strcell{index}=sin(1:(sepind(index)-1));
    end    
end