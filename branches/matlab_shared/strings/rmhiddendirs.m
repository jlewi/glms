%function rmhidden(dlist)
%   dlist= string of paths separated by path  separator
%   
%Explanation: removes the hidden directories from this list
%
%$Revision Histor$: remove hidden directories
%   i.e dirctories beginning with .
%Revision history
%   6-10-2007 Add 2nd parameter to sepecify the separator
%
function sin=rmhiddendirs(sin,sep)
  

if isempty(sin)
    return;
end


%default separator is ':'
if ~exist('sep','var')
   sep=pathsep; 
end
if (length(sep)>1)
    error('length of seperator must be 1');
end

%remove any leading ':'
if (sin(1)==sep)
    sin=sin(2:end);
end

%add a trailing ':'
if (sin(end)~=sep)
    sin=[sin sep];
end

%remove hidden directories
%regexprep(GRAPHPATHS,'[^:]*/\.[^:]*:',':')

%check filesepartor character to determine operating system
if (filesep=='\')
    %windows
     sin=regexprep(sin,['[^' sep ']*\.[^' sep '\.]*' sep], sep);
else
    sin=regexprep(sin,['[^' sep ']*/\.[^' sep '\.]*' sep], sep);
end
%now replace any repeated pathset with a single path sep

sin=regexprep(sin,[ sep '{2,1000}'],sep);


