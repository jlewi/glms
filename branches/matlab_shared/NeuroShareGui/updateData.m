% function handles=updateData(handles)
%   handles - handles to objects
%
%   Explanation: Updates the entries of the gui based on the current
%   selection of the listbox.
function handles=updateData(handles)


hfile=handles.ns.hfile;
totaltime=handles.ns.FileInfo.TimeSpan;

%get the index of the currently selected item
index=get(handles.cbo_entitylist,'value')
eindex=handles.guiData.listIDs(index);

[ns_RESULT, nsAnalogInfo] = ns_GetAnalogInfo(hfile, eindex) ;
[nsresult, ContinuousCount, wave] = ns_GetAnalogData(hfile,eindex,1,totaltime*nsAnalogInfo.SampleRate);
%fprintf('ContinuousCount= %d',ContinuousCount);

%plot the data
 axes(handles.maxes);
 t=linspace(1,totaltime,totaltime*nsAnalogInfo.SampleRate);
 plot(t,wave);
 xlabel('Time Seconds');
 ylabel(nsAnalogInfo.Units);
 
 %save the data in the handles structure
 handles.guiData.t=t;
 handles.guiData.wave=wave;
 
 %display info about the analog data
 einfo=sprintf('Type: Analog Event  \n');
 einfo=sprintf('%s Sample Rate \t %d \n',einfo,nsAnalogInfo.SampleRate);
 einfo=sprintf('%s MinVal \t %d \n',einfo,nsAnalogInfo.MinVal);
 einfo=sprintf('%s MaxVal \t %d \n',einfo,nsAnalogInfo.MaxVal);
 einfo=sprintf('%s Units \t %s \n',einfo,nsAnalogInfo.Units);
 set(handles.txt_info,'String',einfo);
 
 