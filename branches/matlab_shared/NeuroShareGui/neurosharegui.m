function varargout = neurosharegui(varargin)
% NEUROSHAREGUI M-file for neurosharegui.fig
%      NEUROSHAREGUI, by itself, creates a new NEUROSHAREGUI or raises the existing
%      singleton*.
%
%      H = NEUROSHAREGUI returns the handle to a new NEUROSHAREGUI or the handle to
%      the existing singleton*.
%
%      NEUROSHAREGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NEUROSHAREGUI.M with the given input arguments.
%
%      NEUROSHAREGUI('Property','Value',...) creates a new NEUROSHAREGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before neurosharegui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to neurosharegui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help neurosharegui

% Last Modified by GUIDE v2.5 21-Feb-2005 13:15:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @neurosharegui_OpeningFcn, ...
                   'gui_OutputFcn',  @neurosharegui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before neurosharegui is made visible.
function neurosharegui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to neurosharegui (see VARARGIN)

% Choose default command line output for neurosharegui
handles.output = hObject;




%**************************************************************************
%Added Structures to Handles
%**************************************************************************
handles.ns=[];  %array to hold all the NeuroShare Api data
handles.guiData=[];    %holds information about data displayed in the gui
handles.guiData.listIDs=[]; %holds the entityIDs to associate with entries in the listbox.

handles=setdefaults(handles);   %call function to set default values

%Check if we have a valid NeuroShare Library DLL if not 
%open a file and get the path for one
if ~(exist(handles.ns.nsLibFile,'file'))
    [filename, pathname] = uigetfile('*.DLL', 'Choose A NeuroShare Library;');
    handles.ns.nsLibFile=[pathname '/' filename];
end
% UIWAIT makes neurosharegui wait for user response (see UIRESUME)
% uiwait(handles.neurosharegui_fig);

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = neurosharegui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%file = uigetfile('*.fig');
%if ~isequal(file, 0)
%    open(file);
%end

%Load a NeuroShareFile
handles=LoadNeuroFile(handles);
guidata(handles.neurosharegui_fig,handles)  
%[nsresult, ContinuousCount, wave] = ns_GetAnalogData(hfile,3,1,5000);

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.neurosharegui_fig)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.neurosharegui_fig,'Name') '?'],...
                     ['Close ' get(handles.neurosharegui_fig,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

%*****************************************************************
%close the file
if isfield(handles,'ns')
    if (handles.ns.hfile>0)
        ns_CloseFile(handles.ns.hfile);
    end
end

clear mexprogdll;
delete(handles.neurosharegui_fig)


% --- Executes on selection change in cbo_entitylist.
function cbo_entitylist_Callback(hObject, eventdata, handles)
% hObject    handle to cbo_entitylist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns cbo_entitylist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cbo_entitylist


% --- Executes during object creation, after setting all properties.
function cbo_entitylist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cbo_entitylist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end




% --- Executes on button press in cmd_update.
function cmd_update_Callback(hObject, eventdata, handles)
% hObject    handle to cmd_update (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%make sure we've loaded results
if isfield(handles,'ns')
    handles=updateData(handles);
    %totaltime = handles.ns.FileInfo.TimeSpan; %seconds
    %[nsresult, ContinuousCount, wave] = ns_GetAnalogData(handles.ns.hfile,get(handles.cbo_entitylist,'value'),1,totaltime);
    %plot the data
    %axes(handles.maxes);
    %plot(wave);
end
guidata(hObject, handles);


% --------------------------------------------------------------------
function mnu_copyws_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_copyws (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Create a variable in the user workspace called data
% which stores whatever data is currently plotted in the axesh

% --------------------------------------------------------------------
function mnu_data_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%make sure we've loaded results
if isfield(handles,'ns')
    totaltime = handles.ns.FileInfo.TimeSpan; %seconds
    index=get(handles.cbo_entitylist,'value');
    eindex=handles.guiData.listIDs(index);
        assignin('base','data',handles.guiData.wave);
end


% --------------------------------------------------------------------
function mnu_copyws_var_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_copyws_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%make sure we've loaded results
if isfield(handles,'ns')
    totaltime = handles.ns.FileInfo.TimeSpan; %seconds
    index=get(handles.cbo_entitylist,'value');
    eindex=handles.guiData.listIDs(index);
    
    %get the name of the variable to store it in
    varname = inputdlg('Name of variable to store data in','Save to Workspace');
    varname{1}=sprintf('%s',varname{1});
    varname{2}=sprintf('t%s',varname{1});
    assignin('base',varname{1},handles.guiData.wave);
    assignin('base',varname{2},handles.guiData.t);
end

