% function y=movavg(x,len)
%    x   - data to filter
%    len - length of filter
%          
% Return
%    y   - the filtered data
% Explanation:
%       applies a moving average filter of length len to the data x
%       result is the same length as x.
%       NOTE: FCN pads end of X with the first and last value of X to
%       prevent boundary effects. Do not change this. fcrossing depends on
%       it
%
%6-06-2005
%   I fixed the alighnment issues for case where filter was of even length.
%   Before this if you used an even filter, the result would not be in the
%   middle.
%2-28-2005
%   fixed the indexing for y=, so that no longer get noninteger indexes
function y=movavg(x,len)
    dim=size(x);
    if (dim(1))>1;
        %its a row vector take transpose
        x=x';
    end
    xfilt=[ones(1,len-1)*x(1) x ones(1,len-1)*x(end)];
    fcoeff=ones(1,len)*1/len;
    xfilt=conv(xfilt,fcoeff);
    %xfilt is padded with ones
    %y=xfilt(1,len+floor(len/2):end-len-floor(len/2)+1); %this works for od
    %d values for len but not even
    if (len/2==floor(len/2))
        %its even
        %code before 6-06-2005
        %y=xfilt(1,len:end-len);
        %after 6-06-2005
        y=xfilt(1,len+floor(len/2)-1:end-len-floor(len/2)); %this works for od
    else
        %its odd
        y=xfilt(1,len+floor(len/2):end-len-floor(len/2)+1); %this works for od
    end