%this function calculates the probability that one rgb cube arose from another
%by calculating the L1 distance between the two cubes

function p=rgbmatchl1(rgbcube1,rgbcube2)

      %calculate how well they match
    %cacluclate the L1 distance
    pprob=abs(rgbcube1-rgbcube2);
    pprob=sum(pprob,1);
    pprob=sum(pprob,2);
    pprob=sum(pprob,3);
    pprob=(2-pprob)/2;
    p=pprob;