%this function takes as input a matrix representing an image
%this matrix has the same structure as the result of imread
% that is 
% image=[Y X R G B]

% this function than builds an rgb cube of the colors in that image


%***********************************IMPORTANT: USE OF IMAGES*********************************
% imread outputs a 3 dimensional array which gives the RGB value of each pixel
% the dimensions of the output are (Y PIXEL, X Pixel, RGB Value)
% NOTE THAT THE Y PIXEL COMES FIRTST *****************
% I keep this format for the output of imread. 
%*********************************************************************************************

function rgbcube=buildrgbcube(rgbdata,numbins)
%*******************************************************************************************************************************************************
%rgbdata=[]                                     %store rgb matrix that represents the image
%numbins                                        % number of bins in each dimension of the RGB histogram describing the image of this part
                                                % this should be a number evenly divides 256 (b\c 256 possible values for each color)
                                                % if it doesn't it will cause problems

%************************************************************************************************************************************************************

% variables describing the data
%recall rgbdata lists y-pixel first
imagewidth = size(rgbdata,2);         %width of image in pixels
imageheight= size(rgbdata,1) ;        %height in pixels
numpixels=imagewidth * imageheight;   %used as the normalization constant for the histogram



% construct a histogram for the rgb data 
                                                
binwidth=256/numbins;                            %256 b\c that is the number of values for an RGB value
rgbcube=zeros(numbins,numbins,numbins,1);        %this stores the histogram
                                                %its a numbins x numbins x numbins x 1 matrix
                                                % column 1 = bin for RED
                                                % column 2 = bin for Green
                                                % column 3 = bin for blue
                                                % column 4 = relative frequency\probability for each bin
                                                

                                                % loop through image and enter pixel colors into histogram
% normalize as you go. Each time a value occurs add 1/numpixels to the bin 
% so that at the end of the loop, rgbcube contains the relative frequencies of each bin, and not the number of occurences
for xpixel=1:imagewidth
    for ypixel=1:imageheight
        % calculate which bin the color of the pixel belongs in
        % do this by dividing by binwidth, adding1 and then taking the floor
        % e.g) if bindwidth=8 then for Red=0. 
        %         rbin= floor (0/8 +1)=1
        % if you don't add one then red=0 would end up in bin 0 which is invalid (since matrix indexes begin at 1
        % I use the double function b\c RGB values are uint values which need to be converted to double before performing mathematical operations
        rbin=floor(double(rgbdata(ypixel,xpixel,1))/binwidth + 1);
        gbin=floor(double(rgbdata(ypixel,xpixel,2))/binwidth + 1);
        bbin=floor(double(rgbdata(ypixel,xpixel,3))/binwidth + 1);
        
        rgbcube(rbin,gbin,bbin,1)= rgbcube(rbin,gbin,bbin,1) + 1/numpixels;
    end
    %output the xpixel as a way of following progress
    %xpixel
end

%as a way of checking our rgbcube, sum all the probabilities and see if its close to 1
%may not be exactly 1 b\c of rounding errors
%histsum=sum(rgbcube,1);
%histsum=sum(histsum,2);
%histsum=sum(histsum,3)

