%function sind=subind(msize)
%   msize - the dimensions of the matrix
%         - leave blank to randomly generate
function sind=testsubind(msize)

success=true;

if ~exist('msize','var')
    %randomly generate matrix with between 1 and 20 entries
    %and 2-10 dimensions
    ndim=ceil(rand(1,1)*9+1);
    msize=ceil(rand(1,ndim)*20);

else
    ndim=size(msize,2);
end

%generate some random linear indexes
nind=ceil(rand(1,1)*prod(msize));
lind=randperm(prod(msize));
lind=lind(1:nind);
lind=sort(lind);
sind=Indexing.subind(msize,lind);


%mat these subindexes back to the linear index

%form the output string to store the results in
cmd='';
for mind=1:ndim
    cmd=sprintf('%ssind(:,%d)',cmd,mind);
    if (mind<ndim)
        cmd=sprintf('%s,',cmd);
    end
end
cmd=sprintf('ltrue=sub2ind(msize,%s);',cmd);
eval(cmd);

if (any(lind(:)~=ltrue(:)))
    error('subind is not correct');
    success=0;
end

fprintf('Success subind is correct.');


