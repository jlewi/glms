%function testlinindexes(nx,ktlength)
%   nx - number of stimuli to use
%   ktlength - number of elements in static distribution
%
%Explanation: This function tests linindexes.
%
function success=testlind(msize)
success=true;

ndim=size(msize,2);



%generate the linear indexes corresponding to all entries
lall=Indexing.lind(msize,nan(1,ndim));

%the true lindexes
if (any(lall~=[1:prod(msize)]'))
    error('When generating lindexes for all elements. The results were not correct');
    success=false;
end

%now generate the indexes corresponding to marginal over only a subset of
%the elements i.e linear indexes corresponding to
%mat(s_1, :,s_2,s_3,:,...)
%   s_1,s_2,s_3, ... - generated randomly
%   columns for which we select all elements are generated randomly as well

sind=ceil(rand(1,size(msize,2)).*msize);

nmarg=ceil(rand(1,1)*(size(msize,2)-1));
indmarg=randperm(size(msize,2));
indmarg=indmarg(1:nmarg);

sind(1,indmarg)=nan;

ltest=Indexing.lind(msize,sind);

%make sure each element is unique
if (length(ltest)~=length(unique(ltest)))
    error('ltest has duplicate entries.');
end

%to check it we use suball to generate a matrix same dimensions as subind
%which stores the linear index at each element
%we then use (s_1,:,s_2,s_3,:,:) notation to select the appropriate entries
%we then check they equal ltest
matind=reshape(1:prod(msize),msize);

istr='';
for i=1:ndim;
    if isnan(sind(i))
        istr=sprintf('%s:',istr);
    else
        istr=sprintf('%s%d',istr,i);
    end
    if (i<ndim)
        istr=sprintf('%s,',istr);
    end
end
cmd=sprintf('ltrue=matind(%s);',istr);
eval(cmd);
if (any(ltest(:)~=ltrue(:)))
    error('Indexing.lind does not appear to be correct.');
    success=false;
end

if (success)
    fprintf('Success. lindexes works.\n');
end

