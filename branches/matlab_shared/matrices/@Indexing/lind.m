%function lind-linindexes(msize,subind)
%   msize  - the dimensions of the matrix 
%   subind - multiple subscripts to identify the entry in p
%           ktlengthx1 vector
%           e.g
%         p(sub2ind(subind))=p(x_1=stim(:,subind(1)),x_2=stim(:,subind(2),.
%         ..);
%          - if an entry is nan then that means find all possible values
%           of that index 
%  Return value
%   lind- row vector of the linear index
%
function l=lind(msize,subind)
    ndim=size(msize,2);
    if ~isvector(subind)
        error('subind should be a vector');
    end

    subind=colvector(subind);

    %find the number of entries we need to "marginalize" over.
    indmarg=find(isnan(subind));
    nmarg=length(indmarg);
    
    if (nmarg==0)
        nlind=1;
    else
        nlind=prod(msize(indmarg));
    end
    l=zeros(nlind,1);
    
    %generate the indexes for the elements we marginialize over
    %we do this by using ind2sub. This makes it easy to generate
    %a list of the indexes;
    
    sind=ones(nlind,1)*subind';
    
    cmd='[';
    for mind=1:length(indmarg)
        cmd=sprintf('%ssind(:,%d)',cmd,indmarg(mind));
        if (mind<length(indmarg))
            cmd=sprintf('%s,',cmd);
        end
    end

    cmd=sprintf('%s]=ind2sub(msize,1:%d);',cmd,prod(msize(indmarg)));
    eval(cmd);
    
    str='';
    for i=1:ndim
        str=sprintf('%s,sind(:,%d)',str,i);
    end
    cmd=sprintf('l=sub2ind(msize%s);',str);
    eval(cmd);
    
    
    
    
    