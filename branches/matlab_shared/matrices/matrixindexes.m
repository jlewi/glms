%function mind=matrixindexes(dims);
%   dims - vector of size d
%       d - dimensions of the matrix to construct
%         - ith element is size of ith element
%
%  Return Value:
%       mind - matrix with dimensions, sizes
%           [dims, d]
%           i.e mind = zeros([dims d];
%       mind contains d+1 size d matrices.
%       Each of these d matrices gives the index of that point along
%       dimension j.
%       i.e mind(:,:,:,:,j)
%       gives a matrix with dimensions d
%       The value of this matrix is the index at each entry along the jth
%       dimension.
%       Example: dims=[2 2]
%       Then mind is [2 2 2]
%       mind(:,:,1)=[1 1; 2 2];
%       mind(:,:,2)=[1 2; 1 2];
%
% To turn this into a matrix of points where each row represents the
% indexes at that point
%      
%This is very inefficent
%A faster way to do this is to use the ind2sub function
%
function mind=matrixindexes(dims);
    Warning('01-02-2009. The Indexing class should provide better functionaltiy.');
    
    numdims=length(dims);
    
    %declare the matrix
    mind=zeros([dims length(dims)]);
    
 for dindex=1:numdims
     
    for indval=1:dims(dindex)
        %we construct a string to represent the dimensions over which we
        %want to look at all k
        %so if its 2x2 matrix
        %we are going to construct x=[1 2; 1 2], y=[1 1; 2 2]
        %To construct this type of matrix I use eval and construct the
        %command as a string because I want to take all elements from all
        %but one dimension so I want to use the x(:) notation but I want
        %the number of dimensions to be a variable
        cmd=strcat('mind(',strrepeat(':,',dindex-1),num2str(indval),strrepeat(',:',(numdims-dindex)),',',num2str('dindex'),')=',num2str(indval),';');
        eval(cmd);
    end
    
 end
 