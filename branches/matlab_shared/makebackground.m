%function background=makebackground(images)
%       images -imagesequence object containing images
%construct an image representing background by taking median of every pixel for series of images
%
%
function background=makebackground(images)
    numimages=get(images,'numframes');
    %create a cell arrays to store the images
    %3 arrays each of which stores 1 of the 3 color channels for all the images
    width=get(images,'width');
    height=get(images,'height');
    rimages=zeros(height,width,numimages);
    gimages=zeros(height,width,numimages);
    bimages=zeros(height,width,numimages);

    counter=1;
    for index=1:numimages
        img=getImage(images);
        
        rimages(:,:,index)=img(:,:,1);
        gimages(:,:,index)=img(:,:,2);
        bimages(:,:,index)=img(:,:,3);
        
        [images val]=next(images);
        if val <0
            break;
        end
        counter=counter+1;
    end
    
    %take median of each channel at each point
    rimages=rimages(:,:,1:counter);
    gimages=gimages(:,:,1:counter);
    bimages=bimages(:,:,1:counter);
    background=zeros(height,width,3);
    background(:,:,1)=median(rimages,3);
    background(:,:,2)=median(gimages,3);
    background(:,:,3)=median(bimages,3);