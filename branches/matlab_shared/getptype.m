% function ptype-getptype(index,lines)
%       index - index into plot type array
%       lines - 2 - means the graph marker will be a line
%                - if not supplied will return a color, marker style, and
%                linestyle
% Explanation - Returns a combination of color and plot marker
%   useful for plotting a bunch of curves using different
%   color traces/markers
%
% if two output arguments are supplied color and marker are specified
% separately
function [varargout]=getptype(index,lines);
if (~exist('lines','var'))
    lines=3;
end

if ischar(lines)
    lines=str2num(lines);
end

%possible combos
ptypes={'bx','b-';'go','g-';'r+','r-';'c*','c-';'ms','m-';'k.','k-';'bd','b-';'gd','g-';'rv','r-';'c^','c-';'m<','m-';'k>','k-';'bp','b-';'gh','g-';};


%number of compbos
numtypes=size(ptypes,1);

w=mod(index,numtypes);
if (w==0)
    w=numtypes;
end
if (lines<3)
ptype=ptypes{w,lines};
else
    ptype=[ptypes{w} '-'];
end

if (nargout==1 | nargout==0)
    varargout{1}=ptype;
else
    varargout{1}=ptype(1);
    varargout{2}=ptype(2);
end