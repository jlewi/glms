%function apath=abspath(p)
%   p - should just be the path no filename included
% Explanation converts '..' in a path to an absolute path
% also converts ~ to home dir
%
% $Revision$ Handle case where path starts with '..'
% $Revision$: If path starts with '.' replace with pwd
%
% Author: Jeremy Lewi
% Date: 08-16-2007
% email: jlewi@gatech.edu
%
function apath=abspath(p)

 %search for the tilde character
 if p(1)=='~'
     p=[gethomedir() p(2:end)];
 end
 if (p(1)=='.' & p(2)~='.')
     p=[pwd p(2:end)];
 elseif(p(1:2)=='..')
     p=[pwd filesep p];
 end
sepind=strfind(p,filesep);

%unpack the path
pdirs={};
for ind=1:(length(sepind)-1)
    pdirs{ind}=p((sepind(ind)+1):(sepind(ind+1)-1));
end
if ~isempty(sepind)
if (sepind(end)<length(p))
    pdirs{end+1}=p((sepind(end)+1):end);
end
end
%now create an array of directories for the absolute path
adirs={};
anum=0;
for k=1:length(pdirs)
    if ~(strcmp(pdirs{k},'..'))
       
        anum=anum+1;
        adirs{anum}=pdirs{k};
    elseif(strcmp(pdirs{k},'.'));
        %do nothing
    else
        %path is '..'
        %go up to the parent
        adirs{anum}=[];
        anum=anum-1;
    end
end

%assemble the path
%Hack for windows assume its "c drive"
if (filesep=='\')
    apath=['C:' filesep adirs{1}];    
else
    apath=[filesep adirs{1}];
end
for k=2:anum
    apath=[apath filesep adirs{k}];
end

