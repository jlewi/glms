%function [dind]=findsetinfile(msefile,dset)
%   dset    - The dataset we want to find in the file
%
%   msefile - The file containing the mse data
%           -(optional) defaults to output of msefname for this function
%Return value:
%   dind - The index into data and dset in msefile of the data set matching
%          dset if it exists
%           otherwise nan if its not in the file
%   dset - the value of dset
%
%
%
function [dind,msefile]=findsetinfile(dnew,msefile)

if ~exist('msefile','var')
    msefile=[];
end

if isempty(msefile)
    msefile=BSCompareMSE.msefname(dnew.datafile);
end

dind=nan;
dset=struct();

v=load(getpath(msefile),'dsets');
alldsets=v.dsets;

%check if the dataset is already in the file


rind=strmatch(dnew.setupfile,{alldsets.setupfile});


if ~isempty(rind)
    %we found the setupfile but make sure use post is the same
    %there could be more than one match

    for mind=rind';
        %use isequal bcase the value might be the same ore they
        %might both be empty
        %this assumes there is at most one match in the file
        if isequal(dnew.usetanpost,alldsets(mind).usetanpost)
            dind=mind;
            break;
        end
    end
end

% if ~isnan(dind)
%    %copy the lbl and explain fields from alldsets
% 	fnames={'lbl','explain'};
%     for f=fnames
%        if isfield(alldsets(dind),f)
%           dnew.(f)=alldsets(dind).(f);
%        end
%     end
% end
