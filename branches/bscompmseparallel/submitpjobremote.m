%function submitpjobremote(dset)
%   dset - dataset to process
%       .datafile
%       .mfile
%       .cfile
%
%Explanation: This function submits a parallel job remotely to the cluster
%   which computes the mse for all the trials in the file
function submitpjobremote(dset)


%**************************************************************************
%parameters
%
%The values below should be customized for your setup
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/svn_glms/adaptive_sampling';

%whether or not to capture command window output
capcmdout=false;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=true;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
svndirs={'~/svn_glms'};
svndirs={pwd,MATLABPATH};


dset.msefile=BSCompare.msefname(dset.datafile);
%an array of FilePath objects the input files
fnames=rowvector(fnames(dset));
for f=fnames
    if isa(dset.(f),'FilePath')
        filesin={filesin, dset.(f)};
    end
end

%an array of FilePath objects specfying the output files
filesout={dset.msefile};

%name to use for the job
jname='mse';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='/home/jlewi/svn_glms/allresults';

%The directory on your local computer where jobs should be stored
localjobdir='~/jobs';

%directory on the cluster where jobs should be stored
%This path should be accessible from the head nodes and worker nodes
clusterjobdir='/Users/jlewi/jobsremote';

clusterhost='cluster.neuro.gatech.edu';

%if there's some code you want to execute before starting
%the function (i.e setting the path) you can specify a handle to this
%function
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=taskstartupfunc;

%max number of nodes to use 
nworkers=28;

%**************************************************************************
%Setup the scheduler
sched=distsched(localjobdir);


%we have to modify the scheduler object's submit fcn property
%because we have to specify the directory on the cluster where 
%the data is stored
set(sched,'submitFcn',{@sgeremoteSubmitFcn,clusterhost,clusterjobdir})

%**************************************************************************
%create the parallel job
%************************************************************************
[jobdata]=initjobdata(jname,filesin,filesout,startdir,startupfunc,localhost,localdatadir);
pjob=createParallelJob(sched);
pjob.jobData=jobdata;

set(pjob,'MaximumNumberOfWorkers',1);
set(pjob,'MinimumNumberOfWorkers',nworkers);


%********************************************************************************************
%create the task
%*************************************************************************************************
nout=0;

ptask=createTask(pjob,@BSCompareMSE.processdatasetinparallel,nout,{'Throw an error in main code'});

set(ptask,'CaptureCommandWindowOutput', capcmdoutput);
jobdata.jobname=jname;
set(pjob,'JobData',jobdata);

%submit the job
submit(pjob);
