%function [dset,data]=compmse(dset,data,trials)
%   dset    - structure describing this dataset
%   data    - structure containing the data already calculated
%   trials  - which trials to compute the mse on
%           - Leave trials empty to do all trials for which we have the
%            covariance matrix.
%
%
%Return value
%   data
%      .mse - structue array
%           .trial - trial on which we computed the mse
%           .wind  - the index into wavinfo of the wave file for which this the
%                 mse
%           .simind - index into dsets of the simulation for which this is the
%                 mse
%
%Explanation:
% This script looks at a bunch of simulations in which we did not train
% on at least some of the wavefiles. We use those wavefiles as a test set
% and compute the MSE. For each file in the test set, we plot the MSE of
% the fitted model after different numbers of trials. Each curve
% corresponds to a different design.
%
%Revisions:
%   09-22-2008 - Turned it into a function
%              - Allow us to compute the MSE with the posterior projected
%              on the tangent space even for models in which the tangent
%              space wasn't used
%06-03-2008
%
%Cross validation
%
%
% 7-29-2008
%   This script started causing a segmentation fault unless you pause
%   execution
%   by putting a breakpoint before we make the plots
function [dset,data]=compmse(dset,data,trials)

bssim=[];

if isempty(data.wavinfo)
    v=load(getpath(dset.datafile));
    bssim=v.bssimobj;
    data.wavinfo=BSCompareMSE.gettestset(bssim);
end

if (~exist('trials','var') || isempty(trials))
   %set trials to all trials for which we have the covariance matrix 
    v=load(getpath(dset.datafile));
    bssim=v.bssimobj;
    
    trials=readmatids(bssim.allpost.cinfo.caccess);
end
%get the wavefiles in the test set if we haven't already processed them
testwind=rowvector([data.wavinfo.wind]);
trials=BSCompareMSE.trialsleft(data.mse,trials);


%%
%**************************************************************************
%Process the trials
%**************************************************************************
if ~(isempty(trials))

    if (isempty(bssim))
        %haven't loadded bssim yet
        v=load(getpath(dset.datafile));
        bssim=v.bssimobj;
    end

    bdata=bssim.stimobj.bdata;
    mobj=bssim.mobj;

    
    %            dset.lbl=bssim.label;
    dset.niter=bssim.niter;

    ntest=length(data.mse);

    %remove any trials for which we didn't store the covariance matrix
    %terminate if we exceed the number of trials
     %
        trials=trials(trials<=bssim.niter);
        
    %loop over all the trials we do the cross validation for
    %we use a drange loop so that when running on a cluster we do the
    %computation in parallel
    trials=rowvector(trials);
    
    %create a distributed cell array to store the results
    %Each row is the mse on a different test file
    %each column is a different trial. Thus, we distribute this matrix
    %along the 2nd dimension
    results = cell(length(testwind), length(trials), distributor('1d',2));
    
    %since we are using a parfor loop we need to make sure GaussPostFile
    %doesn't create distributed matrices as that will violate the for loop
    %rules
    setdistributed(bssim.allpost,false);
    for tind=drange(1:length(trials));
        trial=trials(tind);
        fprintf('trial:%d \n', trial);
     
        theta=getm(bssim.allpost,trial);
        [strf, shistcoeff, bias]=parsetheta(bssim.mobj,theta);

        strf=reshape(strf,getstrfdim(bdata));

        %make sure the covariance matrix was saved
        post=getpost(bssim.allpost,trial);


        if isempty(getc(post))
            fprintf('Skipping trial %d for simulation %d label=%s. Covariance matrix was not saved \n',trial,sind,bssim.label);
        else

            if (dset.usetanpost)
                if isa(bssim.mobj,'MTanSpace')
             
                    mobj=bssim.mobj;
                else
                    %construct a tan space object;
                    params.klength=bssim.mobj.klength;
                    params.ktlength=bssim.mobj.ktlength;
                    params.alength=bssim.mobj.alength;
                    params.rank=dset.taninfo.rank;
                    params.glm=bssim.mobj.glm;
                    params.hasbias=bssim.mobj.hasbias;
                    params.mmag=bssim.mobj.mmag;
                    eval(sprintf('mobj=%s(params);',dset.taninfo.class));

                end
                pb=PostTanSpace(post,mobj);


                minfo=pb.fullpost.m+pb.tanpoint.basis*pb.tanpost.m;
                cinfo=pb.tanpoint.basis*pb.tanpost.c*pb.tanpoint.basis';
                %compute eigendecomp of cinfo because we will need it for the stimulus
                %optimization.
                post=GaussPost('m',minfo,'c',cinfo);



            end
            for wind=testwind
                results(wind,tind)=struct([]);
                ntest=ntest+1;

                %        testr(ntest).fmapind=fmapind;
                results(wind,tind).wavefile=wind;

                 results(wind,tind).trial=trial;

                [ results(wind,tind).mse  results(wind,tind).exprate  results(wind,tind).emprate]=bspsthexpmse(bssim,post,data.mse(ntest).wavefile);


            end
        end

    end

    %now we need to gather the results
    results=gather(results);
    %convert to a structure array
    results=[results{:}];
    
    data.mse=[data.mse results];
end
