%function [dset,data]=compmseinparallel(dset,data,trials)
%   dset    - structure describing this dataset
%   data    - structure containing the data already calculated
%           .wavinfo - the wave files in the test
%           .mse     - the mse already computy
%   trials  - which trials to compute the mse on
%           - Leave trials empty to do all trials for which we have the
%            covariance matrix.
%
%
%Return value
%   mse - structue array
%       .trial - trial on which we computed the mse
%       .wind  - the index into wavinfo of the wave file for which this the
%                 mse
%       .simind - index into dsets of the simulation for which this is the
%                 mse
%
%Explanation:
% This script looks at a bunch of simulations in which we did not train
% on at least some of the wavefiles. We use those wavefiles as a test set
% and compute the MSE. For each file in the test set, we plot the MSE of
% the fitted model after different numbers of trials. Each curve
% corresponds to a different design.
%
%
%   by putting a breakpoint before we make the plots
function [dset,data]=compmseinparallel(dset,data,trials)

bssim=[];

if isempty(data.wavinfo)
    v=load(getpath(dset.datafile));
    bssim=v.bssimobj;
    data.wavinfo=BSCompareMSE.gettestset(bssim);
end

if (~exist('trials','var') || isempty(trials))
   %set trials to all trials for which we have the covariance matrix 
    v=load(getpath(dset.datafile));
    bssim=v.bssimobj;
    
    trials=readmatids(bssim.allpost.cinfo.caccess);
end
%get the wavefiles in the test set if we haven't already processed them
testwind=rowvector([data.wavinfo.wind]);
trials=BSCompareMSE.trialsleft(data.mse,trials);


%%
%**************************************************************************
%Process the trials
%**************************************************************************
if ~(isempty(trials))
    %each node processes a subset of the different trials

end
