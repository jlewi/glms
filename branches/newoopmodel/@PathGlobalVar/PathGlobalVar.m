%function sim=PathGlobalVar(vname)
%   vname - name of the global variable representing the path
function obj=PathGlobalVar(varargin)



%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%gvar      - the name of the global variable representing the path
%declare the structure
obj=struct('version',080530,'gvar','');


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'PathGlobalVar',FPathBaseCMD());
        return    
end


obj.gvar=varargin{1};


        obj=class(obj,'PathGlobalVar',FPathBaseCMD());

    