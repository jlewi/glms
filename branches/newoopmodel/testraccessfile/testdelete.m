%function testdelete(dim,nmat)
%
%Explanation: This function tests deleting a couple of matrices and then
%overwriting them
%
%%
function testdelete(dim,nmat)
clear mat;
fprintf('Testing ability to delete matrices \n');

%***********************************************
%create the object
%**************************************************
global TMPDIR;
TMPDIR='/tmp/';
fpath=FilePath('bcmd','TMPDIR','rpath','raccessdata.mat');
fpath=seqfname(fpath);
fpath=labBroadcast(1,fpath);

%*************************************************
%create an robject with the fake data
%************************************************
odata=testraccessgendata(dim,nmat);

robj=RAccessFile('fname',fpath,'dim',dim);

%write the data
for j=1:nmat
writematrix(robj,odata.mat,id, overwrite,recindex);

end

%randomly delete 20% of the matrices
%but delete at least 1
ntodelete=max(1,floor(.2*data.nmat));

%randomly generate the ids of the matrices to delete
did=unique(ceil(rand(1,ntodelete)*data.nmat));
did=labBroadcast(1,did);
ntodelete=length(did);

%delete the matrices
for j=1:ntodelete
    robj=deletematrix(robj,did(j));
end

%create the new data structure which reflects the deleted matrices
keepm=ones(1,data.nmat);
keepm(did)=0;
keepind=find(keepm==1);
clear data;
data.mat=odata.mat(keepind);
data.ids=odata.ids(keepind);
data.nmat=length(data.ids);

%verify the header is correct
header=getheader(robj);

if (header.nmat ~=data.nmat)
    error('Number of matrices is not correct after delete operation');
end

%number of records should be the same as the orgiinal number of records
if (header.nrec ~=odata.nmat)
    error('Number of records is not correct after delete operation');
end

%call readdata to see if we return the correct matrices


[dinfile.mat,dinfile.ids]=readdata(robj);

if ~verifydata(data,dinfile)
    error('readdata.m did not return the correct data after deleting a matrix.');
end
fprintf('readdata.m works after deleting matrices \n\n');

%verify the last id in the file
verifylastid(robj,dinfile.ids(end));

%*****************************************************************
%Test writing a new matrix:
%Check that this matrix ends up overwriting one of the deleted matrices
%***********************************************************************
%to verify the header we keep track of what the header should be
htrue=getheader(robj);


%make sure there are deleted records
if (htrue.nmat==htrue.nrec)
    error('We cannot perform the next test because there arent any deleted matrices in the file.');
end

%generate a random matrix
mat=randn(htrue.dim(1),htrue.dim(2));
mid=max(data.ids)+1;
mat=labBroadcast(1,mat);
mid=labBroadcast(1,mid);

data.mat{end+1}=mat;
data.ids(end+1)=mid;
data.nmat=data.nmat+1;

%write the matrix
htrue.nmat=htrue.nmat+1;
writematrix(robj,mat,mid,1);

fprintf('Test writematrix.m overwrites deleted matrices when writing a new matrix. \n');

header=getheader(robj);
fprintf('nmat=%d\n',header.nmat);

%verify the header
%fprintf('File: %s \n',getpath(getfname(robj)));

labBarrier;
if ~(verifyheader(robj,htrue))
    error('Header is inaccurate after overwriting deleted matrix.');
end



[dinfile.mat, dinfile.ids]=readdata(robj);

%verify the last id is correct before sorting the ids
%veryify lastid is correct
if (dinfile.ids(end)~=getlastid(robj))
    error('getlastid did not return the correct id');
end

%sort the data by the id's to facilitate the comparison
[dinfile.ids sindex]=sort(dinfile.ids);
dinfile.mat=dinfile.mat(sindex);

[data.ids sindex]=sort(data.ids);
data.mat=data.mat(sindex);


if ~(verifydata(data,dinfile))
    error('data is inaccurate after overwriting deleted matrix.');
end

%**************************************************************************
%Write a bunch of new matrices. We want to write enough matrices so that
%we overwrite all of the deleted matrices and still have to write some
%matrices at the end of the file
%
%This ensures writedata correctly handles deleted matrices
%**********************************************************************
%to verify the header we keep track of what the header should be
htrue=getheader(robj);


%make sure there are deleted records
if (htrue.nmat==htrue.nrec)
    error('We cannot perform the next test because there arent any deleted matrices in the file.');
end

%generate random matrices
ntoadd=((htrue.nrec-htrue.nmat)*2);
for j=1:ntoadd
    mat=randn(htrue.dim(1),htrue.dim(2));
    mid=max(data.ids)+1;
    mat=labBroadcast(1,mat);
    mid=labBroadcast(1,mid);

    data.mat{end+1}=mat;
    data.ids(end+1)=mid;
    data.nmat=data.nmat+1;
end
%write the matrix
htrue.nmat=htrue.nmat+ntoadd;
htrue.nrec=htrue.nrec+ntoadd/2;

writedata(robj,data.mat(end-ntoadd+1:end),data.ids(end-ntoadd+1:end),1);

fprintf('Test writedata.m overwrites deleted matrices when writing a new matrix. \n');

header=getheader(robj);

%verify the header
if ~(verifyheader(robj,htrue))
    error('Header is inaccurate after overwriting deleted matrix using writedata.');
end



[dinfile.mat, dinfile.ids]=readdata(robj);

%sort the data by the id's to facilitate the comparison
[dinfile.ids sindex]=sort(dinfile.ids);
dinfile.mat=dinfile.mat(sindex);

[data.ids sindex]=sort(data.ids);
data.mat=data.mat(sindex);


if ~(verifydata(data,dinfile))
    error('data is inaccurate after overwriting deleted matrix using writedata.m.');
end

clear ntokeep;
clear did;





