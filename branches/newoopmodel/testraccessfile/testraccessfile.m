%function testraccessfile(dim,nmat)
%   dim - dimensions of the matrices
%   nmat - number of matrices
%
%Test harness for RAAcess file
%
%This tests reading, writing, appending, matrices
%Also tests reading a submatrix
%
%Parallelization: to test the functionality in parallel. start an
%interactive parallel session (pmode) and then call testraccessfile.
function [robj]=testraccessfile(dim,nmat)

if ~exist('dim','var')
    dim=[];
end
if ~exist('nmat','var')
    nmat=[];
end
%setpathvars;

%***********************************************************************
%Test parameters
%***********************************************************************
%generate some data
params=testraccessgendata(dim,nmat);

%*******************************************************************
%Test: Creation of the file
%********************************************************************

global TMPDIR;
TMPDIR='/tmp/';
fpath=FilePath('bcmd','TMPDIR','rpath','raccessdata.mat');
fpath=seqfname(fpath);
fpath=labBroadcast(1,fpath);
params.fpath=fpath;

%*************************************************************
%Read and Write a Single matrix
%***************************************************************
%create the object

testreadwritesingle(params.dim,params.fpath);
%*****************************************************************
%Read and write a whole bunch of NEW matrices
%******************************************************************
%use a different file
%
[fpath,data]=testreadwritemany(params.dim,params.nmat,params.fpath);



%*****************************************************
%clear the object and recreate it
%
%Test: changing some data and appending new data
%*****************************************************
clear robj;
clear mat;
odata=data;
robj=RAccessFile('fname',fpath,'debug',true);
mat=readdata(robj);

for j=1:data.nmat
    if any(any(mat{j}~=data.mat{j}))
        error('Data read does not match. After reloading the file');
    end
end

%randomly change some of the data and append some new data
index=ceil(rand(1,10)*data.nmat);
index=unique(index);

ntochange=labbroadcast(1,length(index));
for j=1:ntochange

    itochange=nan;
    if (labindex==1)
        itochange=index(j);
        data.mat{itochange}= randn(data.dim);
    end

    itochange=labbroadcast(1,itochange);
    data.mat{itochange}=labbroadcast(1,data.mat{itochange});


    %write the data
    robj=writedata(robj,data.mat{itochange},itochange);
end

mat=readdata(robj);
fprintf('Data.nmat=%d \n',data.nmat);

for j=1:data.nmat
    %    labBarrier;
    %    fprintf('Checking matrix %d \n',j);
    if any(any(mat{j}~=data.mat{j}))
        error('Data read does not match. After modifying the data.');
    end

end

%append some new data
nnew=10;
for j=data.nmat+1:data.nmat+10

    data.mat{j}=randn(data.dim);
    data.mat{j}=labBroadcast(1,data.mat{j});
    data.ids(j)=j;
    %write the data
    robj=writedata(robj,data.mat{j},j);
end

data.nmat=data.nmat+10;

%check the header updated the number correctly
header=getheader(robj);
if (header.nmat ~= data.nmat)
    error('number of matrices not correct ofafter adding 10');
end

%close the file
clear robj;
clear mat;

%reopen the file and chekck data matches.
robj=RAccessFile('fname',fpath);
mat=readdata(robj);

for j=1:data.nmat
    if any(any(mat{j}~=data.mat{j}))
        error('Data read does not match. After reloading the file after modifying and appending to the data.');
    end
end

%********************************************************************
%Test reading a submatrix
%**********************************************************************
%%
clear mat;

ntrials=10;
for trial=1:ntrials
    %randmoly generate the matrix to read and the rows,cols to read
    mid=ceil(rand(1)*data.nmat);
    rind=sort(ceil(rand(1,2)*data.dim(1)));
    cind=sort(ceil(rand(1,2)*data.dim(2)));

    mid=labBroadcast(1,mid);
    rind=labBroadcast(1,rind);
    cind=labBroadcast(1,cind);

    subm=readsubmatrix(robj,mid,rind,cind);

    truedata=data.mat{mid}(rind(1):rind(2),cind(1):cind(2));
    if any(any(subm~=truedata))
        error('Read submatrix did not return the correct matrix.');
    end
 
end
clear truedata;
%********************************************************************
%Test writing a submatrix
%**********************************************************************
%%
[robj,data]=testwritesubmatrix(robj,data);


testdelete(robj,data);

testloadsave(robj);
