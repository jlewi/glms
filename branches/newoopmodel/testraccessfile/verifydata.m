%**************************************************************************
%function verifydata(data, dinfile)
%   data    - the data that should be in the file
%       .mat - cell array of matrices
%       .ids - ids associated with matrices
%   dinfile - data that was in the file
%       .mat
%       .ids
% return value
%   true - data matches
%   false - data does not match
%Explanation: checks if the data matches returns tru
function vm=verifydata(data,dinfile)

vm=true;
if (length(data.mat)~=length(dinfile.mat))
    fprintf('Error: data has wrong number of matrices. \n');
    vm=false;
end

if (length(dinfile.mat)~=length(dinfile.ids))
    vm=false;
    fprintf('Error: number of ids does not match number of matrices. \n');
end

nmat=length(data.mat);
for j=1:nmat
    if (data.ids(j)~=dinfile.ids(j))
        fprintf('Error: ID of %d matrix is invalid. \n',j);
        vm=false;
    end
    if any(any(data.mat{j}~=dinfile.mat{j}))
        fprintf('Error: %d matrix is not correct. \n',j);
        vm=false;
    end
end

