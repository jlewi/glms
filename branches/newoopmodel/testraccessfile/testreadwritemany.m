%**************************************************************************
%function testreadwritemany(dim,nmat)
%   dim - dimensionality to use
%   nmat - number of matrices
%   fpath - base filename
%Explanation: This function tests the ability to read and write multiple
%matrices using the readdata and writedata functions
function [fpath, data]=testreadwritemany(dim,nmat,fpath)
fpath=seqfname(fpath);
fpath=labBroadcast(1,fpath);
robj=RAccessFile('fname',fpath,'dim',dim,'debug',true);

%generate some data
data.dim=dim;
data.nmat=nmat;
if labindex==1

    data.ids=[1:data.nmat];

    for j=1:data.nmat
        data.mat{j}=randn(dim);
    end

else
    data=[];
end

%send the data to all labs
data=labBroadcast(1,data);

%write the data
robj=writedata(robj,data.mat,data.ids);

%read the data check it matches
[dinfile.mat dinfile.ids]=readdata(robj);

htrue=[];
htrue.nmat=length(data.mat);
htrue.nrec=length(data.mat);
htrue.dim=data.dim;


%verify the header
hinfile=getheader(robj);
htrue.fver=hinfile.fver;

vm=verifyheader(robj,htrue);

vm=verifydata(data,dinfile);

%verify the last id in the file
verifylastid(robj,data.ids(end));



