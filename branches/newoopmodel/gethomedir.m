%function gethomedir
%   
% d- the home directory
%
%
function [d]=gethomedir()

%save the current directory
currdir=pwd;

%change to the home director
cd('~');

d=pwd;

%change back to the directory
cd(currdir);