%function sim=PathABS(path)
%   path - a string for the path to represent
%
% Explanation: An FPathBaseCMD object where we actually specify the path
%   of the target.
%   This is useful when accessing files on remote machines.

function obj=PathABS(varargin)


%declare the structure
obj=struct('version',080530,'pname',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'PathABS',FPathBaseCMD());
        return    
end

obj.pname=varargin{1};
obj=class(obj,'PathABS',FPathBaseCMD());
