%function ecode=recmkdir(p,user)
%   p - path to create
%
%Return value
%   ecode = 0 - success
%         =other value problem occured either with mkdir or chmod
%Explanation: Creates a new directory. New directory can be new at muliple
%   levels
%
%   05-29-2008: Make the directory world readable and writeable
%       This is a hack to get around the fact that on the cluster
%       when running a parallel job. Matlab is run as root
%       So all read and write operations in matlab will be using root
%       user
%       but when we rsync the files we need to sudo to user jlewi so we
%       can access the client
function ecode=recmkdir(p,user)

ecode=0;

%convert p to an absolute path
p=abspath(p);

if (p(end)~=filesep)
    p=[p filesep];
end

sind=strfind(p,filesep);

plevels=cell(1,length(sind)-1);


%parse the path using the directory separation character
for j=1:(length(sind)-1)
    plevels{j}=p(sind(j)+1:sind(j+1)-1);
    
end

%recursively create the directory
cpath=filesep;

for j=1:(length(sind)-1)
    cpath=fullfile(cpath,plevels{j});
    if ~exist(cpath,'dir')
        status=mkdir(cpath);
        
        if (status~=1)
            ecode=1;
        end
        %change the permisions to be world writeable
        %this is a hack for dealing with issues of parallel jobs on the
        %cluster
        [status]=system(['chmod -R a+rwx ' cpath]);
        if (status~=0);
            ecode=1;
        end
    end
end
