%function lastid=getlastid(obj)
%	 obj=RAccessFile object
% 
%Return value: 
%	 lastid=obj.lastid 
%       Positive integer - the id of the last record which is not deleted
%       nan - there are no records in the file or all records have been
%       deleted
function lastid=getlastid(obj)
    head=getheader(obj);
    
    recindex=head.nrec;
    while ((recindex>0) &&isdeleted(obj,recindex));
	recindex=recindex-1;
    end
    if (recindex>0)
       lastid=idofrec(obj,recindex); 
    else
        lastid=nan;
    end
