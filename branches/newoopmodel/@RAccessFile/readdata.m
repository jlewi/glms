%function [mat,matind]=readdata(obj,ind)
%       trials- id's of the matrices to read
%                empty to read all the matrixes
%               otherwise an array of indexes of matrices to read
%               if matrix isn't stored in the file it returns empty
%               matrices
%               The id should be a unique number associated with each trial
%
% Return value
%       mat - cell array of read matrices
%       matind- indexes/ids associated with the matrices
function [mat,matid]=readdata(obj,trials)

    %create the filename
    fullf=getpath(obj.fname);
    
    if ~exist(fullf,'file')
        error(sprintf('%s doesnt exist \n',fullf));
    end
    if ~exist('trials','var')
        trials=[];
    end
    %read the header so we know how many trials there are
    head=getheader(obj);


%     
    if (isempty(trials) && numlabs==1)
        %***********************************************************
        %read all the data - single processor environment
        %***********************************************************        
        %We use a separate read command because i think its faster to issue
        %1 read command than to do multiple fseek,fread pairs
        %open the file
        %
        %we use readmatrix in parallel environments because it will create
        %distributed matrices. 
        
        fid=fopen(fullf,'r');
        %goto position of first trial
        fseek(fid, offset(obj,1,head.dim), 'bof');

        %read all the data
        %each column of the data matrix stores the index associated with that
        %matrix and then the data in that matrix
        data=zeros(prod(head.dim)+1,head.nrec);
        data(:) = fread(fid, head.nrec*(prod(head.dim)+1), 'double');
        fclose(fid);

        %matrix indexes are first row of data
        matid=data(1,:);
        %now we loop through each column and reshape it
        mat=cell(1,head.nrec);
        for k=1:head.nrec
            mat{k}=reshape(data(2:end,k),head.dim(1),head.dim(2));
        end
      
        %only return those matrices which haven't been deleted
        did=isdeleted(obj,[1:head.nrec]);
        ind=find(did==false);
        matid=matid(ind);
        mat=mat(ind);
        
    else
         if isempty(trials)
             %set the trials to the ids of any non deleted records
             ids=getall(obj.finfo.idsptr.ids);
        trials=ids(~isdeleted(obj,1:head.nrec));
         end
  %       if (obj.debug)
 %           fprintf('readdata.m 62: length(trials)=%d\n',    length(trials));
%         end
         
        matid=zeros(1,length(trials));
        %now we loop through each column and reshape it
        mat=cell(1,length(trials));
   
        %only read the matrices associated with the ids specified
        %use posofid to convert the id of the matrix into a record index
        %which is the # of the record in the file associated with this
        %matrix        
        %loop through the trials to load
        for k=1:length(trials)
            %read each matrix        
            %if (obj.debug)
            %    fprintf('Readddata.m reading matrix %d\n', trials(k));
            %end
            [mat{k},matid(k)]=readmatrix(obj,trials(k));
        end
    end
    
    if (obj.debug)
%        fprintf('Readdata.m done reading matrices.\n');
       validate(obj); 
 %      fprintf('Readdata.m finished validating object \n');
    end