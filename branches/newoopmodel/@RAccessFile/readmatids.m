%function [ids]=readmatids(obj)
%       ids - ids associated with matrixes stored in the file
%
%Explanation: reads the ids associated with the record ins the file
function ids=readmatids(obj)

%create the filename
fullf=getpath(obj.fname);

if ~exist(fullf,'file')
    error(sprintf('%s doesnt exist \n',fullf));
end

head=getheader(obj);
%open the file
fid=fopen(fullf,'r');
%goto position of first trial
fseek(fid, offset(obj,1,head.dim), 'bof');


%read just the indexes of the matrixes stored in the file
%but read all of them
skip=prod(head.dim)*obj.fwidth;
ids = fread(fid, inf, 'double',skip);
fclose(fid);