%function deletematrix(obj,id)
%   id - id of the matrix to delete
%
%Explanation: To delete a matrix in the file we set its id
%   to a negative number. This way subsequent calls to writematrix will
%   cause the record in the file to be overwrriten when we write a new
%   matrix to the file.
function obj=deletematrix(obj,id)

recnum=posofid(obj,id);

%id to assighn a deleted record
did=-1;

if isnan(recnum)
    error('cannot delete id=%d it is not in the file',id);
end

    head=getheader(obj);
    
if (labindex==1)

    fid=fopen(getpath(obj.fname),'r+');

    %seek to the start of this
    %matrix
    %start of the record
    recstart=offset(obj,recnum,head.dim);
    fseek(fid,recstart,'bof');

    fwrite(fid,did,'double');


    %update the header
    head.nmat=head.nmat-1;
    writeheader(obj,fid,head);
    fclose(fid);
else
    
    head.nmat=head.nmat-1;
    %still update the header.
    writeheader(obj,[],head);
end
%update the deleted ptrs on all labs
obj.finfo.idsptr.ids=setat(obj.finfo.idsptr.ids,recnum,did);
obj.finfo.deletedptr.recnums=[obj.finfo.deletedptr.recnums recnum];

labBarrier;