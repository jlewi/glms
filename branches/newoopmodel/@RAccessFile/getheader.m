%Function [head,obj]=getheader(fobj)
%   obj - a CFile2d object
%
%Return value:
%   header - header for the file
%       header.nrec = number of records in the file
%       header.nmat = number of matrices stored in the file
%       header.dim     = dimensionality [#rows, #cols];
%       header.fver     = file format version
%get the header for the file
%
%WARNING: in order for the header to be saved af
function [header,obj]=getheader(obj)

%check if we've already read the header
if ~isempty(obj.finfo.headptr.head)
    header=obj.finfo.headptr.head;
else
fname=getpath(obj.fname);
if ~exist(fname,'file')
    error(sprintf('File %s doesnt exist',fname));
end

fid=fopen(fname,'r');
%goto zeroth byte
fseek(fid, obj.fwidth*0, 'bof');

%read the header
data = fread(fid, obj.hsize, 'double');
fclose(fid);
header.fver=data(1);
header.nrec=data(2);
header.nmat=data(3);
header.dim=data(4:end);

obj.finfo.headptr.head=header;
end