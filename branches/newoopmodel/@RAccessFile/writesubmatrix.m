%function subm=readsubmatrix(robj,mat,id,rind,cind,recindex)
%   robj - The RAccessfile object
%   mat - the full matrix to write
%        - only the elements at rind,cind get written
%        - this is desighned for handling distributed matrices
%          so that the write operations can be in parallel
%   id   - the id of the matrix
%   rind - a 1x2 vector consisting of the first and last row to return
%   cind - a 1x2 vector consisting of the first and last column to return
%   recindex - optional the record index for this id if it is known
%Return value:
%   subm = mat(rind(1):rind(2),cind(1):cind(2))
%   mat is the matrix associated with id
%
%Explanation: This function reads only a portion of the matrix stored in
%the file
function subm=writesubmatrix(obj,mat,id,rind,cind,recindex)

%create the filename
fullf=getpath(obj.fname);

if ~exist(fullf,'file')
    error(sprintf('%s doesnt exist \n',fullf));
end
if ~exist('trials','var')
    trials=[];
end

if (length(rind)~=2)
    error('rind must be vector of length 2');
end

if (length(cind)~=2)
    error('cind must be vector of length 2');
end

if iscell(mat)
    error('mat should be a matrix not a cell array');
end

if ~exist('recindex','var')
    %goto position of record
    recindex=posofid(obj,id);
else
    %make sure id matches of recindex matches id or else
    %id indicates record has been deleted
    if ~(isdeleted(obj,recindex))

        if  (id~=idofrec(obj,recindex))
            error('id does not match the id of the supplied record');
        end
    end
end
cind=rowvector(cind);

%read the header so we know how many trials there are
head=getheader(obj);


%dowrite indicates whether each lab will do some writing
%each lab does some writing unless cind specifies the full column range
%in which case only lab 1 does the writing
%if we parallelize the writing we assume the column indexes written by each
%node have been properly set by a call to write matrix
dowrite=true;

if (diff(cind)+1==head.dim(2) && labindex~=1)
    dowrite=false;
end

if (dowrite)
    %since the matrix is distributed we parallelize the writing

    fid=fopen(fullf,'r+');


  
    %data is arranged in column major order
    %so we need to do 1 seek for each column
    %loop over columns

    %seek to the start of this
    %matrix
    %start of the record
    recstart=offset(obj,recindex,head.dim);
    fseek(fid,recstart,'bof');

    %if record is deleted write the id of the new matrix
    if ((labindex==1) && isdeleted(obj,recindex))
        %only 1 lab writes the new id

        fwrite(fid,id,'double');
    else
        %we add the space for id*fwidth because first number is the id
        idwidth=obj.idsize*obj.fwidth;

        %seek to the start of the matrix
        fseek(fid, idwidth, 0);
    end

    %all labs need to update the id of this record
    if isdeleted(obj,recindex)
        obj.finfo.idsptr.ids=setat(obj.finfo.idsptr.ids,recindex,id);
    end

    for colcount=1:(diff(cind)+1)
        col=cind(1)+colcount-1;

        %we need to seek to (rind(1),col)
        if (colcount==1)
            %fseek is currently pointing to the byte after (rind(end),col);
            %head.dim(1)-rind(end) is how many bytes are left in this column
            ntocolstart=head.dim(1)*(col-1);
        else
            %fseek is currently pointing to the byte after (rind(end),col);
            %head.dim(1)-rind(end) is how many bytes are left in this column
            ntocolstart=head.dim(1)-rind(end);
        end
        ntorowstart=obj.fwidth*(ntocolstart+rind(1)-1);


        %ntorowstart is the number of bytes we need to skip
        %relative to current position so we are pointing to the start of the
        %next column to write
        fseek(fid, ntorowstart, 0);

        fwrite(fid, mat(rind(1):rind(2),col), 'double');

        if (obj.debug)
            fprintf('Writesubmatrix: rind=(%d,%d) col=%d \n', rind(1),rind(2), col);
        end
    end


    fclose(fid);
end

if (obj.debug)
    validate(obj);
end
%wait for all threads;
labBarrier;

