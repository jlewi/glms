%function obj=saveobj(obj)
%   obj -RAccessFile
%
% Explanation: prepare the object to be saved to a file
function obj=saveobj(obj)
    
    %set the pointers to empty matrices
    obj.finfo.idsptr=[];
    obj.finfo.deletedptr=[];
    obj.finfo.headptr=[];
    