%function sim=RAccessFile('fname',fname)
%   fname - name of file where data should loaded from
%
%function sim=RAccessFile('fname',fname,'dim',dim)
%   fname - name of the file to create
%   dim   - dimensions of the matrices that will be stored
%       
%
% Revisions
%   080409 - store the header as a field so that we don't have to reread it
%   from the file
function obj=RAccessFile(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'fname'};
con(1).cfun=1;

con(2).rparams={'fname','dim'};
con(2).cfun=2;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%
%fname     -FilePath object
%          - the file we write to
%fwidth    - specifies how many bytes to use for each field
%           we store everything as doubles so fwidth is 8.
%hsize     - the number of fields for the header
%            the header consists of 5 numbers
%                   #1 - identifier/file version
%                      - the identifier allows us to check
%                        that when we reload RAccessfile
%                        we are accessing the correct file
%                        The identifier is
%                        date.time#
%                           date=yymmdd
%                   #2 - number of records in the file
%                   #3 - number of matrices in the file
%                   #4 - number of rows in each matrix
%                   #5 - number of columns in each matrix
%
%                   -The number of records is not the same
%                    as the number of matrices because some of the records
%                    may store matrices which have been deleted
%                   number of records=number of matrices + #deleted records
%
% fileid     - this is the id# associated with the file
%            - i.e it is the first number stored in the header
%            - we save this number separatly as part of the object
%            - so that if we save the object and then reload it
%               we can check the fileid hasn't changed
%
%idsize    - number of fields comprise the id
%           This was introdcued later so in many places idsize is still
%           hard coded.
%finfo      - used to store information about the file
%       .headptr - the header
%           a ptr to the header
%           we use a pointer so that when we call writeheader and getheader
%           we can change the value without returning a copy of the object
%       .idsptr - a pointer to an array of ids the ids associated with 
%               each record in the file
%            - idsptr.ids(recindex) - the id associated with record
%            recordindex
%            - this array provides a quick lookup table to find the 
%               index of a record associated with a particular id
%
%            - We use a pointer because if we have multiple copies of the
%            same robj, we need a change ids to be reflected in all the
%            copies not just one
%           - a NEGATIVE number means that record has been deleted and
%             can be overwritten on a subsequent write operation.
%       .deletedptr - a pointer to an array of ids corresponding
%             to records which have been delted
%           .recnums
%
%debug      - whether to  do test diagnostics
%distmatrix - boolean
%           - indicates that when operating in parallel (numlabs>1)
%           readmatrix should return distributed matrices.
%
%idsize,hswize, fwidth - these are constants which define the file format
%                        These should not be changed. If they are changed
%                        You should change the file version to reflect that
%                       
%idsptr=pointer();
%doing the following in one line caused a segmentation fault
%so I use multiple lines.
finfo=struct('idsptr',[]);
finfo.idsptr=pointer();
finfo.idsptr.ids=DynArray('length',100,'grow',1);
finfo.deletedptr=pointer();
finfo.deletedptr.recnums=[];
finfo.headptr=pointer();
finfo.headptr.head=[];

%declare the structure
obj=struct('version',080404,'fname',[],'fwidth',8,'hsize',5,'idsize',1,'finfo',finfo,'distmatrix',true,'debug',0,'fileid',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'RAccessFile');
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

if (length(cind)>1)
    cind=cind(end);
end
%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        %open the file and read the ids 
        obj.fname=params.fname;
        
        obj=class(obj,'RAccessFile');
        obj.finfo.idsptr.ids=DynArray('data',readmatids(obj));
        finfo.deletedptr.recnums=find(isdeleted(obj,1:getnrec(obj)));
        
        
    case 2
        %file doesn't exist so we create it
        obj.fname=params.fname;
        fname=getpath(obj.fname);
       
        
        dim=params.dim;
        if (length(dim)~=2)
            error('Dim must be a vector with 2 elements');
        end
        
        obj=class(obj,'RAccessFile');
        fver=str2num(datestr(now,'yymmdd'))+str2num(datestr(now,'HHMMSS'))/10^6;
        %only execute the writing on one node.
        if (labindex==1)
        fid=fopen(fname,'w');
        nmat=0;
        head.nmat=0;
        head.nrec=0;
        head.fver=fver;
        head.dim=dim;
        writeheader(obj,fid,head);
        fclose(fid);
        end
        %broadcast fver to all files in case clocks are different
        fver=labBroadcast(1,fver);
        obj.fileid=fver;
        %we need to wait for all nodes to reach this point
        %otherwise one of the nodes might try to use the file before its
        %created by node 1.
 %       if (numlabs>1)
        labBarrier;
%        end
    otherwise
        error('Constructor not implemented')
end

%optional parametrs
if isfield(params,'distmatrix')
    obj.distmatrix=params.distmatrix;
end


    if isfield(params,'debug')
    obj.debug=params.debug;
end

%*********************************************************
%Create the object
%****************************************
%if no base object




    