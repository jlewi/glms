%function isd=isdeleted(obj,recnums)
%   recnums - array of record numbers to check if they've been deleted
%
%Return value:
%   isd - returns true if the record in the file has been deleted and so
%         can be overwritten by a new matrix
%
%Explanation: deleted records in the file are marked by a negative number
%   for the id
function isd=isdeleted(obj,recnums)

isd=(getat(obj.finfo.idsptr.ids,recnums)<0);
