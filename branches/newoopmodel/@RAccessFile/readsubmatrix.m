%function subm=readsubmatrix(robj,id,rind,cind)
%   robj - The RAccessfile object
%   id   - the id of the matrix
%   rind - a 1x2 vector consisting of the first and last row to return
%   cind - a 1x2 vector consisting of the first and last column to return
%
%Return value:
%   subm = mat(rind(1):rind(2),cind(1):cind(2))
%   mat is the matrix associated with id
%
%Explanation: This function reads only a portion of the matrix stored in
%the file
function subm=readsubmatrix(obj,id,rind,cind)

%create the filename
fullf=getpath(obj.fname);

if ~exist(fullf,'file')
    error(sprintf('%s doesnt exist \n',fullf));
end
if ~exist('trials','var')
    trials=[];
end

if (length(rind)~=2)
    error('rind must be vector of length 2');
end


if (length(cind)~=2)
    error('cind must be vector of length 2');
end

cind=rowvector(cind);

%read the header so we know how many trials there are
head=getheader(obj);


subm=zeros(diff(rind)+1,diff(cind)+1);
%only read the matrices associated with the ids specified
%use posofid to convert the id of the matrix into a record index
%which is the # of the record in the file associated with this
%matrix

%goto position of record
recindex=posofid(obj,id);


%check if its stored in the file
if isnan(recindex)
    warning('Tried to load matrix with id=%d but its not in file \n',id);
    return;
end

fid=fopen(fullf,'r+');


%data is arranged in column major order
%so we need to do 1 seek for each column
%loop over columns

  %data is arranged in column major order
    %so we need to do 1 seek for each column
    %loop over columns

    %seek to the start of this
    %matrix
    %start of the record
    recstart=offset(obj,recindex,head.dim);

    %we add the space for id*fwidth because first number is the id
    matstart=recstart+obj.idsize*obj.fwidth;


    %seek to the start of the matrix
    fseek(fid, matstart, 'bof');
    for colcount=1:(diff(cind)+1)
        col=cind(1)+colcount-1;

        %we need to seek to (rind(1),col)
        if (colcount==1)
            %fseek is currently pointing to the byte after (rind(end),col);
            %head.dim(1)-rind(end) is how many bytes are left in this column
            ntocolstart=head.dim(1)*(col-1);
        else
            %fseek is currently pointing to the byte after (rind(end),col);
            %head.dim(1)-rind(end) is how many bytes are left in this column
            ntocolstart=head.dim(1)-rind(end);
        end
        ntorowstart=obj.fwidth*(ntocolstart+rind(1)-1);


        %ntorowstart is the number of bytes we need to skip
        %relative to current position so we are pointing to the start of the
        %next column to write
        fseek(fid, ntorowstart, 0);

        subm(:,colcount)=fread(fid, diff(rind)+1, 'double');
    end
    
%for colcount=1:(diff(cind)+1)
%    col=cind(1)+colcount-1;
%    fseek(fid, startpos(obj,rind(1),col,recindex,head), 'bof');

%    subm(:,colcount) = fread(fid, diff(rind)+1, 'double');
%end


fclose(fid);
 if (obj.debug)
       validate(obj); 
 end
 labBarrier;
%return the number of bytes for the seek operation to
% %seek to the first element of matrix at row rind, column cind
% function rowstart=startpos(obj,rind,cind,recindex,head)
% 
% %start of the matrix
% recstart=offset(obj,recindex,head.dim);
% 
% %we add the space for id fwidth because first number is the id
% matstart=recstart+obj.idsize*obj.fwidth;
% 
% %seek to the start of the column we want
% colstart=matstart+(cind-1)*head.dim(1)*obj.fwidth;
% 
% %seek to start of row
% rowstart=colstart+(rind-1)*obj.fwidth;

