%function id=idofrec(obj,recindex)
%   obj - RAccessFile
%   recindex- record
% Explanation: Get the id associated with record #recindex
function id=idofrec(obj,recindex)
    id=getat(obj.finfo.idsptr.ids,recindex);