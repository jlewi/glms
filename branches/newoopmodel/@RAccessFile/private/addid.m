%function addid(obj,id)
%   id - the id to add to the array of ids
function obj=addid(obj,id)

obj.finfo.idsptr.ids=setat(obj.finfo.idsptr.ids,getlength(obj.finfo.idsptr.ids)+1,id);