%function rindex=posofid(obj,ids)
%   ids - an array of ids to find
%
%Return value:
%   rindex - an array of the record number corresponding to each id
%       value is nan if id is not in the file
function rindex=posofid(obj,ids)

    
    rindex=zeros(1,length(ids));
    
    
    for index=1:length(ids)
       r=find(getall(obj.finfo.idsptr.ids)==ids(index));
       if isempty(r)
           rindex(index)=nan;
       elseif (length(r)>1)
           error('More than one record in the file has id %d',ids(index));
       else
          rindex(index)=r; 
       end
    end
    
    
    