%function writeheader(obj,head)
%   head - structure with the following fields
%       .fver - file version
%       .nrec - number of records
%       .nmat - number of matrices
%       .dim  - dimensions of the matrix
%Explanation: A private function to write the header information
%   header is the following for fields
%       fileversion
%       nmat - number of matrices stored in the file
%       dim - dimension of the matrices stored
function writeheader(obj,fid,head)

%only 1 lab writes the header
if (labindex==1)
%file should already be opened for writing
%goto zeroth byte
fseek(fid, 0, 'bof');

%make sure dim is a column vector
head.dim=head.dim(:);
if (length(head.dim)~=2)
    error('dim should have 2 elements.');
end

%write the following 
fwrite(fid,[head.fver;head.nrec;head.nmat;head.dim],'double');
end


%save the header
%allnodes need the updated header info.
obj.finfo.headptr.head=head;

%do not put a labbarrier here because it will cause problems
%labbarrier;