%function hid=hasid(obj,ids)
%   ids - ids to check if corresponding matrix is stored in file
%
%
%Return value
%   -Record number of the id if its in the file or nan if id is not in the
%   file
%
function hid=hasid(obj,ids)

hid=posofid(obj,ids);
