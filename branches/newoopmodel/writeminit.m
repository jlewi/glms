%function writedataset(fname,data,info)
%   fname - file to write dataset to
%   data  - a structure array of field name value paris
%   info  - field structure array
%         - written as comments in header of file
%
%Explanation: This creates a matlab script which can be executed
%   to create an array which specifies the parameters for a simulation
%
function writeminit(fname,dsets,info)

if isa(fname,'FilePath')
    fname=getpath(fname);
end
if exist(fname,'file')
    error('File already exists');
end

fid=fopen(fname,'w');

if ~isempty (info)
fprintf(fid,'%%********************************\n');
fprintf(fid,'%%Data set info\n');
fprintf(fid,'%%********************************\n');
fnames=fieldnames(info);
for ind=1:length(fnames)
    if ~isstr(info.(fnames{ind}))
        data=num2str(info.(fnames{ind}));
    else
        data=info.(fnames{ind})
    end
   fprintf(fid,'%% %s=%s \n',fnames{ind},data);
end
end

fprintf(fid,'\n');
fprintf(fid,'\n');


fnames=fieldnames(dsets);

fprintf(fid,'dsets=[];\n');
%loop through the data set
for ind=1:length(dsets)
    
    fprintf(fid,'dind=%d;\n',ind);
    for k=1:length(fnames)
        if isa(dsets(ind).(fnames{k}),'FilePath')
              fprintf(fid,'dsets(dind).%s=FilePath(''bcmd'',''%s'',''rpath'',''%s'',''isdir'',%d);\n',fnames{k},getgvar(getbcmd(dsets(ind).(fnames{k}))),getrpath(dsets(ind).(fnames{k})),isdir(dsets(ind).(fnames{k})));
        elseif isnumeric(dsets(ind).(fnames{k}))
             fprintf(fid,'dsets(dind).%s=%d;\n',fnames{k},dsets(ind).(fnames{k}));
        elseif isempty(dsets(ind).(fnames{k}))
             fprintf(fid,'dsets(dind).%s=[];\n',fnames{k});
        else
               fprintf(fid,'dsets(dind).%s=''%s'';\n',fnames{k},dsets(ind).(fnames{k}));
        end
    end
    fprintf(fid,'\n\n');
end

fclose(fid);