%function d=getrpathdir(fobj)
%
%Return value
%   d= just the directory associated with the relative path
%
function d=getrpathdir(fobj)

[d]=fileparts(getrpath(fobj));