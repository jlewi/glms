%function ('bcmd','rpath')
%   bcmd - name of variable storing the base path
%           this should be an object or array of object of type FPathBaseCMD
%           if not its assumed to be a type of PathBaseGlobalVar
%   rpath   - relative path of the file
%
%   you can also just specify bcmd and rpath without any text
function obj=FilePath(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'bcmd','rpath'};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
% basecmd - variable storing the base path
% rpath   - relative path 
% isdir   - true or false. Indicates path represents a directory as opposed
%              to a file
%declare the structure
obj=struct('version',080530,'basecmd',[],'rpath',[],'isdir',false);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'FilePath');
        return    
        
    case 2
        params.bcmd=varargin{1};
        params.rpath=varargin{2};
        cind=1;
    otherwise
        %determine the constructor given the input parameters
        [cind,params]=constructid(varargin,con);
end



%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        if isa(params.bcmd,'FPathBaseCMD')
            obj.basecmd=params.bcmd;
        else
            if isstruct(params.bcmd)
               %the child class RemoteFilePath will pass in a structure
               %in which each filed stores an FPathBaseCMD
               obj.basecmd=params.bcmd;
            else
            obj.basecmd=PathGlobalVar(params.bcmd);
            end
        end
        obj.rpath=params.rpath;
    otherwise
        error('Constructor not implemented')
end
    

if isfield(params,'isdir')
    obj.isdir=params.isdir;
end


%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
%if no base object
obj=class(obj,'FilePath');


%if the path exists make sure isdir is properly set
%%we can only run this test if basecmd is a FPathBASECMD
%i.e if we are constructing a RemoteFilePAth object
%then we end up calling FilePath to construct the base class
%but we can't evalue the basecmd 
%because obj.basecmd
%is a structure
if isa(obj.basecmd,'FPathBaseCMD')
if exist(getpath(obj),'file')
    isdir=exist(getpath(obj),'dir');
   if (xor(obj.isdir,isdir))
       error('The value of isdir and the type of file do not agree.');
   end
end
end


    