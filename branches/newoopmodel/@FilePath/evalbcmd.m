%function bcmd=bcmd(obj)
%	 obj=FilePath object
% 
%Return value: 
%	 bcmd= the output of evaluating the base command
%
function bcmd=evalbcmd(obj)

%get the base path 
     bcmd=getpath(obj.basecmd);

