%function getrpath=rpath(obj)
%	 obj=FilePath object
% 
%Return value: 
%	 rpath=obj.rpath 
%
function rpath=getrpath(obj)
	 rpath=obj.rpath;
