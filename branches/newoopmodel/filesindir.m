%function fnames=getfnames(TSdir,ext,relative)
%     TSdir - get directory
%     ext   - extension to match
%     rpath - default null, get the path relative to this path
%            -that is before computing the path it searches the begining of
%            the path for 'rpath' and then removes it
%Return value
%  fnames
%       .path - path to file
%Explanation: recursive get the filenames in the directory with ext 'ext'
function fnames=filesindir(TSdir,ext,rpath)

if ~exist('ext','var')
    ext='*';
end
if ~exist('rpath','var')
    rpath=[];
end
fnames=[];

if ~exist(TSdir,'dir')
    warning('directory does not exist');
    return;
end

 %generate cell array of all directories
 fdirs=genpath(TSdir);
 fdirs=sarraytocell(fdirs,pathsep);

 %gat the files in each directory
 fnames=[];
 for n=1:length(fdirs)
            %get the filenames
            fnames=[fnames getfnames(fdirs{n},ext,rpath)];
 end
        
function fnames=getfnames(TSdir,ext,rpath);

fnames=[];



%make sure its a directory
%for i = 1:length(dirlist)
%	if dirlist(i).isdir
if exist(TSdir,'dir')
    %    filelist = dir(TSdir);
    filelist = dir(fullfile(TSdir,['*' ext]));
    %do not process b1 files only .ab1 files
    %filelist = [filelist; dir([TSdir, '/', dirlist(i).name, '*.b1'])];

    %if rpath isn't empty then we remove rpath from TSDIR
if strmatch(rpath,TSdir)
    TSdir=TSdir(length(rpath)+1:end);
end

    for j = 1:length(filelist)
        %if ~isempty(regexp(filelist(j).name,['.*' ext '$']))
        %fnames(end+1).infile=fullfile(TSdir, dirlist(i).name,filelist(j).name);
        fnames(end+1).path=fullfile(TSdir, filelist(j).name);
        %end
    end
end