%function RemoteFilePath (bcmd,rpath)
%   bcmd - structure specifying the base command on different hosts
%        .hostid=path
%       hostid - is a string identifier for the host
%                it is not necessarily the hostname because
%                the hostname may not conform to a proper matlab field name
%
%   
%Revision: 080529
%   added the field isdir
function obj=RemoteFilePath(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'bcmd','rpath'};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%

%declare the structure
obj=struct('version',080529);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'RemoteFilePath',FilePath());
        return    
    case 2
        params.bcmd=varargin{1};
        params.rpath=varargin{2};
        cind=1;
    otherwise
        
%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);
end


%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        %pbase=FilePath('bcmd',params.bcmd,'rpath',params.rpath);
        pbase=FilePath(params);
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one

obj=class(obj,'RemoteFilePath',pbase);





    