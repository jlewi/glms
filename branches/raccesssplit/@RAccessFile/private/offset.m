%function noff=offset(obj,trial,dim)
%           trial - the "nth" matrix in the file
%                     i.e trial=10 means the 10th matrix in the data
%                          don't confuse this with the index stored with
%                          each matrix
%           dim - the dimensions of the matrix.
%Explanation: computes the offset for fseek needed in order to seek to the
%first byte of this trial. This is measured relative to bof
function noff=offset(obj,trial,dim)
%we want to position the file at the first byte of trial 
%therefore the offset is the number of bytes in the header + number of
%bytes in each record
%each matrix has prod(dim) elements +obj.idsize element to store the index associated
%with the matrix
noff=obj.fwidth*obj.hsize+(prod(dim)+obj.idsize)*(trial-1)*obj.fwidth;