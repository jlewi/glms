%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
function obj=loadobj(lobj)

    %check if lobj is a structure
    %this indicates the class structure has changed and we need to handle
    %the conversion   
    if isstruct(lobj)
        warning('Saved object was an older version. Converting to newer version');
        %create a blank object
        error('You must replace NewClass in the following line and delete this error');
        obj=RAccessFile();
        
        %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(lobj);
        
        %struct for object
        sobj=struct(obj);
        for j=1:length(fnames)
            %make sure field hasn't been delete
             if ~isfield(sobj,fnames{j})
                 error('Field %s has been removed in newest version of class.',fnames{j});
             else
                   obj.(fnames{j})=lobj.(fnames{j});
             end
        end
        %provide warning message about any new fields
        nfields='';
        fnames=fieldnames(sobj);
        for j=1:length(fnames)
            if ~isfield(lobj,fnames{j})
                nfields=sprintf('%s \n',fnames{j});
            end
        end
        if ~isempty(nfields)
            warning('The following fields were not in the older version. They will be set to default values. \n %s',nfields);
        end
    else
        obj=lobj;
    end

    %**********************************************************************
    %Convert old objects to new object model
    %*******************************************************************
    if (obj.version<080609)
       finfo=obj.finfo;
       
       %the new version no longer uses pointers to store the data
       finfo=rmfield(finfo,{'headptr','deletedptr','idsptr'});
       
       obj.finfo=finfo;
       obj.version=080609;
    end
    
    if (obj.version<080805)
        %we just added the field DELID so we don't have to do anything else
       obj.version=080805; 
    end
    if (obj.version<080925)
        %do nothing distmatrix is just settable
        obj.version=080925;
    end
    %**********************************************************************
    %Initialization
    %*********************************************************************
    
    
    
    %*read the file from the data
    if ~exist(getpath(obj.fname),'file')
        error('File: %s does not exist', getpath(obj.fname));
    end
    
                    obj.fid=fopen(getpath(obj.fname),'r+');
                    
    % create headptr before readmatids because readmatids will call
    % getheader which tries to access finfo.headptr    
    obj.finfo.header=[];
    %read the id's associated with each record
    %create a pointer to an array to store them
    obj.finfo.ids=DynArray('data',readmatids(obj));

 
    
                    obj.finfo.deletedrecnums=find(isdeleted(obj,1:getnrec(obj)));
                    
    %1. read the header and check the file id hasn't changed
    head=getheader(obj);
    
    if (head.fver~=obj.fileid)
        error('The fileid stored in the object does not match the fileid in the file.');
    end
    
    
    
   