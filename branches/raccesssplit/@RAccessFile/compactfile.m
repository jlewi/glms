%function compactfile(robj)
%
%Explanation:
%   This function gets rid of the dead space in the file created
%   by delted matrices.
%   We also sort the matrices in the file based on their id.
%
%   We do this as follows:
%       1. We create a new file and write the data to it
%       2. we delete the old file and copy the file to it
%
function compactfile(robj)

%08-05-2008
%don't allow compactfile to be called in a parallel job until its fully
%tested
if (numlabs>1)
    error('compactfile can only be called in a non-paralleljob');
end

%docopy indicates whether we have to do a copy
%one of the following conditions must be satsified
%1. There are deleted records
%2. the records are out of order
docopy=false;
if (~isempty(robj.finfo.deletedrecnums) || ~issorted(getall(robj.finfo.ids)))    
  docopy=true; 
end


if (docopy)
   %determine the ids' of the matrices to copy
    ids=getall(robj.finfo.ids);
   idstocopy=ids(ids~=robj.DELID);
   idstocopy=sort(idstocopy);
    
    %create a RAccessFile for the new file
    fold=robj.fname;
    foldrpath=getrpath(robj.fname);
    
    dext=[datestr(now,'yymmdd') datestr(now,'HHMMss')];

    fnewrpath=[foldrpath '.' dext];
    fnew=FilePath(getbcmd(fold),fnewrpath);
    
    header=getheader(robj);
    
    %instantiate an object of the appropriate type.
    eval(sprintf('rnew=%s(''fname'',fnew,''dim'',header.dim);',class(robj),fnew));
    %rnew=RAccessFile('fname',fnew,'dim',header.dim);
    
    for id=idstocopy'
       [mat]=readmatrix(robj,id);
       writematrix(rnew,mat,id);       
    end
    
    %**********************************************
    %Now we overwrite the file. We do this in 3 steps
    %*************************************************
    %1. we rename the original file
    %2. we move the new file to the old filename
    %3. we delete the old file
    foldcopy=[getpath(fold) '.' dext '.uncompacted'];
    
    %close the file we are pointing to
    closefile(robj);
    
    %move the file
    [status,message]=movefile(getpath(fold),foldcopy);
    if (status==0)
       error('Renaming old file failed with message %s. \n. You need code to properly cleanup files and restore old data.' ,message); 
    end

    %move the new file  
    [status,message]=movefile(getpath(fnew),getpath(fold));
     if (status==0)
       error('Renaming the compacted file failed with message %s. \n. You need code to properly cleanup files and restore old data.' ,message); 
     end
    
     %delete the old file
     try
     delete(foldcopy)
     catch e
         error('Error occured while deleting old file \n');
     end
     
     %reopen the new file
     openfile(robj, fold);
     
end