%function validate(robj)
%   robj - object
%
%Explanation: Checks to make sure robj is a valid object
function isvalid=validate(robj)


isvalid=true;


%check if the id's are unique
uid=unique(robj.finfo.ids);

if (length(uid)~=length(robj.finfo.ids))
    error('There are duplicate ids');
end

%check if id's match what is stored in file
idsinfile=readmatids(robj);

if (length(idsinfile)~=getlength(robj.finfo.ids))
    fprintf('Error: number of ids in file does not match stored ids \n');
    fprintf('ids in file: \n');
    rowvector(idsinfile)
    fprintf('ids in object: \n');
    rowvector(robj.finfo.ids)
    
    error('Ids in file dont match ids stored in object.');
end

if any(idsinfile~=getall(robj.finfo.ids))
     fprintf('Error: ids stored in finfo.idsptr dont match ids in file \n');
    fprintf('ids in file: \n');
    rowvector(idsinfile)
    fprintf('ids in object: \n');
    rowvector(robj.finfo.ids)
    
   error('ids stored in finfo.idsptr dont match ids in file'); 
end
 
