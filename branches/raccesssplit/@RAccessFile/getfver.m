%function getfileid=fileid(obj)
%	 obj=RAccessFile object
% 
%Return value: 
%	 fileid=obj.fileid 
%
function fileid=getfver(obj)
     h=getheader(obj);
	 fileid=h.fver;
