%function sim=FPathBaseCMD
%
%Explanation: An abstract class used to represent the base command for the
%file path in a FilePathObject or Remote File Path Object
%
% $Revision$
%Revisions
%   080829 - Convert to new OOP model
%             rename version bversion
classdef (ConstructOnLoad=true) FPathBaseCMD

    %**********************************************************
    %Define Members of object
    %**************************************************************
    % bversion - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    properties(SetAccess=private,GetAccess=public)
        %declare the structure
        bversion=080829;
    end

    methods
        function obj=FPathBaseCMD(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};
            con(1).cfun=1;


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
                    return
                case 1
                    %**********************************************
                    %used as blank constructor when called from subclass
                    %parameters should be empty
                    if ~isempty(varargin{1})
                        error('Parameters should be empty if you specify an input argument');
                    end
                        
            end

        end
    end
end





