%function path=getpath(obj)
%	 obj=FPathBaseCMD object
% 
%Return value: 
%	 path=obj.path 
%
function path=getpath(obj)
	error('getpath is an abstract method for class FPathBaseCMD. It must be overwritten in a derived class');
    