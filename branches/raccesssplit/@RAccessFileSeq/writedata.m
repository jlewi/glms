%function writdata(obj,mat,ids,overwrite)
%       mat - cell array of matrices to append
%       ids  -  indexes to associate with each matrix
%               - these must be >=0
%       overwrite - tells how to handle case where matrix is already in
%                       -default - overwrite data
%                       ~0 - matrix in file is overwritten with new matrix
% Explanation: Writes data to the file
function obj=writedata(obj,mat,ids, overwrite)


if ~exist('overwrite','var')
    overwrite=1;
end



if ~iscell(mat)
    mat={mat};
end

if (length(ids)~=length(mat))
    error('Number of ids and matrices must match.');
end

%make sure ids are positive
if any(ids<0)
    error('ids must be >=0');
end

ids=colvector(ids);

%get the header
head=getheader(obj);

%loop through the trials to write
%          for k=1:length(ids)
%              %read each matrix
%                writematrix(obj,mat{k},ids(k),overwrite);
%          end

%check if any of the indexes of the matrices passed in are already stored
%in the file
%mat is new tells us whether each entry in the mat,ind arrays is new or
%already in the file (nan means its new)
recnums=posofid(obj,ids);
odeletedind=[];

%***************************************************
%write all the new matrices to the end of the file
%**************************************************
newind=find(isnan(recnums)); %indexes in mat,ids to matrices which aren't already in file

%if we are writing new matrices we first overwrite the space used by
%matrices which have been delted
if ~isempty(obj.finfo.deletedrecnums)

    %indexes of recnums which will be used to overwrite deleted matrices
    odeletedind=newind(1:min(length(obj.finfo.deletedrecnums),length(newind)));

    %the first length(obj.finfo.deletedptr.recnums) new matrices
    %are used to overwrite matrices which have been deleted
    newind=newind(length(obj.finfo.deletedrecnums)+1:end);


end
if ~isempty(newind)
    %all new matrices are written at once using lab 1
    
    
        head.nmat=head.nmat+length(newind);
        head.nrec=head.nrec+length(newind);
        
    if (labindex==1)
        %error checking make sure all matrices have correct dimension


        for k=1:length(newind)
            if (size(mat{newind(k)},1)~=head.dim(1))
                error('Cant append data. Matrix doesnt have same number of rows as matrices in database');
            end
            if (size(mat{newind(k)},2)~=head.dim(2))
                error('Cant append data. Matrix doesnt have same number of cols as matrices in database');
            end
        end







        %write the data
        %go to eof
        fseek(obj.fid,obj.fwidth*0,'eof');
        %pack the data
        %we create a 2d matrix to be written in column major order
        data=zeros(prod(head.dim)+1,length(newind));
        for k=1:length(newind)
            data(1,k)=ids(newind(k));
            data(2:end,k)=mat{newind(k)}(:);
        end

        %write all the data
        fwrite(obj.fid,data,'double');
    end
    %   update the header
        writeheader(obj,head);



end

%update the ids on all nodes
obj.finfo.ids=append(obj.finfo.ids,ids(newind));



%we need to wait till node 1 finished writing before we continue
%barrier needs to be before the validate step
labBarrier;

if (obj.debug)
    validate(obj);
end


%**************************************************************************
%Handle matrices already in file
%**************************************************************************
%isnan(recnums) - indexes in mat,ids to matrices which are already in file
%odeletedind - indexes of matrices to write which will overwrite deleted
%matrices
oldind=[find(~isnan(recnums)) odeletedind];

if ~isempty(oldind)
    if (overwrite==0)
        warning('Matrices already in file were not updated because overwrite is set to false');
    else

        %loop through the trials to write
        for k=1:length(oldind)
            %read each matrix
            writematrix(obj,mat{k},ids(oldind(k)),overwrite,recnums(oldind(k)));
        end
    end
end

if (obj.debug)
    validate(obj);
end

