%function getfname(pname)
%   pname - start directory
%
%Explanation: open a file dialog box to get a filename
function varargout=getfname(pname)
    d=pwd;
    if ~isempty(pname)
        cd(pname);
       [fname,pname]=uigetfile();
    end
    if (nargout==1)
        varargout{1}=fullfile(pname,fname);
    else
        varargout{1}=fname;
        varargout{2}=fname;
    end
    cd(d);