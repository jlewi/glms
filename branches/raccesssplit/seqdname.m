%function sedname(dname)
%   dname - base name directory
%
%Generates sequential directories named dname_xxx
%where _xxx are sequential numbers
function odir=seqdname(dname)

dname=abspath(dname);
dnum=1; %number to assign to directory name

%add leading and trailing filesep characters before parsing
%THIS CODE IS THE REASON I have to first convert to absolute path
if (dname(1)~=filesep)
    dname=[filesep dname];
end

if (dname(end)~=filesep)
    dname=[dname filesep];
end

%split the path into the parent and the directory we want to sequentially
%generate
ind=find(dname==filesep);
%parent should end in the filesep character
parent=dname(ind(1):ind(end-1));
    
%child will not include the path separater characters
child=dname(ind(end-1)+1:end-1);

flist = dir([parent child '_*']);


for j=1:numel(flist)
    fmatch = regexp(flist(j).name, ['^' child '_\d*$'], 'match');
    fmatch=fmatch{1};
    if ~isempty(fmatch)
        fnum=fmatch(length(child)+2:end);

       if (dnum<=str2double(fnum))
          dnum=str2double(fnum)+1; 
       end
    end
end    

oname=sprintf('%s_%03g',child,dnum);
odir=[parent oname];


