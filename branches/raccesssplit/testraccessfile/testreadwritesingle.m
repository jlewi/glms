%***********************************************************************
%function testreadwritesingle
%
%Explanation: Test our ability to read a matrix and then overwrite
%**********************************************************************
function testreadwritesingle(dim,fpath)
fpath=seqfname(fpath);
fpath=labBroadcast(1,fpath);

robj=RAccessFile('fname',fpath,'dim',dim,'debug',true);

htrue=[];
htrue.nmat=1;
htrue.nrec=1;
htrue.dim=dim;

data.mat{1}=randn(dim);
data.ids=1;
mid=data.ids;

data=labBroadcast(1,data);

%try writing a new matrix to the file
mid=1;
robj=writematrix(robj,data.mat{1},mid);

[dinfile.mat, dinfile.ids]=readmatrix(robj,mid);

dinfile.mat={dinfile.mat};

vm=verifydata(data,dinfile);

%verify the header
verifyheader(robj,htrue);


%try overwritting the matrix
mid=1;
data.mat{1}=randn(dim);
data=labBroadcast(1,data);

robj=writematrix(robj,data.mat{1},mid);

[dinfile.mat, dinfile.ids]=readmatrix(robj,mid);
dinfile.mat={dinfile.mat};

vm=verifydata(data,dinfile);
vm=verifyheader(robj,htrue);

%verify the last id in the file
verifylastid(robj,data.ids(end));

labBarrier;
