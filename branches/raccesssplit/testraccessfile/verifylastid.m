%verify getlastid returns the approriate id
% tid - the value that should be the last id
function verifylastid(robj, tid)
%veryify last id is the last id as returned b
if (tid~=getlastid(robj))
    error('getlastid did not return the correct id');
end
