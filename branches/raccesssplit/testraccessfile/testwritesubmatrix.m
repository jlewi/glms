%*********************************************************
%testwritesubmatrix
%       robj - the RAccessFile to use
%       data - structure describing data already in the file
%***********************************************************
function [robj,data]=testwritesubmatrix(robj,data)
clear mat;

robj.debug=true;
fprintf('testraccessfile: begginning test of writing a submatrix. \n');

ntrials=10;
for trial=1:ntrials
    %randmoly generate the matrix to read and the rows,cols to read
    mid=ceil(rand(1)*data.nmat);
    rind=sort(ceil(rand(1,2)*data.dim(1)));
    cind=sort(ceil(rand(1,2)*data.dim(2)));

    mid=labBroadcast(1,mid);
    rind=labBroadcast(1,rind);
    cind=labBroadcast(1,cind);

    data.mat{mid}(rind(1):rind(2),cind(1):cind(2))=randn([diff(rind)+1,diff(cind)+1]);

    data.mat{mid}=labBroadcast(1,data.mat{mid})
    labBarrier;
    writesubmatrix(robj,data.mat{mid},mid,rind,cind);
   % fprintf('testraccessfile: writesubmatrix complete \n');
   labBarrier
    mat=readmatrix(robj,mid);
    labBarrier
    %check if the changed part matches
    if any(any(mat(rind(1):rind(2),cind(1):cind(2))~=data.mat{mid}(rind(1):rind(2),cind(1):cind(2))));
        fprintf('Error: Writesubmatrix test failed. \n');
        fprintf('Mat= \n');
     %   mat
      %  fprintf('data.mat{mid}=\n');
       % data.mat{mid}
       % [mat(rind(1):rind(2),cind(1):cind(2)) data.mat{mid}(rind(1):rind(2),cind(1):cind(2))]
        error('Trial %d: Write submatrix the altered part of the matrix is not correct.',trial);
    end

    %check hole matrix
    if any(any(mat~=data.mat{mid}))
        error('Matrix is wrong after writesubmatrix.');
    end
    labBarrier;
end