%function validate(robj)
%   robj - object
%
%Explanation: Checks to make sure robj is a valid object
function isvalid=validate(robj)

%we need a labBarrier - b\c we need all read/write operations to catch up
%before issuing validate
labBarrier;

%callvalidate of the base class
isvalid=validate@RAccessFile(robj);

labBarrier;
