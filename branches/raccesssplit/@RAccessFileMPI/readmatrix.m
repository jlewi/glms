%function [mat,id]=readdata(obj,id)
%       id- id of the matrix to read
%
% Return value
%       mat - matrix
%       matid  - id of the read matrix
%
%Explanation: Reads a single matrix from the file
%
%   Parallel Environments: The matrix is either a distributed matrix or not
%   depending on the value of distmatrix
function [mat,matid]=readmatrix(obj,id)


%Checking the file exists takes too much time so we don't want to do it
%every time we call read. Just do it once at the start of an experiment.
%if ~exist(fullf,'file')
%    error(sprintf('%s doesnt exist \n',fullf));
%end

if ~exist('trials','var')
    trials=[];
end
%read the header so we know how many trials there are
head=getheader(obj);

%only create create a distributed representation if
%   1. we are using a parallel job
%   2. obj.distmatrix is true
%   3. The matrix we are reading is a matrix not a vector
if ((numlabs>1) && obj.distmatrix && head.dim(1)>1 && head.dim(2)>1)
    %create a distributed representation of the matrix
    mat=zeros(head.dim(1),head.dim(2),distributor());

    %each lab reads the appropriate values of the covariance matrix from
    %the file
    %we get the columns of the covariance matrix stored on lab
    %this is a distributed vector, with the local part storing
    %the columns of c stored on each lab
    colind=dcolon(1,head.dim(2));

    clocal=localPart(colind);
else
    mat=zeros(head.dim(1),head.dim(2));
    clocal=1:head.dim(2);
end

%the read command should be executed in parallel
%parallel reads shouldn't be an issue.
%calling readsubmatrix might be subotimal when reading the
%entire matrix because it entails multiple fseeks, one for each
%column
%clocal
if (obj.debug)
    fprintf('ReadMatrix.m calling readsubmatrix \n');
end
%head.dim
%id
%size(mat)
mat(:,clocal(1):clocal(end))=readsubmatrix(obj,id,[1 head.dim(1)],[clocal(1) clocal(end)]);


if (obj.debug)
    fprintf('ReadMatrix.m finished readsubmatrix \n');
end
matid=id;

 if (obj.debug)
       validate(obj); 
 end
%halt execution until all threads get here.
 labBarrier;
