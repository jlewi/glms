%function sim=RAccessFile('fname',fname)
%   fname - name of file where data should loaded from
%
%function sim=RAccessFile('fname',fname,'dim',dim)
%   fname - name of the file to create
%   dim   - dimensions of the matrices that will be stored
%
%
% Revisions
%   09-25-2008 - Make distmatrix public setable
%         
%   080811 - 
%       Declare the properties we don't want to save with the attribute
%       transient
%   080805 - add DELID field
%   080609- Begin conversion of code to new Matlab OOP model
classdef (ConstructOnLoad=true) RAccessFile < handle


    %**************************************************************
    %properties
    %***************************************************************

    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %
    %fname     -FilePath object
    %          - the file we write to
    %fwidth    - specifies how many bytes to use for each field
    %           we store everything as doubles so fwidth is 8.
    %hsize     - the number of fields for the header
    %            the header consists of 5 numbers
    %                   #1 - identifier/file version
    %                      - the identifier allows us to check
    %                        that when we reload RAccessfile
    %                        we are accessing the correct file
    %                        The identifier is
    %                        date.time#
    %                           date=yymmdd
    %                   #2 - number of records in the file
    %                   #3 - number of matrices in the file
    %                   #4 - number of rows in each matrix
    %                   #5 - number of columns in each matrix
    %
    %                   -The number of records is not the same
    %                    as the number of matrices because some of the records
    %                    may store matrices which have been deleted
    %                   number of records=number of matrices + #deleted records
    %
    % fileid     - this is the id# associated with the file
    %            - i.e it is the first number stored in the header
    %            - we save this number separatly as part of the object
    %            - so that if we save the object and then reload it
    %               we can check the fileid hasn't changed
    %
    %idsize    - number of fields comprise the id
    %           This was introdcued later so in many places idsize is still
    %           hard coded.
    %finfo      - used to store information about the file
    %       .header - the header
    %           we can change the value without returning a copy of the object
    %       .ids- an array of ids the ids associated with
    %               each record in the file

    %            - this array provides a quick lookup table to find the
    %               index of a record associated with a particular id
    %
    %           - a NEGATIVE number means that record has been deleted and
    %             can be overwritten on a subsequent write operation.
    %       .deletedrecnums -an array of ids corresponding
    %             to records which have been delted in the file. i.e empty
    %             records in the file to which new arrays can be written.
    %
    %
    %debug      - whether to  do test diagnostics
    %distmatrix - boolean
    %           - indicates that when operating in parallel (numlabs>1)
    %           readmatrix should return distributed matrices.
    %
    %idsize,hswize, fwidth - these are constants which define the file format
    %                        These should not be changed. If they are changed
    %                        You should change the file version to reflect that
    %
    %idsptr=pointer();
    %doing the following in one line caused a segmentation fault
    %so I use multiple lines.


    properties (SetAccess=private, GetAccess=public)
        version=080925;
        fname=[];
        fwidth=8;
        hsize=5;
        idsize=1;
        fileid=[];
    end

    properties(SetAccess=public,GetAccess=public)
        distmatrix=true;
    end
    %following are the transient properties that we don't want to be saved
    properties (SetAccess=private,GetAccess=public,Transient=true)
      finfo=struct('ids',DynArray('length',100,'grow',1),'header',[],'deletedrecnums',[]);
      fid=[];
    end
    properties (SetAccess=public, GetAccess=public)
        debug=false;
    end
    properties(Constant=true, GetAccess=public)
        %value of the id field to use for deleted records
        DELID=-1;
    end
 
    %add functions for opening and closing the file
    %this functions are used by compactfile
    %
    methods(Access=private)
        %********************************************************
        %processing to perform when opening a file
        %**************************************
        function openfile(obj,fname)
            %open the file and read the ids
            obj.fname=fname;


            if ~exist(getpath(obj.fname),'file')
                error('File: %s does not exist \n',getpath(obj.fname));
            end
            obj.fid=fopen(getpath(obj.fname),'r+');
            obj.finfo.ids=DynArray('data',readmatids(obj));
            obj.finfo.deletedrecnums=find(isdeleted(obj,1:getnrec(obj)));

        end

        %****************************************************
        %close file
        %**************************************************
        function closefile(obj)
            if (~isempty(obj.fid) && obj.fid>1)
                fclose(obj.fid);

                %set all fields to initial values
                nobj=RAccessFile();
                fnames=fieldnames(nobj);
                for j=1:length(fnames)
                    try
                   obj.(fnames{j})= nobj.(fnames{j});
                    catch e
                        %DELID is a constant so we can't set it.
                       if ~strcmp(fnames{j},'DELID')
                           throw(e);
                       end
                    end
                end
            end
        end
    end
    methods


        function obj=RAccessFile(varargin)

            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'fname'};
            con(1).cfun=1;

            con(2).rparams={'fname','dim'};
            con(2).cfun=2;
            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
                    return    ;


            end





            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            if (length(cind)>1)
                cind=cind(end);
            end
            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1


                    openfile(obj,params.fname);

                case 2
                    %file doesn't exist so we create it
                    obj.fname=params.fname;
                    fname=getpath(obj.fname);


                    dim=params.dim;
                    if (length(dim)~=2)
                        error('Dim must be a vector with 2 elements');
                    end

                    fver=str2num(datestr(now,'yymmdd'))+str2num(datestr(now,'HHMMSS'))/10^6;
                    %only execute the writing on one node.
                    %since file doesn't exist we open it as w+
                    %we only want to do this on one node
                    %we use node 1 to create the file
                    %
                    if (labindex==1)
                        fid=fopen(fname,'w+');
                        fclose(fid);
                    end
                    labBarrier;

                    %all nodes open the file
                    obj.fid=fopen(fname,'r+');

                    nmat=0;
                    head.nmat=0;
                    head.nrec=0;
                    head.fver=fver;
                    head.dim=dim;
                    %broadcast fver to all files in case clocks are different
                    head.fver=labBroadcast(1,fver);


                    writeheader(obj,head);

                    obj.fileid=head.fver;
                    %we need to wait for all nodes to reach this point
                    %otherwise one of the nodes might try to use the file before its
                    %created by node 1.
                    %       if (numlabs>1)
                    labBarrier;
                    %        end
                otherwise
                    error('Constructor not implemented')
            end

            %optional parametrs
            if isfield(params,'distmatrix')
                obj.distmatrix=params.distmatrix;
            end


            if isfield(params,'debug')
                obj.debug=params.debug;
            end
        end% end function RAccessfile

        obj=deletematrix(obj,id);
        hid=hasid(obj,ids);
        isd=isdeleted(obj,recnums);


        % Delete methods are always called before a object
        % of the class is distroyed
        function delete(obj)
            closefile(obj);
        end

        obj=saveobj(a);

        obj=writematrix(obj,mat,id, overwrite,recindex);
    end %end methods

    methods (Static = true)
        obj=loadobj(a);

    end



end %end classdef



