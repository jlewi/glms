%function addid(obj,id)
%   id - the id to add to the array of ids
function obj=addid(obj,id)

obj.finfo.ids=setat(obj.finfo.ids,getlength(obj.finfo.ids)+1,id);