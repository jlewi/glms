%function v =isdir(fpath)
%
%Explanation: Indicates whether this object
%   represents a directory or a file.
%   Note this returns the value ofa user set property
%   I.e it doesn't actually check the file path
%   becasue the file path may not exist
%   thus its possible isdir could be wrong
function v= isdir(fpath)
    v=fpath.isdir;