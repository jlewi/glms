%function [fname,snum]=seqfname(basename,padding,start)
%       basename - base file name including extension i.e file.ext
%       padding  - number of digits to pad to
%                   default - 3
%       start    - number at which to start
%                   default - 0
%Return:
%   fname - a string with file_###.ext where ### is a sequential number
%       representing the next availble file number
%   num  - the number appended to the filename
%   snum - string representation with padding
function [fnew,num]=seqfname(fpath,padding,start)
    if ~exist('start','var')
        start=1;
    end
    if ~exist('padding','var')
        padding=[];
    end
    if isempty(padding)
        padding=3;
    end
    
    %separate out parts of file
    [pathstr, bname,ext]=fileparts(getpath(fpath));
    num=start;
    
    %add padding to number
    %add padding if number is < 10^(padding-1)
   
    numbname=sprintf(['%s_%0' num2str(padding) '.' num2str(padding) 'd'],bname,num);
    fname=fullfile(pathstr,[numbname ext]);
    while exist(fname,'file')
        %increment num
        num=num+1;
    
        numbname=sprintf(['%s_%0' num2str(padding) '.' num2str(padding) 'd'],bname,num);
        fname=fullfile(pathstr,[numbname ext]);
    end
    
    %now we need to create a file path object
    %evaluate the bcmd
     bcmd=evalbcmd(fpath);
     %remove this from the path
     rpath=fname(length(bcmd)+1:end);
    fnew=FilePath('bcmd',getbcmd(fpath),'rpath',rpath);