%function p=getpath(obj)
%
%Explantion evaluates the path for this object
function p=getpath(obj)

%    bcmd=evalin('base',obj.basecmd);
    %basecmd should be a global variable   
     bcmd=evalbcmd(obj);
    p=fullfile(bcmd,obj.rpath);