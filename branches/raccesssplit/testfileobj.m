%08-05-30
%
%some quick tests to see if FileObj generate errors

fobj=FilePath('bcmd','RESULTSDIR', 'rpath','hello.txt');

pvar=PathGlobalVar('RESULTSDIR');
fobj=FilePath('bcmd',pvar,'rpath','hello.txt');

apath=PathABS('/tmp');
fobj=FilePath(apath,'test');

bcmd.bayes=apath;
bcmd.cluster=pvar;
fremote=RemoteFilePath(bcmd,'test.txt');

getpath(fremote,'bayes');
getpath(fremote,'cluster');
