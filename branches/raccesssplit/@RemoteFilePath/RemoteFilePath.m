%function RemoteFilePath (bcmd,rpath)
%   bcmd - structure specifying the base command on different hosts
%        .hostid=path
%       hostid - is a string identifier for the host
%                it is not necessarily the hostname because
%                the hostname may not conform to a proper matlab field name
%
%
% $Revision$
%Revision: 080529
%   added the field isdir
%
%Revision: 080828
%   convert to new OOP model
%   rename version to rversion
%
classdef (ConstructOnLoad=true) RemoteFilePath<FilePath

    %**********************************************************
    %Define Members of object
    %**************************************************************
    % rversion - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private,GetAccess=public)
        rversion=080828;
    end

    methods
        function obj=RemoteFilePath(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'bcmd','rpath'};
            con(1).cfun=1;



            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    params=[];
                    cind=0;
                case 2
                    params.bcmd=varargin{1};
                    params.rpath=varargin{2};
                    cind=1;
                otherwise

                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end

            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@FilePath(params);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 0
                    %do nothing use by loadobj
                case 1
                    %pbase=FilePath('bcmd',params.bcmd,'rpath',params.rpath);
                    %do nothing

                otherwise
                    error('Constructor not implemented')
            end


        end
    end
end



