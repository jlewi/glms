%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
function obj=loadobj(lobj)

%get the latest version
%save the latest version so we can check to make sure all
%conversions have been completed.

newobj=RemoteFilePath();
latestver=newobj.rversion;

if isstruct(lobj)
    obj=newobj;

    %if lobj has field version then we need to rename it to rversion
    %because as part of the conversion process to the new oop model 
    %the field version was renamed
    if isfield(lobj,'version')
        lobj.rversion=lobj.version;
        lobj=rmfield(lobj,'version');
    end
    obj.rversion=lobj.rversion;
else
    %we don't need to create a new object
    obj=lobj;
end



%******************************************************************
%Sequentially convert one version of the object to the next
%until we get to the latest version
%
%Warning: If lobj is a structure then we haven't copied the data from lobj
% to obj yet. 
%******************************************************************
if (obj.rversion < 080828)
    %do conversion
    %copy fields from old version to new version
    %if lobj is a structure
    if isstruct(lobj)
        fskip={'FilePath'};    %fields not to copy
        obj=copyfields(lobj,obj,fskip);
    end
    obj.rversion=080828;
end

%check if converted all the way to latest version
if (obj.rversion <latestver)
    error('Object has not been fully converted to latest version. \n');
end

%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
function dest=copyfields(source,dest,skip)
 %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(source);

        %struct for object
        sobj=struct(dest);
        for j=1:length(fnames)
            switch fnames{j}
                case skip
                    %do nothing we skip this field
                otherwise
                    %set the new field this will cause an error
                    %if the field isn't a member of the new object
                      dest.(fnames{j})=source.(fnames{j});
            end
        end
    