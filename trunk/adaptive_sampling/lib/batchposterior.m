%function [pg]=batchposterior(prior,x,obsrv,ktrue)
%       prior - structure representing the initial gaussian prior
%              .m - mean
%              .c - covariance matrix
%       x     - column vector representing the stimulus
%               nxt
%               t-number of trials
%       obsrv - struture representing the observations
%              .n - number of spikes
%                 -1xt
%              .twindow -length of window in which spikes are occured
%       ktrue  - true model parameter this is used to try to minimize
%                the numerical error introduced by the fact that a fixed
%                range is used for the numerical integration.
%Return value:
%   pg - structure
%       .m - mean
%       .c - covariance
%   lpobsrv - the log likelihood of the observations (up to addition of a
%           constant)
%        m x n - Each row is the log likelihood of a different observation
%                under a different model. Each column represents a
%                different model
%Explanation:
%   Updates the posterior for a glm by matching its 1st and 2nd moments
%   assuming posterior is represented as a gaussian.
%
%   The response/observations (obsrv) is the number of spikes produced by a
%   homogenous proisson process in the window of length twindow.
%   This assumes batch processing. That is we the response observed in the
%   window of length twindow is due entirely to the stimulus presented at
%   x;
%   
%   The posterior is computed using all the observations it is not computed
%   recursively.
%
%   This only works for 1d data
% 
%   Uses an exponential nonlinearity
% 
% Warning: To efficently calculate the posterior it uses a fixed range
%    for the numerical integration this will cause numerical errors and
%    introduce a bias into the results
function [pbatch,lpobsrv,thetapts]=batchposterior(prior,x,obsrv,ktrue)

%number of observations
nobsrv=size(x,2);

%number of upts to use
numthetapts=500;

%we need to compute the probabilities and perform the integration in theta
%space 

%uvec- unit vector representing the dimension along which we do the update
%uvec=x/(x'*x)^.5;

%Compute the prior variance and mean along the dimension x
%eqns 11-16
%m0=x'*prior.m;
%v0=x'*prior.c*x;

%construct an orthonormal basis to uvec
%D=eye(length(x),length(x))-x*(x'*eye(length(x),length(x))/(x'*x));
%D=orth(D);
%vd=D'*prior.c*D;


%compute the range for the numerical integration
%The range is taken as -4 std to 4 std
%thetapts=linspace(ktrue-4*prior.c^.5,ktrue+4*prior.c^.5,numthetapts);
%We use nonuniform spacing because
%   1) Initially we need to cover a large range because we don't know where
%   theta is going to be
%   2) later on theta will be concentrated within a small range of ktrue
%   and we want to densely sample
%rtheta1=linspace(ktrue-4*prior.c^.5,ktrue-.0015,numthetapts/4);
%rtheta2=linspace(ktrue+.0015,ktrue+4*prior.c^.5,numthetapts/4);
rtheta1=linspace(ktrue-.01,ktrue-.0015,numthetapts/4);
rtheta2=linspace(ktrue+.0015,ktrue+.01,numthetapts/4);
rhdtheta=linspace(ktrue-.001,ktrue+.001,numthetapts/2);
thetapts=[rtheta1 rhdtheta rtheta2];
dtheta=diff(thetapts);
dtheta=[dtheta(1) dtheta];
dtheta=ones(nobsrv,1)*dtheta;
%now for each data point we need to construct a
%matrix which in each row we have the rates for the different observations
%under the different models
u=(x'*ones(1,numthetapts)).*(ones(nobsrv,1)*thetapts);

%now compute rates for the different observations under the different
%models
%each row of r a different observation
%each column a different model
r=exp(u);

%compute the likelihood of the different observations under the
%different models.
%nspikes=obsrv.nspikes'*ones(1,numthetapts);


%spiketrains under the different
%rates r
lpobsrv=zeros(nobsrv,numthetapts);
for oindex=1:nobsrv
ll=hpoissonll(obsrv.n(oindex),r(oindex,:),obsrv.twindow);
lpobsrv(oindex,:)=ll';
end


%*************************************
%numerical integration
%*************************************
lpostact=zeros(nobsrv,numthetapts);

%compute the cumulative sum of the of the observations
clpobsrv=cumsum(lpobsrv,1);
for oindex=1:nobsrv
    %compute the normalization constant
    %log of the posterior as function of u
    lpostact(oindex,:)=-(thetapts-prior.m).^2/(2*prior.c)+sum(lpobsrv(1:oindex,:),1);
end

%these numbers can be very large 
%or very small. Either way this scaling factor drops out when we normalize
%scaling the probability's is equivalent to adding a constant to the
%log of the likelihoods (see my notes)
lpost=lpostact;
lpost=lpostact-max(lpostact,[],2)*ones(1,numthetapts);
%only divide by sc if sc>0
%b\c if sc is close to 0 we just blow up to infinity when we divide by sc
%if sc>0
%    lp=lp./sc;
%end
%if you choose to use lp make sure you use lp in the calculation for the
%mean and the variance
%z=exp(lp)*du;

%multiplying by du might be causing the problem. 
z=exp(lpost).*dtheta;
z=sum(z,2);

if (z==0)
    error('Normalization Constant Zero in gposteriorglm');
end

%update the mean
m1=sum(exp(lpost).*(ones(nobsrv,1)*thetapts).*dtheta,2);
m1=m1./z;


if ~(isreal(m1))
    error('New 1d mean not real');
end
%update the variance
v1=sum(exp(lpost).*(ones(nobsrv,1)*thetapts-m1*ones(1,numthetapts)).^2.*dtheta,2);
v1=v1./z;

pbatch.m=[prior.m m1'];
pbatch.c=num2cell([prior.c v1']);