%function [ll]= ll1dtemp(y,x,k,dt)
%       y - observations - equals a spike train.
%           should be single number
%           mxt
%           -m different observations
%           -t length of each spike train
%               or =1 if y is the number of spikes
%       x - value of the input
%           mxd
%           -m different stimuli - for m different trials
%           -d length of trial
%       k - the filter constants
%           - is d x n
%          -  each column is a different model under which to compute
%             the likelihood
%       dt - binwidth/time of response
%            -this should = the binwidth if y is a spike train
%                         = length of window in which response observed
%                           if y is number of spikes
%
%Return Value:
%    ll =log likelihood of the data
%       = m x n
%        m - the different observations (correspond to rows of y)
%        n - different models under which likelihood is computed 
%
%Explanation: Computes the likelihood of the response y, of a 1-d GLM.
%   r(t)= exp( K'X(t))
%       K- is 1-d temporal filter
%          - is d x n
%          - in which case we compute the rate under n different models
%          -each column is a different filter
%       X(t) - the stimulus
%       r(t) - instantaneous rate of homogenous process
%
%   Currently we do batch processing, so we compute a single rate r(t)
%   using k(t), x(t) and then we assume rate is constant for the duration
%   of the response y
function [ll]= ll1dtemp(y,x,k,dt)
    ll=zeros(size(y,1),size(k,2));
    %compute the rate
    %this gives a mxn matrix which specifies the rate under each model
    r=exp(x*k);
    numspikes=sum(y,2);
    for oind=1:size(y,1)
        %r is a matrix with each row containing all the different rates
        %for a given trial under the different models
        %we need to construct a matrix from each row in which 
        %we repate the rate
        %to indicate we are holding rate constant
        %rtrial=(r(oind,:)')*ones(1,size(y,2));
    
        %We assume the rate is constant to rapidly calculate the likelihood
        %under a homogenous poisson process
        %get the log likelihood of the data under a poisson process
       
        lpoisson=hpoissonll(numspikes(oind),r(oind,:),dt*size(y,2));
        %call the non-conjugate transpose
        ll(oind,:)=(lpoisson).';
    end