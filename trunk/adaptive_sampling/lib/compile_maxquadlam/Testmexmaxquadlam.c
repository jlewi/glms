// Purpose of this script is to test the function mexPOissExpObjFun.c
//reads in matrices from the file testdeflate.mat
//
// 12-20-2007
//    updated to use new code
//

#include "mex.h"
#include "mat.h"
//#extern 

//declaration for the mexfunction
void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]);

int main(){
  
  	//integer indicating whether to continue
	int cont=1;
  //pointer to structure array containing the data to test
  mxArray* testdata; 	
  MATFile* fptr;
  char fname[]="testdata.mat";
  char vname[]="data";	//name of variable in .mat file
  fptr=matOpen(fname, "r");
  if (fptr!=NULL){
  	testdata=matGetVariable(fptr, vname);
  	if (testdata==NULL){
  		printf("Could not load %s from file. \n Unable to continue. \n", vname);
  		matClose(fptr);
  		exit(1);
  	}
  	
  	//index into testdata structure
  	int index=0;
  	//test data should be a structure with the following fields
  	//.eigd, .evecs, .sdir, .rho, threshold,
  	mxArray* vopt=mxGetField(testdata, index,"vopt");
  	mxArray* ediff=mxGetField(testdata, index,"ediff");
  	mxArray* eigd=mxGetField(testdata, index,"eigd");
  	mxArray* wmuproj=mxGetField(testdata, index,"wmuproj");  	
  	mxArray* w=mxGetField(testdata, index,"w");  	  	
  	mxArray* dmuproj=mxGetField(testdata, index,"dmuproj");
  	mxArray* dmuproj2=mxGetField(testdata, index,"dmuproj2");
  	mxArray* d=mxGetField(testdata, index,"d");
  	mxArray* mag=mxGetField(testdata, index,"mag");
  	mxArray* evecs=mxGetField(testdata, index,"evecs");
  	mxArray* muk=mxGetField(testdata, index,"muk");
  	
  	
  	
  		int nlhs=2;
  		int nrhs=11;
  		mxArray* plhs[nlhs];
  		mxArray* prhs[nrhs];
  		int i=0;
  		prhs[i]=vopt;
  		i=i+1;prhs[i]=eigd;
  		i=i+1;prhs[i]=ediff;
  		i=i+1;prhs[i]=wmuproj;
  		i=i+1;prhs[i]=w;
  		i=i+1;prhs[i]=dmuproj2;
  		i=i+1;prhs[i]=dmuproj;
  		i=i+1;prhs[i]=d;
  		i=i+1;prhs[i]=mag;
  		i=i+1;prhs[i]=evecs;
  		i=i+1;prhs[i]=muk;
  		
  		
  	for (i=0;i<nrhs;i++){
  		if (prhs[i]==NULL){
  			mexPrintf("Error parameter # %i is null \n",i); 
  			cont=0;	
  		}	
  	}
  	if (cont==1){
  		mexFunction(nlhs, plhs,nrhs, prhs);
  	}
  	matClose(fptr); 
  		
  	  
  }
  else{
  	printf("Could not open file %s \n",fname);
  }
  
}
