#include "mex.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*****************************************************************************
Author: Jeremy Lewi
Date: 8-08-2007
Explanation: This function computes the NEGATIVE of the mutual information for the canonical poisson as a function of v which is a number between [0 1] which is scaled to emax<lambda<infty. THus, this function serves as our objective function to be used by fminbnd.

Revisions:
12-20-2007 - rewrite this function to use memory more efficiently and avoid memory leaks


Matlab Syntax:

[pcon,sigma, xmax]=maxQuadLam(v,eigd,ediff,wmuproj,w,dmuproj2,d,mag,evecs,muk)

    evecs - eigenvectors these are only needed if we want to compute xmax
    muk   - only needed to compute xmax
          - should be a unit vector in direction of mean

Return value:
   xmax - stimulus which generates this value of pcon and sigma

*********************************************************************************/

#define fmsg         flowmsg(__FILE__,__LINE__)


void flowmsg(char* fname, int line){
 #ifdef DEBUG
  mexPrintf("File %s \t Line %d \n",fname,line);
  #endif
}

//following define which element in the return array plhs corresponds to pcon
// and qmax respectively
#define plind 0
#define qlind 1
void mexFunction(int nlhs, mxArray *plhs[],int nrhs,  const mxArray *prhs[]) {


#ifdef DEBUG
 mexPrintf("Inside mex function \n");
#endif

  /**************************************************
   Get the input variables
  *****************************************************/
  double v=*mxGetPr(prhs[0]);
    double *eigd=mxGetPr(prhs[1]);
   double *ediff=mxGetPr(prhs[2]);
 
 
   double *wmuproj=mxGetPr(prhs[3]);

   double *w=mxGetPr(prhs[4]);

   double dmuproj2=*mxGetPr(prhs[5]);
   double dmuproj=*mxGetPr(prhs[6]);
   double d=*mxGetPr(prhs[7]);

   double mag=*mxGetPr(prhs[8]);

   double *evecs=NULL;
   double *muk=NULL;

   if (nrhs>=10){
     evecs=mxGetPr(prhs[9]);
   }
   if (nrhs>=11){
     muk=mxGetPr(prhs[10]);
   }

   /*count the number of eigenvalues*/
   int length=mxGetNumberOfElements(prhs[4]);  

  
   

 
   int nsols=0;   
   double* pcon=NULL;
   double* qmax=NULL;
  double* xmax=NULL;
   
   //***************************************************
   // compute the coefficients of th equadratic Eqn. we solve to find \alpha= projection
   // of stimulus along unit vector in direction of mean
   //******************************************************
   double a=1;
   double b=0;
   double c=-pow(mag,2);
   
   for (int i=0; i< length;i++){
     double cl=1/pow((ediff[i]-v/(1-v)),2);

     a=a+.25*cl*pow(wmuproj[i],2);
     b=b+.25*2*wmuproj[i]*w[i]*cl;
     c=c+ .25*cl*w[i];
   }




#ifdef DEBUG
 mexPrintf("Computing solutions \n");
#endif

 /*check if there exist real solutions for pcon*/
 if((b*b-4*a*c)>0){

#ifdef DEBUG
 mexPrintf("2 Real solutions exist. \n");
#endif

 //create space for the outputs

    nsols=0;
    plhs[plind]=mxCreateDoubleMatrix(1,2,mxREAL);
    plhs[qlind]=mxCreateDoubleMatrix(1,2,mxREAL);
    pcon=mxGetPr(plhs[plind]);
    qmax=mxGetPr(plhs[qlind]);


    /*1st solution*/
    double qs=(-b - sqrt(b*b-4*a*c))/(2*a);
    /*make sure its valid*/
    if (abs(qs) <=mag){
        nsols=nsols+1;
        pcon[nsols-1]=qs;
    }
    /*2nd solution */
    qs=(-b+ sqrt(b*b-4*a*c))/(2*a);
    
    /*make sure its valid*/
    if (abs(qs) <=mag){
      nsols=nsols+1;
      pcon[nsols-1]=qs;
    }
 }
 else if (b*b-4*a*c==0){
    plhs[plind]=mxCreateDoubleMatrix(1,1,mxREAL);
    plhs[qlind]=mxCreateDoubleMatrix(1,1,mxREAL);
    pcon=mxGetPr(plhs[plind]);
    qmax=mxGetPr(plhs[qlind]);
#ifdef DEBUG
   mexPrintf("Only 1 solution \n");
#endif
   nsols=0;
   /*most 1 solution*/
    
   /*1st solution*/
   double qs=(-b /(2*a));
   //make sure its valid
   if (abs(qs) <=mag){
     nsols=nsols+1;
     pcon[nsols-1]=qs;
   }
 }
 else {

#ifdef DEBUG
   mexPrintf("No solutions. \n");
#endif
   /*no real solutions exist to the quadratic eqn for pcon*/
   nsols=0;
 }


 //********************************************************
 //Allocate space for xmax - the stimuli corresponding to the solutions
 // if we are returning xmax
 bool compy=false;


if (nlhs>=3){
  compy=true;
  plhs[2]=mxCreateDoubleMatrix(length,nsols,mxREAL);
  xmax=mxGetPr(plhs[2]);


//initialize xmax to zeros
  for (int i=0; i<length*nsols;i++){
    xmax[i]=0;
  }
	
}
 /********************************************************
 3. compute xmax,qmax for every valid solution */
 //ymax is an array of two pointers
 //each pointer points to one of the possible vectors

for (int sind = 0; sind < nsols; sind++)
  {
#ifdef DEBUG
    mexPrintf ("Pcon[%d]=%f \n", sind, pcon[sind]);
#endif

    //we compute qmax in place (i.e we don't allocate space to store y,xmax
    // unless we are returning those arguments
    qmax[sind] = 0;

    //temporary variables used to compute contribution of each term
    double wi = 0;
    double yi = 0;



    int xind = 0;
    int eind = 0;

    //multiply eigenvectors by projection along eigenvectors
    for (int i = 0; i < length; i++)
      {
	wi = pcon[sind] * wmuproj[i] + w[i];
	yi = -.5 * wi / (ediff[i] - v / (1 - v));
	//contribution from quadratic term
	qmax[sind] = qmax[sind] + pow (yi, 2) * eigd[i];
	//linear term
	qmax[sind] = qmax[sind] + wi*yi;

	//are we computing xmax
	if (compy)
	  {
	    //we multiply the appropriate eigenvector by yi
	    // and add it to xmax
	    for (int row = 0; row < length; row++)
	      {
		//row indexes the element of the eigenvector and xmax
		//matlab counts data columnwise
		xind = sind * length + row;
		eind = i * length + row;
		xmax[xind] = xmax[xind] + evecs[eind] * yi;		
	      }

	  }

    }
    
    //add in the projection along the mean
    if (compy){
    for (int row=0;row<length;row++){
    	xind=sind*length+row;
		xmax[xind] = xmax[xind] + pcon[sind] * muk[row];
    }
    }
    //include constant term effect
    qmax[sind] =qmax[sind] + pow (pcon[sind], 2) * dmuproj2 + pcon[sind] * dmuproj + d;
#ifdef DEBUG
    mexPrintf ("qmax[%d]=%f\n", sind, qmax[sind]);
#endif
  }


}

