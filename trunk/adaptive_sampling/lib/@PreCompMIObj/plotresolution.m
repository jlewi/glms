function [dist,fdist]=plotresolution(pobj,fname,trials)
if ~(exist('trials','var'))
    trials=[];
end

load(fname);

if (mparam.alength>0)
    error('Currently only handles alength=0');
end

mname='newton1d';
if isempty(trials)
    trials=1:simparammax.niter
end
niter=length(trials);
dist=ones(1,niter)*nan;
for j=1:length(trials)
    trial=trials(j);
    post.m=pmax.(mname).m(:,trial+1);
    post.c=pmax.(mname).c{trial+1};
    stiminfo=srmax.(mname).stiminfo{trial};

    %compute the modified quadratic form
    muk=post.m(1:mparam.klength);
%quad=quadmodshist(post.c,eigck,muk,shist);
    eigck=[];
    shist=[];
    if ~(isnan(stiminfo.dotmuopt) || isnan(stiminfo.sigmaopt))
    qobj=QuadModObj('C',post.c,'eigck',eigck,'muk',muk,'shist',shist);
%checkresolution(pobj,post,qobj,klength,shist,mmag,dotmu,sigmasq)
    dist(j)=checkresolution(pobj,post,qobj,mparam.klength,shist,mparam.mmag,stiminfo.dotmuopt,stiminfo.sigmaopt);
        
    end
end

fdist.hf=figure;
fdist.xlabel='trial';
fdist.ylabel='distance';
fdist.name='Stim. Distance';
ind=find(isnan(dist)==0)
plot(trials(ind),dist(ind),'b.','markersize',15);
lblgraph(fdist);