%function compmi(pobj, dotmu,sigmasq)
%       dotmu - values of dot product mean and stimulus
%       sigmasq - values of sigmasq
%
% Explanation- internal function called whenever we need to compute
% values of the objective function. PUrpose is to standardize the calls.
function [mi]=compmi(pobj,dotmu,sigmasq)

    mi=zeros(length(dotmu),length(sigmasq));    
    for j=1:length(dotmu)
    for k=1:length(sigmasq)
        fprintf('dotmu= %g\t sigmasq=%g \n',dotmu(j),sigmasq(k));
        [mi(j,k)]=pobj.mifunc(dotmu(j),sigmasq(k),pobj.glm,pobj.mifopts);
    end
    end