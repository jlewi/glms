%function [mival, gpoints]=lookup(dotmu,sigmasq)
%       dotmu    - value of dotmu
%       sigmasq - value of sigmasq
%
%Return value
%   dmind - indexes of the points in dotmu used to compute this value
%   sigind - indexes of points in sigmasq used to compute this point.
%
% Explanation: finds the adjacent grid points for which the value is
% precomputed. Returns as the value the average of those gridpoints.
%
%
% if point is outside the computed region we just return the value at the
% closest grid point
function [mival, dmind,sigind]=lookup(pobj,dotmu,sigmasq)

%get the indexes for dotmu
dmind=getind(pobj.dotmu,dotmu);
sigind=getind(pobj.sigmasq,sigmasq);

%take average of these 4 points
mival=pobj.mi(dmind(1),sigind(1))+pobj.mi(dmind(1),sigind(2))+pobj.mi(dmind(2),sigind(1))+pobj.mi(dmind(2),sigind(2));
mival=(1/4)*mival;

%*******************************************
%   darray - array of datavalues
%   dval - value to find close values in darray for
function [ind]=getind(darray, dval)
%do a binay search (because dotmu and sigmasq are sorted)
%to find the indexes of pobj.dotmu and pobj.sigmasq which are closest
%in value to this point and for which the values are known. 
%if dotmu < pobj.dotmu(1) or > pobj.dotmu(end)
%then dmind is the first(last) point for both indexes
ind=zeros(1,2);
if (dval>darray(1)&dval<darray(end))
    %binarysubdivide returns 
    %dval<darray(ind)
ind(2)=binarysubdivide(darray,dval);
ind(1)=ind(2)-1;    
end

%dval is outside the bounds of the array or equal to one of them
if (dval<=darray(1))
    ind=[1 1];
end
if (dval>=darray(end))
    ind(1)=length(darray);
    ind(2)=ind(1);
end