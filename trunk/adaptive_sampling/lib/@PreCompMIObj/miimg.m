%function [fobj]=miimg(pobj)
% 
%make a contour plot of the objective function
function [fobj]=miimg(pobj)
fobj.hf=figure;
fobj.xlabel='\mu_\epsilon';
fobj.ylabel='\sigma^2';
fobj.name='Numerical Comp. of Obj. Func';
fobj.title=fobj.name;
%contourf(muglm,sigmasq,log10(lobjsurf'));
contourf(pobj.dotmu,pobj.sigmasq,pobj.mi');
lblgraph(fobj);
colorbar;
