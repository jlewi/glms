%function checkresolution(pobj,quad,shist,mmag,dotmu,sigmaopt)
%
%
%Explanation: Thispoint looks up the point dotmu and sigmaopt
% in the grid. It computes the stimulus for that value and any neighbors in
% the grid.  It then computes the maximum difference between the otpimal
% stimulus at dotmu,sigmaopt and the stimulus at the neighbors
%Improvements: for speedup pass in the quad object rather than computing it
%each time
function [maxdist]=checkresolution(pobj,post,qobj,klength,shist,mmag,dotmu,sigmasq)
 optim = optimset('Jacobian','on','tolfun',10^-12);

 alength=length(shist);
 %neighbors
 neigh=[];
 n=0;
 %dotmu,sigmaopt should be a grid point
 pt.dmind=find(pobj.dotmu==dotmu);
 pt.sigind=find(pobj.sigmasq==sigmasq);
 
 muk=post.m;
 mumag=(muk'*muk)^.5;
 muk=muk;
 if isempty(pt.dmind)
     error('not a grid point');
 end
 if isempty(pt.sigind)
     error('not a grid point');
 end
 
 if (pt.dmind>1);
     n=n+1;
     neigh(n).dmind=pt.dmind-1;
     neigh(n).sigind=pt.sigind;
 end
 
  
 if (pt.dmind<length(pobj.dotmu));
     n=n+1;
     neigh(n).dmind=pt.dmind+1;
     neigh(n).sigind=pt.sigind;
 end
 
  
 if (pt.sigind>1);
     n=n+1;
     neigh(n).dmind=pt.dmind;
     neigh(n).sigind=pt.sigind-1;
 end
 
  
 if (pt.sigind<length(pobj.sigmasq));
     n=n+1;
     neigh(n).dmind=pt.dmind;
     neigh(n).sigind=pt.sigind+1;
 end
 
 %lookup the indexes in the matrix
%[mival, dmind,sigind]=lookup(dotmu,sigmasq);

%if (length(dmind)<=1)
%    error('why was only 1 pt returned');
%end
%if (length(sigind)<=1)
%    error('why was only 1 pt returned');
%end

   %neded to solve for the actual stimulus
    %so we need to find a linear combination of xmax, xmin which gives
    %sigmaopt
    A=post.c(1:klength,1:klength);
    if (alength>0)
        b=shist'*post.c(klength+1:end,1:klength);
        d=shist'*post.c(klength+1:end,klength+1:end)*shist;
    else
        b=zeros(klength,1);
        d=0;
    end
    
%optimal stimuli for gridpoints
%stim=zeros(1,length(dmind)*length(sigind))
%sind=1;
for j=1:n
        %we can't compute the optimal stimulus for the node if its not
        %fesible
        muproj=pobj.dotmu(neigh(j).dmind)/mumag;
        if (abs(muproj)>mmag)
            %its not valid
            neigh(j).isvalid=0;
        else
            [qmax,qmin,xmax,xmin]=maxminquad(qobj,muproj,mmag,optim);
            if (pobj.sigmasq(neigh(j).sigind)<qmin ||pobj.sigmasq(neigh(j).sigind)>qmax)
                neigh(j).isvalid=0;
            else
                neigh(j).isvalid=1;
        fs=@(s)stimsigma(s,pobj.sigmasq(neigh(j).sigind),A,b,d,xmin,xmax);
        [sopt,fzero]=fsolve(fs,.5,optim);
        neigh(j).stim=(1-sopt)*xmin+sopt*xmax;
            end
        end
        
end

%compute the optimal stimulus at the actual point
muproj=dotmu/(mumag);
   [qmax,qmin,xmax,xmin]=maxminquad(qobj,muproj,mmag,optim);
        fs=@(s)stimsigma(s,pobj.sigmasq(pt.sigind),A,b,d,xmin,xmax);
        [sopt,fzero]=fsolve(fs,.5,optim);
        pt.stim=(1-sopt)*xmin+sopt*xmax;

%compute the distance of the stimuli at the point from its negihbors
ind=find([neigh.isvalid]==1);

if isempty(ind)
    maxdist=nan;
else
dist=pt.stim*ones(1,length(ind))-[neigh(ind).stim];
dist=sum(dist.^2,1).^.5;
[maxdist]=max(dist);
end
%***************************************
%function we need to solve to find stimulus which gives (muopt,
%sigma^2_opt)
%see aistat eqn 24.
function [d,jac]=stimsigma(s,sigmasqopt,A,b,d,xmin,xmax)
x=(1-s)*xmin+s*xmax;

sigmasq=x'*(A*x)+b'*x+d;
d=sigmasq-sigmasqopt;
%return the jacobian
jac=(2*x'*A+b')*(-xmin+xmax);