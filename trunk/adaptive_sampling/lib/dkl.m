%function [dk,param,var]=dkl(q,po, ll, param)
%   q- is gaussian with  
%       q.mu - mean of gaussian
%       q.c  -covariance matrix   
%
%   po - pointer to function which computes the LOG likelihood of the 
%        prior on the model
%        -this will change on what model we're computing the KL divergence
%        with regards to
%        .func - pointer to function
%        .param - cell array giving parameters to pass
%   ll - structure which contains all information regarding the likelihood
%        of the models
%        .obsrv - y x n matrix
%               - gives the N observations so far
%        .llfunc(obsrv, m) - pointer to function which computes
%                           the log likelihood of the observations under
%                           the models
%        .param -paremeters for the liklelihood function
%        .input - d x n matrix
%                 gives the input on each trial
%   param - optional parameters
%       .nsamp - number of samples to use 
%       .z     - matrix d x nsamp
%              - i.i.d samples of d dimensional gaussian
%                with unit covariance, and zero mean
%                this will be used to generate the samples
%
%   Return Value:
%       dk - kL divergence
%       param - contains the random points so they can be reused
%           .nsamp
%           .z
%       vsum -variance of the integration estimated from the sample
% Explanation: Compute the KL divergence between q and p
%   q- is gaussian with  
%       q.mu - mean of gaussian
%       q.c  -covariance matrix

function [dk,param,vsum]=dkl(q,po,ll,param)

%dimensionality of q
d=size(q.mu,1);

%*************************************************************
%error checking/defaults
if (~(exist('param','var')))
    param=[];
end

if (~(isfield(param,'nsamp')))
    param.nsamp=[];
end


if (~(isfield(param,'z')))
 param.z=[];
end

if isempty(param.nsamp)
    param.nsamp=10000;
end

if ~isempty(param.z)
    z=param.z;
    nsamp=size(z,2);
    param.nsamp=nsamp;
else
    z=randn(d,param.nsamp);
    param.z=z;
	nsamp=param.nsamp;
end
   
%**************************************************************************
%generate M samples from our gaussian q
%These our samples of possible models
%we do this by using samples from a gaussian with unit covariance and zero
%mean (that is z)
%Compute the cholesky decomposition
[evec,eval] = eig(q.c);
U=evec*eval.^(.5);

m=q.mu*ones(1,nsamp)+U*z;

%**************************************************************************
%We need to compute the true posterior at various model samples
%So for each model sample, we need to compute the prior on that model
%and we also need to compute the likelihood of all data observed under that
%model

%We want to compute the likelihood of the all data observed under each of the
%model samples we have drawn
%%mll should be 1xml vector which gives the loglikelihood of ALL THE DATA
%under each of our sample models
args=[{ll.obsrv},{ll.input},{m},ll.param];
mll=callfunc(ll.func,args);

%compute the initial prior (that is pt-1(\theta)) for each of our model samples
%mpo should be 1xml vector specifying log likelihood 
%of the initial prior on each model.
%concatenate m to the argument list and call the function
arg=[{m},po.param];
mpo=callfunc(po.func,arg);



%************************************************************************
%compute the normalization constant of p(m)
%use importance sampling
%probability of our model samples under true gaussian
%
%Keep everything in the log domain to avoid numerical issues
%associated with really large negative exponents which give zeros
mq=logmvgauss(m,q.mu,q.c);

%pull out a normalization constant on qm
%approximate the integral of q(m)*log(p(m))dm using monte carlo sampling
%We don't multiply by q(m) this is because we are adding up samples
%This assumes mpo and mll are the log likelihoods not the likelihood
lsum=mpo+mll;
intapprox=1/nsamp*sum(lsum,2);

%normalization constant
%numerical issues
%when mq=0 exp(mp0+mll) should also be close to zero because
%mq is the gaussian approximation of the true posterior
%which is exp(mp0+mll)
%to prevent numerical errors from 0/0
%find mq=0. Set mq=1. And the associated numerators to 0.
%this way the entries end up being zero and not contributing to the
%divergence which I believe is the thing to do.
ind=find(mq==0);

%to avoid numerical errors we need to pull out a factor
%before we compute the normalization constant otherwise it will be zero
%when take exponent
logptot=mpo+mll;
%mfact=max(mtot);

%pest=exp(mpo+mll-mfact);
%if ~isempty(mq)
    %check those entries of pest to make sure they are zero
%    ind2=find(pest(ind)<=10^-16);
 %   if (length(ind2)==length(ind))
  %      pest(ind)=0;
   %     mq(ind)=1;
    %else
     %   warning('Get Divide by zero! in importance sampling !');
        %set it anyway
      %  pest(ind)=0;
      %  mq(ind)=1;
    %end
%end
%compute the importance of the samples
lisamp=logptot-mq;

%now we need to sum the probabilities
%but to avoid numerical issues(0's) we pull out the largest value
mfact=max(lisamp);
lisamp=lisamp-mfact;       
logznorm=-log(nsamp)+mfact+log(sum(exp(lisamp),2));


%***********************************************************************
%compute the entropy of q
if (length(q.mu)>1)
    hq=1/2*log((2*pi*exp(1))^length(q.mu)*det(q.c));
    %entropy should use base 2
    %hq=1/2*log2((2*pi*exp(1))^length(q.mu)*det(q.c));
else
    hq=1/2*log((2*pi*exp(1))*q.c);
    %entropy should use base 2
    %hq=1/2*log2((2*pi*exp(1))*q.c);
end
%have to add mfact back
dk=logznorm-intapprox-hq;

%compute the variance of the integral approximation of 

vsum=1/nsamp^2*sum((mpo+mll-intapprox).^2);

return;

%check the log likelihood for the Guassian model
figure;
hold on;
plot(m,mll,'.')
truell=zeros(1,length(m));
for index=1:length(m)
    
    truell(index)=sum(log(normpdf(ll.obsrv,m(index),1^.5)));
end
plot(m,truell,'r.');

%check that the prior probability is the same
figure;
title('Prior Probability')
hold on;
plot(m,mpo,'.')
truelpo=log(normpdf(m,po.param{1},po.param{2}^.5));
plot(m,truelpo,'r.');


%some debugging code
figure;
plot(m,-logznorm+logptot,'.')
hold on
plot(m,log(normpdf(m,q.mu,q.c^.5)),'r.')
xlabel('\theta');
ylabel('log p(\theta)');



figure
hold on;
h1=plot(m,truell,'b.');
h2=plot(m,truelpo,'g.');
h3=plot(m,truell+truelpo-logznorm,'r.');
h4=plot(m,log(normpdf(m,q.mu,q.c^.5)),'k.');
legend('Log Likelihood','Log Prior','Log likelihood + Log prior','Log Posterior');
xlabel('\theta');
ylabel('P');

%compute the true joint
trueljoint=zeros(1,length(m));

llsigma=1;      %variance of likelihood

for index=1:length(m)
    trueljoint(index)=-1/2*(2*log(2*pi)+log(po.param{2})+log (llsigma));
    trueljoint(index)=    trueljoint(index)-(m(index)^2-2*m(index)*po.param{1}+po.param{1}^2)/(2*po.param{2});
    trueljoint(index)=    trueljoint(index)-(ll.obsrv^2-2*ll.obsrv*m(index)+m(index)^2)/(2*llsigma);
end

%compute true value of the integral for gaussian prior + gaussian
%observations
prior.c=po.param{2}; %variance of prior
prior.m=po.param{1};
like.c=ll.param{1};
tpost.m=q.mu;
tpost.c=q.c;
n=1;                    %number observations
inttrue=-1/2*log(2*pi*prior.c)-1/(2*prior.c)*(tpost.c+tpost.m^2-2*tpost.m*prior.m+prior.m^2);
inttrue=inttrue+-n/2*log(2*pi*like.c)-1/(2*like.c)*(ll.obsrv^2-2*ll.obsrv*tpost.m+tpost.c+tpost.m^2);
%compute the true normalizing constant
%this is just marginal distribution of the data evaluated for the data
%truenormc=