%function stats=comp(bdata,model);
%   bdata - BSdata object for which we want to compute the data
%
% Return value:
%   stats - the mean and covariance of each coefficient
%
%   
%Explaantion:
%
function [stats,info]=comp(bdata,windexes)


strfdim=getstrfdim(bdata);

%no bias and no spike history terms
%because this weill allow us to use bslogpost to compute the projection 
%of each wave file on each basis vector
alength=0;
hasbias=0;

glm=GLMPoisson('canon');

    nmax=ModBSSinewaves.maxn([strfdim(1),strfdim(2)]);

    mobj=ModBSSinewaves('nfreq',nmax(1),'ntime',nmax(2),'glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',hasbias);
    

%only use 1 repeat because the stimulus is the same on each repeat of a
%wafile
bspost= BSlogpost('bdata',bdata,'maxrepeats',1,'windexes',windexes);


%keep track of the sufficient statistics
wstats=struct('amp',struct('sum',[],'sum2',[]),'phase',struct('sum',[],'sum2',[]),'ntrials',0);
wstats=repmat(wstats,1,length(windexes));
%store the amplitude of each component
stats.amp=struct('sum',[],'sum2',[],'mean',[],'var',[]);

stats.ntrials=0;
%loop over the wavefiles
for wind=rowvector(windexes)
    fprintf('Processing wave file %d \n', wind);
    [wstats(wind)]=BSStimStats.processwfile(mobj,bspost,wind);
end

amp=[wstats(:).amp];
phase=[wstats(:).phase];

stats.ntrials=sum([wstats(:).ntrials]);

ampsum=cat(3,amp.sum);
phasesum=cat(3,phase.sum);

stats.amp.mean=sum(ampsum,3)/stats.ntrials;
stats.phase.mean=sum(phasesum,3)/stats.ntrials;

ampsum2=cat(3,amp.sum2);
ampsum2=sum(ampsum2,3);

phasesum2=cat(3,phase.sum2);
phasesum2=sum(phasesum2,3);

%use the unbiased estimator
stats.amp.var=ampsum2/(stats.ntrials-1)-stats.ntrials/(stats.ntrials-1)*stats.amp.mean.^2;
stats.phase.var=phasesum2/(stats.ntrials-1)-stats.ntrials/(stats.ntrials-1)*stats.phase.mean.^2;

info.mobj=mobj;
info.windexes=windexes;