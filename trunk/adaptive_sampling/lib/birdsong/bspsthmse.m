%function mseerr=bspsthmse(bssimobj,theta,wfile)
%   bssimobj - the simulation object
%   theta - theta
%   wfile - index of the wave file
%
% Return value:
%   mseerr - the mean squared error between the empirical and predicted
%   wave files
%   exprate - the expected firing 
%   emprate - the empirical firing rate
%
%Revision:
%   07-23-2008
%       take as input a bssimobj
function [mseerr,exprate,emprate]=bspsthmse(bssim,theta,wfiles)


%data=load(getpath(datafile));
bdata=getbdata(bssim.stimobj);
allpost=bssim.allpost;
mobj=bssim.mobj;



mseerr=zeros(1,length(wfiles));

%********************************************************
%end post processing
%************************************************************
wcount=0;
for wavfile=wfiles
    wcount=wcount+1;

    %get the stimulus and responses for this wavefile
    [bdata,stimspec,obsrv]=getallwaverepeats(bdata,wavfile);

    [ntrials,cstart]=getntrialsinwave(bdata,wavfile);
    meanobsrv=mean(obsrv,1);
    avgspike=meanobsrv(:,cstart:end);


    bspost=BSlogpost('bdata',bdata);

    %compute the expected firing rate
    [bdata,stimspec]=getfullspec(bdata,wavfile);

    glmproj=compglmprojimp(bspost,mobj,theta,stimspec,meanobsrv,ntrials);
    frate=compmu(getglm(mobj),glmproj(1,:));
    warning('We only use a single repeat, a better way might be to take the average of the spike spike histories across repeats');

    if (length(wfiles)>1)
        exprate{wcount}=frate;
        emprate{wcount}=avgspike;
    else
        exprate=frate;
        emprate=avgspike;
    end
    %compute the mse
    mdist=(sum((frate-avgspike).^2)/length(frate))^.5;
    mseerr(wcount)=mdist;

end