%function wavinfo=gettestset(bssim)
%
%Return value:
%   wavinfo - structure array describing the wave files in the test set
%             the indexes of the wavefiles on which we did not train
%
function wavinfo=gettestset(obj)
error('access winfo directly');