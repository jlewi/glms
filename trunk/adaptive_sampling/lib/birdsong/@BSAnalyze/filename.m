%function msefile=filename(datafile,suffix)
%   
%Explanation: Generate a filename for the file to store the results by
%   appending suffix to the filename
function msefile=filename(datafile,suffix)

    [rdir rfname rext]=fileparts(getrpath(datafile));
    rpath=fullfile(rdir,[rfname, '_', suffix,rext]);
    msefile=FilePath(getbcmd(datafile),rpath);