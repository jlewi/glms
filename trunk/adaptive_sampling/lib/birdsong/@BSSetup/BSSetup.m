%function sim=BSSetup(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: A static class for setting up bird song experiments
%
classdef (ConstructOnLoad=true) BSSetup
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        version=081008;

    end

    methods (Static)
        %***************************************************
        %Main functions to initialize different simulations
        %*****************************************************
        %the main function to setup an experiment
        [data,otbl,ofile,bssimobj]=setupinfomax(varargin);

        %setup a simulation to find the MAP
        [data,otbl,ofile]=setupfitpoiss(varargin);

        %********************************************************
        %Helper functions: initialize different pieces of the model
        %**********************************************************
        prior=priornormal(mobj,penalty);

        %initialize the Gaussian prior for the fourier model
        prior = priorfourier(mobj,cutoff);

        %initialize the model
        mobj=initmodel(param,bdata);

        %intialize the stimulus object
        stimobj=initstimobj(param,bdata);
        
        %intialize the bsdata object
        bdata=initbsdata(param);
        
        %initialize the updater object
        updater=initupdater(param);
        
        %initialize the files for the simulation
        [files,fnum]=initfiles(odir,fbname);
       
        allpost=initgausspostfile(param,allfiles,mobj);
        %initialize parameters to the defaults
        param=defaultparam(param);
        %****************************************************************
        %Compute the prior for the stimulus coefficients using the stimulus
        %statistics
        %******************************************************************
        fname=compfouriersoftprior(bdata);
        fname=compftsepsoftprior(bdata,cinitrange,pcutoff);
        
    end
    methods
        function obj=BSSetup(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={};




            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=struct();
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=struct();

                otherwise
                    if isnan(cind)
                        error('no constructor matched');
                    else
                        %remove fields which are for this class
                        try
                            bparams=rmfield(params,'field');
                        catch
                        end
                    end
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 0
                    %do nothing used by load object

                otherwise
                    error('Constructor not implemented')
            end



        end
    end
end


