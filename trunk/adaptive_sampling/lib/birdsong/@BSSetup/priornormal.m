%function prior=priorfourier(mobj,param)
%   mobj - ModBSSineWaves object
%   param - structure containing prior variance for each component.
%       .stimvar
%       .shistvar
%       .biasvar
%Revisions
%   2-12-2009 - Instead of passing in penalty pass in a structure which
%       could have a field penalty
function prior=priornormal(mobj,param)


%we add 1 for the bias term
dim=getparamlen(mobj);
cinit=nan(dim,1);

cinit(mobj.indstim(1):mobj.indstim(2))=param.stimvar;

if (mobj.alength>0)
    cinit(mobj.indshist(1):mobj.indshist(2))=param.shistvar;
end
if (mobj.hasbias)
    cinit(mobj.indbias)=param.biasvar;
end

% if ~isempty(spikeavg)
%     %add a zero for the bias term.
%     minit=[spikeavg(:);0];
% else
%     minit=zeros(dim,1);
% end
minit=zeros(dim,1);
prior=GaussPost('m',minit,'c',diag(cinit));


if isfield(param,'penalty')
    prior=smoothingprior(prior,penalty,mobj);

    %rescale the eigenvalues of the prior covariance
    cprior=getc(prior);
    [u s v]=svd(cprior);
    cprior=u*cfinalscale*s*v';
    prior=setc(prior,cprior);
end