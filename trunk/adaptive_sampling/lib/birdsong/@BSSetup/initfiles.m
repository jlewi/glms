%function [files,fnum]=initfiles(odir,fbname)
%     odir   - output directory
%     fbname - the base name for the files
%
%Explanation:
%   Creates the files needed for the simulation.
%   Makes sure the directories exist.
function [data,fnum]=initfiles(odir,fbname)
%*************************************************************************
%Parameters for the job
%**************************************************************************

setupfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_setup.m',fbname)));


datafile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_data.mat',fbname)));

mfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_mfile.dat',fbname)));
cfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_cfile.dat',fbname)));

%status file contains status information
statusfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_status.txt',fbname)));



[data.setupfile fnum]=seqfname(setupfile);
%use fnum returned by setupfile so the number suffix for all files is the
%same and matches the number attached to the setup file
data.datafile=seqfname(datafile,[],fnum);
data.mfile=seqfname(mfile,[],fnum);
data.cfile=seqfname(cfile,[],fnum);
data.statusfile=seqfname(statusfile,[],fnum);


files=[data.mfile data.cfile data.statusfile];

%********************************************************************
%create the directories for the files if the do not exist
files=[files data.datafile setupfile];
for j=1:length(files)
    [fdir]=fileparts(getpath(files(j)));
    recmkdir(fdir);
end
