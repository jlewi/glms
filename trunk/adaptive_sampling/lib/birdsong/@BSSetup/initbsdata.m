
%function bdata=initbsdata([])
%   create a bdata initialized to the default values
%function bdata(param)
%   param structure with following field
%       rawdatafile
%       stimnobsrvwind
%       freqsubsample
%       obsrvwindowxfs
function bdata=initbsdata(param)
%
if ~exist('param','var')
    param=struct();
end

%    fprintf('Using default values \n');
if ~isfield(param,'rawdatafile')
    param.rawdatafile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1_corrected.mat'));
end
if ~isfield(param,'stimnobsrvwind')

    param.stimnobsrvwind=10;
end
if ~isfield(param,'freqsubsample')
    param.freqsubsample=2;
end

if ~isfield(param,'obsrvwindowxfs');
    param.obsrvwindowxfs=250;
end
bdata=BSData('fname',param.rawdatafile,'stimnobsrvwind',param.stimnobsrvwind,'freqsubsample',param.freqsubsample,'obsrvwindowxfs',param.obsrvwindowxfs);
