%function mobj=initmodel(param,bdata)
%   param - struture array contianing the different parameters
%       .tanparam - parameters for the tangent space
%   bdata - the bdata object
function mobj=initmodel(param,bdata)


%klength=prod(getstimdim(bdata));
strfdim=getstrfdim(bdata);
%mobj=MLogistic('alength',alength,'klength',klength,'mmag',1);

mparam=struct();

mparam.glm=param.glm;
mparam.alength=param.alength;
mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.mmag=1;
mparam.hasbias=1;

switch lower(param.model)
    case 'modbssinewaves'


        %use all frequencies
        mparam.nscale=1;

        nmax=ModBSSinewaves.maxn(strfdim);
        mparam.nfreq=nmax(1);
        mparam.ntime=nmax(2);
        switch param.stimtype


            case {'BSBatchPoissLBTan','BSTanSpaceInfoMax'}
                mparam.rank=param.tanparam.rank;
                mobj=MBSSineLowRank(mparam);
            otherwise
                mobj=ModBSSinewaves(mparam);
        end

    case 'mbsftsep'
        %copy additional parameters
        mparam=copystruct(mparam,1,param.mparam);
        switch param.stimtype


            case {'BSBatchPoissLBTan','BSTanSpaceInfoMax'}
                mparam.rank=param.tanparam.rank;
                mobj=MBSFTSepLowRank(mparam);
            otherwise
                mobj=MBSFTSep(mparam);
        end

    case 'mbsftseplowrank'
        
          %copy additional parameters
        mparam=copystruct(mparam,1,param.mparam);
        switch param.stimtype
            case {'BSBatchPoissLBTan','BSTanSpaceInfoMax'}
                mparam.rank=param.tanparam.rank;
                mobj=MBSFTSepLowRank(mparam);
            otherwise
                    error('wrong stimulus chooser type.');
        end        
    otherwise
        if isfield(mparam,'stimtype')
        switch mparam.stimtype
            case {'BSBatchPoissLBTan','BSTanSpaceInfoMax'}
                mparam.rank=param.tanparam.rank;
                mobj=MTLowRank(mparam);
            otherwise
                mobj=MParamObj(mparam);
        end
        else
                 mobj=MParamObj(mparam);
        end
end