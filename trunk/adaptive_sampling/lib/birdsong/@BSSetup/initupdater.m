%function updater=initupdater(param)
%   param.updater - structure describing the updater
%           .type - type
%           .otherfields - other fields to pass to constructor of updater
function updater=initupdater(param)

%**************************************************************************
%updater
%************************************************************************
if ~isempty(param.updater)
    cname=param.updater.type;
    uparam=rmfield(param.updater,'type');
    
    eval(sprintf('updater=%s(uparam);',cname));
else
switch param.stimtype
    case {'BSTanSpaceInfoMax','BSBatchPoissLBTan'}
        updater=TanSpaceUpdater('compeig',param.updater.compeig);
    otherwise
        updater=Newton1d('compeig',param.updater.compeig);
end
end