%function [data,otbl,ofile]=setupfitpoiss(varargin);
%
%Revisions:
%   10-18-2008
%         made the script setupfitpoiss a static function of BSSetup
%
%08-06-2008
%   started revising the script to create setup of poisson model
%   decided to create a separate script.
%
%4-04-2008
%
%Explanation: This script kicks off a distributed job on the cluster
%   to fit a GLM to the bird song data
function   [allfiles,otbl,ofile]=setupfitpoiss(varargin)

switch nargin
    case 0
        param=struct();
    case 1
        param=varargin{1};
    otherwise
        %load the fields passed in into a structure
        for j=1:2:nargin
            param.(varargin{j})=varargin{j+1};
        end
end

%****************************************************************
%initialize the default parameters
%******************************************************************
param=BSSetup.defaultparam(param);

%directory where we will store the data, relative to RESULTSDIR
odir=fullfile('bird_song',datestr(now,'yymmdd'));


%******************************************************
%create the bsdata object
%******************************************************
%lenght of the window in which we obsrve the responses
%in seconds
bparam.obsrvwindowxfs=param.obsrvwindowxfs;
bparam.rawdatafile=param.rawdatafile;
bparam.stimnobsrvwind=param.stimnobsrvwind;
bparam.freqsubsample=param.freqsubsample;
bdata=BSSetup.initbsdata(bparam);

%************************************************************
%create the model
%***************************************************************
%number of spike history terms
mobj=BSSetup.initmodel(param,bdata);


%**********************************************************************
%create the updater
%***********************************************************************
updater=BSBatchFitGLM('bdata',bdata,'mclass',class(mobj),'windexes',param.windexes,'maxrepeats',param.stimparam.nrepeats);

if isfield(param,'maxiter')
   updater.optim.MaxIter=param.maxiter; 
end
%***********************************************************************
%the names of the files to store the results
%**********************************************************************
[datafiles,fnum]=BSSetup.initfiles(odir,'bsglmfit');




%************************************************************************
%Have user verify that parameters are correct
%***********************************************************************

% r=input('Is this correct(y/n)?','s');
% switch lower(r)
%     case {'y','yes'}
%         %continue
%     otherwise
%         fprintf('Setup aborted on user request. \n');
%         return;
% end





%**************************************************************************
%Wave files
%************************************************************************
%we create a wavefile field of dsets with the path to each file
%we do this so that when we run on the cluster we copy the wave files to
%the cluster
wavefilesdir=getstimdir(bdata);




%***********************************************************************
%create the fmapiter file
%************************************************************************
%Not sure what this was used for
%I think the stored the estimated theta on every run of fminunc
% allfiles.fmapfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,'bsglmfit_fmapiter.dat'));
% allfiles.fmapfile=seqfname(allfiles.fmapfile,[],fnum);
% fmapiterf=RAccessFile('fname',allfiles.fmapfile,'dim',[getparamlen(mobj),1]);
fmapiterf=[];


%************************************************************************
%Create the GaussPostfile object using the prior and save it to gpostfile
%************************************************************************
prior=BSSetup.initgausspostfile(param,datafiles,mobj);


%create the BSBatchMLSim object
bssimobj=BSBatchMLSim('updater',updater,'prior',prior,'mobj',mobj);

%************************************************************************
%save the datav=load('/home/jlewi/svn_glms/allresults/bird_song/081028/bsinfomax_data_004.mat') to a file
%************************************************************************
save(getpath(datafiles.datafile),'bssimobj');



%************************************************************************
%create an array of all the files we need to copy to the cluster
%***********************************************************************
allfiles=datafiles; 
allfiles.wavefilesdir=wavefilesdir;
allfiles.rawdatafile=bparam.rawdatafile;
allfiles.fmapiterf=fmapiterf;
%save the number of windexes so that when we kick off the job we no how
%many workers to use
allfiles.nwindexes=length(bssimobj.updater.windexes);

%*************************************************************************
%Create the status file
%***********************************************************************
%If we don't create the status file than seqfname may return an inccorect
%filename on subsequent calls

    fid=fopen(getpath(allfiles.statusfile),'a');
    fprintf(fid,'Status file. \n');
    fprintf(fid,'stimnobsrvwind=%d\n',bparam.stimnobsrvwind);
    fprintf(fid,'obsrvwindowxfs=%d\n',bparam.obsrvwindowxfs);
    fprintf(fid,'freqsubsample=%d\n',bparam.freqsubsample);
    fclose(fid);
    
    
%information
info.scriptname=getpath(allfiles.setupfile);
info.stimnobsrvwind=bparam.stimnobsrvwind;
info.freqsubsample=bparam.freqsubsample;
info.strfdim=getstrfdim(bdata);
info.obsrvwindowxfs=bparam.obsrvwindowxfs;
info.klength=info.strfdim(1);
info.ktlength=info.strfdim(2);
info.alength=mobj.alength;

%output the information to an mfile
writeminit(allfiles.setupfile,allfiles,info);

%**********************************************************
%create an xml file to describe this experiment
%***********************************************************
otbl={'setupfile',getpath(allfiles.setupfile);'datafile',getpath(allfiles.datafile);'mfile',getpath(allfiles.mfile);'cfile',getpath(allfiles.cfile);'neuron',getfilename(bdata.fname)};
otbl=[otbl;{'STRF Dim', getstrfdim(bdata);'# spike history terms', mobj.alength;}];
if isa(mobj,'MBSFTSep')
    [nf,nt]=getmaxnfnt(mobj);
     otbl=[otbl;{'largest nfreq',nf; 'largest ntime',nt}];
end
otbl=[otbl;{'Observation window (how many samples)', bparam.obsrvwindowxfs;'Stimulus Duration (multiple of observation window)', bparam.stimnobsrvwind; 'stimulus duration (s)', getstimtlength(bdata); 'Observation Window(s)',getobsrvtlength(bdata); 'Freq Subsample', bparam.freqsubsample; 'GLM',mobj.glm}];
otbl=[otbl;{'Simulation Object', bssimobj}];


ofile=seqfname(FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,'bsglmfit.xml')),[],fnum);
onenotetable(otbl,getpath(ofile));