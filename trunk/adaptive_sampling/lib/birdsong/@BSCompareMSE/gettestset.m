%function wavinfo=gettestset(bssim)
%
%Return value:
%   wavinfo - structure array describing the wave files in the test set
%             the indexes of the wavefiles on which we did not train
%
function wavinfo=gettestset(bssim)
bdata=bssim.stimobj.bdata;
wavinfo=[];

%determine which wave files we didn't train on
testwind=ones(1,getnwavefiles(bdata));
testwind(bssim.stimobj.windexes)=0;
testwind=find(testwind~=0);

%check if we haven't stored info about the wavefile in wavinfo
for ind=1:length(testwind)

    wind=length(wavinfo)+1;
    wavinfo(wind).wind=testwind(ind);
    wavinfo(wind).issong=waveissong(bdata,wavinfo(wind).wind);

end