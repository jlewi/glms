%function deletedset(msefile,dind)
%   msefile - The file where we want to delete dind
%   dind    - the index of the dataset to delete
function deletedset(msefile,dind)

v=load(getpath(msefile));

if (dind>length(v.dsets))
    error('dind exceeds the number of datasets in the file');
end

data=[v.data(1:dind-1) v.data(dind+1:end)];
dsets=[v.dsets(1:dind-1) v.dsets(dind+1:end)];

save(getpath(msefile),'data','dsets','-append');
