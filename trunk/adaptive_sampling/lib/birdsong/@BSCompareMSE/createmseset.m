%function [msefile,dind]=createmseset(dnew,msefile)
%   dnew    - structure describing the mse computation we want to compute
%   msefile - The file where we want to save the data
%
%
%Return value:
%   msefile - The file containing the data
%   dind    - The index of the datset in the file
%
%Explanation: The file msefile has the following variables
%   dsets - a Structure array each element describes a different dataset
%   data  - A structure array . Each element is the data for a
%           different dataset
%           data{1}.mse - structure array of the mse and the corresponding
%                         trial and wavefile
%           data{1}.wavinfo - Structure array describing the wave files in
%                            the test set for this dataset
%
%   This function initializes an element of dsets and data for a new
%   dataset, we can then call processdataset to process the data set in the
%   file
%
function [msefile,dind]=createmseset(dnew,msefile)

%indicates whether dataset is already in the file
isnew=true;

if ~exist('msefile','var')
    msefile=[];
end

if isempty(msefile)
  msefile=BSCompareMSE.msefname(dnew.datafile);
   
end

if exist(getpath(msefile),'file')
    [dind]=BSCompareMSE.findsetinfile(dnew,msefile);
    
    if isnan(dind)
        isnew=true;
    else
        isnew=false;
    end
     v=load(getpath(msefile),'data','dsets');
     data=v.data;
     dsets=v.dsets;
else
    isnew=true;
    data=struct([]);
    dsets=struct([]);
end

if (isnew)
    dind=length(dsets)+1;

    %append the datset to the end
    dsets=copystruct(dsets,dind,dnew);

    data(dind).mse=[];
    data(dind).wavinfo=[];

    if (exist(getpath(msefile),'file'))
        save(getpath(msefile),'data','dsets','-append');
    else
        save(getpath(msefile),'data','dsets');
    end
    

end