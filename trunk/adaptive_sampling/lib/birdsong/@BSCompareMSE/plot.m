%function plot(dsets,pstyles)
%   dsets - A structure array of the datasets to process
%   pstyles - optional a PlotStyles object defining the styles to use
%
%Explanation: Make a plot of the MSE
function [fmse,otbl]=plot(dsets,pstyles)

if ~exist('pstyles','var')
    pstyles=[];
end
if isempty(pstyles)
    pstyles=PlotStyles();
         pstyles.linewidth=pstyles.linewidth*2;
        pstyles.markersize=4;
end

%*************************************************************
%get the data for each set
%***************************************************************
for dind=1:length(dsets)
    [dinfile,msefile]=BSCompareMSE.findsetinfile(dsets(dind));
    if isnan(dinfile)
       [msefile,dinfile]=BSCompareMSE.createmseset(dsets(dind),msefile); 
    end
    [alldata(dind)]=BSCompareMSE.processdataset(dinfile,msefile);
    msefiles(dind)=msefile;
end
%**************************************************************
%Make the plots
%************************************************************
%%
%wavefiles in the test set
wavinfo=[alldata.wavinfo];

[wind,index]=unique([wavinfo.wind],'first');
wavinfo=wavinfo(index);

nplots=length(wavinfo);
clear fmse;


for wind=1:length(wavinfo)
    %plot the mse for al the files in the test set
    fmse(wind)=FigObj('name','mse','width',6,'height',6,'xlabel','trial','ylabel', 'log (M.S.E^.5)', 'title', ['Wave file=' num2str(wavinfo(wind).wind)] );

    %loop over the simulations
    pind=0;
    for dind=1:length(dsets)
        pind=pind+1;
        setfocus(fmse(wind).a);

        %find the data points for this wavefile
        mseind=find([alldata(dind).mse.wavefile]==wavinfo(wind).wind);

        %mse as a function of trial
        mse=[alldata(dind).mse(mseind).mse];
        ttrial=[alldata(dind).mse(mseind).trial];

        [ttrial ind]=sort(ttrial);
        mse=mse(ind);

        hp=plot(ttrial,mse);

        %             [c m]=getptype(pind,2);
        %             pstyle.marker='o';
        %             pstyle.markerfacecolor=c;
        %             pstyle.linestyle=m;
        %             pstyle.color=c;
        %             pstyle.markersize=4;
        %             pstyle.linewidth=5;
        ps=pstyles.plotstyle(pind);
   
        addplot(fmse(wind).a,'hp',hp,'lbl',dsets(dind).lbl,'pstyle',ps);
    end


    set(gca,'xscale','log');
    lblgraph(fmse(wind));
end
    


%%
otbl={};

%create the table of output information
%info about the fields
scriptname=mfilename('fullpath');
explain={'wave file', 'A number identifying the wave file';'script',scriptname};
explain=[explain;{'explanation','This plot compares the MSE between the predicted and actual PSTH for a test set for several different designs and various amounts of data. The plot labeled "Info. Max. Tanspace:Tan Post" uses the posterior with the variance orthogonal to the tangent space set to zero to compute the M.S.E. The plot labeled "Info. Max.: Tan Space" used the full posterior on theta space to compute the M.S.E even though this design used the tangent space to select optimal stimuli. '}];
for sind=1:length(dsets)
    if isfield(dsets(sind),'explain')
        explain=[explain; {dsets(sind).lbl, {'setup',dsets(sind).setupfile;'mse data', getpath(msefiles(sind));'explain',dsets(sind).explain}}];
    else
        explain=[explain; {dsets(sind).lbl, {'setup',dsets(sind).setupfile;'mse data', getpath(msefiles(sind))}}];
    end
end

for wind=1:length(wavinfo)
    oinfo={'wave file', wavinfo(wind).wind; 'isbirdsong', wavinfo(wind).issong;};
    odata={fmse(wind) oinfo};

    if (wind==1)
        odata={fmse(wind) {oinfo;explain}};
    end
    otbl=[otbl;odata];
end

onenotetable(otbl,seqfname('~/svn_trunk/notes/bscomparemse.xml'));

return;