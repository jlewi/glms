%function msefile=msefname(datafile)
%
%Explanation: Generate a filename for the mse file by appending _mse
function msefile=msefname(datafile)

    [rdir rfname rext]=fileparts(getrpath(datafile));
    rpath=fullfile(rdir,[rfname '_mse',rext]);
    msefile=FilePath(getbcmd(datafile),rpath);