%function compd2loglike(uobj,bdata,mobj,theta,windexes)
%   uobj
%   mobj - model
%   theta - the point at which we compute the first and second derivative
%       of the log likelihood
%
%
%Return value
%   dglm - first derivative
%   d2glm - secondderivative
%   dinfo - structure with debugging information.
%Explanation: This function computes the derivatives of the log likelihood
%   using compllike. The purpose of this function is to allow us to use
%   the derivative of the log-likelihood as the objective function for
%   fzero
function varargout=compd2loglike(uobj,mobj,theta)

if (nargout==1)
    [ll dl]=compllike(uobj,mobj,theta);
    varargout{1}=dl;
elseif (nargout==2)
    [ll dl d2ll]=compllike(uobj,mobj,theta);
    varargout{1}=dl;
    varargout{2}=d2ll;
elseif (nargout==3)
    [ll dl d2ll dinfo]=compllike(uobj,mobj,theta);
    varargout{1}=dl;
    varargout{2}=d2ll;    
    varargout{3}=dinfo;
end
    
