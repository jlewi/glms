%function glmproj=compglmprojimp(uobj,mobj,theta,stimspec,obsrv)
%   uobj - the BSLogPost object.
%   mobj - the model
%   theta - value of theta to evaluate it for
%   obsrv - the observation vector for all trials used to compute the spike
%           history
%           Leave empty to ignore spike history
%   ntrials - the number of trials we divide the spectrogram into
%Return:
%   glmproj- each row is a vector containing glmproj[t] as a function of
%   time. Each row is a different wave trial. Each wave trial consists of
%   presenting the stimulus corresponding to windex and measuring the
%   response
%
%   fullstimspec
%   allobsrv - same dimensions as fullstimspec
%            - so could include observations at times for which we don't
%            count as a GLM trial because the input is not fully specified
%
%
%Explanation:
%   This is the function which actual computes the projection of glmproj
%   The other functions named compglmproj call this function after doing
%   some appropriate prepocessing (i.e getting the observations.
%
%Note: the value of uobj.maxrepeats controls how many repeats of the
%   wave file we compute the projection for.
%
%Revisions:
%   01-26-2009 - If there is no spike history then replicate glmproj
%              - for the number of repeats
%   10-08-2008 - Fixed bug:when there is no bias initialize sproj to zero
%   08-11-2008 - If obsrv is empty ignore spike history
%
function [glmproj,varargout]=compglmprojimp(uobj,mobj,theta,stimspec,obsrv,ntrials)

bdata=uobj.bdata;
glmproj=[];

strfdim=getstrfdim(bdata);



%************************************************
%get the filters
%************************************************
[stimcoeff,shistcoeff,bias]= parsetheta(mobj,theta);
strf=getstrf(mobj,theta);



%how man trials do we divide the spectrum into
%ntrials=size(stimspec,2)-strfdim(2)+1;
%[bdata,ntrials,cstart]=getntrialsinwave(bdata,windex);

%**********************************************
%compute the dot product of the strf and the stimulus
%****************************************************
%we comput sproj as a function of 2 efficiently by using conv
%add the bias term;
if hasbias(mobj)
sproj=bias;
else
    sproj=0;
end

startind=size(strf,2);
for fi=1:strfdim(1)
    rconv=conv(strf(fi,end:-1:1),stimspec(fi,:));

    %we throw out any elements for time t>length(stimspec)
    %we also throw out the first (size(strf,2)-1) trials because stimulus
    %is not fully known on these trials
    sproj=sproj+rconv(startind:size(stimspec,2));
end


%*******************************************************
%compute the dot product of shistcoeff and the observations
%********************************************************
%obsrv may need to be padded with zeros so that we know the spike history
%for the first trial
%otstart is the column of the observation in obsrv of the first trial
otstart=strfdim(2);

if (isempty(obsrv) || getshistlen(mobj)==0)
    %ignore spike history
    if isempty(obsrv)
        glmproj=sproj;        
    else
    glmproj=ones(size(obsrv,1),1)*sproj;
    end
else
    if ((otstart-1) <getshistlen(mobj))
        %pad obsrv with zeros and change ostart
        nzeros=getshistlen(mobj)-(otstart-1);
        obsrvpad=[zeros(size(obsrv,1),nzeros) obsrv];
        otstartpad=otstart+nzeros;
    else
        obsrvpad=obsrv;
        otstartpad=otstart;
    end

    shistproj=0;
    if (getshistlen(mobj)>0)
        shistproj=zeros(size(obsrv,1),ntrials);

        for row=1:size(obsrv,1)
            %This formula should be in my notes about batch fitting of the bird
            %song data
            %loop over the spike history components
            for toff=1:getshistlen(mobj)
                %the corresponding spike history coefficent
                coeff=shistcoeff(end-toff+1);
                shistproj(row,:)= shistproj(row,:)+coeff*obsrvpad(row,otstartpad-toff:end-toff);
            end
            %        shistproj(row,:)=
            %        rmobsrv=sum(obsrv(row,otstart:end-1
            %        sconv=conv(shistcoeff(end:-1:1),obsrv(row,:));
            %       shistproj(row,:)=sconv(:,startind:size(stimspec,2));
        end
    end

    %add sproj and shistproj
    glmproj=shistproj+ones(size(obsrv,1),1)*sproj;

end
%************************************************************
%optional return arguments
if (nargout>=2)
    varargout{1}=stimspec;
end

if (nargout>=3)
    varargout{2}=obsrv;
end

