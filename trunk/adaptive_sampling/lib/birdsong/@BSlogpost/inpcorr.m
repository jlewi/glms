%function [ll,dlpost,d2lpost]=inpcorr(uobj,mobj)
%   uobj
%   mobj
%
%Return value - computes the log likelihood upto an additive constant
%   ss    - THe correlation matrix of the inputs
%
%Revision
%   070208
%
%Explanation: This function computes the correlation matrix of the input
%   Its based on my function compllike for computing the hessian of the log
%   posterior.
%   Therfore it may note be the most efficient implementation
function inpcorr=inpcorr(uobj,mobj)


bdata=getbdata(uobj);
stimdim=getstrfdim(bdata);


inpcorr=zeros(getparamlen(mobj),getparamlen(mobj));


for windexind=drange(1:length(uobj.windexes))
    windex=uobj.windexes(windexind);

    %wcorr stores the correlation of the inputs for just this wave file
    wcorr=zeros(getparamlen(mobj),getparamlen(mobj));

    %    wcount=wcount+1;
    fprintf('BSlogpost:labindex=%d  file index %d of %d wave files \n',labindex,windex,length(uobj.windexes));




    [bdata,fullstimspec,allobsrv]=getallwaverepeats(bdata,windex);
    %first trial is the index into allobsrv of the observation on the first
    %i.e
    %stim=fullstimspec(:,firstrial-stimdim(2)+1:firsttrial)
    %r= allobsrv(:,firstrial)
    %is the stimulus and response on the first trial
    %nobsrv - number of trials we divide this wave file into
    firsttrial=stimdim(2);
    [ntrials,firsttrial]=getntrialsinwave(getbdata(uobj),windex);

    %remove from allobsrv those observations which don't correspond to
    %obsevations on the trials. The initial observations aren't
    %observations
    %on any trial because the stimulus isn't full known.
    trialobsrv=allobsrv(:,firsttrial:end);

    % if we don't use all repeats then we trim the data
    if ~isinf(uobj.maxrepeats)
        nrepeats=min(size(allobsrv,1),uobj.maxrepeats);

        if (nrepeats<size(allobsrv,1))
            allobsrv=allobsrv(1:nrepeats,:);
        end
    else
        nrepeats=size(allobsrv,1);
    end

    %compute first and 2nd derivative w.r.t to glmproj
    %this should return a vector entry
    %being the derivative for r=allobsrv(i) and glmproj=glmproj(i)




    %to compute dglm we need to multiply dglmproj by inp(t)
    %since each column of dglmproj and d2glmproj is multiplied by the same
    %x or xx', we sum the rows of dglmproj and d2glmproj before computing
    %the convolution. We can only do this for the stimulus and bias terms
    %because the multipliers are the same for the spike history terms the
    %multipliers are different

    %loop over repeated presentations of the input

    %warning('need to set r=1:size(trials,1)');
    %for r=1
    %****************************************************************
    %Derivative of stimulus coefficient terms
    %*********************************************************
    %we need to take the dot product of dglmproj with each row of the spectrogram
    %we use conv to speed this up
    %see my notes


    %*****************************************************************
    %correlation
    %*****************************************************************
    nfreqs=stimdim(1);
    ntbins=stimdim(2);


    %*************************************************************
    %Correlation of the stimulus terms
    %*************************************************************
    %we compute the derivatives of the stimulus terms separatly
    %because for multiple presentations of the wave file xx'
    %is the same across presentations. Wheras for the spike history
    %the spike history vectors could be different.

    %we compute the upper part of H
    %delta is the difference in time between the two frequencies
    %we are cross correlating


    %WE could speed this up by vectorizing the loops

    fullstimspec(:,1:ntrials-ntbins+1);


    %we want to compute
    %sum x_ix_i+dt
    %outer loop is across columns
    for dt=0:ntbins-1
        wwold=fullstimspec(:,1:ntrials)*fullstimspec(:,dt+1:ntrials+dt)';

        wcorr(1:nfreqs,nfreqs*dt+1:nfreqs*(dt+1))=nrepeats*wwold;


        %we can now efficiently compute
        %x_i+nfreqs*delta x_i+dt+nfreqs*delta
        %inner loop is across the diagnoals
        for delta=1:ntbins-1-dt

            x1old=fullstimspec(:,delta);
            x1new=fullstimspec(:,ntrials+delta);

            x2old=fullstimspec(:,dt+delta);
            x2new=fullstimspec(:,ntrials+dt+delta-1);
            wwnew=wwold-x1old*x2old'+x1new*x2new';

            %scale by the number of repeats
            wcorr(nfreqs*(delta)+1:nfreqs*(delta+1),nfreqs*(delta+dt)+1:nfreqs*(delta+dt+1))=nrepeats*wwnew;
            wwold=wwnew;
        end

    end

    %**************************************************************
    %comptue the correlation of the stimulus with the bias
    %sum x_t
    %start by summing
    wwold=sum(fullstimspec(:,1:ntrials),2);
    rows=1:nfreqs;
    cols=nfreqs*ntbins+getshistlen(mobj)+1;
    wcorr(1:nfreqs,cols)=nrepeats*wwold;
    for t=1:ntbins-1




        cols=nfreqs*ntbins+getshistlen(mobj)+1;

        x2old=fullstimspec(:,t);
        x2new=fullstimspec(:,ntrials+t);
        wwnew=wwold-x2old+x2new;

        %scale by the number of repeats
        wcorr(nfreqs*t+1:nfreqs*(t+1),cols)=nrepeats*wwnew;
        wwold=wwnew;
    end

    %*************************************************************
    %spike history
    %*************************************************************
    if (getshistlen(mobj)>0)
        %********************************************
        %loop over each repeat of the spike history
        %***************************************
        for repeat=1:nrepeats

            %compute the product of d2glmproj for this repeat and the
            %responses r(t-i).
            %we have to zeropad the observations if the spike history
            %exceeds the start trial
            %startindex is the index into allobsrv of the first r in
            startindex=max([(firsttrial-1)-getshistlen(mobj)+1,1]);
            %number of zeros to pad with
            ntopad=max([getshistlen(mobj)-(firsttrial-1),0]);
            obsrv=[zeros(1,ntopad) allobsrv(repeat,startindex:end-1)];

            scorr=zeros(getshistlen(mobj),getshistlen(mobj));



            %the oldest spike history terms
            %we loop across the columns of the first row of h_t h_t^T
            for t2=1:getshistlen(mobj)
                t1=1;
                %the following is sum_ r(t-(ta-t1)) r(t-(ta-t2))
                rr=obsrv(1,t1:t1+ntrials-1)*obsrv(1,t2:t2+ntrials-1)';

                scorr(t1,t2)=rr;

                %the diagonals are delayed versions of rr
                for delta=1:(getshistlen(mobj)-t2)
                    rr=rr-obsrv(t1+delta-1)*obsrv(t2+delta-1)+obsrv(ntrials+delta)*obsrv(ntrials+t2+delta-1);
                    scorr(t1+delta,t2+delta)=rr;
                end
            end


            %oldest spike history appear first in the output

            %appear first in the vector of outputs
            %i.e if h_t is the spike history at time t
            %h_t=[r(t-ta) .. r(t-1)]
            %so we have to adjust the indexing appropriately
            %i.e s=[x_t; h_t]
            %h_t = spike history at time t
            %h_t(end)= r(t-1)
            %h_t(1)=r(t-tk)
            startr=nfreqs*ntbins+1;
            endr=nfreqs*ntbins+getshistlen(mobj);
            wcorr(startr:endr,startr:endr)=   wcorr(startr:endr,startr:endr)+scorr;


            %****************************************************
            %Cross Term x r_t^T
            %****************************************************
            xhmat=zeros(nfreqs*ntbins,getshistlen(mobj));
            %    for freq=1:nfreqs
            %could probably speed thisup with a convolution
            for thist=1:getshistlen(mobj)
                for delta=0:ntbins-1
                    xh=sum(fullstimspec(:,delta+1:ntrials+delta).*(ones(nfreqs,1)*obsrv(thist:thist+ntrials-1)),2);


                    xhmat(nfreqs*delta+1:nfreqs*(delta+1),thist)=xh;

                    %xh=xh-fullstimspec(:,delta).*obsrv(thist+delta-1)';
                    %xh=xh+fullstimspec(:,ntrials+delta).*obsrv(thist+ntrials-1+delta)';



                end
            end
            wcorr(1:nfreqs*ntbins,nfreqs*ntbins+1:nfreqs*ntbins+getshistlen(mobj))=wcorr(1:nfreqs*ntbins,nfreqs*ntbins+1:nfreqs*ntbins+getshistlen(mobj))+xhmat;


            %*******************************************************
            %bias term
            %r_t 1
            %********************************************************
            r1=zeros(getshistlen(mobj),1);

            r1(1)=sum(obsrv(1:ntrials));
            for t1=2:getshistlen(mobj)
                r1(t1)=r1(t1-1);
                r1(t1)=r1(t1)-obsrv(t1-1);
                r1(t1)=r1(t1)+obsrv(ntrials+t1-1);
            end
            wcorr(nfreqs*ntbins+1:nfreqs*ntbins+getshistlen(mobj),end)=wcorr(nfreqs*ntbins+1:nfreqs*ntbins+getshistlen(mobj),end)+r1;

        end

    end

    %bias term
    wcorr(end,end)=ntrials*10;

    %accumulate over the wave files
    inpcorr=inpcorr+triu(wcorr);

end

%we need to accumulate the valeus across nodes
inpcorr=gplus(inpcorr);

%sinc inpcorre is upper triangular
inpcorr=inpcorr+triu(inpcorr,1)';

