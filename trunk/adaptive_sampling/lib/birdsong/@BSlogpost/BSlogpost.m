%function obj = BSlogpost('bdata',bdata)
%   bdata - object representing the bird song data
%
%Optional
%   windexes - the indexes of the wave files to include
%       in the computation of the log posterior and its derivative
%   maxrepeats - how many repeats of the wave files to use when computing
%      the data
%Explanation: this class provides methods for efficiently computing the log
%posterior and its derivatives of the log posterior
%
%Revisions
%   07-21-2008: Update to the new object model
classdef (ConstructOnLoad=true) BSlogpost
    properties(SetAccess=private,GetAccess=public)
        %**********************************************************
        %Define Members of object
        %**************************************************************
        % version - version number for the object
        %           store this as yearmonthdate
        %           using 2 digit format. This way version numbers follow numerical
        %           order
        %           -version numbers make it easier to maintain compatibility
        %               if you add and remove fields
        %
        %windexes  - indexes of wavefiles to use to compute the log-posterior and
        %           its derivatives
        %maxrepeats  - maximum number of repeats to use for each wavefile

        version=080510;
        bdata=[];
        windexes=[];
        maxrepeats=inf;

    end
    methods
        function obj=BSlogpost(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'bdata'};
            con(1).cfun=1;



            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required            
                    return;
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    obj.bdata=params.bdata;
                otherwise
                    if isnan(cind)
                        %this will happen if calling constructor from
                        %derived class
                    else
                    error('Constructor not implemented')
                    end
            end


            if isfield(params,'bdata')
            if (isfield(params,'windexes') && ~isempty(params.windexes))
                obj.windexes=params.windexes;
            else
                obj.windexes=1:getnwavefiles(obj.bdata);
            end
            end
            
            if isfield(params,'maxrepeats')
                obj.maxrepeats=params.maxrepeats;
            end
        end
    end
end







