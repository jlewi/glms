%function [ll,dlpost,d2lpost]=compllike(uobj,bdata,mobj,theta,prior)
%   uobj
%   mobj - model
%   theta - the point at which we compute the first and second derivative
%       of the log likelihood
%
%Return value - computes the log likelihood upto an additive constant
%   ll    - the log-likelihood
%   dll   - 1st derivative of the log likelihood
%   d2ll  - 2nd derivative of the log likelihood
%
%Revision
%   07-09-2008
%       replace convolutions with for loops because I think they will be
%       faster
%   05-27-2008 - edited the code in the loop so it could be parallelized
%   using for drange
%       this meant declaring separate variables  (llto,dglmtot,d2glmtot)
%       to act as the reduction variables.
function varargout=compllike(uobj,mobj,theta)

bdata=uobj.bdata;



%whether to compute the derivatives
compdglm=false;
compd2glm=false;

if (nargout>=2)
    compdglm=true;
end

if (nargout>=3)
    compd2glm=true;
end


%the log likelihood
%the variables named tot are the reduction variables which accumlate the values accross
%iterations over the wave files
lltot=0;
dglmtot=[];
d2glmtot=[];

dglmtot=zeros(getparamlen(mobj),1);


stimdim=getstrfdim(bdata);
paramlen=getparamlen(mobj);

%pstimdim is the number of elements in the window we slide over the
%spectrogram to get the part of the spectrogram that is the stimulus on the
%trial
pstimdim=prod(stimdim);
%if we need to compute the 2nd derivative initialize the relevant terms
if (compd2glm)
    d2glmtot=zeros(getparamlen(mobj),getparamlen(mobj));


    %this are the terms we use to recursivel construct the sum
    %ss' where s is the full input at time t
    x=zeros(getklength(mobj)*stimdim(2),1);
    %  xx=zeros(getklength(mobj)*stimdim(2),getklength(mobj)*stimdim(2));
    % xr=zeros(getklength(mobj)*stimdim(2),getshistlen(mobj));
    rr=zeros(getshistlen(mobj),getshistlen(mobj));
end

wcount=0;

%keep track of the total number of observations. We need this to normalize
%the gradients
nobsrv=0;

for windexind=drange(1:length(uobj.windexes))
    windex=uobj.windexes(windexind);
    %declare the temporary variables
    %these are the variables storing the log likelihood and 1st and second
    %derivative evaluated for each wave file
    ll=0;
    if (compdglm)
        dglm=zeros(getparamlen(mobj),1);
    end
    if (compd2glm)
        d2glm=zeros(getparamlen(mobj),getparamlen(mobj));
    end

    %    wcount=wcount+1;
    %fprintf('BSlogpost:labindex=%d compllike  file index %d of %d wave files \n',labindex,windex,length(uobj.windexes));




    %     if isempty(param)
    %         %compute glmproj
    [glmproj,fullstimspec,allobsrv]=compglmproj(uobj,windex,mobj,theta);






    %first trial is the index into allobsrv of the observation on the first
    %i.e
    %stim=fullstimspec(:,firstrial-stimdim(2)+1:firsttrial)
    %r= allobsrv(:,firstrial)
    %is the stimulus and response on the first trial
    %nobsrv - number of trials we divide this wave file into
    firsttrial=stimdim(2);
    [nobsrv,firsttrial]=getntrialsinwave(getbdata(uobj),windex);

    %remove from allobsrv those observations which don't correspond to
    %obsevations on the trials. The initial observations aren't
    %observations
    %on any trial because the stimulus isn't full known.
    trialobsrv=allobsrv(:,firsttrial:end);
    %***************************************************************
    %compute the log likelihood
    %*******************************************************
    ll=sum(loglike(getglm(mobj),trialobsrv(:),glmproj(:)));

    % if we don't use all repeats then we trim the data
    if ~isinf(uobj.maxrepeats)
        nrepeats=min(size(glmproj,1),uobj.maxrepeats);

        if (nrepeats<size(glmproj,1))
            glmproj=glmproj(1:nrepeats,:);
            allobsrv=allobsrv(1:nrepeats,:);
        end
    else
        nrepeats=size(glmproj,1);
    end

    %compute first and 2nd derivative w.r.t to glmproj
    %this should return a vector entry
    %being the derivative for r=allobsrv(i) and glmproj=glmproj(i)
    if (compd2glm)
        [dglmproj,d2glmproj]=d2glmeps(getglm(mobj),glmproj(:),trialobsrv(:),'v');
    elseif(compdglm)
        [dglmproj]=d2glmeps(getglm(mobj),glmproj(:),trialobsrv(:),'v');
    end


    %reshape them into matrices
    if (compdglm)
        dglmproj=reshape(dglmproj,size(glmproj,1),size(glmproj,2));
    end

    if (compd2glm)
        d2glmproj=reshape(d2glmproj,size(glmproj,1),size(glmproj,2));
    end
    %to compute dglm we need to multiply dglmproj by inp(t)
    %since each column of dglmproj and d2glmproj is multiplied by the same
    %x or xx', we sum the rows of dglmproj and d2glmproj before computing
    %the convolution. We can only do this for the stimulus and bias terms
    %because the multipliers are the same for the spike history terms the
    %multipliers are different
    if (compdglm)
        sdglmproj=sum(dglmproj,1);
    end
    if (compd2glm)
        sd2glmproj=sum(d2glmproj,1);
    end
    %loop over repeated presentations of the input

    %warning('need to set r=1:size(trials,1)');
    %for r=1
    %****************************************************************
    %Derivative of stimulus coefficient terms
    %*********************************************************
    %we need to take the dot product of dglmproj with each row of the spectrogram
    %we use conv to speed this up
    %see my notes

    if (compdglm)

        for tbin=1:stimdim(2)
            rowstart=stimdim(1)*(tbin-1)+1;
            rowend=stimdim(1)*tbin;

            cstart=tbin;
            cend=tbin+nobsrv-1;
            dglm(rowstart:rowend,1)=fullstimspec(:,cstart:cend)*sdglmproj';
        end
        %****************************************************************
        %Derivative of spikehistory coefficient terms
        %*********************************************************

        if (getshistlen(mobj)>0)

            %here we construct histobsrv
            %this is a matrix of the observations relevant for spike
            %history

            %if the length of the spike history is > strfdim(2)-1 then
            %the spike history isn't fully known on the first trial so we pad
            %with zeros
            %Note: Histobsrv is used later on to compute the 2nd derivative
            %and it assumes we have zero padded it if needed
            if (getshistlen(mobj)>(firsttrial-1))
                nspikestoadd=getshistlen(mobj)-(firsttrial-1);
                histobsrv=[zeros(size(allobsrv,1),nspikestoadd) allobsrv];
            elseif (getshistlen(mobj)<(firsttrial-1))

                %if the length of the spike history is < strfdim(2)-1
                %we remove the observations on trials  <
                %strfdim(2)-getsthistlen
                %because these don't constitute spike history for any trials
                %Later on when we compute the 2nd derivative it is assumed we
                %have made this modification

                %the index in allobsrv of the first element in the spike
                %history
                firstspikehist=(firsttrial-1)-getshistlen(mobj)+1;
                histobsrv=allobsrv(:,firstspikehist:end);

            else
                %all the observations end up being spike history terms so
                %we include them all
                histobsrv = allobsrv(:,1:end);
            end

            %throw out the last obsrevation because it isn't the spike
            %history
            histobsrv=histobsrv(:,1:end-1);

            %             for r=1:size(histobsrv,1)
            %                 shistconv=conv(dglmproj(r,:),histobsrv(r,end:-1:1));
            %                 %get the relevant part
            %                 d=shistconv(size(histobsrv,2):-1:size(histobsrv,2)-getshistlen(mobj)+1);
            %                 dglm(pstimdim+1:pstimdim+getshistlen(mobj))= dglm(pstimdim+1:pstimdim+getshistlen(mobj))+d';
            %
            %             end
            for rt=1:getshistlen(mobj)
                dglm(pstimdim+rt,1)=sum(sum(histobsrv(:,rt:rt+nobsrv-1).*dglmproj,2),1);
            end
        end


        %****************************************************************
        %Derivative of bias term
        %*********************************************************
        %bias always =1 so we just sum dglmproj
        %        dglm(end)=dglm(end)+sum(sum(dglmproj));
        %        dglm(end)=sum(sum(dglmproj));
        dglm(end)=sum(sdglmproj);
    end

    %*****************************************************************
    %2nd derivative
    %*****************************************************************
    nfreqs=stimdim(1);
    ntbins=stimdim(2);
    if (compd2glm)
        %*************************************************************
        %2nd derivative of just the stimulus terms
        %*************************************************************
        %we compute the derivatives of the stimulus terms separatly
        %because for multiple presentations of the wave file xx'
        %is the same across presentations. Wheras for the spike history
        %the spike history vectors could be different.

        %we compute the upper part of H
        %delta is the difference in time between the two frequencies
        %we are cross correlating
        for freq1=1:nfreqs
            for tb1=1:ntbins

                for tb2=tb1:ntbins
                    %compute the sum
                    %sum_t d2(t) s((tb1-1)*nfreqs +freq,t)
                    %s(nfreqs*(tb2-1)+1:nfreqs*(tb2),t)

                    row=nfreqs*(tb1-1)+freq1;
                    %                colstart=nfreqs*(tb2-1)+freq1;
                    if (tb2==tb1)
                        colstart=nfreqs*(tb2-1)+freq1;
                        colend=nfreqs*(tb2);
                        z=fullstimspec(freq1:end,tb2:tb2+nobsrv-1)*(sd2glmproj.*fullstimspec(freq1,tb1:tb1+nobsrv-1))';
                    else
                        colstart=nfreqs*(tb2-1)+1;
                        colend=nfreqs*(tb2);
                        z=fullstimspec(1:end,tb2:tb2+nobsrv-1)*(sd2glmproj.*fullstimspec(freq1,tb1:tb1+nobsrv-1))';
                    end
                    d2glm(row,colstart:colend)=z';
                end
            end
        end


        %**************************************************************
        %comptue the correlation of the stimulus with the bias
        %sum d(t) x_t

        for tb=1:ntbins
            z=fullstimspec(:,tb:tb+nobsrv-1)*sd2glmproj';
            d2glm(nfreqs*(tb-1)+1:nfreqs*tb,nfreqs*ntbins+getshistlen(mobj)+1)=z;
        end
        %*************************************************************
        %spike history
        %*************************************************************
        if (getshistlen(mobj)>0)
            %***********************************************
            %compute the correlation r_t1 r_t2
            for t1=1:getshistlen(mobj)
                for t2=t1:getshistlen(mobj)

                    %we have to zeropad the observations if the spike history
                    %exceeds the start trial
                    %startindex is the index into allobsrv of the first r in
                    startindex=max([(firsttrial-1)-getshistlen(mobj)+1,1]);

                    %                     %number of zeros to pad with
                    %                     ntopad=max([getshistlen(mobj)-(firsttrial-1),0]);
                    %                     obsrv=[zeros(nrepeats,ntopad) allobsrv(:,startindex:end-1)];
                    %
                    %
                    %                      %compute the correlation for each repeat
                    %                     z=sum(d2glmproj.*obsrv(:,t1:t1+nobsrv-1).*obsrv(:,t2:t2+nobsrv-1),2);

                    %number of spikes which were not recorded for each time
                    %bin
                    nnotrecorded1=max([getshistlen(mobj)-(t1-1)-(firsttrial-1),0]);
                    nnotrecorded2=max([getshistlen(mobj)-(t2-1)-(firsttrial-1),0]);

                    nnotrecorded=max([nnotrecorded1,nnotrecorded2]);

                    %the following would be the start indexes into allobsrv
                    %assuming nnotrecorded is zero
                    start1=(firsttrial-1)-(getshistlen(mobj)-(t1));
                    start2=(firsttrial-1)-(getshistlen(mobj)-(t2));

                    %now we add nnotrecorded
                    start1=start1+nnotrecorded;
                    start2=start2+nnotrecorded;

                    startd2=1+nnotrecorded;

                    row=nfreqs*ntbins+t1;
                    col=nfreqs*ntbins+t2;

                    %how many  observations were actually recorded and our
                    %being summed
                    nrecord=nobsrv-nnotrecorded;
                    %compute the correlation for each repeat
                    z=sum(d2glmproj(:,startd2:startd2+nrecord-1).*allobsrv(:,start1:start1+nrecord-1).*allobsrv(:,start2:start2+nrecord-1),2);

                    %now sum across repeats
                    d2glm(row,col)=sum(z);
                end
            end

            %****************************************************
            %Cross Term x r_t^T
            %****************************************************
            %we do loop over ntbins and getshitlen mobj
            for tb=1:ntbins
                %multiply the spike histor for each repeat
                %by the appropriate derivative.
                %instead of padding obsrv we select the appropriate
                %indexes
                %because padding can potential be exepensive since it
                %requires memory allocation
                %start and end obsrv are the first and last observations
                %in all obsrv
                %firsttrial-1-getshistlen(mobj+1) might be negative
                %because spike history on first trial was not full
                %recorded
                %nnotrecorded are how many trials at the start
                %of the experiment were not recorded but on which the
                %spike history depends. We assume observations were zero
                nnotrecorded=max([getshistlen(mobj)-(firsttrial-1),0]);
                startobsrv=max([(firsttrial-1)-getshistlen(mobj)+1,1]);

                %now we need to compute the appriate indexes in
                %d2glmproj. If a spike is not recorded then we assume
                %the observation is zero so we just ignore that term in
                %the summate

                if (nnotrecorded>0)
                    startd2=nnotrecorded+1;
                else
                    startd2=1;
                end

                for rt=1:getshistlen(mobj)



                    endobsrv=startobsrv+(nobsrv-nnotrecorded-1);
                    endd2=nobsrv;
                    %compute the products and sum across repeats
                    dr=sum(d2glmproj(:,startd2:endd2).*allobsrv(:,startobsrv:endobsrv),1);

                    %now multiply by all frequenzes
                    z=fullstimspec(:,startd2+tb-1:endd2+tb-1)*dr';

                    d2glm(nfreqs*(tb-1)+1:nfreqs*tb,nfreqs*ntbins+rt)=z;

                    if (nnotrecorded>0)
                        nnotrecorded=nnotrecorded-1;
                        startd2=startd2-1;
                    else
                        startobsrv=startobsrv+1;

                    end
                end
            end


            %***********************************************
            %constant term
            %sum d2 r_t
            %*********************************************
            row=nfreqs*ntbins;
            col=nfreqs*ntbins+getshistlen(mobj)+1;

            nnotrecorded=max([getshistlen(mobj)-(firsttrial-1),0]);
            startobsrv=max([(firsttrial-1)-getshistlen(mobj)+1,1]);

            %now we need to compute the appriate indexes in
            %d2glmproj. If a spike is not recorded then we assume
            %the observation is zero so we just ignore that term in
            %the summate
            if (nnotrecorded>0)
                startd2=nnotrecorded+1;
            else
                startd2=1;
            end

            for t=1:getshistlen(mobj)
                row=row+1;

                endobsrv=startobsrv+(nobsrv-nnotrecorded-1);
                endd2=nobsrv;

                d2glm(row,col)=sum(sum(d2glmproj(:,startd2:endd2).*allobsrv(:,startobsrv:endobsrv)));

                %either we increment the starting index or else we
                %decrement the number of trials that weren't recorded
                if(nnotrecorded==0)
                    startobsrv=startobsrv+1;
                    %startd2=1;
                else
                    nnotrecorded=nnotrecorded-1;
                    startd2=startd2-1;
                end

            end
        end




        %*********************************************************
        %bias term
        %*************************************************************
        %        d2glm(end,end)=d2glm(end,end)+sum(sd2glmproj);
        d2glm(end,end)=sum(sd2glmproj);





    end


    %************************************************
    %save variables for debugging if
    %**************************************************
    %I think this might cause problems with parallel loops
    %could just force it to [] if numlabs>1
    if (nargout>=4)
        dvar.glmproj{windexind}=glmproj;
        dvar.dglmproj{windexind}=dglmproj;
        dvar.d2glmproj{windexind}=d2glmproj;
        varargout{4}=dvar;
    end

    %********************************************************
    %accumulation
    %accumulate the values across iteration of the for loop
    %*******************************************************
    lltot=lltot+ll;
    if (compdglm)
        dglmtot=dglmtot+dglm;
    end

    if (compd2glm)
        d2glmtot =d2glmtot+d2glm;
    end
end

%we need to accumulate the values across all workers
lltot=gplus(lltot);

varargout{1}=lltot;

if (compdglm)
    dglmtot=gplus(dglmtot);
    varargout{2}=dglmtot;
end

if (compd2glm)
    d2glmtot=gplus(d2glmtot);
    %we only compute the upper triangulare part of d2glm so now we need
    %to reflect it over the diagonal
    d2glmtot=d2glmtot+triu(d2glmtot,1)';
    varargout{3}=d2glmtot;
end

