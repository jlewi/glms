%function [dlpost,d2lpost]=compd2lpost(uobj,bdata,mobj,theta,prior)
%   uobj
%   mobj - model
%   theta - the point at which we compute the first and second derivative
%       of the log likelihood
%   prior - prior
%
%
%Return value - computes the first and second derivative of the log
%posterior using prior
%   dlpost - derivative of the posterior
%   d2lpost -2nd derivative
%            only computed if there are two output variables
%
% This function calls complpost to do the actual computations
%   we return dlpost as the first argument just so that we can use
%   this function as an objective function for fzero.
function varargout=compd2lpost(uobj,mobj,theta,prior)

if (nargout==1)
   [lpost dlpost]=complpost(uobj,mobj,theta,prior);
   varargout{1}=dlpost;
else
    [lpost dlpost d2lpost]=complpost(uobj,mobj,theta,prior);
       varargout{1}=dlpost;
          varargout{2}=d2lpost;
end