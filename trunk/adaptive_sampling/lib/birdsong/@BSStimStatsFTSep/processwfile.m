%function [stats,bsproj]=processwavefile(mobj,bspost,windex)
%   mobj - The model object defining the strf
%    bsdata - the bird song data
%    windex - which wavefile to process
%
%Explaantion:
%
%Return value
%   stats - the statistics about the coefficients
%   bsproj= the projection on the basis functions

function [stats,bproj,trig]=processwfile(mobj,bspost,windex)


%mobj should not have a bias or any spike history terms
if (hasbias(mobj) || mobj.alength>0)
    %we can recover by just creating a new mobj with no bias and no spike
    %history
    error('Mobj should not have a bias or any spike history terms');    
end

%matrix to store the projection on each basis
%each column is a different trial
bproj=zeros(getparamlen(mobj),getntrialsinwave(bspost.bdata,windex));

for bind=1:getparamlen(mobj)
   bvec=zeros(mobj.nstimcoeff,1);
   bvec(bind)=1;
   bproj(bind,:)=compglmproj(bspost,windex,mobj,bvec);
end



stats.sum=sum(bproj,2);
stats.sum2=sum(bproj.^2,2);

stats.ntrials=getntrialsinwave(bspost.bdata,windex);
