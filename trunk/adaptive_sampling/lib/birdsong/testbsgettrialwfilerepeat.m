%07-22-2008
%
%function to test gettrialwfilerepeat returns the same data as get trial


setpathvars;
%*******************************************************************
%Create a bsdata object
%*********************************************************************

dfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1_corrected.mat'));



%how many columns should be in the STRF 
stimnobsrvwind=10;
%subsample the frequencies of the spectrogram to reduce the dimensionality
%of the stimulus
freqsubsample=4;
obsrvwindowxfs=250;
bdata=BSData('fname',dfile,'stimnobsrvwind',stimnobsrvwind,'freqsubsample',freqsubsample,'obsrvwindowxfs',obsrvwindowxfs);

%controls the amount of spike history to return
alength=10;
strfdim=getstrfdim(bdata);

glm=GLMPoisson('canon');
mobj=MParamObj('glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',1);

%********************************************************************
windex=ceil(getnwavefiles(bdata)*rand(1));

%how many test points
ntest=1000;

repeats=ceil(rand(1,ntest)*getnrepeats(bdata,windex));
[ntrials,cstart]=getntrialsinwave(bdata,windex);
trials=ceil(rand(1,ntest)*ntrials);

[stim,shist,obsrv]=gettrialwfilerepeat(bdata,windex,repeats,trials,mobj);


[bdata,trialind]=gettrialindforwave(bdata,windex);
for tind=1:ntest
    tnum=trialind(repeats(tind),1)+trials(tind)-1;
    [obj,gt.stim,gt.shist,gt.obsrv]=gettrial(obj,tnum,getshistlen(mobj));
    
    if any(stim(:,tind)~=getdata(gt.stim))
        error('Stim dont match for repeat=%d time=%d',repeats(tind),trials(tind));
    end
     if any(shist(:,tind)~=gt.shist)
        error('shist dont match for repeat=%d time=%d',repeats(tind),trials(tind));
     end
     if any(obsrv(:,tind)~=gt.obsrv)
        error('obsrv dont match for repeat=%d time=%d',repeats(tind),trials(tind));
    end
end

fprintf('gettrialwfilerepeat returned no errors \n');
