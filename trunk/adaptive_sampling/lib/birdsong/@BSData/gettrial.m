%function [obj,stim,shist, obsrv,stimid]=gettrial(obj,trial,shistlen)
%   trial - index of the trial to get
%   shistlen - how long of a spike history to return
%
%Return value -
%   obj - The object is returned because we save several values so we only
%        compute them once
%   sr - an SRObject which contains the stimulus and response at this trial
%       - The actual input is a matrix - each column is the spectrogram at
%       a different time point. The most recent time is the last column.
%
%   shist - vector of the spike history with most recent observation being
%   the last element in the vactor
%
%   stimid
%           = we use this to keep track of the order the stimuli were
%           chosen in
%       stimid(1,1) - windex of the stimulus
%       stimid(2,1) - the repeat of the wfile
%       stimid(3,1) - the relative trial within the 
%
%Revisions  
%   07-22-2008: nincrements is set to stimparam.nobsrvwind
%   07-15-2008:
%       return stimid
%       Don't bother creating an SRObject. Instead return the input and
%       response separatly. Hopefully this will reduce some of the
%       overhead.
%   05-28-2008: Allow the observation to be a positive integer
%       equal to the number of spikes in each bin as opposed to just
%       0 or 1
%       I switched to using getobsrvmat to compute the observation in each
%       bin. This isn't efficient because we compute the observations in a
%       lot of bins that we don't care about but it makes the code easier
%       to manage because we have only 1 function for computing the
%       observations.
%   04-29-2008: We have to return shist because we cannot properly
%   reconstruct the spike history from the sr objects for trials that occur
%   at the start of wave file during which not all of the spike history was
%   recorded. I.e let sr(t) be the first trial for wave file j.
%   THen the observation for sr(t-1) is not the observation immediately
%   preceding sr(t) in time.
%
function [obj,data,shist,obsrv,stimid]=gettrial(obj,trial,shistlen)

shist=[];
stimid=zeros(3,1);
if ~exist('shistlen','var')
    shistlen=0;
end
[obj,ntrialssofar]=getntrialssofar(obj);

nincrements=obj.stimparam.nobsrvwind;

%find which wave trial corresponds to this trial
sindex=getwavetrialoftrial(obj,trial);
[stimindex,stimrepeat]=getwindexonwavetrial(obj,sindex);

stimid(1,1)=stimindex;
stimid(2,1)=stimrepeat;

%set startframe to the first frame in the stimulus corresponding to this
%trial
% startframe=1 corresponds to the start of the silence preceding the actual
% wave file
startframe=trial;
if (sindex>1)
    %remove the number of trials which were in the wave files preceding
    %this wave file
    startframe=trial-ntrialssofar(sindex-1);
end

%get the length of the silences before and after the stimulus;
[obj,npre,npost]=nframesinsilence(obj);

%startframe - is the first column in the spectrogram of the stimulus
%(including the pre/post silences) of the input on the this trial


%spike times are measured with respect to the silence preceding the actual
%wave file
%check if there is a spike in the time bin corresponding to this frame
%compute the frame number corresponding to the responce
obsrvframe=startframe+nincrements-1;


%get the observation vector 
obsrvvec=getobsrvmat(obj,stimindex,stimrepeat);
obsrv=obsrvvec(obsrvframe);

stimid(3,1)=obsrvframe;


%***************************************************************
%get the spike history if told to do so
%********************************************************************
if (shistlen>0)
    shist=zeros(shistlen,1);
    if (obsrvframe>1)
        if ((obsrvframe-1)>shistlen)
            shist(:,1)=obsrvvec(obsrvframe-shistlen:obsrvframe-1);
        else
            shist(end-(obsrvframe-1)+1:end,1)=obsrvvec(1:obsrvframe-1);
        end
    end

end
%***********************************************************************
%get the actual stimulus
%**********************************************************************
%get the spectrogram
%because we use pointers if the spectrogram is computed it will be
%saved
[obj stimspec]=getwavespec(obj,obj.data.stimindex(sindex));


m=zeros(size(stimspec,1),nincrements);


%startframe and endframe are the columns of the complete spectrogram
%i.e if s=spectrogram(spre wave spost) where spre is the pre sound stimulus
%wave is the acutal wave file and spost= silence following the wave file
%then
% s(:,startframe:endframe) - is the portion of the spectrogram comprising
% the input on this trial
%
%we have to map startframe, endframe into the appropriate column indexes
%for spec(wav)
%
%startcol,

%if startframe=1 corresponds to the first entry in
%the pre-wave wfile silence
%So we need to determine which portion of the input is the actual wave file
startcol=startframe-npre;
mstart=1;
if (startcol<=0)
    mstart=mstart-startcol+1;
    startcol=1;
end

%if mstart> nincrements then the entire input is just silence
if (mstart<=nincrements)
    endframe=startcol+(nincrements-mstart+1)-1;
    mend=nincrements;

    %if endframe is longer than size(stimspec,2) then final entries
    %correspond to the silence following the stimulus
    if (endframe>size(stimspec,2))
        endframe=size(stimspec,2);
        mend=mstart+(endframe-startcol);
    end
    m(:,mstart:mend)=stimspec(:,startcol:endframe);
end
data=GLMMatrix('matrix',m);

%sr=SRObj(data,obsrv);