%function [obj,stimspec,obsrv]=getallwaverepeats(obj,sindex)
%   obj - bsdata object
%   sindex - index of the wave file
%
%Return value:
%   stimspec- the spectrogram corresponding to this wave file.
%             We want the full input i.e the wave file + pre and post
%             silences
%   obsrv   - a matrix where each matrix is the observation vector this is
%             a vector of the spike counts and it has the same number of columns
%             as stimspec
%
%           - thus obsrv contains responses at times for which the stimulus
%           is not full known. We do this because some of these spikes will
%           be used as the spike history on later trials
function [obj, fullspec,obsrv]=getallwaverepeats(obj,sindex)


stimdim=getstrfdim(obj);
nf=stimdim(1);
nt=stimdim(2);
[obj,fullspec]=getfullspec(obj,sindex);

%****************************************************************
%get a cell array of spike times 
%each array is the spike times on a different presentation of the stimulus
%***********************************************************
obsrv=getobsrvmat(obj,sindex);

