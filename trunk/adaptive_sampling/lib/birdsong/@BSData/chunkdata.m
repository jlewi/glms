%function [obj,data]=chunkdata(obj,maxtrials)
%   obj - the BSData object
%   maxtrials - optional maximum number of trials to return
%Return Value
%   data - an array of SRObj objects. Each object is the stimulus and
%   response for a different trial. Each trial is the result of sliding a
%   window over the input and looking at the response in a bin around the
%   response.
function [obj,data]=chunkdata(obj,maxtrials)

error('ChunkData is obsolete use gettrial instead');

if ~exist('maxtrials','var')
    maxtrials=inf;
end
ntrials=0;


bsinfo.fname=obj.fname;
bsinfo.stimdir=obj.stimdir;

  
    
%compute how man trials there will be
%laststart - This is an array of the index of the first frame in the input
%            on the last trial
%          - laststart(j) - the value for the jth sound file
laststart=zeros(1,length(obj.stimspec));

for sindex=1:length(obj.stimspec)
    %get information about how the stimulus will be divided into frames
    [obj,finfo]=frameinfo(obj,sindex); 
   [obj,stiminfo]=getstiminfo(obj,sindex);
    
   
     %nincrements is the number of frame's (i.e the number of columns of the
    %spectrogram) corresponding to the input on each trial
    nincrements=obj.stimparam.nobsrvwind;
    
     %how many frames are there in total in this stim
    %npre and npost are the number of frames in the silence preceding and
    %following the stimulus
    npre=obj.data.presilence/obj.obsrvparam.tlength;
    npost=obj.data.postsilence/obj.obsrvparam.tlength;
    
    %framecount - the number of frames in the full stimulus
    %   The full stimulus includes the stimulus + the silences before and
    %   after the stimulus.
     %operate on the object  
    framecount=finfo.frameCount+npre+npost;
    
%    [obj,stiminfo]=getstiminfo(ptrbdata.bsdata,sindex);
    laststart(sindex)=framecount-nincrements+1;
end

ntrials=laststart(obj.data.stimindex);
ntrials=sum(ntrials);
ntrials=min([ntrials maxtrials]);

%loop over the stimuli and count the number of trials per stimuli
%to speed the creation of the srobjects we create a
%structure array of the parameters we will need to create each SRObj
%we then create the SRObjects all at once
%this minimizes the number of calls to parseinputs which is expensive
srparams=struct('sindex',[],'startframe',[],'ptrbsdata',[],'bsdatainfo',[]);
srparams=repmat(srparams,ntrials,1);

%get a ptr to this bsdata object
%this creates a copy of bdata object. 
ptrbdata.bsdata=obj;
ptrbdata=pointer(ptrbdata);

%for index=1:length(obj.data.stimindex);
%we loop over each stimulus presented
trial=0;    %keep track of which trial we are computting
for stimindex=1:length(obj.data.stimindex)
    sindex=obj.data.stimindex(stimindex);
    
             
%    specpad=[zeros(size(spec,1),npre) spec zeros(size(spec,1),npost)];
    
    %*********************************************************
    %convert the spike times for this stimulus into a vector of zero's and
    %1's.
    %initialize the obsrv
    obsrvvec=zeros(1,laststart(sindex));
        
    %set the entries corresponding to spikes to 1
    ind=floor(obj.data.spiketimes{stimindex}/obj.obsrvparam.tlength)+1;
    obsrvvec(ind)=1;
    
    %wind is the index of the first frame in each stimulus windo
    for wind=1:laststart(sindex);
            
        if (mod(trial,10)==0)
           fprintf('Trial %d of %d \n',trial,ntrials);
        end
        %startframe - we subtrace npre because we want frames with index
        %<=0 to correspond to frames in pre-stimulus silence
        startframe=wind-npre;

        trial=trial+1;
        %initialize the version so that when list gets concated together in
        %the SRBirdSong constructor
        %its  not an empty array
        srparams(trial).version=nan;
        
        %obsrv
        %create a structure to pass into the constructor of SRBirdSong 
        %this is faster than calling parse inputs
       
        srparams(trial).sindex=sindex;
        srparams(trial).startframe=startframe;
        srparams(trial).ptrbsdata=ptrbdata;
        srparams(trial).bsdatainfo=bsinfo;
        srparams(trial).obsrv=obsrvvec(wind);

       
        
        %start time for the stimulus in this window
       %tstart=(wind-1)*obj.stimparam.tlength;

       %we need to compute the 
       %sounds(sindex).stimsub(:,:,wind)=specpad(:,wind:wind+(nincrements)-1);
       
        if (trial>=maxtrials)
            break;
        end
    end
    
     if (trial>=maxtrials)
        break;
    end
end

%create the SRObjects
data=SRBirdSong(srparams);

%copy the object
obj=ptrbdata.bsdata;