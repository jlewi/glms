%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
function obj=loadobj(lobj)
    %check if lobj is a structure
    %this indicates the class structure has changed and we need to handle
    %the conversion   
    if isstruct(lobj)
        warning('Saved object was an older version. Converting to newer version');
        %create a blank object
        obj=BSData();
        
        %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(lobj);
        
        %struct for object
        sobj=struct(obj);
        for j=1:length(fnames)
            %make sure field hasn't been delete
             if ~isfield(sobj,fnames{j})
                 error('Field %s has been removed in newest version of class.',fnames{j});
             else
                   obj.(fnames{j})=lobj.(fnames{j});
             end
        end
        %provide warning message about any new fields
        nfields='';
        fnames=fieldnames(sobj);
        for j=1:length(fnames)
            if ~isfield(lobj,fnames{j})
                nfields=sprintf('%s \n',fnames{j});
            end
        end
        warning('The following fields were not in the older version. They will be set to default values. \n %s',nfields);
    else
        obj=lobj;
    end
    
     
    %convert old versions
    if (obj.version<080528)
       %we need to compute the fields obsrvparam.tnsamps
       %and obj.stimparam.nobsrvwind
       
       filedata=load(getpath(obj.fname));
       
       %to compute tnsamps we need the sampling rate
       filename1 = fullfile(getpath(obj.stimdir),filedata.waves{1});
       [stim fs]=wavread(filename1);
      
       
       obj.obsrvparam.tnsamps=obj.obsrvparam.tlength/(1/fs);
       obj.stimparam.nobsrvwind=obj.stimparam.tlength/obj.obsrvparam.tlength;
       
       obj.version=080528;
    end
    
    if (obj.version<080722)
        
        %we add 1 to nobsrvwind because before I was still using
        %getnincrements which was rounding up
        obj.stimparam.nobsrvwind=obj.stimparam.nobsrvwind+1;
       obj.stimparam.nfreqs=nan;
       obj.version=080722;
    end
   %load the data
   obj=loaddata(obj);
%    for ind=1:length(obj.filedata.waves)
%       obj.stimspec(ind).fname=obj.filedata.waves{ind}; 
%    end
%    
%    %compute last trial
%    obj=wavechunkinfo(obj);

   