%function getnincrements=nincrements(obj)
%	 obj=BSData object
% 
%Return value: 
%	 nincrements=obj.nincrements 
%         %nincrements is the number of frame's (i.e the number of columns of the
%        %spectrogram) corresponding to the input on each trial
function [obj,nincrements]=getnincrements(obj)
        error('07-22-2008. getnincrements is now obsolete since we store the number of columns in the STRF in stimparam.nobsrvwind'); 
