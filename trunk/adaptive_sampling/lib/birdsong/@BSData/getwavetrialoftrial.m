%function wavetrialoftrial=getwavetrialoftrial(obj,trial)
%	 obj=BSData object
%    trial - the glm trial index
%           
%Return value: 
%	 wavetrialoftrial=obj.wavetrialoftrial 
%       the index into obj.data.stimindex and obj.data.stimrepeat
%       corresponding to trial trial.
function wavetrialoftrial=getwavetrialoftrial(obj,trial)

[obj,ntrialssofar]=getntrialssofar(obj);

wavetrialoftrial=binarysubdivide(ntrialssofar,trial);
