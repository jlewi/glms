%function [ntrials,cstart]=getntrialsinwave(obj,windex)
%       windex - index of wave file
%
% Return value
%     ntrials - This function determines how many trials we divide the
%     windex wave file (including the pre/post silences) into.
%
%     cstart - This is the column in allobsrv corresponding to the
%     observation on the first trial constructed from the windex wave file.
%       
%     i.e:
%        [obj,stimspec,obsrv]=getallwaverepeats(obj,windex);
%        strfdim=getstrfdim(bdata);
%
%        The first trial from this wave file has stimulus
%            stimspec(:,strfdim(2))
%        and observation
%            obsrv(:,cstart)
%Revision:
%
% 07-24-2008: Optimized the call to get ntrials in wav.
%              No longer actually get the spectrograms
%              No longer return the object
% 05-07-2008: I was adding 1 to strfdim(2) to get cstart and that is wrong
function [ntrials,cstart]=getntrialsinwave(obj,windex)

strfdim=getstrfdim(obj);

%get the length of the silences before and after the stimulus;
[obj,npre,npost]=nframesinsilence(obj); 
 
%number of trials is size of spectrogram of stimulus - size of strf
nwave=0;
if isempty(obj.stimspec(windex).spec)
    [obj,wavespec]=getwavespec(obj,windex);
    nwave=size(wavespec,2);
else
    nwave=size(obj.stimspec(windex).spec,2);
end

%ntrials=size(fullspec,2)-strfdim(2)+1;
ntrials=npre+npost+nwave-strfdim(2)+1;
cstart=strfdim(2);
