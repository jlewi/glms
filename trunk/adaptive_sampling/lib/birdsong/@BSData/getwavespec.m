%function [obj,spec,freqs]=getwavespec(obj,sindex)
%   obj    - bsdata object
%   sindex - the index of the stimulus to get the spectrogram for
%
%Return value:
%   obj - return the object so we store the spectrogram if it hasn't been
%   computed yet.
%   spec - spectrogram of the sound
%
%Explanation: Gets the spectrogram of the wavefile. Thus the spectrogram
%   does not include any pre and post silences.
%The spectrogram  of each wave file is computed only once. Once its computed
%it is stored for further use
%
%Uses David Schneider's code to compute the spectrogram.
%
%%Revisions
%   3-25-2008 - use freqsubsample to subsample the frequencies
function [obj,spec,outfreqs]=getwavespec(obj,sindex)

%if we've already computed the spectrogram for this data then just
%return it
if ~isempty(obj.stimspec(sindex).spec)
    spec=obj.stimspec(sindex).spec;
    outfreqs=obj.stimspec(sindex).freqs;
else
    %we need to compute the spectrogram of this stimulus
    %code below was copied from David Schneider's display_stim script


    %****************************************
    % Load the stimulus and calculate necessary parameters
    %****************************************
    %filename as stored in the .mat file is already missing the .wav
    %extension
    filename1 = fullfile(getpath(obj.stimdir),obj.stimspec(sindex).fname);
    %    filename1 = filename1(1:end-4);  %chop off the .wav
    [stimulus fs nbits] = wavread(filename1);

    obj.stimspec(sindex).stimsize = length(stimulus);
    %save the sampling rate in hz
    obj.stimspec(sindex).fs=fs;

    %    stimduration = round(length(stimulus)*1000/fs)+1; %duration of stim in msec

    %get the information about how we will divide this stimulus into frames
    %A frame is the window in which we compute the fft
    [obj,finfo]=frameinfo(obj,sindex);

    %****************************************
    % Compute the spectrogram
    %****************************************
    [spec outfreqs] = spectrogram_ds(stimulus,finfo.binsize,finfo.frameCount,...
        finfo.increment,finfo.nFTfreqs,finfo.DBNOISE,fs,finfo.initialFreq,finfo.endFreq);

    %subsample the frequencies
    spec=spec(1:obj.stimparam.freqsubsample:end,:);
    outfreqs=outfreqs(1:obj.stimparam.freqsubsample:end);
    obj.stimspec(sindex).spec=spec;
    obj.stimspec(sindex).freqs=outfreqs;


end