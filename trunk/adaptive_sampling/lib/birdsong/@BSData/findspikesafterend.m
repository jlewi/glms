%function wind=findspikesafterend(bdata)
%
%Return value:
%   The count of the wave file for which there were spikes
%   occuring after the silence following the post stimulus silence
function wind=findspikesafterend(obj)

wind=nan*zeros(1,length(obj.data.stimindex));


for sindex=1:length(obj.data.stimindex)
   [obj, stiminfo]=getstiminfo(obj,obj.data.stimindex(sindex));
   
   stimes=obj.data.spiketimes{sindex};
   
   if (stimes(end)>stiminfo.duration+obj.data.presilence+obj.data.postsilence)
      wind(sindex)=1; 
   end
end

wind=find(wind==1);
