%function stimtlength=getstimtlength(obj)
%	 obj=BSData object
% 
%Return value: 
%	 stimtlength=obj.stimtlength 
%
function stimtlength=getstimtlength(obj)
	 stimtlength=obj.stimparam.tlength;
