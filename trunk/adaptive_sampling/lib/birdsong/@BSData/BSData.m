%function sim=BSData(fname)
%   fname - name of the matlab file containing the data
%
%Optional parameters
%   stimdir - FilePath object pointing to location of the actual stimuli
%   stimdur - length in seconds of the stimulus on each trial.
%   response (i.e the size of the bin in which we look for a spike).
%       obsolete 05-28-08
%   stimnobsrvwind - how long to make the stimulus window specified as a
%      multiple of the observation window.
%   obsrvwindowxfs - The lenght of the observation window specified
%                  as the number of samples. Samples refers to the
%                   sampling rate of the wave files
%
% Explanation: Class acts as an interfce to the BirdSong data. Handles the
% chunking of the recording into stimulus response pairs.
%
% Revision:
%  080724
%      Add a field wavinfo which contains info about the wave files
%           e.g ntrials in the wavefile
%           #repeats. 
%           I do this because when optimizing the stimuli we often need to
%           know information about the wave files such as the number of
%           trials
%           Currently I compute the number of trials by getting the
%           dimensions of the full spectrogram which could be expensive. 
%  080629
%      start using the new Matlab Object
%      may the object a descendent of class Handles so that
%      we store references to the actual data so that we don't have to
%      recompute things like the spectrogram
%  080520
%       For the responses return a vector of spike counts.
% Revision: 080422. Fixed bug in how I load and parse the spike times.
classdef (ConstructOnLoad=true) BSData < handle
    %**************************************************************
    %Required parameters/Multiple constructors
    %*****************************************************************
    %each element of the con array describes a constructor
    %   .rparams - string array of the required parameters
    %   .cfun   - function (or number) to indicate which constructor to call
    %

    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    % stimparam - structure storing information about the stimuli such as
    %             parameters for the spectrogram of the stimulus
    %           .tlength - 
    %                    stimparam.nobsrvwind*obsrvparam.tlength
    %           .nobsrvwind - 
    %                    - user specifies nobsrvwind. This ensures duration of
    %                    stimulus is a multiple of the observation window.
    %                    - this is how many columns of the spectrogram are
    %                    in each STRF
    %           .freqsubsample - subsample the frequencies in the spectrogram
    %                    - must be an integer
    %                    = 1 - no subsampling
    %                    = 2 - take every other frequency
    %                    =...  etc...
    %           .nfreqs   - number of frequencies in the spectrogram/strf
    %                     - this is set when we call getstrfdim
    %
    % obsrvparam - structure describing information about the response
    %           .tlength - how long should the response window be in sec
    %                    - this also determines the increment in which we
    %                    slide a window over the stimulus for the purpose of
    %                    computing the FFT which form the columns of the
    %                    spectrogram
    %                    - These values are the same because otherwise we have
    %                    alighnment issues
    %           .tnsamps - the length of the observation window in terms of the
    %               samples of the wave files
    %               i.e
    %               tlength=tnsamps*1/fs
    %               fs = the sampling rate of the wave files
    %
    % filedata    - internal variable to store the data loaded from the file
    %             - this stores the unmodified structure loaded from the file
    %
    % data         - this contains a subset of data from filedata which is
    %               reformated to be more convenient to work with
    %   .spiketimes -a cell array each row contains the times of the spikes on
    %                a different trial
    %   .stimindex - the index of the stimulus presented on each trial
    %   .stimrepeat - which repeat of the wave file this is
    %               - i.e The 'l' wave trial is the stimrepeat(l) repeat
    %                 of the stimindex(l) wave file
    %   .ntrialssofar - This is an array same length as stimindex
    %                   ntrialssofar(j)=sum_{i=1:j} stimspec(j).laststart
    %                 - i.e it is a running sum of how many trials we divde
    %                 the stimuli through stimindex(j) into.
    % stimdir  - directory where wave files of the stimuli are stored
    %
    % stimspec - is a structure array - one entry for each stimulus
    %   .fname - name of the wave file
    %   .spec  - matrix of the spectrogram
    %   .freqs - frequencies
    %   .stimsize - number of time samples of the sound
    %   .fs    - sample rate of wave file in herts
    %   .laststart - computed by wavechunkinfo
    %
    %declare the structure
    %
    %******************************************************************
    %Default values
    %******************************************************************

    properties(SetAccess=private,GetAccess=public)
        version=080722;
        fname=[];
        %tlength is in sec
        obsrvparam=struct('tlength',nan,'tnsamps',nan);
        stimparam=struct('tlength',nan,'freqsubsample',1,'nobsrvwind',nan,'nfreqs',nan);
        stimspec=[];
        data=[];
        filedata=[];
        stimdir=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','stimuli'),'isdir',true);       
    end

    %********************************************************************
    %methods
    %******************************************************************

    properties(SetAccess=private,GetAccess=public,Dependent)
       neuron=[]; 
    end
    methods
    
        function neuron=get.neuron(obj)
           neuron=obj.filedata.filename;
        end
        function neuron=set.neuron(obj,val)
            %do nothing
        end
        function obj=BSData(varargin)


            con(1).rparams={'fname'};
            con(1).cfun=1;

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
             
                    return
                    
                case 1
                    %***********************************************
                    %Argument should be of type bsdata
                    %in which case we make a copy of the object
                    %*******************************************
                    if isa(varargin{1},'BSData')
                        eobj=varargin{1};
                        
                        edata=struct(eobj);
                        %copy the object but
                             %leave the data fields blank
                        %we do this because the only time we will want
                        %to copy the object is when we are saving it
                        %so need to waste memory by copying the data when
                        %we're only going to erase them
                        edata=rmfield(edata,{'data','filedata','stimspec'});
                        fnames=fieldnames(edata);
                        for findex=1:length(fnames)
                           obj.(fnames{findex})=edata.(fnames{findex});
                        end
                        %copy the object
%                         obj.version=eobj.version;
%                         obj.fname=eobj.fname;
%                         obj.stimparam=eobj.stimparam;
%                         obj.obsrvparam=eobj.obsrvparam;
                        
                   
                        return;
                      
                    else
                        error('Argument should be of type BSData');
                    end
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    obj.fname=params.fname;

                    %remove fname so we are left with just optional parameters
                    params=rmfield(params,'fname');
                otherwise
                    error('Constructor not implemented')
            end

            %optional fields
            fnames=fieldnames(params);

            compobsrvtlength=false;
            compstimtlength=false;
            for j=1:length(fnames)
                switch fnames{j}
                    case 'stimdir'
                        obj.stimdir=params.('stimdir');
                    case 'stimdur'
                        obj.stimparam.tlength=params.('stimdur');
                        error('stimdur is obsolete: specify stimnobsrvwind instead');
                    case 'freqsubsample'
                        obj.stimparam.freqsubsample=params.freqsubsample;
                    case 'obsrvwindow'
                        obj.obsrvparam.tlength=params.obsrvwindow;
                        error('obsrvwindow is obsolete. specify obsrvwindowxfs instead');
                    case 'stimnobsrvwind'
                        %we need to wait till obsrvtlength is known;
                        obj.stimparam.nobsrvwind=params.stimnobsrvwind;

                    case 'obsrvwindowxfs'
                        %we need to compute tlength
                        %because we need the sampling rate of the wave files

                        obj.obsrvparam.tnsamps=params.obsrvwindowxfs;

                    otherwise
                        error('%s is not a valid optional parameter.',fnames{j});
                end
            end


            %load the data
            obj=loaddata(obj);

        end
        
        %get the number of wavefiles
        nwavefiles=getnwavefiles(obj);
        
        %get the number of trials in a wave file
        [ntrials,cstart]=getntrialsinwave(obj,windex)
    end

    methods (Static = true)
        obj=loadobj(a);

    end
end
