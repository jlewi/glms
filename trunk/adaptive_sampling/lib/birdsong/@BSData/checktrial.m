%function [bsdata,iscorrect]=checktrial(obj,trial)
%   obj -
%   trial
%
%Explanation: This function checks that the SRObj returned
%   on trial=trial is correct.
%
%   To do this we form the spectrogram of the full stimulus presentation
%   for this trial (i.e includes pre and post synaptic silences)
%   and then we take the appropriate submatrix for this input and verify
%   it matches the input returned by get trial
%
%Revision:
%   7-22-2008 
%       check trial now counts the actual number of spikes observed in the
%       observation window
function [obj,iscorrect]=checktrial(obj,trial)

iscorrect=true;
%get the data using gettrial
[obj,stim,shist,obsrv]=gettrial(obj,trial);


%***********************************************************************
%verify the trial
%**********************************************************************

%number of frames corresponding to the pre and postsynaptic silences
%get the length of the silences before and after the stimulus;
 [obj,npre,npost]=nframesinsilence(obj); 


%length of the input in frames
nincrements=obj.stimparam.nobsrvwind;

%determine the number of stimuli which have been presented before this
%trial
ntrialssofar=0;
sindex=0;

while (ntrialssofar<trial)
    sindex=sindex+1;
    %get information about the stimulu
    [obj,finfo]=frameinfo(obj,obj.data.stimindex(sindex));
    %number of trials contained in the sindex presentation of a stimulus
    trialsingroup=npre+npost+finfo.frameCount-nincrements+1;

    ntrialssofar=ntrialssofar+trialsingroup;
end

%how many trials were in the stimulus presentations before the sindex
%presentation
ntrialsprevious=ntrialssofar-trialsingroup;

%make trial relative to the number of trials in the sindex presentation
rtrial=trial-(ntrialsprevious);

%form the spectrogram including the pre and post silences
[obj,fullspec]=getfullspec(obj,obj.data.stimindex(sindex));

tinput=fullspec(:,rtrial:rtrial+nincrements-1);

%get the actual response
spikeframe=rtrial+nincrements-1;
starttime=(spikeframe-1)*obj.obsrvparam.tlength;
endtime=starttime+obj.obsrvparam.tlength;

spikesafter=find(obj.data.spiketimes{sindex}>=starttime & obj.data.spiketimes{sindex}<=endtime);
%warning('Need to update this code to count the actual number of spikes.');
if ~isempty(spikesafter)
 tobsrv=length(spikesafter);
else

tobsrv=0;
end



%compare the data
if (any(getData(stim)~=tinput(:)))
    error('Inputs do not match for trial %d ',trial);
    iscorrect=false;
end
if (obsrv~=tobsrv)
    error('Observations do not match for trial %d ',trial);
    iscorrect=false;
end

