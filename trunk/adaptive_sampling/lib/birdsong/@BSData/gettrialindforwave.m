%function [bdata,trials]=gettrialindforwave(bdata,windex)
%   windex - index of the wavefile
%
%Return value:
%    trials - np x 2
%       np - number of times we present the windex wave file
%
%       The ith row returns the first and last trial corresponding to the
%       first and last trial into which we divide the ith presentation of
%       the windex wavefile into. I.e you can call gettrial with these
%       trials and the stimulus for this trial will be a subwindow
%       of the spectrogram for the windex wave file.
%       
%Revisions:
%   08-23-2008 - Allow a single return object
function [varargout]=gettrialindforwave(bdata,windex)

%get the wavetrial numbers on which we present this wave file
wavetrials=find(bdata.data.stimindex==windex);

trials=zeros(length(wavetrials),2);

[bdata,ntrialssofar]=getntrialssofar(bdata);
for r=1:length(wavetrials)
    if (wavetrials(r)>1)
        trials(r,1)=ntrialssofar(wavetrials(r)-1)+1;
    else
        trials(r,1)=1;
    end
    
        trials(r,2)=ntrialssofar(wavetrials(r)); 
end

if (nargout==2)
    varargout{1}=bdata;
    varargout{2}=trials;
    warning('In future releases we may no longer return BSData as the first argument.');
else
    varargout{1}=trials;
end