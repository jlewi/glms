%function [obj]=wavechunkinfo(obj,sindex)
%   obj - the BSData object
%   sindex - the index of the wave file
%
%Return Value
%   cinfo - a structure array containing information about how this wave file
%       will be chunked into individual trials.
%       ,lastrial - A frame is the column index in the spectrogram
%           of the wave file. lasttrial is the first column index 
%           of the spectrogram of the input on the last trial for this wave
%           file
%           i.e if spec is the spectrogram of this wave file
%           spec(:,lastrial:end) is the last input from this wave file
%
%
function [obj]=wavechunkinfo(obj,sindex)
    
%compute how man trials there will be
%laststart - This is an array of the index of the first frame in the input
%            on the last trial
%          - laststart(j) - the value for the jth sound file
%laststart=zeros(1,length(obj.stimspec));

for sindex=1:length(obj.stimspec)
    %get information about how the stimulus will be divided into frames
    [obj,finfo]=frameinfo(obj,sindex); 
   [obj,stiminfo]=getstiminfo(obj,sindex);
    
   
     %nincrements is the number of frame's (i.e the number of columns of the
    %spectrogram) corresponding to the input on each trial
    nincrements=obj.stimparam.nobsrvwind;
    
    %how many frames are there in total in this stim
    %npre and npost are the number of frames in the silence preceding and
    %following the stimulus
    [obj,npre,npost]=nframesinsilence(obj); 
    
    %framecount - the number of frames in the full stimulus
    %   The full stimulus includes the stimulus + the silences before and
    %   after the stimulus.
    %operate on the object  
    framecount=finfo.frameCount+npre+npost;
    
%    [obj,stiminfo]=getstiminfo(ptrbdata.bsdata,sindex);
    %last start includes the pre and post silences.
    obj.stimspec(sindex).laststart=framecount-nincrements+1;
    
end
