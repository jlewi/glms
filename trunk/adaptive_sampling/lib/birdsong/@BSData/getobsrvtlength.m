%function obsrvtlength=getobsrvtlength(obj)
%	 obj=BSData object
% 
%Return value: 
%	 obsrvtlength=obj.obsrvtlength 
%
function obsrvtlength=getobsrvtlength(obj)
	 obsrvtlength=obj.obsrvparam.tlength;
