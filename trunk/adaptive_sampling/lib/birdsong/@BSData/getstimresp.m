%function obsrv=stimresp(obj,sindex)
%	 obj=BSData object
%    sindex - index of the stimuli to return response for
%Return value: 
%	 obsrv - Cell array nrepeats,1 containing the spike times in response to this
%	 stimulus. Each row is the response on a different trial.
%
%
%Revision:
%   05-16-2008
%       Made obsrv a column vector. This makes it consistent with
%       spikeIndex
%       Also, this way each row represents a row of a raster plot of the
%       spike times.
%   04-29-2008
%       instead of using find to get the indexes just filedata.spikeTimes
function [obsrv]=getstimresp(obj,sindex)
	 
    %find the trials on which we presented this stimulus
    %trials=find(obj.data.stimindex==sindex);

    obsrv=cell(length(obj.filedata.spikeIndex{sindex}),1);
    
    for r=1:length(obsrv)
        spikeind=obj.filedata.spikeIndex{sindex}{r};
    obsrv{r}=obj.filedata.spikeTimes(spikeind);
    end
    %obsrv(1,:)=obj.data.spiketimes(trials)';
    
