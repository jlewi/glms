%function trials=gettrialswithspikes(bdata)
%   bdata - object
%
%Return value:
%   trials - an array of the trial numbers on which spikes occured
%
%Explanation we loop across the spike times of all wave presentations
%   and determine the trial on which each spike occured
%
%   This is useful for computing the spike trigerred average
%
%  Some spikes will be thrown out. This can happen if the spike occured
%  early enough in the trial that we have not been recording long enough to
%  the full input given the length of the window used.
%
function trials=gettrialswithspikes(bdata)

%compute the total number of spikes so we can initialize trials
nspikes=length([bdata.data.spiketimes{:}]);

trials=zeros(1,nspikes);

[bdata,ntrialssofar]=getntrialssofar(bdata);

tind=1;

%length of the input (number of columns from the spectrogram) on each trial
inplength=bdata.stimparam.nobsrvwind;

%get the length of the silences before and after the stimulus;
%[obj,npre,npost]=nframesinsilence(obj); 

for wind=1:length(bdata.data.spiketimes)
    stimes=bdata.data.spiketimes{wind};
    
   
          [bdata,stiminfo]=getstiminfo(bdata,bdata.data.stimindex(wind));
          
        %what frame does this spike correspond to 
        %remember spike times are measured with respect to the silences 
        %spike times are measured in seconds
        spikeframe=ceil(stimes/bdata.obsrvparam.tlength);

        %the frame of the observation for the first trial this wave file
        firstobsrv=inplength;
        
        %relative trial number i.e before including the trials in previous
        %wave files
        %a relative trial <=0 means we will throw this spike out because
        %its too early meaning it occurs before the stimulus is full
        %observed
        reltrial=spikeframe-firstobsrv+1;
    
        %set any spikes to be thrown out to nan
        %throw out spikes which occur too early (i.e we did not start
        %recording the i input soon enough)
        reltrial((reltrial<=0))=nan;
        
        %throw out spikes which occur after the fixed period of silence
        %following the stimulus
        reltrial((reltrial>stiminfo.laststart))=nan;
        
        %now we need to add to reltrial the number of trials due to all
        %previous wave files
        if (wind>1)
           reltrial=reltrial+ntrialssofar(wind-1); 
        end
        
        %set the appropriate entries of trials
        trials(tind:(tind+length(reltrial)-1))=reltrial;
        tind=tind+length(reltrial);
end

%select the entries which aren't nan;
trials=trials(~isnan(trials));
