%function [windex, repeat]=windexonwavetrial(obj,wavetrial)
%	 obj=BSData object
%   wavetrial - the number of the presentation

%%Return value: 
%	 windex - is the index into obj.stimspec corresponding to the n=wavetrial file
%       played to the organism
%   repeat - the repeat of the wave file.
%
%Explanation:An experiment consists of presenting a number of different
%wave files. Each wavetrial denotes the playing of a wave file. Each
%wavetrial consists of playing one (windex) of the wave files. 
%
%
function [windex,repeat]=getwindexonwavetrial(obj,wavetrial)
    windex=obj.data.stimindex(wavetrial);
    repeat=obj.data.stimrepeat(wavetrial);