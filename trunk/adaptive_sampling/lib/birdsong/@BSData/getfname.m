%function fname=getfname(obj)
%	 obj=BSData object
% 
%Return value: 
%	 fname=obj.fname 
%
function fname=getfname(obj)
	 fname=obj.fname;
