%function mseerr=bspsthmse(bssimobj,post,wfile)
%   bssimobj - the simulation object
%   post - distribution on the parameters of theta
%   wfile - index of the wave file
%
% Return value:   
%   mseerr - the log of the mean squared error between the empirical and predicted
%   wave files
%   exprate - the expected firing 
%   emprate - the empirical firing rate
%
%
% Explanation: This function only works for the canonical Poisson model.
% For this model we can compute the expectation
% E_\param|\mu_t,c_t exp(s_t^T \param) 
% This gives the expected value of the firing rate.
% We compare this predicted value to the empirical measurement of the
% firing rate. 
%
% To form the spike history signal we average the spike history across all
% repeats of the stimulus
function [mseerr,exprate,emprate]=bspsthexpmse(bssim,post,wfiles)


%data=load(getpath(datafile));
bdata=bssim.stimobj.bdata;
allpost=bssim.allpost;
mobj=bssim.mobj;

%make we have the canonical poisson
glm=getglm(mobj);

if (~isa(glm,'GLMPoisson') || ~iscanon(glm))
    error('this function only works with the canonical poisson model');
end


mseerr=zeros(1,length(wfiles));

%********************************************************
%end post processing
%************************************************************
wcount=0;
for wavfile=wfiles
    wcount=wcount+1;

    %how many repeats of this wave file are there
    nrepeats=getnrepeats(bdata,wavfile);    
    ntrials=getntrialsinwave(bdata,wavfile);
    
    frate=zeros(1,ntrials);
    allobsrv=zeros(nrepeats,ntrials);

    shist=zeros(getshistlen(mobj),ntrials);
    
    %we form the spike history signals by averaging
    %the spike history signals across repeats
     %loop over the repeats of the wave file
    for repeat=1:nrepeats
       %we need to compute the expected firing rate on each trial
       %1. get the input on each trial
       [stim,shistrep,allobsrv(repeat,:)]=gettrialwfilerepeat(bdata,wavfile,repeat*ones(1,ntrials),[1:ntrials],mobj);
        
       shist=shist+shistrep/nrepeats;
    end
    
    %form the inputs
    inputs=projinp(bssim.mobj,stim,shist);

    %compute the mean and variance of rho
        muproj=getm(post)'*inputs;
        
        %compute sigma
        sigma=qprod(inputs,getc(post));
        

        %the log of the expected firing rate for the canonical poisson.
        %we have to use the log because otherwise it blows up to infinity
    logfrate=muproj+1/2*sigma;
    
    avgspike=mean(allobsrv,1);





    if (length(wfiles)>1)
        exprate{wcount}=frate;
        emprate{wcount}=avgspike;
    else
        exprate=frate;
        emprate=avgspike;
    end

    %compute the mse
    %we have to compute this in the log domain otherwise it blows up
    
    %first we subtract the maximum from logfrate
    %so
    %frate=exp(logsf)*exp(logfratescaled);
    logsf=max(logfrate);
    logfratescaled=logfrate-logsf;
    
    %avgspike=exp(logsf)*exp(logavgspikescaled);
    logavgspike=log(avgspike);
    logavgspikescaled=logavgspike-logsf;
    
    %dfrate=frate-avgspike=
    %exp(logsf)*(exp(logfratescaled)-exp(logavgspikescaled))
    %dfrate.^2=exp(2*logsf)*(exp(logfratescaled)-exp(logavgspikescaled))^2
    
    %mse=sum(dfrate.^2)^.5
    %mse =
    %exp(logsf)*sum((exp(logfratescaled)-exp(logavgspikescaled))^2)^.5
    %logmse=logsf+1/2*logsum((exp(logfratescaled)-exp(logavgspikescaled))^2
    %
    logmse=1/2*log(sum((exp(logfratescaled)-exp(logavgspikescaled)).^2)/length(frate));
    logmse=logsf+logmse;
%    mdist=(sum((frate-avgspike).^2)/length(frate))^.5;
    mseerr(wcount)=logmse;

end