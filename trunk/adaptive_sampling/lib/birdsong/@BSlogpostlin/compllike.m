%function [ll,dlpost,d2lpost]=compllike(uobj,bdata,mobj,theta,prior)
%   uobj
%   mobj - model
%   theta - the point at which we compute the first and second derivative
%       of the log likelihood
%
%Return value - computes the log likelihood upto an additive constant
%   ll    - the log-likelihood
%   dll   - 1st derivative of the log likelihood
%   d2ll  - 2nd derivative of the log likelihood
%
function varargout=compllike(uobj,mobj,theta)

%whether to compute the derivatives
compdglm=false;
compd2glm=false;

if (nargout>=2)
    compdglm=true;
end

if (nargout>=3)
    compd2glm=true;
end

%we need to project the parameter space into full space
fulltheta=mobj.fullbasis*theta;

%now use the base class to compute the log-likelihood and derivatives
%w.r.t. to fulltheta
%we call compllike but using an MParamObj not the actual mobj
%because we need the calls to getparalmen to return the proper dimensions
if isempty(uobj.mfull)
   uobj.mfull=MParamObj('mobj',mobj); 
end

switch nargout
    case {0,1}
        [ll]=compllike@BSlogpost(uobj,uobj.mfull,fulltheta);
    case 2
        [ll,dglm]=compllike@BSlogpost(uobj,uobj.mfull,fulltheta);
    case 3;
        [ll,dglm,d2glm]=compllike@BSlogpost(uobj,uobj.mfull,fulltheta);
end

varargout{1}=ll;

if compdglm
    varargout{2}=mobj.fullbasis'*dglm;
end


if (compd2glm)
    d2glm=mobj.fullbasis'*d2glm*mobj.fullbasis;
    varargout{3}=d2glm;
end

