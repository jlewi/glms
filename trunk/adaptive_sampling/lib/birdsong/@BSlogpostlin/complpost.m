%function [dlpost,d2lpost]=compd2lpost(uobj,bdata,mobj,theta,prior)
%   uobj
%   mobj - model
%   theta - the point at which we compute the first and second derivative
%       of the log likelihood
%   prior - prior
%
%Return value - computes the log posterior upto an additive constant
%
%   dlpost - derivative of the posterior
%   d2lpost -2nd derivative
%            only computed if there are two output variables
%
function varargout=complpost(uobj,mobj,theta,prior)


%error('this function does not return the same value for the derivative of the log posterior as compd2lpost');
%******************************************************
%compute the log of the prior
%******************************************************
%logpdf will return distributed values for logprior, and d2logprior
%if our covariance matrix is represented using a distributed matrix.
if (nargout==1)
    logprior=logpdf(prior,theta);
elseif (nargout==2)
    [logprior, dlogprior]=logpdf(prior,theta);
else
    [logprior, dlogprior,d2logprior]=logpdf(prior,theta);
end
%**********************************************************
%compute the log of the likelihood.
if (nargout<=1)
    loglike=compllike(uobj,mobj,theta);
elseif (nargout==2)

    [loglike,dloglike]=compllike(uobj,mobj,theta);
else

    [loglike,dloglike,d2loglike]=compllike(uobj,mobj,theta);
end

logpost=logprior+loglike;

varargout{1}=logpost;

if (nargout>=2)
    varargout{2}=dlogprior+dloglike;
end

if (nargout>=3)
    varargout{3}=d2logprior+d2loglike;
end

