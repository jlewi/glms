%
% Explanation: This class computes the log-posterior and its derivatives
% when we linearly transform the STRF into some other space like fourier
% space.
%
classdef (ConstructOnLoad=true) BSlogpostlin<BSlogpost
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        vbslogpostlin=081101;

    end

     properties(SetAccess=private, GetAccess=public,Transient=true)
         %the full model
         %i.e the model in which there is no linear transformation of the
         %strf;
         mfull=[];

    end
    methods
        function obj=BSlogpostlin(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={};

           

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=struct();
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=struct();

                otherwise
                    if isnan(cind)
                        bparams=params;
                        cind=1;
                    else
                        %remove fields which are for this class
                        try
                            bparams=rmfield(params,'field');
                        catch
                        end
                    end
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@BSlogpost(bparams);

            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 0
                    %do nothing used by load object
                case 1
                    %do nothing except call base class
                otherwise
                    error('Constructor not implemented')
            end



        end
    end
end


