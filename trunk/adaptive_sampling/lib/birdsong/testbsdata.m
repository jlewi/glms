%4-29-2008
%
%Test harness: This test harness verifies that gettrial and
%getallwaverepeats return the same value.
%	Getallwaverepeats returns a matrix corresponding to the spectrogram to the fulle spectrogram (i.e includes silences) for the ith wave file. And a matrix where each row is the row of corresponding observations.
%   We reconstruct these matrices by calling gettrial for all trials corresponding to this spectrogram.

setpathvars;
%*******************************************************************
%Create a bsdata object
%*********************************************************************

dfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1_corrected.mat'));



%how many columns should be in the STRF 
stimnobsrvwind=10;
%subsample the frequencies of the spectrogram to reduce the dimensionality
%of the stimulus
freqsubsample=4;
obsrvwindowxfs=250;
bdata=BSData('fname',dfile,'stimnobsrvwind',stimnobsrvwind,'freqsubsample',freqsubsample,'obsrvwindowxfs',obsrvwindowxfs);



%**********************************************************************
%loop over the wave files
   strfdim=getstrfdim(bdata);

   iscorrect=true;
%for windex=ceil(rand(1,1)*getnwavefiles(bdata))
for windex=5
   %call getallwaverepeats
   [bdata, allwaves.fullspec,allwaves.obsrv]=getallwaverepeats(bdata,windex);
   
   %get the glm trials corresponding to these wavefiles
   [bdata, tbtrial.trials]=gettrialindforwave(bdata,windex);
   
   %loop over the trials corresponding to each presentation
   %reconstruct the spectrogram and the observation vector and make sure
   %they are unchanged
   tbtrial.fullspec=zeros(strfdim(1),size(allwaves.fullspec,2));
   tbtrial.obsrv=zeros(size(allwaves.obsrv));
   

   for r=1:size(tbtrial.trials,1)
       tr=strfdim(2);
       fprintf('Testing %0.3g presentation of the %g wave file \n', r,windex);
        for trial=tbtrial.trials(r,1):tbtrial.trials(r,2)
            [bdata,stim,shist,obsrv]=gettrial(bdata,trial);
            
            %we take the last column of the glm input and add it to
            %the spectrogram of the full stimulus
            if (tr==strfdim(2))
               tbtrial.fullspec(:,1:tr)=getmatrix(stim);
            else
                imatrix=getmatrix(stim);
                tbtrial.fullspec(:,tr)= imatrix(:,end);
            end
            
            tbtrial.obsrv(r,tr)=obsrv;
            tr=tr+1;
        end
        
        %check the stimulus and observations match
        if (any(tbtrial.fullspec~=allwaves.fullspec))
            fprintf('Error: The spectrogram returned by allwaverepeats and calls to get trial did not match.');
            iscorrect=false;
        end
         if (any(tbtrial.obsrv(:,strfdim(2):end)~=allwaves.obsrv(:,strfdim(2):end)))
            fprintf('Error: The observation vector returned by allwaverepeats and calls to get trial did not match.');
            iscorrect=false;
        end
        
   end
   
   
end

fprintf('gettrial and getallwave repeats are consinstent! \n');