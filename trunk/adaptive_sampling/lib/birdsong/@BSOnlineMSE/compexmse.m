%function compexmse(bsall,nsave)
%   dset - the dataset to compute the expected mse for
%   outfile - file to periodically save the boject to so that
%
%Explanation: COmpute the expected mse error between the map and the final
%batch estimate
function msedata=compexmse(bsall,nsave)

if ~exist('outfile','var')
    outfile=[];
end

if ~exist('nsave','var')
    nsave=[];
end
if isempty(nsave)
    nsave= 100;
end

for bsind=1:length(bsall)
    bsmse=bsall(bsind);
    nsets=length(bsmse.seqsims);

    if isempty(bsmse.exmse)
        bsmse.exmse=repmat(struct('trials',[],'mse',[],'msestim',[],'mseshist',[],'msebias',[]),nsets,1);
    end

    ttrue=bsmse.bmlsim.results(end).theta;


    for sind=1:nsets
        bssim=bsmse.seqsims(sind);

        %check that the dimensions have the sequential simulations and batch
        %simulation match
        mseq=bsmse.seqsims(sind).mobj;
        mbatch=bsmse.bmlsim.mobj;

        if (mseq.nstimcoeff~=mbatch.nstimcoeff)
            error('The batch and sequential simulations do not have the same number of stimulus coefficients.');
        end
        if (mseq.alength~=mbatch.alength)
            error('The batch and sequential simulations do not have the same number of spike history coefficients.');
        end
        if (mseq.hasbias~=mbatch.hasbias)
            if (mseq.hasbias)
                error('The batch simulation has a bias but not the sequential simulations.');
            else
                error('The sequential simulation has a bias but not the batch simulation.');
            end

        end


        mobj=bssim.mobj;

        %determine which trials we saved the covariance matrix on
        %check if we are continuing a previous started computation
        if (~isempty(bsmse.exmse(sind).trials))
            trials=bsmse.exmse(sind).trials;


            %check for new trials
            alltrials=readmatids(bssim.allpost.cinfo.caccess);
            alltrials=sort(alltrials);

            if (length(alltrials)>length(trials))
                nnew=length(alltrials)-length(trials);
                bsmseq.exmse(sind).trials=[bsmseq.exmse(sind).trials nan(1,nnew)];

                bsmseq.exmse(sind).mse=[bsmseq.exmse(sind).mse nan(1,nnew)];
                bsmseq.exmse(sind).msestim=[bsmseq.exmse(sind).msestim nan(1,nnew)];

                if ( mseseq.alength>0)
                    bsmseq.exmse(sind).mseshist=[bsmseq.exmse(sind).mseshist nan(1,nnew)];

                end


                if  ( mseseq.hasbias>0)
                    bsmseq.exmse(sind).msebias=[bsmseq.exmse(sind).msebias nan(1,nnew)];

                end
            end

            tstart=find(isnan(bsmse.exmse(sind).mse),1,'first');

            if isempty(tstart)
                tstart=1+length(bsmse.exmse(sind).trials);
            end

        else
            %make sure trials is sorted
            trials=readmatids(bssim.allpost.cinfo.caccess);
            trials=sort(trials);
            tstart=1;

            bsmse.exmse(sind).trials=trials;

            bsmse.exmse(sind).mse=nan(1,length(trials));
            bsmse.exmse(sind).msestim=nan(1,length(trials));
            if (bssim.mobj.alength>0)
                bsmse.exmse(sind).mseshist=nan(1,length(trials));
            end
            if (bssim.mobj.hasbias)
                bsmse.exmse(sind).msebias=nan(1,length(trials));
            end

        end



        for tind=tstart:length(bsmse.exmse(sind).trials)
            if (mod(tind,100)==0)
                fprintf('sim %d of %d: trial %d of %d \n', sind,nsets, tind,  length(bsmse.exmse(sind).trials));
            end
            trial=bsmse.exmse(sind).trials(tind);

            mu=getm(bssim.allpost,trial);
            c=getc(bssim.allpost,trial);

            mseall=msefun(mu,c,ttrue);
            bsmse.exmse(sind).mse(tind)=mseall;

            mustim=mu(mobj.indstim(1):mobj.indstim(2));
            cstim=c(mobj.indstim(1):mobj.indstim(2),mobj.indstim(1):mobj.indstim(2));
            tstim=ttrue(mobj.indstim(1):mobj.indstim(2));
            msestim=msefun(mustim,cstim,tstim);
            bsmse.exmse(sind).msestim(tind)=msestim;

            if (mobj.alength>0)
                mushist=mu(mobj.indshist(1):mobj.indshist(2));
                cshist=c(mobj.indshist(1):mobj.indshist(2),mobj.indshist(1):mobj.indshist(2));
                tshist=ttrue(mobj.indshist(1):mobj.indshist(2));

                mseshist=msefun(mushist,cshist,tshist);
                bsmse.exmse(sind).mseshist(tind)=mseshist;
            end

            if (mobj.hasbias)
                musbias=mu(mobj.indbias);
                csbias=c(mobj.indbias,mobj.indbias);
                tsbias=ttrue(mobj.indbias:mobj.indbias);

                msebias=msefun(musbias,csbias,tsbias);
                bsmse.exmse(sind).msebias(tind)=msebias;
            end

            %periodically save the data
            if (~isempty(bsmse.outfile) && mod(tind,nsave)==0)
                fprintf('compexmse: saving data to %s \n',getpath(bsmse.outfile));
                if exist(getpath(bsmse.outfile),'file')
                    save(getpath(bsmse.outfile),'bsmse','-append');
                else
                    save(getpath(bsmse.outfile),'bsmse');
                end
            end
        end

        if (tstart<=length(bsmse.exmse(sind).trials))
            %save the data
            if exist(getpath(bsmse.outfile),'file')
                save(getpath(bsmse.outfile),'bsmse','-append');
            else
                save(getpath(bsmse.outfile),'bsmse');
            end
        end
        msedata(bsind,sind)=bsmse.exmse(sind);
    end

end
function mse=msefun(mu,c,ttrue)
mse=trace(c)+mu'*mu-2*ttrue'*mu+ttrue'*ttrue;
