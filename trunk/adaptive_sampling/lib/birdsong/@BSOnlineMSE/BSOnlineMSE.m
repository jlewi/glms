%function sim=BSOnlineMSE('seqfiles','bfiles');
%   seqfiles - an array of the seqfiles to load the bssimulation objects
%                 from
%   bfiles  - file contianing the batchml estimate of the file
%
%function sim=BSOnlineMSE('msefile')
%   msefile  - file to load the object from
% Explanation: Evaluate a sequential design by measuring how rapidly
%   the STRF converges to a batch ML estimate of the strf
%
%
%Revisions:
%   12-27-2008 - Make version a structure with each field the name of a
%             different class.
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) BSOnlineMSE <BSAnalyze
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %
    % seqsims - an array of the BSSimobj objects
    % bmlsim  - the batchml simulation against which we compare.
    %
    % expmse  - a structure storing the expected mse of each
    %           design in seqsims
    %         - we store this because it is expensive to compute
    %
    %seqfiles - the files storing the sequential simulations
    %bfile    - the file storing the batch simulation
    %
    %file     - where we store the object
    %
    %label    - label for the data
    %         - storing the label should make it unnecessary to actual load
    %           the BSSimobjects if we just want to plot the results
    properties(SetAccess=public,GetAccess=public)
        label=[];
    end
    properties(SetAccess=private, GetAccess=public)        
        seqfiles=[];
        bfile=[];
        exmse=[];
        outfile=[];
    end

     properties(SetAccess=private, GetAccess=public,Transient)
        seqsims=[];
        bmlsim=[];        
     end
    methods(Static)
       obj=loadobj(lobj); 
    end
    methods
        function label=get.label(obj)
           if (isempty(obj.label) && ~isempty(obj.seqsims)) 
               obj.label=obj.seqsims(1).label;
           end
           label=obj.label;
        end
        function outfile=get.outfile(obj)
           if (isempty(obj.outfile)  && ~isempty(obj.seqfiles))
              obj.outfile=BSAnalyze.filename(obj.seqfiles(1),obj.suffix); 
           end
           outfile=obj.outfile;
        end
        function seqsims=get.seqsims(obj)
            if isempty(obj.seqsims)
             for j=1:length(obj.seqfiles)
                         v=load(getpath(obj.seqfiles(j)));
                         if (j==1)
                            obj.seqsims =v.bssimobj;
                         else
                             obj.seqsims(j)=v.bssimobj;
                         end
             end
            end
            seqsims=obj.seqsims;
                     
        end
        
        function bmlsim=get.bmlsim(obj)
           if isempty(obj.bmlsim)
               v=load(getpath(obj.bfile));
                     
                     obj.bmlsim=v.bssimobj;
                     
           end
           bmlsim=obj.bmlsim;
        end
        function obj=BSOnlineMSE(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'seqfiles','bfile'};
            con(2).rparams={'msefile'};


               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    %do nothing
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);
            
            obj.suffix='bsonlinemse';
            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     if (length(params.seqfiles)>1)
                         error('seqfiles should be a single file');
                     end
                     
                     %check if there is an mse file for this datafile
                     obj.seqfiles=params.seqfiles;
                     if exist(getpath(obj.outfile),'file')
                        %load the object from the file
                        fprintf('file containing bsonlinemse object exists. loading object from file \n');
                        v=load(getpath(obj.outfile));
                        obj=v.bsmse;
                     end
%                      %load the datasets
%                      for j=1:length(params.seqfiles)
%                          v=load(getpath(params.seqfiles(j)));
%                          if (j==1)
%                             obj.seqsims =v.bssimobj;
%                          else
%                              obj.seqsims(j)=v.bssimobj;
%                          end
%                      end
%                      v=load(getpath(params.bfile));
%                      
%                      obj.bmlsim=v.bssimobj;
                     
                     obj.bfile=params.bfile;
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end


            %set the version to be a structure
            obj.version.BSOnlineMSE=090129;

        end
    end
end


