%function plotexpmse(obj)
%
%explanation: plot the expected mse of each component
%   For this we use the marginal distributions on the stimulus, spike
%   history, or bias components respectively.
%
function [fmse,otbl]=plotexpmse(obj)


%make a plot of the mse
nplots=1;

msedata=compexmse(obj(1));

hasshist=false;
hasbias=false;
if ~isempty(msedata.mseshist)
    nplots=nplots+1;
    hasshist=true;
end

if ~isempty(msedata.msebias)
    nplots=nplots+1;
    hasbias=true;
end

fmse=FigObj('name','mse','width',7,'height',7,'naxes',[nplots 1]);

rind=1;

%loop over the simulations because we only want to read the maps once
for sind=1:length(obj)

    rind=1;
    
    %get theta for this simulation
    lbl=obj(sind).label;
    
    msedata=compexmse(obj(sind));
    
    %*********************************************************************
    %Plot the mse of the stimulus coefficients
    %*****************************************************************
    setfocus(fmse.a,rind,1);
    
    
    hp=plot(msedata.trials,msedata.msestim);
    
    addplot(fmse.a(rind,1),'hp',hp,'lbl',lbl);

    %*********************************************************************
    %Plot the mse of the spike history coefficients
    %*****************************************************************
    if ~isempty(msedata.mseshist)
        rind=rind+1;
        setfocus(fmse.a,rind,1);
    
        hp=plot(msedata.trials,msedata.mseshist);
    
        addplot(fmse.a(rind,1),'hp',hp);
    end
    
    %*********************************************************************
    %Plot the mse of the bias
    %*****************************************************************
    if (hasbias);
       rind=rind+1;
        setfocus(fmse.a,rind,1);
        hp=plot(msedata.trials,msedata.msebias);
    
        addplot(fmse.a(rind,1),'hp',hp);
    end
    
end


%*********************************************************************
%add labels
%*********************************************************************
%%
rind=1;
title(fmse.a(rind,1),'Stimulus coefficients');
ylabel(fmse.a(rind,1),'$E_\theta||\theta- \hat{\theta}||^2$','Interpreter','latex');
    set(fmse.a(rind,1),'xscale','log');
    
if (hasshist)
    rind=rind+1;
    title(fmse.a(rind,1),'Spike history coefficients');
    ylabel(fmse.a(rind,1),'$E_\theta||\theta - \hat{\theta}||^2$','Interpreter','latex');
    set(fmse.a(rind,1),'xscale','log');    
end

if (hasbias)
    rind=rind+1;
    title(fmse.a(rind,1),'Bias');
    ylabel(fmse.a(rind,1),'$E_\theta||\theta- \hat{\theta}||^2$','Interpreter','latex');  
    set(fmse.a(rind,1),'xscale','log');
end

xlabel(fmse.a(rind,1),'trial');

lblgraph(fmse);

setposition(fmse.a(1,1).hlgnd,.5,.82,[],[]);

dinfo=cell(length(obj),1);

for sind=1:length(obj)
   dinfo{sind,1} =obj(sind).label;
   dinfo{sind,2} =getrpath(obj(sind).outfile);
end
dinfo{end+1,1}='explanation';
dinfo{end,2}=sprintf('We plot the expected mean squared error between theta and the true theta (\\hat{\\theta}}). \n The expectation is computed using the marginal distribution of the posterior on the stimulus, spike history, or bias coefficients.');

otbl={fmse,[{'class',mfilename('class');'function',mfilename('full');};dinfo]};
onenotetable(otbl,seqfname('~/svn_trunk/notes/compmse.xml'));
