%function plotexpmse(obj)
%   obj-nx2 matrix
%explanation: plot the expected mse of each component
%   For this we use the marginal distributions on the stimulus, spike
%   history, or bias components respectively.
%
%   we compute an average across rows of the difference between the mse
%   of the two columns.
%
%WARNING I MAKE ALL KINDS OF ASSUMPTIONS ABOUT HOW THE DATA IS ORGANIZED
%   1. second column of obj should contain the infomax designs.
%   2. I assume the trials on which the covariance matrix was saved is the
%   same for all designs.
function [fmse,otbl]=plotavgexpmse(obj)


%make a plot of the mse
nplots=1;

msedata=obj(1).exmse;

hasshist=false;
hasbias=false;
if ~isempty(msedata.mseshist)
    nplots=nplots+1;
    hasshist=true;
end

if ~isempty(msedata.msebias)
    nplots=nplots+1;
    hasbias=true;
end

fmse=FigObj('name','mse','width',7,'height',7,'naxes',[nplots 1]);

rind=1;


%compute the difference for all designs.
alltrials=[];
for sind=1:size(obj,1)


    mseimax=obj(sind,2).exmse;
    msebatch=obj(sind,1).exmse;

    tlast=min([mseimax.trials(end),msebatch.trials(end)]);


    mseimax.trials=mseimax.trials(mseimax.trials<=tlast);
    msebatch.trials=msebatch.trials(msebatch.trials<=tlast);

    tind=find(mseimax.trials-msebatch.trials==0);
    mseimax.trials=mseimax.trials(tind);
    msebatch.trials=msebatch.trials(tind);


    msediff(sind).trials=mseimax.trials;
    msediff(sind).msestim=msebatch.msestim(tind)-mseimax.msestim(tind);

    %compute the are between the curves
    msediff(sind).sumstim=sum(msediff(sind).msestim);
    
    if (hasshist)
        msediff(sind).mseshist=msebatch.mseshist(tind)-mseimax.mseshist(tind);
        
        %compute the are between the curves
        msediff(sind).sumshist=sum(msediff(sind).mseshist);
    
    end

    if (hasbias)
        msediff(sind).msebias=msebatch.msebias(tind)-mseimax.msebias(tind);
        
        %compute the are between the curves
        msediff(sind).sumbias=sum(msediff(sind).msebias);
    end

    msediff(sind).ntrials=length(msediff(sind).trials);

    
    alltrials=unique([mseimax.trials;alltrials]);
end

alltrials=sort(alltrials);

msestim=nan(size(obj,1),length(alltrials));

if (hasshist)
mseshist=nan(size(obj,1),length(alltrials));
end

if (hasbias)
   msebias=nan(size(obj,1),length(alltrials));
end
   
for sind=1:size(obj,1)
    tind=find(msediff(sind).trials-alltrials(1:msediff(sind).ntrials)==0);

    msestim(sind,tind)=msediff(sind).msestim(tind);
    
    if (hasshist)
       mseshist(sind,tind)=msediff(sind).mseshist(tind);       
    end
    
    if (hasbias)
       msebias(sind,tind)=msediff(sind).msebias(tind); 
    end
end
stats.mustim=mean(msestim,1);
stats.stdstim=std(msestim);

if (hasshist)
   stats.mushist=mean(mseshist,1);
   stats.stdshist=std(mseshist);
end

if (hasbias)
   stats.mubias=mean(msebias,1);
   stats.stdbias=mean(msebias,1);
end







%*********************************************************************
%Plot the mse of the stimulus coefficients
%*****************************************************************
setfocus(fmse.a,rind,1);


hp=plot(alltrials,stats.mustim);
%errorbar(alltrials,stats.mustim,stats.stdstim);
addplot(fmse.a(rind,1),'hp',hp);

%*********************************************************************
%Plot the mse of the spike history coefficients
%*****************************************************************
if ~isempty(msedata.mseshist)
    rind=rind+1;
    setfocus(fmse.a,rind,1);

    hp=plot(alltrials,stats.mushist);

    addplot(fmse.a(rind,1),'hp',hp);
end
% 
% %*********************************************************************
% %Plot the mse of the bias
% %*****************************************************************
if (hasbias);
    rind=rind+1;
    setfocus(fmse.a,rind,1);
    hp=plot(alltrials,stats.mubias);

    addplot(fmse.a(rind,1),'hp',hp);
end


%*********************************************************************
%add labels
%*********************************************************************
%%
rind=1;
title(fmse.a(rind,1),'Stimulus coefficients');
ylabel(fmse.a(rind,1),'$E_\theta||\theta- \hat{\theta}||^2$','Interpreter','latex');
set(fmse.a(rind,1),'xscale','log');

if (hasshist)
    rind=rind+1;
    title(fmse.a(rind,1),'Spike history coefficients');
    ylabel(fmse.a(rind,1),'$E_\theta||\theta - \hat{\theta}||^2$','Interpreter','latex');
    set(fmse.a(rind,1),'xscale','log');
end

if (hasbias)
    rind=rind+1;
    title(fmse.a(rind,1),'Bias');
    ylabel(fmse.a(rind,1),'$E_\theta||\theta- \hat{\theta}||^2$','Interpreter','latex');
    set(fmse.a(rind,1),'xscale','log');
end

xlabel(fmse.a(rind,1),'trial');

lblgraph(fmse);


%%
dinfo=cell(size(obj,1),6);

for sind=1:size(obj,1)
   dinfo{sind,1} =sprintf('%s\n%s',obj(sind,1).seqsims(1).neuron,obj(sind,1).label);   
   dinfo{sind,2} = sprintf('%s\n%s',obj(sind,2).seqsims(1).neuron,obj(sind,2).label);   
   dinfo{sind,3}= msediff(sind).sumstim;
   dinfo{sind,4}= msediff(sind).sumshist;
   dinfo{sind,5}= msediff(sind).sumbias;
   
   dinfo{sind,6}={obj(sind,1).label, getrpath(obj(sind,1).outfile);obj(sind,2).label,getrpath(obj(sind,2).outfile)};
end

%compute the mean
dinfo=[dinfo; {'mean', '',mean([msediff.sumstim]),mean([msediff.sumshist]),mean([msediff.sumbias]),''}];
dinfo=[dinfo; {'std', '',std([msediff.sumstim]),std([msediff.sumshist]),std([msediff.sumbias]),''}];

dinfo=[{'Design 1', 'Design 2','Sum Stim', 'Sum Shist', 'Sum Bias','files'}; dinfo];

dinfo{end+1,1}='explanation';
explain=sprintf('We compute the difference between the MSE of Design 1 and the MSE of Design 2.\n');
explain=sprintf('%s We then take the average across the different designs as a function of the trial and plot the result.\n',explain);
explain=sprintf('%s In the table above we compute the difference between the MSE for each design and then sum across the trials (i.e we integrate the area between the curves).\n',explain);
explain=sprintf('%s We compute the mean and standard deviation across the designs.',explain);
dinfo{end,2}=explain;
%make a table of the statistics
otbl={fmse,[{{'class',mfilename('class');'function',mfilename('full')}};{dinfo}]};
onenotetable(otbl,seqfname('~/svn_trunk/notes/avgexpmse.xml'));
