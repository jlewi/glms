%function allpost=bsfitglm(datafile,statusfile)
%   datafile - file contains 2 objects
%       allpost - a GaussPostFile object contianing the posteriors
%       bdata   - a BSData object used to get the actual data
%
%   statusfile - file where we will output status messages to
%   opt        - options
%           .cinterval - how often to save covariance matrix
%           .ntrials   - how many trials to run
% Explanation: Fits a GLM to the data using our online update but only uses
%   the trials where a spike occured. So we ignore all silences. This is
%   for debug purposes only
function allpost=bsfitglmtospikesonly(datafile,statusfile,opt)


if ~exist('opt','var')
   opt=[]; 
end

if ~isfield(opt,'cinterval')
   opt.cinterval=10000; 
end
%**************************************************************************
%load the allpost and bdata
%*************************************************************************
data=load(getpath(datafile));
allpost=data.allpost;
bdata=data.bdata;
clear data;


    [bdata,sr]=gettrial(bdata,1);
dim=prod(getmatdim(getinput(sr)));
%**************************************************************************
%the model
%*******************************************************************
mobj=MLogistic('dim',dim,'mmag',1);
updater=Newton1d('compeig',false);


%*****************************************************************
%we start with the 1st trial after the last one in allposst
starttrial=getlasttrial(allpost)+1;

post=GaussPost('m',getm(allpost,getlasttrial(allpost)),'c',getc(allpost,getlasttrial(allpost)));

%open the status 
fid=fopen(getpath(statusfile),'a');

%get the number of trials
[bdata,ntrials]=getntrials(bdata);

%%
if ~isfield(opt,'ntrials')
lasttrial=ntrials;
else
   lasttrial=starttrial+opt.ntrials; 
end

for trial=starttrial:lasttrial    
    if (mod(trial,10)==0)
        fprintf(fid,'Trial = %d \n', trial);
    end
    
    %get an sr object for this trial
    [bdata,sr]=gettrial(bdata,trial);

    %only include this trial if a spike occured
    if (getobsrv(sr)==1)
    inp=getData(getinput(sr));
    inp=projinp(mobj,inp,[]);
    post=update(updater,post,inp,mobj,getobsrv(sr));

    %determine whether we save the covariance matrix on this trial or not
    if (mod(trial,opt.cinterval)==0 || trial==lasttrial)
        allpost=addpost(allpost,post,trial);
    else
        allpost=addpost(allpost,lowmem(post),trial);
    end
    end
end
fclose(fid);

%**************************************************************************
%save allpost and bdata
%*************************************************************************
save(getpath(datafile),'allpost','bdata','-append');


