%function testbslogpost(strfdim,alength,windexes)
%   alength - how many spike history terms to use
%   windexes - which wave files to use to compute the log likelihood
%
%Explanation: A test harness for BSlogpost. This function verifies that
%   BSlogpost/compllike correctly computes glmproj and the log likelihood
%   and its derivatives. BSlogpost uses filter operations to efficiently
%   compute glmproj for all trials into which we divide a wave file.
%   To verify that this code is correct we compare the results to computing
%   these quantities using gettrial.
%
%Usage: the script runtestbslogpost runs a number of different tests
%(combinations of alength), strfdim, mobj, and windexes.
function testbslogpost(freqsubsample,obsrvwindowxfs,stimnobsrvwind,alength,windexes)

if isempty(alength)
    %lenght of spike history
    alength=5;
end



%what tests to run
tests.compglmproj=false;
tests.d2glm=true;
%*******************************************************************
%Create a bsdata object
%*********************************************************************

dfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1_corrected.mat'));



%how long to make the stimulus in time on each trial
%subtract 1 because the way bdata works we end up adding one
%stimdursamps=stimdursamps-1;
%subsample the frequencies of the spectrogram to reduce the dimensionality
%of the stimulus
bdata=BSData('fname',dfile,'stimnobsrvwind',stimnobsrvwind,'freqsubsample',freqsubsample,'obsrvwindowxfs',obsrvwindowxfs);

if isempty(windexes)
    windexes=1:getnwavefiles(bdata);
end
uobj=BSlogpost('bdata',bdata,'windexes',windexes);

%********************************************************************
%Create a logistic model no spike history
%********************************************************************
%logistic model
% stimdim=getstrfdim(bdata);
% klength=stimdim(1);
% ktlength=stimdim(2);
% mobj=MLogistic('alength',alength,'klength',klength,'mmag',1,'ktlength',ktlength);
%
%poisson model
stimdim=getstrfdim(bdata);
klength=stimdim(1);
ktlength=stimdim(2);
glm=GLMPoisson('canon');
mobj=MParamObj('alength',alength,'klength',klength,'mmag',1,'ktlength',ktlength,'glm',glm,'hasbias',true);

%**************************************************************************
%test: compglmproj
if (tests.compglmproj)
    iscorrect=testcompglmproj(uobj,bdata,mobj);


    if (iscorrect)
        fprintf('BSBatchFitGLM/compglmproj successfull \n');
    else
        error('compglmproj not correct');
    end

end

if (tests.d2glm)
    iscorrect=testcompd2glm(uobj,bdata,mobj);


    if (iscorrect)
        fprintf('BSBatchFitGLM/compgd2glmp was successfull \n');
    else
        error('compd2loglike not correct');
    end

end
%*********************************************************************
%test computation of the derivatives
%**********************************************************************
function iscorrect=testcompd2glm(uobj,bdata,mobj)

iscorrect=true;

windexes=getwindexes(uobj);

%for the poisson model we need to make the magnitude small enough that the
%derivatives don't blow up
theta=floor(randn(getparamlen(mobj),1)*10^-2);

%compute glmproj for all presentations of this stimulus

[batchfit.dglm, batchfit.d2glm, bfitdebug]=compd2loglike(uobj,mobj,theta);
%[batchfit.dglm, batchfit.d2glm]=compd2loglike(uobj,mobj,theta);

strfdim=getstrfdim(bdata);
%now manually check we get the same result if we use gettrial
%get the trials corresponding to this windex

direct.dglm=zeros(getparamlen(mobj),1);
direct.d2glm=zeros(getparamlen(mobj),getparamlen(mobj));

shist=[];


for w=1:length(windexes)

    windex=windexes(w);
    [bdata,trials]=gettrialindforwave(bdata,windex);


    direct.alldglmeps{w}=zeros(size(trials,1),trials(1,2)-trials(1,1)+1);
    direct.alld2glmproj{w}=zeros(size(trials,1),trials(1,2)-trials(1,1)+1);;
    for r=1:size(trials,1)
        %warning('need to set r=1:size(trials,1)');
        %for r=1
        for trial=trials(r,1):trials(r,2)
            if (mod(trial-trials(r,1),1000)==0)
                fprintf('Trial repeat %d of %d: %0.4g of %0.4g \n',r,size(trials,1),trial-trials(r,1),trials(r,2)-trials(r,1));
            end



            [bdata,sr,shist]=gettrial(bdata,trial,getshistlen(mobj));

            glmproj=compglmproj(mobj,getinput(sr),theta,shist);

            [dglmeps, d2glmproj]=d2glmeps(getglm(mobj),glmproj,getobsrv(sr));

            direct.allglmproj{w}(r,trial-trials(r,1)+1)=glmproj;
            direct.alldglmeps{w}(r,trial-trials(r,1)+1)=dglmeps;
            direct.alld2glmproj{w}(r,trial-trials(r,1)+1)=d2glmproj;

            inpvec=projinp(mobj,getData(getinput(sr)),shist);
            direct.dglm=direct.dglm+dglmeps*inpvec;

            d2glm=d2glmproj*inpvec*inpvec';
            direct.d2glm=direct.d2glm+d2glm;
            if (any(isnan(d2glm(:))) || any(isinf(d2glm(:))))
                warning('derivative includes nan or inf terms');
            end
        end
    end
end
%check the stimulus and observations match
%NOte there will be some numerical error because the order we  perform the
%operations matters
threshold=10^-8;
if (any(abs(direct.dglm-batchfit.dglm)>10^-8))
    fprintf('Error: 1st derivative computed directly and using BatchFitGLM did not return the same value.\n');
    %check which terms it is
    if any(abs(direct.dglm(1:getklength(mobj))-batchfit.dglm(1:getklength(mobj)))>threshold)
        fprintf('ERROR: 1st derivative: terms corresponding to stimulus are not correct \n');
    else
        fprintf('1st derivative: terms corresponding to stimulus are accurate\n');
    end

    nstimcoeff=getklength(mobj)*getktlength(mobj);
    dshist=direct.dglm(nstimcoeff+1:nstimcoeff+getshistlen(mobj))-batchfit.dglm(nstimcoeff+1:nstimcoeff+getshistlen(mobj));
    dshist=abs(dshist);
    if (any(dshist>threshold))
        fprintf('ERROR: 1st derivative: terms corresponding to spike history are not correct \n');
    else
        fprintf('1st derivative: terms corresponding to spike history are accurate\n');
    end
    if any(abs(direct.dglm(end)-batchfit.dglm(end))./abs(batchfit.dglm)>threshold)
        fprintf('ERROR: 1st derivative: terms corresponding to bias not correct \n');
    else
        fprintf('1st derivative: terms corresponding to bias are accurate\n');
    end

    %***********************************************************
    %check the actual variables
    for wind=1:length(windexes)
        d=abs(direct.allglmproj{wind}-bfitdebug.glmproj{wind});
        if any(d(:)>threshold)
            fprintf('Error: glmproj computation is not the same for both methods \n');
        end

        dd1=abs(direct.alldglmeps{wind}-bfitdebug.dglmproj{wind});
        if (any(dd1(:)>threshold))
            fprintf('Error: dglmproj computation is not the same for both methods \n');
        end

        dd2=abs(direct.alld2glmproj{wind}-bfitdebug.d2glmproj{wind});
        if (any(dd2(:)>threshold))
            fprintf('Error: d2glmproj computation is not the same for both methods \n');
        end

    end
    iscorrect=false;
    error('Error: 1st derivative not correct');
end

%check second derivative
%since the seond derivative can blow up we want to compute
%the relative error.
dd2=abs(direct.d2glm-batchfit.d2glm);
%normfac=min(abs([direct.d2glm(:) batchfit.d2glm(:)],[],2);
dd2=dd2(:)./abs(batchfit.d2glm(:));

if (any(dd2(:)>threshold))
    fprintf('Error: 2nd derivative computed directly and using BatchFitGLM did not return the same value. \n');
    for wind=1:length(windexes)
        d2ndderv=abs(bfitdebug.d2glmproj{wind}-direct.alld2glmproj{wind});

        if (any(abs(d2ndderv(:))>threshold))
            fprintf('Error: d2glmproj for both methods does not match. \n');
        else
            fprintf('d2glmproj is correct. \n');
        end
    end

    strfdim=getstrfdim(bdata);

    dxx=(direct.d2glm(1:prod(strfdim),1:prod(strfdim))-batchfit.d2glm(1:prod(strfdim),1:prod(strfdim)))./direct.d2glm(1:prod(strfdim),1:prod(strfdim));
    dxx=abs(dxx);
    if (any(dxx(:)>threshold))
        fprintf('Error in Hessian: stimulus terms x_t x_t are  wrong \n');
    else
        fprintf('Hessian: stimulus terms x_t x_t are correct \n');
    end

    col=prod(strfdim)+getshistlen(mobj)+1;
    dx=(direct.d2glm(1:prod(strfdim),col)-batchfit.d2glm(1:prod(strfdim),col))./direct.d2glm(1:prod(strfdim),col);
    dx=abs(dx);
    iscorrect=false;
    if (any(dx(:)>threshold))
        fprintf('Error in Hessian: stimulus-bias terms x_t  are  wrong \n');
    else
        fprintf('Hessian: stimulus-bias terms x_t  are correct \n');
    end

    rowstart=prod(strfdim)+1;
    rowend=rowstart+getshistlen(mobj)-1;
    colstart=rowstart;
    colend=rowend;
    drr=(direct.d2glm(rowstart:rowend,colstart:colend)-batchfit.d2glm(rowstart:rowend,colstart:colend))./direct.d2glm(rowstart:rowend,colstart:colend);
    drr=abs(drr);

    if (any(drr(:)>threshold))
        fprintf('Error in Hessian: spike history- terms r_t r_t  are  wrong \n');
    else
        fprintf('Hessian: spike history-terms r_t r_t  are correct \n');
    end


    %*************************************************
    %checks is a structure specifying the different substructures
    %of the matrix in which to look for errors
    cind=0;

    %check the cross terms x_t r_t
    cind=cind+1;
    checks(cind).rowstart=1;
    checks(cind).rowend=prod(strfdim);
    checks(cind).colstart=prod(strfdim)+1;
    checks(cind).colend=prod(strfdim)+getshistlen(mobj);
    checks(cind).tname='cross terms x_t r_t';


    %check the spike history bias terms r_t
    cind=cind+1;
    checks(cind).rowstart=prod(strfdim)+1;
    checks(cind).rowend=prod(strfdim)+getshistlen(mobj);
    checks(cind).colstart=prod(strfdim)+getshistlen(mobj)+1;
    checks(cind).colend=prod(strfdim)+getshistlen(mobj);
    checks(cind).tname='spike history -bias terms  r_t';

    %bias terms
    cind=cind+1;
    checks(cind).rowstart=getparamlen(mobj);
    checks(cind).rowend=getparamlen(mobj);
    checks(cind).colstart=getparamlen(mobj);
    checks(cind).colend=getparamlen(mobj);
    checks(cind).tname='bias term';

    for cind=1:length(checks)

        dsub=direct.d2glm(checks(cind).rowstart:checks(cind).rowend,checks(cind).colstart:checks(cind).colend);
        bsub=batchfit.d2glm(checks(cind).rowstart:checks(cind).rowend,checks(cind).colstart:checks(cind).colend);

        rerr=abs((dsub-bsub)./dsub);


        if (any(rerr(:)>threshold))
            fprintf('Error in Hessian: %s  are  wrong \n',checks(cind).tname);
        else
            fprintf('Hessian: %s  are correct \n',checks(cind).tname);
        end

    end

    iscorrect=false;
    error('Error: 2nd derivative not correct');
end

%*********************************************************************
%test computation of glmproj
%**********************************************************************
function iscorrect=testcompglmproj(uobj,bdata,mobj)

iscorrect=true;

%index of the wave file to test
windex=ceil(rand(1)*getnwavefiles(bdata));

theta=floor(randn(getparamlen(mobj),1)*10);

%compute glmproj for all presentations of this stimulus
batchfit.glmproj=compglmproj(uobj,windex,mobj,theta);


strfdim=getstrfdim(bdata);
%now manually check we get the same result if we use gettrial
%get the trials corresponding to this windex
[bdata,trials]=gettrialindforwave(bdata,windex);

direct.glmproj=zeros(size(trials,1),trials(1,2)-trials(1,1)+1);

%we need to get allobsrv so we can properly compute the spike history for
%the first elements
[bdata,fullspec,allobsrv]=getallwaverepeats(bdata,windex);
for r=1:size(trials,1)
    fprintf('Testing %0.3g presentation of the %g wave file \n', r,windex);


    shist=[];

    %if the length of the spike history is > strfdim(2)-1 then
    %the spike history isn't fully known on the first trial so we pad
    %with zeros
    %     if (getshistlen(mobj)>(strfdim(2)-1))
    %         shist(end-(strfdim(:,2)-1)+1:end)=allobsrv(r,1:strfdim(2)-1);
    %     else
    %         %length of spike history is less than or equal to strfdim(2)-1
    %         shist=allobsrv(r,(strfdim(2)-1)-getshistlen(mobj)+1);
    %     end

    tr=1;
    for trial=trials(r,1):trials(r,2)
        [bdata,sr,shist]=gettrial(bdata,trial,getshistlen(mobj));

        direct.glmproj(r,tr)=compglmproj(mobj,getinput(sr),theta,shist);
        tr=tr+1;
        %update shist
        shist=[shist(2:end);getobsrv(sr)];
    end



end

%check the stimulus and observations match
d=abs(direct.glmproj-batchfit.glmproj);

if (any(d(:)>10^-8))
    fprintf('Error: glmproj computed directly and using BatchFitGLM compglmproj did not return the same value.');
    iscorrect=false;
    error('Error: compglmproj not correct');
end