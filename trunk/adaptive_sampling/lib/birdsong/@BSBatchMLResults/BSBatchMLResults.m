%function sim=ClassName(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: This is a class I created to store the results of using
% batch ML to find the map of the posterior.
%
%Revisions:
%   11-08-2008: I make all fields settable as a quick and dirty way to
%   create this class.
classdef (ConstructOnLoad=true) BSBatchMLResults < handle 
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        version=081108;
    end

    properties(SetAccess=public, GetAccess=public)

        %w structure identifying the whitening results
        wmat=[];
        
        %estimate of theta
        theta=[];
        exitflag=[];
        fsout=[];
        lpost=[];
        gradient=[];
        thetainit=[];
        updater=[];
        runtime=[];
        svnrevision=[];
        totaliter=[];
        hessian=[];
        iterdata=[];
        y=[];
                
    end
    methods
        function obj=BSBatchMLResults(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con=[];

               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    %do nothing
                case {Constructor.nomatch}
                    %do nothing
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                  
                case {Constructor.nomatch}
                    
                    %process any fields passed in
                    fnames=fieldnames(params);
                    
                    for f=fnames
                       obj.(f{1})=params(f{1}); 
                    end
                otherwise
                 error('unexpected value for cind');
            end


        end
    end
end


