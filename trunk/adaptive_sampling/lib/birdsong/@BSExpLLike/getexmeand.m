%function exmean=getexmeand(obj)
%
%Return value:
%   An array of the expected mean value
%   trials - the trials associated with the values
%
%   We only return those trials for which the mean is a double on all wave
%   files
%
function [exmean,trials]=getexmeand(obj)

nwave=size(obj.exllike,1);

isd=isdouble(obj);

isd=sum(isd,1);
isd=(isd==nwave);

trials=obj.trials(isd);


exmean=nan(nwave,length(trials));
for w=1:nwave
    stats=[obj.exllike(w,isd).stats];    
    exmean(w,:)=[stats.mean];
end
