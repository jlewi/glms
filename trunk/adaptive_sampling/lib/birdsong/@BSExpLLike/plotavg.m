%function plot(dsets,pstyles)
%   dsets - A structure array of the datasets to process
%   pstyles - optional a PlotStyles object defining the styles to use
%
%Explanation: Make a plot of the expected log-likelihood averaged across
%wavefiles in the test set
function [fexp,otbl]=plotavg(bsobj,pstyles)

error('03-02-2009 function is still obsolete.');

if ~exist('pstyles','var')
    pstyles=[];
end
if isempty(pstyles)
    pstyles=PlotStyles();
    pstyles.linewidth=pstyles.linewidth*2;
    pstyles.markersize=4;
end


%**************************************************************
%Make the plots
%************************************************************
%%
%wavefiles in the test set
wavinfo=bsobj(1).wavinfo;

[wind,index]=unique([wavinfo.wind],'first');
wavinfo=wavinfo(index);

nplots=1;
clear fexp;

fexp=FigObj('name','Expected log likelihood','width',6,'height',6,'xlabel','trial','ylabel', 'E_{\theta}log p(r|s_t,\theta_t)');
 %loop over the simulations
    pind=0;
for dind=1:length(bsobj)

    %loop over the wave files and compute the average of the expected
    %log-likelihood over wavefiles


    %unique trials
    trials=unique([alldata(dind).results.trial]);
    counts=zeros(1,length(trials));
    meanll=zeros(1,length(trials));

    %array which maps the trial number to the appropriate index in
    %counts,mean and trials
    totrialind=zeros(1,max(trials));
    totrialind(trials+1)=1:length(trials);

    for wind=1:length(wavinfo)
        %find the data points for this wavefile
        rind=find([alldata(dind).results.wavefile]==wavinfo(wind).wind);

        results=alldata(dind).results;
        stats=[results(rind).stats];

        t=[results(rind).trial]+1;
        meanll(totrialind(t))= meanll(totrialind(t))+[stats.mean];
        counts(totrialind(t))= counts(totrialind(t))+1;

    end
    meanll=meanll./counts;

    %plot the mse for al the files in the test set

   
    pind=pind+1;
    setfocus(fexp.a);


    %throw out any points for the log likelihood is close to zero
    %that we needed to use a multi precision object to store it
    keep=zeros(1,length(trials));


    for j=1:length(trials)
        if isa(meanll(j),'double')
            keep(j)=1;
        end
    end


    trials=trials(logical(keep));
    meanll=meanll(logical(keep));
    

    %throw out any trials for which mean <-10^8
    %otherwise the plot will show an asymptote that will make the plot
    %look bad
    keep=zeros(1,length(trials));
    ind=find(meanll>-10^8);
    keep(ind)=1;


    keep=logical(keep);


    hp=plot(trials(logical(keep)),meanll(logical(keep)));

    %             [c m]=getptype(pind,2);
    %             pstyle.marker='o';
    %             pstyle.markerfacecolor=c;
    %             pstyle.linestyle=m;
    %             pstyle.color=c;
    %             pstyle.markersize=4;
    %             pstyle.linewidth=5;
    ps=pstyles.plotstyle(pind);

    addplot(fexp.a,'hp',hp,'lbl',dsets(dind).lbl,'pstyle',ps);
end


set(gca,'xscale','log');
lblgraph(fexp);
set(fexp.a,'ylim',[-1 0]);


%%
otbl={};

%create the table of output information
%info about the fields
scriptname=mfilename('fullpath');
explain={'wave file', 'A number identifying the wave file';'script',scriptname};
describe=sprintf('This plot compares the expected log-likelihood of the trials in the test as a function of the posterior\n');
describe=sprintf('%sWe don''t plot the log likelihood for any trial where the log likleihood was so negative that we needed a multi-precision object \n',describe);
describe=sprintf('%sOnly 1 repeat for each file in the test set is used\n',describe);
explain=[explain;{'explanation',describe}];
for sind=1:length(dsets)
    if isfield(dsets(sind),'explain')
        explain=[explain; {dsets(sind).lbl, {'setup',dsets(sind).setupfile;'mse data', getpath(fnames(sind));'explain',dsets(sind).explain}}];
    else
        explain=[explain; {dsets(sind).lbl, {'setup',dsets(sind).setupfile;'mse data', getpath(fnames(sind))}}];
    end
end

% for wind=1:length(wavinfo)
%     oinfo={'wave file', wavinfo(wind).wind; 'isbirdsong', wavinfo(wind).issong;};
%     odata={fexp(wind) oinfo};
%
%     if (wind==1)
%         odata={fexp(wind) {oinfo;explain}};
%     end
%     otbl=[otbl;odata];
% end
%
% onenotetable(otbl,seqfname('~/svn_trunk/notes/bscompareexpllike.xml'));

return;