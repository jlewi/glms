%function [data]=compellike(bsexllike, trials)
%   bsexllike     - BSExpLLike object
%
%
%Return value
%   results - structue array
%       .trial - trial of the posterior used to compute the expected llike
%
%       .wind  - the index into wavinfo of the wave file for which this the
%                 expected log likelihood
%       .simind - index into dsets of the simulation for which this is the
%                 mse
%       .stats - statistics (mean and variance) across trials in this wave
%               file of the expected log likelikhood
%Explanation:
% This script looks at a bunch of simulations in which we did not train
% on at least some of the wavefiles. We use those wavefiles as a test set
% and compute the expected log likelihood.
%
%
%Revisions:
%   02-25-2009 - use the tangent space if specified
function compellike(bsexllike)
bssim=[];


mobj=bsexllike.bssimobj.mobj;

%set trials to all trials for which we have the covariance matrix



trials=bsexllike.trials;


%check for new trials
alltrials=readmatids(bsexllike.bssimobj.allpost.cinfo.caccess);


%select those which haven't been deleted
alltrials=alltrials(alltrials~=bsexllike.bssimobj.allpost.cinfo.caccess.DELID);
alltrials=sort(alltrials);

if (length(alltrials)>length(trials))
    nnew=length(alltrials)-length(trials);
    bsexllike.trials=[bsexllike.trials nan(1,nnew)];
end

tstart=size(bsexllike.exllike,2)+1;


%get the wavefiles in the test set if we haven't already processed them
testwind=rowvector([bsexllike.wavinfo.wind]);




%%
%**************************************************************************
%Process the trials
%**************************************************************************




%**********************************************************************
%If we are applying smoothing after the experiment
%to a model object which is not of type ModBSSinewaves
%then we need to form a basis which we use to project the posterior
%onto the tangent space
%we only want to do this once
basis=[];
msmooth=[];

%     if (isfield(dset,'smooth') && ~isempty(dset.smooth))
%
%         if ~isa(mobj,'ModBSSineWaves')
%
%             %project the covariance in the direction of our basis
%             %vectors
%             %our basis vectors are just for the strf so we need to
%             %add vectors for the spike history and bias
%
%             %we need to create a smoothing object
%             nfreq=dset.smooth.nfcutoff-1;
%             ntime=dset.smooth.ntcutoff-1;
%             msmooth=ModBSSinewaves('glm',mobj.glm,'nfreq',nfreq,'ntime',ntime,'klength',mobj.klength,'ktlength',mobj.ktlength,'alength',mobj.alength,'hasbias',mobj.hasbias,'mmag',mobj.mmag);
%             basis=zeros(getparamlen(mobj),getparamlen(msmooth));
%
%             startrow=mobj.indstim(1);
%             endrow=mobj.indstim(2);
%             startcol=msmooth.indstim(1);
%             endcol=msmooth.indstim(2);
%
%             basis(startrow:endrow,startcol:endcol)=msmooth.basis;
%             if (mobj.alength>0)
%                 basis(mobj.indshist(1):mobj.indshist(2),msmooth.indshist(1):msmooth.indshist(2))=eye(mobj.alength);
%
%             end
%             if (mobj.hasbias)
%                 basis(mobj.indbias,msmooth.indbias)=1;
%             end
%         end
%     end
%
%     %*************************************************************
%     %Construct the tan space object if we need it
%     %*********************************************************
    if (bsexllike.taninfo.use)
                if (isa(bsexllike.bssimobj.mobj,'MTanSpace') || isa(bsexllike.bssimobj.mobj,'MTanSpaceInt'))

                    mtan=bsexllike.bssimobj.mobj;
                else
                mtan=bsexllike.taninfo.mtan;

                end
    end
%loop over all the trials we do the cross validation for


for tind=tstart:length(alltrials)
    ntest=length(testwind);
    if (mod(tind,100)==0 || tind==tstart)
        fprintf('trial:%d of %d \n', tind, length(alltrials));
    end

    %make sure the covariance matrix was saved
    post=getpost(bsexllike.bssimobj.allpost,alltrials(tind));


    if isempty(getc(post))
        fprintf('Skipping trial %d for simulation %d label=%s. Covariance matrix was not saved \n',trial,sind,bssim.label);
    else

        %reinitialize mobj to the original mobj
        %b\c we may adjust it when we do smoothing
                  %  mobj=bssim.mobj;
        
             if (bsexllike.taninfo.use)
                        pb=PostTanSpace(post,mtan);
        
        
                        minfo=pb.fullpost.m+pb.tanpoint.basis*pb.tanpost.m;
                        cinfo=pb.tanpoint.basis*pb.tanpost.c*pb.tanpoint.basis';
                        %compute eigendecomp of cinfo because we will need it for the stimulus
                        %optimization.
                        post=GaussPost('m',minfo,'c',cinfo);
               
        
                    end
        %             %************************************************************
        %             %apply smoothing to the MAP
        %             %******************************************************
        %             if (isfield(dset,'smooth') && ~isempty(dset.smooth))
        %
        %                 m=getm(post);
        %                 c=getc(post);
        %
        %                 if isa(mobj,'ModBSSineWaves')
        %
        %
        %                   m=smooth(mobj,theta,nfcutoff,ntcutoff);
        %                     post=GaussPost(m,c);
        %
        %                 else
        %
        %                     %project the mean and variance onto our basis
        %                     m=basis'*m;
        %
        %                     c=basis'*c*basis;
        %                     post=GaussPost(m,c);
        %
        %                     %set mobj to msmooth because we need to use this
        %                     %mobject when we compute the likelihood
        %                     mobj=msmooth;
        %
        %                 end
        %             end
        bsexllike.trials(tind)=alltrials(tind);

        ntest=0;
        for wind=testwind
            ntest=ntest+1;


            [bsexllike.exllike(ntest,tind).stats]=compellikeforwavefile(bsexllike,post,wind,mobj);

            bsexllike.exllike(ntest,tind).isdouble=isa(bsexllike.exllike(ntest,tind).stats.mean,'double');

        end
    end

    if (mod(tind,100)==0)
        fprintf('Saving the data. \n');
        if exist(getpath(bsexllike.outfile),'file')
            save(getpath(bsexllike.outfile),'bsexllike','-append');
        else
            save(getpath(bsexllike.outfile),'bsexllike');
        end
    end
end


fprintf('Saving the data. \n');

if exist(getpath(bsexllike.outfile),'file')
    save(getpath(bsexllike.outfile),'bsexllike','-append');
else
    save(getpath(bsexllike.outfile),'bsexllike');
end
