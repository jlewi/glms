%function [stats,ellike]=compmse(bdata,post,windex,mobj)
%   post - distribution on the parameters of theta
%   wfile - index of the wave file
%   mobj - This is the model to use to compute the expected log likelihood
%        - because of post processing this may not be the same as the model
%        used during the simulation
%Return value
%   stats- Strucutre
%         .mean - mean of the expected log-likelihood across all trials in
%                this wave file
%               - For each trial we compute the expected value of the
%               log-lieklihood w.r.t to the posterior on theta
%               - we then average this across trials
%         .var  - variance across all trials in this wavefile
%         .meantvar - For each trial we compute the variance of the
%               log-likelihood w.r.t to the posterior on theta
%               we then average this across all trials
%
%   ellike - the expected log-likelihood of each trial in this wave file set
%          - we throw out any trials for which the stimulus or spike
%          history isn't full known
%
%Explanation:
% 
%Revisions:
%   01-08-2009 - There was an error in my computation of the sample
%   variance. I wasn't squaring the mean.
%
function [stats,ellike]=compellikeforwavefile(obj,post,windex,mobj)

bdata=obj.bssimobj.stimobj.bdata;
%how many repeats of the test file to use
maxrepeats=1;
bspost=BSlogpost('bdata',bdata,'maxrepeats',maxrepeats);

if (~isa(mobj.glm,'GLMPoisson') || ~iscanon(mobj.glm))
    error('this function only works with the canonical poisson model. For other models we will need to numerically integrate the expectation of the log likelihood');
end






repeat=1;
%fprintf('Only computing the expected log likelihood for the first repeat of the wave file. \n');


[ntrials,cstart]=getntrialsinwave(bdata,windex);
[stim,shist,obsrv]=gettrialwfilerepeat(bdata,windex,repeat*ones(1,ntrials),[1:ntrials],mobj);

%*************************************************************************
%for canonical poisson
%*************************************************************************
%1. COmpute expected value of s_t' \theta

%get the full input on each trial and the associated responses.
[muproj]=compglmproj(bspost,windex,mobj,getm(post));

%next we compute sigma
%the following is a bit of a hack 
%02-24-2009: I changed the input for projinp so we can no longer use
%projinp
if isa(mobj,'MBSFTSep')
    inp=mobj.basis'*stim;
    
    bias=[];
if (hasbias(mobj))
    bias=1;
end
    inp=[inp;zeros(getshistlen(mobj),ntrials);bias*ones(1,ntrials)];
else
    inp=projinp(mobj,stim,zeros(getshistlen(mobj),ntrials));
end

sigma=sum(inp.*rotatebyc(post,inp));

%value for the exponent 
vexp=muproj+1/2*sigma;

if any(vexp>floor(log(realmax)))
    %use the multi precision toolbox
    vexp=mp(vexp,192);
end

%if vexp exceeds the maximum precision then 
ellike=obsrv.*muproj-exp(vexp);

%***************************************************************
%compute the variance of the expected log-likelihood w.r.t to our 
%posterior on theta
%Warning: 01-08-2009. I have some doubts about this formula
%******************************************************

ellvar=obsrv.^2.*sigma;
ellvar=ellvar-2.*obsrv.*exp(vexp).*(sigma+muproj);
ellvar=ellvar+2.*obsrv.*muproj.*exp(vexp);
v2=2*muproj+2*sigma;
if any(v2>floor(log(realmax)))
    %use the multi precision toolbox
    v2=mp(v2,192);
end
ellvar=ellvar+ exp(v2)-exp(2*vexp);

stats.mean=sum(ellike,2)/ntrials;

stats.meantvar=sum(ellvar,2)/ntrials;

s2=sum(ellike.^2,2);
stats.var=s2/(ntrials-1)-ntrials/(ntrials-1)*stats.mean^2;




