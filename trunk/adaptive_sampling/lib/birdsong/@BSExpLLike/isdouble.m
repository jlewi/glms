%function getisdouble(obj)
%
%return value
%   isd - nwavefilesxntrials matrix stores a logical value
%       indicating whether the mean of the log likelihood for that data
%       point is a dobule
%
function isd=isdouble(obj)
isd=zeros(1,length(obj.trials));


 for w=1:size(obj.exllike,1)
            isd(w,:)=[obj.exllike(w,:).isdouble];
            
 end