%function sprior=smoothingprior(prior,penalty)
%   prior - Gauss Post object
%   penalty - penalty object for enforcing smoothing
%
%Return Value:
%   sprior - The Gaussian prior formed by combining prior and penalty
%
%   We compute sprior as follows. We add the log prior to the log penalty
%   We find the peak of this value. We then find the hessian evaluated at
%   the peak.
%
%   We set the covariance to the negative inverse hessian
function sprior=smoothingprior(prior,penalty,mobj)

mo=getm(prior);
co=getc(prior);

%evaluate the penalty at mo
%we do this just to force the penalty to compute the hessian
tmat=reshape(mo(1:getktlength(mobj)*getklength(mobj)),getklength(mobj),getktlength(mobj));

p=comppenalty(penalty,tmat);

%we compute the mean 
dstrf=getktlength(mobj)*getklength(mobj);
dnonstrf=getparamlen(mobj)-dstrf;
%the hessian of the penalty is negative semi-definite (negative
%eigenvalues) because it includes the negative sign
%where in our Guassian expression the negative is outside it
%i.e Co= - inv(hessian)
%
Am=[-penalty.hessian zeros(dstrf, dnonstrf);zeros(dnonstrf, dstrf) eye(dnonstrf,dnonstrf)];
mnew=(inv(co)+Am)*inv(co)*mo;

cnew=inv(inv(co)+Am);

sprior=GaussPost('m',mnew,'c',cnew);




