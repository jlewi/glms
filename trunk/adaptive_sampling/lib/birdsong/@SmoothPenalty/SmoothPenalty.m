%function sim=SmoothPenalty('errscale','size','sigma')
%       errscale - how much to scale the energy of theta - the smoothed
%                  version to generate the penalty term
%       size     - the size of the Guassian Filter
%       sigma    - the variance for the smoothing filter.
%
% Explanation: This class computes a penalty term for fitting the log
% posterior based on how smooth theta is as a matrix.
%   To compute the penalty we apply Gaussian smoothing to
%   the filter. We then subtract this lowpassed version from the actual
%   theta. The penalty is then proportional to the energy of
%   the differential component
%
%Revisions
%   06-30-2008
%       Make the object be derived from type handle
%       This way we can compute the hessian and other values once and only
%       once and store them. This avoids having to specify thetadim
%       in the constructor.
%
%       specifying thetadim in the constructor is annoying
%   06-20-2008 - Only compute the Hessian once and then store it for
%   repeated use.
%       - dimensions of theta must be specified in the constructor so that
%       we can compute the hessian

classdef (ConstructOnLoad=true) SmoothPenalty < handle



    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    % filtcoeff - the filter coefficients of this Gaussian smoother
    %declare the structure
    %
    % debug     - if true does some extra calculations to verify computations
    %              are correct.
    %             Note: This does not ensure computations are actually correct
    %                  This is also very inefficient
    %
    % Hessian   - the hessian of the penalty with respect to theta
    %
    %thetadim   - dimensions of theta
    %            - we need this so that we can compute the Hessian

    properties(SetAccess=private,GetAccess=public)
        version=080620;
        errscale=[];
        size=[];
        sigma=[];
        filtcoeff=[];
        debug=false;
        hessian=[];
        thetadim=[];
    end

    methods
        function obj=SmoothPenalty(varargin)
            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'errscale','size','sigma'};
            con(1).cfun=1;
            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
                    return;
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    if (params.errscale<0)
                        error('errscale should be greater than 0');
                    end

                    obj.errscale=params.errscale;

                    %filter dimensions should be odd so that the center element
                    %is well defined
                    obj.size=params.size;
                    if (any(mod(obj.size,2)==0))
                        error('filter dimensions must be odd so that center element is well defined');
                    end

                    obj.sigma=params.sigma;


                    obj.filtcoeff=zeros(obj.size);
                    x=[1:obj.size(2)]-(1+obj.size(2))/2;
                    y=[1:obj.size(1)]-(1+obj.size(1))/2;

                    [x,y]=meshgrid(x,y);

                    x=x(:);
                    y=y(:);
                    pts=[x';y'];
                    filtcoeff=exp(-1/2*qprod(pts,inv(obj.sigma)));

                    %normalize the filter coefficients
                    filtcoeff=1/sum(filtcoeff)*filtcoeff;

                    %reshape into a  matrix
                    obj.filtcoeff=reshape(filtcoeff,obj.size);


                otherwise
                    error('Constructor not implemented');
            end


            if isfield(params,'debug')
                obj.debug=params.debug;
            end
            if isfield(params,'thetadim')
                obj.thetadim=params.thetadim;
            end
        end

        %******************************************************************
        %compute the gradient filter
        %****************************************************************


        %**********************************************************
        %get method for the hessian
        function hessian=get.hessian(obj)
            %if we haven't computed the hessian yet we compute it
            %to compute the hessian we need to have specified thetadim
            if isempty(obj.hessian)
                %represent the convolution with the smoothing filter as a matrix
                qmat=compquadterm(obj);
                obj.hessian=-(obj.errscale)*(2*eye(size(qmat))-4*qmat+2*qmat'*qmat);

            end
            hessian=obj.hessian;
        end

    end
end

