%function [p,dp,d2p]=dpenaltydtheta(obj,tmat)
%   tmat - theta expressed as a matrix
%
%Return value
%   penalty - the actual penalty
%   dp - the derivative of the penalty with respect to tmat
%   d2p - the second derivative of the penalty with respect to tmat
%         this is a matrix.
function [p,dp,varargout]=dpenaltydtheta(obj,tmat)

p=comppenalty(obj,tmat);
%************************************************************************
%1st derivative
%***********************************************************************
% dp=conv2(tmat,obj.gradfilt,'same');
% 
% %convert to vectors
% dp=dp(:);
dp=tmat(:)'*obj.hessian;
%**************************************************************************
%2nd derivative
%**************************************************************************
if (nargout==3)
%d2p=obj.hessian;
varargout{1}=obj.hessian;
end





