%function compspeedup(obj)
%   
%Explanation: private function to compute the speedup for this data set
function compspeedup(obj)

seqfiles=obj.seqfiles;
bwidth=obj.bwidth;
bcent=obj.bcent;
for pind=1:size(seqfiles,1);

    %for each simulation in this pair create a BSExpLLike object
    for cind=1:size(seqfiles,2);
        bsllike(1,cind)=BSExpLLike('exllfile',[seqfiles(pind,cind).exllfile]);

        %select just those trials for which the predicted accuracy is a
        %double
        ntrials=length(bsllike(1,cind).trials);




        [ldata(pind,cind).exllike,ldata(pind,cind).trials]=getexmeand(bsllike(1,cind));


        tind=find(ldata(pind,cind).trials>obj.tcutoff);


        %different wavfile
        ldata(pind,cind).maxaccuracy=mean(ldata(pind,cind).exllike(:,tind),2);
    end

    %average the maxaccuracy across the datasets
    maxaccuracy=mean([ldata(pind,:).maxaccuracy],2);


    nwave=size(bsllike(1,1).exllike,1);
    for wind=1:nwave
        data(pind,wind).maxaccuracy=maxaccuracy(wind);


        %initialize the structure that stores the mapping from
        %fraction maxaccuracy to the trial on which that accuracy was
        %achieved
        data(pind,wind).trialvacc=nan(size(seqfiles,2),1/bwidth);


        %now for each bssllike object we need to create a plot
        %of the accuracy vs. the trial;

        for cind=1:size(seqfiles,2)
            %normalize the expected rate by the max rate
            nellike=ldata(pind,cind).exllike(wind,:)-data(pind,wind).maxaccuracy;

            %map each exllike to its corresponding bin
            bind=ceil(exp(nellike-log(bwidth)));

            for aind=1:1/bwidth
                %find all trials on which the accuracy maps to this bind
                %take the median of these values
                tind=find(bind==aind,1,'first');
                data(pind,wind).trialvacc(cind,aind)=median(ldata(pind,cind).trials(tind));

            end


            %use interpolation to fill in any missing values
            ind=~isnan(data(pind,wind).trialvacc(cind,:));
            data(pind,wind).trialvacc(cind,:)=interp1(bcent(ind),data(pind,wind).trialvacc(cind,ind),bcent);
        end

        %compute the speedup
        %assume infomax is in the second slot
        %could test this by looking at the stimchooser
        nshuffle=data(pind,wind).trialvacc(1,:);
        dnt=nshuffle./data(pind,wind).trialvacc(2,:);
        data(pind,wind).speedup=dnt;
    end
end

obj.data=data;
obj.ldata=ldata;
obj.bsllike=bsllike;
