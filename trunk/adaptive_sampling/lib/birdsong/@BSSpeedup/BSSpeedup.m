%function sim=BSSpeedup(seqfiles,tcutoff,bwidth)
%   seqfiles - the datafiles for each simulation
%               dimensions=npairsx2
%               First column should be infomax design
%           .exllfiles - the name of the file contianing the BSExpLLike
%           object for this dataset
%   tcutoff - the trial to use to compute the maximum converged value
%
%   bwidth- the width of the bins 
%           in which we measure % converged when computing speedup
%
% Explanation: This class computes the speedup due to the info. max. design
% 
%
%Revisions:
%   02-27-2009 - User has to specify the exllfiles
%             - before it automatically determined which files to use
%   12-27-2008 - Make version a structure with each field the name of a
%             different class.
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) BSSpeedup < BSAnalyze
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %data - is  a structure storing the results for each pair of infomax
    %shuffle  response sets
    %     - each row corresponds to a different pair
    %     - each column corresponds to a different wavefile in the test set
    

    %ldata is  a structure which stores information which is specific to each
    %     BSExpllike object that we process
    %    dimensions - npairs, size(seqfiles,2)
    %
    %bsllike - an array of the bssllike objects for each dataset
    %
    %seqfiles - the datafiles for each simulation
    %           dimensions=npairsx2
    %           Order of info. max. vs. shuffled matters
     properties(SetAccess=private, GetAccess=public)
        tcutoff=2*10^4;
        bwidth=[];
        bcent=[];
        
        seqfiles=[];
        data=[];
        ldata=[];
        bsllike=[];
    end
    
    

    methods(Static)
       obj=loadobj(lobj); 
    end
    methods(Access=private)
        compspeedup(obj); 
    end
    methods
        function bcent=get.bcent(obj)
           if (isempty(obj.bcent) && ~isempty(obj.bwidth)) 
               obj.bcent=obj.bwidth/2:obj.bwidth:1-obj.bwidth/2;
           end
           bcent=obj.bcent;
        end
        function obj=BSSpeedup(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'seqfiles','tcutoff','bwidth'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     obj.seqfiles=params.seqfiles;
                     obj.tcutoff=params.tcutoff;
                     obj.bwidth=params.bwidth;
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end


             %compute the speedup
             compspeedup(obj);
            %set the version to be a structure
            obj.version.BSSpeedup=090227;
        end
    end
end


