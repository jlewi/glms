%function [obj,sr]=gettrial(obj,trial)
%   trial - index of the trial to get
%
%Return value -
%   obj - The object is returned because we save several values so we only
%        compute them once
%   sr - an SRObject which contains the stimulus and response at this trial
%       - The actual input is a matrix - each column is the spectrogram at
%       a different time point. The most recent time is the last column.
function [obj,sr]=gettrial(obj,trial)
error('this function needs to be overwritten');