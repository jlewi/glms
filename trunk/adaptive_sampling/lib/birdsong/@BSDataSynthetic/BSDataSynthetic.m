%function sim=BSDataSynthetic(fname,model,theta)
%   fname - name of the matlab file containing the data
%   mobj  - model object to use to generate the responses
%   theta - model parameters
% Explanation: This class uses real stimuli but generates the responses
%   using a GLM. 
function obj=BSDataSynthetic(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'fname','model','theta'};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%

%declare the structure
obj=struct('version',080428,'model',[],'theta',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        pbase=BSData();
        obj=class(obj,'BSDataSynthetic',pbase);
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
    
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
obj.model=params.model
obj.theta=params.theta;
params=rmfield(params,'model','theta');
pbase=BSData(params);
obj=class(obj,'BSDataSynthetic',pbase);





    