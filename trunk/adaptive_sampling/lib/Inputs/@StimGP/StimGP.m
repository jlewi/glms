%function sim=StimGP(mu,c)
%      mu= mean of each stimulus - 1xdim(x_t)
%      .c = 1x(ktlength) cell array of the correlation matrices
%              .c(i)=v(|t_j-t_l|=i-1) = dim(x_t)xdim(x_t) matrix;
%
classdef (ConstructOnLoad=true) StimGP <handle
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        version=struct('StimGP',[]);

        %the dimensions of the input
        %   klength - dim(xt) - length of the stimulus at time t
        %   ktlength - number of stimuli in the joint distribution
        %            - 1 means p(s)=p(x_t)
        klength=0;
        ktlength=0;

        %mu= mean of each stimulus
        mu=[];
        %            .c = 1x(ktlength) cell array of the correlation matrices
        %               .c(i)=v(|t_j-t_l|=i-1);
        c={};
    end

    properties(SetAccess=private,GetAccess=public,Transient)
        %the mean and covariance matrix of p(s)
        %these are computed from mu and c
        mus=[];
        cs=[];
    end
    
    properties(SetAccess=private,GetAccess=public,Dependent)
        %dimensionality of the input
        dims=[]; 
    end
    methods
        function dims=get.dims(obj)
            dims=obj.klength*obj.ktlength;
        end
        function mus=get.mus(obj)
            if isempty(obj.mus)
                %construct the mean
                %using the gaussian parameters
                obj.mus=repmat(obj.mu, obj.ktlength,1);

            end
            mus=obj.mus;
        end

        function cs=get.cs(gp)
            if isempty(gp.cs)
                %compute the covariance matrix
                gp.cs=zeros(gp.klength*gp.ktlength,gp.klength*gp.ktlength);

                for t1=1:gp.ktlength
                    for t2=t1:gp.ktlength
                        rstart=(t1-1)*gp.klength+1;
                        rend=rstart+gp.klength-1;
                        cstart=(t2-1)*gp.klength+1;
                        cend=cstart+gp.klength-1;

                        gp.cs(rstart:rend,cstart:cend)=gp.c{t2-t1+1};
                    end
                end

                gp.cs=triu(gp.cs)+ triu(gp.cs,1)';

            end
            cs=gp.cs;
        end
        function obj=StimGP(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={'mu','c'};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    obj.mu=params.mu;
                    obj.klength=size(obj.mu,1);
                    obj.ktlength=size(params.c,2);
                    
                    if ~iscell(params.c)
                        error('C should be a cell array.');
                    end
                    obj.c=params.c;
                    
                    for cind=1:length(obj.c)
                       %make sure dimensions are good
                       if any(size(obj.c{cind})~=[obj.klength obj.klength])
                          error('Each matrix in C should be dim(x_t)xdim(x_t)'); 
                       end
                    end
                    
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %set the version to be a structure
            obj.version.StimGP=081229;
        end
    end
end


