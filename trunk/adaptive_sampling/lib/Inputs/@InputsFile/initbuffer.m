%function initbuffer(obj,tstart,tend)
%   tstart - the first trial to store in the buffer
%   tend   - the last trial to store in the buffer
function initbuffer(obj,tstart,tend)

obj.buffer=nan(obj.dim,tend-tstart+1);
obj.tend=tend;
obj.tstart=tstart;

%now read any trials that are in this interval;
%last input trial in the file
lasttrial=getlastid(obj);

if ~isnan(lasttrial)
for trial=tstart:min(lasttrial,tend)
   obj.buffer(:,trial-tstart+1)=readmatrix(obj,trial); 
end
end
