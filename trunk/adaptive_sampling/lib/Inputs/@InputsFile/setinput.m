%function s=getinput(obj,s,trial)
%
%Return value:
%   s - a matrix of inputs on the specified trials
function setinput(obj,s,trial)

if (length(trial)>1)
    error('function needs to modified to write more than one input at a time.');
end

if ~isfloat(s)
   s=getData(s); 
end
writedata(obj,s,trial);

%write it to the buffer
if (trial>=obj.tstart && trial<=obj.tend)
    obj.buffer(:,trial-obj.tstart+1)=s;
end