%function sim=ClassName(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Template for the constructor of a new class. This template
% shows how we can identify which constructor was called based on the
% parameters that were passed in.
%
%
%Revisions:
%   12-27-2008 - Make version a structure with each field the name of a
%             different class.
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) InputsFile < RAccessFile
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
      

    end
    properties(SetAccess=private,GetAccess=public,Transient)
       %a buffer which stores a fixed interval
       %call init  buffer to initialize
       buffer=[];
       tstart=nan; %trial of the first entry in buffer
       tend = nan; %trail of the last entry in buffer.
       
       dim=[];
    end
    methods(Static)
       obj=loadobj(obj); 
    end
    methods
        function dim=get.dim(obj)
           if isempty(obj.dim)
              head=getheader(obj);
              
              obj.dim=head.dim(1);              
           end
           dim=obj.dim;
        end
        function obj=InputsFile(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
           con={};

               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    %pass onto base class
                    bparams=params;
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            bparams=params;
           
            obj=obj@RAccessFile(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    %handled by base class
                otherwise
                 error('unexpected value for cind');
             end


            %set the version to be a structure
            obj.version.InputsFile=090120;
        end
    end
end


