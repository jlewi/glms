%functioon data=getData(inp)
%       inp - object or array of objects
%
%Explanation: Constructs a matrix of the input vectors as cols associated
%with this object. 
function data=getData(inp)
    error('GLMInput is an abstract class call getData on a subclass');
    