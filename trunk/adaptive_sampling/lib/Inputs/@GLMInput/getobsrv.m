%function getobsrv=obsrv(obj)
%	 obj=GLMInput object
% 
%Return value: 
%	 obsrv=obj.obsrv 
%
function obsrv=getobsrv(obj)
	 obsrv=obj.obsrv;
