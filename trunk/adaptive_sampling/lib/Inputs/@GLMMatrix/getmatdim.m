%function getmatdim=matdim(obj)
%	 obj=GLMMatrix object
% 
%Return value: 
%	 matdim=obj.matdim 
%
function matdim=getmatdim(obj)
	 matdim=obj.matdim;
