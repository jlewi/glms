%function getData=Data(obj)
%	 obj=GLMMatrix object
% 
%Return value: 
%	 data -
%
function data=getData(obj)
	 data=obj.data;
