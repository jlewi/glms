%function sind=samppcon(gp,spast)
%   pstim - matrix each column is a past stimulus
%
%Return value:
%       sind - the index of the stimulus
%Explanation: picks the next stimulus by sampling the conditional
%distribution for the Gaussian process.
%
function sind=samppcon(ps,spast)

msize=ones(1,ps.ktlength)*ps.nx;
spastind=[];
if ~isempty(spast)
    if isa(spast,'GLMStimIndex')
        spastind=[spast.index];
    else
        error('Input should be the stimulus index i.e class GLMStimIndex.');
    end
end
if (size(spastind,2)==ps.ktlength-1)

    %we need to get the linear indexes of the entries
    %corresponding to setting the past stimuli to the specified values
    %nan means we want all enteries
    lind=Indexing.lind(msize,[spastind, nan]);
    pcon=ps.p(lind);
else
    %since we have fewer than ps.ktlength-1 stimuli
    %we need to compute the size(spast,2)+1 marginal distribution
    %then compute the conditional distribution for this distribution as
    %use it to sample the stimuli
    pcon=zeros(ps.nx,1);

    nmarg=ps.ktlength-size(spastind,2)-1;
    for sind=1:ps.nx
        lind=Indexing.lind(msize,[spastind, sind, nan*ones(1,nmarg)]);
        pcon(sind)=sum(ps.p(lind));
    end
end

%normalize the distribution
pcon=pcon/sum(pcon);

%generate a random number between 0 and 1;
prob=rand(1,1);

cumprob=cumsum(pcon);
%compute the cumlative probability
sind=binarysubdivide(cumprob,prob);