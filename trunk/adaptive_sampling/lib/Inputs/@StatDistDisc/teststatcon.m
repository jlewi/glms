%Explanation: Create a stimulus distribution which is not stationary
%   and see if teststatcon returns false
%
function success=teststatcon

success=true;

nx=10;
ktlength=2;

stim=rand(1,nx);

%generate the joint distribution by generating the marginal distribution
%p(x) and then just multiplying
pm=rand(nx,1);
pm=pm/sum(pm);

p=pm*pm';

%***********************************************************************
%A stationary distribution
%***********************************************************************
obj=StatDistDisc('stim',stim,'ktlength',2,'p',p);

if ~isstationary(obj)
    success=0;
   error('isstationary  did not properly detect stationary distribution as being stationary');
else
    fprintf('Correctly computed that stationary distribution is stationary.');
end

%***********************************************************************
%A distribution which is not stationary
%***********************************************************************
%generate two different marginals
pm1=rand(nx,1);
pm1=normmag(pm1);
pm2=rand(nx,1);
pm2=normmag(pm2);

p=pm1*pm2';
p=p/sum(p(:));


obj=StatDistDisc('stim',stim,'ktlength',2,'p',p);

if isstationary(obj)
    success=0;
   error('isstationary  did not properly detect that distribution is not stationary');
else
    fprintf('Correctly computed that distribution is not stationary.');
end


warning('This test only tests the case klength=2');

