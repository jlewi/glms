%function getDim=Dim(obj)
%	 obj=GLMInputVec object
% 
%Return value: 
%	 Dim=obj.Dim 
%
%Explanation: get the dimensionality of the vector
function dim=getDim(obj)
	 dim=size(obj.data,1);
