%function [pcon]=pconsingmax(qobj,mag)
%   qobj - our quadratic object
%   mag  -magnitude constraint
%
% Return value
%   pcon=[minpcon maxpcon]
%           -empty if no valid range
%   pcon =[val]
%           -indicates there is a single valid value - should only be
%           possible if we have fixed terms.
%
%Explanation:
%   Computes the range of pcon values for which sigmamax will occur
%   with lam=emax;
%   Solutions for which the maximum occur at lam=emax are what [Fortin00] refers to as hard cas 2 in table
%   3.1.
%
%   The hard case 2 can only happen when the linear term in our quadratic
%   expression is orthogonal to the eigenspace of the maximum eigenector. 
%   This is easy to check when we don't have spike history terms. Because
%   the direction of the linear term is constant.
%
%   When we have spikehistory terms the linear term is a linear combination
%   of two vectors (alpha quad.wmuproj+ quad.w). Since this linear
%   combination depends on alpha only some values of alpha have solutions
%   at lam=emax.
%
%
%  So the following two conditions must be satisified for pcon=(projection
%  of stimulus on mean) to have solutation at lam=emax
%       i) the linear term must be orthogonal to the eigenspace of the
%       maximum eigenvector
%       ii) it must be possible to solve the magnitude constraint for that
%       value of pcon and lam=emax;
function [pcon]=pconsingmax(qobj,mag)
%tolerance for deciding numbers are zero
tol=eps*10^4;
d=length(qobj.quad.eigmod.eigd);            %dimensionality of y

%init return values;
pcon=[];

%*********************************************************************
%Get the indexes corresponding to terms of the max eigenvalue
%*********************************************************************
%if elements are sorted we can get these indexes efficiently
[emax indmax]=maxeval(qobj.quad.eigmod);
emax=emax(1);   %we want emax to be a scalar but if max eigenvalue is repeated
                %maxeval returns an array
                
%get indexes of terms which aren't equal to max eigenvalue
if issorted(qobj.quad.eigmod,1)
    %sorted in ascending order 
    %indexes of terms which don't have max eigenvalue
    indltmax=1:(min(indmax)-1);
    
elseif issorted(qobj.quad.eigmod,-1)
    %eigenvalues are sorted in descending order
     indltmax=max(indmax)+1:d;     %indexes of terms which don't have max eigenvalue
else
    %eigenvalues are not sorted
    indltmax=ones(1,d);
    indltmax(indmax)=0;
    indltmax=find(indltmax==1);
end

%value at this point
%emax=max eigenvalue
%indmax = indexes of terms corresponding to max eigenvalue
%indltmax= indexes of terms not corresponding to max eigenvalue

%**********************************************
%Separate into 2 cases:
%   a) No fixed terms. This means direction of linear term in our quadratic expression is independent
%      of pcon.
%   b) fixed terms. In this case direction of linear term depends on pcon.
%*****************************************************
%linear term= pcon * quad.wuproj + quad.w
%to determine if there are fixed terms we look at quad.w 
%if there are no fixed terms quad.w is zero
wnz=find(abs(qobj.quad.w(indmax))>tol);

if ~(any(abs(qobj.quad.w(indmax))>tol))
    %****************************************************
    %case a: no fixed terms
    %**************************************************
    %i) first we check if linear terms are orthogonal to the max
    %eigenvectors.
    %This is true if .wmuproj= 0 for all entries.
   % wmunz=find(abs(qobj.quad.wmuproj(indmax))>tol);

    if ~any(abs(qobj.quad.wmuproj(indmax))>tol)
        %linear term is orthogonal to the eigenspace of the max eigenvector
        %solutions exist at lam=emax whenever we can solve the first order
        %k.k.t without violating the power cosntraint.
        %Therefore, to find the range of pcon for which solutions exist at
        %lam=emax, we set lam=emax. We compute y_i(lam) for all terms 
        %other than those corresponding to the max eigenvectors. We set
        %y_emax=0. We then compute the power of y_i(\lam). The power is a
        %function of pcon. So we then solve this quadratic equation to get
        %valid values of pcon
        %solve: a pcon^2+b pcon + c =0
        a=1+sum(qobj.quad.wmuproj(indltmax).^2./(4*(qobj.quad.eigmod.eigd(indltmax)-emax).^2));
        b=sum(2*(qobj.quad.wmuproj(indltmax).*qobj.quad.w(indltmax))./(4*(qobj.quad.eigmod.eigd(indltmax)-emax).^2));
        c=sum((qobj.quad.w(indltmax)).^2./(4*(qobj.quad.eigmod.eigd(indltmax)-emax).^2))-mag^2;

%check if there exist real solutions for pcon
if (b^2-4*a*c>0)
    %at most 2 real solutions
    %compute the solutions
    nsols=0;
    pcon=[];
    
    %1st solution
    pmin=(-b - (b^2-4*a*c)^.5)/(2*a);
    %make sure its valid
    if (abs(pmin) <=mag)
        nsols=nsols+1;
        pcon(nsols)=pmin;
    end
    %2nd solution
   pmax=(-b+ (b^2-4*a*c)^.5)/(2*a);
    
    %make sure its valid
    if (abs(pmax) <=mag)
        nsols=nsols+1;
        pcon(nsols)=pmax;
    end
    
elseif  (b^2-4*a*c==0)
     nsols=0;
    %most 1 solution
    
    %1st solution
    qs=(-b /(2*a));
    %make sure its valid
    if (abs(qs) <=mag)
        nsols=nsols+1;
        pcon(nsols)=qs;
    end
else    
    %no real solutions exist to the quadratic eqn for pcon
    nsols=0;
    pcon=[];
end
    else
        %linear terms are not orthogonal to the max eigenvectors
        %therefore there is no way solution can be at lam=emax
         asol=[];
         pcon=[];
    end
else
    %****************************************************
    %case b: fixed terms
    %**************************************************
    %Since quad.w is not equal to zero, the only way for the linear term
    %to be orthogonal to the eigenspace of the max eigenvector is if there
    %exists values of pcon which solve the following linear equations.
    %i)we see if there exist values of pcon which solve 
    % pcon*qobj.quad.wmuproj+ qobj.quad.w=0;
    %
    %  qobj.quad.w - is not the zero vector because if it is it, case A
    %  would have been called.
    %
    %ii) we test these values of pcon to make sure they do not violate
    % the power constraint at lam=emax.
    %
    %
    %i) solve: pcon*qobj.quad.wmuproj+ qobj.quad.w=0;
 
    %a solution doesn't exist unless the projections of qobj.quad.wmuproj
    %and qobj.quad.w in the eigenspace of the max eigenvector are
    %orthogonal
    nwmuproj=normmag(qobj.quad.wmuproj(indmax));
    nw=normmag(qobj.quad.w(indmax));
    if (abs((nwmuproj'*nw-1))< (eps*10^6))
        %they are orthogonal so a solution does exist
    
        asol=zeros(length(indmax),1);
        %find value of asol needed to zero out each entry    
        asol=qobj.quad.w(indmax)./qobj.quad.wmuproj(indmax);
    
        %we might have 0/0 
        ind=find(~isnan(asol));
        asol=asol(ind);
        
        %each value of asol should be the same so just take the first one                
        pcon=asol(1);
     
        %make sure we don't violate the power constraint        
        y=ykkt(emax.mval,pcon*qobj.quad.wmuproj+qobj.quad.w,qobj.quad.eigmod.eigd,indltmax);
   
        if (y'*y>mag^2)
            pcon=[];
        end

    else
        %its impossible to make linear term orthogonal to the maximum
        %eigensapce
         asol=[];
         pcon=[];
    end
end

%set y from the kkt conditions
%       lambda - lambda
%       z       -z
%       emod.eigd - eigenvalues
%       yind - indexes of elements which get set based on first order
%       k.k.td
%                   other elements are zero
function y=ykkt(lambda,z,eigd,yind)
y=zeros(length(eigd),1);
y(yind)=-z(yind)./(2*(eigd(yind)-lambda));


