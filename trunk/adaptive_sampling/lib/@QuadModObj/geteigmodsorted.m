%function [eigmod]=geteigmodsorted(qobj)
%       qobj
%
%Explanation: Returns the eigendecomposition of the modified quadratic form
% It returns emod.evecs, emod.eigd, sorted in ascending order
%thus the first eigenvalue is zero corresponding the direction to which our
%modified quadratic form is orthogonal
function [eigmod]=geteigmodsorted(qobj)
    eigmod.esorted=qobj.quad.esorted;
    eigmod.eigd=qobj.quad.eigd;
    eigmod.evecs=qobj.quad.evecs;
    
    %make sure its sorted in ascending order
    if ~(eigmod.esorted==1)
        [eigmod.eigd ind]=sort(eigmod.eigd,[],'ascend');
        eigmod.evecs=eigmod.evecs(:,ind);
        eigmod.esorted=1;
    end
    