%function [obj,qtime]=QuadModObj('c',C,'eigck',val,'muk',val,'shist',val)
%   C       - the full coriance matrix
%             =[Ck Ckr; Ckr'; Cr];
%   eigck - eigendecomposition of just Ck               
%          .esorted =+1 sorted in ascending order
%                        = -1 sorted in descending order
%          .evecs
%          .eigd
%         These should be sorted in ascending order!!!!!!
%         nx1
%   muk - the stimulus vector we want to be orthogonal to
%       - This is automatically normalized upon creation
%   shist -value of the fixed terms of the inputs
%
%
%function obj=QuadModObj('test',d)
%       d - dummy parameter just set to empty matrix
%           later on could be a different test
%
%       This function runs various test harnesses to test the object
%
% Return value:
%          qtime -time to modify the quadratic form
%          obj.quad    - our modified quadratic expression
%               .eigck - eigendecomposition
%               .w
%               .wmuproj    - term needs to be multiplied by
%                                      pcon=projection on muk
%                           - pcon should be dot product of 
%               .dmuproj
%               .dmuproj2   
%               .mu   - vector in original coordinates to which we are
%               perpindicular
%   Let 
%          y= eigd.evecs'*k;
%   
%   sigma=
%           y'*quad.eigd*y 
%          +(muproj*quad.wmuproj+quad.w)*y
%           +(muproj^2*quad.dmuproj2+muproj*quad.dmuproj+quad.d)
%
%   sigma has the same value as sigma_true
%       sigma_true = xorth'Cxorth
%        xorth= evecs*y-(muk'*evecs*y)-y+muproj * muk 
%        x=[xorth;shist]
%
% Explanation: This object handles computing the modified quadratic form.
% The modified quadratic form is a quadratic form whose value
%  sutch that xorth'Cxorth=sigma (as defined above)
%
% This quadratic expression is completely independent of the magnitude of
% the mean. We only need the direction of the mean because we want to
% control the magnitude of the stimulus in the direction of the mean.
function [obj,qtime]=QuadModObj(varargin)
%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'C','eigck','muk'};
con(1).cfun=1;

con(2).rparams={'test'};
con(2).cfun=2;

%*******************************************************************
%Object structure
%******************************************************************
%eigck - eigendecomposition of the stimulus terms
%quad - structure to store the modified quadratic form
%C       - the full covariance matrix
%shist  - spike history
%muk  - the unit vector we want to be orthogonal to
%     - we automatically normalize this vector
%
%debug - indicates whether we should do checking about whether 
%           modified quadratic form is correct
%
obj=struct('quad',[],'C',[],'eigck',[],'shist',[],'muk',[],'debug',false);

switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'QuadModObj');
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        pnames=fieldnames(params);
        
     for j=1:length(pnames)
            switch pnames{j}
                case {'C','c'}
                    obj.c=params.(pnames{j});

                case 'eigck';
                    obj.eigck=params.(pnames{j});
                    %if its not an EigObj then create one
                    if ~isa(obj.eigck,'EigObj')
                       obj.eigck=EigObj('evecs',obj.eigck.evecs,'eigd',obj.eigck.eigd); 
                    end
                case 'shist';
                    obj.shist=params.(pnames{j});                   
                case 'muk';
                    obj.muk=params.(pnames{j});
                    obj.muk=obj.muk/(obj.muk'*obj.muk)^.5;                  
                case 'debug';
                    obj.debug=params.(pnames{j});
                otherwise
                    error('%s unrecognized parameter \n',pnames{j});
            end
        end

  
     %create the class
     obj=class(obj,'QuadModObj');
     [obj.quad qtime]=qmodshist(obj.c,obj.eigck,obj.muk,obj.shist,obj.debug);
     
     
    case 2
        %call  testquadmax
        testmaxquad;
    otherwise
        error('Constructor not implemented')
end
    

      

       