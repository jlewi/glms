%10-13-06
%script to debug/test function pconsingmax

error('this script needs to be adapted to work with QuadModObj');

%Instructions:
%load some test data which produces a quad structure 
%in the workspace
mag=.61533;                  %what magnitude constraint to use

tol=10^-10;
[pconrange]=pconsingmax(quad,mag);

if isempty(pconrange)
    fprintf('Quadratic function has no values for which max occurs at max eigenvalue');
else
    %generate a random value of pcon on the interval.
    if (length(pconrange)>1)
        r=rand(1);
        pcon=(pconrange(2)-pconrange(1))*r+pconrange(1);
    else
        pcon=pconrange;
    end
    %compute the z term at this value
    z=pcon*quad.wmuproj+quad.w;
    
    emax=max(quad.eigd);
    indmax=find(abs(quad.eigd)<tol);
    
    if ~isempty(find(abs(z(indmax))>tol))
        error('z corresponding to max eigenvector are not zero');
    else
        fprintf('its valid! \n');
    end
end
