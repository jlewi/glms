%10-05-06
% Test max and minimizing our quadratic function when we have fixed terms

error('this script needs to be adapted to work with QuadModObj');

%
klength=2;          %number of stimulus components
                            %this has to be two so that we can check our
                            %answer by doing a 1-d search
                            
alength=1;          %number of fixed components
n=klength+alength;


%**************************************
%parameters
%**************************************
mag=1;
optparam=[];

saveout=0;         %to save data and figs;
datetime=clock;
fname=seqfname(fullfile(RESULTSDIR,'intreg', sprintf('%2.2d_%2.2d_intreg.mat',datetime(2),datetime(3))));
vtosave.qmax='';
vtosave.qmin='';
vtosave.C='';
vtosave.mu='';
vtosave.mag='';


%*********************************
%Generate the mean
%*************************************
mu=ceil(rand(n,1)*10)/10
mu=mu/(mu'*mu)^.5;
muk=mu(1:klength);
muk=muk/(muk'*muk)^.5;
%*********************************
%Generate the fixed components
%*****************************************
shist=ceil(rand(alength,1)*10)/10

%***********************************
%Generate a covariance matrix
%***********************************
evecs=orth(rand(n,n));
eigd=ceil(rand(n,1)*10)/10;
[eigd ind]=sort(eigd,'ascend');
evecs=evecs(:,ind);
C=evecs*diag(eigd)*evecs';
%********************************

%**************************************************************************
%Testing
%************************************************************************

%we need the diagonalization of just the components corresponding to k
[eigck.evecs eigck.eigd]=eig(C(1:klength,1:klength));
eigck.eigd=diag(eigck.eigd);
%Crk=C(klength+1:end,1:klength);
%Cr=C(klength+1:end,klength+1:end);
%Ck=C(1:klength,1:klength);
%compute our modified quadratic function
%muk=muk/(muk'*muk)^.5;
%[quad.eigd, quad.evecs,quad.wmuproj,quad.dmuproj2]=quadmod(eigck.eigd,eigck.evecs,muk);
%quad.w=quad.evecs'*(2*(Crk'*shist-(shist'*Crk*muk)*muk));
%quad.dmuproj=2*(shist'*Crk*muk);
%%quad.dmuproj2=quad.dmuproj2+muk'*Ck*muk;
%quad.d=shist'*Cr*shist;
[quad]=quadmodshist(C,eigck,muk,shist);
%*************************************************
%test our modified quadratic form
%*************************************************
%generate some random vector
xk=ceil(rand(klength,1)*10)/10;
muproj=1.2;
%force projections of X along mu to be muproj
r=xk-(xk'*muk).*muk+muproj*muk;

%compute the true quad value r
quadr=[r;shist]'*C*[r;shist];

%quadmod
qmod=(quad.evecs'*xk)'*diag(quad.eigd)*(quad.evecs'*xk)+(quad.w+muproj*quad.wmuproj)'*(quad.evecs'*xk)+(muproj*quad.dmuproj+muproj^2*quad.dmuproj2)+quad.d;

%***************************************************
%range of alpha to compute max and min over
npts=50;
muprojrange=linspace(-mag, mag,npts);
muprojrange=[muprojrange 0];
%muprojrange=[0];
npts=length(muprojrange);

qmax=zeros(1,npts);
qmin=zeros(1,npts);
xmax=zeros(klength,npts);
xmin=zeros(klength,npts);

for j=1:length(muprojrange);
    muproj=muprojrange(j);
      [qmax(j),qmin(j),xmax(:,j),xmin(:,j)]=maxminquad(quad,muk,muproj,mag,optparam);
end

%*****************************************************************
%compute the correct values
cvals=[];
cvals.qmax=zeros(1,npts);
cvals.qmin=zeros(1,npts);
cvals.xmax=zeros(klength,npts);
cvals.xmin=zeros(klength,npts);
muorth=null(mu(1:klength)');
for j=1:length(muprojrange);
        muproj=muprojrange(j);
    powercon=mag^2-muprojrange(j)^2;
    
    fmaxobj=@(m)(-([m*muorth+muproj*muk; shist]'*C*[m*muorth+muproj*muk; shist]));
    fminobj=@(m)(([m*muorth+muproj*muk; shist]'*C*[m*muorth+muproj*muk; shist]));
    %[mmax cvals.qmax(j)] = fminbnd(fmaxobj,-powercon^.5,powercon^.5);
    [mmax] = fminbnd(fmaxobj,-powercon^.5,powercon^.5);
    cxmax=mmax*muorth+muproj*muk;
    cvals.qmax(j)=[cxmax;shist]'*C*[cxmax;shist];
    %[mmin cvals.qmin(j)]=fminbnd(fminobj,-powercon^.5,powercon^.5);
    [mmin]=fminbnd(fminobj,-powercon^.5,powercon^.5);
    cxmin= mmin*muorth+muproj*muk
    cvals.qmin(j)=[cxmin;shist]'*C*[cxmin;shist];
    cvals.xmax(:,j)=cxmax;
    cvals.xmin(:,j)=cxmin;
end


freg.hf=figure;
freg.xlabel='\mu_{\epsilon}';
freg.ylabel='\sigma';
freg.hp=zeros(1,2);
freg.lbls={};
freg.linewidth=1;
freg.name='Integration Region';
datetime=clock;
freg.fname=seqfname(fullfile(RESULTSDIR,'intreg', sprintf('%2.2d_%2.2d_intreg.eps',datetime(2),datetime(3))));
hold on;
freg.hp(1)=plot(muprojrange,qmax,'r.');
freg.lbls{1}='Max';
freg.hp(2)=plot(muprojrange,qmin,'b.');
freg.lbls{2}='Min';

pind=2+1;
[s ind]=sort(muprojrange);
freg.hp(pind)=plot(muprojrange,cvals.qmax(ind),getptype(pind,2));
freg.lbls{pind}='Max Correct';


pind=pind+1;
freg.hp(pind)=plot(muprojrange(ind),cvals.qmin(ind),getptype(pind,2));
freg.lbls{pind}='Min Correct';
freg=lblgraph(freg);
%fill([muprojrange(ind) muprojrange(ind(end:-1:1))],[qmax(ind) qmin(ind(end:-1:1))],3);

if (saveout~=0)
%save the Graph
exportfig(freg.hf,freg.fname,'bounds','tight','Color','bw');
%save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(fname,vtosave,saveopts);
end