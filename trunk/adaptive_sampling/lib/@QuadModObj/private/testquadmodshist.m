%

error('this script needs to be adapted to work with QuadModObj');

data={}
%Make a plot of the 2-d feasible region on multiple iterations
data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/glmgen/10_12/10_12_maxtrack_001.mat');
data(end).outfile=fullfile(RESULTSDIR,'glmgen/10_12/quadreg_001.eps');
data(end).trials=[80];
data(end).fsuffix='_intreg';
dind=length(data);
load(data(dind).infile);


%************************************************************************
%Obtain the quadratic form
%*************************************************************************
trial=data(dind).trials(tind);
post.m=pmax.newton1d.m(:,trial+1);
post.c=pmax.newton1d.c{trial+1};
srdata=srmax.newton1d;
    maxmu=mparam.mmag*(post.m'*post.m)^.5;
    [eigck.evecs eigck.eigd]=svd(post.c(1:mparam.klength,1:mparam.klength));
    eigck.eigd=diag(eigck.eigd);
   % sort them in ascending order
    [eigck.eigd ind]=sort(eigck.eigd,'ascend');
    eigck.evecs=eigck.evecs(:,ind);
    mu=pmax.newton1d.m(:,trial+1);

    %**********************************************************************
    %obtain the values needed to compute the feasible region
    %**********************************************
    %unit vector in direction of mean of just stimulus components
    %need its magnitude as well
    muk=post.m(1:mparam.klength,1);
    mukmag=(muk'*muk)^.5;
    muk=muk/mukmag;

    %*****************************************************************
    %compute the spike history
    %******************************************************************
    %spike history is just the previous responses
    %we take them to be zero if they happened before the start of the
    %experiment
    %tr-2 is actual number of observations we have already made
    nobsrv=trial-2;    %how many observations have we made
    shist=[];
    %only compute shist if we have spike history terms
    if (mparam.alength >0)
          shist=zeros(mparam.alength,1);
        if (mparam.alength <= (nobsrv))
            shist(:,1)=srdata.nspikes(nobsrv:-1:nobsrv-mparam.alength+1);
        else
            shist(1:nobsrv,1)=srdata.nspikes(nobsrv:-1:1);
        end
    end

    %projection onto mean of stimulus
    if isempty(shist)
        %no spike history terms
        muprojr=0;
    else
        muprojr=shist'*post.m(mparam.klength+1:end,1);
    end
    %*************************************************************************
    %Define the quadratic form
    [quad]=quadmodshist(post.c,eigck,muk,shist);

%*********************************************************************************
% TESTING
%**************************************************************************
%generate some random vvalue for projection on muk
pcon=ceil(rand(1,1)*10)/10;

%generate a random stimulus vector
xk=ceil(rand(mparam.klength,1)*10)/10;

%force projections of X along mu to be muproj
xpcon=xk-(xk'*muk)*muk+pcon*muk;

%compute the true quad value r
quadpcon=xpcon'*(post.c(1:mparam.klength,1:mparam.klength))*xpcon;

%compute the value of x using our modified form and see if it returns the
%same value
y=quad.evecs'*xk;

quadx=(quad.eigd'*(y.^2))+pcon*quad.wmuproj'*y+quad.w'*y+pcon*quad.dmuproj+pcon^2*quad.dmuproj2+quad.d;

%quad=[];
%[eigmod,quad.w,quad.d]=quadmod(eigck,muk)
%quad.eigd=eigmod.eigd;
%quad.evecs=eigmod.evecs;
%quadx=(quad.eigd'*(y.
[quadpcon quadx]
