%function [quad,eigtime]=quadmodshist(C,eigck,muk,shist)
%   C       - the full coriance matrix
%             =[Ck Ckr; Ckr'; Cr];
%   eigck - eigendecomposition of just Ck               
%          .esorted =+1 sorted in ascending order
%                        = -1 sorted in descending order
%          .evecs
%          .eigd
%         These should be sorted in ascending order!!!!!!
%         nx1
%   muk - the stimulus vector we want to be orthogonal to
%   shist -value of the fixed terms of the inputs
%
% Return value:
%          quad     - our quadratic function
%               .emod
%               .w
%               .wmuproj    - term needs to be multiplied by
%                                      pcon=projection on muk
%               .dmuproj
%               .dmuproj2   
%               .mu   - vector in original coordinates to which we are
%               perpindicular
%          eigtime - timing of the eigendecomposition
%   Let 
%          y= eigd.evecs'*k;
%   
%   sigma=
%           y'*quad.eigmod.eigd*y 
%          +(muproj*quad.wmuproj+quad.w)*y
%           +(muproj^2*quad.dmuproj2+muproj*quad.dmuproj+quad.d)
%
%   sigma has the same value as sigma_true
%       sigma_true = x'Cx
%        xorth= evecs*y-(muk'*evecs*y)-y+muproj * muk 
%        x=[xorth;shist]
%       
%Explanation: 7-29-2007
%   use the EigObj instead
function [quad,eigtime]=qmodshist(C,eigck,muk,shist,debug)
klength=length(muk);
alength=length(shist);

if isempty(eigck)
    fprintf('Eigendecomp not supplied. will be computed \n');
    [eigck]=EigObj('matrix',C(1:length(muk),1:length(muk)));
  
end
%compute our modified quadratic function
muk=muk/(muk'*muk)^.5;
[quad.eigmod,quad.wmuproj,quad.dmuproj2,eigtime]=qmod(eigck,muk,debug);

%are there 
%spike history terms?
if (alength>0)
    tic
    warning('using the covariance matrix computed via woodbury may introduce errors due to inaccuracies of woodbury');
Crk=C(klength+1:end,1:klength);
Cr=C(klength+1:end,klength+1:end);
Ck=C(1:klength,1:klength);

quad.w=quad.eigmod.evecs'*(2*(Crk'*shist-(shist'*Crk*muk)*muk));
quad.dmuproj=2*(shist'*Crk*muk);
%quad.dmuproj2=quad.dmuproj2+muk'*Ck*muk;
quad.d=shist'*Cr*shist;
%qtime=qtime+toc;
else
    %no spike history terms
    quad.d=0;
    quad.dmuproj=0;
    quad.w=zeros(klength,1);
end

%1-22-07
%one eigenvector should be parallel (or antiparallel)to muk and have eigenvalue zero
%to avoid numerical issues force the eigenvalue to be zero
%veryify its perpindicular
%
%8-02-2007
%I was assuming that the 
if issorted(quad.eigmod,1)
    %perhap
    mp=quad.eigmod.evecs(:,1)'*muk;
    
    %force eigenvector parallel to mean to have zero eigenvalue
    eigd=quad.eigmod.eigd;
    if (abs(abs(mp)-1)>10^-6)
%        error('muk is not parallel to eigenvector?');
       %mean is not parallel to first eigenvector 
       %maybe its parallel to another one
       mp=getevecs(quad.eigmod)'*muk;
       ind=find(abs(abs(mp)-1)<10^-4,1);
       if isempty(ind)
           %you might night need to increase the threshold for EigObj
           warning('muk is not parallel to an eigenvector;you might night need to increase the threshold for EigObj');
       else
           %I think muk may be parallel to one of the eigenvectors just not
           %the first one because of numerical error (or multiplicities >1)
           warning('muk is parallel to eigenvector %d, not the first eigenvector',ind);
           %set all eigenvalues less than this to zero
          eigd(1:ind)=0; 
       end
    else
            %force corresponding eigenvalue to be zero.
            eigd(1)=0;
    end


    quad.eigmod=EigObj('evecs',quad.eigmod.evecs,'eigd',eigd);
else
    error('why are eigenvectors not sorted in ascending order');
end