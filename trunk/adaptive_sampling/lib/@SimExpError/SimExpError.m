%function sim=SimExpError('simfile');
%   datafile - file containing the simulation
%function sim=SimExpError('datafile')
%   msefile  - file to load the object from
% Explanation: This class is used to compute the expected value of the
% error between theta and the true parameters.
%
%
%Revisions:
%
classdef (ConstructOnLoad=true) SimExpError < handle
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %
    % simfile - file storing the simobject
    %
    % experr  - a structure storing the expected error 
    %         - we store this because it is expensive to compute
    %
    %outfile     - where we store the object
    %
    %label    - label for the data
    %         - storing the label should make it unnecessary to actual load
    %           the Simobjects if we just want to plot the results
    %sim      - the simulation object
    %
    properties(SetAccess=public,GetAccess=public)
        label=[];
    end
    properties(SetAccess=private, GetAccess=public)        
        simfile=[];
        experr=[];
        outfile=[];
        version=struct();
    end

    
     properties(SetAccess=private, GetAccess=public,Transient)
        sim=[];
        
     end
    methods(Static)
       obj=loadobj(lobj); 
    end
    methods
         function label=get.label(obj)
           if (isempty(obj.label) && ~isempty(obj.sim)) 
               obj.label=obj.sim(1).label;
           end
           label=obj.label;
         end
        
       function outfile=get.outfile(obj)
           if (isempty(obj.outfile)  && ~isempty(obj.simfile))
              obj.outfile=BSAnalyze.filename(obj.simfile,'experror'); 
           end
           outfile=obj.outfile;
    end
       
        function sim=get.sim(obj)
            if isempty(obj.sim)
             for j=1:length(obj.simfile)
                         v=load(getpath(obj.simfile(j)));
                         if (j==1)
                            obj.sim =v.simobj;
                         else
                             obj.sim(j)=v.simobj;
                         end
             end
            end
            sim=obj.sim;
                     
        end
        
        function obj=SimExpError(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'simfile'};
            con(2).rparams={'errfile'};


               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    %do nothing
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);
            
            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     if (length(params.simfile)>1)
                         error('simfile should be a single file');
                     end
                     
                     obj.simfile=params.simfile;
                     %check if there is an mse file for this datafile
%                      obj.simfile=params.simfile;
                     if exist(getpath(obj.outfile),'file')
                        %load the object from the file
                        fprintf('file containing SimExpError object exists. loading object from file \n');
                        v=load(getpath(obj.outfile));
                        obj=v.seobj;
                     end

                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end


            %set the version to be a structure
            obj.version.SimExpError=090422;

        end
    end
end


