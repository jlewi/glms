%function compexerr(seall,nsave)
%   dset - the dataset to compute the expected err for
%   outfile - file to periodically save the boject to so that
%
%Explanation: COmpute the expected err error between the map and the final
%batch estimate
function errdata=compexerr(seall,nsave)

if ~exist('outfile','var')
    outfile=[];
end

if ~exist('nsave','var')
    nsave=[];
end
if isempty(nsave)
    nsave= 100;
end

for bsind=1:length(seall)
    seobj=seall(bsind);
    nsets=length(seobj.sim);

    if isempty(seobj.experr)
        seobj.experr=repmat(struct('trials',[],'exerr',[],'errstim',[],'errshist',[],'errbias',[]),nsets,1);
    end

    ttrue=seobj.sim.observer.theta;


    for sind=1:nsets
        sim=seobj.sim(sind);

      
        mobj=sim.mobj;

        %determine which trials we saved the covariance matrix on
        %check if we are continuing a previous started computation
        if (~isempty(seobj.experr(sind).trials))
            trials=seobj.experr(sind).trials;


            %check for new trials
            alltrials=readmatids(sim.allpost.cinfo.caccess);
            alltrials=sort(alltrials);

            if (length(alltrials)>length(trials))
                nnew=length(alltrials)-length(trials);
                seobjq.experr(sind).trials=[seobjq.experr(sind).trials nan(1,nnew)];

                seobjq.experr(sind).err=[seobjq.experr(sind).err nan(1,nnew)];
                seobjq.experr(sind).errstim=[seobjq.experr(sind).errstim nan(1,nnew)];

                if ( errseq.alength>0)
                    seobjq.experr(sind).errshist=[seobjq.experr(sind).errshist nan(1,nnew)];

                end


                if  ( errseq.hasbias>0)
                    seobjq.experr(sind).errbias=[seobjq.experr(sind).errbias nan(1,nnew)];

                end
            end

            tstart=find(isnan(seobj.experr(sind).err),1,'first');

            if isempty(tstart)
                tstart=1+length(seobj.experr(sind).trials);
            end

        else
            %make sure trials is sorted
            trials=readmatids(sim.allpost.cinfo.caccess);
            trials=sort(trials);
            tstart=1;

            seobj.experr(sind).trials=trials;

            seobj.experr(sind).err=nan(1,length(trials));
            seobj.experr(sind).errstim=nan(1,length(trials));
            if (sim.mobj.alength>0)
                seobj.experr(sind).errshist=nan(1,length(trials));
            end
            if (sim.mobj.hasbias)
                seobj.experr(sind).errbias=nan(1,length(trials));
            end

        end



        for tind=tstart:length(seobj.experr(sind).trials)
            if (mod(tind,100)==0)
                fprintf('sim %d of %d: trial %d of %d \n', sind,nsets, tind,  length(seobj.experr(sind).trials));
            end
            trial=seobj.experr(sind).trials(tind);

            mu=getm(sim.allpost,trial);
            c=getc(sim.allpost,trial);

            errall=errfun(mu,c,ttrue);
            seobj.experr(sind).err(tind)=errall;

            mustim=mu(mobj.indstim(1):mobj.indstim(2));
            cstim=c(mobj.indstim(1):mobj.indstim(2),mobj.indstim(1):mobj.indstim(2));
            tstim=ttrue(mobj.indstim(1):mobj.indstim(2));
            errstim=errfun(mustim,cstim,tstim);
            seobj.experr(sind).errstim(tind)=errstim;

            if (mobj.alength>0)
                mushist=mu(mobj.indshist(1):mobj.indshist(2));
                cshist=c(mobj.indshist(1):mobj.indshist(2),mobj.indshist(1):mobj.indshist(2));
                tshist=ttrue(mobj.indshist(1):mobj.indshist(2));

                errshist=errfun(mushist,cshist,tshist);
                seobj.experr(sind).errshist(tind)=errshist;
            end

            if (mobj.hasbias)
                musbias=mu(mobj.indbias);
                csbias=c(mobj.indbias,mobj.indbias);
                tsbias=ttrue(mobj.indbias:mobj.indbias);

                errbias=errfun(musbias,csbias,tsbias);
                seobj.experr(sind).errbias(tind)=errbias;
            end

            %periodically save the data
            if (~isempty(seobj.outfile) && mod(tind,nsave)==0)
                fprintf('compexperr: saving data to %s \n',getpath(seobj.outfile));
                if exist(getpath(seobj.outfile),'file')
                    save(getpath(seobj.outfile),'seobj','-append');
                else
                    save(getpath(seobj.outfile),'seobj');
                end
            end
        end

        if (tstart<=length(seobj.experr(sind).trials))
            %save the data
            if exist(getpath(seobj.outfile),'file')
                save(getpath(seobj.outfile),'seobj','-append');
            else
                save(getpath(seobj.outfile),'seobj');
            end
        end
        errdata(bsind,sind)=seobj.experr(sind);
    end

end
function err=errfun(mu,c,ttrue)
err=trace(c)+mu'*mu-2*ttrue'*mu+ttrue'*ttrue;
