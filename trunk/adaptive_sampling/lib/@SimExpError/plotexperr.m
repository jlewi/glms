%function plotexperr(obj)
%
%explanation: plot the expected err of each component
%   For this we use the marginal distributions on the stimulus, spike
%   history, or bias components respectively.
%
function [ferr,otbl]=plotexperr(obj)


%make a plot of the err
nplots=1;

errdata=compexerr(obj(1));

hasshist=false;
hasbias=false;
if ~isempty(errdata.errshist)
    nplots=nplots+1;
    hasshist=true;
end

if ~isempty(errdata.errbias)
    nplots=nplots+1;
    hasbias=true;
end

ferr=FigObj('name','Expected Error','width',7,'height',7,'naxes',[nplots 1]);

rind=1;

%loop over the simulations because we only want to read the maps once
for sind=1:length(obj)

    rind=1;
    
    %get theta for this simulation
    lbl=obj(sind).label;
    
    errdata=compexerr(obj(sind));
    
    %*********************************************************************
    %Plot the err of the stimulus coefficients
    %*****************************************************************
    setfocus(ferr.a,rind,1);
    
    
    hp=plot(errdata.trials,errdata.errstim);
    
    addplot(ferr.a(rind,1),'hp',hp,'lbl',lbl);

    %*********************************************************************
    %Plot the err of the spike history coefficients
    %*****************************************************************
    if ~isempty(errdata.errshist)
        rind=rind+1;
        setfocus(ferr.a,rind,1);
    
        hp=plot(errdata.trials,errdata.errshist);
    
        addplot(ferr.a(rind,1),'hp',hp);
    end
    
    %*********************************************************************
    %Plot the err of the bias
    %*****************************************************************
    if (hasbias);
       rind=rind+1;
        setfocus(ferr.a,rind,1);
        hp=plot(errdata.trials,errdata.errbias);
    
        addplot(ferr.a(rind,1),'hp',hp);
    end
    
end


%*********************************************************************
%add labels
%*********************************************************************
%%
rind=1;
title(ferr.a(rind,1),'Stimulus coefficients');
ylabel(ferr.a(rind,1),'$E_\theta||\theta- \theta_o||^2$','Interpreter','latex');
    set(ferr.a(rind,1),'xscale','log');
    
if (hasshist)
    rind=rind+1;
    title(ferr.a(rind,1),'Spike history coefficients');
    ylabel(ferr.a(rind,1),'$E_\theta||\theta - \theta_o||^2$','Interpreter','latex');
    set(ferr.a(rind,1),'xscale','log');    
end

if (hasbias)
    rind=rind+1;
    title(ferr.a(rind,1),'Bias');
    ylabel(ferr.a(rind,1),'$E_\theta||\theta- \theta_o||^2$','Interpreter','latex');  
    set(ferr.a(rind,1),'xscale','log');
end

xlabel(ferr.a(rind,1),'trial');

lblgraph(ferr);

setposition(ferr.a(1,1).hlgnd,.5,.82,[],[]);

dinfo=cell(length(obj),1);

for sind=1:length(obj)
   dinfo{sind,1} =obj(sind).label;
   dinfo{sind,2} =getrpath(obj(sind).outfile);
end
dinfo{end+1,1}='explanation';
dinfo{end,2}=sprintf('We plot the expected squared error between theta and the true theta (\\hat{\\theta}}). \n The expectation is computed using the marginal distribution of the posterior on the stimulus, spike history, or bias coefficients.');

otbl={ferr,[{'class',mfilename('class');'function',mfilename('full');};dinfo]};
onenotetable(otbl,seqfname('~/svn_trunk/notes/comperr.xml'));
