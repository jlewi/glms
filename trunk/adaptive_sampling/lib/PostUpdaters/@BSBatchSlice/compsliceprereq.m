%function info=compsliceprereq(uobj,w,prior)
%   
%
%Explanation: This computes a structure which
%   contains all the variables that we only want to compute
%   once to minimize the gradient along a slice w through our posterior.
%
%This is used by update
function info=compsliceprereq(uobj,theta,w,mobj,prior)
cinv=getinvc(prior);
  info.wcow=w'*cinv*w;

    %each inpw is a cell array contain s'*w for all the inputs
    %in a different wave file
	windexes=getwindexes(uobj.bpost);
    nwavefiles=length(windexes);
    info.inpw=cell(1,nwavefiles);
    info.inptheta=cell(1,nwavefiles);
    info.trialobsrv=cell(1,nwavefiles);
    
    for wc=1:length(windexes);
        wind=windexes(wc);
        info.inpw{wind}=compglmproj(uobj.bpost,wind,mobj,w);
        [info.inptheta{wind} stimspec info.trialobsrv{wind}]=compglmproj(uobj.bpost,wind,mobj,theta);
       
        [ntrials,cstart]=getntrialsinwave(getbdata(uobj.bpost),wind);
        info.trialobsrv=info.trialobsrv(:,cstart:end);
    end

    %cross product betwen (theta-muprior)co^-1 w
    info.tcow=-(theta-getm(prior))'*cinv*w;
