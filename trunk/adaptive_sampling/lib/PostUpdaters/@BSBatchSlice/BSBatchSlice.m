% function sim=BSBatchSlice('bpost',bpost)
% 
%   This constructor is mainly used for the test functions
%   since the update method takes a datafile as well.
%   
% Explanation: Fit a GLM to the bird song data using our batch method
%
%
function obj=BSBatchSlice(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'bpost'};
con(1).cfun=1;



con(2).rparams={};
con(2).cfun=2;
%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
% bpost   - a bslogpost object
%declare the structure
obj=struct('version',080510,'bpost',[]);

%optimization settings
optim=[];
optim=optimset(optim,'Jacobian','on');
%optim=optimset(optim,'DerivativeCheck','on');
optim=optimset(optim,'Display','on');
optim=optimset(optim,'TolX',10^-8);
optim=optimset(optim,'TolFun',10^-8);
optim=optimset(optim,'MaxIter',10000000);
if isempty(optimget(optim,'MaxFunEvals'))
    optim.MaxFunEvals=1000;
end

pbparams.optim=optim;
pbase=PostUpdatersBase(pbparams);

switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'BSBatchSlice',pbase);
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);
cind=cind(1);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        
        obj.bpost=params.bpost;
    otherwise
        error('Constructor not implemented')
end
    
% windexes=[];
% maxrepeats=[];
% if isfield(params,'windexes')
%     windexes=params.windexes;
% end
% if isfield(params,'maxnepeats')
%    maxrepeats=params.maxrepeats; 
% end




%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one

obj=class(obj,'BSBatchSlice',pbase);





    

    