%function [theta,]=update(uobj,datafile,thetainit,windexes,maxrepeats)
%   datafile - file containing the BSData object as well as the posterior
%
%windexes - which wave files to use to compute the log-posterior and its
%      derivatives
%   maxrepeats - how many repeats of each wave file to use.
%
% Return value
%   WARNING: post.c and einfo will not always match
%       This is because post.c is computed using sherman-morrison formula
%       einfo is computed if possible using Gu and Eisenstat
%       THis results in slight numerical differences between the post.c and
%       the covariance matrix constructed from the eigendecomposition. Over
%       many iterations these differences accumulate.
%
%
%%%Explanation:
%   Find the MAP by doing a bunch of a 1-d searches along slices of the
%   log-posterior
%
function [theta,uinfo]=update(uobj,datafile,thetainit,windexes,maxrepeats)

optim=getoptim(uobj);

if ~exist('thetainit','var')
    thetainit=[];
end


%**************************************************************************
%load the allpost, mobj, bdata
%*************************************************************************
data=load(getpath(datafile));
allpost=data.allpost;


%load bdata from the file
bdata=data.bdata;

uobj.bpost=BSlogpost('bdata',bdata);
mobj=data.mobj;
clear data;

%create the object to compute the log posterior and its derivatives
uobj.bpost=BSlogpost('bdata',bdata,'windexes',windexes,'maxrepeats',maxrepeats);


if isempty(thetainit)
    thetainit=getm(allpost,0);
end

%***********************************************************
%get the prior
%*************************************************************
prior=GaussPost('m',getm(allpost,0),'c',EigObj('matrix',getc(allpost,0)));
cinv=getinvc(prior);
glm=getglm(mobj);

options=getoptim(uobj);


%information about the update
uinfo=[];
%keep track of the slices along which we find the MAP
%each column is a different slice
%uinfo.slices=zeros(getparamlen(mobj),getparamlen(mobj));

%keep track of the theta for debuggin purposes
%uinfo.theta=zeros(getparamlen(mobj),getparamlen(mobj));
%uinfo.dlpost=zeros(getparamlen(mobj),getparamlen(mobj));
theta=thetainit;




   %get the derivative evaluated at theta. This will be the direction
    %along which we find the map
    [dlpost,d2lpost]=compd2lpost(uobj.bpost,mobj,theta,prior);

    
    %we need to do d=dim(theta) 1-d searches
niter=0;
    fprintf('%0.3g iteration Magnitude of derivative %03.3g \n',niter,(dlpost'*dlpost)^.5);


while ((dlpost'*dlpost)^.5>options.TolFun)
    niter=niter+1;
   
 
%     [u s v]=svd(d2lpost);
%     s=diag(s);
%     invs=s.^-1;
%     
%     %compute the order of the singular values
%     oinvs=floor(log10(invs));
%     
    %when choosing the direction for the search
    %we either want to search in a direction in which the gradient is very
    %flat or very sharp. We don't want to start finding linear combinations
    %of directions in which the gradient is sharp with one where its
    %relatively flat. I think this risks numerical issues because we end up
    %adding small numbers to larger numbers
%     doinvs=diff(oinvs);
%     
%     %ind is the location of the singular values where the order (power of
%     %10)
%     %of the singular value is different from the next lowest value
%     ind=find(doinvs>0);
%     
%     %number of candidate directions
%     ncands=length(ind)+1;
%     
%     wcand=zeros(getparamlen(mobj),ncands);
%     
%     %add a 1 for the first index
%     isub=[1;ind;length(s)];
%     vproj=v'*dlpost;
%     for cind=1:length(isub)-1        
%         sind=isub(cind);
%         eind=isub(cind+1);
%        wcand(:,cind)=u(:,sind:eind)*diag(invs(sind:eind))*vproj(sind:eind);
%     end
%     
%     wcmag=sum(wcand.^2,1).^.5;
%     
%     [wcmagmax mind]=max(wcmag);
%     w=wcand(:,mind);
    
    
%every other iteration we alternate between minimziing the gradient 
%along a direction defined by just the bias term (assuming no spike
%history)
%and a direction which leaves biased term unchaged
w=inv(d2lpost)*dlpost;
% if (mod(niter,getparamlen(mobj))==0)
%     %change bias
%     w(1:end-1)=0;
% else
%     %else leave bias unchanged
%     w(end)=0;
% end
w=normmag(w);
    
  %  w=normmag(inv(d2lpost)*dlpost);
%  w=normmag(w);
   % w=normmag(dlpost);
    
    %remove from w the projections along all the last remaining
    %dim(w-1) existing vectors
    %this doesn't allow our basis to change
    %it would be faster just to iterate over 
     if (niter>1 && niter<=length(w))
         basis=[uinfo.slices{1:niter-1}];
         w=w-basis*(basis'*w);
         w=normmag(w);
     elseif (niter>length(w))
         %basis=[uinfo.slices{niter-length(w)-3:niter-1}];
         %%basis should already be orthogonal vectors
         %w=w-basis*(basis'*w);
         %w=normmag(w);
         w=uinfo.slices{mod(niter-1,length(w))+1};
     end
     
     if (mod(niter,2)==1)
         w=[1;0];
     else
         w=[0;1];
     end
    uinfo.slices{niter}=w;
    uinfo.dlpost{niter}=dlpost;
    
    %debugging check if this slice is orthogonal to previous slices
%     if (any(abs(w'*uinfo.slices(:,1:sind-1))>10^-8))
%         error('current slice is not orthogonal to previous slices.');
%     end
    %compute the values we will need to find the map along the slice
    %we only want to compute these once
    info=compsliceprereq(uobj,theta,w,mobj,prior);
    
%    [theta,dw]=findMAPonslice(uobj,theta,w,info,mobj);

%try doing binary search
dwinit=0;
dwthresh=options.TolX;
    [dwnew,fval,nbiter]=binarysearch(uobj,w,info,mobj,dwthresh*10^-4,dwinit);
    theta=theta+dwnew*w;
    
    uinfo.theta{niter}=theta;
    
      %get the derivative evaluated at theta. This will be the direction
    %along which we find the map
    [dlpost,d2lpost]=compd2lpost(uobj.bpost,mobj,theta,prior);

    %debugging
    %check derivative in direction w is now zero
    if (abs(dlpost'*w)>10^-8)
        error('Derivative in direction w is not zero');
    end
  fprintf('%0.3g iteration Magnitude of derivative %03.3g \n',niter,(dlpost'*dlpost)^.5);

end

if varinfile(getpath(datafile),'MAPonslice')
    data=load(getpath(datafile),'MAPonslice');
    MAPonslice=data.MAPonslice;
    MAPonslice=[MAPonslice uinfo];
else
    MAPonslice=uinfo;
end
save(getpath(datafile),'MAPonslice','-append');