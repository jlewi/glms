%function oinfo=objinfo(obj)
%   obj - object
%
%oinfo - 2d cell array which describes this object
%
%Explanation: idea is to populate a cell array with information about this
%object. We can then pass this cell array to oneNoteTable to create a table
%which can be imported into matlab.
%
%$Revision: 1468 $ - use structinfo
function oinfo=objinfo(obj)

oinfo={'Model',class(obj)};

s=struct(obj);

%remove fields we don't want to grab info for
%s=rmfield(s,);

oinfo=[oinfo;structinfo(s)];

    
