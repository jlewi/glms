%funciton pdata=fillinlowmem(nobj, pdata,mobj,sr,trials)
%   newton1d - object
%   pdata - data we want to fill in
%       .m - mean
%       .c - covariance matrix
%   mobj   - model
%   sr    - spike data
%   trials - last trial on which to compute the covariance matrix
%            -leave blank to compute it for all trials.
%
%Explanation: This computes all covariance matrices for 1:max(Trials)
%      we compute all covariance matrices because we need them all to
%      compute the updated covariance matrix. 
%
%5-28-2007
%   made it function of Newton1d
%4-18-2007
%   modified it so it computes the covariance matrix without refinding the
%   peak of the posterior
%   turned it into a function
%
%   got rid of loop over trials and simvars.
%2-16-2007
%   modified it so that I can use files in old format using the new object
%   oriented model.
%10_27_06
%This is a script to compute missing covariance matrices for the newton1d update.
%I.e find the posterior covariance matrices which we did not save
%clear all;
%setpathvars;
function [pdata]=fillinlowmem(nobj, pdata,mobj,sr,trials)

if ~exist('trials','var')
    trials=[];
end

if isempty(trials)
    trials=size(pdata.m,2)-1;
end

plast.m=pdata.m(:,1);
plast.c=pdata.c{1};
for trial=1:max(trials);       
        %make sure covariance matrix doesn't exist for this trial.
        if ~isempty(pdata.c{trial+1})
           fprintf('Skipping trial covariance already exists  for trial %d \n ',trial);
        else
          
            x=sr.y(:,trial);
            obsrv.n=sr.nspikes(trial);
            [gd1, gd2]=d2glmeps(mobj.glm,pdata.m(:,trial+1)'*x,obsrv.n);
            post.c=plast.c-plast.c*x*(-gd2/(1-gd2*x'*plast.c*x))*x'*plast.c;

            
         
            pdata.c{trial+1}=post.c;
            plast=post;
                        
        end
    end


