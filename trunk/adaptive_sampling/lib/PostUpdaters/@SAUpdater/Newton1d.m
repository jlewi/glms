%function nobj=Newton1d(varargin)
%
%Explanation: base class for the newton1d updater. 
%
%Constructors
%   Newton1d()
%   Newton1d('compeig',val)
%           compeig - indicates whether or not to compute the
%           eigendecomposition of the covariance matrix
function nobj=Newton1d(varargin)

nobj=struct([]);
    



switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        nobj=class(nobj,'Newton1d',PostUpdatersBase());
        return
    case 1
        %varargin should be a structure corresponding to fieldname value
        %pairs
        %this is useful for recieving arguments passed in from the
        %constructor of a child class
        params=varargin{1};
    otherwise
        %convert varargin into a structure array
        params=parseinputs(varargin);
end

%pass all arguments to base class
pbase=PostUpdatersBase(params);
pbase.mname='Newt1on1d';
nobj=class(nobj,'Newton1d',pbase);

%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        otherwise
            params.(vars{j})=vars{j+1};
    end
end