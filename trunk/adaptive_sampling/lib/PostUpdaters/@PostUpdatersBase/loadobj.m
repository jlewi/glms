%function loadobj(obj)
%
%Explanation: 
%   -handle backwards compatability
%
function obj=loadobj(fobj)
    obj=fobj;
    if isstruct(fobj)        
        %warning('Saved object was an older version. Converting to newer version');
        
        %initialize object
        obj=PostUpdatersBase();
        latestver=obj.bversion;
    
        %convert versions prior to 070615 
        %070615 introduced the version field
        if isfield(fobj,'version')
           fprintf('PostUpdatersBase: Converting to version 070615 \n');
           fobj.version=070615; 
        end
        
        %version 080731 renamed version bversion
        if ~isfield(fobj,'bversion')
          fprintf('PostUpdatersBase: Converting to bversion 080731 \n');
           fobj.bversion=080731;
           
           %copy the fields skip version
           obj=copyfields(fobj,obj,{'version'});
        end
        
        if (obj.bversion<090108)
            %do nothing we just renamed the field
            obj.bversion=090108;
            obj.version.PostUpdatersBase=090108;
        end
        if (obj.bversion<latestver)
            error('Need to update conversion to latest revision');
        end
    end
    
%function copyfields 
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
function dest=copyfields(source,dest,skip)
 %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(source);

        %struct for object
        sobj=struct(dest);
        for j=1:length(fnames)
            switch fnames{j}
                case skip
                    %do nothing we skip this field
                otherwise
                    %set the new field this will cause an error
                    %if the field isn't a member of the new object
                      dest.(fnames{j})=source.(fnames{j});
            end
        end