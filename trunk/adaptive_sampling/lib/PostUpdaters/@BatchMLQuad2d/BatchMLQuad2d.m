%function sim=BatchMLQuad2d(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object 
%
% Explanation: This class uses maximum likelihood to estimate the
% parameters of an energy model. That is the GLM is:
%E(r)=f((k1'x)^2+(k2'x)^2)
% This is a GLM with a nonlinearity on the input. We use max likelihood to
% estimate the filter coefficients k1, k2.
%
% Currently this class is not intended to be used to construct posterior
% distributions for use during the optimization step.
%
% Therefore, the calss doesn't inherit from PostUpdersBase. I think with
% some slight modifications it could be made to work but that is not my
% intention.

function obj=BatchMLQuad2d(varargin)

%**************************************************************
%Required parameters
%*****************************************************************
%if fielname is required then set a field of req.fieldname=0
%when we read in that parameter we set req.fieldname=1
req=[];

%**********************************************************
%Define Members of object
%**************************************************************
% var1          - description
% var2          -description

%declare the structure
optim=optimset('TolX',10^-12,'TolF',10^-12);
obj=struct('optim',optim);


%create the optimzation structure to specify the options for calls to
%optimization functions
%use default values 
%simparam.optparam=optimset();

%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    %pbase=PostUpdatersBase();
    obj=class(obj,'BatchMLQuad2d');
    return
end

%**************************************************************************
%Parse the input arguments
%create a new simulation
for j=1:2:nargin
    switch varargin{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        otherwise
            %*********************************
            %generic handler: set any fields of the structure which are
            %passed in. 
            %Remove this if you don't want user to be able to set any field
            %of the obj structure 
            %*****************************
            if isfield(obj,varargin{j})
                obj.(varargin{j})=varargin{j+1};
                    %check if its required
            if isfield(req,varargin{j})
                req.(varargin{j})=1;
            end
            else          
            error('Constructor:inputarg','%s unrecognized parameter ',varargin{j});
            end
    end
end

%************************************************************
%Check if all required parameters were supplied
        freq=fieldnames(req);
        for k=1:length(freq)
            if (req.(freq{k})==0)
                error('Constructor:missing_arg','Required parameter %s not specified',freq{k});
            end
        end
%**********************************************************************

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
%pbase=PostUpdatersBase();
pbase.mname='BatchMLQuad2d';
pbase.compeig=0;                %don't compute eigenvectors
obj=class(obj,'BatchMLQuad2d');


    