% Explanation: Template function for dereferencing a user defined object
%
%   obj - pointer to object
%   index -
%           .type 
%           .subs
function [out] = subsref(obj,index)

% SUBSREF Define field name indexing for GLModel objects
switch index(1).type
    case '.'
        %for backwards compatibility with my old structure
        %allow user to specify data as obj.fname
        
        if (size(index,2)==1)
            %only 1 level of indexing is provided
            try
            out=obj.(index(1).subs);
            catch
                %try the base class
               out=obj.PostUpdatersBase.(index(1).subs);
            end
        else
            %call the indexing on the appropriate field
            fname=index(1).subs;
            %recursively process any further dereferencing
            out=subsref(obj.(fname),index(2:end));
        end
        
end