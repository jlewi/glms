%function [tanspace,tanpost]=comptanpost(obj,fpost,mobj)
%   obj - TanSpaceUpdater object
%   fpost - GaussPOst - represents the posterior on the full theta space
%   mobj  - A MTanSpace model object
%Return value
%   tanpoint - the location of the MAP projected onto the manifold
%   tanpost  - projeciton of fpost onto tanspace
%
%Explanation
%   Projects the posterior into the tangent space defined at the projection
%   of the MAP (mean of fpost) onto the submanifold.
%
%Revision: 
%   07-31-2008:
%       Use my new tangent space object model
%   
%   03-11-2008
%   The previous way of computing the posterior on the tangent space was
%   incorrect. I rewrote the code to do the correct thing
function [tanpoint,tanpost]= comptanpost(obj,fpost,mobj)


%compute the projection of the MAP of the full posterior onto
%the manifold
tanpoint=comptanpoint(mobj,getm(fpost));


%basis of the tangent space
basis=tanpoint.basis;
%the full covariance matrix
%cfull=getc(fpost);
cfullinv=getinvc(fpost);

%zero out variance in directions orthogonal to the tangent space. 
cinfo=inv(basis'*cfullinv*basis);


%posterior to use for infomax
minfo=-cinfo*basis'*cfullinv*(tanpoint.theta-getm(fpost));


tanpost=GaussPost('m',minfo,'c',cinfo);

%compute the entropy if desired
if (obj.comptanentropy)
    entropy=compentropy(obj,tanpost);
    tanpost=setentropy(tanpost,entropy);
end