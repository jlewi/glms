%function [post,]=updatesimpost(uobj,simobj,trial)
%       uobj - update object
%       sobj - Simulation object
%               get all data from this object
%       trial - latest trial
%             -i.e the time index of the data point that is being added to
%             the dataset
%
%Explanation: Extract the relevant parameters from simobj and then call
%update
function [post]=updatesimpost(uobj,simobj,trial)

mobj=getmobj(simobj);
postlast=getpost(simobj,trial-1);

if ~isa(postlast,'PostTanSpace')
   error('postlast must must be of type PostTanSpace');
   %get the full posterior from postlast
end

sr=getsr(simobj,trial);
stim=getinput(sr);
obsrv=getobsrv(sr);


%1. We update the full posterior using Newton 1d object
%   this just requires the most recent observation
unewt=Newton1d('optim',getoptim(uobj));

%get most recent stimuli and observations


%stim=projinp(mobj,getstim(simobj,trial),getsr(simobj,[max((trial-getshistlen(mobj)),1):trial-1]));
fpnew=update(unewt,getfullpost(postlast),getData(stim),mobj,obsrv);

tanold=gettanspace(postlast);
%2. Compute the tangent space at the mean
switch class(gettanspace(postlast))
    case 'PolyTanSpace'
        tanspace=PolyTanSpace('theta',getm(fpnew),'dimsparam',getdimsparam(tanold));        
    case 'SinTanSpace'
        tanspace=SinTanSpace('theta',getm(fpnew),'dimsparam',getdimsparam(tanold)); 
    case 'GaborTanSpace'
        tanspace=GaborTanSpace('theta',getm(fpnew),'dimsparam',getdimsparam(tanold)); 
    otherwise
        error('TanSpaceUpdater update has not implement update for tangent space of type %s \n', class(gettanspace(postlast)));
end

%project the posterior onto the tan space
%the projection of the covariance matrix is just the basis'*covart*basis
basis=getbasis(tanspace);
fptan=GaussPost('m',getsparam(tanspace),'c',basis'*getc(fpnew)*basis);

post=PostTanSpace('fullpost',fpnew,'tanpost',fptan,'tanspace',tanspace);