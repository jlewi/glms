%function sim=TanSpaceUpdater()
%
%Optional parameters
%   comptanpost - whether or not to compute the posterior on the tangent
%   space (i.e project mean of full posterior onto submanifold) and then
%   compute tangent space and project full posterior into the tangent
%   space.
%
%   comptanentropy - compute the entropy on the tangent space
%       comptanpost must be true
% Explanation: Updater when we represent the posterior on a tangent space.
%
%
% Revisions:
%   080730- Revised for new tangent space object model
%       Use Matlab's new OOP model
classdef (ConstructOnLoad=true) TanSpaceUpdater < PostUpdatersBase
    properties(SetAccess=private,GetAccess=public)
       
        comptanpost=true;
        comptanentropy=false;
    end

    methods
        function obj=TanSpaceUpdater(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};
            con(1).cfun=1;


       

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
                    params=[];
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);

            end

            %remove any paramters for this class
            fdel={'comptanpost','comptanentropy'};
            bparams=params;
            try
                for fname=fdel
                   if isfield(bparams,fname)
                    bparams=rmfield(bparams,fname);
                   end
                end
            catch
            end

            %construct the base class
            obj=obj@PostUpdatersBase(bparams);



            %optional parameters
            if isfield(params,'comptanpost')
                obj.comptanpost=params.comptanpost;
            end

            if isfield(params,'comptanentropy')
                obj.comptanentropy=params.comptanentropy;
            end

            
            obj.version.TanSpaceUpdater=090108;
        end
    end
end


