%function [anew,niter]=newtonsearch(obj, muproj,sigma,dumag,obsrv,glm,amin,amax)
%   muproj- projection of the old mean on the input
%   sigma - inp' C_t * inp/ ||C_t*inp||
%   dumag -||C_t*inp||
%   amin - minimum value of a using the linear constraints
%   amax - the maximum value of a using the linear constraints
%
%Explanation: solve the 1-d eqn numerically using a newton search
%
%Revision
%   10-30-2008:
%       Simplified the update to try to make it more stable
%       my newton search code is proving very unstable.
%       Therefore I switch to using fsolve to find the zero.
%   04-25-2008:
%       I added the check (abs(aerr)>10) && (abs(aerr)-abs(lastaerr))/abs(lasaerr) <.001
%       and do a binary step when this condition is met.
%       I added this because in my update I was still oscillating between
%       two values of anew and therefore terminating without converging
%       when maxiter was reached.
%
%       aerr wasn't changing but ferr was changing slightly during these
%       oscialltions. As a result, my existing checks were insufficient to
%       catch this oscillation and kick off a binary step.
%
%       This condition checks if the change in aerr is changing by some
%       small percent
%       I only evaluate this check when abs(aerr) is large because as aerr
%       gets small, i.e we start converging, the changes in aerr will be
%       small but we don't want to kick off a binary search.
%
%       If we reach the max number of iterations try a binary search.
%
%   11-18-2007: my calls to binarysearch weren't passing in the most recent
%   value of a. Thus they were always starting the binary search over at
%   a=0;
%       -include the number of binary search iterations in the iteration
%       count
function [anew,niter]=newtonsearch(obj,muproj,sigma,dumag,obsrv,glm,amin,amax)

%keep track of the number of times we do a binary search
nbinary=0;
%maximum number of failures for fsolve
maxfails=2;
nfails=0;
failed=true;

options=getoptim(obj);
aerr=inf;
ferr=inf;
aleft=-inf;
aright=inf;
niter=0;

%check if the root occurs outside the interval [amin, amax] if it does then
%we just set aopt to the boundary
if (stepsizefun(obj,amin,muproj,sigma,dumag,obsrv,glm)>0)
    anew=amin;
    fprintf('newtonsearch: anew=amax\n');
    return;
end
if (stepsizefun(obj,amax,muproj,sigma,dumag,obsrv,glm)<0)
    anew=amax;
        fprintf('newtonsearch: anew=amin\n');
    return;
end

aleft=amin;
aright=amax;

%aleft,aright maintain an interval in which a must lie
%if our newtson step takes outside this interval then we take a binary step
%[aleft,aright]=binaryinit(obj,muproj,sigma,dumag,obsrv,glm);

%start by doing a binary search until aleft and aright differ by at most 1
abinitdiff=10^-1;
[ainit,aleft,aright,ferr]=binarysearch(obj,muproj,sigma,dumag,obsrv,glm,abinitdiff,aleft,aright);


%now use fsolve to find the correct value
while((failed) && (nfails<maxfails))
    [anew,ferr,exitflag]=fsolve(@(x)(stepsizefun(obj,x,muproj,sigma,dumag,obsrv,glm)),ainit,options);
    switch exitflag
        case {1,2,3}
            %successful
            failed=false;

        case {0,-2,-3,-4}
            if ((exitflag==-3) && abs(ferr)<options.TolFun)
                failed=false;
            else
            fprintf('Fsolve could not converge to the correct stepsize. Try binary search\n');

            [ainit,aleft,aright,ferr]=binarysearch(obj,muproj,sigma,dumag,obsrv,glm,options.TolX,aleft,aright);
            end
    end
    nfails=nfails+1;
        
end

if (failed)    
    error('Fsolve could not converge to the correct stepsize. ');
end

if ~isreal(anew)
    error('anew is not real.');
end

if (anew<amin)
    fprintf('newtonsearch: anew exceeds amin \n');
    anew=amin;
end

if (anew>amax)
    fprintf('newtonsearch: anew exceeds amax \n');
    anew=amax;
end
% if (abs(ferr)>options.TolFun)
%     %For the canonical poisson and obsrv=0.  Its quite likely that ferr
%     %will violate our conditions. Thats because, for the canonical Poisson
%     %if r=0, the maximum likelihood estimate for inp'theta is undefined.
%     %However, as we collect data, our observations for r not equal to 0
%     %should properly constrain theta
% %     if (obsrv==0 && isa(glm,'GLMPoisson') && iscanon(glm))
% %         warning('The search for the proper step size failed. But I don''t think that matters because obsrv=0.');
% %     else
%     error('The search for the proper step size failed to converge. Try increasing the number of iterations.');
%     %end
% end
