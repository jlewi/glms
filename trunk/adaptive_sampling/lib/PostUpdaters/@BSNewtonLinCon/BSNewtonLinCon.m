%function sim=ClassName('rmax','windexes')
%   rmax - the maximum firing rate
%   windexes - which wave files to use to establish the linear constraint
%            - step size is chosen so that for the new theta, the expected
%            firing rate for any of the stimuli in these wave files never
%            exceeds rmax
%
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: This class implements the Newton updater but imposes a
% series of linear constraints which ensures that theta is never so large
% as to exceed some constraint on the firing rate
%
%
%Revisions:
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) BSNewtonLinCon < Newton1d
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        vbsnewtonlincon=081104;

        rmax=[];
        windexes=[];
    end

    %transient variables used to compute the linear constraints 
    %Note: If you try to update two different models with the same Newton
    %Object you will have problems because these values won't be reset
    properties(SetAccess=private,GetAccess=public,Transient)        
       bpost=[]; 
       muprojmax=[];
    end
    methods
        function obj=BSNewtonLinCon(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'rmax','windexes'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                case 1
                    bparams=rmfield(params,{'rmax','windexes'});
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@Newton1d(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     obj.windexes=params.windexes;
                     obj.rmax=params.rmax;
                     if (obj.rmax<=0)
                         error('rmax should be >0');
                     end
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


        end
    end
end


