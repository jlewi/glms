%function bpost=createbspost(uobj,param,mclass)
%   param
%       .bdata
%       .windexes (optional)
%Explanation: create the appropriate bpost object for the model
%
function bpost=createbspost(uobj,param,mclass)

                switch lower (mclass)
                case 'mparamobj'
                    bpost=BSlogpost(param);
                case {'mbsftsep','modbssinewaves'}
                    bpost=BSlogpostlin(param);                    
                otherwise
                    error('Unrecognized value for mclass');
            end
                    