%function stepsizefun(a,muproj,sigma,dmumag,obsrv,glm)
%   muproj- projection of the old mean on the input
%   sigma - inp' C_t * inp/ ||C_t*inp||
%   dumag -||C_t*inp||
%Explanation: This function is the function whose root gives the size
%  of the step we need to take in order to update the mean.
%
%Our objective function is written so that its always increasing with a
%This makes it easy to determine an appropriate interval on which the
%interval must be contained
function [f,varargout]=stepsizefun(obj,dt,muproj,sigma,dumag,obsrv,glm)

unew=muproj+dt*sigma/dumag;
%special cases
if (isa(glm,'GLMPoisson') && iscanon(glm))

    f=dt-(obsrv-exp(muproj+dt*sigma/dumag))*dumag;
    if (nargout==2)
       dfdt=1+exp(muproj+dt*sigma/dumag)*sigma;
       varargout{1}=dfdt;
    end

    %these rewrites don't really work because its not well defined for all
    %dt (i.e dt=0)
%     if (obsrv>0)
%         f=dt-(obsrv/exp(dt*sigma/dumag)-exp(muproj))*exp(dt*sigma/dumag)*dumag;
%         
%         if (nargout==2)
%             
%            dfdt=1-(-obsrv*sigma*dumag/exp(dt*sigma/dumag)-exp(muproj))*exp(dt*sigma/dumag)*dumag;
%            dfdt=dfdt-(obsrv/exp(dt*sigma/dumag)-exp(muproj))*sigma/dumag*exp(dt*sigma/dumag);
%            varargout{1}=dfdt;
%         end
%     else
%         %if obsrv =0 convert it into the log domain
%         %dt<0
%         %we choose the sign of this function so that 
%         %f is still increasing with dt
%         f=-log(-dt)+dt*sigma/dumag+muproj+log(dumag);
%         
%         if (nargout==2)
%            dfdt=1/(-dt)+sigma/dumag;
%            varargout{1}=dfdt;
%         end
%     end

else

    if (nargout==2)
        [gd1, gd2]=d2glmeps(glm,unew,obsrv);
    else
        [gd1]=d2glmeps(glm,unew,obsrv);
    end

    %non specific for u
    %la is the derivative of the log of the posterior as a function of a
    f=dt-dumag*gd1;

    if nargout==2
        %return the derivative with respect to a
        varargout{1}=1-dumag*gd2*sigma;
    end
end