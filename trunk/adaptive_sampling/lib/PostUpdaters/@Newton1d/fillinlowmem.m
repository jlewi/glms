%funciton simobj=fillinlowmem(nobj, pdata,mobj,sr,trials)
%   newton1d - object
%   simobj - simulation object containing the data
%   trials - trials on which to compute the covariance matrix
%            -leave blank to compute it for all trials.
%
%Explanation: This computes all covariance matrices for 1:max(Trials)
%      we compute all covariance matrices because we need them all to
%      compute the updated covariance matrix. 
%
%12-27-2007
%   updated for new object model
%5-28-2007
%   made it function of Newton1d
%4-18-2007
%   modified it so it computes the covariance matrix without refinding the
%   peak of the posterior
%   turned it into a function
%
%   got rid of loop over trials and simvars.
%2-16-2007
%   modified it so that I can use files in old format using the new object
%   oriented model.
%10_27_06
%This is a script to compute missing covariance matrices for the newton1d update.
%I.e find the posterior covariance matrices which we did not save
%clear all;
%setpathvars;
function [simobj]=fillinlowmem(nobj, simobj,trials)

if ~exist('trials','var')
    trials=[];
end

if isempty(trials)
    trials=1:getniter(simobj);
end

%determine which trials we have the posteior for allready
%thave post does not include the posterior
%thavepost =1 we have the posterior 
%thavepost =0 we do not have the posterior
thavepost=zeros(1,max(trials));
for ind=1:max(trials)
    if ~isempty(getpostc(simobj,ind))
        thavepost(ind)=1;
    end
end

glm=getglm(getmobj(simobj));

%plast will store the posterior from previous iteration
plast=[];
tlast=0;    %trial for which plast represents the posterior
for trial=trials;       
        if (mod(trial,10)==0)
            fprintf('Trial = %d \n',trial);
        end
        %find most recent trial for which we have post
        tlast=find(thavepost(1:trial)==1);
        if isempty(tlast)
           %set it to the prior
            tlast=0;
        else
            tlast=tlast(end);
        end
        
        if (tlast==trial)
            %we already have this trial
            %make sure covariance matrix doesn't exist for this trial.
           fprintf('Skipping trial covariance already exists  for trial %d \n ',trial);
        else
            %get the posterior of trial
            plast=getpost(simobj,tlast);
           
            %to compute the covariance matrix we need to compute all
            %covariance matrices upto time trial
            for tup=(tlast+1):trial
                sr=getsr(simobj,tup);
                x=getData(getInput(sr));
                obsrv=getObsrv(sr);
                
                
                %use gu-eisenstat code to compute the rank 1 update                
               [enew]=compceig(nobj,plast,getpostm(simobj,tup),x,obsrv,glm);
               % c=getc(plast)-getc(plast)*x*(-gd2/(1-gd2*x'*getc(plast)*x))*x'*getc(plast.c;
                
               %update plast
               tlast=tup;
               plast=getpost(simobj,tlast);
               plast=setc(plast,enew);
            end
            
            %set the posterior in simobj
            simobj=setpost(simobj,trial,plast);
            thavepost(trial)=1;
                        
        end
    end
end

