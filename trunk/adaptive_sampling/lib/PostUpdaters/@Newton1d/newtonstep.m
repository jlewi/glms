%function newtonstep(obj,a,muproj,sigma,dumag,,obsrv,glm)
%   muproj- projection of the old mean on the input
%   sigma - inp' C_t * inp/ ||C_t*inp||
%   dumag -||C_t*inp||
%Explanation: Takes a newton step for solving the 1-d eqn that we need to
%solve to determine the new mean).
%
%Return value:
%   anew - the new value of the gain term
%   aerr - the change in a
%   ferr - the value of the function whose root we are trying to find
function [anew,aerr,ferr]=newtonstep(obj,a,muproj,sigma,dumag,obsrv,glm)

    
    [ferr, df]=stepsizefun(obj,a,muproj,sigma,dumag,obsrv,glm);
    anew=a-1/df*ferr;
    
    if isa(anew,'mp')
        if (anew>realmax || anew <realmin)                        
           error('anew needs to be a multi-precision number. This most likely indicates numerical instability.');
        else
            anew=double(anew);
        end
    end
    
     aerr=abs(anew-a);
