%function compceig(uobj,postlast,mnew,inp,obsrv)
%   uobj - Newton1d object
%   postlast -posterior at previous trial
%   mnew - the new mean
%   inp  - the input
%   obsrv - the observation
%   glm - glm
%   statusfid - fid of file to print status information to
%Explanation: This function use gu-eisenstat code to update the
%eigendecomposition of the new covariance matrix
%   The code was copied from Newton1d/update);
function [einfo oinfo]=compceig(uobj,postlast,mnew,inp,obsrv,glm,statusfid)
if ~exist('statusfid','var')
    statusfid=[];
end
if isempty(statusfid)
    %default to standard output
    statusfid=1;
end

%******************************************************************
%Compute the Eigenvectors and the covariance matrix
%****************************************
%compute the covariance matrix from the eigendecomposition

%*******************************************************
%efficiently compute the eigenvectors
%*******************************************************

%can we compute the eigenvectors using the rank 1
[gd1, gd2]=d2glmeps(glm,mnew'*inp,obsrv);

%cfull=prior.c-rho*(prior.c*stipdata.timing.optimize(1,tr-1)=searchtime+eigtime;m)*(stim'*prior.c);

%*******************************************************************
%Numerical Issue:
%*******************************************************************
%we need to comput [x r]'*C*[x r] in order to compute rho
%but for numerical reasons we don't want to use postlast.c
%b\c we use the woodbury lemma to compute postlast.c. The woodbury
%lemma is unstable. Therefore we should use the eigendecomposition.
%so we should use the eigendecomposition instead
%
%
%z=evecs'*xmax;
%sigma=z'*(eigd.*z);
sigma=compquad(postlast,inp);
rho=gd2/(1-gd2*sigma);


%*************************************************************
%compute the eigendecomposition of the full covariance matrix
%************************************************************
u=rotatebyc(postlast,inp);


einfolast=getefull(postlast);
[einfo,oinfo]=rankOneEigUpdate(einfolast,u,rho);


if any(einfo.eigd<0)
    warning('eigenvalues produced by Gu-Eisenstat are not positive');
    fprintf(statusfid,'Warning: Eigenvalues produced by Gu-Eisenstat are not postive. Will try to use SVD instead.');

    %try using the SVD
    einfo=EigObj('matrix',getc(postlast)+rho*u*u');

    if (any(einfo.eigd<0))
        fprintf(statusfid,'Error: Eigenvalues of new covariance matrix were negative.');
        error('eigenvalues of new covariance matrix produced by SVD are not positive');
    end
end
