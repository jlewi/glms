%function sim=BSNewtonBatchInit('ninit', 'online','batch,)
%   ninit - how many trials to use batch initialization for on the initial
%         trials
%   online - the Newton1d online updater object
%   batch  - the batch updater method
%
% Explanation: Starts off by using batch methods to estimate the posterior
% and then switches to Newton1d.
%
classdef (ConstructOnLoad=true) BSNewtonBatchInit <PostUpdatersBase
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        version=081003;
        ninit=[];
        
        %the batch updater
        bupdater=[];
        
        %the newton updater
        nupdater=[];
        
    end

    properties(SetAccess=private,GetAccess=public,Transient)
        %inputs is an array which will store the inputs on the first ninit
        %trials. We do this so that we can speed up the batch updates on
        %the first steps.
        inputs=[];
        
        %obsrv - array of the observations on the initiali ninit trials
        obsrv=[];
    end
    methods
        function obj=BSNewtonBatchInit(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.        
            con(1).rparams={'ninit','batch','online'};

                    [cind,params]=Constructor.id(varargin,con);
           

            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('Did not match a constructor');
                case 1
                    bparams=struct();
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@PostUpdatersBase(bparams);

            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case 1
                    obj.ninit=params.ninit;
                    obj.bupdater=params.batch;
                    obj.nupdater=params.online;
                otherwise
                    error('Constructor not implemented')
            end



        end
    end
end


