%function sim=ClassName(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object 
%
% Explanation: Template for the constructor of a new class. This template
% shows how we can identify which constructor was called based on the
% parameters that were passed in.

function obj=LRMixtureUpdate(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'nmodels'};
con(1).cfun=1;


%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%  nmodels - how many models to use in the mixture
%declare the structure
obj=struct('version',071116,'nmodels',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        %set this to false or it will cause problems.
        params.compeig=false;
        obj=class(obj,'LRMixtureUpdate',PostUpdatersBase(params));
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
    
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one

params.compeig=false;
%if no base object
obj=class(obj,'LRMixtureUpdate',PostUpdatersBase(params));




    