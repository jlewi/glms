%function [post,]=update(uobj,postlast,stim,mobj,obsrv,sobj)
%       postlast - the posterior (an LRMixturePost object) after the previous observation
%       stim - full stimulus - spike history and stimulus part
%           - also stimulus after any input transformations
%       obsrv- observation
%
%       mobj - model object
%       sobj - Simulation object
%               get all data from this object
%       trial - latest trial
%             -i.e the time index of the data point that is being added to
%             the dataset
function [post]=update(uobj,postlast,stim,mobj,allobsrv,simobj,trial)

mobj=getmobj(simobj);

if ~isa(postlast,'LRMixturePost')
   error('postlast must must be of type LRMixturePost');
   %get the full posterior from postlast
end

sr=getsr(simobj,1:trial);

%1. We update the full posterior using Newton 1d object
%   this just requires the most recent observation
unewt=Newton1d('optim',getoptim(uobj));

%stim=projinp(mobj,getstim(simobj,trial),getsr(simobj,[max((trial-getshistlen(mobj)),1):trial-1]));

%get most recent stimuli and observations
stim=getinput(sr(trial));
obsrv=getobsrv(sr(trial));
fpnew=update(unewt,getfpost(postlast),getData(stim),mobj,obsrv);

%2. Build the mixture model
%   this requires all stimuli and observations to date
stim=getData(getinput(sr));
obsrv=getobsrv(sr);
post=LRMixturePost('nmodels',getnmodels(postlast),'fpost',fpnew,'stim',getinput(sr),'obsrv',getobsrv(sr),'mobj',mobj);
