%function [mu]=postgradasc(thetaest,po,x,obsrv,glm)
%       thetaest - mean of theta from prior time step
%                   this is used as initial point for gradient ascent
%       po - structure representing the gaussian prior at time 0
%              .m - mean
%              .c - covariance matrix
%       x     - column vector representing the stimulus
%               1xt 
%               -observations on each trial upto this time point
%       f    - pointer to the nonlinear function and its derivatives which computes
%               the rate as a fcn of u=k'x
%       obsrv - struture representing the observations
%       
%       glmcf -  structure 
%               .fglmnc - pointer to function which computes the
%                               normalizing value as a function of the canonical parameter
%               .fglmmu  -pointer to function which computes the canonical
%                                    parameter as a function of the input
%                               input - x^t\theta
%                               output - mean
%               .fglmetamu - computes the canonical parameter as function
%                                      of mean
%                               input - mean 
%                               output -canonical parameter
%               -both functions should compute first and 2nd derivatives as
%               well. 
%Return value:
%   mu - peak of posterior

%
%Explanation:
%   Finds the maximum of the posterior using gradient ascent
%
%   Explanation: In general its expected you would call this from another
%   function i.e allobsrvupdate
%
%Revision History:
%$Revision$: 06-14-2007
%              
%
%Revision
%   11-03-2008:
%       use fminunc instead of fsolve, because with bird song data
%       we can often have trouble finding the root but fminunc works pretty
%       
%           
%06-14-07: Added checking of fsolve output
%1-29-07 - added the parameter optim
%10-10-06 - Generalized this so it works with generalized linear models
function [mu]=postgradasc(thetaest,po,x,obsrv,glm,optim)

error('this function is outdated. Generally use maxlpost to maximize the log posterior.');
%use matlab's gradient numerical solver
%fsolve to find the location of the maximum by solving for location where
%derivative=0
optim=optimset(optim,'Jacobian','on');
optim=optimset(optim,'Display','off');
optim=optimset(optim,'TolX',10^-10);
optim=optimset(optim,'TolFun',10^-16);
optim=optimset(optim,'MaxIter',10000000);
if isempty(optimget(optim,'MaxFunEvals'))
    optim.MaxFunEvals=1000;
end
%location of zeros of derivative
%initialization: we don't want dglmeps to be to large in magnitude
% glmproj=thetaest'*x;
% [gpmax gpind]=max(glmproj);
% sf=1/gpmax;
% thetaest=sf*thetaest;
[mu fv exitflag,fsout]= fsolve(@(theta)(d2glm(glm,theta,po,obsrv,x)),thetaest,optim);

%make sure we converged
%exitflag==1 it converged
switch (exitflag)
    case {-3,0}
        %potentially try changing initialization point
    error('postgradasc: fsolve did not converge after %2.2g Iterations. Exited with message: \n %s',fsout.iterations,fsout.message);
    case {1,2,3}
        %do nothing it terminated ok
    otherwise
    %fprintf('grad: fsolve terminated with message \n %s \n',fsout.message);
end

%error checking
%check derivative is zero
if ~isempty(find(abs(fv)>10^-5))
    warning('Derivative at MAP is not zero');
end

