%function [post, ]=update(uobj,postlast,stim,mparam,obsrv)
%       postlast - the most poster after the previous observation
%       stim - full stimulus - spike history and stimulus part
%               should be mx n array
%                       m=klength+alength
%                       n = number of observations
%       obsrv- observation
%
% Return value
%   post - the new posterior
%
%$Revision$: use the optim object stored in the base class
%
%01-11-2009
%   - get past inputs from simobj
%07-07-2008
%   Modified it to work with my new GLMModel objects
%
%03-18-2007
%       1. GLM is no longer passed in as input
%       2. Test if mparam is derived from MGLMNonLinInp
%                   if so transform the input
function [post]=update(uobj,postlast,inp,mobj,obsrv,simobj,trial)


glm=getglm(mobj);

%get all inputs and observations
[inp,obsrv]=getpastdata(uobj,simobj,trial);

%check if we need to nonlinearly transform the input
%**************************************************************
%special processing for poisson-2-14-07
%********************************************************
%Special processing is needed to deal with nonlinearities
%for which f(u)=0 for u<0
if (isa(glm,'GLMPoisson') && (glm.fglmonesided==1))
    %if the nonlinearity is one sided (that is fglmmu=0 for
    %\glmproj <0)
    %then for any observations for which r>0
    %we need theta'*stim> 0
    %otherwise we have problems trying to do gradient ascent
    %because gradient is flat.
    %fprintf('Need to check distribution is one sided\n');

    %find any points for which
    ind=find((postlast.m'*stim<0) & (obsrv>0));
    stimmag=sum(stim(:,ind).^2,1).^.5;
    %set projection of postlast.m along these stimuli to be
    %slightly positive.
    %The derivative should be well defined in this case
    %provided the model is well specified
    %I'm not sure however, that as we do gradient ascent we're
    %guaranteed that this condition remains true.
    %doesn't handle spike history
    postlast.m=postlast.m-sum(ones(mparam.klength,1)*(-.0001+postlast.m'*normmag(stim(:,ind))).*normmag(stim(:,ind)),2);
    %check it worked
    ind=find((postlast.m'*stim<0) & (obsrv>0));
    if ~isempty(ind)
        error('Current estimate yields theta such that glmproj<0 for observations>0');
    end
end
%compute the peak of the posterior using all observations
%we initialize gradient ascent with the peak from the previous
thetainit=getm(postlast);
prior=getpost(simobj.allpost,0);


if isdistributed(getc(prior))
    prior=GaussPost('m',getm(prior),'c',gather(getc(prior)));
end
[mnew,dlp2dt]=maxlpost(uobj,thetainit,prior,inp,obsrv,glm);


%error checking


%compute the eigendecomposition
%we use this to compute the entropy and the inverse
%            [edlp2.evecs eldp2.eigd]=svd(-dlp2dt);

cnew=-inv(dlp2dt);
%post.invc=-dlp2dt;
post=GaussPost('m',mnew,'c',cnew);