%function sim=BSUpdateBatchReplay('oldsimfile', fname)
%   fname - the name of the old simulation file from which we want to
%   replay the data. 
%
% Explanation: This class is used for diagnosing problems with the updates.
% It allows us to do batch updates but process the data in the order it was
% actually selected whereas the other bird song batch estimators require
% the data to be selected in the order it was actually presented so we can
% use convolution
%
%
%Revisions:
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) BSUpdateBatchReplay<BatchML
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        ver=struct('BSUpdateBatchReplay',081125);
        
        %this is the name of the file containing the simulation from which
        %we loaded paststim
        oldsimfile=[];
    end

   properties(SetAccess=private, GetAccess=public, Transient)
        %the old simulation
        oldsim=[];
        
        %the actual inputs
        inputs=[];
        
        %the actual observations
        obsrv=[];
        
        
       
    end
    methods
        function oldsim=get.oldsim(obj)
           if isempty(obj.oldsim)
              v=load(getpath(obj.oldsimfile));
              obj.oldsim=v.bssimobj;
           end
           oldsim=obj.oldsim;
        end
        function obj=BSUpdateBatchReplay(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'oldsimfile'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=rmfield(params,'oldsimfile');
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@BatchML(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     obj.oldsimfile=params.oldsimfile;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


        end
    end
end


