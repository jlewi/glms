%function [pekf]=post1stepgrad(po,post,x,obsrv,fu,maxdm)
%       po - structure representing the gaussian prior at time 0
%              .m - mean
%              .c - covariance matrix
%              .invc - inverse of c. This gets reused so its better if you
%              provide it so we don't have to recalculate it, it only needs
%              to be calculated once.
%       post - structure representing the gaussian posterior from the   
%              previous time step or newton step
%               post.m is point at which we take a newton step from
%               post.c determines how large a step to take
%                       -this is the covariance matrix which is taken to be
%                       the negative inverse of the hessian.
%               .invc - this is the inverse of post.c
%                       -if this exists then we don't need to compute it
%       x     - column vector representing the stimulus
%               dimstimxt 
%               -observations on each trial upto this time point
%       obsrv - struture representing the observations
%              .n - number of spikes
%       `           1xt - each column represents the number of spikes on a
%                   different trial
%                   the observations are all observations upto this time
%                   point
%              .twindow -window in which spikes are occured
%       fu    - pointer to the nonlinear function and its derivatives which computes
%               the rate as a fcn of u=k'x
%       maxdm - this is the max amount the posterior mean can change along
%               any dimension in a single update. This is meant to prevent
%               instability due to a horizontal asymptote.
%      
%Return value:
%   pekf - gaussian representing the updated posterior
%       .m - mean
%       .c - covariance
%       .invc - inverse of c - we return this becaue we already compute it
%               and if we do multiple newton steps we will need it on the
%               next iteration.
%       .dlpdt - derivative of the log posterior at post.m
%                return this so we don't have to recompute it on next
%                iteration
%Stability Issues:
%   In rare cases newton's method can blow up due to horizontal asymptotes.
%   See my notes 1-27-2006. To prevent this I limit the amount the mean can
%   change on an interation using maxdm. This prevents a single horizontal
%   asymptote from causing it to blow up to infinity.
%
%Explanation:
%   Implements a single iteration of Newton's method for the purpose of
%   estimating the posterior mean
%   Computes the exact gradient of the posterior at prior.m
%   and uses this gradient to update the mean
%   as 
%   Assumes response is homogenous during window in which spikes observed
%
%
%OPtimization ideas:
%   can we do anything about computing inverse of hessian to get covariance
%   matrix?
%   
% dlpdt and dlp2dt should be saved from previous iterations so we don't
% have to recompute them
%
% Revision history:
%   4-08-2006: Optimize it to use po.invc and to return pekf.invc
%              by returning this information we automatically have it
%              availble for our next iteration if we do multiple newton
%              steps
%   3-30-2006: I modified it so that if x is a single observation
%               then we a rank 1 update. So we can compute the inverse
%               efficently using the matrix inversion lemma
function [pekf]=post1stepgrad(po,post,x,obsrv,fu,maxdm)

if ~exist('maxdm', 'var')
    %maxdm=1;
    maxdm=.2;
end


%**************************************************************************
%Rank 1 Update: use matrix inversion lemma
%**************************************************************************
if (size(x,2)==1)
    %this is basically the ekf rank 1 update
    %except we iterate it. 
    %get the derivatives of fu. and the expected rate
%r0, r1, and r2 are the rate and its 1st and 2nd derivtives
%that is r0 is the average number of spikes per unit time.
%it is the evluation of the nonlinear function in the glm
[r0,r1,r2]=fu(post.m'*x);

%We need to scale the terms by obsrv.twindow
%to get the expected number of spikes in the observed window
twin=obsrv.twindow;
r0s=r0*obsrv.twindow;
r1s=r1*obsrv.twindow;
r2s=r2*obsrv.twindow;

%g is the log likelihood up to a scaling factor
g=-r0s+ obsrv.n*log(r0s);
gd1=-r1s+obsrv.n*1/(r0s)*r1s;
gd2=-r2s+obsrv.n*(-1/(r0s^2)*(r1s)^2+1/r0s*r2s);


    %compute the first derivative
    if isempty(po.invc)
       %if inverse of covariance matrix is not supplied we have to compute it.
       %warning('Inverse was not previously computed');
        po.invc=inv(po.c);
    end

    %1st derivative of prior
    dprior=-(post.m-po.m)'*po.invc;
       
    %1st derivative of log likelihood
    dll=gd1*x';
    %gradient of the log likelihood is sum of the gradients
    %will be row vector
    dlpdt=dll+dprior;
    
    %compute lambda the inverse of the covariance matrix of the 
    %gaussian approximating the log likelihood function (this will be singular)
    lambda=-gd2*x*x';
    %now we compute the second derivative of at our current estimate of the
    %mean (po.m)
    %pekf.c= -(dl2prior+dll2)^-2
    %   dl2prior=2nd derivative of prior  (po is our prior)
    %   dll2    = 2nd derivative of log likelihood 
    %   dl2prior = -(Co^-1)  ;  Co covariance of prior
    %   dll2 = -(Cll^-1); covariance of log likelihood
    %   pekf.c=(Co^-1+Cll^1)^-1
    % this is a rank one update (Cll is rank 1) therefore we use the
    % woodbury matrix inversion lemma to compute it
    pekf.c=po.c-po.c*x*(-gd2/(1-gd2*x'*po.c*x))*x'*po.c;
    
    
    %compute the inverse of the 2nd derivative  dl2inv
    %this would be the negative of the covariance matrix
    %dl2inv=-(po.c-po.c*x*(-gd2/(1-gd2*x'*po.c*x))*x'*po.c);

    %now use dl2inv to take step
    %dm=- (dlp2dt*dlpdt');
    
    %now compute the size of the newton step
    % this is (dl2inv) *dlp 
    %which we work out to be
    %dm=-po.c*x*(-gd2/(1-gd2*x'*po.c*x))*x'*post.m+pekf.c*(gd1*x+lambda*post.m);
    dm=pekf.c*dlpdt';
    
    %don't allow the absolute value of the change along any dimension to exceed the value of max dm
    %this prevents a horizontal asymptote (2nd derivative near zero) from
    %causing it to blow up
    ind=find(abs(dm)>maxdm);
    if ~isempty(ind)
           dm;
        %keyboard;
            warning('Max step size exceed in iteration of newtons method');
            dm(ind)=sign(dm(ind))*maxdm;
    end
    
    %update the mean
   pekf.m=post.m+dm;
   
   %***********************************************************************
   %now we recompute the covariance matrix at this location.
   [r0,r1,r2]=fu(pekf.m'*x);

    %We need to scale the terms by obsrv.twindow
    %to get the expected number of spikes in the observed window
    twin=obsrv.twindow;
    r0s=r0*obsrv.twindow;
    r1s=r1*obsrv.twindow;
    r2s=r2*obsrv.twindow;

    %g is the log likelihood up to a scaling factor
    g=-r0s+ obsrv.n*log(r0s);
    gd1=-r1s+obsrv.n*1/(r0s)*r1s;
    gd2=-r2s+obsrv.n*(-1/(r0s^2)*(r1s)^2+1/r0s*r2s);

     %now compute the new covariance matrix at this location 
   %using the woodbury lemma;
   pekf.c=po.c-po.c*x*(-gd2/(1-gd2*x'*po.c*x))*x'*po.c;

    %set the derivative of the log posterior and the hessian of the
    %logposterior to empty matrices because we didn't compute them
    %(although we probably could).
    pekf.dlpdt=[];
    pekfl.invc=[];
    
else
%******************************************************************************************
%Non rank 1 update of the posterior
%**********************************************************************
%pekf.m=post.m+.6*post.c*grad;
obsrv.nspikes=obsrv.n;

%if the inverse of the covariance matrix of the posterior
%matrix is supplied then assume this is the negative of the hessian
%and avoid recomputing the hessian
dlp2st=[];
if ~isempty(post.invc)
    dlp2dt=-post.invc;

    %was the 1st derivative also supplied?
%if the derivative of the log posterior was not supplied we
%need to compute it. If this is the second newton iteration
%the we should already have it from our last iteration.
%If its our first iteration our starting point for newton's method
%should be the same as the final posterior at t-1. In which case we should
%also have it available. However this will not be the case if diffusion
%or some other process has changed the posterior covariance
if ~isempty(post.dlpdt)
    dlpdt=post.dlpdt;
else
    %compute just the first derivative of the posterior
    [dlpdt]=d2posterior(post.m,po,obsrv,x,fu);
end
else
    %compute the first and second derivatives of the true posterior
    [dlpdt,dlp2dt]=d2posterior(post.m,po,obsrv,x,fu);
end


%Compute Step: what is the change in the mean -
dm=- (dlp2dt\dlpdt');
%don't allow the absolute value of the change along any dimension to exceed the value of max dm
%this prevents a horizontal asymptote (2nd derivative near zero) from
%causing it to blow up
ind=find(abs(dm)>maxdm);
if ~isempty(ind)
dm
%keyboard;
    warning('Max step size exceed in iteration of newtons method');
    dm(ind)=sign(dm(ind))*maxdm;
end
pekf.m=post.m+dm;

%now we recompute the second and 1st derivative derivative at the new location
obsrv.nspikes=obsrv.n;
[dlpdt,dlp2dt]=d2posterior(pekf.m,po,obsrv,x,fu);


if (sum(imag(pekf.m))~=0)
    warning('Updated mean is not real');
end
%the new covariance matrix should be the negative inverse of the hessian
%but if the hessian is singular we can't invert this will happen
%if we have fewer observations than dimensions of theta because
%our likelihood is one dimensional. in this case we update using
%the ekf based rule to compute the posterior c
%use rcond to test for singularity
if (rcond(dlp2dt)>10^-17)
    %save the inverse of the covariance matrix
    %so if we do a second iteration of newton's method we don't have to
    %recompute ite
    pekf.c=-inv(dlp2dt);
    pekf.invc=-dlp2dt;
    
    %save the derivative of the log posterior aswell
    pekf.dlpdt=dlpdt;
    %4-08-2006
    %Optimizing performance I commented out the following check to avoid
    %doing the eigendecomposition    
    %eigval=eig(pekf.c);    
    %if ~isempty(find(eigval<0))
    %    warning('Covariance matrix no longer positive definite');
    %end
else
    %error('hessian illconditioned');
    fprintf('Post1stepgrad.m: hessian nearly singular. Updating covariance using EKF rule \n');
    %don't update covariance
    pekf.c=post.c;
    return;
    pekf.c=po.c;
    
    for index=1:size(x,2)
        
        %get the derivatives of fu. and the expected rate
        %r0, r1, and r2 are the rate and its 1st and 2nd derivtives
        %that is r0 is the average number of spikes per unit time.
        %it is the evluation of the nonlinear function in the glm
        [r0,r1,r2]=fu(pekf.m'*x(:,index));

        %We need to scale the terms by obsrv.twindow
        %to get the expected number of spikes in the observed window
        twin=obsrv.twindow;
        r0s=r0*obsrv.twindow;
        r1s=r1*obsrv.twindow;
        r2s=r2*obsrv.twindow;
    
        %g is the log likelihood up to a scaling factor
        g=-r0s+ obsrv.n*log(r0s);
        gd1=-r1s+obsrv.n*1/(r0s)*r1s;
        gd2=-r2s+obsrv.n*(-1/(r0s^2)*(r1s)^2+1/r0s*r2s);
        %fprintf('One Step: gradient %d \n', grad);

        %should it be po.c in or pekf.c below
        pekf.c=pekf.c-po.c*x*(-gd2/(1-gd2*x'*po.c*x))*x'*po.c;
    end
end

end
