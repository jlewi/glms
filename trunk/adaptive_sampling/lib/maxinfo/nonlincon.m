%function nonlincon(x)
%       x - vector at which nonlinear constraints of maximization problem
%           are to be evaluated
%
%Explanation: Used by fmincon to compute the nonlinear constraints
function [c,ceq]=nonlincon(x)
    %no nonlinear inequality constraints
    c=[];
    
    %compute the equality constraint
    %this is that that magintude of x is less than 1
    %Matlab needs ceq=0
    %so we compute x'*x-1
    ceq=x'*x-1;