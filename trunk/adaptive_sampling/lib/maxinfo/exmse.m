%function err=exmse(pg,theta)
%       pg - gaussian for theta
%            -.m - can be matrix
%           -.c - can be cell array
%       theta - true theta
%           can be a single vector or array if theta is different on each
%           iteration
%           in which case it should have same number of cols as pg.m
%Explanation:
%   Computes the expected mean squared error for the gaussian distributed
%   theta and the true theta

function err=exmse(pall,thetaall)

dimx=size(pall.m,1);
ntrials=size(pall.m,2);

if ~(iscell(pall.c))
    pall.c=mat2cell(pall.c);
end

err=zeros(1,ntrials);

for index=1:ntrials
pg.m=pall.m(:,index);
pg.c=pall.c{index};

if (size(thetaall,2)>1)
    theta=thetaall(:,index);
end

%we compute the eigen vectors and values of the covariance matrix
[eigv,eigd] = eig(pg.c);

%project mean onto eigen vectors
u=eigv'*pg.m;

%compute sum of the eigen values 
seigvals=ones(1,dimx)*eigd*ones(dimx,1);

%expected value of magnitude of theta
emag=sum(u.^2)+seigvals;

%mean squared error
err(index)=emag-2*theta'*pg.m+theta'*theta;


end