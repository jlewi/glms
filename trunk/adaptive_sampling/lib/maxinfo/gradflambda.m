%function gradflambda(lambda1,v,c,mmag)
%   lambda1 - current value of lambda
%   v - mean of posterior expressed in basis using eigenvectors of
%        covariance matrix
%   c - eigenvalues of covariance matrix
%   mmag - the constraint on the magnitude of x
%         i.e ||x||^2=mmag^2
%
% Computes the gradient in terms 
%
%Return Value:
%   dfdl - derivative of F(lambda) w.r.t lambda
function [dfdl]=gradflambda(lambda,v,c,mmag)

%compute the relevant values at this point
y=v./(c-lambda);
Z=(y'*y)^.5/mmag;
y=1/Z*y;

g=exp(v'*y);

%use true h not simplification as C->0
e=sum(c.*y.^2);
h=exp(.5*e)*e;

%**********************************************************************
%now compute derivatives
%*********************************************************************

%dz/dlambda 
dzdl=(1/mmag^2*sum(v.^2./(c-lambda).^2))^-.5*(1/mmag^2*sum(v.^2./(c-lambda).^3));

%dyi/dl - this is column vector
dyidl=v.*(Z)^-1.*(c-lambda).^-2-(v*(Z^-2)).*(c-lambda).^-1*dzdl;

%dgdlamda
dgdl=g*v'*dyidl;

%dh/dlambda
dedl=sum(2*c.*y.*dyidl);
dhdl=e*exp(.5*e)*.5*dedl+exp(.5*e)*dedl;

%full derivative
%df/dl
dfdl=h*dgdl+g*dhdl;



