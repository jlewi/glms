%function [xopt,fopt,einfo]=fishermax(post,mmag,einfo,opts)
%   post - current posterior
%   mmmag constraint on stimulus
%   einfo - optional structure containing the eigenvectors and eigenvalues
%        of the covariance matrix. Avoids having to compute them
%        these need to be sorted in ASCENDING ORDER
%        .evecs
%       .eigd
%           -can pass empty array if values not known.
%       .esorted = +1 - sorted in ascending order
%                         -1 - sorted in descending order
%                         0  - not sorted
%   opts
%Explanation: Chooses the stimulus which maximizes the expected fisher information
%
%Return value
%  xopt - optimized stimulus
%  fopt - fisher info at optimal stimulus
%  einfo    - structure containing eigenvalues
%           and eigenvectors. We can use this
%           in our first newton iteration
%           to compute the inverse
%       .evecs - matrix of eigenvectos
%       .eigd   - vector of eigenvalues
%       .esorted - see above
%
% eigtime  - time to compute the eigenvalues
%               - is empty if  eigenvalues are supplied
% searchtime - time to perform the search
%
%10-14
%   changed eigvec to evecs
function [xopt,fopt,einfo,eigtime,searchtime]=fishermax(post,mmag,einfo,optparam)

n=length(post.m);
%**************************************************************************
%compute the eigenvectors if they are not supplied

if ~exist('einfo','var')
    einfo=[];
 
end
  
%start timing for eigendecomp
if ~isempty(einfo)
    eigtime=[];
    
    if ~isfield(einfo,'esorted')
        einfo.esorted=0;
        
    end
    %the eigenvectors/values need to be sorted in descending order
    if (einfo.esorted==1)
         %eigenvectors are in asending order
        %so reverse them
        einfo.eigd=einfo.eigd(end:-1:1);
        einfo.evecs=einfo.evecs(:,end:-1:1);
        einfo.esorted=-1;
    end
    
    if (einfo.esorted==0)
        %sort eigenvectors in descending order
        [einfo.eigd ind]=sort(einfo.eigd,1,'descend');
        einfo.evecs=einfo.evecs(:,ind);
        einfo.esorted=-1;
    end
else
     einfo=[];
    %svd returns eigenvalues sorted in descending order
    warning('Eigenvectors of covariance matrix were not supplied');
     tic;
    [einfo.evecs s]=svd(post.c);
    einfo.eigd=diag(s);
    eigtime=toc;
    einfo.esorted=-1;
    
end
    emean=einfo.evecs'*post.m;
    y=zeros(n,1);
  
%**************************************************************************
%we need to find the number of unique eigenvalues
%*************************************************************************
%do this computation here instead of inside yrescaled so that we don't have
%to recompute on each call to yrescaled
%threshold for saying to eigenvalues are distinct
threshold=eps^.5;
%structure to store information about distinct eigenvalues
%   .i   - index of first occurence in einfo.eigd
%   .m - multiplicity

tic
ndist=0;    %number of distinct eigenvalues
ii=1;
    while ii<=n-1
        j=ii+1;
        %this should handle multiplicities >1       
        while (abs((einfo.eigd(j)-einfo.eigd(ii)))<threshold)
            j=j+1;          
            if (j>n)
                break;
            end
              
        end
        ndist=ndist+1;
        edist(ndist).i=ii;
        %j-1= index of last element for which eig(i)=eig(j-1)
        %therefore multiplicity= (j-1)-i +1=j-i
        edist(ndist).m=j-ii;
        edist(ndist).eigd=einfo.eigd(ii);
        ii=j;
    end
    %handle last eigenvalue 
    %if ii==n then last eigenvalue is distinct otherwise its not and we
    %don't need to do anything
    if (ii==n)
       ndist=ndist+1;
        edist(ndist).i=n;       
      edist(ndist).m=1;
        edist(ndist).eigd=einfo.eigd(n);
    end
    
%now we need to compute emmag
for j=1:ndist
    %relevant components of m
    if (edist(j).m>1)
        mc=emean(edist(j).i:edist(j).i+edist(j).m-1);
        edist(j).emmag=(sum(mc.^2))^.5;
    else
        edist(j).emmag=abs(emean(edist(j).i));
    end
end


%Now we reparamterize our stimulus using eta coordinates.
%eta is (k,1) where k is the number of distinct eigenvalues
%we do this because the rescaling function assumes the eigenvalues are
%distinct it also simplifies the computation of the fisher information
%we also rexpress the mean in this space so we can compute the 
emmag=[edist.emmag]';
eigdist=[edist.eigd]';

    tic
    
        %create an anonymous function to get fisherinfo for a particular w
        %we multiply by negative 1 b\c fminbnd finds minimum
        fofw=@(w)(-finfoestim(yrescaled(w,eigdist, emmag,mmag),eigdist,emmag));
        %Find the optimal stimulus
        %we use the rescaled y of lambda and pass that to to finfoestim
        %in the rescaled verison
        %wopt should be between 0 and 1
        [wopt fopt exitflag output]=fminbnd(fofw,0,1,optparam);
        fprintf('Iterations=%d \t funccounts=%d \n',output.iterations,output.funcCount);
        %compute the optimal stimulus
        fcase=-1;   %fcase is now obsolete
        etaopt=yrescaled(wopt,eigdist, emmag,mmag);

        
        
        %now we have to compute xs from eta
        %loop over the distinct eigenvalue
        %eta(i) is the magnitude of the stimulus projected into the eigensapce
        %   of the ith distinct eigenvalue
        %   the projection should be proporitonal to the projection of the
        %   mean. We use this fact to determine the optimal stimulus
        for eind=1:length(eigdist)
            %loop over components of y for this eigenvalue
            for j=edist(eind).i:edist(eind).i+edist(eind).m-1
                %if mean is not othonormal to this eigenvalue we 
                %we distribute energy proportional to mean projected into
                %this eigenspace
                if ( emmag(eind)>threshold)
                yopt(j,:)=emean(j)/emmag(eind)*etaopt(eind);
                else
                    %mean is orthonormal so distrbute the energy evenly
                    %among the projections
                    yopt(j,:)=etaopt(eind)/(edist(eind).m)^.5;
                end
            end
        end
        
        xopt=einfo.evecs*yopt;
        fopt=finfoestim(yopt,einfo.eigd,emean);        
        
        searchtime=toc;
    