%function [finfo,y]=fleigmax(lambda,ysign,b,c,v,emax,mmag);
%       lambda - value of lambda to compute y at
%       ysign  - sign of lambda2 (what sign to give y)
%       b      - what percent of the energy of the stimulus we want to be
%               in the eigenspace of the max eigenvector
%       c   - just the eigenvalues for dimensions along which solution
%       varies
%       v   - just projection of mean along evectors it varies
%       emax - the largest eigenvector - used for computing fisher info
%       mmag - magnitude
%Explanation: This function is used by fishermax to compute the optimal
%stimulus in case 3, where the mean is perpindicular to the eigenspace of
%the max eigenvector but the mean is not parallel to any eigenvector.
%Therefore, we have to complete a 2-dimensional search for the optimium.
%fleigmax is used by fminbnd to compute the optimization of the inner
%problem.
%That is given the constraint b (that is b% of energy of optimal stimulus
%will lie in eigenspace of max eigenvector) what is the optimal stimulus.
%
%
% SIGN!!!! Since b is energy there are two possible signs for the
% correlation of v0*(b*mmag^2)^.5 (that is the correlation of the mean and the stim
% along the max eigenvector ) We assume sign is the same in order to
% maximize the correlation and fisherinfo
function [finfo,y]=fleigmax(lambda,ysign,b,c,v,emax,v0,mmag);

if isempty(lambda)
    keyboard
    finfo=0;
    y=10;
end

%compute direction of y using lambda in space orthogonal to max eigenvector
%if lambda=eigenvector we get singularity and set 
%y=eigenvector
denom=c-lambda;
zind=find(denom==0);
if ~(isempty(zind))
    y=zeros(length(v),1);
    y(zind)=mmag;
else
    y=v./(c-lambda);
end
%now scale y based on b
y=y*((1-b)*mmag^2)^.5/(y'*y)^.5;
y=sign(ysign)*y;
%z=(y'*y)^.5/mmag;
%y=1/z*y;


%when computing v0*(b*mmag^2)^.5
%we take abs of v0 because the component of the stim along the max
%eigenvector could have either + or - sign
%we want sign to be same as v0 in order to maximize correlation
finfo=exp(.5*emax*b*mmag^2)*exp(y'*v+abs(v0)*(b*mmag^2)^.5+.5*y'*diag(c)*y)*(emax*b*mmag^2+y'*diag(c)*y);
end
%f=(1-b-sum(v1.^2./(eval2-ltest).^2));
%df=-2*sum(v1.^2./(eval2-ltest).^3);