 %function [pmethods,sr,simparam]=updateposteriors(modparam,simparam,methods,ropts)
%
%       mparam - model parameters governing model to use
%           .tlength - length of input vectors = length of filter
%           .ktrue   - value of the true filter
%           .tresponse - length of response window in which we
%                        count the number of spikes after presenting the
%                        stimulus
%            .mparam.dk       -step size used for discrete representations of
%                        posteriors
%                        -this is used for some analysis and for brute
%                        force updates
%            .mparam.krange   - range for k when doing discrete representations
%            .pinit    - initial gaussian matrix
%            .mmag - constraint on stimulus
%       sparam - simulation parameters
%           .niter - number of iterations
%           .rstate - state of the random number generators
%                     leave blank or set to -1 if you don't want to set it
%                     default is to use a different state each time based
%                     on the clock
%           .normalize - if this field exists, all random stimuli are
%           normalized to have this magnitude. We do this to ensure a valid
%           comparison to when we choose the optimal stimulus as we impose
%           that constraint then.
%                   this should be the square of the magnitude.
%       methods - gets passed to initmethods.m
%               - specifies which methods are on and their options.
%       ropts  - options regarding the results/analysis
%           .fname  - name to save data to
%                   - leave blank to not save data
% Return Value:
%       pmethods - results of updating the posteriors
%       sr   -   stimulus and response
%            sr.y - the stimuli - 1 trial per column
%            sr.nspikes - #of spikes after each stimulus
%       simparam - contains the actual simparam used
%                - i.e rstate will store the state of the random number
%                generator
%Explanation:
%   This function runs the specified method and on each iteration
%   uses the stimulus likely to maximize the info.
%
function [pmethods,sr,simparam]=maxupdate(mparam,simparam,methods,ropts)

warning('4-08-2006 maxupdate is in general considered outdated. ')
warning('Maxupdateshist should provide all needed functionality.');
warning('additionally maxupdateshist includes optimizations for the update which have not been');
warning('implemented in maxupdate');
user_entry = input('Do you want to continue (y/n)','s');
if (strcmp(user_entry,'y')==0)
    return;
end

sr=[];
%error checking defaults
if ~exist('simparam','var')
    simparam=[];
end
if ~isfield(simparam,'niter')
    simparam.niter=100;
    fprintf('updateposteriors: Using default of %0.3g iterations \n',simparam.niter);
end


if ~isfield(simparam,'rstate')
    simparam.rstate=-1;
end
if (simparam.rstate <0)
    %reset the states of the random number generators to a different state
    %each time
    %save the state so that we can reproduce the results later
    simparam.rstate=sum(100*clock);    
end
randn('state',simparam.rstate);
rand('state',simparam.rstate);

if ~exist('mparam','var')
    mparam=[];
end
if ~isfield(mparam,'tlength')
    error('Updateposteriors: mparam.tlength not specified');
end
if ~isfield(mparam,'ktrue')
    error('Updateposteriors: mparam.ktrue not specified');
end

if ~isfield(mparam,'tresponse')
    error('Updateposteriors: mparam.tresponse not provided');
end

%make sure tlength and ktrue are vectors of same size
mparam.ktrue=colvector(mparam.ktrue);

if (size(mparam.ktrue,1)~=mparam.tlength)
    error('Updatposteriros: mparam.ktrue must have length = mpraram.tlength');
end

if ~isfield(mparam,'dk')
    error('Updateposteriors: mparam.dk not specified');
end

if ~isfield(mparam,'krange')
    error('Updateposteriors: mparam.krange not specified');
end

if (size(mparam.krange,1)==1)
    %assume mparam.krange should be same for each dimension
    mparam.krange=ones(mparam.tlength,1)*mparam.krange;
end

%printiter is the interval to use when printing the iteration number 
printiter=10;

%**************************************************************************
%Options concerning results
if ~exist('ropts','var')
   ropts=[]; 
end
if ~isfield(ropts,'fname')
    ropts.fname=[RESULTSDIR '\posteriors\' sprintf('%0.2g_%0.2g_pdfs_data.mat',datetime(2),datetime(3))];
    ropts.fname=seqfname(data.fname);
    fprintf('UpdatePosteriors: Date will be saved to: \n \t %s \n',ropts.fname);
else
    if isempty(ropts.fname)
        fprintf('UpdatePosteriors: Data will not be saved \n');
    end
end
%*************************************************************************
%initialization
%************************************************************************
%specify which variables get saved
%vtosave
%name of variables to save
vtosave.pmethods='';
vtosave.kldist='';
vtosave.simparam='';
vtosave.ropts='';
vtosave.mparam='';
vtosave.sr='';

%save options
saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
saveopts.cdir =1; %create directory if it doesn't exist
%*************************************************************************
%setup pmethods structure
%************************************************************************

%initialize the pmethods structure
pmethods=initmethods(methods);

if (pmethods.newton.on~=0)
    pmethods.newton.newtonerr=zeros(1,simparam.niter+1);
    pmethods.newton.niter=zeros(1,simparam.niter+1);
    
end
%**************************************************************************
%Compute the posterior after each iteration using a gaussian
%approximation and brute force
%**************************************************************************
%mean and covariance of initial prior
%warning('Initial Mean not zero to prevent problems with fmaxjensen.m');
pinit.m=zeros(mparam.tlength,1);
pinit.c=eye(mparam.tlength,mparam.tlength);
if isfield(mparam,'pinit')
    pinit=mparam.pinit;
end


%number of points at which we compute k
numkpts=diff(mparam.krange,1,2)/mparam.dk+1;

%clean up temp variables
clear('m','dindex','tr');

%construct a list of enabled methods
mon={};
mnames=fieldnames(pmethods);
for index=1:length(fieldnames(pmethods))
    if pmethods.(mnames{index}).on >0
        mon{length(mon)+1}=mnames{index};
    end
end


%*************************************************************************
%Loop to update the posterior
%*************************************************************************

%**************************************************************************
%EKF: Compute the Gaussian Approximation of the posterior using the EKF idea
%**************************************************************************
%loop through enabled methods
fprintf('Begin infomax \n');
for mindex=1:length(mon)
    
    %declare a structure to hold the stimuli
    sr.(mon{mindex}).y=zeros(mparam.tlength,simparam.niter);
    sr.(mon{mindex}).nspikes=zeros(1,simparam.niter);
    sr.(mon{mindex}).fcase=zeros(1,simparam.niter);
    fprintf('Compute the %s approximation of the posterior \n',pmethods.(mon{mindex}).label);

    %initialize structure to store timing
    pmethods.(mon{mindex}).timing=[];
    pmethods.(mon{mindex}).timing.update=zeros(1,simparam.niter);
    pmethods.(mon{mindex}).timing.optimize=zeros(1,simparam.niter);
    
    %initialize the pdf
    pmethods.(mon{mindex}).m(:,1)=pinit.m;
    pmethods.(mon{mindex}).c{1}=pinit.c;

    post.m=pmethods.(mon{mindex}).m(:,1);
    post.c=pmethods.(mon{mindex}).c{1};

    obsrv.twindow=mparam.tresponse;
    for tr=2:simparam.niter+1
        
        
        if (mod(tr-1,printiter)==0)
            
            fprintf('Infomax: iteration %0.2g \n',tr-1);
        end
        %maximize the information
        %to choose the stimulus
        %[xmax]=fmaxjensen(post,simparam.normalize);
        
      
        %xmax=fmaxlambda(post,mmag);
        param.plots.on=0;
        %[finfo,jlbound,theta,xpts,hf]=fisherucirc(post,param);
        %[val,ind]=max(finfo);
        %xmax=xpts(:,ind);
        %xmax=xmax/(xmax'*xmax)^.5;
        %[u s]=svd(post.c);
        %s=diag(s);
        %v=u'*post.m;
        
        %handle 2d case specially
        %if (length(s)==2)
            %check if eigenvectors are equal
            %if(s(1)==s(2))
            %    y=v;
            %    yopt=y*(mmag^2/(y'*y))^.5;
            %else
        %        y0opt=fminbnd(@(y0)(-fishermax(s,v,y0,mmag)),-1,1);
        %        [finfo,yopt]=fishermax(s,v,y0opt,mmag);
            %end
        %else
        %    error('Max not implemented for >2d ');
        %end
        %bopt=fminbnd(@(btest)(-flambda(s,v,btest,mmag)),0,1);
        %keyboard;
        %[finfo,yopt]=flambda(s,v,bopt,mmag);
        %xmax=u*yopt;

        %time the optimization
        tic;
        [xmax,fopt,fcase]=fishermax(post,mparam.mmag);
        toptimize=toc;
        
        sr.(mon{mindex}).fcase(:,tr-1)=fcase;
        %save the case for further reference
        
        %if (tr>66)
        %    xmax=post.m/(post.m'*post.m)^.5;
        %end
        if (sum(imag(xmax))~=0)
             warning('optimal stimulus is not real');
        end
        %if (sum(abs(sign(xmax)))>3)
        %    xmax=-xmax;
        %end
        
        %sample the model to generate the response
        tinfo.twindow=mparam.tresponse;
    
    
        %stimes=samp1dglmh(@glm1dexp,mparam.ktrue,xmax,tinfo);
        sr.(mon{mindex}).y(:,tr-1)=xmax;                     %save the stimulus
        %just generate spike counts
        sr.(mon{mindex}).nspikes(1,tr-1)=poissrnd(glm1dexp(mparam.ktrue,xmax));
        fprintf('calling poissrnd \n');
        obsrv.n=sr.(mon{mindex}).nspikes(1,tr-1);
       
       %call the appropriate method to update the posterior
       %doing this check on each iteration might be slow
       %might be better to give each method a unique number
       %and then just check that element in array to see if its on or off
       
       switch mon{mindex}
           case {'ekf'}            
            
            [post,pll]=ekfposteriorglm(post,xmax,obsrv,@glm1dexp);
           case {'ekfmax'}
               tic;
               [post,pll]=ekfposteriormax(post,xmax,obsrv,@glm1dexp);
               pmethods.ekfmax.timing.update(1,tr-1)=toc;
           case {'ekf1step'}
              
               %compute posterior using 1step update
                %every kth iteration we do a 1 step update
                if (mod((tr-1),pmethods.ekf1step.onestep)==0)
                      %first update pekf using just the observation on this trial
                    [post,pll]=pmethods.ekf1step.ekffunc(post,(sr.(mon{mindex}).y(:,tr-1)),obsrv,@glm1dexp);
                  
                    %obsrevations is all observations up to this time pt
                    %want a row vector each column a different trial
                    obsrv.n=(sr.(mon{mindex}).nspikes(1:tr-1));
                    %keyboard
                    [post]=post1stepgrad(pinit,post,sr.(mon{mindex}).y(:,1:tr-1),obsrv,@glm1dexp);
                    %fprintf('Ekf one step correction \n');
                  
                else
                    %for debugging of the ekf I also return
                    %pll which describes gaussian approximation of the likleihod
                    [post,pll]=pmethods.ekf1step.ekffunc(post,xmax,obsrv,@glm1dexp);

                end
                
           case {'g'}
               [ppost]=gposteriorglm(ppost,xmax,obsrv,@glm1dexp);
        
           case {'newton'}
        

                    %prior
                    prior.m=pmethods.newton.m(:,1);
                    prior.c=pmethods.newton.c{1};

            
                    obsrv.n=(sr.(mon{mindex}).nspikes(1:tr-1));

                    % The mean of our posterior is the peak of the true posterior
                    % therefore its a root of the derivative of the log of the posterior
                    % we use newton's method to compute it
                    % The new covariance matrix is the inverse of the - hessian at the
                    % new ut
                    % The seed for newton's methods is the mean from the previous
                    % iteration
    
                    %time the update
                    tic;
                    post.m=pmethods.newton.m(:,tr-1);
                    post.c=pmethods.newton.c{tr-1};
                    newtonerr=inf;
                    while (newtonerr >pmethods.newton.tolerance);
                        [newpost]=post1stepgrad(prior,post,sr.(mon{mindex}).y(:,1:tr-1),obsrv,@glm1dexp);
                        newtonerr=(newpost.m-post.m);
                        newtonerr=(newtonerr'*newtonerr)^.5;
                        post=newpost;
                        pmethods.newton.niter(1,tr)=pmethods.newton.niter(1,tr)+1;
                    end
                    pmethods.newton.newtonerr(tr)=newtonerr;
                    pmethods.newton.timing.update(1,tr-1)=toc;
                    %pmethods.newton.timing.optimize(1,tr-1)=toptimize;
       end
       
        pmethods.(mon{mindex}).m(:,tr)=post.m;
        pmethods.(mon{mindex}).c{tr}=post.c;
        pmethods.(mon{mindex}).timing.optimize(1,tr-1)=toptimize;
    end
    
    
end
fprintf('Finished infomax ! \n');

clear('priormat');


%save data
if (~(isempty(ropts.fname)))
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname)
end


