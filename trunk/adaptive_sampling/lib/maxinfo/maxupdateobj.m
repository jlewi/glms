%function [pall,srall,simparam,mparam,timing]=maxupdateobj('mparam',mparam,'simparam',simparam,'ntorun',ntroun)
%
%       mparam -object of mparamObj type
%                             -allows you to track moving parameter
%       sparam - object derived from SimulationBase
%       ntorun  - number of simulations to run
%function [pall,srall,simparam,mparam]=obj('simfile',simfile,'simvar',simvar,'ntorun',ntroun)
%       simfile - file to which previous simulation was saved
%       simvar - name of variable in file to which simparam object was
%       saved
%       ntorun  - number of trials to run
%
%       continue an earlier simulation
% Return Value:
%       pmethods - results of updating the posteriors
%       sr   -   stimulus and response
%            sr.y - the stimuli - 1 trial per column
%            sr.nspikes - #of spikes after each stimulus
%       simparam - contains the actual simparam used
%                - i.e rstate will store the state of the random number
%                generator
%       mparam
%               .ktrue
%               .atrue - if a diffusion parameter is specified then the
%                         return values with be matrices
%                         (filterlengthxsimparam.niter) each column will be
%                         the theta used on that observation
%       timing - structure to contain info about the timing
%
%Explanation:
%   This function runs the specified method and on each iteration
%   uses the stimulus likely to maximize the info.
%
%
% Implementation notes:
function [pall,srall,simparam,mparam,timing]=maxupdateobj(varargin)


ntorun=-1;
%parameters required for continuing new sim
rnew.mparam=0;
rnew.simparam=0;
rnew.ntorun=0;
%parameters required for continuing sim
rcont.simfile=0;
rcont.simvar=0;
rcont.ntorun=0;
newsim=0;
contsim=0;
for j=1:2:nargin
    switch varargin{j}
        case 'mparam'
            mparam=varargin{j+1};
            if ~isa(mparam,'MParamObj')
                error('mparam must be derived from ModelParamObj');
            end
            rnew.mparam=1;
            newsim=1;
        case 'simparam'
            simparam=varargin{j+1};
            if ~isa(simparam,'SimulationBase')
                error('simparam must be derived from SimulationBase');
            end
            rnew.simparam=1;
            newsim=1;
        case 'simfile'
            simfile=varargin{j+1};
            rcont.simfile=1;
            contsim=1;
        case 'simvar'
            simvar=varargin{j+1};
            contsim=1;
            rcont.simvar=1;
        case 'ntorun'
            ntorun=varargin{j+1};
            rnew.ntorun=1;
            rcont.ntorun=1;
    end
end

if (ntorun<1)
    error('Number of iterations was not set');
end

%make sure we didn't supply parameters for continuing a sim and starting a
%new one
if ((newsim==1 && contsim==1) || (newsim==0 && contsim==0))
    error('argument list doesnt match call for starting new sim or continuing old sim');
end

sr=[];
%printiter is the interval to use when printing the iteration number
printiter=10;



%********************************************************************
%Initialize/Load simulation
%*****************************************************
%declare the structures to hold the results for this simulation
einfo=[];
post=[];
newpost=[];
prior=[];
sr=[];
pdata=[];


if (newsim==1)
    % check required parameters are provided
    freq=fieldnames(rnew);
    for k=1:length(freq)
        if (rnew.(freq{k})==0)
            error(sprintf('Required parameter %s not specified \n',freq{k}));
        end
    end
    %**************************************************************************
    %if a diffusion parameter is specified the only update we can use is the 1d
    %step
    %check if diffusion is on
    if (mparam.diffusion.on~=0)
        if ~isfield(mparam.diffusion,'q')
            error('No diffusion matrix given');
        end

        %
        if ~(isa(simparam.updater,'Newton1d'))
            error('A diffusion parameter can only be specified with newton1d update method (allobsrv should also work??)');
        end
    end
    %priors for newsim
    [pinit,shist]=priornewsim(mparam);

    %pdata=pmethods.(mname);
    %initialize the data for a new simulation
    %declare a structure to hold the stimuli
    sr.y=zeros(getstimlen(mparam),ntorun);
    sr.nspikes=zeros(1,ntorun);
    sr.fcase=zeros(1,ntorun);
    sr.stiminfo=cell(1,ntorun);
    %initialize structure to store mean
    pdata.m=zeros(getparamlen(mparam),ntorun+1);
    pdata.c=cell(1,ntorun+1);
    %a new simulation
    [pinit,shist]=priornewsim(mparam);

    %initialize the pdf
    pdata.m(:,1)=pinit.m;
    pdata.c{1}=pinit.c;

    simparam.niter=ntorun;
else %load previous simulation
    freq=fieldnames(rcont);
    for k=1:length(freq)
        if (rcont.(freq{k})==0)
            error(sprintf('Required parameter %s not specified \n',freq{k}));
        end
    end
    fprintf('Continuing an earlier simulation \n');
    if ~exist(simfile,'file')
        error('Simulation file doesnt exist. Cannot continue previous simulation.');
    end


    %load the prexisting data
    [ppast, simparam, mparam, srpast]=loadsim('simfile',simfile,'simvar',simvar);


    %pdata=pmethods.(mname);
    %initialize the data for a new simulation
    %declare a structure to hold the stimuli
    sr.y=zeros(getstimlen(mparam),ntorun);
    sr.nspikes=zeros(1,ntorun);
    sr.stiminfo=cell(1,ntorun);

    %initialize structure to store mean
    pdata.m=zeros(getparamlen(mparam),ntorun+1);

    %information about the continuing simulation
    continfo.piter=simparam.niter; %number of previous iterations
    continfo.ntorun=ntorun;            %additional number of iterations to run
    continfo.totaliter=continfo.piter+continfo.ntorun;
    simparam.niter=continfo.totaliter;
    %initialize the random number generators to the state we left off
    %on
    randn('state',simparam.rstate(end).rfinishn);
    rand('state',simparam.rstate(end).rfinish);

    %load the data for this method from the file
    %and get the posterior we left off on
    pdata.m(:,1)=ppast.m(:,end);
    pdata.c{1}=ppast.c{end};



    %allocate space for shist
    shist=zeros(mparam.alength,1);

    if (mparam.alength>0)
        %get the spike history into shist
        nobsrv=length(srpast.nspikes);
        if (mparam.alength <= (nobsrv))
            shist(:,1)=srpast.nspikes(nobsrv:-1:nobsrv-mparam.alength+1);
        else
            shist(1:nobsrv,1)=srpast.nspikes(nobsrv:-1:1);
        end
    end

end

%*******************************************************
%stucture to store timing info
timing.update=zeros(1,ntorun);
timing.choosestim=zeros(1,ntorun);
timing.total=zeros(1,ntorun);

%tell simparam to save the current start state of the random number
%generators as the start state.
%this is not backwards compatible
simparam=savestartstate(simparam);

%*************************************************************************
%Loop over the iterations
%*************************************************************************
%loop through enabled methods
fprintf('Begin trials \n');


%initialize einfolast to the eigeninfo of the prior
%we do this because in the rank 1 case we can use the eigenvectors to
%efficiently compute the entropy
%keyboard
%compeig variable determines whether we compute the eigendecomposition or
%not
%if (isa(simparam.updater,'Newton1d'))
if (simparam.updater.compeig~=0)
    %get the initial eigen decomposition
    [einfolast.evecs einfolast.eigd]=svd(pdata.c{1}(1:getstimlen(mparam),1:getstimlen(mparam)));
    einfolast.eigd=diag(einfolast.eigd);
    einfolast.esorted=-1;   %eigenvalues are in descending order
    einfo=einfolast;
else
    einfolast=[];
end
%store entropy
pdata.entropy=zeros(1,ntorun+1);
%compute entropy of prior
eigd=eig(pdata.c{1});
pdata.entropy(1)=1/2*getstimlen(mparam)*(log2(2*pi*exp(1)))+1/2*sum(log2(abs(eigd)));

post.m=pdata.m(:,1);
post.c=pdata.c{1};

obsrv.twindow=mparam.tresponse;

%matrix to store stimuli - stimulus + spike history terms
%for all iterations
%do this because some methods requir all stimuli
stimmat=zeros(getstimlen(mparam)+mparam.alength,ntorun);

%it is absolutely important that we perform all of the iterations
%for a particular method before going onto a different method
%this is is because I make assumptions about what post, prior
%and newpost store in each methods update.
for tr=2:ntorun+1

    if (mod(tr-1,printiter)==0)

        fprintf('%s: iteration %0.2g \n',simparam.simid,tr-1);
    end

    %*****************************************************************
    %compute the spike history
    %******************************************************************
    %spike history is just the previous responses
    %we take them to be zero if they happened before the start of the
    %experiment
    %tr-2 is actual number of observations we have already made
    nobsrv=tr-2;    %how many observations have we made

    %only compute shist if we have spike history terms
    if (mparam.alength >0)

        if (mparam.alength <= (nobsrv))
            shist(:,1)=sr.nspikes(nobsrv:-1:nobsrv-mparam.alength+1);
        else
            shist(1:nobsrv,1)=sr.nspikes(nobsrv:-1:1);
        end
    end

    %tic;
    %initialize time for optimization step
    %and to compute the eigendecomposition
    searchtime=0;
    eigtime=0;

    %if diffusion is on we need to add the diffusion covariance matrix
    %to our posterior matrix before we optimize the information
    %except if this is the first iteration
    %
    %07-03-2007
    %This code should be absorbed into the MParamObj
    %   should add a funciton getPrior(mobj,trial)
    %   gets our prior estimate of theta before trial="trial"
    %   this function should also return the eigendecomposition if it
    %   exists
    if (mparam.diffusion.on~=0 & ((tr>2)|contsim~=0))
        keyboard
        %fprintf('Addiding diffusion matrix');
        post.c=mparam.diffusion.diffc^2*post.c+mparam.diffusion.q;
        %when we multiply it we should also change the variance.
        post.m=post.m*mparam.diffusion.diffc;

        %if the noise is white then we can compute the eigenvectors &
        %eigenvalues of the new posterior matrix
        %this would allow us to use the rank 1 update
        if(mparam.diffusion.iswhite~=0)
            if (~isempty(einfo) && (simparam.updater.compeig~=0))
                %qeig should be computed inside maxupdeshist
                einfo.eigd=mparam.diffusion.diffc^2*einfo.eigd+mparam.diffusion.qeig;

                if isfield(post,'invc')

                    if ~isempty(post.invc)
                        post.invc=einfo.evecs*diag(1./einfo.eigd)*einfo.evecs';
                    end
                end
            else
                einfo=[];
                post.invc=[];
            end
            %do we need to recompute first derivative.
            post.dlpdt=[];

        else

            %set post.invc and einfo to empty matrixes becaue our
            %we no longer know their value
            post.invc=[];
            %measurement of 1st derivative is also innaccurate
            post.dlpdt=[];

            %the eigendecomposition is no longer valid
            einfo=[];
        end
    end




    %need to figure out the absolute trial number.
    if exist('continfo','var')
        ctrial=continfo.piter+tr-1;
    else
        ctrial=tr-1;
    end
    %we need to save the object because some calls to choosestim
    %modify the object.
    tic;
    %different updaters require different parameters
    %so we have special code for the different function calls
    switch class(simparam.stimchooser)
        case 'Poolstimrank1'
            %we need the covariance matrix from t-1
            %this is stored at tr-2

            poolparam=[];
            if (tr>2)
                poolparam.oldcovar=pdata.c{tr-2};
                %dot product of mean at time t and stimulus
                poolparam.oldglmproj=pdata.m(:,tr-1)'*sr.y(:,tr-1);
                poolparam.oldobsrv=sr.nspikes(tr-1);
                poolparam.oldstim=sr.y(:,tr-1);

            end
            [xmax,stiminfo,simparam.stimchooser]=choosestim(simparam.stimchooser,post,shist,einfo,mparam,'trial',ctrial,poolparam);
            clear('poolparam');
        otherwise
            %default handler
            [xmax,stiminfo,simparam.stimchooser]=choosestim(simparam.stimchooser,post,shist,einfo,mparam,'trial',ctrial);
    end
    timing.choosestim(tr-1)=toc;
    clear('ctrial');


    %check if its an object
    xobj=[];
    if isa(xmax,'GLMInput')
        %check to make sure xmax is valid
       xobj=xmax;
        xmax=getData(xobj);
    end
        %check to make sure xmax is valid
        if (any(isnan(xmax)))
            error('Optimal stimulus is invalid');
        end


        if (sum(imag(xmax))~=0)
            error('optimal stimulus is not real');
        end
    


    %sample the model to generate the response
    tinfo.twindow=mparam.tresponse;

    %************************************************************
    %Full stimulus
    %**************************************************************
    %form the full stimulus by combining the spike history
    %and stim terms
    stim=[xmax;shist];
    stimmat(:,tr-1)=stim;


    %******************************************************************
    %generate the observation
    %stimes=samp1dglmh(@glm1dexp,mparam.ktrue,xmax,tinfo);
    %******************************************************************

    sr.y(:,tr-1)=xmax;                     %save the stimulus
    sr.stiminfo{tr-1}=stiminfo;
    %just generate spike counts

    %Revision: 1485  if xobj is a GLMInput object pass it to observe
    %instead of stim
    if ~isempty(xobj)        
        sr.nspikes(1,tr-1)=observe(simparam.observer,xobj);
    else
        sr.nspikes(1,tr-1)=observe(simparam.observer,stim);
    end

    if isinf(sr.nspikes(1,tr-1))
        error('infinite # of spikes');
    end
    if isnan(sr.nspikes(1,tr-1))
        error('observation is nan');
    end

    %call the appropriate method to update the posterior
    %doing this check on each iteration might be slow
    %might be better to give each method a unique number
    %and then just check that element in array to see if its on or off


    %compute the inverse of the covariance matrix using the
    %eigenvectors/values of the covariance matrix if they are available
    %This can save us some from calculating it in the newtonian
    %update

    if ~(isempty(einfo))
        %we want to time the computation of computing the inverse
        %include it in our update step time
        %if we method doesn't use post.invc then
        %we shouldn't add it later on to the timing
        % tic;
        %could proballly optimize this  pdata.timing.searchtime(1,tr-1) by not creating a matrix for the
        %eigenvalues
        post.invc=einfo.evecs*diag(1./einfo.eigd )*einfo.evecs';
        %we add the time of the newton iteration to what ever time
        %it took to compute the inverse of the posterior
        %if we did that earlier using the eigenvectors from the
        %stimulus optimization step
        %pdata.timing.update(1,tr-1)=pdata.timing.update(1,tr-1)+toc;

        %set einfo to empty matrix because once we compute the update
        %the eigenvectors will no longer be accurate
        %the efficient update of the eigendecompostion requires the
        %last eigenvectors so we save them
        einfolast=einfo;
        einfo=[];

        %Reset post.invc=to be empty to see if algorithm works if we
        %don't use the eigenvectors to compute the inverse efficiently
        %post.invc=[];
    else
        post.invc=[];
    end


    %with spike history terms
    %we compute ML estimate as before except now the stimulus is
    %the coatenation of the stimulus and spike history term

    %could also check the class
    tic
    switch class(simparam.updater)
        case {'BatchML'}
            %11-16
            %compute gaussian approximation by performing gradient
            %ascent on the true posterior
            %Coded this up to see how it would perform if we have
            %misspecification
            %************************************************
            %1. Create the stimulus matrix each column is the stimulus +
            %any spike history appended to it
            %
            % VERY IMPORTANT: Since we are using all observations we
            % need to load any past observations from the file
            % we do this once

            %initialize the structures to store the observations and
            %stimuli do this on just first iteration
            if (tr==2)
                allobsrv=[];           %store variables for just the all obsrv update rule
                allobsrv.obsrv=zeros(1,simparam.niter);
                allobsrv.allstim=zeros(getstimlen(mparam)+mparam.alength,simparam.niter);
            end
            %load all previous stimuli and observations from a file on
            %first iteration
            if (contsim~=0 && tr==2)
                allobsrv.allstim=zeros(getstimlen(mparam)+mparam.alength,simparam.niter);
                allobsrv.allstim(1:getstimlen(mparam),1:continfo.piter)=srpast.y;

                allobsrv.obsrv(1,1:continfo.piter)=srpast.nspikes;
            end

            nobsrv=tr-1;            %number of trials for which stimuli have been presented and responses are known
            %on just this iteration
            %2. Insert the most recent stimulus
            if (contsim~=0)
                allobsrv.totobsrv=nobsrv+continfo.piter;
            else
                allobsrv.totobsrv=nobsrv;
            end
            allobsrv.allstim(1:getstimlen(mparam),allobsrv.totobsrv)=sr.y(:,nobsrv);
            allobsrv.obsrv(1,allobsrv.totobsrv)=sr.nspikes(1,nobsrv);

            %3. add the spike history
            if (mparam.alength >0)
                %if we are continuing a simulation and we have spike
                %history terms we possible need to load spike history
                %terms from a past file.
                if (contsim~=0 && nobsrv < mparam.alength)
                    %we need to append the shist terms which were
                    %loaded from the file
                    %these should be stored in shist and they
                    %should be stored with most recent spike in
                    %first element so we need to reverse them
                    spikes=[shist(end:-1:1) sr.nspikes(1:tr-1)];
                    warning('Need to check this code is correct');
                else
                    spikes=[ones(1,mparam.alength) sr.nspikes(1:tr-1)];
                end
                for titer=1:nobsrv
                    %we need to obtain the spikes which could have acted as
                    %stimulus for trial titer
                    npspikes=nobsrv-1;  %number of spikes which came before the most recent trial
                    %most recent observation is first
                    allobsrv.allstim(getstimlen(mparam)+1:end,allobsrv.totobsrv)=spikes(npspikes+mparam.alength:-1:npspikes+1);
                end

                %I should rewrite this using an object
                warning('code assembling observations with spike history has not been debugged');
            end
            ao=[];

            [post,uinfo,einfo]=update(simparam.updater,post,allobsrv.allstim(:,1:allobsrv.totobsrv),mparam,allobsrv.obsrv(:,1:allobsrv.totobsrv),einfolast);

            %compute the entropy
            %3-20-07
            %don't compute the entropy anymore unless explicitly told to do
            %so (i.e add a field to SimulationBase). DO this to avoid
            %computation of an extra svd
            %this is somewhat inefficient because we do an extra svd that
            %we don't need
            %eigd=svd(post.c);
            %pdata.entropy(tr)=1/2*(getstimlen(mparam)+mparam.alength)*(log2(2*pi*exp(1)))+1/2*sum(log2(eigd));

            clear('spikes','titer','nobsrv','npspikes','eigd');

        case {'Newton1d'}

            %can we compute the eigenvectors using the rank 1
            %if we can set einfolast to be the eigendecomposition of
            %the last covariance matrix. Otherwise make it an empty
            %matrix
            %updates yes if:
            %   1. diffusion is not on
            %   2. we have the previous eigendecomposition
            %   (einfolast
            %  3. diffusion is on but diffusion is white
            rank1eig=1; %set it to compute rank1eig and then test if conditons are met
            if (mparam.diffusion.on==1)
                if (mparam.diffusion.iswhite==0)
                    rank1eig=0;
                    einfolast=[];
                end
            end
            [post,uinfo,einfo]=update(simparam.updater,post,stim,mparam,sr.nspikes(1,tr-1),einfolast);


            %compute the entropy

            if ~isempty(einfo)
                %2-11-07
                %changed it from getstimlen(mparam)
                %to getstimlen(mparam)+mparam.alength
                pdata.entropy(tr)=1/2*(getstimlen(mparam)+mparam.alength)*(log2(2*pi*exp(1)))+1/2*sum(log2(einfo.eigd));
            end

            if ~isempty(find(isnan(post.m)))
                error('new mean is nan');
            end
            if ~isempty(find(isnan(post.c)))
                error('Covariance has Nan');
            end
            pdata.uinfo(tr-1)=uinfo;


            %save memory by not saving covariance matrices which
            %are no longer needed
            %zero out old matrices which are no longer needed
            %we will save every simparam.lowmem iter
            %set it to inf to save only first and last
            if (simparam.lowmem~=0)
                if (tr>2)
                    if isinf(simparam.lowmem)
                        pdata.c{tr-1}=[];
                    else
                        %we subtract 2
                        %b\c tr-1 is the number of iterations
                        %but we always need to wait 1 iteration before
                        %getting rid of the matrix because we need it
                        %to serve as the prior
                        d=(tr-2)/simparam.lowmem;
                        if (d~=floor(d))
                            pdata.c{tr-1}=[];
                        end
                    end
                end
                %pdata.c{tr-1}=[];
            end

        otherwise
            error('Maxupdateobj:update','Code for updater %s doesnt exist',class(simparam.updater));

    end
    timing.update(tr-1)=toc;

    pdata.m(:,tr)=post.m;
    pdata.c{tr}=post.c;
    %if isstruct(searchtime)
    %   pdata.timing.optimize(1,tr-1)=searchtime.fishermax+eigtime;
    % else
    %        pdata.timing.optimize(1,tr-1)=searchtime+eigtime;
    %   end
    %      pdata.timing.searchtime(1,tr-1)=searchtime;
    %        pdata.timing.eigtime(1,tr-1)=eigtime;

    %for logistic model
    %computing accuracy on training set
    if isa(mparam,'BCLogisticModel')
       lbls=classify(mparam,post.m,sr.y(:,1:tr-1));
       icorr=find(lbls==sr.nspikes(1:tr-1));
       fprintf('Traing set size=%2.2g \t Fraction correct %.3g \n',length(lbls),length(icorr)/length(lbls));
    end
end %loop over trials



%tell simparam to save the current start state of the random number
%generators as the end state.
%this is not backwards compatible
simparam=saveendstate(simparam);

%*********************************************************
%end of simulation for this method
%m
if(newsim==1)
    pall=pdata;
    srall=sr;
else


    %get a list of all fields of pdata
    %each field should either be a matrix, cell array, or structure
    %array with 1 dimension equal to the number of iterations we just
    %ran.

    pall=[];
    srall=[];
    fnames=fieldnames(pdata);
    for fi=1:length(fnames)
        switch(fnames{fi})
            case 'm'
                pall.m=[ppast.m pdata.m(:,2:end)];
            case 'c'
                pall.c=[ppast.c pdata.c(2:end)];
            otherwise
                %its vector of scalars or structure arrays
                %we can handle it generically
                %otherwise we need special handle
                if isvector(pdata.(fnames{fi}))

                    %check that length dimension is = to ntorun or ntrorun+1
                    %assumption is that there should be 1 entry for every
                    %trial and possible 1 for the prior
                    %dims=size(pdata.(fnames{fi}));

                    if (ntorun<=length(pdata.(fnames{fi})) &&length(pdata.(fnames{fi}))<=ntorun+1)
                        %concatenate along the dimension id
                        newfdata=pdata.(fnames{fi});
                        pall.(fnames{fi})=[ppast.(fnames{fi}) newfdata(2:end)];
                    else

                        warning(sprintf('cannot merge data for field %s of pdata. no dimension of size niter',fnames{fi}));
                    end
                else
                    warning(sprintf('cannot merge data for field %s of pdata. special handler required',fnames{fi}));
                end
        end
    end

    %with sr we don't have to strip out any iterations
    %so concatenation is a little easier
    fnames=fieldnames(sr);
    for fi=1:length(fnames)
        switch(fnames{fi})
            case  'y'
                srall.y=[srpast.y sr.y];
            otherwise
                %its vector of scalars or structure arrays
                %we can handle it generically
                %otherwise we need special handle
                if isvector(sr.(fnames{fi}))
                    %check that one dimension is = to ntorun
                    dims=size(sr.(fnames{fi}));
                    id=find(dims==ntorun);
                    if ~isempty(id)
                        %concatenate along the dimension id
                        srall.(fnames{fi})=[srpast.(fnames{fi}) sr.(fnames{fi})];
                    else

                        warning(sprintf('cannot merge data for field %s of sr. no dimension of size niter',fnames{fi}));
                    end
                else
                    warning(sprintf('cannot merge data for field %s of sr. special handler required',fnames{fi}));

                end
        end
    end %loop over field names

    %******************************************************************
    %verification- verify merge is accurate
    %make sure end posterior from previous trial was copied
    if (ppast.m(:,end)~=pall.m(:,continfo.piter+1))
        error('merge is inaccurate');
    end
    if (ppast.c{end}~=pall.c{:,continfo.piter+1})
        error('merge is inaccurate');
    end

    %make sure first post from new stim was copied
    if (pdata.m(:,2)~=pall.m(:,continfo.piter+2))
        error('merge is inaccurate');
    end

    if (pdata.c{2}~=pall.c{:,continfo.piter+2})
        error('merge is inaccurate');
    end


    %make sure the prior for the new sim was the last posterior of the
    %previous sim
    if (pdata.m(:,1)~=ppast.m(:,end))
        error('merge is inaccurate');
    end

    if (pdata.c{1}~=ppast.c{:,end})
        error('merge is inaccurate');
    end

    %make sure prior was copied
    if (ppast.m(:,1)~=pall.m(:,1))
        error('merge is inaccurate');
    end
    if (ppast.c{1}~=pall.c{:,1})
        error('merge is inaccurate');
    end

    %******************************************
    %check merge of sr info
    %*******************************************

    if (srall.y(:,continfo.piter)~=srpast.y(:,end))
        error('merge is inaccurate');
    end
    if (srall.nspikes(continfo.piter)~=srpast.nspikes(:,end))
        error('merge is inaccurate');
    end

    if (srall.y(:,end)~=sr.y(:,end))
        error('merge is inaccurate');
    end
    if (srall.nspikes(end)~=sr.nspikes(:,end))
        error('merge is inaccurate');
    end




end % if statement for merging results


fprintf('Finished ! \n');




%**************************************************************************
%
%
%                                           END MAIN FUNCTION
%
%
%
%
%**************************************************************************
%**************************************************************************
%
%
%                                           Update Functions
%
%
%
%
%**************************************************************************


%**************************************************************************
%
%
%                                           Initialization/setup functions
%
%
%
%
%**************************************************************************
%**************************************************************************
%initialize the prior for a new simulation
%**************************************************************************
function [pinit,shist]=priornewsim(mparam)
%**************************************************************************
%mean and covariance of initial prior
pinit=mparam.pinit;

%allocate space for shist
shist=zeros(mparam.alength,1);






