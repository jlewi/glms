% function [finfo]=finfologistic(pcon,sigma)
function [finfo]=finfologistic(pcon,sigma)
error('obsolete: use AppoxLogisticMI object instead');

%value of E(log(1+var(r|mu)*sigma))
s=1.5;
c=.25*(2*pi)^.5*s;
%expected variance of the logisitic output
expvar=c/((2*pi)^.5*(sigma^2+s^2)^.5)*exp(-pcon^2/(2*(sigma^2+s^2)));
finfo=expvar*sigma^2;
%finfo=expvar;