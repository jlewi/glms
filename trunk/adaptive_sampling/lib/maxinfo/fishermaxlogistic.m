%function [xopt,muprojpt,lfopt,timing,fcase]=fishermaxshist(post,shist,eigck,mparam,objfunc,optparam)
%   post - current posterior
%   shist - spike history terms
%           -  needed in order to maximize the fisher info
%   eigck  - eigendecomposition of
%   post.c(1:mparam.klength,1:mparam:klength)
%           .eigd
%           .evecs
%   mparam - model parameters following values are needed
%           .alength
%           .klength
%           .mmag
%   objfunc - pointer to the objective function we want to maximize.
%                -it is a function of muproj and sigmmax(muproj)
%               -leave empty to set to default
%
%   fcase - 0 means optimium occurs for lam>emax
%               1 means optimium occurs for lam=emax 
%Explanation: Chooses the stimulus which maximizes the expected fisher information
%  xopt - optimized stimulus
%  fopt - fisher info at optimal stimulus
% 
%Return value
%   lfopt - log of the fisher information at optimal stimulus
%   timing
%       .eigtime - time of the eigendecompositions in obtaining the
%                  modified quadratic form
%       .fishermax-time to compute to find the maximum of the fisher
%       information
%fishermaxshist is outdated
%Following issues have to be resolved
%       1. making sure the eigenvalues are properly sorted before doing
%       anything
function [xopt,muprojopt,lfopt,timing]=fishermaxlogistic(post,shist,eigck,mparam,objfunc,optparam)

%choose a stimulus which lies on boundary 
%then put all remaining energy perpindicular to it
%xopt=post.m(1:end-1);
%xopt=xopt/(xopt'*xopt)^.5;
%xopt=-post.m(end)*xopt;
%if ((xopt'*xopt)^.5)>mparam.mmag
 %   xopt=mparam.mmag*xopt/(xopt'*xopt)^.5;
%else
 %   orthx=null(xopt');
  %  xopt=xopt+(mparam.mmag^2-xopt'*xopt)^.5*orthx;
%end
%muprojopt=1;
%lfopt=1;
%timing.fishermax=-1;
%timing.eigtime=-1;
%return;
%**************************************************************************

if isempty(objfunc)
   error('no objective function');
end

n=size(post.m,1);   %dimensionality
%**********************************************
%unit vector in direction of mean of just stimulus components
%need its magnitude as well
muk=post.m(1:mparam.klength,1);
mukmag=(muk'*muk)^.5;
muk=muk/mukmag;

%projection onto mean of stimulus
if isempty(shist)
    %no spike history terms
    muprojr=0;
else
    muprojr=shist'*post.m(mparam.klength+1:end,1);
end
%*************************************************************************
%Define the quadratic form
%x^t A x + Bx +d
%which has a null space defined by the current mean of the stimulus terms
%so the quadratic function we want to find max and min as function of
%muproj=muk'k; is
%y'*quad.eigd*y +
%(muproj*quad.wmuproj+quad.w)*y+(muproj^2*quad.dmuproj2+muproj*quad.muproj+
%quad.d)
[quad,timing.eigtime]=quadmodshist(post.c,eigck,muk,shist);


%define the function to pass to fminbind
tic;
fmaxobj=@(m)(-funcformax(m,muprojr,quad,objfunc,muk,mukmag,mparam.mmag,optparam));
[muprojopt lfopt] = fminbnd(fmaxobj,-mparam.mmag,mparam.mmag,optparam); 
   
%compute the max stimulus
[qmax,qmin,xmax,xmin]=maxminquad(quad,muk,muprojopt,mparam.mmag,optparam);

xopt=xmax;
lfopt=-lfopt;
timing.fishermax=toc;

%
%   muproj - projection onto mean of stimulus-this can vary
%   murpojr - projeciton onto mean of spike history - we can't control this
%   quad    - quadratic function to optimize
%   
function [funcval]= funcformax(muproj,muprojr,quad,objfunc,muk,mukmag,mmag,optparam)
    %find sigma max for this value
    [qmax,qmin,xmax,xmin]=maxminquad(quad,muk,muproj,mmag,optparam);
    %evaluate the objective function
    funcval=objfunc(muproj*mukmag+muprojr,qmax);
