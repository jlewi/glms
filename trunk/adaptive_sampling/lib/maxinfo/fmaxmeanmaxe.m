%flambda(eigd,v,lambda)
%   eigd - eigen values
%   v    - mean projected onto eigenvalues
%   y0  -  amount of energy along max eigen vector
%           as percent so -1 to 1
%   lambda - location at which to calculate fisher info
%
%Explanation: Used when optimal stimulus lies in space spanned
%   by max eigenvector and mean
function [finfo,y]=fmaxmeanmaxe(eigd,v,y0,mmag)
    %eigd=diag(eigd);
    y=zeros(length(v),1);
    y(1)=y0;
    
    [maxe ind]=max(eigd);
    v0=v(ind);
    
    %compute lambda1
    lambda1=maxe-v0/y0;
    
    %compute the other v
    y(2:end)=v(2:end)./(eigd(2:end)-lambda1);
    
    %now we normalize it
    y(1)=y0*mmag;
    
    y(2:end)=y(2:end)/(y(2:end)'*y(2:end))^.5*((1-y0^2)*mmag^2)^.5;
    
   
    
    %compute the vector associated with this point
    %y=y*(mmag/(y'*y))^.5;

    finfo=exp(y'*v+.5*y'*diag(eigd)*y)*y'*diag(eigd)*y;