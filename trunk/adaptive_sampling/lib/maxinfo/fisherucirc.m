%function [finfo,jlbound,theta,xpts,hf]=fisherucirc(pg)
%       pg  - specifies the gaussian describing the posterior
%       param
%           .plots.on - turn plots on
%           .plots.lbound.on = 0 or 1 turn lower bound plot on or off
%Explanation: Computes the fisher information on the unit circle
%
function [finfo,jlbound,theta,xpts,hf]=fisherucirc(pg,param)
hf=0;
defparam.plots.on=0;

if ~exist('param','var')
    param=[];
    param.plots.on=0;
    param.lbound.on=1;
end
if ~isfield(param,'lbound')
        param.lbound.on=0;
end
param=checkfields(param,defparam);

%**************************************************************************
%make a 1-d plot
%that is a plot of just the pots where ||x||=1
%*************************************************************************

theta=[0:2*pi/90:2*pi];
x=1*cos(theta);
y=1*sin(theta);

param.xpts=[x;y];
xpts=param.xpts;

[finfo,xpts]=fcurve(pg,param);


%************************************************************************
%compute the lower bound that results from jensen's inequality
%************************************************************************
[evec, eval]=eig(pg.c);


dimstim=size(pg.m,1);
ind=[1:dimstim]'*ones(1,2);
diagind=indexoffset(ind,size(pg.c));
eval=eval(diagind);
%we need to project everything into the eigenbasis
ypts=evec'*param.xpts;
u=evec'*pg.m;

Gvec=eval/2+log(eval)/1;
g=eye(dimstim,dimstim);
g(diagind)=Gvec;

%compute y^t g y
%step 1. Co x
%   this gives a matrix 
%   each comlumn corresponds to column of y
com=g*ypts;
%step 2. compute x'*(Cox)
%We want to compute the dot prodcut
%so we do term by term multiplication and then sum across rows
%yielding a row vectors
com=ypts.*com;
com=sum(com,1);
jlbound=exp(u'*ypts+com);

if (param.plots.on~=0)

pind=1;    
hf=figure;
set(hf,'Name', 'Fisher Info: Unit Circle');

subplot(3,1,1);
hold on;
plot(theta,finfo);
h_p(1)=plot(theta,finfo,'b.');
lgnd{1}='Fisher Information';
xlabel('\theta');
ylabel('F(x)');
xlim([0 2*pi]);

if (param.lbound.on~=0)
    pind=pind+1;
plot(theta,jlbound,getptype(pind,2));
h_p(pind)=plot(theta,jlbound,getptype(pind,2));
lgnd{pind}='Lower Bound';
legend(h_p,lgnd);
end

%*************************************************************************
%Compute the Information at the Eigenvectors and the mean
%*************************************************************************
%We want to compute it at +- the eigenvectors
%plot the eigen values
%compute finfo at the eigen values
pind=pind+1;
%eigenvectors are already normalized 
%we want to plot +-1* the eigenvectors
param.xpts=[evec -1*evec pg.m/(pg.m'*pg.m)^.5];
evec=[evec -1*evec pg.m/(pg.m'*pg.m)^.5];
[feig]=fcurve(pg,param)
thetaeig=acos([evec(1,:)]);
%theta will be in 1st or 2nd quadrant's
%angles in 4quadrant
ind=(evec(2,:)<0).*(evec(1,:)>=0);
thetaeig=thetaeig+(ind)*pi;
%3rd quadrant
ind=(evec(2,:)<0).*(evec(1,:)<0);
thetaeig=thetaeig+(ind)*pi/2;

%now plot the eigenvectors & mean
for index=1:size(evec,2)
    if (index==size(evec,2))
        %its the mean
        pind=pind+1;
        lgnd{pind}='Mean (Normalized)';
    else
        lgnd{pind}='Eigen Vectors'
    end
    plot([thetaeig(index) thetaeig(index)],[0 feig(index)],getptype(pind,2));
    h_p(pind)=plot([thetaeig(index) thetaeig(index)],[0 feig(index)],getptype(pind,1));
    
end

    
legend(h_p,lgnd);


%plot x and y
subplot(3,1,2);
plot(theta,x(1,:));
ylabel('x');
xlim([0 2*pi])

subplot(3,1,3);
plot(theta,y(1,:));
ylabel('y');
xlim([0 2*pi])

end