%function getobsrv(obsrv, trial)
%
%explanation: get the observation on that trial
function s=getobsrv(obj, trial)


if ~exist('trial','var')
    [s,matids]=readdata(obj);
    s=cell2mat(s);
    ind=find(matids~=obj.DELID);
    s=s(ind);
else
    s=zeros(obj.dim,length(trial));

    %find out which trials are in the buffer
    tbuffind=[];
    if (~isempty(obj.buffer))
        tbuffind=find(trial>=obj.tstart & trial<=obj.tend);

        if ~isempty(tbuffind)
            s(:,tbuffind)=obj.buffer(:,trial(tbuffind)-obj.tstart+1);
        end
    end
    
    if (length(tbuffind)<length(trial))
        tnotinbuff=ones(1,length(trial));
        tnotinbuff(tbuffind)=0;

        r=readdata(obj,trial(logical(tnotinbuff)));
        s(:,logical(tnotinbuff))=cell2mat(r);

    end
end