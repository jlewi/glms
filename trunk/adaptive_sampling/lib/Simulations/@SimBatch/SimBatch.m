%function sim=SimBatch('stimchooser',stimchooser,'observer', osbrv, 'updater',updater','mobj',mobj)
%   -for description of constructor check out SimulationBase
function simobj=SimulationBase(varargin)
%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'stimchooser','updater','mobj','observer'};
con(1).cfun=1;

con(2).rparams={'fname','simvar'};
con(2).cfun=2;

%***********************************************************
simobj=struct('simver',070726,'bname','StimChooserObj');


%create the optimzation structure to specify the options for calls to
%optimization functions
%use default values
%simparam.optparam=optimset();


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        simobj=class(simobj,'Simbatch');
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
    
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=SimulationBase(params);
simobj=class(simobj,'SimBatch',pbase);











