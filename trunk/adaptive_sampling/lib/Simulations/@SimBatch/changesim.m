%function changesim(sobj,field,val)
%       field - name of field to change
%       val   - value to set it
%
%Explanation: This is a special function to allow fields to be changed
%which shouldn't really be changed.
%
%I.e suppose I use newton1d to generate some data
%   afterwards I go back and recompute the posterior using the batch
%   updater. Now I want to save the data 
%   So I want to change the .updater field to BatchML
function sobj=changesim(sobj,field,val)
    warning('You are changing a criticial field of the object\n Make sure you know what you are doing');
    
    sobj.(field)=val;
    