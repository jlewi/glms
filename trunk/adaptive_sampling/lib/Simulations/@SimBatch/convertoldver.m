%function convertoldver(mobj,'observermodel',mobserver)
%   mobj
%   mobserver - MParamObj to assign to the observer
%
%Explanation: This function is intended to be used to handle conversion of
%old versions of MParamObj to the new version.
%Thus it allows the GLM field to be set as in the old version this was not
%a field of mparamobj.
function sobj=convertoldver(sobj,varargin)

for j=1:2:length(varargin)
    switch lower(varargin{j})
        case {'observermodel'}
            sobj.observer=convertoldver(sobj.observer,'model',varargin{j+1});
    end
end
