function obj = subsasgn(obj,index,val)
% SUBSASGN Define index assignment for ASResults objects
switch index(1).type
    case '.'
        switch index(1).subs
            %for backwards compatibility with my old structure
            %allow user to specify data as obj.newton1d.m
            %this will return obj.m
            case 'niter'
                obj.niter=val;

                %out=subsasgn(obj,index(2:end),val);
                %out=obj.(index(2:end),val);
                return;
            case 'stimchooser'
                obj.stimchooser=val;
                return;
            case 'simid'
                obj.simid=val;
                return;
            case 'extra'
                if length(size(index,2)>2)
                    obj.extra=subsasgn(obj.extra,index(1,2:end),val)
                else
                    obj.extra=val
                end
            otherwise
                error(sprintf('field %s cannot be changed ', index(1).subs));
                %out=subsasgn(obj.2
                %              if (length(index)==2)
                %                  switch index(2).type
                %                    case '()'
                %                    obj.data.(index(1).subs)(index(2).subs{:})=val;
                %                  case '{}'
                %                 obj.data.(index(1).subs){index(2).subs{:}}=val;
                %        end
                % else
                %   obj.data.(index(1).subs)=val;
        end
end

