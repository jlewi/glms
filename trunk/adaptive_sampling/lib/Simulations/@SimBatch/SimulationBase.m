%function sim=SimulationBase('stimchooser',stimchooser,'observer', osbrv, 'updater',updater','mobj',mobj)
%
%       -load the specified simulation from the file.
%       -simparam should be a variable named
% Explanation: constructor for the simulation object
%
%function sim=SimulationBase('fname',fname,'simvar',var)
%   fname - filename containing the simulation object
%   var   - name of the simulation object to load
%Revision History
%   7-26-2007 ver 070726
%       I want to encapsulate the update functions inside this object.
%       I make the data a member of this class by adding two objects
%       post - information about the posterior
%            -this is a structure array consisting of the fields
%            .m and .c
%       sr   - information about the inputs and responses
%            - this is also a structure array
%       I use structure arrays because appending data to a structure array
%       should be an O(n) time operation
%       structure arrays will also make it easy to split the data among
%       many files so that we don't have to load all of the data to
%       continue very long simulations.
%
%       mobj  - model object
function simobj=SimulationBase(varargin)
%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'stimchooser','updater','mobj','observer'};
con(1).cfun=1;

con(2).rparams={'fname','simvar'};
con(2).cfun=2;

%**********************************************************
% rstate         - state for the random number generation
%                       this is a structure array
%                          .rstartn - randn state
%                          .rstart  - rand state
%                          .rfinishn
%                          .rfinish
%                   - each entry stores the random number state at the
%                   start and end of a simulation
%                   -this is controlled by the functions saveendstate
% optparam  - parameters for optimization routines
% niter           - number of iterations
% glm           - 03-18-07: no longer a parameter
%                   general linear model
%stimchooser  - object to pick the stimuli
%observer     - object to label the stimuli
%updater       - PostUpdater object to update the posterior
%lowmem      - controls how much data we should save
%           - set to inf to not record any covariance matrices
%simid          - string identifying the simulation i.e 'rand' or 'max'
%                         this used to be called varsuffix
%                   -when saved to file the object is labeled 'simsimid'
%simfile        - name of the file to which the simulation was saved.
%simcont      - indicates whether this is a continuation of a previous
%                       simulation or a new simulation
%simver        - used to store version number for the simulation
%                     format is yearmonthday. This way later versions are
%                     increasing
%extra          - optional
%                   this structure stores any additional variables we want
%                   intention is it can store additional information used
%                   to construct the parameters etc.
%label          - label for use in plots
%                 added 3-28-2007
%
%deleted        - structure to store deleted fields
%               - this allows loadsim to access the deleted fields in order
%               to handle conversion of old structures
%               -added 3-28-2007
optparam=optimset('TolX',10^-12,'TolF',10^-12);

simobj=struct('simver',070726,'rstate',[],'optparm',optparam,'niter',0,'stimchooser',[],'observer',[],'updater',[],'lowmem',1,'simid','','simcont',0,'extra',[],'label',[],'deleted',[],'post',struct('m',[],'c',[],'entropy',[],'uinfo',[]),'mobj',[],'sr',struct('y',[],'nspikes',[],'stiminfo',[]));


%create the optimzation structure to specify the options for calls to
%optimization functions
%use default values
%simparam.optparam=optimset();




switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        simobj=class(simobj,'SimulationBase');
        return
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

if (length(cind)>1)
    error('Matched multiple constructors. Resolve this');
end

switch cind
    case 1
        %process the parameters
        pnames=fieldnames(params);
        for j=1:length(pnames)

            switch pnames{j}
                case 'observer'
                    simobj.observer=params.(pnames{j});
                    if ~isa(simobj.observer,'ObserverBase');
                        error('observer must be derived from observerbase');
                    end

                case 'stimchooser'
                    simobj.stimchooser=params.(pnames{j});;
                    if ~isa(simobj.stimchooser,'StimChooserObj');
                        error('stimchooser must be derived from stimchooserobj');
                    end

                case 'updater'
                    simobj.updater=params.(pnames{j});
                    if ~isa(simobj.updater,'PostUpdatersBase');
                        error('updater must be derived from PostUpdatersBase');
                    end
                case 'mobj'
                    simobj.mobj=params.(pnames{j});
                    if ~isa(simobj.mobj,'MParamObj')
                        error('mobj must be an MParamObj');
                    end

                case 'simid'
                    simobj.simid=params.(pnames{j});

                case 'glm'
                    warning('GLM should no longer be given as parameter for SimulationBase');
                case 'extra'
                    simobj.extra=params.(pnames{j});
                case 'label'
                    simobj.label=params.(pnames{j});
                case 'lowmem'
                    simobj.lowmem=params.(pnames{j});
                otherwise
                    error('%s unrecognized parameter ',pnames{j});
            end
        end

        %create a new simulation
        simobj=class(simobj,'SimulationBase');

        %initialize post to the prior
        simobj.post.m=simobj.mobj.pinit.m;
        simobj.post.c=simobj.mobj.pinit.c;
        %**********************************************************************
        %Print warning message about misspecification
        %**********************************************************************

        %check for misspecification is a bit of a hack because observer may not have a glm
        if (isglmspecific(simobj.stimchooser)==1)
            %we only do the check if stimchooser depends on the glm
            %should really compare the MParamObjects
            %if ~isequal(simobj.observer.model.glm,simobj.stimchooser.glm)
            %    warning('SimulationBase:ModelMisspec','MODEL IS MISSPECIFIED. stimchooser and observer are using different GLMs');
            %end
        end
    case 2
        %load simobj from a file
        try
            simobj=load(params.fname,params.simvar);
           
        catch
            error('could not load %s in file %s ', params.simvar,params.fname);
        end
         simobj=simobj.(params.simvar);
    otherwise
        error('constructor not implemented');

end










