%function oinfo=objinfo(obj)
%   obj - object
%
%oinfo - 2d cell array which describes this object
%
%Explanation: idea is to populate a cell array with information about this
%object. We can then pass this cell array to oneNoteTable to create a table
%which can be imported into matlab.
%
%$Revision: 1474 $ - use structinfo to create a cell array which contains
% information about the fields of this object. This is useful for creating 
% tables to send to OneNote.
%
function oinfo=objinfo(obj)

oinfo={'Simulation',class(obj)};

s=struct(obj);

%remove fields we don't want to grab info for
try
s=rmfield(s,{'extra','simcont','rstate','post','sr','deleted'});
catch    
    warning('%s.objinfo: %s \n',class(obj),lasterr);
end

%remove from optim any empty fields
fnames=fieldnames(s.optparm);
for j=1:length(fnames)
    if isempty(s.optparm.(fnames{j}))
        s.optparm=rmfield(s.optparm,fnames{j});
    end
end
%s.mobj=objinfo(s.mobj);
%s.stimchooser=objinfo(s.stimchooser);
%s.updater=objinfo(s.updater);
oinfo=[oinfo;structinfo(s)];



    
