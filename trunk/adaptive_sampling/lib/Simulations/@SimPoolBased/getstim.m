%function stim=getgetstim(obj,trial)
%	 obj=SimulationBase object
%    trial = which trials on which to get the stimuli
%Return value: 
%	 stim=obj.sr(trial).y
%
%Overload getstim because we save the stimulus index on each trial
%   and from this we want to get the actual stimulus
function stim=getstim(obj,trial)
if isa(obj.sr,'SRObj')    
	 stim=[getinput(obj.sr(trial))];
     
     if isa(stim,'GLMStimIndex')
        stim=obj.stimobj.stim([stim.index]); 
     end
else
	 stim=[obj.sr(trial).y];
end
