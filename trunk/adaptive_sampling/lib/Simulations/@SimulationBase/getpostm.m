%function m=getpostm(simobj,trial)
%   simobj - Simulation Object
%   trial    - trials on which to retun the posterior mean
%           trial=0 - prior.
%
% Explanation: gets the posterior mean for the
% specified trial
function m=getpostm(simobj,trial)

m=getm(simobj.allpost,trial);
