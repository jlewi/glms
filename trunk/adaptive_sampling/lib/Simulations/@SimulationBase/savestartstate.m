%funciton [sobj=saveendstate(sobj)
%   
% Explanation: Save the current state of the random number generators as
% the start state of the current batch of trials. This allows the random
% number generators to be reset to the same state.
function [sobj]=savestartstate(sobj)

    %we add an element to the rstate structure array in which 
    %to save the start and end states for the current batch of trials
    %we set the finishing states to Nan to indicate we haven't saved those
    %states yet
    ind=length(sobj.rstate)+1;

    
    sobj.rstate(ind).rstartn=randn('state');
    sobj.rstate(ind).rstart=rand('state');
    sobj.rstate(ind).rfinishn=nan;
    sobj.rstate(ind).rfinish=nan;
