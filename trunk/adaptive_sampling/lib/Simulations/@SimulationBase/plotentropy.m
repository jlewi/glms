%function [ffilt]=plotmse(simobj)
%   simobj - a simulation object or array of object
%
%Explanation makes a plot of the entropy
%
function [ffilt]=plotentropy(simobj)

pstyles=PlotStyles();

pstyles.order={'colors','lstyles','markers'};
ffilt=FigObj('width',6.3,'height',4,'name','Entropy of posterior','naxes',[1,1],'xlabel','trial','ylabel','Entropy(bits)');

%*********************************************
%plot the estimated filters
pind=0;
for dind=1:length(simobj)

    %plot the mean of the full posterior

    ent=simobj(dind).entropy(1:simobj(dind).niter);
   
    if ~isempty(ent)
    iind=1:getniter(simobj(dind));

   
    


    hp=plot(iind,ent);
    
    pind=pind+1;
    pstyle=plotstyle(pstyles,pind);
    
    ffilt.a=addplot(ffilt.a,'hp',hp,'pstyle',pstyle,'lbl',getlabel(simobj(dind)));
    

    end

end

lblgraph(ffilt);

setposition(ffilt.a.hlgnd,.5,.6,.35,[]);
set(ffilt.a.ha,'xscale','log');