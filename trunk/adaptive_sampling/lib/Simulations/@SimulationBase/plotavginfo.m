%function [ffilt]=plotmse(simobj)
%   simobj - a simulation object or array of object
%
%Explanation makes a plot of the average information per trial
%   average information per trial is the negative entropy divided by the
%   number of trials
function [ffilt]=plotavginfo(simobj)

pstyles=PlotStyles();

pstyles.order={'colors','lstyles','markers'};
ffilt=FigObj('width',6.3,'height',4,'name','Average Information per trial','naxes',[1,1],'xlabel','trial','ylabel','Avg Information(bits) per trial');

%*********************************************
%plot the estimated filters
pind=0;
for dind=1:length(simobj)

    %plot the mean of the full posterior

    ent=simobj(dind).entropy;
   
    if ~isempty(ent)
    iind=1:getniter(simobj(dind));

   
    


    hp=plot(iind,-ent-getparamlen(simobj(dind).mobj)*log(iind));
    
    pind=pind+1;
    pstyle=plotstyle(pstyles,pind);
    
    ffilt.a=addplot(ffilt.a,'hp',hp,'pstyle',pstyle,'lbl',getlabel(simobj(dind)));
    

    end

end

lblgraph(ffilt);

setposition(ffilt.a.hlgnd,.5,.6,.35,[]);
set(ffilt.a.ha,'xscale','log');