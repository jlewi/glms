%function obsrv=plotobsrvfrac(simobj)
%
%Explanation: make a bar plot showing the fraction of trials
%      for which the observation took on some discrete value.
function fh=plotobsrvfrac(simobj)

    fh=	FigObj('name','Observation histogram','xlabel','obsrv','ylabel','fraction');
    
    data=struct('counts',[],'obsrv',[],'frac',[]);
    data=repmat(data,length(simobj),1);
    
    for sind=1:length(simobj)
        o=getobsrv(simobj(sind).obsrv);
        bc=[0:max(o)];
        [data(sind).counts data(sind).obsrv]=hist(o,bc);
       
    end
    
    obsrv=unique([data.obsrv]);
    
    otoind=nan(1,max(obsrv));
    otoind(obsrv+1)=1:length(obsrv);
    
    
    allfrac=zeros(length(obsrv),length(simobj));
    for sind=1:length(simobj)
        counts=zeros(1,length(obsrv));
        
        counts(otoind(data(sind).obsrv+1))=data(sind).counts;
        data(sind).counts=counts;            
        data(sind).frac=data(sind).counts/sum(data(sind).counts);
        
        allfrac(:,sind)=data(sind).frac;
    end

    

    
    hb=bar(obsrv,allfrac);
    legend({simobj.label})