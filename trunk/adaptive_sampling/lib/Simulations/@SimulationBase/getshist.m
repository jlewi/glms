%function shist=getshist(simobj,trial)
%   trial - trial on which to get the spike history
%
%Return value:
%   shist - the spike history for trial trial
function shist=getshist(simobj,trial)


       if (gethistdep(simobj.mobj)>0 && trial>1)
  shist=getobsrv(simobj,[max(1,trial-getshistlen(simobj.mobj)):(trial-1 )])';
       else
           shist=[];
       end