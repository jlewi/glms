%function obsrv=getobsrv(obj,trial)
%	 obj=Simulation base object
%    trial = which trial to get the observation for
%
%Return value: 
%	 obsrv=obj.sr(trial).nspikes
%       -returns a scalar or potentialy object or vector representing the
%       observation
function obsrv=getobsrv(obj,trial)


	 obsrv=getobsrv(obj.obsrv,trial);
