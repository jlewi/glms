%function [simobj,od]=savesim(simobj,fname,force);
%     simobj- simulation object to save to
%     fname - name of file to save to
%     force - set this to true to force the save
%             this forces the save if it has been dissallowed
%             This is meant as a check to prevent you from accidentally
%             overriding a simulation or a file
% Return value:
%   simobj - the object
% Explanation:
%   saves the data to a file
%
%
% Revisions:
%   01-10-2008 - revised for new object model.
%           Function is now intended to be used by the iterate function to
%           save the simulation
%       
function [simobj,od]=savesim(simobj,datafile,force)

if ~exist('force','var')
    force=false;
end
if ~(simobj.sinfo.allowed )
    if ~(force)
    error('cant save sim. Saving has been disallowed. Most likely because object was created from an old version.');
    end
end


 
        noerr=1;
        emsg='';
        try
            %save without append becasue append causes error
            if isa(datafile,'FilePath')
                %save(getpath(datafile),'simobj','-append');
                save(getpath(datafile),'simobj');
            else
                save(datafile,'simobj');
            end
            fprintf(simobj.statusfid,'Trial=%d: Saved the simulation \n',simobj.niter);
        catch e
            emsg=sprintf('Error occured while trying to do a periodic save of the data. Error message:\n %s\n',e.message);
            noerr=0
        end
        noerr=gcat(noerr);
        if any(noerr==0)
            %any error occured on one of the nodes
            if (noerr(labindex)==1)
                emsg=sprintf('Error occured on labs %d while trying to do a periodic save of the data. \n', find(noerr==0));
            end
            emsg=sprintf('Trial=%d: %s This error appears to be due to the use of the append option when saving the data. \n Simulation will continue without saving the data. \n',simobj.niter,emsg);
            fprintf(simobj.statusfid,'%s', emsg);

        end