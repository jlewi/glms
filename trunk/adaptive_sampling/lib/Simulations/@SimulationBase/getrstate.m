%function rstate=getrstate(sim)
%   sim - SimulationBase object
%
%Return value:
%   rstate
function rstate=getrstate(sim)
    rstate=sim.rstate;