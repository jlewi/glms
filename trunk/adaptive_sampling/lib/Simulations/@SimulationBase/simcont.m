%function bssimobj=simcont(datafile,ntorun,psize,datafile,statusfile)
%
%Explanation: This function loads a simjob from datafile
%   and calls iterate
%   The purpose of this function is to use with parallel jobs on the
%   cluster
%   On the cluster we don't want to actually load the simobj because we
%   haven't copied the file from the cluster yet
%
function simobj=simcont(datafile,ntorun,statusfile)


fid=fopen(getpath(statusfile),'a');
fprintf('lab %d: calling load \n', labindex);
fprintf(fid,'lab %d: calling load \n', labindex);
v=load(getpath(datafile),'simobj');
fprintf('lab %d: finished load \n', labindex);
fprintf(fid,'lab %d: finished load \n', labindex);
simobj=v.simobj;
fprintf(fid,'lab %d: calling iterate \n', labindex);
[simobj]=iterate(simobj,ntorun,datafile,statusfile);
fprintf(fid,'lab %d: finished iterate \n', labindex);
fclose(fid);