%function getfield=field(obj,fname)
%	 obj=SimulationBase object
% 
%Return value: 
%	 field=obj.field 
%   get value of the specified field
function fval=getfield(obj,fname)
warning('12-30-2008 This function is now obsolete. Access properties directly.');
	 fval=obj.(fname);
