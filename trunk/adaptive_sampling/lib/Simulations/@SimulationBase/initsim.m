%function [starttrial,lastttrial,prior,infoint,saveint,datafile]=initsim(simobj,ntorun)
%   simobj
%   ntorun
%Explanation: This is an internal function used to initialize the
%simulation which consists of 
%   saving state information about the run
%
function  [starttrial,lasttrial,prior,infoint,saveint]=initsim(simobj,ntorun,statusfile)

if ~exist('statusfile','var')
    statusfile=[];
end

printiter=10;   %how often to print out trial info.
fid=1;  %default to screen

%how many trials to run before saving the object and printing status
%information
%saveint must be a multiple of savecint because we only want to save the
%simulation if we saved the covariance matrix because otherwise we won't be
%able to continue the simulation

saveint=floor(simobj.saveint/simobj.savecint)*simobj.savecint;

%info int is the interval at which to print out the trial number
%infoint get set to min(infoint, ceil(.1*ntorun));
infoint=10;
starttrial=simobj.niter+1;
lasttrial=starttrial+ntorun-1;
ntorun=lasttrial-starttrial+1;

infoint=min([infoint .1*ntorun]);

%open the status file
if isempty(statusfile)
    fid=1; %standard output
    simobj.statusfid=fid;
else
    %make sure the directory to contain the statusfile exists

    fdir=fileparts(getpath(statusfile));
    recmkdir(fdir);
    fid=fopen(getpath(statusfile),'a');
    simobj.statusfid=fid;
end

%****************************************************************
%update the structure storing information about this run
%**************************************************************
runnum=length(simobj.runs)+1;
simobj.runs(runnum).starttrial=starttrial;
simobj.runs(runnum).ntrials=(lasttrial-starttrial)+1;
fprintf(fid, 'starttrial=%d \nntrials=%d \n',starttrial,simobj.runs(runnum).ntrials);

[stat,svnr]=system('~/svn_trunk/scripts/get-svn-revision');
simobj.runs(runnum).svnrevision=svnr;
fprintf(fid, 'svn revision=%s \n',svnr);

job=getCurrentJob();

if ~isempty(job)
    simobj.runs(runnum).jobid=get(job,'id');
end

%*****************************************************************
%get the prior
%******************************************************************

prior=getpost(simobj.allpost,starttrial-1);

%make sure prior doesn't represent c is a distributed matrix
%I think isdistributed would be better but I don't thing its impelemnted
%untill 2008a
%DISTRIBUTED MATRIX FOR C WILL CAUSE PROBLEMS because we use a parallel for
%loop
if isdistributed(getc(prior))
    prior=GaussPost('m',getm(prior),'c',EigObj('matrix',gather(getc(prior))));
end

if isempty(getc(prior))
    error('We cannot continue the simulation because the covariance matrix on the lasttrial was not saved');
end




%**************************************************************************
%old function
%get a reference to updater
updater=getupdater(simobj);
mobj=getmobj(simobj);

%********************************************************************
%Initialize/Load simulation
%*****************************************************

post=[];



%information about the continuing simulation
% continfo.piter=getniter(simobj); %number of previous iterations
% continfo.ntorun=ntorun;            %additional number of iterations to run
% continfo.totaliter=continfo.piter+continfo.ntorun;


%initialize the random number generators to the state we left off
%on
rstate=getrstate(simobj);
if (length(rstate)>1)
    randn('state',rstate(end).rfinishn);
    rand('state',rstate(end).rfinish);
end



%*******************************************************
%stucture to store timing info
timing.update=zeros(1,ntorun);
timing.choosestim=zeros(1,ntorun);
timing.total=zeros(1,ntorun);

%tell simparam to save the current start state of the random number
%generators as the start state.
%this is not backwards compatible
simobj=savestartstate(simobj);


%initalize the entropy array
if (simobj.compentropy)
   entropy=simobj.entropy;
   simobj.entropy=nan(1,lasttrial);
   
   indlast=min(length(entropy),starttrial-1);
   simobj.entropy(1:indlast)=entropy(1:indlast);
end

simobj.lasttrial=lasttrial;

%initialize the buffer for the inputs
initbuffer(simobj.inputs,simobj.niter+1,simobj.lasttrial);
initbuffer(simobj.obsrv,simobj.niter+1,simobj.lasttrial);