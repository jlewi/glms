%function obj=setpost(obj,trial,post)
%	 obj=SimulationBase object
%    trial - set the posterior after the "trial"- trial to post
%function setpost(obj,post)
%    set the array of post objects to post
%       must have same length as current array
%Return value: 
%	 obj= the modified object 
%
function obj=setpost(obj,trial,val)


error('12-30-2008 this function is oboslete.');
if (nargin==3)
      %add 1 to the trial because we want to convert trial to an index
     %the post object.
	 obj.post(trial+1)=val;
elseif (nargin==2)
   if length(trial)==length(obj.post)
        obj.post=trial;
   else
       error('In order to override the post array you must supply an array of equal length');
   end
end
