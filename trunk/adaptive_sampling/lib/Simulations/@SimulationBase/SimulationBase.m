%function sim=SimulationBase('stimchooser',stimchooser,'observer', osbrv, 'updater',updater','mobj',mobj)
%
%       obsrvfile - the name of the file to store the observations
%       inputsfile - name of the file to store the inputs
%
%       -load the specified simulation from the file.
%       -simparam should be a variable named
% Explanation: constructor for the simulation object
%
%function sim=SimulationBase('fname',fname,'simvar',var)
%   fname - filename containing the simulation object
%   var   - name of the simulation object to load
%
%function sim=SimulationBase('dsets',dsets)
%   dsets - structure array of datasets to load
%         .fname
%         .simvar
%Revision History
%   01-23-2009
%       added field about
%       deleted field simid
%   01-20-2009
%       replaced sr field by InputsFile and ObsrFile
%   01-10-2009 
%             - add fields entropy and compentropy
%               if compentropy is true then we compute the entropy
%             - add field idnum
%   01-08-2009 - replace the field simver by version
%   12-30-2008
%          -Updated to new Matlab object model
%          - Breaks backward compatibility
%          - renamed field lowmem to savecint
%   03-16-2008
%          -rename the internal function loadsimulation so it has different
%          name from function loadsim
%          -there was bug in my conversion of data older than 7-27-2007
%           This bug led to an off by one error with the means.
%   03-13-2008
%          - Added the array svn revisions
%          - This trial stores information about what revision of the code
%          was used to run the simulation
%           - Note: This revision number is only accurate if all changes
%           have been committed and we have updated to the latest
%           repository version.
%   12-27-2007
%           - when loading files older then 7-27-2007, convert
%             sr structure to an array of SrObj
%   071024  - posterior is array of GaussPost
%           -added backwards compatibility - convert old file formats
%           (ver<070726) to new object model
%           -added sinfo structure
%   7-26-2007 ver 070726
%       I want to encapsulate the update functions inside this object.
%       I make the data a member of this class by adding two objects
%       post - information about the posterior
%            -this is a structure array consisting of the fields
%            .m and .c
%       sr   - information about the inputs and responses
%            - this is also a structure array
%       I use structure arrays because appending data to a structure array
%       should be an O(n) time operation
%       structure arrays will also make it easy to split the data among
%       many files so that we don't have to load all of the data to
%       continue very long simulations.
%
%       mobj  - model object

classdef (ConstructOnLoad=true) SimulationBase < handle

    properties(SetAccess=protected,GetAccess=public)
        %**********************************************************
        %about           - descriptiono of the simulation
        % rstate         - state for the random number generation
        %                       this is a structure array
        %                          .rstartn - randn state
        %                          .rstart  - rand state
        %                          .rfinishn
        %                          .rfinish
        %                   - each entry stores the random number state at the
        %                   start and end of a simulation
        %                   -this is controlled by the functions saveendstate
        % optparam  - parameters for optimization routines
        % niter           - number of iterations
        % glm           - 03-18-07: no longer a parameter
        %                   general linear model
        %stimchooser  - object to pick the stimuli
        %observer     - object to label the stimuli
        %updater       - PostUpdater object to update the posterior
        %lowmem      - controls how much data we should save
        %           - set to inf to not record any covariance matrices
        %
        %simfile        - name of the file to which the simulation was saved.     
        %version        - used to store version number for the simulation
        %                     format is yearmonthday. This way later versions are
        %                     increasing
        %extra          - optional
        %                   this structure stores any additional variables we want
        %                   intention is it can store additional information used
        %                   to construct the parameters etc.
        %label          - label for use in plots
        %                 added 3-28-2007
        %
        %deleted        - structure to store deleted fields
        %               - this allows loadsim to access the deleted fields in order
        %               to handle conversion of old structures
        %               -added 3-28-2007
        %sinfo          -info about saving the object.
        %               -I set this to false if I want to prevent the object in a
        %               file from being overwritten.
        %      .allowed - whether
        %maxspikes      - is the number of observed spikes is larger than this
        %                 number then we throw an error
        %                 this avoids numerical errors caused by really large
        %                 spikes
        %allpost        - A GaussPostFile object to store all the postierors
        %                 includes the prior
        %savecint   - how often to save the covariance matrix
        %          - inf means only save the first and last covariance
        %          matrix
        %          = 1 - save it on every trial
        %label - string to describe this simulation in plots
        %saveint   - the interval at which to save the results to a file
        %             if one is provided
        % runs     - A structure array each entry keeps track of information about
        %            a different run
        %           .starttrial - first trial
        %           .ntrials - number trials run
        %           .svnrevision - revision number of the code
        %           .lab1host  - store the hostname of lab1
        %           .jobid
        %entropy   - store the entory if told to compute it
        %
        %compentropy - indicates we want to compute the entropy
        %
        %inputs   - A InputsFile object to access the inputs
        %obsrv    - A ObsrvFile object to access the observations
        about='';
        savecint=1;
        saveint=500;
       % simver=081230;
        rstate=[];
        optparm=optimset('TolX',10^-12,'TolF',10^-12);
        niter=0;
        stimchooser=[];
        observer=[];
        updater=[];
        extra=[];
        label=[];
        deleted=[];
        mobj=[];
        sr=[];
        sinfo=struct('allowed',true);
        maxspikes=20000;     
        runs=[];
         allpost=[];
         entropy=[];
         
         inputs=[];
         obsrv=[];
    end
    properties(SetAccess=private,GetAccess=public)
        %idnum is a time stamp which serves to provide a unique
        %identifier number for each simulation. 
        %this number is first generated when the SImulationBase object is
        %created. This way classes can store information about the
        %simulation and then verify that they are accessing the same
        %simulation
       idnum=[]; 
    end
    properties(SetAccess=protected,GetAccess=public)
        
        version=struct();
    end
    properties(SetAccess=public,GetAccess=public)
        
       compentropy=false;
    end

    %11-09-2009 make it protected access.
   properties(SetAccess=protected,GetAccess=public,Transient)
        %the file id where we want to print status messages to
        %we make this a member variable so that other objects
        %can print messages to this file.
        %statusfid gets set in iterate
       statusfid=1;
       
       %keep track of the number of simulations we are running.
       %this way classes can use it for things like memory allocation.
       lasttrial=[];
       
     
   end
    properties(SetAccess=private,GetAccess=public,Transient)
       %datafile - the file we loaded the simulation from
       %           and we should save it to.
       datafile=[]; 
    end
    methods
        function simobj=SimulationBase(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'stimchooser','updater','mobj','observer','allpost','obsrvfile','inputsfile'};
           

            con(2).rparams={'fname','simvar'};
           

            con(3).rparams={'dsets'};
           
            con(4).rparams={'datafile'};
            
         %determine the constructor given the input parameters
             [cind,params]=Constructor.id(varargin,con);
           
 %**************************************************************************
            %otherwise parse out which parameters are for this class and
           simobj.idnum=datestr(now,'yymmddHHMMSS');  %which are for the base class
            %********************************************************************
            switch cind
                case {1,2,3}
                case {4}
                    bparams=rmfield(params,'datafile');
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                       %process the parameters
                    pnames=fieldnames(params);
                    for j=1:length(pnames)

                        switch pnames{j}
                            case 'observer'
                                simobj.observer=params.(pnames{j});
                                if ~isa(simobj.observer,'ObserverBase');
                                    error('observer must be derived from observerbase');
                                end

                            case 'stimchooser'
                                simobj.stimchooser=params.(pnames{j});
                                if ~isa(simobj.stimchooser,'StimChooserObj');
                                    error('stimchooser must be derived from stimchooserobj');
                                end

                            case 'updater'
                                simobj.updater=params.(pnames{j});
                                if ~isa(simobj.updater,'PostUpdatersBase');
                                    error('updater must be derived from PostUpdatersBase');
                                end
                            case 'mobj'
                                simobj.mobj=params.(pnames{j});
                                if ~isa(simobj.mobj,'MParamObj')
                                    error('mobj must be an MParamObj');
                                end

                            case 'simid'
                                simobj.simid=params.(pnames{j});

                            case 'glm'
                                warning('GLM should no longer be given as parameter for SimulationBase');
                            case 'extra'
                                simobj.extra=params.(pnames{j});
                            case 'label'
                                simobj.label=params.(pnames{j});
                            case 'lowmem'
                                error('field lowmem has been replaced by field savecint');
                            case 'savecint'
                                simobj.savecint=params.(pnames{j});
                             case 'saveint'
                                simobj.saveint=params.(pnames{j});
                            case 'allpost'
                                simobj.allpost=params.(pnames{j});
                            case 'label'
                                simobj.label=params.(pnames{j});
                            case 'compentropy'
                                 simobj.compentropy=params.(pnames{j});
                            case 'inputsfile'
                            case  'obsrvfile'
                            case 'about'
                                simobj.about=params.(pnames{j});
                            otherwise
                                error('%s unrecognized parameter ',pnames{j});
                        end
                    end
                    
                    %create the inputs and observation objects
                    simobj.inputs=InputsFile('fname',params.inputsfile,'dim',[simobj.mobj.klength 1]);
                    simobj.obsrv=ObsrvFile('fname',params.obsrvfile,'dim',[1 1]);
                    

                    %**********************************************************************
                    %Print warning message about misspecification
                    %**********************************************************************

                    %check for misspecification is a bit of a hack because observer may not have a glm
                    if (isglmspecific(simobj.stimchooser)==1)
                        %we only do the check if stimchooser depends on the glm
                        %should really compare the MParamObjects
                        %if ~isequal(simobj.observer.model.glm,simobj.stimchooser.glm)
                        %    warning('SimulationBase:ModelMisspec','MODEL IS MISSPECIFIED. stimchooser and observer are using different GLMs');
                        %end
                    end
                 case 2
                     warning('01-08-2008 started writing constructor to handle conversion of old object.');
                                          
                    simobj=SimulationBase.loadoldsim(params.fname,params.simvar);
                 case 3
                     error('12-30-2008 not sure how to process this constructor since converting the object to the new object model.');
                     
                         dsets=params.dsets;
                    if ~isfield(dsets,'fname')
                        error('dsets must have field fname');
                    end
                    if ~isfield(dsets,'simvar')
                        error('dsets must have field simvar');
                    end

                    %load all the data sets
                    clear simobj;
                    for dind=1:length(dsets)
                        simobj(dind)=loadsimulation(dsets(dind).fname,dsets(dind).simvar);
                    end
                 case 4
                     v=load(getpath(param.datafile));
                     simobj=v.simobj;
                     simobj.datafile=param.datafile;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end



            %create the optimzation structure to specify the options for calls to
            %optimization functions
            %use default values
            %simparam.optparam=optimset();



            simobj.version.SimulationBase=090123;
           

            %generate a timestamp for idnum
            %this will be overwritten by loadobj
            %this should use 24 notation
            simobj.idnum=str2double([datestr(now,'yymmdd') datestr(now,'HHMMSS')]);
        end

        
        %rollback the simulation to an earlier trial.
        rollback(simobj,trial);
    end
    methods(Access=protected)
        [starttrial,lasttrial,prior,infoint,saveint,datafile]=initsim(simobj,ntorun,statusfile); 
        endsim(simobj,datafile);
        
    end
    methods(Static)
       %convert an oldversion of the simulation
       simobj=loadoldsim(fname,simvar);
          
       %load and continue a simulation
       %this is useful for running on the cluster.
       simobj=simcont(datafile,ntorun,statusfile);
    end
end







