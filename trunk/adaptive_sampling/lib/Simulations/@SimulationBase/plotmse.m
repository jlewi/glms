%function [ffilt]=plotmse(simobj,subsamp)
%   simobj - a simulation object or array of object
%   subsamp - how often to subsample the data before plotting
%
%Explanation makes a plot of the posterior means as an image
%
%
%4-22-2009 - Before I was just computing the distance between the MAP and
%the true parameters. Now I'm computing the expected value.
%
% I don't know that this is actually the MSE. Really what we are computing
% is the average squared distance between the mean and true parameters
function [ffilt]=plotmse(simobj,subsamp)

error('THis function has been renamed plotmsd to be more accurate.');
