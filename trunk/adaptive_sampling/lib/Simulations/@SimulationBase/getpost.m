%function post=getpost(obj,trial)
%	 obj=SimulationBase object
%    trial - Return the posterior after "n=trial" trials
%          - trial can be array
%          -WARNING trial=0 = prior;
%Return value: 
%	 post=obj.post(trial)
%
function post=getpost(obj,trial)
if ~exist('trial','var')
    %return them all
    post=getpost(obj.allpost);
else

	 ost=getpost(obj.allpost,trial);
end
