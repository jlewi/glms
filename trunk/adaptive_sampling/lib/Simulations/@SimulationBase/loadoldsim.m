%load the simulation from a file handling any conversion as needed
%
%Explanation: 01-08-2008
%   Handles the loading and conversion of one of my older simulations
%
function simobj=loadoldsim(fname,simvar)

%load simobj from a file
%  try
%check if its an old version of the object model and if so
%convert it
oldver=0;
%12-10-07
%calling who is very slow so just use a try block instead
%            vinfile=who('-file',fname);

if isa(fname,'FilePath')
    fname=getpath(fname);
end
try
    fparam=load(fname,'fparam');
    fparam=fparam.fparam;
    %in versions older than 070727
    %filever may have been stored as field filver
    if (isfield(fparam,'filver') && ~isfield(fparam,'filever'))
        fparam.filever=fparam.filver;
    end

    if(fparam.filever<070727)
        oldver=1;
    end
catch
    %since we couldn't load fparam it is old version
    oldver=1;
end

if ~(oldver)

    %turn the jit feature off otherwise matlab will crash
    %when loading old MCOS objects
    feature jitallow off;
    %its the newest version
    %do not put  a try block arround the following code
    %it will hide important errors
    simobj=load(fname,simvar);
    %catch
    % error('could not load %s in file %s ', simvar,fname);
    %end
    simobj=simobj.(simvar);
    setpathvars;
    [fdir, fbase]=fileparts(fname);
    if (strcmpi(fdir(1:length(RESULTSDIR)),RESULTSDIR))
        odir=fdir(length(RESULTSDIR)+1:end);
        mfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_mfile.dat',fbase)));
        cfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_cfile.dat',fbase)));
    else
        error('could not properly parse out RESULTSDIR in the path. Code it up to use the absolute path');
    end

    f=[mfile cfile];
    for ind=1:2

        if exist(getpath(f(ind)),'file')
            fprintf('The File %s \n already exists \n', getpath(f(ind)));
            y=input('Do you want to overwrite the file (y/n)? \n','s');
            switch lower(y)
                case {'y','yes'}
                    %delete the file
                    delete(getpath(f(ind)));
                otherwise
                    f(ind)=seqfname(f(ind));
            end
        end
    end

    mfile=f(1);
    cfile=f(2);

    post=simobj.allpost;

    prior=post(1);
    simobj.allpost=GaussPostFile('mfile',mfile,'cfile',cfile,'m',getm(prior),'c',getc(prior),'id',0);

    %first trial is the prior
    
    for ind=2:length(post)
        %subtract 1 because first entry is the prior
        trial=ind-1;
        setm(simobj.allpost,getm(post(ind)),trial);
        c=getc(post(ind));
        if ~isempty(c)
            setc(simobj.allpost,c,trial);
        end
    end

    if (length(post)==1)
       warning('The posterior objects could not be loaded. Matlab loaded a single object instead of an array. \n You will need to manually convert the posteriors.\n To do this load the datafile but make sure none of the classes are on the Matlab path.\n This will cause the object to be loaded as a structure.');  
    end
    %turn jit back on
    %when loading old MCOS objects
    feature jitallow on;
   
else
    %convert the old version

    [pdata, simparam, mparam, sr]=loadsim('simfile',fname,'simvar',simvar);

    warning('File is older than 07-27-07. Converting to new version. Data loss could occur');

    %disable saving so that we can't overwrite the file we
    %loaded it from
    simobj=simparam;
    simobj.sinfo.allowed=false;

    %convert sr to an object
    clear srall;
    for d=1:size(sr.y,2)
        srd.y=GLMInputVec('data',sr.y(:,d));
        srd.nspikes=sr.nspikes(d);
        srd.stiminfo=sr.stiminfo{d};
        srall(d)=SrObj('sr',srd);
    end
    simobj.sr=srall;
    simobj.mobj=mparam;
    %copy the posteriors
    %pinit=getpinit(mparam);
    %               simobj.post=GaussPost('m',pinit.m,'c',pinit.c);

    %3-17-2008
    %I believe that in the old version pdata.m(:,1), pdata.c{1} stored
    %the prior we check this by making sure the number of
    %observations is 1 less than the number of columns of
    %pdata.m
    if (length(simobj.sr)+1==size(pdata.m,2))
        %we need to initialize .post so we can grow it
        simobj.post=GaussPost('m',pdata.m(:,1),'c',pdata.c{1});
        for d=2:size(pdata.m,2)
            simobj.post(d)=GaussPost('m',pdata.m(:,d),'c',pdata.c{d},'entropy',pdata.entropy(d));
            %simobj.post(d+1)=gaussPost('m',pdata.m(:,d),'c',[],'entropy',pdata.entropy(d));
        end
    else
        %This conversion is easy I just didn't bother to write
        %code for it because I don't think it happens.
        error('Need to write code for when pinit is not first entry of pdata.m');
    end


end
end