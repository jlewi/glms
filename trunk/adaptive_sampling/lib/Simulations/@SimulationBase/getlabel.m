%function getlabel=label(obj)
%	 obj=SimulationBase object
% 
%Return value: 
%	 label=obj.label 
%
function label=getlabel(obj)
	 label=obj.label;
     
     if isempty(label)
         label=obj.simid;
     end
