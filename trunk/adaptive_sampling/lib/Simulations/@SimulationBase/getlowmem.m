%function lowmem=getlowmem(obj)
%	 obj=lowmem object
% 
%Return value: 
%	 prop=obj.lowmem 
%
function lowmem=getlowmem(obj)
	 lowmem=obj.lowmem;
