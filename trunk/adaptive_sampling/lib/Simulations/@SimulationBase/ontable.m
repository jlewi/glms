%function ontable(obj,fname)
%   obj-SimualtionBase object
%   fname - filename to save xml to 
%
%Explanation: Create an xml decription of this simulation in the specified
%file
function ontable(obj,fname)

tinfo=objinfo(obj);
onenotetable(tinfo,fname);