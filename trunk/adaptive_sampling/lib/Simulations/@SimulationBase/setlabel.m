%function obj=setlabel(obj,val)
%	 obj=SimulationBase object
% 
%Return value: 
%	 obj= the modified object 
%
function obj=setlabel(obj,val)
	 obj.label=val;
