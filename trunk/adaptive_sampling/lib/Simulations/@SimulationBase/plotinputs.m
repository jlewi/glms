%function plotobsrv(simobj)
%   simobj - simulation object or array of simulations
function finp=plotinputs(simobj)


nrows=length(simobj);
ncols=1;
finp=FigObj('width',6.3,'height',4,'name','Inputs','naxes',[nrows ncols]);


%*********************************************
%plot the estimated filters
for dind=1:length(simobj)


    setfocus(finp.a,dind,1);

	stim=getinput(simobj(dind).inputs,1:simobj(dind).niter);
    
%     if isa(simobj(dind).mobj,'MBSFTSep')
%         indstim=simobj(dind).mobj(1):simobj(dind).mobj(2);
%        stim=tospecdom(simobj(dind).mobj,stim(indstim));
%     end
    trials=1:simobj.niter;
    
    imagesc(trials,1:simobj(dind).mobj.klength,stim);
    
    finp.a(dind,1)=title(finp.a(dind,1),sprintf('Inputs: sim=%s',simobj(dind).label));
 
    finp.a(dind,1)=ylabel(finp.a(dind,1),'i');
    set(gca,'ylim',[1,size(stim,1)]);
    set(gca,'xlim',[1,size(stim,2)]);    
end


%make the color limits the same
if (length(simobj)>1)
cl=get(finp.a,'clim');
cl=cell2mat(cl);
cl=[min(cl(:,1)) max(cl(:,2))];
set(finp.a,'clim',cl);
end

%****************************************************************
%label graphs
%*******************************************************************
%add a colorbar
rind=length(simobj);
cind=1;
setfocus(finp.a,rind,cind);
hc=colorbar;
finp.a(rind,cind)=sethc(finp.a(rind,cind),hc);

%left column
finp.a(rind,cind)=ylabel(finp.a(rind,cind),'i');
setfocus(finp.a,rind,cind);
%set(gca,'ytickmode','auto');

rind=nrows;
ncols=1;
xlabel(finp.a(rind,cind),'trial');

sizesubplots(finp);