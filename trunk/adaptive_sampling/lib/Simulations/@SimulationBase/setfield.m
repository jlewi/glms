%function simobj=setfield(simobj,fname,fval)
%       fname - field name
%       fval - value
%
%Explanation: general method for setting the fields
%
function simobj=setfield(simobj,fname,fval)

simobj.(fname)=fval;