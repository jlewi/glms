%function rollback(simobj,trial)
%   trial - this is the first trial we want to run when iterate is called
%
%Explanation:
% rollsback the simulation to the appropriate trial
function rollback(simobj,trial)


if (trial>simobj.niter)
    error('trial must be < simobj.niter');
end

%call rollback on the stimulus chooser
%rollback the stimulus chooser first because they may need to access the
%observations and/or inputs.
rollback(simobj.stimchooser,simobj,trial);


rollback(simobj.allpost,simobj,trial);

%last trial to save
tlast=trial-1;
%obsrv and inputs are just are RACCessFile so we just delete the matrices
oids=readmatids(simobj.obsrv);
oind=find(oids>tlast);
if ~isempty(oind)
deletematrix(simobj.obsrv,oids(oind));
end

iids=readmatids(simobj.inputs);
iind=find(iids>tlast);

if ~isempty(iind)
deletematrix(simobj.inputs,iids(iind));
end

if ~isempty(simobj.entropy)
    simobj.entropy=simobj.entropy(1:trial-1);
end

simobj.niter=trial-1;


