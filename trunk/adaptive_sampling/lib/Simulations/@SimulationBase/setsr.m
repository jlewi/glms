%function obj=setsr(obj,trial,val)
%	 obj=SimulationBase object
%    trial - trial on which to set it
%    val - value to set the sr structure 
%Return value: 
%	 obj= the modified object 
%
function obj=setsr(obj,trial,val)
     
error('Function is abolsution 1-20-2009 because we write inputs and observations to a file.');

%if version>071120
%we store sr as an SRObject
if (~isa(val,'SRObj') && (obj.simver>=071120))
    val=SRObj('sr',val);
end

if isempty(obj.sr) 
    if (trial~=1)
        error('cant set sr');
    else
        obj.sr=val;
    end
    
else
 obj.sr(trial)=val;
end
     