%function stim=getgetstim(obj,trial)
%	 obj=SimulationBase object
%    trial = which trials on which to get the stimuli
%Return value: 
%	 inputs
%Explanation This function actually computes the inputs which could depend on past
%responses and stimuli.'
function inputs=getinputs(simobj,alltrials)

mobj=simobj.mobj;
sobj=simobj.stimchooser;
inputs=zeros(getparamlen(simobj.mobj),length(alltrials));


for tind=1:length(alltrials)
trial=alltrials(tind);
    
  %get the past sr objects

        tlaststim=trial-(mobj.ktlength)+1;

        %01-27-2009 
        %Can I speed this up by using reshape?
        %Create a matrix of the inputs 
        %[x(t-k), x(t-k+1),x(t-k+2),...x(t-1);
        %x(t-k+1, x(t-k+2),...x(t-2)];
        %Then by taking each column we should get all stimuli for a given
        %trial 
        %(I think I made some mistakes in how i laid out the matrix above);
        allstim=getinput(simobj.inputs,max(tlaststim,1):trial);
         
        %11-08-2009
        %use projmat instead of projinp
        %inputs(:,tind)=projinp(mobj,allstim,getshist(simobj,trial));
        inputs(:,tind)=projinpmat(mobj,allstim,getshist(simobj,trial));
        
end