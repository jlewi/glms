%function updater=getupdater(obj)
%	 obj=updater object
% 
%Return value: 
%	 prop=obj.updater 
%
function updater=getupdater(obj)
	 updater=obj.updater;
