%function sim=BSBatchMLSim('updater','mobj','prior');
%
%Explanation: Class encapsulates a batch ML simulation.
%
%Revisions:
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) BSBatchMLSim < handle
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        version=081109;

        mobj=[];
        
        prior=[];
        
        results=[];
    end
    properties(SetAccess=public,GetAccess=public);
       updater=[]; 
    end

    properties(GetAccess=public, Dependent)
        bdata=[];
    end
    methods
        function bdata=get.bdata(obj)
           bdata=obj.updater.bdata; 
        end
        function obj=BSBatchMLSim(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'mobj','prior','updater'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=struct();
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     obj.mobj=params.mobj;
                     obj.updater=params.updater;
                     obj.prior=params.prior;                     
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


        end
    end
    
    methods (Static)
       %a static function for kicking off a simulation in a datafile
       %this is useful for running findMAP as a parallel job
       bssim=runonfile(datfile,statusfile);
    end
end


