%function [results]=findMAP(bssimobj,thetainit,datafile)
%   thetainit - the value of theta to use to initialize the search for the
%   MAP
%   datafile - the datafile to save the data to
%   statusfile - optional file to print status messages to
%return value:
%   results - a BSBatchMLResults object with the latest results
function [results]=findMAP(bssimobj,thetainit,datafile,statusfile)



%***********************************************************
%get the prior
%*************************************************************
%we don't want to represent the covariance matrix as a distributed marix
%because that leads to lpost and its derivatives as being distributed
%arrays
%therefore we do a gather operations
if (numlabs>1)
    prior=GaussPost('m',getm(bssimobj.prior,0),'c',EigObj('matrix',gather(getc(bssimobj.prior,0))));
else
    prior=GaussPost('m',getm(bssimobj.prior,0),'c',EigObj('matrix',getc(bssimobj.prior,0)));
end
%compute the inverse of the prior covariance matrix so that we only do it
%once


results=findMAP(bssimobj.updater,thetainit,bssimobj.mobj,prior,statusfile);


if isempty(bssimobj.results)
    bssimobj.results=results;
else
    bssimobj.results(end+1)=results;
end



if ~isempty(datafile)

    if isempty(statusfile)
        fid=1; %standard output
    else
        %make sure the directory to contain the statusfile exists
        fdir=fileparts(getpath(statusfile));
        recmkdir(fdir);
        fid=fopen(getpath(statusfile),'a');
    end

    fprintf(fid,'Saving the data \n');
    ecode=0;
    err=[];
    try
        if isa(datafile,'FilePath')
            fname=getpath(datafile);
        else
            fname=datafile;
        end




        save(fname,'-append','bssimobj');

        
        fprintf(fid,'Succesfully saved the data \n');
    catch e
        ecode=1;
        err=e;
        fprintf(fid,'Error saving the data. Error message:\n%s \n',e.message);
    end


    if (fid>1)
        fclose(fid);
    end

    if (gplus(ecode)>0)
        %close the file and if an error occurs during close ignore it.
        try
            if (fid>1)
                fclose(fid);


            end
        catch
        end
        %error occured on one of the nodes
        if (ecode>0)
            %I think a task object can only store a structure
            %as the error
            %ex=MExceptionData('User:Error','Error occured. Check the cause. fmap is stored in data',fmap);
            %ex=addCause(ex,err);
            %ex=err;
            error('MPI:Error on node',['Error on this node. ' err.message]);
        else
            %ex=MExceptionData('MPI:DeadLock','Error occured on one of the other nodes. fmap is stored in data',fmap);
            error('MPI:Deadlock','An error occured on a different node. To avoid a deadlock all nodes throw an error');
        end

        throw(ex);
    end
end

