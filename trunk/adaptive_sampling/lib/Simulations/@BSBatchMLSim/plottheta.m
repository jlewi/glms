%function plottheta(bssim,rind)
%   rind - (optional) which results to plot
function [fh,otbl]=plottheta(bssimall,rind)

if ~exist('rind','var')
    rind=nan;
end

if (rind==0)
    fprintf('No results to plot \n');
    return;
end

nobj=length(bssimall);

otbl=cell(nobj,2);

for bind=1:length(bssimall)
    bssim=bssimall(bind);
    
    if isnan(rind)
        rind=length(bssim.results);
    end
    fh=plottheta(bssim.mobj,bssim.results(rind).theta,[],bssim);
    
    [nfreqs]=bindexinv(bssim.mobj,1:length(bssim.mobj.btouse));

    otbl{bind,1}=fh;
    otbl{bind,2}={'neuron',getfilename(bssim.updater.bdata.fname);'max nfreq',max(nfreqs(:,1)); 'max ntime', max(nfreqs(:,2)); 'nrepeats',bssim.results(rind).updater.maxrepeats;'windexes',bssim.results(rind).updater.windexes;};

end
fname=seqfname('~/svn_trunk/notes/batchmap.xml');
onenotetable(otbl,fname);

%fprintf('Plot saved to %s \n', fname);
