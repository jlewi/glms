%function compexpllike(bssim,rind,wind)
%   bssim - the simulation
%   rind - which results
%   wind - which wavefile(s) to compute it for
function ll=compexpllike(bssim,windexes,rind)

if (~exist('rind','var') || isempty(rind))
   rind=length(bssim.results);   
end

post=GaussPost(bssim.results(rind).theta,-inv(bssim.results(rind).hessian));
for wind=1:length(windexes)
   
   [ll(wind)]=BSExpLLike.compellikeforwavefile(bssim.updater.bpost.bdata,post,windexes(wind),bssim.mobj);
end