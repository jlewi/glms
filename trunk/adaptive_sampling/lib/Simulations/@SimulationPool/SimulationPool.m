%function SimulationPool
%
%Explanation: This class represents the simulation when we use a pool of a
%stim that is constant over the experiment. In this case, we store the
%stimulus as the index in the stimulus pool 
%   The stimulus pool is stored as part of the StimChooser obj
classdef (ConstructOnLoad=true) SimulationPool <SimulationBase
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    properties(SetAccess=private, GetAccess=public)

    end

    methods
        function obj=SimulationPool(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=params;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@SimulationBase(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1;
                     %make sure the stimchooser object
                     %is of an appropriate type
                     %the stimulus chooser must be one whose choosestim
                     %function returns the index of the stimulus in a poool
                     %of stimuli
                     if (~isa(obj.stimchooser,'SampStimDistDisc') && ~isa(obj.stimchooser,'POptAsymDistDisc'))
                        error('Stimchooser is the wrong type for use with SimulationPool. The stimchooser object must pick the stimulus from a pool which does not change.'); 
                     end
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end


            %set the version to be a structure
            obj.version.SimulationPool=090110;
        end
    end
end


