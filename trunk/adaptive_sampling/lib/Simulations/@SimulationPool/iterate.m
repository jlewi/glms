%function simobj=update(simobj,niter)
%   simobj - SimulationObject
%   niter  - number of simulations to run
%   datafile - (optional) file where this object should be saved at the end of the run
%            - This also allows us to save the object at various intervals
%
%  statusfile - (optional) print status information to a
%              file
%
% Return value:
%   simobj - updated simulation object
%Explanation:
%   This function runs the specified method and on each iteration
%   uses the stimulus likely to maximize the info.
%
%

function [simobj,post]=iterate(simobj,ntorun,datafile,statusfile)

if ~exist('datafile','var')
    datafile=[];
end

if ~exist('statusfile','var')
    statusfile=[];
end
[starttrial,lasttrial,prior,infoint,saveint]=initsim(simobj,ntorun,statusfile);

tracemsg=false;
%*************************************************************************
%Loop over the iterations
%*************************************************************************
%loop through enabled methods
fprintf(simobj.statusfid, 'Begin trials simid=%s \n',simobj.simid);

stimlen=getstimlen(simobj.mobj);




obsrv.twindow=gettresponse(simobj.mobj);

%it is absolutely important that we perform all of the iterations
%for a particular method before going onto a different method
%this is is because I make assumptions about what post, prior
%and newpost store in each methods update.
%
%trial - the absolute number of trials
post=prior;

%get references to the objects
mobj=getmobj(simobj);
sobj=getstimchooser(simobj);
observer=getobserver(simobj);

%try
for trial=starttrial:lasttrial
    if (mod(trial-starttrial,infoint)==0)
        %add a time stamp
        fprintf(simobj.statusfid,'%s: Trial %d \t\t %s \n',getsimid(simobj),trial,datestr(clock,31));
    end


    %*****************************************************************
    %compute the spike history
    %******************************************************************
    %spike history is just the previous responses
    %we take them to be zero if they happened before the start of the
    %experiment
    nobsrv=trial-1;    %how many observations have we made



    %****************************************************************
    %choose the stimulus
    %****************************************************************
    %relevant posterior is stored at post(trial) b\c post(1) = prior



    %choosestim should return the index in the pool
    [stimind]=choosestim(sobj,simobj,post);

   sr=SRObj('input',stimind,'obsrv',nan);

    %we need to set the input part of sr for this trial so that calls to
    %getinputs return it correctly
    simobj=setsr(simobj,trial,sr);
  

    %sample the model to generate the response
    tinfo.twindow=gettresponse(simobj.mobj);

    %******************************************************************
    %generate the observation
    %******************************************************************
    pastsind=[];
    
    srpast=[];
    
    input=getinputs(simobj,trial);
    
    obsrv=observe(observer,simobj,trial);
    sr=SRObj('input',stimind,'obsrv',obsrv);

    %sr.stiminfo=stiminfo;
    simobj=setsr(simobj,trial,sr);
    
    if isinf(sr.obsrv)
        error('infinite # of spoissexpmaxmipikes');
    end
    if isnan(sr.obsrv)
        error('observation is nan');
    end

    %quick error checking if number spikes is too large we will have problems
    %with fitting the posterior
    %therefore do a quick check. I should make this more formal (i.e define
    %a parameter for max number of spikes)
    if (sr.obsrv >simobj.maxspikes)
        error('spikes is > %2.4g this will likely cause numerical problems',simobj.maxspikes);
    end
    
  

    %update
    [post]=update(simobj.updater,post,input,mobj,sr.obsrv,simobj,trial);
    %compute the entropy
   %compute the entropy
    if (simobj.compentropy)
       simobj.entropy(trial)=compentropy(post); 
    end

    %save the updated posterior
    if (mod(trial,simobj.savecint)==0 || trial==lasttrial)
        simobj.allpost=addpost(simobj.allpost,post,trial);
    else
        simobj.allpost=addpost(simobj.allpost,lowmem(post),trial);
    end

   

    simobj.niter=simobj.niter+1;


    if (~isempty(datafile) && mod(trial,saveint)==0)
         if (tracemsg)
            fprintf(fiderr,'lab %d: trial %d: starting periodic save of data\n', labindex,trial);
        end
        savesim(simobj,datafile);
    end
end

%comment this out during debugging so we can stop on error
% catch errinsim
% 
%     fprintf(simobj.statusfid,'Error occured during simulation. Error identifier:%s. \n',errinsim.identifier);
%     simobj.runs(runnum).abortedon=trial;
%     simobj.runs(runnum).error=errinsim;
%     switch lower(errinsim.identifier)
%         case 'birdsong:outofdata'
%             %we have run out of data
%             %in this case we want to exit the simulation nicely
%             %i.e we want to save the data
%             %so we just continue execution
%             fprintf(simobj.statusfid,'We have run out data. Simulation will end nicely. \n');
% 
%             %we want to save the covariance matrix on the last trial
%             simobj.allpost=setc(simobj.allpost,getc(prior),simobj.niter);
%         otherwise
% 
%             %we want to save the covariance matrix on the last trial
%             %check to make sure we didn't update prior yet
%             if (trial==simobj.niter-1)
%                 simobj.allpost=setc(simobj.allpost,getc(prior),simobj.niter);
%             else
%                 fprintf(simobj.statusfid,'Couldn''t save covariance matrix on trial on which error occured because covariance matrix wasn''t updated yet. \n');
%             end
%             %an error occured in the simulation
%             %if an error occured save the simulation to a file named
%             %fname_aborted
%             if ~isempty(datafile)
%                 if isa(datafile,'FilePath')
%                     [fdir fn fext]=fileparts(getpath(datafile));
%                 else
%                     [fdir fn fext]=fileparts(datafile);
%                 end
%                 ferr=seqfname(fullfile(fdir,sprintf('%s_aborted.mat',fn)));
%                 %   savesim(simobj,ferr);
%                 emsg=sprintf('Simulation aborted due to error on trial=%d. \n Simulation and error saved to %s on lab=%d\n',trial,ferr,labindex);
%                 emsg=sprintf('%sError Message:\n%s\n',errinsim.message);
% 
%                 fprintf(simobj.statusfid,'%s',emsg);
%                 save(ferr,'simobj','errinsim');
%                 error(emsg);
%             end
%     end
% end


endsim(simobj,datafile);

