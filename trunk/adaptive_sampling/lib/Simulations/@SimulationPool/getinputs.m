%function stim=getgetstim(obj,trial)
%	 obj=SimulationBase object
%    trial = which trials on which to get the stimuli
%Return value: 
%	 inputs
%Explanation This function actually computes the inputs which could depend on past
%responses and stimuli.'
function inputs=getinputs(simobj,alltrials)


mobj=simobj.mobj;
sobj=simobj.stimchooser;
inputs=zeros(getparamlen(simobj.mobj),length(alltrials));


for tind=1:length(alltrials)
trial=alltrials(tind);
    
  %get the past sr objects
        tlaststim=trial-(mobj.ktlength)+1;
        srpast=simobj.sr(max(tlaststim,1):trial);


    
   
        pastsind=getinput(srpast);
        allstim=sobj.stim(:,[pastsind.index]);
   
    inputs(:,tind)=projinp(mobj,allstim,getshist(simobj,trial));
end