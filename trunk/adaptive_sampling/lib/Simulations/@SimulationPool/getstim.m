%function stim=getgetstim(obj,trial)
%	 obj=SimulationBase object
%    trial = which trials on which to get the stimuli
%Return value: 
%	 stim=obj.sr(trial).y
%
%Overload getstim because we save the stimulus index on each trial
%   and from this we want to get the actual stimulus
function stim=getstim(obj,trial)
if isa(obj.sr,'SRObj')    
    stimind=getinput(obj.sr(trial));
    
	 stim=[obj.stimchooser.stim(:,[stimind.index])];
end
