%function prior=priorfourier(mobj)
%   mobj - MParamObj
%
function prior=initprior(mobj)

%we add 1 for the bias term
dim=getparamlen(mobj);
cinitscale=10;
cinit=cinitscale*eye(dim);


    minit=zeros(dim,1);


prior=GaussPost('m',minit,'c',cinit);
