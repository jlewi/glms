%function [files,fnum]=initfiles(odir,fbname)
%     odir   - output directory
%     fbname - the base name for the files
%     compsimerror - added 11-08-2009. Indicates we should create a file to
%                    store the simulation error
%Explanation:
%   Creates the files needed for the simulation.
%   Makes sure the directories exist.
function [data,fnum]=initfiles(odir,fbname,compsimerror)

if ~exist('compsimerror','var')
    compsimerror=false
    
end


%*************************************************************************
%Parameters for the job
%**************************************************************************

setupfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_setup.m',fbname)));


datafile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_data.mat',fbname)));

mfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_mfile.dat',fbname)));
cfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_cfile.dat',fbname)));

%status file contains status information
statusfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_status.txt',fbname)));

inputsfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_inputs.dat',fbname)));
obsrvfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_obsrv.dat',fbname)));

if (compsimerror)
   errfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile(odir,sprintf('%s_simerr.mat',fbname)));    
end

[data.setupfile fnum]=seqfname(setupfile);
%use fnum returned by setupfile so the number suffix for all files is the
%same and matches the number attached to the setup file
data.datafile=seqfname(datafile,[],fnum);
data.mfile=seqfname(mfile,[],fnum);
data.cfile=seqfname(cfile,[],fnum);
data.statusfile=seqfname(statusfile,[],fnum);
data.inputsfile=seqfname(inputsfile,[],fnum);
data.obsrvfile=seqfname(obsrvfile,[],fnum);


files=[data.mfile data.cfile data.statusfile data.inputsfile data.obsrvfile];

if (compsimerror)
   data.errfile=seqfname(errfile,[],fnum);
   files=[files data.errfile];
   
   %create the error file. Otherwise the script will fail on the cluster
   %because the file doesn't exist
   garbagedata='hello';
   [fdir]=fileparts(getpath(data.errfile));
   recmkdir(fdir);
   save(data.errfile.getpath(),'garbagedata');
end

%********************************************************************
%create the directories for the files if the do not exist
files=[files data.datafile setupfile];
for j=1:length(files)
    [fdir]=fileparts(getpath(files(j)));
    recmkdir(fdir);
end

%*************************************************************************
%Create the status file
%***********************************************************************
fid=fopen(getpath(data.statusfile),'a');
fprintf(fid,'Status file. \n');
fclose(fid);
