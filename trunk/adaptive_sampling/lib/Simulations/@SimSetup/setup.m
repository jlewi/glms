%function setup(param)
%   param - structure containing optional parameters
%
%       mparam - additional model specific parameters
%
%
%Required parameters
%       fbase - the base name for the files to create
%       fdir  - the subdirectory of RESUTLSDIR to use
%       observer - the observer
%       updater - the updater
%       stimchooser - the stimulus chooser
%
%optional parameters
%       simgp - 11-08-2009 added his value to do the special processing
%               for new simulations created for Batch09 paper
%
%function setupinfomax(fname,val)
%   enter the options in field name value pairs
%08-06-2008
%
%
%Explanation: This script sets up an infomax simulation
%
%Revisions:
%   11-08-2009 Modified it to work with SimulationGP
%   08-11-2008
%      make it  a function
function [dfiles,otbl,ofile]=setup(varargin)

switch nargin
    case 0
        params=struct();
    case 1
        params=varargin{1};
    otherwise
        %load the fields passed in into a structure
        for j=1:2:nargin
            params.(varargin{j})=varargin{j+1};
        end
end

if ~exist('params','var')
    params=struct();
end

if ~isfield(params,'fbase')
    error('You need to specify fbase');
end

if ~isfield(params,'fdir')
    error('You need to specify fdir');
end

if ~isfield(params,'simgp')
   simgp=false
else
    simgp=params.simgp;
    params=rmfield(params,'simgp');
end
setpathvars;
fprintf('\n\n');


%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;
RDIR=fullfile(params.fdir, datestr(datetime,'yymmdd'));



%*******************************************************************
%setup the required files
%*******************************************************************
%simerror whether we should create a file to store the expected error
if (simgp)
    compsimerror=true;
else
    compsimerror=false;
end
[dfiles,fnum]=SimSetup.initfiles(RDIR,params.fbase,compsimerror);


%***************************************************************
%Prior
%*********************************************************************
if isempty(params.prior)
    prior=SimSetup.initprior(params.mobj);
else
    prior=params.prior;
    params=rmfield(params,'prior');
end
%************************************************************************
%Create the GaussPostfile object using the prior and save it to gpostfile
%************************************************************************
allpost=SimSetup.initgausspostfile(dfiles,prior);


if isfield(params,'label')
    label=params.label;
else
    label='';
end

sparams=rmfield(params,{'fbase','fdir'});
sparams.allpost=allpost;
sparams.label=label;
sparams.inputsfile=dfiles.inputsfile;
sparams.obsrvfile=dfiles.obsrvfile;

%11-08-2009 speciall processing for the SimulationGP objects
%to use for the batch09 simulations.
if (simgp)

    simerror=SimExpErrorOnline('outfile',dfiles.errfile,'simfile',dfiles.datafile,'label',label);
    sparams.simerror=simerror;
    simobj=SimulationGP(sparams);
else
    switch class(params.stimchooser)
        case {'SampStimDistDisc','POptAsymDistDisc'}
            simobj=SimulationPool(sparams);
        otherwise
            simobj=SimulationBase(sparams);
    end
end

save(getpath(dfiles.datafile),'simobj');


%*************************************************************
%add to a our setup file the number of windexes so that
% we know how many nodes to use
%********************************************************
%allfiles.nwindexes=length(windexes);

%information
info.scriptname=getpath(dfiles.setupfile);
info.klength=getklength(params.mobj);
info.ktlength=getktlength(params.mobj);
info.alength=params.mobj.alength;



%output the information to an mfile
writeminit(dfiles.setupfile,dfiles,info);

%**********************************************************
%create an xml file to describe this experiment
%***********************************************************


otbl=[{'setupfile',getpath(dfiles.setupfile); 'datafile',getpath(dfiles.datafile);}];
otbl=[otbl;{'SimObj', simobj}];

%make the first text in the table the label so that the title of the page
%will default to ti
otbl=[{{simobj.label}};{otbl}];



ofile=seqfname(FilePath('bcmd','RESULTSDIR','rpath',fullfile(RDIR,'sim.xml')),[],fnum);
onenotetable(otbl,getpath(ofile));


