%function rollback(bssim,trial)
%   trial - trial to rollback to
%         - This must be a trial on which we saved the covariance matrix
%Explanation: This function rolls a simulation back to an earlier trial 
%   from which we can then continue the simulation
%   This is mainly intended for debugging
%
function bssim=rollback(bssim,trial)
    
    error('This function has not been tested or debugged 08-28-2008.');
    %check the covariance matrix was saved on trial
    post=getpost(bssim.allpost,trial);
    
    if isempty(getc(post))
        error('Cannot rollback to trial %d because the covariance matrix was not saved \n',trial);
    end
    
    %set the number of trials to trial
    bssim.niter=trial;
    
    
    %rollback the stimulus chooser
    bssim.stimobj=rollback(bssim.stimobj);
    
