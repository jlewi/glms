%function bssimobj=iterate(bssimobj,ntorun,datafile,statusfile,starttrial)
%   ntorun  - number iterations to run
%   datafile - (optional) file where this object should be saved at the end of the run
%            - This also allows us to save the object at various intervals
%
%   statusfile - (optional) print status information to a file
%   starttrial - optional() - the trial to start with
%              - if you use starttrial you can overwrite data
%                from an earlier run.
%              - starttrial is useful if you didn't save the covariance
%              matrix on the lasttrial so you want to continue from an
%              earlier sim
%              - WARNING - Starting from an earlier trial can cause
%              problems with some stimulus choosers which save state
%              information.
%              - e.g BSBatchPoissLB stores information about the batch
%                so if you restart from an earlier trial it will cause
%                problems
%
%Revisions:
%  08-13-2008
%      bssimobj.runs(runnum).starttrial was not being set correctly
%
function [bssimobj]=iterate(bssimobj,ntorun,datafile,statusfile,starttrial)

if ~exist('starttrial','var')
    starttrial=[];
end
%how many trials to run before saving the object and printing status
%information
%saveint must be a multiple of savecint because we only want to save the
%simulation if we saved the covariance matrix because otherwise we won't be
%able to continue the simulation
saveint=500;
saveint=floor(saveint/bssimobj.savecint)*bssimobj.savecint;

%info int is the interval at which to print out the trial number
%infoint get set to min(infoint, ceil(.1*ntorun));
infoint=10;

%save the updater so we know what updater was used if we continue this
%simulation

%get references to the objects
mobj=bssimobj.mobj;



%get the last trial we actual saved the posterior for
%starttrial=getlasttrial(bssimobj.allpost)+1;

%We set the starttrial to niter+1
%Its possible that GaussPostFile contains more posteriors than
%niter. This can happen because after every trial we save the posterior to
%a file. But we only save the object bssimobj every so often
%We don't want to use the last available posterior in GaussPostFile because
%other objects (like the stimulus chooser) depend on the trial number
%Those objects weren't saved either so they expect us to be operating on a
%lower trial number.
if isempty(starttrial)
    starttrial=bssimobj.niter+1;
else
    warning('Restarting the simulation from an early trial will not work properly with all stimulus choosers');
    if isa(bssimobj.stimobj,'BSBatchBase')
        rollback(bssimobj.stimobj,bssimobj,starttrial);
    else
        if isa(bssimobj.stimobj,'BSStimReplay')
            
        else
        error('You cannot restart from an earlier trial when the stimulus chooser is BSBatchPoissLB because  BSBatchPoissLB  keeps track of the batch and is out of sync');
        end
    end
    %reset niter
    bssimobj.niter=starttrial-1;
end
[ntrials]=getntrials(bssimobj.stimobj);

lasttrial=min(ntrials,starttrial+ntorun-1);
ntorun=lasttrial-starttrial+1;

infoint=min([infoint .1*ntorun]);

%open the status file
if isempty(statusfile)
    fid=1; %standard output
    bssimobj.statusfid=fid;
else
    %make sure the directory to contain the statusfile exists

    fdir=fileparts(getpath(statusfile));
    recmkdir(fdir);
    fid=fopen(getpath(statusfile),'a');
    bssimobj.statusfid=fid;
end

%control whether we print out trace information
%this is only intended for diagnosing why my simulations appear
%to be deadlocking when run on the cluster
tracemsg=bssimobj.tracemsg;
fiderr=[];
if (tracemsg)
    fname=[];
    
    %lab 1 creates the file and opens it for writing
    if (labindex==1)
        fname='~/bssimlog.txt';
        fname=seqfname(fname);     
        fiderr=fopen(fname,'w');
        fclose(fiderr)
    end
    
    %broadcast the name of the file
   fname = labBroadcast( 1, fname );
   fiderr=fopen(fname,'a');
   
end

%****************************************************************
%update the structure storing information about this run
%**************************************************************
runnum=length(bssimobj.runs)+1;
bssimobj.runs(runnum).starttrial=starttrial;
bssimobj.runs(runnum).ntrials=(lasttrial-starttrial)+1;
fprintf(fid, 'starttrial=%d \nntrials=%d \n',starttrial,bssimobj.runs(runnum).ntrials);

[stat,svnr]=system('~/svn_trunk/scripts/get-svn-revision');
bssimobj.runs(runnum).svnrevision=svnr;
fprintf(fid, 'svn revision=%s \n',svnr);

job=getCurrentJob();

if ~isempty(job)
    bssimobj.runs(runnum).jobid=get(job,'id');
end

%***************************************************************

%*****************************************************************
%get the prior
%******************************************************************

prior=getpost(bssimobj.allpost,starttrial-1);

%make sure prior doesn't represent c is a distributed matrix
%I think isdistributed would be better but I don't thing its impelemnted
%untill 2008a
%DISTRIBUTED MATRIX FOR C WILL CAUSE PROBLEMS because we use a parallel for
%loop
if isdistributed(getc(prior))
    prior=GaussPost('m',getm(prior),'c',EigObj('matrix',gather(getc(prior))));
end

if isempty(getc(prior))
    error('We cannot continue the simulation because the covariance matrix on the lasttrial was not saved');
end

%initialize passtim
paststim=zeros(3,lasttrial);
if ~isempty(bssimobj.paststim)
    paststim(:,1:starttrial-1)=bssimobj.paststim(:,1:starttrial-1);
end
bssimobj.paststim=paststim;

%initialize objfunval
objfunval=nan*zeros(1,lasttrial);
if ~isempty(bssimobj.objfunval)
    objfunval(1:starttrial-1)=bssimobj.objfunval(:,1:starttrial-1);
end
bssimobj.objfunval=objfunval;



%fprintf('bssim.iterat: Uncomment try block before running for real. \n');
try
    for trial=starttrial:lasttrial
        if (mod(trial-starttrial,infoint)==0)
            %add a time stamp
            fprintf(fid,'Trial %d \t\t %s \n',trial,datestr(clock,31));
        end


        %fprintf('iterate: calling choosetrial \n');
        %*******************************************************
        %choose the stimulus
        %****************************************************
        %print trace information to try to determine where sims are failing
        %on cluster
        if (tracemsg)
            fprintf(fiderr,'lab %d: trial %d: calling choosetrial \n', labindex,trial);
        end
        [stim, shist,obsrv,stimindex,blogminfo]=choosetrial(bssimobj.stimobj,bssimobj,prior);
        if (tracemsg)
            fprintf(fiderr,'lab %d: trial %d: done calling choosetrial \n', labindex,trial);
        end
        %keep track of the stimulus
        bssimobj.paststim(:,trial)=stimindex;
        bssimobj.objfunval(:,trial)=blogminfo;

        

        inp=projinpvec(mobj,stim,shist);
        %11-20-2007
        %   start passing the Simulation object to the updater
        %   This way the updater can access any data it needs.
        %   This should be efficient because matlab won't copy the data unless its
        %   modified and its not
        if (tracemsg)
            fprintf(fiderr,'lab %d: trial %d: calling PostUpdater/update \n', labindex,trial);
        end
        [post]=update(bssimobj.updater,prior,inp,mobj,obsrv,bssimobj,trial);
        if (tracemsg)
            fprintf(fiderr,'lab %d: trial %d: done calling PostUpdater/update \n', labindex,trial);
        end


        %we need to make post lowmemory before we add it
        %otherwise we will run out of disk space trying to save the
        %covariance matrix on every trial
        if (tracemsg)
            fprintf(fiderr,'lab %d: trial %d: adjusting the posterior \n', labindex,trial);
        end
        if (mod(trial,bssimobj.savecint)==0 || trial==lasttrial)
            bssimobj.allpost=addpost(bssimobj.allpost,post,trial);
        else
            bssimobj.allpost=addpost(bssimobj.allpost,lowmem(post),trial);
        end
        if (tracemsg)
            fprintf(fiderr,'lab %d: trial %d: done adjusting the posterior \n', labindex,trial);
        end

        prior=post;

        %only update niter at the end because if we didn't make it here we
        %didn't finish processing.
        %We need to update niter though before we save the data
        bssimobj.niter=trial;

        if (~isempty(datafile) && mod(trial,saveint)==0)
            if (tracemsg)
                fprintf(fiderr,'lab %d: trial %d: starting periodic save of data\n', labindex,trial);
            end
            noerr=1;
            emsg='';
            try
                %save without append becasue append causes error
                if isa(datafile,'FilePath')
                    %save(getpath(datafile),'bssimobj','-append');
                    save(getpath(datafile),'bssimobj');
                else
                    save(datafile,'bssimobj');
                end
                fprintf(fid,'Trial %d. Saved the simulation \n',trial);
            catch e
                emsg=sprintf('Error occured while trying to do a periodic save of the data. Error message:\n %s\n',e.message);
                noerr=0
            end
            noerr=gcat(noerr);
            if any(noerr==0)
                %any error occured on one of the nodes
                if (noerr(labindex)==1)
                    emsg=sprintf('Error occured on labs %d while trying to do a periodic save of the data. \n', find(noerr==0));
                end
                emsg=sprintf('Trial=%d: %s This error appears to be due to the use of the append option when saving the data. \n Simulation will continue without saving the data. \n',trial,emsg);
                fprintf(fid,'%s', emsg);

            end

            if (tracemsg)
                fprintf(fiderr,'lab %d: done with periodic save of data\n', labindex);
            end

        end


    end

    %comment this out during debugging so we can stop on error
catch errinsim

    fprintf(fid,'Error occured during simulation. Error identifier:%s. \n',errinsim.identifier);
    bssimobj.runs(runnum).abortedon=trial;
    bssimobj.runs(runnum).error=errinsim;
    switch lower(errinsim.identifier)
        case 'birdsong:outofdata'
            %we have run out of data
            %in this case we want to exit the simulation nicely
            %i.e we want to save the data
            %so we just continue execution
            fprintf(fid,'We have run out data. Simulation will end nicely. \n');

            %we want to save the covariance matrix on the last trial
            bssimobj.allpost=setc(bssimobj.allpost,getc(prior),bssimobj.niter);
        otherwise

            %we want to save the covariance matrix on the last trial
            %check to make sure we didn't update prior yet
            if (trial==bssimobj.niter-1)
                bssimobj.allpost=setc(bssimobj.allpost,getc(prior),bssimobj.niter);
            else
                fprintf(fid,'Couldn''t save covariance matrix on trial on which error occured because covariance matrix wasn''t updated yet. \n');
            end
            %an error occured in the simulation
            %if an error occured save the simulation to a file named
            %fname_aborted
            if ~isempty(datafile)
                if isa(datafile,'FilePath')
                    [fdir fn fext]=fileparts(getpath(datafile));
                else
                    [fdir fn fext]=fileparts(datafile);
                end
                ferr=seqfname(fullfile(fdir,sprintf('%s_aborted.mat',fn)));
                %   savesim(bssimobj,ferr);
                emsg=sprintf('Simulation aborted due to error on trial=%d. \n Simulation and error saved to %s on lab=%d\n',trial,ferr,labindex);
                emsg=sprintf('%sError Message:\n%s\n',errinsim.message);

                fprintf(fid,'%s',emsg);
                save(ferr,'bssimobj','errinsim');
                error(emsg);
            end
    end
end


%all nodes save a copy of the file
%only lab1  transfers its file back to the client in the taskfinish
%function
%save this object to the datfaile


if ~isempty(datafile)
    %try saving it one lab at a time
    try


        if isa(datafile,'FilePath')
            save(getpath(datafile),'bssimobj','-append');
        else
            save(datafile,'bssimobj','-append');
        end
        fprintf(fid,'Done saving the file \n');
    catch e

        fprintf(fid,'Error saving the file. Message was:\n %s \n', e.message);
        fprintf(fid,'Try saving the data without the append option \n');



        try
            if isa(datafile,'FilePath')
                save(getpath(datafile),'bssimobj');
            else
                save(datafile,'bssimobj');
            end
            fprintf(fid,'saved without append option \n');
        catch e
            fprintf(fid,'Error saving to %s file. Message was:\n %s \n', datafile, e.message);
        end

    end



end

if (fid>1)
    fclose(fid);
    %set bssimobj.statusfid back to 1
    bssimobj.statusfid=1;
end

if ((tracemsg) && fiderr>1)
   fclose(fiderr); 
end