%function sim=SimulationBase('stimchooser',stimchooser,'observer', osbrv, 'updater',updater','mobj',mobj)
%
%       obsrvfile - the name of the file to store the observations
%       inputsfile - name of the file to store the inputs
%
%       -load the specified simulation from the file.
%       -simparam should be a variable named
% Explanation: constructor for the simulation object
%
%function sim=SimulationBase('fname',fname,'simvar',var)
%   fname - filename containing the simulation object
%   var   - name of the simulation object to load
%
%function sim=SimulationBase('dsets',dsets)
%   dsets - structure array of datasets to load
%         .fname
%         .simvar
%Revision History
%   11-08-2009 I created this class because for my simulations for our Batch09 paper
%              I needed to modify iterate.m But I didn't want to do this in
%              SimulationBase because I didn't want to Break anything
%
classdef (ConstructOnLoad=true) SimulationGP < SimulationBase

    properties(SetAccess=protected,GetAccess=public)
        %simerror the expected error taken with respect to the posterior
        % of the estimated parameters

        %comperrorint - the interval in which we compute the exepected error
        comperrorint=[];
        simerror=[];
        
        %interval in which we save the posterior
        savepostint=[];
    end


    methods
        function simobj=SimulationGP(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'simerror','comperrorint'};


      
            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %********************************************************************
            switch cind
                case {1}
                    bparams=rmfield(params,'simerror');
                    bparams=rmfield(bparams,'comperrorint');
                    bparams=rmfield(bparams,'savepostint');
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            simobj=simobj@SimulationBase(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    
                    simobj.simerror=params.simerror;
                    simobj.comperrorint=params.comperrorint;
                    simobj.savepostint=params.savepostint;
                otherwise
                    %do nothing entirely handled by base class
            end






            simobj.version.SimulationGP=091108;


        end


    end
   
      methods(Access=protected)
        endsim(simobj,datafile);        
    end
end







