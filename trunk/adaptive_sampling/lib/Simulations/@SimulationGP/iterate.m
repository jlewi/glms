%function simobj=update(simobj,niter,datafile,starttrial)
%   simobj - SimulationObject
%   niter  - number of simulations to run
%   datafile - (optional) file where this object should be saved at the end of the run
%            - This also allows us to save the object at various intervals
%
%  statusfile - (optional) print status information to a
%              file
%
%  starttrial - specify a start trial that is not the last trial.
%       WARNING - starting from an earlier trial will delete
%               - all the old data because we write the data to a file
%               - so we will start overwriting the data
% Return value:
%   simobj - updated simulation object
%Explanation:
%   This function runs the specified method and on each iteration
%   uses the stimulus likely to maximize the info.
%
%
%This function should not becalled directly except by the SimulationClass
%and its children. External functions should call update
%
% Return value
%   simobj - updated simulation object
%   einfo  - eigendecomposition of our new posterior
%
%%Revision History:
%   091108 - Compute the expected error under the posterior of the
%            parameters
%   081230 - iterate now performs a bunch of iterations not just a single
%         iteration
%          - Pass in the prior
%          - return the updated posterior
%   071019 - pass spike history to observe
%   070914 - started modifying it to use accessor get/set methods
%            for all objects instead of using .notation to call functions.
%            using getset methods is faster and works better with
%            inheritance.

function [simobj,post]=iterate(simobj,ntorun,datafile,statusfile,starttrial)

if ~exist('datafile','var')
    datafile=[];
end

if ~exist('statusfile','var')
    statusfile=[];
end

if ~exist('starttrial','var')
    starttrial=[];

end

if ~isempty(starttrial)
    fprintf('Starting from an earlier trial will overwrite data from later trials.\n');
    r=input('Continue(y/n)?','s');
    switch lower(r)
        case {'y','yes'}
            %continue
        otherwise
            fprintf('Simulation aborted on user request. No data was overwrriten.\n');
            return;
    end

    %rollback the simulation
    rollback(simobj,starttrial);

end

[starttrial,lastttrial,prior,infoint,saveint]=initsim(simobj,ntorun,statusfile);

tracemsg=false;
%*************************************************************************
%Loop over the iterations
%*************************************************************************
%loop through enabled methods
fprintf(simobj.statusfid, 'Begin trials label=%s \n',simobj.label);

stimlen=getstimlen(simobj.mobj);




obsrv.twindow=gettresponse(simobj.mobj);

%it is absolutely important that we perform all of the iterations
%for a particular method before going onto a different method
%this is is because I make assumptions about what post, prior
%and newpost store in each methods update.
%
%trial - the absolute number of trials
post=prior;

%get references to the objects
mobj=getmobj(simobj);
sobj=getstimchooser(simobj);
observer=getobserver(simobj);

try
    for trial=starttrial:simobj.lasttrial
        if (mod(trial-starttrial,infoint)==0)
            %add a time stamp
            fprintf(simobj.statusfid,'%s: Trial %d \t\t %s \n',simobj.label,trial,datestr(clock,31));
        end


        %*****************************************************************
        %compute the spike history
        %******************************************************************
        %spike history is just the previous responses
        %we take them to be zero if they happened before the start of the
        %experiment
        nobsrv=trial-1;    %how many observations have we made



        %****************************************************************
        %choose the stimulus
        %****************************************************************
        %relevant posterior is stored at post(trial) b\c post(1) = prior



        %       if (gethistdep(mobj)>0 && trial>1)
        %            % we only call this code if trial >1 otherwise we get error when
        % accessing .nspikes of sr(1:(trial-1))
        % only pass the most recent spikes
        %            [xobj,stiminfo]=choosestim(sobj,simobj,post);
        %        else
        %12-31-2008
        %Choosestim function gets spike history if its needed
        %We should implement a function in simobj - compshist
        %   this way we could potentially save shist in a transient
        %   structure to avoid recomputation or to speed up computation

        [xobj]=choosestim(sobj,simobj,post);
        %      end

        setinput(simobj.inputs,xobj,trial);

        %if choosestim returned a vector create a GLMInputVec object
        %     if ~isa(xobj,'GLMInput')
        %         xobj=GLMInputVec('data',xobj);
        %     end

        %we need to set the input part of sr for this trial so that calls to
        %getinputs return it correctly
        % sr=SRObj('input',xobj,'obsrv',nan);
        %simobj=setsr(simobj,trial,sr);

        %sample the model to generate the response
        tinfo.twindow=gettresponse(simobj.mobj);

        %******************************************************************
        %generate the observation
        %******************************************************************
        srpast=[];
        if (mobj.ktlength>1)
            tlaststim=trial-(mobj.ktlength)+1;
            if (tlaststim>=1 && tlaststim<trial)
                srpast=getinput(simobj.inputs,tlaststim:trial-1);

            end

        end

        %11-08-2009 updated to use projinpmat which is the replacement for
        %projinp
        %minput=projinp(mobj,[srpast xobj],getshist(simobj,trial));
        minput=projinpmat(mobj,[srpast xobj],getshist(simobj,trial));

        obsrv=observe(observer,simobj,trial);

        setobsrv(simobj.obsrv,obsrv,trial);


        if isinf(obsrv)
            error('infinite # of spoissexpmaxmipikes');
        end
        if isnan(obsrv)
            error('observation is nan');
        end

        %quick error checking if number spikes is too large we will have problems
        %with fitting the posterior
        %therefore do a quick check. I should make this more formal (i.e define
        %a parameter for max number of spikes)
        if (obsrv >simobj.maxspikes)
            error('spikes is > %2.4g this will likely cause numerical problems',simobj.maxspikes);
        end
        %************************************************************
        %Full stimulus
        %**************************************************************
        %form the full stimulus for the update by combining the spike history, the stimulus
        %and any nonlinear transformations of the stimulus. This is all handled
        %by projinp
        %depending on the updater we may need to use the observations from all
        %trials or just the most recent
        %we set obsrv and stim approriately

        %doing this check on each iteration might be slow
        %might be better to give each method a unique number
        %and then just check that element in array to see if its on or off




        [post]=update(simobj.updater,post,minput,mobj,obsrv,simobj,trial);
        %compute the entropy
        if (simobj.compentropy)
            simobj.entropy(trial)=compentropy(post);
        end

        %save the updated posterior
        if (mod(trial,simobj.savepostint)==0 || trial==simobj.lasttrial)
            if (mod(trial,simobj.savecint)==0 || trial==simobj.lasttrial)
                simobj.allpost=addpost(simobj.allpost,post,trial);
            else
                simobj.allpost=addpost(simobj.allpost,lowmem(post),trial);
            end
        end

        %compute the error
        if (mod(trial,simobj.comperrorint)==0)

            compexerr(simobj.simerror,simobj,trial,post);
        end


        simobj.niter=simobj.niter+1;


        if (~isempty(datafile) && mod(trial,saveint)==0)
            if (tracemsg)
                fprintf(fiderr,'lab %d: trial %d: starting periodic save of data\n', labindex,trial);
            end
            savesim(simobj,datafile);
        end
    end

    %comment this out during debugging so we can stop on error
catch errinsim

    runnum=length(simobj.runs);
    fprintf(simobj.statusfid,'Error occured during simulation. Error identifier:%s. \n',errinsim.identifier);
    simobj.runs(runnum).abortedon=trial;
    simobj.runs(runnum).error=errinsim;
    switch lower(errinsim.identifier)
        case 'birdsong:outofdata'
            %we have run out of data
            %in this case we want to exit the simulation nicely
            %i.e we want to save the data
            %so we just continue execution
            fprintf(simobj.statusfid,'We have run out data. Simulation will end nicely. \n');

            %we want to save the covariance matrix on the last trial
            simobj.allpost=setc(simobj.allpost,getc(prior),simobj.niter);
        otherwise

            %we want to save the covariance matrix on the last trial
            %check to make sure we didn't update prior yet
            if (trial==simobj.niter-1)
                simobj.allpost=setc(simobj.allpost,getc(prior),simobj.niter);
            else
                fprintf(simobj.statusfid,'Couldn''t save covariance matrix on trial on which error occured because covariance matrix wasn''t updated yet. \n');
            end
            %an error occured in the simulation
            %if an error occured save the simulation to a file named
            %fname_aborted
            if ~isempty(datafile)
                if isa(datafile,'FilePath')
                    [fdir fn fext]=fileparts(getpath(datafile));
                else
                    [fdir fn fext]=fileparts(datafile);
                end
                ferr=seqfname(fullfile(fdir,sprintf('%s_aborted.mat',fn)));
                %   savesim(simobj,ferr);
                emsg=sprintf('Simulation aborted due to error on trial=%d. \n Simulation and error saved to %s on lab=%d\n',trial,ferr,labindex);
                emsg=sprintf('%sError Message:\n%s\n',emsg,errinsim.message);

                %print stack of original error
                emsg=sprintf('%s Stack of error: function \t line \t file \n',emsg);
                for j=1:length(errinsim.stack)
                    emsg=sprintf('%s %s \t %d \t %s \n',emsg,errinsim.stack(j).name, errinsim.stack(j).line, errinsim.stack(j).file');
                end
                fprintf(simobj.statusfid,'%s',emsg);
                save(ferr,'simobj','errinsim');
                error(emsg);
            end
    end
end


endsim(simobj,datafile);

