%function [simobj,od]=savesim(simobj,fname,force);
%     simobj- simulation object to save to
%     fname - name of file to save to
%     force - set this to true to force the save
%             this forces the save if it has been dissallowed
%             This is meant as a check to prevent you from accidentally
%             overriding a simulation or a file
% Return value:
%   simobj - the object
% Explanation:
%   saves the data to a file
%
%
% Revisions:
%   01-10-2008 - revised for new object model.
%           Function is now intended to be used by the iterate function to
%           save the simulation
%       
function [simobj]=savesim(simobj,datafile,force)

if ~exist('force','var')
    force=false;
end

%call save on simerror
saveerr(simobj.simerror);

%call the base class
simobj=savesim@SimulationBase(simobj,datafile,force);

