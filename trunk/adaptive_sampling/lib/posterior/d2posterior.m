%function [dlpdt, dlp2dt]=d2posterior(theta,po,obsrv,fu)
%   theta - value of theta to compute derivatives at
%   po - gaussian of prior
%       invc - inverse of our prior.
%            - if its supplied we don't recompute it
%   obsrv - observations
%       .nspikes(1xntrials) - MUST BE ROW VECTOR
%       .twindow - time window
%   x    - stimuli
%       (dimstim x ntrials)
%   fu  - 1d function which computes lambda
% Computes the 1st and 2nd derivative of the true posterior
%
%Return Value:
%   dfdl - derivative 
%   [df2dl2] - 2nd derivative 
%
%   Optimization: If dlp2dt is not required (only 1 output argument) than
%       dlp2dt is not computed
%   Note the order of derivatives is to allow this function to be used with
%   matlab's fsolve routines to determine the optimal lambda
%
% 
%Revision History:
%   4-08-2006: Optimization. don't compute dlp2dt if not required. 
%       don't comput inv(po.c) if its supplied
function [varargout]=d2posterior(theta,po,obsrv,x,fu)
if isempty(po.invc)
     po.invc=inv(po.c);
end

%****************************************
%gaussian prior
%**************************************
%1st derivative
dprior=-(theta-po.m)'*po.invc;


%****************************************
%derivatives of likelihood
%**************************************
[r0,r1,r2]=fu(theta'*x);

nobsrv=length(obsrv.nspikes);
dimstim=size(x,1);

%gd1=(((-obsrv.twindow+obsrv.nspikes.*1./(r0)).*r1)'*ones(1,dimstim)).*x';
gd1=((-obsrv.twindow*r1+obsrv.nspikes)'*ones(1,dimstim)).*x';
%gradient of the log likelihood is sum of the gradients
%will be row vector
dll=sum(gd1,1);

%***************************************************
%1st derivative: full derivative
dlpdt=dll+dprior;
varargout{1}=dlpdt;
%***************************************************
%if 2 output arguments are supplied
%   compute the 2nd derivative
if (nargout==2)
    %2nd derivative
    
    if ~isempty(po.invc)
        d2prior=-po.invc;
    else
        d2prior=-inv(po.c);
    end

    d2ll=zeros(dimstim,dimstim);
    for index=1:nobsrv
        d2ll=d2ll-obsrv.twindow*r0(index)*x(:,index)*x(:,index)';
    end

    dlp2dt=d2ll+d2prior;
    varargout{2}=dlp2dt;
end
    
