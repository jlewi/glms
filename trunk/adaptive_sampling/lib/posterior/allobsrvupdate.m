
THIS IS SCRAP WORK.



%function allobsrvgauss(klength,alength,glm,stim
%compute gaussian approximation by performing gradient
                %ascent on the true posterior
                %Coded this up to see how it would perform if we have
                %misspecification
                %************************************************
post=allobsrvupdate(mparam,sr,tr-1,
%generate x which is not only stimulus but spike history
        x=zeros(mparam.klength+mparam.alength,tr-1);
        x(1:mparam.klength,:)=sr.(srname).newton1d.y(:,1:alldata{dind}.trials(tind));

        if (mparam.alength>0)
            shist=zeros(mparam.alength,1);
            for titer=1:alldata{dind}.trials(tind)
                nobsrv=titer-1;
                %only compute shist if we have spike history terms
                if (mparam.alength >0)
                    if isequal(simparam.(spname).newton1d.glm.fglmnc,@logisticA)
                        shist(1)=1;
                    else
                        if (mparam.alength <= (nobsrv))
                            shist(:,1)=sr.(srname).newton1d.shist(nobsrv:-1:nobsrv-mparam.alength+1);
                        else
                            shist(1:nobsrv,1)=sr.(srname).newton1d.shist(nobsrv:-1:1);
                        end
                    end
                end
                x(mparam.klength+1:end,titer)=shist;
            end
        end
        %to initialize gradient ascent choose the estimate of the peak of the
        %posterior computed with our gaussian aproximation
        thetainit=data.(pname).newton1d.m(:,alldata{dind}.trials(tind)+1);

        %prior
        po.m=mparam.pinit.m(:,1);
        po.c=mparam.pinit.c;
        obsrv=sr.(srname).newton1d.nspikes(1,1:alldata{dind}.trials(tind));

        %compute the peak of the posterior using all observations
        [post.m]=postgradasc(thetainit,po,x,obsrv,simparam.(spname).glm);

        %compute the covariance matrix  using all the observations
        [dldpt dlp2dt]=d2glm(post.m,po,obsrv,x,simparam.(spname).glm);
        post.c=-inv(dlp2dt);