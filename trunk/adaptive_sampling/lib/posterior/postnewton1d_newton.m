%function [pekf]=postnewton1d(post,x,obsrv,fu,maxdm)
%       post - structure representing the gaussian posterior from the
%              previous time step or newton step
%               post.m is point at which we take a newton step from
%               post.c determines how large a step to take
%                       -this is the covariance matrix which is taken to be
%                       the negative inverse of the hessian.
%               .invc - this is the inverse of post.c
%                       -if this exists then we don't need to compute it
%       x     - column vector representing the stimulus
%               dimstimxt
%               -observations on each trial upto this time point
%       obsrv - struture representing the observations
%              .n - number of spikes
%       `           1x1 -
%              .twindow -window in which spikes are occured
%       fu    - pointer to the nonlinear function and its derivatives which computes
%               the rate as a fcn of u=k'x
%       tolerance - tolerance for determining convergence
%       maxdm - this is the max amount the posterior mean can change along
%               any dimension in a single update. This is meant to prevent
%               instability due to a horizontal asymptote.
%
%      wopts - options about warnings
%           .warnings =0 don't print warning messages
%Return value:
%   pnew - gaussian representing the updated posterior
%       .m - mean
%       .c - covariance
%
%   niter  - number of iterations
%   ntime - time for newton update
%Explanation:
%   when the posterior is just a rank one modification of the posterior
%   from the previous time step we can solve a 1-d optimization problem
%   using Newton's method rather than take a full newton step
%
function [pnew,niter,ntime]=postnewton1d(post,x,obsrv,fu,tolerance,wopts)

if ~exist('maxdm', 'var')
    %maxdm=1;
    maxdm=1000;
end

if ~exist('wopts')
    wopts=[];
end
if ~isfield(wopts,'warnings')
    wopts.warnings=0;
end

xmag=(x'*x)^.5;
tw=obsrv.twindow;

sproj=x'*post.m;

c1=-tw*exp(sproj);
%c1=tw*exp(sproj);
c2=x'*post.c*x;


%initialize a  to zero
%this corresponds to guessing new mean is same as old mean
a=0;
niter=0;
%time the update
tic;
newtonerr=inf;
lasterr=inf;
%alast2=a;         %stores a from two iterations ago
%alast1=a;          %stores a from past iteration

%keep track of the range in which a must lie
%do this so we can do a binary search if needed for when the derivative
%blows up
aleft=-inf;
aright=inf;

c=x'*post.c*x;      %only comput this once
d=x'*post.m;        %only compute once
while (newtonerr >tolerance);
    %compute the new value of a
    %dlda=1/xmag+(c1*exp(a*c2/xmag)*c2/xmag);
    %la=a/xmag+(c1*exp(a*c2/xmag)+obsrv.n)

    %specific to f(u=k'x)=exp(u)
    %la=-a/xmag-tw*exp(x'*post.m+a*x'*post.c*x/xmag)+obsrv.n;
    %dlda=-1/xmag-tw*exp(x'*post.m+a*x'*post.c*x/xmag)*x'*post.c*x/xmag;

    %unew is the new value of u=k'theatnew
    %but we evaluate in terms of a
    %keyboard
    unew=d+a*c/xmag;
    %evaluate the nonlinearity with respect to u
    [r, d1,d2]=fu(unew);
    
    %non specific for u
    %la is the derivative of the log of the posterior as a function of a
   
    la=-a/xmag-tw*d1+obsrv.n/r*d1;
    dlda=-1/xmag+(-tw*d2-obsrv.n/(r^2)*d1^2+obsrv.n/r*d2)*c/xmag;
    %replace our bounds
    if (la>0)
        aleft=a;
    else
        aright=a;
    end
    
    %la is linear on one side and exponential on the other.
    %therefore its possible we start with a very inaccurate a on the linear
    %side as a result we make a large jump to the right side
    %where the slop is very steep. So the error is large but the slope is
    %small so would take forever to converge.
    %if this is the case take the midpoint of the old and new a 
 
    anew=a-1/dlda*la;
    
    %if we are just jumping between aleft and aright
    %then do binary search
    if (anew==aleft || aright==anew || anew >aright || anew <aleft)
    %    keyboard
        if ~(isinf(aleft)|| isinf(aright))
            
        anew=(aleft+aright)/2;
        else
            if (isinf(aleft) && isinf(aright))
                anew=0;
            else
                if (isinf(aleft))
                    anew=aright-10;
                else
                    anew=aleft+10;
                end
            end
        end
    end
    %if the slope of the current point is > than 10^10
    %set anew to be the midpoint
    if (abs(dlda)>10^10)
        if ~(isinf(aleft) || isinf(aright))
            if ~(wopts.warnings==0)
                warning('Setting anew to the midpoint');
            end
        anew=(aleft+aright)/2;
        end
    end
    %    newtonerr=abs(la);
    newtonerr=abs(anew-a);
    
    niter=niter+1;
a=anew;
    %if error is increasing produce warning
    if (newtonerr>lasterr)            
        if ~(wopts.warnings==0)
            warning('Newton1d: error increased');
        end
    end
    if (newtonerr==lasterr)
                if ~(wopts.warnings==0)
        warning('Newton1d: Error did not change');
              end
    end
    lasterr=newtonerr;
    
    if niter>100
        keyboard;
    end
end


%compute the updated mean
pnew.m=post.m+a*post.c*x/xmag;

%compute the new covariance matrix
%***********************************************************************
%now we recompute the covariance matrix at this location.
[r0,r1,r2]=fu(pnew.m'*x);

%We need to scale the terms by obsrv.twindow
%to get the expected number of spikes in the observed window
twin=obsrv.twindow;
r0s=r0*obsrv.twindow;
r1s=r1*obsrv.twindow;
r2s=r2*obsrv.twindow;

%g is the log likelihood up to a scaling factor
g=-r0s+ obsrv.n*log(r0s);
gd1=-r1s+obsrv.n*1/(r0s)*r1s;
gd2=-r2s+obsrv.n*(-1/(r0s^2)*(r1s)^2+1/r0s*r2s);


%now compute the new covariance matrix at this location
%using the woodbury lemma;
pnew.c=post.c-post.c*x*(-gd2/(1-gd2*x'*post.c*x))*x'*post.c;

ntime=toc;
