%function [pmethods,sr,simparam]=updateposteriors(modparam,simparam,methods,ropts)
%
%       mparam - model parameters governing model to use
%           .tlength - length of input vectors = length of filter
%           .ktrue   - value of the true filter
%           .tresponse - length of response window in which we
%                        count the number of spikes after presenting the
%                        stimulus
%            .mparam.dk       -step size used for discrete representations of
%                        posteriors
%                        -this is used for some analysis and for brute
%                        force updates
%            .mparam.krange   - range for k when doing discrete representations
%
%       sparam - simulation parameters
%           .niter - number of iterations
%           .rstate - state of the random number generators
%                     leave blank or set to -1 if you don't want to set it
%                     default is to use a different state each time based
%                     on the clock
%           .normalize - if this field exists, all random stimuli are
%           normalized to have this magnitude. We do this to ensure a valid
%                   that is (x'*x)^.5=normalize
%           comparison to when we choose the optimal stimulus as we impose
%           that constraint then.
%                   this should be the square of the magnitude.
%       methods - gets passed to initmethods. see initmethods for details
%
%       ropts  - options regarding the results/analysis
%           .fname  - name to save data to
%                   - leave blank to not save data
% Return Value:
%       pmethods - results of updating the posteriors
%       sr   -   stimulus and response
%            sr.y - the stimuli - 1 trial per column
%            sr.nspikes - #of spikes after each stimulus
%       simparam - contains the actual simparam used
%                - i.e rstate will store the state of the random number
%                generator
%Explanation:
%   This script runs the various update rules for the posterior.
%   No parameters are set in this script
%   The point is that the actual experiments get setup in a different 
%   script and this is just the common code to run all the experiments and
%   do all the analysis.
%   
%   This makes it easy to write script files which correspond to different
%   experiments and automatically save the results.
%
% Revision History:
%   3-20-2005 changed it so that for trials < than dimensionality of data
%   it uses independent vectors
function [pmethods,sr,simparam]=updateposteriors(mparam,simparam,methods,ropts)
%error checking defaults
if ~exist('simparam','var')
    simparam=[];
end
if ~isfield(simparam,'niter')
    simparam.niter=100;
    fprintf('updateposteriors: Using default of %0.3g iterations \n',simparam.niter);
end


if ~isfield(simparam,'rstate')
    simparam.rstate=-1;
end
if (simparam.rstate <0)
    %
    fprintf('Randomly setting random state \n');
    %reset the states of the random number generators to a different state
    %each time
    %save the state so that we can reproduce the results later
    simparam.rstate=sum(100*clock);    
end
randn('state',simparam.rstate);
rand('state',simparam.rstate);

if ~exist('mparam','var')
    mparam=[];
end
if ~isfield(mparam,'tlength')
    error('Updateposteriors: mparam.tlength not specified');
end
if ~isfield(mparam,'ktrue')
    error('Updateposteriors: mparam.ktrue not specified');
end

if ~isfield(mparam,'tresponse')
    error('Updateposteriors: mparam.tresponse not provided');
end

%make sure tlength and ktrue are vectors of same size
mparam.ktrue=colvector(mparam.ktrue);

if (size(mparam.ktrue)~=mparam.tlength)
    error('Updatposteriros: mparam.ktrue must have length = mpraram.tlength');
end

if ~isfield(mparam,'dk')
    error('Updateposteriors: mparam.dk not specified');
end

if ~isfield(mparam,'krange')
    error('Updateposteriors: mparam.krange not specified');
end

if (size(mparam.krange,1)==1)
    %assume mparam.krange should be same for each dimension
    mparam.krange=ones(mparam.tlength,1)*mparam.krange;
end

%**************************************************************************
%Options concerning results
if ~exist('ropts','var')
   ropts=[]; 
end
if ~isfield(ropts,'fname')
    ropts.fname=[RESULTSDIR '\posteriors\' sprintf('%0.2g_%0.2g_pdfs_data.mat',datetime(2),datetime(3))];
    ropts.fname=seqfname(data.fname);
    fprintf('UpdatePosteriors: Date will be saved to: \n \t %s \n',ropts.fname);
else
    if isempty(ropts.fname)
        fprintf('UpdatePosteriors: Data will not be saved \n');
    end
end

%printiter is the interval to use when printing the iteration number 
printiter=10;
%*************************************************************************
%initialization
%************************************************************************
%specify which variables get saved
%vtosave
%name of variables to save
vtosave.pmethods='';
vtosave.kldist='';
vtosave.simparam='';
vtosave.ropts='';
vtosave.mparam='';
vtosave.sr='';

%save options
saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
saveopts.cdir =1; %create directory if it doesn't exist

%********************
%initialize the pmethods structure
pmethods=initmethods(methods);

if (pmethods.newton.on~=0)
    pmethods.newton.newtonerr=zeros(1,simparam.niter+1);
    pmethods.newton.niter=zeros(1,simparam.niter+1);
end

%*************************************************************************
%Generate Sample data
%************************************************************************
%generate random stimuli
numiter=simparam.niter;                  %how many iterations to try
%x=rand(mparam.tlength,numiter)*2-1;
%add .02 so we don't allow x to be 0 because we get a singularity
%in our likelihood function as x-->0 b\c we lose all information about
%theta since theta*0=0
%x=rand(mparam.tlength,numiter)*2+.02;
%we allow x to be negative in our maximization strategy so we should allow
%it be negative now as well
%I have updated ekfposteriormax so it should be able to handle it.
%x=rand(mparam.tlength,numiter)*2-1;
x=normrnd(0,1,mparam.tlength,numiter);

%set the first d stimuli to be indepenent, orthogonal vectors (just choose unit vectors
%take the svd to get orthonormal basis
[u,s,v]=svd(x(:,1:min(mparam.tlength,simparam.niter)));
x(:,1:min(mparam.tlength,simparam.niter))=u(:,1:min(mparam.tlength,simparam.niter));

%to avoid instability due to x close to zero
%add .01 and -.01 to all values >0 <0
%ind=find(x>=0);
%x(ind)=x(ind)+.5;

%ind=find(x<0);
%x(ind)=x(ind);

%************************************************************************
%normalize the x
%***********************************************************************
%we normalize the x so that the stimulus magnitude is always equal to
%some value. We do this because we impose that constraint on the
%maximization procedure and we want to be able to compare.
if isfield(simparam,'normalize')
    xmag=x.*x;
    xmag=sum(xmag,1);
    normconst=simparam.normalize^2*1./(ones(mparam.tlength,1)*xmag);
    
    x=(normconst.^.5).*x;
end

%tinfo.dt=dt;
tinfo.twindow=mparam.tresponse;
%stimes=samp1dglmh(@glm1dexp,mparam.ktrue,x,tinfo);
sr.y=x;                     %save the stimulus

%compute the number of observed spikes on each trial
sr.nspikes=zeros(1,size(sr.y,2));
for index=1:simparam.niter
   %sr.nspikes(index)=length(stimes{index});
   sr.nspikes(index)=poissrnd(glm1dexp(mparam.ktrue,x(:,index)));
end


%**************************************************************************
%Compute the posterior after each iteration using a gaussian
%approximation and brute force
%**************************************************************************
%mean and covariance of initial prior
pinit=mparam.pinit;
%number of points at which we compute k
numkpts=diff(mparam.krange,1,2)/mparam.dk+1;

%clean up temp variables
clear('m','dindex','tr');

%**************************************************************************
%EKF: Compute the Gaussian Approximation of the posterior using the EKF idea
%**************************************************************************
if (pmethods.ekf.on>0)
fprintf('Compute the ekf approximation of the posterior \n');
numspikes=sr.nspikes';
obsrv.twindow=mparam.tresponse;

%initialize pekf
pmethods.ekf.m=zeros(mparam.tlength,numiter+1);
pmethods.ekf.c=cell(1,numiter+1);
pmethods.ekf.m(:,1)=pinit.m;
pmethods.ekf.c{1}=pinit.c;

pekf.m=pmethods.ekf.m(:,1);
pekf.c=pmethods.ekf.c{1};

for tr=2:numiter+1
    fprintf('EKF iteration %0.2g \n',tr-1);
 
    %construct the observations
    obsrv.n=numspikes(tr-1);
    
    %for debugging of the ekf I also return
    %pll which describes gaussian approximation of the likleihod
    [pekf,pll]=ekfposteriorglm(pekf,(x(:,tr-1)),obsrv,@glm1dexp);
    pmethods.ekf.m(:,tr)=pekf.m;
    pmethods.ekf.c{tr}=pekf.c;
    
    pmethods.ekf.ll.m(:,tr-1)=pll.m;
    pmethods.ekf.ll.eta(:,tr-1)=pll.eta;
    pmethods.ekf.ll.c{tr-1}=pll.eta;
    pmethods.ekf.ll.lambda{tr-1}=pll.lambda;
    
    
end
end

%**************************************************************************
%EKF: Compute the Gaussian Approximation of the posterior using the EKF idea
%     But take the taylor expansion around the max of the likelihood fcn
%     for a single observation
%**************************************************************************
if (pmethods.ekfmax.on>0)
fprintf('Compute the ekf max approximation of the posterior \n');
numspikes=sr.nspikes';
obsrv.twindow=mparam.tresponse;

%initialize pekf
pmethods.ekfmax.m=zeros(mparam.tlength,numiter+1);
pmethods.ekfmax.c=cell(1,numiter+1);
pmethods.ekfmax.m(:,1)=pinit.m;
pmethods.ekfmax.c{1}=pinit.c;

pekfmax.m=pmethods.ekfmax.m(:,1);
pekfmax.c=pmethods.ekfmax.c{1};

for tr=2:numiter+1
    if (mod(tr-1,printiter)==0)
        fprintf('EKF Max iteration %0.2g \n',tr-1);
    end
    %construct the observations
    obsrv.n=numspikes(tr-1);
    
    %for debugging of the ekf I also return
    %pll which describes gaussian approximation of the likleihod
    [pekfmax,pll]=ekfposteriormax(pekfmax,(x(:,tr-1)),obsrv,@glm1dexp);
    pmethods.ekfmax.m(:,tr)=pekfmax.m;
    pmethods.ekfmax.c{tr}=pekfmax.c;
    
    pmethods.ekfmax.ll.m(:,tr-1)=pll.m;
    pmethods.ekfmax.ll.eta(:,tr-1)=pll.eta;
    pmethods.ekfmax.ll.c{tr-1}=pll.eta;
    pmethods.ekfmax.ll.lambda{tr-1}=pll.lambda;
    
    
end
end
fprintf('EKF Max iteration %0.2g: Finished! \n',numiter);

%**************************************************************************
%EKF: Compute the Gaussian Approximation of the posterior using the EKF idea
%     And a 1 step estimator
%**************************************************************************
if (pmethods.ekf1step.on>0)
fprintf('Compute the ekf approximation with one step correction \n');
numspikes=sr.nspikes';
obsrv.twindow=mparam.tresponse;

%initialize pekf
pmethods.ekf1step.m=zeros(mparam.tlength,numiter+1);
pmethods.ekf1step.c=cell(1,numiter+1);
pmethods.ekf1step.m(:,1)=pinit.m;
pmethods.ekf1step.c{1}=pinit.c;

pekf.m=pmethods.ekf1step.m(:,1);
pekf.c=pmethods.ekf1step.c{1};

%initial prior
po=pekf;

for tr=2:numiter+1
    
    fprintf('EKF iteration %0.2g \n',tr-1);
    %warning('1 step estimator for EKF may not be properly implemented \n');
    %construct the observations
    obsrv.n=numspikes(tr-1);
    
    %first update pekf using just the observation on this trial
    %[pekf,pll]=ekfposteriormax(pekf,(x(:,tr-1)),obsrv,@glm1dexp);
    [pekf,pll]=pmethods.ekf1step.ekffunc(pekf,(x(:,tr-1)),obsrv,@glm1dexp);
    pmethods.ekf1step.ll.m(:,tr-1)=pll.m;
    pmethods.ekf1step.ll.eta(:,tr-1)=pll.eta;
    pmethods.ekf1step.ll.c{tr-1}=pll.eta;
    pmethods.ekf1step.ll.lambda{tr-1}=pll.lambda;
    %every kth iteration we do a 1 step update
    if (mod((tr-1),pmethods.ekf1step.onestep)==0)
        
        
        %obsrevations is all observations up to this time pt
        %want a row vector each column a different trial
       obsrv.n=(numspikes(1:tr-1))';
        %keyboard;
       [pekf]=post1stepgrad(po,pekf,(x(:,1:tr-1)),obsrv,@glm1dexp);
        fprintf('Ekf one step correction \n');
    
    else    
    
        %pmethods.ekf1step.ll.m(:,tr-1)=pll.m;
        %pmethods.ekf1step.ll.eta(:,tr-1)=pll.eta;
        %pmethods.ekf1step.ll.c{tr-1}=pll.eta;
        %pmethods.ekf1step.ll.lambda{tr-1}=pll.lambda;
    end
    pmethods.ekf1step.m(:,tr)=pekf.m;
    pmethods.ekf1step.c{tr}=pekf.c;
end
end

%**************************************************************************
%Compute the Gaussian Approximation of the posterior
%**************************************************************************
if (pmethods.g.on>0)
fprintf('Compute the gaussian approximation of the posterior \n');

numspikes=sr.nspikes';
obsrv.twindow=mparam.tresponse;

%initialize 
pmethods.g.m=zeros(mparam.tlength,numiter+1);
pmethods.g.c=cell(1,numiter+1);
pmethods.g.m(:,1)=pinit.m;
pmethods.g.c{1}=pinit.c;

pg.m=pmethods.g.m(:,1);
pg.c=pmethods.g.c{1};

for tr=2:numiter+1
    fprintf('Gauss Approx: iteration %0.3g \n',tr-1);
    %construct the observations
    obsrv.n=numspikes(tr-1);
    
    [pg]=gposteriorglm(pg,(x(:,tr-1)),obsrv,@glm1dexp);
    pmethods.g.m(:,tr)=pg.m;
    pmethods.g.c{tr}=pg.c;
end
end


%**************************************************************************
%Compute the Gaussian Approximation of the posterior using batch updates
%that is considering all observations
%**************************************************************************
if (pmethods.batchg.on>0)
    if (mparam.tlength>1)
        fprintf('Warning: Batch Update of gaussian can only be down if temporal filter is a scalar \n');
        pmethods.batchg.on=0;
    else
        fprintf('Compute gaussian approximation using all the observations the posterior \n');

        bobsrv.twindow=mparam.tresponse;
        bobsrv.n=sr.nspikes;
        %batch update
        [pbatch,lpobsrv,thetapts]=batchposterior(pinit,sr.y,bobsrv,mparam.ktrue);
        %initialize 
        pmethods.batchg.m=pbatch.m;
        pmethods.batchg.c=pbatch.c;
        %mainly for debugging
        pmethods.batchg.thetapts=thetapts;
        pmethods.batchg.lpobsrv=lpobsrv;
        fprintf('finished gaussian approximation using all the observations the posterior \n');

    end
end
%************************************************************************
%compute the likelihood of the data using brute force
%************************************************************************
if (pmethods.b.on>0)
%we want to compute the likelihood of each observation under each possible
%model
%llobsrv -= m x n
%        m - the different observations (correspond to cols of y)
%        n - different models under which likelihood is computed 
%

%kpts=ones(mparam.tlength,1)*[mparam.krange(1):mparam.dk:mparam.krange(2)];
%create kvals for brute for computation = 2d matrix
%where each column is a different model
kpts=matrixindexes((diff(mparam.krange,1,2)/mparam.dk+1)');
kpts=(compmatrix(kpts)-1)*mparam.dk+mparam.krange(1);
kpts=kpts';

llobsrv=ll1dtemp(spikes,x',kpts,dt);


[pmethods.b.p{1} pmethods.b.kvals]=mvgaussmatrix(mparam.krange,mparam.dk,pinit.m,pinit.c);
pmethods.b.p{1}=pmethods.b.p{1}/sum(pmethods.b.p{1});
%***************************************************************
%compute the posterior by brute force
%***************************************************************

for tr=2:numiter+1
    fprintf('Brute Force: Iteration %0.2g \n',tr-1);
    %reshape the likelihood for this observation under all the different
    %models
    %and raise it to the exponential
    if (mparam.tlength==1)
        pobsrv=exp(reshape(llobsrv(tr-1,:),numkpts*ones(1,mparam.tlength),1));
    else
        pobsrv=exp(reshape(llobsrv(tr-1,:),numkpts*ones(1,mparam.tlength)));
    end
    
    pmethods.b.p{tr}=pobsrv.*pmethods.b.p{tr-1};
    
    %normalize it
    z=sum(pmethods.b.p{tr}(:));
    pmethods.b.p{tr}=1/z*pmethods.b.p{tr};
end

clear('tr');
end
%***************************************************************
%compute the posterior by brute force
%using the gaussian prior
%***************************************************************
if (pmethods.bg.on>0)
    %if we havent already computed the log likelihood do so
    if ~(exist(llobsrv,'var') & pmethods.b.on ==0)
        llobsrv=ll1dtemp(spikes,x',kvals',dt);
    end
%so for each update we use brute force but the prior is the gaussian prior
%not the exact prior
pmethods.bg.p=cell(1,numiter+1);
pmethods.bg.p{1}=pinit;

for iter=2:numiter+1
    fprintf('Brute Force Gaussian: Iteration %0.2g \n',iter-1);
    
    %reshape the likelihood for this observation under all the different
    %models
    %and raise it to the exponential
    if (mparam.tlength==1)
        pobsrv=exp(reshape(llobsrv(iter-1,:),numkpts*ones(1,mparam.tlength),1));
    else
        pobsrv=exp(reshape(llobsrv(iter-1,:),numkpts*ones(1,mparam.tlength)));
    end
    
    %construct a matrix to represent the gaussian prior
    %multiply by mparam.dk to handle the continous to discrete conversion
    priormat=mvgaussmatrix(mparam.krange,mparam.dk,pg{iter-1}.m,pg{iter-1}.c)*mparam.dk;
    pmethods.bg.p{iter}=pobsrv.*priormat;
    
    %normalize it
    z=sum(pmethods.bg.p{iter}(:));
    pmethods.bg.p{iter}=1/z*pmethods.bg.p{iter};
end
end
%**************************************************************************
%Compute the peak of the posterior using gradient ascent
%Ascent: is performed on true posterior (i.e using all obsevations)
%**************************************************************************

if (pmethods.gasc.on~=0)
fprintf('Compute the peak of posterior using gradient ascent \n');

obsrv.twindow=mparam.tresponse;

%initialize pekf
pmethods.gasc.m=zeros(mparam.tlength,numiter+1);
pmethods.gasc.m(:,1)=pinit.m;

numspikes=zeros(1,simparam.niter);
numspikes(1,:)=sr.nspikes;
       
for tr=2:numiter+1
    if (mod(tr-1,printiter)==0)
        fprintf('Gradient ascent iteration %0.2g \n',tr-1);
    end
    %construct the observations
     %obsrevations is all observations up to this time pt
        %want a row vector each column a different trial
        obsrv.nspikes=numspikes(1,1:tr-1);
       [mu]=postgradasc(pmethods.gasc.m(:,tr-1),pinit,x(:,1:tr-1),obsrv,@glm1dexp);
    
       %compute the peak of the posterior using all observations
       % [post.m]=postgradasc(thetainit,po,x,obsrv,simparam.(spname).glm);
        
       pmethods.gasc.m(:,tr)=mu;
       %set covariance to zero just so we can plot it
       pmethods.gasc.c{tr}=zeros(mparam.tlength,mparam.tlength);
end
end
fprintf('Gradient Ascent iteration %0.2g: Finished! \n',numiter);


%**************************************************************************
%Newtons method
%**************************************************************************
if (pmethods.newton.on>0)

    fprintf('Compute the ekf approximation with Newtons method \n');
    numspikes=sr.nspikes';
    obsrv.twindow=mparam.tresponse;

    %initialize pekf
    pmethods.newton.m=zeros(mparam.tlength,numiter+1);
    pmethods.newton.c=cell(1,numiter+1);
    pmethods.newton.m(:,1)=pinit.m;
    pmethods.newton.c{1}=pinit.c;

    %prior
    prior.m=pmethods.newton.m(:,1);
    prior.c=pmethods.newton.c{1};
    %obsrv.nspikes=sr.nspikes(1);
    %[dlpdt,dlp2dt]=d2posterior(prior.m,prior,obsrv,x(:,1),@glm1dexp);

%    pmethods.newton.c{1}=dlp2dt;
    %want a row vector each column a different trial
    for tr=2:numiter+1
        obsrv.n=(numspikes(1:tr-1))';
        fprintf('Newton: iteration %0.2g \n',tr-1);
        % The mean of our posterior is the peak of the true posterior
        % therefore its a root of the derivative of the log of the posterior
        % we use newton's method to compute it
        % The new covariance matrix is the inverse of the - hessian at the
        % new ut
        % The seed for newton's methods is the mean from the previous
        % iteration
        
        post.m=pmethods.newton.m(:,tr-1);
        post.c=pmethods.newton.c{tr-1};
        
        newtonerr=inf;
            while (newtonerr >pmethods.newton.tolerance);
                [newpost]=post1stepgrad(prior,post,(x(:,1:tr-1)),obsrv,@glm1dexp);
                newtonerr=(newpost.m-post.m);
                newtonerr=(newtonerr'*newtonerr)^.5;
                post=newpost;
                pmethods.newton.niter(1,tr)=pmethods.newton.niter(1,tr)+1;
            end
        pmethods.newton.newtonerr(tr)=newtonerr;
        pmethods.newton.m(:,tr)=post.m;
        pmethods.newton.c{tr}=post.c;
    end
end


clear('priormat');

%save data
if (~(isempty(ropts.fname)))
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('UpdatePosters: data  saved to %s \n', ropts.fname);
end


