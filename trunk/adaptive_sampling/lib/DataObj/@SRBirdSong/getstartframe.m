%function getstartframe=startframe(obj)
%	 obj=SRBirdSong object
% 
%Return value: 
%	 startframe=obj.startframe 
%
function startframe=getstartframe(obj)
	 startframe=obj.startframe;
