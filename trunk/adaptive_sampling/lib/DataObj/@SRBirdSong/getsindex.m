%function getsindex=sindex(obj)
%	 obj=SRBirdSong object
% 
%Return value: 
%	 sindex=obj.sindex 
%
function sindex=getsindex(obj)
	 sindex=[obj(:).sindex];
