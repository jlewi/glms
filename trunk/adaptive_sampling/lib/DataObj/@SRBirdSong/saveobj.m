%function sobj=saveobj(sobj)
%
%
%Explanation: this function gets called by save when we save the object
%
%Revisions: 
%   03-26-2008: We need to save one copy of the BSData object so that when
%   we reload this SRBirdSong data we can process the data with the same
%   parameters as used to create the SRBirdSong data
function sobj=saveobj(sobj)

    %ptrbsdata- is a pointer to a BSData object
    %   We need to save one copy of the object, since all objects in the
    %   array should point to the same BSData object
    %   Therefore we set obj(1).ptrbsdata to an actual copy of the BSData
    %   object and zero out the entries for the rest of the elements.
    %zero out ptrbsdata because we can't save pointers
    sobj(1).ptrbsdata=sobj(1).ptrbsdata.bsdata;
    for index=2:numel(sobj)
        sobj(index).ptrbsdata=[];
    end
 %   for index=1:length(sobj)
  %      sobj(index).ptrbsdata=[
    
   