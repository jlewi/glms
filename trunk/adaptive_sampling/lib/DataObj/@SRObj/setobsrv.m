%function obj=setobsrv(obj,val)
%	 obj=SRObj object
% 
%Return value: 
%	 obj= the modified object 
%
function obj=setobsrv(obj,val)
    for index=1:numel(obj)
	 obj(index).obsrv=val(:,index);
    end