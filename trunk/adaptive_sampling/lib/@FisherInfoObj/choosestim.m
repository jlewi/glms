%function stim=choosestim(finf,post,shist,einfo,mparam,glm)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%       glm   - structure specifying the glm
%
% Return value
%       stim - the stimulus for the next trial
%       fmax    - expected information for this stimulus
%
% Explanation: Chooses the stimulus based on the procedure and parameters specified
%   when this object was created.
function [stim fmax]=choosestim(finfo,post,shist,einfo,mparam,glm)
%structure which specifies the method to call based on the procedure
func.('poiss2d')=@choosepoiss2d;
func.('boundbox')=@boundbox;

%check if einfo is suppled
if isempty(einfo)
    einfo=[];
end
if ~isfield(einfo,'eigd')
    einfo.eigd=[];
end
if ~isfield(einfo,'evecs')
    einfo.evecs=[];
end

%call the appropriate function for the method
[stim, fmax]=func.(finfo.procedure)(finfo,post,shist,einfo,mparam,glm);

%*************************************************************************
%
% End Main Function
%
%************************************************************************

%**************************************************************
%choosepoiss2d:
%
%This function optimizes the poisson distribution in (mu, sigma) space.
%It does a full 2-d search in this space so it can potentially handle
%non-canonical responses. Here we do 1-d search using fminbnd over mu
%for each value of mu we compute numpts evenly spaced points between
%sigmamin and sigmax. We return the value 
function stim=choosepoiss2d(finfo,post,shist,einfo,mparam,glm)
error('not implemented');