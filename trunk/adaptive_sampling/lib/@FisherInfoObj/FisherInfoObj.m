%function finfo=FisherInfoObj(initp)
%       initp- stucture of field value pairs
%               -field names specify the fields to intiate
%function finfo=FisherInfoObj(fieldname,value, fieldname, value,....)
%   fieldname,value - list of fieldname and value pairs to intialize to
%
% Explanation: initializes the FisherInfoObj .
function finfo=FisherInfoObj(varargin)
%list of fields which must be provided by the user
%we set these values to 1 if the user provided them
%this way at the end its each to check if user provided all the required
%parameters
required.procedure=0;;
required.pproc=0;

%for each procedure we call a different method
%to initializae the pproc field
%the following structure specifies which method to call for each procedure
initf.('poisson2d')=@initpoiss2d;
initf.('boundbox')=@initboundbox;

%**************************************************************************
%FisherInfo structure
%**************************************************************************
%optparam - optimset - structure to specify optimization parameters
%procedure - name - describing which procedure to call to handle the
%                                   optimization
%                   -procedure affects what parameters need to be specified
%                   -possible values 'logistic', 'poissoncanon','poisson2d'
%
%pproc       - parameters for the various procedures
%                       -see initialization procedures for each method at
%                       end of function for specific fields
%
%debug      - whether to do additional error checking within the methods
%wmsgon   - set to ~=0 in order to print warning/info messages 
%                    setting to zero causes the 'display' option pased to
%                    fmincon and other methods to be set to off
%numtol     - numerical tolerance. This value is used to determine
%                       how much tolerance to allow when deciding if 2
%                       numbers are equal


%*********************************************************
%Procedure POissonCanon'
%*********************************************************
finfo=struct('optparam',[],'procedure','poissoncanon','pproc',[],'debug',1,'wmsgon',0,'numtol',10^-8);

%**************************************************************************
%if the user specified a list of string value pairs, create a structure
%from this and then do initialization
if (nargin==1)
	initp=varargin{1};
    if ~isstruct(initp)
        error('If initialized with 1 argument. Argument must be a structure');
    end
else
    %convert the string names to key value pairs
    if (mod(nargin,2)~=0)
        error('You must specify key-value pairs');
    end
    initp=[];
    for j=1:2:nargin-1
        initp.(varargin{j})=varargin{j+1};
    end
end

%*****************************************************************
%initp stores all the fields user wants to set
%loop through them and set the appropriate fields of mparam
%************************************************************
fnames=fieldnames(initp);
for k=1:length(fnames)
switch fnames{k}
    case 'optparam';
        finfo.optparam=initp.optparam;        
    case 'procedure';
        finfo.procedure=initp.procedure;
        required.procedure=1;
    case 'pproc';
        finfo.pproc=initp.pproc;
        required.pproc=1;        
    otherwise
        error(sprintf('%s not a recognized parameter for FisherInfoObj \n'));
end
end

%**************************************************************************
%validate the parameters
%**************************************************************************
%*********************************************************
% check required parameters are provided
freq=fieldnames(required);
for k=1:length(freq)
    if (required.(freq{k})==0)
        error(sprintf('Required parameter %s not specified \n',freq{k}));
    end
end
%***************************************8
%check the procedure name is valid
switch finfo.procedure
    case 'poisson2d'
        warning('Code for this procedure has never be tested and I dont think its complete');
    case 'boundbox'
    case 'poissoncanon'
        error('Code to handele poissoncanon is not yet implemented use legacy code');
    case 'logistic'
         error('Code to handele poissoncanon is not yet implemented use legacy code');
    otherwise
        error('Unrecognized procedure name');
end

%call the appropriate function to iniatlize and validate the parameters for
%this procedure
finfo.pproc=initf.(finfo.procedure)(finfo.pproc);

%inherit from stimchooser
finfo=class(finfo,'FisherInfoObj',StimChooserObj);

%**************************************************************************
%
%End main function
%
%**************************************************************************

%***************************************************************
%ipoisson2d Procedure: parameters
%   .numpts - number of points to use
%*****************************************************************
function [pproc]=initpoiss2d(initp)
pproc=[];
fnames=fieldnames(initp);
for k=1:length(fnames)
switch fnames{k}
    case 'numpts';
        if (numpts <1)
            error('numpts must be > 0');
        end
        pproc.numpts=initp.numpts;
    otherwise
        error(sprintf('%s not a recognized parameter for this optimization procedure \n'));
end
end

%***************************************************************
%initboundbox Procedure: parameters
%   .numint - structure of paraterms for numerical integration of the
%   objective function
%               .confint=.999;               %how much confidence for integrating the inner expectation
%              .inttol=10^-6;        % tolerance for numerical integration
%*****************************************************************
function [pproc]=initboundbox(initp)
pproc=[];
fnames=fieldnames(initp);
for k=1:length(fnames)
switch fnames{k}
    case 'numint';
        pproc.numint=initp.numint;
    otherwise
        error(sprintf('%s not a recognized parameter for this optimization procedure \n'));
end
end
if ~isfield(pproc,'numint')
    error('You must specify the parameter .numint which is the tolerances for numerical optimization');
end
%check user specified the tolerances
%do this so we can ensure tolerances get saved in the simulation results
if ~isfield(pproc.numint,'confint')
    error(sprintf('You must specify numint.confint'));
end
if ~isfield(pproc.numint,'inttol')
    error(sprintf('You must specify numint.inttol'));
end