%function stim=boundbox(finfo,post,shist,einfo,mparam,glm)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%                   -this should be the full eigendecomposition
%       mparam - MParamObj object of model parameters
%       glm   - structure specifying the glm
%
% Return value
%       stim - the stimulus for the next trial
%       fmax -expected information for the stimulus
% Explanation: Chooses the stimulus by computing the smallest rectangle in
% (mu_epsilon,sigma^2) space that encloses the feasible region under
%the magnitude constraint. OPtimize over this region.
%Test whether the optimal solution is feasible under the magnitude
%constraint.
function [stim fmax]=boundbox(finfo,post,shist,einfo,mparam,glm)

%******************************************************************
%compute the bounding box
%lb =[min(mu); min(sigmasq)];
%ub =[max(mu); max(sigmasq)];
lb=[0;0];
ub=[0;0];
%bounds in mu
%magnitude of mean
mumag=(post.m'*post.m)^.5;
%if magnitude of mean is zero than mu_epsilon is always zero (so just do
%line search)
if (mumag~=0)
    lb(1)=-mparam.mmag*mumag;
    ub(1)=-lb(1);
end


%if einfo is empty compute it
if isempty(einfo.evecs)
    warning('einfo not supplied computing svd');
    [einfo.evecs einfo.eigd]=svd(post.c);
    einfo.esorted=-1;
    einfo.eigd=diag(einfo.eigd);
end

%make 
%bounds in sigma
%einfo must be eigenvalues for full covariance matrix not just the stimulus
%terms. If we only have the eigendecomposition of the stimulus terms
%we can either 
%1) compute the svd or 
%2) find sigmamax=max x^T Ck x^t +2
%x^t*Cka shist - so find sigmamax when we have linear term due to the spike
%history terms. Solution 2 should be more efficient. But I didn't want to
%implement this until I knew it was necessary
if (mparam.alength+mparam.klength==length(einfo.eigd))
lb(2)=mineval(einfo)*mparam.mmag^2;
ub(2)=maxeval(einfo)*mparam.mmag^2;
else
    warning(sprintf(['Eigendecomposition of only stimulus covariance matrix supplied\n computing eigdendecomposition of full covariance matrix.' ...
    'more efficient solution is to solve the quadratic optimization with a linear term for sigmamax and sigmamin']));
    [eigd]=svd(post.c);
    lb(2)=eigd(end)*mparam.mmag^2;
    ub(2)=eigd(1)*mparam.mmag^2;
end

%************************************************************************
%optimize over the bounding box
%**************************************************************************
%implicit function which computes the function to optimize
%multiply by -1 bc we will minimize it not maximize it
funcfinfo=@(x)(-1*expdetint(x(1),x(2),glm,finfo.pproc.numint));   

A=[];
b=[];
Aeq=[];
beq=[];

%initial guess
%use guess from previous iteration which is stored in finfo.pproc.xinit;
%if this field is not defined (indicating optimization hasn't been
%performed then use middle of feasible region.
xinit=(lb+ub)/2;
if isfield(finfo.pproc,'xinit')
    xinit=fino.pproc;
    %make sure its right dimensions
    if (size(xinit,1)~=2)
        xinit=[];
    end
      if (size(xinit,2)~=1)
        xinit=[];
      end
end
%make sure xinit is in valid region
if (xinit(1)<lb(1))
    xinit(1)=lb(1);
end
if (xinit(1)>ub(1))
    xinit(1)=ub(1);
end
if (xinit(2)<lb(2))
    xinit(2)=lb(2);
end
if (xinit(2)>ub(2))
    xinit(2)=ub(2);
end
%compute the optimium in mu sigma space. 
if (finfo.wmsgon==0)
options=optimset('Display','off');
end
[msopt fmax]= fmincon(funcfinfo,xinit,A,b,Aeq,beq,lb,ub,[],options);
fmax=-fmax;
%******************************************
%find stimulus which yields muopt, sigmaopt 
%*****************************************
%1. compute sigmamax, sigmamin for this value of mu
% quadmod requires eigendecomposition of just stimulus terms
if (length(einfo.eigd)==mparam.klength)
    eigck=einfo;
else
    warning('eigendecomposition decomposition of just stim terms Ck is required \n computing it via SVD');
    [eigck.evecs eigck.eigd]=svd(post.c(1:mparam.klength,1:mparam.klength));
    eigck.eigd=diag(eigck.eigd);
    eigck.esorted=-1;
end
muk=post.m(1:mparam.klength);
quad=quadmodshist(post.c,eigck,muk,shist);
optparam=optimset('TolX',10^-12,'TolF',10^-12);

%the optimal value of the projection along mu
%msopt(1) stores the optimal value of the dot product of the mean and the
%stimulus
%so we have to divide this by the magnitude of the mean
%because we want the projeciton along the mean of the stimulus
muprojopt=msopt(1)/(muk'*muk)^.5;
[qmax,qmin,xmax,xmin]=maxminquad(quad,muk,muprojopt,mparam.mmag,optparam);
%**********************************************************
%2.check if stimulus is in valid region i.e does it violate power constraint
%Note: It is expected that this optimization procedure will return
%stimuli which violate the power constraint.
%************************************************************
if ((qmin<msopt(2)) && (msopt(2)<qmax))
    %stimulus is valid
    %so we need to find a linear combination of xmax, xmin which gives
    %sigmaopt
    A=post.c(1:mparam.klength,1:mparam.klength);
    if (mparam.alength>0)
    b=shist'*post.c(mparam.klength+1:end,1:mparam.klength);
    d=shist'*post.c(mparam.klength+1:end,mparam.klength+1:end)*shist;
    else
        b=zeros(mparam.klength,1);
        d=0;
    end
    options = optimset('Jacobian','on');
    fs=@(s)stimsigma(s,msopt(2),A,b,d,xmin,xmax);
    [sopt,sigma]=fsolve(fs,.5,options);
    
    stim=(1-sopt)*xmin+sopt*xmax;
    %check it if debuggin is turned on
    if (finfo.debug~=0)
        if ((stim'*post.m-msopt(1))>finfo.numtol)
            error('Stimulus doesnt have optimal projection on mean');
        end
          if (([stim;shist]'*post.c*[stim;shist]-msopt(2))>finfo.numtol)
            error('Stimulus doesnt yield optimal value of sigma');
        end
    end
    
else
    warning('Optimal stimulus violates magnitude constraint \n solution is not guaranteed to be optimal');
    %do search over upper boundary to find a solution which doesn't violate the
    %magnitude constraint
    optparam=optimset('TolX',10^-12,'TolF',10^-12);
    finfofunc=@(mueps,sigmasq)expdetint(mueps,sigmasq,glm,optparam);
    [stim,muprojpt,fmax]=fishermaxshist(post,shist,einfo,mparam,finfofunc,optparam);
end


%save stim as initialization for next run
finfo.pproc.xinit=stim;


%***************************************
%function we need to solve to find stimulus which gives (muopt,
%sigma^2_opt)
%see aistat eqn 24.
function [d,jac]=stimsigma(s,sigmasqopt,A,b,d,xmin,xmax)
    x=(1-s)*xmin+s*xmax;
   
    sigmasq=x'*(A*x)+b'*x+d;
    d=sigmasq-sigmasqopt;
    %return the jacobian
    jac=(2*x'*A+b')*(-xmin+xmax);