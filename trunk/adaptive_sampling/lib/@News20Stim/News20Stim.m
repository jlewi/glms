%function pobj=PreCompMIObj('filename',fname,'docfile', docfile,'nrand',nrand)
%   -fname, file which contains the PreCompMIObj to base this object on
%   -nrand - number of initial stimuli which should be selected randomly
%                -set this to inf to use only random sampling.
%   -docfile - file contianin the matrix which provides information about
%             the documents
%
% Explanation: Child of PreCompMI and ObserverBase
%   - this allows this object to serve as both the StimChooser object
%   (which picks the stimuli) and the OBserver object which labels them
%   - we want to do this because we need to look up the stored label for
%   the stimulus.
%   - when we call choosestim, we keep track of the index selected
%       this way when we call observe we can quickly look up the label for
%       the stimulus without having to search through all the inputs
%
%   the initial nrand objects are chosen randomly. After that the stimuli
%   ar chosen by calling PreCompMIOBj.choosestim
function pobj=News20Stim(varargin)

%nrand - number of initial stimuli which should be chosen randomly
%been chosen already);
%
% docfile - file from which document info is loaded
%               this is a file containing columns where each column is
%               vector saying which words are in a particular document
%
%               this can also be a .mat file which contains the docs()
%               structure as previously read in.
%               this is useful because my parsing routine is unnecessarily
%               slow.
% docs    - structure array identifying the different documents
%       .words - vector of 1's and 0's specifying which words are in the
%       document
%       .classid - number identifying the class
% classses - structure identifying the classes
%       .class - newsgroup name
%       .classid - number identifying the newsgroup
%
% trainset - indexes of the elements in docs to serve as training set
% testset   - indexes of the elements in docs to serve as testing set
%
% stimset - the set of indexes from which to select the stimulus
%                  this starts off as just trianset. But because we don't
%                  sample with replacement this will decrease on every
%                  iteration
%                  THIS REQUIRES THAT CHOOSE STIM RETURN THE OBJECT
%                   because we modify it
%laststimind - index of the last stimulus chosen by choosestim
pobj=struct('nrand',0,'docfile',[],'docs',[],'classes',[],'trainset',[],'testset',[],'stimset',[],'laststimind',0,'classname',[]);
pbase=[];   %will point the base object.

%blank constructor for when we load from file
if (nargin==0)

    %create the object and register it as subclass of StimChooserObj
    pbase=PrecompMIObj();
    obase=ObserverBase();
    pobj=class(pobj,'News20Stim',pbase,obase);
else

    %list of required parameters
    required.nrand=0;
    required.filename=0;
    required.docfile=0;
    required.classname=0;
    for j=1:2:nargin
        switch varargin{j}
            case {'filename','fname'}
                filename=varargin{j+1};
                required.filename=1;
            case 'nrand'
                pobj.nrand=varargin{j+1};
                if (pobj.nrand<=0)
                    error('nrand must be >0');
                end
                required.nrand=1;
            case 'docfile'
                pobj.docfile=varargin{j+1};
                required.docfile=1;
          case 'classname'
                pobj.classname=varargin{j+1};
                required.classname=1;
            otherwise
                error(sprintf('%s unrecognized parameter \n',varargin{j}));
        end
    end

    %check if all required parameters were supplied
    freq=fieldnames(required);
    for k=1:length(freq)
        if (required.(freq{k})==0)
            error(sprintf('Required parameter %s not specified \n',freq{k}));
        end
    end

    %parse the document
    [pathstr,name,ext]=fileparts(pobj.docfile);
    if (strcmp(ext,'.mat')==1)
       docs=load(pobj.docfile,'docs');
       pobj.docs=docs.docs;
       clear('docs');
    else
    [pobj.docs]=parsedocs(pobj.docfile);
    end
    [pobj.docs,pobj.trainset,pobj.testset,pobj.classes]=splitdocs(pobj.docs,pobj.classname);
    %set of possible stimuli is just the trainingset
    pobj.stimset=pobj.trainset;
    %create the base object from the file
    pbase=PreCompMIObj('filename',filename);
    obase=ObserverBase();

    %create the object and register it as subclass of StimChooserObj
    pobj=class(pobj,'News20Stim',pbase,obase);


end

%*******************************************************************
%   docs - aray of documents
%   classname - name of documents to be put in class 1
%
% Explanation: splits the documents into training and testing sets
%   classname - name of newsgroup to be group 1. All other documents are
%   put in group 0.
function  [docs,trainset,testset,classes]=splitdocs(docs,classname)  
    %split the data in half to construct the test sets
    iclass1=(strmatch(classname, {docs(:).class}))';
    iclass2=1:length(docs);
    iclass2(iclass1)=nan;
    iclass2=iclass2(find(~isnan(iclass2)));
    mid1=ceil(length(iclass1)/2);
    mid2=ceil(length(iclass2)/2);
    trainset=[iclass1(1:mid1) iclass2(1:mid2)];
    testset=[iclass1(mid1+1:end) iclass2(mid2+1:end)];
    
    %create the classes
    classes=[];
    classes(1).classid=0;
    classes(1).class='other';
    classes(2).classid=1;
    classes(2).class=classname;
    
    for j=1:length(iclass1)
        docs(iclass1(j)).classid=classes(2).classid;
    end
    for j=1:length(iclass2)
        docs(iclass2(j)).classid=classes(1).classid;
    end
    
    fprintf('Documents in class %s : %d\n',classname,length(iclass1));
    fprintf('Documents not in class %s : %d\n',classname,length(iclass2));
    
%*************************************************************
%Load the structure containing the docs from a matlab file
%split the data into training and testing samples
%
%gname - the newsgroups we want to recognize
%               that is all newssgroups with this newsgroup id will be
%               associated with class 1, all others will be class 0;
function [docs,trainset,testset,classes]=docsfrommat(docfile,classname)

    docs=load(docfile,'docs');
    docs=docs.docs;
 
%*************************************************************
%Load the file containing the matrix describing the data
%construct the docs structure
%split the data into training and testing samples
function [docs,trainset,testset,classes]=parsedocs(docfile)

fid= fopen(docfile,'r') ;
if (fid~=-1)
    tline=fgetl(fid);
    docs=[];
    ndocs=0;
    classes=[];         %contain information about the classes
    nclasses=0;
    while (tline~=-1)
        ndocs=ndocs+1;
        if (mod(ndocs,100)==0)
            fprintf('Parsing ndocs =%d \n', ndocs);
        end
        ind=strfind(tline,' ');
        docs(ndocs).path=tline(1:(ind(1)-1));
        docs(ndocs).class=tline((ind(1)+1):(ind(2)-1));
        docs(ndocs).wcounts=(str2num(tline((ind(2)+1):end)))';

        %see if this class has already been identified
        if (nclasses>0);
            cind=strmatch(docs(ndocs).class,{classes(:).class},'exact');
        else
            cind=[];
        end
        %its a new class
        if isempty(cind)           
            %its a new class
            nclasses=nclasses+1;
            classes(nclasses).classid=nclasses-1;  %classid's should start with 0.
            classes(nclasses).class=docs(ndocs).class;
            cind=nclasses;
        end
        docs(ndocs).classid=classes(cind).classid;
        tline=fgetl(fid);
    end
    fclose(fid);

  %  if (nclasses>2)
   %     error('Can only handle 2 classes');
    %end
end

%save it to a file
[pathstr,name,ext]=fileparts(docfile);
save(fullfile(pathstr,[name '.mat']),'docs');
fprintf('Docs saved to %s \n',fullfile(pathstr,[name '.mat']));
    
