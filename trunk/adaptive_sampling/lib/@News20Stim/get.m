%function get(News20Stim,prop1,prop2)
%   'nwords', 'klength' - vocabulary size. i.e the dimension of each input
%                                    vector
%Explanation 
%       returns the specified properties
function [varargout]=get(news,varargin);

    nout=0;
    for j=1:length(varargin)
        switch varargin{j}
            case {'nwords','klength'}
                nout=nout+1;
                varargout{nout}=size(news.docs(1).wcounts,1);
            otherwise
                error(sprintf('unrecognized property %s \n'),varargin{j})
        end
    end