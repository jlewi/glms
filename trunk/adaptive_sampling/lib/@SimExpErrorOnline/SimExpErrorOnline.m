%function sim=SimExpErrorOnline('outfile','label',simfile);
%   outfile - File where we save the data
%   label   - label for the data
%   simfile - The file containing the actual simulation object used to
%             compute tis data
%
%This object is used to compute and store the mean squared error of the
%estimated and true theta
%
%Revisions:
%
classdef (ConstructOnLoad=true) SimExpErrorOnline < handle
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %
    %
    %outfile     - where we store the object
    %
    %label    - label for the data
    %         - storing the label should make it unnecessary to actual load
    %           the Simobjects if we just want to plot the results 
    %
    %experr   - This is a structure which stores the actual data
    %
    %nerr     - the current size of the arrays
    %eindex   - The location in the arrays where the next data point will
    %           go
    %gsize    - How many elements to increase the array size by when we
    %           grow the array
    properties(SetAccess=public,GetAccess=public)
        label=[];
    end
    properties(SetAccess=private, GetAccess=public)        
        outfile=[];
        simfile=[];
        version=struct();
        
        experr=struct('trials',[],'exerr',[],'errstim',[],'errshist',[],'errbias',[]);
        
        nerr=0;
        eindex=0;
        gsize=10000
    end

    
    methods(Static)
       obj=loadobj(lobj); 
    end
    methods
        
     
       
        
        function obj=SimExpErrorOnline(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.                   
            con(1).rparams={'outfile','simfile','label'};


               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    %do nothing
                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);
            
            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     if (length(params.simfile)>1)
                         error('simfile should be a single file');
                     end
                     
                     obj.simfile=params.simfile;
                     obj.outfile=params.outfile;
                     obj.label=params.label;

                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end


            %set the version to be a structure
            obj.version.SimExpErrorOnline=091108;

        end
    end
end


