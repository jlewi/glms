%function compexerr(obj,npts)
%       -npts - Desired number of data points. If the size of the arrays is
%       less than this then we grow them.
%
%Explanation: Grow the arrays if necessary to ccreate a data point 
%              where index can be stored
%
function grow(obj,npts)


if (npts>obj.nerr)
   %we need to grow the arrays
   fnames=fieldnames(obj.experr);
   
   
   gsize=max([npts-obj.nerr,obj.gsize]);
   
   for index=1:length(fnames)
      tmp=obj.experr.(fnames{index});
      
      obj.experr.(fnames{index})=nan(1,length(tmp)+gsize);
      obj.experr.(fnames{index})(1:length(tmp))=tmp;
   end
   obj.nerr=length(tmp)+gsize;
    
end