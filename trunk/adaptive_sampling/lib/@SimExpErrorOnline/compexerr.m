%function compexerr(obj)
%   obj  - the SimExpErrorOnline object
%   sim  - The simulation object
%   trial - which trial are we computing the error for
%   post  - the posterior
%Explanation: COmpute the expected err error between the map and the final
%batch estimate. The data is then added to the data stored in this
%instance of SimExpErrorOnline
function compexerr(obj,sim,trial,post)


%check if we need to grow the array
obj.eindex=obj.eindex+1;


grow(obj,obj.eindex)

ttrue=sim.observer.theta;

mobj=sim.mobj;


mu=getm(post);
c=getc(post);

obj.experr.trials(obj.eindex)=trial;

errall=errfun(mu,c,ttrue);


obj.experr.exerr(obj.eindex)=errall;



experr=struct('trials',[],'exerr',[],'errstim',[],'errshist',[],'errbias',[]);

mustim=mu(mobj.indstim(1):mobj.indstim(2));
cstim=c(mobj.indstim(1):mobj.indstim(2),mobj.indstim(1):mobj.indstim(2));
tstim=ttrue(mobj.indstim(1):mobj.indstim(2));
errstim=errfun(mustim,cstim,tstim);
obj.experr.errstim(obj.eindex)=errstim;



if (mobj.alength>0)
    mushist=mu(mobj.indshist(1):mobj.indshist(2));
    cshist=c(mobj.indshist(1):mobj.indshist(2),mobj.indshist(1):mobj.indshist(2));
    tshist=ttrue(mobj.indshist(1):mobj.indshist(2));

    errshist=errfun(mushist,cshist,tshist);

    obj.experr.errshist(obj.eindex)=errshist;

end

if (mobj.hasbias)
    musbias=mu(mobj.indbias);
    csbias=c(mobj.indbias,mobj.indbias);
    tsbias=ttrue(mobj.indbias:mobj.indbias);

    errbias=errfun(musbias,csbias,tsbias);
    obj.experr.errbias(obj.eindex)    =errbias;
end


function err=errfun(mu,c,ttrue)
err=trace(c)+mu'*mu-2*ttrue'*mu+ttrue'*ttrue;
