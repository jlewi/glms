%function saveerr(obj)
%
%
%Explanation: Saves the instance to the file given by outfile
function simerr=saveerr(simerr)


noerr=1;
emsg='';
try
    %save without append becasue append causes error
    if isa(simerr.outfile,'FilePath')
        save(simerr.outfile.getpath(),'simerr');
    else
        save(simerr.outfile,'simerr');
    end
    fprintf('Saved the simulation error \n');
catch e
    emsg=sprintf('Error occured while trying to do a periodic save of the data. Error message:\n %s\n',e.message);
    noerr=0
end

noerr=gcat(noerr);
if any(noerr==0)
    %any error occured on one of the nodes
    if (noerr(labindex)==1)
        emsg=sprintf('Error occured on labs %d while trying to do a periodic save of the data. \n', find(noerr==0));
    end
    emsg=sprintf('This error appears to be due to the use of the append option when saving the data. \n Simulation will continue without saving the data. \n',emsg);
    fprintf('%s', emsg);

end