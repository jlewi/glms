%function [r,d1,d2]=glm1dexp(k,x)
%       k - filter coefficents
%           should be column vector
%       x - stimulus
%
%function r=glm1dexp(u)
%       u - k'x
%       d1 - 1st derivative
%       d2 - 2nd derivative
%       d1,d2 computed w.r.t u=kx if only 1 parameter provided otherwise
%           w.r.t x
%Explanation computes the rate of a 1-d GLM that 
%   uses an exponential function as the nonlinear transformation
%
%   r(t)=exp(k'x);
function [r,d1,d2]=glm1dexp(k,x)
    
    if exist('x','var')
        r=exp(k'*x);
        %compute the derivatives
        d1=k*r;
        d2=k.^2*r;
    else
        r=exp(k);
        %derivatives arejust the rate
        d1=r;
        d2=r;
    end
    
    
    

