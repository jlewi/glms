%function [r,d1,d2]=glmproj(glmproj,alpha)
%   glmproj - input to the nonlinearity
%  alpha - parameter >1
%function r=glm1dexp(u)
%       u - k'x
%       d1 - 1st derivative
%       d2 - 2nd derivative
%       d1,d2 computed w.r.t u=kx if only 1 parameter provided otherwise
%           w.r.t x
%Explanation computes the rate of a 1-d GLM that 
%       function provides an alternate nonlinearity for a GLM which can
%   be used for generating simulated responses.
%   This function fulfills the criteria for the function to be log concave
%   see [Paninski04]
%
%   see mathematica notebook nonlinlinks.nb for a computation of the
%   derivatives showing its convex and log concave.
%
%WARNING: This function has a different syntax from my other nonlinearities
%because the second parameter is alpha rather than x.
%We do some error checking to make sure we aren't called with the form k,x
function [r,d1,d2]=nonlin_power(glmproj,alpha)
    

    %check alpha is not a vector
    if ~isscalar(alpha)
        error('alpha should be a scalar parameter');
    end
    if (size(glmproj,1)>1)
        error('glmproj should be a scalar or a row vector');
    end

    if (alpha<1)
        error('alpha must be >=1');
    end
    
    %nonlinearity is
    % f(glmproj)=glmproj^alpha if glmproj>0
    %                = 0                         glmproj <0
    
    ind=find(glmproj<=0);
   
    r=glmproj.^alpha;
    r(ind)=0;
    
    %first derivative
    d1=alpha*glmproj.^(alpha-1);
    d1(ind)=0;
    
    %second derivative
    if (alpha==1)
        d2=zeros(length(glmproj));
    else
        d2=alpha*(alpha-1)*glmproj.^(alpha-2);
    end
    