%function [r,d1,d2]=nonlin_explin(k,x)
%       k - filter coefficents
%           should be column vector
%       x - stimulus
%
%function r=nonlin_explin(u)
%       u - k'x
%       d1 - 1st derivative
%       d2 - 2nd derivative
%       d1,d2 computed w.r.t u=kx if only 1 parameter provided otherwise
%           w.r.t x
%Explanation computes the rate of a 1-d GLM that 
%       function provides an alternate nonlinearity for a GLM which can
%   be used for generating simulated responses.
%   This function fulfills the criteria for the function to be log concave
%   see [Paninski04]
function [r,d1,d2]=nonlin_explinlog(k,x)
    
    if exist('x','var')
        r=zeros(1,size(k,2));
        u=sum(k.*x,1);
        gz=find(u>=0);
        lz=find(u<0);
        r(lz)=exp(u(lz));
        r(gz)=1+u(gz);
        %compute the derivatives
        %d1=k*r;
        %d2=k.^2*r;
    else
        
        r=zeros(1,size(k,2));
        d1=zeros(1,size(k,2));
        d2=zeros(1,size(k,2));

        
        gz=find(k>=0);
        lz=find(k<0);
        %u<0        
        r(lz)=exp(k(lz));
        d1(lz)=r(lz);
        d2(lz)=r(lz);
        
        %u>0        
        r(gz)=1+k(gz);
        d1(gz)=1;
        d2(gz)=0;
    end
    
    
    

