%function [r,d1,d2]=glm1dlog(k,x)
%       k - filter coefficents
%           should be column vector
%       x - stimulus
%
%function r=glm1dexp(u)
%       u - k'x
%       d1 - 1st derivative
%       d2 - 2nd derivative
%       d1,d2 computed w.r.t u=kx if only 1 parameter provided otherwise
%           w.r.t x
%Explanation computes the rate of a 1-d GLM that 
%       function provides an alternate nonlinearity for a GLM which can
%   be used for generating simulated responses.
%   This function fulfills the criteria for the function to be log concave
%   see [Paninski04]
%
%   see mathematica notebook nonlinlinks.nb for a computation of the
%   derivatives showing its convex and log concave.
function [r,d1,d2]=glm1dlog(k,x)
    
    if exist('x','var')
        r=log(1+exp(k'*x));
        %compute the derivatives
        %d1=k*r;
        %d2=k.^2*r;
    else
        ek=exp(k);

        r=log(1+ek);
        %derivatives arejust the rate
        d1=ek./(1+ek);
        %the following computation caused numerical errors
        %could this be because of how it represents floats when
        %subtracting matrices
        %oldway=ek./(1+ek)-ek.^2/(1+ek).^2;
        d2=ek./(1+ek).^2;

        %warning('not sure derivatives are correct');
    end
    
    
    

