%QuadInpTanSpace('evec','eig')
%   evec,eigd- factorization of the matrix into a r principal compoennts
%         Matrix is evec*diag(eigd)*evec'
%
%QuadInpTanSpace('mat',m,'rank')
%   mat - the full matrix i.e not factorized
%         this is a dim(stim)xdim(stim) matrix
%       - This is the matrix formed by summing the basis vectors
%         mat= sum_i \phi_i \phi_i^T
%           
%   rank - what rank of an approximation of the matrix to use
%
%QuadInpTanSpace('theta','dimsparam','rank')
%   theta - vector of (dim(stim)^2x dim(stim))/2
%         - this vector is the upper triangle part of Q
%           where Q is the matrix in feature space
%           i. E(r)=f(x'Qx);
%
%Explanation: This object computes the tangent space for the quadratic
%input nonlinearity (i.e the energy model)
%   01-20-2008 This object was created from LowRankTanSpace so there is
%   probably still some old code
function obj=QuadInpTanSpace(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'evec','eigd'};
con(1).cfun=1;

con(2).rparams={'mat','rank'};
con(2).cfun=1;

con(3).rparams={'theta','dimsparam','rank','inplen'};

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
% rank     - rank of the manifold
%
%qinp      - is an object of QuadNonLinInp
%            this provides functions for reshaping the parameters as a
%            matrix
%
%declare the structure
obj=struct('version',071204,'rank',[],'qinp',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'QuadInpTanSpace',TanSpaceBase());
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        evec=params.evec;
        eigd=params.eigd;
        if (size(evec,2)~=length(eigd))
            error('Size of evec and eigd dont match');
        end
        obj.rank=size(evec,2);
        
        %create the input object which maps stimuli into feature space
        obj.qinp=QuadNonLinInp('d',size(evec,1));
        mat=evec*diag(eigd)*evec';
        
        theta=projqmat(obj.qinp,mat);
        
        dimsparam=numel(evec)+numel(eigd);
        
        %instantiate a base class if there is one
        pbase=TanSpaceBase('theta',theta,'dimsparam',dimsparam);
        obj=class(obj,'QuadInpTanSpace',pbase);
        
        obj=setsparam(obj,packasvec(obj,evec,eigd));

        obj=setbasis(obj,gradsubmanifold(obj,getsparam(obj)));
    case 2        
        m=params.mat;
        obj.rank=params.rank;
        stimlen=size(m,1);
        %reshape m as a vector and construct the class
        %QuadInpNonLin handles the conversions between vectors and matrix
        %representations
        obj.qinp=QuadNonLinInp('d',stimlen);
        bparams.theta=projqmat(obj.qinp,m);
        
        %we need to store the basis vectors and eigenvalues
        bparams.dimsparam=stimlen*obj.rank+obj.rank;
        pbase=TanSpaceBase(bparams);
        
        obj=class(obj,'QuadInpTanSpace',pbase);

        %project m onto the manifold
        sparam=proj(obj,m);
        obj=setsparam(obj,sparam);
        
        %compute point on the manifold
        theta=submanifold(obj,sparam);
        obj=setheta(obj,theta);
        
        %compute and set the basis
        obj=setbasis(obj,gradsubmanifold(obj,getsparam(obj)));
    case 3
        pbase=TanSpaceBase(params);
        
        obj.qinp=QuadNonLinInp('d',params.inplen);
        obj.rank=params.rank;
        obj=class(obj,'QuadInpTanSpace',pbase);

        %project theta onto the manifold and reset sparam, and theta
        sparam=proj(obj,gettheta(obj));
        theta=submanifold(obj,sparam);
        
        obj=settheta(obj,theta);
        obj=setsparam(obj,sparam);
        
        
        %compute and set the basis
        obj=setbasis(obj,gradsubmanifold(obj,getsparam(obj)));
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************






    