%function [evec,eigd]=vectouv(obj,sp)
%   sp - is a vector representing the factorization of the matrix
%       as 
%        Q=evec*diag(eigd)*evec
%
%   We unpac from this vector eigd and evec           
function [evec,eigd]=unpackvec(obj,sp)

stimlen=getinputlen(obj.qinp);
rank=getrank(obj);
evec=reshape(sp(1:(stimlen*rank)),[stimlen,rank]);

eigd=reshape(sp((stimlen*rank+1):end),[rank,1]);

