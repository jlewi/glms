%function theta=submanifold(obj,sp)
%   obj - descendent of TanSpaceObj
%   sp  - mxn array
%         -each column is a point at which to evaluate the manifold
%         point at which to evaluate the manifold
%         This is the vector describing u,v as a vector
%         use uvtovec to form this vector from u,v
%
%Explanation: Evaluates the submanifold for this point.
function theta=submanifold(obj,sp)

% 
%stimlen is the length of the stimulus/input which is pushed through
%the quadratic input nonlinearity
stimlen=getinputlen(obj.qinp);
theta=zeros(getdimtheta(obj),size(sp,2));

for cind=1:size(sp,2)
%convert sp back to uv
[evec,eigd]=unpackvec(obj,sp(:,cind));

m=evec*diag(eigd)*evec';

theta(:,cind)=projqmat(obj.qinp,m);
end

