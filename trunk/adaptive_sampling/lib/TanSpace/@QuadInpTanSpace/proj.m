%function ptheta=proj(obj,theta)
%   theta- value of theta
%
%Return value:
%   ptheta - projection of theta onto the submanifold of rank r matrices
%          - this is a vector, use vectouv to convert vector to matrices
%          u,v
function ptheta=proj(obj,theta)


%reshape theta as a matrix
m=qmatquad(obj.qinp, theta);

[u,s,v]=svd(m);

evec=u(:,1:obj.rank);
eigd=diag(s(1:obj.rank,1:obj.rank));

ptheta=packasvec(obj,evec,eigd);