%function grad=gradsubmanifold(obj,u,v)
%   obj - child of TanSpaceBase
%   sparam - parameters of the submanifold on which to evaluate the
%   gradient
%Return value:
%   grad = dim(theta) x nb
%       gradient of theta with respect to the submanifold
%       nb is the number of vectors needed to form an orthonormal basis
%Explantion: Computes the jacobian of theta w.r.t to the parameters of the
%submanifold (i.e of the gabor function).
%
function grad=gradsubmanifold(obj,gp)

%dimension of the matrix
mdim=getinputlen(obj.qinp);

%rank of the matrix
rank=obj.rank;

%form the matrices of the derivative
dmat=zeros(mdim,mdim,mdim*rank+rank);

%unpack the paramaters
[evec,eigd]=unpackvec(obj,gp);


%derivatives with respect to elements of evec
dind=0;

%loop across the eigenvectors
for eind=1:rank
    %loop across elements of the eigenvector
    for j=1:mdim
        dind=dind+1;
        dmat(:,j,dind)=eigd(eind)*evec(:,eind);
        dmat(j,:,dind)= dmat(j,:,dind)+eigd(eind)*evec(:,eind)';
    end
end

%derivatives with respect to elements of eigd
dind=1*mdim*rank;

for j=1:rank
    dind=dind+1;
    dmat(:,:,dind)=evec(:,j)*evec(:,j)';
end

%we need to take upper triangular parts of the matrices
%this is handled by qinp
dvec=zeros(getdimtheta(obj),mdim*rank+rank);
for dind=1:mdim*rank+rank
    dvec(:,dind)=projqmat(obj.qinp,dmat(:,:,dind));
end

%form an orthonormal basis
grad=orth(dvec);