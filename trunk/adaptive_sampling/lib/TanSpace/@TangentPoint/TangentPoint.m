%function TangentPoint('theta','manifoldp','basis')
%   theta - the point in full theta coordinates at which we evaluate the
%     manifold
%   manifoldp - the value of the manifold parameters corresponding to theta
%   basis - basis for the tangent space at theta
%
%Explanation:
%   This class represents a point at which we evaluate the tangent space
%   In combination with the MTanSpace classes this object replaces
%   the functionality in the TanSpaceBase Objects
classdef (ConstructOnLoad=true) TangentPoint 
   %   theta - the point in full theta coordinates at which we evaluate the
%     manifold
%   manifoldp - the value of the manifold parameters corresponding to theta
%   basis - basis for the tangent space at theta
%
    properties(SetAccess=private,GetAccess=public)
        version=080730;
        theta=[];
       manifoldp=[];
       basis=[];        
    end
    
    methods
        function tp=TangentPoint(varargin)
            
            switch nargin
                case 0
                    %used by loadobj
                    return;
                case 3
                    tp.theta=varargin{1};
                    tp.manifoldp=varargin{2};
                    tp.basis=varargin{3};
                otherwise
                    error('insufficient number of input arguments');
            end
        
    end
    end
end