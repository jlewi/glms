%function getsplabels=splabels(obj)
%	 obj=GaborTanSpace object
% 
%Return value: 
%	 splabels=obj.splabels 
%
function splabels=getsplabels(obj)
	 splabels={'A';'\mu_x'};         
