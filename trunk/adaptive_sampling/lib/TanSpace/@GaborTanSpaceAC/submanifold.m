%function sval=submanifold(obj,gp)
%   obj - descendent of TanSpaceObj
%   gp  - 2xn matrix of gabor parameters
%       [A;center;];
%
%Explanation: Evaluates the submanifold for this point.
function sval=submanifold(obj,gp)

width=getdimtheta(obj);

ngabors=size(gp,2);

A=ones(width,1)*gp(1,:);
center=ones(width,1)*gp(2,:);


x=[((1:width)-(1+width)/2)]';
x=x*ones(1,ngabors);


%shift by cent
x=x-center;

%
npdf=exp(-x.^2/(2*obj.sigmasq));
k=npdf.*cos(x*obj.omega);

sval=A.*k;
