%function sparam=proj(obj,theta)
%   theta- value of theta
%
%Return value:
%   sparam - [A omega phi]
%          - Amplitude, angular frequency and phase of sinusoid that most
%          closely matches this theta
function sp=proj(obj,theta)


%sampling period
dt=1/(length(theta)-1);
%sampling frequency
Fs=1/dt; %in hz


N=(length(theta));
%radians per sample
dw=2 *pi/(N-1)/dt;
wmax=pi/dt;
w=-wmax:dw:wmax;


freq=w./(2*pi);

%we divide by N to get the proper magnitude
tfft=fftshift(fft(theta))/N;

%length of theta should be odd (enforced by constructor)
%so zero frequency should be in center
cind=ceil(length(freq)/2);

% if (freq(cind)~=0)
%     error('Zero frequency is not in center');
% end

%get the magnitude by taking the peak frequency.
%mind is the offset relative to the center
[A mind]=max(abs(tfft(cind:end)));
mind=mind-1;
if (length(A)>1)
    warning('Peak is not unique');
end

A=2*A;
omega=2*pi*freq(mind+cind);

%get the phase
phi=atan2(imag(tfft(cind+mind)),real(tfft(cind+mind)));
%add pi/2 b\c we use a sinewave but phase would be for a cosine wave;
phi=phi+pi/2;
spinit=[A; omega;phi];

optim=optimset('GradObj','on');
optim=optimset(optim,'FunValCheck','on');
optim=optimset(optim,'TolX',10^-14);
optim=optimset(optim,'MaxFunEvals',1000);
%optim=optimset(optim,'DerivativeCheck','on');

A=[];
b=[];
Aeq=[];
beq=[];
lb=[0;0;-inf];
ub=[inf;inf;inf];

%gp= [A; sigmasq; center; omega];

nlcon=[];
%[sp,fval,exitflag,output] =fmincon(@(gp)(msdtosub(obj,gp,theta)),spinit,A,b,Aeq,beq,lb,ub,nlcon,optim);

%Refine just the phase 
A=[];
b=[];
Aeq=[];
beq=[];
lb=[-pi];
ub=[pi];
nlcon=[];
[phiopt,fval,exitflag,output] =fmincon(@(phi)(objphase(obj,phi,spinit(1),spinit(2),theta)),spinit(3),A,b,Aeq,beq,lb,ub,nlcon,optim);

sp=spinit;
sp(3)=phiopt;
switch exitflag
    case  {-2,-1,0}
        error('fmincon did not converge');
end

%refine the parameters by using gradient ascent to minimize the MSE
%do this because I think sampling error in the DFT will prevent parameters
%recovered from the DFT being completely accurate
% figure;
% subplot(1,2,1);
% plot(freq,abs(tfft));
% xlabel('Frequency');
% ylabel('Amplitude');
% subplot(1,2,2);
% plot(freq,angle(tfft));
% xlabel('Frequency');
% ylabel('Phase');

%objective function for varying the phase to minimize the msd of the
%estimated filter
function [msd,dphase]=objphase(obj,phi,A,omega,theta)
    [msd,jacob]=msdtosub(obj,[A;omega;phi],theta);

    dphase=jacob(:,3);
    