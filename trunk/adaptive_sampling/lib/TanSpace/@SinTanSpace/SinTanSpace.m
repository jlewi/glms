%function sim=SinTanSpace('theta')
%function sim=SinTanSpace('sparam','dimtheta');
function obj=SinTanSpace(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'sparam','dimtheta'};
con(1).cfun=1;

con(2).rparams={'theta'};
con(2).cfun=2;


%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%  bname  - name of base class. This allows us to refer to it templates
%declare the structure
obj=struct('version',071126);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'SinTanSpace',TanSpaceBase());
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
   case 1
        bparams.sparam=params.sparam;
        bparams.dimtheta=params.dimtheta;
        
        pbase=TanSpaceBase(bparams);
        
        
        obj=class(obj,'SinTanSpace',pbase);
        
        %compute theta corresponding to this sparam
        theta=submanifold(obj,getsparam(obj));
        obj=settheta(obj,theta);
        
        %compute the basis
        basis=gradsubmanifold(obj,getsparam(obj));
        
        obj=setbasis(obj,orth(basis));
        
        
    case 2
        bparams.theta=params.theta;
        bparams.dimsparam=3;
        
        
        pbase=TanSpaceBase(bparams);
        obj=class(obj,'SinTanSpace',pbase);
        
        %project theta onto the manifold
        sparam=proj(obj,gettheta(obj));
        %compute theta corresponding to sparam
        theta=submanifold(obj,sparam);
        
        obj=setsparam(obj,sparam);
        obj=settheta(obj,theta);
        
        basis=gradsubmanifold(obj,getsparam(obj));
        obj=setbasis(obj,orth(basis));
    otherwise
        error('Constructor not implemented')
end


%if (mod(getdimtheta(obj),2)==0)
%    error('Length of theta should be odd to avoid ambiguity with fft');
%end

    