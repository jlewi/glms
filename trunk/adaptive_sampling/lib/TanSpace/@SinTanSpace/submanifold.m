%function theta=submanifold(obj,s)
%   obj - descendent of TanSpaceObj
%   sp   - parameters of the sinusoid
%          -3xn matrix
%          -n is the number of different sinusoids
%         [A; omega; phi]
%
%Explanation: Evaluates the submanifold for this point.
function theta=submanifold(obj,sp)

%we assume that theta is also one second in length
%only the sampling frequency changes.
t=linspace(0,1,getdimtheta(obj))';

%make it so s
rad=t*sp(2,:);
A=ones(length(t),1)*sp(1,:);
phi=ones(length(t),1)*sp(3,:);
theta=A.*sin(rad+phi);