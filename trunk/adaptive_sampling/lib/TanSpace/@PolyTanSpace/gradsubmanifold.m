%function grad=gradsubmanifold(obj,sparam)
%   obj - child of TanSpaceBase 
%   sparam - parameters of the submanifold on which to evaluate the
%   gradient
%
%Return value:
%   grad = dim(theta) x dimsparam
%       gradient of theta with respect to the submanifold
%
%Explantion: Computes the jacobian of theta w.r.t to the parameters of the
%submanifold (i.e of the gabor function).
%   
function grad=gradsubmanifold(obj,gp)



x=[1:getdimtheta(obj)]'*ones(1,getdegree(obj));

%exponents to raise x to 
powers=ones(getdimtheta(obj),1)*[0:(getdimsparam(obj)-1)];
grad=x.^powers;

