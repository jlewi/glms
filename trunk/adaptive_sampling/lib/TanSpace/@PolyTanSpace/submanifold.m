%function submanifold(obj,coeff)
%   obj - descendent of TanSpaceObj
%   coeff   - coefficients of the polynomial
%
%Explanation: Evaluates the submanifold for this point.
%
function sval=submanifold(obj,coeff)

x=[1:getdimtheta(obj)]'*ones(1,getdimsparam(obj));

%exponents to raise x to 
powers=ones(getdimtheta(obj),1)*[0:(getdimsparam(obj)-1)];
x=x.^powers;

sval=x*coeff;
