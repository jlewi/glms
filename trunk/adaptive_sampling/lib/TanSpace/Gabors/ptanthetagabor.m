%function ptanthetagabor(x,theta, pparam,omega,sigma)
%   tind - index of the element in the gabor we want to to comput
%
%    pparam - is a 2d gaussian distribution on the parameters of the gabor
%           which are the amplitude and center of the gabor
%           [A;C]
%
%
%Return value
%   p=p(theta(x)=theta);
%Explanation:
%   theta(x)=A cos((x-c)*omega)*exp(-.5*(sigma^-2)*(x-c))^2;
%
%   Given omega, sigma, and Gaussian distributions on C and A
%   This function computes the probability
%   p(theta(x)=theta);
%   Note this is the marginal distribution; i.e we marginalize over the
%   other components of theta. 
function p=ptanthetagabor(x,theta, pparam,omega,sigmasq)

%we compute the probability by numerically integrating. 
%we need to compute the minimum and maximum value of  c.
tol=10^-3;
mu=getm(pparam);
covar=getc(pparam);
cinv=inv(covar);
ctail=norminv(tol,0,covar(2,2));
cmin=mu(2)+ctail;
cmax=mu(2)-ctail;

p = quad(@(c)(prob(c,x,theta,mu,cinv,omega,sigmasq)),cmin,cmax);

%we need to multiple p by the normalizing constant of the gaussian.
p=(2*pi)^-1*det(covar)^(-1/2)*p;

 %this is the funciton which evaluates the probability as a funciton of c
 %  mu - mean of the Gaussian on [A;c];
 %  cinv - inverse of the covariance matrix on c
 %          we pass it in so its not recomputed on each call to prob
function p=prob(c,x,theta,mu,cinv,omega,sigmasq)
    %for this value of c we need to compute a
    a=theta./(cos((x-c)*omega).*exp(-.5*1/sigmasq*(x-c).^2));
    
    %we need to evaluate the probability of c,a 
    t=[a;c];
    
    %note we don't multiply by the normalizing factor of the gaussian
    %we do this at the end;
    dmu=t-mu*ones(1,length(c));
    qterm=cinv*dmu;
    qterm=dmu.*qterm;
    qterm=sum(qterm,1);
    p=exp(-.5*qterm);
    
    