%function p=postgaborac(a,c,stim,obsrv,glm,prior,omega,sigmasq)
%   a - value of the amplitude - can be an array
%   c - value of the center
%       can be an array
%   stim - matrix of the stimuli on each trial
%   obsrv - vector of the observations on each trial
%   glm  - GLMModel
%   prior - Gaussian prior
%   omega - frequency of the gabor
%   sigmasq - decay constant of the gabor
% Explanation: Evaluates the true posterior probability on A,C up to the
% normalization constant)
function p=postgaborac(a,c,stim,obsrv,glm,prior,omega,sigmasq)

nparam=length(c);

a=rowvector(a);
c=rowvector(c);

width=size(stim,1);

%the indexes of theta
%this is a row vector
j=[((1:width)-(1+width)/2)]';

dc=j*ones(1,length(c))-ones(width,1)*c;

%compute theta for these a, c
theta=(ones(width,1)*a).*(cos(dc*omega).*exp(dc.^2*-.5/sigmasq));

%compute glmproj for each theta and each trial
%glmproj(i,j) - theta(:,i)'stim(:,j);
glmproj=theta'*stim;

obsrv=ones(nparam,1)*obsrv;

%evaluate the log likelihood for each observation
ll=loglike(glm,obsrv,glmproj);

%evaluate the log prior for each theta
%make this a row vector
lprior=logpdf(prior, theta);

%sum the log likelihoods 
%the result should be a row vector. With each entry being proportional to
%the log posterior for that theta.
lpost=sum(ll,2)'+lprior;

p=exp(lpost);