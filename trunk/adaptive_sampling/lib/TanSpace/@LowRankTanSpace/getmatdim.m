%function getmatdim=matdim(obj)
%	 obj=LowRankTanSpace object
% 
%Return value: 
%	 matdim=obj.matdim 
%   Return the dimensions of the matrix
function matdim=getmatdim(obj)
	 matdim=obj.matdim;
