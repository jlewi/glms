%function [u,s,v]=vectouv(obj,sp)
%   sp - is a vector representing the matrices u,s,v
%       we return the matrices u,v
%   s is a vector of singular values. These are stored in first rank elements of sp
%   then come the matrices u and v
function [u,s,v]=vectouv(obj,sp)

rank=getrank(obj);
mdim=getmatdim(obj);

s=sp(1:rank);

sind=rank+1;
eind=sind+mdim(1)*rank-1;
u=reshape(sp(sind:eind),[mdim(1),rank]);

sind=eind+1;
v=reshape(sp(sind:end),[mdim(2),rank]);

