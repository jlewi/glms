%function m=thetatomat(obj,theta)
%   theta - vector representation of the matrix
%
%Return value
%   m - matrix representation
function m=thetatomat(obj,theta)

matdim=getmatdim(obj);
m=reshape(theta,[matdim(1),matdim(2)]);