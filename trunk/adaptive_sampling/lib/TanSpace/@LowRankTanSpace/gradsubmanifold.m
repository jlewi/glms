%function grad=gradsubmanifold(obj,u,v)
%   obj - child of TanSpaceBase 
%   sparam - parameters of the submanifold on which to evaluate the
%   gradient
%           - sparam is a structure 
%           .u
%           .v
%           sparam.u x sparam.v' is the rank r matrix at which to evaluate
%           the gradient
%Return value:
%   grad = dim(theta) x nb
%       gradient of theta with respect to the submanifold
%       nb is the number of vectors needed to form an orthonormal basis
%Explantion: Computes the jacobian of theta w.r.t to the parameters of the
%submanifold (i.e of the gabor function).
%   
function grad=gradsubmanifold(obj,gp)

%dimension of the matrix
mdim=getmatdim(obj);

%rank of the matrix
rank=obj.rank;
%form the matrices of the derivative
%number of partial derivatives
npartial=getdimsparam(obj);
dmat=zeros(mdim(1),mdim(2),npartial);

[u,s,v]=vectouv(obj,gp);
%derivatives with respect to elements of u
dind=0;
for i=1:mdim(1)
    for j=1:rank       
        dind=dind+1;
        dmat(i,:,dind)=s(j)*v(:,j)';
    end
end

%derivatives with respect to elements of v
dind=1*mdim(2)*rank;
for i=1:mdim
    for j=1:rank       
        dind=dind+1;
        dmat(:,i,dind)=s(j)*u(:,j);
    end
end

%derivatives with respect to elements of s
for j=1:rank
   dind=dind+1;
   dmat(:,:,dind)=u(:,j)*v(:,j)';
end

%form vectors by reshaping
dimtheta=getdimtheta(obj);
dmat=reshape(dmat,[dimtheta,npartial]);

%form an orthonormal basis
grad=orth(dmat);