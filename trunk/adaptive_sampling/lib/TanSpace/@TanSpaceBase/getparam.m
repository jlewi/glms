%function p=getparam(obj)
%   obj
%
%Return  Value:
%   p - structure containing parameters specific to the derived class
%
function p=getparam(obj)
    p=[];
    warning('getparam not overloaded by derived class');