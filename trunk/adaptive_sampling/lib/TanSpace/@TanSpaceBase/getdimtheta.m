%function getdimtheta=dimtheta(obj)
%	 obj=TanSpaceBase object
% 
%Return value: 
%	 dimtheta=obj.dimtheta 
%
function dimtheta=getdimtheta(obj)
	dimtheta=obj.dimtheta;