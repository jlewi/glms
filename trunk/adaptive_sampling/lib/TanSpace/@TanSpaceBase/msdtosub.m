%[mse, grad]=msetosub(theta)
%
%Explantion: compute the MSE between theta and the gabor function at gp.
%   We also compute the jacobian
function [msd,jacob]=msdtosub(obj,gp,theta)

    test=submanifold(obj,gp);
    grad=gradsubmanifold(obj,gp);
    
    dp=(theta-test)'*(theta-test);
    msd=dp^.5;
    jacob=1/(msd)*(theta-test)'*-1*grad;
    
    if (msd==0)
        %if msd =0 theta is a perfect fit then set jacobian to 0
        jacob=zeros(1,length(jacob));
    end
    if (any(isnan(msd))||any(isnan(jacob)))
        error('nan');
    end