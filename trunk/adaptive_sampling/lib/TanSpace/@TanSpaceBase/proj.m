%function ptheta=proj(obj,theta)
%   theta- value of theta
%
%Return value:
%   ptheta - projection of theta onto the submanifold
function ptheta=proj(obj,theta)

error('proj must be called on a child class of TanSpaceBase');