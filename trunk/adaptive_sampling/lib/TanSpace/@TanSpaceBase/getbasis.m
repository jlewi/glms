%function getbasis=basis(obj)
%	 obj=TanSpaceBase object
% 
%Return value: 
%	 basis=obj.basis 
%
function basis=getbasis(obj)
	 basis=obj.basis;
