%function grad=gradsubmanifold(obj,sparam)
%   obj - child of TanSpaceBase 
%   sparam - parameters of the submanifold on which to evaluate the
%   gradient
%
%Return value:
%   grad = dim(theta) x dimsparam
%       gradient of theta with respect to the submanifold
%
%Explantion: Computes the jacobian of theta w.r.t to the parameters of the
%submanifold (i.e of the gabor function).
%   
function grad=gradsubmanifold(obj,gp)

error('grad submanifold needs to be overwritten by the derived class.');