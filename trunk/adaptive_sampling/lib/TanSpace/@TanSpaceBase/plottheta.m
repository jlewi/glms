%function plottheta(obj)
%   obj - TanSpace object
%
function plottheta(obj)
%make a plot of theta
figure;
plot(1:obj.dimtheta,obj.theta);
xlabel('i');
ylabel('\theta_i');