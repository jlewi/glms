%function getsplabels=splabels(obj)
%	 obj=TanSpaceBase object
% 
%Return value: 
%	 splabels=obj.splabels 
%       -labels for each parameter. Or empty matrix if there are none.
%       -labels are set by the child classes
function splabels=getsplabels(obj)
	 splabels=[];
