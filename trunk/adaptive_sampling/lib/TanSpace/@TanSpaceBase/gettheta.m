%function gettheta=theta(obj)
%	 obj=TanSpaceBase object
% 
%Return value: 
%	 theta=obj.theta 
%
function theta=gettheta(obj)
	 theta=[obj.theta];
