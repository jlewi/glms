%TanSpaceBase('theta','dimsparam')
%   theta - Construct the tangent space at the projection of theta onto the
%         submanifold
%TanSpaceBase('sparam',dimtheta)
%   sparam - pt in submanifold at which we construct the tangent space.
%
%
%
function obj=TanSpaceBase(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'theta','dimsparam'};
con(1).cfun=1;

con(2).rparams={'dimtheta','sparam'};
con(2).cfun=2;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
% theta   - point in full theta space where we compute the tangent space
%
% sparam  - the point in the coordinates of the submanifold at which we
%           compute the tangent space
% dimtheta  - dimensionality of theta
% dimsparam - dimensionality of sparam
% basis   - basis in theta space for the tangent space
%declare the structure
obj=struct('version',080605,'theta',[],'basis',[],'sparam',[],'dimtheta',[],'dimsparam',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'TanSpaceBase');
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        %just set theta
        %all additional processing happens in base class
        obj.theta=colvector(params.theta);
        obj.dimtheta=length(obj.theta);
        obj.dimsparam=params.dimsparam;
    case 2
        %just set sparam
        %all additional proecessing happens in base class
        obj.sparam=colvector(params.sparam);
        obj.dimsparam=length(obj.sparam);
        obj.dimtheta=params.dimtheta;
    
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one

obj=class(obj,'TanSpaceBase');





    