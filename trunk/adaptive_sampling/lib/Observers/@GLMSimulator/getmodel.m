%function getmodel=model(obj)
%	 obj=GLMSimulator object
% 
%Return value: 
%	 model=obj.model 
%
function model=getmodel(obj)
	 model=obj.model;
