%function theta=gettheta(sobj)
%   sobj - GLMSimulator
%
%Return value:
%   theta - true parameters

function theta=gettheta(sobj)
    theta=sobj.theta;