%function GLMSimulator('model',mobj,'theta',theta)
%   mobj  - the MParamObj describing the general linear model
%               to simulate
%   theta - the parameters for the glm
%
% Explanation - create a simulator for the glm.
% To simulate a misspecified model, just make the glm different from the
% one used to fit the data.
%
%Parent: Observerbase
%
%Revisions: 
%   01-25-2008 - removed field maxobsrv
%              - Forcing the observations to maxobsrv when maxobsrv is
%              exceeded causes the updates to fail.
%               
%   01-24-2008 - added field maxobsrv
%
%   12-30-2008 - Convert to Matlab's new object model
%
classdef (ConstructOnLoad=true) GLMSimulator< ObserverBase

    properties(SetAccess=private,GetAccess=public)

        %model - store MParamObj - describes GLM to simulate
        %theta - true parameters to use for simulation
        %deleted - structure of any deleted fields
        %        added 3-28-07
        model=[];
        theta=[];

    end
    methods
        function [glmsim]=GLMSimulator(varargin)
            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={'model','theta'};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=struct();
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            glmsim=glmsim@ObserverBase(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    for j=1:2:nargin
                        switch varargin{j}
                            case 'glm'
                                %3-19-07
                                %no longer take the GLM as input
                                error('GLMSimulator no longer takes GLM as input');
                            case 'model'
                                if ~isa(varargin{j+1},'MParamObj')
                                    error('model must be derived from MParamObj');
                                end
                                glmsim.model=varargin{j+1};
                                required.model=1;
                            case 'theta'
                                glmsim.theta=varargin{j+1};
                                if ~isvector(glmsim.theta)
                                    error('theta must be vector');
                                end
                                %make it a column vector
                                if (size(glmsim.theta,2)>1)
                                    glmsim.theta=glmsim.theta';
                                end
                                required.theta=1;
                       
                            otherwise
                                error(sprintf('%s unrecognized parameter \n',varargin{j}));
                        end
                    end

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %set the version to be a structure
            glmsim.version.GLMSimulator=090125;

        end
    end
end
