%function r=observe(glmsim,simobj,trial)
%   glmsim - glm simulator
%   simobj 
%   trial 
%
%Return value:
%   r- the response\label for this stim
%
%Explanation: Simulates the active learner by obtaining a label/response
%for the specified input.
%
%History
%   01-11-2009 - changed calling syntax to 
%       glmsim,simobj,trial
%   10-19-2007 - add support for spike history dependence
function r=observe(glmsim,simobj,trial)


    input=getinputs(simobj,trial);
    
%r=glmsim.glm.sampdist(glmsim.glm.fglmmu(glmsim.theta'*stim));
glm=getglm(glmsim.model);
r=sampdist(glm,compexpr(glmsim.model,input,glmsim.theta));

