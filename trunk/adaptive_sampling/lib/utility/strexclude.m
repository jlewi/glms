%function strexclude(varargin)
%   varargin- list of variables which shouldn't be cleared
%
% Explanation: clears all variables in the base workspace except those
% listed
function clearexcept(varargin)

    evalin('vtoclear=whos;','caller');
    evalin('for j=1:length(vtoclear); if isempty(vtoclear(j).name,
