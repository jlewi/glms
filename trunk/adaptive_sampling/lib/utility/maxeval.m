%function [mval]=maxeval(eig) 
%       eigck - eigendecomposition                
%          .esorted =+1 sorted in ascending order
%                        = -1 sorted in descending order
%          .evecs
%          .eigd
%Return:
%   mval    - maximum eigenvalue
%
% Explanation: returns the maximum eigenvalue.
% Checks the eig structure to see if its sorted. If its sorted it returns
% the eigenvalue without having to do a search.
function [mval mind]=maxeval(edec)

warning('Should reaslly use EigObj');
mind=0;
if isfield(edec,'esorted')
    if (edec.esorted==1)
        mind=length(edec.eigd);
    elseif (edec.esorted==-1)
        mind=1;
    else
        error('value of esorted should be 1 or -1');
    end
    [mval]= edec.eigd(mind);
else
    [mval mind]=max(edec.eigd);
end