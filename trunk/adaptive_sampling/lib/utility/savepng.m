%function 
%       facc- handle to the graph
%
%save a png of the current graphics
%
%$Revision$: Adjust paperpposition based on position so that font sizes are
%   look good.
%
function fname=savepng(fh,fname)

if isstruct(fh)
    if isfield(fh,'hf')
        fs=fh;
        fh=fh.hf;
    end
end
%if ~isa(fh,fig')

if ~exist('fname','var')
if isfield(fh,'name')
    fname=sprintf('%s.png',get(fh,'name'));
else
    fname=[];
end
end


if isempty(fname)
    fname=sprintf('%s_fig.png',datestr(clock,'yymmdd'));
end
%fname=fullfile('/home/jlewi/svn_trunk/allresults/figs', sprintf('%2.2d_%2.2d_%s.png',datetime(2),datetime(3),filename));
%fname=seqfname(fname);
%end
fname=fullfile('/home/jlewi/svn_trunk/allresults/figs',fname);
fname=seqfname(fname);
%set paper position to be same as position
un=get(fh,'units');
set(fh,'units','inches');
pos=get(fh,'position');
paperpos(1:2)=[0 0];
paperpos(3:4)=pos(3:4);
set(fh,'paperposition',paperpos);
saveas(fh,fname);
set(fh,'units',un);
fprintf('saved to %s \n',fname);