%funciton x=normmag(x)
%       x - column vector or matrix of column vectors
%
%Explanation: Normalizes each column so that magnitude of vector is 1
function x=normmag(x)

     
     mmag=sum(x.^2,1);
     mmag=1./(mmag.^.5);
x=(ones(size(x,1),1)*mmag).*x;