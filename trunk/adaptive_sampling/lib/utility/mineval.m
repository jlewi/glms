%function [mval]=mineval(eig) 
%       eigck - eigendecomposition                
%          .esorted =+1 sorted in ascending order
%                        = -1 sorted in descending order
%          .evecs
%          .eigd
%Return:
%   mval    - maximum eigenvalue
%   mind    - index of minimum eigenvalue
% Explanation: returns the minimum eigenvalue.
% Checks the eig structure to see if its sorted. If its sorted it returns
% the eigenvalue without having to do a search.
function [mval mind]=mineval(edec)
mind=0;
if isfield(edec,'esorted')
    if (edec.esorted==1)
        mind=1;
    elseif (edec.esorted==-1)
        mind=length(edec.eigd);
    else
        error('value of esorted should be 1 or -1');
    end
    [mval]= edec.eigd(mind);
else
    [mval mind]=min(edec.eigd);
end