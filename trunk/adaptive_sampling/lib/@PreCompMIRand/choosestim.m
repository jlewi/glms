%function stim=choosestim(finf,post,shist,einfo,mparam,glm)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
% Return value
%       stim - the stimulus for the next trial
%       opreturn    - optional return information about the procedure
%               .ocase - a number giving information on how the stimulus was
%               selected.
% Explanation: Chooses the stimulus by doing a 2-d optimization over the
% precomputed region.
function [optstim, oreturn,pobj, varargout]=choosestim(pobj,post,shist,einfo,mparam,varargin)

    oreturn=struct('ocase',0,'dotmuopt',[],'sigmaopt',[],'dotmulim',[]);
  
    glm=mparam.glm;
    %process the optional arguments to get the trial number
    for j=1:2:length(varargin)
        switch varargin{j}
            case 'trial'
                trial=varargin{j+1};
        end
    end
    
    if (trial>pobj.nrand)
        [optstim oreturn]=choosestim(pobj.PreCompMIObj,post,shist,einfo,mparam);
    else
        %random stimulus
          pbase=pobj.PreCompMIObj;     
          ocases=pbase.ocases;
          oreturn.ocase=ocases.randstim;

           obj=RandStimBall('mmag',mparam.mmag, 'klength',mparam.klength);
           optstim=choosestim(obj);
    end
    
    %the following allows other choosestim functions to return more
    %arguments
    if (nargout>3)
        for j=1:(nargout-3)
            varargout{j}=[];
        end
    end