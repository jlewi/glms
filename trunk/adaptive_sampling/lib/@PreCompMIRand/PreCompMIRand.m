%function pobj=PreCompMIObj('filename',fname,'nrand',nrand)
%   -fname, file which contains the PreCompMIObj to base this object on
%   -nrand - number of initial stimuli which should be selected randomly
%
% Explanation: Child of PreCompMI
%   the initial nrand objects are chosen randomly. After that the stimuli
%   ar chosen by calling PreCompMIOBj.choosestim
function pobj=PreCompMIRand(varargin)

%nrand - number of initial stimuli which should be chosen randomly
%been chosen already);

pobj=struct('nrand',0);
pbase=[];   %will point the base object.

%blank constructor for when we load from file
if (nargin==0)
    
    %create the object and register it as subclass of StimChooserObj
    pbase=PreCompMIObj();
    pobj=class(pobj,'PreCompMIRand',pbase);
else
    
        %list of required parameters
        required.nrand=0;
        required.filename=0;

        for j=1:2:nargin
            switch varargin{j}
                case {'filename','fname'}
                    filename=varargin{j+1};
                    required.filename=1;
                case 'nrand'
                    pobj.nrand=varargin{j+1};
                    if (pobj.nrand<=0)
                        error('nrand must be >0');
                    end
                    required.nrand=1;
                otherwise
                    error(sprintf('%s unrecognized parameter \n',varargin{j}));
            end
        end

        %check if all required parameters were supplied
        freq=fieldnames(required);
        for k=1:length(freq)
            if (required.(freq{k})==0)
                error(sprintf('Required parameter %s not specified \n',freq{k}));
            end
        end

        %create the base object from the file
        pbase=PreCompMIObj('filename',filename);
        
      

        %create the object and register it as subclass of StimChooserObj
        pobj=class(pobj,'PreCompMIRand',pbase);

    
end

