%07-31-2008
%
%A script for testing various functions of PostTanSpace.
%Mostly this function looks for syntax errors

%start by defining a model object
trank=2;
glm=GLMPoisson('canon');

strfdim=[10 5];

%**************************************************************************
%no spike history
alength=0;

mobj=MTLowRank('glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',1,'rank',trank);

%********************************************************************
%Check the First Constructor
%********************************************************************
c=randn(getparamlen(mobj),getparamlen(mobj));
c=c*c';
fullpost=GaussPost('m',randn(getparamlen(mobj),1),'c',c);

ptan=PostTanSpace(fullpost,mobj);



%*****************************************************************
%check the second constructor
%*****************************************************************
ptan2=PostTanSpace(ptan.fullpost,ptan.tanpost,ptan.tanpoint);

%***************************************************************
%Check the updater
%*************************************************************
uobj=TanSpaceUpdater();

stim=rand(getparamlen(mobj),1);
obsrv=ceil(rand(1)*100);
post=update(uobj,ptan2,stim,mobj,obsrv);