%function getm=m(obj)
%	 obj=GaussPost object
%    trial - trial
%Return value: 
%	 m=obj.m 
%
%Explanation: If numlabs is >1 we automatically create a distributed
%representation of the covariance matrix
function m=getm(obj,trial)

m=getm(getpost(obj,trial));

    

