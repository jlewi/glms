%function gettanspace=tanspace(obj)
%	 obj=PostTanSpace object
% 
%Return value: 
%	 tanspace=obj.tanspace 
%
function tanspace=gettanspace(obj)
     error('Obsolete since version 07-30-2008');
	 tanspace=[obj.tanspace];
