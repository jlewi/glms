%function getc=c(obj)
%	 obj=PostTanSpace object
% 
%Return value: 
%	 c=obj.c 
%
%   Return the covariance of the full posterior 
function c=getc(obj)
	 c=getc(obj.fullpost);
