%function obj=lowmem(obj)
%   obj - PostObjBase
%
%Explanation: Creates a low memory representation of the posterior. Zeros
%out the covariance of the full posterior and the posterior on the
%subspace.
%   Also set the  tanpoint to empty because we don't want to store the
%   basis matrix. We can always reconstruct the tanpoint by projecting 
%   the MAP onto the mean.
function obj=lowmem(obj)

obj.fullpost=setc(obj.fullpost,[]);
obj.tanpost=setc(obj.tanpost,[]);
