%function getm=m(obj)
%	 obj=PostTanSpace object
% 
%Return value: 
%	 m=obj.m 
%       -mean of the full posterior
%
%Explanation: We return the mean of the full posterior because we want to
%be able to use this object with our existing updaters to update the full
%posterior without any mod.
function m=getm(obj)
	 m=getm([obj.fullpost]);
