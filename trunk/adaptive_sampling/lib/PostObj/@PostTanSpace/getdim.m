%function getdim=dim(obj)
%	 obj=PostTanSpace object
% 
%Return value: 
%	 dim=obj.dim 
%     get dimensionality of the full posterior
function dim=getdim(obj)
	 dim=getdim(obj.fullpost);
