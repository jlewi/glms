%function parray=addpost(parray,npost)
%   parray - an array of PostObjects
%   npost  - a posterior object to add to the array
%
% Explanation: Expands the array of PostObjects by npost
%   This was added because I need it for GaussPostFile
function parray=addpost(parray,npost)
    error('addpost needs to be overwritten in the derived class.');