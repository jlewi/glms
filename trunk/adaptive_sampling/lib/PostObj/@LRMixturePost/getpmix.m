%function getpmix=pmix(obj)
%	 obj=LRMixturePost object
% 
%Return value: 
%	 pmix=obj.pmix 
%
function pmix=getpmix(obj,ind)


    if exist('ind','var')
        pmix=obj.pmix(ind);
    else
        pmix=obj.pmix;
    end