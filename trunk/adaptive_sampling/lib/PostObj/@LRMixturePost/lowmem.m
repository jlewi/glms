%function lowmem(pobj)
%
%Explanation: COnvert this Posterior object into a low memory version.
%   i.e zero out the covariance matrix.
function pobj=lowmem(pobj)

%zero out covariance of full posterior
pobj.fpost=setc(pobj.fpost,[]);