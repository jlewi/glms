%function getm=m(obj)
%	 obj=LRMixturePost object
% 
%Return value: 
%	 m=obj.m 
%
%Explanation get the mean of the full posterior.
function m=getm(obj)
	 m=getm(obj.fpost);
