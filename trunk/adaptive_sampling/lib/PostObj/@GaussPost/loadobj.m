%function obj=loadobj(lobj)
%
%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
%
%Revision:
%   10-01-2008
%       Use New template for laodobj
function obj=loadobj(lobj)

%indicates whether we have already performed a copy
%copying the fields of lobj to obj when lobj is a structure
%because we only want to do the copying once
cstruct=false;

%get the latest version
%save the latest version so we can check to make sure all
%conversions have been completed.
newobj=GaussPost();
latestver=newobj.version;


if isstruct(lobj)
    obj=newobj;
 
    obj.version=lobj.version;
else
    %we don't need to create a new object
    obj=lobj;
end


%******************************************************************
%Sequentially convert one version of the object to the next
%until we get to the latest version
%
%Warning: If lobj is a structure then we haven't copied the data from lobj
% to obj yet.
%******************************************************************
if (obj.version<=71021)
    %version is before we made GaussPost a child of
    %PostUpdatersBase
    %Its also an MCOS object
    obj=GaussPost('gpstruct',lobj);
    obj.version=071021;
end

if (obj.version < 080730)
    %do conversion
    %copy fields from old version to new version
    %if lobj is a structure
    obj.version=080730;
    if (isstruct(lobj) && ~cstruct)

        obj=repmat(obj,size(lobj));
        
        nel=numel(obj);
        for j=1:nel
             if (mod(nel,10)==0)
                 fprintf('Converting element %d of %d in GaussPost array \n',j,nel);
             end
            fskip={'PostObjBase','bname','version'};    %fields not to copy
            obj(j)=copyfields(lobj(j),obj(j),fskip);
        end
        cstruct=true;
    end
end

%check if converted all the way to latest version
if (obj(1).version <latestver)
    error('Object has not been fully converted to latest version. \n');
end

%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
%
%Copy fields must be declared within loadobj because otherwise it won't
%have permssion to set the private fields of the object.
function dest=copyfields(source,dest,skip)
%we just need to copy the fields
%and handle any special cases if required
fnames=fieldnames(source);

%struct for object
sobj=struct(dest);
for j=1:length(fnames)
    switch fnames{j}
        case skip
            %do nothing we skip this field
        otherwise
            %set the new field this will cause an error
            %if the field isn't a member of the new object
            dest.(fnames{j})=source.(fnames{j});
    end
end

