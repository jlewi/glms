%function obj=setc(obj,val)
%	 obj=GaussPost object
%    val - covariance matrix
%        - can either be a matrix or 
%Return value: 
%	 obj= the modified object 
%
function obj=setc(obj,val)

if isempty(val)
    %zero out the covariance matrix
    obj.c=[];
    obj.efull=[];
    obj.esub=[];
else

if isa(val,'EigObj')
    obj.efull=val;
else
	 obj.c=val;
end
end
