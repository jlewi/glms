%function getc=c(obj)
%	 obj=GaussPost object
% 
%Return value: 
%	 c=obj.c 
%
function c=getc(obj)

if ~(isempty(obj.efull))
    %compute c from the eigendecomposition
%    eigd=geteigd(obj.efull);
 %   evecs=getevecs(obj.efull);
  %  c=evecs*diag(eigd)*evecs';
    c=getmatrix(obj.efull);
else
    c=obj.c;
end

