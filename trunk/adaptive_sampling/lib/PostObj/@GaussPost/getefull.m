%function getefull=efull(obj)
%	 obj=GaussPost object
% 
%Return value: 
%	 efull=obj.efull 
%
function efull=getefull(obj)

efull=[];
%if eigendecomposition doesn't exist, create it
if isempty(obj.efull)
    if ~isempty(obj.c)
    warning('Eigendecomposition for posterior is not stored, it is beign computed.');
    efull=EigObj('matrix',obj.c);
    end
else
	 efull=obj.efull;
end
