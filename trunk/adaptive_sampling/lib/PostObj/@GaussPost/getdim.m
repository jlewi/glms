%function getdim=dim(obj)
%	 obj=GaussPost object
% 
%Return value: 
%	 dim=obj.dim 
%
%Explanation: Return the dimensionality
function dim=getdim(obj)
	 dim=size(obj.m,1);
