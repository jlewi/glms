%function q=compquad(post,v)
%   post - posterior
%   v    - vector
%
% Return value:
%   q= v'*post.c*v;
%
%
function q=compquad(post,v)

    q=v'*rotatebyc(post,v);
