%function getslen=slen(obj)
%	 obj=GaussPost object
% 
%Return value: 
%	 slen=obj.slen 
%
function slen=getslen(obj)
	 slen=obj.slen;
