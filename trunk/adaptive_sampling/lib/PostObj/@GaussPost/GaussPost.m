%function sim=GaussPost(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
%GaussPost(m,c)
%
%GaussPost('gpstruct',gpstruct)
%   gpstruct - array of structures with all the fields of
%               a GaussObject
%            I added this constructor for converting old GaussObjects to
%            the new version which is a subclass of PostUpdatersBase
% Explanation: Object to rerpresent a Gaussian posterior
%
% Revision
%   07-30-2008
%           Switch to Matlab's new object model
%           Deleted fields
%               bname
%           Add constructor which doesn't involve specifing the
%               inputs as fieldname value pairs
%   071120 - make it child of POstObjBase
classdef (ConstructOnLoad=true) GaussPost < PostObjBase
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % m       - the mean
    % c       - covariance matrix
    %           this is an internal variable
    %           b\c covariance matrix is not actually stored if we know efull
    %
    % efull   -eigendecomposition of the full matrix
    % esub    -eigenecomposition of .c(1:slen,1:slen)
    % slen    -see above
    %         -0 means we do not keep track of the eigdecomp of the submatrix.
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private,GetAccess=public)
        version=080730;
        entropy=[];
        m=[];
        c=[];
        efull=[];
        esub=[];
        slen=0;
        uinfo=[];

    end

    methods
        function obj=GaussPost(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'m','c'};
            con(1).cfun=1;

            con(2).rparams={'gpstruct'};
            con(2).cfun=2;


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    return
                case 2
                    %we just specify m and c
                    cind=3;
                otherwise

                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);

            end


            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    obj.m=params.m;
                    if isa(params.c,'EigObj')
                        obj.efull=params.c;
                    else
                        obj.c=params.c;
                    end
                    params=rmfield(params,'c');
                    params=rmfield(params,'m');
                case 2
                    obj=params.gpstruct;
                    params=rmfield(params,'gpstruct');
                case 3
                    obj.m=varargin{1};
                    if isa(varargin{2},'EigObj')
                        obj.efull=varargin{2};
                    else
                        obj.c=varargin{2};
                    end
                otherwise
                    error('Constructor not implemented')
            end

            if exist('params','var')
                %optional fields
                fnames=fieldnames(params);
                for f=1:length(fnames)
                    switch fnames{f}
                        case 'entropy'
                            obj.entropy=params.entropy;
                        otherwise
                            error('Unrecognized option %s',fnames{f});
                    end
                end
            end

            %*********************************************************
            %Create the object
            %****************************************
            %instantiate a base class if there is one


        end
    end
end



