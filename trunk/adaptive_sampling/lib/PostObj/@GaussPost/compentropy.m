%function entropy=compentropy(post)
% 
% Return value:
%   entropy of the Gaussian posterior in bits or else nan if the covariance
%   matrix is []
function entropy=compentropy(post)

entropy=nan;

c=getc(post);

if ~isempty(c)
    %compute the entropy in NATs
    %using log(detc(c)) was blowing up to negative infinity b\c det(c)=0
   entropy=.5*getdim(post)*(1+ log(2*pi))+.5*sum(log(eig(c)));
   
   %convert to bits
   entropy=entropy/log(2);
    
end