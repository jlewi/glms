%function obj=setentropy(obj,val)
%	 obj=GaussPost object
% 
%Return value: 
%	 obj= the modified object 
%
function obj=setentropy(obj,val)
	 obj.entropy=val;
