%function getentropy=entropy(obj,trial)
%	 obj=GaussPost object
% 
%Return value: 
%	 entropy=obj.entropy 
%
function entropy=getentropy(obj,trial)
    if ~exist('trial','var')
        trial=[];
    end
    if isempty(trial)
        entropy=[obj.entropy];
    else
	 entropy=obj.entropy(trial+1);
    end
