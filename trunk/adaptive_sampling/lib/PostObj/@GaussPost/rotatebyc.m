%function p=rotatebyc(post,v)
%       post - GaussPost object
%       v    - vector or matrix
%
% Result:
%   p=efull.evecs*efull.eigd*(efull.evecs'*v);
%   
% Explanation: This function computes the product of the covarance matrix and a
% vector
%   It does this efficiently using the eigenvectors of  matrix if
%   they are represented or using the covariance matrix if thats whats
%   stored.
%
% Revision
%   08-11-2008 - V can be a matrix
function p=rotatebyc(post,v)

if ~isempty(post.efull)
evecs=getevecs(post.efull);
eigd=geteigd(post.efull);
p=evecs*((eigd*ones(1,size(v,2))).*(evecs'*v));
else
    p=post.c'*v;
end

