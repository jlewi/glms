%function saveobj(obj)
%   obj - object to save
%
%Explanation: This function gets called before the object is saved to a mat
%file. We use this function to prepare the object.
%
%Revision 080825
%   no longer make a copy of the object because inmem is now declared as a
%   transient property
%
%   do not take trial as argument
function obj=saveobj(obj)

