%function parray=addpost(parray,npost,id)
%   parray - an array of GaussPostFile objects
%   npost  - a GaussPost object to add to the array
%   trial     - id to associate with this trial
% Explanation: Expands the array of PostObjects by npost
%   This was added because I need it for GaussPostFile
function parray=addpost(parray,npost,trial)

    if isa(npost,'GaussPost')
   
        %set the mean and the covariance
        parray=setm(parray,getm(npost),trial);
        
        %set the covariance
        parray=setc(parray,getc(npost),trial);
    elseif isa(npost,'PostTanSpace')
        
        %set the mean and the covariance
        parray=setm(parray,getm(npost.fullpost),trial);
        
        %set the covariance
        parray=setc(parray,getc(npost.fullpost),trial);
    else
        error('npost must be of type GaussPost');
    end