%function post=getpost(obj,trial)
%	 obj=GaussPostFile object
% 
%Return value: 
%	 post=obj.post 
%        - a GaussPost object representing the posterior on the specified
%        trial
function post=getpost(obj,trial)
	 post=GaussPost('m',getm(obj,trial),'c',getc(obj,trial));
