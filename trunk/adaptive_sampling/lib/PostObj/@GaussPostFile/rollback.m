%function rollback(simobj,trial)
%   trial - this is the first trial we want to run when iterate is called
%
%Explanation:
% rollsback the simulation to the appropriate trial
function rollback(gobj,simobj,trial)

%tpast the last trial to store
tpast=trial-1;


%delete all matrices older than tpast
mids=readmatids(gobj.minfo.maccess);
cids=readmatids(gobj.cinfo.caccess);

mind=find(mids>tpast);
if ~isempty(mind)
    deletematrix(gobj.minfo.maccess,mids(mind));
end
cind=find(cids>tpast);
if ~isempty(cind)
deletematrix(gobj.cinfo.caccess,cids(cind));
end
%   inmem=
obj.inmem=struct('m',[],'c',[],'id',[]);    

