%function obj=('mfile','cfile','m','c','id')
%   mfile - FilePath object to store mean
%   cfile - FilePath object to store covariance
%   m     - the mean of the Gaussian
%   c     - the covariance of the gaussian (can be empty matrix)
%   id    - id to associate with this m and c. Must be a positive number.
%
%   Explantion: This assumes the files don't exist and creates blank files
%   to store the data
%
%function obj=(mfile,cfile)
%   mfile - FilePath object to store mean
%   cfile - FilePath object to store covariance
%
%   Explanation: The files mfile and cfile already exist.
%       We create RAccessFile objects for each file.
%       We then create an array of GaussPostFile objects
%       one for each mean in the file.
%
%
% version 080825
%   declare inmem to be transient
%
% Version 080630
%       Use the new object model and declare the class of type handle
%           do not use pointers
%
%       As part of this conversion process
%           The following fields have been replaced
%           (inmemptr, caccessptr, maccessptr)
%            with the fields
%            (inmem,caccess, maccess)
%
%       deleted field id
% Version 080409
%       no longer create an array of GaussPostFile objects because its too
%       expensive
%
%       .id - no longer relevant

classdef (ConstructOnLoad=true) GaussPostFile < handle
    %******************************************************************
    %Default values
    %******************************************************************

    properties(SetAccess=private,GetAccess=public)
        version=080825;
        minfo=[];
        cinfo=[];
    end
        
   properties(SetAccess=private,GetAccess=public,Transient=true)
       %we declare inmem to be transient so that it is not saved.
        inmem=struct('m',[],'c',[],'id',[]);    
    end

    methods
        %obj=setdistributed(obj,val);
         
        function obj=GaussPostFile(varargin)
            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'mfile','cfile','m','c','id'};
            con(1).cfun=1;

            con(2).rparams={'mfile','cfile'};
            con(2).cfun=2;

            %**********************************************************
            %Define Members of object
            %**************************************************************
            % version - version number for the object
            %           store this as yearmonthdate
            %           using 2 digit format. This way version numbers follow numerical
            %           order
            %           -version numbers make it easier to maintain compatibility
            %               if you add and remove fields
            %
            % minfo     - structure storing info about the mean
            %    .maccess - the RAccessFile which stores the means
            %                - the value of this pointer should be accessed through
            %                 the private get/set methods
            %
            %cinfo      - structure storing info about covariance matrix
            %   caccess - pointer to the RAccessFile which stores the covariance
            %                 matrices
            %
            %id         - the id of the matrix in maccess and cacces to associate with
            %               this object
            %            06-30-2008: I think this field is obsolete
            %                since version 080409
            %                I think this goes back to when I was creating
            %                arrays of GaussPostFile objects with each
            %                element representing a different element in
            %                the array. 
            %inmem   - a structure containing (m,c,id)
            %           - when we access the mean and c we load them from the file
            %             but we save them so future accesses won't require re-reading
            %             from the file
            %           - we only store 1 (m,c,id) at a time for each array of
            %           GaussPostFile.
            %declare the structure


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    return
                case 1
                    %*********************************************
                    %make a copy of an existing GaussPostFile object
                    %***********************************************                  
                    if isa(varargin{1},'GaussPostFile')
                    eobj=varargin{1};
                    obj=GaussPostFile('mfile',getfname(eobj.minfo.maccess),'cfile',getfname(eobj.cinfo.caccess));
                    return;
                    else
                        error('Expected argument is of type GaussPostFile');
                    end
                
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            if (length(cind)==2)
                cind=1;
            end

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    if exist(params.mfile)
                        error('mfile points to a file which already exists.');
                    end
                    if exist(params.cfile)
                        error('cfile points to a file which already exists.');
                    end

                    mdim=size(params.m,1);
                    %create the files and pointers
                    %file for the mean
                    robj=RAccessFile('fname',params.mfile,'dim',[mdim, 1],'distmatrix',true);

                    obj.minfo.maccess=robj;

                    %for the covariance
                    robj=RAccessFile('fname',params.cfile,'dim',[mdim, mdim],'distmatrix',true);
                    obj.cinfo.caccess=robj;



                    %set the mean and covariance
                    obj=setm(obj,params.m,params.id);
                    obj=setc(obj,params.c,params.id);

                case 2
                    if ~exist(params.mfile)
                        error('mfile points to a file which does not exist.');
                    end
                    if ~exist(params.cfile)
                        error('cfile points to a file which does not exist.');
                    end

                    %create the files
                    %file for the mean
                    robj=RAccessFile('fname',params.mfile,'distmatrix',true);
                    obj.minfo.maccess=robj;

                    %for the covariance
                    robj=RAccessFile('fname',params.cfile,'distmatrix',true);
                    obj.cinfo.caccess=robj;



                otherwise
                    error('Constructor not implemented')
            end

        end
    end
    
       methods (Static = true)
        obj=loadobj(a);

    end
end





