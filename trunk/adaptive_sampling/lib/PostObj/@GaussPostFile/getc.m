%function getc=c(obj,trial)
%	 obj=GaussPost object
%    trial = trial on which to get the 
%Return value: 
%	 c=obj.c 
%
%Explanation: If numlabs is >1 we automatically create a distributed
%representation of the covariance matrix
function c=getc(obj,trial)

%check if c is stored in the file
%nan - means we haven't loaded c into memory
%c could also be empty
if (trial==obj.inmem.id)
    %inmem is pointing to this object
    if (numel(obj.inmem.c)>1)
        c=obj.inmem.c;
        return;
    end
    
    if isempty(obj.inmem.c)
        c=[];
        return;
    end
    
  
end

%c wasn't stored in inmemptr therefore we need to read it
caccess=getcaccess(obj);
   
%get the covariance marix    
%if we are running in paralle we create a distributed representation of
% c. This is handled by the RAccessFile object

%check if c is stored in the file
recnum=hasid(caccess,trial);

if ~isnan(recnum)
c=readmatrix(caccess,trial);
   
else
    c=[];
end

%set the inmemptr
obj=setinmemptr(obj,trial,nan,c);
    

