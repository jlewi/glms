%function getmaccess=maccess(obj)
%	 obj=GaussPostFile object
% 
%Return value: 
%	 maccess=obj.maccess 
%
function maccess=getmaccess(obj)
	 maccess=obj.minfo.maccess;
