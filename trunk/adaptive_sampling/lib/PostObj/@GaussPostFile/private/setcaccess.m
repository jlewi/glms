%function obj=setcaccess(obj,val)
%	 obj=GaussPostFile object
% 
%Return value: 
%	 obj= Sets the pointer for the RAccessFile object for the covariance
%	 matrix
%
function obj=setcaccess(obj,val)
	 obj.cinfo.caccess=val;
