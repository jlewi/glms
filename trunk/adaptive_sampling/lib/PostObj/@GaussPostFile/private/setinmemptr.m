%function obj=setinmemptr(obj,id,m,c)
%	 obj- GaussPostFile object
%     id - value of id
%      m - value of the mean or nan to indicate we are not setting/changing
%         this value
%     c  - value of the covariance matrix or nan to indicate we are not
%         setting  changing this value
%Return value: 
%	 obj= the modified object 
%
function obj=setinmemptr(obj,id,m,c)

%check if id matches the object already stored
%if so then we only update the appropriate fields
if (obj.inmem.id==id)
    if ~(prod(size(m))==1 && isnan(m))
        obj.inmem.m=m;
    end
    if ~(prod(size(c))==1 && isnan(c))
        obj.inmem.c=c;
    end
else
    obj.inmem.id=id;
    obj.inmem.m=m;
    obj.inmem.c=c;
end

