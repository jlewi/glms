%function getm=m(obj)
%	 obj=GaussPost object
%    trial - trial
%Return value:
%	 m=obj.m
%
%Explanation: If numlabs is >1 we automatically create a distributed
%representation of the covariance matrix
%
%Revisions:
%   01-11-2009 - if trial is blank get all means
function m=getm(obj,trial)

if ~exist('trial','var')
    m=readdata(getmaccess(obj));
    m=cell2mat(m);
else
    %check if c is stored in the file
    if (trial==obj.inmem.id)
        %inmem is pointing to this object

        if (numel(obj.inmem.m)>1)
            m=obj.inmem.m;
            return;
        end

        if isempty(obj.inmem.m)
            m=[];
            return
        end

    end

    %c wasn't stored in inmemptr therefore we need to read it
    maccess=getmaccess(obj);

    %get the covariance marix
    %if we are running in paralle we create a distributed representation of
    % c. This is handled by the RAccessFile object

    %check if c is stored in the file
    recnum=hasid(maccess,trial);

    if ~isnan(recnum)
        m=readmatrix(maccess,trial);

    else
        m=[];
    end

    %set the inmemptr
    obj=setinmemptr(obj,trial,m,nan);

end

