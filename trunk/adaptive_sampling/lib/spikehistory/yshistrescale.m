%function yhistrescale(w,estimmean,eigd,signs,signl2,mmag)
%           w- parameter between [0 1] which is variable we search over
%           estimmean - mean of stimulus terms projected into eigenspace
%           z - z= eigvec' Cka r
%                   r - spike history 
%                   Cka - correlation matrix between stimulus coefficents
%                   and spike history coefficent
%                   eigvec - eigenvectors of Ck=covariance matrix of just
%                   stimulus coefficents
%           eigd    - eigen values of the covariance matrix of just the
%           stimulus (not spike history) terms
%           iexpand - which eigenvalue to take expansion around
%           signs - specifies which sign of s to take
%                   s= sign(signs) w/(1-w)
%           signl2 - which solution (+ or -) to take for the solution of
%               quadratic equation for lambda2
%           mmag - is the magnitude of thestimulus 
%                       i.e (y'y)^.5=mmag
% Explanation: This function computes the optimal stimulus in eigenspace
%   
function [y]=yshistrescale(w,estimmean,z,eigd,iexpand,signs,signl2,mmag)
    signl2=sign(signl2);
    s=sign(signs)*w./(1-w);
    
    ej=[estimmean(1:iexpand-1,1); estimmean(iexpand+1:end,1)];
    zj=[z(1:iexpand-1,1); z(iexpand+1:end)];
    ei=estimmean(iexpand);
    cj=[eigd(1:iexpand-1,1);  eigd(iexpand+1:end,1)];
    ci=eigd(iexpand);
    zi=z(iexpand);
    denom=1+s*(cj-ci);

    %square root term
    a=(ei^2/4+1/4*sum(ej.^2./(denom.^2),1));
    b=-ei*zi-sum(ej.*zj./denom.^2,1);
    c=zi^2 + sum(zj.^2./denom.^2,1)-mmag^2/s^2;
   % lambda(1,lindex)=ei*zi+sum(ej*zg./denom^2,1);
    
    %which sign to take 
    %we should take both signs as we did in other case
    lambda2=(-b+signl2*(b^2-4*a*c)^.5)/(2*a);

   
    
    y=1/2*s*(lambda2*estimmean-2*z)./(1+s*(eigd-eigd(iexpand)));
    