%function finfo=finfoestimshist(y,eigd,estimmean,z,shist,Chist) 
%   y   - stimulus expressed in eigen basis of Ck
%           eigd    - eigen values of the covariance matrix of just the
%           stimulus (not spike history) terms
%           estimmean - mean of stimulus terms projected into eigenspace
%           z - z= eigvec' Cka r
%                   r - spike history 
%                   Cka - correlation matrix between stimulus coefficents
%                   and spike history coefficent
%                   eigvec - eigenvectors of Ck=covariance matrix of just
%                   stimulus coefficents
%           shist - spike history terms (not the spike history coefficents)
%           shistmean - mean of spike history terms
%           Chist   - covariance matrix of just the spike history
%           coefficents
%
% Explanation: computes the fisher information when spike history terms are
% included. Stimulus is given using eigenvectors as basis.
%
%compute the log of the fisher information
%with spike history terms the inner prodcut can be very large and if we
%take exp to this we get inf
function finfo=logfinfoestimshist(y,eigd,estimmean,z,shist,shistmean,Chist)

%if y is empty return -inf
%we might pass in y=[] bc yrescaleshist returns an empty matrix if 
%the stimulus would turn out to be imagginary
%we return -inf so that this won't be returned as a maxmimum
if isempty(y)
    finfo=-Inf;
    return;
end

    g=y'*estimmean+shist'*shistmean;
    inner=sum(y.^2.*eigd)+2*y'*z+shist'*Chist*shist;
    h=.5*inner+log(inner);
    finfo=g+h;
    