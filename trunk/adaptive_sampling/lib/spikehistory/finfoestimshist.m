%function finfo=finfoestimshist(y,eigd,estimmean,z,shist,Chist) 
%   y   - stimulus expressed in eigen basis of Ck
%           eigd    - eigen values of the covariance matrix of just the
%           stimulus (not spike history) terms
%           estimmean - mean of stimulus terms projected into eigenspace
%           z - z= eigvec' Cka r
%                   r - spike history 
%                   Cka - correlation matrix between stimulus coefficents
%                   and spike history coefficent
%                   eigvec - eigenvectors of Ck=covariance matrix of just
%                   stimulus coefficents
%           shist - spike history terms (not the spike history coefficents)
%           shistmean - mean of spike history terms
%           Chist   - covariance matrix of just the spike history
%           coefficents
%
% Explanation: computes the fisher information when spike history terms are
% included. Stimulus is given using eigenvectors as basis.
%
function finfo=finfoestimshist(y,eigd,estimmean,z,shist,shistmean,Chist)
    g=exp(y'*estimmean+shist'*shistmean);
    inner=sum(y.^2.*eigd)+2*y'*z+shist'*Chist*shist;
    h=exp(.5*inner)*inner;
    
    finfo=g*h;
    