%function genkldata(outfile,msamps,trial,simvar,simfile,umeth)
%       outfile - output file where data will be saved
%       msamps - number of monte carlo steps to generate
%       trial     - which trial to compute KL distance for
%       simvar -  string name of simulation variable in file
%       simfile - simulation file which has the results from which we want
%                      to compute the KL distance
%                       if its not provided we assume we are continuing
%                       previous simulation
%
% ASSUMPTIONS:
%       1. assumes the log likelihood is the poisson model 
%       2. Assumes no spike history terms or fixed terms
%
%3-13-07
%   modified for new object oriented model
function genkldata(outfile,msamps,trial,simvar,simfile)
printinc=min([10^5 msamps/100]);          %how often to print a message;
%*********************************************************************
%Error Checking
%*********************************************************************
if ~isempty(simfile)
    if ~(exist(simfile,'file'))
        error('Simulation file doesnt exist')
    end
else
    %verify outfile exists
    if ~(exist(outfile,'file'))
        error(sprintf('Cant continue sim in %s \n File doesnt exist',outfile));
    end
end


if (msamps <=0)
    error('Msamps must be number > 0');
end

%**************************************************************************
%load the required variables
%*************************************************************************
%model data
[pdata simparam mparam sr]=loadsim('simfile',simfile,'simvar',simvar);

if (mparam.alength>0)
    error('Code currently cannot handle spike history terms.');
end

%load our gaussian posterior
gpost.m=pdata.m(:,trial+1);
gpost.c=pdata.c{trial+1};

if isempty(gpost.c)
    error('Posterior Covariance matrix was not saved for this trial you should write code to create it');
end

%load the prior
prior.m=pdata.m(:,1);
prior.c=pdata.c{1};


%load the observations and the responses
ll.obsrv=sr.nspikes(1:trial);
ll.input=sr.y(:,1:trial);

clear('sr');

%load data about what glm to use
glm=mparam.glm;
clear('simparam');


%**************************************************************************
%Generate the data
%**************************************************************************
%generate M samples from our gaussian q
%These our samples of possible models
%we do this by using samples from a gaussian with unit covariance and zero
%mean (that is z)
%Compute the cholesky decomposition
[evec,eigd] = svd(gpost.c);
U=evec*eigd.^(.5);

randn('state',sum(100*clock));
z=randn(size(gpost.m,1),msamps);
%generate the model samples
allmod=gpost.m*ones(1,msamps)+U*z;

%**************************************************************************
%We need to compute the true posterior at various model samples
%So for each model sample, we need to compute the prior on that model
%and we also need to compute the likelihood of all data observed under that
%model

%We want to compute the likelihood of the all data observed under each of the
%model samples we have drawn
%%mll should be 1xml vector which gives the loglikelihood of ALL THE DATA
%under each of our sample models
%add switch statements for everpossible combination of sampling
%distribution and nonlinearity we are interested in
if (isequal(glm.sampdist,@poissrnd)  && isequal(glm.fglmmu,@glm1dexp) )
    %likelihood function is just poisson distribution with canonical link
    %funciton
    mll=zeros(1,msamps);
    
    %loop over the model samples
    for mind=1:msamps;
        if (mod(mind,printinc)==0)
            fprintf('Generating sample: %g \n',mind);
        end
        ms=allmod(:,mind);
        
        %compute the dot prodcut of the model and the input on each trial
        epsilon=ms'*ll.input;
        %since its the canonical response lets not bother computing
        %exp(epsilon) b\c then we just take log
        %we compute an unormalized version of the likelihood
        %in particular we drop the factorial term since this can be
        %absorbed in the normalizing constant for the posterior
        mll(mind)=sum(epsilon.*ll.obsrv-exp(epsilon));        
    end
else
    error('Have not implemented code for distributions other than poisson with canonical link');
end

%compute the initial prior (that is pt-1(\theta)) for each of our model samples
%mpo should be 1xml vector specifying log likelihood 
%of the initial prior on each model.
%concatenate m to the argument list and call the function

%loop over the model samples
mpo=logmvgauss(allmod,prior.m,prior.c);

lpost=mpo+mll; 
%************************************************************************
%compute probability of our models under our approximate posterior
%Keep everything in the log domain to avoid numerical issues
%associated with really large negative exponents which give zeros
lpapprox=logmvgauss(allmod,gpost.m,gpost.c);


%******************************************
%sort the results
%do this so when we add lpost together we can avoid numerical errors
[lpost, sind]=sort(lpost,'ascend');
lpapprox=lpapprox(sind);
clear('allmod');                        %clear all mod because we didn't sort them
                                                %so they are no longer
                                                %properly aligned with
                                                %lpapprox and lpost
                                                

                                                
%create the output variable name to save in the file
vname=sprintf('dkdata%s%d',simvar,trial);

fprintf('Done creating samples. saving data \n');
%*************************************************************
%merge results?
%************************************************************
%check if we need to merge these results with previous results
merger=0;

if exist(outfile,'file')
    vars=whos('-file',outfile);
    varnames={vars.name};
    if ~isempty(strmatch(vname,varnames,'exact'))
        fprintf('genkldata: merging samples \n');
        %we need to merge the results
        %load the previous data
        olddat=load(outfile,vname);
        odat.lpapprox=olddat.(vname).lpapprox;
        odat.lpost=olddat.(vname).lpost;
        clear('olddat');
        %we do a merge sort 
        alldat.lpapprox=zeros(1,msamps+length(odat.lpapprox));
        alldat.lpost=zeros(1,msamps+length(odat.lpost));
        moldsamps=length(odat.lpapprox); %number of old samples
        %pointers
        oind=1;   %ptr to index in old list
        nind=1;     %ptr to new index in new list
        aind=1;     %index to merged list
        while (nind<=msamps && oind<=moldsamps)
            if (odat.lpost(oind)<lpost(nind))
                alldat.lpapprox(aind)=odat.lpapprox(oind);
                alldat.lpost(aind)=odat.lpost(oind);
                oind=oind+1;
            else
                alldat.lpapprox(aind)=lpapprox(nind);
                alldat.lpost(aind)=lpost(nind);
                nind=nind+1;
            end
                aind=aind+1;
        end
        %add on remaining elements
        if (nind>msamps)
            alldat.lpapprox(aind:end)=odat.lpapprox(oind:end);
            alldat.lpost(aind:end)=odat.lpost(oind:end);
        else
            alldat.lpapprox(aind:end)=lpapprox(nind:end);
            alldat.lpost(aind:end)=lpost(nind:end);
        end
        lpapprox=alldat.lpapprox;
        lpost=alldat.lpost;
        msamps=length(lpapprox);
        clear('alldat','odat');
    end
end
%create the variable with the results
cmd=sprintf('%s=[];',vname);
eval(cmd);
cmd=sprintf('%s.simfile=simfile;',vname);
eval(cmd);
cmd=sprintf('%s.glm=glm;',vname);
eval(cmd);
cmd=sprintf('%s.lpapprox=lpapprox;',vname);
eval(cmd);
cmd=sprintf('%s.lpost=lpost;',vname);
eval(cmd);
cmd=sprintf('%s.gpost=gpost;',vname);
eval(cmd);
cmd=sprintf('%s.msamps=msamps;',vname);
eval(cmd);
cmd=sprintf('%s.trial=trial;',vname);
eval(cmd);
%save to file
%create a variable named 'dkdatmethtrial#'
%merge with existing results if they exist
vtosave.(vname)='';
saveopts.append=1;  %append results so that we don't create multiple files 
savedata(outfile,vtosave,saveopts);

