%function [iidvar]=compiidasymvar(n,theta, magcon)
%   n - the length of the stimulus
%   theta - vector of parameters 
%   magcon - magnitude constraint on the stimulus
%
%Return value:
%   iidvar - structure containing predicted values for i.i.d design
%         .covarexp   - expected covariance matrix
%         
%          .asymevals - asymptotic eigenvalues of the covariance matrix
%                      - sorted in descending order
%          .asymevec - asymptotic eigenvectors of the covariance matrix
%   
%Explanation: This function computes the asymptotic covariance matrix for
%the iid design under a powerconstraint with EXPONENTIAL NONLINEARITY. The covariance matrix is
%computed in a coordinate system in which all components of theta except th
%e first one are zeros
%
%Revision History
%   3-17-2008
%       Bug Fix: In order to compute the asymptotic eigenvectors correctly
%       we need to know theta not just its magnitude because the first
%       eigenvector is parallel to theta
%   1-08-07 - This function is created from code in compasymvar.m
function iidvar=compiidasymvar(n,theta,mmag)


theta1=(theta'*theta)^.5;

%iidvariance in direction of theta
iidvar.asymevals=zeros(n,1);
iidvar.asymevals(1)=(exp(mmag^2*theta1^2/(2*n))*(mmag^4*theta1^2+n*mmag^2)/n^2)^-1;
%iid variance in directions orthogonal to theta
iidvar.asymevals(2:end)=(exp(1/2*theta1^2*mmag^2/n)*(mmag^2-(mmag^4*theta1^2+n*mmag^2)/n^2)/(n-1))^-1;
%iidvar.xp=exp(1/2*theta1^2*mmag^2/n)*(mmag^2-(mmag^4*theta1^2+n*mmag^2)/n^2)/(n-1);
%iidvar.xp=iidvar.xp^-1;

%iidvar.asymevecs=imax.asymevecs;

%we need to compute the eigen vectors for the asymptotic covariance matrix
%the first eigenvector is theta. The other eigenvectors are any set of
%othogonal vectors
iidvar.asymevecs=zeros(n,n);
iidvar.asymevecs(:,1)=normmag(theta);
%********************************************
%vectors orthonormal to theta
%we need a set of 
iidvar.asymevecs(:,2:end)=null(iidvar.asymevecs(:,1)');

%asymptotic covariance matrix
iidvar.covarexp=iidvar.asymevecs*diag(iidvar.asymevals)*iidvar.asymevecs;
