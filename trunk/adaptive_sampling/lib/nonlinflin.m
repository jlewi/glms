%function r=glm1dexp(k,x)
%       k - filter
%       x - input
%
% r:
%   r=exp(k'x) if k'x <0
%   r=1+k'x    if k'x >0
%
%Explanation:
%   function provides an alternate nonlinearity for a GLM which can
%   be used for generating simulated responses.
%   This function fulfills the criteria for the function to be log concave
%   see [Paninski04]
function [r,d1,d2]=nonlinflin(k,x)
   if exist('x','var')
       
u=k'*x;
    if (u<0)
        r=exp(u);
        d1=k*r;
        d2=k.^2*r;
    else
        r=1+u;
        d1=k;
        d2=zeros(length(k),1);
    end
   else
       u=k;
       if (u<0)
        r=exp(u);
        d1=r;
        d2=r;
    else
        r=1+u;
        d1=1;
        d2=0;
    end
   end
    
    
    

