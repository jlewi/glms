%6-01-2007
%check that projinp and qmat are consistent
function test(nobj);
d=nobj.d;

x=floor(rand(d,1)*10);


%project x into a higher space
xproj=projinp(nobj,x);

%generate an estimate of theta
theta=floor(rand(size(xproj))*10);
qmat=qmatquad(nobj,theta);
%test it
if (theta'*xproj~=x'*qmatquad(nobj,theta)*x)
    tproj=theta'*xproj;
    qexpr=x'*qmatquad(nobj,theta)*x;
    fprintf('Dot product=%d \t quad value=%d \n',tproj,qexpr);
    error('functions are not correct');
else 
    fprintf('functions are correct \n');
end



%********************************************
%test projqmat
%generate a random quadratic matrix it must be symettric
qmat=floor(rand(d,d)*10);
qmat=qmat*qmat';                

%check x'*qmat*x=projqmat(qmat)'*projinp(x);
qtrue=x'*qmat*x;
qproj=projqmat(nobj,qmat)'*projinp(nobj,x);

if (qtrue~=qproj)
%    fprintf('Dot product=%d \t quad value=%d \n',tproj,qexpr);
    error('projqmat is not correct');
else 
    fprintf('projqmat is correct \n');
end
return

%************************************************
%break it into components
%diagonal elements
dpterm=theta(1:d)'*xproj(1:d);
qterm=diag(qmat)'*(x.^2);
if ~(dpterm==qterm)
    fprintf('Diag: Dot prod=%d \t quad=%d \t\n',dpterm,qterm);
    error('Diagonal elements do not match');
else
    fprintf('Diag = %d \n',dpterm);
    fprintf('Diagonal elements are ok \n');    
end

%check rows
sind=d+1;
for r=1:d-1
   qterm=2*qmat(r,r+1:end)*x(r+1:end)*x(r);
   dpterm=theta(sind:sind+(d-r)-1)'*xproj(sind:sind+(d-r)-1)
   
   if ~(dpterm==qterm)
    fprintf('Diag: Dot prod=%d \t quad=%d \t\n',dpterm,qterm);
    error('elements do not match row=%d ',r);
else
    fprintf('Diag = %d \n',dpterm);
    fprintf('elements are ok \n');    
   end
sind=sind+d-r;
   
end