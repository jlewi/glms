%function x=projinp(nobj,stim)
%   nobj = the object responsible for projecting the input into the higher
%                dimension
%   stim=dxn
%            -each column is a different stimulus
%
%Explanation: Projects the input into the higher dimensional space in which
%a quadratic function (no linear term) is linear with the input
%
%6-01-2007: modify the projection so as to enforce symmetry of the Qmatrix
%
%Warning: If you change how x is represented you need to change the 
%   other functions of this class: qmatquad, projparam
%   use the test function to make sure they are all consistent
function x=projinp(nobj,stim)
d=nobj.d;

if isa(stim,'GLMInputVec')
if (d~=getDim(stim))
    error('This QuadNonLin was initialized with a size that does not match stim');
end
nstim=getnstim(stim);
stim=getData(stim);
else
if (d~=size(stim,1))
    error('This QuadNonLin was initialized with a size that does not match stim');
end
nstim=size(stim,2);
end
d=nobj.d;



%x=zeros(d^2,nstim);
x=zeros(d/2+d^2/2,nstim);

%first d components are just the stimulus terms squared
x(1:d,:)=stim.^2;

%the rest of the components are the upper triangular matrix of stim*stim'
% for j=1:nstim
%     s=stim(:,j)*stim(:,j)';
%     x(d+1:end,j)=s(:);
% end

% %to do this quickly for large numbers of stim
% %we take the matrix nstim and multiply by a shifted version of itself
% sind=d+1;
% for dshift=1:d-1
% %    dshift
% %    stim(dshift+1:end,:)
% %    stim(1:(end-dshift),:)
%    x(sind:sind+(d-dshift)-1,:)=2*stim(dshift+1:end,:).*stim(1:(end-dshift),:);
%    sind=sind+(d-dshift);
% end

%construct the rest of the terms 
%we basically need the upper triangular matrix
%of x*x'
%we multiply by 2 because we are only taking the upper triangular part
sind=d+1;
for r=1:d
    %number of terms
    nt=d-r;
    lind=sind+nt-1;
   x(sind:lind,:)=2*(ones(nt,1)*stim(r,:)).*stim(r+1:end,:);
   sind=lind+1;
end