%function qmat-qmatquad(nobj,theta)
%   nobj  - the nonlinear object
%   \theta- parameter corresponding to the stimulus when stimulus is pushed
%           through projinp
%
% Return value:
%   this returns a matrix q - dim(x)xdim(x) s.t
%   x'qmat*x=param^t projinp(nobj,x);
function qmat=qmatquad(nobj, theta)

d=nobj.d;
%verify dimension of theta
if (length(theta)~=d/2+d^2/2)
    error('dimension mismatch');
end

%first d components are the diagnal entries
qmat=diag(theta(1:d));

%construct the upper triangular part
tu=zeros(d,d);

sind=d+1;
for r=1:d;
    tu(r,r+1:end)=theta(sind:sind+(d-r)-1);
    sind=sind+(d-r);
end

qmat=tu'+qmat+tu;
