%function testexpdetint()
%
%Return value:
%   tbl - Cell array of the values computed using numerical integration
%         and computed using the approximation log(1+x)~x
%   otbl - onenotetable
%
%Revisions:
% 09-21-2008 - Turned this script into a static function of CompMINum
%            - Adjusted the function to work with the new CompMINum object
%            and and GLMModel objects
%
%Explanation: This function tests CompMINum for the canonical poisson.
%  We compute the expected value using numerical integration and comparing
%  the result to the value obtained using the approximation
%   log (1+x)~ x
%
function [tbl,otbl]=testexpdetint()

glm=GLMPoisson('canon');


opt.confint=.9999999;
mi=CompMINum('glm',glm,'confint',opt.confint);

%*************************************************************
%Test inner expectation over a discrete variable
%****************************************************************
% For the canonical response this is just the log of the variance times
% sigma see eqn 10 in aistat.
% CompMINum should handle this special case appropriately and avoid
% numerical integration
%

sigma=[10^-4 .001 .01 .1 1];
mueps=-2:.5:2;
%sigma=.05;

inint.numval=zeros(length(mueps),length(sigma));
for j=1:length(mueps)
    for k=1:length(sigma)
        inint.numval(j,k)=rexpdisc(mi,mueps(j),sigma(k));

        %compute true val
        %note use of log2 because thats what we use in rexpdisc
        inint.tval(j,k)=log2(1+glm.fglmmu(mueps(j))*sigma(k));
    end
end

%**************************************************************************
%Compute the expectation numerically and compare to the approximate value
%**********************************************************************
%compute the values for the canonical poisson case using our approximation
exvals.fapprox=zeros(length(mueps),length(sigma));
exvals.numval=zeros(length(mueps),length(sigma));

for j=1:length(mueps)
    for k=1:length(sigma)
        [exvals.numval(j,k)]=expdetint(mi,mueps(j),sigma(k));

        exvals.fapprox(j,k)=exp(mueps(j))*exp(1/2*sigma(k))*sigma(k);
        %we need to convert to base 2 because thats what expdet int uses
        exvals.fapprox(j,k)= exvals.fapprox(j,k)*log2(exp(1));
    end
end


%create a table to display the values
tbl=cell(length(mueps)*length(sigma),3);
tbl{1,1}='\mu';
tbl{1,2}='\sigma';
tbl{1,3}='Numerical';
tbl{1,4}='Approx';
row=1;

for k=1:length(sigma)
    for j=1:length(mueps)
        row=row+1;
        tbl{row,1}=mueps(j);
        tbl{row,2}=sigma(k);
        tbl{row,3}=exvals.numval(j,k);
        tbl{row,4}=exvals.fapprox(j,k);
    end
end

tinfo={'function',mfilename('full');};
tinfo=[tinfo;{'Numerical', 'Expectation w.r.t to \theta computed using numerical integration.'}];
tinfo=[tinfo;{'Approx','Use the approximation log(1+x)~x'}];
tinfo=[tinfo;{'Explanation','Test CompMINum numerical computation of E_{\theta}E_{\obsrv} log (1+ J_obs(r,x)x''c_t x). We evaluate the value using numerical computation and the approximation log(1+a)~a. When a is small the values should be close.'}];

otbl={tbl,tinfo};
onenotetable(otbl,seqfname('~/svn_trunk/notes/intcompare.xml'));

%
%compute the values for the canonical poisson case using our approximation
%fapprx=exp(mueps)*exp(1/2*sigma)*sigma;