%function compminfo(obj, batches,post)
%   batches - matrix of inputs each column is a different stimulus 
%   post    - GaussPost object        
%   
%function compminfo(obj, muproj,sglmproj)
%   muproj -1xn array of values 
%               \mu^t\stim
%   sigma - 1 xn array of values
%       \stim^t\covar\stim
%
%Return Value:
%   logmi= Log of the minfo - always a double
%
% Explanation: Note there are two different calling syntaxes for this
% function
function logmi=complogminfo(obj, var1,var2)
    
    logmi=double(log(compminfo(obj,var1,var2)));