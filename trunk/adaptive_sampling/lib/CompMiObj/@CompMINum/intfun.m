%function intfun(miobj,glmproj,mueps,sigma)
%   miobj - CompMINum object
%   glmproj - the projection x'theta for which we want to evaluate the
%              function
%   mueps
%   sigma
%Explanation: This is the function we want to integrate. Evaluating the
%integral gives the expected value of the objective function with respect
%to mu.
function v=intfun(miobj,glmproj,mueps,sigma)

    v=(rexpdisc(miobj,glmproj,sigma).*normpdf(glmproj,mueps,sigma^.5));
    
    if (any(isinf(v)))
       error('CompMINum:MIisInf','Mutual info times probability is infinite.'); 
    end