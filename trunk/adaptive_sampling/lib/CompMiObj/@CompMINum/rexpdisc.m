%function expvals=rexpdisc(miobj,glmproj,sigma)
%   miobj - The CompMINum object
%   glmproj - values of epsilon for which we compute the inner
%               expectation
%   sigma   - value of sigma for which we compute the expection
%
%Return value:
%   expvals - value of the integral for each value of glmrpoj
%
%Explanation:
%   This function numerically computes the expected mutual information (up
%   to a constant). That is it computes the expected value (over the
%   responses) of the determinant of the new covariance matrix.
%   This is similar to GLMModel/expjobs. expjobs however, computes the
%   expectation of the observed fisher information as opposed to the
%   expectation of the mutual information.
%
%   NOTE: THIS USE BASE LOG2 so its in bits not NATS
%
%   Since (1+small number)~ 0 we use a Taylor approximation of log(1+x)
%   to compute log(1+x) when x is small
%
%12-01-2008:
%   scale by jscale so that we can compute the lower bound using
%   CompMIPoissCanonLB properly
%09-21-2008
%    - Handle the Canonical Poisson as a special case because
%      the Fisher information is independent of the response.
%    - Update the code to use the New OOP model
%    - Each distribution for a GLM has its own class
%
%8-01-2007
%   d2glmeps is now a member of the GLMModel object
%Revision: 1485
%   -a member of the CompMINum object
%   -miobj.CompMIBase.glm and confint are properties of this class
% 2-14
%   fixed some bugs related to using the new miobj.CompMIBase.glm object
%
function expvals=rexpdisc(miobj,glmproj,sigma)

nvals=length(glmproj);      %how many values we need to evaluate it for
expvals=zeros(1,nvals);


%*****************************************************************
%Distribution of specific code
%****************************************************************
%this code depends on the distribution for the responses
%determine the bounds for integration and a pointer to the function
%which computes the pdf.
%
%pdfdist(r,mu) - pointer to function to compute pdf
%                       r - value of observation
%                       mu - mean parameter

%special case: Canonical Poisson. Observed Fisher info. is independent of r
%so we don't need to evaluate an expectation with respect to r
if (isa(miobj.glm,'GLMPoisson') && iscanon(miobj.glm))

    %the observation shouldn't matter
    r=0;
    [dlde, dlldde]=d2glmeps(miobj.glm,glmproj,r);

    %check if we need to use arbitrary precision to avoid overflow
    %scale it by jscale
    p=miobj.jscale*dlldde.*sigma;
    if any(isinf(p))
       p=mp(dlldde,miobj.nbits).* mp(sigma,miobj.nbits);
    end
    s=1-p;


    %convert s to a double because d2glmeps can return an mp object
    %we multiply by 1/2 so that we are computing bits
    expvals=double(1/2*log2(s));
    
     if isnan(s)
            warning('rexpdisc: sum is NaN');
     end
else

    %compute the mean of the sampling distribution.
    mu= fglmmu(miobj.glm,glmproj);
    rbounds=zeros(nvals,2);

    %we need to evaluate an expectation w.r.t to r
    pdfdist=miobj.glm.pdf;

    %get the bounds for integration
    tail=(1-miobj.confint)/2;
    rbounds(:,1)=cdfinv(miobj.glm,tail,mu');
    rbounds(:,2)=cdfinv(miobj.glm,1-tail,mu');

    %evaluate the integral for each value
    for k=1:nvals
        %the responses over which we sum to compute the expectation.
        r=floor(rbounds(k,1)):1:ceil(rbounds(k,2));
        r=r';         %needs to a be a column vector for when we call d2glmeps
        %for each response we need to compute
        %log(1-d^2p(r|epsilon)de^2 sigma)

        %1. compute d^2p(r|epsilon)de^2 sigma
        [dlde, dlldde]=d2glmeps(miobj.glm,glmproj(k),r);
        clear('dlde');
        %      error('Need to modify d2glmeps to ensure it computes the derivative correctly when obsrv is a matrix');

        %scale by jscale
        s=1-miobj.jscale*dlldde*sigma;

        if (s <1)
            s=log2(s);                    %use base 2 so its in bits not nats
                %convert s to a double because d2glmeps can return an mp
                %object
                s=double(s);
        else
            %if dlldde*sigma is small than 1-dlldde*sigma~0.
            %A better approxiation in this case comes from a taylor expansion
            warning('Do we really want to do this?');
            for rind=1:length(r)
                nterms=3;
                x=-dlldde(rind)*sigma;
                s(rind)=1/log(2)*sum((-1).^([0:nterms-1]).*(x).^[1:nterms]./[1:nterms]);
            end

            
        end
        %2. multiply each by the pdf
        %scale s by 1/2 to be bits
        %i.e entropy for Gaussian is proportional to 1/2 log |C_t+1|
        %so the 1/2 is unnecessary except if we want the units to be bits.
        s=1/2*s.*pdfdist(r,mu(k));
        s=sum(s);

        if isnan(s)
            warning('rexpdisc: sum is NaN');
        end
        expvals(k)=s;


    end
end