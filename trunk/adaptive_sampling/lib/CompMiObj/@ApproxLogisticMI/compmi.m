%function compmi(obj, muproj,sigma)
%   muproj -1xn array of values 
%               \mu^t\stim
%   sigma - 1 xn array of values
%       \stim^t\covar\stim
%
% Explanation: evaluates the mutual information for a stimulus as a
% function of the two scalar variables
function mi=compmi(obj,muproj,sigma)
%value of E(log(1+var(r|mu)*sigma))
s=1.5;
c=.25*(2*pi)^.5*s;
%expected variance of the logisitic output
expvar=c./((2*pi)^.5*(sigma.^2+s^2).^.5).*exp(-muproj.^2./(2*(sigma.^2+s^2)));
mi=expvar.*sigma.^2;
%finfo=expvar;