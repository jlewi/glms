%function [fobj]=miimg(pobj,muproj,sigma)
%   muproj - optional points to show on the grid
%   sigma - optional points to show
%make a contour plot of the objective function
function [fobj]=miimg(pobj,muproj,sigma)
fobj.hf=figure;
fobj.xlabel='\mu_\epsilon';
fobj.ylabel='\sigma^2';
fobj.name='Numerical Comp. of Obj. Func';
fobj.title=fobj.name;
%contourf(muglm,sigmasq,log10(lobjsurf'));
contourf(pobj.muproj,pobj.sigma,log10(pobj.mi));
lblgraph(fobj);
colorbar;
set(gca,'yscale','log');

%add lines to show the grid on which its computed
hold on;
for j=1:length(pobj.muproj)
   plot(pobj.muproj(j)*[1 1],[pobj.sigma(1) pobj.sigma(end)],'--');
end


for j=1:length(pobj.sigma)
   plot([pobj.muproj(1) pobj.muproj(end)],pobj.sigma(j)*[1 1],'--');
end

%if muproj and sigma were provided then plot them
if (exist('muproj','var') && exist('sigma','var')) 
fobj.hp=plot(muproj,sigma,'k.');
set(fobj.hp,'MarkerFaceColor','k');
end