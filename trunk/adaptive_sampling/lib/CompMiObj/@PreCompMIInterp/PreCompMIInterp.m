%function pobj=PreCompMIObj('filename',fname)
%   -load the object from the specified file
%
%function pobj=PreCompMIObj('filename',fname,'dotmu',mu,'sigmasq',sigma,'glm',glm)
%      fname - name of file to save object to
%      dotmu      - value of mu'*stim at which to compute objective
%      function
%      sigmasq - value of \sigma^2 at which to compute objective function
%      glm          - general linear model for which we compute the objective function
%
%     Precomputes the objective function on the specified values of
%     (dotmu,sigmasq) and then saves it to a file.
%
%    Parameters are specified as name value pairs so they can be specified
%    in any order
function pobj=PreCompMIObj(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'fname'};
con(1).cfun=1;

con(2).rparams={'fname','muproj','sigma','glm'};
con(2).cfun=2;
%ver internal object to keep track of class version so when we load the
%object we can handle any conversion

%**********************************************************************
% Members
%**************************************************************************
% mesh - an interernal structure array, 
%        TO use interp2 we need to restructure muproj and sigma 
%        rarther than doing this on every call to compmi we store the
%        results
pobj=struct('version',070718,'bname','CompMINum','fname',[],'muproj',[],'sigma',[],'mi',[]);

switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        %create the object and register it as subclass of StimChooserObj
        pobj=class(pobj,'PreCompMIInterp',CompMINum);
        return
    case 1
        %varargin should be a structure corresponding to fieldname value
        %pairs
        %this is useful for recieving arguments passed in from the
        %constructor of a child class
        params=varargin{1};
    otherwise
        %convert varargin into a structure array
        params=parseinputs(varargin);
end

%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    ismatch=1;
    for f=1:length(con(j).rparams)
        %check if  parameter was passed in
        if isempty(strmatch(con(j).rparams{f},pnames))
            ismatch=0;
            break;
        end
    end
    if (ismatch==1)
        cind=[cind j];
    end
end

if isempty(cind)
    error('Constructor:missing_arg','Required parameters for one of the constructors was not passed in');
elseif (length(cind)>1)
    cind=2;
%    error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        %load from file
        pobj=load(params.fname,'pobj');
        pobj=pobj.pobj;
        pobj.fname=params.fname;
    case 2
        %process each parameter
        pnames=fieldnames(params);
        for j=1:length(pnames)
            switch pnames{j}
                case 'fname'
                    pobj.fname=params.fname;
                case 'muproj'
                    pobj.muproj=params.muproj;
                    %sort them in ascending order
                    %and remove any duplicates
                    pobj.muproj=unique(pobj.muproj);

                    if isempty(pobj.muproj)
                        error('muproj cannot be empty');
                    end

                    %make sure its a vector check number of dimensions
                    if (isvector(pobj.muproj)==0)
                        error('dotmu must be vector');
                    end
                case 'sigma'
                    pobj.sigma=params.sigma;

                    %remove any duplicates and sort
                    pobj.sigma=unique(pobj.sigma);

                    %make sure they are greater than zero
                    if any(pobj.sigma<=0)
                        error('sigma must be >=0');
                    end
                    if (isvector(pobj.sigma)==0)
                        error('sigma must be vector');
                    end

                case 'glm'
                      params.glm;
                    
                    if ~isa(params.glm,'GLMModel')
                        error('GLM must be an object of class GLMModel');
                    end
                otherwise
                    error('unrecognized parameter %s',pnames{j});
            end
        end

        %*********************************************************
        %Create the object
        %****************************************
        %instantiate a base class if there is one
        pbase=CompMINum('glm',params.glm);
        pobj=class(pobj,'PreCompMIInterp',pbase);

        %compute the mutual information
        [pobj.mi]=precompmi(pobj,pobj.muproj,pobj.sigma);

        %save it to a file
        %if an error occurs still return the object so that we don't lose all
        %the pecomputed values
        try
            saveopts.append=1;  %append results so that we don't create multiple files
            %each time we save data.
            saveopts.cdir =1; %create directory if it doesn't exist
            vtosave.pobj='';
            savedata(pobj.fname,vtosave,saveopts);
            fprintf('Saved to %s \n',pobj.fname);
        catch
            fprintf('Error occured while trying to save object. Try to save it manually \n');
            s=lasterror;
            fprintf('Error msg %s \n',s.message);
        end

    otherwise
        error('not a valid constructor');
end

%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case {'fname','filename'}
            params.('fname')=vars{j+1};
        otherwise
            params.(vars{j})=vars{j+1};
    end
end
