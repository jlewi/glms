%function refinemu(dotmu)
%       dotmu - additional values of mu at which to precompute the dot
%       product
%
% Explanation: allows new values of mu to be added to the grid 
function [pobj]=refinesigmasq(pobj,muproj)

   muproj=sort(muproj,'ascend');
   
   %remove any values from muproj if we've already computed them
   for i=1:length(muproj)
       if ~isempty(find(pobj.muproj==muproj(i)))
           muproj(i)=NaN;
       end
   end
   ind=find(~isnan(muproj));
   muproj=muproj(ind);
   
   
   %compute the values of the objective for all values of dotmu
   nmi=zeros(length(pobj.sigma),length(muproj));
   nmi=precompmi(pobj,muproj,pobj.sigma);
   
   
   olen=length(pobj.muproj);        %original length
   ntoadd=length(muproj);        %number to add
   newfmi=zeros(length(pobj.sigma),olen+ntoadd);
   newmuproj=zeros(olen+ntoadd,1);
   %now we need to merge the arrays
   %we do this as a merge sort
   indold=1;
   indnew=1;
   
   fullind=1;
   while (indold<=olen && indnew <=ntoadd)
    if (muproj(indnew)<pobj.muproj(indold))
        newmuproj(fullind)=muproj(indnew);
        newfmi(:,fullind)=nmi(:,indnew);
        indnew=indnew+1;
    else
         newmuproj(fullind)=pobj.muproj(indold);
        newfmi(:,fullind)=pobj.mi(:,indold);
        indold=indold+1;
    end
        fullind=fullind+1;
   end
    
   if (indold<=olen)
       newmuproj(fullind:end)=pobj.muproj(indold:end);
       newfmi(:,fullind:end)=pobj.mi(:,indold:end);
   end
   if (indnew<=ntoadd)
       newmuproj(fullind:end)=muproj(indnew:end);
       newfmi(:,fullind:end)=nmi(:,indnew:end);
   end
   
   pobj.muproj=newmuproj;
   pobj.mi=newfmi;