%function precompmi(pobj, dotmu,sigmasq)
%       dotmu - values of dot product mean and stimulus
%       sigmasq - values of sigmasq
%
% Explanation- internal function called whenever we need to compute
% values of the objective function. PUrpose is to standardize the calls.
function [mi]=precompmi(pobj,dotmu,sigmasq)

    mi=zeros(length(sigmasq),length(dotmu));    
    for j=1:length(dotmu)
    for k=1:length(sigmasq)
        fprintf('dotmu= %g\t sigmasq=%g \n',dotmu(j),sigmasq(k));
        [mi(k,j)]=compmi(pobj.CompMINum,dotmu(j),sigmasq(k));
    end
    end