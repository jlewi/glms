%function cterms=compcterms(obj,inpbatch,post,pow)
%   inpbatch- batch of stimuli to compute the objective function for
%   post    - the posterior
%   pow     - which power we are raising the matrix to 
%
%Explanation: This is a function which should only be called by comptrpow
%to compute the fisherinfo.
%
%This function computes the portion of tr(A^n) which doesn't depend on the
%fisher info. 
function cterms=compcterms(obj,inpbatch,post,pow)
A=inpbatch'*getc(post)*inpbatch;

%get the linear indexes for the terms in A in each term in the expansion of
%tr(A^n)
[lindex]=getindexes(obj,size(inpbatch,2),pow);

%terms in the summation
cterms=ones(size(lindex,1),1);
for p=1:pow
    cterms=cterms.*A(lindex(:,p));
end