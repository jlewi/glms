%function sim=CompMIPoissExpTaylor('nterms')
%   nterms - how many terms in the Taylor expansion to use.
%
% Explanation: Class to compute the mutual information for a stimulus as
% function of the two scalar parameters \mu_proj=\stim^t\mu
% \sigma_proj=\stim^t\covar\stim.
%
%  This is intended to be used by the poolstim object
%
%
% Compute MI of a batch of stimuli for the canonical Poisson using a Taylor
% expansion. This method is only accurate if there are no spike history. 
%
classdef (ConstructOnLoad=true) CompMIPoissExpTaylor < CompMIBase
    %nterms - how many terms to use in the Taylor expansion
    properties(SetAccess=private,GetAccess=public)
        version=080904;
        nterms=[];
    end

    %***************************************
    %Properties: store temporary values we use to compute taylor terms
    properties(SetAccess=private,GetAccess=public,Transient=true)
        %tinfo - a structure array containing 
        %        information we use to compute each term in the taylor
        %        expansion
        %           .bsize - the size of the batch
        %                  - use this so we can verify batch size hasn't
        %                  changed from what we computed values for
        %           .indexes - this is a matrix
        %                    - each row of this matrix gives the indexes
        %                     into inpbatch'c_t inpbatch  of the terms we
        %                     need to sum.
        tinfo=[];
    end
    %we allow protected access so that we can test these functions using
    %our test harness
    
    methods(Access=protected)
        %the following two functions are used by comptrpow to compute
        %tr(A^n). We split the code into these two functions to enable
        %testing the code. 
       cterms=compcterms(obj,inpbatch,post,pow); 
       expjexp=compjterms(obj,inpbatch,post,pow);
       [lindex,indexes]=getindexes(obj,bsize,pow)
       
        
        %Return value:
        %  mi - always double
        [mi]=complogminfo(obj,batches,post);

    end
    methods
        function obj=CompMIPoissExpTaylor(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'nterms'};
            con(1).cfun=1;


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=[];
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=[];
                otherwise
                    %remove fields which are for this class
                    try
                      %  bparams=rmfield(params,'field');
                    catch
                    end
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@CompMIBase('glm',GLMPoisson('canon'));
            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 0
                    %do nothing used by load object

                case 1
                    obj.nterms=params.nterms;
                otherwise
                    error('Constructor not implemented')
            end





        end

    end
end


