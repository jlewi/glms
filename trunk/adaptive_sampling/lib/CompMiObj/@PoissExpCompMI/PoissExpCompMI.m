%function sim=PoissExpCompMI()
%
% Explanation: Class to compute the mutual information for a stimulus as
% function of the two scalar parameters \mu_proj=\stim^t\mu
% \sigma_proj=\stim^t\covar\stim.
%
%  This is intended to be used by the poolstim object
%
% $Revision$
%   i) moved class to directory CompMiObj
%   ii) made it subclass of newly created CompMIBase
%   iii) made glm parameter of base class
%   iv) added version, bname, as fields
%
%Revision:
%   08-11-2008
%       Switch to new OOP model
%       delete the field bname
classdef (ConstructOnLoad=true) PoissExpCompMI < CompMIBase
    properties(SetAccess=private,GetAccess=public)
        version=080811;
    end

    methods
        function obj=PoissExpCompMI(varargin)


            %**************************************************************
            %Required parameters
            %*****************************************************************
            %if fielname is required then set a field of req.fieldname=0
            %when we read in that parameter we set req.fieldname=1

            %**********************************************************
            %Define Members of object
            %**************************************************************
            % var1          - description
            % var2          -description
            %glm            - the glm for which we compute the mutual information
            %declare the structure


            %***************************************************
            %Blank Construtor: used by loadobj
            %****************************************************


            %call baseclass construct

            obj=obj@CompMIBase('glm',GLMPoisson('canon'));


        end

    end
end


