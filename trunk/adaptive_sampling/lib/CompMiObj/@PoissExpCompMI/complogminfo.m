%function compmi(obj, muproj,sigma)
%   muproj -1xn array of values 
%               \mu^t\stim
%   sigma - 1 xn array of values
%       \stim^t\covar\stim
%
% Explanation: evaluates the logarithm of the mutual information for a stimulus as a
% function of the two scalar variables
function mi=complogminfo(obj,muproj,sigma)
    %
    %compute the log of the fisherinfo
    mi=muproj+.5*sigma+log(sigma);