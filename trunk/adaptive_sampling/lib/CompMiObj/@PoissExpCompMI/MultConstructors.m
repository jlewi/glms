%function sim=PoissExpCompMI()
%
% Explanation: Class to compute the mutual information for a stimulus as
% function of the two scalar parameters \mu_proj=\stim^t\mu
% \sigma_proj=\stim^t\covar\stim.
%
%  This is intended to be used by the poolstim object
function obj=PoissExpCompMI(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
%con(1).rparams={'field1','parm2'};
%con(1).cfun=1;


%**********************************************************
%Define Members of object
%**************************************************************
% degree          - description
% coef              -coefficients for the fitted surface

%declare the structure
obj=struct();


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    obj=class(obj,'PoissExpCompMI');
    return
end

%convert varargin into a structure array
params=parseinputs(varargin);

%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor 
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    ismatch=1;
    for f=1:length(con(j).rparams)
        %check if  parameter was passed in
        if isempty(strmatch(con(j).rparams{f},pnames))
            ismatch=0;
            break;
        end
    end
    if (ismatch==1)
        cind=[cind j];
    end
end

if isempty(cind)
     error('Constructor:missing_arg','Required parameters for one of the constructors was not passed in');
elseif (length(cind)>1)
    error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
    
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=BaseClass();
obj=class(obj,'ClassName',pbase);

%if no base object
obj=class(obj,'ClassName',pbase);

%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case {'fname','filename'}
            params.('fname')=vars{j+1};          
        otherwise
            params.(vars{j})=vars{j+1};
    end
end


    