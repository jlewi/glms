%function sim=CompMIPoissCanonLB('method',method)
%   method - 'approx' or 'numerical'
%          - controls how we compute the mutual information for each
%          individual input. 'approx' means we use the approx log(1+x)~x
%          whereas numerical we use numerical integration
%   
%
%optional:
%   confint - the confidence interval to use when using numerical
%   integration
%
% Explanation: Computes a lower bound for the mutual information of a batch
% of stimuli. This lower bound is just the sum of the mutual information
% for each stimulus in the batch.
%Revisions:
%   12-01-2008: There was a bug in my formula for the lower bound. 
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) CompMIPoissCanonLB < CompMIBase
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %  mihelper - the miobj which we use to compute the mutual information
    %            of each input in the batch
    properties(SetAccess=private, GetAccess=public)
        ver=struct('CompMIPoissCanonLB',081201);

        mihelper=[];
    end


    methods
        %This should return the information in bits
        [mi]=compminfo(obj,batches,post);
        function obj=CompMIPoissCanonLB(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'method'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@CompMIBase('glm',GLMPoisson('canon'));
            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                    switch lower(params.method)
                        case 'numerical'
                            miparam.glm=GLMPoisson('canon');
                            if isfield(params,'confint')
                                miparam.confint=params.confint;
                            end
                            
                                obj.mihelper=CompMINum(miparam);
                        case 'approx'
                            error('To make it work with PoissExpCompMI we need to properly scale Jobs x''C_Tx by b, where b is the batch size');
                            obj.mihelper=PoissExpCompMI();
                        otherwise
                            error('Improper value for method.');
                    end
                                
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end

            

        end
    end
end


