%function sim=CompMIPoissCanonUB
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: For the canonical Poisson, compute an upper bound of the
% mutual information fora  batch of stimuli.
classdef (ConstructOnLoad=true) CompMIPoissCanonUB < CompMIBase
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        ver=struct('CompMIPoissCanonUB',081125);

    end

    methods
        function obj=CompMIPoissCanonUB(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'field1','parm2'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           
            
            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@CompMIBase('glm',GLMPoisson('canon'));
            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


        end
    end
end


