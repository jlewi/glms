%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
%
%Revisions:
%   10-01-2008 
%       This is now an Abstract class. So we can no longer instantiate
%       instances of this object.
%       However, this function might be called when loading an older MCOS
%       object. If that happens we just return the structure.
%
   function obj=loadobj(lobj)

    %check if lobj is a structure
    %this indicates the class structure has changed and we need to handle
    %the conversion   
    if isstruct(lobj)
        %create a blank object

        if isfield(lobj,'version')
            obj.bversion=lobj.version;
        else
                   obj.bversion=lobj.bversion;
        end
        
                   latestver=080917;
        
        %******************************************************************
        %Sequentially convert one version of the object to the next
        %until we get to the latest version
        %******************************************************************
        if (obj.bversion < 080811)
           %converted to new OOP model
           %field bname has been deleted
           fskip={'bname'};    %fields not to copy
           obj=copyfields(lobj,obj,fskip);
           obj.bversion=080811;
        end
        
        if (obj.bversion < 080917)
            %field nbits had been added
            %it is just initialized to default value
            obj.bversion=080917; 
        end
        
        %check if converted all the way to latest version
        if (obj.bversion <latestver)
            error('Object has not been fully converted to latest version. \n');
        end
        
            
        %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(lobj);              
        
    else
        obj=lobj;
    end
