%function miterm1(bsize)
%   bsize - size of the batch
%
%Explanation: Tests that the first term of the taylor expansion
%   of the the mutual info a batch of stimuli as computed by CompMIPoissExpTaylor
%  is correct
function success=miterm1(bsize)

success=true;

if ~exist('bsize','var')
    bsize=[];   
end

if isempty(bsize)
%size of the batch
bsize=ceil(rand(1,1)*10);
end
fprintf('bsize=%d \n',bsize);

%dimensionality of the inputs
inpdim=4;

%how many trials to run
ntrials=5;

%generate a posterior
%we need the eigenvalues of the covariance matrix to be < 1 so that 
%the sum converges
mu=randn(inpdim,1);
evecs=orth(randn(inpdim,inpdim));
eigd=rand(inpdim,1);
post=GaussPost(mu,evecs*diag(eigd)*evecs');

taylor=CompMIPoissExpTaylor('nterms',1);

greedy=PoissExpCompMI();

migreedy=zeros(1,ntrials);
for tind=1:ntrials
   %generate some random inputs
   batches{tind}=randn(inpdim,bsize)*10;

   muproj=getm(post)'*batches{tind};
   sigma=qprod(batches{tind},getc(post));
   
   migreedy(tind)=sum(compmi(greedy,muproj,sigma));
end

[mitaylor,taylor]=compmi(taylor,batches,post);
%take the log of mitaylor so we can compare to PoissExpCompMI
mitaylor=log(mitaylor);
if any(abs((mitaylor-migreedy)./migreedy)>10^-8)
    success=0;
    error('First term in taylor expansion is not correct.');
else
    fprintf('Success first term in Taylor Expansion is correct. \n');
end
