%function sim=ClassName(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: A test harness for the CompMIPoissExpTaylorclass.
%   The test functions are declared as static methods
%
classdef (ConstructOnLoad=true) TestCompMIPoissExpTaylor < CompMIPoissExpTaylor
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        tversion=080905;

    end

    %****************************************************************
    %test functions
    %******************************************************************
    methods (Static=true)
       success=miterm1(bsize); 
       success=testsub(bsize,pow);
    end
    methods
        function obj=TestCompMIPoissExpTaylor(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};
            con(1).cfun=1;


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=[];
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=[];
                otherwise
                    %remove fields which are for this class
                    try
                    catch
                    end
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 0
                    %do nothing used by load object

                otherwise
                    error('Constructor not implemented')
            end



        end
    end
    
    
end


