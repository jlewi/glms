%expval=expjobs(miobj,muglmproj,sigma)
%****************************************************************
%
%******************************************************************
%Computes the expectation: E_\epsilon E_r log Jobs(r,\epsilon)
%
%       muglmproj - mean of epsilon
%       sigma - variance of epsilon
%
%Return value:
%   expval - value of the explanation
%
%Explanation:
%   This function numerically computes the expected mutual information (up
%   to a constant). That is it computes the expected value (over the
%   responses) of the determinant of the new covariance matrix.
%   This is similar to GLMModel/expjobs. expjobs however, computes the
%   expectation of the observed fisher information as opposed to the
%   expectation of the mutual information.
%
%   NOTE: THIS USE BASE LOG2 so its in bits not NATS
%
%
%8-01-2007
%   d2glmeps is now a member of the GLMModel object
%Revision: 1485
%   -a member of the CompMINum object
%   -miobj.CompMIBase.glm and confint are properties of this class
% 2-14
%   fixed some bugs related to using the new miobj.CompMIBase.glm object
%
function expval=expjobs(miobj,mueps,sigma)
mbounds=zeros(1,2);     %bounds for integration over the gaussian variable.

if ~isscalar(mueps)
    error('mueps should be scalar');
end

if ~exist('opt','var')
    opt=[];
end
confint=miobj.confint;
if ~isfield(opt,'inttol')
    inttol=10^-6;
else
    inttol=opt.inttol; %tolerance for integration function
end
isdiscrete=1;           %determines whether r is discrete or continuous variable


%*****************************
%compute the bounds for mu
stdnorm=norminv(1-(1-confint)/2,0,sigma^.5);
mbounds=[mueps-stdnorm,mueps+stdnorm];

%*****************************************************************
%Distribution of specific code
%****************************************************************
isdiscrete=miobj.CompMIBase.glm.isdiscrete;
pdfdist=miobj.CompMIBase.glm.pdf;


%****************************************************************
%integrate by calling quad if its discrete distribution
%integrate by calling dblquad if its continuous likelihood funciton
%*************************************************************
if (isdiscrete==1)
    %the function we integrate is the value of the inner expectation
    %times the probability of that value of mu
    intfun=@(g)(innerexp(miobj,g).*normpdf(g,mueps,sigma^.5));
    expval=quad(intfun,mbounds(1),mbounds(2),inttol);
else
    error('Code to handle continuous distributions is not implemented');
end


%innerexp(miobj,glmproj)
%    miobj - miobj
%    glmproj - glmproj
%Explanation: Numerically compute the inner expectation
%    E_r log Jobs(r,epsilon)
%
function expvals=innerexp(miobj,glmproj)
nvals=length(glmproj);      %how many values we need to evaluate it for
expvals=zeros(1,nvals);
%compute the mean of the sampling distribution.
glm=getglm(miobj.CompMIBase);
mu=glm.fglmmu(glmproj);
rbounds=zeros(nvals,2);
%*****************************************************************
%Distribution of specific code
%****************************************************************
%this code depends on the distribution for the responses
%determine the bounds for integration and a pointer to the function
%which computes the pdf.
%
%pdfdist(r,mu) - pointer to function to compute pdf
%                       r - value of observation
%                       mu - mean parameter
pdfdist=miobj.CompMIBase.glm.pdf;

%get the bounds for integration
tail=(1-miobj.confint)/2;
rbounds(:,1)=miobj.CompMIBase.glm.cdfinv(tail,mu');
rbounds(:,2)=miobj.CompMIBase.glm.cdfinv(1-tail,mu');



%evaluate the integral for each value
for k=1:nvals
    %the responses over which we sum to compute the expectation.
    r=floor(rbounds(k,1)):1:ceil(rbounds(k,2));
    r=r';         %needs to a be a column vector for when we call d2glmeps
    %for each response we need to compute
    %log(1-d^2p(r|epsilon)de^2 sigma)

    %1. compute d^2p(r|epsilon)de^2 sigma
    [dlde, dlldde]=d2glmeps(miobj.CompMIBase.glm,glmproj(k),r);
    clear('dlde');
    %      error('Need to modify d2glmeps to ensure it computes the derivative correctly when obsrv is a matrix');


    %2. multiply each by the pdf
    s=log2(-dlldde).*pdfdist(r,mu(k));
    s=sum(s);

    if isnan(s)
        warning('rexpdisc: sum is NaN');
    end
    expvals(k)=s;

end