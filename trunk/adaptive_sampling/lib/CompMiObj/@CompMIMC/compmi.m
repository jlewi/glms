%function compmi(obj, muproj,sigma)
%   muproj -1xn array of values 
%               \mu^t\stim
%   sigma - 1 xn array of values
%       \stim^t\covar\stim
%
% Explanation: evaluates the mutual information for a stimulus as a
% function of the two scalar variables by using a combination of numberical integration
%   and monte carlo sampling.
function mi=compmi(obj,muproj,sigma)

if (length(muproj)~=length(sigma))
    error('mu and sigma should have same dimension');
end

npts=length(muproj);
mi=zeros(1,length(muproj));

pinc=.1;
plast=0;
for j=1:length(muproj)
    pcomplete=j/npts*100;
    if ((pcomplete-plast)>pinc)
       fprintf('Compute mi, %2.2g%% complete \n',pcomplete);
       pcomplete=plast;
    end
[mi(j)]=expdetint(obj,muproj(j),sigma(j));
end