%function sim=CompMIMC('glm',glm,opt parameters)
%
% OPtional parameters
%   confint - confidence interval. Used to determine bounds of integration
%   nmcsamps - number of monte carlo samples to use
%   
% Explanation: This class computes the mutual information as a function of
%  the \glmproj, \sglmproj using numerical integration to evaluate the
%  integrals for each call
%
function obj=CompMIMC(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'glm'};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%  bname  - name of base class. This allows us to refer to it templates
%declare the structure
%  confinit - confidence interval is used to define the interval for 
%             numerical integration
obj=struct('version',[],'bname',[],'nmcsamps',100,'confint',.99);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required        
        pbase=CompMIBase;
        obj=class(obj,'CompMIMC',pbase);
        return
    case 1
        %varargin should be a structure corresponding to fieldname value
        %pairs
        %this is useful for recieving arguments passed in from the
        %constructor of a child class
        params=varargin{1};
    otherwise
        %convert varargin into a structure array
        params=parseinputs(varargin);
end

%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor 
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    ismatch=1;
    for f=1:length(con(j).rparams)
        %check if  parameter was passed in
        if isempty(strmatch(con(j).rparams{f},pnames))
            ismatch=0;
            break;
        end
    end
    if (ismatch==1)
        cind=[cind j];
    end
end

if isempty(cind)
     error('Constructor:missing_arg','Required parameters for one of the constructors was not passed in');
elseif (length(cind)>1)
    error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        pbase=CompMIBase(params);
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
obj=class(obj,'CompMIMC',pbase);


%*****************************************************
%optional parameters
%******************************************************
if isfield(params,'confint')
    obj.confint=params.confint;
end

if isfield(params,'nmcsamps')
    obj.nmcsamps=params.nmcsamps;
end

%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case {'fname','filename'}
            params.('fname')=vars{j+1};          
        otherwise
            params.(vars{j})=vars{j+1};
    end
end


    