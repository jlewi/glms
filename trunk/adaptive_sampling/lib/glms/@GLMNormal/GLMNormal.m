%function GLMNormal('variance',sigma)
%   variance - the variance for the Normal distribution
%   Nonlinearity - in this case its just the linear function
%
% Explanation:
%   instead of defining properties which store pointers to the appropriate
%   functions we define the actual functions.
classdef (ConstructOnLoad=true) GLMNormal < GLMModel
    
   %**********************************************************
   %properties
   %
   %    variance - the variance for the output variable
   %************************************************************
    
   properties(SetAccess=private, GetAccess=public)
      variance=[]; 

      %we have to give a different name to version because verison is
      %already defined in the super class
      normversion=080707;
   end
    
   methods
       function glm=GLMNormal(varargin)
           glm=glm@GLMModel();
            if (nargin==0)
                return;
            end
            
            if (nargin==1)
                glm.variance=varargin{1};
                if (glm.variance<=0)
                    error('variance must be >0');
                end
            end
            
            %set the base class properties
            glm.linkcanon=@linearlink;
            glm.isdiscrete=false;
           
            glm.fglmmuhandle=@lincanon;
            %glm.fglmnc=;  %normalizing function for the distribution.
            %glm.fglmetamu=;   %function to convert mean into canonical parameter depends on the distribution
       end

       

      
       %********************************************************
       %function p=pdf(glm,obsrv,mu)
       %    obsrv - observations
       %    mu    - the mean
       %
       %Return value
       %    p - the probability of the observations
       function p=pdf(glm,obsrv,mu)
          %compute the probability of the observations given the mean
          p=normpdf(obsrv,mu,glm.variance);
       end
              
       %**********************************************************
       %function obsrv=cdfinv(glm,prob,mu)
       %   prob - the probability
       %
       %Return value - the values of the output 
       %    which have the specified probabilities given mean mu
       function obsrv=cdfinv(glm,prob,mu)
           obsrv=norminv(prob,mu,glm.variance);           
       end
      
   end
end