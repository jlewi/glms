%function [eta,deta, d2eta]=fetamu(glm,mu)
%       mu=1xn - means of the glm
%
% Explanation:
%       etamu computes eta, the canonical parameter as a function of the
%       mean=mu=f(glmproj=inp^T mu_t). f is the link function.
%
%       For the linear model, eta is just the output of f scaled by 1/glm.variance%2
%
function [eta,deta, d2eta]=fglmetamu(glm,mu)

eta=mu*1/glm.variance;

deta=1/glm.variance;
d2eta=0;