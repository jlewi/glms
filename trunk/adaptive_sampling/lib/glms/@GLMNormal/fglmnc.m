%****************************************************
%function nc=fglmnc(glm,eta)
%    eta - the value of eta
%
%Return value
%    A - the normalizing constant
%    dA - derivative of A w.r.t to eta
%    d2A -2nd derivative A. w.r.t to eta
%Explanation: The normalizing constant for the distribution
function [A, dA, d2A]=fglmnc(glm,eta)
A=glm.variance/2*eta.^2;
dA=glm.variance*eta;
d2A=glm.variance*ones(1,length(eta));
end