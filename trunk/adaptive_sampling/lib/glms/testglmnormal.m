%test script for glmnormal
clear variables
gnormal=GLMNormal(2);


%generate a random number to represent epsilon=theta'*stim
epsvar=rand(1);

%check link function and function to canonical eta up giving eta=epsvar
eta=fglmetamu(gnormal,fglmmu(gnormal,epsvar));

%***************************************************
%Test GLMNormal by generating some sample data and fitting a model to it
%and seeing if it converges. 


theta=[2;1];
dtheta=size(theta,1);
npts=10000;
x=normrnd(zeros(dtheta,npts),ones(dtheta,npts));

glmproj=theta'*x;
obsrv=sampdist(gnormal,compmu(gnormal,glmproj));

uobj=BatchML();


prior=GaussPost('m',zeros(dtheta,1),'c',eye(dtheta));

mobj=MParamObj('klength',2,'mmag',1,'glm',gnormal,'pinit',prior);
[post]=update(uobj,prior,x,mobj,obsrv);
