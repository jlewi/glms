%function [eta,deta,deta2]=fglmetamu(glm,mu)
%       mu - the mean of the logistic function
%
%Explanation function for Logistic GLM. Computes canonical parameter from
%the mean and its derivatives
function [eta,deta,deta2]=fglmetamu(glm,mu)
    %to avoid numerical errors due to divide by zero
   ind=find(mu==1);
   mu(ind)=1-eps;
   eta=log(mu./(1-mu));
   deta=1./(mu-mu.^2);
   deta2=1./(-1+mu).^2-1./mu.^2;
