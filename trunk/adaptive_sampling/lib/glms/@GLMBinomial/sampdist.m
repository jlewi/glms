%function obsrv=sampdist(glm,rexp)
%   rexp - expected value of the observations
%
%Return value
%   obsrv- samples drawn from the poisson distribution with mean rexp
function obsrv=sampdist(glm,rexp)
obsrv=binornd(1,rexp);
