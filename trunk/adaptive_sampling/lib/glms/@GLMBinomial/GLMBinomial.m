%function GLMNormal('variance',sigma)
%   variance - the variance for the Normal distribution
%   Nonlinearity - in this case its just the linear function
%
% Explanation:
%   instead of defining properties which store pointers to the appropriate
%   functions we define the actual functions.
classdef (ConstructOnLoad=true) GLMBinomial < GLMModel

    %**********************************************************
    %properties
    %
    %************************************************************

    properties(SetAccess=private, GetAccess=public)
   
        %we have to give a different name to version because verison is
        %already defined in the super class
        binomversion=080707;
    end

    methods
        function glm=GLMBinomial(varargin)
            
            glm=glm@GLMModel();
            if (nargin==0)
                return;
            end


            %set the base class properties

            glm.linkcanon=@logisticcanon;          %link funciton - funciton to generate the mean;
            glm.isdiscrete=true;

              switch nargin
                case 1
                    if isa(varargin{1},'function_handle')
                        glm.fglmmuhandle=varargin{1};
                    elseif (ischar(varargin{1}) && strcmp(varargin{1},'canon'))
                        glm.fglmmuhandle=glm.linkcanon;
                    else
                        error('incorrect type for fglmmu');
                    end
                otherwise
                    error('too many input arguments');
            end
            
          
            %this will test whether nonlinearity is onesided
            foneside=glm.fglmonesided;
        end


        

    end
end