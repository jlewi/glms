%function j=jexp(glm, glmproj)
%   glm - the object
%   glmproj-1xn vector of values for which we want to compute the expected
%   fisher information.
%
% Return value:
%   j - the expected fisher information: Jexp(x'theta)
%       1xn
%Explanation:computes the expected fisher information as a function of the
%projection of the stimulus on the true parameter. The
%expected fisher information if a function of the stimulus, and the true
%paramter. Because of the 1-d mondel Jexp(x,theta)=Jexp(x'theta)x*x'.
%
%
% Computes the expected fisher information numerically.
%
% LImitations:
%   1. currently only works for observations which are discrete
%
function j=jexp(glm,glmproj)


%if this is the canonical distribution
%Jexp=jobs. So avoid computing it
if iscanon(glm)
    %observed information is independent of r
    %so plug in any value
    j=jobs(glm,0,glmproj);
else

    %determine the bounds for integration and then call jexp in the base
    %class

    confint=.999;  %confidence interval. Used to determine the bounds for computing the expected fisher information.

    nvals=length(glmproj);      %how many values we need to evaluate it for
    rbounds=zeros(nvals,2);
    j=zeros(1,nvals);
    mu=glm.fglmmu(glmproj);

    rbounds(:,1)=0;
    rbounds(:,2)=poissinv(confint,mu);
    %find any zeros
    j=find(mu==0);
    rbounds(j,2)=0;

    j=jexp@GLMModel(glm,glmproj,rbounds);
end

