%function [eta, deta, d2eta]  =fglmetamu(glm,mu)
%       mu=1xn - means of the glm
%
% Explanation:
%       etamu computes eta, the canonical parameter as a function of the
%       mean
%       This version assumes eta=log(mu)
%       which is the case for the poisson distribution
function [eta, deta, d2eta]  =fglmetamu(glm,mu)
    
    %add a small number so that we don't take log of 0
    eta=log(mu+eps);
    deta=1./(mu+eps);
    d2eta=-1./((mu+eps).^2);