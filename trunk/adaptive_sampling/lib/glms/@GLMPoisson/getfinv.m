%function finv=getfinv(glm)
%   glm - GLMModel object
%
%Explanation: Returns a point to a function to invert the nonlinearity with respect to \glmproj.
function finv=getfinv(glm)

        
    if isequal(glm.fglmmuhandle,glm.linkcanon)
        finv=@log;
    else
        error('Only implemented for canonical poisson.');
        
    end
    