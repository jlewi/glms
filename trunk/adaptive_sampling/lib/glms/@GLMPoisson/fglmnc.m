%function nc=fglmnc(glm,eta)
%    eta - the value of eta
%
%Return value
%    A - the normalizing constant
%    dA - derivative of A w.r.t to eta
%    d2A -2nd derivative A. w.r.t to eta
%Explanation: The normalizing constant for the distribution
%
%
%Revisions:
%   09-21-2008 - Return mp objects if we would return infinity otherwise
%
function [A, dA, d2A]=fglmnc(glm,eta)

if (any(eta>log(realmax)))
    eta=mp(eta,glm.nbits);
end

[A,dA, d2A]=glm1dexp(eta);
end