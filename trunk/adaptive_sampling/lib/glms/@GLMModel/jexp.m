%function j=jexp(glm, glmproj, rbounds)
%   glm - the object
%   glmproj-1xn vector of values for which we want to compute the expected
%   fisher information.
%
% Return value:
%   j - the expected fisher information: Jexp(x'theta)
%       1xn
%Explanation:computes the expected fisher information as a function of the
%projection of the stimulus on the true parameter. The
%expected fisher information if a function of the stimulus, and the true
%paramter. Because of the 1-d mondel Jexp(x,theta)=Jexp(x'theta)x*x'.
%
%
% Computes the expected fisher information numerically.
%
% LImitations:
%   1. currently only works for observations which are discrete
%   

function j=jexp(glm,glmproj,rbounds)

error('Need to revise this function. It should be called from the subclass and have rbounds passed in');
confint=.999;  %confidence interval. Used to determine the bounds for computing the expected fisher information.
     
      nvals=length(glmproj);      %how many values we need to evaluate it for  
    rbounds=zeros(nvals,2);
    j=zeros(1,nvals);
    mu=glm.fglmmu(glmproj);
%*****************************************************************
%Distribution of specific code
%****************************************************************
%this code depends on the distribution for the responses
%determine the bounds for integration and a pointer to the function
%which computes the pdf.
%
%pdfdist(r,mu) - pointer to functioNNn to compute pdf 
%                       r - value of observation
%                       mu - mean parameter

switch glm.dist
    case 'poisson'
    rbounds(:,1)=0;
    rbounds(:,2)=poissinv(confint,mu);
    %find any zeros
    j=find(mu==0);
    rbounds(j,2)=0;
    case 'binomial'

    rbounds(:,1)=0;
    rbounds(:,2)=1;
    otherwise
    error('Cannot handle the specified sampling distribution');
end

%if this is the canonical distribution 
%Jexp=jobs. So avoid computing it
if iscanon(glm)
   %observed information is independent of r
   %so plug in any value
   j=jobs(glm,0,glmproj); 
else
%evaluate the integral for each value
for k=1:nvals
      %the responses over which we sum to compute the expectation.
      r=floor(rbounds(k,1)):1:ceil(rbounds(k,2));
      r=r';         %needs to a be a column vector for when we call jobs
      %for each response we need to compute 
      %log(1-d^2p(r|epsilon)de^2 sigma)
      
      %1. compute the observed fisher information
      jo=jobs(glm,r,glmproj(k));
  
      %2. multiply each by the pdf
      p=glm.pdf(r,glm.fglmmu(glmproj(k)));
      %normalize the pdf
      p=p/(sum(p));
      s=jo.*p;
      s=sum(s);
  
      j(k)=s;
      
end
end