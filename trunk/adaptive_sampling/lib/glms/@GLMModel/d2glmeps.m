%function [deps, d2eps]=d2glmeps(glm, epsvar,obsrv,dwhat)
%   glm    - GLModel object
%   epsvar -post.m'*x or mu'*x
%               -this is the value of the input times theta
%               -i.e it is the input to the link funciton of the glm
%               -1xn where n is number of epsvar to compute
%       obsrv -observation
%               -mx1 where m is number of observations for which we want to
%               evaluate the derivative if dwhat='m'
%             -dwhat ='v'
%               1xm - where epsvar is 1xm as well
%
%       dwhat - 'm' or 'v' controls whether return value is a matrix or vector
%               default: 'm'
%               'v'
%                   - obsrv and epsvar must have the same dimensions
% Computes the 1st and 2nd derivative of the the glm log likelihood (deta.*dmu.*(obsrv-dA))
% w.r.t to x'*theta= input to the link function of the glm
%
%Return Value:
%   if dwhat='v'
%   if dwhat='m'
%   dfdl - derivative
%           mxn
%   [df2dl2] - 2nd derivative
%           mxn
%   the (i,j)th entry of the return matrices give the value of the second
%   derivative for obsrv(i) and epsvar(j)
%   Optimization: If dlp2dt is not required (only 1 output argument) than
%       dlp2dt is not computed
%   Note the order of derivatives is to allow this function to be used with
%   matlab's fsolve routines to determine the optimal lambda
%
%
%Revision History:
%   09-21-2008 - Special processing for the Poisson model is now handled by
%   GLMPoisson
%
%   07-07-2008
%       New GLMModel object. The functions computing normalizing constants,
%       eta, etc... are now defined as methods of the class instead of
%       being stored as pointers. We now have a different object for each
%       distribution.
%   06-14-2007
%       Require obsrv to be row vector when dwhat='v'
%   6-13-2007: fixed bug related to obsrv not being same size as epsvar
%          when dwhat is vector
%   6-05-2007: Modified it to handle canoncial distribution as special case
%              also added dwhat
%   4-08-2006: Optimization. don't compute dlp2dt if not required.
%       don't comput inv(po.c) if its supplied
function [varargout]=d2glmeps(glm,epsvar,obsrv,dwhat)

if ~exist('dwhat','var')
    dwhat='m';
end
nepsvar=length(epsvar);


%************************************************************
%Special processing based on w.*(deta).^2.*(dmu).^2;hether we are computing a vector or a matrix
switch lower(dwhat)
    case 'm'
        nobsrv=size(obsrv,1);
        %make sure epsvar is row vector;
        %obsrv is col vector
        if (size(epsvar,1)>1)
            error('epsvar must be row vector');
        end
        %create a row and column of ones to use to create proper dimensions
        row1s=ones(1,nepsvar);
        col1s=ones(nobsrv,1);
        if (size(obsrv,2)>1)
            error('obsrv must be a col vector');
        end


    case 'v'
        row1s=1;
        col1s=1;
        nobsrv=length(obsrv);
        %obsrv and epsvar should both be vectors
        if ~isvector(obsrv)
            error('obsrv should be a vector');
        end
        if ~isvector(epsvar)
            error('epsvar should be a vector');
        end

        if (nobsrv~=nepsvar)
            error('to return a vector number of observations must equal number of glmproj');
        end
        %make obsrv and epsvar both row vectors
        obsrv=rowvector(obsrv);
        epsvar=rowvector(epsvar);
    otherwise
        error('unkown value for dwhat \n');
end



%***********************************************
%special case: canon
if iscanon(glm)
    %for canonical distribution don't compute eta and then compute mu
    %because this can introduce numerical problems
    eta=epsvar;

    %compute derivatives of normalizing function
        [A dA d2A]=fglmnc(glm,eta);
        
    %special processing for binomial distribution
    if isa(glm,'GLMBinomial')
        %to avoid numerical errors
        %we don't want to do obsrv-dA
        %because when obsrv is 1 and dA is close to 1 this will give
        %numerical errors
        %dA= exp(eta)/(1+exp(eta))
        %so 1- dA= 1/(1+exp(eta))
        %Better solution might be to use an mp object to handle arbitrary
        %precision.
        if ((lower(dwhat)=='v')|| numel(epsvar)==1 )
            dleps=zeros(1,length(eta));

            %set all entires as if obsrv=0
            dleps=-exp(eta)./(1+exp(eta));

            %change those entries for which obsrv =1

            ind=find(obsrv==1);
            if ~isempty(ind)
            dleps(:,ind)=(1+exp(eta(ind))).^-1;

            end
        else
            error('have not written code to handle the matrix yet');
        end
    else


        

        dleps=(obsrv*row1s-col1s*dA);
    end
    varargout{1}=dleps;

    if (nargout==2)
        %for canoncial distribution the first term is zero
        %t1=(obsrv*row1s-col1s*dA).*(col1s*(d2eta.*((dmu).^2)+deta.*d2mu));
        %t2=-d2A.*(deta).^2.*(dmu).^2;
        %for canonical distribution derivative is just -d2A
        %i.e for poisson its just -Var
        d2leps=col1s*-d2A;
        varargout{2}=d2leps;
    end

else
    %compute the derivatives of the mean
    [mu dmu d2mu]=compmu(glm,epsvar);

    %compute the derivatives of the canonical parameter
    [eta deta d2eta]=fglmetamu(glm,mu);

    %compute derivatives of normalizing function
    [A dA d2A]=fglmnc(glm,eta);

    dleps=(col1s*(deta.*dmu)).*(obsrv*row1s-col1s*dA);
    varargout{1}=dleps;

    if (nargout==2)
        t1=(obsrv*row1s-col1s*dA).*(col1s*(d2eta.*((dmu).^2)+deta.*d2mu));
        t2=-d2A.*(deta).^2.*(dmu).^2;
        d2leps=(t1+col1s*t2);
        varargout{2}=d2leps;
    end

end