%function finv=getfinv(glm)
%   glm - GLMModel object
%
%Explanation: Returns a point to a function to invert the nonlinearity with respect to \glmproj.
function finv=getfinv(glm)

        error('Only implemented for canonical poisson.');
        
    if (strcmp(glm.dist,'poisson') && (isequal(glm.fglmmu,glm.linkcanon)))
        finv=@log;
    else
    end
    