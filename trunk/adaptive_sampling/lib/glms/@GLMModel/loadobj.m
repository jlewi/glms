%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
function glm=loadobj(lobj)

    %check if lobj is a structure
    %this indicates the class structure has changed and we need to handle
    %the conversion   
    if isstruct(lobj)
    else
        glm=lobj;
    end
    
    if ~isfield(lobj,'version')
        lobj.version=080101;
    end
    
    if (lobj.version<=080101)
       %This version of the object is from before we started creating separate classes for
       %each distribution in the GLM family
       %therefore we need to create the object of the appropriate type
       switch lobj.dist
           case 'poisson'
               glm=GLMPoisson(lobj.fglmmu);
           case 'binomial'
               glm=GLMBinomial(lobj.fglmmu);
           otherwise
               error('unrecognized distribution');
       end
    end
   