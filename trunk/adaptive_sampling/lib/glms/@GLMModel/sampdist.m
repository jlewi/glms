%function samps=sampdist(glm,rexp)
%   glm  - glmModel object
%   rexp - expected value of the resopnse
%
%Explanation: sample the response distribution for this model
function samps=sampdist(glm,rexp)

samps=glm.sampdist(rexp);