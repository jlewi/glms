%function [dlpdt, dlp2dt]=d2glm(glm,theta,po,obsrv,x,glm)
%   theta - value of theta to compute derivatives at
%   po - gaussian of prior
%       invc - inverse of our prior.
%            - if its supplied we don't recompute it
%   obsrv - observations
%       (1xntrials) - MUST BE ROW VECTOR
%         - the observed responses for each stimulus presented
%   x    - stimuli
%       (dimstim x ntrials)
%       glmcf -  structure 
%               .fglmnc - pointer to function which computes the
%                               normalizing value as a function of the canonical parameter
%                               this is determined by the choice of
%                               distribution
%               .fglmmu  -pointer to function which computes the canonical
%                                    parameter as a function of the input
%                               input - x^t\theta
%                               output - mean
%                               this is the link function
%                               it is not determined by the choice of
%                               distribution
%               .fglmetamu - computes the canonical parameter as function
%                                      of mean
%                               input - mean 
%                               output -canonical parameter
%                                this is determined by the choice of
%                                distribution
%               -both functions should compute first and 2nd derivatives as
%               well. 
% Computes the 1st and 2nd derivative of the true posterior
%
%Return Value:
%   dfdl - derivative 
%              1xlength(theta)
%               derivative of likelihood w.r.t to theta
%   [df2dl2] - 2nd derivative 
%
%   Optimization: If dlp2dt is not required (only 1 output argument) than
%       dlp2dt is not computed
%   Note the order of derivatives is to allow this function to be used with
%   matlab's fsolve routines to determine the optimal lambda
%
% 
%Revision History:
%10-22-2007
%   made it a member of GLMModel
%$Revision: 1455 $: 6-14-2007
%   Needed to handle nargout=0
%   6-05-2007: use d2glmeps
%   2-14-2007: modified to properly handle onesided nonlinearities
%   4-08-2006: Optimization. don't compute dlp2dt if not required. 
%       don't comput inv(po.c) if its supplied
function [varargout]=d2glm(glm,theta,po,obsrv,x)

poinvc=getinvc(po);
%****************************************
%gaussian prior
%**************************************
%1st derivative
dprior=-(theta-getm(po))'*poinvc;

glmproj=theta'*x;
%******************************************
%special code to handle one sided nonlinearities for poisson model
%***********************************************
%if nonlinearity is one sided that is f(u)=0 for u<0
%then theta'*x<0  should only be zero for observations with r=0
%these points should be excluded from the computation of the gradient and
%jacobian because the likelihood for them is not changing
if (isa(glm,'GLMPoisson') && (glm.fglmonesided==1))
       %check theta'*x<0 iff  r=0
       ind=find((glmproj<0) & (obsrv>0));
       if ~isempty(ind)
               error('Current estimate yields theta such that glmproj<0 for observations>0');
       end            
       
       %remove points for which theta'*x<0 and r=0 so they don't influence
       %the gradient
        ind=find(glmproj>0);
        glmproj=glmproj(ind);
        x=x(:,ind);
        obsrv=obsrv(ind);     
end

%6-05-2007
%use d2glmeps to compute the derivatives with respect to epsvar=glmproj
if (nargout<2)
    [deps]=d2glmeps(glm,glmproj,obsrv,'v');
elseif (nargout==2)
    [deps, d2eps]=d2glmeps(glm,glmproj,obsrv,'v');
end

%***************************************
%1st derivative: multiply by the stimulus
%compute the gradient of the log likelihood
nobsrv=length(obsrv);
dimstim=size(x,1);
dll=(ones(dimstim,1)*deps);
dll=dll.*x;
%gradient of the log likelihood is sum of the gradients
%will be row vector
dll=sum(dll,2);

%***************************************************
%1st derivative: full derivative
dlpdt=dll'+dprior;
varargout{1}=dlpdt;

%***************************************************
%if 2 output arguments are supplied
%   compute the 2nd derivative
if (nargout==2)
    %2nd derivatpost=update(updater,mparam.pinit,stim,mobj,obsrv,[]);ive
    
   
     d2prior=-poinvc;
   

    d2ll=zeros(dimstim,dimstim);
    for index=1:nobsrv
        d2ll=d2ll+d2eps(index)*(x(:,index)*x(:,index)');
    end
    dlp2dt=d2ll+d2prior;
    varargout{2}=dlp2dt;
end

return;
