%return true or false indicating nonlinearity is oneside
function t=isonesided(glm)
    t=glm.fglmonesided;