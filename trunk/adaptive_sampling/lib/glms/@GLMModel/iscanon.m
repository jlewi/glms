%function b=iscanon(glm)
%   glm -
%
% Return value
%   b-true if nonlinearity is canonical nonlinearity
%     false if nonlinearity is not canonical nonlinearity
function b=iscanon(glm)

if isequal(glm.linkcanon,glm.fglmmuhandle)
    b= true;
    
else
    b=false;
end