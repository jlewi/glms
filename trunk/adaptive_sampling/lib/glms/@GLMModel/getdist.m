%function getdist=dist(obj)
%	 obj=GLMModel object
% 
%Return value: 
%	 dist=obj.dist 
%
function dist=getdist(obj)
    error('The dist property is obsolete since 07-07-2008');
