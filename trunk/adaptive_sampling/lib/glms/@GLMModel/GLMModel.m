%function glm=GLMModel(dist,nonlin)
%       dist - string representing the distribution for the GLM
%                 'poisson' or 'binomial'
%       nonlin- pointer to the function for the nonlinearity
%                   or 'canon' to use canonical nonlinearity
%   Explanation - instantiates a GLM structure to represent the given
%                       distribution
%
%Revisions
%   080921 - define a base implementation of cdfinv
%   080707 - Use New OOP model. I should really start having a separate
%       object for each possible distribution.
%       Add a function for fglmmu
%           rename the pointer fglmmu fglmmuhandle to stor a handle to the
%           link function
classdef (ConstructOnLoad=true) GLMModel



    %**************************************************************************
    %Glm structure
    %**************************************************************************
    %   fglmnc - normalizing function for the distribution.
    %   fglmetamu -function to convert mean into canonical
    %                        parameter depends on the distribution
    %   isdiscrete  - indicates whether the distribution for the glm is
    %                       continuous or discrete. This is used by expdetint
    %   cdfinv       - function to invert the cdf distribution
    %                         cdfinv(prob,mu)
    %                         prob - cumulative probability
    %                          mu - parameter mean of the pdf
    %                                =glm.fglmmu(\stim'*\theta)
    %                        added 1-07-07
    %
    %
    %fglmonesided - this means fglmmu(u)=0 if u<0
    %                         as part of the initialization we test to see if
    %                         this is true
    %                        -this is important because it affects how we do
    %                        gradient ascent to find the maximum of the
    %                        likelihood
    %   fglmmuhandle - handle to the function for the link function
    properties (SetAccess=protected, GetAccess=public)
        fglmmuhandle=[];
        linkcanon=[];
        isdiscrete=1;
        fglmonesided=[];
        version=080707;
    end

    methods
        %define a base implementation of cdfinv 
        function cdfinv(obj,p,rexp)
           error('cdfinv should be overriden in the subclass of GLMModel.'); 
        end
        function glm=GLMModel(varargin)

            %blank constructor for loading objects
            if (nargin>0)
                error('Too many input arguments. Probably using old version of class');
            end

            %***************************************************************
            %test if nonlinearity is one sided
            %**********************************************************

        end

        function foneside = get.fglmonesided(glm)
            %we make sure fglmmuhandle is not empty because
            %we don't want display to cause an error for an unitialized
            %object
            if (isempty(glm.fglmonesided) && ~isempty(glm.fglmmuhandle))
                glm.fglmonesided=false;
                glmproj=[-100 -10 -10:-1 -.9:.1:-.1];

                if isempty(find(fglmmu(glm,glmproj)~=0,1))
                    warning('Nonlinearity appears to be one sided. Fix code if not the case.');
                    glm.fglmonesided=true;
                end

            end
             foneside=glm.fglmonesided;

        end
    end
end
