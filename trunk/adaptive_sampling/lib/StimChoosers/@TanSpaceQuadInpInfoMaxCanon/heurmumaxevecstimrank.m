%function [stim]=modstimpoolmusigma(sobj,post,,mparam)
%   sobj - PoolStimQuadInpHeur
%   post - posterior
%   
%
%Return value:
%   stim - set of stimuli 
%
%Explanation this function uses a heuristic to create a set of stimuli
%   which will likely span the feasible region in mglmproj,sglmproj space
%
%
% This function takes into account the rank of the Q matrix
%   i.e the fact that we know how many basis vectors in feature space there
%   should be.
% Therefore to vary the stimuli along the MAP we vary the projection 
% of the stimuli along the first rank- principal components of the MAP
% reshaped as a matrix.
function [stimpool,sobj]=heurmumaxevecstimrank(sobj,post,mobj,rank)

dim=getstimlen(mobj);

%number of stimuli
psize=getnstim(sobj);

magcon=getmag(mobj);


%compute the vector in stimulus space most correlated with the mean of the
%posterior
%get a quadratic matrix
%i.e returna matrix qmat such that x'*qmat*x;
m.mat=qmatquad(getninpobj(mobj),getm(post));
%due an svd 
[m.evecs m.eigd]=svd(m.mat);

%pull out the max eigenvector
%version:070531 - instead of always selecting 
%   the max eigenvector select one of the principal components
%   sample the principal components in proprotion to the relative energy as
%   measured by the svd.
m.eigd=diag(m.eigd);
%normalize the energy of the singular values
neigd=m.eigd/sum(m.eigd);

%we know the model has rank basis vectors
%therefore we only tank the first rank-rank principal components
%each of these components should appear an equal number of times.
m.emax=[repmat(m.evecs(:,1:rank),1,floor(psize/rank)) m.evecs(:,1:mod(psize,rank))];

%randomly generate the projections along the mean
muproj=rand(1,psize)*2*magcon-magcon;


%******************************************************************
%To vary sigma
%*******************************************************************
%get the maximum eigenvector of the covariance matrix
efull=getefull(post);
[maxeigd mind]=maxeval(efull);

%max eigenvector
cpmax=getevecs(efull,mind(1));

%%compute the svd of the covariance matrix
%[csvd usvd]=svd(getc(post));

%pick the max eigenvector of the svd.
%cpmax=csvd(:,1);

%take the mag eigenvector
cmax.mat=qmatquad(getninpobj(mobj),cpmax);
%due an svd 
[cmax.evecs cmax.eigd]=svd(cmax.mat);
cmax.max=cmax.evecs(:,1);

cmax.eigd=diag(cmax.eigd);
%normalize the energy of the singular values
neigd=cmax.eigd/sum(cmax.eigd);

%each eigenvector appears a number of times proportional to probability
%of its eigenvalue.
%as we create the matrix of cmax.max we simultaneously 
%make the columns orthogonal to the corresponding 
%columns of m.emax. We use the repeated structure of m.emax to do this
%efficiently
cmax.max=zeros(dim,psize);
sind=1;
for j=1:dim
     eind=sind+floor(neigd(j)*psize)-1;
     
     %create a matrix of this vector -projection along each of the first
     %rank r compoents of m.evecs
     %m.evecs consists of the first rank vectors repeated 
     mvecs=m.evecs(:,1:rank);
     msind=mod(sind,rank);
     if (msind==0)
         msind=rank;
     end
     
     mvecs=[mvecs(:,msind:end)  mvecs(:,1:msind-1)];  
     bmat=cmax.evecs(:,j)*ones(1,rank)-(ones(dim,1)*(cmax.evecs(:,j)'*mvecs(:,1:rank))).*mvecs(:,1:rank);

     bmat=normmag(bmat);
     
     %now we create these columns by replicating the matrix
     ncols=(eind-sind)+1;
     cmax.max(:,sind:eind)=[repmat(bmat,1,floor(ncols/rank)) bmat(:,1:mod(ncols,rank))];

   sind=eind+1;
end

%any leftover due to rounding error we set to max evector
if (eind<psize);
    ncols=psize-eind;
    j=1;
    
      mvecs=m.evecs(:,1:rank);
     msind=mod(sind,rank);
     if (msind==0)
         msind=rank;
     end
     
     mvecs=[mvecs(:,msind:end)  mvecs(:,1:msind-1)];  
     bmat=cmax.evecs(:,j)*ones(1,rank)-(ones(dim,1)*(cmax.evecs(:,j)'*mvecs(:,1:rank))).*mvecs(:,1:rank);

     bmat=normmag(bmat);

     %now we create these columns by replicating the matrix    
     cmax.max(:,eind+1:end)=[repmat(bmat,1,floor(ncols/rank)) bmat(:,1:mod(ncols,rank))];

end


%normalize the vectors
%cmax.max=normmag(cmax.max);

%any energy not along the mean is along the max eigenvector
cpproj=(magcon^2-muproj.^2).^.5;

%********************************************************************
%add the projections along the cpmax and m.emax
stimpool=m.emax.*(ones(dim,1)*muproj)+cmax.max.*(ones(dim,1)*cpproj);

