%function stim=choosestim(sobj,simobj,trial)
%       sobj - the stimulus object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3
%Explanation: maximize the mutual information for the canonical poisson
%model
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mparam,varargin)
oreturn=[];

error('3-11-2008 this code has not been updated to use new method for computing posterior on the tangent space.');

if (strcmp(varargin{1},'trial')==1)
    trial=varargin{2};
else
    error('Trial was not passed in.');
end

%check whether we are doing infomax on the full posterior or tangent space
if ((isinf(sobj.iterfullmax) || mod(trial,sobj.iterfullmax+1)~=0) && (trial>sobj.startfullmax))

    pinfo=gettanpost(post);
         tanspace=gettanspace(post);
    rank=getrank(tanspace);
    %     %infomax on tangent space
%     %we compute the posterior on the tangent space by zeroing out the variance
%     %in directions orthogonal to the tangent space.

%     basis=getbasis(tanspace);
%     cfull=getc(getfullpost(post));
% 
% 
%     %we need a basis for the space orthogonal to the basis  of the tangent
%     %space
%     borth=null(basis');
% 
%     %zero out variance in directions orthogonal to the tangent space.
%     cinfo=cfull-borth*borth'*cfull-cfull*borth*borth'+borth*borth'*cfull*borth*borth';
% 
%     %posterior to use for infomax
%     minfo=gettheta(tanspace);
%     %compute the eigendecomposition of the object because we will need it to
%     %optimize the stimulus
%     pinfo=GaussPost('m',minfo,'c',EigObj('matrix',cinfo));

else

    %info. max on full space set posterior to full posterior
    pinfo=getfullpost(post);
    
    %we set rank= to the dimensionality of the stimulus because we don't
    %want to assume the rank is known because thats something we would only
    %know if we knew what manifold it was restricted to
    rank=getstimlen(mparam);
end

%we need to form the stimuli
stimpool=sobj.heur(sobj,pinfo,mparam,rank);

%*****************************************************************
%Compute the Mutual Information
%********************************************************************

miobj=PoissExpCompMI();

%psuh the inputs through the nonlinearity
stimnonlin=projinp(mparam,stimpool,shist);
muproj=getm(pinfo)'*stimnonlin;


sigma=getc(pinfo)*stimnonlin;
sigma=stimnonlin.*sigma;
sigma=sum(sigma,1);

mi=compmi(miobj,muproj,sigma);

%maximize the mi

[maxmi indmax]=max(mi);



poolind=indmax;

xmax=stimpool(:,poolind);
oreturn.sind=poolind;

%if nargout is > 3 return parameters related to the pool
if (nargout>3)

    varargout{1}.muproj= muproj;
    varargout{1}.sigma=sigma;

end