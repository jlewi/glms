%function stimpool=heurmumaxevec(sobj,tanspace,post,magcon)
%   sobj - object
%   tanspace - tangent space
%   post - posterior
%        - posterior after setting variance in directions orthogonal to the
%        tangent space to zero
%       
%   magcon -magcon
%Explanation: This heuristic takes the mean and a random vector in the
%eigenspace of the max eigenvector. We than form a basis from this two
%vectors and take a random combination of these vectors. 
%
%Note we set the stimulus magnitude= magnitude constraint so this may not
%be ideal for nonlinearities other than Poissexp
%
%
%This heuristic is very inefficient. Because we need to compute
%an svd for each stimulus in the pool.
function stimpool=heurmumaxevec(sobj, post,mobj,rank)

%length of the actual stimulus
stimlen=getstimlen(mobj);

%magnitude constraint
magcon=getmag(mobj);
%get the mean of just the stimulus terms
mustim=getm(post);


%normalize the mean
mustim=normmag(mustim);

nstim=getnstim(sobj);

%get the maximum eigenvectors
efull=getefull(post);
[maxeigd mind]=maxeval(efull);

%max eigenvector
vmax=getevecs(efull,mind);



%if the maxeigenvector has a multiplicity greater than 1 take random linear
%combinations of the vectors in this eigenspace
%generating these random numbers will be expensive when the multiplicity is
%high (i.e on early trials)
if (length(mind)>1)
    bpts=normrnd(zeros(length(mind),nstim),ones(length(mind),nstim));
    bpts=normmag(bpts);
    %form a vector by multiplying bpts by the maximum eigenvector
    vbasis=vmax*bpts;
    
    %make these vectors orthogonal to the mean
    vbasis=vbasis-mustim*(mustim'*vbasis);
else    
    vbasis=vmax;
    %make it orthogonal to the mean
    vbasis=vbasis-mustim*(mustim'*vbasis);
    
    vbasis=vbasis*ones(1,nstim);
    
end
%normalize vbasis
vbasis=normmag(vbasis);

%remove from vbasis projection in direction mustim
vbasis=vbasis-mustim*(mustim'*vbasis);


%mustim and vbasis are orthonormal 
%randomly generate pair which specify the amount of energy along
%vbasis, mustim.
y=normrnd(zeros(2,nstim),ones(2,nstim));
y=magcon*normmag(y);

stimpoolfull=vbasis.*(ones(size(vbasis,1),1)*y(1,:));
stimpoolfull=stimpoolfull+mustim*y(2,:);

%current the values in stimpoolfull live in feature space
%we need to compute actual stimuli which we do by taking the svd. 
stimpool=zeros(stimlen,nstim);


qinp=QuadNonLinInp('d',stimlen);


for j=1:nstim
   smat=qmatquad(qinp,stimpoolfull(:,j));
   [u, s]=svd(smat);
   vmax=u(:,1);
   
   
%using eigs appears to be slower than the SVD. I think because most of the
%eigenvalues of smat are close to zero.
%opts let eigs no that the matrix is real and symetric
% opts.issym = 1;
% opts.isreal=1;
% opts.disp=0;
% opts.tol=10^-3;
% opts.maxit=5;
   % [vmax emax]=eigs(@(x)(eigobj(x,stimpoolfull(:,j))),stimlen,1,'lm',opts);
%   [vmax,emax]=eigs(smat,1,'lm',opts);

 %  opts.v0=vmax;
   %scale it so it has full energy
   %u is of course a unit vector
   stimpool(:,j)=magcon*vmax;
end

%this function computes
%   smat'x 
%   smat=qmatquad(qinp,vec);
%   This function is used by eigs to evaluate the maximum eigenvetor of
%   smat
%   by using this function we can avoid having to reshape vec to find the
%   eigenvectors which is expensive. 
%
%   Warning: this assumes vec is stored as follows
%   first d entries are the diagonal entries of smat
%   remaining entrials are the lower triangular part stored as a vector
function vprod=eigobj(x,v)

d=length(x);

vprod=zeros(d,1);


sind=d+1;
%multiply the lower triangular part of smat by x
%loop across the columns of smat
for r=1:d;
    %multiply the diagonal element and add it to the product
    vprod(r)=v(r)*x(r);
    
    %entries below the diagonal
    %get the indexes of these elemends
    %r must be >1 to have any of these elements 
    if (r>1)
    j=1:r-1;
    ind=d+(j-1).*(d-j/2)+r-j;
    vprod(r)=vprod(r)+v(ind)'*x(1:r-1);
    end

    %elements above the diagonal
    if (r<d)
       sind=r*d-1/2*(r)*(r-1)+1;
       eind=sind+(d-r)-1;
       vprod(r)=vprod(r)+v(sind:eind)'*x(r+1:end);
    end
        
end
