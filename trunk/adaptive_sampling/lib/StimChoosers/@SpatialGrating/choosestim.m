%function [stim,stiminfo,obj]=choosestim(obj,post,shist)
%   obj - the stimchooserobj
%   post - the current posterior on the model parameters
%           .m
%           .c
%   shist - spike history
%
% Explanation: Create a random sinusoid.
function [stim,stiminfo,obj]=choosestim(obj,post,shist,mparam,varargin)
stiminfo=[];
width=obj.dim(1);
height=obj.dim(2);

x=ones(height,1)*[0:width-1];
y=[0:height-1]'*ones(1,width);


%draw the frequency by uniformly sampling the period
%we sample from a uniform distibution on the log of the period
%this will give each order of magnitude equal probability
%whereas if we uniformly sample on period it won't be
%(it would be biased towards large periods)

%randomly generate the log10(period)
lprange=log10(obj.speriod);
period=rand*(lprange(2)-lprange(1))+lprange(1);
period=10^period;
k=2*pi*1/(period);

%randomly draw the orientation
r=rand*2*pi;

%draw the phase 
phi=rand*2*pi;

stim=cos(k*x*cos(r)+k*y*sin(r)+phi);

figure;
imagesc(stim);

stim=stim(:)';
%normalize the power
stim=normmag(stim')*getmag(mparam);

stim=GlMInputVec('data',stim);

