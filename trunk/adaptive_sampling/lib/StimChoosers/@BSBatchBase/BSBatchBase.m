%function sim=BSBatchBase('bdata')
%   bsize - size of the batch
%
%
%
%Optional Parameters:
%   windexes - wave files to use. Only trials from these wave files will be
%             selected
%
%Explanation: An abstract base class for choosing a batch of stimuli from
%the bird song data
%
%We also save the mutual information for each stimulus in the pool to a
%file
%
%This class assumes we are dealing with the canonical Poisson
%This object assumes we are working
%Revisions:
%   11-06-2008 - New version add transient property paststim
%   10-17-2008
%       redefine the bdata property. No longer abstract
%       instead have it call the function getbdata which can be overloaded
%       in subclasses
%   080916 - Define the bdata property as abstract so subclass can redfine
%   how we access it
%   080828 - Initiliaze bindex to nan to indicate its not set
classdef (ConstructOnLoad=true) BSBatchBase < BSStimChooser



    %windexes - which wave files to choose the stimuli from
    %           by excluding some wave files you can create a test set
    %
    %how many   repeats of each wave file should we include
    %           - its pointless to include more than 1 repeat because only
    %             the spike history would change and in a normal experiemnt
    %             we would treat that as fixed anyway
    %bsize      - how big of a batch to choose
    %binfo      - keep track of the batch we are presenting
    %             choosetrial only returns a single stimulus
    %             The class BSBatchPoissLB takes care of presenting all
    %             stimuli in the batch and then selecting the appropriate
    %             optimal batch for the next bsize trials.
    %           .trial - keep track of how many trials we have presented
    %                  - this way we can make sure we are properly aligned
    %                     with the trial in simobj
    %
    %           .bstart - a structure array denoting the start of the batch
    %               .windex - the index of the wave file
    %               .repeat - the repeat of the wave file
    %               .trial - the relative trial within this wavefile
    %                       for the first stimulus in the batch
    %
    %           .bindex - The number of stimuli in the batch which have
    %                    already been presented
    %                   -thus when bindex>=bsize we need to pick a new
    %                   batch
    properties(SetAccess=protected,GetAccess=public)
        baseversion=081106;

        windexes=[];
        nrepeats=1;
        bsize=[];
        binfo=struct('trial',0,'bstart',[],'bindex',nan);
    end

     properties(SetAccess=private,GetAccess=public,Transient)
        %past stim is used to store information about which
        %wave files were already presented
        %this is used to prevent us from improperly selecting the same
        %batch.
        paststim=[];
     end
    
    properties (Dependent=true)
        bdata;
    end

    methods(Abstract)
        %an abstract function which chooses the batch of stimuli when
        %necessary. This should be overloaded in the derived classes
        %it gets called by choosetrial.
        %returns a structure with fields windex, repeat, and trial
        %which identify the start of the batch.
        [bstart, blogminfo]=choosebatch(obj,simobj,post);
      
        bdata=getbdata(obj);
    end
    methods
        function bdata=get.bdata(obj)
            bdata=getbdata(obj);
        end
        function obj=BSBatchBase(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            %The blank constructor will be used by the base class when
            %loading an object
            con(1).rparams={};
            con(1).cfun=1;

            con(2).rparams={'bsize'};
            con(2).cfun=1;

            %try calling constructor for base class
            obj=obj@BSStimChooser();


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************

                    return
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            if (~isempty(params) && length(cind)>1)
                cind=cind(2:end);
            end

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    %no processing 
                    return;
                case 2

                    obj.bsize=params.bsize;


                otherwise
                    error('Constructor not implemented')
            end


            %***********************************************************
            %process optional arguments
            %***********************************************************
            
            fnames=fieldnames(params);
            for  fn=fnames'
               switch lower(fn{1})
                   case {'bsize','bdata'}
                       %do nothing already processed                      
                   case 'windexes'
                       obj.windexes=params.windexes;
                   case 'nrepeats'
                       obj.nrepeats=params.nrepeats;
                   otherwise
                       error('Unrecognized optional parameter %s \n',fn{1});
               end
            end
                   
            

        end
        %Return Value:
        %     ntrials - the number of trials we have data for
        function ntrials=getntrials(obj)
            %we need to compute the number of trials in the allowed indexes
            nwindex=length(obj.windexes);

            ntrials=0;
            for wind=1:nwindex

                [ntrialsinwave]=getntrialsinwave(obj.bdata,obj.windexes(wind));
                ntrials=ntrials+ntrialsinwave*obj.nrepeats;
            end

        end

        [stim, shist,obsrv,stimindex,blogminfo]=choosetrial(stimobj,bssimobj,prior);
           
        

    end
end



