%function sim=RSMutualInfo(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object 
%
%function ('dotmu',dotmu,'sigmasq',sigmasq,'imutual',imutual,'degree',degree)
%   dotmu - values of dotmu at which imutual is evaluated
%   sigmasq - values of sigmasq at which imutual is evaluated
%   imutual - table of values for the 2-d surface we want to fit a surface
%            to
%           imutual(j,k)=f(dotmu(j),sigmasq(k))
%   degree  -highest degree for the polynomial
% Explanation: Template for the constructor of a new class.
function obj=RSMutualInfo(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'fname','degree'};
con(1).cfun=1;

con(2).rparams={'dotmu','sigmasq','imutual','degree'};
con(2).cfun=2;

%**********************************************************
%Define Members of object
%**************************************************************
% degree          - description
% coef              -coefficients for the fitted surface
% cdegree        - (degree+1)^2x2 array
%                          matrix which gives the degree of dotmu(col 1),
%                          sigmasq (col2) associated with each coefficient
% construct      - structure which describes which constructor was called
%                           has different fields based on method of
%                           construction
%                      .cind - number corresponding to the constructor
%declare the structure
obj=struct('degree',[],'coef',[],'construct',[],'cdegree',[]);


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    obj=class(obj,'RSMutualInfo');
    return
end

%convert varargin into a structure array
params=parseinputs(varargin);

%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor 
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    ismatch=1;
    for f=1:length(con(j).rparams)
        %check if  parameter was passed in
        if isempty(strmatch(con(j).rparams{f},pnames))
            ismatch=0;
            break;
        end
    end
    if (ismatch==1)
        cind=[cind j];
    end
end

if isempty(cind)
     error('Constructor:missing_arg','Required parameters for one of the constructors was not passed in');
elseif (length(cind)>1)
    error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        %construct the response surface based on data stored in a
        %precomputed file
        if ((params.degree<1) || (params.degree~=floor(params.degree)))
            error('degree must be an integer >0')
        end
        
        %load the precomputed object.
        obj.degree=params.degree;       
        pobj=PreCompMIObj('filename',params.fname);
        
        %save info about this constructor
        obj.construct.cind=1;
        obj.construct.fname=params.fname;
        
        %create the class
        obj=class(obj,'RSMutualInfo');
        [obj mse]=fitrsm(obj,pobj.dotmu,pobj.sigmasq,pobj.mi,obj.degree);
        fprintf('Fitted surface degree %d \t Avg \% error=%d \n',obj.degree, mse*100);
    case 2
        obj.degree=params.degree; 
        obj.construct.cind=2;
        obj.construct.imutual=params.imutual;
        obj=class(obj,'RSMutualInfo');
        [obj, mse]=fitrsm(obj,params.dotmu,params.sigmasq,params.imutual,obj.degree);
        fprintf('Fitted surface degree %d \t Avg %% error=%d \n',obj.degree, mse*100);
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
%pbase=BaseClass();
%obj=class(obj,'ClassName',pbase);

%if no base object
%obj=class(obj,'RSMutualInfo');

%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case {'fname','filename'}
            params.('fname')=vars{j+1};          
        otherwise
            params.(vars{j})=vars{j+1};
    end
end


    