%function stim=choosestim(finf,post,shist,einfo,mparam,)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
% Return value
%       stim - the stimulus for the next trial
%       opreturn    - optional return information about the procedure
%               .ocase - a number giving information on how the stimulus was
%               selected.
% Explanation: Chooses the stimulus by doing a 2-d optimization over the
% precomputed region.
function [optstim, oreturn,pobj, varargout]=choosestim(pobj,post,shist,mparam,glm,varargin)
oreturn=struct('ocase',0,'dotmuopt',[],'sigmaopt',[],'dotmulim',[]),'miopt',[]);
%process the optional arguments to get the trial number
for j=1:2:length(varargin)
    switch varargin{j}
        case 'trial'
            trial=varargin{j+1};
    end
end

if (trial>pobj.nrand)
    %choose the optimal stimulus by doing a search over the precomputed
    %region
    pbase=pobj.PreCompMIObj;
    [optstim oreturn pbase qmod]=choosestim(pobj,post,shist,einfo,mparam);

    %use the RS to search parts of the feasible region which were not
    %compute the values of dotmu over which we have to do a search
    dotmu=[];
    if (oreturn.dotmulim(1)<pbase.dotmu(1))
        dotmu=[oreturn.dotmulim(1):pobj.ddotmu:(pbase.dotmu(1)-pobj.ddotmu)];
    end

    if (oreturn.dotmulim(2)>pbase.dotmu(end))
        dotmu=[(pbase.dotmu(end)+pobj.ddotmu):pobj.ddotmu:oreturn.dotmulim(2)];
    end

    if ~isempty(dotmu)
        %use the RSM to optimize over the feasible region for which
        %no precomputed values exist
        %for each value of dotmu we compute sigmamin, sigmamax
        %we then use the RS to find the optimal value on this range
        [optrs]=optoverrsm(pobj,dotmu,qmod,mmag,optim)

        %if the optimium over the precomputed region is better
        if ((optrs.miopt>optstim.miopt) || (bitand(oreturn.ocase,pbase.ocases.nooverlap)) || isempty(oreturn.miopt))
            oreturn.ocase=pbase.ocases.normal;
            oreturn.dotmuopt=optrs.dotmuopt;
            oreturn.sigmaopt=optrs.sigmaopt;
            oreturn.miopt=optrs.miopt;


            %compute the actual stimulus
            %solve for the actual stimulus
            %so we need to find a linear combination of xmax, xmin which gives
            %sigmaopt
            A=post.c(1:mparam.klength,1:mparam.klength);
            if ~isempty(mparam.alength>0)
                b=shist'*post.c(mparam.klength+1:end,1:mparam.klength);
                d=shist'*post.c(mparam.klength+1:end,mparam.klength+1:end)*shist;
            else
                b=zeros(mparam.klength,1);
                d=0;
            end
            options = optimset('Jacobian','on','tolfun',10^-12);
            fs=@(s)stimsigma(s,optdmu.sigmaopt,A,b,d,optdmu.xmin,optdmu.xmax);
            [sopt,fzero]=fsolve(fs,.5,options);
            optstim=(1-sopt)*optdmu.xmin+sopt*optdmu.xmax;

        end
    end
else
    %random stimulus
    pbase=pobj.PreCompMIObj;
    ocases=pbase.ocases;
    oreturn.ocase=ocases.randstim;

    obj=RandStimBall('mmag',mparam.mmag, 'klength',mparam.klength);
    optstim=choosestim(obj);
end

%the following allows other choosestim functions to return more
%arguments
if (nargout>3)
    for j=1:(nargout-3)
        varargout{j}=[];
    end
end

%use the rsm to optimize over the region dotmu
function [optdmu]=optoverrsm(pobj,dotmu,qmod,mmag,optim)
klength=length(qmod.muk);
%optimize over the bounding box two steps
%1. for each value of dotmu on our grid, comput sigmamax and sigmamin
%2. find the maximum value for this value of dotmu
%**********************************************************************

optdmu.mi=-inf;
optdmu.qmax=[];
optdmu.qmin=[];
optdmu.xmax=zeros(klength,1);
optdmu.xmin=zeros(klength,1);
optdmu.dotmuopt;=[];
optdmu.sigmaopt=[];

for dmu=dotmu


    %dotmu is dotpduct stimulus and mean
    %stimulus we have to divide this by the magnitude of the mean
    %because we want the projeciton along the mean of the stimulus
    muproj=dmu/mumag;
    %compute qmax and qmin
    [qmax,qmin,xmax,xmin]=maxminquad(quad,muproj,mmag,optim);

    %compute the mutual information for these values
    sigmasq=qmin:pobj.dsigmasq:qmax;
    imutual=mi(pobj.rsm(dmu,sigmasq));

    %find the max
    [imax,imind]=max(imutual);

    if (imax>optdmu.mi)
        optdmu.mi=imax;
        optdmu.qmax=qmax;
        optdmu.qmin=qmin;
        optdmu.xmax=xmax;
        optdmu.xmin=xmin;
        optdmu.mi=imax;
        optdmu.dotmuopt=dmu;
        optdmu.sigmaopt=sigmasq(imind);
    end

end




%***************************************
%function we need to solve to find stimulus which gives (muopt,
%sigma^2_opt)
%see aistat eqn 24.
function [d,jac]=stimsigma(s,sigmasqopt,A,b,d,xmin,xmax)
x=(1-s)*xmin+s*xmax;

sigmasq=x'*(A*x)+b'*x+d;
d=sigmasq-sigmasqopt;
%return the jacobian
jac=(2*x'*A+b')*(-xmin+xmax);