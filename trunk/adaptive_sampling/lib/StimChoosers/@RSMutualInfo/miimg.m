%function [fobj]=miimg(robj,dotmu,sigmasq)
%   dotmu - falues at which to compute
%   sigmasq
%make a contour plot of the objective function
function [fobj]=miimg(robj,x,y)
fobj.hf=figure;
fobj.xlabel='\mu_\epsilon';
fobj.ylabel='\sigma^2';
fobj.name=sprintf('Fitted Surface to Mutual Information Degree %d',robj.degree);
fobj.title=fobj.name;

m=mi(robj,x,y);
%contourf(muglm,sigmasq,log10(lobjsurf'));
contourf(x,y,m');
lblgraph(fobj);
colorbar;
