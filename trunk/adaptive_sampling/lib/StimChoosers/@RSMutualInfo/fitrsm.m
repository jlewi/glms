%function [robj,mse]=fitrsm(robj, x,y,fxy, degree)
%       x - vector of x values
%       y - vector of y values
%   Return value
%       acoef - coeffcients of rsm
%       cdegree -(degree+1)^2x2 array specifying degree of x and
%       y associated with each coefficient
%       nrmse   - root of the normalized mean squared error of the RSM
%               -for each value we compute the square root of the error and
%               divide by the actual value
% Explanation: 
%       fits a response surface of degree degree to 
%        fxy(j,k)=f(x(j),y(k))
%**********************
function [robj,nrmse]=fitrsm(robj,x,y,fxy,degree)
    %************************************
    %error checking
    %****************************************
    if ~isvector(x)
         error('x must be vector');
    end
    
    if ~isvector(y)
         error('y must be vector');
    end
    
    if (length(x)~=size(fxy,1))
        error('dimension mismatch between fxy and x');
    end
    
   if (length(y)~=size(fxy,2))
        error('dimension mismatch between fxy and y');
   end
   %make x,y col vectors
   if (size(x,2)>1)
       x=x';
   end
   if (size(y,2)>1)
       y=y';
   end
   
   
   %*****************************************************************
   %form the augmented vectors
   %for each data point the augmented vector is (degree+1)^2 long
   npts=length(x)*length(y);
   %each row of x is a different input
   pts=zeros(npts,(degree+1)^2);
   

   %we need to loop over x in the outer loop
   %because we reshape fxy to be a vector which means we take the
   %elements column wize    
   %so get the indexes of x and y corresponding to each point in fxy
   [xind,yind]=ind2sub(size(fxy),1:prod(size(fxy)));
   
   %form the augmented points
   %
   %xdegreemat=ones(length(xind),1)*xdegree';
   
   %ydegree=ones(degree+1,1)*[0:degree];
   %ydegree=ydegree(:);
  % ydegreemat=ones(length(yind),1)*ydegree';
  % xaug=((x(xind)*ones(1,(degree+1)^2)).^xdegreemat);
 %  yaug=((y(yind)*ones(1,(degree+1)^2)).^ydegreemat);
%   pts=xaug.*yaug;
   
    cdegree=zeros((degree+1)^2,2);
   %dd=[0:degree]'*ones(1,degree+1);
   %cdegree(:,1)=reshape(dd,(degree+1)^2,1);
   xdegree=[0:degree]'*ones(1,degree+1);
   xdegree=xdegree(:);
   robj.cdegree(:,1)=xdegree;

   %ds=ones(degree+1,1)*[0:degree];
%   cdegree(:,2)=reshape(ds,(degree+1)^2,1);
   ydegree=ones(degree+1,1)*[0:degree];
  ydegree=ydegree(:);
  robj.cdegree(:,2)=ydegree;
   
   pts=augpts(robj,[x(xind),y(yind)]);
   %for dind=1:length(x)
    %       daug=(x(dind).^[0:degree])';
     %  for sind=1:length(y)
      %      saug=(y(sind).^[0:degree]);
   %         pts(xind,:)=reshape(daug*saug,1,(degree+1)^2);
       %     xind=xind+1;
       %end
   %end
   %compute the degree
   
   
   fxy=reshape(fxy,npts,1);
   
   %do least squares to estimate the coefficients
   robj.coef=pts\fxy;
   
   %compute the mse
   denom=fxy;
   ind=find(fxy==0);
   denom(ind)=1;
   nerr=abs(fxy-pts*robj.coef)./denom;
   nrmse=1/npts*sum(nerr,1);
   
   