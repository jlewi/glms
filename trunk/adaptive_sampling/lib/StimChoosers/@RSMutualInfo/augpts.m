%function [pts]=augpts(robj,xy)
%   robj - the RSMutualInfo object
%   xy -nptsx2 array
%       each row is the x,y point at which we want to evaluate the function
% Return value
%   pts - n*(degree+1)^2
%       each row represents the vector which we multiply by the linear
%       coefficient to get the predicted output
% Explanation: This function computes the augmented vectors
%   that is the input point after pushing the input through the appropriate
%   kernel
function pts=augpts(robj,xy)
    if (size(xy,2)~=2)
        error('xy must have 2 columns');
    end
        
   degree=robj.degree;
   npts=size(xy,1);
   pts=zeros(npts,1,(degree+1)^2 );
   %form the augmented points
   xdegree=robj.cdegree(:,1);
   xdegreemat=ones(npts,1)*xdegree';
   
   ydegree=robj.cdegree(:,2);
   ydegreemat=ones(npts,1)*ydegree';
   
   xaug=((xy(:,1)*ones(1,(degree+1)^2)).^xdegreemat);
   yaug=((xy(:,2)*ones(1,(degree+1)^2)).^ydegreemat);
   pts=xaug.*yaug;