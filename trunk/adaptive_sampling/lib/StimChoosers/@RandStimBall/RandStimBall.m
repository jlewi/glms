%function[rstim]= RandStimNorm('mmag',val,'klength', klength)
%       
% Explanation: Object which picks stimuli randomly from a ball of radius
% mmag. Parameters are passed in as 'name', value pairs. The inputs are the
% magnitude to which all stimuli should be normalized and klength is the
% dimensionality of the stimuli.
%
% Revisions:
%   09-19-2009 - convert to new object model
%           mmag and klength are no longer fields of RandStimBall
%           instead we get these from the simulation object passed
%           into choosestim
classdef (ConstructOnLoad=true) RandStimBall <StimChooserObj
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    properties(SetAccess=private, GetAccess=public)

    end

    methods
        function obj=RandStimBall(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
          


               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            bparams.glmspecific=false;
            obj=obj@StimChooserObj(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind

                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end


            %set the version to be a structure
            obj.version.RandStimBall=090119;
        end
    end
end




