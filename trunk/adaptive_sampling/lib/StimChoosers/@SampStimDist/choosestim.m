%function [stim,varargout]=choosestim(obj,simobj,post)
%   obj - the stimchooserobj
%   simobj - the simulation object
%   post the posterior
% Explanation: Select the stimulus by sampling the Gaussian process.
%
function [stim]=choosestim(obj,simobj,post)

mobj=simobj.mobj;
%we need to get past values of the stimuli if they are needed
%we then compute the appropriate Gaussian conditioning formulas. 
if (mobj.ktlength>1)
    tlast=simobj.niter;
    if (simobj.niter-(mobj.ktlength-1)>=0)
        sr=simobj.sr(tlast-(mobj.ktlength-1)+1:tlast);
        spast=getinput(sr);
        spast=getData(spast);
    else
        if (tlast>0)
        sr=simobj.sr(1:tlast);
        spast=getinput(sr); 
        spast=getData(spast);
        else
            spast=[];
        end
    end
    
    %zero padd spast with zeros for any stimuli which are missing
    spast=[zeros(mobj.klength,(mobj.ktlength-1) -size(spast,2)) spast];
    
else
    spast=[];
end
    stim=samppcon(obj.ps,spast);
    



