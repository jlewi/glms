%function stim=choosestim(finf,post,shist,einfo,mparam)
%       finfo - the object
%       post - current posterior
%              an LRMixturePost object
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3 
%Explanation: maximize the mutual information for the canonical poisson
%model
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mobj,varargin)
oreturn=[];

if ~isa(post,'LRMixturePost')
    error('LRMixtureInfoMaxCanon requires post to be LRMixturePost');
end


%extract the submodel associated with the mean
mind=1;
subm=getsubm(post,mind);
submpost=getsubmpost(post,mind);

%call PoissExpMaxMi/choosestim on the submodel
%adjust the magnitude constraint of the submodel
%to ensure the stimulus will end up being normalized
mmag=getmag(subm);

%bmat=formmat(subm,ones(getrank(subm),1));
%bmag=sum(sum(bmat.^2))^.5;

%subm=setmmag(subm,mmag/bmag);

[substim,oreturn,sobj.PoissExpMaxMI]=choosestim(sobj.PoissExpMaxMI,submpost,[],subm);

%now we need to convert this stimulus into the full dim stim
%the stimulus returned by the call to PoissExpMaxMi is the projection of
%the stimulus along the basis.
stimmat=formmat(subm,substim);

xmax=formvec(mobj,stimmat);

