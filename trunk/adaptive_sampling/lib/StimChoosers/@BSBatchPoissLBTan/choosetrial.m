%function [obj, stim, shist, obsrv,stimindex,blogminfo]= choosetrial(obj, simobj,post);
%   obj - A BSOptStim object
%   simobj - a BSSim object
%
%Explanation:
%   stim - vector representing the stimulus
%   shist - vector of the spike history
%   obsrv - the response
%   stimindex=3 x1 vector indicating the trial corresponding to the
%   stimulus
%           = we use this to keep track of the order the stimuli were
%           chosen in
%       stimindex(1,1) - windex of the stimulus
%       stimindex(2,1) - the repeat of the wfile
%       stimindex(3,1) - the relative trial within the
%
%Revisions
%   We construct the appropriate posterior for using the tangent space
%   We then call choose trial from the base class BSBatchPoissLB
%
%   09-22-2008 - Return the mutual information.
function [stim, shist, obsrv,stimindex,blogminfo]= choosetrial(obj, simobj,post)


%get the posterior
pinfo=comppost(obj.bstan,simobj,post);

%choose the stimulus by calling choosetrial on the base class
%pinfo as our posterior.
[stim, shist, obsrv,stimindex,blogminfo]= choosetrial@BSBatchPoissLB(obj, simobj,pinfo);