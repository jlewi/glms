%function sim=BSBatchTanSpaceInfoMax(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Choose on optimal batch of stimuli by placing a lower
%  bound on the informativeness of the batch. Use the posterior on the
%  tangent space
%
classdef (ConstructOnLoad=true) BSBatchPoissLBTan < BSBatchPoissLB
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %bstan     - a BSTanSpaceInfoMax object which we use to store
    %            information about how to processs the tangent space and to
    %            compute the appropriate posterior.
    properties(SetAccess=private, GetAccess=public)
        btversion=080902;
        bstan=[];
    end

    methods
        function obj=BSBatchPoissLBTan(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'bdata','bsize'};
            con(1).cfun=1;

            %define structures to store parameters for the call to the
            %constructor of each superclass
            tanparams=[];
            batchparams=[];

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=[];
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing

                case 1

                    batchparams.bdata=params.bdata;
                    batchparams.bsize=params.bsize;
                    
                    params=rmfield(params,{'bdata','bsize'});
                    
                    %process optional arguments
                    fnames=fieldnames(params);
                    for field=fnames'
                        f=field{1};
                       switch lower(f)
                           %parameters for the tan space object
                           case {'iterfullmax','startfullmax'}
                               tanparams.(f)=params.(f);
                           %parameters for both objects
                           case {'bdata','windexes'}                           
                               tanparams.(f)=params.(f);
                               batchparams.(f)=params.(f);
                           otherwise
                               batchparams.(f)=params.(f);
                       end
                    end
                otherwise
                    error('Constructor not implemented')
            end

            
            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@BSBatchPoissLB(batchparams);
            
            %construct the bstan object
            tanparams.miobj=obj.miobj;
            obj.bstan=BSTanSpaceInfoMax(tanparams);

        end
    end
end


