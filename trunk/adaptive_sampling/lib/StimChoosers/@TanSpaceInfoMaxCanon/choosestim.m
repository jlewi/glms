%function stim=choosestim(sobj,simobj,trial)
%       sobj - the stimulus object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3 
%Explanation: maximize the mutual information for the canonical poisson
%model
%
%Revision:
%   3-11-2008 - compute the mean and variance for the optimization the new
%   way
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mparam,varargin)
oreturn=[];

if (strcmp(varargin{1},'trial')==1)
    trial=varargin{2};
else
    error('Trial was not passed in.');
end

%check whether we are doing infomax on the full posterior or tangent space
if ((isinf(sobj.iterfullmax) || mod(trial,sobj.iterfullmax+1)~=0) && (trial>sobj.startfullmax))
%3-11-2008
%we need to create the appropriate mean and covariance matrix for use with
%our method
fpost=getfullpost(post);
tanpost=gettanpost(post);
tspace=gettanspace(post);
basis=getbasis(tspace);

minfo=getm(fpost)+getbasis(tspace)*getm(tanpost);
cinfo=basis*getc(tanpost)*basis';
%compute eigendecomp of cinfo because we will need it for the stimulus
%optimization.
pinfo=GaussPost('m',minfo,'c',EigObj('matrix',cinfo));

else
    
    %info. max on full space set posterior to full posterior
    pinfo=getfullpost(post);
end

%choose the stimulus by calling choosestim on the base class using 
%pinfo as our posterior.
[xmax,oreturn,sobj.PoissExpMaxMI,varargout]=choosestim(sobj.PoissExpMaxMI,pinfo,shist,mparam,varargin);