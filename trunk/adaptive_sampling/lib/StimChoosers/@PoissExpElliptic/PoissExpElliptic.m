%function sim=PoissExpElliptic(fieldname, value,)
%   fieldname, value - pairs of fields and values used to initialize the object 
%
% Explanation: This class tries to enforce elliptic symetry about the
% estimated MAP when choosing optimal stimuli for the canonical poisson
% model. It does this by maximizing the stimuli using the PoissExpMaxMI
% object to first choose the optimal stimulus. It then randomly flips the
% signs of the projections along eigenvectors orthogonal to the MAP. Since
% the eigenvectors are never perfectly orthogonal to the MAP this can
% introduce some inefficiency into the choice of optimal stimulus
%
%Base Clase: PoissExpMaxMI

function obj=PoissExpElliptic(varargin)

%**************************************************************
%Required parameters
%*****************************************************************
%if fielname is required then set a field of req.fieldname=0
%when we read in that parameter we set req.fieldname=1
%req.fieldname1=0;
%req.fieldname2=0;
req=[];
%**********************************************************
%Define Members of object
%**************************************************************
% ellipmeth - method for trying to enforce ellipticity
%                   0 - randomly flip signs of projections orthogonal to
%                   the mean. 
%                   1 - randomly flip the signs of projections on all but
%                   the smallest eigenvector. 
% var2          -description

%declare the structure
obj=struct('ellipmeth',0);


%create the optimzation structure to specify the options for calls to
%optimization functions
%use default values 
%simparam.optparam=optimset();

%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    pbase=PoissExpMaxMI();
    obj=class(obj,'PoissExpElliptic',pbase);
    return
end

%**************************************************************************
%Parse the input arguments
%create a new simulation
for j=1:2:nargin
    switch varargin{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case 'ellipmeth'
            obj.(varargin{j})=varargin{j+1};
            %****************************************
            %special processing
            %*****************************************
            %check if its required
            if (obj.ellipmeth<0 || obj.ellipmeth>1)
                error('ellipmeth must be 0 or 1');
            end
        otherwise
            %*********************************
            %generic handler: set any fields of the structure which are
            %passed in. 
            %Remove this if you don't want user to be able to set any field
            %of the obj structure 
            %*****************************
            if isfield(obj,varargin{j})
                obj.(varargin{j})=varargin{j+1};
                    %check if its required
            if isfield(req,varargin{j})
                req.(varargin{j})=1;
            end
            else          
            error('Constructor:inputarg','%s unrecognized parameter ',varargin{j});
            end
    end
end

%************************************************************
%Check if all required parameters were supplied
if ~isempty(req)
        freq=fieldnames(req);
        for k=1:length(freq)
            if (req.(freq{k})==0)
                error('Constructor:missing_arg','Required parameter %s not specified',freq{k});
            end
        end
end

%**********************************************************************

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=PoissExpMaxMI();
obj=class(obj,'PoissExpElliptic',pbase);


    