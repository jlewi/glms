%function stim=choosestim(finf,post,shist,einfo,mparam)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%       glm   - structure specifying the glm
%                
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3 
%Explanation: maximize the mutual information for the canonical poisson
%model
%
% GLM MUST BE THE CANONICAL
%                   POISSON
function [xoptsflip,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mparam,varargin)
oreturn=[];

%call the choose stim method on the base class PoissExpMaxMi
 [xopt,oreturn,sbase,qmod]=choosestim(sobj.PoissExpMaxMI,post,shist,mparam,varargin);


%**************************************************************************
%1-30-2007
%*************************************************************************
%we can choose arbitrary sign for the projections of xopt along the
%eigenvectors orthogonal to the mean.
%
% If the model is misspecified the mean will not be parallel to one of the
% eigenvectors. Lets still try to enforce the distribution of the stimuli
%in the space orthogonal the MAP.
%
if (sobj.ellipmeth==0)
% We take the eigendecomposition of the modied quadratic form
% One of its eigenvectors is parallel to the mean and has eigenvalue zero. 
% We randomly flip the sign of all projections orthogonal to this vector.
%
%we need the eigendecomposition od fhte modified quadratic form
%not of the original covariance matrix
[eigmod]=geteigmodsorted(qmod);

%1. project mean out eigenvectors
y=eigmod.evecs'*xopt;
%randomly flip the signs of all elements not perpindicualr to the mean
sflip=sign(binornd(1,.5,length(y),1)-.5); 
%make sure we don't flip projection along mean
%eigenvector parallel to mean should have eigenvalue 0 which should be minimum.
[mval mind]=mineval(eigmod);
if (mval~=0)
    error('Smallest eigenvector is not parallel to the MAP');
end
sflip(mind)=1;
y=sflip.*y;
xoptsflip=eigmod.evecs*y;
else
    %flip sign of projections along all but the smallest eigenvector
    %so \sigmasq stays the same but \mu_glmproj changes
    if (mparam.alength>0)
        error('Not sure what we should do if we have spike history or fixed terms');
    end
    if isempty(einfo)
        [einfo.evecs, einfo.eigd]=svd(post.c(1:mparam.klength,1:mparam.klength));
        einfo.esorted=-1;
        einfo.eigd=diag(einfo.eigd);
    end
    y=einfo.evecs'*xopt;
   sflip=sign(binornd(1,.5,length(y),1)-.5); 
    [mval mind]=mineval(einfo);
sflip(mind)=1;
y=sflip.*y;
xoptsflip=einfo.evecs*y;

end

%This stimulus will have a different value of sigmasq
%if the MAP is not parallel to one of the eigenvectors of Ck
%therefore the following error checks would fail as expected

%error checking
%make sure sign flipping didn't affect results
%if (abs(xoptsflip'*post.m-xopt'*post.m)>10^-12)
%    error('sign flipping messed up optimal stimulus');
%end

%if (abs(xoptsflip'*post.c*xoptsflip-xopt'*post.c*xopt)>10^-12)
%    error('sign flipping messed up optimal stimulus');
%end
%xopt=xoptsflip;