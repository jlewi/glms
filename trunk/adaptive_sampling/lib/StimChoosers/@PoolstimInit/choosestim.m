%function stim=choosestim(sobj,post,shist,einfo,mparam,varargin)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
% Return value
%   xmax -
%   oreturn -
%       isinit= true/false - indicates whether stimulus was selected from
%       the initialization pool or not
%   sobj - the stimulus object
%   stimpool - information about the stimulus pool
%            - only returned if number of arguments is >3
%       .muproj
%       .sigma
%Explanation: choose the stimulus
%
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mparam,varargin)
oreturn=[];

%if the initpool hasn't been exhausted select the stimulus from there
if ~isempty(sobj.initpool)
    xmax=sobj.initpool(1);
    %remove the stimulus from the pool
    sobj.initpool=sobj.initpool(2:end);
    oreturn.isinit=true;
else
   %call choose stim on base class
   [xmax,oreturn,sobj.Poolstim]=choosestim(sobj.Poolstim,post,shist,mparam,varargin);
   oreturn.isinit=false;
   
end