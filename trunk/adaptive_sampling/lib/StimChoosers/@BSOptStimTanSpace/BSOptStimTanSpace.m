%function sim=BSOptStim('bdata','miobj'
%   datafile
%   miobj  - the object which computes the mutual information as a function
%           of mu and sigma
%
%Optional Parameters:
%   windexes - wave files to use
%
%Explanation: A class to choose the optimal stimulus from the bird song
%data using the tangent sapce
function obj=BSOptStimTanSpace(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'bdata','miobj'};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%
%miobj      - object which computes the mutual information as a function of
%              mu and sigma
%
%paststim  - structure to keep track of stimuli we already presented
%           This ensures we don't pick the same datapoint twice
%          this is an array of cell arrays
%       paststim(nwavefile)- this is a dynarray
%           indicating which trials from this wave file
%           have already been presented. See optstiminwav
%   
% mpool - method for selecting the pool of stimuli
%         -a pointer to a function
%declare the structure
obj=struct('version',080604,'iterfullmax',inf,'startfullmax',0);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'BSOptStimTanSpace',BSOptStim());
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

if ~isfield(params,'windexes')
    params.windexes=[];
end
%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        
        %data=load(getpath(params.datafile));
  
    otherwise
        error('Constructor not implemented')
end
    
pbase=BSOptStim('bdata',params.bdata,'windexes',params.windexes,'miobj',PoissExpCompMI(),'poolmethod','@heurmumax');  
%if no base object
obj=class(obj,'BSOptStimTanSpace',pbase);




    