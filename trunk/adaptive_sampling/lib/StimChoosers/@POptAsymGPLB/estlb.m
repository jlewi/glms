%function lbdirtheta(obj,gp,dirtheta,mobj,nsamps)
%   obj - the POptAsymGPLB object
%   gp - the parameters of our gaussian process on the inputs
%      .mu= mean of each stimulus
%      .c = 1x(tk+1) cell array of the correlation matrices
%           .c(i)=v(|t_j-t_l|=i-1);
%   post - Posterior on theta
%   nsamps -how many samples of the direction of theta to use.
%
%explanation:
%   This function estimates the lower bound by using monte carlo
%   integration to estimate the expectation over the direction of theta
%
%   To draw samples of the direction we just sample our posterior on theta
%   and then normalie the magnitudes
%
%   To increase efficiency we'll probably want to reuse samples on
%   subsequent trials
function lb=estlb(obj,gp,post,mobj,nsamps)

lb=0;
%sample the directions of theta
theta=mvgausssamp(getm(post),getc(post),nsamps);

dirtheta=normmag(theta);

for ind=1:nsamps

    lbval=lbdirtheta(obj,gp,dirtheta(:,ind),post,mobj);
    lb=lb+lbval/nsamps;
end