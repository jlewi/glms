%function lbdirtheta(obj,gp,dirtheta)
%   obj - the POptAsymGPLB object
%   gp - the parameters of our gaussian process on the inputs
%      .mu= mean of each stimulus
%      .c = 1x(tk+1) cell array of the correlation matrices
%           .c(i)=v(|t_j-t_l|=i-1);
%
%   dirtheta - a unit vector indictating the direction in which we compute
%   the lower bound
%   post - Posterior on theta 
%   mobj - the model object
%explanation:
%   This function evaluates the integrals in direction dirtheta.
function lbval=lbdirtheta(obj,gp,dirtheta,post,mobj)

lbval=0;

dirtheta=normmag(dirtheta);

%compute the rotation matrix
rmat=[dirtheta null(dirtheta')];

%*************************************************************************
%Construct p(s) from the Gaussian process
%*************************************************************************
%construct the mean and variance of the matrix p(s)
%using the gaussian parameters
gp.mus=repmat(gp.mu, mobj.ktlength,1);

%compute the covariance matrix
gp.cs=zeros(mobj.klength*mobj.ktlength,mobj.klength*mobj.ktlength);

for t1=1:mobj.ktlength
    for t2=t1:mobj.ktlength
        rstart=(t1-1)*mobj.klength+1;
        rend=rstart+mobj.klength-1;
        cstart=(t2-1)*mobj.klength+1;
        cend=cstart+mobj.klength-1;
        
        gp.cs(rstart:rend,cstart:cend)=gp.c{t2-t1+1};
    end
end

gp.cs=triu(gp.cs)+ triu(gp.cs,1)';

%*******************************************************************
%compute the mean and variance of the Gaussian distribution on the first
%component of the rotated input

prinp1.mu=rmat(:,1)'*gp.mus;
prinp1.c=rmat(:,1)'*gp.cs*rmat(:,1);

%compute the "mean","variance" for the unnormalized Gaussian
%distribution on the magnitude of theta. Also compute the scaling constant
rtmu=getm(post)'*getinvc(post)*rmat(:,1);

ptmag.c=(rmat(:,1)'*getinvc(post)*rmat(:,1));
ptmag.mu=rtmu/ptmag.c;


ptmag.sconst=1/(2*pi)^(obj.dimtheta/2)*1/det(getc(post))^.5;
ptmag.sconst=ptmag.sconst*exp(-1/2*1/ptmag.c*(-(rtmu)^2/ptmag.c^2+getm(post)'*getinvc(post)*getm(post)));

%compute the bounds for integration
%these bounds are computed assuming distribution on ptmag is Gaussian with
%mean mu, and variance c. In reality, the probability on ptmag should decay
%much faster so these bounds should be better than confint
tmag.min=ptmag.mu-obj.nsigma*ptmag.c^.5;
tmag.max=ptmag.mu+obj.nsigma*ptmag.c^.5;

rinp1.min=prinp1.mu-obj.nsigma*prinp1.c^.5;
rinp1.max=prinp1.mu+obj.nsigma*prinp1.c^.5;



%compute the first term
func=@(tmag,rinp1)(int2dfun(tmag,rinp1,obj.dimtheta,ptmag,prinp1,mobj.glm));
t1 = dblquad(func,tmag.min,tmag.max,rinp1.min,rinp1.max);


%************************************************************
%Compute the second term
%E_rinp1 log | E_rinp2...rinpd|inp1 inp inp'|
%**************************************************************
func=@(rinp1)(intlogdet(obj,rinp1,prinp1,obj.dimtheta,rmat,gp));
t2=quad(func,rinp1.min,rinp1.max);



lbval=t1+t2;
%function evaluate the probablity of the magnitude of theta
%given this direction
function p=probtmag(tmag,ptmag)
p=ptmag.sconst*exp(-1/2*1/ptmag.c*(tmag-ptmag.mu)^2);

%this is the function we need to integrate to compute the first term
%we integrate over the magnitude of theta and the first component of the
%rotatied input
function v=int2dfun(tmag,rinp1,dimtheta,ptmag,prinp1,glm)

%probability of the magnitudes
probtmag=ptmag.sconst*exp(-1/2*1/ptmag.c*(tmag-ptmag.mu).^2);

%probability of rinp1
probrinp1=normpdf(rinp1, prinp1.mu,prinp1.c^.5);

%the expected fisher information
je=jexp(glm,tmag.*rinp1);

v=dimtheta*probtmag.*probrinp1.*log(je);



%****************************************************************
%Functions for the 2nd term
%****************************************************************
%Function we integrate to compute the expected value of log |E ss'|
function f=intlogdet(obj,rinp1,prinp1,dimtheta,rmat,gp)

f=zeros(1,length(rinp1));

for index=1:length(rinp1)
   m=compmoments(obj,rinp1(index),dimtheta,rmat,gp);
   
   dss=subs(obj.dfuncsym,obj.symvars,m);
   
   f(index)=normpdf(rinp1(index),prinp1.mu,prinp1.c^.5)*log(dss);
end


%This function computes the values of obj.symvars as a function of rinp1
function m=compmoments(obj,rinp1,dimtheta, rmat,gp)
m= cell(1,length(obj.symvars));

%first dimtheta elements should be first moments
m{1}=rinp1;

rmu=rmat'*gp.mus;
for col=2:dimtheta
    m{col}=rmu(obj.symind(1,col));
end

%correlation momements
rc=rmat'*gp.cs*rmat;

a=rmat(1);
A=rc(1,1);
for col=dimtheta+1:length(obj.symvars)
    i=obj.symind(1,col);
    j=obj.symind(2,col);
    

    b=[rc(i);rc(j)];
    B=[rc(i,i) rc(i,j); rc(j,i) rc(j,j)];
    C=[rc(1,i) rc(1,j)];

    Ainv=inv(A);
    v=(b+C'*Ainv*(rinp1-a));
    
    cmat=B-C'*Ainv*C;
    m{col}=cmat(1,2)+v(1)*v(2);
end


