%function sim=POptAsymGPLB('dimtheta')
%   dimtheta - dimensionality of theta
%
% Explanation: This stimulus chooser computes a Gaussian Process which
% maximizes a lower bound of the asymptotic informative. 
%
%
%Revisions:
%   11-03-2008: use Constructor.id
classdef (ConstructOnLoad=true) POptAsymGPLB < StimChooserObj
    %**********************************************************
    %Define Members of object
    %**************************************************************

    properties(SetAccess=private, GetAccess=public)
       %dimensionality of theta. This is used to make sure 
        dimtheta=[];

        %what confidence interval to use for computing the bounds of
        %integration over the magnitude of theta and the first stimulus
        %component
        confint=.999;
    end

    properties(SetAccess=private,GetAccess=public,Transient=true)
      
     
    end
    
    properties(SetAccess=private,GetAccess=public,Transient=true)
       %how many standard deviations of the mean we need to get the desired confidence interval 
       nsigma=[];
    end
    methods
        function nsigma=get.nsigma(obj)
           if isempty(obj.nsigma)
               tails=(1-obj.confint)/2;
               nsigma=norminv(1-tails,0,1);
               obj.nsigma=nsigma;
           end
           nsigma=obj.nsigma;
        end
 
        
        function obj=POptAsymGPLB(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'dimtheta'};


            error('12-31-2008. This class is useless. The lower bound ends up being - infinity.');
               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=rmfield(params,'dimtheta');
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@StimChooserObj(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     obj.dimtheta=params.dimtheta;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end

            
            obj.version.POptAsymGPLB=081226;
        end
    end
end


