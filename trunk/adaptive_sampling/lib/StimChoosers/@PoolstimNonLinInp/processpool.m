%function [pobj]=processpool(pobj,type,rawpool)
%       type - 'file' or 'matrix'
%               -specifies whether rawpool is a file containing the
%                   matrix of data or the actual matrix of stimuli
%       rawpool - matrix or name of file
%       pobj.Poolstim.srescale - parameters for rescaling the stimuli
%
% Explanation:
%   Functions as follows
%       1. load the actual stimulus and select a subset if required
%       2. selecting a subset of the stimuli in the pool or matrix
%       3. rescale the stimuli
%       4. project the stimuli into the higher d space
%
%This function handles all processing
%   of the raw stimulus pool to get the actual stimulus pool
%   processing entails three steps
%   1.
%   2. rescaling the stimuli
function [pobj]=processpool(pobj,type,rawpool)

%set stimloaded to true
pobj=setstimloaded(pobj,true);

%***************************************************
%create a matrix of rawpool stimuli
switch lower(type)
    case {'file','fname','filename'}
        %load the pool from a file
%        fname=rawpool;
        if isa(getpoolfile(pobj),'FilePath')
            fname=getpath(getpoolfile(pobj));
        else
            fname=getpoolfile(pobj);
        end
        
        try
            rawpool=load(fname,'stim');
            rawpool=rawpool.stim;              
        catch
            error('Could not load variable stim in file %s',fname);
        end
        poolfile.fname=getpoolfile(pobj);

        try

            fileid=load(fname,'fileid');
            poolfile.fileid=fileid.fileid;
        catch
            warning('File containing stimuli did not have a fileid. Appending one now');
            rand('state', sum(100*clock));
            fileid=rand(1)+floor(rand(1)*10);
            poolfile.fileid=fileid;
            save(fname,'fileid','-append');
        end
        %update the poolfile info
         pobj.Poolstim=set(pobj.Poolstim,'poolfile',poolfile);
    case {'matrix'}
        %do nothing
    otherwise
        error('unrecognized value of type: %s',type);
end


%************************************************************************
%Create the appropriate subsample of the rawpool
%***********************************************************************
%we keep the entire pool if we are doing one of the following
%   i) number of stimuli = size of pool
%       -turn resampling off if this is the case
%   ii) resampling is turned on
%       -we need to keep the entire pool so we can redraw stimuli on each
%       trial
%
nraw=size(rawpool,2);
poolfile=pobj.Poolstim.poolfile;
poolfile.nraw=nraw;
pobj.Poolstim=set(pobj.Poolstim,'poolfile',poolfile);

%if nstim is empty or infinity we use entire pool
if isempty(pobj.Poolstim.nstim)
    pobj.Poolstim=set(pobj.Poolstim,'nstim',nraw);
end

if isinf(pobj.Poolstim.nstim)
    pobj.Poolstim=set(pobj.Poolstim,'nstim',nraw);
end

%error checking
if (nraw<pobj.Poolstim.nstim)
    error('Number of stimuli in pool is less than nstim=%d \n',pobj.Poolstim.nstim);
end

%only trucate the pool if we are not resampling
if ((pobj.Poolstim.resample)||(nraw==pobj.Poolstim.nstim))
    if (pobj.Poolstim.resample)
        (params.nstim==size(rawppol,2))
        fprintf('Turning resampling off because #of stimuli = size of stimulus pool\n');
        pobj.Poolstim=set(pobj.Poolstim,'resample',false);
    end
    %store the stimuli
    pobj.stimpool=rawpool;
else
    fprintf('Selecting first %d stimuli in pool \n',pobj.Poolstim.nstim);
    pobj.stimpool=rawpool(:,1:pobj.Poolstim.nstim);
end

%*********************************************************************
%Process the stimuli
%************************************************************

%rescale the stimuli if told to do so
if ~isempty(pobj.Poolstim.srescale)
    %  pobj.Poolstim.srescale=params.stimrescale;
    if (length(fieldnames(pobj.Poolstim.srescale))>1)

        niter=0;
        maxiter=100;
        totfactor=1;
        expr=compexpr(mobj,obj.Poolstim.stimpool,pobj.Poolstim.srescale.theta);
        [maxrate sind]=max(expr);
        while ((niter<maxiter) & (maxrate>pobj.Poolstim.srescale.maxrate))
            %compute the expected firing rates for all stimuli in the pool
            %scale the inputs so that the maxrate for this input is 1/2 the
            %maxrate
            optim=optimset('TolX',10^-12,'TolF',10^-12);
            fsetstimscale=@(s)(compexpr(mobj,   s*pobj.stimpool(:,sind),pobj.Poolstim.srescale.theta)-pobj.Poolstim.srescale.maxrate/2);
            sfactor=fsolve(fsetstimscale,.5,optim);

            pobj.stimpool=sfactor*pobj.stimpool;
            niter=niter+1;
            expr=compexpr(mobj,pobj.stimpool,pobj.Poolstim.srescale.theta);
            [maxrate sind]=max(expr);

            totfactor=totfactor*sfactor; %cumulative scaling facotr
        end
        actualrate=compexpr(mobj,   s*pobj.Poolstim.stimpool(:,sind),pobj.Poolstim.srescale.theta);
        fprintf('stimuli adjusted so that maxrate is %d \n', actualrate);
        sfactor=totfactor;
    else
        %pobj.Poolstim.srescale contains the scale factor for the stimuli
        %we store the actual stimuli in the field of this object not the
        %base classe
        sfactor=pobj.Poolstim.srescale.sfactor;
        pobj.stimpool=sfactor*pobj.stimpool;
    end

end

%5-30-2007
%We need to transform the stimuli after we rescale them.
%transform the stimuli
%we can only transform the stimuli if the transformation is independent of
%the spike history terms
%we only need to do this if stimuli aren't random
if (pobj.Poolstim.infomax~=0)
    %make sure nonlinearity doesn't act on the spike history
    if (pobj.onshist==0)
        pobj.Poolstim=set(pobj.Poolstim,'stimpool',projinp(pobj.ninpobj, pobj.stimpool));
    end
end