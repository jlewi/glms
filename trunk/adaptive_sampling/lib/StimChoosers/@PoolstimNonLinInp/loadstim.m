%function sobj=loadstim(sobj)
%
%Explanation: load the stimuli if not already loaded
function sobj=loadstim(sobj)

if ~(getstimloaded(sobj))
    fprintf('PoolstimNonLinInp: Loading stimpool from file \n');
    
    %check file hasn't changed
    checkfileid(sobj);
    
    [sobj]=processpool(sobj,'file',getpoolfile(sobj));
    %sobj.Poolstim=set(sobj.Poolstim,'stimloaded',true);
    sobj=setstimloaded(sobj,true);
end