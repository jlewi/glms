%function stim=choosestim(sobj,post,shist,einfo,mparam,simobj)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%       simobj - simulation object 
%              - need this to get spike history and stimulus history if
%              needed
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   stimpool - information about the stimulus pool
%            - only returned if number of arguments is >3
%       .muproj
%       .sigma
%Explanation: maximize the mutual information for the canonical poisson
%model
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mobj,simobj)
oreturn=[];

sobj.Poolstim=loadstim(sobj.Poolstim);

bsize=getnstimuli(sobj);
klength=getklength(mobj);
ktlength=getktlength(mobj);
psize=getpoolsize(sobj);

%how many batches to construct
nbatch=floor(psize/bsize);

%number of trials already conducted
ptrials=getniter(simobj);


%I could speed this up for i.i.d. by only doing this for a single stimulus

%get the mean for each spatial filter
mu=reshape(post.m(1:klength*ktlength),klength,ktlength);

%create an orthonormal basis for mu
bmu=orth(mu);

%randomly generate the projections along bmu by sampling 
%the uniform ball
bmuproj=uballrnd(sobj.magcon,klength,psize);

%adjust the stimuli in the pool
%make each stimulus orthogonal to bmu
%by removing projection along bmu
%its possible this will result in zero vectors because 
%its possible that mu spans the entire stimulus space. 
%or just the space spanned by the vector
stimpool=getData(getstimpool(sobj));
stimpool=stimpool-bmu*(bmu'*stimpool);

%normalize them so they are just unit vectors
%only do this for vectors which aren't zero vector
poolenergy=sum(stimpool.^2,1);
ind=find(poolenergy>10^-6);
stimpool(:,ind)=stimpool(:,ind).*(ones(klength,1)*[poolenergy(ind).^-.5]);

%2. scale the stimuli so that all energy not in bmu is in space orthogonal
%to bmu
energy=sobj.magcon^2-sum(bmuproj(:,ind).^2,1);
stimpool(:,ind)=stimpool(:,ind).*(ones(klength,1)*[energy.^.5]);

%if the stimulus orthogonal to bmu is zero then we scale bmuproj
%to place all energy in it
ind=find(poolenergy<=10^-6);
energy=sobj.magcon^2./sum(bmuproj(:,ind).^2,1);
stimpool(:,ind)=zeros(klength,length(ind));
bmuproj(:,ind)=bmuproj(:,ind).*(ones(klength,1)*[energy.^.5]);



%normalize
%3. add in projection along the mean
stimpool=stimpool+bmu*bmuproj;

if (getdebug(sobj))
    %check magnitudes sum to magcon
    mag=sum(stimpool.^2,1).^.5;
    if ~isempty(find(abs((mag-sobj.magcon))>10^-8))
        error('Magnitudes not properly normalized');
    end
end
%*****************************************************************
% 2 possibilities
%1) select stimuli randomly from the pool
%2) select stimuli from pool which maximizes the mutual info
%******************************************************************
if (getinfomax(sobj)==0)
    %randomly select stimulus
    %keyboard
    %select bsize random stimului
    sind=ceil(rand(1,bsize)*nstim);
    xmax=stimpool(:,sind);
    oreturn.sind=sind;
else
    if isempty(shist)
        stim=stimpool;
    else
        stim=[stimpool;shist*ones(1,nstim)];
    end
    stimind=[];

    %*********************************************************
    %group the stimuli in the stim pool into batches
    %for each batch compute muproj and sigma
    %and then compute the mi
    %
    %muproj=zeros(bsize,nbatch);
    %sigma=zeros(bsize,bsize,nbatch);
    %
    %mi=zeros(1,nbatch)
    x=zeros(klength,bsize+(ktlength-1));
    
    %get past stimuli set to zero if they aren't any
    if (ptrials<(ktlength-1))
        x(:,bsize+1:bsize+ptrials)=getData(getstim(simobj,ptrials:-1:1));
    else
    x(:,bsize+1:end)=getData(getstim(simobj,ptrials:-1:ptrials-ktlength+1));
    end
    for bind=1:nbatch
    %matrix of stimuli
    %include previous stimuli because of spatio-temporal dependence
    %we reverse the temporal oder of the stimuli i.e time decreases across
    %columns so that we when we use reshape last stimulus is first
    
    x(:,1:bsize)=[getdata(stim(bind*nbatch:-1:(bind-1)*nbatch+1))];
    
    %create the matrix of stimuli
    %i'th row is stimulus at time t+i - including relevent stimulus history
    xbar=zeros(bsize,ktlength*klength);
    
    for t=1:bsize
       xbar(t,:)=reshape(x(:,bsize-t+1:bsize+ktlength-t),1,ktlength*klength); 
    end
    
    
 
    muproj=xbar*post.m;
    
    sigma=xbar*post.c*xbar';
    
    
  
    mi=compmi(getmiobj(sobj),muproj,sigma);
    end
    %maximize the mi

    [maxmi indmax]=max(mi);

    poolind=indmax;

    %we order the returned stimuli so that time increases across columns
    xmax=stimpool(:,(bind-1)*nbatch+1:bind*nbatch(bind-1)*nbatch+1);
    oreturn.sind=poolind;

end

%if nargout is > 3 return parameters related to the pool
if (nargout>3)
    if (sobj.Poolstim.infomax)
        varargout{1}.muproj= muproj;
        varargout{1}.sigma=sigma;
    end
end
