%function sim=BSOptStim('bdata')
%   bsize - size of the batch
%
%
%
%Optional Parameters:
%   windexes - wave files to use. Only trials from these wave files will be
%             selected
%
%Explanation: A class to choose the optimal stimulus from the bird song
%data. This object does not use any heuristics for selecting a subset of
%the stimulus pool. Instead it computes the mutual information for all
%possible stimuli.
%
%We also save the mutual information for each stimulus in the pool to a
%file
%
%This class assumes we are dealing with the canonical Poisson
%This object assumes we are working 
%Revisions:
%   10-19-2008
%          - make it a subclass of BSBatch Base instead of BSStimChooser
%          - fields windexes, nrepeats, bsize, binfo, are not defined as
%          part of BSBatchBase
%
%   080828 - Initiliaze bindex to nan to indicate its not set
classdef (ConstructOnLoad=true) BSBatchShuffle < BSBatchBase



    %bspost - a BSLogPOst object
    %
    %windexes - which wave files to choose the stimuli from
    %           by excluding some wave files you can create a test set
    %
    %how many   repeats of each wave file should we include
    %           - its pointless to include more than 1 repeat because only
    %             the spike history would change and in a normal experiemnt
    %             we would treat that as fixed anyway
    %bsize      - how big of a batch to choose
    %binfo      - keep track of the batch we are presenting
    %             choosetrial only returns a single stimulus
    %             The class BSBatchPoissLB takes care of presenting all
    %             stimuli in the batch and then selecting the appropriate
    %             optimal batch for the next bsize trials. 
    %           .trial - keep track of how many trials we have presented
    %                  - this way we can make sure we are properly aligned
    %                     with the trial in simobj
    %
    %           .bstart - a structure array denoting the start of the batch
    %               .windex - the index of the wave file
    %               .repeat - the repeat of the wave file
    %               .trial - the relative trial within this wavefile
    %                       for the first stimulus in the batch
    %           
    %           .bindex - The number of stimuli in the batch which have
    %                    already been presented
    %                   -thus when bindex>=bsize we need to pick a new
    %                   batch
    properties(SetAccess=private,GetAccess=public)
        version=081019;   
        bspost=[];
    end

  
    methods

        function obj=BSBatchShuffle(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            %The blank constructor will be used by the base class when
            %loading an object
            con(1).rparams={'bdata'};
            
            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);
           
            
             %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=params;
                    
                case {Constructor.nomatch}
                   %handled by base class
                  error('no match for constructor');
                case 1
                    %all parameters are for base class
                    bparams=rmfield(params,'bdata');
                otherwise
                    
            end
            %try calling constructor for base class
            obj=obj@BSBatchBase(bparams);

            switch cind
                case 1
                    obj.bspost=BSlogpost('bdata',params.bdata,'windexes',obj.windexes','maxrepeats',obj.nrepeats);
            end

            
           
            



        end
        %Return Value:
        %     ntrials - the number of trials we have data for
        %     Note we actually have fewer trials because we pick
        %     the data in batches and this means some trials get thrown
        %     out.
        %
        function ntrials=getntrials(obj)
            %we need to compute the number of trials in the allowed indexes
            nwindex=length(obj.windexes);

            ntrials=0;
            for wind=1:nwindex

                [ntrialsinwave]=getntrialsinwave(obj.bdata,obj.windexes(wind));
                ntrials=ntrials+ntrialsinwave*obj.nrepeats;
            end

        end
        
        %get the bdata object stored in bpost
        function bdata=getbdata(obj)
            if ~isempty(obj.bspost)
           bdata=obj.bspost.bdata; 
            else
                bdata=[];
            end
        end
        
           %a function which chooses the batch of stimuli when
        %necessary.
        [bstart, blogminfo]=choosebatch(obj,simobj,post);
      
    end
end



