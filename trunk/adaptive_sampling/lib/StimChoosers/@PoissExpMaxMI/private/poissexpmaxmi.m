%function [xopt,muprojpt,lfopt,timing,fcase]=fishermaxshist(post,shist,eigck,mparam)
%   post - current posterior
%   shist - spike history terms
%           -  needed in order to maximize the fisher info
%   
%   mparam - model parameters following values are needed
%           .alength
%           .klength
%           .mmag
%Explanation: Chooses the stimulus which maximizes the expected fisher information
%  xopt - optimized stimulus
%  fopt - fisher info at optimal stimulus
%
%Return value
%   lfopt - log of the fisher information at optimal stimulus
%               (value depends on objfunc which may or may not return the log of the fisher info)
%   timing
%       .tquadmod - time to modify the quadratic form
%       .fishermax-time to compute to find the maximum of the fisher
%       information
%   fcase
%   quad - modified quadratic form
%fishermaxshist is outdated
%Following issues have to be resolved
%       1. making sure the eigenvalues are properly sorted before doing
%       anything
%
%Modifications:
%   12-10-2007: eigendecomposition of Ck is now stored as part of the post
%   object as opposed to a separate object
function [xopt,muprojopt,lfopt,timing,fcase,quad]=poissexpmaxmi(sobj,post,shist,mparam)


%two different ways to compute the optimium (see below)
%0) vary the projection along mu
%1) vary the lagrange multiplier and compute mu
switch sobj.searchmeth
    case 'varymu'
        optmethod=0;            %0= vary the projection along mu
    case 'varylagrange'
        optmethod=1;
    otherwise
        error('Invalid method specified must be varymu or varylagrange');
end

%set murange to [] so we will search entire range
murange=[];
if (~isempty(murange) && optmethod==1)
    warning('Range of mu values passed in. But optimizatio method is varylagrange. murange has no effect');
end

objfunc=@finfopoissexp;

%eigck  - eigendecomposition of
%   post.c(1:mparam.klength,1:mparam:klength)
%           .eigd
%           .evecs
%these should be stored as part of the posterior
if (getshistlen(mparam)==0)
    %we want eigendecomposition of full matrix
    eigck=getefull(post);
else
    error('Code for spike history terms needs to be updated');
    %we need eigendecomp of 
%   post.c(1:mparam.klength,1:mparam:klength)
    %which should be stored in esub.
    eigck=getesub(post);
end
if isempty(eigck)
    %declare a persistent variable to keep track of how often this warning
    %is printed
    persistent eigwarn;
    if isempty(eigwarn)
        eigwarn=1;
    end
    eigwarn=eigwarn+1;
   % if (mod(eigwarn,25)==0)
        warning('Inefficiency:rank1','poissexpmaxmi: Eigendecomp not supplied Computing SVD. Further warnings suppressed \n');
    %end


    c=getc(post);
    klength=getklength(mparam);
    %[eigck.evecs eigck.eigd]=svd(c(1:klength,1:klength));
    %eigck.eigd=diag(eigck.eigd);
    %eigck.esorted=-1;
    eigck=EigObj('matrix',c(1:klength,1:klength));
end
n=getdim(post);   %dimensionality
klength=getklength(mparam);
mpost=getm(post);
%**********************************************
%unit vector in direction of mean of just stimulus components
%need its magnitude as well
muk=mpost(1:klength,1);
mukmag=(muk'*muk)^.5;
%if mukmag happens to be 0 (possible due to intialization)
%then pick a vector in direction [1;...]
%correct solution would to pick stimulus in direction of max eigenvalue
%(but I think we still get that result)
%but I don't want to mess with the code. a mean of 0 should be an anomly
if (mukmag==0)
    warning('Magnitude of muk is zero \n');
    muk=1/(klength^.5)*ones(klength,1);
else
    muk=muk/mukmag;
end

%projection onto mean of stimulus
if isempty(shist)
    %no spike history terms
    muprojr=0;
else
    m=getm(post);
    muprojr=shist'*m(klength+1:end,1);
end
%*************************************************************************
%Define the quadratic form
%x^t A x + Bx +d
%which has a null space defined by the current mean of the stimulus terms
%so the quadratic function we want to find max and min as function of
%muproj=muk'k; is
%y'*quad.eigd*y +
%(muproj*quad.wmuproj+quad.w)*y+(muproj^2*quad.dmuproj2+muproj*quad.muproj+
%quad.d)
%[quad,timing.eigtime]=quadmodshist(post.c,eigck,muk,shist);

[quad,qtime]=QuadModObj('C',getc(post),'eigck',eigck,'muk',muk,'shist',shist);
tquadmod=qtime;
%***********************************************************************************
%Code Fork: 2 different ways to compute the maximum
%       there are two different ways to compute the maximum.
%       The methods differ in how we compute sigmamax. In particular do we
%       vary mu_\epsilon (the projection of the stimulus along the mean) or
%       do we vary the lagrange multiplier and compute \mu_epsilon
%************************************************************************
if (optmethod==0)
    [xopt,muprojopt,lfopt,timing]=varymuproj(muprojr,quad,objfunc,muk,mukmag,mparam,sobj.optim);
    fcase=0;
else
    if (getshistlen(mparam)>0)
    warning('need to debug varying the lagrange multiplier to compute the optimium with spike history');
    end
    [xopt,muprojopt,lfopt,timing,fcase]=varylagrangemult(muprojr,quad,objfunc,mukmag,mparam,sobj.optim);
end
timing.tquadmod=tquadmod;


%computes the fisher information for the poisson case with exponential
%response






