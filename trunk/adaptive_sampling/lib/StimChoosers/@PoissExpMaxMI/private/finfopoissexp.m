%computes the fisher information for the poisson case with exponential
%response
%this is an approximation
function [sobj]=finfopoissexp(muproj,sigma)
%compute the log of the fisherinfo
sobj=muproj+.5*sigma+log(sigma);