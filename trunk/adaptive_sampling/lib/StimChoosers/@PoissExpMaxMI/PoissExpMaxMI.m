%function sim=Constructor(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Select optimial stimuli for the canonical poisson.
%
%
%Revision
%   01-08-2009 - convert to new object model

classdef (ConstructOnLoad=true) PoissExpMaxMI < StimChooserObj

    properties(SetAccess=private,GetAccess=public)
        %**********************************************************
        %Define Members of object
        %**************************************************************
        % ignoreshist          - whether or not to ignore the spike history when
        %                                  doing  the optimization
        % optim                   - optim structure controlling the parameters
        %                                 passed to matlab's numerical integration
        %                                 routines
        % glm                     - glm for the canonical poisson
        %                                this object only maximizes the info for
        %                                the canonical poisson
        %searchmeth          -'varymu' or 'varylagrange'
        %                              - determines how we perform the search over
        %                              the upper bound
        % debug                 - set to true if you want to run checks
        %                           regarding the quadratic form and boundaries
        %declare the structure
        optim=optimset('TolX',10^-12,'TolF',10^-12);
        ignoreshist=0;
        glm=[];
        searchmeth='varymu';

    end


    methods
        function obj=PoissExpMaxMI(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=params;
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            bparams.glmspecific=true;
            obj=obj@StimChooserObj(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %set the version to be a structure
            obj.version.PoissExpMaxMI=090108;
            
                    obj.glm=GLMPoisson('canon');


            fnames=fieldnames(params);


            %**************************************************************************
            %Parse the input arguments
            %create a new simulation
            for j=1:length(fnames)
                switch fnames{j}
                    %have a case statement for any fieldname which recieves special
                    %tratement
                    case{'searchmethod','searchmeth'}
                        switch params.(fnames{j})
                            case {'varymu','varylagrange'}
                                obj.searchmeth=params.(fnames{j});

                            otherwise
                                error('PoissExpMaxMI:InputArg','Invalid method specified for searchmethod must be varymu or varylagrange');
                        end
                    case 'glm'
                        error('You cannot specify the GLM to use with this object. The GLM must be the canonical poisson');
                    case 'ignoreshist'
                        obj.ignoreshist=params.ignoreshist;
                end
            end

        end
    end
end

