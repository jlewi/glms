%function testpoissexpmaxmi(sobj)
%   sobj - Poissexpmaxmi object
%
%Explanation: This is a test harness. It 
%   optimizes some random data using both methods and checks if they are
%   the same.
%
function testpoissexpmaxmi(sobj)

%*************************************************************
%Randomly generate the posterior for which we want to choose the optimal
%stimulus
%*******************************************************************
klength=2;
muk=normrnd(zeros(klength,1),ones(klength,1));
c=normrnd(zeros(klength,klength),ones(klength,klength));
c=c*c';
shist=[];

pinit=GaussPost('m',zeros(klength,1),'c',c);

mparam=MparamObj('glm',GLMModel('poisson','canon'),'mmag',1,'klength',klength,'alength',0,'pinit',pinit);
eigck=EigObj('matrix',c);

%set murange to [] so we will search entire range
murange=[];
objfunc=@finfopoissexp;

%eigck  - eigendecomposition of
%   post.c(1:mparam.klength,1:mparam:klength)

%**********************************************
%unit vector in direction of mean of just stimulus components
%need its magnitude as well
mukmag=(muk'*muk)^.5;
%if mukmag happens to be 0 (possible due to intialization)
%then pick a vector in direction [1;...]
%correct solution would to pick stimulus in direction of max eigenvalue
%(but I think we still get that result)
%but I don't want to mess with the code. a mean of 0 should be an anomly
if (mukmag==0)
    warning('Magnitude of muk is zero \n');
    muk=1/(klength^.5)*ones(klength,1);
else
    muk=muk/mukmag;
end

%projection onto mean of stimulus
if isempty(shist)
    %no spike history terms
    muprojr=0;
else
    m=getm(post);
    muprojr=shist'*m(klength+1:end,1);
end
%*************************************************************************
%Define the quadratic form
%x^t A x + Bx +d
%which has a null space defined by the current mean of the stimulus terms
%so the quadratic function we want to find max and min as function of
[quad,qtime]=QuadModObj('C',c,'eigck',eigck,'muk',muk,'shist',shist);
tquadmod=qtime;

%***********************************************************************************
%Compute th optimal stimulus both ways
%************************************************************************

    [vmu.xopt,vmu.muprojopt,vmu.lfopt,vmu.timing]=varymuproj(muprojr,quad,objfunc,muk,mukmag,mparam,sobj.optim);


    [vlm.xopt,vlm.muprojopt,vlm.lfopt,vlm.timing,vlm.fcase]=varylagrangemult(muprojr,quad,objfunc,mukmag,mparam,sobj.optim);


    if ~any(vlm.xopt~=vmu.xopt)
        error('varymu and vary lagrange did not return the same results');
    else
        fprintf('Vary lagrange and varymu returned same answer. \n');
    end
    
    