%PoolstimSubMuProj('nstim','miobj')
%   miobj - object which computes the mutual information
%   nstim - number of stimuli in the pool
%
%Explanation: This stim chooser creates the pool by taking linear
%combinations of the mean and max eigenvector. Hence it does not need to
%store a stimulus pool. This should save memory and help in
%high-dimensionali simulations.
function [obj,sfactor]=PoolstimHeurMuMax(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'nstim','miobj'};
con(1).cfun=1;

%**********************************************************
%Define Members of object
%**************************************************************
% nstim - number of stimuli to put in the pool
% 
% 
%               
obj=struct('version',071217,'nstim',[],'miobj',[]);


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    obj=class(obj,'PoolstimHeurMuMax',StimChooserObj());
    return
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);


%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
       %do nothing     
       if (params.nstim<=0)
           error('nstim must be an integer > 0');
       end
       if ~isa(params.miobj,'CompMIBase')
          error('miobj must be derived from CompMIBase'); 
       end
       obj.miobj=params.miobj;
       obj.nstim=params.nstim;
       
       params=rmfield(params,'miobj');
       params=rmfield(params,'nstim');
    otherwise        
        error('Constructor not implemented')
end



%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=StimChooserObj(params);
obj=class(obj,'PoolstimHeurMuMax',pbase);


