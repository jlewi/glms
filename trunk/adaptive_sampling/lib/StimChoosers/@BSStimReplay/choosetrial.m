% [stim, shist, obsrv,stimindex]= choosetrial(obj, simobj,post) 
%   obj 
%   simobj - 
%   post - the posterior
%
% Explanation: Choose the appropriate stimulus. We just get the appropriate
% trial based on stimorder
function   [stim, shist, obsrv,stimindex,blogminfo]= choosetrial(obj, simobj,post) 

blogminfo=nan;
trial=simobj.niter+1;

stimindex=(obj.stimorder(:,trial));

windex=stimindex(1);
repeat=stimindex(2);
stimtrial=stimindex(3);
[stim,shist,obsrv]=gettrialwfilerepeat(obj.bdata,windex,repeat,stimtrial,simobj.mobj);

