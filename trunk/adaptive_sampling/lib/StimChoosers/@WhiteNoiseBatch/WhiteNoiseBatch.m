%function sim=WhiteNoiseBatch
%
%
%Explanation: Select a batch of stimuli using white noise. 
%Revisions:
classdef (ConstructOnLoad=true) WhiteNoiseBatch <StimChooserObj
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %lastbatch is a structure storing information about when we last
    %           sampled a stimulus and the stimulus on that trial
    %       .stim  - matrix of dimensions klength x ktlength
    %       .trial - trial on which it was drawn.
    properties(SetAccess=private, GetAccess=public)

        
        lastbatch=struct('stim',[],'trial',0);
    end

    methods(Static)
       obj=loadobj(lobj); 
    end
    methods
        function obj=WhiteNoiseBatch(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind

                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            bparams=params;            
            bparams.glmspecific=false;
            obj=obj@StimChooserObj(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind

                case {Constructor.noargs,Constructor.emptyin,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end


            %set the version to be a structure
            obj.version.WhiteNoiseBatch=090204;
        end
    end
end


