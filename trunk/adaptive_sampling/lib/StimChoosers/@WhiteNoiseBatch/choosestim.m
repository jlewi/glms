%function [stim]=choosestim(obj,simobj,post)
%   obj - the stimchooserobj
%   simobj - the simulation object
%   post the posterior
%
%Return value
%   stim - the stimulus
%
% Explanation: Select the stimulus by sampling the Gaussian process.
%  if mobj has a temporal receptive field then we only optimize the
%  stimulus ever ktlength trials. On the other trials we just pick the
%  stimuli
function [stim]=choosestim(obj,simobj,post)

trial=simobj.niter+1;
mobj=simobj.mobj;


spast=[];
%determine if we need to draw the stimulus from a gaussian distribution
rtrial=trial-obj.lastbatch.trial+1;
if (mobj.ktlength==1 || rtrial>mobj.ktlength || isempty(obj.lastbatch.stim))

    
    %randomly sample a white Gaussian. 
    %we want the average power to be mobj.mmag^2
    %therefore the variance in each direction should be 
    %mobj.mmag^2/obj.nstimcoveff
    vdir=mobj.mmag^2/mobj.nstimcoeff;
    stim=normrnd(zeros(mobj.nstimcoeff,1),ones(mobj.nstimcoeff,1)*vdir^.5);
    
    if (isa(mobj,'MBSFTSep'))
       %the stimulus will be specified in terms of the fourier coefficients
       %therefore we need to project it into the stimulus domain

       stim=tospecdom(mobj,stim);
    end
    obj.lastbatch.stim=reshape(stim,[mobj.klength mobj.ktlength]);
    obj.lastbatch.trial=trial;

    rtrial=1;
end


stim=obj.lastbatch.stim(:,rtrial);

%store the value of minfo obtained for the distribution on this trial




