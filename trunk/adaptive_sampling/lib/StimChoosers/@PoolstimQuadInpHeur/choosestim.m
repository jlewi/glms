%function stim=choosestim(sobj,post,shist,mparam,varargin)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   qobj - modified quadratic form. Only returned if number of arguments
%               is >3
%Explanation: maximize the mutual information for the canonical poisson
%model
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mparam,varargin)
oreturn=[];

%select the heuristic for modifying the quadratic form
%return the object because modheur might load the stimuli and we want
%to keep track of that.
[stimpool,sobj]=sobj.modheur(sobj,post,shist,mparam);
%[stimpool]=modstimpoolmuvary(sobj,post,shist,mparam);
%[stimpool]=modstimpoolmusigma(sobj,post,shist,mparam);
%[stimpool]=modstimpoolmuvary(sobj,post,shist,mparam);

if (getdebug(sobj))
    %check magnitudes sum to magcon
    mag=sum(stimpool.^2,1).^.5;
    if ~isempty(find(abs((mag-sobj.magcon))>10^-8))
        error('Magnitudes not properly normalized');
    end
end
%*****************************************************************
% 2 possibilities
%1) select stimuli randomly from the pool
%2) select stimuli from pool which maximizes the mutual info
%******************************************************************
%nstim=size(sobj.stimpool,2);
nstim=getpoolsize(sobj);
if (getinfomax(sobj)==0)
    %randomly select stimulus
    %keyboard
    sind=ceil(rand(1)*nstim);
    xmax=stimpool(:,sind);
    oreturn.sind=sind;
    muproj=[];
    sigma=[];

else

    %        stim=[stimpool;shist*ones(1,nstim)];
    stimind=[];

    %compute the nonlinear projections of the stimuli
    %        stimnonlin=sobj.(bname).ninpfunc(stim,sobj.(bname).fparam);

    %stimnonlin=projinp(sobj.(bname).ninpobj,stim);
    stimnonlin=projinp(mparam,stimpool,shist);
    muproj=getm(post)'*stimnonlin;

    %compute the
    % sigma=zeros(1,nstim);
    %4-19-2007
    %try to speed this up using matrix operations
    %   for j=1:nstim
    %      sigma(j)=stim(:,j)'*post.c*stim(:,j);
    %    end
    %each column is the post.c*(:,j)
    sigma=getc(post)*stimnonlin;
    sigma=stimnonlin.*sigma;
    sigma=sum(sigma,1);

    mi=compmi(getmiobj(sobj),muproj,sigma);

    %maximize the mi

    [maxmi indmax]=max(mi);



    poolind=indmax;

    xmax=stimpool(:,poolind);
    oreturn.sind=poolind;

end

%***********************
%optional output
%Return the stimulus pool if we ask for it
%****************************************
if (nargout>3)
    varargout{1}.muproj=muproj;
    varargout{1}.sigma=sigma;
    varargout{1}.mi=mi;
end

