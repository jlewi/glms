%function [stim,sobj]=modstimpoolmu(sobj,post,shist,mparam)
%   sobj - PoolStimQuadInpHeur
%   post - posterior
%   
%
%Return value:
%   stim - set of stimuli 
%
%Explanation this function uses a heuristic to create a set of stimuli
%   which will likely span the feasible region in mglmproj,sglmproj space
%
%   This heuristic just looks at getting mu to span the full
%   range.
function [stimpool,sobj]=modstimpoolmu(sobj,post,shist,mparam)


bname=sobj.bname;   %name of base class;
sobj.(bname)=loadstim(sobj.(bname));

%compute the vector in stimulus space most correlated with the mean of the
%posterior
%get a quadratic matrix
%i.e returna matrix qmat such that x'*qmat*x;
m.mat=qmatquad(getninpobj(mparam),getm(post));
%due an svd 
[m.evecs m.eigd]=svd(m.mat);

%pull out the max eigenvector
%version:070531 - instead of always selecting 
%   the max eigenvector select one of the principal components
%   sample the principal components in proprotion to the relative energy as
%   measured by the svd.
m.eigd=diag(m.eigd);
%normalize the energy of the singular values
neigd=m.eigd/sum(m.eigd);

%rangdomly generate the index of one of the eigenvectors
reigind=find(cumsum(neigd)>rand(1,1),1);
m.emax=m.evecs(:,reigind);


%randomly generate the projections along the mean
muproj=rand(1,sobj.(bname).nstim)*2*sobj.magcon-sobj.magcon;

%adjust the stimuli accordingly 
%1. remove projection along the max eigenvector correlated with the mean
stimpool=sobj.(bname).stimpool;
stimpool=stimpool-m.emax*(m.emax'*stimpool);

%2. scale the stimuli so that all energy not in muproj is along current
%direction
energy=sobj.magcon^2-muproj.^2;

%normalize
mag=sum(stimpool.^2,1).^.5;
stimpool=(ones(size(stimpool,1),1)*(energy.^.5./(mag))).*stimpool;

%3. add in projection along the mean
stimpool=stimpool+m.emax*muproj;

