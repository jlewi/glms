%function [stim]=modstimpoolmusigma(sobj,post,shist,mparam)
%   sobj - PoolStimQuadInpHeur
%   post - posterior
%   
%
%Return value:
%   stim - set of stimuli 
%
%Explanation this function uses a heuristic to create a set of stimuli
%   which will likely span the feasible region in mglmproj,sglmproj space
%
%   This heuristic tries to get sigma inaddition to mu to span the full
%   range.
function [stimpool,sobj]=modstimpoolmusigma(sobj,post,shist,mobj)

dim=getstimlen(mobj);
psize=getpoolsize(sobj);

bname=sobj.bname;   %name of base class;
sobj=loadstim(sobj);

%compute the vector in stimulus space most correlated with the mean of the
%posterior
%get a quadratic matrix
%i.e returna matrix qmat such that x'*qmat*x;
m.mat=qmatquad(getninpobj(mobj),getm(post));
%due an svd 
[m.evecs m.eigd]=svd(m.mat);

%pull out the max eigenvector
%version:070531 - instead of always selecting 
%   the max eigenvector select one of the principal components
%   sample the principal components in proprotion to the relative energy as
%   measured by the svd.
m.eigd=diag(m.eigd);
%normalize the energy of the singular values
neigd=m.eigd/sum(m.eigd);

%each eigenvector appears a number of times proportional to probability
%of its eigenvalue.
m.emax=zeros(dim,psize);
sind=1;
for j=1:dim
    eind=sind+floor(neigd(j)*psize)-1;
   m.emax(:,sind:eind)=m.evecs(:,j)*ones(1,eind-sind+1); 
   sind=eind+1;
end

%any leftover due to rounding error we set to max evector
if (eind<psize);
   m.emax(:,eind+1:end)=m.evecs(:,1)*ones(1,psize-eind); 
end

%rnums=rand(1,getpoolsize(sobj));

%reigind=find(cumsum(neigd)>rand(1,1),1);
%m.emax=m.evecs(:,reigind);


%randomly generate the projections along the mean
muproj=rand(1,sobj.(bname).nstim)*2*sobj.magcon-sobj.magcon;


%******************************************************************
%To vary sigma
%*******************************************************************
%compute the svd of the covariance matrix
[csvd usvd]=svd(getc(post));

%pick the max eigenvector of the svd.
cpmax=csvd(:,1);

%take the mag eigenvector
cmax.mat=qmatquad(getninpobj(mobj),cpmax);
%due an svd 
[cmax.evecs cmax.eigd]=svd(cmax.mat);
cmax.max=cmax.evecs(:,1);

cmax.eigd=diag(cmax.eigd);
%normalize the energy of the singular values
neigd=cmax.eigd/sum(cmax.eigd);

%each eigenvector appears a number of times proportional to probability
%of its eigenvalue.
cmax.max=zeros(dim,psize);
sind=1;
for j=1:dim
     eind=sind+floor(neigd(j)*psize)-1;
   cmax.max(:,sind:eind)=cmax.evecs(:,j)*ones(1,eind-sind+1); 
   sind=eind+1;
end

%any leftover due to rounding error we set to max evector
if (eind<psize);
   cmax.max(:,eind+1:end)=cmax.evecs(:,1)*ones(1,psize-eind); 
end

%randomly generate the projections along this vector
cpproj=rand(1,psize)*2*getmagcon(sobj)-getmagcon(sobj);

%********************************************************************
%add the projections along the cpmax and m.emax
vset=m.emax.*(ones(dim,1)*muproj)+cmax.max.*(ones(dim,1)*cpproj);
venergy=sum(vset.^2,1);

%1. remove projection along the max eigenvector correlated with the mean
% and with cpmax
stimpool=getstimpool(sobj.(bname));
stimpool=stimpool-(ones(dim,1)*(sum(stimpool.*vset,1)./venergy)).*vset;

%2. scale the stimuli so that all energy not in muproj is along current
%direction

energy=getmagcon(sobj)^2-venergy;

%find energy which exceeds magconstraint
itomuch=find(energy<0);
energy(itomuch)=0;

%normalize
mag=sum(stimpool.^2,1).^.5;
stimpool=(ones(size(stimpool,1),1)*(energy.^.5./(mag))).*stimpool;

%normalize any venergy which have to much
vset(:,itomuch)=ones(size(vset,1),1)*(getmagcon(sobj)./venergy(:,itomuch).^.5).*vset(:,itomuch);

%3. add in projection along the vector
stimpool=stimpool+vset;