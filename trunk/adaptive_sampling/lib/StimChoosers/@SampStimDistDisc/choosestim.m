%function [sind]=choosestim(obj,simobj,post)
%   obj - the stimchooserobj
%   simobj - the simulation object
%   post the posterior
%
%Return value
%   sind - the index into the stimulus pool of the stimulus 
%
% Explanation: Select the stimulus by sampling the Gaussian process.
%
function [sind]=choosestim(obj,simobj,post)

mobj=simobj.mobj;
%we need to get past values of the stimuli if they are needed
%we then compute the appropriate Gaussian conditioning formulas. 
if (mobj.ktlength>1)
    tlast=simobj.niter;
    if (simobj.niter-(mobj.ktlength-1)>=0)
        sr=simobj.sr(tlast-(mobj.ktlength-1)+1:tlast);
        spast=getinput(sr);
        
    else
        if (tlast>0)
        sr=simobj.sr(1:tlast);
        spast=getinput(sr); 
        
        else
            spast=[];
        end
    end
    
    %zero padd spast with zeros for any stimuli which are missing
 %01-10-2009
 %      don't zero pad. Instead we should use the appropriate conditional
 %      or marginal distribution to draw the stimuli
  %  spast=[zeros(mobj.klength,(mobj.ktlength-1) -size(spast,2)) spast];

 

    sind=samppcon(obj.ps,spast);
    
else
   error('Need to write code to just sample the distribution'); 
end

sind=GLMStimIndex('index',sind);