%function sim=SampStimDistDisc('ps')
%   ps - the stimulus distribution
%        
classdef (ConstructOnLoad=true) SampStimDistDisc < SampStimDist
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public,Dependent)
        %provide access to the stimulus pool stored in ps
        %the point is to provide an interface to the stimuli which can be
        %made constant across manny stimchooser objects 
        stim;
    end

    methods
        function stim=get.stim(obj)
           stim=obj.ps.stim; 
        end
        function obj=SampStimDistDisc(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.         
            con(1).rparams={'ps'};



               %determine the constructor given the input parameters
              [cind,params]=Constructor.id(varargin,con);
           

            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=params;
                    
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@SampStimDist(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************          
             switch cind
                 case 1
                     %make sure ps is of appropriate type
                     if ~isa(obj.ps,'StatDistDisc')
                         error('ps must be of stype StatDistDisc');
                     end
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                 error('unexpected value for cind');
             end


            %set the version to be a structure
            obj.version.SampStimDistDisc=090110;
        end
    end
end


