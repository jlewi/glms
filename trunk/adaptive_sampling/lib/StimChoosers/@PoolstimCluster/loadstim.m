%function sobj=loadstim(sobj)
%
%Explanation: load the stimuli if not already loaded
function sobj=loadstim(sobj)
if ~(sobj.stimloaded)

    
     fprintf('Loading stimpool from file \n');
        %check the fileid hasn't changed
        %this fileid provides a check to ensure the file containing the
        %stimulus pool hasn't changed since we first ran a simulation.
        fileid=load(sobj.poolfile.fname,'fileid');
        if (fileid.fileid ~= sobj.poolfile.fileid)
            error('File id for file containing stimuli does not match file id in file \n');
        end        
        [sobj]=processpool(sobj,'file',sobj.poolfile.fname);
        sobj.stimloaded=true;
end