%function sim=SpatialGrating(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object 
%
%Explanation: Set the stimulus (a 1-d signal) to a pure tone
% with a random phase, and frequency.
function obj=SpatialGrating(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'dim','period'};
con(1).cfun=1;


%**********************************************************
%Define Members of object
%**************************************************************
% version - version number for the object
%           store this as yearmonthdate
%           using 2 digit format. This way version numbers follow numerical
%           order
%           -version numbers make it easier to maintain compatibility
%               if you add and remove fields
%   dim         -dimension of the image (height,width)
%   speriod        -range of temporal periods to sample
obj=struct('version',071029,'dim',[],'period',[]);


switch nargin
    case 0
        %***************************************************
        %Blank Construtor: used by loadobj
        %***************************************************
        %instantiate the base class if one is required
        obj=class(obj,'PureTones',StimChooserObj);
        return    
end

%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        obj.dim=params.dim;
        obj.period=params.period;
        
        if (max(obj.period)>max(obj.dim))
            warning('Period is larger than dimensions of image.');
        end
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=StimChooserObj();

%if no base object
obj=class(obj,'PureTones',pbase);




    