%function [obj,optstim]=optbatchinwav(obj,simobj,windex,post)
%   obj - The stimulation chooser object
%   simobj - the simulation object
%   windex - the index of the wave file we are searching for the optimal
%          stimulus
%   post   - the posterior object
% Explanation - this function constructs  a pool of psize stimuli
%       from the trials corresponding to wavefile wfile
%       We then select from this pool the optimal stimulus
%Return value:
%  	opstim.windex - windex - wave file
%   optstim.repeat - which repeat of this wavefile
%   opstim.trial - which trial of this wavefile
%   opstim.mi -
%
%
%
%Explanation: Computes the mutual information.
%
%Revisions:
function [optstim]=optbatchinwav(obj,simobj,windex,post)


mobj=simobj.mobj;

bdata=obj.bdata;


%    wcount=wcount+1;
%fprintf('BSlogpost:labindex=%d compllike  file index %d of %d wave files \n',labindex,windex,length(windexes));


%to compute the mutual information we need to compute
%muproj and sigmaproj
%compute muproj and sigmaproj setting spike history to zero.
[ntrials,cstart]=getntrialsinwave(bdata,windex);

if (obj.nrepeats~=1)
    error('this function assumes we only compute the pool for 1 repeat');
end


repeat=1;
[stim,shist,obsrv]=gettrialwfilerepeat(bdata,windex,repeat*ones(1,ntrials),[1:ntrials],mobj);

bias=1;


inp=packinput(mobj,stim,zeros(getshistlen(mobj),ntrials),bias*ones(1,ntrials));


%paststim - will be an array of the trials from this wave file which have
%already been presented
paststim=[];
%muproj is a nrepeatsxnstim matrix
%where nstim is the number of trials we divide this wave file into
if  (simobj.niter>0)
    %I should probably switch to using stimindtopoolind
    %get those stimulis which occured on this wave file
    pthiswave=find(simobj.paststim(1,1:simobj.niter)==windex);

    %see if any are from this repeat
    indrepeat=find(simobj.paststim(2,pthiswave)==repeat);
    if ~isempty(indrepeat)
        %past trial is the past trials on which we presented a stimulus
        %from repeat 'repeat' of this wave file
        pastrial=pthiswave(indrepeat);


        paststim=[simobj.paststim(3,pastrial)];
    end
end

%********************************************************************
%Determine which trials represent a valid start for a batch
%A trial is the valid start for the batch if it and the next bsize-1
%stimuli haven't been presented yet
%*******************************************************************
trials=[1:ntrials];
%set those trials which have already been presented to nan;
trials(paststim)=nan;

%compute how many overlapping batches we can get from this wave file
nbatches=ntrials-obj.bsize+1;
batchstart=conv(trials,ones(1,obj.bsize));
batchstart=batchstart(obj.bsize:obj.bsize+nbatches-1);
batchstart=find(~isnan(batchstart));


%if maxmi is nan then there are no batches in this wave file
%containing bsize stimuli
if isempty(batchstart)
   %no stimuli left in this file
    optstim.mi=nan;
    optstim.windex=windex;
    optstim.repeat=nan;
    optstim.ntrial=nan;

else
    %find the batch with the maximum mutual info
    optstim.windex=windex;
    optstim.repeat=repeat;
    optstim.ntrial=nan;
    optstim.mi=-inf;
    
    for (bstart=batchstart)
       %compute the mutual information for this batch
        %use complogminfo and not compminfo b\c complogminfo
        %always returns a double
       mi=complogminfo(obj.miobj,inp(:,bstart:bstart+obj.bsize-1),post);
       
       if (mi>optstim.mi)
          optstim.ntrial=bstart;
          optstim.mi=mi;
       end
    end
end
