%function poolind=stimindtopoolind(obj,stimind)
%   obj 
%   stimind - 3xn matrix
%             stimind(1,:) index of the wave file
%             stimind(2,:) repeat
%             stimind(3,:) relative trial index for this wave file and
%             repeat
%
%Return value:
%   poolind - contains the row of the matrices stored in poolfile
%            continaing the mutual information for this stimulus
function poolind=stimindtopoolind(obj,stimind)

    %make sure the repeats doesn't execeed the allowed number
    if any(stimind(2,:)>obj.nrepeats);
        error('You are trying to get row for stimuli which were not included in the pool because the repeat exceeds the number of repeats included in the pool.');
    end
    
    %make sure we don't include wave files not in the pool
    windexes=unique(stimind(1,:));
    
    for wind=windexes
        if ~(any(obj.windexes==wind))
            error('The wave file was not inlucded in the stimulus pool.');
        end
    end
    
    if (obj.nrepeats>1)
        %to handle more than 1 repeats we need to know how we arange the
        %repeats in the matrix
        error('This code doesnt handle obj.nrepeats>1 yet.');
    end
    
    %map windex into the corresponding index in obj.windexes
    maptowind=zeros(1,max(obj.windexes));
    maptowind(obj.windexes)=1:length(obj.windexes);
    poolind=obj.wavpoolind(maptowind(stimind(1,:)),1)+stimind(3,:)'-1;