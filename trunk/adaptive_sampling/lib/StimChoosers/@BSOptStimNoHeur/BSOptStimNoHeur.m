%function sim=BSOptStim('bdata','miobj')
%   datafile
%   miobj  - the object which computes the mutual information as a function
%           of mu and sigma
%
%
%function sim=BSOptStim('miobj')
%   This constructor is hack. B\c for BSBatchPoissLBTan we declare a member
%   variable of type BSOptStimTan. 
%
%Optional Parameters:
%   windexes - wave files to use. Only trials from these wave files will be
%             selected
%
%   poolfile - is a FilePath to where we will store a matrix containing the
%            mutual information for all stimuli in the pool
%Explanation: A class to choose the optimal stimulus from the bird song
%data. This object does not use any heuristics for selecting a subset of
%the stimulus pool. Instead it computes the mutual information for all
%possible stimuli.
%
%We also save the mutual information for each stimulus in the pool to a
%file
%
%Revisions:
%
classdef (ConstructOnLoad=true) BSOptStimNoHeur < handle

    % mpool - method for selecting the pool of stimuli
    %         -a pointer to a function
    %bspost - a BSLogPOst object
    %poolfile - an RaccessFile where we store the mutual information
    %              for every stimulus in the pool after trial trial
    %           We store for every trial
    %           an ntrials x 3 matrix
    %           col 1 - muproj
    %           col 2 - sigma
    %           col 3 - mutual info
    %           The information is stored in the order of windexes
    %           e.g the first block of stimuli will be for the stimuli coming from all repeats of
    %           the first wave file in windexes
    %windexes - which wave files to choose the stimuli from
    %           by excluding some wave files you can create a test set
    %
    %wavpoolind - this is a windexes by 2 matrix
    %           - each row gives the start and end row for stimuli
    %             from this wave file in poolfile
    %how many   repeats of each wave file should we include
    %           - its pointless to include more than 1 repeat because only
    %             the spike history would change and in a normal experiemnt
    %             we would treat that as fixed anyway
    properties(SetAccess=private,GetAccess=public)
        version=080724;
        miobj=[];
        bspost=[];
        windexes=[];
        poolfile=[];
        wavpoolind=[];
        nrepeats=1;
    end

    properties (Dependent=true)
       bdata; 
    end
    methods

        function obj=BSOptStimNoHeur(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            %The blank constructor will be used by the base class when
            %loading an object
            con(1).rparams={};
       
            
            con(2).rparams={'bdata','miobj'};
       
           con(3).rparams={'miobj'};
           

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************

                    return
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);
            
            if (~isempty(params) && length(cind)>1)
               cind=cind(2:end); 
            end

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                case 2

                    %data=load(getpath(params.datafile));
                    obj.bspost=BSlogpost('bdata',params.bdata);
                
                    obj.miobj=params.miobj;

                    if isfield(params,'windexes')
                        obj.windexes=params.windexes;
                    else
                        obj.windexes=1:getnwavefiles(params.bdata);
                    end

                    %create the RAccessFile to store the data
                    matdim=[getntrials(obj),3];
                    %we need to compute
                    %we set distrmatrix to false because only one lab will
                    %be doing the writing
                    if isfield(params,'poolfile')
                        if ~isempty(params.poolfile)
                        obj.poolfile=RAccessFile('fname',params.poolfile,'dim',matdim,'distmatrix',false);
                        end
                    end
                case 3
                    obj.miobj=params.miobj;
                    
                  
                    if isfield(params,'windexes')
                        obj.windexes=params.windexes;
                    end
                   
                    %we need to compute
                    %we set distrmatrix to false because only one lab will
                    %be doing the writing
                    if isfield(params,'poolfile')
                             %create the RAccessFile to store the data
                    matdim=[getntrials(obj),3];
                    
                        if ~isempty(params.poolfile)
                        obj.poolfile=RAccessFile('fname',params.poolfile,'dim',matdim,'distmatrix',false);
                        end
                    end
                otherwise
                    
                    error('Constructor not implemented')
            end



        end
        %Return Value:
        %     ntrials - the number of trials we have data for
        function ntrials=getntrials(obj)
            %we need to compute the number of trials in the allowed indexes
            nwindex=length(obj.windexes);

            %determine how many times each eave file is repeated
%             nrepeats=zeros(nwindex,1);
%             for wind=1:nwindex
%                 nrepeats(wind)=getnrepeats(obj.bdata,obj.windexes(wind)) ;
%             end

%             trialint=zeros(nwindex*obj.nrepeats,2);
% 
%             startrow=1;
            ntrials=0;
            for wind=1:nwindex

                [ntrialsinwave]=getntrialsinwave(obj.bdata,obj.windexes(wind));
                ntrials=ntrials+ntrialsinwave*obj.nrepeats;
            end

        end
        function bdata=getbdata(obj)
            bdata=obj.bspost.bdata;
        end
        function wavpoolind=get.wavpoolind(obj)            
            if (isempty(obj.wavpoolind) && ~isempty(obj.bspost))
                wavpoolind=zeros(length(obj.windexes),1);
                for wind=1:length(obj.windexes)
                    windex=obj.windexes(wind);

                    if (wind==1)
                        wavpoolind(wind,1)=1;
                    else
                        wavpoolind(wind,1)=wavpoolind(wind-1,2)+1;
                    end
                    [ntrials]=getntrialsinwave(obj.bdata,windex);
                    nrepeats=obj.nrepeats;
                    wavpoolind(wind,2)=wavpoolind(wind,1)+ntrials*nrepeats-1;

                end
                obj.wavpoolind=wavpoolind;
            end

            wavpoolind=obj.wavpoolind;
        end
        
        %get the bdata object stored in bpost
        function bdata=get.bdata(obj)
            if isempty(obj.bspost)
                bdata=[];
            else
               bdata=obj.bspost.bdata; 
            end
        end
    end
end



