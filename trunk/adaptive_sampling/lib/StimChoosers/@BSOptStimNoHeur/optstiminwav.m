%function [obj,optstim]=optstiminwav(obj,simobj,windex,post)
%   obj - The stimulation chooser object
%   simobj - the simulation object
%   windex - the index of the wave file we are searching for the optimal
%          stimulus
%   post   - the posterior object
% Explanation - this function constructs  a pool of psize stimuli
%       from the trials corresponding to wavefile wfile
%       We then select from this pool the optimal stimulus
%Return value:
%  	opstim.windex - windex - wave file
%   optstim.repeat - which repeat of this wavefile
%   opstim.trial - which trial of this wavefile
%   opstim.mi -
%
%   stimpool - return the stimulus pool
%              an nx3 array
%      col 1 - projection of stimulus on the mean
%      col 2 - sigma
%      col 3 - mutual info

%Explanation: Computes the mutual information.
function [optstim,stimpool]=optstiminwav(obj,simobj,windex,post)


mobj=simobj.mobj;

bdata=getbdata(obj.bspost);


%    wcount=wcount+1;
%fprintf('BSlogpost:labindex=%d compllike  file index %d of %d wave files \n',labindex,windex,length(windexes));


%to compute the mutual information we need to compute
%muproj and sigmaproj
[muproj]=compglmproj(obj.bspost,windex,mobj,getm(post));
[ntrials,cstart]=getntrialsinwave(bdata,windex);
stimpool=zeros(ntrials*obj.nrepeats,3);

if (obj.nrepeats~=1)
    error('this function assumes we only compute the pool for 1 repeat');
end


repeat=1;
[stim,shist,obsrv]=gettrialwfilerepeat(bdata,windex,repeat*ones(1,ntrials),[1:ntrials],mobj);

bias=1;


for pind=1:ntrials
    %compute the full input
    inp=packtheta(mobj,stim(:,pind),shist(:,pind),bias);
    %fprintf('Optstiminwav: getc \n');
    stimpool(pind,1)=muproj(repeat,pind);
    stimpool(pind,2)=inp'*rotatebyc(post,inp);
end

%compute the mutual information for the points in the pool
stimpool(:,3)=compmi(obj.miobj, stimpool(:,1),stimpool(:,2));


tr=[];
%muproj is a nrepeatsxnstim matrix
%where nstim is the number of trials we divide this wave file into
if  (simobj.niter>0)
    %I should probably switch to using stimindtopoolind
    %get those stimulis which occured on this wave file
    pthiswave=find(simobj.paststim(1,1:simobj.niter)==windex);

    %see if any are from this repeat
    indrepeat=find(simobj.paststim(2,pthiswave)==repeat);
    if ~isempty(indrepeat)
        %past trial is the past trials on which we presented a stimulus
        %from repeat 'repeat' of this wave file
        pastrial=pthiswave(indrepeat);
        
        
        tr=[simobj.paststim(3,pastrial)];
    end
end

%get the indexes of the trials which we have already presented
tnotshown=ones(1,ntrials);
tnotshown(tr)=0;
tnotshown=find(tnotshown==1);



if (~isempty(tnotshown))


    %find the maximum;
    [maxmi,mind]=max(stimpool(tnotshown,3));
    optstim.windex=windex;
    optstim.repeat=repeat;
    optstim.ntrial=tnotshown(mind);
    optstim.mi=maxmi;


else
    %no stimuli left in this file
    opstim.mi=-inf;
    optstim.windex=windex;
    optstim.repeat=[];
    optstim.ntrial=[];

  
end


