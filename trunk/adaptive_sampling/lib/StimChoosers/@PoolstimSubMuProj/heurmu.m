%function stimpool=heurmu(sobj,post,magcon)
%   sobj - object
%   post - posterior
%   magcon -magcon
%Explanation: This is my original heuristic which just varies the
%projection along the mean but does not take into account the eigenvectors.
function stimpool=heurmu(sobj,post,mobj)
magcon=getmag(mobj);
%randomly generate the projections along the mean
muproj=rand(1,getpoolsize(sobj))*2*magcon-magcon;

%get the mean of just the stimulus terms
fullpostm=getm(post);
mustim=fullpostm(1:getstimlen(mobj),1);

%adjust the stimuli accordingly
%1. remove projection along mu
stimpool=getstimpool(sobj);
stimpool=stimpool-mustim/(mustim'*mustim)*(mustim'*stimpool);

%2. scale the stimuli so that all energy not in muproj is along current
%direction
energy=magcon^2-muproj.^2;

%normalize
mag=sum(stimpool.^2,1).^.5;
stimpool=(ones(size(stimpool,1),1)*(energy.^.5./(mag))).*stimpool;

%3. add in projection along the mean
stimpool=stimpool+mustim/(mustim'*mustim)^.5*muproj;
