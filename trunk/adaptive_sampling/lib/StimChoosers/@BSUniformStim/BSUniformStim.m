%function sim=BSUniformStim('bdata')
%Optional Parameters:
%   windexes - wave files to use. Only trials from these wave files will be
%             selected
%
%Explanation: A class to choose the optimal stimulus from the bird song
%data. This object does not use any heuristics for selecting a subset of
%the stimulus pool. Instead it computes the mutual information for all
%possible stimuli.
%
%We also save the mutual information for each stimulus in the pool to a
%file
%
%Revisions:
%
classdef (ConstructOnLoad=true) BSUniformStim < BSStimChooser

    % bdata   - a BSData object 
    %windexes - which wave files to choose the stimuli from
    %           by excluding some wave files you can create a test set
    %
    %
    %how many   repeats of each wave file should we include
    %           - its pointless to include more than 1 repeat because only
    %             the spike history would change and in a normal experiemnt
    %             we would treat that as fixed anyway
    %
    %ntrialsleft - a length(windexes)x1 matrix
    %            - each entry stores the number of trials
    %              remaining (i.e not selected) from each wave file
    %
    properties(SetAccess=private,GetAccess=public)
        version=080729;
        bdata=[];
        nrepeats=1;
        ntrialsleft=[];
        windexes=[];
    end

    methods

        function obj=BSUniformStim(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'bdata'};
            con(1).cfun=1;




            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************

                    return
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);


            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1

                    %data=load(getpath(params.datafile));
                    obj.bdata=params.bdata;

                    if isfield(params,'windexes')
                        obj.windexes=params.windexes;
                    else
                        obj.windexes=1:getnwavefiles(params.bdata);
                    end

                otherwise
                    error('Constructor not implemented')
            end



        end
        %Return Value:
        %     ntrials - the number of trials we have data for
        function ntrials=getntrials(obj)
            %we need to compute the number of trials in the allowed indexes
            nwindex=length(obj.windexes);

            ntrials=0;
            for wind=1:nwindex

                [ntrialsinwave]=getntrialsinwave(obj.bdata,obj.windexes(wind));
                ntrials=ntrials+ntrialsinwave*obj.nrepeats;
            end

        end
    
        function ntrialsleft=get.ntrialsleft(obj)
           if isempty(obj.ntrialsleft)
               obj.ntrialsleft=zeros(length(obj.windexes),1);
               for wind=1:length(obj.windexes)
                   [obj.ntrialsleft(wind)]=getntrialsinwave(obj.bdata,obj.windexes(wind));
               end
           end
           ntrialsleft=obj.ntrialsleft;
        end
        
        function bdata=getbdata(obj)
           bdata=obj.bdata; 
        end
    end
end



