%function stim=PreCompRS('pname',pname,'degree',degree,'nrand',nrand,'ddotmu',ddotmu,'dsigmasq',disgmasq)
%       pname - name of the file with the precomputed object 
%       degree - degree of response surface to fit to the function
%       nrand  - number of random stimuli to run before doing info. max. 
%       ddotmu - 
%       dsigmasq- when using the RS to optimize the mutual information
%                      we compute the RS on a grid with spacing ddotmu and
%                      dsigmasq and then take the optimium for this region
%   fieldname, value - pairs of fields and values used to initialize the object 
%
% Explanation:
%
function obj=PreCompRS(varargin)

%**************************************************************
%Required parameters/Multiple constructors
%*****************************************************************
%each element of the con array describes a constructor
%   .rparams - string array of the required parameters
%   .cfun   - function (or number) to indicate which constructor to call
con(1).rparams={'pname','degree','nrand','ddotmu','dsigmasq'};
con(1).cfun=1;

%con(2).rparams={'param1','param4','imutual','ddotmu'};
%con(2).cfun=2;

%**********************************************************
%Define Members of object
%**************************************************************
% nrand - number random stimuli
% rsm    - the response surface object
%declare the structure
obj=struct('nrand',0,'rsm',[],'ddotmu',[],'dsigmasq',[]);


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
    pbase=PreCompMIObj();
    obj=class(obj,'PreCompRS');
    return
end

%convert varargin into a structure array
params=parseinputs(varargin);

%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor 
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    ismatch=1;
    for f=1:length(con(j).rparams)
        %check if  parameter was passed in
        if isempty(strmatch(con(j).rparams{f},pnames))
            ismatch=0;
            break;
        end
    end
    if (ismatch==1)
        cind=[cind j];
    end
end

if isempty(cind)
     error('Constructor:missing_arg','Required parameters for one of the constructors was not passed in');
elseif (length(cind)>1)
    error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Cind stores the index of the appropriate constructor
%********************************************************************
switch cind
    case 1
        %create the rsm object
        obj.rsm=RSMutualInfo('fname',params.pname,'degree',params.degree);

        if ((params.nrand<0) || (params.nrand~=floor(params.nrand)))
            error('nrand must be an integer >=0');
        end
        pbase=PreCompMIObj('filename',params.pname);
        obj.nrand=params.nrand;
        obj.ddotmu=params.ddotmu;
        obj.dsigmasq=params.dsigmasq;
    otherwise
        error('Constructor not implemented')
end
    

%*********************************************************
%Create the objectNNN
%****************************************
obj=class(obj,'PreCompRS',pbase);


%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    switch vars{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case {'fname','pname'}
            params.('pname')=vars{j+1};          
        otherwise
            params.(vars{j})=vars{j+1};
    end
end


    