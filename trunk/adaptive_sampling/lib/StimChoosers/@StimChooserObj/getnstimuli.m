%function nstim=getnstimuli(obj)
%   obj - StimChooserObj
%
%Explanation: returns the number of stimuli picked by this object.

function nstim=getnstimuli(obj)
    nstim=obj.nstimuli;