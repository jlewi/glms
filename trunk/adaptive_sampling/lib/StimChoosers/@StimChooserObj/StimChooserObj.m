%function finfo=StimChooserObj(initp)
%
% Explanation: abstract base class for all objects for choosing the
% stimulus
%
%
%
%Revisions:
%   12-26-2008 - Convert the object to Matlab's new OOP model
%             - make it a subclass of handle
classdef (ConstructOnLoad=true) StimChooserObj < handle

    %create an empty object
    %   glmspecific - value to indicate whether method for picking stimuli
    %                           is dependent on a GLM.  This value is used
    %                           to detect/check if the model is misspecified
    %                           it is returned by the function is glm specific
    %                           -child clases which are not glm specific (i.e
    %                           random stimuli should override its value).
    %   debug       - turn on extra debugging
    %   nstimuli    - number of stimuli picked by the object
    %                 i.e nstimuli >1 means we pick a batch of stimuli
    properties(SetAccess=private, GetAccess=public)
        glmspecific=true;
       
        nstimuli=1;
    end
    properties(SetAccess=protected,GetAccess=public)
        version=struct('StimChooserObj',081226);
         debug=false;
    end


    methods
        function stimobj=StimChooserObj(varargin)


            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing

                case {Constructor.nomatch}
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1                    
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                case {Constructor.nomatch}
                otherwise
                    error('unexpected value for cind');
            end

            if isfield(params,'debug')
                stimobj.debug=params.debug;
            end

            if isfield(params,'glmspecific')
               stimobj.glmspecific=params.glmspecific; 
            end
            %set the default version
            %when loading an object this version will be overloaded with the
            %version in the file.
            sobj.version.StimChooserObj=081226;
        end
    end
end
