%function stim=choosestim(sobj,post,shist,einfo,mparam,varargin)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
% Return value
%   xmax -
%           - either the vector stimulus or a GLMInput object if the
%           stimpool is represented as an array of GLMInput objects
%
%   oreturn
%   sobj - the stimulus object
%   stimpool - information about the stimulus pool
%            - only returned if number of arguments is >3
%       .muproj
%       .sigma
%Explanation: maximize the mutual information for the canonical poisson
%model
%
%Revision History
% $Revision$ -
%   i) modifications to handle case where stimpool is GLMInput class
%   ii) if Poolstim.repeatstim=false then remove the stimulus just selected
%        from the pool
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mparam,varargin)
oreturn=[];
stimind=[];
%load the stimulus pool if its not already loaded
sobj=loadstim(sobj);

%*****************************************************************
% 2 possibilities
%1) select stimuli randomly from the pool
%2) select stimuli from pool which maximizes the mutual info
%******************************************************************
%nstim=size(sobj.stimpool,2);
if isa(sobj.stimpool,'GLMInput');
    nstim=numel(sobj.stimpool);
else
    nstim=size(sobj.stimpool,2);
end

if (sobj.infomax==0)
    %randomly select stimulus

    poolind=ceil(rand(1)*nstim);
    if isa(sobj.stimpool,'GLMInput')
        xmax=sobj.stimpool(poolind);
    else
        xmax=sobj.stimpool(:,poolind);
    end
    oreturn.sind=poolind;
    %oreturn.maxmi=maxmi;        %save the mutual information
else
    %if we resample the stimuli to construct our pool
    %compute the mi for each stimulus
    %concatenate the stimulus onto the spikehistory
    if ~(isa(sobj.stimpool,'GLMInput'))
        if (sobj.resample)
            %check if we are retaining any stimuli from last trial
            if ~isempty(sobj.rolloverind)
                nrind=nstim-sobj.nkeepmax;
                stimind=[ceil(rand(1,nrind)*sobj.nstim) sobj.rolloverind];
            else
                stimind=ceil(rand(1,nstim)*sobj.nstim);
            end
            stim=[sobj.stimpool(:,stimind);shist*ones(1,sobj.nstim)];
        else
            if ~isempty(shist)
                stim=[sobj.stimpool;shist*ones(1,nstim)];
            else
                stim=sobj.stimpool;
            end
            stimind=[];
        end
    else
        %get the actual stimulus
        %i.e account for fixed terms, input projections, depenendcy on spike
        %history. This is all handled by projinp
        %if ~isempty(shist)
        %    error('070703 I need to modify the projinp code to properly handle spike history');
        %end
        
        stim=projinp(mparam,sobj.stimpool,shist);        
    end

    %compute muproj and sigma
    muproj=getm(post)'*stim;

    % sigma=zeros(1,nstim);
    %4-19-2007
    %try to speed this up using matrix operations
    %   for j=1:nstim
    %      sigma(j)=stim(:,j)'*post.c*stim(:,j);
    %    end
    %each column is the post.c*(:,j)
    sigma=getc(post)*stim;
    sigma=stim.*sigma;
    sigma=sum(sigma,1);

    mi=compmi(sobj.miobj,muproj,sigma);

    %maximize the mi
    if (sobj.nkeepmax<1 || (sobj.resample==false))
        [maxmi indmax]=max(mi);
    else
        %keep the ones with largest mutual info
        [mi smind]=sort(mi,'descend');
        maxmi=mi(1);
        indmax=smind(1);
        sobj.rolloverind=smind(1:sobj.nkeepmax);
    end
    if ~isempty(stimind)
        %if we drew a subsample of the stimuli from our pool
        %than stimind contains the indexes of the stimuli in our subpool
        %indmax specifies which element of this subsample is the optimium
        %we need to convert this into the appropriate index for the entire
        %pool
        poolind=stimind(indmax);
    else
        poolind=indmax;
    end
    if isa(sobj.stimpool,'GLMInput')
        xmax=sobj.stimpool(poolind);
    else
        xmax=sobj.stimpool(:,poolind);
    end
    oreturn.sind=poolind;
    oreturn.mi=maxmi;        %save the mutual information
end

%remove the stimulus from the pool if told to do so
if ~(sobj.stimrepeat)
    if isa(sobj.stimpool,'GLMInput')
        sobj.stimpool=[sobj.stimpool(1:poolind-1) sobj.stimpool(poolind+1:end)];
    else
        sobj.stimpool=[sobj.stimpool(:,1:poolind-1) sobj.stimpool(:,poolind+1:end)];
    end
end
%if nargout is > 3 return parameters related to the pool
if (nargout>3)
    if (sobj.infomax)
        varargout{1}.muproj= muproj;
        varargout{1}.sigma=sigma;
        varargout{1}.mi=mi;
    end
end

