%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
%
%   Following processing is performed
%       1.) construct the stimulus pool from the file containing the
%       stimuli
function obj=loadobj(lobj)

    %check if lobj is a structure
    %this indicates the class structure has changed and we need to handle
    %the conversion   
    if isstruct(lobj)
        warning('Saved object was an older version. Converting to newer version');
        

        %create a blank object
        obj=Poolstim();
        
        %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(lobj);
        
        %struct for object
        sobj=struct(obj);
        for j=1:length(fnames)
            %make sure field hasn't been delete
             if ~isfield(sobj,fnames{j})
                 error('Field has been removed in newest version of class. Add special handler code');
             else
                   obj.(fnames{j})=lobj.(fnames{j});
             end
        end
        %provide warning message about any new fields
        nfields='';
        fnames=fieldnames(sobj);
        for j=1:length(fnames)
            if ~isfield(lobj,fnames{j})
                nfields=sprintf('%s \n',fnames{j});
                
                %special processing for new fields
                switch fnames{j}
                    case 'version'
                        sobj.version=0; %set version to 0 if it wasn't set
                end
            end
        end
        warning('The following fields were not in the older version. They will be set to default values. \n %s',nfields);
    else
        obj=lobj;
    end
    
    %load the stimulus pool from the file
    %4-20-2007 older versions may have already saved the stimuli to the
    %file
    %in which case we do nothing
    if isempty(obj.stimpool)
        %06-10-2007
        %************************************************
        %if the file isn't there print a warning message
        %but continue
        %we do this because sometimes we should be able to continue running
        %code which doesn't require the pool of stimuli. If/when we try to
        %access the stimuli we will throw an error and deal with it then
if isa(obj.poolfile.fname,'FilePath')
        fname=getpath(obj.poolfile.fname);
    else
        fname=obj.poolfile.fname;
    end
        if ~exist(fname,'file')
            warning('File containing stimulus pool does not exist.\n File: %s \n',fname);
        else
        %check the fileid hasn't changed
        checkfileid(obj);
        %delay loading of the stimulus pool until actually required
        %do this to avoid performance issues
        fprintf('Delaying Load of pool file until actually required\n');        
       obj.stimloaded=false; %indicate we haven't loaded the stimuli
       %[obj]=processpool(obj,'file',obj.poolfile.fname) 
        end
    else
        obj.stimloaded=true;
    end