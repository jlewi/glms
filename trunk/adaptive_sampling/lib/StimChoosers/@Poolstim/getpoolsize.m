%function nstim=getpoolsize(sobj)
%   sobj - stimulus object
%
%Return value:
%   nstim - number of stimuli in the pool
function nstim=getpoolsize(sobj)

    nstim=sobj.nstim;