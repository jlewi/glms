%function getstimloaded=stimloaded(obj)
%	 obj=Poolstim object
% 
%Return value: 
%	 stimloaded=obj.stimloaded 
%
function stimloaded=getstimloaded(obj)
	 stimloaded=obj.stimloaded;
