%function oinfo=objinfo(obj)
%   obj - object
%
%oinfo - 2d cell array which describes this object
%
%Explanation: idea is to populate a cell array with information about this
%object. We can then pass this cell array to oneNoteTable to create a table
%which can be imported into matlab.
%
%$Revision: 1468 $ - use structinfo
function oinfo=objinfo(obj)

oinfo={'class',class(obj)};

s=struct(obj);

%remove fields we don't want to grab info for
fdel={};
try
    if isempty(fdel)
s=rmfield(s,fdel);
    end
catch
    warning('%s.objinfo: %s \n',class(obj),lasterr);
end
oinfo=[oinfo;structinfo(s)];

    
