%function sim=Constructor(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object 
%
% Explanation: This class sets the stimulus by putting all stimulus energy
%   along our current mean

function obj=Constructor(varargin)

%**************************************************************
%Required parameters
%*****************************************************************
%if fielname is required then set a field of req.fieldname=0
%when we read in that parameter we set req.fieldname=1
req.fieldname1=0;
req.fieldname2=0;
%**********************************************************
%Define Members of object
%**************************************************************
% var1          - description
% var2          -description

%declare the structure
obj=struct();


%***************************************************
%Blank Construtor: used by loadobj
%****************************************************
if (nargin==0)
    %instantiate the base class if one is required
pbase=StimChooserObj();
pbase=set(pbase,'glmspecific',false);
obj=class(obj,'ChooseMean',pbase);

    return
end

%**************************************************************************
%Parse the input arguments
%create a new simulation
for j=1:2:nargin
    switch varargin{j}
        %have a case statement for any fieldname which recieves special
        %tratement
        case 'fieldname'
            obj.(varargin{j})=varargin{j+1};
            %****************************************
            %special processing
            %*****************************************
            %check if its required
            if isfield(req,varargin{j})
                req.(varargin{j})=1;
            end
        otherwise
            %*********************************
            %generic handler: set any fields of the structure which are
            %passed in. 
            %Remove this if you don't want user to be able to set any field
            %of the obj structure 
            %*****************************
            if isfield(obj,varargin{j})
                obj.(varargin{j})=varargin{j+1};
                    %check if its required
            if isfield(req,varargin{j})
                req.(varargin{j})=1;
            end
            else          
            error('Constructor:inputarg','%s unrecognized parameter ',varargin{j});
            end
    end
end

%************************************************************
%Check if all required parameters were supplied
        freq=fieldnames(req);
        for k=1:length(freq)
            if (req.(freq{k})==0)
                error('Constructor:missing_arg','Required parameter %s not specified',freq{k});
            end
        end
%**********************************************************************

%*********************************************************
%Create the object
%****************************************
%instantiate a base class if there is one
pbase=StimChooserObj();
pbase=set(pbase,'glmspecific',false);
obj=class(obj,'ChooseMean',pbase);

%if no base object



    