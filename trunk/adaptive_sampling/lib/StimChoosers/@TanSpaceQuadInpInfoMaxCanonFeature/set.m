%function obj=set(obj,fieldname, value,....)
%   
%Explanation: Basic set method for setting the fields of an object.
%
% Tips for customizing this template:
%       1. add error checking.
function obj=set(obj,varargin)

for j=1:2:length(varargin)
    %NOTE: Use fieldnames(obj) to get a list of the fields of the object's
    %structure.
    %the isfield method will not work because obj is a class
    %we could convert it to a structure, using struct.
    %But if field doesn't exist MATLAB throws an error which is what we
    %want
    obj.(varargin{j}) = varargin{j+1};
end