%function ex=exlogdetexss(obj,post,gp, thetadir, thetaproj)
%  post - posterior on param
%   gp   - Gaussian process on the inputs for which we are computing the
%          objective function
%   thetadir - dim(theta)xnsdir - each column is a different sample
%            for the direction of theta
%   thetaproj - nsprojxnsdir - each column is the different samples
%               of the projection of theta along the corresponding
%               direction.
%function ex=exlogdetexss(obj,post,gp, nsampdir, nsampproj)
%   post - posterior on param
%   gp   - Gaussian process on the inputs for which we are computing the
%          objective function
%   nsdir - how man samples of the direction of theta to use
%   nsproj - how many samples of the magnitude to draw
%
%Return value
%   ex-
%   uses monte carlo integration to approximate the expectation
%   E_theta log |E_s Jexp(s,theta) s s'|
%
function ex=exlogdetexss(obj,post,gp, param4, param5)

if (isscalar(param4) && isscalar(param5))
    [thetadir,thetaproj]=samptheta(obj,post,param4,param5);
    nsdir=param4;
    nsproj=param5;
elseif (~isscalar(param4) && size(param5,2)==size(param4,2))
    thetadir=param4;
    thetaproj=param5;
    
    nsdir=size(thetadir,2);
    nsproj=size(thetaproj,1);
else
    error('must either specify nsdir and nsproj or thetadir and thetaproj');
end
    
svals=zeros(nsdir,nsproj);




for dind=1:nsdir    
   svals(:,dind)=logdetexss(obj,thetadir(:,dind),thetaproj(:,dind),gp);    
end

ex=sum(svals(:))*1/(nsdir*nsproj);