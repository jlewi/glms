%function logdetexss(thetadir,thetamag,gp)
%   thetadir - unit vector representing the direction of theta to compute
%              the function for
%   thetamag - projections of theta to compute the funciton for
%   gp       -A StimGP object representing the Gaussian process of the
%              stimulus
%
%Return value
%   ex - 1xlength(thetamag)
%      - contains the values log|E_s Jexp(s'*theta)ss'| for the values of
%      thetmag and thetadir
function ex=logdetexss(obj,thetadir,thetamag,gp)

thetadir=normmag(thetadir);

%compute the rotation matrix
rmat=[thetadir null(thetadir')];


ex=zeros(1,length(thetamag));

%compute the matrices which don't depend on thetamag, only thetadir
alpha=rmat'*gp.cs*rmat;
beta=thetadir'*gp.cs*thetadir;
gamma=rmat'*gp.cs*thetadir;
omega=rmat'*gp.mus;
phi=omega(1);

%marginal distribution on the first component of the input
mu1=gp.mus(1);
var1=gp.cs(1,1);

%compute the constant matrix
%term scaled by E Jexp(s1'theta1^1)
constmat=alpha-1/beta*gamma*gamma'+(omega-1/beta*gamma*phi)*(omega-1/beta*gamma*phi)';


%term scaled by E Jexp(s1'theta1^1) s1
linterm=(omega-1/beta*gamma*phi)*1/beta*gamma';
linterm=linterm+linterm';

%term scaled by E Jexp(s1'theta1^1) s1 s1
quadterm=1/beta^2*gamma*gamma';

lmax=log(realmax);

 
for mind=1:length(thetamag)
    tmag=thetamag(mind);
    %compute the expectation
    %THESE FORMULAS ARE FOR THE CANONICAL POISSON

    %all the terms get multiplied by
    %exp(tmag*mu1+var1*tmag^2/2)    
    %therefore we pull this out of the determinant and take its logarithm
    %b\c this exponential term can easily blow 
    
    %E Jexp(s1'theta1^1) / exp(tmag*mu1+var1*tmag^2/2) 
    exj=1;

    
    %E Jexp(s1'theta1^1) s1/exp(tmag*mu1+var1*tmag^2/2) 

    exjs1=(var1*tmag+mu1);

    %E Jexp(s1'theta1^1) s1 s1/exp(tmag*mu1+var1*tmag^2/2) 
    exjs1s1=((var1*tmag+mu1)^2+var1);


    exss=constmat*exj+linterm*exjs1+quadterm*exjs1s1;

    dexss=det(exss);
    
    %numerical issue
    if isinf(dexss)
        %try computing singular values
        %DO NOT USE SVD b\c SVD can return positive "singular values"
        %but U!=V (i.e the eigenvalue is negative and the negative sign is
        %absorbed intothe V matrix);
        evals=eig(exss);
         ex(mind)=sum(log(evals));
    else
        ex(mind)=log(dexss);
    end
    
    %now add the term due to exp(tmag*mu1+var*tmag^2/2)
    ex(mind)=ex(mind)+(tmag*mu1+var1*tmag^2/2)*gp.dims;
end



