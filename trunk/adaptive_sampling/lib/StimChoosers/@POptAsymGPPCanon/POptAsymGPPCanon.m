%function sim=POptAsymGPPCanon
%
%
% Explanation
%   This class computes the optimal Gaussian process for the input
%   distribution to maximizes the asymptotic objective function.
classdef (ConstructOnLoad=true) POptAsymGPPCanon < StimChooserObj
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        %the dimensions of the input
        %   klength - dim(xt) - length of the stimulus at time t
        %   ktlength - number of stimuli in the joint distribution
        %            - 1 means p(s)=p(x_t)
        klength=0;
        ktlength=0;
       
        
    end

    properties(SetAccess=private,GetAccess=public,Transient)
        %v0tulind stores the linear indexes corresponding to the upper
        %triangular part of the variance matrix gp.c{1}
        %this is used to convert the parameters of the gp into a vector for
        %use with fmincon
        v0tulind=[];
    end
    methods

        function v0tulind=get.v0tulind(obj)

            if isempty(obj.v0tulind)
                %compoute v0tulind
                %get the linear indexes of the upper triangular part
                mat=reshape(1:obj.klength^2,obj.klength,obj.klength);
                lind=triu(mat);
                lind=lind(lind~=0);
                obj.v0tulind=lind;
            end

            v0tulind=obj.v0tulind;
        end
        function obj=POptAsymGPPCanon(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={'klength','ktlength'};



            %determine the constructor given the input parameters
            [cind,params]=Constructor.id(varargin,con);


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    bparams=rmfield(params,{'klength','ktlength'});
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            obj=obj@StimChooserObj(bparams);

            %****************************************************
            %additional object construction after constructing the base
            %class
            %******************************************************
            switch cind
                case 1
                    obj.klength=params.klength;
                    obj.ktlength=params.ktlength;
                    
                case {Constructor.noargs,Constructor.empty,Constructor.emptystruct}
                    %used by load object do nothing
                    bparams=struct();
                case {Constructor.nomatch}
                    error('no constructor matched');
                otherwise
                    error('unexpected value for cind');
            end

            obj.version.POptAsymGPPCanon=081229;
        end

    end
end


