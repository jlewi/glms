%function [stim,varargout]=choosestim(obj,simobj,post)
%   obj - the stimchooserobj
%   simobj - the simulation object
%   post the posterior
%
% Explanation:
%   choose stimuli by sampling a gaussian distribution
%
% Revisions:
%   12-30-2008 - changed the signature fore the choosestim function
%
function [stim,varargout]=choosestim(obj,simobj,post)

mobj=simobj.mobj;
stim=normrnd(zeros(mobj.klength,1),ones(mobj.klength,1)*obj.var^.5);