%function stim=choosestim(sobj,post,shist,einfo,mparam,varargin)
%       finfo - the object
%       post - current posterior
%               .m - mean
%               .c
%       shist - spike history 
%               should be a column vector
%       einfo   - eigendecomposition of the covariance matrix
%       mparam - MParamObj object of model parameters
%
% Return value
%   xmax -
%   oreturn
%   sobj - the stimulus object
%   stimpool - information about the stimulus pool
%            - only returned if number of arguments is >3
%       .muproj
%       .sigma
%Explanation: maximize the mutual information for the canonical poisson
%model
%
%Revision:
%   >1504 - work with spike history as well
function [xmax,oreturn,sobj,varargout]=choosestim(sobj,post,shist,mobj,varargin)

oreturn=[];


magcon=getmag(mobj);

%call the heuristic which constructs the stimulus pool
stimpool=heurmumaxevec(sobj,post,mobj);

if (getdebug(sobj))
    %check magnitudes sum to magcon
    mag=sum(stimpool.^2,1).^.5;
%    if ~isempty(find(abs((mag-sobj.magcon))>10^-8))
    if ~isempty(find(abs((mag-magcon))>10^-8))
        error('Magnitudes not properly normalized');
    end
end
%*****************************************************************
%Compute the Mutual Information 
%********************************************************************
%nstim=size(sobj.stimpool,2);
nstim=getnstim(sobj);

    if (getshistlen(mobj)>0)
        %spike history dependence we need to append spike history to
        %stimulus
        nspikes=shist;
        nspikes=[nspikes;zeros(getshistlen(mobj)-length(nspikes),1)];
        stim=[stimpool;nspikes*ones(1,nstim)];
        
    else

        stim=stimpool;
    end
    stimind=[];

    muproj=getm(post)'*stim;

    % sigma=zeros(1,nstim);
    %4-19-2007
    %try to speed this up using matrix operations
    %   for j=1:nstim
    %      sigma(j)=stim(:,j)'*post.c*stim(:,j);
    %    end
    %each column is the post.c*(:,j)
    sigma=getc(post)*stim;
    sigma=stim.*sigma;
    sigma=sum(sigma,1);

    mi=compmi(sobj.miobj,muproj,sigma);

    %maximize the mi

    [maxmi indmax]=max(mi);



    poolind=indmax;

    xmax=stimpool(:,poolind);
    oreturn.sind=poolind;

%if nargout is > 3 return parameters related to the pool
if (nargout>3)
    
        varargout{1}.muproj= muproj;
        varargout{1}.sigma=sigma;
    
end
