%function rollback(simobj,trial)
%   trial - this is the first trial we want to run when iterate is called
%
%Explanation:
% rollsback the simulation to the appropriate trial
function rollback(sobj,simobj,trial)

%to rollback the stimulus chooser object
%we need to properly set lastbatch based on trial

%batch size=mobj.ktlength;

tpast=trial-1;

%we pick a new stimulus on trials
%trials=[1:mobj.ktlength:end];
%trials=1+mobj.ktlength*n;
n=floor((tpast-1)/simobj.mobj.ktlength);
tlast=n*simobj.mobj.ktlength+1;

if (tlast<sobj.lastbatch.trial)
   sobj.lastbatch.stim=getinput(simobj.inputs,[tlast:tlast+simobj.mobj.ktlength-1]);
   sobj.lastbatch.trial=tlast;
else
    %do nothing lastbatch is already set appropriately
end




