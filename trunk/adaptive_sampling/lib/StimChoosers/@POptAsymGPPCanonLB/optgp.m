%function gp=optgp(obj,post,mobj)
%
%Return Value
%   gp - the optimized gaussian process
%   minfo - the value of our lower bound for this gp
%Explanation:
%   Compute the optimal gaussian process by optimizing the lower bound
%
%  If the model includes spike history and or bias terms then we simply
%  ignore them when optimizing the stimulus
%
%Revisions:
%   02-08-2008
%       fixed bugs
%           1. Wasn't properly computing the magnitude of the mean
%                   when we have bias and spike history
%           2. wasn't properly computing the power along the eigenvectors
%
%   02-04-2008
%       tried to fix numerical issues with convergence
function [gp,minfo]=optgp(obj,post,mobj,statusfid,trial)

if ~exist('statusfid','var')
    statusfid=1;
end
%dims - dimensionality of s
%dims=mobj.klength*mobj.ktlength;
dims=getparamlen(mobj)-mobj.alength-mobj.hasbias;

postm=getm(post);
postc=getc(post);

%extract the marginal distribution of the posterior on just
%the stimulus coefficients

postm=postm(mobj.indstim(1):mobj.indstim(2),1);
postc=postc(mobj.indstim(1):mobj.indstim(2),mobj.indstim(1):mobj.indstim(2));
magmut=(postm'*postm)^.5;

R=postm*postm'+postc;
% [revec,reig]=eig(R);
% reig=diag(reig);
%svd appears more stable
%code later on assumes we use svd becasue we assume
%reig is sorted in descending order
[revec reig]=svd(R);
reig=diag(reig);
%make sure eigenvalues are positive
%ineg=find(reig<0);
if any(reig<0)
    fprintf(statusfid,'Trial %d: optgp.m some of the eigenvalues of R are negative\n',trial);
    error('Trial %d: some of the eigenvalues of R are negative',trial);
end


%*********************************************************************
%Lower bound for lambda
%*******************************************************************
%compute the minimum value for lambda.
%the minimum value for lambda is lambda s.t
%powmus^2=0
%lmininit assume all eigenvalues equal rmax
rmax=max(reig);
rmax=rmax(1);

%compute the difference between the max eigenvalue and each eigenvalue
dreig=rmax-reig;
dreig(1)=0;

%scale the difference in the eignevalues by the power con and the
%imensionality
drs=dreig*dims/(2*obj.powcon);

optimlmin=optimset('gradobj','on','hessian','on','display','off','TolFun',10^-15);


%bneg neg is a value such that the power along mu_t
%would be negative
bneg=1;
opfmin=@(beta)(mupowfrac(beta,drs));

%we find the value dlzero such that
%at lmabda=dlzero+rmax
%the power along the mean would be zero
[bzero,fval,exitflag,output]=fsolve(opfmin,bneg,optimlmin);
switch exitflag
    case {1,2,3}
        %successful

    case {0,-2,-3,-4}

        warning('fsolve did not converge to solution for bzero. Power  frac at bzero=%d. Exited with message:\n %s\n',trial, oflmin(bzero),output.message);
        fprintf(statusfid,'Trial %d: fsolve did not converge to solution for bzero. Power at bzero=%d. \nExited with message:\n %s\n',trial, oflmin(bzero),output.message);
end

if (bzero>bneg)
    error('Trial %d: bzero must be less than bneg=1 \n',trial);
end

if (abs(opfmin(bzero))>100*optimlmin.TolFun)
    wmsg=sprintf('Trial %d: minimum power at bzero is not zero. Power frac at bzero=%d. \n',trial,oflmin(bzero));
    fprintf(statusfid, wmsg);
    warning(wmsg);
end
%**************************************************************
%compute optimal value of Beta
%   we can then use beta to compute the fraction of the power along
%   the mean
%   Basically we are doing a search to find how much power we should place
%   along the mean
%************************************************************
%special case: magmut==0. In this case the mean should be zero
%   so betaopt=bzer
if (magmut<10^-8)
    mus=zeros(dims,1);
    %since magmut = 0. The optimal solution is bzero
    [minfo,powmus,b]=finfolambda(bzero,drs,magmut,obj.powcon,dims,reig);

else
    objfun=@(beta)(fobj(beta,drs,magmut,obj.powcon,dims,reig));

    betamin=0;
    %fminbnd should never evaluate the boundaries (see help for fminbnd on the
    %algorithm. Therefore, fact that objective function isn't defined for inf shouldn't matter)
    %However, the function dhould always be well defined
    [betaopt,negminfo,exitflag,output] = fminbnd(objfun,betamin,bzero,obj.optim);

    switch exitflag
        case {1,2,3}
            %successful

        case {0,-2,-3,-4}

            warning('fminbnd did not converge. Exited with message:\n %s\n',output.message);
            fprintf(statusfid,'fminbnd did not converge. Exited with message:\n %s\n',output.message);
    end

    [minfo,powmus,b]=finfolambda(betaopt,drs,magmut,obj.powcon,dims,reig);

    if (powmus<-100*optimlmin.TolFun)
        error('Trial %d: powmus should not be negative',trial );
    elseif (powmus<0 && -100*optimlmin.TolFun<powmus )
        %powmus is essentially zero so force it to be zero
        powmus=0;

    end

    mus=powmus^.5*postm/magmut;
end
gp=StimGP('mu',mus,'c',{revec*diag(b)*revec'});


if (abs((mus'*mus+sum(b)-obj.powcon)/obj.powcon)>10^-8)
    error('Trial %d: The optimal gaussian process violates the power constraint.', trial);
end


%function negminfo
function negminfo=fobj(beta,drs,magmut,powcon,dims,reig)

%multiple by negative one because we are using fmin
negminfo=-1*finfolambda(beta,drs,magmut,powcon,dims,reig);


%compute the optimal Cs and mus as a function of lambda
%   drs     - values needed to compute what fraction of the power should be
%             devoted to the mean
%   reig   - the eignevalues of (mu_t mu_t' + C_t)
%   magmut - magnitude of the mean of the posterior
%   powcon - The power constraint
%   dims   - dimensionality of s (i.e the Gaussian process)
%Return value:
%   minfo - value of our objective function
%   powmus - the optimal value for projection along the mean
%   b      - eigenvalues of the covariance matrix of our gp
function [minfo,varargout]=finfolambda(beta,drs,magmut,powcon,dims,reig)



powmus=powcon*mupowfrac(beta,drs);


%compute the eigenvalues of oru gp
b=powcon*beta./(1+beta*drs);
minfo=dims*magmut*powmus^.5+dims/2*(b'*reig)+sum(log(b));

if (nargout>=2)
    varargout{1}=powmus;
end
if (nargout>=3)
    varargout{2}=b;
end

%function [pfrac,varargout]=mupowfrac(beta,drs);
%   beta - value of beta
%
%
%
%Return value:
%   prfrac - The fraction of the power that should be distributed to the
%   MAP
%   dfrac - the gradient
%   d2frac - the hessian
function [pfrac,varargout]=mupowfrac(beta,drs)

pfrac=1-sum(beta./(1+beta*drs));


if (nargout>=2)
    dfrac=sum(-1./(1+beta*drs).^2);
    varargout{1}=dfrac;
end

if (nargout>=3)
    d2frac=sum(2*drs./(1+beta*drs).^3);
    varargout{2}=d2frac;
end

