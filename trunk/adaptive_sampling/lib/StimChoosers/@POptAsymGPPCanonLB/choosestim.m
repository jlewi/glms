%function [stim]=choosestim(obj,simobj,post)
%   obj - the stimchooserobj
%   simobj - the simulation object
%   post the posterior
%
%Return value
%   stim - the stimulus
%
% Explanation: Select the stimulus by sampling the Gaussian process.
%  if mobj has a temporal receptive field then we only optimize the
%  stimulus ever ktlength trials. On the other trials we just pick the
%  stimuli
function [stim]=choosestim(obj,simobj,post)

trial=simobj.niter+1;
mobj=simobj.mobj;
%we need to get past values of the stimuli if they are needed
%we then compute the appropriate Gaussian conditioning formulas.
% if (mobj.ktlength>1)
%     tlast=simobj.niter;
%     if (simobj.niter-(mobj.ktlength-1)>=0)
%         sr=simobj.sr(tlast-(mobj.ktlength-1)+1:tlast);
%         spast=getinput(sr);
%
%     else
%         if (tlast>0)
%             sr=simobj.sr(1:tlast);
%             spast=getinput(sr);
%
%         else
%             spast=[];
%         end
%     end
% else
%     spast=[];
% end

spast=[];
%determine if we need to draw the stimulus from a gaussian distribution
rtrial=trial-obj.lastbatch.trial+1;
if (mobj.ktlength==1 || rtrial>mobj.ktlength || isempty(obj.lastbatch.stim))

    %11-08-2009. Don't optimize the gp after every trial. Instead optimize
    %it every obj.gpupdateint trials
    if ((trial-obj.gplastupdatetrial)>=obj.gpupdateint)
        [gp,minfo]=optgp(obj,post,simobj.mobj,simobj.statusfid,trial);
         %save the gp

        obj.gp=gp;
   
        obj.gplastupdatetrial=trial;
    else
       minfo=nan; 
    end
        
    
   
    stim=samppcon(obj.gp,spast);
    if (isa(mobj,'MBSFTSep'))
       %the stimulus will be specified in terms of the fourier coefficients
       %therefore we need to project it into the stimulus domain

       stim=tospecdom(mobj,stim);
    end
    obj.lastbatch.stim=reshape(stim,[mobj.klength mobj.ktlength]);
    obj.lastbatch.trial=trial;

    rtrial=1;
else
    minfo=nan;
end


stim=obj.lastbatch.stim(:,rtrial);

%store the value of minfo obtained for the distribution on this trial

if (length(obj.minfo)<simobj.lasttrial)
    allminfo=obj.minfo;
    obj.minfo=nan(1,simobj.lasttrial);
    obj.minfo(1:length(allminfo))=allminfo;
end

obj.minfo(trial)=minfo;



%rescale stimulus so it has the magnitude specified by the mparam obj
if (obj.normalize)
    stim=stim/(stim'*stim)^.5*simobj.mobj.mmag;
end

