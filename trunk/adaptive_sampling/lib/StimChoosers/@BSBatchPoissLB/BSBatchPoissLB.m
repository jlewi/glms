%function sim=BSOptStim('bdata')
%   bsize - size of the batch
%
%
%
%Optional Parameters:
%   windexes - wave files to use. Only trials from these wave files will be
%             selected
%   micomp    - String describing how to compute the mutual info
%              - 'approx' - use approximation log(1+x)~x
%              - 'numerical' - use numerical integration to compute the
%              expectation
%
%Explanation: A class to choose the optimal stimulus from the bird song
%data. This object does not use any heuristics for selecting a subset of
%the stimulus pool. Instead it computes the mutual information for all
%possible stimuli.
%
%We also save the mutual information for each stimulus in the pool to a
%file
%
%This class assumes we are dealing with the canonical Poisson
%This object assumes we are working
%Revisions:
%   09-21-2008 - Make the object a child of BSBatchBase instead of
%      BSStimchooser
%      binfo, bsize, windexes, nrepeats, are now defined as part of
%        BSBatchBase
%   080828 - Initiliaze bindex to nan to indicate its not set
classdef (ConstructOnLoad=true) BSBatchPoissLB < BSBatchBase



    %bspost - a BSLogPOst object
    %
    %
    properties(SetAccess=private,GetAccess=public)
        version=080921;
        miobj=[];
        bspost=[];
    end

   
    methods
        %returns a structure with fields windex, repeat, and trial
        %which identify the start of the batch.
        [bstart, blogminfo]=choosebatch(obj,simobj,post);
        
        function obj=BSBatchPoissLB(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            %The blank constructor will be used by the base class when
            %loading an object
            con(1).rparams={};
            con(1).cfun=1;

            con(2).rparams={'bdata'};
            con(2).cfun=1;



            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=[];
                    bparams=[];
                    cind=1;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
                    bparams=params;

                    %remove anyparamaters to be processed by this class

                    if isfield(bparams,'bdata')
                        bparams=rmfield(bparams,'bdata');
                    end

                    if isfield(bparams,'micomp')
                        bparams=rmfield(bparams,'micomp');
                    end
            end

            if (length(cind)>1)
                cind=cind(end);
            end

            %call constructor for the base class
            obj=obj@BSBatchBase(bparams);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    %do nothing                    
                case 2

                    %data=load(getpath(params.datafile));
                    obj.bspost=BSlogpost('bdata',params.bdata);

                otherwise
                    error('Constructor not implemented')
            end


            if isfield(params,'micomp')
                switch lower(params.micomp)
                    case 'approx'
                        obj.miobj=PoissExpCompMI();
                    case 'numerical'
                        obj.miobj=CompMINum('glm',GLMPoisson('canon'));
                    otherwise
                        error('Unrecognized value for micomp.');

                end
            else
                obj.miobj=PoissExpCompMI();
            end


        end
        %Return Value:
        %     ntrials - the number of trials we have data for
        function ntrials=getntrials(obj)
            %we need to compute the number of trials in the allowed indexes
            nwindex=length(obj.windexes);

            ntrials=0;
            for wind=1:nwindex

                [ntrialsinwave]=getntrialsinwave(obj.bdata,obj.windexes(wind));
                ntrials=ntrials+ntrialsinwave*obj.nrepeats;
            end

        end

        %get the bdata object stored in bpost
        function bdata=getbdata(obj)
            if ~isempty(obj.bspost)
                bdata=obj.bspost.bdata;
            else
                bdata=[];
            end
        end
    end
end



