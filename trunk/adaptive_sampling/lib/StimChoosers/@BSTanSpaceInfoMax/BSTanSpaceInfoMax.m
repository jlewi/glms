%function sim=BSTanSpaceInfoMax('bdata','miobj')
%
%Parameters for base class (see BSOptStimNoHeur)
%Optional Parameters:
%
%We also save the mutual information for each stimulus in the pool to a
%file
%
%Revisions:
%   09-02-2008
%       add a separate method,comppost, to get the posterior on the tangent
%       space from the full posterior when appropriate.
classdef (ConstructOnLoad=true) BSTanSpaceInfoMax < BSOptStimNoHeur
    %  iterfullmax - how often we do an infomax step using the full posterior
    %  startfullmax - how many trials at start do we do full info. max.
    %                 before switching to the tangent space.
    properties(SetAccess=private,GetAccess=public)
        aversion=080902;
        iterfullmax=inf;
        startfullmax=0;
    end


    methods
        post=comppost(obj,simobj,post);

        function obj=BSTanSpaceInfoMax(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};




            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************

                    cind=1;
                    params=[];
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);


            end


            %remove any fields for this class and call constructor for base
            %class
            bparams=params;
            if isfield(params,'iterfullmax')
                bparams=rmfield(bparams,'iterfullmax');
            end
            if isfield(params,'startfullmax')
                bparams=rmfield(bparams,'startfullmax');
            end

            obj=obj@BSOptStimNoHeur(bparams);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 0
                    %do nothing for loading objects
                    return
                case 1
                otherwise
                    %do nothing match any constructor
            end

            %process optional parameters
            if isfield(params,'iterfullmax')
                obj.iterfullmax=params.iterfullmax;
            end
            if isfield(params,'startfullmax')
                obj.startfullmax=params.startfullmax;
            end

        end
    end
end



