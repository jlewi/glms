%function post=comppost(obj,simobj,post)
%   obj      - BSTanSpaceInfoMax object
%   post     - full post or PostTanSpace object
%
%Return value:
%   pinfo - A Gauss Post object. The actual Gaussian posterior to use to
%   compute the mutual information.
%
% Revisions:
%   11-03-2008 - change how we compute the interval at which we do fullmax
function pinfo=comppost(obj,simobj,post)


%check whether we are doing infomax on the full posterior or tangent space
%add 1 to niter b\c niter is how many trials we've already run
if ((isinf(obj.iterfullmax) || mod(simobj.niter+1,obj.iterfullmax+1)~=0) && (simobj.niter+1>obj.startfullmax))


    %we need to construct PostTanSpace object
    if ~isa(post,'PostTanSpace')
        warning('Posterior is not of type PostTanSpace. Constructing PostTanSpace from full posterior.');
        post=PostTanSpace(post,simobj.mobj);
    end

    %3-11-2008
    %we need to create the appropriate mean and covariance matrix for use with
    %our method
    fpost=getfullpost(post);
    tanpost=gettanpost(post);
    tanpt=post.tanpoint;
    basis=tanpt.basis;

    minfo=getm(fpost)+basis*getm(tanpost);
    cinfo=basis*getc(tanpost)*basis';
    %compute eigendecomp of cinfo because we will need it for the stimulus
    %optimization.
    pinfo=GaussPost('m',minfo,'c',cinfo);

else

    %info. max on full space set posterior to full posterior
    if isa(post,'PostTanSpace')
    pinfo=getfullpost(post);
    else
    pinfo=post;
    end
end