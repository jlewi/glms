%function optdist(obj,post,psinit,glm,nsamp)
%   post - posterior on theta
%   nsamp - how many samples of theta to use
%
%function optdist(obj,post,psinit,glm,theta)
%   post - posterior on theta
%   theta samples of theta to use
%
%   gpinit (optional) - initial guess for the gaussian process
%Return value:
%   fval - This is the negative of the funciton we want to maximize because
%           we are using fmincon but we want to find the maximum.
%Explanation: Use fmincon to find the optimal Gaussian process
function [psopt,fval,exitflag,output,theta]=optdist(obj,post,psinit,glm,param4)


if ~exist('psinit','var')
    psinit=[];
end

%generate samples of theta
%we use the same samples on all iterations of fmincon because otherwise
%changing the samples will cause the value of the function to change
if (isscalar(param4) )
    nsamp=param4;
    theta=mvgausssamp(getm(post),getc(post),nsamp);

elseif (~isscalar(param4) )
    theta=param4;
    nsamp=size(theta,2);
else
    error('must either specify nsdir and nsproj or thetadir and thetaproj');
end


%initialize psinit to a uniform distribution
if isempty(psinit)
    psinit=StatDistDisc('stim',obj.stim,'ktlength',obj.ktlength);
end

%********************************************************
%Error checking
%*********************************************************
if (size(theta,1)~=psinit.dims)
    error('Theta and s do not have correct dimensions');
end

A=[];
b=[];

%equality constratints enforce the constraint that the distribution is
%stationary and that it sums to 1;
Aeq=[psinit.statcon;ones(1,psinit.nstim^psinit.ktlength)];
beq=[zeros(size(psinit.statcon,1),1);1];

%probabilities must be positive
lb=zeros(psinit.nstim^psinit.ktlength,1);
ub=[];


nlcon=[];


%we pass psinit to our objective function.
%We use psinit to store the s and ss matrices
%which we use to evaluate our objective function
%this way we can avoid recomputing them.
psopt=StatDistDisc('ps',psinit);
fobj=@(x)(ofun(obj,post,x, theta,glm,psopt));

obj.optim=optimset(obj.optim,'GradObj','on');
obj.optim=optimset(obj.optim,'Display','iter');
obj.optim=optimset(obj.optim,'Hessian','on');
obj.optim=optimset(obj.optim,'Algorithm','Interior-point');
[xopt, fval,exitflag,output]= fmincon(fobj,psinit.p(:),A,b,Aeq,beq,lb,ub,nlcon,obj.optim);
psopt.p=reshape(xopt,ones(1,obj.ktlength)*psopt.nstim);


%the function to minimize
function [f varargout]=ofun(obj,post,x, theta, glm, pstim)
pstim.p=reshape(x,ones(1,obj.ktlength)*pstim.nstim);

switch nargout
    case 0
        ex=exlogdetexss(obj,post,pstim, glm, theta );
    case 1
        ex=exlogdetexss(obj,post,pstim, glm, theta );
    case 2
        [ex,dex]=exlogdetexss(obj,post,pstim, glm, theta );
        varargout{1}=-1*dex;
    case 3
        [ex,dex,d2ex]=exlogdetexss(obj,post,pstim, glm, theta );
        varargout{1}=-1*dex;
        varargout{2}=-1*d2ex;
end

f=-1*ex;




