%function [sind]=choosestim(obj,simobj,post)
%   obj - the stimchooserobj
%   simobj - the simulation object
%   post the posterior
%
%Return value
%   sind - the index into the stimulus pool of the stimulus
%
% Explanation: Select the stimulus by sampling the Gaussian process.
%
function [sind]=choosestim(obj,simobj,post)

trial=simobj.niter+1;
mobj=simobj.mobj;
%we need to get past values of the stimuli if they are needed
%we then compute the appropriate Gaussian conditioning formulas.
if (mobj.ktlength>1)
    tlast=simobj.niter;
    if (simobj.niter-(mobj.ktlength-1)>=0)
        sr=simobj.sr(tlast-(mobj.ktlength-1)+1:tlast);
        spast=getinput(sr);

    else
        if (tlast>0)
            sr=simobj.sr(1:tlast);
            spast=getinput(sr);

        else
            spast=[];
        end
    end


    %generate samples of theta
    theta=mvgausssamp(getm(post),getc(post),obj.nthetasamps);

    %do gradient ascent to optimize ps
    nsteps=100;
    eta=.1;

    [psopt,fopt,exitflag,output,theta]=optdistnull(obj,post,obj.ps,mobj.glm,theta);

        switch exitflag
        case {1,2,3}
            %successful
           
        case {0,-2,-3,-4}
           
            fprintf(simobj.statusfid,'Trial %d: POptAsymDistDisc optdistnull did not converge. \n Exited with messag: \n %s \n',simobj.niter,output.message);
    end
    if isempty(obj.ps)
        %initialize ps
        obj.ps=StatDistDisc('stim',obj.stim,'ktlength',mobj.ktlength);
    end
  
    
    %store the value of fopt obtained for the distribution on this trial

    if (length(obj.fopt)<simobj.lasttrial)
        allfopt=obj.fopt;
        obj.fopt=nan(1,simobj.lasttrial);
        obj.fopt(1:length(allfopt))=allfopt;
    end

    obj.fopt(trial)=fopt;

    obj.ps=psopt;
    sind=samppcon(obj.ps,spast);

else
    error('Need to write code to just sample the distribution');
end

sind=GLMStimIndex('index',sind);