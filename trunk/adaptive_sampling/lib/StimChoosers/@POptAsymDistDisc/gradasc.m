%function fopt=gradasc(obj,ps,nsteps,post,glm,theta)
%   ps - the initial (and final) distribution
%
%Return value
%   None - since ps is a handle we modify that object directly
%Explanation: Take the specified number of gradient steps to try to
%   optimize the distribution
function fopt=gradasc(obj,psopt,nsteps,post,glm,theta)

[fopt dfopt]=exlogdetexss(obj,post,psopt, glm, theta );
fprintf('Step 0: fopt=%d \n',fopt);

eta=.01;



for sind=1:nsteps
    %project derivative onto null space
    db=psopt.lpmat*dfopt';
    psopt.p=psopt.p(:)+eta/sind*db;

    %find all entries =0 get forced to zero
    ind=find(psopt.p<0);
    if ~isempty(ind);
       fprintf('Step %d: some entries are negative \n', sind);
        psopt.p(psopt.p<0)=0;
    end

    
    if ~isnormalized(psopt)
        fprintf('Step %d: no longer normalized \n', sind);
        psopt.p=psopt.p/sum(psopt.p(:));
    end
    
    [fopt dfopt]=exlogdetexss(obj,post,psopt, glm, theta );
    fprintf('Step %d: fopt=%d \n',sind,fopt);

    %check stationarity constraints
    if ~isstationary(psopt)
        fprintf('Step %d: stationarity violated \n',sind);
        %project it onto the nullspace
        psopt.p=psopt.lpstatmat*psopt.p(:);

        %normalize
        psopt.p=psopt.p/sum(psopt.p(:));
        if ~isstationary(psopt)
            error('Even after projecting distribution onto null space it is not stationary.');
        end
    end
end