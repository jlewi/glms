%function logdetexss(obj,theta,pstim,glm)
%   theta - theta
%   pstim - StatDistDisc object representing the stimulus distribution
%   glm - the GLMModel - need it to compute Jexp
%
%Return value
%   ex - the expected value
%   dex - gradient w.r.t to pstim.p
%   d2eq - the hessian
function [ex,varargout]=logdetexss(obj,theta,pstim,glm)

if (nargout>=2)
    compdex=true;
else
    compdex=false;
end
if (nargout>=3)
    compd2ex=true;

else
    compd2ex=false;
end

pdim=pstim.ns;
%compute jexp
j=jexp(glm,theta'*pstim.s);

%compute E_s Jexp s s';
exss=pstim.ss*(j'.*pstim.p(:));

%exss is a vector containing the upper triangular part so we need
%to reshape it
%linear indexes of upper triangular part
ltriu=triu(reshape(1:pstim.dims*pstim.dims,pstim.dims,pstim.dims));
ltriu=ltriu(ltriu~=0);


exmat=zeros(pstim.dims,pstim.dims);
exmat(ltriu)=exss;
exmat=exmat+triu(exmat,1)';

ex=log (det(exmat));

if (compdex)

    %compute the gradient
    dex=zeros(pstim.nstim^obj.ktlength,1);

    iexmat=inv(exmat);

    %we want to compute sum_rows,cols imat .* J_i s_i s_i' (multiplication
    %is elementwise)
    %We can rewwrite this as
    % vec(imat)'* vec(j_i s_i s_i')=j_i* vec(imat)'* vec( s_i s_i')
    %
    % the upper triangulagular part of s_i s_i is alrady stored in pstim.ss
    %so we double the upper triangular part of iexmat and then just take
    %the upper triangular part. we can then just multiply by iexmat
    i2exmat=iexmat+triu(iexmat,1);
    iexut=i2exmat(ltriu);

    dex=(iexut'*pstim.ss).*j;

    varargout{1}=dex;
end


ssmat=pstim.ssmat;
if (compd2ex)
    d2ex=zeros(pdim,pdim);

    %only fill the upper triangular part because its symettric
    for i=1:pdim
        iimat=iexmat*ssmat{i}*iexmat;
        for j=i:pdim
            smat=ssmat{j};
            d2ex(i,j)=-iimat(:)'*smat(:);
        end
    end

    %flip the upper triangular part
    d2ex=d2ex+triu(d2ex,1)';

    varargout{2}=d2ex;
end


