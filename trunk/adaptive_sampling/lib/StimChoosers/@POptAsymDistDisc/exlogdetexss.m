%function ex=exlogdetexss(obj,post,pstim, glm, theta)
%  post - posterior on param
%   pstim - StatDistDisc object representing the distribution on the
%          stimuli
%   theta - samples of theta. drawn from the posterior distribution 
%           theta must have more than 1 dimension
%           otherwise function assumes parm 4 is nsamp
%
%function ex=exlogdetexss(obj,post,pstim,glm, nsamp)
%   post - posterior on param
%   pstim - StatDistDisc object representing the distribution on the
%          stimuli
%   nsamp - how many samples of theta to use
%Return value
%   ex-
%   uses monte carlo integration to approximate the expectation
%   E_theta log |E_s Jexp(s,theta) s s'|
%
%   dex - the derivative
%   d2ex - the Hessian
function [ex,varargout]=exlogdetexss(obj,post,pstim, glm, param4 )

if (isscalar(param4) )
    nsamp=param4;
    theta=mvgausssamp(getm(post),getc(post),nsamp);

elseif (~isscalar(param4) )
    theta=param4;
    
nsamp=size(theta,2);
else
    error('must either specify nsdir and nsproj or thetadir and thetaproj');
end
    
svals=zeros(1,nsamp);

if (nargout>=2)
    compderv=true;
else
    compderv=false;
end

if (nargout >=3)
    comphess=true;
else
    comphess=false;
end

pdim=pstim.nstim^pstim.ktlength;
if (compderv)
   dex=zeros(nsamp,pdim); 
end

if (comphess)
    d2ex=zeros(pdim,pdim,nsamp);
end


for dind=1:nsamp    
    switch nargout
        case 0
                   svals(:,dind)=logdetexss(obj,theta(:,dind),pstim,glm);  
        case 1
       svals(:,dind)=logdetexss(obj,theta(:,dind),pstim,glm);  
        case 2 
        [svals(:,dind), dex(dind,:)]=logdetexss(obj,theta(:,dind),pstim,glm);  
        case 3
            
        [svals(:,dind), dex(dind,:),d2ex(:,:,dind)]=logdetexss(obj,theta(:,dind),pstim,glm); 
    end
end

ex=sum(svals(:))*1/(nsamp);

if (compderv)
   varargout{1}=sum(dex,1); 
end

if (comphess)
   d2ex=sum(d2ex,3);
   varargout{2}=reshape(d2ex,[pdim,pdim]);
end