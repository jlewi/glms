%function [pg]=gposteriorglm(prior,x,obsrv,fu)
%       prior - structure representing the gaussian prior
%              .m - mean
%              .c - covariance matrix
%       x     - column vector representing the stimulus
%       fu    - pointer to the nonlinear function which computes
%               the rate as a fcn of u=k'x
%       obsrv - struture representing the observations
%              .n - number of spikes
%              .twindow -window in which spikes are occured
%       k     - structure providing information about
%               the model parameters
%             .d      -dimensionality of the k vector
%Return value:
%   pg - gaussian representing the updated posterior
%       .m - mean
%       .c - covariance
%
%Explanation:
%   Updates the posterior for a glm by matching its 1st and 2nd moments
%   assuming posterior is represented as a gaussian.
%
%   The response/observations (obsrv) is the number of spikes produced by a
%   homogenous proisson process in the window of length twindow.
%   This assumes batch processing. That is we the response observed in the
%   window of length twindow is due entirely to the stimulus presented at
%   x;
%   
%   The points at which to compute the 1-d numerical integration
%   are determined by looking at evenly spaced points in a window
%   surrounding the mean. The window is determined using the standard 
%   deviation.
function [pg]=gposteriorglm(prior,x,obsrv,fu)

%number of upts to use
numupts=1000;

%uvec- unit vector representing the dimension along which we do the update
uvec=x/(x'*x)^.5;

%Compute the prior variance and mean along the dimension x
%eqns 11-16
m0=x'*prior.m;
v0=x'*prior.c*x;

%construct an orthonormal basis to uvec
D=eye(length(x),length(x))-x*(x'*eye(length(x),length(x))/(x'*x));
D=orth(D);
vd=D'*prior.c*D;

%debugging construct an orthonormal basis perpendicular to x
%assume 2d
%d=[-x(2) x(1)]';
%d=d/(d'*d)^.5;
%variance along d
%vd=d'*prior.c*d;

%compute the range for the numerical integration
%The range is taken as -4 std to 4 std
upts=linspace(m0-4*v0^.5,m0+4*v0^.5,numupts);
du=upts(2)-upts(1);
r=fu(upts);

%compute the likelihood of the spiketrains under the different
%rates r
%we don't bother calculating the factorial in the denominator
%of the poisson likelihood because we absorb it into the normalization
%of the posterir because it will not depend on the different models
%pobsrv=exp(-r*obsrv.twindow).*(r*obsrv.twindow).^(obsrv.n);

%compute the log of probability
%lpobsrv=-r*obsrv.twindow+obsrv.n*log(r*obsrv.twindow);

%llobsrv is the exact likelihood so we need the facotrial term
%llobsrv=lpobsrv-sum(log(1:obsrv.n));

%for debugging
lpobsrv=hpoissonll(obsrv.n,r,obsrv.twindow);
lpobsrv=lpobsrv';
llobsrv=lpobsrv;

%find any elements of pobsrv which are NAN and set to zero
%ind=find(isnan(pobsrv)==1);
%pobsrv(ind)=1;

%*************************************
%numerical integration
%*************************************
%compute the normalization constant
%log of the posterior as function of u
lpostact=-(upts-m0).^2/(2*v0)+lpobsrv;

%these numbers can be very large 
%or very small. Either way this scaling factor drops out when we normalize
%scaling the probability's is equivalent to adding a constant to the
%log of the likelihoods (see my notes)
lpost=lpostact;
%lpost=lpostact-max(lpostact);
%only divide by sc if sc>0
%b\c if sc is close to 0 we just blow up to infinity when we divide by sc
%if sc>0
%    lp=lp./sc;
%end
%if you choose to use lp make sure you use lp in the calculation for the
%mean and the variance
%z=exp(lp)*du;

%multiplying by du might be causing the problem. 
z=exp(lpost)*du;
z=sum(z);

if (z==0)
    error('Normalization Constant Zero in gposteriorglm');
end

%update the mean
m1=sum(exp(lpost).*upts*du);
m1=m1/z;


if ~(isreal(m1))
    error('New 1d mean not real');
end
%update the variance
v1=sum(exp(lpost).*(upts-m1).^2*du);
v1=v1/z;

if (v1<0)
    error('New 1d variance is negative');
end

if ~(isreal(v1))
    error('New 1d variance not real');
end

pg.m=prior.m+(m1-m0)*x/(x'*x);
%pg.c=prior.c+(v1-v0)*x*x'/(x'*x)^2;

%c1=prior.c+(v1-v0)*x*x'/(x'*x)^2;
%compute it using the orthogonal martrix
%pg.c=v1*x*x'/(x'*x)^2+vd*d*d';

pg.c=v1*x*x'/(x'*x)^2+D*vd*D';

%if (pg.c >prior.c)
%    error('New Variance is larger');
%end

%compute the least mean squared error beteween the two methods
%lse=(pg.c-c1).^2;
%sum(lse(:));
%fprintf('lse=%0.5g \t',sum(lse(:)));

%check decomposition of the prior
%v0d=uvec'*prior.c*uvec;
%c0d=v0d*(uvec*uvec')+vd*d*d';
%keyboard;
%debugging purposes
%ensure eignevalues are positive to check its positive definite
ev=eig(pg.c);
if ~isempty(find(ev<0))
    error('New Covariance matrix has negative eigen values');
end

if isnan(pg.m)
    error('Updated mean not a number');
end
if isnan(pg.c)
    error('Updated covariance matrix not a number');
end

