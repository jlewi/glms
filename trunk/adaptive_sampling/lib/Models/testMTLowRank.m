%08-07-30
%
%Test harness to test basic functionality of my classes for Low Rank
%tangent space approximations.
%
%This test mainly looks for syntactical errors which would cause an error
function testMTLowRank
%start by defining a model object
trank=2;
glm=GLMPoisson('canon');

strfdim=[10 5];

%**************************************************************************
%no spike history
alength=0;

mobj=MTLowRank('glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',1,'rank',trank);

testmantotheta(mobj);
testthetatoman(mobj);

%***********************************************************************
%with spike history
%***********************************************************************
alength=4;

mobj=MTLowRank('glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',1,'rank',trank);

testmantotheta(mobj);
testthetatoman(mobj);

%**************************************************************************
%test that the gradient of the manifold has the appropriate block matrix
%structure with
alength=4;
mobj=MTLowRank('glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',1,'rank',trank);

testgradstructure(mobj);
%check upper right

fprintf('Success \n');

function testgradstructure(mobj)
%test that the gradient of the manifold has the block matrix structure with
%respect to the spike history and bias terms that we expect
%i.e grad=[dstrf 0; 0 I]  dstrf is the gradients with respect to the 
%parameters of the SVD. I is an identity matrix whos dimensions = the
%number of spike history terms + the number of bias terms

%the gradient won't actually have this block structure because we run orth
%on the gradient
%therefor we just need to check that each of the vectors in [0;I] lives
%entirely in the column space of our basis
%we do this by constructing the null space of grad.basis' 
%we then check that [0;I] is orthogonal to this space. 

%generate some random theta
theta=randn(getparamlen(mobj),1);

tp=comptanpoint(mobj,theta);

nullbasis=null(tp.basis');

%project [0;I] onto this vectors
bshist=[zeros(mobj.klength*mobj.ktlength,mobj.alength+mobj.hasbias);eye(mobj.alength+mobj.hasbias)];

pj=nullbasis'*bshist;

%compute the energy in each project
ep=sum(pj.^2,1).^.5;

if (any(abs(ep)>10^-10))
    error('Basis vectors for spike history and bias term do not appera to be in the tangent space');
end

%**************************************************************************
%Test projection of theta onto Manifold
%***********************************************************************
function testthetatoman(mobj)
u=normrnd(zeros(mobj.klength,mobj.rank),ones(mobj.klength,mobj.rank));
u=orth(u);
v=normrnd(zeros(mobj.ktlength,mobj.rank),ones(mobj.ktlength,mobj.rank));
v=orth(v);
s=normrnd(zeros(mobj.rank,1),ones(mobj.rank,1));

stimcoeff=u*diag(s)*v';
if (mobj.alength>0)
shist=rand(mobj.alength,1);
else
    shist=[];
end
if hasbias(mobj)
    bias=rand(1,1);
else
    bias=[];
end


theta=packtheta(mobj,stimcoeff(:),shist,bias);

%project theta onto the manifold
manp.manp=proj(mobj,theta);

[manp.u,manp.s,manp.v,manp.shist,manp.bias]=vectouv(mobj,manp.manp);

%check values match
%we reconstruct the matrix because its possible we flip the sign of one of
%the basis vectors
manp.stimcoeff=manp.u*diag(manp.s)*manp.v';

dstimcoeff=abs(manp.stimcoeff-stimcoeff)./abs(stimcoeff);
if any(dstimcoeff(:)>=10^-10)
    error('Matrix reconstructed from manifold is not correct');
end

if any(manp.shist~=shist)
    error('shist for projection of theta is invalid');
end

if any(manp.bias~=bias)
    error('bias for projection of theta is invalid');
end
%**************************************************************************
%Map a Low Rank Matrix to theta
%***********************************************************************
function testmantotheta(mobj)
u=normrnd(zeros(mobj.klength,mobj.rank),ones(mobj.klength,mobj.rank));
u=orth(u);
v=normrnd(zeros(mobj.ktlength,mobj.rank),ones(mobj.ktlength,mobj.rank));
v=orth(v);
s=normrnd(zeros(mobj.rank,1),ones(mobj.rank,1));

shist=rand(mobj.alength,1);
if hasbias(mobj)
    bias=rand(1,1);
else
    bias=[];
end

manp=uvtovec(mobj,u,s,v,shist,bias);
theta.theta=mantotheta(mobj,manp);

%***********************************************
%check it matches
%***********************************************
[theta.stimcoeff theta.shistcoeff theta.bias]= parsetheta(mobj,theta.theta);

truemat=u*diag(s)*v';
if (any(theta.stimcoeff~=truemat(:)))
    error('Mapping from manifold to theta does not return correct stimulus matrix');
end


if (any(theta.shistcoeff~=shist))
    error('Mapping from manifold to theta does not return correct spike history coefficents');
end

if (any(theta.bias~=bias))
    error('Mapping from manifold to theta does not return correct bias');
end