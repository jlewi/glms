%function tinp=projinp(mparam,inp,sr)
%       mparam- the MParamObj object
%       inp         -object array of stimuli
%                   -should be GLMInput objects
%       sr    - vector of spike history most recent observation is first
%
% Return value
%       mxn - the input
% Explanation:
%   This function constructs the input to the glm. That is it combines the
%   following:
%   1. the stimulus
%   2. Past responses
%   3. past stimuli
%   4. nonlinear projections of the input (using the subclass
%   MGLMNonLinInp)
%   5. Fixed terms in the input
%
% Revision History:
%   04-29-2008 - Changed this function to be consistent 
%   10-25-2007 - inp can be a matrix because projinp might be called
%       by MNonLinInp after pushing inputs through a nonlinearity
%   10-19-2007 - include spike history terms
%
%   10-19-2007 - include spike history terms
function tinp=projinp(mobj,inp,shist)
%1. if inp is an object array call getData to create an array
%2. if we have spike history terms add them
%3. add a bias/ fix terms as necessary

if isa(inp,'GLMInput')


    tinp=getData(inp);
else
    tinp=inp;
end

if (getshistlen(mobj)>0)
%    if ~isempty(sr)
%        shist=getobsrv(sr)';
%    end
    shist=[zeros(getshistlen(mobj)-length(shist),1);shist];
else
    shist=[];
end


%add a bias term
tinp=[tinp;shist;1];
