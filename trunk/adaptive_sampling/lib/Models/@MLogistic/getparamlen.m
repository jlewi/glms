%function d=getparamlen(mobj)
%   mobj -MParamObj
%
%Return value
%   d - length of the actual parameters
%
%Explanation: Returns the length of the actual parameters of the vector for
%the GLM
function d=getparamlen(mobj)
    %add one for the bias term
    d=getklength(mobj)*getktlength(mobj)+getshistlen(mobj)+1;
