%function getrank=rank(obj)
%	 obj=MBiLinear object
% 
%Return value: 
%	 rank=obj.rank 
%
function rank=getrank(obj)
	 rank=obj.rank;
