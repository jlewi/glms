% Explanation: Template function for dereferencing a user defined object
%
%   obj - pointer to object
%   index -
%           .type 
%           .subs
% 2-2-07:
%   check if field is member of base class.
function [out] = subsref(obj,index)

% SUBSREF Define field name indexing for GLModel objects
switch index(1).type
    case '.'
     
        %*************************************************
        %Special Handlers
        %***************************************************

        %*************************************************
        %Generic Handlers
        %***********************************************
        %check if its field of base class
        if ~isempty(strmatch(index(1).subs,fieldnames(obj)))
            %field is member of this structure
            if (size(index,2)==1)
                %only 1 level of indexing is provided
                out=obj.(index(1).subs);
            else
                %call the indexing on the appropriate field
                fname=index(1).subs;
                %recursively process any further dereferencing
                out=subsref(obj.(fname),index(2:end));
            end

        else
            %check if its a member of the base class
            if ~isempty(strmatch(index(1).subs,fieldnames(obj.MParamObj)))
                fname=index(1).subs;
                out=subsref(obj.MParamObj,index);
            end
        end
end