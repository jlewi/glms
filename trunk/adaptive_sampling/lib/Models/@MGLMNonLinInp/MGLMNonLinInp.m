%function sim=MGLMNonLinInp(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: Constructs an object to represent a GLM model when a
% nonlinear function operates on the input before feeding it into the glm.
%
%Required parameters
%       ninpfunc - pointer to a function for the input nonlinearity
%                     @funcname(stim,spikehistory,fparam)
%                       fparam-structure containing the parameters for the
%                       input nonlinearity
%       fparam   - parameters for the nonlinear input function
%       stimlen  - this should be the actual length of the stimulus
%                  not the dimensionality after pushing it through the
%                  input nonlinearity.
%       parameters required by MParamObj
%Revision:
%   10-01-2008
%       Convert it to the new oop model.
%       Field version has been renamed version1 (version is field of bae
%       class)
classdef (ConstructOnLoad=true) MGLMNonLinInp < MParamObj

    %**********************************************************
    %Define Members of object
    %**************************************************************
    %       ninpobj  - added version 070601
    %                   object describing the input nonlinearity
    %       ninpfunc -  removed in version 070601
    %                   - pointer to a function to represent one of
    %                     the built in functions for the input nonlinearity
    %                     @funcname(stim,spikehistory,fparam)
    %                       fparam-structure containing the parameters for the
    %                       input nonlinearity
    %       fparam   -  removed in version 070601
    %                   parameters for the nonlinear input function
    %       stimlen  - actual length of the stimulus before projecting into
    %                 higher space
    %declare the structure
    properties(SetAccess=private,GetAccess=public)
        version1=081001;
        ninpobj=[];
        stimlen=0;
    end

    methods
        function obj=MGLMNonLinInp(varargin)
            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'ninpfunc','fparam','stimlen'};


            con(2).rparams={'ninpobj','stimlen'};
            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=struct();
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end



            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=struct();



                otherwise

                    %remove fields which are for this class
                    try
                        bparams=rmfield(params,con(cind).rparams);
                    catch
                    end

            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass

            obj=obj@MParamObj(bparams);

            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 0
                    %do nothing used by load object
                case 1
                    error('This constructor is depreciated as of version 070601');
                case 2
                    obj.ninpobj=params.ninpobj;

                    if (params.stimlen <=0)
                        error('stimlen needs to be>0');
                    end
                    obj.stimlen=params.stimlen;
                otherwise
                    error('Constructor not implemented')
            end

        end


    end

end
