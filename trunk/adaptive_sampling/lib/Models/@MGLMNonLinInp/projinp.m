%function tinp=projinp(mparam,inp,sr)
%       mparam- the MGLMNonLinInp object
%       inp         -dxn 
%                       the input
%                       each column is an input to transform
%       sr    - spike history
%
% Return value
%       mxn - the input after pushing it through the nonlinearity
%
%% Explanation:
%   This function constructs the input to the glm. That is it combines the
%   following:
%   1. the stimulus
%   2. Past responses
%   3. past stimuli
%   4. nonlinear projections of the input 
%   5. Fixed terms in the input
%
% Revision History:
function tinp=projinp(mparam,inp,sr)
    
    %1. push the stimuli through the nonlinearity
    if isa(inp,'GLMInput')
        inp=getData(inp);
    end
    nonstim=projinp(getninpobj(mparam),inp);
    
    %2. Use the projinp of the base class to combine 
    %   with any spike history
    tinp=projinp(mparam.MParamObj,nonstim);
    