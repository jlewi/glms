%function [stimcoeff shistcoeff bias]= parsetheta(mobj,theta)
%   theta - vector of parameers
%
%Return value:
%   bcoeff - the components of theta corresponding to the basis functions
%            of the STRF
%   shistcoeff - the components of theta corresponding to the spike history
%   bias     - the bias coefficient
%
%Explanation: Divides theta into its component parts
%
%
function [stimcoeff,shistcoeff,bias]= parsetheta(mobj,theta)

nstimcoeff=mobj.nstimcoeff;

stimcoeff=theta(mobj.indstim(1):mobj.indstim(2));

if (mobj.alength>0)
    shistcoeff=theta(mobj.indshist(1):mobj.indshist(2));
else
    shistcoeff=[];
end

if hasbias(mobj)
   bias=theta(mobj.indbias);
else
    bias=[];
end