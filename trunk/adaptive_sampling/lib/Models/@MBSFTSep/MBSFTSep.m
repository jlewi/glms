%function sim=ModBSSinewaves()
%   
%   Explanation: Create the object setting nfreq, ntime to the maximum
%   values without exceeding the Nyquist rate.
%
%function sim=ModBSSinewaves(nfreq, ntime.)
%   nfreq - set to [] if you want to use the max number of frequencies
%           without 
% Explanation: Represent the receptive field in the frequency domain when
% the fourier transform is applied to time and frequency separatly.
%   
classdef (ConstructOnLoad=true) MBSFTSep<MParamObj
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %ntime      - 01-29-2009 I'm not sure we use this anymore
    %            I think its always set to value of .maxn because
    %           we use btouse to determine which frequencies to include in
    %           our basis
    %          - how many frequencies to use in the time domain
    %           - this is the maximum multiple of the fundamental frequency
    %             thus it does not include the zero frequencies
    %           - use nstimcoeff to get the number of stimulus coefficients
    %nfreq      - - 01-29-2009 I'm not sure we use this anymore
    %            I think its always set to value of .maxn because
    %           we use btouse to determine which frequencies to include in
    %           our basis
    %           - how many frequencies to use in the frequency domain
    %           - this is the maximum multiple of the fundamental frequency
    %             thus it does not include the zero frequencies
    %
    %btouse      - Which terms are included in our basis
    %             To achieve dimensionality reduction we can throw out some
    %             terms in our basis because the projection on most terms
    %             is zeros. 
    %             - a full basis would have
    %             4*mobj.nfreq*mobj.ntime+2*mobj.ntime+2*mobj.nfreq+1;
    %               terms
    %             -btouse is an array storing the indexes of the vectors to
    %             actually use in our basis
    %
   
    properties(SetAccess=private, GetAccess=public)
        version1=081024;
        ntime=[];
        nfreq=[];
        
        %border specifies the order in which we arrange 
        %the four basis vectors  for each frequency
        %i.e for each frequency pair (i,j)
        %we have up to four basis functions 
        %corrsponding to cos(i)cos(j) cos(i)sin(j)
        %sin(i)cos(j) sin(i)sin(j)
        border=struct('cc',1,'cs',2,'sc',3,'ss',4);

        btouse=[];
    end

    %***********************************************************
    %Dependent properties are the fundamental frequencies
    %  The fundamental frequencies are 1/the corresponding dimension of the
    %  STRF
    properties(SetAccess=private,GetAccess=public, Dependent=true)
        fftime;
        fffreq;

        
        
        
        
        %maximum number of basis vectors for the fourier representation of
        %the strf
        %this is the number of basis vectors assuming we don't throw any
        %out
        maxbvecs;
    end


    %*****************************************************************
    %basis - These are our basis functions
    %      - we use transient property because its not worth saving them
    %      - but we don't want to recompute them every time we need them
    %      - basis stores the values of the cosines and sine waves
    %      evaluated at the correct location for each combination of
    %      frequencies we don't bother to store a matrix for the constant
    %      term of the fourier series
    %       
    %btouseinv  - is an array which stores which terms we want to
    %             include in our basis. These correspond to the linear
    %             indexes of the terms in our full basis. 
    %           - This is an array with the number of elements= to the
    %           maximum number of basis functions
    %           - each entry is either nan (if that basis function is not
    %           included) or a number indicating the position in theta at
    %           which that frequency is stored.
    %           - we can use btouseinv as a lookup table to decide which
    %           indexes we need
    %           - the functions which access bon are
    %               nstimcoeff
    %               bindex
    %               bindexinv
    %
    %fullbasis   - transient
    %********************************************************************
    properties(SetAccess=private,GetAccess=public,Transient=true)
        basis=[];
        btouseinv=[];
        
        %fullbasis
        %i.e basis vectors have dimension= dim(theta)
        fullbasis;
    end
    
   methods(Access=public)
        tinp=projinpmat(mobj,inp,sr);
        tinp=projinpvec(mobj,inp,sr); 
    end
    methods
      
        function maxbvecs=get.maxbvecs(mobj)
            maxbvecs=4*mobj.nfreq*mobj.ntime+2*mobj.ntime+2*mobj.nfreq+1;
        end
        function btouseinv=get.btouseinv(mobj)
            if isempty(mobj.btouseinv)
                btouseinv=nan(mobj.maxbvecs,1);
                btouseinv(mobj.btouse,1)=[1:length(mobj.btouse)]';
                            mobj.btouseinv=btouseinv;
            end
            btouseinv=mobj.btouseinv;
            
        end
        function basis=get.basis(obj)
            if isempty(obj.basis)
                %basis is a structure of the sinusoids and cosines
                %b\c for each pair of frequencies in the temporal and
                %spectral domain we have to coefficients, a sin and cosine
                obj.basis=zeros(obj.klength*obj.ktlength,obj.nstimcoeff);

               for bind=1:obj.nstimcoeff
                   bvec=compbvec(obj,bind);
                        obj.basis(:,bind)=bvec;
               end
            end
            basis=obj.basis;
        end
        function basis=get.fullbasis(mobj)
            if isempty(mobj.fullbasis)
            %to form the full basis we need to combine the basis of the strf
            %with unit vectors for the spike history and bias terms

            %we need to get the indexes of the coefficients in the base
            %functions
            [indstim]=getindstim(mobj);
            [indshist]=getindshist(mobj);
            [indbias]=getindbias(mobj);
                 
            %location of terms in full matrix
            %it might be better to create
            mbase=MParamObj('glm',mobj.glm,'klength',mobj.klength,'ktlength',mobj.ktlength,'mmag',mobj.mmag,'hasbias',mobj.hasbias,'alength',mobj.alength);


            startcol=mobj.indstim(1);
            endcol=mobj.indstim(2);
            
            basis(mbase.indstim(1):mbase.indstim(2),startcol:endcol)=mobj.basis;
            if (mobj.alength>0)

                
                basis(mbase.indshist(1):mbase.indshist(2),indshist(1):indshist(2))=eye(mobj.alength);

            end
            if (mobj.hasbias)
               basis(mbase.indbias,indbias)=1;
            end 
            obj.fullbasis=basis;
            end
            basis=obj.fullbasis;
        end
        function fftime=get.fftime(obj)
            %the fundamental frequency is the dimensionality
            %This ensures that if we sum a cosine over its period,
            %the integral is zero.
            fftime=1/(obj.ktlength);
        end
        function fffreq=get.fffreq(obj)
            %the fundamental frequency is the dimensionality minus 1
            %This ensures that if we sum a cosine over its period,
            %the integral is zero.
            fffreq=1/(obj.klength);
        end

        %this function computes the strf in the spectral domain (i.e domain
        %of the wave files) from the projections of the strf on different
        %basis function
        theta=tospecdom(obj,coeff);

        %project the strf onto the basis functions.
        bcoeff=tosinewaves(obj,theta);

        function obj=MBSFTSep(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            %blank constructor for loading the object.
            con(1).rparams={};

            con(2).rparams={'nfreq','ntime'};

            con(3).rparams={'mobj'};

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=struct();
                    cind=1;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
                    
                    if (isnan(cind))
                        %user didn't pass in any parameters specific to
                        %ModBSSinewaves.
                        %in this case set nfreq,ntime to nan
                        %and cind to 2. This will cause us to set nfreq and
                        %nitme to the max values
                        if ~isfield(params,'nfreq')
                            params.nfreq=[];
                        end
                        if ~isfield(params,'ntime')
                            params.ntime=[];
                        end
                       cind=2; 
                    end
            end

            if (cind==[2 3])
                cind=3;
            end
            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 1
                    %used by load object do nothing
                    bparams=struct();
                case 2

                    bparams=rmfield(params,{'nfreq','ntime'});

                case 3
                    if ~isa(params.mobj,'MBSFTSep')
                        if (~isfield(params,'nfreq') || ~isfield(params,'ntime'))
                            error('If mobj is not of type MBSFTSep then you must specify nfreq and ntime');
                        end
                    else
                        params.nfreq=params.mobj.nfreq;
                        params.ntime=params.mobj.ntime;      
                        params.btouse=params.mobj.btouse;
                    end
                    bparams.mobj=params.mobj;
                    
                    
                    %call constructor two
                    cind=2;
                otherwise
                    if isnan(cind)
                        error('no constructor matched');
                    end
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass           
            obj=obj@MParamObj(bparams);

            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 1
                    %do nothing used by load object

                case 2
                    %error('01-29-2009. I think this constructor is obsolete. We use btouse to specify which frequencies to use. not ntime and nfreq.');
                    obj.nfreq=params.nfreq;
                    obj.ntime=params.ntime;

                    %make sure we don't exceed the Nyquist frequency as
                    %this will lead to aliasing issues.


                    [nmax]=ModBSSinewaves.maxn([obj.klength,obj.ktlength]);


                    if isempty(obj.nfreq)
                        obj.nfreq=nmax(1);
                    end
                    if isempty(obj.ntime)
                        obj.ntime=nmax(2);
                    end
                    if ((obj.nfreq)>nmax(1))                        
                        error('nfreq is too large. This leads to frequencies larger than the Nyquist rate which leads to aliasing. Maximum value is %d',nmax(1)+1);
                    end
                    if (obj.ntime>(nmax(2)))
                        error('ntime is too large. This leads to frequencies larger than the Nyquist rate which leads to aliasing. Maximum value is %d',nmax(2)+1);
                    end

                    
                    if isfield(params,'btouse')
                     
                        obj.btouse=colvector(sort(params.btouse));
                    else
                        obj.btouse=[1:obj.maxbvecs]';
                    end
                otherwise
                    error('Constructor not implemented')
            end



        end
    end

    methods(Static)
        %run all tests
        [sucess]=alltests();
        %test function
        [success,mobj]=test();

        %test function to test basis functions are orthogonal and
        %normalized
        [success]=testbasis(strfdim);

        %compute the maximum number of frequencies that we can have.
        [nm]=maxn(strfdim);
        
        %test bindex and bindexinv
        [success]=testbindex();
        
        %This is a helper function which allows you
        %to select which basis vectors to include based on the strf
        %dimensions and some nf and nt cutoffs that you set.
        [btouse]=btouseforcutoff(strfdim,nf,nt);
    end
end


