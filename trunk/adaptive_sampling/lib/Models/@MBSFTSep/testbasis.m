%function success=testbasis()
%
%Explanation: Test the basis functions of ModBSSinewaves are orthonormal
%
function [success,obj]=testbasis(strfdim)


if ~exist('strfdim','var')
    strfdim=[8 8];
end

klength=strfdim(1);
ktlength=strfdim(2);
success=true;
nmax=MBSFTSep.maxn([klength ktlength]);

nfreq=nmax(1);
ntime=nmax(2);

obj=MBSFTSep('nfreq',nfreq,'ntime',ntime,'klength',klength,'ktlength',ktlength,'mmag',1);



%***********************************************************************
%Sum to zero
%***********************************************************************
%all of the basis vectors except the first basis vector corresponding to
%the constant term should sum to zero
%determine which vector is the constant term
iconst=bindex(obj,0,0);
iconst=iconst(~isnan(iconst));
s=sum(obj.basis(:,[1:iconst-1 iconst+1:obj.nstimcoeff]),1);
s=abs(s);
if (any(s>10^-8))
    error('Not all basis vectors integrate to 1.');
end
%check basis is orthonormal
mags=obj.basis'*obj.basis;

trueval=eye(obj.nstimcoeff);
d=abs(mags-trueval);

if (any(d(:)>10^-8))
    error('basis is not orthonormal');
end


if (success)
    fprintf('Success: Basis vectors are orthonormal.\n') ;
end
