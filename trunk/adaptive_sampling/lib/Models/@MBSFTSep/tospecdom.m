%function theta=tospecdom(obj,bcoeff)
%   bcoeff - the projections of the STRF along the basis functions
%
%Return value:
%   strf - The STRF in the domain of the spectrogram of the wave files.
%
%   this function computes the strf in the spectral domain (i.e domain
%   of the wave files) from the projections of the strf on different
%   basis function
function [strf,mobj]=tospecdom(mobj,bcoeff)

bcoeff=colvector(bcoeff);


strf=mobj.basis*bcoeff;


%create the basis functions
strf=reshape(strf,mobj.klength,mobj.ktlength);
