%function success=test()
%
%Explanation: Test the MBSFTSep
%
function [success,obj]=test()


success=true;

klength=10;
ktlength=10;

nmax=MBSFTSep.maxn([klength ktlength]);
nfreq=nmax(1);
ntime=nmax(2);

mobj=MBSFTSep('nfreq',nfreq,'ntime',ntime,'klength',klength,'ktlength',ktlength,'mmag',1);

%********************************************************************
%make sure tosindom and tospecdom return analgous answers
%********************************************************************


%********************************************************************
%test tospecdom
%********************************************************************
%%randomly generate the coefficients
theta=floor(randn(mobj.nstimcoeff,1)*100)/100;

strf=tospecdom(mobj,theta);

bproj=tosindom(mobj,strf);


if (any(abs((bproj-theta)>10^-8)))
    success=false;
    error('There is in error in tosindom or tospecdom because the recovered basis coefficients are not correct');
end

fprintf('Success: tospecdom and tosindom are correct. \n');


%*******************************************************************
%test compglmproj
%********************************************************************
stim=randn(mobj.klength,mobj.ktlength);
lmproj=compglmproj(mobj, stim(:),theta,[]);
