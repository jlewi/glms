%function tinp=projinp(mparam,inp,sr)
%       mparam- the MParamObj object
%       inp         -matrix representing the stimuli
%                   - each column represents the stimulus on a different
%                   trial
%                   - i.e inp(:,t)=\{\stim[t-tk],\ldots,\stim[t]\}
%
%       sr    - object array storing past stimuli and responses
%             - or can be a vector of the spike history
% Return value
%       mxn - the input
% Explanation:
%   This function maps the input into a vector which lives in the same
%   space as theta (i.e fourier space.)
%
% Revision History:
%   02-27-2009: Replace this function by projinpvec and projinpmat
%       
function tinp=projinpvec(mobj,inp,sr)
%
fprintf('tinp projinpvec \n');
%1. if inp is an object array call getData to create an array
%2. if we have spike history terms add them
%3. add a bias/ fix terms as necessary
bias=[];
if (hasbias(mobj))
    bias=1;
end
if isa(inp,'GLMInput')
    tinp=getData(inp);
else
    tinp=inp;
end

%project the input onto the basis vectors
tinp=mobj.basis'*tinp;
shist=[];
if (getshistlen(mobj)>0)
    if (~isempty(sr) && isa(sr,'SRObj'))
        shist=getobsrv(sr)';
    else
        shist=sr;
    end
    shist=[zeros(getshistlen(mobj)-length(shist),1);shist];
end

tinp=[tinp;shist;bias*ones(1,size(tinp,2))];
