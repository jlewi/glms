%function [ind]=bindexinv(mobj,bind)
%   bind - linear index into bcoeff
%
%Return value
%   bind - nx 3 matrix
%          col 1 - nf for this index
%          col 2 - nt for this index
%          col 3 - border.cc || border.cs, || border.sc || border.ss
%                - specifies which basis function this is
%
%
%Explanation:
%   This is the inverse of bindex. Bindex essentially maps multiple subscripts into the linear
%   index. This function does the oppositie. It maps the linear index
%   into multiple subscripts
%
%   we do this in two steps.
%       1. We use btouse to map the linear index into the equivalent linear
%           index assuming we were using all possible basis
%       2. We then map this linear index into the appropriate nf,nt
%
%This function must be consistent with bindex
function subind=bindexinv(mobj,bind)

if any(bind> mobj.nstimcoeff)
    error('bind can''t exceed mobj.nstimcoeff');
end

subind=zeros(1,3);

border=mobj.border;

%map bind into the appropriate index assuming we use all possible basis
%vectors
bind=colvector(mobj.btouse(bind));

%**************************************************************
%ind=1
%Constant
%***************************************************************
ind=find(bind==1);
b=bind(ind);
subind(b,:)=ones(length(b),1)*[0 0 border.cc];


%**************************************************************
%2<=ind<mobj.ntime+1
%cos cos for nf=0 and nt~=0
%***************************************************************
ind=find(bind>=2 & bind<=(mobj.ntime+1));
b=bind(ind);

offset=b-1;

subind(ind,1)=0;
subind(ind,2)=offset;
subind(ind,3)=border.cc;

%**************************************************************
%mobj.ntime+2<=ind<2*mobj.ntime+1
%cos sin for nf=0 and nt~=0
%***************************************************************
istart=mobj.ntime+2;
iend=2*mobj.ntime+1;
ind=find(bind>=istart & bind<=iend);
b=bind(ind);

offset=b-mobj.ntime-1;

subind(ind,1)=0;
subind(ind,2)=offset;
subind(ind,3)=border.cs;

%**************************************************************
%2*mobj.ntime+2<=ind<2*mobj.ntime+1+mobj.nfreq
%cos cos for nf~=0 and nt=0
%***************************************************************
istart=2*mobj.ntime+2;
iend=2*mobj.ntime+1+mobj.nfreq;
ind=find(bind>=istart & bind<=iend);
b=bind(ind);

offset=b-(istart-1);

subind(ind,1)=offset;
subind(ind,2)=0;
subind(ind,3)=border.cc;

%**************************************************************
%mobj.ntime+2<=ind<2*mobj.ntime+1
%sin cos for nf~=0 and nt=0
%***************************************************************
istart=iend+1;
iend=istart+mobj.nfreq-1;
ind=find(bind>=istart & bind<=iend);
b=bind(ind);

offset=b-(istart-1);

subind(ind,1)=offset;
subind(ind,2)=0;
subind(ind,3)=border.sc;


%**************************************************************
%all other terms.
%***************************************************************
istart=iend+1;
ind=find(bind>=istart);
b=bind(ind);

if ~isempty(ind)
    %we start by determining which basis function it is
    subind(ind,3)=ceil((b-(istart-1))/(mobj.nfreq*mobj.ntime));

    offset=(b-(istart-1))-(subind(ind,3)-1)*mobj.nfreq*mobj.ntime;

    %offset now contains the linear index of nfreqxntime matrix
    [nf, nt]=ind2sub([mobj.nfreq mobj.ntime],offset);

    subind(ind,1)=nf;
    subind(ind,2)=nt;
end

