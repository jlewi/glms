%function [theta= parsetheta(mobj,stimcoeff shistcoeff bias)
%   stimcoeff - structure of the stimulus coefficients
%
%Return value:
%   theta
%Explanation: Packs the different terms of theta into a vector
%
function theta= packtheta(mobj,stimcoeff,shistcoeff,bias)



theta=[stimcoeff;shistcoeff;bias];    
