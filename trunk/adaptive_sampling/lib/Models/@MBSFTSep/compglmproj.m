%%function glmproj=compglmproj(mobj, stim,theta,sr)
%       stim - the input - vector of stimulus and spike history
%                    concatenated together
%       theta - parameters for the model
%             - THe strf should be represented as the projections along the
%               sinusoidal basis functions
%       sr    - stimulus and spike history
% Return value:
%   glmproj - this is the output of the linearity in the GLM
%
% Explanation: Computes the expected value of the observation for input
%   stim and model parameters theta.
%
function glmproj=compglmproj(mobj,stim,theta,sr)

    %Theta stores the projections of the STRF along our sinusoidal basis
    %projinp handles the projection of stimulus into the same space as
    %theta
    glmproj=projinp(mobj,stim,sr)'*theta;
