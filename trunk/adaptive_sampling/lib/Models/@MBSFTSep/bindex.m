%function bindex(mobj,nf,nt)
%   nf - Which frequency to return
%        0 to obj.nfreq
%   nt - which time frequency to return
%        0 to obj.ntime
%   
%Return value
%   bind - the column in mobj.basis corresponding to this frequency
%        - 1x4 matrix. 
%        - each column is for 1 of the four possible basis functions
%        - the basis function corresponding to each column is specified by
%        border
%
%         - for nf=nt=0 we return nan since we have a single
%         coefficient
%
%
%Explanation: This function works by first mapping nf,nt into the
%appropriate linear index assuming all possible basis vectors are used
%(i.e nstimcoeff=nan(4*mobj.nfreq*mobj.ntime+2*mobj.ntime+2*mobj.nfreq+1,1);
%
%We then use mobj.btouseinv to map this index into the appropriate index based on
%the fact that we only include a subset of all possible basis vectors in
%our representation
function bind=bindex(mobj,nf,nt)

if any(nf>mobj.nfreq)
    error('nf can''t exceed mobj.nfreq');
end
if any(nt>mobj.ntime)
    error('nt can''t exceed mobj.ntime');
end

if (length(nf)~=length(nt))
    error('nf and nt must have the same length');
end
border=mobj.border;
bind=nan(length(nf),4);

n=[colvector(nf) colvector(nt)];

%**************************************************************
%Find nf=nt=0
%**************************************************************
%we only have a cosine cosine basis for this terms 
istart=1;
ind=find(n(:,1)==0 & n(:,2)==0);

%we use btouseinv to map the linear index (assuming all basis vectors are used)
%into the appropriate linear index given that we only use the vectors
%in btouse as part of our basis
bind(ind,mobj.border.cc)=mobj.btouseinv(1);


%*************************************************************
%find nf=0 nt~=0
%*************************************************************

ind=find(n(:,1)==0 & n(:,2)~=0);

%offset is just nt
offset=n(ind,2);

%the start index is 
istart=istart+1;
bind(ind,border.cc)=mobj.btouseinv(istart-1+offset);
istart=istart+mobj.ntime;
bind(ind,border.cs)=mobj.btouseinv(istart-1+offset);


%*************************************************************
%find nf=~0 nt=0
%*************************************************************
ind=find(n(:,1)~=0 & n(:,2)==0);

%offset is just nf
offset=n(ind,1);

%the start index is 
istart=istart+mobj.ntime;
bind(ind,border.cc)=mobj.btouseinv(istart-1+offset);
istart=istart+mobj.nfreq;
bind(ind,border.sc)=mobj.btouseinv(istart-1+offset);


%************************************************************
%nf~=0 and nt~=0
%*************************************************************
ind=find(n(:,1)~=0 & n(:,2)~=0);

%to compute the offset, consider a matrix which is nfreqxntime
%then we just want to compute the linear indexes for each (nf,nt)
offset=sub2ind([mobj.nfreq,mobj.ntime],n(ind,1),n(ind,2));


%start=the first position of cos cos for these frequencies
istart=istart+mobj.nfreq;


bind(ind,border.cc)=mobj.btouseinv(istart-1+offset);

istart=istart+mobj.nfreq*mobj.ntime;

bind(ind,border.cs)=mobj.btouseinv(istart-1+offset);

%start=(2*mobj.ntime+2*mobj.nfreq+1)+2*(mobj.ntime*mobj.nfreq)+1;
istart=istart+mobj.nfreq*mobj.ntime;
bind(ind,border.sc)=mobj.btouseinv(istart-1+offset);

%start=(2*mobj.ntime+2*mobj.nfreq+1)+3*(mobj.ntime*mobj.nfreq)+1;
istart=istart+mobj.nfreq*mobj.ntime;
bind(ind,border.ss)=mobj.btouseinv(istart-1+offset);
