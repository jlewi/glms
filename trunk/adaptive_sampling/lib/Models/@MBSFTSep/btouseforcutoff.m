%  function  [btouse]=btouseforcutoff(strfdim,nfreq,ntime)
%   strfdim - dimensions of the STRF
%   nf -largest multiple of the fundamental frequency in the spectral
%     dimension to use
%   nt - largest multiple of the fundamental frequency in the temporal
%   dimension to use
%
% Return value:
%   btouse - the index of the basis vectors
%            corresponding to nf<=nfreq nt<=ntime
%
%Explanation:
%   When running simulations using frequency smoothing. We often
%   only want to use those basis vectors corresponding to low frequencies
%   This function makes it easy to determine which basisvectors to use
%   based on the cuttoff

function  [btouse]=btouseforcutoff(strfdim,nfreq,ntime)


mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.alength=0;
mparam.hasbias=true;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mobj=MBSFTSep(mparam);


subind=bindexinv(mobj,1:mobj.nstimcoeff);
btouse=subind(:,1)<=nfreq & subind(:,2)<=ntime;
btouse=find(btouse==1);