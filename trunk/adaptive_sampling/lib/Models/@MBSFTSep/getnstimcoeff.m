%function nstimcoeff=getnstimcoeff(mobj)
%   
%Return value:
%   nstimcoeff - the number of terms in theta which correspond to the
%   stimulus (as opposed to bias and spike history)
%   
%   Using a function allows us to override how it is computed in derived
%   classes
%
function [nstimcoeff]=getnstimcoeff(mobj)


%for all pairs i=1:nfreq, j=1:nfreq - we have 4 basis functions
%for i=1:nfreq j=0 we have 2 basis functions
%for i=0 j=1:ntime we have 1 basis function
%for i=0 and j=0 we have 1 basis function
%nstimcoeff=4*mobj.nfreq*mobj.ntime+2*mobj.ntime+2*mobj.nfreq+1;
nstimcoeff=length(mobj.btouse);
    