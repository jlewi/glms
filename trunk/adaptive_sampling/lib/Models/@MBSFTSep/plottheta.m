%function =plottheta(mobj,theta,txt,bssim)
%   theta - fourier coefficients or a structure containing the amplitudes
%         - can also be the complete theta
%
%   txt   - optional text to add to top of plot
%   bssim - bird song object. We use this to get parameters for the plots
%         - i.e the bin widths
%Explanation: Plots the fourier coefficinets
function [fh,oinfo]=plottheta(mobj,theta,txt,bssim)

oinfo=[];

if ~exist('txt','var')
    txt=[];
end

if (length(theta)==getparamlen(mobj))
    [stimcoeff,shistcoeff,bias]=parsetheta(mobj,theta);
else
    shistcoeff=[];
    bias=[];
end

ncols=1;

if (mobj.alength>0)
    ncols=ncols+1;
end

fh=FigObj('name','Fourier Coefficents','naxes',[1,ncols],'width',5,'height',6);

%if theta is empty return
if isempty(theta)
    return;
end
%**************************************************************************
%plot the strf
%**************************************************************************
row=1;
col=1;
setfocus(fh.a,row,col);

%determine the labels for the axis
if (isa(bssim,'BSSimobj') || isa(bssim,'BSBatchMLSim'))
    [t,freqs]=getstrftimefreq(bssim.bdata);
else
    t=bssim.extra.t;
    freqs=bssim.extra.f;
end
%convert to ms and hz
t=t*1000;
freqs=freqs/1000;
title(fh.a(row,col),'STRF');
imagesc(t,freqs,tospecdom(mobj,stimcoeff));
xlabel(fh.a(row,col),'Time(ms)');
ylabel(fh.a(row,col),'Frequencies(khz)');
hc=colorbar;
sethc(fh.a(row,col),hc);
xlim([t(1) t(end)]);
ylim([freqs(1) freqs(end)]);



%************************************************************************
%Plot the spike history
%***********************************************************************
row=1;
col=2;
if (mobj.alength>0)
    setfocus(fh.a,row,col);

    t=-1*[getshistlen(mobj):-1:1];
    hp=plot(t,shistcoeff);
    pstyle.marker='.';
    pstyle.markerfacecolor='b';
    addplot(fh.a(row,col),'hp',hp,'pstyle',pstyle);
    xlabel(fh.a(row,col),'time (# time bins)');
    title(fh.a(row,col),'spike history');
    xlim([t(1) t(end)]);
else
    if (ncols>1)
    set(fh.a(row,col),'visible','off');
    end
end



%**************************************************************************
%Add some text: describing the simulation
%***********************************************************************
tpos=[0 0];
[tpos(1) tpos(2)]=intorel(fh,fh.width/2,-.35);

if isempty(txt)
    txt='';
end

%add the bias to the text
if (~isempty(bias))
    txt=sprintf('%sbias: %d',txt,bias);
else
    txt=sprintf('%s\nbias: none',bias);
end


ht=addtext(fh,tpos,txt);

lblgraph(fh);

if ~isempty(txt)
    %leave buffer for the text
    ebtxt=get(ht,'extent');
    sizesubplots(fh,[],[],[],[.02 0 .02 (1-ebtxt(2))+.02]);
else
    sizesubplots(fh);
end


return;