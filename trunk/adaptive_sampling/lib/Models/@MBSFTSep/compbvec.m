%function [bmat]=compbvec(mobj,nf,nt)
%   nf - value of nf
%   nt - value of nt
%
%function compbvec(mobj,bind)
%   bind - the linear index of the vector to compute
%Return value:
%   bmat - matrix of the basis vectors for this frequency
%        - the columns correspond to the different basis vectors
%           as specified by the structure border
%       - if there is no basis function for this frequency (i.e the
%       correesponding function is all zeros) then we return a column of
%       nan's
%

function [bmat]=compbvec(mobj,varargin)

%three variables determine which vectors to compute
%nf - frequency
%nt - ntime
%btype - array indicating which of the 4 types can and should be computed

border=mobj.border;

if (nargin==3)
    nf=varargin{1};
    nt=varargin{2};

    btype=zeros(1,4);

    bmat=nan(mobj.ktlength*mobj.klength,4);

    if (nf~=0)
        btype(border.sc)=1;
    end

    if (nt~=0)
        btype(border.cs)=1;
    end

    if (nf~=0 && nt~=0)
        btype(border.ss)=1;
    end


    btype(border.cc)=1;

    ncols=4;
elseif (nargin==2)

    ncols=1;
    bmat=nan(mobj.ktlength*mobj.klength,1);
    bind=varargin{1};

    [subind]=bindexinv(mobj,bind);
    nf=subind(1);
    nt=subind(2);

    btype=zeros(1,4);
    btype(subind(3))=1;

end


t=[0:mobj.ktlength-1];
f=[0:mobj.klength-1]';





if (btype(border.sc))
    bvec=sin(2*pi*mobj.fffreq*nf*f)*cos(2*pi*mobj.fftime*nt*t);


    if (ncols>1)
        bmat(:,mobj.border.sc)=bvec(:);
    else
        bmat(:,1)=bvec(:);
    end
end
if (btype(border.cs))
    bvec=cos(2*pi*mobj.fffreq*nf*f)*sin(2*pi*mobj.fftime*nt*t);


    if (ncols>1)
        bmat(:,mobj.border.cs)=bvec(:);
    else
        bmat(:,1)=bvec(:);
    end
end


if (btype(border.ss))
    bvec=sin(2*pi*mobj.fffreq*nf*f)*sin(2*pi*mobj.fftime*nt*t);


    if (ncols>1)
        bmat(:,mobj.border.ss)=bvec(:);
    else
        bmat(:,1)=bvec(:);
    end
end

if (btype(border.cc))
    bvec=cos(2*pi*mobj.fffreq*nf*f)*cos(2*pi*mobj.fftime*nt*t);


    if (ncols>1)
        bmat(:,mobj.border.cc)=bvec(:);
    else
        bmat(:,1)=bvec(:);
    end
end

bmat=normmag(bmat);
