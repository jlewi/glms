%function [nf,nt]=getmaxnfnt(mobj)
%   mobj - 
%
%Return value:
%   nf - the maximum multiple of the fundamental frequency in the spectral
%       direction that we use
%   nt - the maximum multiple of the fundamental frequency in the time
%   direction of the STRF
%Explanation:
%   To apply smoothing not all basis vectors are used.
%   The vectors that are used are indicated by the variable btouse.
%   We would like to know what's the largest multiple of the fundamental
%   frequencies that we use. Thats what this function returns.
function [nf,nt]=getmaxnfnt(mobj)

[ind]=bindexinv(mobj,1:mobj.nstimcoeff);

[nf]=max(ind(:,1));
[nt]=max(ind(:,2));
