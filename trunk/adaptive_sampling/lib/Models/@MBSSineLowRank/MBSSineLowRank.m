%function sim=MBSSineLowRank(fieldname, value,....)
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: This class represents the posterior in the fourier space
%   of the STRF and also uses the low rank idea to form a low rank
%   approximation
%
%Revisions:
%   08-10-24
%       -Previous versions for computing the tangent space were wrong.
classdef (ConstructOnLoad=true) MBSSineLowRank < ModBSSinewaves & MTanSpaceInt
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        %version
        vermbssinelowrank=081024;

        %rank - what rank to use for the approximation
        rank=[];
        
        %***********************************************
        %whether to run some extra debugging
        debug=true;
    end

    properties(SetAccess=private,GetAccess=public,Transient)
        %fbasis and tbasis are matrices. Each column is a sinweave or
        %cosine of wave
        %We want to construct the principal components of our low rank
        %approximation by taking linear combinations of these vectors
        fbasis=[];
        tbasis=[];
        
    end
    methods
        function fbasis=get.fbasis(mobj)
           if isempty(mobj.fbasis)
               %We have mobj.nfreq sinewavs
               %and mobj.nfreq+1 ncosines 
               fbasis=zeros(mobj.klength,2*mobj.nfreq+1);

               f=[0:mobj.klength-1]';               

               %sinusoidal functions
               sfreq=2*pi*(mobj.fffreq*[1:mobj.nfreq]);               
               fbasis(:,1:mobj.nfreq)=sin(f*sfreq);
               
               
               %cosine basis functions
               cfreq=2*pi*(mobj.fffreq*[0:mobj.nfreq]);               
               fbasis(:,mobj.nfreq+1:end)=cos(f*cfreq);
               
               mobj.fbasis=fbasis;
           end
           fbasis=mobj.fbasis;
        end
        function tbasis=get.tbasis(mobj)
           if isempty(mobj.tbasis)
               %We have mobj.nfreq sinewavs
               %and mobj.nfreq+1 ncosines 
               tbasis=zeros(mobj.klength,2*mobj.nfreq+1);

               t=[0:mobj.ktlength-1]';               

               %sinusoidal functions
               sfreq=2*pi*(mobj.fftime*[1:mobj.ntime]);               
               tbasis(:,1:mobj.ntime)=sin(f*sfreq);
               
               
               %cosine basis functions
               cfreq=2*pi*(mobj.fffreq*[0:mobj.nfreq]);               
               tbasis(:,mobj.ntime+1:end)=cos(t*cfreq);
               
               mobj.tbasis=tbasis;
           end
           tbasis=mobj.tbasis;
        end
        
        function obj=MBSSineLowRank(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={'rank'};

            error('This class is totally screwed up. I don''t think what I was trying to do was valid. I don''t think the manifold as I''ve defined it is contained within theta space as its defined. \n');
            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=struct();
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=struct();
                    blowrank=struct();
                case 1
                    blowrank.rank=params.rank;
                    
                            bparams=rmfield(params,'rank');
                otherwise
                    if isnan(cind)
                        error('no constructor matched');
                    else
                        %remove fields which are for this class
                        try                            
                            bparams=rmfield(params,'rank');
                        catch
                        end
                    end
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %call the constructor for MBSSineWaves
            obj=obj@ModBSSinewaves(bparams);
    


            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 0
                    %do nothing used by load object

                case 1
                    %do nothing
                    obj.rank=params.rank;
                otherwise
                    error('Constructor not implemented')
            end



        end
        
           %function m=uvtovec(obj,u,s,v,shist,bias)
        %   u,s,v - are the matrices making up the rank r approximation of m
        %         we reshape these a vector, so that they can be stored as vector
        %         parameters of the manifold
        %         - u, v - each column is the linear combination of the
        %         columns of fbasis or tbasis needed to give us one of our
        %         principal components
        %   shist - the spike history terms
        %   bias  - bias term
        function m=uvtovec(obj,u,s,v,shist,bias)

            m=[s(:);u(:);v(:);shist;bias];

        end

        %function [u,s,v,bias]=vectouv(obj,sp)
        %   sp - is a vector representing the matrices u,s,v, the spike
        %   history terms and bias
        %       we return the matrices u,v
        %   s is a vector of singular values. These are stored in first rank elements of sp
        %   then come the matrices u and v
        function [u,s,v,shist,bias]=vectouv(obj,sp)

            rank=obj.rank;
            mdim=[obj.klength obj.ktlength];

            s=sp(1:rank);

            sind=rank+1;
            eind=sind+mdim(1)*rank-1;
            u=reshape(sp(sind:eind),[mdim(1),rank]);

            sind=eind+1;
            eind=sind+mdim(2)*rank-1;
            v=reshape(sp(sind:eind),[mdim(2),rank]);

            %get the spike history terms
            if (obj.alength>0)
                sind=eind+1;
                eind=sind+obj.alength-1;
                shist=sp(sind:eind);
            else
                shist=[];
            end
            if (obj.hasbias)
                bias=sp(end);
            else
                bias=[];
            end
        end
    end
    
    methods (Static)
       success=testconstruct(); 
       
       %run some tests to check that using the manifold functions works
       %correctly
       success=testmanifold();
    end
end


