%function success=testconstruct()
%
%Explanation: Test construction of the object
function mobj=testconstruct()

alength=2;
glm=GLMPoisson('canon');
nfreq=3;
ntime=3;
klength=10;
ktlength=10;
rank=2;

mobj=MBSSineLowRank('rank',2,'nfreq',nfreq,'ntime',ntime,'glm',glm,'alength',alength,'klength',klength,'ktlength',ktlength,'mmag',1,'hasbias',1);