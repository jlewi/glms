%function success=testmanifold()
%
%Explanation: Test manifold
%
function success=testmanifold()

%create an object
%no spike history, no bias
mparam.klength=10;
mparam.ktlength=10;
mparam.hasbias=0;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mparam.rank=1;
mparam.nfreq=[];
mparam.ntime=[];
mparam.alength=0;

mobj=MBSSineLowRank(mparam);

%generate some theta (use a 2-d gabor);
%strf=gabortheta(10,10,[],1);

%generate a frequency separable theta so that it should lie on the manifold
t=cos(2*pi*mobj.fftime*[0:mobj.ktlength-1]);
f=sin(2*pi*mobj.fffreq*[0:mobj.klength-1])';

strf=f*t;

%project theta onto our fourier domain
theta=tosindom(mobj,strf);
