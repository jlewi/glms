%function [stimcoeff shistcoeff bias]= parsetheta(mobj,theta)
%   theta - vector of parameers
%
%Return value:
%   bcoeff - the components of theta corresponding to the basis functions
%            of the STRF
%   shistcoeff - the components of theta corresponding to the spike history
%   bias     - the bias coefficient
%
%Explanation: Divides theta into its component parts
%
%
function [stimcoeff,shistcoeff,bias]= parsetheta(mobj,theta)

%call parsetheta of ModBSSineWaves
%the point of this function is to disambiguate which function should be
%called
[stimcoeff,shistcoeff,bias]= parsetheta@MBSFTSep(mobj,theta);