%function theta=submanifold(obj,sp)
%   obj - descendent of TanSpaceObj
%   sp  - mxn array
%         -each column is a point at which to evaluate the manifold
%         point at which to evaluate the manifold
%         This is the vector describing u,v as a vector
%         use uvtovec to form this vector from u,v
%
%Explanation: Evaluates the submanifold for this point.
function theta=mantotheta(obj,sp)

%could probably go either way
%warning('1-20-2008: why do we use getmatdim and not getthetadim?');
theta=zeros(getparamlen(obj),size(sp,2));

for cind=1:size(sp,2)
%convert sp back to uv
[u,s,v,shist,bias]=vectouv(obj,sp(:,cind));

%compute the strf
strf=obj.fbasis*u*diag(s)*v'*obj.tbasis';

%now project the strf onto the fourier basis
scoeff=obj.basis'*strf(:);

% if (obj.debug)
%    fprintf('MBSSinewLowRank: checking that the point on the manifold can be mapped to a pot in theta space \n');
%    
%    %check to make sure our representation in the fourier space matches are
%    %low rank representation
%    err=(strf-obj.basis*scoeff).^2;
%    if (sum(err(:)).^.5>10^-8)
%        error('The point on the manifold has components orthogonal to our parameter space');
%    end
% end

theta(:,cind)=packtheta(obj,scoeff,shist,bias);
end

