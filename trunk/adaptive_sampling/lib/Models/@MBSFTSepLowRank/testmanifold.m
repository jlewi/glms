%function success=testmanifold()
%
%Explanation: Test manifold
%
function success=testmanifold()

success=true;

%create an object
%no spike history, no bias
mparam.klength=40;
mparam.ktlength=10;
mparam.hasbias=0;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mparam.rank=1;
mparam.nfreq=[];
mparam.ntime=[];
mparam.alength=0;

mobj=MBSFTSepLowRank(mparam);

%generate some theta (use a 2-d gabor);
%strf=gabortheta(10,10,[],1);

%generate a frequency separable theta so that it should lie on the manifold
t=cos(2*pi*mobj.fftime*[0:mobj.ktlength-1]);
f=sin(2*pi*mobj.fffreq*[0:mobj.klength-1])';

strf=f*t;

%project theta onto our fourier domain
theta=tosindom(mobj,strf);

%recovered strf
strfrec=tospecdom(mobj,theta);

rerr=(strf-strfrec).^2;
rerr=sum(rerr(:))^.5;
if (rerr>10^-10)
   error('When representing a rank 1 STRF  in our model space. The error of the estimate is not zero.');
end

%**************************************************************************
%Test projection onto the manifold
%1. generate rank r randomly
%2. Construct a rank r matrix whose principal components are linear
%  combinations of the cosines and sines
%3. Compute the corresponding representation in our model space (separable
%fourier space). - check to make sure there is no error it is perfect
%reconstruction
%4. project  theta onto our manifold and make sure it matches our original
%matrix
%**************************************************************************
rank=ceil(rand(1,1)*min(size(mobj.fbasis,2),size(mobj.tbasis,2)));
mparam.rank=rank;
mobj=MBSFTSepLowRank(mparam);

%randomly generate the linear combinations of the sines and cosine terms
tdata.fcombo=randn(size(mobj.fbasis,2),rank);
tdata.fcombo=orth(tdata.fcombo);

tdata.tcombo=randn(size(mobj.tbasis,2),rank);
tdata.tcombo=orth(tdata.tcombo);

%what projection to place on each component
tdata.lrproj=diag(rand(rank,1));

%compute the strf
tdata.strf=mobj.fbasis*tdata.fcombo*tdata.lrproj*tdata.tcombo'*mobj.tbasis';

%compute theta for this manifold p
tdata.theta=mantotheta(mobj,uvtovec(mobj,tdata.fcombo,tdata.lrproj,tdata.tcombo,[],[]));

%verify that tdata.theta is a perfect representation of the strf
rerr=(tospecdom(mobj,tdata.theta)-tdata.strf).^2;
rerr=sum(rerr(:))^.5;
if (rerr>10^-10)
   error('When representing a low rank STRF  in our model space. The error of the estimate is not zero.');
end


%project theta onto the manifold and verify it matches the true value
man.tanpoint=comptanpoint(mobj,tdata.theta);

%to verify that the tangent point is correct
%we comput fcombo*lproj*tcombo and make sure it matches
[man.fcombo, man.lrproj, man.tcombo]=vectouv(mobj,man.tanpoint.manifoldp);

err=(tdata.fcombo*tdata.lrproj*tdata.tcombo'-man.fcombo*diag(man.lrproj)*man.tcombo');
err=err.^2;
err=sum(err(:)).^.5;
if (err>10^-10)
   error('Projection of theta onto the manifold does not appear to be correct.'); 
   success=false;
end

if (success)
    fprintf('Success: Projection of theta onto manifold, and the inverse operation are correct \n');
end
