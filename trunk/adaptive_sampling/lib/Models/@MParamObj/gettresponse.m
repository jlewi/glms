%function tresponse=gettresponse(obj)
%	 obj=tresponse object
% 
%Return value: 
%	 tresponse=obj.tresponse 
%
function tresponse=gettresponse(obj)
	 tresponse=obj.tresponse;
