%function indstim=getindstim(mobj)
%
%Return value
%   indstim=1x2 matrix containing the first and last element of theta
%   storing the stimulus coefficents
%
function indstim=getindstim(mobj)
        %use nstimcoeff for this function not that of any derived class
        nstimcoeff=getnstimcoeff(mobj);
        indstim=[1 nstimcoeff];

 end