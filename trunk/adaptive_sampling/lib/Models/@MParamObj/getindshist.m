%function indstim=getindstim(mobj)
%
%Return value
%   indstim=1x2 matrix containing the first and last element of theta
%   storing the stimulus coefficents
%
function indshist=getindshist(mobj)
        %use nstimcoeff for this function not that of any derived class
        nstimcoeff=getnstimcoeff(mobj);
      
  %use nstimcoeff so that if derived object redefines nstim 
            if (mobj.alength>0)
                indshist=[nstimcoeff+1 nstimcoeff+mobj.alength];
            else
                indshist=[nan nan];
            end