%function d=getparamlen(mobj)
%   mobj -MParamObj
%
%Return value
%   d - length of the actual parameters
%
%Explanation: Returns the length of the actual parameters of the vector for
%the GLM
function d=getparamlen(mobj)
    d=mobj.klength*mobj.ktlength+mobj.alength;

    if (hasbias(mobj))
        d=d+1;
    end