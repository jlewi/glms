%function obj=loadobj(lobj)
%
%function obj=loadobj(obj,lobj)
%   this syntax is used to call load obj from a derived class
%
%Explanation: this function gets called by load when we load an object.
%   purpose of this function is to handle backwards compatibility issues
%   caused by change in the class definition. That is when the object was
%   saved with an older version of the class.
%
%Revisions:
%   09-23-2008  - Update to latest template of the object.
function obj=loadobj(varargin)

if (nargin==2)
    obj=varargin{1};
    lobj=varargin{2};
    
    if isa(lobj,'MParamObj')
       obj=copyfields(lobj,obj,[]);
    end
    newobj=MParamObj();
    latestver=newobj.ver;
else
    newobj=MParamObj();
    latestver=newobj.ver;
    lobj=varargin{1};
    if isstruct(lobj)
        obj=newobj;

        obj.ver=lobj.ver;
    else
        %we don't need to create a new object
        obj=lobj;
    end
end

%get the latest version
%save the latest version so we can check to make sure all
%conversions have been completed.






%******************************************************************
%Sequentially convert one version of the object to the next
%until we get to the latest version
%
%Warning: If lobj is a structure then we haven't copied the data from lobj
% to obj yet.
%******************************************************************





%**************************************************
%forwards conversion
%******************************************************
if (obj.ver<080520)
    %older version did not have a bias term
    obj.hasbias=0;
    obj.ver=080520;
end

if (obj.ver<080730)
    if ~isa(obj.glm,'GLMModel')
        fprintf('The MParamObj object stored in the file was an older version which did not store a GLM. \n');
        y=input('Do you want to set the GLM to the canonical Poisson (y/n)? \n','s');
        switch lower(y)
            case {'y','yes'}
                obj.glm=GLMPoisson('canon');
            otherwise
                warning('GLM objects stored in versions of MParamObj older than 080730 cannot be loaded from file. You will need to manually set it.');
                obj.glm=[];
        end
    end
    obj.ver=080730;
end

if (obj.ver<080923)
    %The field pinit was deleted
    if isstruct(lobj)
        fskip={'pinit'};    %fields not to copy
        obj=copyfields(lobj,obj,fskip);
    end
    obj.ver=080923;
end

%check if converted all the way to latest version
if (obj.ver <latestver)
    error('Object has not been fully converted to latest version. \n');
end

%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
%
%Copy fields must be declared within loadobj because otherwise it won't
%have permssion to set the private fields of the object.
function dest=copyfields(source,dest,skip)
%we just need to copy the fields
%and handle any special cases if required
fnames=fieldnames(source);

%struct for object
sobj=struct(dest);
for j=1:length(fnames)
    switch fnames{j}
        case skip
            %do nothing we skip this field
        otherwise
            %set the new field this will cause an error
            %if the field isn't a member of the new object
            dest.(fnames{j})=source.(fnames{j});
    end
end