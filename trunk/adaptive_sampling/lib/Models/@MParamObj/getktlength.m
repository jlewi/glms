%function ktlength=getktlength(obj)
%	 obj=MParamObj object
% 
%Return value: 
%	 ktlength=obj.ktlength 
%
function ktlength=getktlength(obj)
	 ktlength=obj.ktlength;
