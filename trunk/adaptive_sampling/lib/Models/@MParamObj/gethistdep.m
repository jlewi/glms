%function gethistdep=getgethistdep(obj)
%	 obj=MParamObj object
% 
%Return value: 
%	 returns a value depending on how far back in time the current firing
%	 rate depends. 
%    a zero means the filter has no temporal component
function histdep=gethistdep(obj)
    histdep=max(obj.ktlength-1,obj.alength);
   
