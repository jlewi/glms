%function r=compexpr(mobj,input,theta)
%   input - the input after calling projinp
%function r=compexpr(mobj, stim,theta,sr)
%       stim - the input - vector of stimulus and spike history
%                    concatenated together
%       theta - parameters for the model
%       sr    - stimulus and spike history
% Return value:
%       r - the expected value of the observations computed under the model
%
% Explanation: Computes the expected value of the observation for input
%   stim and model parameters theta.
%
%Revision 
%   01-11-2009 added calling syntax compexr(mobj,input,theta)
%
%   10-19-2007 - added sr
function r=compexpr(mobj,varargin)
    if (nargin==3)
        r=mobj.glm.fglmmu(varargin{1}'*varargin{2});
    else
         r=mobj.glm.fglmmu(projinp(mobj,varargin{1},varargin{3})'*varargin{2});
    end
