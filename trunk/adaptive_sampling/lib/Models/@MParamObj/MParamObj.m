%function mparam=MParamObj(initp)
%       initp- stucture of field value pairs
%               -field names specify the fields to intiate
%function mparam=MParamobj(fieldname,value, fieldname, value,....)
%   fieldname,value - list of fieldname and value pairs to intialize to
%
% fields to provide:
%       ktrue
%       atrue
%       mmag
%
%Optional parameters
%   atrue
%   diffusion
%   tresponse
%
% Explanation: initializes the mparam object.
%
%
% Revision:
%   10-20-2008
%          -add constructor to construct object from a different mparam
%          object
%   10-11-2008 -Added dependent property nstimcoeff
%               -added indbias, indstim, indshist
%   080923 - Use constructid to check the calling syntax and parse the
%            input arguments
%          - delete pinit - its not part of the model object any more
%
%   080730 - Use Matlab's new OOP model
%            and derive it from class handle
%   080520 - add field hasbias to keep track of whether there is a bias
%   term
classdef (ConstructOnLoad=true) MParamObj < handle

    %**************************************************************************
    %mparam structure
    %**************************************************************************
    %klength    - length of stimulus filter
    %ktlength   - how far back in history the stimulus filter extends
    %               added in ver 070910
    %           - a value of 1 means no stimulus history
    %alength    - length of spike history filter
    %glm          - GLMModel object which represents the GLM
    %
    %ktrue   - value of the true stimulus filter update
    %             klengthx1 - if parameter is constant
    %            klengthxsimparam.niter - if parameter is
    %            changing on each iteration due to diffusion
    %atrue   - value of the true spike history coefficents
    %             first coefficent corresponds to most recently
    %            fired spikes
    %              klengthx1 - if parameter is constant
    %              klengthxsimparam.niter - if parameter is
    %               changing on each iteration due to diffusion
    %.tresponse - length of response window in which we
    %            count the number of spikes after presenting the
    %            stimulus
    %.mmag - constraint on stimulus
    %               (maximum allowed magnitude) - NOT the square of the
    %               magnitude
    %.diffusion - structure specifiying diffusion parameters
    %          .on  - turn diffusion on
    %           .q    -   covariance matrix specifying gaussian diffusion
    %                   of parameter
    %                 -allows you to track moving parameter
    %hasbias    - indicates whether there is a bias term

    properties(SetAccess=private,GetAccess=public)
        ver=080923;
        klength=0;
        ktlength=1;
        alength=0;
        glm=[];
        tresponse=1;
        mmag=0;
        diffusion=struct('on',0,'q',[]);
        hasbias=false;
    end

    properties(GetAccess=public, Dependent=true)
        %number of coefficients of theta corresponding to the stimulus
        nstimcoeff;

        %stimind a matrix containing the first and last element of theta
        %which correspond to stimulus coefficients
        indstim;

        %stimind a matrix containing the first and last element of theta
        %which correspond to shist coefficients
        indshist;

        %stimind a matrix containing the first and last element of theta
        %which correspond to bias terms
        indbias;

    end

    methods(Access=public);
        indbias=getindbias(mobj);
    end
    methods
        %**********************************************************
        %For convenience we define the following dependent properties
        %which just call a get method of the same name.
        %Using a separate get method allows us to override how the
        %dependent property is computed in derived classes.
        function nstimcoeff=get.nstimcoeff(mobj)
            nstimcoeff=getnstimcoeff(mobj);
        end

        %use a function so that we can overload it if necessary and
        %distinguish between derived and base classes
        function indstim=get.indstim(mobj)
            indstim=getindstim(mobj);
        end

        function indshist=get.indshist(mobj)
            indshist=getindshist(mobj);
        end
        function indbias=get.indbias(mobj)
            indbias=getindbias(mobj);
        end

        function mparam=MParamObj(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={};

            con(2).rparams={'mmag'};

            con(3).rparams={'mobj'};

            %******************************************************
            %initialize the default structure
            %******************************************************
            mparam.diffusion=struct('on',0,'q',[]);
            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    initp=struct();
                    cind=1;
                    return;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,initp]=constructid(varargin,con);
            end



            switch cind
                case 1
                    %blank constructor do nothing


                case 2
                    %*****************************************************************
                    %initp stores all the fields user wants to set
                    %loop through them and set the appropriate fields of mparam
                    %************************************************************
                    fnames=fieldnames(initp);
                    for k=1:length(fnames)
                        switch fnames{k}
                            case 'atrue';
                                %backwards compatibility: print warning message and don't load it
                                warning('ature has been removed from newer version of mparamobj');
                            case 'ktrue'
                                %backwards compatibility: print warning message and don't load it
                                warning('ktrue has been removed from newer version of mparamobj');
                            case 'alength'
                                if (initp.alength<0)
                                    error('alength must be >0');
                                end
                                mparam.alength=initp.alength;
                            case 'klength'
                                if (initp.klength<=0)
                                    error('klength must be >=0');
                                end
                                mparam.klength=initp.klength;
                            case 'ktlength'
                                if (initp.ktlength<1)
                                    error('ktlength must be >0');
                                end
                                mparam.ktlength=initp.ktlength;
                            case 'glm'
                                if ~isa(initp.glm,'GLMModel')
                                    error('GLM must be derived from GLMModel');
                                end
                                mparam.glm=initp.glm;
                            case  'tresponse'
                                if (initp.tresponse<=0)
                                    error('tresponse must be >0');
                                end
                                mparam.tresponse=initp.tresponse;
                            case 'pinit'
                                warning('MParamObj no longer takes pinit as an argument.');
                            case 'mmag'
                                if  (initp.mmag<=0)
                                    error('mmag must be >0');
                                end
                                if ~isreal(initp.mmag)
                                    error('mmag must be real');
                                end
                                mparam.mmag=initp.mmag;
                                required.mmag=1;
                            case 'diffusion'
                                if ~isstruct(initp.diffusion)
                                    error('diffusion must be a structure with at least field .on');
                                end
                                if ~isfield(initp.diffusion,'on')
                                    error('if diffusion is given it must contain at least the field on');
                                end
                                %diffusion is on user must pass diffusion matrix
                                if (initp.diffusion.on~=0)
                                    if ~isfield(initp.diffusion,'q')
                                        error('No diffusion matrix given and diffusion is turned on');
                                    end
                                end
                                mparam.diffusion=initp.diffusion;
                            case 'hasbias'
                                mparam.hasbias=initp.hasbias;
                            otherwise
                                error(sprintf('%s not a recognized parameter for mparam \n'));
                        end
                    end

                case 3
                    %create the object from a different mparam object
                    ftocopy={'klength','ktlength','glm','mmag','alength','tresponse','hasbias','diffusion'};

                    for fn=ftocopy
                        mparam.(fn{1})=initp.mobj.(fn{1});
                    end
            end





            %******************************************************************
            %diffusion matrix
            %******************************************************************
            if (mparam.diffusion.on~=0)
                diffeig=eig(mparam.diffusion.q);

                %check dffifusion  is positive definite
                if ~isempty(find(diffeig<0))
                    error('Diffusion matrix must be postiive definite');
                end

                %check if diffusion is white
                if isempty(find(diffeig~=diffeig(1)))
                    mparam.diffusion.iswhite=1;
                    mparam.diffusion.qeig=diffeig(1);
                    fprintf('Diffusion is white \n');
                else
                    mparam.diffusion.iswhite=0;
                end

                if ~isfield(mparam.diffusion,'diffc')
                    mparam.diffusion.diffc=1;
                end
            end

            %******************************************************************
            %check pinit
            %*****************************************************************
            %this check won't work for input nonlinearities
            % if (getdim(mparam.pinit)~=(mparam.klength*mparam.ktlength+mparam.alength))
            %     error('error pinit must have dimensionality =klength*ktlength+alength');
            % end


        end
    end
end

