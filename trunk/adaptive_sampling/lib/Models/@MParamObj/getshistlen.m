%function getshistlen=shistlen(obj)
%	 obj=MParamObj object
% 
%Return value: 
%	 shistlen=obj.shistlen 
%           -The lenght in time of the spike history dependence
function shistlen=getshistlen(obj)
	 shistlen=obj.alength;
