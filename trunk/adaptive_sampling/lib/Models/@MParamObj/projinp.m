%function tinp=projinp(mparam,inp,sr)
%       mparam- the MParamObj object
%       inp         -object array of stimuli
%                   -should be GLMInput objects
%                   should be 1xmobj.ktlength long
%       sr    - object array storing past stimuli and responses
%             - or can be a vector of the spike history
% Return value
%       mxn - the input
% Explanation:
%   This function constructs the input to the glm. That is it combines the
%   following:
%   1. the stimulus
%   2. Past responses
%   3. past stimuli
%   4. nonlinear projections of the input (using the subclass
%   MGLMNonLinInp)
%   5. Fixed terms in the input
%
% Revision History:
%   01-25-2008: Because the input can now depend on past inputs
%             we can no longer project multiple inputs by passing in 
%            a matrix to inp. A matrix to inp now represents stimuli on
%            multiple trials.
%   12-31-2008: Combine past inputs if the model depends on past inputs
%
%   4-29-2008: Bug. The spike history should be ordered in ascending order
%       i.e most recent spike is last. But the way I was adding zeros I was
%       adding zeros to the end of the vector when I should have been
%       adding them to the start.
%   10-25-2007 - inp can be a matrix because projinp might be called
%       by MNonLinInp after pushing inputs through a nonlinearity
%   10-19-2007 - include spike history terms
%
%   10-19-2007 - include spike history terms
function tinp=projinp(mobj,inp,sr)
%1. if inp is an object array call getData to create an array
%2. if we have spike history terms add them
%3. add a bias/ fix terms as necessary
bias=[];
if (hasbias(mobj))
    bias=1;
end
if isa(inp,'GLMInput')


    tinp=getData(inp);
    
   
else
    tinp=inp;
end

 %append any zeros for any unknown stimuli in the past
    tinp=[zeros(mobj.klength,mobj.ktlength-size(tinp,2)) tinp];
    tinp=tinp(:);
    
shist=[];
if (getshistlen(mobj)>0)
    if (~isempty(sr) && isa(sr,'SRObj'))
        shist=getobsrv(sr)';
    else
        shist=sr;
    end
    shist=[zeros(getshistlen(mobj)-length(shist),1);shist];
end

if (mobj.hasbias)
tinp=[tinp;shist;bias];
else
    tinp=[tinp;shist;];
end
