%function theta=submanifold(obj,sp)
%   obj - descendent of TanSpaceObj
%   sp  - mxn array
%         -each column is a point at which to evaluate the manifold
%         point at which to evaluate the manifold
%         This is the vector describing u,v as a vector
%         use uvtovec to form this vector from u,v
%
%Explanation: Evaluates the submanifold for this point.
function theta=mantotheta(obj,sp)

%could probably go either way
%warning('1-20-2008: why do we use getmatdim and not getthetadim?');
theta=zeros(getparamlen(obj),size(sp,2));

for cind=1:size(sp,2)
%convert sp back to uv
[u,s,v,shist,bias]=vectouv(obj,sp(:,cind));

m=u*diag(s)*v';

theta(:,cind)=packtheta(obj,m(:),shist,bias);
end

