%function grad=gradsubmanifold(obj,manifoldp)
%   obj - child of TanSpaceBase 
%   sparam - parameters of the submanifold on which to evaluate the
%   gradient
%   
%Return value:
%   grad = dim(theta) x nb
%       gradient of theta with respect to the submanifold
%       nb is the number of vectors needed to form an orthonormal basis
%Explantion: Computes the jacobian of theta w.r.t to the parameters of the
%submanifold (i.e of the gabor function).
%   
%
%Revisions:
%   08-07-03: 
function grad=gradsubmanifold(obj,gp)

%******************************************************************
%compute the basis ignoring spike history terms and bias terms
%******************************************************************
%dimension of the matrix
mdim=[obj.klength obj.ktlength];

%rank of the matrix
rank=obj.rank;

%form the matrices of the derivative
%number of partial derivatives
npartial=dimmanifold(obj)-obj.alength-obj.hasbias;
dmat=zeros(mdim(1),mdim(2),npartial);

[u,s,v,shist,bias]=vectouv(obj,gp);
%derivatives with respect to elements of u
dind=0;
for i=1:mdim(1)
    for j=1:rank       
        dind=dind+1;
        dmat(i,:,dind)=s(j)*v(:,j)';
    end
end

%derivatives with respect to elements of v
dind=1*mdim(2)*rank;
for i=1:mdim(2)
    for j=1:rank       
        dind=dind+1;
        dmat(:,i,dind)=s(j)*u(:,j);
    end
end

%derivatives with respect to elements of s
for j=1:rank
   dind=dind+1;
   dmat(:,:,dind)=u(:,j)*v(:,j)';
end

%form vectors by reshaping
dimstim=prod(mdim);
dmat=reshape(dmat,[dimstim,npartial]);

%**************************************************************************
%incoporate bias and spike history
%*************************************************************************
%for the spike history and bias terms the gradient is just the identity
%matrix because the manifold preserves those dimensions without any
%reduction
nextra=obj.alength+obj.hasbias;
dextra=eye(nextra);

dmat=[dmat zeros(dimstim,nextra); zeros(nextra,npartial) dextra];

%form an orthonormal basis
grad=orth(dmat);