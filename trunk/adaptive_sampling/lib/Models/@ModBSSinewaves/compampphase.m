%function [tcoeff=]compamphase(mobj,bcoeff)
%   bcoeff - a vector containing the coefficients of
%            the basis functions
%          - i.e the stimcoeff as returned by parsetheta
%           -this can also be a matrix eac column corresponding to a
%           different vector
%   
%Return value:
%   tcoeff -structure array with
%   amp - the amplitude of each frequency
%           (nfreq+1)x(ntime+1) matrix
%   phase - the phase of each frequency
%
%Explanation: it would probably be faster to operate on matrices 
% I.e to compute the amplitude and phase for each stimulus treating bcoeff
% as its original matrix 
function [tcoeff]=compamphase(mobj,bcoeff)

amp=zeros(mobj.nfreq+1,mobj.ntime+1,size(bcoeff,2));

scoeff=[zeros(1,size(bcoeff,2)); bcoeff(1:mobj.nsincoeff,:)];
ccoeff=[bcoeff(mobj.nsincoeff+1:end,:)];

amp=(scoeff.^2+ccoeff.^2).^.5;

phase=atan2(scoeff,ccoeff);


%reshape amplitude and phase into a matrix
lind=(mobj.nsincoeff+1):mobj.nstimcoeff;

subind=bindexinv(mobj,lind);

tcoeff=struct('amp',zeros(mobj.nfreq+1,mobj.ntime+1),'phase',zeros(mobj.nfreq+1,mobj.ntime+1));
tcoeff=repmat(tcoeff,1,size(bcoeff,2));

aind=sub2ind([mobj.nfreq+1,mobj.ntime+1],subind.nf+1,subind.nt+1);
for col=1:size(bcoeff,2)
    
   tcoeff(col).amp(aind)=amp(:,col); 
   tcoeff(col).phase(aind)=phase(:,col); 
end


