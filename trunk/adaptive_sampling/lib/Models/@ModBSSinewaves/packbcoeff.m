%function bcoeff=packbcoeff(bcoeff)
%   bcoeff- a structure containing the coefficients for the different basis
%   functions
%           .sin - the coefficients of the sinewaves
%                - nfreqxntime matrix
%           .cos - the coefficients of the cosines
%                - nfreqxntime matrix
%           .con - constant term
%Return value:
%   bvec - A vector containing ccon,csin, packed together
%       not sin(1,1) is not stored in bvec because sin(1,1) is irrelevant
%       because the corresponding basis vector is all zeros
%   
function bvec=packbcoeff(obj,bcoeff)

%verfify the lengths
if (size(bcoeff.sin)~=[obj.nfreq+1,obj.ntime+1])
    error('Dimensions of bcoeff.sin are incorrect.');
end

if (size(bcoeff.cos)~=[obj.nfreq+1,obj.ntime+1])
    error('Dimensions of bcoeff.cos are incorrect.');
end

%create an array of nf and nt pairs which correspond
%to sin(:) and cos(:)
[nfind ntind]=ind2sub([obj.nfreq+1,obj.ntime+1],1:(obj.nfreq+1)*(obj.ntime+1));

%now we want to map nf and nt to the corresponding elements
%of bcoef
lind=bindex(obj,nfind-1,ntind-1);

bvec=zeros(obj.nstimcoeff,1);

sind=lind(:,1);
sind=sind(find(~isnan(sind)));

cind=lind(:,2);

%skip zero frequency
bvec(sind)=bcoeff.sin(2:end);

bvec(cind)=bcoeff.cos(:);

