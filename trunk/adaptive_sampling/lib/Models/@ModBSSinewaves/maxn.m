%function [nmax]=maxn(strfdim)
%   strfdim - dimensions of the strf
%
%Return value:
%   nmax - If fo is our fundamental frequency
%          then nmax is the largest value
%          s.t fo*nmax < fn
%          where fn is the nyquist frequency
%          given the strf has dimensions strfdim
%
%          thus nmax+1
%          is maximum value for nfreqs, and ntime
%          (we have to add 1 b\c the first element corresponds to n=0);
%
%Explanation: The dimensions of the strfdim determine the Nyquist
%frequency.
%
function [nmax]=maxn(strfdim)

%the fundamental frequency in each dimension
%the period for each dimension is the number of samples because
%each sample corresponds to one unit of time 
ff=1./strfdim;

%each sample corresponds to 1 unit of time
%therefore the sampling interval is 1
%so sampling freq =1
sampfreq=1;



maxfreq=sampfreq/2;



%to avoid aliasing sampling rate must be greater than maximum frequency in
%the signal.
%Its possible maxfreq./ff is not an even number. I think this happens
%if the number of samples is some odd number i.e if ff is irattional
nmax=ceil(maxfreq./ff-1);