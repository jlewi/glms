%function [theta= parsetheta(mobj,bcoeff shistcoeff bias)
%   bcoeff - structure of the basis coefficients
%
%Return value:
%   theta
%Explanation: Packs the different terms of theta into a vector
%
function theta= packtheta(mobj,bcoeff,shistcoeff,bias)

if isstruct(bcoeff)
theta=[packbcoeff(mobj,bcoeff);shistcoeff;bias];
else
theta=[bcoeff;shistcoeff;bias];    
end