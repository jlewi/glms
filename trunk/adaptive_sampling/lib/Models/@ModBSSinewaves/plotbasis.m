%function plotbasis(mobj)
%
%Explanation: Make a plot of all the basis vectors of the STRF
%   we make two figures. One for the cos and one for the sine wave basis
%   functions
function plotbasis(mobj)


width=5;
height=6;
fsin=FigObj('width',width,'height',height,'naxes',[mobj.nfreq+1 mobj.ntime+1],'name','Sine basis functions');
fcos=FigObj('width',width,'height',height,'naxes',[mobj.nfreq+1 mobj.ntime+1],'name','Cosine basis functions');


for nf=0:mobj.nfreq
    for nt=0:mobj.ntime
        
        bind=bindex(mobj,nf,nt);
        
        if (length(bind)==2)            
    setfocus(fsin.a, nf+1,nt+1);   
    imagesc(reshape(mobj.basis(:,bind(1)),mobj.klength,mobj.ktlength));
    
            cind=bind(2);
        else
           cind=bind(1);
        end
        
        
    setfocus(fcos.a, nf+1,nt+1);    
    imagesc(reshape(mobj.basis(:,cind),mobj.klength,mobj.ktlength));
    end
end

hasin=[fsin.a.ha];
hacos=[fcos.a.ha];

h=[[fsin.a.ha] [fcos.a.ha]];
   set(h, 'xlim',[1 mobj.ktlength]);
    set(h,'ylim',[1 mobj.klength]);
    
clsin=get(fsin.a,'clim');
clsin=cell2mat(clsin);
clsin=[min(clsin(:,1)) max(clsin(:,2))];
set(fsin.a,'clim',clsin);

cl=get(fcos.a,'clim');
cl=cell2mat(cl);
cl=[min(cl(:,1)) max(cl(:,2))];
set(fcos.a,'clim',cl);

lblgraph(fcos);
lblgraph(fsin);


sizesubplots(fsin);
sizesubplots(fcos);


