%function ploterrvsnfreqs(strf,maxfreqs)
%   strf - STRF
%   maxfreqs - maximum frequencies to use to represent this strf
%
%Explanation:
%   This static function helps you determine how many frequencies you need
%   to provide a good representation of some STRF.
%   We compute the error of the reconstruction as a function of the number
%   of frequencies used
%
function [mobj]=ploterrvsnfreqs(strf,maxfreqs)


mobj=ModBSSinewaves('nfreqs',maxfreqs(1),'ntime',maxfreqs,'klength',size(strf,1),'ktlength',size(strf,2),'mmag',1);

bcoeff=tosindom(mobj,strf);

err=zeros(1,maxfreqs);
%to be efficient we only compute bcoeff once. We then compute the strf for
%each frequencies by setting the coefficients to zero
%we start by using the most frequencies

%strferr - is the difference between the strf and the reconstructed strf
strferr=strf;

for nf=1:maxfreqs
    mobj=ModBSSinewaves('nfreqs',nf,'ntime',nf,'klength',size(strf,1),'ktlength',size(strf,2),'mmag',1);

    coef=tosindom(mobj,strf);
    %     coef.sin=zeros(maxfreqs,maxfreqs);
    %      coef.cos=zeros(maxfreqs,maxfreqs);
    %
    %      coef.sin(1:nf,1:nf)=bcoeff.sin(1:nf,1:nf);
    %
    %      coef.cos(1:nf,1:nf)=bcoeff.cos(1:nf,1:nf);
    %the contribution of this frequency to the strf
    scontrib=tospecdom(mobj,coef);

    %subtract this computation from the error signal
    d=strf-scontrib;
    strferr=d.^2;
    strferr=sum(strferr(:));
    strferr=strferr^.5;
    err(nf)=strferr;
end
% for nf=1:maxfreqs
%     coef.sin=zeros(maxfreqs,maxfreqs);
%     coef.cos=zeros(maxfreqs,maxfreqs);
%
%     coef.sin(:,nf)=[bcoeff.sin(1:nf,nf) ;zeros(maxfreqs-nf,1)];
%     coef.sin(nf,:)=[bcoeff.sin(nf,1:nf) zeros(1,maxfreqs-nf)];
%
%     coef.cos(:,nf)=[bcoeff.cos(1:nf,nf); zeros(maxfreqs-nf,1)];
%     coef.cos(nf,:)=[bcoeff.cos(nf,1:nf) zeros(1,maxfreqs-nf)];
%
%     %the contribution of this frequency to the strf
%     scontrib=tospecdom(mobj,coef);
%
%     %subtract this computation from the error signal
%     strferr=strferr-scontrib;
%
%    err(nf)=(strferr(:)'*strferr(:))^.5;
% end


fobj=FigObj('xlabel','# frequencies','ylabel','error');
plot(1:maxfreqs,err);