%function success=test()
%
%Explanation: Test the ModBSSinewaves
%
function [success,obj]=test()

success=true;

klength=10;
ktlength=10;

nmax=ModBSSinewaves.maxn([klength ktlength]);
nfreq=nmax(1);
ntime=nmax(2);

obj=ModBSSinewaves('nfreq',nfreq,'ntime',ntime,'klength',klength,'ktlength',ktlength,'mmag',1);

%********************************************************************
%make sure tosindom and tospecdom return analgous answers
%********************************************************************


%********************************************************************
%test tospecdom
%********************************************************************
%%randomly generate the coefficients

bcoeff.sin=floor(randn(nfreq+1,ntime+1)*100)/100;

bcoeff.cos=floor(randn(nfreq+1,ntime+1)*100)/100;
bcoeff.con=floor(randn(1,1)*100)/100;

bvec=packbcoeff(obj,bcoeff);

strf=tospecdom(obj,bvec);

bproj=tosindom(obj,strf);


if (any(abs((bproj-bvec)>10^-8)))
    success=false;
    error('There is in error in tosindom or tospecdom because the recovered basis coefficients are not correct');
end

fprintf('Success: tospecdom and tosindom are correct. \n');

% if (any(abs((bproj.cos(:)-bcoeff.cos(:))./bcoeff.cos(:))>10^-8))
  %  success=false;
  %  error('There is in error in tosindom or tospecdom because the recovered basis coefficients are not correct');
%end

% %I couldn't get the following code to work properly
% %compute the frequencies attached to each wave coefficient
% %sampling rate
% fsrow=obj.klength-1;
% fscol=obj.ktlength-1;
%
%
% %we need to compute how many points N in the fft we want to use
% %this will determine how many frequencies we have
% %we choose N so the points align with the frequencies
% %in our basis
% %so we want the increment between frequencies to be our fundamental
% %frequency
% firow=obj.fffreq;
% ficol=obj.fftime;
%
% %now compute N
% nrow=fsrow/firow;
% ncol=fscol/ficol;
%
% %now we use the fft to map the strf back to frequency domain and see if the
% %match
%
% fcoeff=fft2(strf,nrow,ncol);
%
% %normalize the coefficents by the number of samles
% fcoeff=fcoeff/(obj.klength*obj.ktlength);
%
% fshift=fftshift(fcoeff);
%
%
% %compute the sinusoidal coefficents
% % cfreq=[ceil(nrow/2), ceil(ncol/2))];
% freq.an=fshift+fshift(end:-1:1,end:-1:1);
% freq.bn=i*(fshift-fshift(end:-1:1,end:-1:1));
% freq.row=firow*[-floor(nrow/2):floor(nrow/2)];
% freq.col=ficol*[-floor(ncol/2):floor(ncol/2)];
%
%
% %find which freq.row correspond to the frequencies in our sineave
% %%
% rind=[find(freq.row==0) find(freq.row==obj.fffreq*(nfreq-1))];
% cind=[find(freq.col==0) find(freq.col==obj.fftime*(ntime-1))];
%
% fobj=FigObj('naxes',[2,2],'width',4,'height',4);
% setfocus(fobj.a,1,1);
%
% imagesc(obj.fftime*[0:ntime-1],obj.fffreq*[0:nfreq-1],bcoeff.cos);
% colorbar
% xlim(obj.fftime*[0 ntime-1]);
% ylim(obj.fffreq*[0 nfreq-1]);
%
% setfocus(fobj.a,1,2);
%
% imagesc(freq.col(cind(1):cind(2)),freq.row(rind(1):rind(2)),freq.an(rind(1):rind(2),cind(1):cind(2)));
% %xlim(obj.fftime*[0 ntime-1]);
% %ylim(obj.fffreq*[0 nfreq-1]);
% colorbar
% xlim([freq.col(cind)]);
% ylim([freq.row(rind)]);
%
% setfocus(fobj.a,2,1);
% imagesc(obj.fftime*[0:ntime-1],obj.fffreq*[0:nfreq-1],bcoeff.sin);
% xlim(obj.fftime*[0 ntime-1]);
% ylim(obj.fffreq*[0 nfreq-1]);
% colorbar
%
% setfocus(fobj.a,2,2);
% imagesc(freq.col(cind(1):cind(2)),freq.row(rind(1):rind(2)),freq.bn(rind(1):rind(2),cind(1):cind(2)));
% colorbar
% xlim([freq.col(cind)]);
% ylim([freq.row(rind)]);
%
% sizesubplots(fobj)

%keyboard

%*******************************************************************
%test compglmproj
%********************************************************************
stim=randn(obj.klength,obj.ktlength);
lmproj=compglmproj(obj, stim(:),bvec,[]);
