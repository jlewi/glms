%function bindex(mobj,nf,nt)
%   nf - Which frequency to return
%        0 to obj.nfreq
%   nt - which time frequency to return
%        0 to obj.ntime
%   
%Return value
%   bind - the column in mobj.basis corresponding to this frequency
%        - 1x2 matrix. First element is for the sin frequency
%        - second is the cosine 
%         - for nf=nt=0 we return nan since we have a single
%         coefficient
%Basically, the frequencies of the different basis vectors correspond to two matrices which have 
%   (mobj.nfreq+1) x (mobj.ntime+1) elements
%    We have two such matrices one for the sin coefficients and one for 
%   cosine coefficients. 
%   We can generate the linear index from the matrix index
%       using sub2ind
%
%   The sin term doesn't have a basis vector corresponding to the nf=nt=0
%   frequency
%
%Revisions
%   10-08-2008 - Allow nf,nt to be arrays
function bind=bindex(mobj,nf,nt)
if any(nf>mobj.nfreq)
    error('nf can''t exceed mobj.nfreq');
end
if any(nt>mobj.ntime)
    error('nt can''t exceed mobj.ntime');
end

if (length(nf)~=length(nt))
    error('nf and nt must have the same length');
end
bind=zeros(length(nf),2);

%***************************************************************
%index of the sin term
%****************************************************************
 %we add 1 to nf and nt because of the zero frequencies
  %we then subtract 1 because for the zero frequency there is no sinewave
 bind(:,1)=sub2ind([mobj.nfreq+1,mobj.ntime+1],nf+1,nt+1)-1;

 %find elements where nf and nt are both 0
j=find((nf+nt)==0);
if ~isempty(j)
    bind(j,1)=[nan];
end

 %if any term corresponds to [nf==0 and nt==0]
 %then we set the index to nan because there is no sine term for this
 %frequency
   
%******************************************************************
%index of the cos term
%*********************************************************************
%last index corresponding to a sine term
send=mobj.nsincoeff;

bind(:,2)=send+sub2ind([mobj.nfreq+1,mobj.ntime+1],nf+1,nt+1);

