%function bcoeff=packbcoeff(bvec)
%   bvec- a vector containing the coefficients for the different basis
%   functions
%         
%Return value:
%   bcoeff - A stucture array containing 
%         .sin - the coefficients of the sinewaves
%                - nfreqxntime matrix
%              
%           .cos - the coefficients of the cosines
%                - nfreqxntime matrix
%         .con - scalar for the constant term
function bcoeff=parsebcoeff(mobj,bvec)

if (length(bvec)~=mobj.nstimcoeff)
    error('bvec has incorrect dimensions');
end

bcoeff.sin=zeros(mobj.nfreq+1,mobj.ntime+1);
bcoeff.cos=zeros(mobj.nfreq+1,mobj.ntime+1);

bcoeff=repmat(bcoeff,1,size(bvec,2));

%generate the multiple subscripts for each element
%linear indexes
lind=1:mobj.nstimcoeff;
subind=bindexinv(mobj,1:mobj.nstimcoeff);

sind=find(subind.issin);
cind=find(~subind.issin);

slinind=sub2ind([mobj.nfreq+1,mobj.ntime+1],subind.nf(sind)+1,subind.nt(sind)+1);
clinind=sub2ind([mobj.nfreq+1,mobj.ntime+1],subind.nf(cind)+1,subind.nt(cind)+1);
for col=1:size(bvec,2)
%skip the first element because for zero freq there is no sin component
%we skip the zero frequency for the sin term
bcoeff(col).sin(slinind)=bvec(lind(sind),col);

bcoeff(col).cos(clinind)=bvec(lind(cind),col);

end

