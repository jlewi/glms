%function testbindex()
%
%Explanation: Test function to verify bindex and bindexinv are consistent
function success=testbindex()
success=true;
klength=40;
ktlength=10;

strfdim=[klength ktlength];
alength=0;
hasbias=0;

glm=GLMPoisson('canon');

nmax=ModBSSinewaves.maxn([klength,ktlength]);

mobj=ModBSSinewaves('nfreq',nmax(1),'ntime',nmax(2),'glm',glm,'alength',alength,'klength',strfdim(1),'ktlength',strfdim(2),'mmag',1,'hasbias',hasbias);


%generate the linear indexes
lind=1:mobj.nstimcoeff;

%generate mutliple indexes
subind=bindexinv(mobj,lind);

%verify bindexinv
for l=1:mobj.nstimcoeff
    %map this mupltiple index to a linear index
    %lincheck is 1x2 array with the indexes of the corresponding sin and
    %cosine terms in its two columns
    lincheck=bindex(mobj,subind.nf(l),subind.nt(l));
    
    %its a sin term so we take the first element of lincheck
    if (subind.issin(l))
        lincheck=lincheck(1);
    else
        lincheck=lincheck(2);
    end

    %so lincheck and l should bequal
    if (lincheck~=l)        
        error('bindexinv and bindex are not consistent');
        success=false;
    end
 
end

if (success)
    fprintf('Success: bindex and bindexinv are correctly handling the mappings. \n');
else
    
        error('bindexinv and bindex are not consistent');
end



    

