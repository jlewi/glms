%function =plottheta(mobj,theta,txt)
%   theta - fourier coefficients or a structure containing the amplitudes
%         - can also be the complete theta
%
%   txt   - optional text to add to top of plot
%
%Explanation: Plots the fourier coefficinets
function [fh,oinfo]=plottheta(mobj,theta,txt)

if ~exist('txt','var')
    txt=[];
end

if isstruct(theta)
    bcoeff=theta;
    bvec=packbcoeff(mobj,theta);

    shistcoeff=[];
    bias=[];
else
    if (length(theta)==getparamlen(mobj))
        [theta,shistcoeff,bias]=parsetheta(mobj,theta);
    else
        shistcoeff=[];
        bias=[];
    end

    bvec=theta;
    bcoeff=parsebcoeff(mobj,theta);
end

fh=FigObj('name','Fourier Coefficents','naxes',[2,2],'width',5,'height',6);

%**************************************************************************
%plot the strf
%**************************************************************************
row=1;
col=1;
setfocus(fh.a,row,col);


title(fh.a(row,col),'STRF');
imagesc(tospecdom(mobj,bvec));

hc=colorbar;
sethc(fh.a(row,col),hc);
xlim([1 mobj.ktlength]);
ylim([1 mobj.klength]);



%************************************************************************
%Plot the spike history
%***********************************************************************
row=1;
col=2;
if (mobj.alength>0)
    setfocus(fh.a,row,col);

    t=-1*[getshistlen(mobj):-1:1];
    hp=plot(t,shistcoeff);
    pstyle.marker='.';
    pstyle.markerfacecolor='b';
    addplot(fh.a(row,col),'hp',hp,'pstyle',pstyle);
    xlabel(fh.a(row,col),'time(s)');
    title(fh.a(row,col),'spike history');
    xlim([t(1) t(end)]);
else

    set(fh.a(row,col),'visible','off');
end
%*************************************************************************
%plot the amplitude and phase
%*************************************************************************

x=0:mobj.ntime;
y=0:mobj.nfreq;
[tcoeff]=compampphase(mobj,bvec);

row=2;
col=1;
setfocus(fh.a,row,col);

imagesc(x,y,log10(tcoeff.amp));
hc=colorbar;
sethc(fh.a(row,col),hc);

title(fh.a(row,col),'log10(Amplitude)');
xlabel(fh.a(row,col),'ntime');
ylabel(fh.a(row,col),'nfreq');
xlim([0 mobj.ntime]);
ylim([0 mobj.nfreq]);

%%
row=2;
col=2;
setfocus(fh.a,row,col);

imagesc(x,y,tcoeff.phase);
hc=colorbar;
sethc(fh.a(row,col),hc);

title(fh.a(row,col),'phase');
xlabel(fh.a(row,col),'ntime');
xlim([0 mobj.ntime]);
ylim([0 mobj.nfreq]);


%**************************************************************************
%Add some text: describing the simulation
%***********************************************************************
tpos=[0 0];
[tpos(1) tpos(2)]=intorel(fh,fh.width/2,-.25);

if isempty(txt)
txt='';
end

%add the bias to the text
if (~isempty(bias))
    txt=sprintf('%s,bias: %d',txt,bias);
else
   txt=sprintf('%s\nbias: none',bias);
end
    

ht=addtext(fh,tpos,txt);
lblgraph(fh);

if ~isempty(txt)
    %leave buffer for the text
    ebtxt=get(ht,'extent');
sizesubplots(fh,[],[],[],[0 0 0 (1-ebtxt(2))+.02]);
else
sizesubplots(fh);
end