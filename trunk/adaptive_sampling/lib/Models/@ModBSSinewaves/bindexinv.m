%function [ind]=bindexinv(mobj,bind)
%   bind - linear index into bcoeff
%   
%Return value
%   bind - structure
%         .issin - true false indicating whethter its a sine term or not
%         .nf   - the corresponding nf
%                0:mobj.nfreq
%         .nt   - the corresponding nt
%                0:mobj.ntime;
%
%
%Explanation:
%   This is the inverse of bindex. Bindex essentially maps multiple subscripts into the linear
%   index. This function does the oppositie. It maps the linear index 
%   into multiple subscripts
%
%This function must be consistent with bindex
function subind=bindexinv(mobj,bind)
if any(bind> mobj.nstimcoeff)
    error('bind can''t exceed mobj.nstimcoeff');
end
bind=colvector(bind);
ind=zeros(length(bind),3);

%**************************************************************
%determine whether each term is a sine term or a cosine term
%***************************************************************
%ind is 0 for sine terms and 1 for cosine terms
%we use this later
ind(:,1)=(bind>mobj.nsincoeff);

%sind is an array with 1's for the sine terms
sind=1-ind(:,1);

%***************************************************************
%compute the linear index
% 1. For the sine terms we need to add 1 
%       because we do not store the sine term for the zero frequency
% 2. for the cosine terms we need to subtract nsincoeff
%****************************************************************
bind=bind+sind*1;

bind=bind-ind(:,1)*mobj.nsincoeff;

[ind(:,2),ind(:,3)]=ind2sub([mobj.nfreq+1,mobj.ntime+1],bind);

%we need to flip the bit
subind.issin=sind;

%we subtract 1 because nf ant nt should range from 0:mobj.nfreq and
%0:mong.ntime
subind.nf=ind(:,2)-1;
subind.nt=ind(:,3)-1;

