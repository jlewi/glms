%function [stimcoeff shistcoeff bias]= parsetheta(mobj,theta)
%   theta - vector of parameers
%
%Return value:
%   bcoeff - the components of theta corresponding to the basis functions
%            of the STRF
%   shistcoeff - the components of theta corresponding to the spike history
%   bias     - the bias coefficient
%
%Explanation: Divides theta into its component parts
%
%
function [stimcoeff,shistcoeff,bias]= parsetheta(mobj,theta)

nstimcoeff=mobj.nstimcoeff;

stimcoeff=theta(1:nstimcoeff,1);
shistcoeff=theta(nstimcoeff+1:nstimcoeff+getshistlen(mobj));

if hasbias(mobj)
   bias=theta(end);
else
    bias=[];
end