%function success=alltests()
%
%Explanation run all of the different tests
function allsuccess=alltests()

allsuccess=true;

%test function
[success,mobj]=ModBSSinewaves.test();

if ~(success)
    allsuccess=false;
end

%test function to test basis functions are orthogonal and
%normalized
strfdim=ceil([rand(1,2)*20]);

[success]=ModBSSinewaves.testbasis(strfdim);

if ~(success)
    allsuccess=false;
end


%test bindex and bindexinv
[success]=ModBSSinewaves.testbindex();


if ~(success)
    allsuccess=false;
end
