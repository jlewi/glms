%***********************************************************************
%plot the estimated posterior entropy
%*************************************************************************
% can be called as function or script
function  [fentropy]=plotentropy(varargin)
%*****************************************************************
%default values
%****************************************************************
setpaths;
fname=fullfile(RESULTSDIR,'logistic','01_23','01_23_numlog_001.mat');
%*******************************************************************
%override variables
for j=1:2:nargin
    switch varargin{j}
        case {'fname','filename','file','simfile'}
            fname=varargin{j+1};
        otherwise
            if exist(varargin{j},'var')
                eval(sprintf('%s=varargin{j+1};',varargin{j}));
            else
                fprintf('%s unrecognized parameter. \n',varargin{j});
            end
    end
end


fprintf('\n***************************\n Entropy for file: %s \n***********************\n',fname);
 [pmax, simparammax, mparam, srmax]=loadsim('simfile',fname, 'simvar','simmax');
 [prand, simparamrand, mparam, srrand]=loadsim('simfile',fname, 'simvar','simrand');

fentropy.hf=figure;
fentropy.hp=[];
fentropy.lbls={};
fentropy.xlabel='Iteration';
fentropy.ylabel='Entropy';
fentropy.title='';
fentropy.name='Entropy';    %window name

fentropy.fname='entropy';
fentropy.axisfontsize=20;
fentropy.lgndfontsize=20;

%random
hold on
pind=1;
fentropy.hp(pind)=plot(0:simparammax.niter,pmax.entropy,getptype(pind));
fentropy.lbls{pind}='info. max.';

pind=pind+1;
fentropy.hp(pind)=plot(0:simparamrand.niter,prand.entropy,getptype(pind));
fentropy.lbls{pind}='random';

lblgraph(fentropy);

