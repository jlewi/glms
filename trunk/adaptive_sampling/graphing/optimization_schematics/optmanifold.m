%8-30-2006
%
%make a plot of the manifold in 2-d over which optimization takes place.

fman.hf=figure;
%compute a posterior
prior.m=[0;0];
prior.c=.05*[1 0;0 1];
stim=[2.3;2.3];
dt=1;
obsrv.n=10;
obsrv.twindow=1;
mmag=1;

%**************************************************************************
%load parameters from a file
%*****************************************************************
data=[];
data(end+1).infile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/glmgen/10_12/10_12_maxtrack_001.mat');
data(end).outfile=fullfile(RESULTSDIR,'glmgen/10_12/quadreg_001.eps');
data(end).trials=[1];
dind=1;
load(data(1).infile);
   trial=data(dind).trials;
post.m=pmax.newton1d.m(:,trial+1);
    % post.c=pmax.newton1d.c{trial+1}(1:mparam.klength,1:mparam.klength);
  post.c=pmax.newton1d.c{trial+1};
mmag=mparam.mmag;
    
axisfontsize=30;

muproj=[-mmag:.005:mmag];
mu=muproj*(post.m'*post.m)^.5;
sigma=[0:.0005:.01 .01:.005:1.15];
zvals=zeros(length(sigma),length(mu));

for xind=1:length(mu)
    for yind=1:length(sigma)
         %compute the terms
         zvals(yind,xind)=-sigma(yind)*exp(mu(xind)+.5*sigma(yind));
    end
end
       

fopt.hf=figure;

%contour surface
hcont=contourf(mu,sigma,zvals,50);
clim=get(gca,'clim');
%hsurf=surf(mu,sigma,zvals);
%get rid of the edge colors     
set(gca,'FontSize',axisfontsize);
zlim([-6 2]);
xlabel('\mu_\epsilon');
ylabel('\sigma^2');
zlabel('Elog|C_{t+1}|');

hc=colorbar;
set(hc,'FontSize',axisfontsize);
%set(gca,'xtick',[]);
%set(gca,'ytick',[]);
%set(gca,'ztick',[]);
set(gca,'Visible','on');

%saveas(fopt.hf,fullfile('/home/jlewi/cvs_ece/writeup/nips','optmanifold.png'))

%****************************************************************
%now make a plot just over the feasible region. 
%*************************************************
freg.hf=figure;
freg.axisfontsize=30;
freg.xlabel='\mu_\epsilon';
freg.ylabel='\sigma^2';

%to do this we iterate over the points and set the value of z for points
%outside the feasible region to inf
zreg=zvals;
qmax=zeros(1,length(mu));
qmin=zeros(1,length(mu));
     %*************************************************************************
    %Define the quadratic form
        muk=post.m(1:mparam.klength,1);
    mukmag=(muk'*muk)^.5;
    muk=muk/mukmag;
     muk=post.m;
    mukmag=(muk'*muk)^.5;
    muk=muk/mukmag;
    
    
    shist=[];
    [eigck.evecs eigck.eigd]=svd(post.c);
    eigck.eigd=diag(eigck.eigd);
    
    [quad]=quadmodshist(post.c,eigck,muk,shist);

%set points outside the valid region to NaN
%this will prevent them from being drawn.

ival=NaN;
for mind=1:length(mu)    
        [qmax(mind),qmin(mind),xmax,xmin]=maxminquad(quad,muk,muproj(mind),mmag,simparam.optparam);
        ind=find(sigma>qmax(mind));
        zreg(ind,mind)=ival;
        ind=find(sigma<qmin(mind));

                zreg(ind,mind)=ival;
    
end


hreg=[];
%use same color limits as before
hreg=contourf(mu,sigma,zreg,50);
set(gca,'FontSize',freg.axisfontsize);
%use same color limits as previous fig
set(gca,'clim',clim);
xlabel(freg.xlabel);
ylabel(freg.ylabel);

hc=colorbar;
set(hc,'FontSize',freg.axisfontsize);
%saveas(freg.hf,fullfile('/home/jlewi/cvs_ece/writeup/nips','optmanifoldreg.png'))
return
%plot a contour map of just the region
%add the color white to the current colorbar
%this will zero out points we don't like (i.e those set to ival)
hcmap=colormap;
%hcmap(end+1,:)=[1 1 1];

colormap(hcmap);
%set the limits for the colorbar
clim=[min(zvals(:)) max(zvals(:))];
set(gca,'clim',clim);
colorbar;