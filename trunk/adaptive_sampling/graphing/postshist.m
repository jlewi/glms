%make a plot of the means estimated when spikehistory terms are or arenot
%included

dind=4;
data=[];

data(end+1).infile=fullfile('results', 'tracking','06_05','06_05_nohistopt_010.mat');
data(end).koutfile=fullfile('writeup','nips','shistcompare_kmean.eps');
data(end).aoutfile=fullfile('writeup','nips','shistcompare_amean.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[-1 1];
data(end).aylimits=[-.3 0];
data(end).trials=[150];

data(end+1).infile=fullfile('results', 'tracking','06_05','06_05_nohistopt_011.mat');
data(end).koutfile=fullfile('writeup','nips','shistcompare_kmean.eps');
data(end).aoutfile=fullfile('writeup','nips','shistcompare_amean.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[-1 1];
data(end).aylimits=[-.3 0];
data(end).trials=[150];

data(end+1).infile=fullfile('results', 'tracking','06_05','06_05_nohistopt_012.mat');
data(end).koutfile=fullfile('writeup','nips','shistcompare_kmean.eps');
data(end).aoutfile=fullfile('writeup','nips','shistcompare_amean.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1.5];
data(end).aylimits=[-.3 0];
data(end).trials=[150];

data(end+1).infile=fullfile('results', 'tracking','06_06','06_06_nohistopt_005.mat');
data(end).koutfile=fullfile('writeup','nips','shistcompare_kmean.eps');
data(end).aoutfile=fullfile('writeup','nips','shistcompare_amean.eps');
data(end).ylimits=[-.1 1.5];
data(end).aylimits=[-.3 0];
data(end).trials=[100 150, 200, 500];

data(end+1).infile=fullfile(RESULTSDIR, 'nips','12_19','12_19_shist_002.mat');
data(end).koutfile=fullfile(RESULTSDIR,'nips','12_19','shistcompare_kmean.eps');
data(end).aoutfile=fullfile(RESULTSDIR,'nips','12_19','shistcompare_amean.eps');
data(end).ylimits=[-.1 1.5];
data(end).aylimits=[-.3 0];
data(end).trials=[400 800];
dind=length(data);
marksize=10;
if (dind==0)
    %use following parameters and data in the workspace
    
    %which trials to plot it on.
    %trial of 0 corresponds to prior
    %we add 1 to trials to get the actual index into the array at which this
    %trial is stored this is taken care of by the script
    trials=[0:30:100];
    trials=[0 200 400];
    
    ylimits=[-1 1]; %limits for the axes

    %create suffix for file
    %get directory of current file
    if ~isempty(ropts.fname)
        [outdir,fname] =fileparts(ropts.fname);
        if isfield(ropts,'trialindex')
            trialindex=ropts.trialindex;
        else
            trialindex=str2num(fname(end-2:end));
        end
        trialdate=(fname(1:5));

        fsuffix=sprintf('_%s_%.3i.eps',trialdate,trialindex);
    else
     outdir=RESULTSDIR;
     fusffix='';
    end
else
    %load data from file
    load(data(dind).infile);
    trials=data(dind).trials;
   ylimits=data(dind).ylimits;
      aylimits=data(dind).aylimits;
    koutfile=data(end).koutfile;
    aoutfile=data(end).aoutfile;
    fprintf('\n\n Plotting data from file: \n %s \n', data(dind).infile);
end


ntrials=length(trials);
%linestyles
ls={'--',':','-.'};
lwidthtrue=3;
lwidth=2;
%methods structure is an array of structures
% each element describes a structure in which results are stored
% .rvar - name of the structure storing the results for the simulation
% .simvar-  name of variable containing the simulation parameters
% i.e methods(1).pname='pmax'
%     means workspace should contain a structure called pmax
%
methods=[];
methods(1).rvar='pmax';
methods(1).simvar='simparammax';
methods(1).label='info max';

methods(2).rvar='prand';
methods(2).simvar='simparamrand';
methods(2).label='random';

methods(3).rvar='pnohist';
methods(3).simvar='simparamnohist';
methods(3).label='Info max - incorrect model';


%construct a list of enabled methods
monf={};
mnames=fieldnames(pmax);
for index=1:length(fieldnames(pmax))
    %1-02-07 had to modify followign code because it looks like I changed
    %my structure
    if isfield(pmax.(mnames{index}),'on')
    if pmax.(mnames{index}).on >0
        monf{length(monf)+1}=mnames{index};
    end
    else
        %assume method is on
        monf{length(monf)+1}=mnames{index};
    end
end

%if the number of trials exceeds the number of iterations then truncate it
ind=find((trials+1)>(simparam.niter+1));
if ~isempty(ind)
    trials=trials(1:ind(1)-1);
    ntrials=length(trials);
end
%**************************************************************************
%obtain the data
%**************************************************************************
%we collect the data into an array of structures.
%each element of the array contains the data for the posterior on multiple
%trials it has following fields
%   .trials - a list of trials specifying the trial on which following
%               was collected
%   .m      - each col is mean after corresponding trial in trials
%   .var  - i,j th element is the varance of element i of theta on trial j
%   .lbl     -lbl for this trace
%   .ktrue - true parameters
%   .hp     - handle to plots
%number of data series = elements of pdata
nseries=0;

for index=1:length(methods)
    %extract the results parameters and simulation parameters
    eval(sprintf('presults=%s;',methods(index).rvar));
    eval(sprintf('sparam=%s;',methods(index).simvar));

    nseries=nseries+1;
    pdata(nseries).lbl=methods(index).label;

    pdata(nseries).m=zeros(size(presults.newton1d.m,1),length(trials));
    pdata(nseries).var=zeros(size(presults.newton1d.m,1),length(trials));


    for tindex=1:length(trials);

        %add 1 b\c a trial of 0 is prior
        %trial is the index into the array presults
        trial=trials(tindex)+1;

        pdata(nseries).trial(tindex)=trials(tindex);
        %add 1 so that trial of 0 corresponds to prior
        pdata(nseries).kmean(:,tindex)=presults.newton1d.m(1:mparam.klength,trial);
        kvar=presults.newton1d.c{trial};
        kvar=diag(kvar(1:mparam.klength,1:mparam.klength));
        pdata(nseries).kstd(:,tindex)=kvar.^2;
        
        if (size(presults.newton1d.m,1)>mparam.klength)
            pdata(nseries).amean(:,tindex)=presults.newton1d.m(mparam.klength+1:end,trial);
            
            avar=presults.newton1d.c{trial};
           avar=diag(avar(mparam.klength+1:end,mparam.klength+1:end));
            pdata(nseries).astd(:,tindex)=avar.^2;
        end
        pdata(nseries).hp=zeros(1,ntrials);
        

    end
end

%**************************************************************************
%Plots
%************************************************************************
%if we want to make the axes square i.e 4x4
%nrows=ceil(ntrials^.5);
%while (ceil(ntrials/nrows)~=(ntrials/nrows))
%    nrows=nrows+1;
%end
%ncols=ntrials/nrows;
nrows=ntrials;
ncols=1;

fseries.hf=figure();
fseries.axisfontsize=15;
fseries.xlabel='i'
fseries.fname=koutfile;
set(fseries.hf,'name',sprintf('postseries:'));
ntrials=length(trials);
for tindex=1:ntrials
    subplot(nrows,ncols,tindex);
    set(gca,'FontSize',fseries.axisfontsize);
    hold on;
    %plot all series on same axes
    for sindex=1:nseries

        %ftiming.hp(pind)=plot(d,searchtime,getptype(pind,1));
        d=size(pdata(nseries).m,1);
        %pdata(sindex).hp(tindex)=errorbar([1:d],pdata(sindex).m(:,tindex),pdata(sindex).var(:,tindex));
        %pdata(sindex).hp(tindex)=errorbar([1:d],pdata(sindex).m(:,tindex),0);
        
        %plot without error bars
        if sindex==3
                    pdata(sindex).hp(tindex)=plot([1:d],pdata(sindex).kmean(:,tindex),getptype(sindex,1));
         plot([1:d],pdata(sindex).kmean(:,tindex),getptype(sindex,2));
        else
            
        pdata(sindex).hp(tindex)=plot([1:d],pdata(sindex).kmean(:,tindex));
                set(pdata(sindex).hp(tindex),'LineStyle',ls{sindex});
        end
        ylim(ylimits);
        [c,mark]=getptype(sindex,1);
        %set(pdata(sindex).hp(tindex),'Marker', mark);
        set(pdata(sindex).hp(tindex),'MarkerSize', marksize);
        set(pdata(sindex).hp(tindex),'LineWidth',lwidth);
        set(pdata(sindex).hp(tindex),'Color',c);

    end
    %set(ftiming.hp(pind),'LineStyle','none');

    %turn off xlabels and on all but the last graph
    if (tindex <(ntrials))
          set(gca,'xtick',[]);  
    end
   

    xlim([1 d]);
    %title(sprintf('Trial %d', pdata(1).trial(tindex)));
    %don't use title as this ends up wasting space
    %subplot(nrows,ncols,tindex);
    ylimits=ylim;
    
    %ht=text((d+1)/2,ylimits(end),sprintf('Trial %d', pdata(1).trial(tindex)));    
    %set(ht,'FontSize',fseries.axisfontsize);
    
    %plot as a reference the true parameter value
    hptrue=plot([1:d],mparam.ktrue,getptype(nseries+1,2));
    set(hptrue,'LineStyle','-');
    set(hptrue,'LineWidth',lwidthtrue);
    ylbl=sprintf('Trial %d\n\n k_i',pdata(1).trial(tindex));
    ylabel(ylbl);
    legend([pdata(1).hp(1) pdata(2).hp(1) pdata(3).hp(1)  hptrue],{pdata(:).lbl, 'k true'});
end


lblgraph(fseries);
%attach the ylabel to middle subplot
%subplot(nrows,ncols ,ceil(ntrials/2));
%fseries.hylabel=ylabel('$\hat{\theta}_i$');
%set(fseries.hylabel,'interpreter','Latex');


%**************************************************************************
%Plot the spike history terms
%**************************************************************************

fhcoef.hf=figure();
fhcoef.axisfontsize=15;
fhcoef.xlabel='i'
fhcoef.fname=aoutfile;
set(fhcoef.hf,'name',sprintf('postseries:'));
ntrials=length(trials);
for tindex=1:ntrials
    subplot(nrows,ncols,tindex);
    set(gca,'FontSize',fhcoef.axisfontsize);
    hold on;
    %plot all series on same axes
    for sindex=1:nseries

        %ftiming.hp(pind)=plot(d,searchtime,getptype(pind,1));

        %pdata(sindex).hp(tindex)=errorbar([1:d],pdata(sindex).m(:,tindex),pdata(sindex).var(:,tindex));
        %pdata(sindex).hp(tindex)=errorbar([1:d],pdata(sindex).m(:,tindex),0);
        
        %plot without error bars
        if ~isempty(pdata(sindex).amean)
        pdata(sindex).hp(tindex)=plot([1:mparam.alength],pdata(sindex).amean(:,tindex));
        ylim(aylimits);
        [c,mark]=getptype(sindex,1);
        %set(pdata(sindex).hp(tindex),'Marker', mark);
        set(pdata(sindex).hp(tindex),'LineWidth',lwidth);
        set(pdata(sindex).hp(tindex),'Color',c);
        set(pdata(sindex).hp(tindex),'LineStyle',ls{sindex});
        end
    end
    %set(ftiming.hp(pind),'LineStyle','none');

    %turn off xlabels and on all but the last graph
    if (tindex <(ntrials))
          set(gca,'xtick',[]);  
    end
   

    xlim([1 mparam.alength]);
    %title(sprintf('Trial %d', pdata(1).trial(tindex)));
    %don't use title as this ends up wasting space
    %subplot(nrows,ncols,tindex);
    ylimits=ylim;
    
    %ht=text((d+1)/2,ylimits(end),sprintf('Trial %d', pdata(1).trial(tindex)));    
    %set(ht,'FontSize',fhcoef.axisfontsize);
    
    %plot as a reference the true parameter value
    hptrue=plot([1:mparam.alength],mparam.atrue,getptype(nseries+1,2));
    set(hptrue,'LineStyle','-');
    set(hptrue,'LineWidth',lwidth);
    ylbl=sprintf('Trial %d\n\n a_i',pdata(1).trial(tindex));
    ylabel(ylbl);
    xlabel('i');
    legend([pdata(1).hp(1) pdata(2).hp(1) hptrue],{pdata(1:2).lbl, 'a true'});
end

exportfig(fseries.hf,koutfile,'bounds','tight','Color','bw');
exportfig(fhcoef.hf,aoutfile,'bounds','tight','Color','bw');