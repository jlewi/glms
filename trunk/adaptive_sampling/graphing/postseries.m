%make a series of horizontal plots
%each horizontal plot indicates the posterior after a trial

%create a structure which contains the relevant fields for different data
%sets we then select which element of the structure we want to select the
%data. if we want to use data in the current workspace set dind=0;
dind=3;

data=[];
%tracking results for nips
%these results are for diffusion
data(end+1).infile=fullfile('results', 'tracking','05_31','05_31_maxtrack_004.mat');
data(end).outdir=fullfile('writeup','nips');
data(end).xlimits=[0 499];
data(end).ylimits=[-1 1];
data(end).trials=[0 50 100 300 400];
data(end).fsuffix='_tracking';

data(end+1).infile=fullfile('results', 'tracking','06_02','06_02_bumptrack_005.mat');
data(end).outfile=fullfile('writeup','nips','bumptrack_series.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[0 1];
data(end).trials=[0 200 400];
data(end).fsuffix='_bumptrack';

data(end+1).infile=fullfile('results', 'tracking','06_03','06_03_bumptrack_018.mat');
data(end).outfile=fullfile('writeup','nips','bumptrack_series.eps');
data(end).xlimits=[0 100];
data(end).ylimits=[-.5 1];
data(end).trials=[0 200 400];
data(end).fsuffix='_bumptrack';

if (dind==0)
    %use following parameters and data in the workspace
    
    %which trials to plot it on.
    %trial of 0 corresponds to prior
    %we add 1 to trials to get the actual index into the array at which this
    %trial is stored this is taken care of by the script
    trials=[0:30:100];
    trials=[0 200 400];
    
    ylimits=[-1 1]; %limits for the axes

    %create suffix for file
    %get directory of current file
    if ~isempty(ropts.fname)
        [outdir,fname] =fileparts(ropts.fname);
        if isfield(ropts,'trialindex')
            trialindex=ropts.trialindex;
        else
            trialindex=str2num(fname(end-2:end));
        end
        trialdate=(fname(1:5));

        fsuffix=sprintf('_%s_%.3i.eps',trialdate,trialindex);
    else
     outdir=RESULTSDIR;
     fusffix='';
    end
else
    %load data from file
    load(data(dind).infile);
    trials=data(dind).trials;
   ylimits=data(dind).ylimits;
    
    if isfield(data(dind),'outfile')
        outfile=data(dind).outfile;
        
    else
         outdir=data(dind).outdir;
         fsuffix=data(dind).fsuffix;
         outfile=fullfile(outdir,sprintf('postseries%s.eps',fsuffix))
    end
    fprintf('\n\n Plotting data from file: \n %s \n', data(dind).infile);
end


ntrials=length(trials);
%linestyles
ls={'--',':'};
lwidth=3;
%methods structure is an array of structures
% each element describes a structure in which results are stored
% .rvar - name of the structure storing the results for the simulation
% .simvar-  name of variable containing the simulation parameters
% i.e methods(1).pname='pmax'
%     means workspace should contain a structure called pmax
%
methods=[];
methods(1).rvar='pmax';
methods(1).simvar='simparammax';

methods(1).label='InfoMax';
methods(2).rvar='prand';
methods(2).simvar='simparamrand';

methods(2).label='Random';



%construct a list of enabled methods
monf={};
mnames=fieldnames(pmax);
for index=1:length(fieldnames(pmax))
    if pmax.(mnames{index}).on >0
        monf{length(monf)+1}=mnames{index};
    end
end

%if the number of trials exceeds the number of iterations then truncate it
ind=find((trials+1)>(simparam.niter+1));
if ~isempty(ind)
    trials=trials(1:ind(1)-1);
    ntrials=length(trials);
end
%**************************************************************************
%obtain the data
%**************************************************************************
%we collect the data into an array of structures.
%each element of the array contains the data for the posterior on multiple
%trials it has following fields
%   .trials - a list of trials specifying the trial on which following
%               was collected
%   .m      - each col is mean after corresponding trial in trials
%   .var  - i,j th element is the varance of element i of theta on trial j
%   .lbl     -lbl for this trace
%   .ktrue - true parameters
%   .hp     - handle to plots
%number of data series = elements of pdata
nseries=0;

for index=1:length(methods)
    %extract the results parameters and simulation parameters
    eval(sprintf('presults=%s;',methods(index).rvar));
    eval(sprintf('sparam=%s;',methods(index).simvar));

    nseries=nseries+1;
    pdata(nseries).lbl=methods(index).label;

    pdata(nseries).m=zeros(size(presults.newton1d.m,1),length(trials));
    pdata(nseries).var=zeros(size(presults.newton1d.m,1),length(trials));


    for tindex=1:length(trials);

        %add 1 b\c a trial of 0 is prior
        %trial is the index into the array presults
        trial=trials(tindex)+1;

        pdata(nseries).trial(tindex)=trials(tindex);
        %add 1 so that trial of 0 corresponds to prior
        pdata(nseries).m(:,tindex)=presults.newton1d.m(:,trial);
     %   pdata(nseries).var(:,tindex)=diag(presults.newton1d.c{trial});
        pdata(nseries).hp=zeros(1,ntrials);
        

    end
   %compute the squared error of each term. So if we were to sum across
   %rows and take the square root we would get the MSE.
   if (size(mparam.ktrue,2)==1)
       %in non-diffusion case mparam.ktrue is same on all trials
       pdata(nseries).mse=(pdata(nseries).m-mparam.ktrue *ones(1,length(trials))).^2;
   else
       %mparam.ktrue has simparam.niter columns - one for each iteration
       %thus mparam.ktrue(:,i) is ktrue on ith simulation
       %we need to get the indexes (kind) corresponding to the trial
       %trias+1. For the prior. THe true kparam is stored in the first
       %column of ktrue as it is for the second trial
       ind=find(trials==0);
       kind=trials;
       kind(ind)=1;
       pdata(nseries).mse=(pdata(nseries).m-mparam.ktrue(:,kind)).^2;
   end
end

%**************************************************************************
%Plots
%************************************************************************
%if we want to make the axes square i.e 4x4
%nrows=ceil(ntrials^.5);
%while (ceil(ntrials/nrows)~=(ntrials/nrows))
%    nrows=nrows+1;
%end
%ncols=ntrials/nrows;
nrows=ntrials;
ncols=1;

fseries.hf=figure();
fseries.axisfontsize=15;
fseries.xlabel='i'
fseries.fname=outfile;
set(fseries.hf,'name',sprintf('postseries:'));
ntrials=length(trials);
for tindex=1:ntrials
    subplot(nrows,ncols,tindex);
    set(gca,'FontSize',fseries.axisfontsize);
    hold on;
    %plot all series on same axes
    for sindex=1:nseries

        %ftiming.hp(pind)=plot(d,searchtime,getptype(pind,1));
        d=size(pdata(nseries).m,1);
        %pdata(sindex).hp(tindex)=errorbar([1:d],pdata(sindex).m(:,tindex),pdata(sindex).var(:,tindex));
        %pdata(sindex).hp(tindex)=errorbar([1:d],pdata(sindex).m(:,tindex),0);
        
        %plot without error bars
        pdata(sindex).hp(tindex)=plot([1:d],pdata(sindex).m(:,tindex));
        ylim(ylimits);
        [c,mark]=getptype(sindex,1);
        %set(pdata(sindex).hp(tindex),'Marker', mark);
        set(pdata(sindex).hp(tindex),'LineWidth',lwidth);
        set(pdata(sindex).hp(tindex),'Color',c);
        set(pdata(sindex).hp(tindex),'LineStyle',ls{sindex});
    end
    %set(ftiming.hp(pind),'LineStyle','none');

    %turn off xlabels and on all but the last graph
    if (tindex <(ntrials))
          set(gca,'xtick',[]);  
    end
   

    xlim([1 d]);
    %title(sprintf('Trial %d', pdata(1).trial(tindex)));
    %don't use title as this ends up wasting space
    %subplot(nrows,ncols,tindex);
    ylimits=ylim;
    
    %ht=text((d+1)/2,ylimits(end),sprintf('Trial %d', pdata(1).trial(tindex)));    
    %set(ht,'FontSize',fseries.axisfontsize);
    
    %plot as a reference the true parameter value
    hptrue=plot([1:d],mparam.ktrue(:,trials(tindex)+1),getptype(nseries+1,2));
    set(hptrue,'LineStyle','-');
    set(hptrue,'LineWidth',lwidth);
    ylbl=sprintf('Trial %d\n\n \\theta_i',pdata(1).trial(tindex));
    ylabel(ylbl);
    legend([pdata(1).hp(1) pdata(2).hp(1) hptrue],{pdata(:).lbl, '\theta true'});
end


lblgraph(fseries);
%attach the ylabel to middle subplot
%subplot(nrows,ncols ,ceil(ntrials/2));
%fseries.hylabel=ylabel('$\hat{\theta}_i$');
%set(fseries.hylabel,'interpreter','Latex');
exportfig(fseries.hf,fseries.fname,'bounds','tight','Color','bw');

%**************************************************************************
%determine how man rows and columns to have



%make a plot of the meansquared error mse
fmseser.hf=figure();
fmseser.hp=[];
fmseser.lbls={};
fmseser.xlabel='$i$';
fmseser.ylabel='';
fmseser.name='Series Error';
fmseser.axisfontsize=15;

%subplot handles
fmseser.hsubs=zeros(1,ntrials);

ntrials=length(trials);
for tindex=1:ntrials
    fmseser.hsubs(tindex)=subplot(nrows,ncols,tindex);
    hold on;
    %plot all series on same axes
    %for sindex=1:nseries

        %ftiming.hp(pind)=plot(d,searchtime,getptype(pind,1));
        d=size(pdata(sindex).m,1);
        
        %pdata(sindex).hp(tindex)=plot([1:d],pdata(sindex).mse(:,tindex),getptype(sindex,2));
      
        data=zeros(d,nseries);
        for sindex=1:nseries
             data(:,sindex)=pdata(sindex).mse(:,tindex);
        end
      hb=bar([1:d],data);
      set(hb(1),'FaceColor','r');
      set(hb(2),'FaceColor','g');
      set(gca,'FontSize',fmseser.axisfontsize);
        %fmseser.hp(sindex)=pdata(sindex).hp(1);
        %fmseser.lbls{sindex}=pdata(sindex).lbl;
    %end
    %set(ftiming.hp(pind),'LineStyle','none');

    %plot as a reference the true parameter value
    %hptrue=plot([1:d],mparam.ktrue,getptype(nseries+1,2));
    %legend(fmseser.hp,fmseser.lbls);
    
    %turn off xlabels on all but the last graph
    if (tindex <ntrials)
          set(gca,'xtick',[]);  
    end
    %title(sprintf('Trial %d', pdata(1).trial(tindex)));
    %put the text at top of graph don't use title because this puts
    %unnecessary space between the suplots
    text(.5,1,sprintf('Trial %d', pdata(1).trial(tindex)));
    
    xlim([1 d]);
end
lblgraph(fmseser);
legend(hb,{pdata(:).lbl});
%attach the ylabel to middle subplot
subplot(fmseser.hsubs(ceil(ntrials/2)));
fmseser.hylabel=ylabel('$(\hat{\theta}_i\textrm{--}\theta_i)^2$');
set(fmseser.hylabel,'interpreter','Latex');

%add text for the ylabel
%we want to use latex as the interpreter so we need to use the text command
%[fmseser]=lblgraph(fmseser);
%move the ylabel to midway in figure
%spos=get(fmseser.hsubs(1),'Position');
%tpos=get(fmseser.hylabel,'Position');
%coordinates are measured relative to current axis
%tpos(2)=spos(4)*10;
%set(fmseser.hylabel,'Position',tpos);
