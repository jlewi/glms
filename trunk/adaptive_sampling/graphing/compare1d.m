%function compare1d(data,param)
%   data - structure containing different gaussians to plot
%        .plot1-each field is taken as 1d gaussian to plot
%             .y - yvalues
%             .x - xvalues
%        .plot2
%             .y
%             .x
%   param - optional parameters
%       .labels - labels for each field of data
%                this is a structure where each named field corresponds
%                to one of the fields of data.
%       .hf     -handle to the figure
%       .xlim   -limits of graph
function [param]=compare1d(data,param)

%pmass is the fraction of total probability of the array we want our xlimits to encompass
pmass=.97;

fnames=fieldnames(data);

if ~(exist('param','var'))
    param=[];
end
if ~(isfield(param,'hf'))
    param.hf=figure;
end

if ~(isfield(param,'labels'))
    for index=1:length(fnames)
        param.labels.(fnames{index})=fnames{index};
    end
end



if ~(isfield(param,'xlim'))
    param.xlim=[-1 1];    
end

figure(param.hf)

%matrix used to compute what the xlimits should be
xl=zeros(length(fnames),2);

hold on;
h_p=zeros(1,length(fnames));
lbls=cell(1,length(fnames));
for index=1:length(fnames)
    h_p(index)=plot(data.(fnames{index}).x,data.(fnames{index}).y,getptype(index,1));
    plot(data.(fnames{index}).x,data.(fnames{index}).y,getptype(index,2));
    lbls{index}=param.labels.(fnames{index});
    
    %compute the cumulative distribution for this density
    cdf=cumsum(data.(fnames{index}).y);
    
    %find the interval containing pmass of the density
    ind=find(cdf<=((1-pmass*cdf(end))/2));
    if isempty(ind)
        ind=1;
    end
    xl(index,1)=data.(fnames{index}).x(ind(end));
    
    ind=find(cdf>=(1/2+pmass*cdf(end)/2));
    if isempty(ind)
        ind=length(data.(fnames{index}).x);
    end
    xl(index,2)=data.(fnames{index}).x(ind(1));
end

%take as the xlim limits the smalles value of xl(:,1) and largest value of
%xl2
nxlim(1,1)=min(xl(:,1));
nxlim(1,2)=max(xl(:,2));

%always zoom out if needed
if (nxlim(2)>param.xlim(2))
    param.xlim(2)=nxlim(2);
end
if (nxlim(1)<param.xlim(1))
    param.xlim(1)=nxlim(1);
end

%zoom in when ever white space on either side of pmass
%is >25%
width=diff(param.xlim);
if (((param.xlim(2)-nxlim(2))/width)>.45)
    param.xlim(2)=nxlim(2);
end
if (((nxlim(1)-param.xlim(1))/width)>.45)
    param.xlim(1)=nxlim(1);
end


legend(h_p,lbls);
xlabel('\theta');
ylabel('Probability');
xlim(param.xlim);
