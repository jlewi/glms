%function [fmse,tset]=plotmse('fname',fname,'npoints',1000)
%   npoints - number of points to generate in the test set
%   tset - test data
%           .y - stimuli
%           .nspikes - observations
% Explanation: generates a test set using the true underlying model.
%   1. computes the log likelihood of the test using the estimated
%   parameters after each trial
function  [fll,tset]=plotlogll(varargin)
npoints=1000;
%override variables
for j=1:2:nargin
    switch varargin{j}
        case {'fname','filename','file','simfile'}
            fname=varargin{j+1};
      otherwise
            if exist(varargin{j},'var')
                eval(sprintf('%s=varargin{j+1};',varargin{j}));
            else
                fprintf('%s unrecognized parameter. \n',varargin{j});
            end
    end
end

fprintf('\n***************************\n Log Likelihood for file: %s \n***********************\n',fname);
 [pmax, simparammax, mparam, srmax]=loadsim('simfile',fname, 'simvar','simmax');
 [prand, simparamrand, mparam, srrand]=loadsim('simfile',fname, 'simvar','simrand');

 if (mparam.alength>0)
     error('cant handle spike history terms');
 end
 
%generate the test set
tset.y=zeros(mparam.klength,1);
tset.stimgen=RandStimBall('mmag',mparam.mmag,'klength',mparam.klength);
tset.observer=simparammax.observer;
tset.nspikes=zeros(1,npoints);
%generate the observations
for j=1:npoints
    tset.y(:,j)=choosestim(tset.stimgen,[]);
    tset.nspikes(1,j)=observe(tset.observer,tset.y(:,j));
end

fll.hf=figure;
fll.xlabel='Trial';
fll.ylabel='Log Likelihood';
fll.hp=[];
fll.lbls={};
fll.name='Log Likelihood';
hold on;

pmax.ll=zeros(1,simparammax.niter);
prand.ll=zeros(1,simparammax.niter);
%for each data set
for tind=1:simparammax.niter
    %for each filter
    mu=simparammax.glm.fglmmu(pmax.m(:,tind+1)'*tset.y);
    pmax.ll(tind)=sum(log(eps+simparammax.glm.pdf(tset.nspikes,mu)));
    
     mu=simparamrand.glm.fglmmu(prand.m(:,tind+1)'*tset.y);
    prand.ll(tind)=sum(log(eps+simparamrand.glm.pdf(tset.nspikes,mu)));
end

pind=1;
 fll.hp(1)=plot(1:simparammax.niter,pmax.ll,getptype(1));
 fll.lbls{pind}='Info. Max';
 
 pind=2;
 fll.hp(pind)=plot(1:simparamrand.niter,prand.ll,getptype(pind));
 fll.lbls{pind}='I.I.D';
 
 %true log likelihood
  mu=tset.observer.glm.fglmmu(mparam.ktrue'*tset.y);
truell=sum(log(tset.observer.glm.pdf(tset.nspikes,mu)));

pind=pind+1;
fll.hp(pind)=plot([1 simparammax.niter],[truell, truell],getptype(pind));
fll.lbls{pind}='True';

lblgraph(fll);

