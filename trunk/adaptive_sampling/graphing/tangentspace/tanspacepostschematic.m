%03-12-2008
%
%This script makes a schematic illustrating how we compute the posterior on
%the tangent space.

%dimensions for paper
%fschem=FigObj('name','TanSpace Posterior Schematic','width',5,'height',5);
fontsize=12;
position.bottom=.13;
position.gheight=.3412;

%dimensions for slide show
%************************************************************************
%values for slide show
fschem=FigObj('name','TanSpace Posterior Schematic','width',7,'height',5,'naxes',[2,3]);
fontsize=16;
%position of the bottom for the 2nd ro
position.bottom=.07;
position.gheight=.32;




%generate the x, y points
pts=[-1:.05:1];
[x,y]=meshgrid(pts,pts);


view=[14 18];

%properties for the graphs
gprop.zl=[0 .16]; %limits for the plots of the probability
%********************************************************************
%Top Row - True posterior
%***********************************************************************
%Plot in the first column is the true posterior but ignoring 
%the Manifold information. Use a quadratic function

tfullpost.x=x;
tfullpost.y=y;

%for the true posterior we want it to be some log-concave function
%so we set the log probability to a quadratic function
%we use a quadratic function
%because we want it to be some concave function
tfullpost.xcent=0;
tfullpost.ycent=0;
tfullpost.height=2; %The value of z of our quad function when x=xcent and y=ycent
tfullpost.rzero=1;  %The distance from the center at which we want the quad function 
                    %to have decayed to zero
                    
%compute the scaling constant based on rzero
tfullpost.s=(tfullpost.rzero^2/tfullpost.height)^.5;

%define a function for the probability
logpunnorm=@(x,y)(-1/tfullpost.s^2*((x-tfullpost.xcent).^2+(y-tfullpost.ycent).^2)+tfullpost.height);

%we need to normalize logp
normconst=sum(sum(exp(logpunnorm(tfullpost.x,tfullpost.y))));
logp=@(x,y)(logpunnorm(x,y)-log(normconst));
tfullpost.logp=logp(tfullpost.x,tfullpost.y);


rind=1;
cind=1;
fschem.a=setfocus(fschem.a,rind,cind);

%make a surface plot of the true posterior
tfullpost.h=surf(x,y,exp(tfullpost.logp));

%adjust properties of the surface
set(tfullpost.h,'EdgeColor','none');
set(fschem.a(rind,cind),'xticklabel',[]);
set(fschem.a(rind,cind),'yticklabel',[]);
set(fschem.a(rind,cind),'zticklabel',[]);

%turn on the grid
set(fschem.a(rind,cind),'xgrid','on');
set(fschem.a(rind,cind),'ygrid','on');
set(fschem.a(rind,cind),'zgrid','on');

%add labels
fschem.a(rind,cind)=xlabel(fschem.a(rind,cind),'$\theta_1$','interpreter','latex');
fschem.a(rind,cind)=ylabel(fschem.a(rind,cind),'$\theta_2$','interpreter','latex');
fschem.a(rind,cind)=title(fschem.a(rind,cind),'$p(\vec{\theta}|s_{1:t},r_{1:t})$','interpreter','latex');

%set the view point
set(gca,'view',view);


%***************************************************************
%Top Row: Delta function indicating the true manifold
%***************************************************************
%%
%compute the x,y pts which define the manifold. That is we compute y as a
%function of x
%let the manifold be a parabola
manifold.xman=pts;
manifold.yman=1-manifold.xman.^2;
manifold.zman=ones(1,length(pts));
rind=1;
cind=2;
fschem.a=setfocus(fschem.a,rind,cind);

%manifold.h=plot3(manifold.xman,manifold.yman,manifold.zman);
%plot the manifold as a patch
xpts=[manifold.xman manifold.xman(end:-1:1)]
ypts=[manifold.yman manifold.yman(end:-1:1)];
zpts=[manifold.zman zeros(1,length(pts))];

manifold.h=patch(xpts',ypts',zpts','b');

%adjust properties of the surface
set(fschem.a(rind,cind),'xticklabel',[]);
set(fschem.a(rind,cind),'yticklabel',[]);

%turn on the grid
set(fschem.a(rind,cind),'xgrid','on');
set(fschem.a(rind,cind),'ygrid','on');
set(fschem.a(rind,cind),'zgrid','on');

%title
fschem.a(rind,cind)=xlabel(fschem.a(rind,cind),'$\theta_1$','interpreter','latex');
fschem.a(rind,cind)=ylabel(fschem.a(rind,cind),'$\theta_2$','interpreter','latex');
fschem.a(rind,cind)=title(fschem.a(rind,cind),'$\delta(\theta\in\mathcal{M})$','interpreter','latex');

zlim([0 2]);
set(gca,'ztick',[0 1]);

%set(manifold.h,'FaceAlpha',0)
set(gca,'view',view);


%***************************************************************
%Top Row: True posterior on the manifold
%***************************************************************
%%
%the x,y pts corresponding to the manifold
tpost.xman=manifold.xman;
tpost.yman=manifold.yman;

%compute the probability on these pts
tpost.prob=exp(logp(tpost.xman,tpost.yman));

%make a 3-d plot 
rind=1;
cind=3;
fschem.a=setfocus(fschem.a,rind,cind);

%plot the probability
tpost.h=plot3(tpost.xman,tpost.yman,tpost.prob,'k');

%plot the manifold 
tpost.hman=plot(tpost.xman,tpost.yman);

%turn on the grid
set(fschem.a(rind,cind),'xgrid','on');
set(fschem.a(rind,cind),'ygrid','on');
set(fschem.a(rind,cind),'zgrid','on');


%adjust properties of the surface
set(fschem.a(rind,cind),'xticklabel',[]);
set(fschem.a(rind,cind),'yticklabel',[]);
set(fschem.a(rind,cind),'zticklabel',[]);

%title
fschem.a(rind,cind)=xlabel(fschem.a(rind,cind),'$\theta_1$','interpreter','latex');
fschem.a(rind,cind)=ylabel(fschem.a(rind,cind),'$\theta_2$','interpreter','latex');
fschem.a(rind,cind)=title(fschem.a(rind,cind),'$p(\vec{\theta}|s_{1:t},r_{1:t},\mathcal{M})$','interpreter','latex');


%add a text label for the manifold
xpos=0-.1;
ypos=1-.1;
zpos=0;
tpost.htxtman=text(xpos,ypos,zpos,'$\mathcal{M}$');
set(tpost.htxtman,'interpreter','latex');
set(gca,'view',view);


%********************************************************************
%2nd Row - Gaussian Approximation of posterior on full space 
%***********************************************************************
%Plot in the first column is the true posterior but ignoring 
%the Manifold information. Use a quadratic function

fullgauss.x=x;
fullgauss.y=y;

%compute the gaussian 
fullgauss.x=x;
fullgauss.y=y;

fullgauss.mu=[tfullpost.xcent ;tfullpost.ycent];
fullgauss.c=.25*eye(2,2);

fullgauss.prob=mvgauss([fullgauss.x(:)'; fullgauss.y(:)'],fullgauss.mu,fullgauss.c);
fullgauss.prob=reshape(fullgauss.prob,size(fullgauss.y,1),size(fullgauss.x,2))

rind=2;
cind=1;
fschem.a=setfocus(fschem.a,rind,cind);

%make a surface plot of the true posterior
fullgauss.h=surf(fullgauss.x,fullgauss.y,fullgauss.prob);

%adjust properties of the surface
set(fullgauss.h,'EdgeColor','none');
set(fschem.a(rind,cind),'xticklabel',[]);
set(fschem.a(rind,cind),'yticklabel',[]);
set(fschem.a(rind,cind),'zticklabel',[]);

%turn on the grid
set(fschem.a(rind,cind),'xgrid','on');
set(fschem.a(rind,cind),'ygrid','on');
set(fschem.a(rind,cind),'zgrid','on');



%add labels
fschem.a(rind,cind)=xlabel(fschem.a(rind,cind),'$\theta_1$','interpreter','latex');
fschem.a(rind,cind)=ylabel(fschem.a(rind,cind),'$\theta_2$','interpreter','latex');
fschem.a(rind,cind)=title(fschem.a(rind,cind),'$p(\vec{\theta}|\mu_t,C_t)$','interpreter','latex');

%set the view point
set(gca,'view',view);


%***************************************************************
%Bottom row middle: Delta function on the tangent space
%***************************************************************
%%
%we need to compute the tangent space
%1. start by defining the projection of the mean onto the manifold
tanspace.muproj=[0;1];

%2. define the x,y pts corresponding to the tangent space
%   for the quadratic manifold, these pts are just a flat line
tanspace.xtan=pts;
tanspace.ytan=ones(1,length(pts));

rind=2;
cind=2;
fschem.a=setfocus(fschem.a,rind,cind);
hold on;

%plot a surface representing a delta function on the tangent space
%manifold.h=plot3(manifold.xman,manifold.yman,manifold.zman);
%plot the manifold as a patch
xpts=[tanspace.xtan tanspace.xtan(end:-1:1)]
ypts=[tanspace.ytan tanspace.ytan(end:-1:1)];
zpts=[ones(1,length(pts)) zeros(1,length(pts))];

tanspace.hdelta=patch(xpts',ypts',zpts','b');

%plot the tangent space
tanspace.htan=plot(tanspace.xtan,tanspace.ytan,'r');

%plot the manifold
tanspace.hman=plot(manifold.xman,manifold.yman);


%adjust properties of the surface
set(fschem.a(rind,cind),'xticklabel',[]);
set(fschem.a(rind,cind),'yticklabel',[]);

%turn on the grid
set(fschem.a(rind,cind),'xgrid','on');
set(fschem.a(rind,cind),'ygrid','on');
set(fschem.a(rind,cind),'zgrid','on');

%title
fschem.a(rind,cind)=xlabel(fschem.a(rind,cind),'$\theta_1$','interpreter','latex');
fschem.a(rind,cind)=ylabel(fschem.a(rind,cind),'$\theta_2$','interpreter','latex');
fschem.a(rind,cind)=title(fschem.a(rind,cind),'$\delta(\theta\in\mathcal{T}_{\mu_M}\mathcal{M})$','interpreter','latex');

zlim([0 2]);
set(gca,'ztick',[0 1]);

%add labels
%plot the location of the full mean
tanspace.hmu=plot(fullgauss.mu(1),fullgauss.mu(2),'o','markerfacecolor','b');
tanspace.hmutxt=text(fullgauss.mu(1),fullgauss.mu(2)+.15,'$\mu_t$');
set(tanspace.hmutxt,'interpreter','latex');

%plot the location of the mean projected on the manifold
tanspace.hmuproj=plot(tanspace.muproj(1),tanspace.muproj(2)-.15,'o','markerfacecolor','b');
tanspace.hmuprojtxt=text(tanspace.muproj(1),tanspace.muproj(2)+.15,'$\mu_{M}$');
set(tanspace.hmuprojtxt,'interpreter','latex');

%draw an arrow between \mu_t and \mu_M
%draw an arrow
tanspace.harrow=arrow([fullgauss.mu;0],[tanspace.muproj;0],'LineStyle',':');
set(gca,'view',view);

%label the tangent space
tanspace.htantxt=text(.85,.85,'$T_{\mu_M} \mathcal{M}$');
set(tanspace.htantxt,'interpreter','latex');



%***************************************************************
%Bottom row final: gaussian on the tangent space
%***************************************************************


%points on the manifold
tangauss.x=tanspace.xtan;
tangauss.y=tanspace.ytan;
tangauss.B=[1;0];  %basis vector of the tangent space
tangauss.bproj=tangauss.B'*[tangauss.x;tangauss.y]; %projection of x,y onto the basis vector of the tangent space

%compute the gaussian 
tangauss.mu=0;
tangauss.c=.25;

tangauss.prob=mvgauss(tangauss.bproj,tangauss.mu,tangauss.c);


rind=2;
cind=3;
fschem.a=setfocus(fschem.a,rind,cind);
hold on;

%make a plot of the Gaussian posterior 
tangauss.hprob=plot3(tangauss.x,tangauss.y,tangauss.prob);

%plot the manifold
tangauss.hman=plot(manifold.xman,manifold.yman);


%draw an arrow and label it with the covariance
%we place the line at a value of sigma along the gaussian
bval=(tangauss.c*2);
%comput the x,y pts for the arrow by multiplying by the basis vector
apts=tangauss.B*[-(bval-.05) (bval-.05)]+tanspace.muproj*ones(1,2);
tangauss.hacb=arrow([apts(:,1);mvgauss(bval,tangauss.mu,tangauss.c)],[apts(:,2);mvgauss(bval,tangauss.mu,tangauss.c)],'LineStyle',':','length',5,'ends','both');
arrow fixlimits;

%tick labels
set(fschem.a(rind,cind),'xticklabel',[]);
set(fschem.a(rind,cind),'yticklabel',[]);
set(fschem.a(rind,cind),'zticklabel',[]);

%turn on the grid
set(fschem.a(rind,cind),'xgrid','on');
set(fschem.a(rind,cind),'ygrid','on');
set(fschem.a(rind,cind),'zgrid','on');



%add labels
fschem.a(rind,cind)=xlabel(fschem.a(rind,cind),'$\theta_1$','interpreter','latex');
fschem.a(rind,cind)=ylabel(fschem.a(rind,cind),'$\theta_2$','interpreter','latex');
fschem.a(rind,cind)=title(fschem.a(rind,cind),'$p(\vec{\theta}|\mu_b,C_b)$','interpreter','latex');


%add text label for cb and mub
tangauss.hcbtxt=text(mean(apts(1,:)),mean(apts(2,:)),mvgauss(bval,tangauss.mu,tangauss.c),'C_b');

%set the view point
set(gca,'view',view);

%**************************************************************************
%Position the axes
%*************************************************************************
%shift the bottom row down
for cind=1:3
   pos=get(getha(fschem.a(2,cind)),'position');
   pos(2)=position.bottom;
   set(getha(fschem.a(2,cind)),'position',pos);
end

%adjust the heights
for rind=1:2
    for cind=1:3
        
   pos=get(getha(fschem.a(rind,cind)),'position');
   pos(4)=position.gheight;
   set(getha(fschem.a(rind,cind)),'position',pos);
    end
end
%**************************************************************************
%Add text labels
%**************************************************************************
%we want to add text labels in between the plots
%to do this we create a blank axes whose inner position takes up the whole
%figure and whos limits are 0-1
tlbls.hatxt=axes('position',[0 0 1 1],'xlim',[0 1],'ylim',[0 1]);
axis off;


%add multiply signs in between columns 1 and 2
%determine the position for the first multiply sign
pos11=get(getha(fschem.a(1,1)),'position'); %position of axis in row 1, col1
pos12=get(getha(fschem.a(1,2)),'position'); %position of axis in row 1, col1
tlbls.mult1.x=(pos11(1)+pos11(3)+pos12(1))/2;
tlbls.mult1.y=pos11(2)+pos11(4)+.025;
tlbls.mult1.h=text(tlbls.mult1.x,tlbls.mult1.y,0,'x');


%multiply sign for second row
pos21=get(getha(fschem.a(2,1)),'position'); %position of axis in row 2, col1
tlbls.mult2.x=tlbls.mult1.x;
tlbls.mult2.y=pos21(2)+pos21(4)+.025;
tlbls.mult2.h=text(tlbls.mult2.x,tlbls.mult2.y,0,'x');

%*****************************************
%add = sign in between 2 and 3rd column
%*******************************************
pos12=get(getha(fschem.a(1,2)),'position'); 
pos13=get(getha(fschem.a(1,3)),'position'); 
tlbls.eq1.x=(pos12(1)+pos12(3)+pos13(1))/2;
tlbls.eq1.y=pos12(2)+pos12(4)+.025;
tlbls.eq1.h=text(tlbls.eq1.x,tlbls.eq1.y,0,'=');

pos22=get(getha(fschem.a(2,2)),'position'); 
tlbls.eq2.x=tlbls.eq1.x;
tlbls.eq2.y=pos22(2)+pos22(4)+.025;
tlbls.eq2.h=text(tlbls.eq2.x,tlbls.eq2.y,0,'=');


%***************************************************
%add approximation signs in between row 1 and row 2
%%
%to get the xposition we need the position of the title
title21=gettitle(fschem.a(2,1));
%position of tpos is in limits of axes so we need to convert
%to axes units
xl=get(getha(fschem.a(2,1)),'xlim');
yl=get(getha(fschem.a(2,1)),'ylim');

tpos21=get(title21.h,'position');
tpos21(1)=pos21(1)+tpos21(1)/diff(xl)*pos21(3);



opos11=get(getha(fschem.a(1,1)),'outerposition');
opos21=get(getha(fschem.a(2,1)),'outerposition');

%tlbls.approx1.x=(pos11(1)+pos11(3))/2;
%Hack: The extent property for the title doesn't return 
% a propert bounding box most likely because we are using latex
%so we manually adjust the position
tlbls.approx1.x=(tpos21(1))+.07;
tlbls.approx1.y=(opos21(2)+opos21(4)+opos11(2))/2;
tlbls.approx1.h=text(tlbls.approx1.x,tlbls.approx1.y,0,'\approx');
%set(tlbls.approx1.h,'interpreter','latex');
set(tlbls.approx1.h,'rotation',90);

%***************************************************
%approximation sign: col 2
%****************************************************
%%
%to get the xposition we need the position of the title
title22=gettitle(fschem.a(2,2));
%position of tpos is in limits of axes so we need to convert
%to axes units
xl=get(getha(fschem.a(2,2)),'xlim');
yl=get(getha(fschem.a(2,2)),'ylim');

tpos22=get(title22.h,'position');
tpos22(1)=pos22(1)+tpos22(1)/diff(xl)*pos22(3);



opos12=get(getha(fschem.a(1,2)),'outerposition');
opos22=get(getha(fschem.a(2,2)),'outerposition');

%tlbls.approx1.x=(pos11(1)+pos11(3))/2;
%Hack: The extent property for the title doesn't return 
% a propert bounding box most likely because we are using latex
%so we manually adjust the position
tlbls.approx2.x=(tpos22(1))+.07;
tlbls.approx2.y=(opos22(2)+opos22(4)+opos12(2))/2;
tlbls.approx2.h=text(tlbls.approx2.x,tlbls.approx2.y,0,'\approx');
%set(tlbls.approx1.h,'interpreter','latex');
set(tlbls.approx2.h,'rotation',90);

%***************************************************
%approximation sign: col 3
%****************************************************
%%
%to get the xposition we need the position of the title
title23=gettitle(fschem.a(2,3));
%position of tpos is in limits of axes so we need to convert
%to axes units
xl=get(getha(fschem.a(2,3)),'xlim');
yl=get(getha(fschem.a(2,3)),'ylim');


pos23=get(getha(fschem.a(2,3)),'position'); 
tpos23=get(title23.h,'position');
tpos23(1)=pos23(1)+tpos23(1)/diff(xl)*pos23(3);



opos13=get(getha(fschem.a(1,3)),'outerposition');
opos23=get(getha(fschem.a(2,3)),'outerposition');

%tlbls.approx1.x=(pos11(1)+pos11(3))/2;
%Hack: The extent property for the title doesn't return 
% a propert bounding box most likely because we are using latex
%so we manually adjust the position
tlbls.approx3.x=(tpos23(1))+.07;
tlbls.approx3.y=(opos23(2)+opos23(4)+opos13(2))/2;
tlbls.approx3.h=text(tlbls.approx3.x,tlbls.approx3.y,0,'\approx');
%set(tlbls.approx1.h,'interpreter','latex');
set(tlbls.approx3.h,'rotation',90);

alltext=findall(gethf(fschem),'type','text');
set(alltext,'FontSize',fontsize);

%saveas(gethf(fschem),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/tanpostschematic.eps','epsc2');
%saveas(gethf(fschem),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/tanpostschematic.png');