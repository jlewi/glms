%make a picture of the update schematic
%we will need to create a bunch of axes. 
%   bh - this will be a set of axes equal to the size of the image
%       this will allow us to place text objects anywhere in the image
%
%
% make the update schematics with out any labels. Use skencil to add the
% labels to the eps image
%   11-05-07 - changed the labels. get rid of panel showing gaussian approx
%   for batch. 
%           - changes for new object model
clear all;
setpathvars
gspath='~/svn_trunk/research/figures/scripts';
addpath(gspath);


%graph parameters
gparam.width=5.75;       %inches
gparam.height=1;        %inches
gparam.nclines=20;      %number of null clines for the graph

msize=6;    %size for dots indicating the means
cprior=[.7 .7 .7]; %color for mean of prior
cpost =[0 0 0];
cedge=[0 0 0];  %color for the edge
%**************************************************************************
%image data
%create an update object to compute the Gaussian approximation
opts=optimset('TolFun',10^-10,'TolX',10^-10);
uobj=Newton1d('optim',opts);
glm=GLMModel('poisson','canon');
prior=GaussPost('m',[0;0],'c',2*eye(2));

mobj=MParamObj('klength',2,'alength',0,'pinit',prior,'mmag',1,'glm',glm);


stim=[1 ;1];
obsrv=exp(stim'*[.4;.4]);
[post]=update(uobj,prior,stim,mobj,obsrv);

mintheta=2.5;
thetax=[-mintheta:.05:mintheta];
thetay=[-mintheta:.05:mintheta];

[f data.loglike]=ll1d('nspikes',obsrv,'stim',stim','thetax',thetax,'thetay',thetay,'lpthreshold',-20);
set(f.hf,'name','Likelihood');
[f data.logprior]=dgaussian('mu',getm(prior),'sigma',getc(prior),'x',thetax,'y',thetay);
set(f.hf,'name','Prior');
[fpost data.logpost data.lpostx data.lposty data.means]=logpostcontour('lprior',data.logprior,'zll',data.loglike,'thetax',thetax,'thetay',thetay,'lpthreshold',-10);
set(fpost.hf,'name','Posterior');
[fpgauss data.logpgauss]=dgaussian('mu',getm(post),'sigma',getc(post),'lpthreshold',-5,'x',thetax,'y',thetay); %gaussian approximation of the posterior
set(fpgauss.hf,'name','Posterior Gaussian');
%zero out the corners
threshval=max(data.logprior(:,1));
ind=find(data.logprior<threshval);
data.logprior(ind)=nan;

threshval=max(data.logpgauss(:,1));
ind=find(data.logpgauss<threshval);
data.logpgauss(ind)=nan;

%*********************************************************
%Graphs
%%
fupdate.hf=figure;
fupdate.fontsize=12;
fupdate.name='batch';

fbottom.hf=figure;
fbottom.fontsize=12;
fbottom.name='online';

setfsize(fupdate.hf,gparam);
setfsize(fbottom.hf,gparam);
set(fbottom.hf,'name','online');
set(fupdate.hf,'name','batch');


fh=[fupdate.hf, fbottom.hf];
for h=fh
figure(h);
%change the color map so the peak of the gaussian doesn't completely
%blend in with the background
%do this by removing white from the colormap
%uncomment for black and white
%cmap=colormap(gray);
%cmap=cmap(1:end-5,:);
%colormap(cmap);

%colormap('default');
end
%**********************************************************************
%toprow has 5 images
%   1. log prior
%   2. log likelihood
%   3. log likelihood
%   4. log posterior
%   5. gaussian approximation of log posterior
%
%indexes for the axes

tr.lprior=1;
tr.ll=2;
tr.llt=3;
tr.lpost=4;
tr.lpgauss=5;

br.lprior=1;
br.ll=2;
br.lpost=3;
br.lpgauss=4;

figure(fupdate.hf);
%1.log prior
fupdate.a{tr.lprior}.ha=axes;
[c fupdate.a{tr.lprior}.hc]=contourf(data.logprior,gparam.nclines);
fupdate.a{tr.lprior}.title='$\log p(\vec{\theta})$';
fupdate.a{tr.lprior}.ylabel='batch:';

%2.log likelihod for all but most recent terms
fupdate.a{tr.ll}.ha=axes;
[c fupdate.a{tr.ll}.hc]=contourf(data.loglike,gparam.nclines);
fupdate.a{tr.ll}.title='$\log p(r_i|\vec{x}_i,\vec{\theta})$';

%3. log likelihood for most recent term
fupdate.a{tr.llt}.ha=axes;
[c fupdate.a{tr.llt}.hc]=contourf(data.loglike,gparam.nclines);
fupdate.a{tr.llt}.title='$\log p(r_t|\vec{x}_t,\vec{\theta})$';

%4. log posterior
fupdate.a{tr.lpost}.ha=axes;
[c fupdate.a{tr.lpost}.hc]=contourf(data.lpostx,data.lposty,data.logpost,gparam.nclines);
fupdate.a{tr.lpost}.title='$\log p(\vec{\theta}|\vec{\underline{x}}_t,\vec{\underline{r}}_t)$';

%add dots to show the means
hold on

mprior=getm(prior);
mpost=getm(post);
hprior=plot([mprior(1)],[mprior(2)],'ko');
hpost=plot([mpost(1)],[mpost(2)],'ko');
set(hprior,'MarkerFaceColor',cprior);
set(hprior,'MarkerEdgeColor',cedge);
set(hpost,'MarkerEdgeColor',cpost);
set(hpost,'MarkerFaceColor',cpost);
%set(hpost,'MarkerFaceColor','k');
set([hpost hprior],'MarkerSize',msize);


%5. Gaussian approximation of the posterior
fupdate.a{tr.lpgauss}.ha=axes;
[c fupdate.a{tr.lpgauss}.hc]=contourf(data.logpgauss,gparam.nclines);
fupdate.a{tr.lpgauss}.title='$\log p(\vec{\theta}|\vec{\mu}_t,C_t)$';

for j=1:5
    axes(fupdate.a{j}.ha)
    axis square;
set(fupdate.a{j}.ha,'xtick',[]);
set(fupdate.a{j}.ha,'ytick',[]);

%get rid of the edge colors
set(fupdate.a{j}.hc,'linecolor','none');
end


%**************************************************************************
%bottom row
%**************************************************************************
%1. Gaussian approximation of the posterior
figure(fbottom.hf);
fbottom.a{br.lprior}.ha=axes;
[c fbottom.a{br.lprior}.hc]=contourf(data.logprior,gparam.nclines);
fbottom.a{br.lprior}.title='$\log p(\vec{\theta}|\vec{\mu}_{t\frac{\,\,}{\,\,}1},C_{t\frac{\,\,}{\,\,}1})$'; 
fbottom.a{br.lprior}.ylabel='online:';

%2. log likelihood for most recent term
fbottom.a{br.ll}.ha=axes;
[c fbottom.a{br.ll}.hc]=contourf(data.loglike,gparam.nclines);
fbottom.a{br.ll}.title='$\log p(r_t|\vec{x}_t,\vec{\theta})$';

%3. log posterior
fbottom.a{br.lpost}.ha=axes;
[c fbottom.a{br.lpost}.hc]=contourf(thetax,thetay,data.logpost,gparam.nclines);
fbottom.a{br.lpost}.title='$\log p(\vec{\theta}|\vec{\underline{x}}_t,\vec{\underline{r}}_t)$';
%add dots to show the means
hold on
hprior=plot([mprior(1)],[mprior(2)],'ko');
hpost=plot([mpost(1)],[mpost(2)],'ko');
set(hprior,'MarkerFaceColor',cprior);
%make edge black
set(hprior,'MarkerEdgeColor',[0 0 0]);
set(hpost,'MarkerEdgeColor',cpost);
set(hpost,'MarkerFaceColor',cpost);
set([hprior hpost],'MarkerSize',msize);

%4. Gaussian approximation of the posterior
fbottom.a{br.lpgauss}.ha=axes;
[c fbottom.a{br.lpgauss}.hc]=contourf(data.logpgauss,gparam.nclines);
fbottom.a{br.lpgauss}.title='$\log p(\vec{\theta}|\vec{\mu}_t,C_t)$';

%turn off tick marks
for j=1:4
    axes(fbottom.a{j}.ha)
    axis square;
set(fbottom.a{j}.ha,'xtick',[]);
set(fbottom.a{j}.ha,'ytick',[]);

%get rid of the edge colors
set(fbottom.a{j}.hc,'linecolor','none');
end



%determine the width for the graphs
%relative widths
twidth=.05; %how much space to leave for text in between figures
ssize.width=(1-twidth*5)/5;


vpad=0;   %padding in order to make sure figures not cut off
%ssize.height=(1-vpad-ssize.vspace-max(trtinset(:,2))-max(trtinset(:,4))-max(brtinset(:,2))-max(brtinset(:,4)))/2
ssize.height=1;
brow.bottom=0;
trow.bottom=0;
%**************************************
%set the position of the top row
for j=1:5    
    pos=get(fupdate.a{j}.ha,'position');
    pos(1)=(j-1)*(ssize.width+twidth);
    pos(2)=trow.bottom;
    pos(3)=ssize.width;
    pos(4)=ssize.height;
    set(fupdate.a{j}.ha,'position',pos);
    fupdate.a{j}.pos=pos;
end



%*************************************************
%position elements in the bottom row
%1. space the image in the center of the two above it
left=(fupdate.a{1}.pos(1)+fupdate.a{1}.pos(3)+fupdate.a{2}.pos(1))/2-ssize.width/2;
pos=get(fbottom.a{br.lprior}.ha,'position');
pos(1)=left;
pos(2)=brow.bottom;
pos(3)=ssize.width;
pos(4)=ssize.height;
set(fbottom.a{br.lprior}.ha,'position',pos);
fupdate.a{br.lprior}.pos=pos;

%2. log likelihood
pos=get(fbottom.a{br.ll}.ha,'position');
pos(1)=fupdate.a{tr.llt}.pos(1);
pos(2)=brow.bottom;
pos(3)=ssize.width;
pos(4)=ssize.height;
set(fbottom.a{br.ll}.ha,'position',pos);
fbottom.a{br.ll}.pos=pos;

%3. lpost
pos=get(fbottom.a{br.lpost}.ha,'position');
pos(1)=fupdate.a{tr.lpost}.pos(1);
pos(2)=brow.bottom;
pos(3)=ssize.width;
pos(4)=ssize.height;
set(fbottom.a{br.lpost}.ha,'position',pos);
fbottom.a{br.lpost}.pos=pos;
%add dots to show the means


%4. lpgauss
pos=get(fbottom.a{br.lpgauss}.ha,'position');
pos(1)=fupdate.a{tr.lpgauss}.pos(1);
pos(2)=brow.bottom;
pos(3)=ssize.width;
pos(4)=ssize.height;
set(fbottom.a{br.lpgauss}.ha,'position',pos);
fbottom.a{br.lpgauss}.pos=pos;



pos=get(fbottom.hf,'position');
pos(3)=gparam.width;
pos(4)=gparam.height;
set(fbottom.hf,'position',pos);

%***********************************************************************
%turn off the panel showing the Gaussian approximation of the batch mode"
%************************************************************
delete(fupdate.a{tr.lpgauss}.ha);



%saveas(fupdate.hf,'~/svn_trunk/research/figures/adaptive_sampling/update_batch_nolbls.eps','epsc2');
%saveas(fbottom.hf,'~/svn_trunk/research/figures/adaptive_sampling/update_online_nolbls.eps','epsc2');
%e_nolbls_bw.eps','epsc2');
%previewfig(fupdate.hf,'bounds','tight','FontMode','Fixed','FontSize',fupdate.fontsize,'Width',gparam.width,'Height',gparam.height);
%saveas(fupdate.hf,'~/svn_trunk/adaptive_sampling/writeup/NC06/update_true_nolbls_bw.eps','epsc2');
%saveas(fbottom.hf,'~/svn_trunk/adaptive_sampling/writeup/NC06/update_online_nolbls_bw.eps','epsc2');
%exportfig(fupdate.hf,'~/cvs_ece/writeup/NC06/update_schematics.eps','bounds','tight','FontMode','Fixed','FontSize',fupdate.fontsize,'Width',gparam.width,'Height',gparam.height);