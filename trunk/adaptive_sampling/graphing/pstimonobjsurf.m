%1-12-07
%1. make a plot of the expected fisher information as a function of (mu,
%sigma)
%2. overlay over this dots indicating the locations of the actual stimuli
%  on successive trials

%**************************************************************
%values to use to compute objective surface
%**************************************************************
muglm=[-2.25:.25:2.25];
sigmasq=[1:.25:5];
trials=[1:25] ;                                 %which trials to plot
offset=.1;
%**************************************************************************
%construct the data sets
%each data set is a structure with fields
%   .m
%   .c
%   .trial
%   .stim
%  .label - label for the data set
%  .h     - this will store handle to each point
%**************************************************************************
infiles{1}=fullfile(RESULTSDIR,'numint','01_10','01_10_numint_002.mat');
load(infiles{1});
data=[];
handles=[];

sr=srmax.newton1d;
glm=simparam.glm;
data(1).m=pmax.newton1d.m(:,trials);
for j=1:length(trials)
    if ~isempty(pmax.newton1d.c{trials(j)})
        data(1).c{j}=pmax.newton1d.c{trials(j)};
    else
    prior=mparam.pinit;
    [post,niter,ntime,a]=postnewton1d(prior,sr.y(:,1:trials(j)),sr.nspikes,glm);
    data(1).c{j}=post.c;
    end
end
data(1).stim=sr.y(:,trials);
data(1).label='Info. max';
data(1).trial=trials;
data(1).h=zeros(1,length(trials));
data(1).htxt=zeros(1,length(trials));
data(1).ptype='k.';
clear('sr','srmax','pmax');

dind=2;
sr=srrand.newton1d;
p=prand.newton1d;
data(dind).m=p.m(:,trials);
for j=1:length(trials)
    if ~isempty(p.c{trials(j)})
        data(dind).c{j}=p.c{trials(j)};
    else
    prior=mparam.pinit;
    [post,niter,ntime,a]=postnewton1d(prior,sr.y(:,1:trials(j)),sr.nspikes,glm);
    data(dind).c{j}=post.c;
    end
end
data(dind).stim=sr.y(:,trials);
data(dind).label='rand';
data(dind).trial=trials;
data(dind).h=zeros(1,length(trials));
data(dind).htxt=zeros(1,length(trials));
data(dind).ptype='k*';
%*****************************************************
%PLOTS
%******************************************************
%plot the objective surface
[fobj]=objsurf('muglm',muglm,'sigmasq',sigmasq);
figure(fobj.hf);
for dind=1:length(data)
    for pind=11:length(trials)
        hold on;
        muproj=data(dind).stim(:,pind)'*data(dind).m(:,pind);
        sigma=data(dind).stim(:,pind)'*data(dind).c{pind}*data(dind).stim(:,pind);
        data(dind).muproj(pind)=muproj;
        data(dind).sigma(pind)=sigma;
        %only plot it if its within the range
        if ~(muproj<muglm(1) || muproj>muglm(end) || sigma < sigmasq(1) || sigma >sigmasq(end))
        data(dind).h(pind)=plot(muproj,sigma,data(dind).ptype,'markersize',15);
        data(dind).htxt(pind)=text(muproj,sigma+offset,sprintf('%d',data(dind).trial(pind)),'fontsize',15);
        
        data(dind).hlgnd=data(dind).h(pind);
        end
    end
end
%create a list of any handles so we can delete them
handles=[handles data(:).h data(:).htxt];
legend([data(1).hlgnd,data(2).hlgnd],{data(1).label, data(2).label});

%delete any handles
handles=handles(find(handles~=0));
delete(handles);
handles=[];