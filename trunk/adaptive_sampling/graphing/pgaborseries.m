%FOr the case where the true k represents a gabor posterior or some other
%2d shape. We can make an image of the mean after iterations to see how
%good it looks.


%create a structure which contains the relevant fields for different data
%sets we then select which element of the structure we want to select the
%data. if we want to use data in the current workspace set dind=0;
dind=5;

data=[];
%tracking results for nips
%these results are for diffusion
data(end+1).infile=fullfile('results', 'tracking','06_01','06_01_maxtrack_001.mat');
data(end).outdir=fullfile('writeup','nips');
data(end).trials=[0 10 100 500 1000 2000 3000];
data(end).fsuffix='';


%tracking results for nips
%these results are for diffusion
data(end+1).infile=fullfile('results', 'tracking','06_02','06_02_gabor_001.mat');
data(end).outdir=fullfile('writeup','nips');
data(end).trials=[0 10 100 500 1000 5000 10000];
data(end).fsuffix='';

%tracking results for nips
%these results are for diffusion
data(end+1).infile=fullfile('results', 'tracking','06_01','06_01_gabortrack_001.mat');
data(end).outdir=fullfile('results','tracking','06_01');
data(end).trials=[0 100:100:500];
data(end).fsuffix=sprintf('_06_01_001.eps');

%tracking results for nips
%these results are for diffusion
data(end+1).infile=fullfile('results', 'tracking','06_03','06_03_gabor_001.mat');
data(end).outdir=fullfile('results','tracking','06_03');
data(end).trials=[0 10 100 1000 5000];
data(end).fsuffix=sprintf('_06_03_001.eps');


%tracking results for nips
%these results are for diffusion
data(end+1).infile=fullfile('results', 'tracking','06_03','06_03_gabor_001.mat');
data(end).outdir=fullfile('writeup','nips');
data(end).trials=[0 100 500 2500 5000];
data(end).fsuffix=sprintf('');

if dind==0;
    fprintf('\n\n Plotting Data in Work Space \n\n');
    %get directory of current file
    if ~isempty(ropts.fname)
        [outdir,fname] =fileparts(ropts.fname);
        if isfield(ropts,'trialindex')
            trialindex=ropts.trialindex;
        else
            trialindex=str2num(fname(end-2:end));
        end
        trialdate=(fname(1:5));
    else
        outdir=RESULTSDIR;
    end
    %which trials to plot it on.
    %trial of 0 corresponds to prior
    %trials=[200:400:1200 1500];
    trials=[0 100 500:500:3000];
    %trials=[10:20:100];
    trials=[100:200:500];
    %trials=[100 250 300:100:400];
    %trials=[0:20:60];

    ntrials=length(trials);
    %color scaling for the images
    clim=[min(mparam.ktrue(:)) max(mparam.ktrue(:))];
    fsuffix=sprintf('_%s_%.3i',trialdate,trialindex);
else
    load(data(dind).infile);
    fprintf('\n\n Plotting Data in file: \n %s \n\n',data(dind).infile);
    trials=data(dind).trials;
    ntrials=length(trials);
    fsuffix=data(dind).fsuffix;
    outdir=data(dind).outdir;
    clim=[min(mparam.ktrue(:)) max(mparam.ktrue(:))];
end
%clim=[0 .5];


%if the number of trials exceeds the number of iterations then truncate it
ind=find((trials+1)>(simparam.niter+1));
if ~isempty(ind)
    trials=trials(1:ind(1)-1);
    ntrials=length(trials);
end

%methods structure is an array of structures
% each element describes a structure in which results are stored
% .rvar - name of the structure storing the results for the simulation
% .simvar-  name of variable containing the simulation parameters
% i.e methods(1).pname='pmax'
%     means workspace should contain a structure called pmax
%
methods=[];
methods(1).rvar='pmax';
methods(1).simvar='simparammax';
methods(1).srvar='srmax';

methods(1).label='info. max.';

methods(2).rvar='prand';
methods(2).simvar='simparamrand';
methods(2).srvar='srrand';
methods(2).label='random';


%construct a list of enabled methods
monf={};
mnames=fieldnames(pmax);
for index=1:length(fieldnames(pmax))
    if pmax.(mnames{index}).on >0
        monf{length(monf)+1}=mnames{index};
    end
end

%************************************************************************
%image parameters
%**************************************************************************
if isfield(mparam,'gwidth')
    gwidth=mparam.gwidth;
    gheight=mparam.gheight;
else
    gwidth=mparam.klength^.5;
    gheight=mparam.klength^.5;
end

%**************************************************************************
%obtain the data
%**************************************************************************
%we collect the data into an array of structures.
%each element of the array contains the data for the posterior on multiple
%trials it has following fields
%   .trials - a list of trials specifying the trial on which following
%               was collected
%   .m      - each col is mean after corresponding trial in trials
%   .var  - i,j th element is the varance of element i of theta on trial j
%   .lbl     -lbl for this trace
%   .ktrue - true parameters
%   .hp     - handle to plots
%number of data series = elements of pdata
nseries=0;

for index=1:length(methods)
    %extract the results parameters and simulation parameters
    eval(sprintf('presults=%s;',methods(index).rvar));
    eval(sprintf('sparam=%s;',methods(index).simvar));
    eval(sprintf('sr=%s;',methods(index).srvar));
    nseries=nseries+1;
    pdata(nseries).lbl=methods(index).label;

    pdata(nseries).m=zeros(size(presults.newton1d.m,1),length(trials));
    pdata(nseries).var=zeros(size(presults.newton1d.m,1),length(trials));

    pdata(nseries).mmat=cell(1,ntrials)
    for tindex=1:length(trials);

        %add 1 b\c a trial of 0 is prior
        %trial is the index into the array presults
        trial=trials(tindex)+1;

        pdata(nseries).trial(tindex)=trials(tindex);
        %add 1 so that trial of 0 corresponds to prior
        pdata(nseries).m(:,tindex)=presults.newton1d.m(:,trial);
        if ~isempty(presults.newton1d.c{trial})
            pdata(nseries).var(:,tindex)=diag(presults.newton1d.c{trial});
        end
        pdata(nseries).hp=zeros(1,ntrials);

        %reshape the matrix
        pdata(nseries).mmat{tindex}=reshape(pdata(nseries).m(:,tindex), [gheight gwidth]);

        %we subtract 1 from the trial before getting the stimulus
        %set the clim information
        if (trial-1)==0
            %there is no stimulus for the prior so just set it to zeros
            pdata(nseries).stim{tindex}=zeros(gheight,gwidth);
            pdata(nseries).sclimmin=[0];
            pdata(nseries).sclimmax=[0];
        else
            pdata(nseries).stim{tindex}=reshape(sr.newton1d.y(:,trial-1), [gheight gwidth]);
            pdata(nseries).sclimmin=[min(sr.newton1d.y(:,trial-1))];
            pdata(nseries).sclimmax=[max(sr.newton1d.y(:,trial-1))];
        end
    end
    %compute the squared error of each term. So if we were to sum across
    %rows and take the square root we would get the MSE.
    %   pdata(nseries).mse=(pdata(nseries).m-mparam.ktrue *ones(1,length(trials))).^2;
end

%**************************************************************************
%Make plots of mean
%************************************************************************
%ArrangeMents of plots
%if ktrue is changing plot them in rows
ntrials=length(trials);
if (size(mparam.ktrue,2)>1)
    nrows=ntrials;
    ncols=1;
    kconst=0;
else
    kconst=1;
    %determine how man rows and columns to have
    %we add 1 because we want to plot the true gabor as well
    nplots=ntrials+1;
    nrows=ceil(nplots^.5);
    while (ceil(nplots/nrows)~=(nplots/nrows))
        nrows=nrows+1;
    end
    ncolsnoser=nplots/nrows;
end

%nrows=ntrials;
%ncols=1;

%we multiply ncols by (nseries+1).
%because for each trial we will display side by side the image of random
%and max methods
%we then leave a blank axis to provide spacing
%if ktrue is not constant we will also plot it
%if (kconst~=0)
%    ncols=ncolsnoser*(nseries);
%else
%    ncols=ncolsnoser*(nseries+1);
%end

%layout figures like in Paninksi 05 paper
nrows=2;
ncols=ntrials+1;

%loop through and create all the subplots
%do this because if we plot the trials rowise (so consecutive trials) go
%down rows, then we end up looping through the axes out of order and this
%will create problems
for index=1:nrows*ncols
    subplot(nrows,ncols,index);
end

%we will plot the matrices side by side from each trial.
findex=1;
fseries(findex).hf=figure();
fseries(findex).axisfontsize=30;
fseries(findex).xlabel='i';
fseries(findex).fname=fullfile(outdir,sprintf('gaborseries%s.eps',fsuffix));
set(fseries(findex).hf,'name',sprintf('postseries:'));
%colormap(gray);
pindex=0;
hsubs=zeros(1,nrows*ncols);
for tindex=1:ntrials
    %subplot(ntrials,1,tindex);

    %if you want to make trials increase across columns then do the
    %following
    %pindex=pindex+1;

    %else we need to get the index of the first plot in the row for these
    %elements
    %get the row for this plot
    %pcol=floor(tindex*nseries/ncols);
    %prow=tindex-pcol*(ncols/nseries);

    % pindex=prow*ncols+(pcol-1)*nseries+1;

    for sindex=1:nseries
        row=sindex;
        col=tindex;
        pindex=(row-1)*ncols+col;

        hsubs(pindex)=subplot(nrows,ncols,pindex,'align');
        set(gca,'FontSize',fseries(findex).axisfontsize);
        hold on;
        imagesc(pdata(sindex).mmat{tindex},clim);
      
        if (row==1)
         
             title(sprintf('\n trial %d', pdata(1).trial(tindex)));
        end
        %add ylabels to the first column
        if (col==1)
            ylabel(pdata(sindex).lbl);
        end

        xlim([1 gwidth]);
        ylim([1 gheight]);
        axis square;
        set(gca,'xtick',[]);
        set(gca,'ytick',[]);


        %if k is not constant plot the true k
        if (kconst==0)
            pindex=pindex+1;
            hsubs(pindex)=subplot(nrows,ncols,pindex);
            set(gca,'FontSize',fseries(findex).axisfontsize);
            hold on;
            %index of the k parameter
            if (trials(tindex)==0)
                kindex=1;
            else
                kindex=trials(tindex);
            end
            imagesc(reshape(mparam.ktrue(:,kindex),[gheight,gwidth]),clim);
            %imagesc(pdata(sindex).mmat{tindex});

            pos=get(hsubs(pindex),'Position');
            set(hsubs(pindex),'Position',pos);
          %  axis square
            set(gca,'xtick',[]);
            if (sindex==1)
                title(sprintf('Trial %d', pdata(1).trial(tindex)));
            end
            xlim([1 gwidth]);
            ylim([1 gheight]);
        end
        %increment pindex by 1 to leave a blank axes
        %pindex=pindex+1;
    end
end

%in the last row we plot the true mparam
pindex=ncols;
hsubs(pindex)=subplot(nrows,ncols,pindex,'align');
drawnow;

set(gca,'FontSize',fseries(findex).axisfontsize);

imagesc(reshape(mparam.ktrue(:,1),[mparam.gheight,mparam.gwidth]),clim);
hold on;
colorbar;
axis square;
pos=get(hsubs(ncols-1),'Position');
kpos=get(gca,'Position');
kpos(3:end)=pos(3:end);
set(gca,'Position',kpos);

title('\theta true');
xlim([1 gwidth]);
ylim([1 gheight]);
set(gca,'xtick',[]);
set(gca,'ytick',[]);


for index=1:ncols
    pos=get(hsubs(1,index),'position');
    pos(2)=pos(2)-.1;
    set(hsubs(index),'position',pos);
end

return

%now we loop through them and set the sizes of each subplot appropriately
drawnow;
subplot(nrows,ncols,1);
ps1=get(gca,'Position');

swidth=ps1(3);
sheight=ps1(4);
sleft=ps1(1);
sbottom=ps1(2);

vspace=0;             % vertical spacing
for row=1:nrows
    for col = 1:ncols
        if(row==2 & col==ncols)
            break;
        end
                pindex=(row-1)*ncols+col
                if (pindex==ncols)
                    subplot(hktrue)
                else
                            subplot(nrows,ncols,pindex);
                end

        pos=get(gca,'Position');
        
        if (row==2)
            pos(2)=sbottom-vspace-sheight;
        end
        pos(3)=swidth;
        pos(4)=sheight;
        set(gca,'Position',pos);
        
        tpos=get(gca,'TightInset');
        opos(1)=pos(1)-tpos(1);
        opos(2)=pos(2)-tpos(3);
        opos(3)=pos(3)+tpos(1)+tpos(2);
        opos(4)=pos(4)+tpos(3)+tpos(4);
      %  set(gca,'OuterPosition',opos);
        set(gca,'ActivePositionProperty','Position');
        
       
    end
end


return

if(sindex>1)

    lastpos=get(hsubs(pindex-1),'Position');
    pos=get(hsubs(pindex),'Position');

    %leave a little space between the last plot
    pos(1)=lastpos(1)+1.1*lastpos(3);
    pos(3:4)=lastpos(3:4);

    set(hsubs(pindex),'Position',pos);
    axis square
else
    pos=get(hsubs(pindex),'Position');
    set(hsubs(pindex),'Position',pos);
    axis square
end

if (sindex==1)
    if (row==1)
        %title(sprintf('Information Maximizing\n Trial %d', pdata(1).trial(tindex)));
        title(sprintf('Trial %d', pdata(1).trial(tindex)));
    else
        title(sprintf('Trial %d', pdata(1).trial(tindex)));
    end
elseif (sindex==2 && row==1)
    %      title(sprintf('Random', pdata(1).trial(tindex)));
end







%exportfig(fseries(findex).hf,fseries(findex).fname,'bounds','tight','Color','bw');


%**************************************************************************
%Plot Ktrue in a separate figure
%make ktrue
fktrue.hf=figure;
fktrue.fname=fullfile(outdir,sprintf('gabortrue%s.eps',fsuffix));
fktrue.name='True K';
lblgraph(fktrue);

imagesc(reshape(mparam.ktrue(:,1),[gheight gwidth]),clim);
set(gca,'xtick',[]);
set(gca,'ytick',[]);
xlabel('x');
ylabel('y');
colorbar;

exportfig(fktrue.hf,fktrue.fname,'bounds','tight','Color','rgb');

%**************************************************************************
%make plots of stimuli


%we will plot the matrices side by side from each trial.
findex=1;
fstim(findex).hf=figure();
fstim(findex).axisfontsize=15;
fstim(findex).xlabel='i';
fstim(findex).name='Stimuli';
fstim(findex).fname=fullfile(outdir,sprintf('gaborstim%s.eps',fsuffix));

set(fstim(findex).hf,'name',sprintf('postseries:'));


%***********************************
%determine the color limits for the stimulit
%**************************************
stimclim(1,1)=min([pdata(nseries).sclimmin]);
stimclim(1,2)=max([pdata(nseries).sclimmax]);

pindex=0;
hsubs=zeros(nrows*ncols);
for tindex=1:ntrials
    %subplot(ntrials,1,tindex);
    for sindex=1:nseries
        pindex=pindex+1;
        hsubs(pindex)=subplot(nrows,ncols,pindex);
        set(gca,'FontSize',fstim(findex).axisfontsize);
        hold on;


        fstim(findex).hp(tindex)=imagesc(pdata(sindex).stim{tindex},stimclim);
        %imagesc(pdata(sindex).stim{tindex});
        axis square;
        %reposition it next to the end of the previous plot
        if(sindex>1)

            lastpos=get(hsubs(pindex-1),'Position');
            pos=get(hsubs(pindex),'Position');

            %leave a little space between the last plot
            pos(1)=lastpos(1)+1.1*lastpos(3);
            pos(3:4)=lastpos(3:4);

            set(hsubs(pindex),'Position',pos);
            %  axis square
        else
            pos=get(hsubs(pindex),'Position');
            set(hsubs(pindex),'Position',pos);
            axis square
        end
        set(gca,'xtick',[]);
        if (sindex==1)
            title(sprintf('Trial %d', pdata(1).trial(tindex)));
        end
        xlim([1 gwidth]);
        ylim([1 gheight]);



        set(gca,'ytick',[]);

    end
    %colorbar('North');
    %increment pindex by 1 to leave a blank axes
    %pindex=pindex+1;
end




exportfig(fstim.hf,fstim.fname,'bounds','tight','Color','rgb');
set(fstim.hf,'name','Stimuli');


figure(fseries(1).hf);