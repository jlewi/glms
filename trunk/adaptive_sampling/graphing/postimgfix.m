%function fmean =postimgfix('fname',filename)
%
% Optinal parameters
%       'nmag'  - normalize the magnitudes.
%                      Useful for looking at misspecified models where
%                      estimates are only accurate up to a scaling factor
%
%       trials   - what trials to display
%       simvars  - 1x2 structure of simulation variable names
%1-30-07
%       1. modified it to handle new object orientated model
%       2.  go back to earlier version for version used in aistats and nips
%       3. stripped a lot of the info about different files
%       4. can be run as function
%
%10-06-06
%make plots of the 1d plots of the mean as a function of trial

%create a structure which contains the relevant fields for different data
%sets we then select which element of the structure we want to select the
%data. if we want to use data in the current workspace set dind=0;
function [fmean]=postimgfix(varargin)

simfile=[];
%outfile=fullfile(RESULTSDIR,'numint','01_15','01_15_numint.eps');
outfile=[];
xlimits=[];
ylimits=[];
trials=[];
fsuffix='_shist';
yticks=[];
xticks=[];
clim=[];
width=4;
height=4;
fsize=16;
nmag=0;
%override variables
simvars={'simmax','simrand'};

for j=1:2:nargin
    switch varargin{j}
        case {'fname','filename','file','simfile','simfiles'}
            simfile=varargin{j+1};
        otherwise
            if exist(varargin{j},'var')
                eval(sprintf('%s=varargin{j+1};',varargin{j}));
            else
                fprintf('%s unrecognized parameter. \n',varargin{j});
            end
    end
end


%dind=3;
%**************************************************************************
%Select the data set
%**************************************************************************
%load data from file
[pmax simmax, mparam,srmax]=loadsim('simfile',simfile,'simvar',simvars{1});
[prand simrand, mparam,srrand]=loadsim('simfile',simfile,'simvar',simvars{2});

[PATHSTR,NAME,EXT,VERSN] = fileparts(simfile);
outfile=fullfile(PATHSTR,sprintf('%s.eps',NAME));

if ~isempty(trials)
    pmax.m=pmax.m(:,trials);
    prand.m=prand.m(:,trials);
end
%normalize the magnitudes
if (nmag~=0)
    pmax.m=normmag(pmax.m);
    prand.m=normmag(prand.m);
    ktrue=normmag(mparam.ktrue);
else 
    %ktrue=mparam.ktrue;
    %get the true parameters
    ktrue=gettheta(simmax.observer);
end
if isempty(clim)
    dk=max(ktrue)-min(ktrue);
    clim=[min(ktrue)-.1*dk max(ktrue)+.1*dk];
end
%pmax.newton1d.m=pmax.newton1d.m(:,790:800);
%prand.newton1d.m=prand.newton1d.m(:,790:800);
%***************************************************
%Plot the mean
%*****************************************************
plog=0;
%index =1 : plot stimulus coefficients
%index =2 :plot spikehistory coefficients
if (mparam.alength>0)
    nfigs=2;
else
    nfigs=1;
end
figs=cell(1,nfigs);
for index=1:nfigs
    fmean=[];
    fmean.hf=figure;

    
    %**********************************************************************
    %Configure the image for aistat
    %***********************************
    fmean.font='Times'; %use a scalable font
    set(fmean.hf,'units','inches');
    fpos=get(fmean.hf,'position');
    fpos(3)=width;
    fpos(4)=height;
    set(fmean.hf,'position',fpos);
    fmean.axisfontsize=10;% was point 1
    fmean.lgndfontsize=10;
    fmean.fontunits='Points';
    fmean.font='Times'; %use a scalable font

    %extract the data
    if index==1
        pdata.pmax=pmax.m(1:mparam.klength,:)';
        pdata.prand=prand.m(1:mparam.klength,:)';
        pdata.true=ktrue';
        fmean.name='Stimulus Coefficients'
        [pathstr, name,ext]=fileparts(outfile);
        fmean.fname=fullfile(pathstr,sprintf('%s_stim%s',name,ext));
    else
        %plot spike history on log scale
        %assume its all inhibitory
        plog=0;
        if isfield(data(dind),'shistlog')
            if (data(dind).shistlog~=0)
                plog=1;

            end
        end
        if (plog~=0)

            pdata.pmax=log10(abs(pmax.m(mparam.klength+1:end,:)))';
            pdata.prand=log10(abs(prand.m(mparam.klength+1:end,:)))';
            pdata.true=log10(abs(mparam.atrue))';


        else

            pdata.pmax=pmax.m(mparam.klength+1:end,:)';
            pdata.prand=prand.m(mparam.klength+1:end,:)';
            pdata.true=mparam.atrue';
        end

        fmean.name='Spike History Coefficients';
        [pathstr, name,ext]=fileparts(outfile);
        fmean.fname=fullfile(pathstr,sprintf('%s_hist%s',name,ext));
        %clim(1)=min(pdata.true(:));
        %clim(2)=max(pdata.true(:));
    end
    if iscell(clim)
        clim=clim{index};
    end
    if iscell(xticks)
        xticks=xticks{index};
    end


    fmean.hp=[];
    set(fmean.hf,'name',sprintf('postseries:'));

    set(fmean.hf,'name',sprintf('postseries:'));

    %*********************************
    %we plot the results for infomax and random trials and them the
    %true values below them
    nrows=2;
    ncols=2;
    %colormap(gray);


    %*************************************
    %Plot: infomax
    %***************************************
    fmean.a{1}.ha=subplot(nrows,ncols,1);
    set(gca,'fontunits',fmean.fontunits);
    set(gca,'fontsize',fmean.axisfontsize);

    
    fmean.a{1}.hp(1)=imagesc(pdata.pmax,clim);
    title(sprintf('info. max.'));
    %xlabel('i');
    %*****************************
    %remove the yaxis labels from the spike history plot
    if ~isempty(yticks)
      set(gca,'ytick',yticks);
    end
    if (index==1)
        ylabel('Trial');
   %     set(gca,'YTickLabel',yticks);
    else
        set(gca,'YtickLabel','');
    end;
    set(gca,'xtick',[]);

    %set(gca,'xtick',xticks);
    %set(gca,'XTickLabel',xticks);

    %*****************************************
    %Plot: True below infomax
    %****************************************
    fmean.a{3}.ha=subplot(nrows,ncols,3);
    set(gca,'fontunits',fmean.fontunits);
    set(gca,'fontsize',fmean.axisfontsize);
    imagesc(pdata.true,clim);

    %title('True \theta');
    xlabel('i');
    set(gca,'xtick',xticks);
    %set(gca,'XTickLabel',xticks);
    set(gca,'ytick',[]);

    %colorbar resizes this image so lets set its to be equal to other axes
    %execute a drawnow command to force matlab to determine the sizes for the
    %individual axes
    %Too make each axis the same size we need to do the following
    %   1. set the position of the last subplot appropriately (its size is not
    %   the same because we add the colorbar)
    %   2. switch the ActivePositionProperty to 'position' so it scales the
    %   image based on Position not outerposition.
    drawnow;
    fmean.a{1}.ha=subplot(nrows,ncols,1);
    fmean.a{2}.ha=subplot(nrows,ncols,2);%
    subplot(nrows,ncols,3);


    ha(2)=subplot(nrows,ncols,2);
    set(gca,'fontsize',fmean.axisfontsize);
    imagesc(pdata.prand,clim);
    hc=colorbar;


    %***************************************************
    %Plot: Random stimuli
    %**************************************************
    title(simvars{2});
    %xlabel('i');
    %ylabel('Trial');
    set(fmean.hf,'name','Post Series');
    set(gca,'fontunits',fmean.fontunits);
    set(gca,'fontsize',fmean.axisfontsize);
    set(gca,'xtick',[]);
%    set(gca,'xtick',xticks);
    %set(gca,'XTickLabel',xticks);
    set(gca,'ytick',[]);
    %set(gca,'ActivePositionProperty','Position');
    %subplot(nrows,ncols,4);
    %hc=colorbar;
    %set(hc,'CLim',clim);

    %***********************************************
    %Plot: true beneath random
    %**********************************************
    fmean.a{4}.ha=subplot(nrows,ncols,4);
    set(gca,'fontsize',fmean.axisfontsize);
    set(gca,'fontunits',fmean.fontunits);
    imagesc(pdata.true,clim);

    %title('True \theta');
    xlabel('i');
    set(gca,'xtick',xticks);
    %set(gca,'XTickLabel',xticks);
    set(gca,'ytick',[]);

    %tick labels  for the colorbar get added later after we've positioned
    %everything
    if (index==2)
        if (plog~=0)
            cytick=get(hc,'ytick');
            cytick=[-7  -4 -1];
            set(hc,'ytick',cytick);
            cyticklbls=cell(1,length(cytick));
            for cind=1:length(cytick)
                cyticklbls{cind}=sprintf('-10^{%0.2d}',cytick(cind));
            end
            %      cyticklbls{end}=1;
            set(hc,'yticklabel',cyticklbls);

        end
    end
    fmean=lblgraph(fmean);


    %*********************************************************************
    %Position the figures
    %*******************************************************************
    %findall text objects
    %set the fontname and fontsize of all objects
    allText   = findall(fmean.hf, 'type', 'text');
    set(allText,'FontName','Times');
    set(allText,'FontSize',fsize);

    allAxes   = findall(fmean.hf, 'type', 'axes');
    set(allAxes,'FontName','Times');
    set(allAxes,'FontSize',fsize);

    %set the figure size based on how big it will be in our pdf so we don't
    %have to resize
    %set(fmean.hf,'units','inches');
    %fpos=get(fmean.hf,'position');
    %fpos(3)=1.5;
    %fpos(4)=1.75;
    %set(fmean.hf,'Position',fpos);
    %everthing is measured in relative screen coordinates
    %determine how much space we need for the labels
    %tinset=[left bottom right top]
    tinset=zeros(nrows*ncols+1,4);

    for hind=1:4
        tinset(hind,:)=get(fmean.a{hind}.ha,'tightinset');
    end
    %text is being cuttoff on xlabel so add some padding
    tinset(1)=tinset(1)+.02;
    tinset(5,:)=get(hc,'tightinset');               %tightinset for colorbar
    %we want to find the effective width and height which is how much room we
    %have which isn't taken up by the labels
    %effective width measure the left and right margins need
    %for the infomax, random, and colorbar
    effwidth=1-(tinset(1,1)+tinset(1,3))-(tinset(2,1)+tinset(2,3))-(tinset(5,1)+tinset(5,3));
    %effective height depends on top bottom
    %for infomax and true
    effheight=1-(tinset(1,2)+tinset(1,4))-(tinset(3,2)+tinset(3,4));


    %we need to position the figures
    %we want the interiors of the figures to be aligned
    %when positioning the interior of the figures we need to make sure we
    %allowe room for the labels

    wspace=.07;         %spacing between the two columns of figures
    hspace=.05;         %horizontal spacing

    htrue=.12;           %height for the true graph

    hpost=effheight-hspace-htrue;   %height for graph of posteriors

    wcb=.05;             %width for colorbar
    wtocb=.03;          %distance from edge of random to colorbar
    wpost=(effwidth-wspace-wcb-wtocb)/2;

    bpost=1-tinset(1,4)-hpost; %bottom of the posterior results
    %its set relative to top of
    %figure
    %set the position of the infomax results
    linfo=tinset(1,1);                  %left coordinate of post info max
    set(fmean.a{1}.ha,'position',[linfo bpost wpost hpost]);

    %set the position of ktrue
    btrue=tinset(3,2);                  %bottom of true is set relative to bottom of figure

    set(fmean.a{3}.ha,'position',[linfo btrue wpost htrue]);

    %set the position of the results for random
    lrandom=linfo+wspace+wpost;     %left coordinate of posterior results for random
    set(fmean.a{2}.ha,'position',[lrandom bpost wpost hpost]);


    %set the position of ktrue below random results
    set(fmean.a{4}.ha,'position',[lrandom btrue wpost htrue]);
    refresh;
    %adjust the width and bottom of the colorbar
    hcpos=get(hc,'position')
    hcpos(1)=lrandom+wpost+wtocb;
    hcpos(2)=bpost;
    hcpos(4)=hpost;
    set(hc,'position',hcpos);

    %readjust the position of the second plot because above code seems to
    %affect it
    lrandom=linfo+wspace+wpost;     %left coordinate of posterior results for random
    set(fmean.a{2}.ha,'position',[lrandom bpost wpost hpost]);




    %modify the tick labels for the spike history terms to make them true
    %exponentials
    %everything
    if (index==2)
        if (plog~=0)
            cytick=get(hc,'ytick');
            cytick=[-7  -4 -1];
            set(hc,'ytick',cytick);
            cyticklbls=cell(1,length(cytick));
            %THis code is slightly unstable
            %point is to make the labels of the colorbar true
            %exponentials. First set the ticklabels to be strings in
            %tex format then call extyticklbls
            for cind=1:length(cytick)
                cyticklbls{cind}=sprintf('-10^{%0.1d}',cytick(cind));
            end
            %      cyticklbls{end}=1;
            set(hc,'yticklabel',cyticklbls);
            %now call Extyticklabels
            hcytick=Extyticklbls(hc);
        end
    end

    %previewfig(fmean.hf,'bounds','tight','Color','bw');
    %exportfig(fmean.hf,fmean.fname,'bounds','tight','Color','bw');



    %exportfig
    %for nips specify the figure size and the fontsize
    %k=1;previewfig(figs{k}.hf,'bounds','tight','FontMode','Fixed','FontSize',fsize,'Width',width,'Height',height)
    %k=1;exportfig(figs{k}.hf,figs{k}.fname,'bounds','tight','FontMode','Fixed','FontSize',fsize,'Width',width,'Height',height)


    figs{index}=fmean;
end