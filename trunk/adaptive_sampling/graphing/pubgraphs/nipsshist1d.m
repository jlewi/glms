%make 1d plots of the posterior estiamtes when we have spike history
%included
data=[];
data(end+1).infile=fullfile(RESULTSDIR, 'nips','12_19','12_19_shist_002.mat');
data(end).koutfile=fullfile(RESULTSDIR,'nips','12_19','shistcompare_kmean.eps');
data(end).aoutfile=fullfile(RESULTSDIR,'nips','12_19','shistcompare_amean.eps');
data(end).lgndfile=fullfile(RESULTSDIR,'nips','12_19','shistcompare_lgnd.eps');

data(end).ylimits=[-4 3];
data(end).aylimits=[-.02 -.0004];
data(end).trial=[800];
data(end).width=2.5;                %width of image in inches
data(end).height=2;                 %height of images
data(end).fsize=10;                 %fontsize


dind=length(data);
marksize=10;
%load data from file
load(data(dind).infile);
ylimits=data(dind).ylimits;
aylimits=data(dind).aylimits;
koutfile=data(end).koutfile;
aoutfile=data(end).aoutfile;
fprintf('\n\n Plotting data from file: \n %s \n', data(dind).infile);



%linestyles
ls={'--',':','-.'};
lwidthtrue=3;
lwidth=3;

mind='max';
methods.(mind).label='info. max.';
methods.(mind).ls='-';
methods.(mind).lcolor=[.5 .5 .5];
methods.(mind).lwidth=lwidth+1;

mind='rand';
methods.(mind).label='random';
methods.(mind).ls='-'
methods.(mind).lwidth=lwidth+2;
methods.(mind).lcolor=[.85 .85 .85];
%methods.(mind).lcolor=[1 0 0];


mind='true';
%methods.(mind).label=sprintf('%s \nincorrect model',labels.infomax);
methods.(mind).label='true';
methods.(mind).ls='--';
%methods(2).lcolor=[.5 .5 .5];
methods.(mind).lcolor=[0 0 0];
methods.(mind).lwidth=lwidth;

%**************************************************************************
%Plots
%************************************************************************

%**************************************************************************
%Stimulus Coefficients
%**************************************************************************

fstim.hf=figure();
fstim.axisfontsize=data(dind).fsize;
fstim.xlabel='i'
fstim.ylabel=sprintf('Trial %d\n\n k_i',data(dind).trial);
fstim.fname=koutfile;
fstim.name='Stimulus Coefficients 1d plot';
fstim.hp=[];
fstim.lbls={};
set(fstim.hf, 'units','inches');
pos=get(fstim.hf,'position');
set(fstim.hf,'position',[pos(1) pos(2) data(dind).width data(dind).height]);
set(fstim.hf,'name',sprintf('postseries:'));
set(gca,'FontSize',fstim.axisfontsize);
hold on;

%get the data
%this just makes it to reorder the plots
m.('max')=pmax.newton1d.m(1:klength,data(dind).trial+1);
m.('rand')=prand.newton1d.m(1:klength,data(dind).trial+1);
m.('true')=mparam.ktrue;

a.('max')=pmax.newton1d.m(klength+1:end,data(dind).trial+1);
a.('rand')=prand.newton1d.m(klength+1:end,data(dind).trial+1);
a.('true')=mparam.atrue;

%************************
%plot the random value value of stimulus coefficients
%*********************************************
klength=mparam.klength;
pind=1;
mind='rand';
%add 1 to account for prior
fstim.hp(pind)=plot([1:klength],m.(mind));
hp=fstim.hp(pind);

fstim.lbls{pind}=methods.(mind).label;
set(hp(pind),'LineStyle',methods.(mind).ls);
set(hp(pind),'LineWidth',methods.(mind).lwidth);
set(hp(pind),'Color',methods.(mind).lcolor);

%******************************************
%random stimuli: stimulus coefficients
%**************************************
pind=pind+1;
mind='max';
%add 1 to account for prior

fstim.hp(pind)=plot([1:klength],m.(mind));
hp=fstim.hp(pind);
fstim.lbls{pind}=methods.(mind).label;
set(hp,'LineStyle',methods.(mind).ls);
set(hp,'LineWidth',methods.(mind).lwidth);
set(hp,'Color',methods.(mind).lcolor);


%*****************************************************
%true parameter
%*****************************************************
pind=pind+1;
mind='true';
%add 1 to account for prior

fstim.hp(pind)=plot([1:klength],m.(mind));
hp=fstim.hp(pind);
fstim.lbls{pind}=methods.(mind).label;

set(hp,'LineStyle',methods.(mind).ls);
set(hp,'LineWidth',methods.(mind).lwidth);
set(hp,'Color',methods.(mind).lcolor);
%return;
%ylabel(ylbl);
%legend([pdata(1).hp(1) pdata(2).hp(1) pdata(3).hp(1)  hptrue],{pdata(:).lbl, 'k true'});

fstim=lblgraph(fstim);
legend(gca,'off');
ylim(data(dind).ylimits);
xlim([1 klength]);

alltext=findall(fstim.hf,'type','text');
set(alltext,'FontName','times');
set(alltext,'FontSize',data(dind).fsize);



%**************************************************************************
%spike history Coefficients
%**************************************************************************

fhcoef.hf=figure();
fhcoef.axisfontsize=data(dind).fsize;
fhcoef.xlabel='i'
fhcoef.ylabel=sprintf('Trial %d\n\n a_i',data(dind).trial);
fhcoef.fname=aoutfile;
fhcoef.name='Spike history 1d plot';
fhcoef.hp=[];
fhcoef.lbls={};

%we use two axes because the second axis is for the legend

set(fhcoef.hf, 'units','inches');
pos=get(fhcoef.hf,'position');
set(fhcoef.hf,'position',[pos(1) pos(2) data(dind).width data(dind).height]);
set(fhcoef.hf,'name',sprintf('postseries:'));

%create a blank axis as large as the figure
%we will use this to position the legend
fhcoef.hafull=axes('position',[0 0 1 1]);
axis off; 
xlim([0 1]);
ylim([0 1]);
set(gca,'FontSize',fhcoef.axisfontsize);

%create axes for plot
fhcoef.haplot=axes;
hold on;
%************************
%plot the max value value of stimulus coefficients
%*********************************************
alength=mparam.alength;
pind=1;
mind='rand';
%add 1 to account for prior

fhcoef.hp(pind)=plot([1:alength],a.(mind));
hp=fhcoef.hp(pind);

fhcoef.lbls{pind}=methods.(mind).label;
set(hp(pind),'LineStyle',methods.(mind).ls);
set(hp(pind),'LineWidth',methods.(mind).lwidth);
set(hp(pind),'Color',methods.(mind).lcolor);

%******************************************
%random stimuli: stimulus coefficients
%**************************************
pind=pind+1;
mind='max';
%add 1 to account for prior
%m=prand.newton1d.m(klength+1:end,data(dind).trial+1);
fhcoef.hp(pind)=plot([1:alength],a.(mind));
hp=fhcoef.hp(pind);
fhcoef.lbls{pind}=methods.(mind).label;
set(hp,'LineStyle',methods.(mind).ls);
set(hp,'LineWidth',methods.(mind).lwidth);
set(hp,'Color',methods.(mind).lcolor);

%turn of legend
legend(gca,'off');
%*****************************************************
%true parameter
%*****************************************************
pind=pind+1;
mind='true';
%add 1 to account for prior
m=prand.newton1d.m(klength+1:end,data(dind).trial+1);
fhcoef.hp(pind)=plot([1:alength],a.(mind));
hp=fhcoef.hp(pind);
fhcoef.lbls{pind}=methods.(mind).label;

set(hp,'LineStyle',methods.(mind).ls);
set(hp,'LineWidth',methods.(mind).lwidth);
set(hp,'Color',methods.(mind).lcolor);

fhcoef=lblgraph(fhcoef);
%turn off legend
legend(gca,'off')

ylim('auto');
xlim([1 alength]);

%******************************************************************
%position/create the legend
%******************************************************************
%1. adjust the height of the main plot so we have room for the legend
ppos=get(fhcoef.haplot,'position');
ppos(4)=.55;
set(fhcoef.haplot,'position',ppos);
%create the legend
hl=legend(fhcoef.hafull,fhcoef.hp,fhcoef.lbls,'Location','NorthWest');
%align the legend with the start of the axes
hlpos=get(hl,'position');
hlpos(1)=ppos(1);
hlpos(2)=1-hlpos(4);
set(hl,'position',hlpos);

alltext=findall(fhcoef.hf,'type','text');
set(alltext,'FontName','times');
set(alltext,'FontSize',data(dind).fsize);

%****************************************
%plot legend on separate figure
%***************************************
%on second subplot place legend
%flg.hf=figure;
%flg.width=1.5;
%flg.height=1;
%set(flg.hf,'units','inches');
%pos=get(flg.hf,'position');
%set(flg.hf,'position',[pos(1:2) flg.width flg.height]);
%axis off;
%legend(gca,fhcoef.hp,fhcoef.lbls);
%alltext=findall(fhcoef.hf,'type','text');
%set(alltext,'FontName','times');
%set(alltext,'FontSize',data(dind).fsize);

%exportfig(fseries.hf,koutfile,'bounds','tight','Color','bw');
%exportfig(fhcoef.hf,aoutfile,'bounds','tight','Color','bw');
%previewfig(fstim.hf,'bounds','tight','FontMode','Fixed','FontSize',data(dind).fsize,'Width',data(dind).width,'Height',data(dind).height)
%previewfig(fhcoef.hf,'bounds','tight','FontMode','Fixed','FontSize',data(dind).fsize,'Width',data(dind).width,'Height',data(dind).height)
%exportfig(fstim.hf,koutfile,'bounds','tight','FontMode','Fixed','FontSize',data(dind).fsize,'Width',data(dind).width,'Height',data(dind).height,'Color','rgb')
%exportfig(fhcoef.hf,aoutfile,'bounds','tight','FontMode','Fixed','FontSize',data(dind).fsize,'Width',data(dind).width,'Height',data(dind).height,'Color','rgb')
%exportfig(flg.hf,data(dind).lgndfile,'bounds','tight','FontMode','Fixed','FontSize',data(dind).fsize,'Width',flg.width,'Height',flg.height,'Color','rgb')