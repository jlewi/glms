%11_15
%Plot for nips presentation comparaing the kl distance between our
%Gaussian approximation and the true posterior
%
%
outfile=fullfile(RESULTSDIR,'nips','11_15','11_15_kldata_001.mat');
figfile=fullfile(RESULTSDIR,'nips','11_15','11_15_kllowdcos_001.eps');
simmeth='max';
umeth={'newton1d','allobsrv'};           %which method to compute the posterior.

%load the results
%compute the kl distance
muse=[0];     %set to 0 use all samples
trials=[100:100:400 500:500:2000];
dk=[];
vsum=[];

for mind=1:length(umeth)
dk.(umeth{mind})=zeros(length(trials),length(muse));
vsum.(umeth{mind})=zeros(length(trials),length(muse));
for tind=1:length(trials)
    trial=trials(tind);
    vname=sprintf('dkdata%s%s%d',simmeth,umeth{mind},trial);
    vars=whos('-file',outfile);
    if isempty(strmatch(vname,{vars.name},'exact'))
       warning(sprintf('File did not contain variable %s \n'),vname)
       vname=sprintf('dkdata%s%d',simmeth,trial);
       warning(sprintf('Checking for %s \n',vname));
    end
     if isempty(strmatch(vname,{vars.name},'exact'))
       warning(sprintf('File did not contain variable %s \n'),vname)             

     else
    load(outfile,vname);
    eval(sprintf('data=%s',vname));
    for k=1:length(muse)
        %the lpost is sorted so if we use less than all the samples we need to
        %randomly select them
        if (muse(k)<length(data.lpost) && muse(k)>0)
            rind=ceil(rand(1,muse(k))*length(data.lpost));
            warning('Im not sure my shuffling is correct');
            [dk.(umeth{mind})(tind,k),vsum.(umeth{mind})(tind,k)]=dkcompute(data.lpost(rind),data.lpapprox(rind),data.gpost);
        else
            [dk.(umeth{mind})(tind,k),vsum.(umeth{mind})(tind,k)]=dkcompute(data.lpost,data.lpapprox,data.gpost);
            muse(k)=length(data.lpost);            
        end
    end
     end
end
end
fkl.hf=figure;
fkl.xlabel='Trials';
fkl.ylabel='KL Distance';
fkl.hp=[];
fkl.lbls={};
fkl.markersize=10;
hold on;

for mind=1:length(umeth)
    %plot(ksamps,dk);
    %[c,m]=getptype(1,1)
    fkl.hp(mind)=errorbar(trials,dk.(umeth{mind})(:,1),2*vsum.(umeth{mind})(:,1).^.5);
    [c,mark]=getptype(mind,1);
    set(fkl.hp(mind),'Color', c);
    set(fkl.hp(mind),'Marker', mark);
    set(fkl.hp(mind),'LineStyle','-');
    set(fkl.hp(mind),'MarkerSize',fkl.markersize);
    set(fkl.hp(mind),'LineWidth',2);
    if (strcmp(umeth{mind},'newton1d'))
         fkl.lbls{mind}='Online';
    elseif (strcmp(umeth{mind},'allobsrv'))        
        fkl.lbls{mind}='Batch';
    else
        fkl.lbls{mind}=sprintf('%s',umeth{mind});
    end
end

legend(fkl.lbls)
lblgraph(fkl);
xlim([0 max(trials)]);
%set(gca,'xscale','log');
%exportfig(fkl.hf,figfile,'bounds','tight','color','rgb');