%function postimg(map,ktrue)
%   map- matrix of the means
%   ktrue - the true parameter
%
% Explanation: the purpose of this file is to make a single plot of the
% MAPs and the true parameter below it. This is primarily intended for
% making quick figures of a SINGLE SIMULATION not to COMPARE SIMS
%
%1-30-07
%   1. turned it into a funciton
%   2. plot the results for a single simulation
%   3. pass the data in, not a file.
function [fmean]=postimg(map,ktrue);

%***************************************************
%Plot the mean
fmean=[];

fmean.hf=figure;
fmean.axisfontsize=15;

fmean.hp=[];
fmean.name='MAP';
fmean.title='MAP';

    set(fmean.hf,'name',fmean.name);

    %*********************************
    %we plot the results for infomax and random trials and them the
    %true values below them
    nrows=2;
    ncols=1;
    %colormap(gray);


    %*************************************
    %Plot: the data
    %***************************************
    ha(1)=subplot(nrows,ncols,1);
%    set(gca,'fontunits',fmean.fontunits);
    set(gca,'fontsize',fmean.axisfontsize);

    fmean.hp(1)=imagesc(map');
        set(gca,'xtick',[]);
    ylabel('Trial');
    title(fmean.title);
    clim=get(gca,'clim');
   colorbar;
    %*****************************************
    %Plot: True below infomax
    %****************************************
    ha(2)=subplot(nrows,ncols,2);
    set(gca,'fontsize',fmean.axisfontsize);
    imagesc(ktrue',clim);

    %title('True \theta');
    xlabel('i');
    set(gca,'ytick',[]);

    drawnow
    %*************************
    %adjust the sizes
    mpos=get(ha(1),'position');
    mpos(4)=.7;             %adjust the height
    mpos(2)=.2
    set(ha(1),'position',mpos);
    
    tpos=get(ha(2),'position');
    tpos(1)=mpos(1);
    tpos(3)=mpos(3);
    tpos(4)=.05;
    set(ha(2),'position',tpos);
    
    
    refresh;
