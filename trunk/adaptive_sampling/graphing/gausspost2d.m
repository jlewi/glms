%function gpostd(data,param)
%   data - a 2-d gaussian to plot 
%         .m - mean
%         .c - covariance matrix
%   param - optional parameters
%       .labels - labels for each field of data
%                this is a structure where each named field corresponds
%                to one of the fields of data.
%       .hf     -handle to the figure
%       .x      -x values to  plot
%       .xlim   -limits of graph
%       .ylim
%       .ktru   - mark true k
function [param]=gausspost2d(data,param)

%numpts is number of pts to sample gaussian in each direction
numpts=1000;
%nstd is the number of std's in each direction we want graph to show
nstd=4;

fnames=fieldnames(data);

if ~(exist('param','var'))
    param=[];
end
if ~(isfield(param,'hf'))
    param.hf=figure;
end

if ~(isfield(param,'title'))
    param.title='';
end
dimchange=0;
if ~(isfield(param,'pts'))
    param.pts=[];
    dimchange=1;
else
    dimchange=0;
end

if ~(isfield(param,'xlim'))
    param.xlim=[-1 1];
end
if ~(isfield(param,'ylim'))
    param.ylim=[-1 1];
end
figure(param.hf)

%matrix used to compute what the xlimits should be
xl=zeros(length(fnames),2);

hold on;
h_p=zeros(1,length(fnames));
lbls=cell(1,length(fnames));


%current limits
climits=[param.xlim;param.ylim];

%new limits
nlim(1,:)=[data.m(1)-nstd*(data.c(1,1))^.5 data.m(1)+nstd*(data.c(1,1))^.5];
nlim(2,:)=[data.m(2)-nstd*(data.c(2,2))^.5 data.m(2)+nstd*(data.c(2,2))^.5];

for d=1:2
    %take as the xlim limits the smalles value of xl(:,1) and largest value of
   
    %always zoom out if needed
    if (nlim(d,2)>climits(d,2))
        climits(d,2)=nlim(d,2);
       dimchange=1;
    end
    
    if (nlim(d,1)<climits(d,1))
        climits(d,1)=nlim(d,1);
       dimchange=1;
    end

    %zoom in when ever white space on either side of pmass
    %is >25%
    width=diff(climits(d,:));
    if (((climits(d,2)-nlim(d,2))/width)>.45)
        climits(d,2)=nlim(d,2);
        dimchange=1;
    end
    if (((nlim(d,1)-climits(d,1))/width)>.45)
        climits(d,1)=nlim(d,1);
        dimchange=1;
    end
end

param.xlim=climits(1,:);
param.ylim=climits(2,:);

%sample the gaussian
%only get new points if dimchange=1
if (dimchange==1)
    [p,pts]=mvgaussmatrix(climits,diff(climits,1,2)/numpts,data.m,data.c);
    
    param.y=climits(2,1):diff(climits(2,:),1,2)/numpts:climits(2,2);
    param.x=climits(1,1):diff(climits(1,:),1,2)/numpts:climits(1,2);
    param.pts=pts;
    
else
    %now we just evalute the gaussian on these points
    p=mvgauss(param.pts',data.m,data.c);

    %now we reshape it to get the initial values

   p=reshape(p,[(numpts+1) (numpts+1)]);
end

imagesc(param.x,param.y,p);
hold on;
if isfield(param,'ktrue')
    h=plot(param.ktrue(1),param.ktrue(2),'k.');
    set(h,'MarkerSize',24);
end

xlim(param.xlim);
ylim(param.ylim);

colorbar;
%legend(h_p,lbls);
xlabel('\theta_1');
ylabel('\theta_2');
title(param.title);
