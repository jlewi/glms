%function fmean =plotstim('fname',filename)
%
%
% Explanation maeke a plot of the stimuli as an image
function [fstim]=plotstim(varargin)
ylims=[];
for j=1:2:nargin
    switch varargin{j}
        case {'fname','filename','file','simfile','simfiles'}
            simfile=varargin{j+1};
        otherwise
            if exist(varargin{j},'var')
                eval(sprintf('%s=varargin{j+1};',varargin{j}));
            else
                fprintf('%s unrecognized parameter. \n',varargin{j});
            end
    end
end


%load data from file
[pmax simmax, mparam,srmax]=loadsim('simfile',simfile,'simvar','simmax');
[prand simrand, mparam,srrand]=loadsim('simfile',simfile,'simvar','simrand');

fstim.hf=figure;
fstim.name='Stimuli';

aind=1;
fstim.a{1}.ha=subplot(1,2,1);
fstim.a{aind}.xlabel='i';
fstim.a{aind}.ylabel='trial';
fstim.a{aind}.title='info. max';

imagesc(srmax.y');
title('info. max');
if ~isempty(ylims)
    ylim(ylims);
end

aind=2;
fstim.a{aind}.ha=subplot(1,2,2);
fstim.a{aind}.xlabel='i';
fstim.a{aind}.title='i.i.d';
imagesc(srrand.y');
if ~isempty(ylims)
    ylim(ylims);
end

lblgraph(fstim);

%make the clims the same
clim=[get(fstim.a{1}.ha,'clim');get(fstim.a{2}.ha,'clim')];
clim=max(clim,[],1);
set(fstim.a{1}.ha,'clim',clim);
set(fstim.a{2}.ha,'clim',clim);
subplot(1,2,1);
colorbar;
subplot(1,2,2);
colorbar;




