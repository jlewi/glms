%function gpostd(data,param)
%   data - structure containing different gaussians to plot
%        -each field is taken as 2d gaussian to plot
%       WARNING IF THERE ARE MULTIPLE GRAPHS/DATASETS ONLY THE LAST ONE
%       WILL BE PLOTTED
%   param - optional parameters
%       .labels - labels for each field of data
%                this is a structure where each named field corresponds
%                to one of the fields of data.
%       .hf     -handle to the figure
%       .x      -x values to  plot
%       .xlim   -limits of graph
% Explanation plots 2d gaussians intended to be used with slideshowf
function [param]=gpost2d(data,param)

%pmass is the fraction of total probability of the array we want our xlimits to encompass
pmass=.97;

fnames=fieldnames(data);

if ~(exist('param','var'))
    param=[];
end
if ~(isfield(param,'hf'))
    param.hf=figure;
end

if ~(isfield(param,'labels'))
    for index=1:length(fnames)
        param.labels.(fnames{index})=fnames{index};
    end
end

if ~(isfield(param,'x'));
    param.x=1:size(data.(fnames{1}),1);
end

if ~(isfield(param,'y'));
    param.y=1:size(data.(fnames{1}),1);
end

if ~(isfield(param,'xlim'))
    param.xlim=[-1 1];
end
if ~(isfield(param,'ylim'))
    param.ylim=[-1 1];
end
figure(param.hf)

%matrix used to compute what the xlimits should be
xl=zeros(length(fnames),2);

hold on;
h_p=zeros(1,length(fnames));
lbls=cell(1,length(fnames));

pts={param.x;param.y};
for index=1:length(fnames)
    %h_p(index)=plot(param.x,data.(fnames{index}),getptype(index,1));
    %plot(param.x,data.(fnames{index}),getptype(index,2));
    %transpose data so X coordinates run along x axis
    imagesc(param.y,param.x,(log(data.(fnames{index})))');
    
    lbls{index}=param.labels.(fnames{index});
    
    %compute the cumulative distributions of the marginals for this density
    dim1m=sum(data.(fnames{index}),2);
    dim1m=dim1m/sum(dim1m);
    dim2m=sum(data.(fnames{index}),1);
    dim2m=dim2m/sum(dim2m);
    
    cdfa(1,:)=cumsum(dim1m);
    cdfa(2,:)=cumsum(dim2m);
    
    %normalize=
    cdfa(1,:)=cdfa(1,:);
    cdfa(2,:)=cdfa(2,:);
    %do this for each dimension
    %find the interval containing pmass of the density
    %din indexes which dimension we are processing
    for dind=1:2
        cdf=cdfa(dind,:);
        ind=find(cdf<=((1-pmass*cdf(end))/2));
        if isempty(ind)
            ind=1;
        end
        xl(index,1)=pts{dind}(ind(end));

        ind=find(cdf>=(1/2+pmass*cdf(end)/2));
        if isempty(ind)
            ind=size(data.(fnames{index}),dind);
        end
        xl(index,2)=pts{dind}(ind(1));
        
        dlimits(index,dind,:)=xl(index,:);
            
    end
end

%current limits
climits=[param.xlim;param.ylim];

for d=1:2
    %take as the xlim limits the smalles value of xl(:,1) and largest value of
    %xl2
    nxlim(d,1)=min(dlimits(:,d,1));
    nxlim(d,2)=max(dlimits(:,d,2));

    %always zoom out if needed
    if (nxlim(d,2)>climits(d,2))
        climits(d,2)=nxlim(d,2);
    end
    
    if (nxlim(d,1)<climits(d,1))
        climits(d,1)=nxlim(d,1);
    end

    %zoom in when ever white space on either side of pmass
    %is >25%
    width=diff(climits(d,:));
    if (((climits(d,2)-nxlim(d,2))/width)>.45)
        climits(d,2)=nxlim(d,2);
    end
    if (((nxlim(d,1)-climits(d,1))/width)>.45)
        climits(d,1)=nxlim(d,1);
    end
end

param.xlim=climits(1,:);
param.ylim=climits(2,:);

xlim(param.xlim);
ylim(param.ylim);

%legend(h_p,lbls);
xlabel('\theta');
ylabel('Probability');

