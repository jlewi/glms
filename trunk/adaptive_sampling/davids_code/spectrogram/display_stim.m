function display_stim

% display_stim.m
% Display the spectrogram and envelope of a user selected stimulus.
% Calls spectrogram.m
% The basic processing is the same as used in STRFpak.
% dms 7/26/07

%****************************************
% Define some constants
%****************************************
ampsamprate = 1000; %This is input by the user in strfpak
fwidth = 125; %This is input by the user in strfpak
DBNOISE = 80; %Not sure where this # came from
initialFreq = 250; %define frequency range that i care about
endFreq = 8000;
psth_smooth = 20; %This is input by the user in strfpak
ftype = 2; %Filter for processing PSTH
smooth_rt = 41; %Used in autocorrelation, user input in strfpak
window = 200; %in msec
timeLag = round(0.5*window); %msec, used in autocorrelation, input in strfpak
pre = round(0.5*window); %amount of stim collected before each spike
post = round(window-pre); %amount of stim collected after each spike
nt = 2*round(timeLag) +1; %size of strf in msec

%****************************************
% Load the stimulus and calculate necessary parameters
%****************************************
% cd 'C:\Spike\WaveFiles'
filename1 = uigetfile('*.wav');
filename1 = filename1(1:end-4);  %chop off the .wav
[stimulus fs nbits] = wavread(filename1);
stimsize = length(stimulus);
stimduration = round(length(stimulus)*1000/fs)+1; %duration of stim in msec
binsize = floor(1/(2*pi*fwidth)*6*fs); %size of time bin over which fft is computed (in samples)
nFTfreqs = binsize; %# of frequencies at which fft is computed
increment = floor(fs/ampsamprate); %# samples to shift for each new bin start point
frameCount = floor((stimsize-binsize)/increment)+1; %# of bins

%****************************************
% Compute the spectrogram
%****************************************
[outSpectrum outfreqs] = spectrogram(stimulus,binsize,frameCount,...
    increment,nFTfreqs,DBNOISE,fs,initialFreq,endFreq);

%****************************************
% Plot the stimulus envelope and the spectrogram
%****************************************
figure
subplot(3,1,1)
envelope_time = (1/fs:1/fs:length(stimulus)/fs)*1000;
plot(envelope_time,stimulus)
axis tight
title(filename1,'Interpreter','none')
subplot(3,1,2:3)
imagesc(outSpectrum);axis xy
set(gca,'YTick',[])
xlabel('Time (msec)')
ylabel('Frequency')