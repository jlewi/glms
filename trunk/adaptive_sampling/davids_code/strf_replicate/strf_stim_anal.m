function [outSpectrum outfreqs] = strf_stim_anal2(stimulus,binsize,frameCount,increment,nFTfreqs,DBNOISE,fs,initialFreq,endFreq)

%****************************************
% Zero-pad the stimulus appropriately
%****************************************
stimulus = stimulus';
stimulus = [zeros(1,binsize/2) stimulus zeros(1, binsize/2)];

%****************************************
% Compute the gaussian filter
%****************************************
wx2 = ((1:binsize)-binsize/2).^2;
wvar = (binsize/6)^2;
ws = exp(-0.5*(wx2./wvar));

%****************************************
% Compute spectrogram of entire stimulus
% Use a sliding fourier transform
%****************************************
s = zeros(binsize/2+1, frameCount);
for i=1:frameCount
    start = (i-1)*increment + 1;
    last = start + binsize - 1;
    f = zeros(binsize, 1);
    f(1:binsize) = ws.*stimulus(start:last);
    binspec = fft(f);
    s(:,i) = binspec(1:(nFTfreqs/2+1));
end

%****************************************
% Translate to dB, rectify
%****************************************
tmp = max(0, 20*log10(abs(s)./max(max(abs(s))))+DBNOISE);

%****************************************
% Edit out the appropriate range of frequencies
%****************************************
select = [1:nFTfreqs/2+1];
fo = (select-1)'*fs/nFTfreqs;
freq_range = find(fo>=initialFreq & fo<=endFreq);
outSpectrum = tmp(freq_range,:);
outfreqs = fo(freq_range);