% strf_anal.m
% dms
% 5/8/07
% This is an attempt to replicate the strfpak algorithm for calculating the
% STRF.  It seems to be working
% This version is modularized into functions
%
% Updated: 7/22/07 - trying to go trial-by-trial w/psth

%****************************************
% Define some constants
%****************************************
clear all
ampsamprate = 1000; %This is input by the user in strfpak
fwidth = 125; %This is input by the user in strfpak
DBNOISE = 80; %Not sure where this # came from
initialFreq = 250; %define frequency range that i care about
endFreq = 8000;
psth_smooth = 20; %This is input by the user in strfpak
ftype = 2; %Filter for processing PSTH
% smooth_rt = 41; %Used in autocorrelation, user input in strfpak
window = 400; %in msec
timeLag = round(0.5*window); %msec, used in autocorrelation, input in strfpak
% pre = round(0.5*window); %amount of stim collected before each spike
% post = round(window-pre); %amount of stim collected after each spike
nt = 2*round(timeLag) +1; %size of strf in msec

%****************************************
% Define some modifiable parameters
%****************************************
Tol_val = 10; %tolerance value for strf in %
compute = [31];
test = [32];

%****************************************
% Load and parse data
%****************************************
cd /Users/dms/Documents/matlab/PaninskiLab/strf
[dummy list] = xlsread('correspondences.xls','A');
num = size(list,1)-1;
wave_type = list(2:num+1,1);
stimuli = list(2:num+1,2);
adapting_cells = list(2:num+1,3);
nonadapting_cells = list(2:num+1,4);

count = 0;
for i = compute,
    %****************************************
    % Select the appropriate stimulus/response combinations
    %****************************************
    count = count + 1;
    wave_dir = ['/Users/dms/Documents/matlab/WoolleyLab/stimuli/', wave_type{i}, '_in_order'];
    cd(wave_dir)
    [stimulus,fs,nbits] = wavread(stimuli{i});
    stimsize = length(stimulus);
    stimduration = round(length(stimulus)*1000/fs)+1; %duration of stim in msec
    binsize = floor(1/(2*pi*fwidth)*6*fs); %size of time bin over which fft is computed (in samples)
    nFTfreqs = binsize; %# of frequencies at which fft is computed
    increment = floor(fs/ampsamprate); %# samples to shift for each new bin start point
    frameCount = floor((stimsize-binsize)/increment)+1; %# of bins

    adapting_dir = ['/Users/dms/Documents/matlab/WoolleyLab/data/old/adapting_cells/lghp1616.7_a/responses_to_', wave_type{i}];
    cd(adapting_dir)
    adapting_cell = dlmread(adapting_cells{i});
    neuron = adapting_cell;

    %****************************************
    % 1: Process the stimulus
    %****************************************
    [outSpectrum outfreqs] = strf_stim_anal(stimulus,binsize,frameCount,...
        increment,nFTfreqs,DBNOISE,fs,initialFreq,endFreq);
    nband = size(outSpectrum,1);

    %****************************************
    % 2: Process the response
    % This stage is mainly for presentation of psth only
    %****************************************
    [neuronpsth neuronmean neuronmat ntrials] = strf_resp_simple(neuron,stimduration,psth_smooth,ftype,ampsamprate);
    
    %****************************************
    % Added on 7/22/07
    % Concatinate the stimulus for each trial
    % Reshape the spike train into a vector
    % Subtract off the mean of each
    %****************************************
    psthval = [neuronmat zeros(ntrials,frameCount-size(neuronmat,2))];
    psthval = psthval(:,1:frameCount);
    psthval = reshape(psthval,1,ntrials*frameCount);
    psth_avg = mean(psthval);
    psthval_keep{count} = psthval - psth_avg;
    stim_avg = sum(outSpectrum*ntrials, 2)/(frameCount*ntrials);
    stimval_keep{count} = repmat((outSpectrum - repmat(stim_avg,1,frameCount)),1,ntrials);
    frameCount_keep(count) = frameCount;
    ntrials_keep(count) = ntrials;
    nband_keep(count) = nband;
    clear stimulus fs nbits stimsize stimduration binsize nFTfreqs increment frameCount
    clear neuron outSpectrum outfreqs nband neuronpsth neuronmean neuronmat ntrials
    clear psth_avg stim_avg psthval
end

%****************************************
% Concatonate the psth's and stimuli from all pairs
%****************************************
psthval = [];
stimval = [];
frameCount = sum(frameCount_keep);
ntrials = mean(ntrials_keep);
nband = mean(nband_keep);
for i = 1:count,
    psthval = [psthval psthval_keep{i}];
    stimval = [stimval stimval_keep{i}];
end
clear psthval_keep stimval_keep
    
%****************************************
% 4: Calculate the autocorrelation of the song spectrogram
% 5: Calculate the cross-correlation btween the song and spikes
% 6: Calculate the strf
%****************************************
[CStime,CSfreq] = strf_AC_anal2(timeLag,nband,frameCount,ntrials,stimval);
CSR = strf_XC_anal2(nband,stimval,psthval,timeLag,frameCount,ntrials);
strfH = strf_STRF_anal3(CSR,nt,nband,Tol_val,CStime,CSfreq);
strf = fliplr(strfH);

% %****************************************
% % Test on an unused data set
% %****************************************
% i = test;
% wave_dir = ['/Users/dms/Documents/matlab/WoolleyLab/stimuli/', wave_type{i}, '_in_order'];
% cd(wave_dir)
% [stimulus,fs,nbits] = wavread(stimuli{i});
% stimsize = length(stimulus);
% stimduration = round(length(stimulus)*1000/fs)+1; %duration of stim in msec
% binsize = floor(1/(2*pi*fwidth)*6*fs); %size of time bin over which fft is computed (in samples)
% nFTfreqs = binsize; %# of frequencies at which fft is computed
% increment = floor(fs/ampsamprate); %# samples to shift for each new bin start point
% frameCount = floor((stimsize-binsize)/increment)+1; %# of bins
% 
% adapting_dir = ['/Users/dms/Documents/matlab/WoolleyLab/data/old/adapting_cells/lghp1616.7_a/responses_to_', wave_type{i}];
% cd(adapting_dir)
% adapting_cell = dlmread(adapting_cells{i});
% neuron = adapting_cell;
% 
% %****************************************
% % 1: Process the stimulus
% %****************************************
% [outSpectrum outfreqs] = strf_stim_anal(stimulus,binsize,frameCount,...
%     increment,nFTfreqs,DBNOISE,fs,initialFreq,endFreq);
% nband = size(outSpectrum,1);
% 
% %****************************************
% % 2: Process the response
% % This stage is mainly for presentation of psth only
% %****************************************
% [neuronpsth neuronmean neuronmat ntrials] = strf_resp_simple(neuron,stimduration,psth_smooth,ftype,ampsamprate);
% 
% %****************************************
% % 3: Convolve the stimulus with the strf
% %****************************************
% prediction = convn(outSpectrum,strf,'same');
% prediction = sum(prediction);
% 
% dms = (prediction-min(prediction))/max(prediction-min(prediction));
% n2 = (neuronpsth-min(neuronpsth))/max(neuronpsth-min(neuronpsth));
% figure;plot(1-dms);hold on;plot(n2,'r--')
% title('strf_anal2 inverse');
% figure;plot(dms);hold on;plot(n2,'r--')
% title('strf_anal2');
% figure;imagesc(strf);colorbar
% cd '/Users/dms/Documents/matlab/PaninskiLab/strf/strf_replicate'
% save strf2 strf