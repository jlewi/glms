function [CStime,CSfreq] = strf_AC_anal(timeLag,nband,frameCount,ntrials,stimval)

%****************************************
% Calculate AutoCorrelation for each frequency band
% CStime = AutoCorrelation of Stimulus in time dimension
% CSfreq = AutoCorrelation of Stimlus is frequency dimension
%****************************************
twindow = [-timeLag timeLag];
corrtime = diff(twindow) + 1;
lengthVec = ones(1, frameCount*ntrials);

% CSfreq = ntrials.*stimval*stimval';
% CSfreq_ns = ntrials*frameCount;

CSfreq = stimval*stimval';
CSfreq_ns = ntrials*frameCount;

CStime = zeros(1,corrtime*2-1);
for j = 1:nband
%     CStime = CStime + xcorr(stimval(j,:),stimval(j, :),corrtime-1).*ntrials;
    CStime = CStime + xcorr(stimval(j,:),stimval(j, :),corrtime-1);
end

CStime_ns = nband*xcorr(lengthVec, lengthVec, corrtime-1);
% CStime_ns = ntrials*nband*xcorr(lengthVec, lengthVec, corrtime-1);

%****************************************
% Normalize the matrices
% Normalize CStime to its max; max(CStime) = mean(diag(CSfreq))
%****************************************
CStime=CStime./CStime_ns;
CSfreq=CSfreq./CSfreq_ns;
CStime=CStime./mean(diag(CSfreq));