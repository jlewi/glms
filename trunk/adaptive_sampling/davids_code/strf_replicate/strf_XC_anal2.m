function [CSR] = strf_XC_anal(nband,stimval,psthval,timeLag,frameCount,ntrials)

%****************************************
% Compute xcorr in each of the frequncy bands
%****************************************
twindow = [-timeLag timeLag];
for j = 1:nband
    CSR(j,:) = xcorr(stimval(j,:), psthval, twindow(2));
end

%****************************************
% Create normalization vector
% This is necessary because a convolution (of squares) creates a triangle...
% So divide by a triangle to normalize
% CSR finally is the normalized, spike-triggered spectrogram
%****************************************
%Calculate the normalization matrix
CSR_ns = xcorr(ones(1, ntrials*frameCount), ones(1, ntrials*frameCount), twindow(2));
CSR_ns(find(CSR_ns==0)) = 1;

CSR = CSR./repmat(CSR_ns,nband,1);