fprintf('\n\n setpathvars.m \n\n');

%3-28-2007
%script to set the path variables
%so we don't have to run through setting the actual path after doing a
%clear all
PARENT=pwd;
%we want want to add all the subdirectories in LIBPATH to our path
%use GENPATH to create the string
LIBPATH=fullfile(PARENT, 'lib');
%LIBPATHS={LIBPATH,fullfile(LIBPATH, 'spikehistory'), fullfile(LIBPATH, 'posterior'),fullfile(LIBPATH,'maxinfo')};
LIBPATHS=genpath(LIBPATH);
%remove hidden directories

LIBPATHS=regexprep(LIBPATHS,'[^:]*/\.[^:]*:',':');

SCRIPTS=fullfile(PARENT, 'scripts');
TESTING=fullfile(PARENT, 'testing');

%SCRIPTPATHS={SCRIPTS, fullfile(SCRIPTS,'tracking'),fullfile(SCRIPTS,'spikehistory'), fullfile(SCRIPTS,'gauss_approx'),fullfile(SCRIPTS,'daily'),fullfile(SCRIPTS,'maxinfo'),fullfile(SCRIPTS,'svd')};
SCRIPTPATHS=genpath(SCRIPTS);
SCRIPTPATHS=regexprep(SCRIPTPATHS,'[^:]*/\.[^:]*:',':');
GRAPHING=fullfile(PARENT,'graphing');
GRAPHPATHS=genpath(GRAPHING);

%remove all hidden directories;
GRAPHPATHS=regexprep(GRAPHPATHS,'[^:]*/\.[^:]*:',':');
%FIGURES=[PARENT '\figures'];
%Directory on BAYES
%RESULTSDIR=abspath(fullfile(PARENT,'..','..','allresults'));
%Directory on Java
%RESULTSDIR=abspath(fullfile(PARENT,'..','allresults'));

[s,hname]=system('hostname -s');

global RESULTSDIR;

hname=deblank(hname);

HOMEDIR='~';

%06-24-2008
%  I'm going to start phasing this out
%  My new version of initjobdata uses a global variable NL_JOBDIR

%if (strcmp(hname(1:4),'node'))
fprintf('Executing setpath vars \n');
j=getCurrentJob();

global TMPDIR;
TMPDIR='/tmp';
if ~isempty(j)
    %the taskstartup function should declare a global variable NL_JOBDIR
    %set RESULTSDIR equal to this directory
    fprintf('Setting RESULTSDIR to NL_JOBDIR \n');
    global NL_JOBDIR;
    fprintf('NL_JOBDIR=%s \n',NL_JOBDIR);
    RESULTSDIR=NL_JOBDIR;

else
    %RESULTSDIR=abspath(fullfile(PARENT,'..','allresults'));
    RESULTSDIR=abspath(fullfile(PARENT,'..','allresults'));
end
if ~exist(RESULTSDIR,'dir')
     % causes problems on cluster?  
  %       error('RESULTSDIR does not point to a valid path. You must set it to a path which exists.');
end

fprintf('RESULTSDIR=%s \n',RESULTSDIR);
