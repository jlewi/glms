#include <stdlib.h>
#include "stdio.h"
#include <string.h>
#include "dividework.h"
#include "debug.h"
#include <math.h>
//*****************************************************************************
//this function divdes up the number of neurons
//among the processes
//it returns an array of three integers
//    int[0] - the index of the starting neuron for this node to process
//    int[1] - the number of neurons for this node to process
//        
sneuronrng* divideneurons(int myrank, int numneurons, int nnodes){
  if (nnodes<1){
    emsg("dividenodes: nnodes must be >0 \n");
  }


  sneuronrng* rng=malloc(sizeof(sneuronrng));
  
  //determine the low and high number for the number
  //of neurons per node
  int minnum= (int)floor(((double)numneurons)/((double)nnodes));
  int maxnum= minnum+1;

  //figure out how many nodes have the maxnum
  int remainder=numneurons-minnum*nnodes;

  //therefore the first remainder neurons have maxnum
  //and rest have minnum
  if(myrank<remainder){
    rng->nstart=(myrank)*maxnum;
    rng->num=maxnum;
    rng->maxnum=maxnum;
    rng->minnum=minnum;
  }
  else{
    rng->nstart=remainder*maxnum+(myrank-remainder)*minnum;
    rng->num=minnum;
    rng->maxnum=maxnum;
    rng->minnum=minnum;
  }
  
  return rng;
}

char* rngtostring(sneuronrng* rng){
  char* msg=malloc(200);
  
  sprintf(msg,"neuronrng: nstart=%d \t num=%d \n",rng->nstart,rng->num);

  return msg;
}
