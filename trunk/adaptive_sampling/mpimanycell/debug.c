#include <stdio.h>
#include "debug.h"
#include <stdlib.h>
#include "matrixops.h"

int DEBUGOUT_INIT=0;
int MATLABMSG=0;

FILE* debugFP=NULL;
FILE* outFP=NULL;
FILE* outERR=NULL;

int outFPFile=FALSE;
int outERRFile=FALSE;
char* OUTFNAME=NULL;

void openfps(char* fname){
  
  if (fname !=NULL){
    //open the file
     outFP=fopen(fname,"a");
     outERR=outFP;
     OUTFNAME=fname;
     //make sure both files are set
     if (outFP !=NULL){
       outFPFile=TRUE;
     }
      if (outERR !=NULL){
       outERRFile=TRUE;
     }
  }

  if (outERRFile==FALSE){
    outERR=stderr;
  }
  if (outFPFile==FALSE){
    outFP=stdout;
  }  
}

void closefps(){
  if (outFPFile==TRUE){
    fclose(outFP);
  }
}
//open a file to which all output
//messages and errors can be written
void opendebugfile(int rank){
  debugFP=NULL;
  #ifdef DEBUG
  DEBUGOUT_INIT=1;
  char* fname=malloc(100);
  sprintf(fname,"outr%d.txt",rank);
  debugFP=fopen(fname,"w");
  free(fname);
  #endif

}


//open a file to which all output
//messages and errors can be written
void closedebugfile(){
  if (debugFP!=NULL){
    fclose(debugFP);
  }
  debugFP=NULL;
}
void flowmsg(char* msg){
  #ifdef FMSGS
  fprintf(outFP,"%s",msg);  
  if (debugFP!=NULL){
    fprintf(debugFP,"%s",msg);
  }
  #endif
}
void fmsg(char* msg){
  flowmsg(msg);

}

//newline should be included in whatever you send to the function
void errmsg(char* msg){
  #ifdef EMSGS
  fprintf(outERR,"\n Error: %s",msg);
  #endif

 if (debugFP!=NULL){
    fprintf(debugFP,"%s",msg);
  } 
 if (outFP!=NULL){
    fprintf(outFP,"%s",msg);
  }
}
void emsg(char* msg){
  errmsg(msg);

}

//test pointer is null 
//if it is print  error message regarding is null
int isnull(const void * ptr, char* ptrname, char* file, int line){
  if (ptr==NULL){
    char* msg=malloc(sizeof(char)*200);
    sprintf(msg,"Null Pointer %s in %s at line %d \n",ptrname,file,line);
    errmsg(msg);
    return TRUE;
  }
  return FALSE;
}
void emsgsl(char* msg,char* fname, int line){
  #ifdef EMSGS
  fprintf(outERR, "Error in %s at line %d \n", fname,line);
  
 if (debugFP!=NULL){
    fprintf(debugFP,"Error in %s at line %d \n",fname,line);
  }
 #endif
  errmsg(msg);
  /* #ifdef EMSGS
  fprintf(stderr, "\n", fname,line);
  
 if (debugFP!=NULL){
    fprintf(debugFP,"\n",fname,line);
  }
  #endif*/
  //print a blank line to indicate the end of the error msg
  errmsg("\n");


}
//print warning msgs
void warning(char* msg){
  #ifdef WARNINGS
  fprintf(outERR,"Warning: %s",msg);


  #endif
 if (debugFP!=NULL){
    fprintf(debugFP,"%s",msg);
  }
}
void wmsg(char* msg){
  warning(msg);
}

void wmsgfl(char* msg, char* file, int line){
  char* longmsg=malloc(200);
  sprintf(longmsg,"warning in %s at %d \n",file,line);
  wmsg(longmsg);
  wmsg(msg);

  free(longmsg);
}



//print matlab output
void matmsgbase(char* msg){
  #ifdef MMSGS
  //only print matlab msgs if its turned on
  if (MATLABMSG==TRUE){
  fprintf(outFP,"%s", msg);
  fprintf(outFP,"\n");
 if (debugFP!=NULL){
    fprintf(debugFP,"%s",msg);
  }
  }
  #endif
}

void matmsgfl(char* msg, char * fname, int line){
  #ifdef MMSGS
  char* lmsg=malloc(200);
  sprintf(lmsg,"Matlab output from: %s %d \n",fname,line);
  matmsgbase(lmsg);
  matmsgbase(msg);

  free(lmsg);
  #endif
}
void fmsgfl(char* msg, char* fname, int line){
  char* longmsg=malloc(200);
  sprintf(longmsg,"in %s at %d \n",fname,line);
  fmsg(longmsg);
  fmsg(msg);

  free(longmsg);
}
