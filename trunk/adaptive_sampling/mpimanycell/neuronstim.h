#ifndef NEURONSTIM_INC
#define NEURONSTIM_INC
#include "neuron.h"
#include "fullstim.h"
#include "stiminfo.h"

sMatrix* getneuronstim(sneuron* neuron, sFullStim* fullstim);
sstimInfo* createstiminfo(sneuron** neurons, int numneurons);
sFullStim* createfullstim(const sstiminfo* stiminfo, const srfield** rfields, sMatrix** stims, int n);
#endif
