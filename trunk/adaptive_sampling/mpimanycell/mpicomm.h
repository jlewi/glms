#include "mparam.h"
#include "matrixops.h"
#include <stdlib.h>
#include <stdio.h>
#include "simulation.h"
#include "matlabinter.h"
#include "mpiinfo.h"

#include "debug.h"

#ifndef MPICOMM_INC
#define MPICOMM_INC
#ifdef MPI
sMatrix* bcaststim(ssimparam* simparam, smparam* mparam, sMatrix* stim);
#endif
#endif
