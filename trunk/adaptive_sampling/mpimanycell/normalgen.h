#include "mkl_vsl.h"
#include <stdlib.h>
#include "debug.h"
#include <time.h>
#include "matrixops.h"
#include "matrixmath.h"

#ifndef NORMALGEN_INC
#define NORMALGEN_INC
#define snormgen snormalgen
typedef struct st_normalgen {
      VSLStreamStatePtr stream;
  
} snormalgen;

snormalgen* createnormalgen();
void closenormalgen(snormalgen* normalgen);
sMatrix* normrnd(snormalgen* gen, int length, double mmag);
#endif
