//******************************************************************************
//header file for all mpi communication 
// structures and functions
//******************************************************************************

//structure which will store information about a node in the array communicator grid
typedef struct mpi_commNode {
  int rank;				 	 //rank identifying the node this commNode refers to 
  int ndims;                 //number of dimensions
  MPI_Comm comm_grid;        //handle to grid communicator
  int* pos;

} commNode;