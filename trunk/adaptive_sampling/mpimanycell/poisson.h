#include "mkl_vsl.h"


#ifndef POISSON_INC
#define POISSON_INC
typedef struct st_poissgen {
      VSLStreamStatePtr stream;
  
} spoissgen;

spoissgen* createpoissongen();
void closepoissongen(spoissgen* pgen);
int poissrnd(const spoissgen* gen, double lambda);

#endif
