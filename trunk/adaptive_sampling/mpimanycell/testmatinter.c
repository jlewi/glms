#include "matlabinter.h"
#include "mparam.h"
#include <stdlib.h>
#include <stdio.h>

void testopensimfile(char* fname);
void testwritematfile(char* fname);
void twritempiinfo();
int main (int argc, char **argv){

  //defaults
  char* fname=(char*)malloc(200);
  char* test=(char*)malloc(200); //name of test to run
   
  int fileprovided=FALSE;


  //***************************************************************
  //process the command line arguments
  //*************************************************************
  int argind=1;
  
  while(argind <argc ){
    if (strcmp(argv[argind],"-mat")==0){
      fname=argv[argind+1];   
      fileprovided=TRUE;
    }
    else if (strcmp(argv[argind],"-test")==0){
      test=argv[argind+1];
    
    }
    argind=argind+2;  
  }

  //***********************************************************
  //call the appropriate test function
  //**********************************************************
  if (strcmp(test, "write")==0){
   testwritematfile("testout.mat");
  }
  if (strcmp(test, "mpiinfo")==0){
    twritempiinfo();
  }
  else{
     //was a file provided
    if (fileprovided==TRUE){
      testopensimfile(fname);
	     
    }
    else{
      //check if default file exists
      //do this by trying to read it
      FILE* fp;
      int defexists=FALSE;
      fp=fopen("simdata.mat","r");
      if (fp !=NULL){
	defexists=TRUE;
	fclose(fp);
      }
	   
      if (defexists==TRUE){
	testopensimfile("simdata.mat");
      }
      else{
	printf("No file specified and default doesn't exist \n");
      }
    }
	  
  }  
    //printf("Unkown test %s \n",test); 
	
 

  //close the debugging
  // closedebugfile();
}

void testopensimfile(char* fname){
  //test opening and saving of a simulation file
  void** vars;
  int*  nvars=malloc(sizeof(int));
  int* err=malloc(sizeof(int));
 
  vars=readsimparam(fname, nvars, err);

  if (vars[0]!=NULL){
    printf("read mparam from file \n");
    printmparam((smparam*)(vars[0]));
  }
}

void twritempiinfo(){
  smpiinfo* mpiinfo=malloc(sizeof(mpiinfo));
  mpiinfo->myrank=2;
  mpiinfo->nprocs=0;
  mpiinfo->nevents=0;

  mxArray* mxmpi=createmxmpiinfo(mpiinfo);

   int nvars=1;
  smxsave* saveneurons=malloc(sizeof(smxsave)*nvars);

  //saveneurons->data=mxneurons;
  //saveneurons->name="neurons";
  (saveneurons)->data=mxmpi;
  (saveneurons)->name="mpiinfo";
  
//mxArray* mxmpi=mxCreateDoubleScalar(10);*/
  writematfile("testout.mat",saveneurons,1);
 
  /*  const char** field_names=malloc(sizeof(char*)*1);
field_names[0]="test";
  mxArray*   mxmpi=mxCreateStructMatrix(1,1,1,field_names);
  mxArray* dtest=mxCreateDoubleScalar(10);
  mxSetField(mxmpi,0,field_names[0],dtest);
  MATFile *pmat = matOpen("testout.mat", "w");
     int status = matPutVariable(pmat, "iinfo",mxmpi);
  status = matPutVariable(pmat, "test",dtest);
    matClose(pmat);
  */
  printf("done creating file \n");
  mxFree(mxmpi);
}
void testwritematfile(char* fname){
  //create a sample gaussian to write
  double data[2]={1,2};
  int size=2;
  sMatrix* m=array2vector(data,size);

  sMatrix* c=identitymatrix(size);

  sgauss* post=createGauss(m,c);

  mxArray* mxpost=createmxgauss(&post,1);

  smxsave* vtosave=malloc(sizeof(smxsave));
  vtosave->data=mxpost;
  vtosave->name="post";

  writematfile(fname,vtosave,1);
 
}
