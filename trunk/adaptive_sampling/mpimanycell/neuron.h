#include "matrixops.h"
#include "probability.h"
#include "simulation.h"
//#include "fullstim.h"
//#include "stiminfo.h"


#ifndef NEURON_INC
#define NEURON_INC

//structure to store the receptive field of this neuron
//kstart tells which position of full stim this starts at
//klength tells how long it is
typedef struct st_rfield{
  int kstart;
  int klength;

 
} srfield;

typedef struct st_neuron {

  sMatrix* ktrue;    //true value of this neurons filter
  sGauss* prior;
  //first dimension is iteration
  sGauss** post;
 
  //matrix to store the responses
  int* resp;
  srfield* rfield;
  //
  //number of trials its expecting
  int ntrials;

} sneuron;

sneuron* copyneuron(sneuron* neuron);

void printneuron(sneuron* neuron);
sneuron* createneuron(int ntrials,sgauss* prior,sMatrix* ktrue, int kstart, int klength);
void  setneuronpost(sneuron* neuron, int trial, sgauss* post);
srfield* createrfield(int kstart, int klength);

sgauss*  getneuronpost(sneuron* neuron,int trial);
sMatrix* getktrue(sneuron* neuron);
int getneuronklength(sneuron* neuron);

sneuron* neuronfrommxstruct(mxArray* mxneurons,int nindex);

mxArray* mxstructfromneurons(sneuron** neurons, int num);

void setobsrv(sneuron* neuron, int trial, int nspikes);
void freerfield(srfield* rfield);
void freeneuron(sneuron* neuron);


#endif
