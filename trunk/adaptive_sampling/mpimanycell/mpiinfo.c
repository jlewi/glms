#include "mpiinfo.h"
#include <stdlib.h>
#include <sys/time.h>
#include "debug.h"
#include "matrixops.h"
#include <string.h>
#include <unistd.h>

#define BUFFSIZE 200
//create an MPI INFO structure
smpiinfo* creatempiinfo(){
  smpiinfo* mpiinfo=malloc(sizeof(smpiinfo));
  #ifdef MPI
  mpiinfo->comm=MPI_COMM_WORLD;
    MPI_Comm_rank(mpiinfo->comm, &(mpiinfo->myrank));
    MPI_Comm_size(MPI_COMM_WORLD, &(mpiinfo->nprocs));
  #else 
    mpiinfo->myrank=0;
    mpiinfo->nprocs=1;
  #endif

    //structure to hold event times
    mpiinfo->nevents=0;
    mpiinfo->maxnevents=20;
    mpiinfo->eventtimes=malloc(sizeof(double)*(mpiinfo->maxnevents));
    mpiinfo->eventnames=malloc(sizeof(char*)*(mpiinfo->maxnevents));
    mpiinfo->hostname=malloc(BUFFSIZE);
    gethostname(mpiinfo->hostname,BUFFSIZE);
    return mpiinfo;
}

int getrank(smpiinfo* mpiinfo){

  return mpiinfo->myrank;
}

int getnprocs(smpiinfo* mpiinfo){
  return mpiinfo->nprocs;
}

double recordtime(smpiinfo* mpiinfo,char* ename){
  if (isnullm(mpiinfo,"mpiinfo")==TRUE){
    emsg("can't record time\n");
    return 0;
  }
  mpiinfo->nevents=mpiinfo->nevents+1;
  if (mpiinfo->nevents>mpiinfo->maxnevents){
    emsgm("Exceeded mpiinfo->maxnevents. can't record time \n");
    return 0;
  }

  struct timeval etime;
  gettimeofday( &etime, NULL);
  mpiinfo->eventtimes[mpiinfo->nevents-1]=(double)etime.tv_sec*1000000+(double)etime.tv_usec;

  //copy the ename otherwise we can have trouble if we try to free it
  int flen=strlen(ename);
  //add 1 because of terminal character
  char *fname=malloc(flen+1);
  memcpy(fname,ename,flen+1);
  mpiinfo->eventnames[mpiinfo->nevents-1]=fname;

  //return the time 
  return  mpiinfo->eventtimes[mpiinfo->nevents-1];
}

void freempiinfo(smpiinfo* mpiinfo){
  for(int index=0;index<mpiinfo->nevents;index++){
    if (mpiinfo->eventnames[index]!=NULL){
      free(mpiinfo->eventnames[index]);
    }
  }
  free(mpiinfo->eventtimes);
  free(mpiinfo->eventnames);
  free(mpiinfo->hostname);
  free(mpiinfo);
}

#ifdef MATLAB

//****************************************************************
//this function creates an MxStructure array to represent
//a full stim object so that we can write it to matlab
//don't forget to delete by call to mxDestroyStructArray
mxArray* createmxmpiinfo(smpiinfo* mpiinfo){

  mxArray * mxmpiinfo=NULL;
  int ndim=2;
  int dims[2];
  dims[0]=1;
  dims[1]=1;
  //these are just the additional fields in addition to 
  //the event times
  int nfields=3+mpiinfo->nevents;
  

  int CBUFFSIZE=200;
  const char** field_names=malloc(sizeof(char*)*nfields);
  
  for(int findex=0;findex<mpiinfo->nevents;findex++){
    field_names[findex]=mpiinfo->eventnames[findex];
  }
  field_names[0]="mainloopstart";
  field_names[1]="mainloopend";
  field_names[mpiinfo->nevents]="rank";
  field_names[mpiinfo->nevents+1]="nprocs";
    field_names[mpiinfo->nevents+2]="host";
  //field_names[0]="test";

  mxmpiinfo=mxCreateStructMatrix(1,1,nfields,field_names);

  int mindex=0;   //indexes element in the array we are setting

  //mxmpiinfo=mxCreateStructArray(ndim,dims,1,field_names);
  //add the event times to the structure
  for (int findex=0;findex<mpiinfo->nevents;findex++){
    mxArray* etime=mxCreateDoubleScalar(mpiinfo->eventtimes[findex]);
    //mxArray* etime=mxCreateDoubleMatrix(1, 1,mxREAL);
    //double * ptr=mxGetPr(etime);
    //*ptr=mpiinfo->eventtimes[findex];
    if (isnullm(etime,"etime")==TRUE){
      emsgm("Couldn't create array to represent field \n");
    }
 
    mxSetField(mxmpiinfo, mindex,field_names[findex],etime);
    //mxSetFieldByNumber(mxmpiinfo, 1,findex,etime);
   #ifdef DEBUG   
    mxArray *newdata= mxGetField(mxmpiinfo, mindex,field_names[findex]);
    double *ptr=mxGetPr(newdata);
    #endif
   }

  //now ad the rank and nprocs fields
  mxArray* mxrank=mxCreateDoubleScalar(mpiinfo->myrank);
  mxSetField(mxmpiinfo, mindex,field_names[mpiinfo->nevents],mxrank);

  mxArray* mxnprocs=mxCreateDoubleScalar(mpiinfo->nprocs);
  mxSetField(mxmpiinfo, mindex,field_names[mpiinfo->nevents+1],mxnprocs);
  
  mxArray* mxhost=mxCreateString(mpiinfo->hostname);
  mxSetField(mxmpiinfo, mindex,field_names[mpiinfo->nevents+2],mxhost);
  

  //mxArray* dtest=mxCreateDoubleScalar(10);
  //mxSetField(mxmpiinfo,1,"test",dtest);
  //mxAssertS(int expr, char *error_message);
  //return mxmpiinfo;
  return mxmpiinfo;
}


#endif

