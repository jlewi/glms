#include <math.h>
#include "matrixops.h"
#include "matrixmath.h"
#include <stdio.h>
#include "mkl.h"
#include <stdlib.h>
#include "debug.h"

//matlab headers
#ifdef MATLAB
#include "mat.h"
#include "matrix.h"
#ifdef MATLABMEX
#include "mex.h"
#endif 
#include "engine.h"
#endif

//print matlab header file from redefining printf as mexprintf
#define printf printf
//************************************************
//Cblas interface
//************************************************

//compute the dot product of two vectors
//compute the dot product of two matrices
double dotprod(sMatrix* vec1, sMatrix* vec2){
  //error checking
  //make sure vec1 and vec2 are col vectors
  if (vec1->ndims>2 || vec2->ndims>2){
    printf("Error dotprod: matrices of dimension greater than 2 \n");
  }
 
  if (vec1->dims[1]>1 || vec2->dims[1]>1){
    printf("Error dotprod: inputs must be column vectors \n");
  }

  //make sure they have same number of rows
  if (vec1->dims[0]!=vec2->dims[0]){
     printf("Error dotprod: inputs must be column vectors \n");
  }

  //allocate storage for the result
  double result;

  //compute the dot product using the cblas function
  int incx=1;  //increment for x
  int incy=1;
  result=cblas_ddot(vec1->dims[0],vec1->data,incx,vec2->data,incy);

  return result;
}
//check if two matrices are equal
int matrixEqual(sMatrix* mat1, sMatrix* mat2){
	//error checking
	if (validmatrix(mat1)==FALSE){
		return FALSE;
	}
	if (validmatrix(mat2)==FALSE){
		return FALSE;	
	}
	
	//check the dimensions are equal
	if (mat1->ndims != mat2 ->ndims){
		emsg("matrixEqual: matrices must have same number of dimensions");	
		return FALSE;
	}
	
	//check all dimenions are equal
	//also compute total number of elements in matrix
	int numel=1;
	for (int index=0;index<mat1->ndims;index++){
		if (mat1->dims[index]!=mat2->dims[index]){
			printf("matrixEqual: matrices must have the same size. Dimension %d is not the same \n",index);
			return FALSE;	
		}	
		numel=numel* mat1->dims[index];
	}
	
	//*****************************************************************
	//check if the matrices are equal
	for (int index=0;index<numel;index++){
		if (mat1->data[index]!=mat2->data[index]){
			return FALSE;	
		}
	}
	//if we get here the two matrices are equal
	return TRUE;
	
}
//compute matrix difference
sMatrix* matrixDiff(sMatrix* mat1, sMatrix* mat2){
	//error checking
	validmatrix(mat1);
	validmatrix(mat2);
	
	//check the dimensions are equal
	if (mat1->ndims != mat2 ->ndims){
		emsg("matrixDiff: matrices must have same number of dimensions");	
		return NULL;
	}
	
	//check all dimenions are equal
	//also compute total number of elements in matrix
	int numel=1;
	for (int index=0;index<mat1->ndims;index++){
		if (mat1->dims[index]!=mat2->dims[index]){
			printf("matrixDiff: matrices must have the same size. Dimension %d is not the same \n",index);
			return NULL;	
		}	
		numel=numel* mat1->dims[index];
	}
	//create the matrix
	sMatrix* result=createMatrix(mat1->ndims,mat1->dims);
	
	//now we loop through the data and subtract the elements
	//we loop through the data linearly which ignores issue of whether 
	//data is row or column major and doesn't require the marix to be two dimensional
	for (int index=0;index<numel;index++){
		result->data[index]=mat1->data[index]-mat2->data[index];	
	}
			return result;
}

//********************************************************
//Matrix Product
//*******************************************************
sMatrix* matrixProd(sMatrix* mat1, sMatrix* mat2){
  //error checking
  if (mat1->dims[1]!=mat2->dims[0]){
    printf("Error: matrixprod matrices don't have compatible dimensions \n");
  }

  //specify whehter the matrices are transposed or not
  //when we take the product
  CBLAS_TRANSPOSE trans1=CblasNoTrans;
  CBLAS_TRANSPOSE trans2=CblasNoTrans;

  //speficy whether matrices are column
  //or row major. We use column major
  CBLAS_ORDER matorder=CblasColMajor;

  //alpha is a scalar constant to multiply matrix by
  double alpha=1;
  double beta=0;  //beta is a matrix to b

  //dimensions of the result
  int rdims[2];
  rdims[0]=mat1->dims[0];
  rdims[1]=mat2->dims[1];

  sMatrix* prod=createMatrix(2,rdims);

  //dgemm computes
  //alpha*op(a)*op(b)+beta*c
  //since beta is zero 
  //matrix c (prod) gets overwritten with result
  cblas_dgemm(matorder, trans1,trans2,mat1->dims[0],mat2->dims[1],mat1->dims[1],alpha,mat1->data,mat1->dims[0],mat2->data,mat2->dims[0],beta,prod->data,prod->dims[0]);
 
  return prod;
}


//***********************************************
//return the sign of a value
//*************************************************
// n >=0 - return 1
// n <0 - return -1

int sign(double n){
  if (n>=0){
    return 1;
  }
  else{
    return -1;
  }
}

//********************************************************
//function 
//**********************************************************
//computes the eigendecomposition of data
//      data - must be a symetric matrix
//           - will not be overwritten
//      evecs - matrix containing eigenvectors in ascending order
//            only space for the sMatrix pointer needs to be allocated
//             all other space is handled inside 
//      evals - VECTOR of eigenvalues in ascending order
//            -only space for the sMatrix pointer needs to be allocated
//             all other space is handled inside
//Return value:
//  int - -1 - error occurred

int eig(const sMatrix* data, sMatrix* evecs, sMatrix* eigd) {
  //matrix must be square
  int sdim=0;
  if (data->dims[0]!=data->dims[1]){
    printf("Error: eig - data matrix must be square \n");
  }
  else{
    sdim=data->dims[0];
}
 
 
  

  char job='V';        //we need eigenvalues and eigenvectors
  char uplo='L';      //matrix is symetric. Therefore
                      //only need to store upper or lower triangular part
                      //L says that matrix is lower triangular
                      //if a stores the full matrix you can choose which ever yo lik

                      
  
  int lda=sdim;
  //workspace array not sure what its for
  //just make it same size as data
  //theres a note to see the application notes to figure out 
  //the appropriate size
  //after the first run it will contain the minium value for lwork
  //and thats the value that should be used on subsequent runs
  int k= ceil(log(sdim)/log(2));
  int lwork=3*sdim*sdim+(5+2*k)*sdim+1; //length of array work
  
  
  double* work=(double*)(malloc(lwork*sizeof(double)));
  
  //a second workspace array 
  int liwork=5*sdim+3;
  //changed to type int from double* to int*
  int* iwork=(int*)(malloc(liwork*sizeof(int)));
  

  //output parameters
  //make sure we've allocated space for the eigenvalues
  eigd->ndims=2;
  eigd->dims=(int*)(malloc(eigd->ndims*sizeof(int)));
  eigd->dims[0]=sdim;
  eigd->dims[1]=1;

  eigd->data=(double *)(malloc(sdim*sizeof(double)));

  int info;           //indicates if execution is successful or not
                      //0 execution is successful.
  
  
  //call the function which reduces a to a bidrectional matrix
  //since tis is a Fortran function everything needs to be called by reference
  //I think we need to provid a reference to it
  //use the divide and conquer algorithm for finding eigenvalues
  //and eigenvectors
  void dsyevd();

  //copy the matrix so we don't overwrite the input
  //this matrix will end up storing the eigenvectors
  copyMatrixTo(evecs,data);
  dsyevd(&job,&uplo,&sdim,evecs->data,&lda,eigd->data,work,&lwork,iwork,&liwork,&info);			


 return info;
}


sMatrix* transpose(sMatrix* mat){
  sMatrix* newmat=NULL;
  //error checking make sure 
  //mat is two dimensional
  if (mat->ndims !=2){
    emsg("tranpose: only works for 2d matrices \n");
    return NULL;
}

  else{
    int newdims[2];
    newdims[0]=mat->dims[1];
    newdims[1]=mat->dims[0];
    double val=0;
    newmat=createMatrix(2,newdims);

    for(int orow=0;orow<mat->dims[0];orow++){
      for(int ocol=0;ocol<mat->dims[1];ocol++){
	val=getij(mat,orow,ocol);
	setij(newmat,ocol,orow,val);
      }
    }
    
  }
  
  return newmat;

}
