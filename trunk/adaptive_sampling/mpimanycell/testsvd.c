// Purpose of this script is to test calling
// the intel Mkl Fortran svd function
// from C.

#include "datastruct.h"
//#extern 
int main(){
  

  int nrows=2;
  int ncols=2;
  int lda=nrows;

  double data[2][2]={{1,0},{0,1}};

  //workspace array not sure what its for
  //just make it same size as data
  //theres a note to see the application notes to figure out 
  //the appropriate size
  //after the first run it will contain the minium value for lwork
  //and thats the value that should be used on subsequent runs
  double** work;
  int lwork=nrows;   //length of work matrix
  work=create_double_array(nrows,ncols);


  //output parameters
  double d[nrows];  //store diagnoal elements of B
  double e[nrows-1];  //stores off diagonal elements of B
  double tauq[nrows]; // contains details of Q
  double taup[nrows]; //contains details of P

  int info;           //indicates if execution is successful or not
                      //0 execution is successful.
  

  //call the function which reduces a to a bidrectional matrix
  //since tis is a Fortran function everything needs to be called by reference
  void dgebrd();
 dgebrd(&nrows,&ncols,data,&lda,&d,&e,tauq,taup,work,&lwork,&info);
}
