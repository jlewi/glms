#include <stdlib.h>
#include "matrixops.h"
#include "model.h"
#include "mparam.h"
#include "debug.h"
#include "datastruct.h"
#include "stiminfo.h"
#include "matrixmath.h"
#include "neuronstim.h"
#ifdef MATLAB
#include "mat.h"
#endif

//****************************************************************
//Contains structures and functions
//for our model
//******************************************************************
//smParam* createmParam(int klength, ssubStim** sregions, int nregions, double mmag, double twindow){
smParam* createmParam(sneuron** neurons, int nneurons,double mmag,double twindow){
  //validate the set of substimuli
  /*if (validsubstims(sregions,nregions,klength)==FALSE){
    emsgsl("createmparam: set of  substimuli is invalid \n",__FILE__,__LINE__);
    }*/

  if (mmag<=0){
    emsg("createmParam: mmag needs to be >0 \n");
  }
  smParam*  mParam= malloc(sizeof(smParam));
  //mParam->klength=klength;
  //mParam->ktrue=createVector(klength);
  mParam->mmag=mmag;

  if (isnullm(neurons,"neurons")==TRUE){
    emsgm("can't create mparam neurons is null \n");
    return NULL;
  }
  mParam->neurons=malloc(sizeof(sneuron*)*nneurons);
  mParam->numneurons=nneurons;
  //copy the neurons
  for (int nindex=0;nindex <mParam->numneurons;nindex++){
    mParam->neurons[nindex]=copyneuron(neurons[nindex]);
  }
  
  if (twindow <=0){

    emsgm("twindow can't be <=0");
    exit(FALSE);
  }
  else{
    mParam->twindow=twindow;
  }
  //create the stiminfo structure
  mParam->stiminfo=createstiminfo(neurons,nneurons);
  // wmsgm("Warning mparam stiminfo set to null \n");
  //wmsgm("need to create stiminfo from neurons \n");
  //mParam->stiminfo=createStimInfo(klength,sregions, nregions);

  /* if (nregions<1){
     emsg("creaemParam: number of regions should be at least 1 \n");
     }*/
  
  return mParam;
}
//*********************************************************************
//printmparam
//*********************************************************************
void printmparam(smParam* mparam){
  if (mparam==NULL){
    emsg("printmparam: tried to print null mparam structure \n");
  }
  
  char* msg=malloc(200);
  sprintf(msg, "mparam.mmag=%g \n",getmmag(mparam));
  fmsg(msg);
  
  sprintf(msg,"mparam.nneurons=%d \n", getnumneurons(mparam));
  fmsg(msg);

  int numneurons=getnumneurons(mparam);

  for (int nindex=0; nindex<numneurons;nindex++){
    sprintf(msg,"\n\nmparam.neuron \t %d \n", nindex);
    fmsg(msg);
    printneuron(getneuron(mparam,nindex));
  }

  free(msg);
}

int getnumneurons(smparam* mparam){
  if (isnullm(mparam,"mparam")==TRUE){
    return 0;
  }
  return mparam->numneurons;
}
sneuron* getneuron(smparam* mparam, int nindex){
    
  if (isnullm(mparam,"mparam")==TRUE){
    return NULL;
  }
  if ((nindex<0) || (nindex>=mparam->numneurons)){
    emsgm("Invalid neuron index given \n");
    return NULL;
  }

  return copyneuron(mparam->neurons[nindex]);
}
//********************************************************************
//get a constant reference to the stiminfo
//i.e object can't modify the stiminfo
//
const sstimInfo* conststiminforef(smParam* mparam){
  return mparam->stiminfo;
} 





//****************************************************************************
//

sFullStim* fullstimfromvec(smparam* mparam, smatrix* vec){
  sFullStim* fstim=(sFullStim*)malloc(sizeof(sFullStim));

  if (vec->dims[0]!= getmparamstimlength(mparam)){
    emsg("Error length of stimulus we're trying to create and model stimulus is not the same\n");
  }

  fstim->stim=copymatrix(vec);
  fstim->stiminfo=conststiminforef(mparam);
  return fstim;
}



//****************************************************************************
//sMatrix* constructFullStim(smParam* mparam, sOptRegion* shared,int nshared, sMatrix** xsopt,sOptRegion* unique, int nunique, sMatrix** xuopt)
//constructFullStim(smParam* mparam, ssubStim** subStim, sMatrix** x,int nsubstim)
//
//
//
//Explanation:
//      This constructs a full stimulus object from an array of subStim objects
//      which describe each region of the stimulus, and an array of vectors which specify
//      the value of the stimulus at these reigion
//
//     All data is copied to the new location in memory so that no chance for someone else
//     deleting memory it depends on
sFullStim* constructFullStim(smParam* mparam, ssubStim** subStims, sMatrix** x,int nsubstim){

  sFullStim* fstim=(sFullStim*)malloc(sizeof(sFullStim));
  fstim->stim=createVector(getmparamstimlength(mparam));
  fstim->nsubStims=nsubstim;
  fstim->stiminfo=conststiminforef(mparam);

  //set the substimuli part of the object
  fstim->subStims=(ssubStim**)malloc(sizeof(ssubStim*)*fstim->nsubStims);

  int isub=0;

#ifdef DEBUG
  //debugging check if we try to write multiple stimuli to the same
  //part of the full stimulus
  //do this by creating an array of integers
  //as long as the fullstimus and setting each entry to 1 
  //when it gets set
  int *stimwrote=malloc(sizeof(int)*getmparamstimlength(mparam));
  int length=getmparamstimlength(mparam);
  for (int index=0;index<length;index++){
    stimwrote[index]=FALSE;
  }
#endif

  //now loop over all regions
  //and set the appriporiate part of the stimulus
  for (isub=0;isub<nsubstim;isub++){
    //make a copy of the substim object
    fstim->subStims[isub]=copysubStim(subStims[isub]);
    setSubMatrix(fstim->stim,x[isub],fstim->subStims[isub]->kstart,0);

#ifdef DEBUG
    //check if we overwrote an entries of the stimulus
    int sindex=0;
    for (sindex=fstim->subStims[isub]->kstart; sindex<(fstim->subStims[isub]->kstart+x[isub]->dims[0]);sindex++){
      if (stimwrote[sindex]==TRUE){
	emsg("createfullstim: multiple substimuli both attempted to write \n");
	emsg("createfullstim: to same part of full stimulus\n");
      }
      else{
	stimwrote[sindex]=TRUE;
      }
    }
#endif
  }

#ifdef DEBUG
  free(stimwrote);
#endif
  return fstim;

}

//*********************************************************************
//mparamfrommxarray
//********************************************************************
#ifdef MATLAB
//create an mparam object from an mxArray structure stored in a mat file
smParam* mparamfrommxarray(mxArray* mxparam){
  //variables we need to load from mxparam
  sMatrix* ktrue;
  double mmag;
  double twindow;

  //ssubStim** sregions;
  //int nregions;
  
  int nneurons;
  sneuron** neurons;

  //fieldnames which must be provided
  enum FNAMES {f_neurons, f_mag, f_twindow};
  //create an array to keep track of whether field was provided
  int fexists[3];
  int NNEEDED=3;   //how many elements in FNAMES are there
  fexists[0]=FALSE;
  fexists[1]=FALSE;
  fexists[2]=FALSE;


  bool isstruct=mxIsStruct(mxparam);
  if (isstruct==0){
    emsg("mparamfrommxarray: mxparam is not a structure \n");
    return NULL;
  }

  int numfields=mxGetNumberOfFields(mxparam);

  //stor name of field
  const char* fname;
  //********************************************************************
  //loop through all the fields in the structure and proces them
  int findex;
  for (findex=0;findex<numfields;findex++){
    bool mxIsStruct(const mxArray *array_ptr);
    fname=mxGetFieldNameByNumber(mxparam, findex);

    if(strcmp(fname,"ktrue")==0){
      wmsgm("ktrue should no longer be given as field of mparam but as field of individual neurons \n");
      //fexists[f_ktrue]=TRUE;
      //mxArray* mxdata=mxGetField(mxparam,0,fname);
      //ktrue=mxArrayTosMatrix(mxdata);
      
    }

    if(strcmp(fname,"mmag")==0){
      fexists[f_mag]=TRUE;
      mxArray* mxdata=mxGetField(mxparam,0,fname);
      mmag=*((double *)(mxGetData(mxdata)));
    }

    if(strcmp(fname,"stimregions")==0){
      wmsgm("ktrue should no longer be given as field of mparam but as field of individual neurons \n");
      //stimregions should be a structure array
      //of substim elements
      //mxArray* mxstimreg=mxGetField(mxparam,0,fname);
      //isstruct=mxIsStruct(mxparam);
      
      //if (isstruct==0){
      //emsg("mparamfrommxarray: field stimregions  is not a structure array \n");	
      //}
      /*else{
      //determine the number of regions
      nregions=mxGetNumberOfElements(mxstimreg);

      sregions=malloc(sizeof(ssubstim*)*nregions);

      //loop through and create all the substimuli
      int sindex;
      for (sindex=0;sindex<nregions;sindex++){
      sregions[sindex]=substimfrommxstruct(mxstimreg,sindex);
      }

      fexists[f_sregions]=TRUE;
      }*/ //end else

    } //end if stimregions

    if(strcmp(fname,"twindow")==0){

      mxArray* mxdata=mxGetField(mxparam,0,fname);
      twindow=*((double *)(mxGetData(mxdata)));

      if (mxdata!=NULL){
	fexists[f_twindow]=TRUE;
	mxFree(mxdata);
	}
    }
    if(strcmp(fname,"neurons")==0){
      //neurons should be a structure array
      
      mxArray* mxneurons=mxGetField(mxparam,0,fname);
      int isstruct=mxIsStruct(mxneurons);
      
      if (isstruct==0){
	emsgm("mparamfrommxarray: field neurons  is not a structure array \n");	
      }
      else{
	//determine the number of neurons
	nneurons=mxGetNumberOfElements(mxneurons);

	neurons=malloc(sizeof(sneuron)*nneurons);

	//loop through and create all the substimuli
	for (int nindex=0;nindex<nneurons;nindex++){
	  neurons[nindex]=neuronfrommxstruct(mxneurons,nindex);
	}

	fexists[f_neurons]=TRUE;
      } //end else

    }//end if neurons

  }//end loop over all fields


  //check all fields were provided
  int isvalid=TRUE;

  //  fname=malloc(100);
  for (findex=0;findex<NNEEDED;findex++){
    
    switch (findex){
      /*    case f_ktrue:
	    fname="ktrue";
	    break;*/
    case f_mag:
      fname="mmag";
      break;   
    case f_neurons:
      fname="neurons";
      break;
      /* case f_sregions:
	 fname="stimregions";
	 break;*/
    case f_twindow:
      fname="twindow";
      break;
    }
    if (fexists[findex]!=TRUE){
      isvalid=FALSE;
      fprintf(stderr,"mparamfrommxarray: error mising field %s \n", fname);
    }
  }


  if (isvalid==TRUE){
    smParam* mparam=createmParam(neurons,nneurons,mmag,twindow);
    fmsg("Created mparam: \n");
    //printmparam(mparam);
    return mparam;
  }
  else{
    return NULL;
  }
  //free(fname);
}//end mparam from mxarray

#endif


int getmparamstimlength(smParam* mparam){
if (isnullm(mparam,"mparam")==TRUE){
return 0;
}
return mparam->stiminfo->klength;
}

double getmmag(smParam* mparam){

return mparam->mmag;
}

//**********************************************************
//Free an mparam structure
//**********************************************************
void freemParam(smParam* mparam){
if (mparam==NULL){
emsg("freemParam: tried to free null pointer \n");
return;
}

if (isnullm(mparam->neurons,"mparam->neurons")==TRUE){
emsg("can't free mparam->neurons. Null pointer \n");
}
else{
for (int nindex=0;nindex <mparam->numneurons;nindex++){
freeneuron(mparam->neurons[nindex]);
}
}
//freeMatrix(mparam->ktrue);
//freeStimInfo(mparam->stiminfo);;
free(mparam);
}



  
