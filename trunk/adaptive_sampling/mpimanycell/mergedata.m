function [trials]=mergedata(fname)
load(fname)
nprocs=trials00(1,1).nprocs;

%merge results from all different ranks
nfiles=size(trials00,1);
ntrials=size(trials00,2);



%trials(1:nfiles,1:ntrials).mpiinfo
for findex=1:nfiles
for tindex=1:ntrials
for proc=0:nprocs-1
%get name of proc to combine
vname=sprintf('trials%0.2d(%d,%d).mpiinfo',proc,findex,tindex);

%where to store it
vout=sprintf('trials(%d,%d).mpiinfo(%d)',findex,tindex,proc+1);
cmd=sprintf('%s=%s;',vout,vname);
fprintf('%s\n',cmd)
eval(cmd);
end
end
end


