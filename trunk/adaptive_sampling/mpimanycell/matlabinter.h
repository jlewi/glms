#include <stdio.h>
#include <string.h> /* For strcmp() */
#include <stdlib.h> /* For EXIT_FAILURE, EXIT_SUCCESS */

#ifndef MATLABINTER_INC
  #define MATLABINTER_INC
#ifdef MATLAB
#include "mat.h"

typedef struct st_smxsave{
  mxArray* data;      //variable to store matlab data in
  char* name;         //name of variable to save/load in matlab

} smxsave;
int getmxint(mxArray* data);
int writematfile(char* fname, smxsave*  vtosave, int ntosave); 
void** readsimparam(char* fname, int* nvars, int* err);
#endif
#endif
