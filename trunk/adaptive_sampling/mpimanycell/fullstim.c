#include "model.h"
#include <stdlib.h>
#include "matrixops.h"
#include "model.h"
#include "debug.h"
#include "datastruct.h"
#include "substim.h"
#include "fullstim.h"
#include "stiminfo.h"

#ifdef MATLAB
#include "mat.h"
#include "matlabinter.h"
#endif


//*************************************************************
//set the stimulus to the specified sMatrix	
void setfullstim(sFullStim* fullstim, sMatrix* stim){
  if (isnull(fullstim,"fullstim",__FILE__,__LINE__)){
    return;
  }
  if (!(iscolvector(stim))){
    emsg("setfullstim: stim must be a column vector \n");
    return;
  }
    
  if (getstimlength(fullstim)==stim->dims[0]){
    freeMatrix(fullstim->stim);
    fullstim->stim=copyMatrix(stim);
  }
  else{
    fullstim->stim=copyMatrix(stim);
  }
}

int getstimlength(sFullStim* fullstim){
   if (isnull(fullstim,"fullstim",__FILE__,__LINE__)){
    return 0;
  }
  
  if (isnull((void*)fullstim->stiminfo,"fullstim->stiminfo",__FILE__,__LINE__)){
    return 0;
  }
  return fullstim->stiminfo->klength;
}
//****************************************************************
//this function creates an MxStructure array to represent
//a full stim object so that we can write it to matlab
//don't forget to delete by call to mxDestroyStructArray
#ifdef MATLAB

mxArray* createmxfullstim(sFullStim** stim,int nstim){
  mxArray * mxfullstim=NULL;
  int ndim=1;
  int dims[1];
  dims[0]=nstim;
  int nfields=1;
  char **fieldnames;
  int CBUFFSIZE=200;
  const char** field_names=malloc(sizeof(char*)*nfields);
  field_names[0]="stim";
  mxfullstim=mxCreateStructArray(ndim,dims,nfields,field_names);

  //create an mxArray to represent the stimulus
  int stimindex=0;
  for (stimindex=0;stimindex<nstim;stimindex++){
    mxArray* stimvals=smatrixtomxarray(stim[stimindex]->stim);
   mxSetField(mxfullstim, stimindex,field_names[0], stimvals);
   }

  return mxfullstim;
}

#endif
