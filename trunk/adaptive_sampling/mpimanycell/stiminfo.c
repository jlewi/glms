#include <stdlib.h>
#include "matrixops.h"
#include "model.h"
#include "debug.h"
#include "datastruct.h"
#include "stiminfo.h"




void freestiminfo(sstimInfo* stiminfo){
  if (stiminfo==NULL){
    emsg("freestiminfo: tried to free null pointer");
  }

  //loop through all regions and free them
  for (int index=0;index<stiminfo->nregions;index++){
    freesubStim(stiminfo->substims[index]);
  }

  free(stiminfo);
}


