#include "model.h"
#include "matrixops.h"
#include "datastruct.h"

#include "substim.h"

//#ifndef STIMINFO_INC
#include "stiminfo.h"
//#endif

#ifndef FULLSTIM_INC
  #define FULLSTIM_INC
 
//forward declaration of stiminfo structure



typedef struct st_fullStim {
  sMatrix* stim;					//pointer to
							//the
  //mparam is responsible for maintaining
  //stiminfo //stimulus
  const sstiminfo * stiminfo;                   //this pointer can't be used to modify it
  ssubStim** subStims;			//partitioning of full
					//stimulus into a disjoint set
  //of shared and unshared regions for the different neurons
  int nsubStims;				//how many sub stimuli
						//are there.
} sFullStim;


//****************************************************************************
//
//
//

void freesubStim(ssubStim* substim);

//set the stimulus 
void setfullstim(sFullStim* fullstim, sMatrix* stim);

int getstimlength(sFullStim* fullstim);
#ifdef MATLAB
mxArray* createmxfullstim(sFullStim** stim,int nstim);
#endif
#endif
