#ifndef  MODEL_INC
#include "model.h"
#endif

#ifndef SUBSTIM_INC
#include "substim.h"
#endif 

#ifndef NEURON_INC
#include "neuron.h"
#endif

#ifndef STIMINFO_INC
#define STIMINFO_INC


#define freeStimInfo   freestiminfo

//structure which describes the stimulus 
// i.e how the stimulus is parsed into different regions
struct st_stiminfo{
  int klength;
  ssubStim** substims;
  int nregions;
  int nshared;        //int number of shared regions unique
  int nunique;        //int number of unique regions

  //the data in substims should be laid out with shared regions first
  //and then unique regions
  //the following to pointers point into substims to the locations where
  //the unique and shared regions start
  ssubStim** subshared; //
  ssubStim** subunique; 
};
typedef struct st_stiminfo sstimInfo;
typedef struct st_stiminfo  sstiminfo;




void freestiminfo(sstimInfo* stiminfo);
#endif
