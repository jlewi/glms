#include "simulation.h"
#include <stdio.h>
#include <string.h> /* For strcmp() */
#include <stdlib.h> /* For EXIT_FAILURE, EXIT_SUCCESS */
#include "matrixops.h"
#include "simulation.h"
#include "mparam.h"

#include "debug.h"
#define BUFSIZE 256

#ifdef MATLAB
#include "matlabinter.h"
#include "mat.h"
//vtosave - an array of vtosave variables
//ntosave - number of elements in vtosave


//this takes a pointer to an mxArray
//and returns an integer from the data
int getmxint(mxArray* data){
  if(isnullm(data,"data")==TRUE){
    return FALSE;
  }
  double v=*(mxGetPr(data));
  return ((int)v);
}

int writematfile(char* fname, smxsave*  vtosave, int ntosave) {
  MATFile *pmat;
  mxArray *pa1, *pa2, *pa3;

  char str[BUFSIZE];
  int status; 
  
  if (isnullm(fname,"fname")==TRUE){
    emsgm("No filename specified \n");
    return FALSE;
  }
  printf("Creating file %s...\n", fname);
  //this assumes the file exists since it won't create it.
  pmat = matOpen(fname, "u");
  if (pmat == NULL) {
    printf("writematfile: Error creating file %s\n", fname);
    emsg("(Do you have write permission in this directory?)\n");
    return(EXIT_FAILURE);
  }

  
  int varindex;
  for (varindex=0; varindex<ntosave; varindex++){
    if (vtosave[varindex].name==NULL){
      emsg("writematfile: name of variable is NULL \n");
      return(EXIT_FAILURE);
    }
    
     status = matPutVariable(pmat, vtosave[varindex].name, vtosave[varindex].data);
    if (status != 0) {
      printf("%s :  Error using matPutVariable on line %d\n", 
	     __FILE__, __LINE__);
      return(EXIT_FAILURE);
    } 


    }

  
 
 
  if (matClose(pmat) != 0) {
    printf("Error closing file %s\n",fname);
    return(EXIT_FAILURE);
  }
  fmsg("Done writing file.\n");
  return(EXIT_SUCCESS);
}


//****************************************************
// return
//     vars -void** this is an array of pointers
//          -you need to intialize the first dimension
//          -the second dimension gets initialized
//          -by calls to the appropriate function
void** readsimparam(char* fname, int* nvars, int* err){
  char* msg=malloc(200);
  int ERROR=FALSE; //indicates if error occured;

  isnullm(nvars,"nvars");
  isnullm(err,"err");

  (*nvars)=2;
  void **vars=malloc(sizeof(void*)*(*nvars));

  if (nvars==NULL){
    emsg("readsimparam - you did not allocte space for nvars");
    ERROR=TRUE;
  }
  //open the matlab file that contains the data
  printf("readsimparam: data file %s \n", fname);
  MATFile *mfile=matOpen(fname, "r");
  if (mfile == NULL) {
    printf("Error opening file %s\n", fname);
    return NULL;
  }

  //***************************************
  //load mparam
  //**************************************
  mxArray* mxmparam=NULL;
  mxmparam=matGetVariable(mfile, "mparam");	
  if (mxmparam==NULL){
    emsg("readstimparam: could not load trials from matlab file \n");   
    
  }
  mxAssert(mxmparam,msg);
  vars[0]=(void*)mparamfrommxarray(mxmparam);
  if (vars[0]==NULL){
    ERROR=TRUE;
    emsg("readstimparam: error occured could not load mparam from file \n");
  }//***************************************
  //load simmparam
  //**************************************
  mxArray* mxsimparam=NULL;
  mxsimparam=matGetVariable(mfile, "simparam");	
  if (mxsimparam==NULL){
    emsgm("could not get simparam from mat file \n");    
  }
  else{
  mxAssert(mxsimparam,msg);
  vars[1]=(void*)simparamfrommxarray(mxsimparam);
  }
  if (vars[1]==NULL){
    ERROR=TRUE;
    emsg("readstimparam: error occured could not load simmparam from file \n");
  }


  //**********************************************************************
  //Free the variables and close the file
  //close the file
  
  //free the variables
  if (mxmparam!=NULL){
  mxFree(mxmparam);
  }

  if (mxsimparam!=NULL){
    mxFree(mxsimparam);
  }
  if (matClose(mfile) != 0) {
    printf("Error closing file %s\n",fname);
    
  }

  return vars;
}
#endif
