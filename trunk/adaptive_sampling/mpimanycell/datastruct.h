#ifndef DATASTRUCT_INC
#define DATASTRUCT_INC
/*************************************************************************************************
	Arrays	
******************************************/    
float **create2d_farray(int,int);
double *create2d_darray(int nrows, int ncols);
void destroy_float_array(float **,int);

int* copyintarray(const int* data, int length);
void freeintarray(int* data);
#endif
