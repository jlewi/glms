#include "searchrange.h"
#include "mpiinfo.h"
#include "matrixops.h"
#include "poisson.h"
#include "normalgen.h"

#ifdef MATLAB
#include "mat.h"
#ifdef MATLABENG
#include "engine.h"
#endif
#endif
//******************************************************************************************
//Simulation structures:
//******************************************************************************************



#ifndef SIMULATION_INC
#define SIMULATION_INC
typedef struct st_simParam {
  //number of neurons should be retrieved
  //through getnumneurons(mparam)
  //int numneurons;			// number of neurons in the simulation
  int niter;				// number of iterations
  int trial;                           // keeps track of the current trial
  smpiinfo* mpiinfo;
  spoissgen* poissgen;                  //our poisson random number generator
  snormgen* normgen;


  int randstim;                          //indicates whether or not to use random stimuli
  #ifdef MATLABENG
  Engine *matlabeng;                   //pointer to matlab engine.
  #endif
} ssimParam,ssimparam;

const spoissgen* getpoissgen(ssimparam* simparam);
int gettrial(ssimparam* smparam);
ssimparam* initsim(int niter,int randstimuli);
void freesimparam(ssimparam* simparam);
ssimparam* simparamfrommxarray(mxArray* mxsim);
int getrandstimuli(ssimparam* simparam);

#ifdef MATLABENG
Engine* getmateng(ssimparam* simparam);
#endif
#endif

