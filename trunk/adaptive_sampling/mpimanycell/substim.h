#include "model.h"
#include "matrixops.h"
#include "datastruct.h"

#ifndef SUBSTIM_INC
  #define SUBSTIM_INC
  #define copysubstim  copysubStim
  #define createsubstim createsubStm
  #define freesubStim   freesubstim


//this stimulus could be spread across multiple neurons
//**************************************************************************

//structre subStim is used to correlate the different parts of the
//stimulus with different neurons.  it specifies which components of
//the stimulus it refers to and it specifies which neurons recieve
//that part of the stim as input.  it contains a pointer to the
//location in the full stim where this subpart begins but it also
//includes the start index in the full stim The latter is needed in
//the optimization step when we parse things up.
typedef struct st_subStim{
  int kstart;				//the index into the fullstim
					//where this sub stimulus
  //begins
  int klength;			//how long is this subcomponent
  	
  int* neurons;			//an array of integers identifying the
				//neurons
  //which have this subcomponent as part of their input
  int nneurons;			//length of neurons.
} ssubStim,ssubstim;

#ifdef MATLAB
ssubstim* substimfrommxstruct(mxArray* mxstimreg,int sindex);
#endif
//*********************************************************************
//copy a SubStim object
//******************************************************

ssubStim* copysubStim(const ssubStim* substim);
ssubStim* createsubStim(int kstart, int klength, int* neurons, int nneurons);

void freesubStim(ssubStim* substim);

int validsubstims(ssubstim** , int nsubstims, int klength);

#endif
