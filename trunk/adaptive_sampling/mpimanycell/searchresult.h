#include "matrixops.h"

#ifndef SEARCHRESULT_INC
#define SEARCHRESULT_INC
  //declare an array of structures to store results of these searches
  typedef struct st_SearchResult{
    //ysopt and yupot is an array of pointers
    //number of elements in the arrays
    int nysopt;
    int nyuopt;
    sMatrix** ysopt;
    sMatrix** yuopt;  //array of stimuli for each unique region
    double smag;
    double finfo;
    double sproj;
  } sSearchResult; 


//************************************************************************************
//sSearchResult* initSearchResult(int nshared, int nunique)
//      nshared - number of shared regions
//      nunique - number of unique regions
//Explanation: allocate space for the Search Result structure
//
sSearchResult* initSearchResult(int nshared, int nunique);
void freeSearchResult(sSearchResult* result);
#endif
