%function newton1dupdate(prior, stim,obsrv)
%       prior - serves as the prior which is the posterior from the last
%       iteration
%      stim  - stimulus
%      obsrv - number of observed spikes
%      twindow - observation window
function [m, c]=newton1dupdate(prior, stim,nspikes,twindow)
   tolerance=10^-10;
   maxdm=.1;
   obsrv.twindow=twindow;
   obsrv.n=nspikes;
   niter=0;
                    %post.m=pmethods.(mname).m(:,tr-1);
                    %post.c=pmethods.(mname).c{tr-1};
                    newtonerr=inf;
                  
                    %initialize the posterior to the prior
                    post=prior;
                    while (newtonerr >tolerance);
                        %observations are all obsrvations upto this point
                        %stimuli are all stimuli upto this point                       
                        [newpost]=post1stepgrad(prior,post,stim,obsrv,@glm1dexp,maxdm);
                        newtonerr=(newpost.m-post.m);
                        newtonerr=(newtonerr'*newtonerr)^.5;
                        

                        %if any of the entries are NaN 
                        %set it to the previous prior and exit
                        if ~(isempty(find(isnan(newpost.m)==1)))
                          newpost=post;
                          break;
                        else
                        post=newpost;
                        niter=niter+1;    
                        end
                    end

                    
                    m=newpost.m;
                    c=newpost.c;
