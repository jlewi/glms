%setpaths
PARENT=fullfile(pwd,'..');
MATLABPATH=fullfile(PARENT,'..','matlab');
LIBPATH=fullfile(PARENT, 'lib');
LIBPATHS={LIBPATH,fullfile(LIBPATH, 'spikehistory'), fullfile(LIBPATH, 'posterior'),fullfile(LIBPATH,'maxinfo')};
SCRIPTS=fullfile(PARENT, 'scripts');
TESTING=fullfile(PARENT, 'testing');
SCRIPTPATHS={SCRIPTS, fullfile(SCRIPTS,'spikehistory'), fullfile(SCRIPTS,'gauss_approx'),fullfile(SCRIPTS,'daily'),fullfile(SCRIPTS,'maxinfo'),fullfile(SCRIPTS,'svd')};
GRAPHING=[PARENT '\graphing'];
%FIGURES=[PARENT '\figures'];
RESULTSDIR=fullfile(PARENT, 'results');

    
paths={GRAPHING, TESTING, MATLABPATH};
paths=[paths SCRIPTPATHS, LIBPATHS];

cpaths=path;
for index=1:length(paths)
    r=strfind(cpaths,paths{index});
    if isempty(r)
        if (exist(paths{index},'dir'))
            addpath(paths{index});
        else
            sprintf('Warning %s directory does not exist',paths{index}); 
        end
    end
end

%add matlab paths
setmatlabpath
