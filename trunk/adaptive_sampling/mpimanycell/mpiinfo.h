#ifdef MPI
#include "mpi.h"
#endif
#include <sys/time.h>

#ifdef MATLAB
#include "mat.h"
#endif
#ifndef MPIINFO_INC
#define MPIINFO_INC
typedef struct st_smpiinfo {
  int nprocs;                           //number of mpi nodes
  int myrank;                           //rank of this node
  
  #ifdef MPI
  MPI_Comm comm;		//integer handle to grid communicator
  #endif


  //we will store the event times
  //by allocating an array to store them
  //along with field names
  double* eventtimes;
  int nevents;
  int maxnevents;
  char** eventnames;

  char* hostname;
} smpiinfo;

void freempiinfo(smpiinfo* mpiinfo);
     int getnprocs(smpiinfo* mpiinfo);
     int getrank(smpiinfo* mpiinfo);
     smpiinfo* creatempiinfo();

#ifdef MATLAB
mxArray* createmxmpiinfo(smpiinfo* mpiinfo);

#endif

double recordtime(smpiinfo* mpiinfo,char* ename);
#endif
