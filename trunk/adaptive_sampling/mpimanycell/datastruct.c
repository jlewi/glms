/* Code Written by Paul Royal.
Modified by Jeremy Lewi
*/
#include <stdlib.h>
#include "datastruct.h"
#include <stdio.h>
#include "debug.h"

//This creates a single array with proper dimensions for a 2d
//array
//The reason we do this is because we need a true multi-dimensional
//array that is the storage needs to be continuous
//if you do an array of pointers its not continguous
double* create2d_darray(int nrows, int ncols){
  double* array;
  int row_iterator;
  int column_iterator;
   

  array = malloc(nrows * ncols*sizeof(double));
  

  //initialize the array to the empty value
  for(row_iterator = 0; row_iterator < nrows; row_iterator++){
    for(column_iterator = 0; column_iterator < ncols; column_iterator++){
      array[row_iterator*ncols+column_iterator]=0;
    }
  }
  return array;
}

//Create a 2x2 array of floats
float **create2d_farray(int nrows, int ncols){
  float **array;
  int row_iterator;
  int column_iterator;
   

  array = malloc(nrows * sizeof(float *));


  //initialize the array to the empty value
  for(row_iterator = 0; row_iterator < nrows; row_iterator++){
    array[row_iterator] = malloc(ncols * sizeof(float));
    for(column_iterator = 0; column_iterator < ncols; column_iterator++){
      array[row_iterator][column_iterator] = 0;
    }
  }
  return array;
}


void destroy_float_array(float **array,int nrows){
  int row_iterator;
  
  for(row_iterator = 0; row_iterator < nrows; row_iterator++){
    free(array[row_iterator]);
  }

  free(array);
  array= NULL;
}


int* copyintarray(const int* data, int length){
  if (length<1){
    emsg("copyintarray: array can't have length less than 1");
    return NULL;
  }
  int* newcopy=malloc(sizeof(int)*length);
  int index;
  if (data==NULL){
    emsg("copyintarray: tried to copy null array \n");    
    //initialize it to zeros;
    for (index=0;index<length;index++){
      newcopy[index]=0;
    }
  }
else{
 for (index=0;index<length;index++){
      newcopy[index]=data[index];
    }
}
return newcopy;
 
}

void freeintarray(int* data){
  

  if (data==NULL){
    emsg("freeintarray: null array \n");    
}
else{
  free(data);
  }


 
}
