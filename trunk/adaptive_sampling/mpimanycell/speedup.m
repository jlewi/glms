%******************************************************************
%Make plot of the timing and the speedup
%
%clear all
%setpaths
%allfiles

ftiming.hf=figure;
ftiming.ylabel='Time/Iteration (sec)';
ftiming.xlabel='Number of Processes';
ftiming.hp=[];
ftiming.fname='timing';
ftiming.name='Timing';
ftiming.title='Running Time per Iteration';
ftiming.axisfontsize=15;
ftiming.lgndfontsize=15;
ftiming.lbls={};
ftiming.markersize=10;

fspeedup.hf=figure;
fspeedup.ylabel='Speedup';
fspeedup.xlabel='Number of Processes';
fspeedup.hp=[];
fspeedup.fname='speedup';
fspeedup.name='Speedup';
fspeedup.title='Speedup';
fspeedup.axisfontsize=15;
fspeedup.lgndfontsize=15;
fspeedup.lgnd={};
fspeedup.markersize=10;

%find the different numbers of neurons for which we have trials
nneurons=[trials(:).numneurons];
nneurons=sort(nneurons);
%now take difference
%where ever there is a number greater than zero
%corresponds to a unique count
dn=[1 diff(nneurons)];
dn=find(dn>0);
nneurons=nneurons(dn);

nexclude=[3 5 7 9 20 40 60 80];  %exclude these values for the number of neurons
pind=0;

%slopes of the fitted lines
slopes=zeros(length(nneurons),2);
for nindex=nneurons

    %check if we're excluding this number
    if isempty(find(nexclude==nindex))
   %get the indexes for all trials with this number of neurons
   tindexes=find([trials.numneurons]==nindex);
     
     pind=pind+1;
   %plot the mean and standard deviation of the timing for these trials  
  tpiter=[trials(tindexes).tpiter];
tpiterstd=[trials(tindexes).tpiterstd];
procs=[trials(tindexes).nprocs];

    [procs ind]=sort(procs);
    tpiter=tpiter(ind);
    tpiterstd=tpiterstd(ind);
    
    figure(ftiming.hf);
    hold on;
   ftiming.hp(pind)=errorbar(procs,tpiter,10*tpiterstd);
   [c,mark]=getptype(pind,1);
   set(ftiming.hp(pind),'Marker', mark);
    set(ftiming.hp(pind),'Color', c);
   set(ftiming.hp(pind),'LineStyle','none');
    ftiming.lbls{pind}=sprintf('%d', nindex);
    
    plot(procs,tpiter,getptype(pind,2));
    %*************************************************************
    %plot speedup
    %get the time of a single process
    [v mindex]=min(procs);
    t1proc=tpiter(mindex)*procs(mindex);
    
    sup=t1proc./tpiter;
    figure(fspeedup.hf);
    hold on;
    fspeedup.hp(pind)=plot(procs,sup,getptype(pind,1));
     fspeedup.lbls{pind}=sprintf('%d', nindex);
     set(fspeedup.hp(pind),'color','k');
     %fit a line 
     %only fit the part where processes is less than the number of neurons
     maxind=find(procs>nindex);
     if ~isempty(maxind)
         maxind=maxind(1)-1;
     else
         maxind=length(procs);
     end
     
     p=polyfit(procs(1:maxind),sup(1:maxind),1);
     m=p(1);
     b=p(2);
     hl=plot([1 procs(1:maxind)],[b+m m*procs(1:maxind)+b]);
     set(hl,'Color','k');
     
     fprintf('Numneurons =%d \t slope = %g \n',nindex,m);
     
    end
    
end

figure(ftiming.hf);
ylim([-10 150]);

lblgraph(ftiming);
lblgraph(fspeedup);


outfile=fullfile(pwd,'writeup','timing.eps');
exportfig(ftiming.hf,outfile,'bounds','tight','Color','bw');


outfile=fullfile(pwd,'writeup','speedup.eps');
exportfig(fspeedup.hf,outfile,'bounds','tight','Color','bw');

outfile=fullfile(pwd,'writeup','timing.eps');
exportfig(ftiming.hf,outfile,'bounds','tight','Color','bw');


outfile=fullfile(pwd,'writeup','speedup.eps');
exportfig(fspeedup.hf,outfile,'bounds','tight','Color','bw');
