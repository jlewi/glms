#include "debug.h"
#include "stdlib.h"

#include "searchrange.h"

const int NSTEPS=100;
sSearchRng* createsearchrng(double min, double max, double dw){
  //error checking
  if (min >max){
    emsg("createsearchrng: minimum must be less than max");

  }

  if (dw <=0){
    emsg("createsearchrng: dw must be > 0");
  }
  sSearchRng* rng=malloc(sizeof(ssearchrng));

  rng->min=min;
  rng->max=max;
  rng->dw=dw;

  
  return rng;
}

//*****************************************************************************
//Parse a range of values
sSearchRng* parserng(double min, double max, int myrank, int nnodes){
  if (nnodes<1){
    emsg("parserng: nnodes must be >0 \n");
  }

  if (max<min){
    emsg("parserng: max must be >= min \n");
    }
  sSearchRng* rng=malloc(sizeof(sSearchRng));
  
  double width= (max-min)/nnodes;
  
  //rank starts at 0 
  rng->min=min+(myrank)*width;
  rng->max=rng->min+width;
  rng->dw=(rng->max-rng->min)/NSTEPS;

  return rng;
}

//set dw based on the number of steps we want to take
void setinterval(sSearchRng* rng, int nsteps){
  if (rng==NULL){
    emsg("setinterval: null pointer rng \n");
    return;
  }

  double dw=(rng->max-rng->min)/nsteps;
  rng->dw=dw;
}


void freesearchrng(sSearchRng* rng){
  if (rng==NULL){
    emsg("freesearchrng: tried to free null pointer \n");
  }

  free(rng);
}
