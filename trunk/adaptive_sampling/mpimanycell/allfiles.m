%This gets the timing results from a whole bunch of files
%and stores relevant information in a structure.
clear all;

%directory where files are stored

dnames={fullfile(pwd,'data','batch4'),fullfile(pwd,'data','batch5'),fullfile(pwd,'data','batch6'),fullfile(pwd,'data','batch7'),fullfile(pwd,'data','batch8'),fullfile(pwd,'data','batch9'),fullfile(pwd,'data','batch10')};
%dnames={fullfile(pwd,'data','batch4'),fullfile(pwd,'data','batch5'),fullfile(pwd,'data','batch6'),fullfile(pwd,'data','batch7'),fullfile(pwd,'data','batch8'),fullfile(pwd,'data','batch9')};

matfiles={};


for dindex=1:length(dnames)
    %get list of matfiles in
    dircont=what(dnames{dindex});
    for findex=1:length(dircont.mat)
        matfiles=[matfiles {fullfile(dnames{dindex},dircont.mat{findex})}];
    end

end

tindex=1;
for findex=1:length(matfiles)
    %only process the file if its an output file
    isoutput=strfind(matfiles{findex},'simout');

    if ~isempty(isoutput)
        err=0;
        fprintf('Processing %s \n',matfiles{findex});
        try
            fdata=load(matfiles{findex});
            trials00=fdata.trials00;
        catch
            [lastmsg]=lasterr;
            fprintf('Couldnt load file \n');
            fprintf('%s \n',lastmsg);
            err=1;
        end

        if(err==0)
            for rindex=1:size(trials00,1)
                trials(tindex).fname=matfiles{findex};
                trials(tindex).numneurons=trials00(rindex,1).numneurons;
                trials(tindex).nprocs=trials00(rindex,1).nprocs;
                trials(tindex).klength=trials00(rindex,1).klength;
                trials(tindex).niter=trials00(rindex,1).niter;
                %compute the total avg running time
                mpiinfo=[trials00(rindex,:).mpiinfo];
                
                if ~isempty(mpiinfo)
                        
                stime=[mpiinfo(:).mainloopstart];
                endtime=[mpiinfo(:).mainloopend];
                trials(tindex).time=mean(endtime-stime)/10^6;
                %average running time per iteration
                trials(tindex).tpiter=trials(tindex).time/trials(tindex).niter;

                trials(tindex).tpiterstd=std((endtime-stime)/trials(tindex).niter/10^6);

                tindex=tindex+1;
                end
            end
        end
    end
end
