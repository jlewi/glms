%generate some test data for the ystim function
klength=2;
fname='./mpimanycell/matlab/data.mat';

mmag=1;
tindex=1;
post.m=(round(rand(klength,1)*15)+1)/10;

%**********************************************************************
%generate a random covariance matrix
%**********************************************************************
%eigenvalues for diffusion matrix
%the expected entropy 
nmaxe=10
eigd=[1:klength];

%eigenvectors
evecs=orth(rand(klength,klength));
post.c=evecs*diag(eigd)*evecs';

%perform the svd
%do this because we need to have eigenvectors and eigenvals in sorted
%order descending for yrescaled.m
%HOWEVER, my C function expects the eigenvalues
%to be sorted in descending value
[evecs eigd]=svd(post.c);
eigd=diag(eigd);
trials(tindex).post=post;
trials(tindex).evecs=evecs; 
%eigenvalues need to be sorted
trials(tindex).eigd=eigd; 
trials(tindex).klength=klength;
trials(tindex).c=post.c;
trials(tindex).m=post.m;
trials(tindex).w=[.01:.05:.099];
trials(tindex).emean=evecs'*post.m;
trials(tindex).mmag=mmag;
trials(tindex).yrescaled=zeros(klength,length(trials(tindex).w))

%compute the correct yrescaled
for (windex=1:length(trials(tindex).w))
    trials(tindex).yrescaled(:,windex)=yrescaled(trials(tindex).w(windex), eigd, trials(tindex).emean,mmag);
    trials(tindex).xrescaled(:,windex)=evecs'*trials(tindex).yrescaled(:,windex);
end

save(fname,'trials');
