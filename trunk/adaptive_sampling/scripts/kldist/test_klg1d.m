%2-09
%Modified 10-23-06
%
%Script to test that monte carlo sampling is working
%generate some sample data according to a gaussian distribution
%Therefore the relative entropy(KL distance) to our Gaussian approximation
% should always be zero
%

%The likelihood of the data is a gaussian with unknown mean and known
%variance

%True posterior: Gaussian
%Approximate posterior: Gaussian
%
%KL divergence between approximate and true posterior is zero so we can
%test our monte carlo computation of the divergence.
clear all;
setpaths;

%ksamps
ksamps=10000:40000:100000;   %how many monte carlo samples to use
%ksamps=10000;
%true parameters
%mean and variance of likelihood
param.mu=1;
param.c=1;

%prior
prior.mu=0;
prior.c=1;

klparam=[];
dk=zeros(1,length(ksamps));
vsum=zeros(1,length(ksamps));

%*************************************************
%generate the observations
ntrials=1;
obsrv=mvgausssamp(param.mu,param.c,ntrials);



%true posterior
truepost=[];

truepost.c=inv(inv(prior.c)+ntrials*inv(prior.c));
truepost.mu=inv(prior.c)*prior.mu;
for index=1:ntrials
    truepost.mu=truepost.mu+inv(prior.c)*obsrv(:,index);
end
truepost.mu=truepost.c*truepost.mu;

%our approximate posterior
%this is just our true posterior
approxpost=truepost;

%compute the kl distance

        %We are computing the KL distance of our approximation from the
        %true posterior. To do this, the KL divergence needs to consider
        %the likelihood of all observations.
        %So the llhood function is the batch update function
        %To compute the KL Divergence the KL divergence
        %needs to be able to compute the likelihood of the observation
        %under the different models
        %so we pass the information needed to compute the likelihood
        %in the structure ll
        
        ll.func=@loggaussmodel;
        %we need all the observations so far
        ll.obsrv=obsrv;    
        ll.obsrv=ll.obsrv';                        %needs to be a column vector
                                                   %different observation
                                                   %in each row
        ll.input=[];                        %we just need a place holder to be compatible with kl
        ll.param={param.c};              
        
        %function to compute prior likleihood of model
        po.func=@logmvgauss;
         
        po.param={prior.mu,prior.c};

        
        klparam.z=[];
        for kindex=length(ksamps):-1:1
            klparam.nsamp=ksamps(kindex);
            if (klparam.nsamp<size(klparam.z,2))
                klparam.z=klparam.z(1:klparam.nsamp);
            end
            
        [dk(kindex), p, vsum(kindex)]=dkl(approxpost,po,ll,klparam);
        klparam.z=p.z;
        end
        
        figure;
        fkl.hf=figure;
        fkl.xlabel='Number Samples';
        fkl.ylabel='KL Distance';
        %plot(ksamps,dk);
        errorbar(ksamps,dk,2*vsum.^.5);
        lblgraph(fkl);

        
        
        
%compute the true normalizing constant
%this is just marginal distribution of the data evaluated for the data
marginal.c=inv(inv(param.c)*truepost.c*inv(param.c));
marginal.mu=0;
truenormc=logmvgauss(obsrv,marginal.mu,marginal.c)




return;
figure;
hold on
for sindex=1:length(nsamps)
    plot3(nsamps(sindex)*ones(1,length(iter{sindex})),iter{sindex},dk{sindex},'.');
    xlabel('Num Samples');
    ylabel('Iteration');
    zlabel('Dk');
end



%not finished
%compute the KL divergence by brute force
brute.m=linspace(-10,10,10000);
brute.dm=brute.m(2)-brute.m(1);
brute.lpo=logmvgauss(brute.m,prior.mu,prior.c);
brute.ll=logmvgauss(obsrv,brute.m,param.c);

