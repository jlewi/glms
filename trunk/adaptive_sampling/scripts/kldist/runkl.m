%3/13/2007
%modified for my object oriented model
%10-25-06
%script to generate the data needed to compute the KL distance

%outfile=fullfile(RESULTSDIR,'testing','10_25','10_25_kldata_001.mat');
%simfile=fullfile(RESULTSDIR,'testing','10_25','10_25_test_001.mat');

%outfile=fullfile(RESULTSDIR,'testing','10_27','10_27_kldata_003.mat');
%outfile=fullfile(RESULTSDIR,'aistat','10_15','10_15_kldata_013.mat');
%outfile=fullfile(RESULTSDIR,'nips','11_15','11_15_kldata_001.mat');
%outfile=fullfile(RESULTSDIR,'nips','11_15','11_15_kldata.mat');
%outfile=fullfile(RESULTSDIR,'poissexp','03_13','03_13_kldata_002.mat');
outfile=fullfile(RESULTSDIR,'poissexp','04_19','kldata_04_19_001.mat');
simvar='simmax';
%load the results
%compute the kl distance
muse=[0];     %set to 0 use all samples
%trials=[100 500:500:2000];
trials=[100 500 1000 1500 2000];
dk=zeros(length(trials),length(muse));
vsum=zeros(length(trials),length(muse));
for tind=1:length(trials)
    trial=trials(tind);    
    vname=sprintf('dkdata%s%d',simvar,trial);
    vars=whos('-file',outfile);
    if isempty(strmatch(vname,{vars.name},'exact'))
        warning(sprintf('File did not contain variable %s \n'),vname)
        vname=sprintf('dkdata%s%d',simvar,trial);
        warning(sprintf('Checking for %s \n',vname));
    end
    if isempty(strmatch(vname,{vars.name},'exact'))
        warning(sprintf('File did not contain variable %s \n'),vname)

    else
        load(outfile,vname);
        eval(sprintf('data=%s',vname));
        for k=1:length(muse)
            %the lpost is sorted so if we use less than all the samples we need to
            %randomly select them
            if (muse(k)<length(data.lpost) && muse(k)>0)
                rind=ceil(rand(1,muse(k))*length(data.lpost));
                warning('Im not sure my shuffling is correct');
                [dk(tind,k),vsum(tind,k)]=dkcompute(data.lpost(rind),data.lpapprox(rind),data.gpost);
            else
                [dk(tind,k),vsum(tind,k)]=dkcompute(data.lpost,data.lpapprox,data.gpost);
                muse(k)=length(data.lpost);
            end
        end
    end
end

fkl.hf=figure;
fkl.xlabel='Trials';
fkl.ylabel='KL Distance';
fkl.hp=[];
fkl.lbls={};
fkl.markersize=10;
hold on;
for uind=1:length(muse)
    %plot(ksamps,dk);
    %[c,m]=getptype(1,1)
    fkl.hp(uind)=errorbar(trials,dk(:,uind),2*vsum(:,uind).^.5);
    [c,mark]=getptype(uind,1);
    set(fkl.hp(uind),'Color', c);
    set(fkl.hp(uind),'Marker', mark);
    set(fkl.hp(uind),'LineStyle','-');
    set(fkl.hp(uind),'MarkerSize',fkl.markersize);
    set(fkl.hp(uind),'LineWidth',2);
    fkl.lbls{uind}=sprintf('M.C Samps=%d',muse(uind));
end
legend(fkl.lbls)
%lblgraph(fkl);
set(gca,'xscale','log');
set(fkl.hf,'name','kldistance');

return

%******************************************************************
%make some histograms
fhist.hf=figure;
trials=[100 1000 2000];
bwidth=.05;         %use uniform bin width for all plots so we can easily compare them
xwidth=8;         %want to make width of graphs the same to make it easier to compare
figure;

for tind=1:length(trials)
    trial=trials(tind);
    vname=sprintf('dkdata%s%d',simmeth,trial);
    if ~exist(vname,'var')
        load(outfile,vname);
    end
    eval(sprintf('data=%s',vname));
    %    for k=1:length(muse)
    subplot(length(trials),1,tind);
    %we want to make binwidth unfirom so we specify the edges to use
    d=data.lpost-data.lpost(1)-data.lpapprox;
    mind=min(d);
    maxd=max(d);
    nbins=ceil((maxd-mind)/bwidth);
    bedges=(mind):bwidth:(mind+bwidth*nbins);
    %histc(d,bedges);
    [counts bcenters]=hist(d,bedges+bwidth/2);
    title(sprintf('Trials %d',trials(tind)));
    %  xl=xlim;
    %    dw=(xwidth-(xl(2)-xl(1)))/2;
    %center the limits
    [mc mind]=max(counts);
    %    xlim([xl(1)-dw xl(2)+dw]);
    xlim(bcenters(mind)+[-xwidth/2 xwidth/2]);
    %  end
end