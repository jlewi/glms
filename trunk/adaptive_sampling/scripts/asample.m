%8-08-2005
%
%script to test the one dimensional example of adaptive sampling given in
%[Paninski05]
%It is important to always sample xopt and theta at a constant resolution
%otherwise you will get normalizaiton and other errors because of the
%continuous to discrete approximation issue
clear all;

%************************************************************************
%usegradd = 1 means use gradient descent to find the maximum of
%I(y;theta|x). Otherwise compute I(y;theta|x) and then find all local
%maxima
usegradd=1;

%*************************************************************************
%set up figures
%*************************************************************************
h_f=figure;                     %figure to plot mutual information after each trial
num_figs=5;
h_i=subplot(num_figs,1,1);             %mutual info
h_ptheta=subplot(num_figs,1,2);        %h_ptheta
h_xopt=subplot(num_figs,1,3);          
h_y=subplot(num_figs,1,4);
h_entropy=subplot(num_figs,1,5);

%*************************************************************************
%Probability Distributions
%*************************************************************************
%probability of p(y=1|x,theta) is a gaussian
sigma=.1;
truetheta=.25;           %true theta.
binsize=.01;            %sampling interval
maxp=.75;
pyxt=@(y,x,theta)llhood(y,x,theta,sigma,binsize,maxp);

%limits on theta
tmin=0;
tmax=1;
tinterval=binsize;      %resolution for sampling the interval
theta=tmin:tinterval:tmax;
thetalength=length(theta);

%**************************************************************************
%convergence
%**************************************************************************
%Two convergence criteria
%   1. Estimate of theta does not vary over some number of previous trials
%   2. ptheta has a well defined peak
%       -measured using confidence interval surrounding max value
%
%Convergence Criterion 1:
%  Look at guess of theta over past trials (# of trials specified by
%  ntconverge). What the value to be unchanged
ntconverge=100;
%Convergence Criterion 2:
%convergence is measured when thetabest=argmax ptheta is s.t
% p(thetabest - alpha < theta <thetabest+alpha)>=pconverge;
alpha=.01;
alphaind=ceil(alpha/tinterval); %convert alpha to number of corresponding indexes
pconverge=.99;

%**************************************************************************




%************************************************************************
%Gradient descent parameters
%************************************************************************
%for when gradient descent is used to find max I(y;Theta |x)
%should use a more sophisticated convergence criteria
%rather than fixed number of iterations
numiter=100;            %number of iterations for gradient descent
dstep=1;                %step size for gradient descent

%**************************************************************************
%Initialization
%**************************************************************************
%store the product p(y1|x1,theta)*....p(yn-1|xn-1,theta)
%b\c we need it to recompute the distribution ptheta.
likelihood=ones(1,thetalength);

%distribution of theta after each iteration. (1 row per iteration);
numtrials=100;
entropy=zeros(1,numtrials);         %store entropy of p(theta)

ptheta=zeros(numtrials,thetalength);
ptheta(1,:)=ones(1,thetalength)*1/thetalength;

entropy(1)=sum(-ptheta(1,:).*log(ptheta(1,:)));

%data to store the data from each trial
%x - the point at which we sample
%y - the observation generated for that location
%theta - the best guess of theta after collecting this sample
%        this is used to look for convergence
data=struct('y',0,'x',0,'theta',0);
xopt=.25;            %initial guess

%**************************************************************************
% Main Loop = Repeated Sampling
%**************************************************************************
%variable signifying whether or not to keep iterating
cont=1
trial=1;    %we will start with trial 2

%trial 1 represents the prior case used to store prior
while cont==1
    trial=trial+1;
    fprintf('Trial %d \n',trial);
    
    %Check:
    %Compute I(y;theta|x) for a bunch of x.
    x=[0:binsize:1];         %values of x for which will compute I(y;theta|x)
    p1xt=pyxt(1,x,theta);
    p0xt=1-p1xt;
    
    pthetamat=ones(length(x),1)*ptheta(trial-1,:);
    
    
    intnorm1=sum(pthetamat.*p1xt,2); %gives col vector
    intnorm1=intnorm1*ones(1,length(theta));
    
   i1=pthetamat.*p1xt.*log(p1xt./intnorm1);
   i1=sum(i1,2);                            %mutual information I(y=1,theta|x)
   
   intnorm0=sum(pthetamat.*p0xt,2); %gives col vector
   intnorm0=intnorm0*ones(1,length(theta));
   
   i2=pthetamat.*p0xt.*log(p0xt./intnorm0);
   i2=sum(i2,2);
   
   %I(y;theta|x) as function of x
   icond=i1+i2;
   
   figure(h_f);
   axes(h_i)
   plot(x',icond);
   xlabel('x');
   ylabel('I(y;\theta|x)');
   %fprintf('I(y;theta|x)=%d \n', icond');
   
   %****************************************************************88
   %Find optimal x which maximizes the mutual information
   %*******************************************************************
   %determine whether we are using gradient descent or not 
   if (usegradd==1)
    %randomly generate xopt which will serve as seed for gradient ascent
    %otherwise we could get stuck in local minimum
    xinit=rand;
    xopt=xinit*(tmax-tmin)+tmin;

    
    for index=1:numiter
    %compute vectors to represent the following quantities over which
    %integral gets performed
    p1xt=pyxt(1,xopt,theta);
    
    
    %compute the integral
    intdp=sum(ptheta(trial-1,:).*(xopt-theta)/sigma.*p1xt);
    intpn=sum(ptheta(trial-1,:).*p1xt);
    
    %compute the gradient. 
    difflogs=log((1-p1xt)/(1-intpn))- log(p1xt/intpn);
    diffp=(-(1-p1xt)/(1-intpn)+p1xt/intpn);
    
    di=sum(ptheta(trial-1,:).*(((xopt-theta)/sigma).*p1xt.*difflogs + intdp*diffp));
    %we do gradient ascent b\c we want to find x which maximizes the
    %information
    xopt=xopt+di*dstep;
    
    
    end
    
   else
    %getting xopt by maximizing mutual information
    %uncomment if want to use this instead of gradient descent
    %get local maximimum
    [lmin,lmax]=lminmax(icond,0);
    %choose one of the maximum randomly
    r=rand;
    %want to generate a random integer between 1 and length(lmax)
    ind=ceil(rand*length(lmax));
    xopt=theta(lmax(ind));
   end
   
    %sample the data using xopt
    data(trial).x=xopt;
    rndnum=rand(1);
    if (rndnum <= pyxt(1,xopt,truetheta));
        data(trial).y=1;
    else
        data(trial).y=0;
    end
    
    %recomptue the posterior distribution of the model
    likelihood=likelihood.*pyxt(data(trial).y,xopt,theta);
    ptheta(trial,:)=ptheta(trial-1,:).*likelihood;
    %make sure all entries are at least eps
    %to avoid numerical errors when we compute the log
    ptheta(trial,:)=ptheta(trial,:)/sum(ptheta(trial,:))+eps;   %normalize it

    
    entropy(trial)=sum(-ptheta(trial,:).*log2(ptheta(trial,:)));
   figure(h_f);
   axes(h_ptheta)
   plot(theta,ptheta(trial,:));
   xlabel('theta');
   ylabel('ptheta');
   %fprintf('I(y;theta|x)=%d \n', icond');
   
   axes(h_xopt);
   plot(1:trial,[data.x]);
   xlabel('Trial');
   ylabel('xopt');
   
   axes(h_y);
   plot(1:trial,[data.y]);
   xlabel('trial');
   ylabel('y');
   
   axes(h_entropy);
   plot(1:trial,entropy(1:trial));
   xlabel('trial');
   ylabel('entropy');
   
   %test for convergence
   %get the theta which maximizes ptheta
   %consider this our best guess
   %[lmin,lmax]=lminmax(ptheta(trial,:));
   [pthetabest,indbest]=max(ptheta(trial,:));
   data(trial).theta=theta(indbest);
   
   %compute the probability that it lies in the specified interval
   %note ptheta sumes to one so we dont' need to multiply by the binwidth
   %we need to compute the indexes for the corresponding interval
   lind=indbest-alphaind;
   if (lind<1)
       lind=1;
   end
   
   uind=indbest+alphaind;
   if (uind>length(theta))
       uind=length(theta);
   end
   
   p=sum(ptheta(trial,lind:uind));

   
   
   %keyboard
   pause(.05);
   
    %should we continue looping
    %Convergence Criterion 1
    %check if past ntconverge trials give same theta
    if (trial>ntconverge)
    guesses=[data(trial-ntconverge+1:trial).theta];
    if (sum(guesses-guesses(end))==0)
        %Criterion 1 met
        %Check Criterion 2
        if (p>=pconverge);
            fprintf('Convergence! \n');
            fprintf('ThetaBest = %d \n',data(trial).theta);
            cont=0;
        end
        
    end
    end
    if ((trial+1)>numtrials)
        cont=0;
        fprintf('Max Iterations reached! \n');
    end
end