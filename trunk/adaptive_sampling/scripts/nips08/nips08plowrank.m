%4-19-2007
%
% Create an image with three rows and several columns
% each row shows plots of the MAP estimate of a 2-d receptive field for a
%       different method
% We will have 3 rows corresponding to
%   1. infomax - under power constraint
%   2. infomax - using pool based stimuli
%   3. iid
%
% Last column displays the true receptive field

clear variables;

%new data set
dsets=[];
%co08separable;
%data for some post cosyne experiments
%setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080311','dataset_lowrank_080311_001.m');
%setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080605','lowrankbs_dataset_080605_009.m');
setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080606','nips08_bslowd_converted.m');
%setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080606','nips08_bsmod.m');
xscript(setupfile);
%***************************************************
%parameters for the graph
%***************************************************
%gparam.trials= [100 500 1000 1500 2000 2200];
gparam.trials=[500 1000 1500 2000 2500];

gparam.trials=[250 500 750 1000];

%dsets=dsets([3 7 8]);
%dsets=dsets(3);
hold on;

%how columns and rows of axes we will need
gparam.ncols=length(gparam.trials)+1;
gparam.nrows=length(dsets);
%gparam.clim=10^-6*[-5 5];       %limits for color mapping


%create a data structure to store the data from all the simulations
data=struct('m',[],'lbl',[],'theta',[]);
data=repmat(data,1,length(dsets));

%**************************************************************************
%Make plots of mean
%************************************************************************
%loop over the data sets
clear simobj;
clear simall;
%run this loop in parallel using parfor
%matlabpool('open','conf',2);
%%
for dind = 1:length(dsets)
   
        %[simobj]=SimulationBase('fname',getpath(dsets(dind).fname),'simvar',dsets(dind).simvar);
        v=load(getpath(dsets(dind).fname));
        simobj=v.simobj;
        
    extra=getextra(simobj);

    height=extra.mdim(1);
    width=extra.mdim(2);

    %allocate space to save the results
    data(dind).m=cell(1,length(gparam.trials));
    data(dind).trial=zeros(1,length(gparam.trials));
    for tind=1:length(gparam.trials)

        trial=gparam.trials(tind);

        if isinf(trial)
            trial=getniter(simobj);
        end
        %check mean exists
        if (trial>getniter(simobj))
            fprintf('Data set %s: trial %d exceeds number of iterations \n;',getlabel(simobj),trial);
            break;   %stop plotting for this dataset
        else
            data(dind).m{tind}=reshape(getm(simobj.allpost,trial),height,width);
        end
        data(dind).trial(tind)=trial;
        %turn off tick marks
    end %end loop over trials

    %the true parameter
    data(dind).theta=reshape(gettheta(getobserver(simobj)),height,width);
    
    %keep track of all simulations
%    simall(dind)=simobj;
end


%************************************************************************
%Plot the results
%************************************************************************
%%
height=height;
width=width;
flowrank=FigObj('name','Gabor means','width',5.4,'height',3,'naxes',[length(dsets) length(gparam.trials)+1],'fontsize',10);

%dorder is the order in which we want to plot the graphs
%dorder=[dnames.infomax dnames.infomaxfull dnames.rand];
dorder=[1:length(dsets)]
row=0;

%gparam.clim=10^-6*[-5 5];       %limits for color mapping
gparam.clim=[min(data(1).theta(:)) max(data(1).theta(:))];
gparam.clim=[-2.7 5.6];
%time and frequency to go with the plots
%time (ms)
%frequency in Khz
t=[-size(data(1).m{1},2)-1:0];
f=(250+129/2):129:8000-129/2;
f=f(1:2:end);
f=f/1000;

for dind=dorder
    row=row+1;
    for tind=1:length(gparam.trials)
        if ~isempty(data(dind).m{tind})
            setfocus(flowrank.a,row,tind);
            imagesc(t,f,data(dind).m{tind},gparam.clim);
            set(gca,'xlim',[t(1) t(end)],'ylim',[f(1) f(end)]);
            set(gca,'xtick',[]);
            set(gca,'ytick',[]);

            %set(gca,'ydir','reverse');
        end

        %title
       % flowrank.a(dind,tind)=title(flowrank.a(dind,tind),sprintf('t=%d',data(dind).trial(tind)));

    end

    %plot the true parameter
    setfocus(flowrank.a,row,length(gparam.trials)+1);
    imagesc(t,f,data(dind).theta,gparam.clim);
    set(gca,'xlim',[t(1) t(end)],'ylim',[f(1) f(end)]);
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
%    set(gca,'ydir','reverse');
    %add a ylabel to each row

    flowrank.a(row,1)=ylabel(flowrank.a(row,1),dsets(dind).lbl);
    hylbl=getylabel(flowrank.a(row,1));
    %set(hylbl.h,'interpreter','latex');


end

%***************************
%add labels to the first theta true
% rind=1;
% cind=length(gparam.trials)+1;
%     setfocus(flowrank.a,rind,cind);
%     set(gca,'xtickmode','auto');
%     set(gca,'ytickmode','auto');
%     flowrank.a(rind,cind)=xlabel('t(s)');
%     flowrank.a(rind,cind)=ylabel('F(hz)');
    
%add colorbar to the last plot
%plot the true parameter
setfocus(flowrank.a,length(dsets),length(gparam.trials)+1);
hc=colorbar;
flowrank.a(length(dsets),length(gparam.trials)+1)=sethc(flowrank.a(length(dsets),length(gparam.trials)+1),hc);


%add a title to each graph in the top row
for col=1:length(gparam.trials)
    flowrank.a(1,col)=title(flowrank.a(1,col),sprintf('t=%d',gparam.trials(col)));
end

col=length(gparam.trials)+1;
flowrank.a(1,col)=title(flowrank.a(1,col),'\theta');

%add x and y labels to lower left graph
set(flowrank.a(length(dsets),1).ha,'xtickmode','auto');
set(flowrank.a(length(dsets),1).ha,'ytickmode','auto');

xlabel(flowrank.a(length(dsets),1).ha,'Time(ms)');
ylbl=dsets(dorder(end)).lbl;
ylbl=sprintf('%s\nFrequency(KHz)',ylbl);
ylabel(flowrank.a(length(dsets),1).ha,ylbl);

%label the graphs
flowrank=lblgraph(flowrank);
flowrank=sizesubplots(flowrank,[],[],[],[.05 0 0 0]);

%***************************




feps='~/svn_trunk/publications/adaptive_sampling/nips08/figs/lowrankmaps.eps';

%saveas(gethf(flowrank),feps,'epsc2');
%odata={'data sets',[{'label'},{'variable'},{'file'};{dsets.lbl}' {dsets.simvar}' {dsets.fname}'];flowrank,'stimulus coefficients';};

otbl={'script','nips08plowrank.m'};
otbl=[otbl;{'setup file',setupfile;'image file',feps;'explanation',''}];

odata={flowrank;otbl};
onenotetable(odata,seqfname('~/svn_trunk/notes/nips08lowrank.xml'));