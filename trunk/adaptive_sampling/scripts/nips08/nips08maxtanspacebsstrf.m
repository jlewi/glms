%11-18-2007
%
%Run a simulation using a theta which is a matrix but which is low rank
%
%03-11-2008
%   modified it so it creates a dataset file for the datasets
clear variables
%close all;

setpathvars;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile=seqfname('/tmp/maxnumint.out');
%diary(dfile);


niter=50;

dsets=[];
dind=0;
%***********************************************************
%should replace with code for GLM object.
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile('tanspace', datestr(datetime,'yymmdd'));


%data.fname
%to save data to file
%specify where to save data
%leave blank not to save
%fbase=fullfile(RDIR, sprintf('lowrankauditory.mat'));

%data.fname
%to save data to file
%specify where to save data
%leave blank not to save
fbase=FilePath('bcmd','RESULTSDIR','rpath',fullfile(RDIR, sprintf('lowrankbs.mat')));


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.lowmem=5000; %how often to save the covariance matrix;

%what rank to use for the tangent space approximation

%**************************************************************************
%Simulations: we create 1 simulation object for each simulation we want to
%**************************************************************************
%which simulations to initialize
infomax=true;
simrand=true;
infomaxfull=true;

%****************************************************************
%Stimulus features
%****************************************************************
%use 2 gabors to approximate what the receptive fields look like
%in Linden03
%%

%use David's strf
dstrf=load('~/svn_trunk/allresults/bird_song/david_strfs/br2523_06_003_song_strf.mat');

%subsample it
midpoint=(1+size(dstrf.strf,2))/2;

strf=dstrf.strf(1:3:end,midpoint:midpoint+.02/.001);
%flip it so its consistent with the way I represent time
strf=10^6*strf(:,end:-1:1);

mparam.mdim=size(strf);
mparam.ktrue=strf(:);

ranks=[2];

for rank=ranks
    mparam.rank=rank;
    %**************************************************************************
    %initialize the posterior
    %**************************************************************************
    %make the initial mean slightly larger than zero
    randn('state',9);
    rand('state',9);


    %we initialize the tangent space with some rank mdim.rank 1 matrices of the
    %appropriate dimensions
    uinit=normrnd(zeros(mparam.mdim(1),mparam.rank),ones(mparam.mdim(1),mparam.rank));
    uinit=normmag(uinit);
    vinit=normrnd(zeros(mparam.mdim(2),mparam.rank),ones(mparam.mdim(2),mparam.rank));
    vinit=normmag(vinit);
    %make sinit very small but nonzero so that it isn't singular
    sinit=10^-6*ones(mparam.rank,1);
    tinit=LowRankTanSpace('u',uinit,'v',vinit,'s',sinit);
    fprior=GaussPost('m',gettheta(tinit),'c',eye(getdimtheta(tinit)));
    tparam.dimsparam=getdimsparam(tinit);
    tparam.rank=getrank(tinit);
    tparam.matdim=mparam.mdim;
    mparam.pinit=PostTanSpace('fullpost',fprior,'tanspacetype','LowRankTanSpace','tanparam',tparam);

    %%
    %set mparam.mmag so that when 100% of energy is along the true parameter
    %the avg number of spikes is 1000
    optim=optimset('TolX',10^-12,'TolF',10^-12);
    mparam.avgspike=5000;
    optim=optimset('TolX',10^-12,'TolF',10^-12);
    fsetmmag=@(m)(glm.fglmmu(m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.avgspike);
    [mmag, fmag, exitflag,fsout]=fsolve(fsetmmag,.1,optim);
    mparam.mmag=mmag;

    if (exitflag<=0)
        error('Magnitude is not properly normalized');
    end

    mobj=MParamObj('glm',glm,'klength',getdimtheta(tinit),'pinit',mparam.pinit,'mmag',mparam.mmag);

    %**************************************************************************
    %observer
    %create observer which actually generates the observations
    observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);
    %**************************************************************************
    %Specify how we want to update the posterior
    %**************************************************************************
    updater=TanSpaceUpdater();



    slist={};
    %**********************************************************************
    %InfoMax simulation
    %********************************************************************
    %run the trials but when we optimize the stimulus
    %use so many trials of infomax on the full space
    nstartvals=[10];
    if (infomax)
        simid='infomax';
        %reseed the random number generator
        rstate=10;
        randn('state',rstate);
        rand('state',rstate);
        nstart=0;
        fullinterval=25;
        for nstart=nstartvals

            stimmax=TanSpaceInfoMaxCanon('iterfullmax',fullinterval,'startfullmax',nstart);
            explain=sprintf('Info max on the tangent space. Rank=%d,First %d trials use full info max. Full infomax every %d.',rank,nstart,fullinterval);
            lbl='Info. Max.';
            simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem, 'label',lbl,'extra',mparam);
            fname=seqfname(fbase);
            [simmax,slist(end+1,:)]=savesim(simmax,fname);
            fprintf('Saved to %s \n',getpath(fname));

            dind=dind+1;
            dsets(dind).fname=fname;
            dsets(dind).simvar=simid;
            dsets(dind).lbl=lbl;
            dsets(dind).explain=explain;
        end
    end
end

%%**********************************************************
%Simulation: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus
if (simrand)
    simid='rand';
    %reseed the random number generator
    rstate=10;
    randn('state',rstate);
    rand('state',rstate);
    explain='Random stimuli';
    lbl='i.i.d.';
    stimrand=RandStimNorm('mmag',mparam.mmag,'klength',getklength(mobj));
    urand=TanSpaceUpdater('comptanpost',true);
    simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',urand,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'label',lbl,'extra',mparam);
    fname=seqfname(fbase);
    [simrand,slist(end+1,:)]=savesim(simrand,fname);
    fprintf('Saved to %s \n',getpath(fname));

    dind=dind+1;
    dsets(dind).fname=fname;
    dsets(dind).simvar=simid;
    dsets(dind).lbl=lbl;
    dsets(dind).explain=explain;
end
%********************************************************************
%InfoMax: but infomax on full theta space not submanifold
%*****************************************************
if (infomaxfull)
    simid='infomaxfull';
    %reseed the random number generator
    rstate=10;
    randn('state',rstate);
    rand('state',rstate);
    stimfull=PoissExpMaxMI();
    ufull=Newton1d();
    explain='Info. Max. Full';
    lbl='info. full';
    mobjfull=MParamObj('glm',glm,'klength',getdimtheta(tinit),'pinit',getfullpost(mparam.pinit),'mmag',mparam.mmag);
    simmfull=SimulationBase('stimchooser',stimfull,'observer',observer,'updater',ufull,'mobj',mobjfull,'simid',simid,'lowmem',mparam.lowmem,'label',lbl,'extra',mparam);
    fname=seqfname(fbase);
    [simfull,slist(end+1,:)]=savesim(simmfull,fname);


    dind=dind+1;
    dsets(dind).fname=fname;
    dsets(dind).simvar=simid;
    dsets(dind).lbl=lbl;
    dsets(dind).explain=explain;
end

%create an xml file listing the saved simulations
onenotetable(slist,seqfname(fullfile(RESULTSDIR,RDIR,'lowrankbs_simlist.xml')));



%create a .m file to recreate this data set
fsetoutname=sprintf('lowrankbs_dataset_%s.m',datestr(datetime,'yymmdd'));
dsetfilebase=FilePath('bcmd','RESULTSDIR','rpath', fullfile('tanspace', datestr(datetime,'yymmdd'),fsetoutname));
dsetfile=seqfname(dsetfilebase);

info.avgspike=mparam.avgspike;
info.lowmem=mparam.lowmem;
info.rank=mparam.rank;

dsets(1).niter=[];
writeminit(dsetfile,dsets,info);
fprintf('Dataset file %s \n', getpath(dsetfile))
