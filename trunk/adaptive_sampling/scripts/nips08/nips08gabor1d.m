%11-25-2007
%
%Make a plot of the estimated parameters on each trial

dsets=[];

data=[];
%cosyne08gabors1d;

setupfile='/home/jlewi/svn_trunk/allresults/tanspace/080311/co08gabors1dAC_080311_001.m';
xscript(setupfile);

%remove dsets 4 from the data
dsets=dsets([1 3 2]);
maxtrial=1000;


for dind=1:length(dsets)
    %[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
    simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);


        data(dind).ktrue=gettheta(getobserver(simobj));
    %compute the mse
    postobj=getpost(simobj);
    if isa(postobj,'PostTanSpace')
        fullpost=getfullpost(postobj);
        data(dind).fullm=getm(fullpost);


        %get the mean on the tangent space
        data(dind).tanm=getmusingtan(postobj);

    else
        data(dind).fullm=getm(postobj);

   
    end
    %last trial to plot
    data(dind).tlast=min(maxtrial,getniter(simobj));
end

%**************************************************************************
%plot the Recovered values
%*************************************************************
%%


ffilt=FigObj('width',5,'height',3.55,'name','Estimated Parameters','fontsize',10);
ffilt.a=AxesObj('nrows',2,'ncols',(length(dsets)));

%*********************************************
%plot the estimated filters
for dind=1:length(dsets)

    pind=dind;
    %plot the mean of the full posterior
    setfocus(ffilt.a,1,pind);

    imagesc(data(dind).fullm(:,1:data(dind).tlast)');
    set(gca,'ydir','reverse');
    ffilt.a(1,pind)=title(ffilt.a(1,pind),sprintf('%s',dsets(dind).tlbl));
    %turn off xtick and ytick
    set(gca,'xtick',[]);
    
    xlim([1 length(data(dind).ktrue)]);
    ylim([1 data(dind).tlast-1]);
    
    %set(gca,'ytick',[]);

    %plot the truth
    setfocus(ffilt.a,2,pind);
    imagesc(data(dind).ktrue');
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);

    xlim([1 length(data(dind).ktrue)]);
    

end

%make the color limits the same
cl=get(ffilt.a,'clim');
cl=cell2mat(cl);
cl=[-2 5];
set(ffilt.a,'clim',cl);

%****************************************************************
%label graphs
%*******************************************************************
%add a colorbar
cind=length(dsets);
setfocus(ffilt.a,1,(length(dsets)));
hc=colorbar;
ffilt.a(1,cind)=sethc(ffilt.a(1,cind),hc);
%left column
ffilt.a(1,1)=ylabel(ffilt.a(1,1),'Trial');
setfocus(ffilt.a,1,1);
set(gca,'ytickmode','auto');


ffilt.a(2,1)=ylabel(ffilt.a(2,1),'true');
% for j=1:length(dsets)*2
%     setfocus(ffilt.a,2,j);
%     set(gca,'xtickmode','auto');
% end

ffilt=lblgraph(ffilt);

space.cbwidth=.1;
space.vspace=.01;
ffilt=sizesubplots(ffilt,space,ones(1,length(dsets)),[.9 .1]);

imgfile=fullfile('~/svn_trunk/adaptive_sampling/writeup/nips08/figs',sprintf('gaborac_map.eps'));
saveas(gethf(ffilt),imgfile,'epsc');

odata={ffilt;{'Data',{dsets.fname}'; 'Script', 'nips08gabor1d.m';'setupfile',setupfile;'image file',imgfile;'InfoMax Full', 'Infomax on full theta space as opposed to infomax on tangent space'}};
onenotetable(odata,seqfname('~/svn_trunk/notes/nips08cmptanimg.xml'));
    

