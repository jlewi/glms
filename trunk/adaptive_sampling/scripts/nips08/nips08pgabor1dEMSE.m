%5-14-2008
%   For our 1-d Gabor whose only unknown parameters are the amplitude and
%   center, we compare our full posterior, to our posterior on the tangent space,
%   to the true poster. We do this by computing and ploting E ||\theta
%   -\theta_0||. Theta_0 is the true parameters. The expectation is over
%   one of the 3 posteriors.
%
%   For the Guassian posteriors, the expectation has a simple analytical
%   expression.
%
%   For the true posterior, we compute the expectation numerically by
%   integrating over A,C. We do 2 integrations. The first integration
%   computes the normalization constant.

clear variables;
setpathvars;
%1-d gabor
setupfile='/home/jlewi/svn_trunk/allresults/tanspace/080311/co08gabors1dAC_080311_001.m';
xscript(setupfile);
dind=1;
simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

ptrue=[];
pfull=[];
ptan=[];
%%
%what trials to compute it for.
%if you try to compute the full posterior for trial 40 or later
%numerical integration methods fail because true posterior is essentially
%an impulse function on the true parameters. 
fulltrials=[1 5 10:10:30];
trials=[1 5 10:10:50];

%bound for the integration
arange=[0 6];
crange=[-20 20];
%The results are stored in three structure arrays
%   ptrue - using the true posterior
%   pfull - MSE using the full posterior
%   ptan  - MSE using the posterior on the tangent space


%*************************************************************************
%get the necessary quantities from simobj
%************************************************************************
observer=getobserver(simobj);
theta=gettheta(observer);

sr=getsr(simobj);
allstim=getData(getinput(sr));
allobsrv=getobsrv(sr);

mobj=getmobj(simobj);
glm=getglm(mobj);

fullprior=getpost(simobj,0);
prior=getfullpost(fullprior);

extra=getextra(simobj);
mparam=extra.mparam;
sigmasq=extra.mparam.sigmasq;
omega=extra.mparam.omega;


%tangent space at the true parameters
%this is used to map the parameters of the gabor to actual points in theta
%space
tspace=GaborTanSpaceAC('sparam',mparam.gp,'dimtheta',getparamlen(mobj),'sigmasq',sigmasq,'omega',omega);

%%
%*************************************************************************
%Compute the MSE for the true posterior
%**************************************************************************
%only compute the MSE for trials for which we haven't computed it already
for tind=1:length(fulltrials)
    trial=fulltrials(tind);

    if (isempty(ptrue) || isempty(find([ptrue.trial]==trial)))
        ind=length(ptrue)+1;
        ptrue(ind).trial=trial;

        obsrv=allobsrv(1:trial);
        stim=allstim(:,1:trial);
        %compute the normalization constant of the posterior
        %the function for dblquad takes two inputs (x,y) x - is a vector
        % y - is a scalar.
        fnormc=@(a,c)(postgaborac(a,c*ones(1,length(a)),stim,obsrv,glm,prior,omega,sigmasq));
        ptrue(ind).normc=dblquad(fnormc,arange(1),arange(2),crange(1),crange(2));

        %function for computing the expected MSE
        fpost=@(a,c)(1/ptrue(ind).normc*postgaborac(a,c,stim,obsrv,glm,prior,omega,sigmasq));

        fmse=@(a,c)(sum((submanifold(tspace,[a;c])-theta*ones(1,length(a))).^2).^.5);
        fobj=@(a,c)(fpost(a,c*ones(1,length(a))).*fmse(a,c*ones(1,length(a))));
        ptrue(ind).mse=dblquad(fobj,arange(1),arange(2),crange(1),crange(2));

    end

end


%%
%*************************************************************************
%Compute the MSE for the full posterior
%**************************************************************************
%only compute the MSE for trials for which we haven't computed it already
for tind=1:length(trials)
    trial=trials(tind);

    if (isempty(pfull) || isempty(find([pfull.trial]==trial,1)))
        ind=length(pfull)+1;
        pfull(ind).trial=trial;

        post=getpost(simobj,trial);


        if isempty(getc(post))
            fprintf('Computing covariance matrix for trial %d \n', trial);
            uobj=getupdater(simobj);
            [simobj]=fillinlowmem(uobj, simobj,trial);
            post=getpost(simobj,trial);
        end

        fullpost=getfullpost(post);

        m=getm(fullpost);
        c=getc(fullpost);
        pfull(ind).mse=trace(c)-2*m'*theta+theta'*theta+m'*m;
    end

end

%*************************************************************************
%Compute the MSE for the posterior on the tangent space
%**************************************************************************
%only compute the MSE for trials for which we haven't computed it already
for tind=1:length(trials)
    trial=trials(tind);

    if (isempty(ptan) || isempty(find([ptan.trial]==trial,1)))
        ind=length(ptan)+1;
        ptan(ind).trial=trial;

        post=getpost(simobj,trial);


%         if isempty(getc(post))
%             fprintf('Computing covariance matrix for trial %d \n', trial);
%             uobj=getupdater(simobj);
%             [simobj]=fillinlowmem(uobj, simobj,trial);
%             post=getpost(simobj,trial);
%         end

        tanpost=gettanpost(post);
        tspace=gettanspace(post);
        
        mf=gettheta(tspace);
        mb=getm(tanpost);
        cb=getc(tanpost);
        b=getbasis(tspace);
        ptan(ind).mse=(mf-theta)'*(mf-theta)+2*(mf-theta)'*b*mb+trace(cb)+(b*mb)'*(b*mb);
    end

end
return;

%
%%
%********************************************************************
%Plot the results
%*********************************************************************
%plot the probability
ferr=FigObj('name','mse','width',4,'height',2);
ferr.a=AxesObj('xlabel','trial','ylabel','E||\theta -\theta_o||^2');
hlines=[];

pstyle.color='r';
pstyle.marker='o';
pstyle.markersize=7;
pstyle.markerfacecolor='r';
hp=plot([pfull.trial],[pfull.mse]);
ferr.a=addplot(ferr.a,'hp',hp,'pstyle',pstyle);
hlines=[hlines hp];

pstyle.color='b';
pstyle.marker='o';
pstyle.markersize=7;
pstyle.markerfacecolor='b';
hp=plot([ptan.trial],[ptan.mse]);
ferr.a=addplot(ferr.a,'hp',hp,'pstyle',pstyle);
hlines=[hlines hp];

pstyle.color='g';
pstyle.marker='o';
pstyle.markersize=7;
pstyle.markerfacecolor='g';
hp=plot([ptrue.trial],[ptrue.mse]);
ferr.a=addplot(ferr.a,'hp',hp,'pstyle',pstyle);
hlines=[hlines hp];

lobj=LegendObj('hlines',hlines,'lbls',{'p(\theta|\mu_t,c_t)','p(\theta|\mu_{b,t},c_{b,t})','p(\theta|s_{1:t},r_{1:t},M)'});
lblgraph(ferr);
lobj=setposition(lobj,.68,.65,.3,.35);
lobj=settleft(lobj,.25);
lobj=setvoffset(lobj,-.05);
lobj=setvborder(lobj,.13,.15);

    fname='~/svn_trunk/adaptive_sampling/writeup/nips08/figs/gaborac_mse.eps';
    %saveas(gethf(ferr),fname,'epsc2');
  explain='The mean squared error evaluated using the true posterior on the manifold, the gaussian approximation on the full theta space, and gaussian approximation on the tangent space. True theta is gabor with unknown amplitude and center. ';
   onenotetable({ferr;{'script','nips08gabor1dEMS'; 'data file',getpath(dsets(dind).fname);'simvar',dsets(dind).simvar;'explanation',explain}},seqfname('~/svn_trunk/notes/gaboracmse.xml'));
