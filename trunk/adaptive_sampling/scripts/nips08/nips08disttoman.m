%05-13-2008
%
%Compute via sampling the average distance of points from the manifold
%we compute this for our posterior on the tangent space and the full
%posterior.

clear variables;
%new data set
dsets=[];
%data for some post cosyne experiments
%bird son data
%setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080311','dataset_lowrank_080311_001.m');
setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080606','nips08_bslowd.m');


%low rank data
%setupfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080311','dataset_lowrank_080311_001.m');

%1-d gabor
%setupfile='/home/jlewi/svn_trunk/allresults/tanspace/080311/co08gabors1dAC_080311_001.m';

xscript(setupfile);

dsets=dsets;
%***************************************************
%parameters for the graph
%***************************************************
%trials= [1:10:100];  
trials=[100 500 1000 1500 2000 2200 2500];
trials=[250 500 750 1000];
nsamps=1000;    %how many samples to draw.

hold on;


%create a data structure to store the data from all the simulations
dind=1;
    [simobj]=SimulationBase('fname',getpath(dsets(dind).fname),'simvar',dsets(dind).simvar);
    
%%
%structures to store the results
%dfullpost - distance of the full posterior on different trials
dfullpost=struct('trial',[],'dist',[]);

dtanpost=struct('trial',[],'dist',[]);

observer=getobserver(simobj);
theta=gettheta(observer);

thetamag=(theta'*theta)^.5;
for tind=1:length(trials)
    trial=trials(tind);
    %********************************************************
    %sample the distance of the full posterior
    %******************************************
    dfullpost(tind).trial=trial;
    
    post=getpost(simobj,trial);
    
    if isempty(getc(post))
%        error('Cannot compute distance on trial %d because covariance matrix was not saved' \n,trial);
        fprintf('Computing covariance matrix on trial %0.3g \n', trial);
        uobj=getupdater(simobj);
        [simobj]=fillinlowmem(uobj, simobj,trial);
 post=getpost(simobj,trial);
    
    end
    
    %get the fullposterior
    fpost=getfullpost(post);
    tspace=gettanspace(post);
    dfullpost(tind).dist=sampdisttoman(fpost,tspace,nsamps);
    dfullpost(tind).avgdist=sum(dfullpost(tind).dist/thetamag)/length(dfullpost(tind).dist);
  
    dfullpost(tind).stddist=std(dfullpost(tind).dist/thetamag);
    %********************************************************
    %sample the distance of the posterior on the tangent space
    %******************************************
    
    dtanpost(tind).trial=trial;
    %sampdisttoman handles computing the theta in the tangent space
     dtanpost(tind).dist=sampdisttoman(post,tspace,nsamps);
    dtanpost(tind).avgdist=sum(dtanpost(tind).dist/thetamag)/length(dtanpost(tind).dist);

    dtanpost(tind).stddist=std(dtanpost(tind).dist/thetamag);
end


%**************************************************************
%plot the results
%**************************************************************
%%
fdist=FigObj('name','distance to manifold','width',2.2,'height',1.7);
fdist.a=AxesObj('xlabel','trial','ylabel','$ \frac{E ||\theta\textrm{-}P_{m}\theta||}{||\theta_o||}$');

hy=getylabel(fdist.a);
set(hy.h,'interpreter','latex');

hold on;

%plot the results for the full posterior
%normalize the distance by the magnitude of the true theta to get some
%sense of the significance. 
pstyle=[];
hp(1)=plot([dfullpost.trial],[dfullpost.avgdist]);
pstyle.color='g';
pstyle.marker='o';
pstyle.markersize=7;
pstyle.markerfacecolor='g';
fdist.a=addplot(fdist.a,'hp',hp(1),'pstyle',pstyle);
errorbar([dfullpost.trial],[dfullpost.avgdist],[dfullpost.stddist],'Color','g');

hp(2)=plot([dtanpost.trial],[dtanpost.avgdist]);
pstyle.color='b';
pstyle.marker='o';
pstyle.markersize=7;
pstyle.markerfacecolor='b';
fdist.a=addplot(fdist.a,'hp',hp(2),'pstyle',pstyle);


%add error bar
errorbar([dtanpost.trial],[dtanpost.avgdist],[dtanpost.stddist]);

xlim([trials(1) trials(end)]);

lobj=LegendObj('hlines',hp,'lbls',{'p(\theta|\mu_t,\mu_t)','p_{tan}(\theta|\mu_{b,t},\mu_{b,t})','p(\theta|s_{1:t},r_{1:t},M)'});
lblgraph(fdist);


%lobj=setposition(lobj,.42,.77,.54,.27);
lobj=setposition(lobj,.35,.72,.64,.27);
lobj=settleft(lobj,.32);
lobj=setvoffset(lobj,-.05);
lobj=setvborder(lobj,.2,.3);


otable={fdist;{'script:','nips08disttoman.m';'setupfile', setupfile;'#samples',nsamps}};
onenotetable(otable,seqfname('~/svn_trunk/notes/disttoman.xml'));

%saveas(gethf(fdist),'~/svn_trunk/adaptive_sampling/writeup/nips08/figs/lowrankdtoman.eps','epsc2');