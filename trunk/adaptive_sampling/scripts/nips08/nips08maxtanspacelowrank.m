%11-18-2007
%
%Run a simulation using a theta which is a matrix but which is low rank
%
%03-11-2008
%   modified it so it creates a dataset file for the datasets
clear variables
%close all;

setpathvars;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile=seqfname('/tmp/maxnumint.out');
%diary(dfile);


niter=50;

dsets=[];

dind=0;
%***********************************************************
%should replace with code for GLM object.
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile('tanspace', datestr(datetime,'yymmdd'));


%data.fname
%to save data to file
%specify where to save data
%leave blank not to save
%fbase=fullfile(RDIR, sprintf('lowrankauditory.mat'));

%data.fname
%to save data to file
%specify where to save data
%leave blank not to save
fbase=FilePath('bcmd','RESULTSDIR','rpath',fullfile(RDIR, sprintf('lowrankauditory.mat')));


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.lowmem=5000; %how often to save the covariance matrix;

%****************************************************************
%Stimulus features
%****************************************************************
%use 2 gabors to approximate what the receptive fields look like
%in Linden03
%%
mdim=30; %dimension of the matrix
randn('state',9);
rand('state',9);

%make the receptive field a low rank matrix of rank 2
%each component is a gabor
%This is based on Qiu03
%the first temporal component is a gabor which looks almost
%like a gaussian because the inhibitory peaks are so small. 
%set tau so that amplitude of inhibiotory peak is .05
period=2*.75*(mdim);  %set the period so that the width of the peak is half the length
tau=-(period^2/4)/log(.01);
cent=-mdim/2+period/4;
mparam.f1=gabor1d(mdim,cent,period,tau)';

%first temp filter
period=.9*mdim;
cent=-mdim/2+period/4;
tau=-period^2/4/(log(.5));
mparam.t1=gabor1d(mdim,cent,period,tau)';

%for the second filters we use 1 period of a sine wave.
%2nd frequency filter
period=.75*mdim;
cent=-.9*mdim/2+period/2;
x=(1:mdim)-(1+mdim)/2-cent;
tau=-(period^2/4)/log(.5);
mparam.f2=sin(2*pi/period*x)'.*exp(-(x-period/4).^2/tau)';
ind=(find(abs(x)>period/2));
mparam.f2(ind)=0;
mparam.mdim=mdim;


%2nd time filter
%make at a antisymetric filter same as mparam.f2
%period=2/3*.9*mdim;
%cent=-mdim/2+3*period/4;
%tau=-period^2/4/(log(.25));
mparam.t2=mparam.f2;
mparam.ktrue=mparam.f2*mparam.t2'+mparam.f1*mparam.t1';
mparam.ktrue=mparam.ktrue(:);

%%figure;imagesc(mparam.f2*mparam.t2'+mparam.f1*mparam.t1');
%%




%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);


uinit=normrnd(zeros(mdim,2),ones(mdim,2));
uinit=normmag(uinit);
vinit=normrnd(zeros(mdim,2),ones(mdim,2));
vinit=normmag(vinit);
%make sinit very small but nonzero so that it isn't singular
sinit=10^-6*ones(2,1);
tinit=LowRankTanSpace('u',uinit,'v',vinit,'s',sinit);
fprior=GaussPost('m',gettheta(tinit),'c',eye(getdimtheta(tinit)));
tparam.dimsparam=getdimsparam(tinit);
tparam.rank=getrank(tinit);
tparam.matdim=[mdim mdim];
mparam.pinit=PostTanSpace('fullpost',fprior,'tanspacetype','LowRankTanSpace','tanparam',tparam);

%%
%set mparam.mmag so that when 100% of energy is along the true parameter
%the avg number of spikes is 1000
optim=optimset('TolX',10^-12,'TolF',10^-12);
mparam.avgspike=500;
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.avgspike);
[mmag, fmag, exitflag]=fsolve(fsetmmag,.1,optim);
mparam.mmag=mmag;

if (exitflag<=0)
    error('Magnitude is not properly normalized');
end

mobj=MParamObj('glm',glm,'klength',getdimtheta(tinit),'pinit',mparam.pinit,'mmag',mparam.mmag);

%**************************************************************************
%observer
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);
%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
updater=TanSpaceUpdater();

%**************************************************************************
%Simulations: we create 1 simulation object for each simulation we want to
%**************************************************************************
%which simulations to initialize
infomax=true;
simrand=true;
infomaxfull=true;

slist={};
%**********************************************************************
%InfoMax simulation
%********************************************************************
%run the trials but when we optimize the stimulus
nstartvals=[10 100];
if (infomax)
simid='infomax';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
nstart=0;
fullinterval=25;
for nstart=nstartvals
    
stimmax=TanSpaceInfoMaxCanon('iterfullmax',fullinterval,'startfullmax',nstart);
explain=sprintf('Info max on the tangent space. First %d trials use full info max. Full infomax every %d.',nstart,fullinterval);
lbl='Info. Max.';
simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem, 'label',lbl,'extra',mparam);
fname=seqfname(fbase);
[simmax,slist(end+1,:)]=savesim(simmax,fname);
fprintf('Saved to %s \n',getpath(fname));

    dind=dind+1;
    dsets(dind).fname=fname;
    dsets(dind).simvar=simid;
    dsets(dind).lbl=lbl;
        dsets(dind).explain=explain;
end
end


%%**********************************************************
%Simulation: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus
if (simrand)
simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
explain='Random stimuli';
lbl='i.i.d.';
stimrand=RandStimNorm('mmag',mparam.mmag,'klength',getklength(mobj));
urand=TanSpaceUpdater('comptanpost',true);
simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',urand,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'label',lbl,'extra',mparam);
fname=seqfname(fbase);
[simrand,slist(end+1,:)]=savesim(simrand,fname);
fprintf('Saved to %s \n',getpath(fname));

    dind=dind+1;
    dsets(dind).fname=fname;
    dsets(dind).simvar=simid;
    dsets(dind).lbl=lbl;
    dsets(dind).explain=explain;
end
%********************************************************************
%InfoMax: but infomax on full theta space not submanifold
%*****************************************************
if (infomaxfull)
simid='infomaxfull'
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimfull=PoissExpMaxMI();
ufull=Newton1d();
explain='Info. Max. Full';
mobjfull=MParamObj('glm',glm,'klength',getdimtheta(tinit),'pinit',getfullpost(mparam.pinit),'mmag',mparam.mmag);
simmfull=SimulationBase('stimchooser',stimfull,'observer',observer,'updater',ufull,'mobj',mobjfull,'simid',simid,'lowmem',mparam.lowmem,'label',lbl,'extra',mparam);
fname=seqfname(fbase);
[simfull,slist(end+1,:)]=savesim(simmfull,fname);


    dind=dind+1;
    dsets(dind).fname=fname;
    dsets(dind).simvar=simid;
    dsets(dind).lbl=lbl;
    dsets(dind).explain=explain;
end

%create an xml file listing the saved simulations
onenotetable(slist,seqfname(fullfile(RESULTSDIR,RDIR,'simlist.xml')));



%create a .m file to recreate this data set
%add the field niter 
for dind=1:length(dsets)
dsets(dind).niter=500;
end
fsetoutname=sprintf('dataset_lowrank_%s.m',datestr(datetime,'yymmdd'));
dsetfilebase=FilePath('bcmd','RESULTSDIR','rpath', fullfile('tanspace', datestr(datetime,'yymmdd'),fsetoutname));
dsetfile=seqfname(dsetfilebase);

info.avgspike=mparam.avgspike;
info.lowmem=mparam.lowmem;


writedataset(dsetfile,dsets,info);
fprintf('Dataset file %s \n', getpath(dsetfile))
