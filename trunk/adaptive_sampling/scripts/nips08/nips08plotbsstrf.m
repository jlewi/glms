dstrf=load('~/svn_trunk/allresults/bird_song/david_strfs/br2523_06_003_song_strf.mat')

f=(250+129/2):129:8000-129/2;
t=(1:size(dstrf.strf,2))-size(dstrf.strf,2)/2;
t=t*10^-3; %its in milliseconds

%subsmample it
%f=f(1:2:end);
%
%scale it
%strf=strf*10^6;

%subsample it
% midpoint=(1+size(dstrf.strf,2))/2;
% strf=dstrf.strf(1:2:end,midpoint:midpoint+.04/.001);
% %flip it so its consistent with the way I represent time
% strf=10^6*strf(:,end:-1:1);

midpoint=(1+size(dstrf.strf,2))/2;

strf=dstrf.strf(1:3:end,midpoint:midpoint+.02/.001);
%flip it so its consistent with the way I represent time
strf=10^6*strf(:,end:-1:1);
mparam.mdim=size(strf);
mparam.ktrue=strf(:);
t=[size(strf,2)-1:0]*10^-3;
f=f(1:3:end);

%svd of strf 
[ustrf sstrf vstrf]=svd(strf);

%make a plot of the strf and a rank 2 approximation
gparam=[];
gparam.height=2;
gparam.width=4;

%iwidth - what % of the width is for the image of the combined filter
%fwidth - what % of the width is for the plot of filter component
gparam.iwidth=.2;
gparam.fwidth=.05;

%iheight - what % of the height is for the image of the combined filter
%fheight - what % of the height is for the plot of time component
gparam.iheight=.5;
gparam.theight=.2;

gparam.ibottom=.2;

%spacing between the components of the rank-1 matrices
%and the plots of the actual rank 1 matrix 
%we scale the spacing based on the dimensions of the figure so that
%the vertical and horizontal spacing are the same.
gparam.stofiltshorz=.0177;
gparam.stofiltsvert=gparam.stofiltshorz*gparam.width/gparam.height;

%how much of a border to leave on left and right side
gparam.bhorz=.0177;
%spacing between the plots of the matrices
gparam.mspace=(1-(gparam.fwidth)*2-gparam.iwidth*3-gparam.stofiltshorz*2-gparam.bhorz*2)/2;


%set the left position of each matrix 
gparam.ileft(1)=gparam.fwidth+gparam.stofiltshorz+gparam.bhorz;
gparam.ileft(2)=gparam.ileft(1)+gparam.iwidth+gparam.fwidth+gparam.stofiltshorz+gparam.mspace;
gparam.ileft(3)=gparam.ileft(2)+gparam.iwidth+gparam.mspace;
  
gparam.clim=[-2.7 5.6];
gparam.linewidth=2;



hf=[];

    hf=figure;
    setfsize(hf,gparam);
    
for fi=1:2
    filts.f=[ustrf(:,fi)];
    filts.t=[vstrf(:,fi)];
    
%     if (fi==1)
%         filts.f=extra.f1;
%         filts.t=extra.t1;
%     else
%         filts.f=extra.f2;
%         filts.t=extra.t2;
%     end
    filts.theta=filts.f*filts.t'*sstrf(fi,fi);


    %plot the main filter
    ha(fi).main=axes;
    imagesc(filts.theta,gparam.clim);
    set(gca,'ydir','reverse');
    set(gca,'yaxislocation','right');
    mpos=get(ha(fi).main,'position');
    

    
    
    mpos=[gparam.ileft(fi) gparam.ibottom gparam.iwidth gparam.iheight];
    set(gca,'position',mpos);
    
    
set(gca,'xtick',[]);
set(gca,'ytick',[]);
    %***************************************************
    %plot the temporal filter
    %**************************************************
    ha(fi).tfilt=axes;
    hp=plot(filts.t);
    set(hp,'linewidth',gparam.linewidth);

    set(gca,'xtick',[]);
    set(gca,'ytick',[-1 1]);
    
    tpos=get(gca,'position');
    tpos=[mpos(1) mpos(2)+gparam.iheight+gparam.stofiltsvert gparam.iwidth gparam.theight];
    set(gca,'position',tpos);
    xlim([1 ,length(filts.t)]);
    
   %***************************************************
    %plot the freq filter
    %***************************************************
    ha(fi).ffilt=axes;
   

    %we want this axes to be oriented vertically
    %so we need to effectively switch the x and y axes
    hp=plot(filts.f,1:length(filts.f));
    set(hp,'linewidth',gparam.linewidth);

    set(gca,'ytick',[]);
    set(gca,'xtick',[-1 1]);
    %put the y-axis on the right side
    set(gca,'YAxisLocation','right');
    set(gca,'xdir','reverse');
    set(gca,'ydir','reverse');

    fpos=get(gca,'position');
    fpos=[mpos(1)-gparam.fwidth-gparam.stofiltshorz mpos(2) gparam.fwidth gparam.iheight];
    set(gca,'position',fpos);
    ylim([1 ,length(filts.f)]);


    
end

%plot the true filter
fi=fi+1;
ha(fi).ffilt=axes;
mdim=length(filts.t);
imagesc(t,f,strf,gparam.clim);
set(gca,'ydir','reverse');

ktpos=[gparam.ileft(fi) gparam.ibottom gparam.iwidth gparam.iheight];
set(gca,'position',ktpos);

 set(gca,'xtick',[]);
 set(gca,'ytick',[]);
% xlabel('t(s)');
% ylabel('f(hz)');

%**************************************************************************
%add the text symbols
%**************************************************************************
%create an axes which fills the screen
fi=fi+1;
ha(fi).ffilt=axes;
%scale the axes to the full window and set its coordinates to 0,1
xlim([0 1]);
ylim([0 1]);
set(gca,'position',[0 0 1 1]);
axis off;

%+ symbol
txt.x=gparam.ileft(2)-gparam.fwidth-gparam.stofiltshorz-gparam.mspace/2;
txt.y=gparam.ibottom+gparam.iheight/2;
ht=text(txt.x,txt.y,'+');
set(ht,'fontsize',12);

%+ symbol
txte.x=gparam.ileft(3)-gparam.mspace/2;
txte.y=gparam.ibottom+gparam.iheight/2;
hequal=text(txte.x,txte.y,'=');
set(hequal,'fontsize',12);

foutfile=sprintf('~/svn_trunk/adaptive_sampling/writeup/nips08/figs/bsstrflowrank.eps');
%saveas(hf,foutfile,'epsc2');