%date: 10-29-2008
%
%Explanation: For the bird song FFT applied separatly, plot the expected
%log likelihood comparing the info. max. and info. max. tangent spaces. 
%Here we plot the average of the expected log-likelihood across both wave
%files
%
%Revisions:
%   01-07-2009 - vary the line styles so it will be visible in black and
%   white
[dsets,pstyles]=nips08databstanhighd;


%make plots for poster or paper
poster=false;
if (poster)
width=6;
height=3;

gdir='~/svn_trunk/publications/adaptive_sampling/nips08/poster_figs';
%extension for graphics
fext='.png';
gtype='png';

else
    %width and height for the
width=6;
height=2;

gdir='~/svn_trunk/publications/adaptive_sampling/nips08/figs';
fext='.eps';
gtype='epsc2';

end


fh=BSExpLLike.plotavg(dsets,pstyles);

%************************************************************************
%adjust the plots
%************************************************************************
%%
%fontsize
fsize=10;

for ind=1:length(fh)
fh(ind).width=width;
fh(ind).height=height;
setfsize(fh(ind));
setfontsize(fh(ind).a.hlgnd,fsize);
autosizetight(fh(ind).a.hlgnd);
set(fh(ind).a,'xlim',[900,15000]);
set(fh(ind).a,'xtick',[10^3 10^4]);
end

%change the linestyles so that all traces are visible
 pind=1;
 pstyle=fh.a.p(pind).pstyle;
% pstyle.LineWidth=4;
 pstyle.color= [1 0 0];
 setpstyle(fh.a,pind,pstyle);
% 
 pind=2;
 pstyle=fh.a.p(pind).pstyle;
% pstyle.LineWidth=3;
 pstyle.LineStyle='--';
 pstyle.color= [0 1 0]; 
 setpstyle(fh.a,pind,pstyle);

 pind=3;
 pstyle=fh.a.p(pind).pstyle;
 pstyle.color= [0 0 1];
 pstyle.LineStyle='-.';
 setpstyle(fh.a,pind,pstyle);

 %redo the labels of the legend so that the legend colors are accurate
 plotlegend(fh.a.hlgnd)
 ind=1;
set(fh.a,'ylim',[-1 -.05]);

%add a little space to prevent it being cut off when we save it
setposition(fh.a.hlgnd,.44,.32,[],[]);


bname='bsexpllike';
fname=fullfile(gdir,[bname fext]);
saveas(fh.hf,fname,gtype);
% fname1=fullfile('~/svn_trunk/publications/adaptive_sampling/nips08/figs',[bname '_birdsong.eps']);
% fname2=fullfile('~/svn_trunk/publications/adaptive_sampling/nips08/figs',[bname '_noise.eps']);
% saveas(fh(1).hf,fname1,'epsc2');
% saveas(fh(2).hf,fname2,'epsc2');