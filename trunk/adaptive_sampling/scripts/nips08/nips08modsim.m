%08-06-0
%
%Since the simulation is not converging we want to modify the sim 
%so it does full infomax after 

dsets=[];
dind=1;
niter=[]
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath','/tanspace/080605/lowrankbs_021.mat','isdir',0);
dsets(dind).simvar='infomax';
dsets(dind).lbl='info. max.';
dsets(dind).explain='Info max on the tangent space. Rank=2,First 10 trials use full info max. Full infomax every 25.';
dsets(dind).niter=niter;
outfile=FilePath('bcmd','RESULTSDIR','rpath','/tanspace/080606/lowrankbs_021.mat','isdir',0);
siminfomax=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

%modify the updater 
stimchooser=getstimchooser(siminfomax);

iterfullmax=getiterfullmax(stimchooser);
%set it to 1
iterfullmax=0;
stimchooser=setiterfullmax(stimchooser,iterfullmax);

siminfomax=setstimchooser(siminfomax,stimchooser);

%save the modified trial
%[siminfomax]=savesim(siminfomax,outfile);