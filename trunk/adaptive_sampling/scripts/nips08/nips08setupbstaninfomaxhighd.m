%**************************************************************************
%
%Explanation: Setup the info. max. simulations using the tangent space
%   Sets up the following simulations
%   1. shuffled
%   2. info. max.
%   3. info. max. tan with rank
%       rank =2
%       rank =4
%       rank =8
%
%Theta is represented in the fourier domain
%
%Revision
%   11-10-2008: Revised script to change the cutoff frequencies, and the
%   prior
%   
function nips08setupbstaninfomaxhighd()

%try decreasing the bin size and increasing the number of time bins
allparam.obsrvwindowxfs=124;
allparam.stimnobsrvwind=20;
allparam.freqsubsample=1;

allparam.model='MBSFTSep';





scale=1;
allparam.prior.stimvar=10^-2*scale;
allparam.prior.shistvar=1*scale;
allparam.prior.biasvar=10*scale^2;

%create a bdata an mobject to use to decide which frequency components to
%include
bdata=BSSetup.initbsdata(allparam);

strfdim=getstrfdim(bdata);
mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.alength=0;
mparam.hasbias=0;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mobj=MBSFTSep(mparam);


%  nfreq=20;
% ntime=4;
nfreq=10;
ntime=2;
subind=bindexinv(mobj,1:mobj.nstimcoeff);
btouse=subind(:,1)<=nfreq & subind(:,2)<=ntime;
btouse=find(btouse==1);

allparam.mparam.btouse=btouse;


%how many repeats of the data to use
allparam.stimparam.nrepeats=5;

allparam.updater.compeig=false;

%************************************************************
%Shuffled
%************************************************************
% shparam=allparam;
% shparam.stimtype='BSBatchShuffle';
% shparam.updater.type='Newton1d';
% BSSetup.setupinfomax(shparam);
% 
% 
% %************************************************************
% %Info. Max.
% %************************************************************
% imparam=allparam;
% imparam.stimtype='BSBatchPoissLB';
% imparam.updater.type='Newton1d';
% BSSetup.setupinfomax(imparam);

%**************************************************************
%setup the infomax with the tangent space
%***************************************************************
tanparam=allparam;

tanparam.stimtype='BSBatchPoissLBTan';
tanparam.updater.type='TanSpaceUpdater';
for rank=[2];
    tanparam.tanparam.rank=rank;
    BSSetup.setupinfomax(tanparam);    
end
