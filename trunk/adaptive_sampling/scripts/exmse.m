%1-18-2005
%
% Numerically compute the expected value of the MSE and show its a minimum
% when the estimator = the true parameter
%   y - value of the stimulus
%       we are integrating over the joint probability of p(y,nspikes)
%       nspikes is the number of spikes in response to y
%   mlest - the estimated likelihood
%   ml    - true max likelihood
%   mldiff - the difference between the likelihood 
%
%
%We do not consider zero spikes because this causes error when take log 0.
%True theta parameter
theta=1;
% Since we are take the expectation over a poisson process
% we need to sum over the number of observed events
% smax is the maximum value of our integral
smax=150;
%smax=70;
%length of the time window in which the response is observed
twindow=1; 

%pt at which to compute expected error
thetaest=theta;

%to compute the expectation we construct 2d matrices to store the joint
%distribution of p(y,nspikes)
%we start by constructing 2 2d matrices y and nspikes
%which store the associated value 
%range of y
ymin=.02;
ymax=2.02;
dy=.001;
y=(ymin:dy:ymax)';
y=y*ones(1,smax);

nspikes=1:smax;
nspikes=ones(size(y,1),1)*nspikes;

%construct the joint distribution
%we need a matrix to represent factorial of nspikes
factn=[1:smax];
for index=2:smax
    factn(index)=index*factn(index-1);
end
factn=ones(size(y,1),1)*factn;

%joint probability is p(y)*p(nspikes|y)
%p(y) is uniform distribution
%p(nspikes|y) is poisson with rate lambda=twindow*e^(theta*y)
lambda=twindow*exp(theta*y);
pj=1/(size(y,1))*exp(-lambda).*(lambda).^nspikes./factn;



%compute derivative of mse
derror=(log(nspikes/twindow)-y*thetaest).*y;



exderror=derror.*pj;
exderror=sum(exderror(:))

%expected error
%error
thetaest=theta-1:.1:theta+1;
for ind=1:length(thetaest)
    err=(log(nspikes/twindow)-y*thetaest(ind)).^2;
    exerrmat=err.*pj;
    exerr(ind)=sum(exerrmat(:));
end
figure;
hold on;
plot(thetaest-theta,exerr,'.');
plot(thetaest-theta,exerr);
xlabel('\theta_e_s_t-\theta_t_r_u_e');
ylabel('Expected Error');
    








        