%9-30-06
% Test maxminquad.m

%
d=2;

mu=[1;1];
C=diag([2 1]);
[evecs, eigd]=eig(C);

eigd=diag(eigd);

mu=mu/(mu'*mu)^.5;
mag=1;
optparam=[];
%***************************************************
%range of alpha to compute max and min over
npts=50;
alpharange=linspace(-mag, mag,npts);
alpharange=[alpharange 0];

alpharange=linspace(.75,1,npts);


qmax=zeros(1,npts);
qmin=zeros(1,npts);

for j=1:length(alpharange);
      [qmax(j),qmin(j),xmax,xmin]=maxminquad(eigd,evecs,C,mu,alpharange(j),mag,optparam);
end

freg.hf=figure;
freg.xlabel='\mu_{\epsilon}';
freg.ylabel='\sigma';
freg.hp=zeros(1,2);
freg.lbls={};
hold on;
freg.hp(1)=plot(alpharange,qmax,'r.');
freg.lbls{1}='Max';
freg.hp(2)=plot(alpharange,qmin,'b.');
freg.lbls{2}='Min';
lblgraph(freg);

