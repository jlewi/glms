%make a plot of the inputs
for sind=1:length(simobj)
   fstim=FigObj('name','stimuli','title',simobj(sind).label,'xlabel','x1','ylabel','x2');
   
   x=getinput(simobj(sind).sr);
   x=[x.data];
   
   %plot first 100 stimuli in one color
   tstop=100;
   hp=plot(x(1,1:tstop),x(2,1:tstop),'ro','markerfacecolor','b');
   addplot(fstim.a,'hp',hp,'lbl','t=1:100');

   hp=plot(x(1,tstop+1:end),x(2,tstop+1:end),'bo','markerfacecolor','b');
   addplot(fstim.a,'hp',hp,'lbl',sprintf('t=%g:%g',tstop,simobj(sind).niter));   
   
      axis equal;
   lblgraph(fstim);
   

end