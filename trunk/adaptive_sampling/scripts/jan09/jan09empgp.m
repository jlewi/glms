%Date: 01-27-2009
%
%Explanation: For a simulation go back and estimate a Gaussian distribution
%   from the inputs actually picked

%clear variables;
%setpathvars;


[dsets,simobj]=jan09data2dgpbsstrfspikehisthighd();
simobj=simobj(5);
dsets=dsets(5);


%%
%get the inputs
stim=getinput(simobj.inputs);

%now we need to group the stimuli in batches of ktlength stimuli
ktlength=simobj.mobj.ktlength;
nbatches=floor(size(stim,2)/ktlength);

stim=stim(:,1:nbatches*ktlength);

%reshape stim taking mobj.klength*mobj.ktlength stimuli at a time
inputs=reshape(stim(:),ktlength*simobj.mobj.klength,nbatches);

%compute the mean and covariance matrix of our inputs

mus=mean(inputs,2);

zinputs=inputs-mus*ones(1,size(inputs,2));
cs=1/(size(inputs,2))*zinputs*zinputs';

%compute projection of mean on true theta
theta=simobj.observer.theta(1:simobj.mobj.nstimcoeff);
muproj=mus'/(mus'*mus)^.5*theta/(theta'*theta)^.5;



[evec,eigd]=svd(cs);
eigd=diag(eigd);
%%
fh=FigObj('name','Eigenvalues of Cs','xlabel','i','ylabel','log10(eigenvalue)','title','Eigenvalues of Cs');

plot(log10(eigd),'MarkerSize',3,'Marker','o','MarkerFaceColor','b');
