%play around with block circulant matrices

k=5;

%indexes for a circulant matrix
cind=nan(k,k);
for j=1:k
    
   cind(j,:)=abs([0:k-1]-(j-1))+1; 
end

%create a random symetric matrix m
m=randn(k,1);
%make toeplitz matrix
M=toeplitz(m');

%create a second random symetric matrix
A=randn(k,k);
A=A*A';



%create circulant matrix with same sufficient statistic
s=zeros(k,1);

% rind=[1:k]'*ones(1,k);
% coind=ones(k,1)*[1:k];
% 
% indmod=mod(abs((rind-coind)),k)+1;
% for delta=1:k
%     for i=1:k
%    s(delta)=s(delta)+A(i,abs(i-delta)+1);
%     end
% end

for delta=0:k-1

    s(delta+1)=sum(diag(A,delta));
    if (delta>0)
        s(delta+1)=s(delta+1)*2;
    end
end

%create a toeplitz matrix with the same sufficient statistic
a=1/2*s'./([k:-1:1]);
a(1)=a(1)*2;

At=toeplitz(a);

[m'*s trace(M*A) trace(M*At)]



[evecAt evalAt]=eig(At);
evalAt=diag(evalAt);

bfun=@(lambda)(1./(lambda-evalAt));

Cs=evecAt*diag(bfun(7.5))*evecAt'
