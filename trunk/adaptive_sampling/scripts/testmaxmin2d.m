%9-30-06
% Test maxminquad.m

%
d=3;

mu=[1;1];
mu=ceil(rand(d,2)*10)/10
%C=diag([2 1]);
%[evecs, eigd]=eig(C);
%eigd=diag(eigd);
evecs=orth(rand(d,d));
eigd=ceil(rand(d,1)*10)/10;
[eigd ind]=sort(eigd,'ascend');
evecs=evecs(:,ind);
C=evecs*diag(eigd)*evecs';

%mu=mu/(mu'*mu)^.5;
mu=mu./(ones(d,1)*sum(mu.^2,1).^.5);
%mu needs to be orthogonal
mu=orth(mu);

beta=0;         %projection along second mu vector
mag=3.5;
optparam=[];

saveout=0;         %to save data and figs;
datetime=clock;
fname=seqfname(fullfile(RESULTSDIR,'intreg', sprintf('%2.2d_%2.2d_intreg.mat',datetime(2),datetime(3))));
vtosave.qmax='';
vtosave.qmin='';
vtosave.C='';
vtosave.mu='';
vtosave.mag='';


%***************************************************
%range of alpha to compute max and min over
npts=50;
alpharange=linspace(-mag, mag,npts);
alpharange=[alpharange 0];
%alpharange=0;

%alpharange=linspace(.75,1,npts);


qmax=zeros(1,npts);
qmin=zeros(1,npts);

for j=1:length(alpharange);
      [qmax(j),qmin(j),xmax,xmin]=maxminquad(eigd,evecs,C,mu,[alpharange(j) beta],mag,optparam);
end

%*****************************************************************
%compute the correct values
cvals=[];
cvals.qmax=zeros(1,npts);
cvals.qmin=zeros(1,npts);

muorth=null(mu');
for j=1:length(alpharange);
    
    powercon=mag^2-alpharange(j)^2;
    
    fmaxobj=@(m)(-(m^2*muorth'*C*muorth+2*[alpharange(j)*mu(:,1)+beta*mu(:,2)]'*C*m*muorth));
    fminobj=@(m)((m^2*muorth'*C*muorth+2*[alpharange(j)*mu(:,1)+beta*mu(:,2)]'*C*m*muorth));
    %[mmax cvals.qmax(j)] = fminbnd(fmaxobj,-powercon^.5,powercon^.5);
    [mmax] = fminbnd(fmaxobj,-powercon^.5,powercon^.5);
    xmax=[alpharange(j)*mu(:,1)+beta*mu(:,2)]+muorth*mmax;
    cvals.qmax(j)=xmax'*C*xmax;
    %[mmin cvals.qmin(j)]=fminbnd(fminobj,-powercon^.5,powercon^.5);
    [mmin]=fminbnd(fminobj,-powercon^.5,powercon^.5);
    xmin=[alpharange(j)*mu(:,1)+beta*mu(:,2)]+muorth*mmin;
    cvals.qmin(j)=xmin'*C*xmin;
    
end


freg.hf=figure;
freg.xlabel='\mu_{\epsilon}';
freg.ylabel='\sigma';
freg.hp=zeros(1,2);
freg.lbls={};
freg.linewidth=1;
freg.name='Integration Region';
datetime=clock;
freg.fname=seqfname(fullfile(RESULTSDIR,'intreg', sprintf('%2.2d_%2.2d_intreg.eps',datetime(2),datetime(3))));
hold on;
freg.hp(1)=plot(alpharange,qmax,'r.');
freg.lbls{1}='Max';
freg.hp(2)=plot(alpharange,qmin,'b.');
freg.lbls{2}='Min';

pind=2+1;
[s ind]=sort(alpharange);
freg.hp(pind)=plot(alpharange(ind),cvals.qmax(ind),getptype(pind,2));
freg.lbls{pind}='Max Correct';


pind=pind+1;
freg.hp(pind)=plot(alpharange(ind),cvals.qmin(ind),getptype(pind,2));
freg.lbls{pind}='Min Correct';
freg=lblgraph(freg);
fill([alpharange(ind) alpharange(ind(end:-1:1))],[qmax(ind) qmin(ind(end:-1:1))],3);

if (saveout~=0)
%save the Graph
exportfig(freg.hf,freg.fname,'bounds','tight','Color','bw');
%save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(fname,vtosave,saveopts);
end