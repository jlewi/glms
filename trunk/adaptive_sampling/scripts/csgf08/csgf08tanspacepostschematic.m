%03-12-2008
%
%This script makes a schematic illustrating how we compute the posterior on
%the tangent space.

%dimensions for paper
fschem=FigObj('name','TanSpace Posterior Schematic','width',10,'height',4,'naxes',[1 3]);
fontsize=14;
%position of the bottom for the 2nd ro
position.bottom=.1;
position.gheight=.22;


lwidth=4;% - line thickneses
%how big to make the height as % of
%***********************************************



%generate the x, y points
pts=[-2:.05:2];
[x,y]=meshgrid(pts,pts);


view=[14 18];

%properties for the graphs
gprop.zl=[0 .16]; %limits for the plots of the probability

%for the true posterior we want it to be some log-concave function
%so we set the log probability to a quadratic function
%we use a quadratic function
%because we want it to be some concave function
tfullpost.xcent=0;
tfullpost.ycent=0;
tfullpost.height=2; %The value of z of our quad function when x=xcent and y=ycent
tfullpost.rzero=1;  %The distance from the center at which we want the quad function 
                    %to have decayed to zero
                    
                    
                    %compute the x,y pts which define the manifold. That is we compute y as a
%function of x
%let the manifold be cbic function
%take a cubic through zero and shift it up and right 1
%so that the projection of the MAP is at 1,1
%the MAP is at zero
manifold.xman=pts;
%manifold.yman=(pts+1).*(pts).*(pts-1)+1;
%specify the 4 points and then solve for them
px=[-.5 0 .5 1]';
py=[0 1 .5 0]';
X=[px.^3 px.^2 px ones(4,1)];
pcoef=inv(X)*py;
manifold.yman=pcoef(1)*pts.^3+pcoef(2)*pts.^2+pcoef(3)*pts+pcoef(4);
manifold.zman=ones(1,length(pts));

%********************************************************************
%2nd Row - Gaussian Approximation of posterior on full space 
%***********************************************************************
%Plot in the first column is the true posterior but ignoring 
%the Manifold information. Use a quadratic function

fullgauss.x=x;
fullgauss.y=y;

%compute the gaussian 
fullgauss.x=x;
fullgauss.y=y;

fullgauss.mu=[tfullpost.xcent ;tfullpost.ycent];
fullgauss.c=.25*eye(2,2);

fullgauss.prob=mvgauss([fullgauss.x(:)'; fullgauss.y(:)'],fullgauss.mu,fullgauss.c);
fullgauss.prob=reshape(fullgauss.prob,size(fullgauss.y,1),size(fullgauss.x,2))

rind=1;
cind=1;
fschem.a=setfocus(fschem.a,rind,cind);

%make a surface plot of the true posterior
fullgauss.h=surf(fullgauss.x,fullgauss.y,fullgauss.prob);

%adjust properties of the surface
set(fullgauss.h,'EdgeColor','none');
set(fschem.a(rind,cind),'xticklabel',[]);
set(fschem.a(rind,cind),'yticklabel',[]);
set(fschem.a(rind,cind),'zticklabel',[]);

%turn on the grid
set(fschem.a(rind,cind),'xgrid','on');
set(fschem.a(rind,cind),'ygrid','on');
set(fschem.a(rind,cind),'zgrid','on');



%add labels
fschem.a(rind,cind)=xlabel(fschem.a(rind,cind),'$\theta_1$','interpreter','latex');
fschem.a(rind,cind)=ylabel(fschem.a(rind,cind),'$\theta_2$','interpreter','latex');
fschem.a(rind,cind)=title(fschem.a(rind,cind),'$p(\vec{\theta}|\mu_t,C_t)$','interpreter','latex');

%set the view point
set(gca,'view',view);


%***************************************************************
%Middle show the manifold and the tangent space 
%***************************************************************
%%
%we need to compute the tangent space
%1. start by defining the projection of the mean onto the manifold
tanspace.muproj=[.5;.5];

%2. define the x,y pts corresponding to the tangent space
%   for the quadratic manifold, these pts are just a flat line
tanspace.xtan=pts;
m=3*pcoef(1)*tanspace.muproj(1).^2+2*pcoef(2)*tanspace.muproj(1)+pcoef(3);
b=tanspace.muproj(2)-m*tanspace.muproj(1);
tanspace.ytan=m*pts+b;

rind=1;
cind=2;
fschem.a=setfocus(fschem.a,rind,cind);
hold on;

%set the limits before we draw the arrow otherwise we have problems
xlim([-.5 1.5]);
 ylim([-.5 1.5]);
%plot a surface representing a delta function on the tangent space
%manifold.h=plot3(manifold.xman,manifold.yman,manifold.zman);
%plot the manifold as a patch
xpts=[tanspace.xtan tanspace.xtan(end:-1:1)];
ypts=[tanspace.ytan tanspace.ytan(end:-1:1)];
zpts=[ones(1,length(pts)) zeros(1,length(pts))];

%tanspace.hdelta=patch(xpts',ypts',zpts','b');

%plot the tangent space
tanspace.htan=plot(tanspace.xtan,tanspace.ytan,'r');
set(tanspace.htan,'linewidth',lwidth);

%plot the manifold
tanspace.hman=plot(manifold.xman,manifold.yman);
set(tanspace.hman,'linewidth',lwidth);

%adjust properties of the surface
%set(fschem.a(rind,cind),'xticklabel',[]);
%set(fschem.a(rind,cind),'yticklabel',[]);

%turn on the grid
set(fschem.a(rind,cind),'xgrid','off');
set(fschem.a(rind,cind),'ygrid','off');

%title
fschem.a(rind,cind)=xlabel(fschem.a(rind,cind),'$\theta_1$','interpreter','latex');
fschem.a(rind,cind)=ylabel(fschem.a(rind,cind),'$\theta_2$','interpreter','latex');
%fschem.a(rind,cind)=title(fschem.a(rind,cind),'$\delta(\theta\in\mathcal{T}_{\mu_M}\mathcal{M})$','interpreter','latex');

%zlim([0 2]);
%set(gca,'ztick',[0 1]);

%add labels
%plot the location of the full mean
tanspace.hmu=plot(fullgauss.mu(1),fullgauss.mu(2),'o','markerfacecolor','b');
set(tanspace.hmu,'markersize',8);
tanspace.hmutxt=text(fullgauss.mu(1)-.1,fullgauss.mu(2)+.15,'$\mu_t$');
set(tanspace.hmutxt,'interpreter','latex');

%plot the location of the mean projected on the manifold
tanspace.hmuproj=plot(tanspace.muproj(1),tanspace.muproj(2),'o','markerfacecolor','g');
tanspace.hmuprojtxt=text(tanspace.muproj(1),tanspace.muproj(2)+.15,'$\mu_{M}$');
set(tanspace.hmuprojtxt,'interpreter','latex');
set(tanspace.hmuproj,'markersize',8);

tanspace.hmantxt=text(1.1,.85,'$\mathcal{M}$');
set(tanspace.hmantxt,'interpreter','latex');

%draw an arrow between \mu_t and \mu_M
%draw an arrow
tanspace.harrow=arrow([fullgauss.mu;0],[tanspace.muproj;0],'LineStyle','--');
%set(gca,'view',view);

%label the tangent space
%tanspace.htantxt=text(.15,1.2,'$T_{\mu_M} \mathcal{M}$');
tanspace.htantxt=text(.15,1.2,'$T\mathcal{M}$');
set(tanspace.htantxt,'interpreter','latex');


 
 set(gca,'xtick',[]);
 set(gca,'ytick',[]);
 
%***************************************************************
%Bottom row final: gaussian on the tangent space
%***************************************************************
%%

%points on the manifold
tangauss.x=tanspace.xtan;
tangauss.y=tanspace.ytan;
tangauss.B=[1;0];  %basis vector of the tangent space
tangauss.bproj=tangauss.B'*[tangauss.x;tangauss.y]; %projection of x,y onto the basis vector of the tangent space

%compute the gaussian 
tangauss.mu=0;
tangauss.c=.25;

tangauss.prob=mvgauss(tangauss.bproj,tangauss.mu,tangauss.c);


rind=1;
cind=3;
fschem.a=setfocus(fschem.a,rind,cind);
hold on;

%make a plot of the Gaussian posterior 
%a mesh plot 
tangauss.hprob=plot3(tangauss.x,tangauss.y,tangauss.prob);
set(tangauss.hprob,'linewidth',lwidth);

% [x,y]=meshgrid(pts,pts);
% z=zeros(size(x));
% 
% ndiag=length(diag(z));
% 
% vdiag=(1:ndiag)-(1+ndiag)/2;
% zp=normpdf(x,zeros(1,ndiag),ones(1,ndiag));
% z=diag(zp);

%meshgrid(x,y,z);

%plot the manifold
%tangauss.hman=plot(manifold.xman,manifold.yman);


%draw an arrow and label it with the covariance
%we place the line at a value of sigma along the gaussian
% bval=(tangauss.c*2);
% %comput the x,y pts for the arrow by multiplying by the basis vector
% apts=tangauss.B*[-(bval-.05) (bval-.05)]+tanspace.muproj*ones(1,2);
% tangauss.hacb=arrow([apts(:,1);mvgauss(bval,tangauss.mu,tangauss.c)],[apts(:,2);mvgauss(bval,tangauss.mu,tangauss.c)],'LineStyle',':','length',5,'ends','both');
% arrow fixlimits;

%tick labels
set(fschem.a(rind,cind),'xticklabel',[]);
set(fschem.a(rind,cind),'yticklabel',[]);
set(fschem.a(rind,cind),'zticklabel',[]);

%turn on the grid
set(fschem.a(rind,cind),'xgrid','on');
set(fschem.a(rind,cind),'ygrid','on');
set(fschem.a(rind,cind),'zgrid','on');



%add labels
fschem.a(rind,cind)=xlabel(fschem.a(rind,cind),'$\theta_1$','interpreter','latex');
fschem.a(rind,cind)=ylabel(fschem.a(rind,cind),'$\theta_2$','interpreter','latex');
fschem.a(rind,cind)=title(fschem.a(rind,cind),'$p(\vec{\theta}|\mu_b,C_b)$','interpreter','latex');


%add text label for cb and mub
% tangauss.hcbtxt=text(mean(apts(1,:)),mean(apts(2,:)),mvgauss(bval,tangauss.mu,tangauss.c),'C_b');

%set the view point
set(gca,'view',view);

%**************************************************************************
%Position the axes
%*************************************************************************


%**************************************************************************
%Add text labels
%**************************************************************************
%we want to add text labels in between the plots
%to do this we create a blank axes whose inner position takes up the whole
%figure and whos limits are 0-1
tlbls.hatxt=axes('position',[0 0 1 1],'xlim',[0 1],'ylim',[0 1]);
axis off;











alltext=findall(gethf(fschem),'type','text');
set(alltext,'FontSize',fontsize);

saveas(gethf(fschem),'~/svn_trunk/adaptive_sampling/writeup/csgf08/tanpostschematic.eps','epsc2');
%saveas(gethf(fschem),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/tanpostschematic.png');