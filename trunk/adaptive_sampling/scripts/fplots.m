%12-02
%script to make plots of fcurves - which is quantity we want to maximize
close all;
clear all;
setpaths;
pg.m=[1;0];
pg.c=[10 0; 0 1];
%************************************************************************
%make a 2d plot
param.xrange=[-3 3; -3 3];
[fx,xpts]=fcurve(pg,param);

%for debugging
%figure;
%p=mvgauss(xpts,pg.m,pg.c);
%numpts=500;
%pmat=reshape(p,numpts,numpts);
%hold on
%plot subspace spanned by e1


%***************************************************
%make a plot of fisher information on the unit circle
%****************************************************
fisherucirc;

%********************************************************
%make a plot of fisher information as function of lambda
%**********************************************************
fisherlambda;

%***********************************************************************
%Compute the maximium by using jensen's inequality
%to get an expression for the exponent of F(x) which we can maximize
%directly
%************************************************************************
%b is the constraint on the magnitude of x
%it is the magnitude squared 
%code below doesn't work
return;
b=1;

%gi is function of the eigenvalues
gi=eval'/2+log(eval')/b;


evmu=evec'*pg.m;       %projection of mean onto eigen basis

%the langrange multiplier is a quadratic equation
%below are the equations for the coefficents of the quadratic equation
qa=dimstim*4;
qb=8*sum(gi);
qc=4*sum(gi.^2)-sum(evmu.^2)/b;

lgm1=(-qb+(qb^2-4*qa*qc)^.5)/(2*qa);

%approximate maximum of x in terms of the eigen basis
ymax=-evmu./(2*gi'+2*lgm1);

%convert to regular coordinates
xmax=evec*ymax;


