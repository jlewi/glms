%11-28-2007
%This script goes back and projects the posterior onto the tangent space

dsets=[];

dind=0;
dind=dind+1;
dsets(dind).infile=fullfile(RESULTSDIR,'tanspace','071128','gabortan_001.mat');
dsets(dind).simvar='rand';

fsuffix='_tanpost';

%type of tanspace and any parameters
tantype='GaborTanSpace';
tparams=[];

for dind=1:length(dsets)
   simobj=SimulationBase('fname',dsets(dind).infile,'simvar',dsets(dind).simvar);
    
    %get the full posterior
    fullpost=getpost(simobj); 

    updater=TanSpaceUpdater();
    
    for t=1:length(postobj)
       %project the full posterior onto the tangent space
       [tanspace,tanpost]=comptanpost(updater,fullpost(t),tantype,tparams);
       
       %compute the TanSpacePostObject
        allpost(t)=PostTanSpace('fullpost',fullpost(t),'tanpost',tanpost,'tanspace',tanspace);
    end
    
    %set the posteriors of the simobj
    simobj=setpost(simobj,allpost);
    
    [fdir, fname, fext]=fileparts(dsets(dind).infile);
    dsets(dind).outfile=fullfile(fdir,[fname fsuffix fext]);
    savesim(simobj,dsets(dind).outfile);
end