%11-24-2007
%   Test the TanSpacePost object and PostTanUpdat

%********************************************************
%0. create the true theta in the tanspace 
%**********************************************************
ptrue=PolyTanSpace('dimtheta',10,'sparam',[0 1]);

glm=GLMModel('poisson','canon');


%*********************************************************
%1. Create the Prior 
%**************************************************************
dimtheta=10;
fprior=GaussPost('m',zeros(dimtheta,1),'c',eye(dimtheta));

tparam.dimsparam=getdimsparam(ptrue);
prior=PostTanSpace('fullpost',fprior,'tanspacetype','PolyTanSpace','tanparam',tparam);

%**************************************************************
%2. create some fake data
%***************************************************************
niter=10;
clear sr;


%Try constructing the tanspace for a theta which is really a gabor.
%Make sure we can recover its parameters;


%check projection of theta yields correct value
if any(abs(diff((getsparam(ptrue)-proj(ptrue,gettheta(ptrue)))))>10^-8)
    eror('Projection of theta does not equal stored value');
end


tn=gettheta(ptrue)+2*randn(getdimtheta(ptrue),1);

pfit=PolyTanSpace('theta',tn,'dimsparam',getdimsparam(ptrue));

figure;
hold on;
hp=plot([1:getdimtheta(ptrue)]',gettheta(ptrue),'r',[1:getdimtheta(ptrue)]',tn,'b',[1:getdimtheta(ptrue)]',gettheta(pfit),'g');
legend(hp,{'true','noisy','fitted'});