%test the tan space for a sinusoidal function
%
%Try constructing the tanspace for a theta which is really a sinewave.
%Make sure we can recover its parameters;

%
dimtheta=51;
A=2.5;
freq=2;
omega=2*pi*freq;
phi=0;

sp= [A; omega; phi];

strue=SinTanSpace('sparam',sp,'dimtheta',dimtheta);

sproj=proj(strue,gettheta(strue));
sfit=SinTanSpace('sparam',sproj,'dimtheta',dimtheta);

figure;
t=linspace(0,1,getdimtheta(strue));
hp=plot(t',gettheta(strue),'r',t',gettheta(sfit),'b');
legend(hp,{'true','fit'});


%********************************************************************
%Now try a noise corrupted version
%%
tnoisy=gettheta(strue)+.5*randn(getdimtheta(strue),1);
snoisy=SinTanSpace('theta',tnoisy);

figure;
t=linspace(0,1,getdimtheta(strue));
hp=plot(t',tnoisy,'r',t',gettheta(snoisy),'b');
legend(hp,{'noisy','projection'});