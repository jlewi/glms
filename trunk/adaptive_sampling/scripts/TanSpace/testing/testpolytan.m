%test the tan space for a gabor function
%
%Try constructing the tanspace for a theta which is really a gabor.
%Make sure we can recover its parameters;

ptrue=PolyTanSpace('dimtheta',10,'sparam',[0 1 1]);

%check projection of theta yields correct value
if any(abs(diff((getsparam(ptrue)-proj(ptrue,gettheta(ptrue)))))>10^-8)
    eror('Projection of theta does not equal stored value');
end


tn=gettheta(ptrue)+2*randn(getdimtheta(ptrue),1);

pfit=PolyTanSpace('theta',tn,'dimsparam',getdimsparam(ptrue));

figure;
hold on;
hp=plot([1:getdimtheta(ptrue)]',gettheta(ptrue),'r',[1:getdimtheta(ptrue)]',tn,'b',[1:getdimtheta(ptrue)]',gettheta(pfit),'g');
legend(hp,{'true','noisy','fitted'});