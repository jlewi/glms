%1-28-2008
%FOR THE CANONICAL POISSON
%
%compute the average of the score function (the average log-likelihood with
%respect to the gaussian approximation of the posterior).

%co08datagammatones40d;
co08separable;

dind=1;
simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

glm=getglm(getmobj(simobj));

if ~iscanon(glm)
    error('Code assumes glm is canonical poission');
end

%%
%trials on which to compute the score function
%trials=[100:100:1900];
trials=[50:50:500];

%we need the covariance matrices
[simobj]=fillinlowmem(getupdater(simobj), simobj,trials);

data=[];

mobj=getmobj(simobj);
%%


%generate a test set 
%we generate the stimuli as follows. Randomly generate the projection along
%theta. Then pick a random vector and add it to theta. Then normalize it.
nstim=1000;
theta=gettheta(getobserver(simobj));
mmag=getmag(mobj);
ptheta=unifrnd(-mmag,mmag,[1,nstim]);
vptheta=normmag(normrnd(zeros(getstimlen(mobj),nstim),ones(getstimlen(mobj),nstim)));
vptheta=vptheta.*(ones(getstimlen(mobj),1)*(mmag^2-ptheta.^2).^.5);
inputs=normmag(theta)*ptheta+vptheta;
inputs=normmag(inputs)*mmag;

obsrv=observe(getobserver(simobj),inputs,[])';
%%
%generate the observations
data.fscore=zeros(1,length(trials));
data.tscore=zeros(1,length(trials));

for tind=1:length(trials);
   %get the observations up to this time
   %obsrv=getobsrv(simobj,[1:trials(tind)]);
   
   %getinputs and push them through any nonlinearities
   %inputs=projinp(mobj,getstim(simobj,[1:trials(tind)]));
   
   %get the full posterior
   post=getpost(simobj,trials(tind));
  
   %**********************************************
   %for full posterior
   %******************************************
   %get the full posterior
   fpost=getfullpost(post);

   mf=getm(fpost);
  dpf=mf'*inputs;
  crossf=qprod(inputs,getc(fpost));
  
  data.fscore(tind)=mean(obsrv.*dpf-exp(dpf+crossf/2));
  
  %*************************************************
  %for posterior on tangent space
  %*************************************************
  tpost=gettanpost(post);
  m=getm(tpost);
  dp=m'*inputs;
  cross=qprod(inputs,getc(tpost));
  
  data.tscore(tind)=mean(obsrv.*dp-exp(dp+cross/2));
end


%**************************************************************
%plot
%%

fh=FigObj('width',4,'height',4,'name','tanspace score');
fh.a=AxesObj('rows',1,'cols',1,'xlabel','trial','ylabel','\Delta score=score_{tan}-score_{full}');

hold on;

%plot fscore
pind=1;
hp=plot(trials,data.tscore-data.fscore);
fh.a=addplot(fh.a,'hp',hp);

lblgraph(fh);

onenotetable({fh;{'script','tanspacescore.m';'data', getpath(dsets(dind).fname);'simvar',dsets(dind).simvar}},seqfname('~/svn_trunk/notes/tanscore.xml'));

