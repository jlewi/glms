%11-29-2007
%
%Look at the projection of the infomax stimuli along the basis vectors
%   to see if we can find any structure

dsets=[];
dind=1;
 dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071128','gabortan_001.mat');
 dsets(dind).simvar='infomax';
 
 data=[];
 
 simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);
 
 %for each trial project the stimuli onto the gradient
 data.mproj=zeros(1,getniter(simobj));
 data.gproj=zeros(getdimsparam(gettanspace(getpost(simobj,1))),getniter(simobj));
 
 for t=1:getniter(simobj)
    stim=getData(getinput(getsr(simobj,t)));
    
    post=getpost(simobj,t);
    tanpost=gettanpost(post);
    tanspace=gettanspace(post);
    data.mproj(1,t)=(stim'*normmag(gettheta(tanspace)))/(stim'*stim)^.5;
    
    tanspace=gettanspace(getpost(simobj,t));
    grad=gradsubmanifold(tanspace,getsparam(tanspace));
    grad=normmag(grad);
    data.gproj(:,t)=grad'*stim/(stim'*stim)^.5;
 end
 
 glabels=getsplabels(tanspace);
 %*************************
 %plot
 fproj=FigObj('name','Stim Projections','width',5,'height',4);
 fproj.a=AxesObj('xlabel','trial','ylabel','Projection (fraction of magnitude)');
 
 %plot the mean
 hp=plot(1:getniter(simobj),data.mproj);
 fproj.a=addplot(fproj.a,'hp',hp,'lbl','\mu sub.');
 
 %gradient
 for gind=1:size(data.gproj,1);
 hp=plot(1:getniter(simobj),data.gproj(gind,:));
 fproj.a=addplot(fproj.a,'hp',hp,'lbl',glabels{gind});
 end
 
fproj=lblgraph(fproj);
 

descr='Projection of the stimulus along the mean and unit vectors in directions of partial derivatives with respect to the parameters of the tangent space. Projection is given as the fraction of the magnitude';
odata={fproj,{'Data', dsets(dind).fname;'Simvar',dsets(dind).simvar;'script','tanstimanalyze'; '\mu sub','Projection of stimulus along the projection of the MAP  into the submanifold';'Description' , descr}};

onenotetable(odata,seqfname('~/svn_trunk/notes/tanstimanalyze.xml'));
 