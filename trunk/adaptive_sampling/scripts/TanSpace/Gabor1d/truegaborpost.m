%compute the probability of a Gabor numerically so we can see what the true
%probability looks like
%
%decrease the number of theta vals to make it run faster decrease the
%number of theta vals
%simulation to process

%3-11-2008
%   This code uses my new method for computing the posterior on the tangent
%   space
%Results for cosyne
xscript('/home/jlewi/svn_trunk/allresults/tanspace/080311/co08gabors1dAC_080311_001.m');


%co08gabors1dAC;

%what trials to do computation on
trials=[30 40 60];
%results=[];

for dind=1
    simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

    glm=getglm(getmobj(simobj));
    extra=getextra(simobj);
    if ~isempty(extra)
        mparam=extra.mparam;
        gptrue=mparam.gp;
        omega=mparam.omega;
        sigmasq=mparam.sigmasq;
    else
        post=getpost(simobj,0);
        tspace=gettanspace(post);
        param=getparam(tspace);
        sigmasq=param.sigmasq;
        omega=param.omega;

    end
    dimtheta=length(mparam.ktrue);





    %*******************************************************
    %create a matrix on which to store and compute the probability of theta
    %%
    x=[1:dimtheta]-(1+dimtheta)/2;
    thetavals=-1:.01:3;

    ptheta=zeros(length(thetavals),length(x),length(trials));

    for trialind=1:length(trials)
        trial=trials(trialind)
        for tind=1:length(thetavals);
            for xind=1:length(x);

                %thetavals- values of theta for which we compute the probability;
                ptheta(tind,xind,trialind)=liketanthetagabor(x(xind),thetavals(tind), getdata(getstim(simobj,[1:trial])),getobsrv(simobj,[1:trial]),glm,omega,sigmasq);
            end
        end
        %normalize each column because it corresponds to a marginal
        %distribution
        ptheta(:,:,trialind)=ptheta(:,:,trialind)./(ones(length(thetavals),1)*sum(ptheta(:,:,trialind),1));
    end
    %%
%%
    %******************************************************************
    %Project the full posterior onto the tangent space and see what the
    %probability looks like
    %*******************************************************************


    %and the posterior on the tangent space
    ptanpost=zeros(length(thetavals),length(x),length(trials));


    tup=TanSpaceUpdater();

    for trialind=1:length(trials)
        trial=trials(trialind);
        %form a posterior on the full theta space
        fpost=getpost(simobj,trial);
        params.gpstart=mparam.gp;
        params.omega=mparam.omega;
        params.sigmasq=mparam.sigmasq;
        %[tanspace,tanpost]= comptanpost(tup,fpost,'GaborTanSpaceAC',params);

        %get the posterior on the tangent space
        tanpost=gettanpost(fpost);
        
        
        basis=getbasis(gettanspace(fpost));
        %compute the marginal likelihood
        %ptanpost(i,j,trialind)=p(\theta_j=thetavals(i))
        %this marginal distribution is Gaussian
        mfull=getm(getfullpost(fpost));
        for xind=1:length(x);
            sigmasq=basis(xind,:)*getc(tanpost)*basis(xind,:)';
            mu=basis(xind,:)*getm(tanpost)+mfull(xind);
            
            ptanpost(:,xind,trialind)=-1/(2*sigmasq)*(thetavals-mu).^2;
        end



        %normalize each column because it corresponds to a marginal
        %distribution
        ptanpost(:,:,trialind)=exp(ptanpost(:,:,trialind))./(ones(length(thetavals),1)*sum(exp(ptanpost(:,:,trialind)),1));

    end
%%
    %*************************************************************
    %compute the probability on these values using our full posterior
    pfullpost=zeros(length(thetavals),length(x),length(trials));

    for trialind=1:length(trials)
        trial=trials(trialind);
        %form a posterior on the full theta space
        fpost=getpost(simobj,trial);

        mfull=getm(fpost);
        cfull=getc(fpost);


        for xind=1:length(x);
            pfullpost(:,xind,trialind)=-1/(2*cfull(xind,xind))*(thetavals-mfull(xind)).^2;
        end

        %normalize each column because it corresponds to a marginal
        %distribution

        pfullpost(:,:,trialind)=exp(pfullpost(:,:,trialind))./(ones(length(thetavals),1)*sum(exp(pfullpost(:,:,trialind)),1));
    end
    %*****************************************************************
    %add the results to a structure
    results(dind).pfullpost=pfullpost;
    results(dind).ptanpost=ptanpost;
    results(dind).ptheta=ptheta;
end
%
%%
%plot the probability

%parameters for the axes
fontsize=10;
for dind=1:length(results)
    fpost=FigObj('name',sprintf('Comparing Posteriors: %s',dsets(dind).simvar),'width',5.5,'height',3.5,'fontsize',fontsize);
    fpost.a=AxesObj('nrows',length(trials),'ncols',3);

    ncountours=30;
  % clim=[-3*10^2 0];

    %limits of colorbar are set to account for fraction of cfrac of the data;
    cfrac=.9;
    for trialind=1:length(trials)
        trial=trials(trialind);
        %plot the true posterior
        fpost.a=setfocus(fpost.a,trialind, 1);

        ptheta=results(dind).ptheta;
        pfullpost=results(dind).pfullpost;
        ptanpost=results(dind).ptanpost;

        %plot it on log scale
        cind=1;

        for cind=1:3
            %select the appropriate data for the column
            if (cind==1)
                data=ptheta(:,:,trialind);
                %ttl=sprintf('p(\\theta_i|x_{1:t},r_{1:t}) for t=%d',trial);
                ttl=sprintf('log p(x_{1:t},r_{1:t}|\\theta_i)');
            elseif(cind==2)
                data=pfullpost(:,:,trialind);
%                ttl=sprintf('p(\\theta_i|\\mu_t,C_t) for t=%d',trial);
              ttl=sprintf('log p(\\theta_i|\\mu_t,C_t)');
            else
                data=ptanpost(:,:,trialind);
                %ttl=sprintf('p(\\theta_i|T(\\mu_t,C_t)) for t=%d',trial);
                ttl=sprintf('log p(\\theta_i|T(\\mu_t,C_t))');
            end

            %we plot the log of the data
            %set log 0 to nan
            ldata=log10(data);
            ind=find(isinf(ldata));
            ldata(ind)=nan;

            fpost.a=setfocus(fpost.a,trialind, cind);
            imagesc(x,thetavals,ldata);
            %        [c hcon]=contourf(x,thetavals,ldata,ncountours);
            %       set(hcon,'Edgecolor','none');
            set(gca,'ydir','normal');

          %  cl=detclim(data,cfrac);
          %  if ((cl(2)-cl(1))==0)
          %     cl=clim; 
          %  end
           % set(gca,'clim',cl);
      %     set(gca,'clim',clim);

            axis tight;

            hc= colorbar;
            fpost.a(trialind,cind)=sethc(fpost.a(trialind,cind),hc);


            set(gca,'ytick',[]);
            set(gca,'xtick',[]);

            %add titles

           

            if (trialind==1)
            fpost.a(trialind,cind)=title(fpost.a(trialind,cind),ttl);
            end
        end

    end

    %add xlabels
    for cind=1:3
        rind=length(trials);
        fpost.a(rind,cind)=xlabel(fpost.a(rind,cind),'i');
        setfocus(fpost.a,rind,cind);
        set(gca,'xtickmode','auto');
    end

    %add ylabels
    for rind=1:3
        cind=1;
        fpost.a(rind,cind)=ylabel(fpost.a(rind,cind),sprintf('t=%d\n\\theta_i',trials(rind)));
        setfocus(fpost.a,rind,cind);
        set(gca,'ytickmode','auto');
    end
    %adjust the color limits
    % for rind=1:length(trials)
    %     for cind=1:3
    %         fpost.a=setfocus(fpost.a,rind, cind);
    %         set(gca,'clim',clim);
    %     end
    % end

    fpost=lblgraph(fpost);
    fpost=sizesubplots(fpost);

    figs(dind)=fpost;
    fname=fullfile('~/svn_trunk/adaptive_sampling/writeup/cosyne08',sprintf('gaborac_%s.eps',dsets(dind).simvar));
    %saveas(gethf(fpost),fname,'epsc');
    explain='True theta is gabor with unknown amplitude and center. The left plot shows the true log-likleihood. Middle plot shows Gaussian posterior on full theta space. Last column shows posterior projected onto tangent space.';
    onenotetable({fpost;{'script','truegaborpost'; 'data file',getpath(dsets(dind).fname);'simvar',dsets(dind).simvar;'explanation',explain}},seqfname('~/svn_trunk/notes/truegaborpost.xml'));
end