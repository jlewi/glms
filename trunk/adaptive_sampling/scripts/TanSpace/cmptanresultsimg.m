%11-25-2007
%
%Make a plot of the estimated parameters on each trial

dsets=[];

% dind=1;
% dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071125','lowrank_009.mat');
% dsets(dind).simvar='rand';
% 
% dind=2;
% dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071125','lowrank_009.mat');
% dsets(dind).simvar='infomax';
% 
% dind=3;
% dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071126','lowrank_001.mat');
% dsets(dind).simvar='infomaxfull';
% data=[];

%***************************************************
%Sinusoids
dsets=[]
dind=0;
% dind=1;
%  dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071127','tanspace_002.mat');
%  dsets(dind).simvar='rand';

% dind=dind+1;
%  dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071127','tanspace_001.mat');
%  dsets(dind).simvar='infomax';

%  dind=3;
%  dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071127','tanspace_002.mat');
%  dsets(dind).simvar='tones';
%  
%  dind=4;
%  dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071127','tanspace_005.mat');
%  dsets(dind).simvar='infomaxfull';

 
 %***************************************************
%Gabors
% dsets=[]
% dind=0;
% 
% dind=dind+1;
%  dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071128','gabortan_001_tanpost.mat');
%  dsets(dind).simvar='rand';
% 
% dind=dind+1;
%  dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071128','gabortan_001.mat');
%  dsets(dind).simvar='infomax';
% % 
%   dind=dind+1;
%   dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071128','gabortan_001_tanpost.mat');
%   dsets(dind).simvar='infomaxfull';


%*********************************************************
%Low rank matrices
dsets=[]
dind=0;

dind=dind+1;
 dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071206','lowrank_001.mat');
 dsets(dind).simvar='rand';
 
 
dind=dind+1;
 dsets(dind).fname=fullfile(RESULTSDIR,'tanspace','071206','lowrank_001.mat');
 dsets(dind).simvar='infomax';
 
maxtrial=1000;


for dind=1:length(dsets)
    %[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
    simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);


        data(dind).ktrue=gettheta(getobserver(simobj));
    %compute the mse
    postobj=getpost(simobj);
    if isa(postobj,'PostTanSpace')
        fullpost=getfullpost(postobj);
        data(dind).fullm=getm(fullpost);


        %compute mse of estimate on the subspace
        tanspace=gettanspace(postobj);
        tanpost=gettanpost(postobj);
        tsubm=getm(tanpost);
        tm=submanifold(tanspace(1),tsubm);

        data(dind).tanm=tm;


    else
        data(dind).fullm=getm(postobj);

        %we need to project the full stimulus onto the submanifold
        data(dind).tanm=zeros(size(data(dind).fullm));
        for j=1:size(data(dind).fullm,2)
            data(dind).tanm(:,j)=submanifold(tanspace(1),proj(tanspace(1),data(dind).fullm(:,j)));
        end
    end
    %last trial to plot
    data(dind).tlast=min(maxtrial,getniter(simobj));
end

%**************************************************************************
%plot the Recovered values
%*************************************************************
%%


ffilt=FigObj('width',6.3,'height',4,'name','Estimated Parameters');
ffilt.a=AxesObj('nrows',2,'ncols',2*length(dsets));

%*********************************************
%plot the estimated filters
for dind=1:length(dsets)

    %plot the mean of the full posterior
    setfocus(ffilt.a,1,2*dind-1);

    imagesc(data(dind).fullm(:,1:data(dind).tlast)',cl);
    set(gca,'ydir','reverse');
    ffilt.a(1,2*dind-1)=title(ffilt.a(1,2*dind-1),sprintf('%s',dsets(dind).simvar));
    %turn off xtick and ytick
    set(gca,'xtick',[]);
    
    xlim([1 length(data(dind).ktrue)]);
    ylim([0 data(dind).tlast-1]);
    
    %set(gca,'ytick',[]);

    %plot the truth
    setfocus(ffilt.a,2,2*dind-1);
    imagesc(data(dind).ktrue');
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);

    xlim([1 length(data(dind).ktrue)]);
    %plot the mean of the projection on the submanifold posterior
    setfocus(ffilt.a,1,2*dind);

    if (size(data(dind).tanm,2)>1)
    imagesc(data(dind).tanm(:,1:data(dind).tlast)');
    set(gca,'ydir','reverse');
    ffilt.a(1,2*dind)=title(ffilt.a(1,2*dind),sprintf('%s:\n tan space',dsets(dind).simvar));
    %turn off xtick and ytick
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);

    xlim([1 length(data(dind).ktrue)]);
    ylim([0 data(dind).tlast-1]);
    %plot the truth
    setfocus(ffilt.a,2,2*dind);
    imagesc(data(dind).ktrue',cl);
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
    xlim([1 length(data(dind).ktrue)]);

    end
end

%make the color limits the same
cl=get(ffilt.a,'clim');
cl=cell2mat(cl);
cl=[min(cl(:,1)) max(cl(:,2))];
cl=[-.5 .5];
set(ffilt.a,'clim',cl);

%****************************************************************
%label graphs
%*******************************************************************
%add a colorbar
setfocus(ffilt.a,1,2*length(dsets));
hc=colorbar;
ffilt.a(1,2*length(dsets))=sethc(ffilt.a(1,2*length(dsets)),hc);
%left column
ffilt.a(1,1)=ylabel(ffilt.a(1,1),'Trial');
setfocus(ffilt.a,1,1);
set(gca,'ytickmode','auto');


ffilt.a(2,1)=ylabel(ffilt.a(2,1),'true');
for j=1:length(dsets)*2
    setfocus(ffilt.a,2,1);
    set(gca,'xtickmode','auto');
end


ffilt=lblgraph(ffilt);


ffilt=sizesubplots(ffilt,[],ones(1,2*length(dsets)),[.8 .2]);

odata={ffilt,{'Data',{dsets.fname}'; 'Script', 'cmptanresultsimg.m';'InfoMax Full', 'Infomax on full theta space as opposed to infomax on tangent space'}};
onenotetable(odata,seqfname('~/svn_trunk/notes/cmptanimg.xml'));

