%script to test that poollogdetjexp is working properly

dstim=2;
nstim=100;
mmag=rand(1)*10;
%random matrix of stimuli
stim=mmag*normmag(rand(dstim,nstim));

jeps=rand(1,nstim)*10;

popt=rand(1,nstim);
popt=popt/sum(popt);


%***********************************************************
%compute log|ExJexp| using the functions
%************************************************************
cmat=pooldetmat(stim,jeps);
comp.ldetjexp=poollogdetjexp(popt',cmat);

%**********************************************************
%compute it directly
true.jexp=zeros(dstim,dstim);

for j=1:nstim
   true.jexp=true.jexp+popt(j)*jeps(j)*stim(:,j)*stim(:,j)'; 
end
true.ldetjexp=log(det(true.jexp));


if (abs(true.ldetjexp-comp.ldetjexp)>10^-8)
    error('poollogdetjexp did not return correct value');
else
    fprintf('Sucess poollogdetjexp works \n');
end
    