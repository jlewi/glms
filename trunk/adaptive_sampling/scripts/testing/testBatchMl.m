%$Revision$: 06-14-2007
%      Created script to test BatchML in 2-d. I was having convergence
%      problems with the logistic model and genome data.
%

%Compute the Jacobian using the functions. Compare it to the value computed
%using a finite difference method
glm=GLMModel('binomial','canon');
%glm=GLMModel('poisson','canon');
epsvar=-10:10;
nepsvar=length(epsvar);
%generate some random observations
obsrv=(0:10:100)';
nobsrv=length(obsrv);
%need to be same length
dvec=min(nepsvar,nobsrv);
epsvar=epsvar(1:dvec);
nepsvar=dvec;
obsrv=obsrv(1:dvec);
nobsrv=dvec;


%**************************************************************************
%Compute the 2nd derivative using the functions
%******************************************************
[dfunc.deps,dfunc.d2eps]=d2glmeps(epsvar,obsrv,glm,'v');

d1func=@(x)d2glmeps(x,obsrv(1),glm,'v');
%****************************************************
%compute second derivative using finite difference method
%**********************************************************