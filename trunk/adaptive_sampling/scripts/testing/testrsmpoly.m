%script to test the RSMutualRsm
%
%Test the code by fitting a polynomial to some actual polynomial data

x=[1:20];
y=[21:30];

%the data we want to fit
fxy=ones(length(x),1)*y+x.^2'*ones(1,length(y));

rsxy=RSMutualInfo('dotmu',x,'sigmasq',y,'imutual',fxy,'degree',2);



fobj.hf=figure;
fobj.xlabel='x';
fobj.ylabel='y';
fobj.name='true data';
fobj.title=fobj.name;
contourf(x,y,fxy');
colorbar;
lblgraph(fobj);

fmi=miimg(rsxy,x,y); 
fmi.xlabel='x';
fmi.ylabel='y';
fmi.title='Fitted Polynomial';
lblgraph(fmi);

%check the coefficients
[rsxy.coef rsxy.cdegree]