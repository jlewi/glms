%11-20
%make an if (frequency vs. stimulus magnitude curve)
%goal is to try to show we can recover nonlinearities using spike history
%terms

%how many trials to run
strial=100;                                                         %start trial for assuming steadystate
trials=500;
%stim=[2:2:16];                                        %what values of the stimulus to use
stim=[-3:.25:2];

%specify what glm to use
simparam.glm.fglmnc=@glm1dexp;  %normalizing function for the distribution.
simparam.glm.fglmetamu=@etamupoiss;   %function to convert mean into canonical parameter depends on the distribution

simparam.glm.fglmmu=@glm1dexp;          %link funciton - funciton to generate the mean
simparam.glm.sampdist=@poissrnd;
%simparam.glm.fglmnc=@glm1dexp;  %normalizing function for the distribution.
%simparam.glm.fglmcf=@lincanon;      %mapping from linear input to canonical parameter
%simparam.nonlinf=@glm1dlog;
simparam.truenonlinf=@glm1dexp;   %the nonlinearity used to sample the data
%simparam.truenonlinf=@glm1dlog;   %the nonlinearity used to sample the data
%simparam.truenonlinf=@glm1dexp;   %the nonlinearity used to sample the data
simparam.sampdist=@poissrnd;            %distribution from which observations are sampled.


%model parameters
mparam.klength=1;
mparam.alength=15;
mparam.ktrue=1;         %just have one component for the stimulus
%mparam.atrue=[];
%force spike history to be inhibitory to prevent positive feedback
%make atrue a decaying exponential
%tdecay=-mparam.alength/log(.03);
%tdecay=-mparam.alength/log(.01);
%mparam.atrue=-[exp(-[0:(mparam.alength-1)]/tdecay)]';
%mparam.atrue=-1*[sin([0:mparam.alength-1]*pi/(mparam.alength-1))]';
mparam.atrue=-7/5*ones(mparam.alength,1);
%obsrvations
nspikes=zeros(length(stim),trials);

%**************************************************************************
%simulate the spikes for each stimulus
%************************************************************************
printiter=10;
for sind=1:length(stim)

    for tr=1:trials

        if (mod(tr-1,printiter)==0)
            fprintf('iteration %0.2g \n',tr);
        end

        %*****************************************************************
        %compute the spike history
        %******************************************************************
        %spike history is just the previous responses
        %we take them to be zero if they happened before the start of the
        %experiment
        %tr-2 is actual number of observations we have already made
        nobsrv=tr-1;

        %only compute shist if we have spike history terms
        if (mparam.alength >0)
            shist=zeros(mparam.alength,1);
            if (mparam.alength <= (nobsrv))
                shist(:,1)=nspikes(nobsrv:-1:nobsrv-mparam.alength+1);
            else
                shist(1:nobsrv,1)=nspikes(nobsrv:-1:1);
            end
        end
    
    %************************************************************
    %Full stimulus
    %**************************************************************
    %form the full stimulus by combining the spike history
    %and stim terms
    fullstim=[stim(sind);shist];

    theta=[mparam.ktrue;mparam.atrue];
    nspikes(sind,tr)=simparam.glm.sampdist(simparam.truenonlinf(theta'*fullstim));
end
end

%plot the results
fif.hf=figure;
fif.xlabel='Stimulus';
fif.ylabel='Firing Rate';
subplot(2,1,1);
hold on;
mrate=mean(nspikes(:,strial:end),2);
plot(stim,mrate);

%plot the frequency
threshold=10;
freq=zeros(1,length(stim));
freq=mean(nspikes(:,strial:end)>threshold,2);
subplot(2,1,2);
plot(stim,freq);