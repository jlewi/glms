%function taylorlogdet
%   d - dimension of the matrix A- can be an array
%       to do multiple dimensions
%   nmax - the maximum number of terms to use
%
%09-04-2008
%
%The purpose of this script is to investigate how well our taylor expansion
%(see batchopt_taylor) of log | 1+ A| converges to the true value
%We only generate matrices A with poisitve eigenvalues less than 1.
function [fh,taylor,mat]=taylorlogdet(d,nmax)

%**************************************************
%structure identifying the different matrices A
%**************************************************
mat=struct('d',[],'A',[],'logdet',[]);

for mind=1:length(d)
   evecs=orth(randn(d(mind),d(mind)));
   eigd=rand(d(mind),1);
   
   mat(mind).A=evecs*diag(eigd)*evecs';
   mat(mind).d=d(mind);
   mat(mind).logdet=log (det(eye(d(mind))+mat(mind).A));
end


%****************************************************
%generate the taylor expansions
%**********************************************************
%cumsum - sumultative sum of the terms
%rerr   - relative error of using an approximation with that many terms
taylor=struct('mind',[],'terms',[],'cumsum',[],'rerr',[]);

for mind=1:length(mat)
    taylor(mind).terms=zeros(1,nmax);
    matpow=eye(mat(mind).d);
    for tind=1:nmax
        matpow=matpow*mat(mind).A;
       taylor(mind).terms(tind)=(-1)^(tind-1)/tind *trace(matpow); 
    end
    taylor(mind).cumsum=cumsum(taylor(mind).terms);
    
    taylor(mind).rerr=abs(taylor(mind).cumsum-mat(mind).logdet)/abs(mat(mind).logdet);
end

%**************************************************************
%Make a plot
%*************************************************************
fh=FigObj('xlabel','# terms', 'ylabel','relative error','width',3,'height',3);

 hold on;
for tind=1:length(taylor)
   
    hp=plot([1:nmax],taylor(tind).rerr);
    addplot(fh.a,'hp',hp,'lbl',sprintf('d=%d',mat(tind).d));
end
xlim([1 nmax]);
lblgraph(fh)


