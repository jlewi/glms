%function saveresults(wtosave,where)
%   wtosave - what to save
%       'figs'-figs
%       'data'-data
%       -not supplied save both
%   where - where to save
%       'java' - save to my tablet
%
%   narg - name of an argument
%           'fig' - pass handle to a specific figure;
%   pparam - value 
%          hf   - name of figure structure
%
%Rather than using evalin for everything
%I should use evalin to transfer values from the base
%to this function
function saveresults(wtosave,where,narg,pparam)

if ~exist('wtosave','var')
    tosave.data=1;
    tosave.figs=1;
else
    if (strcmp(wtosave,'figs')==1)
        tosave.figs=1;
        tosave.data=0;
    end
    if (strcmp(wtosave,'data')==1)
        tosave.figs=0;
        tosave.data=1;
    end
end
if ~exist('where','var')
    wheresave.local=1;
    wheresave.tablet=1;
else
    if strcmp(where,'java')
        wheresave.tablet=1;
        wheresave.local=0;
    else
        if strcmp(where,'local')
            wheresave.tablet=0;
        wheresave.local=1;
        else
        wheresave.tablet=1;
        wheresave.local=1;
        end
    end
end

if ~exist('narg','var')
    tosave.figs=1;
    tosave.data=0;
    tosave.fighandle=1;
    fig=pparam;
end
%3-20-2006
%A quick script to save the data
%this allows you to run an experiment and decides after looking at data
%whether to save or not
%save options

%all of the evaluation happens in the base
%so that we don't have to pass any variables
evalin('base','saveopts.append=1;');  %append results so that we don't create multiple files
%each time we save data.
evalin('base','saveopts.cdir =1;'); %create directory if it doesn't exist

if (tosave.data==1)
    cmdstring='savedata(ropts.fname,vtosave,saveopts';
    evalin('base',cmdstring);
    fprintf('Saved to \n');
    evalin('base', 'ropts.fname;');
end




%save the graphs as well
%label all the figures
%create a structure of all possible graphs
if isfield(tosave,'fighandle')
    %save specific figure
    fignames={fig};
else
fignames={'fmdist','fentropy','fkl','fmse','ftiming','femax','ftheta','fmapdiff'};
end
numfigs=0;
for index=1:length(fignames)
    cmdstring=sprintf('exist(''%s'',''var'');',fignames{index});
    result=evalin('base',cmdstring);
    if (result>0)
        numfigs=numfigs+1;
        cmdstring=sprintf('%s;',fignames{index});
        figs{numfigs}=evalin('base',cmdstring);
    end
end
%get the trial number of the file
[PATHSTR,name,EXT,VERSN]=evalin('base','fileparts(ropts.fname);');
trialnum=name(end-2:end);   %this is a string not a number

if (tosave.figs==1)
    for index=1:length(figs)
        if (figs{index}.on~=0)
            figure(figs{index}.hf);

            %create filename for graph
            if ~(exist('datetime','var'))
                datetime=zeros(1,4);
                datetime(2)=str2num(name(1:2));
                datetime(3)=str2num(name(4:5));
            end
            
            
            popts.sdir=evalin('base','popts.sdir');
            %strip the directory of its base directory
            %(i.e the part of the path that depends on which
            %machine we're on
            bdir='adaptive_sampling';
            bind=strfind(popts.sdir,bdir);
            
            %relative path relative to our base project directory
            rpath=popts.sdir(bind+length(bdir)+1:end);
            
            if (wheresave.local==1)
                
                fname=fullfile(rpath,sprintf('%0.2d_%0.2d_%s_%s.eps',datetime(2),datetime(3),figs{index}.fname,trialnum));
                if exist(fname,'file')
                    fname=seqfname(fname);
                end
                savegraph(fname,figs{index}.hf);
            end
            if (wheresave.tablet==1)
                %if my java directory for figures is mounted
                %copy images there. This facilitates incorporating
                %them into gobinder
               
                if exist('/misc/java','dir')
                     javadir=fullfile('/misc','java','tmp','figures',sprintf('%0.2d_%0.2d',datetime(2),datetime(3)));
                %make sure mount exists
                    
                else
                    if exist('z:\tmp','dir')
                        javadir=fullfile('z:\','tmp','figures',sprintf('%0.2d_%0.2d',datetime(2),datetime(3)));

                            
                        end
   

                     
                end

                        % copyfile(fname,fullfile(javadir,[name EXT]));
                        jopts.cdir=1;   %create directory in tmp directory if it doesn't exist
                        %savegraph(fullfile(javadir,[name '.jpg']),figs{index}.hf,jopts);
                        jfname=fullfile(javadir,sprintf('%0.2d_%0.2d_%s_%s.jpg',datetime(2),datetime(3),figs{index}.fname,trialnum));
                                        
                        %check if directory exists
                        if ~(exist(javadir,'dir'))
                            fprintf('Warning: Directory %s does not exist. \n', javadir);
                            
                                fprintf('Creating: Directory \n');
                                r=mkdir(javadir);
                                if (r==0)
                                    fprintf('Could not create directory %s. \n', javadir);
                                    fprintf('Warning: Saving to working directory %s \n', pwd);
                                    javadir=pwd;
                                end
                        end
                           %need to use double quotes to properly include quotes
                        %cmdstring='exportfig(figs{index}.hf,),jfname,''bounds'',''tight'',''Color'',''cmyk''';
                        %assignin('base','jopts',jopts);
                        %assignin('base','jfname',jfname);
                        %evalin('base',cmdstring);
                        exportfig(figs{index}.hf,jfname,'bounds','tight','Color','cmyk','Format','jpeg');
                end
            end
        end
    end

end