%make plot of the number of iterations of newton's method to get
%convergence
figure;
subplot(2,1,1);
plot(pmax.newton.niter);
ylabel('Number Newton Steps');

subplot(2,1,2);
plot((pmax.newton.newtonerr));
ylabel('MSD');
xlabel('Iteration');

[u s v]=svd(pg.c);
v=u'*pg.m;

theta=pi/4;
%y=[cos(theta);sin(theta)];
y=u'*xpts(:,87);
lambda=diag(s)-v./y;