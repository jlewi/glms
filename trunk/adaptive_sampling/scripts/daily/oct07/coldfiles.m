%convert old files to new files and save them
dsets=[];

%using our heuristic with 1000 stimuli


dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','04_20','poolgabor2d_04_20_002.mat');
dsets(end).simvar='simmax';

 dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','04_23','poolgabor2d_04_23_001.mat');
 dsets(end).simvar='simmax';


dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','04_23','poolgabor2d_04_23_002.mat');
dsets(end).simvar='simmax';

dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001.mat');
dsets(end).simvar='simmax';

dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001.mat');
dsets(end).simvar='simrand';


dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_13','poolgabor2d_05_13_001.mat');
dsets(end).simvar='simmax';

 dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','05_16','gabor_05_16_001.mat');
 dsets(end).simvar='simmax';

 dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','05_16','gabor_05_16_002.mat');
 dsets(end).simvar='simmax';

 %*************************************************************************
 %Gammatone data
 dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_simmax.mat');
dsets(end).lbl='info. max. heuristic';
dsets(end).simvar='simmax';
%dsets(end).maxtrial=10000; %max trial to plot

dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001.mat');
dsets(end).lbl='info. max. i.i.d.';
dsets(end).simvar='simunif';
 
%**************************************************************************
%Misspecified models
%*************************************************************************
dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisspowercanon_001.mat');
dsets(end).simvar='simmax';

dsets(end+1).fname=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisspowercanon_001.mat');
dsets(end).simvar='simrand';

dsets(end+1).fname=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisslogcanon_001.mat');
dsets(end).simvar='simmax';

dsets(end+1).fname=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisslogcanon_001.mat');
dsets(end).simvar='simrand';
 %convert the old files and save them to a new file
 for dind=1:length(dsets)
          [simobj]=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);
          
          %newfile
          [pathstr,name,ext] = fileparts(dsets(dind).fname);
          
          fname=fullfile(pathstr,[name,'_newver',ext]);
          
          savesim(simobj,fname,true);
          sprintf('Saved converted version to file:\n %s \n',fname);
 end