u=zeros(1,2000);
u(1)=5;

for j=2:length(u)
   u(j)=1+(-exp(u(j-1))); fff
end

%%
%make a plot of the step sizes for our updates
m=getpostm(simnewt,[0:getniter(simnewt)]);

dm=diff(m,[],2);

fd.hf=figure;
fd.xlabel='Trial';
fd.ylabel='Diff';

plot(sum(dm.^2,1).^.5);

lblgraph(fd);

onenotetable({fd,'Newton1d'});f


%%
fr=@(dt)(dt/fglm(.5*m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.avgspike);
mmag=fsolve(fsetmmag,1,optim);


%**********************************************************
%integrate Er dt
%1-d eqn we need to solve numerically


%%
opts=[];
opts=optimset(opts,'TolX',10^-9);
tail=.0000005;

mu=5;
x=1;
glm=GLMModel('poisson','canon');


epstrue=1;

fglm=getfglmmu(glm);

%rbounds(:,1)=tglm.cdfinv(tail,glm.fglm(epstrue));
rbounds(:,1)=1;
rbounds(:,2)=glm.cdfinv(1-tail,fglm(mu));

r=floor(rbounds(1,1)):1:ceil(rbounds(1,2));

%for each r compute delta t
epsnew=zeros(1,length(r));
dt=zeros(1,length(r));

efinv=0;
for rind=1:length(r)
    efinv=efinv+log(r(rind))*glm.pdf(r(rind),fglm(mu));
end


%%
r=[0:20];

fd.hf=figure;
%plot the value of dt
sigma=1;
epso=1;


dt=zeros(1,length(r))
for rind=1:length(r)
    fdt=@(dt)(r(rind)-exp(epso+dt*sigma)-dt);
    dt(rind)=fsolve(fdt,0);
end