%
% I think the actual timing figure for the EMBS abstract was made by the
% timing fig.m script 
%
% THis script plots the running time on each iteration which is not what we
% want.
%run maxplots
%parameters for tracking entropy
whichdata=1;    %0 means tracking entrop
                          %1 means high d results
                          
tpind=1;

%timing
ftiming.hf=figure;
ftiming.hp=[];
ftiming.x=[1:simparam.niter];
ftiming.xlabel='Iteration';
ftiming.ylabel='Time(Seconds)';
ftiming.title='';
ftiming.name='Timing';
ftiming.on=1;
ftiming.fname='timing';
ftiming.semilog=1;
ftiming.axisfontsize=20;
ftiming.lgndfontsize=20;

for whichdata=0:1
if (whichdata==1)
  %  load(fullfile('results', 'tracking','03_31','03_31_maxtrack_003.mat'))
       %trackd=load(fullfile('results', 'tracking','04_02','04_02_maxtrack_002.mat'));
%    xlimits=[30 200];  %what xlimits to use
    monf{1}='newton1d'
    outfile=fullfile(pwd,'writeup','embs_abstract','tracktiming.eps');
    xlimits=[0 1000];
    mname='Changing';
    pmax=trackd.pmax;
    simparam=trackd.simparam;
else
    %for high d entropy

%    load('results\maxinfo\03_22\03_22_max2d_001.mat')
   % highd=load(fullfile('results', 'maxinfo','03_22','03_22_max2d_001.mat'));
  monf{1}='newton';

    outfile=fullfile(pwd,'writeup','embs_abstract','highdtiming.eps');
    mname='Constant'
    pmax=highd.pmax;
    simparam=highd.simparam;
end
outfile=fullfile(pwd,'writeup','embs_abstract','bothtiming.eps');
xlimits=[0  500];  %what xlimits to use

pmethods=pmax;
 if(ftiming.on~=0)
        figure(ftiming.hf);
        hold on
        subsamp=50;
        if isfield(pmethods.(monf{mindex}),'timing')
            tpind=length(ftiming.hp)+1;
            ftiming.lbls{tpind}=sprintf('\\theta %s: Update Posterior', mname);
            ftiming.hp(tpind)=plot(1:subsamp:simparam.niter,pmethods.(monf{mindex}).timing.update(1:subsamp:simparam.niter),getptype(tpind,1));
            ftiming.hpline(tpind)=plot(1:simparam.niter,pmethods.(monf{mindex}).timing.update,'k-');

            tpind=length(ftiming.hp)+1;
            ftiming.lbls{tpind}=sprintf('\\theta %s: Optimize Stimulus',mname);
            ftiming.hp(tpind)=plot(1:subsamp:simparam.niter,pmethods.(monf{mindex}).timing.optimize(1:subsamp:simparam.niter),getptype(tpind,1));
            ftiming.hpline(tpind)=plot(1:simparam.niter,pmethods.(monf{mindex}).timing.optimize,'k-');            

        end
 end
        
tpind=tpind+1;
end

lblgraph(ftiming);

figure(ftiming.hf);
xlim(xlimits);
hold on;
        
%figure(ftiming.hf);
for tind=1:(tpind-1);
set(ftiming.hpline(tind),'LineWidth',1);
set(ftiming.hp(tind),'MarkerSize',10);
set(ftiming.hp(tind),'Color',[0 0 0])

end
set(ftiming.hp(1),'Marker','s');

set(ftiming.hp(3),'Marker','v');
set(ftiming.hp(4),'Marker','*');

%savegraph('entropy.eps',fentopy.hf)
exportfig(ftiming.hf,outfile,'bounds','tight');
