%4_17_2006
%
% This script plots the running time for a variety of different sized
% unkown parameters
% it averages the time per iteration because the time per iteration should
% be constant
%
% an earlier version of this script was used to make the timing figure for
% the embs abstract.
%directory for files

%data files used for embs abstract
%tfiles={'04_17/04_17_maxtrack_001.mat','04_17/04_17_maxtrack_002.mat','04_17/04_17_maxtrack_004.mat'};
%tfiles{end+1}='04_17/04_17_maxtrack_006.mat';
%tfiles{end+1}='04_17/04_17_maxtrack_007.mat';
%tfiles{end+1}='04_17/04_17_maxtrack_008.mat';
%tfiles{end+1}='04_17_maxtrack_009.mat';
%tfiles{end+1}='04_18/04_18_maxtrack_002.mat'
tfiles={};
%tfiles{end+1}='05_21/05_21_maxtrack_001.mat';
%tfiles{end+1}='05_21/05_21_maxtrack_002.mat';

%set 2
%tfiles{end+1}='05_22/05_22_maxtrack_001.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_002.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_003.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_004.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_005.mat';
%
%tfiles{end+1}='05_22/05_22_maxtrack_006.mat';
%tfiles{end+1}='05_22/05_22_maxtrack_007.mat';
%
%set 3 for nips
tdir=fullfile(RESULTSDIR,'tracking');
tfiles{end+1}=fullfile(tdir,'05_30/05_30_maxtrack_004.mat');
tfiles{end+1}=fullfile(tdir,'05_30/05_30_maxtrack_005.mat');
tfiles{end+1}=fullfile(tdir,'05_30/05_30_maxtrack_006.mat');
tfiles{end+1}='05_30/05_30_maxtrack_007.mat';
tfiles{end+1}='05_30/05_30_maxtrack_008.mat';
tfiles{end+1}='05_30/05_30_maxtrack_009.mat';
tfiles{end+1}='05_30/05_30_maxtrack_010.mat';
tfiles{end+1}='06_01/06_01_maxtrack_001.mat';
%tfiles{end+1}='06_03/06_03_gabor_001.mat';
outfile=fullfile('writeup','nips','simtiming.eps');

%start trial
%specfies how many trials at the beginning to throw out
tstart=100;
dstart=1000;
%**************************************************************
%Files for new method 
%************************************************************
tfiles={};
tfiles{end+1}=fullfile(RESULTSDIR, 'aistat/10_15/10_15_timing_001.mat'); %100 d
tfiles{end+1}=fullfile(RESULTSDIR, 'aistat/10_15/10_15_timing_002.mat'); %klength= 300d
tfiles{end+1}=fullfile(RESULTSDIR, 'aistat/10_15/10_15_timing_003.mat'); %klength = 400d
tfiles{end+1}=fullfile(RESULTSDIR, 'aistat/10_15/10_15_timing_004.mat'); %klength = 500d
%tfiles{end+1}=fullfile(RESULTSDIR, 'aistat/10_15/10_15_timing_005.mat'); %klength = 600d
tfiles{end+1}=fullfile(RESULTSDIR, 'aistat/10_15/10_15_maxtrack_013.mat'); %klength = 200d
%tfiles{end+1}='06_03/06_03_gabor_001.mat';
outfile=fullfile(RESULTSDIR,'aistat/10_15/','simtiming.eps');
tstart=300;                                          %start trial
                                                            %specfies how many trials at the beginning to throw out
dstart=100;                                         %which dimensionality to use for fitting the data
%**************************************************************************

%ftiming.hf=figure;
ftiming.hf=figure;
ftiming.hp=[];
ftiming.x=[];
ftiming.xlabel='Dimensionality';
ftiming.ylabel='Time(Seconds)';
ftiming.title='';
ftiming.name='Timing';
ftiming.on=1;
ftiming.fname='timing';
ftiming.semilog=1;
ftiming.axisfontsize=30;
ftiming.lgndfontsize=30;
ftiming.linewidth=6;
ftiming.markersize=30;
ftiming.semilogx=0;


ftiming.font='Times'; %use a scalable font
set(ftiming.hf,'units','inches');
fpos=get(ftiming.hf,'position');
fpos(3)=3;
fpos(4)=2;
set(ftiming.hf,'position',fpos);
ftiming.axisfontsize=10;% was point 1
ftiming.lgndfontsize=10; 
ftiming.linewidth=2.5;
ftiming.markersize=12;
ftiming.fontunits='Points';
ftiming.font='Times'; %use a scalable font

%time for each step
nfiles=length(tfiles);
d=zeros(1,nfiles);              %dimensionality of the data for that trial
eigtime=zeros(1,nfiles);
eigtimestd=zeros(1,nfiles);
searchtime=zeros(1,nfiles);
searchtimestd=zeros(1,nfiles);
updatetime=zeros(1,nfiles);
updatetimestd=zeros(1,nfiles);

data={};

for findex=1:length(tfiles);
    %load the data
    data{findex}=load(tfiles{findex});
    
    d(findex)=data{findex}.mparam.klength;
    
    eigtime(findex)=mean(data{findex}.pmax.newton1d.timing.eigtime(tstart:end));
    eigtimestd(findex)=std(data{findex}.pmax.newton1d.timing.eigtime(tstart:end));
    
        searchtime(findex)=mean([data{findex}.pmax.newton1d.timing.searchtime(tstart:end).fishermax]);
            searchtimestd(findex)=std([data{findex}.pmax.newton1d.timing.searchtime(tstart:end).fishermax]);
            
            updatetime(findex)=mean(data{findex}.pmax.newton1d.timing.update(tstart:end));
            updatetimestd(findex)=std(data{findex}.pmax.newton1d.timing.update(tstart:end));

            totaltime(findex)=mean(data{findex}.pmax.newton1d.timing.update(tstart:end)+data{findex}.pmax.newton1d.timing.eigtime(tstart:end)+[data{findex}.pmax.newton1d.timing.searchtime(tstart:end).fishermax]);
            totaltimestd(findex)=std(data{findex}.pmax.newton1d.timing.update(tstart:end)+data{findex}.pmax.newton1d.timing.eigtime(tstart:end)+[data{findex}.pmax.newton1d.timing.searchtime(tstart:end).fishermax]);
            %clear data or we'll run out of memory
            clear data;
            
end

%sort the data
[d index]=sort(d);
eigtime=eigtime(index);
updatetime=updatetime(index);
searchtime=searchtime(index);
totaltime=totaltime(index);

eigtimestd=eigtimestd(index);
searchtimestd=searchtimestd(index);
updatetimestd=updatetimestd(index);
totaltimestd=totaltimestd(index);

%*******************************************************************
%Plot it


%ftiming.hf=figure;
figure(ftiming.hf);
hold on;

%index of first pt to use for fitting the data
istart=find(d>=dstart);
istart=istart(1);

%********************************************************
%total time
%*****************************************************
pind=1;
ftiming.hp(pind)=errorbar(d,totaltime,totaltimestd);
[c,mark]=getptype(pind,1);
set(ftiming.hp(pind),'Marker', mark);
set(ftiming.hp(pind),'LineStyle','none');
set(ftiming.hp(pind),'MarkerSize',ftiming.markersize*2);
set(ftiming.hp(pind),'LineWidth',2);

%fit total time to to a 2nd degree
%polynomial eventhough it really is O(n^3)
%do this to illustrate the speedp do to the rank 1 step
%[p,s] = POLYFIT(d,eigtime,3);
%x=[50:d(end)];
%y=p(1)*x.^3+p(2)*x.^2+p(3)*x + p(4);
[p,s]=polyfit(d(istart:end),totaltime(istart:end),2);
x=[dstart:d(end)];
y=p(1)*x.^2+p(2)*x+p(3);
fprintf('Total time fitted to 2nd degree polynomial\n');
fprintf('p(1)=%0.3g \t p(2)=%0.3g \t p(3)=%0.3g \n',p(1),p(2),p(3)); 
ftiming.hpline(pind)=plot(x,y,getptype(pind,2));
ftiming.lbls{pind}=sprintf('total');
set(ftiming.hpline(pind),'LineWidth',ftiming.linewidth);

%diagonalization
pind=pind+1;
ftiming.hp(pind)=plot(d,eigtime,getptype(pind,1));
%ftiming.hp(pind)=errorbar(d,eigtime,eigtimestd);
[c,mark]=getptype(pind,1);
set(ftiming.hp(pind),'Marker', mark);
set(ftiming.hp(pind),'LineStyle','none');
set(ftiming.hp(pind),'Color',c);
set(ftiming.hp(pind),'MarkerSize',ftiming.markersize*2);

%**********************************
%try fitting the diagonalization to a 2nd degree
%polynomial eventhough it really is O(n^3)
%do this to illustrate the speedp do to the rank 1 step
%[p,s] = POLYFIT(d,eigtime,3);
%x=[50:d(end)];
%y=p(1)*x.^3+p(2)*x.^2+p(3)*x + p(4);
[p,s]=polyfit(d(istart:end),eigtime(istart:end),2);
x=[100:d(end)];
y=p(1)*x.^2+p(2)*x+p(3);
fprintf('Diagonalization fitted to 2nd degree polynomial \n');
fprintf('p(1)=%0.3g \t p(2)=%0.3g \t p(3)=%0.3g \n',p(1),p(2),p(3)); 
ftiming.hpline(pind)=plot(x,y,getptype(pind,2));
set(ftiming.hpline(pind),'Linewidth',ftiming.linewidth);
ftiming.lbls{pind}=sprintf('eigen.');


%Update TIming
pind=pind+1;
ftiming.hp(pind)=plot(d,updatetime,getptype(pind,1));
%ftiming.hp(pind)=errorbar(d,updatetime,updatetimestd);
[c,mark]=getptype(pind,1);
set(ftiming.hp(pind),'Marker', mark);
set(ftiming.hp(pind),'LineStyle','none');
set(ftiming.hp(pind),'Color',c);
set(ftiming.hp(pind),'MarkerSize',ftiming.markersize);

%fit 2nd degree
[p,s] = POLYFIT(d(istart:end),updatetime(istart:end),2);
y=p(1)*x.^2+p(2)*x+p(3);
ftiming.hpline(pind)=plot(x,y,getptype(pind,2));
ftiming.lbls{pind}=sprintf('posterior');
set(ftiming.hpline(pind),'Linewidth',ftiming.linewidth);



%fit a third order polynomial
%ftiming.hpline(pind)=plot(d,eigtime,getptype(pind,2));

pind=pind+1;
ftiming.hp(pind)=plot(d,searchtime,getptype(pind,1));
%ftiming.hp(pind)=errorbar(d,searchtime,searchtimestd);
[c,mark]=getptype(pind,1);
set(ftiming.hp(pind),'Marker', mark);
set(ftiming.hp(pind),'LineStyle','none');
set(ftiming.hp(pind),'Color',c);
set(ftiming.hp(pind),'MarkerSize',ftiming.markersize);

%fit a linear polynomial
[p,s] = POLYFIT(d(istart:end),searchtime(istart:end),1);
y=p(1)*x+p(2);
ftiming.hpline(pind)=plot(x,y,getptype(pind,2));
ftiming.lbls{pind}=sprintf('optimize');
set(ftiming.hpline(pind),'Linewidth',ftiming.linewidth);

ftiming=lblgraph(ftiming);


figure(ftiming.hf);
%set the ticklabels otherwise if its a log graph it only prints the
%exponent

%ylimits=[updatetime(1) max(totaltime(end))];
xlim([100  500])

ytick=get(gca,'YTick');
ytick=[10^-2 .1 1];
set(gca,'ytick',ytick);
axis tight;
ylimits=[.5*10^-3 .5];
ylim(ylimits);
%yticklbl=['10^-4', '10^-2', '10^-1'];
%yticklbl=ytick;

%for aistat
gpos=get(gca,'Position');
gpos(3)=.5;
set(gca,'position',gpos);

%savegraph('entropy.eps',fentopy.hf)
%exportfig(ftiming.hf,outfile,'bounds','tight','Color','bw');
%exportfig(ftiming.hf,outfile,'bounds','tight','Color','rgb');


