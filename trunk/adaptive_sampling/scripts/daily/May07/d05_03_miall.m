%5-30-2007
%TO see why info. max. i.i.d is worse than i.i.d. try plotting the
%information
%of the stimuli from the two methods
dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
% dsets(end).lbl='info. max. heuristic';
% dsets(end).simvar='simmax';
% 
 dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
 dsets(end).lbl='info. max. i.i.d:10000';
 dsets(end).simvar='simunif';
% 

stimunif=[srunif.stiminfo{:}];

miobj=PoissExpCompMI(),

nrand=1000;
for dind=1:length(dsets)
[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
   dsets(dind).mi=zeros(1,simdata.niter);
   dsets(dind).mi(:)=nan;

   simdata.stimchooser=loadstim(simdata.stimchooser);
  dsets(dind).mirand=zeros(nrand,simdata.niter);
  dsets(dind).mirand(:)=nan;
   for t=1:simdata.niter
       %if covariance matrix was saved
       %compute the mutual information from the stimulus
       if ~isempty(pdata.c{t})
           fprintf('t=%d\n',t);
           stimhighd=mobj.ninpfunc(sr.y(:,t));
          muproj=stimhighd'*pdata.m(:,t);
          sigma=stimhighd'*pdata.c{t}*stimhighd;
          dsets(dind).mi(t)=compmi(miobj,muproj,sigma);
       
       %randomly draw stimuli in the pool and compute the mutual
       %information
      
      % stimind=ceil(rand(1,nrand)*(simdata.stimchooser.nstim));
      stimind=1:nrand;
       stimrndhd=simdata.stimchooser.Poolstim.stimpool(:,stimind);
       
       muproj=pdata.m(:,t)'*stimrndhd;

           sigma=pdata.c{t}*stimrndhd;
    sigma=stimrndhd.*sigma;
    sigma=sum(sigma,1);
        dsets(dind).mirand(:,t)=sort(compmi(miobj,muproj,sigma),'descend')';
       end
    end
end

fmi=[];
fmi.hf=figure;

hold on;

for dind=1:length(dsets)
    trials=find(~isnan(dsets(dind).mi));
    fmi.hp(dind)=plot(trials,dsets(dind).mi(trials),getptype(dind));
    fmi.lbls{dind}=dsets(dind).lbl;
    
    plot(trials,dsets(dind).mirand(:,trials));
end
lblgraph(fmi);