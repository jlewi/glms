%5-30-2007
%TO see why info. max. i.i.d is worse than i.i.d. try plotting the
%true value of glmproj on each iteration
dsets=[];


 dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
 dsets(end).lbl='info. max. heur';
 dsets(end).simvar='simmax';
 
 dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
 dsets(end).lbl='i.i.d.:10000';
 dsets(end).simvar='simrand';
 
 dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
 dsets(end).lbl='info. max. i.i.d:10000';
 dsets(end).simvar='simunif';

 


ndiff=5;
for dind=1:length(dsets)
[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
  k=[simdata.extra.mparam.k1' simdata.extra.mparam.k2'];
  
  glmproj=k'*sr.y;
  glmproj=sum(glmproj.^2,1);
  dsets(dind).glmproj=glmproj;
   
end

%*******************************************************************
%%plot it
fmi=[];
fmi.hf=figure;
fmi.hf
hold on;

    hold on;
for dind=1:length(dsets)
    
    fmi.hp(dind)=plot(dsets(dind).glmproj,getptype(dind));
    fmi.lbls{dind}=dsets(dind).lbl;
    
%    plot(trials,dsets(dind).mirand(:,trials));
end

lblgraph(fmi);