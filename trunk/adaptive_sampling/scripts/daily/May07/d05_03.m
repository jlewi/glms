%5-30-2007
%TO see why info. max. i.i.d is worse than i.i.d. try plotting the
%information
%of the stimuli from the two methods
dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
% dsets(end).lbl='info. max. heuristic';
% dsets(end).simvar='simmax';
% 
 dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
 dsets(end).lbl='info. max. i.i.d:10000';
 dsets(end).simvar='simunif';
% 
 dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_001.mat');
 dsets(end).lbl='i.i.d.:10000';
 dsets(end).simvar='simrand';

stimunif=[srunif.stiminfo{:}];

miobj=PoissExpCompMI(),
for dind=1:length(dsets)
[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
   dsets(dind).mi=zeros(1,simdata.niter);
   dsets(dind).mi(:)=nan;
    for t=1:simdata.niter
       %if covariance matrix was saved
       %compute the mutual information from the stimulus
       if ~isempty(pdata.c{t})
           stimhighd=mobj.ninpfunc(sr.y(:,t));
          muproj=stimhighd'*pdata.m(:,t);
          sigma=stimhighd'*pdata.c{t}*stimhighd;
          dsets(dind).mi(t)=compmi(miobj,muproj,sigma);
       end
    end
end

fmi=[];
fmi.hf=figure;

hold on;

for dind=1:length(dsets)
    trials=find(~isnan(dsets(dind).mi));
    fmi.hp(dind)=plot(trials,dsets(dind).mi(trials),getptype(dind));
    fmi.lbls{dind}=dsets(dind).lbl;
end
lblgraph(fmi);