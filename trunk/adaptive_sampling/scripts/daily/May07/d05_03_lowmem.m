%5-31-2007
%
%Load simulations from a file
%zero out all covariance matrices except the last one and save to another
%filename
clear all;
setpathvars;
%50d data
dsets=[]


% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_006.mat');
% dsets(end).lbl='i.i.d.';
% dsets(end).simvar='simrand';
% 
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_006.mat');
%  dsets(end).lbl='info. max. i.i.d';
%  dsets(end).simvar='simunif';
% % 
dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001.mat');
dsets(end).lbl='info. max. i.i.d.';
dsets(end).simvar='simunif';


%********************************************************************
%compute the subspace angle
for dind=1:length(dsets)
    %outfile
    [pathstr, name,ext] = fileparts(dsets(dind).fname);
    dsets(dind).outfile=fullfile(pathstr,sprintf('%s_%s%s',name,dsets(dind).simvar,ext));
    if exist(dsets(dind).outfile,'file')
        warning('File %s exists. Terminating',dsets(dind).outfile);
        return;
    end
    
   [pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
   
   %remove all covariance matrices except the llast
   for t=1:simdata.niter
      pdata.c{t}=[]; 
   end
  savesim('simfile',dsets(dind).outfile,'pdata',pdata,'sr',sr,'simparam',simdata,'mparam',mobj);
end