%04-10-2008
%
%Compare the time it takes to write to a file in my home directory versus
%a  file on the local harddrive
setpaths;

global TMPDIR;
TMPDIR='/tmp';

dsets=struct('fname',[],'tname',[],'time',[]);
dsets(1).fname=seqfname(FilePath('bcmd','RESULTSDIR','rpath','testmat.mat'));
dsets(1).tname='Home directory:';

dsets(2).fname=seqfname(FilePath('bcmd','TMPDIR','rpath','testmat.mat'));
dsets(2).tname='Local directory;';

dim=[16 43];

nmat=10;

%generate the data
data=[];
for j=1:nmat
  data.mat{j}=randn(dim);
 
  end

  data.ids=1:nmat;
  
  for dind=1:length(dsets)
    tic;
      
  robj=RAccessFile('fname',dsets(1).fname,'dim',dim);
  robj=writedata(robj,data.mat,data.ids);
   dsets(dind).time=toc;
  
   fprintf('%s: %d sec \n',dsets(dind).tname, dsets(dind).time);
  end
  
  