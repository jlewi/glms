figure;
subplot(2,1,1);
plot(compmu(glm,glmproj(1,:)));
xlabel('trial');
ylabel('E(r)');

subplot(2,1,2);
imagesc(trialobsrv);
xlabel('t');
ylabel('repeat')
title('raster plot');
%%

glmproj=stimproj+fakemodel.bias;
fc=FigObj('name','check bsfakedata');
fc.a=AxesObj('xlabel','trial');
hold on

hp=plot(1:length(glmproj),compmu(glm,glmproj));
pstyle.LineWidth=4;
pstyle.Color='r';

fc.a=addplot(fc.a,'hp',hp,'pstyle',pstyle,'lbl','E(r_t)');

hp=plot(1:length(glmproj),count);
pstyle.LineWidth=1;
pstyle.Color='b';
fc.a=addplot(fc.a,'hp',hp,'pstyle',pstyle,'lbl','r_t');

fc=lblgraph(fc)