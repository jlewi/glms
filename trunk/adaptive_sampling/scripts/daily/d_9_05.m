%check the integration for the variance in the i.i.d case
n=5;
theta1=.5;
mmag=1;

x=linspace(-7,7,10000);
dx=x(2)-x(1);
px=normpdf(x,0,(mmag^2/n)^.5);

%compute the integral numerically
intnum=sum(exp(x*theta1).*x.^2.*px.*dx)


%comput it with the formula
intact=exp(mmag^2*theta1^2/(2*n))*(mmag^4*theta1^2+n*mmag^2)/n^2

%integral for covariance of all but first elemetns
inthigh=sum(exp(x*theta1).*(mmag^2-x.^2)/(n-1).*px.*dx)


inthighact=exp(1/2*theta1^2*mmag^2/n)*(mmag^2-(mmag^4*theta1^2+n*mmag^2)/n^2)/(n-1)