files{1}='results\maxinfo\02_02\02_02_max2d_003.mat';
files{2}='results\maxinfo\02_05\max3d_001.mat';
fentropy.hf=figure;
figure(fentropy.hf);
    
hold on;
subsamp=10;

pind=0;
for findex=1:length(files)
    load(files{findex});
    for index=0:simparam.niter
         mentropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(pmax.newton.c{index+1})));
    end

 
     for index=0:simparam.niter
         rentropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(prand.newton.c{index+1})));
     end
    popts.sgraphs=0;


    
    
    %plot the maximum from this file
    pind=1;
    ind=[1:10,10:10:100, 100:100:length(fentropy.x)];
    fentropy.hp(pind)=plot(fentropy.x(ind),mentropy(ind),'kx');
    set(fentropy.hp(pind),'Marker','x');
    set(fentropy.hp(pind),'LineWidth',2);
    set(fentropy.hp(pind),'MarkerSize',10);
    set(fentropy.hp(pind),'Color',[0 0 0]) 
    hl(pind)=plot(fentropy.x,mentropy,'k-');
    set(hl(pind),'LineWidth',2);
    fentropy.lbls{pind}='Optimized';
    
    %plot the random
    pind=pind+1;
    fentropy.hp(pind)=plot(fentropy.x(ind),rentropy(ind),'k.');
    set(fentropy.hp(pind),'Marker','.');
    set(fentropy.hp(pind),'LineWidth',2);
    set(fentropy.hp(pind),'MarkerSize',20);
    set(fentropy.hp(pind),'Color',[0 0 0])
    hl(pind)=plot(fentropy.x,rentropy,'k:');
    set(hl(pind),'LineWidth',2);
    fentropy.lbls{pind}='Random';


end
legend(fentropy.hp(1:2), fentropy.lbls(1:2));
hg=gca;
    set(hg,'FontSize',20)
    ylabel(fentropy.ylabel);
    xlabel('Trial');
    xlim([0 fentropy.x(end)]);

 set(hg,'Xscale','Log');
set(hg,'XTick',[1 100 1000])
