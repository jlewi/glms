%09-18-2008
%
%We want to see how well log(1+x) is approximated by
%log(x) and the taylor approximation as a function of x.

x=[0:.25:5];
%how many terms to use in the Taylor approximation
nterms=[1:3];

truval=log(1+x);

logx=log(x);


%compute the value of the taylor approximation.

%we start by constructing a matrix where each row is a different term 
%in the summation
mterm=max(nterms);
t=zeros(max(nterms),length(x));

t=ones(mterm,1)*x;
n=[1:mterm]'*ones(1,length(x));
t=(-1).^(n-1).*t.^n./n;

tp=cumsum(t,1);

tp=tp(nterms,:);



%make a plot
fh=FigObj('name','log(1+x) approximations');
hold on;

hp=plot(x,truval);
addplot(fh.a,'hp',hp,'lbl','True');

hp=plot(x,logx);
addplot(fh.a,'hp',hp,'lbl','log x');

for ind=1:length(nterms)
    hp=plot(x,tp(ind,:));
    addplot(fh.a,'hp',hp,'lbl',sprintf('n=%d',nterms(ind)));
end


xlabel(fh.a,'x');
title(fh.a,'Approximations of log(1+x)');
lblgraph(fh);





