%*************************************
%analyze the bird song data
%**************************************

xscript('/home/jlewi/svn_trunk/allresults/bird_song/080420/bsglmfit_setup_001.m');
%how many of the spikes occur when there is no input
bdata=load(getpath(dsets.datafile),'bdata');
bdata=bdata.bdata;


[bdata,sr]=gettrial(bdata,1);


trials=gettrialswithspikes(bdata);

nspikesnoinput=0;

for j=1:length(trials)
    trial=trials(j);
    [bdata,sr]=gettrial(bdata,trial);
    if (mod(j,100)==0)
        fprintf('Spike: %d of %d\n',j,length(trials));
    end
    %     m=getmatrix(getinput(sr))/length(trials)+m;
    if ~(getobsrv(sr)==1)
        error('No spike on try even though trial is returned by gettrialswithspikes');

    end
    stim=getData(getinput(sr));

    if ((stim'*stim)<10^-6)
        nspikesnoinput=nspikesnoinput+1;
    end
end

fprintf('%% spikes no input =%0.3g %%\n',100*nspikesnoinput/length(trials));



%%
%********************************************************************
%Make a plot of the stimuli on some of the trials with spikes
%*********************************************************************
%%


ntoplot=4;
trials=gettrialswithspikes(bdata);

ttoplot=[];
while(length(ttoplot)<ntoplot)
    ttoplot=unique([ttoplot ceil(rand(1,ntoplot)*length(trials))]);
    ttoplot=ttoplot(1:min(length(ttoplot),ntoplot));
end
ttoplot=trials(ttoplot);

fh=FigObj('name','Input on trials with spikes','width',5,'height',8);
fh.a=AxesObj('nrows',ntoplot,'ncols',1,'xlabel','time(s)','ylabel','Frequenzy(hz)');


[t,f]=getstrftimefreq(bdata);
for  ind=1:ntoplot
    fh.a=setfocus(fh.a,ind,1);
    [bdata,sr]=gettrial(bdata,ttoplot(ind));
    imagesc(t,f,getmatrix(getinput(sr)));
    hc=colorbar;
    fh.a(ind,1)=sethc(fh.a(ind,1),hc);
    fh.a(ind,1)=title(fh.a(ind,1),sprintf('Trial %g',ttoplot(ind)));

    xlim([t(1) t(end)]);
    ylim([f(1) f(end)]);
end

%make the colorbars uniform
ha=getha(fh.a);
cl=get(ha,'clim');
cl=cell2mat(cl);
set(ha,'clim',[min(cl(:,1)),max(cl(:,2))]);

fh=lblgraph(fh);
fh=sizesubplots(fh);

tbl={fh;{'script','dbirsongspikes.m'; 'Data File',getpath(dsets.datafile);'Explanation','A sample of the stimuli preceding spikes'}};
onenotetable(tbl,seqfname('~/svn_trunk/notes/bsspikestimuli.xml'));