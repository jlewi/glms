%8-24-2007
%try computing our objective function using the Monte Carlo method that I
%described in my notes that can be extended to the batch setting.

glm=GLMModel('poisson','canon');
confint=.99;
nmcsamps=1000;
mc=CompMIMC('glm',glm,'confint',confint,'nmcsamps',nmcsamps);
mnum=CompMINum('glm',glm,'confint',confint);
mapprox=PoissExpCompMI();


mueps=[-3:.25:3];
sigma=10.^[-3:1];

odata=[];

odata={'mueps','sigma','CompMIC','CompMINum','PoissExpCompMi'};
odata=[odata;cell(length(mueps)*length(sigma),5)];

oind=1;

imc=zeros(length(sigma),length(mueps));
iapprox=zeros(length(sigma),length(mueps));
inum=zeros(length(sigma),length(mueps));


imct1=zeros(length(sigma),length(mueps));;
imct2=zeros(length(sigma),length(mueps));;

%%
oind=1;
for mind=1:length(mueps)

    for sind=1:length(sigma)
    fprintf('mueps=%d \t sigma=%d \n',mueps(mind),sigma(sind));
    oind=oind+1;

    [imc(sind,mind) imct1(sind,mind) imct2(sind,mind)]=expdetint(mc,mueps(mind),sigma(sind));
    end
end
%%
for mind=1:length(mueps)

    for sind=1:length(sigma)
    fprintf('mueps=%d \t sigma=%d \n',mueps(mind),sigma(sind));
    oind=oind+1;
iapprox(sind,mind)=2^compmi(mapprox,mueps(mind),sigma(sind));
inum(sind,mind)=compmi(mnum,mueps(mind),sigma(sind));


    end
end

%%
oind=1;
for mind=1:length(mueps)

    for sind=1:length(sigma)
         oind=oind+1;
odata{oind,1}=mueps(mind);
odata{oind,2}=sigma(sind);
odata{oind,3}=imc(sind,mind);
odata{oind,4}=inum(sind,mind);
odata{oind,5}=iapprox(sind,mind);
    end
end
onenotetable(odata,'../notes/compmc.xml');


odters=[{'mueps','sigma','E_eps_Er log Jobs', 'E_epsE_r log |JIJ+XCX|','CompMIMC'};cell(length(mueps)*length(sigma),5)];

oind=1;
for mind=1:length(mueps)

    for sind=1:length(sigma)
         oind=oind+1;
         odters{oind,1}=mueps(mind);
odters{oind,2}=sigma(sind);
        odters{oind,3}=imct1(sind,mind);
        odters{oind,4}=imct2(sind,mind);
        odters{oind,5}=imc(sind,mind);
    end
end

onenotetable(odters,'../notes/compmc_terms.xml');

%%
if (numel(imc)>2)
fmc.hf=figure;
aind=1;
fmc.a{1}.ha=subplot(1,3,1);
contourf(mueps,sigma,imc);
colorbar;
fmc.a{aind}.xlabel='\mu';
fmc.a{aind}.ylabel='\sigma^2';
fmc.a{aind}.title='Monte Carlo';

aind=aind+1;
fmc.a{aind}.ha=subplot(1,3,aind);
contourf(mueps,sigma,inum);
colorbar
fmc.a{aind}.xlabel='\mu';
fmc.a{aind}.ylabel='\sigma^2';
fmc.a{aind}.title='Numerical';

aind=aind+1;
fmc.a{aind}.ha=subplot(1,3,aind);
contourf(mueps,sigma,iapprox);
colorbar;
fmc.a{aind}.xlabel='\mu';
fmc.a{aind}.ylabel='\sigma^2';
fmc.a{aind}.title='Approx.';

cl=get([fmc.a{1}.ha, fmc.a{2}.ha],'clim');
cl=cell2mat(cl);
ncl(1)=min(cl(:,1));
ncl(2)=max(cl(:,2));
set([fmc.a{1}.ha, fmc.a{2}.ha,fmc.a{3}.ha],'clim',ncl);

lblgraph(fmc);

onenotetable({fmc,{'confint',confint;'# mc samps', nmcsamps}},'../notes/compmc_fig.xml')
end


%make a plot of each component of the information
if (numel(imc)>2)
ft.hf=figure;
ft.name='Terms';

aind=1;
ft.a{1}.ha=subplot(1,3,1);
contourf(mueps,sigma,imct1);
colorbar;
ft.a{aind}.xlabel='\mu';
ft.a{aind}.ylabel='\sigma^2';
ft.a{aind}.title='E_epsE_r log Jobs';

aind=aind+1;
ft.a{aind}.ha=subplot(1,3,aind);
contourf(mueps,sigma,imct2);
colorbar
ft.a{aind}.xlabel='\mu';
ft.a{aind}.ylabel='\sigma^2';
ft.a{aind}.title='E_epsE_r log |JIJ+sigma|';

aind=aind+1;
ft.a{aind}.ha=subplot(1,3,aind);
contourf(mueps,sigma,imc);
colorbar;
ft.a{aind}.xlabel='\mu';
ft.a{aind}.ylabel='\sigma^2';
ft.a{aind}.title='CompMIMC';

cl=get([ft.a{1}.ha, ft.a{2}.ha],'clim');
cl=cell2mat(cl);
ncl(1)=min(cl(:,1));
ncl(2)=max(cl(:,2));
set([ft.a{1}.ha, ft.a{2}.ha,ft.a{3}.ha],'clim',ncl);

lblgraph(ft);

onenotetable({ft,{'confint',confint;'# mc samps', nmcsamps}},'../notes/compmc_terms_fig.xml')
end