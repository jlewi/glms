%08-05-2008
%
%For testing the update methods for the logistic model
%with fake data
%choose data where dim(theta)=1. (i.e the strf has 1 term and then there's 
%the bias)
%
%
% Plot the log posterior as a 2-d image


clear variables;
setpathvars;

datedir='080520';
setupfile='bsglmfit_setup_001.m';
setupfile=fullfile(RESULTSDIR,'bird_song/fakedata',datedir,setupfile);
xscript(setupfile);

data=load(getpath(dsets.datafile));

rdata=load(getpath(dsets.rawdatafile));
fakemodel=rdata.fakemodel;
mobj=fakemodel.mobj;


bpost=BSlogpost('bdata',data.bdata');
%************************************************************
%compute the log likelihood
%************************************************************
if (length(fakemodel.strfcoeff)>1)
    error('This script assumes strf dimensionality is 1');
end

dstrf=.5;
dbias=.5;
strfoff=1;
biasoff=1;
strfvals=[fakemodel.strfcoeff-strfoff:dstrf:fakemodel.strfcoeff+strfoff];
biasvals=[fakemodel.bias-biasoff:dbias:fakemodel.bias+biasoff];


lpost=zeros(length(biasvals),length(strfvals));

prior=getpost(data.allpost,0);


parfor bind=1:length(biasvals)
    slicepost=zeros(1,length(strfvals));
    for sind=1:length(strfvals)
       fprintf('sind=%d  of %d \n' ,sind,length(strfvals));    
       slicepost(sind)=complpost(bpost,mobj,[strfvals(sind);biasvals(bind)],prior) ;
    end
    lpost(bind,:)=slicepost;
end

%*******************************************************************
%Plot it
%*********************************************************************
%%
fl=FigObj('name','Log posterior');
fl.a=AxesObj('xlabel','STRF','ylabel','bias');
title('Log Posterior');


imagesc(strfvals,biasvals,lpost);
colorbar;

fl=lblgraph(fl);
xlim([strfvals(1) strfvals(end)]);
ylim([biasvals(1) biasvals(end)]);


otbl={{fl};{'script','bsfake2dlogpost';'setupfile',setupfile; 'dstrf', dstrf;'dbias',dbias}};
onenotetable(otbl,seqfname('~/svn_trunk/notes/logpost.xml'));