[prand,simrand,mparam,srrand]=loadsim('simfile',fname,'simvar','simrand');
[pmax,simmax,mparam,srmax]=loadsim('simfile',fname,'simvar','simmax');

%normalize the stimuli
stimrand=normmag(srrand.y);
stimmax=normmag(srmax.y);

%estimate the model by 
%averaging the stimuli to gether.
%we multiply the stimuli for category zero by negative 1
%when we average stimuli together the average should be the direction of
%the decision boundary
ind=find(srrand.nspikes==0);
stimrand(:,ind)=-1*stimrand(:,ind);


ind=find(srmax.nspikes==0);
stimmax(:,ind)=-1*stimmax(:,ind);

%estimate the model
mmax=mean(stimmax,2);
mmrand=mean(stimrand,2);

acc.max=testmodel(simmax.stimchooser,mmax);
acc.rand=testmodel(simrand.stimchooser,mmrand);

%try increasing covariance of prior so its closer to ML
pinit=mparam.pinit;
pinit.c=100*eye(mparam.klength);

uobj=simmax.updater;
postlast=pinit;
stim=srmax.y
glm=simmax.glm;
mp.pinit=pinit;
einfolast=[];
obsrv=srmax.nspikes;
mmaxll=update(uobj,postlast,stim,glm,mp,obsrv,einfolast)



%minimize the m.s.e
optim = optimset(optim,'Jacobian','on');
optim=optimset(optim,'Display','off');

%locate zero of derivative of m.s.e
stim=stimmax; nspikes=srmax.nspikes;
stim=stimrand;nspikes=srrand.nspikes;
fmse=@(mu,stim,nspikes)(sum(abs([mu'*stim>0]-nspikes)))
minmse.mmax = minlogmse(srmax);
minmse.mrand = minlogmse(srrand);
minmse.accmmax=testmodel(simmax.stimchooser,minmse.mmax);
minmse.accrand=testmodel(simrand.stimchooser,minmse.mrand);