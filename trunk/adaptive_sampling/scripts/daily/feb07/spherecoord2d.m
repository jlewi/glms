%play around with fitting a posterior measured in spherical coordinates
%data should be 2d
function [rpost,apost]=spherecoord2(trial)
%trial=20;   %which trial to fit the posterior to

%file containing data
%this should be 2-d data with the MAP estimated by BatchML
%we use this to get the MAP.
fname='/home/jlewi/cvs_ece/allresults/poissexp/02_07/02_07_poissexp_001.mat';
[prand,simparam,mparam,srrand]=loadsim('fname',fname,'simvar','simrand');

stim=srrand.y(:,1:trial);
obsrv=srrand.nspikes(1:trial);

%***********************
%structures to store posterior on the radius and the angle
%mu - mean 
%a  - shape parameter of gamma distribution
%b - scale parameter
rpost=struct('mu',[],'a',[],'b',[]);

%posterior on angle is beta * pi
%a - alpha which is always 1
apost=struct('mu',[],'a',1,'b',1);

%conver the map estimate to radial coordinates
rpost.mu=(prand.m(:,trial+1)'*prand.m(:,trial+1))^.5;

apost.mu=atan2(prand.m(2,trial+1),prand.m(1,trial+1));

%**********************************************************
%fit a beta distribution to the angle
%*********************************************************
%1. Fix r and integrate to determine a normalizing constant
fint=@(ang)(jpdfang(ang,rpost.mu,obsrv,stim,simparam.glm));
apost.normconst=quad(fint,0,pi);

fint=@(ang)((ang/pi).^2.*1/pi.*jpdfang(ang,rpost.mu,obsrv,stim,simparam.glm));
apost.expang2=1/apost.normconst*quad(fint,0,pi);
apost.b=roots([1, 3,2-2/apost.expang2]);
ind=find(apost.b>0)
apost.b=apost.b(ind);

%**********************************************************
%fit a gamma distribution to the radius
%*********************************************************
%1. Fix r and integrate to determine a normalizing constant
fint=@(r)(jpdfr(r,apost.mu,obsrv,stim,simparam.glm));
rmax=10;
rpost.normconst=quad(fint,0,rmax);

fint=@(r)(r.^2.*jpdfr(r,apost.mu,obsrv,stim,simparam.glm));
rpost.expr2=1/rpost.normconst*quad(fint,0,rmax);

%check integration by computing mean numerically
%and seeing if its close to our estimate based on the MAP
fintrmu=@(r)(r.*jpdfr(r,apost.mu,obsrv,stim,simparam.glm));
rpost.checkrmu=1/rpost.normconst*quad(fintrmu,0,rmax);

rpost.var=rpost.expr2-rpost.mu^2;
rpost.b=rpost.var/rpost.mu;
rpost.a=rpost.mu/rpost.b;

%************************************************************
%compute the log of the joint probability of the observations and theta
%for a whole bunch of values of theta 
%   ang - values of angle to evalue joint at
function jp=jpdfang(ang,rfix,obsrv,stim,glm)
    jp=ones(1,length(ang));
    %compute theta from angular coordinates
    theta=rfix*[cos(ang);sin(ang)];

    %loop over the observations 
    for k=1:size(stim,2);
        glmproj=stim(:,k)'*theta;
        jp=jp.*glm.pdf(obsrv(k),glm.fglmmu(glmproj));
    end
    
    %************************************************************
%compute the log of the joint probability of the observations and theta
%for a whole bunch of values of theta 
%   r - values of r to evalue joint at
function jp=jpdfr(r,afix,obsrv,stim,glm)
    jp=ones(1,length(r));
    %compute theta from angular coordinates
    theta=[cos(afix);sin(afix)]*r;

    %loop over the observations 
    for k=1:size(stim,2);
        glmproj=stim(:,k)'*theta;
        jp=jp.*glm.pdf(obsrv(k),glm.fglmmu(glmproj));
    end