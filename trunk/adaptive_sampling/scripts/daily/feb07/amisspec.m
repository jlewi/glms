fop.hf=figure;
fop.name='Optimal Point'


fop.a{1}.ha=subplot(2,1,1);
fop.a{1}.ylabel='dotmuopt';
ind=find(~isempty([stim.dotmuopt]));
fop.a{1}.hp=plot(51:simparam.niter,[stim.dotmuopt]);

fop.a{2}.ha=subplot(2,1,2)
fop.a{2}.ylabel='sigmaopt';
fop.a{2}.hp=plot(51:simparam.niter,[stim.sigmaopt]);

lblgraph(fop);


%plot the change in the mean
fdm.hf=figure;
fdm.xlabel='Trial';
fdm.name='Mean Distance';
fdm.ylabel='Distance in Mean';

plot((sum(diff(pmax.m,[],2).^2,1).^.5)./(sum(pmax.m(:,2:end).^2,1).^5));

lblgraph(fdm);

%look at just the value of the stimulus in the direction of the posterior
%compute the magnitude of the map
mapmag=sum(pmax.m.^2,1).^.5;

fstim.hf=figure;
fstim.name='Stim projected on MAP';
fstim.ylabel='Trial';
fstim.xlabel='i';
sproj=sum(sr.y.*normmag(pmax.m(:,1:end-1)),1);
sproj=(ones(mparam.klength,1)*sproj).*normmag(pmax.m(:,1:end-1));
imagesc(sproj');
colorbar;
lblgraph(fstim)


%**********************************
%pllot the magnitude of the projections
fproj.hf=figure;
fproj.name='Projection Magnitudes';
fproj.a{1}.ha=subplot(2,1,1);
fproj.a{1}.ylabel='Projection Along Mean';
fproj.a{1}.title='stimulus';
plot((sum(sproj.^2,1)).^.5);

fproj.a{2}.ha=subplot(2,1,2);
fproj.a{2}.ylabel='Magnitude';
fproj.a{2}.title='MAP';
plot(mapmag);
lblgraph(fproj)