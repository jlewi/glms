pname=fullfile(RESULTSDIR,'precompmi/02_14_powerlaw_002.mat');
pobj=PreCompMIObj('filename',pname);
rsm=RSMutualInfo('fname',pname,'degree',6);

%make images of the surfaces
miimg(rsm,pobj.dotmu,pobj.sigmasq)
miimg(pobj)

%plot the error
ferr.hf=figure;
ferr.xlabel='\mu_\epsilon';
ferr.ylabel='\sigma^2';
ferr.name=sprintf('Difference in fitted and true mutual info degree %d',rsm.degree);
ferr.title=ferr.name;

m=mi(rsm,pobj.dotmu,pobj.sigmasq);
%contourf(muglm,sigmasq,log10(lobjsurf'));
contourf(pobj.dotmu,pobj.sigmasq,(pobj.mi-m)',[-2:.25:2]);
lblgraph(ferr);
colorbar;


%plot tht projections of the real data
fproj.hf=figure;
fproj.name='Projections of mi on sigma axis';
fproj.title=fproj.name;
fproj.xlabel='\sigma^2';
fproj.ylabel='Mutual Info';
fproj.hp=[];
fproj.lbls={};
pind=0;
hold on
for k=1:length(pobj.dotmu)
    pind=pind+1;
    fproj.hp(pind)=plot(pobj.sigmasq,pobj.mi(k,:),getptype(pind))
    fproj.lbls{pind}=sprintf('dotmu=%d',pobj.dotmu(k));
end
lblgraph(fproj);

fproj.hf=figure;
fproj.name='Projections of mi on mu axis';
fproj.title=fproj.name;
fproj.xlabel='dotmu';
fproj.ylabel='Mutual Info';
fproj.hp=[];
fproj.lbls={};
pind=0;
hold on
pind=0;
for k=1:length(pobj.sigmasq)
    pind=pind+1;
    fproj.hp(pind)=plot(pobj.dotmu,pobj.mi(:,k),getptype(pind))
    fproj.lbls{pind}=sprintf('sigma^2=%d',pobj.sigmasq(k));
end
lblgraph(fproj);
    