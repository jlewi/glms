%test the function GLMModel\Jexp
%by computing the expected fisher information of the canonical poisson
%model
%This should just be exp(glmproj)
glm=GLMModel('poisson','canon');

gproj=2;
je=jexp(glm,gproj);