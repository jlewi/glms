%compare the accuracy of our simulations
%for the news group 20 data

function facc=news20compare(fname,data)
[pmax,simparam,mparam,sr]=loadsim('simfile',fname,'simvar','simmax');
[prand,simparam,mparam,sr]=loadsim('simfile',fname,'simvar','simrand');



acc.max=testmodel(simparam.stimchooser,[pmax.m]);
acc.rand=testmodel(simparam.stimchooser,[prand.m]);
facc.hf=figure;
facc.xlabel='trial';
facc.ylabel='%correct';
facc.name='%correct';
facc.hp=[];
facc.lbls={};
hold on;
pind=1;
facc.hp(pind)=plot(acc.max,getptype(pind));
facc.lbls{pind}='info. max';

pind=pind+1;
facc.hp(pind)=plot(acc.rand,getptype(pind));
facc.lbls{pind}='random';

lblgraph(facc);