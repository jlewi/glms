%explanation estimate a model by minimizing the mse of the logistic
%function
%
% Not sure this is working correctly
function mu=minlogmse(sr)

fderv=@(mu)(dmserror(mu,sr))
optim=optimset('TolX',10^-12,'TolF',10^-12);;
mu=fsolve(fderv,mean(sr.y,2),optim);

function dmse=dmserror(mu,sr)
glmproj=mu'*sr.y;

[gmu dgmu]=logisticcanon(glmproj);

dmse=2*(gmu-sr.nspikes).*dgmu;
dmse=ones(size(sr.y,1),1)*dmse;
dmse=dmse.*sr.y;
dmse=sum(dmse,2);