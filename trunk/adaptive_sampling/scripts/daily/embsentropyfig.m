%modified:
%   05_31_2006 - made the structure more general so that it would be easy
%   to use this script to make entropy figures for papers for different
%   data
%make embs figures for the entropy
%
%run maxplots
%parameters for tracking entropy
whichdata=1;    %0 means tracking entrop
%1 means high d results

%create a structure which contains the relevant fields for different data
%sets we then select which element of the structure we want to select the
%data.
data=[];
data(end+1).infile=fullfile('results', 'tracking','05_31','05_31_maxtrack_004.mat');
data(end).outfile=fullfile('writeup','nips','trackentropy.eps');
data(end).xlimits=[0 499];
data(end).monf{1}='newton1d';
data(end).subsample=10;

%following data is for the nips figure of the gabor data
data(end+1).infile=fullfile('results', 'tracking','05_30','05_30_maxtrack_019.mat');
data(end).outfile=fullfile('writeup','nips','highdentropy.eps');
data(end).xlimits=[0 5000];
data(end).monf{1}='newton1d';
data(end).subsample=100;

data(end+1).infile=fullfile('results', 'tracking','06_01','06_01_maxtrack_001.mat');
data(end).outfile=fullfile('writeup','nips','gaborentropy.eps');
data(end).xlimits=[0 3000];
data(end).monf{1}='newton1d';
data(end).subsample=100;


data(end+1).infile=fullfile('results', 'tracking','06_02','06_02_sinetrack_001.mat');
data(end).outfile=fullfile('writeup','nips','sinetrack_entropy.eps');
data(end).xlimits=[0 700];
data(end).monf{1}='newton1d';
data(end).subsample=10;

data(end+1).infile=fullfile('results', 'tracking','06_02','06_02_bumptrack_002.mat');
data(end).outfile=fullfile('writeup','nips','bumptrack_entropy.eps');
data(end).xlimits=[0 700];
data(end).monf{1}='newton1d';
data(end).subsample=10;

data(end+1).infile=fullfile('results', 'tracking','06_02','06_02_bumptrack_005.mat');
data(end).outfile=fullfile('writeup','nips','bumptrack_entropy.eps');
data(end).xlimits=[0 800];
data(end).monf{1}='newton1d';
data(end).subsample=10;

data(end+1).infile=fullfile('results', 'tracking','06_03','06_03_bumptrack_018.mat');
data(end).outfile=fullfile('writeup','nips','bumptrack_entropy.eps');
data(end).xlimits=[0 800];
data(end).monf{1}='newton1d';
data(end).subsample=10;

data(end+1).infile=fullfile('results', 'tracking','06_03','06_03_gabor_001.mat');
data(end).outfile=fullfile('writeup','nips','gaborentropy.eps');
data(end).xlimits=[0 5000];
data(end).monf{1}='newton1d';
data(end).subsample=100;

data(end+1).infile=fullfile('results', 'tracking','06_03','06_03_gabor_001.mat');
data(end).outfile=fullfile('writeup','nips','gaborentropy_rgb.eps');
data(end).color=1;
data(end).xlimits=[0 5000];
data(end).monf{1}='newton1d';
data(end).subsample=100;



%which file do we want
dind=length(data);

if (dind==0)
    subsample=10;
    
    fprintf('\n\n Plotting data in workspace: \n %s \n', data(dind).infile);
    fsuffix=sprintf('_%s_%.3i',ropts.trialdate,ropts.trialindex);
    [rdir]=fileparts(ropts.fname);
    outfile=fullfile(rdir,sprintf('entropy%s',fsuffix));
else

%select the appropriate data
    fprintf('\n\n Plotting data from file: \n %s \n', data(dind).infile);
infile=data(dind).infile;
load(infile);
outfile=data(dind).outfile;
xlimits=data(dind).xlimits;
monf=data(dind).monf;
subsample=data(dind).subsample;

end

if ~isfield(data(dind),'color')
    data(dind).color=0;
end

%maxplots;

fentropy.hf=figure;
fentropy.hp=[];
fentropy.x=[0:simparam.niter];
fentropy.xlabel='Iteration';
fentropy.ylabel='Entropy';
fentropy.title='';
fentropy.name='Entropy';    %window name
fentropy.on=1;
fentropy.fname='entropy';
fentropy.axisfontsize=30;
fentropy.lgndfontsize=30;

mindex=1;
%***************************************************************
%figure of the posterior entropy
%*********************************************************
%compute the entropy if not already computed
if ~isfield(prand.(monf{mindex}),'entropy')
    %need to know dimensionlaity of filter to compute entropy
    filtlength=size(pmethods.(monf{mindex}).m,1);

    mindex=1;
    entropy=zeros(1,simparam.niter+1);

    %need to know dimensionlaity of filter to compute entropy
    filtlength=size(pmax.(monf{mindex}).m,1);
    for index=0:simparam.niter
        eigd=eig(pmethods.(monf{mindex}).c{index+1});
        %entropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(pmethods.(monf{mindex}).c{index+1})));
        %this computation avoids
        entropy(index+1)=1/2*filtlength*(1+log2(2*pi))+1/2*sum(log2(abs(eigd)));
    end
    randentropy=entropy;
else
    randentropy=prand.(monf{mindex}).entropy;
end
%compute the entropy if not already computed
if ~isfield(pmax.(monf{mindex}),'entropy')

    mindex=1;
    entropy=zeros(1,simparam.niter+1);
    %need to know dimensionlaity of filter to compute entropy
    filtlength=size(pmethods.(monf{mindex}).m,1);
    for index=0:simparam.niter
        eigd=eig(pmethods.(monf{mindex}).c{index+1});
        %entropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(pmethods.(monf{mindex}).c{index+1})));
        %this computation avoids
        entropy(index+1)=1/2*filtlength*(1+log2(2*pi))+1/2*sum(log2(abs(eigd)));
    end
    maxentropy=entropy;
else
    maxentropy=pmax.(monf{mindex}).entropy;
end

figure(fentropy.hf);
hold on;

pind=1;

fentropy.hp(pind)=plot(0:subsample:simparam.niter,randentropy(1:subsample:end),'r.');
fentropy.hpline(pind)=plot(0:simparam.niter,randentropy,'r-');
fentropy.lbls{pind}='random';

pind=2;
fentropy.hp(pind)=plot(0:subsample:simparam.niter,maxentropy(1:subsample:end),'bx');
fentropy.hpline(pind)=plot(0:simparam.niter,maxentropy,'b-');
fentropy.lbls{pind}='info. max.';


lblgraph(fentropy);

figure(fentropy.hf);
xlim(xlimits);
hold on;

%figure(fentropy.hf);
set(fentropy.hp(1),'Marker','x');
set(fentropy.hpline(1),'LineWidth',2);
set(fentropy.hp(1),'MarkerSize',15);
%set(fentropy.hp(1),'Color',[0 0 0])

set(fentropy.hp(2),'Marker','.');
set(fentropy.hpline(2),'LineWidth',2);
set(fentropy.hp(2),'MarkerSize',25);
%set(fentropy.hpline(2),'Color',[0 0 0])


%savegraph('entropy.eps',fentopy.hf)
axis tight;

if data(dind).color==0
    exportfig(fentropy.hf,outfile,'bounds','tight','color','bw');
else
    exportfig(fentropy.hf,outfile,'bounds','tight','color','rgb');
end

