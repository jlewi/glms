%numerically compute the expected value of dt- the gain in our update
%function expdt

opts=[];
opts=optimset(opts,'TolX',10^-9);
tail=.0000005;

post.m=.95;
post.c=1;
x=1;
glm=GLMModel('poisson','canon');


epstrue=1;

fglm=getfglmmu(glm);

%rbounds(:,1)=tglm.cdfinv(tail,glm.fglm(epstrue));
rbounds(:,1)=0;
rbounds(:,2)=glm.cdfinv(1-tail,fglm(epstrue));

r=floor(rbounds(1,1)):1:ceil(rbounds(1,2));

%for each r compute delta t
epsnew=zeros(1,length(r));
dt=zeros(1,length(r));

edt=0;
for rind=1:length(r)
    pnew=postnewton1d(post,x,r(rind),glm,opts);
    dt(rind)=(pnew.m-post.m)/post.c;

    edt=edt+dt(rind)*glm.pdf(r(rind),fglm(epstrue));
end
