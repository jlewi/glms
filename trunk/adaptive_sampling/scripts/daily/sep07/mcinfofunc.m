%9-07-2007
%
%Make plots of the function whose expected value we need to compute using
%monte carlo methods for batch optimization.
%
%the function depends on \epsilon, r, and \sigma
%\epislon - bx1
%\sigma   - bxb
%r        - 1x1
%b        - number of stimuli in the batch


b=2;

%generate a random positive definite matrix sigma
%don't want it to be diagonal because that leads to a trivial case b\c it
%factors
eigd=10^-1*[.01 .1];
sigma=getmatrix(EigObj('eigd',eigd,'evecs',orth(rand(b,b))));


r=[0 10 100];


%create a bxn matrix of the epsilon values to evaluate it at
epsilon=ones(b,1)*[-10:1:10];

    neps=size(epsilon,2);


glm=GLMModel('poisson','canon');
%%
%compute the values  for each r we compute a matrix

fvals=zeros(neps,neps);

[egrid.x egrid.y]=meshgrid(epsilon(1,:),epsilon(2,:));
for rind=1:length(r)
    %compute a diagonal matrix with jobs the elements along the diagonal
    j.eps1=jobs(glm,r(rind),epsilon(1,:));
    j.eps2=jobs(glm,r(rind),epsilon(2,:));
    
    [jgrid.x jgrid.y]=meshgrid(j.eps1,j.eps2);

    for e1ind=1:neps
        for e2ind=2:neps
            fvals(e2ind,e1ind,rind)=log(det(diag([j.eps1(e1ind) j.eps2(e2ind)])^-1+sigma));
        end
    end
end

%%
%make plots
finf.hf=figure;
finf.width=10;
finf.height=4;
finf.fontsize=14;
for rind=1:length(r)
    finf.a{rind}.ha=subplot(1,length(r),rind);
    contourf(egrid.x,egrid.y,fvals(:,:,rind));
    colorbar;
    finf.a{rind}.ylabel='\epsilon_2';
    finf.a{rind}.xlabel='\epsilon_1';
    finf.a{rind}.title=sprintf('r=%d',r(rind));
end
lblgraph(finf);

onenotetable({finf,{'script','mcinfofunc';'epsilon',sprintf('[%d:%d:%d]',epsilon(1,1),epsilon(1,2)-epsilon(1,1),epsilon(end));'sigma',sigma;'eig(sigma)',eigd}},'../notes/compmc_terms_fig.xml')
