abthresh=10^-6;
tic
ainit=0;
    [la, dlda]=objpost(ainit,d,c,xmag,obsrv,glm);
    %we need to find aleft and aright to make sure they include the zero
    %the terms aleft - value for which objective funcition is negative
    %aright - value for which objective function is positive
    %following assumes ainit=0
    if (la<0)
        aleft=ainit;
        
        %determine the sign for aright       
        aright=sign(dlda);
        
        %we need to increase the magnitude (but not change the sign) of
        %right until the objective function is positive
        while(objpost(aright,d,c,xmag,obsrv,glm)<0)
            aright=aright*10;
        end
    elseif (la>0)
        aright=ainit;
        
        %we need to increase aright unti its positive
        aleft=-1*sign(dlda);
        while (objpost(left,d,c,xmag,obsrv,glm)>0)
            aleft=aleft*10;
        end    
    end
    %make sure aleft and aright don't both have same sign
    saleft=sign(objpost(aleft,d,c,xmag,obsrv,glm));
    saright=sign(objpost(aright,d,c,xmag,obsrv,glm));
    if (saright==saleft)
        error('binary search cannot work initialization is invalid');
    end
    %while aleft-aright is bigger then some threshold do binary step

    nbiter=0;
    while (abs(aleft-aright)>abthresh)
        nbiter=nbiter+1;
        anew=(aleft+aright)/2;
        if (objpost(anew,d,c,xmag,obsrv,glm)<0)
            aleft=anew;
        else
            aright=anew;
        end
    end
    
toc