%plot the mse but take into account only the 
%stimulus coeffecicents
%*****************
%figures

incacoef=0;     %whether to include spike history coefficents in mse

axisfontsize=30;
lgndfontsize=16;

fkl.hf=[];      %only create the figure if we have something to plot
fkl.hp=[];
fkl.x=[0:simparam.niter];
fkl.xlabel='Iteration';
fkl.ylabel='Distance';
fkl.title='Kl Divergence';
fkl.on=0;
fkl.name='KL Divergence';

fmdist.hf=figure;
fmdist.hp=[];
fmdist.x=[0:simparam.niter];
fmdist.xlabel='Iteration';
fmdist.ylabel='Distance';
fmdist.title='Distance From Mean to True Value';
fmdist.on=1;
fmdist.fname='mdist';
fmdist.name='Mean Distance';
fmdist.semilog=1;
%entropy
fentropy.hf=figure;
fentropy.hp=[];
fentropy.x=[0:simparam.niter];
fentropy.xlabel='Iteration';
fentropy.ylabel='Entropy';
fentropy.title='';
fentropy.name='Entropy';    %window name
fentropy.on=1;
fentropy.fname='entropy';

%mean squared error
fmse.hf=figure;
fmse.hp=[];
fmse.x=[0:simparam.niter];
fmse.xlabel='Iteration';
fmse.ylabel='MSE';
fmse.title='';
fmse.name='MSE';
fmse.on=1;
fmse.fname='mse';
fmse.semilog=1;


%timing
ftiming.hf=figure;
ftiming.hp=[];
ftiming.x=[1:simparam.niter];
ftiming.xlabel='Iteration';
ftiming.ylabel='Time(Seconds)';
ftiming.title='';
ftiming.name='Timing';
ftiming.on=1;
ftiming.fname='timing';
ftiming.semilog=0;

pind =0;

%methods structure
%specifies which method structures to check for
methods.pmax.vname='pmax';
methods.pmax.label='InfoMax';
methods.prand.vname='prand';
methods.prand.label='Random';
methods.pignore.vname='pignore';
methods.pignore.label='Ignore spike history';

%loop through the max and random sampling methods
fmethods=fieldnames(methods);
for methodind=1:length(fieldnames(methods));
    %for each field name
    %check if the variable is defined in the workspace if it is
    %set it to pmethods
    
    if (exist(methods.(fmethods{methodind}).vname,'var'))
        evalin('base',sprintf('pmethods=%s',methods.(fmethods{methodind}).vname));
        lbltxt=methods.(fmethods{methodind}).label;
   
    %if (methodind==1)
     %   pmethods=pmax;
      %  lbltxt='Max';
    %else
    %    pmethods=prand;
    %    lbltxt='Rnd';
    %end
    
    %construct a list of enabled methods
monf={};
mnames=fieldnames(pmethods);
for index=1:length(fieldnames(pmethods))
    if pmethods.(mnames{index}).on >0
        monf{length(monf)+1}=mnames{index};
    end
end

%loop through enabled methods
for mindex=1:length(monf)
         pind=pind+1;
         %plot the kl distance
        
        
        if isfield(pmethods.(monf{mindex}),'dktest')
            if (isempty(fkl.hf))
                fkl.hf=figure;
            else
                figure(fkl.hf);
            end
            hold on;
            fkl.on=1;
            fkl.hp(pind)=plot(fkl.x,pmethods.(monf{mindex}).dk(1,:),getptype(pind,1));
            plot(fkl.x,pmethods.(monf{mindex}).dk(1,:),getptype(pind,2));
            fkl.lbls{pind}=sprintf('monfte Carlo: %s', pmethods.(monf{mindex}).label);   
        end

        %***************************************************************
        %figure of the least mean squared distance of mean and true mean
        %*********************************************************
        figure(fmdist.hf);
        hold on;
        %to handle backwards compatibility
        if ~isfield(mparam,'klength')
            mparam.klength=mparam.tlength;
        end
        
        %compute the mse of just the stimulus coefficents
        lse=pmethods.(monf{mindex}).m(1:mparam.klength,:)-mparam.ktrue*ones(1,simparam.niter+1);
        lse=lse.^2;
        lse=sum(lse,1);
        
        %take into account the spike history terms if they exist
        if (incacoef~=0)
        if isfield(mparam,'atrue')
        lsea=pmethods.(monf{mindex}).m(mparam.klength+1:end,:)-mparam.atrue*ones(1,simparam.niter+1)
        lsea=lsea.^2;
        lsea=sum(lsea,1);
        end
        else
            lsea=0;
        end
        
        lse=(lse+lsea).^.5;
        fmdist.lbls{pind}=sprintf('%s %s',lbltxt,pmethods.(monf{mindex}).label);
        
        fmdist.hp(pind)=plot(fmdist.x,lse,getptype(pind,1));
        plot(fmdist.x,lse,getptype(pind,2));
        
        %***************************************************************
        %figure of the posterior entropy
        %*********************************************************
        figure(fentropy.hf);
        hold on;
        entropy=zeros(1,simparam.niter+1);
        %need to know dimensionlaity of filter to compute entropy
        filtlength=size(pmethods.(monf{mindex}).m,1);
        for index=0:simparam.niter
            eigd=eig(pmethods.(monf{mindex}).c{index+1});
            %entropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(pmethods.(monf{mindex}).c{index+1})));
            %this computation avoids
            entropy(index+1)=1/2*filtlength*(1+log(2*pi))+1/2*sum(log(abs(eigd)));
        end
        fentropy.hp(pind)=plot(fentropy.x,entropy,getptype(pind,1));
        plot(fentropy.x,entropy,getptype(pind,2));

		fentropy.lbls{pind}=sprintf('%s %s',lbltxt,pmethods.(monf{mindex}).label);
        
         %***************************************************************
        %Mean Squared Error
        %*********************************************************
        figure(fmse.hf);
        hold on;
        if isfield(mparam,'atrue')
            thetatrue=[mparam.ktrue; mparam.atrue];
        else
            thetatrue=[mparam.ktrue];
        end
        err=exmse(pmethods.(monf{mindex}),thetatrue);
        fmse.hp(pind)=plot(fmse.x,err,getptype(pind,1));
        plot(fmse.x,err,getptype(pind,2));

		fmse.lbls{pind}=sprintf('%s %s',lbltxt,pmethods.(monf{mindex}).label);
        
         %***************************************************************
        %timing figure
        %*********************************************************
        figure(ftiming.hf);
        hold on
        if isfield(pmethods.(monf{mindex}),'timing')
            tpind=length(ftiming.hp)+1;
            ftiming.lbls{tpind}=sprintf('%s Update Posterior', monf{mindex});
            ftiming.hp(tpind)=plot(1:simparam.niter,pmethods.(monf{mindex}).timing.update,getptype(tpind,1));
            plot(1:simparam.niter,pmethods.(monf{mindex}).timing.update,getptype(tpind,2));

            tpind=length(ftiming.hp)+1;
            ftiming.lbls{tpind}=sprintf('%s Optimize Stimulus', monf{mindex});
            ftiming.hp(tpind)=plot(1:simparam.niter,pmethods.(monf{mindex}).timing.optimize,getptype(tpind,1));
            plot(1:simparam.niter,pmethods.(monf{mindex}).timing.optimize,getptype(tpind,2));

            

        end
end
    end
    
end
%if (fkl.on~=0)
%figure(fkl.hf);
%legend(fkl.hp,fkl.lbls);
%xlabel(fkl.xlabel);
%ylabel(fkl.ylabel);
%title(fkl.title);
%end

%label all the figures
figs={fmdist,fentropy,fkl,fmse,ftiming};
%which ones to do on semilog axis

for index=1:length(figs)
    if (figs{index}.on~=0)
        figure(figs{index}.hf);
        set(gca,'FontSize',axisfontsize);
        hl=legend(figs{index}.hp,figs{index}.lbls);        
        set(hl,'FontSize',lgndfontsize);
        xlabel(figs{index}.xlabel);
        ylabel(figs{index}.ylabel);
 
        title(figs{index}.title);
         set(figs{index}.hf,'name',figs{index}.name);
        if (isfield(figs{index},'semilog'))
            if (figs{index}.semilog~=0)
                set(gca,'YScale','Log');
            end
        end
       if (popts.sgraphs ~=0)
        %create filename for graph
        fname=fullfile(popts.sdir,sprintf('%0.2d_%0.2d_%s.eps',datetime(2),datetime(3),figs{index}.fname));
        fname=seqfname(fname);
        savegraph(fname,figs{index}.hf);

        end
    
    end
    
    
end



