%Look at the divergence of the entropy estimated with a gaussian
%and true entropy
fname='.\results\maxinfo\02_07\02_07_max2d_004.mat';
%fname='y:\cvs_ece\adaptive_sampling\results\maxinfo\02_09\02_09_max2d_006.mat';

load(fname);
sr=srmax.newton;
aopts.mckl=1;


%compue the KL divergence using monte carlo
index=0;
iter=[];
pmak.newton.dk=[];
klparam=[];
klparam.nsamp=10000;
klparam.z=randn(mparam.tlength,klparam.nsamp);

trials=[1:50 60:10:100];

pdfs={'pmax.newton'};

for pind=1:length(pdfs)
    eval(sprintf('pdf=%s;',pdfs{pind}));
    dk=zeros(1, length(trials));
    index=0;
    
for tr=trials
    index=index+1;
    q.mu=pdf.m(:,tr);
    q.c=pdf.c{tr};


        %We are computing the KL distance of our approximation from the
        %true posterior. To do this, the KL divergence needs to consider
        %the likelihood of all observations so far.
        %So the llhood function is the batch update function
        %To compute the KL Divergence the KL divergence
        %needs to be able to compute the likelihood of the observation
        %under the different models
        %so we pass the information needed to compute the likelihood
        %in the structure ll
        
        ll.func=@batchll;
        %we need all the observations so far
        ll.obsrv=srmax.newton.nspikes(1:tr);    
        ll.obsrv=ll.obsrv';                        %needs to be a column vector
                                                   %different observation
                                                   %in each row
        ll.input=srmax.newton.y(:,1:tr);           %stimulus on this trial
        ll.input=ll.input';                        %needs to mx mparam.tlength
                                                   %each row different
                                                   %stimulus on different
                                                   %trial
        ll.param={@ll1dtemp,mparam.tresponse};              
        
        %function to compute prior likleihood of model
        po.func=@logmvgauss;
         
        po.param={pdf.m(:,1),pdf.c{1}};
        
        [dk(1,index),p,dkvar(1,index)]=dkl(q,po,ll,klparam);
        iter(index)=tr-1;
        
end
    eval(sprintf('%s.dk.val=dk;',pdfs{pind}));
    eval(sprintf('%s.dk.iter=iter;',pdfs{pind}));
    
    
end

vtosave.pmax=[];
opts.append=1;
figure;
hold on;
plot(iter,pmax.newton.dk.val,'.')
plot(iter,pmax.newton.dk.val,'-')
%errorbar(iter,pdf.dk,pdf.dkvar.^.5);
set(gca,'YScale','log');
set(gca,'XScale','log');
xlabel('Iteration');
ylabel('KL Divergence');
