%d_3_20
%make a plot on the same axes
%of the random case when first d vectors are independent
%vs case where the are not independent and compare to infomax approach
RDIR=fullfile(RESULTSDIR,'maxinfo','03_20');
findep=fullfile(RDIR,'03_20_max2d_007.mat ');
frnd=fullfile(RDIR,'03_20_max2d_008.mat');

load(rnd);
maxplots;

dindep=load('~/cvs_ece/adaptive_sampling/results/maxinfo/03_20/03_20_max2d_007.mat')
    figure(fentropy.hf);
    hold on;
   entropy=zeros(1,simparam.niter+1);
   pind=3;
   
  presults=dindep.prand.newton;
  for index=0:simparam.niter
            entropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(presults.c{index+1})));
     end
        fentropy.hp(pind)=plot(fentropy.x,entropy,getptype(pind,1));
        fentropy.lbls{pind}=sprintf('Rnd Newton:  Independent stimuli');
        plot(fentropy.x,entropy,getptype(pind,2));
        legend(fentropy.hp, fentropy.lbls);