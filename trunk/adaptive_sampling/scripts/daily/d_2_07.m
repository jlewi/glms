c0=.05;
c1=.01;
cdiff=c0-c1;
m=1;
v1=1;
y0=[-.99:.01:.99];
val=(cdiff*y0.^2+m^2*c1).*(-2*v1*(m^2-y0.^2).^-.5+cdiff*y0)+2*y0*cdiff;