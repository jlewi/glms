%make plots of the 1d plots of the mean as a function of trial

%***************************************************
%Plot the mean

yticks=[1 200:200:800];
xticks=[1 50 100];
fsuffix='';
fmean.hf=figure;
fmean.axisfontsize=15;
fmean.fname=fullfile('writeup','nips',sprintf('bumptrack_post%s.eps',fsuffix));
fmean.hp=[];
set(fmean.hf,'name',sprintf('postseries:'));
clim(1)=min(mparam.ktrue(:));
clim(2)=max(mparam.ktrue(:));

ncols=3;
colormap(gray);
subplot(1,ncols,1);

set(gca,'fontsize',fmean.axisfontsize);
fmean.hp(end+1)=imagesc(pmax.newton1d.m',clim);

title('Information Maximizing');
xlabel('\theta_i');
ylabel('Trial');
set(gca,'ytick',yticks);
set(gca,'YTickLabel',yticks);
set(gca,'xtick',xticks);
set(gca,'XTickLabel',xticks);

subplot(1,ncols,2);
set(gca,'fontsize',fmean.axisfontsize);
imagesc(mparam.ktrue',clim);

title('True \theta');
xlabel('\theta_i');
set(gca,'xtick',xticks);
set(gca,'XTickLabel',xticks);
set(gca,'ytick',[]);

%colorbar resizes this image so lets set its to be equal to other axes
%execute a drawnow command to force matlab to determine the sizes for the
%individual axes
drawnow;
subplot(1,ncols,1);
set(gca,'ActivePositionProperty','Position');
subplot(1,ncols,2);
set(gca,'ActivePositionProperty','Position');
lastpos=get(gca,'Position');
subplot(1,3,3);

pos=get(gca,'Position');
pos(2:end)=lastpos(2:end);

subplot(1,ncols,3);
set(gca,'fontsize',fmean.axisfontsize);
imagesc(prand.newton1d.m',clim);
hc=colorbar;
set(gca,'Position',pos);

%hcpos=get(hc,'position');
%hcpos(1)=hcpos(1)-.02;
%set(hc,'position',hcpos);

title('Random Stimuli');
xlabel('\theta_i');
ylabel('Trial');
set(fmean.hf,'name','Post Series');
set(gca,'fontsize',fmean.axisfontsize);
set(gca,'xtick',xticks);
set(gca,'XTickLabel',xticks);
set(gca,'ytick',[]);
set(gca,'ActivePositionProperty','Position');
%subplot(1,ncols,4);
%hc=colorbar;
%set(hc,'CLim',clim);

lblgraph(fmean);
exportfig(fmean.hf,fmean.fname,'bounds','tight','Color','bw');

%**********************************************************
%Plot Simuli
fstim.hf=figure;
fstim.axisfontsize=15;
fstim.fname=fullfile('writeup','nips',sprintf('sinetrack_stim%s.eps',fsuffix));
fstim.xlabel='K_i';
colormap(gray);
set(gca,'fontsize',fstim.axisfontsize);
imagesc(srmax.newton1d.y');
title('Info Max Stimuli');
set(fstim.hf,'name','Stimuli');
colorbar;
xlabel('K_i');
ylabel('Trial');
lblgraph(fstim);

exportfig(fstim.hf,fstim.fname,'bounds','tight','Color','bw');
%make a plot of the maxium eigenvectors
emax=zeros(simparam.niter,mparam.klength);

for (tindex=1:simparam.niter)
    [evecs eigd]=svd(pmax.newton1d.c{tindex+1});
    emax(tindex,:)=[evecs(:,1)]';
end
    

femax.hf=figure;
imagesc(emax);
colorbar;
xlabel('i');
ylabel('Trial');
title('Maximum eigenvector on each trial');
set(femax.hf,'name','Max Eigenvector');

