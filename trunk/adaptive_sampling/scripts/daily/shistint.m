%10-01
%example to provie that in spike history case need to search
%all intervals not just lambda>cmax

Ck=[2 0; 0 1];
[evecs eigd]=eig(Ck);
eigd=diag(eigd);
u=[2^.5; 2^.5];
z=[0;1];


angles=[0:15/180*pi:2*pi];
%mag=[.1:.1:1];
mag=[1:1:5];
y=zeros(2,length(angles));

objfunc=zeros(length(mag),length(angles));

fobj.hf=figure;
fobj.xlabel='Angle';
fobj.ylabel='\sigma_{max}';
fobj.hp=[];
fobj.lbls={};
hold on;
pind=0;
for mindex=length(mag):-1:1;
    for aind=1:length(angles);
        y(:,aind)=mag(mindex)*[cos(angles(aind));sin(angles(aind))];
        objfunc(mindex,aind)=y(:,aind)'*(evecs*diag(eigd)*evecs')*y(:,aind)+z'*y(:,aind);        
    end
    pind=pind+1;
    fobj.hp(pind)=plot(angles,objfunc(mindex,:),getptype(mindex,1));
    plot(angles,objfunc(mindex,:),getptype(mindex,2));
    fobj.lbls{pind}=sprintf('%d',mag(mindex));
end
lblgraph(fobj);

%yshistrescale(w,estimmean,z,edist,iexpand,quads,mmag)
iexpand=1;
quads=1;
mmag=5;
for j=1:d
edist(j).eigd=eigd(j);
edist(j).i=[j];
edist(j).m=1;
end
yshistrescale(0,u,z,edist,iexpand,quads,mmag)
%yshistrescale(0,u,z,eigd,1,1,1,.5)
lambda=.01;
%solve the quadratic eqn for l2.
    
flam.hf=figure;
flam.xlabel='\lambda_1';
flam.ylabel='Angle';
flam.markersize=20;
flam.hp=[];
flam.lbls={};

%subplot(1,1,1);
lamrange=[0:.05:.999 1.001:.05:1.999 2.001:.05:10];
hold on;
langle=zeros(1,length(lamrange));
for quads=-1:2:1
    for lind=1:length(lamrange)
        lambda=lamrange(lind);
        a=sum(u.^2./(eigd-lambda).^2,1)/4;
        b=-sum((u.*z)./(eigd-lambda).^2,1);
        c=sum(z.^2./(eigd-lambda).^2,1)-mmag^2;
        % lambda(1,lindex)=ei*zi+sum(ej*zg./denom^2,1);

        %which sign to take
        %we should take both signs as we did in other case
        lambda2=(-b+quads*(b^2-4*a*c)^.5)/(2*a);

        if ~isreal(lambda2)
            langle(lind)=-1;
        else
            y=1/2*(lambda2*u-2*z)./(eigd-lambda);

            langle(lind)=atan(abs(y(2))/abs(y(1)));
            %compute the quadrant
            if (sign(y(1))==-1 & (sign(y(2))==1))
                langle(lind)=pi-langle(lind);
            elseif (sign(y(1))==-1 & (sign(y(2))==-1))
                langle(lind)=pi+langle(lind);
            elseif (sign(y(1))==1 & (sign(y(2))==-1))
                langle(lind)=2*pi-langle(lind);
            end
        end
    end
    if (quads==-1)
        flam.hp(1)=plot(lamrange,langle,'b.');
        flam.lbls{1}='Negative';
    else
        flam.hp(2)=plot(lamrange,langle,'r.');
        flam.lbls{2}='Positive';
    end
end

lblgraph(flam);