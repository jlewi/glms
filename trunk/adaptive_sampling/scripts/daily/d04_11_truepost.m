%04_11_2006
%the purpose of this script 
%is to compare the accuracy of the MAP estimate when we make the
%approximation used in the tracking case.
%to do this we go back after processing the data
%and compute the true posterior covariance and mean if we had used the full
%newton update on the results
%to summarrize all previous results
%load './results/tracking/04_09/04_09_maxtrack_005.mat';
%load './results/tracking/04_11/04_11_maxtrack_001.mat'

fmapdiff.hf=[];
fmapdiff.title='Distance of MAPs';
fmapdiff.name='Distance of MAPs newton1d and Newton';
fmapdiff.fname='dist_truepost';
fmapdiff.hf=[];      %only create the figure if we have something to plot
fmapdiff.hp=[];
fmapdiff.x=[0:simparam.niter];
fmapdiff.xlabel='Iteration';
fmapdiff.ylabel='n^{.5}||\mu_{1d}-\mu_t_r_u_e||_2';
fmapdiff.on=1;
fmapdiff.axisfontsize=16;
fmapdiff.lgndfontsize=16;



%compute the true posterior using newton's method
truepost.m=zeros(mparam.klength,simparam.niter+1);
truepost.m(:,1)=pmax.newton1d.m(:,1);
truepost.c{1}=pmax.newton1d.c{1};
truepost.tolerance=10^-10;
truepost.niter=zeros(1,simparam.niter);

%run newton's method to compute true map estimate at each iteration
       %only set the prior once
	            %on the first trial
prior.m=truepost.m(:,1);
prior.c=truepost.c{1};
prior.invc=inv(prior.c);
                    
sr=srmax.newton1d;
stimmat=sr.y;

post=prior;

mname='newton';
obsrv.twindow=mparam.tresponse;

for tr=2:simparam.niter+1
obsrv.n=(sr.nspikes(1:tr-1));

                    % The mean of our posterior is the peak of the true posterior
                    % therefore its a root of the derivative of the log of the posterior
                    % we use newton's method to compute it
                    % The new covariance matrix is the inverse of the - hessian at the
                    % new ut
                    % The seed for newton's methods is the mean from the previous
                    % iteration
    
                    %time the update
                   
                    %post.m=pmethods.newton.m(:,tr-1);
                    %post.c=pmethods.newton.c{tr-1};
                    newtonerr=inf;
                  tic
                    while (newtonerr >truepost.tolerance);
                        %observations are all obsrvations upto this point
                        %stimuli are all stimuli upto this point
                        [newpost]=post1stepgrad(prior,post,stimmat(:,1:tr-1),obsrv,@glm1dexp);
                        newtonerr=(newpost.m-post.m);
                        newtonerr=(newtonerr'*newtonerr)^.5;
                        post=newpost;
                       truepost.niter(1,tr-1)=truepost.niter(1,tr-1)+1;
                    end
                   truepost.newtonerr(1,tr-1)=newtonerr;
                    truepost.timing.update(1,tr-1)=toc; 
                    %we add the time of the newton iteration to what ever time
                    %it took to compute the inverse of the posterior 
                    %if we did that earlier using the eigenvectors from the
                    %stimulus optimization step
                   truepost.timing.update(1,tr-1)=toc+truepost.timing.update(1,tr-1);
                    %pmethods.newton.timing.optimize(1,tr-1)=toptimize;
                    post.invc=[];
                    
                    truepost.m(:,tr)=post.m;
                    truepost.c{tr}=post.c;  
                    
                if (mod(tr-1,10)==0)
                    fprintf('Iteration: %0.2g \n',tr-1);
                end

end
                    
%append truepost to pmax
%so we can use it to compare the posterior entropy
pmax.truepost=truepost;
pmax.truepost.on=1;
pmax.truepost.label='True Posterior';
pmax.newton.on=0;
prand.newton1d.on=0;

fmapdiff.hf=figure;
hold on
dist=(pmax.newton1d.m-truepost.m).^2;
dist=sum(dist,1);
dist=dist.^.5
plot(0:simparam.niter, ((0:simparam.niter).^.5).*dist);
lblgraph(fmapdiff);

%fit a 1/n plot
%x=1./(1:simparam.niter)^.5;
%y=sum(((pmax.newton1d.m-pmax.newton.m).^2)^.5,1);
%y=(2:end);
%p=polyfit(x,y,1);
%plot(
%save data before performing random sampling
if (ropts.savedata~=0)
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end
