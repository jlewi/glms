%try creating a precomp objec
datetime=clock;
simparam.glm=GLMModel('poisson',@glm1dlog);

bname=fullfile(RESULTSDIR, 'precompmi', sprintf('%2.2d_%2.2d_compmi.mat',datetime(2),datetime(3)));
fname=seqfname(bname);
mifopts.confint=.999;               %how much confidence for integrating the inner expectation
mifopts.inttol=10^-6;        % tolerance for numerical integration
%pobj=PreCompMIObj('filename',fname,'dotmu',[-10:10], 'sigmasq',[1:1:50],'glm',glm,'mifunc',@expdetint,'mifopts',mifopts);
pobj=PreCompMIObj('filename',sefname(bname),'dotmu',[-10:.25:20], 'sigmasq',[.01:.01:1 2:50 100:50:400],'glm',simparam.glm,'mifunc',@expdetint,'mifopts',mifopts);

%try loading it
%pob=PreCompMIObj('filename',fname);
%refine it and save it
pobj=refinesigmasq(pobj,[1:.5:5])

%compute it all at once and then see if they match
pobjc=PreCompMIObj('filename',seqfname(bname),'dotmu',pobjr.dotmu, 'sigmasq',pobjr.sigmasq,'glm',simparam.glm,'mifunc',@expdetint,'mifopts',mifopts);

if ~isempty(find(pobjr.mi-pobjc.mi)>10^-10)
    error('Refinesigmasq doesnt match all at once computation');
end

pobjr=refinedotmu(pobjr,[-1:.25:1]);

%compute it all at once and then see if they match
pobjc=PreCompMIObj('filename',seqfname(bname),'dotmu',pobjr.dotmu, 'sigmasq',pobjr.sigmasq,'glm',simparam.glm,'mifunc',@expdetint,'mifopts',mifopts);

if ~isempty(find(pobjr.mi-pobjc.mi)>10^-10)
    error('Refinesigmasq doesnt match all at once computation');
end

%************************
%check we remove duplicates
pobj=PreCompMIObj('filename',fname,'dotmu',[-10 1 1 20], 'sigmasq',[1 2 3 4 4 4 400],'glm',simparam.glm,'mifunc',@expdetint,'mifopts',mifopts);


%test choosing the stimulus
shist=[];
mparam.mmag=1;
mparam.klength=2;
post.m=[1;0];
post.c=eye(2);
[einfo.evecs einfo.eigd]=svd(post.c);
einfo.eigd=diag(einfo.eigd);
[opt2d.optstim]=choosestim(pobj,post,shist,einfo,mparam,glm)