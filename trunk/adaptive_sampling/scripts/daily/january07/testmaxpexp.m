%test that maxpoissexp using new model gives same results
%1-28-07
%   revised to use new object model
%Script to run optimization for canonical poisson 
clear all
%close all;

setpaths;

oldfile=fullfile(RESULTSDIR,'../../adaptive_sampling/results/aistat/10_15/10_15_maxtrack_013.mat');
%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/maxnumint.out';
diary(dfile);


niter=400;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'poissexp', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_poissexp.mat',datetime(2),datetime(3)));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************
mparam.klength=10;
mparam.alength=0;
mparam.ktrue=zeros(mparam.klength,1);
%set ktrue to be a sine wave
mparam.ktrue=2*[cos([0:mparam.klength-1]*pi/(mparam.klength-1))]';
%mparam.ktrue(:,1)=2*mparam.ktrue/max(mparam.ktrue);
%mparam.ktrue=[1;0];


%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);    

%set mparam.mmag so that when 50% of energy is along the true parameter
%the avg number of spikes is 5 
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(.5*m*(mparam.ktrue'*mparam.ktrue)^.5)-5);
mmag=fsolve(fsetmmag,1,optim);


v=load(oldfile);
mparam.pinit=v.mparam.pinit;
mmag=v.mparam.mmag;
mparam.ktrue=v.mparam.ktrue;
mparam=MParamObj('ktrue',mparam.ktrue,'pinit',mparam.pinit,'mmag',mmag);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('glm',glm,'theta', [mparam.ktrue;mparam.atrue]);

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d();

%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimmax=PoissExpMaxMI();

simmax=SimulationBase('glm',glm,'stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid);
[pmax,srmax,simparammax,mparammax]=maxupdateobj('mparam',mparam,'simparam',simmax,'ntorun',niter);
savesim('simfile',fname,'pdata',pmax,'sr',srmax,'simparam',simparammax,'mparam',mparammax);
fprintf('Saved to %s \n',fname);

%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimrand=RandStimNorm('mmag',mparam.mmag,'klength',mparam.klength);
simrand=SimulationBase('glm',glm,'stimchooser',stimrand,'observer',observer,'updater',updater,'simid',simid);
[prand,srrand,simparamrand,mparamrand]=maxupdateobj('mparam',mparam,'simparam',simrand,'ntorun',niter);
savesim('simfile',fname,'pdata',prand,'sr',srrand,'simparam',simparamrand,'mparam',mparamrand);
fprintf('Saved to %s \n',fname);

%turn off diary
diary('off');
%open the file
edit(dfile);

%check results match old results
if (pmax.m(:,niter+1)~=v.pmax.newton1d.m(:,niter+1))
    error('results dont match');
else
    fprintf('Success results match');
end
return;

