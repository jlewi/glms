%make plots of the 2-dobjective surface for both the log nonlinearity
%and the poisson nonlinearity

dotmu=[-10:1:10];
sigmasq=1:1:40;

objlog=PreCompMIObj('filename',fullfile(RESULTSDIR,'precompmi/01_22_compmi_001.mat'));
pname=fullfile(RESULTSDIR,'precompmi/01_29_poisscanonobj_001.mat')

micanon=@(muproj,sigma,v1,v2)(muproj+.5*sigma+log(sigma));

objexp=PreCompMIObj('filename',pname,'glm',GLMModel('poisson','canon'),'mifunc',micanon,'mifopts',[],'dotmu',dotmu,'sigmasq',sigmasq);