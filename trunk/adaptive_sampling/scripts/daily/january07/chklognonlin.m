%1_05_07
%Plot the firing rates to check the nonlinearity is really log(1+exp(u)

fdata=fullfile(RESULTSDIR,'numint','01_05','01_05_numint_002.mat');

fdata=fullfile(RESULTSDIR,'numint','01_13','01_13_numint_001.mat');
%fdata=fullfile(RESULTSDIR,'numint','01_15','01_15_numint_001.mat');
load(fdata);

fchk.hf=figure;
fchk.xlabel='\epsilon';
fchk.ylabel='f(\epsilon)';
fchk.name='Check nonlinearity';
fchk.linewidth=4;

r=[srmax.newton1d.nspikes srrand.newton1d.nspikes]';
glmproj=mparam.ktrue'*[srmax.newton1d.y srrand.newton1d.y];

%sort the values in ascending order
[glmproj, ind]=sort(glmproj);
r=r(ind);

hold on;
fchk.hp(1)=plot(glmproj,r,getptype(1,1));
%plot(glmproj,r,getptype(1,2));
fchk.lbls{1}='Actual';

fchk.hp(2)=plot(glmproj,simparam.glm.fglmmu(glmproj),getptype(2,1));
plot(glmproj,simparam.glm.fglmmu(glmproj),getptype(2,2));
fchk.lbls{2}='predicted';

lblgraph(fchk);

%***********************************************************************
%plot the mean squared distance between the estimated parameters
%and the true theta
%*************************************************************************
mnames={'newton1d'};
fmse.hf=figure;
fmse.xlabel='Trial';
fmse.ylabel='M.S.E';
fmse.name='Mean Squared Error';
hold on;
pind=0;
for mind=1:length(mnames)
    mname=mnames{mind};
    
 
    pind=pind+1;
niter=simparammax.niter;
nmse=pmax.(mname).m-mparam.ktrue*ones(1,1+niter);
nmse=nmse.^2;
nmse=sum(nmse,1).^.5;
fmse.hp(1)=plot(0:niter,nmse,getptype(pind,2));
fmse.lbls{1}='Info. max.';

pind=pind+1;
niter=simparamrand.niter;
amse=prand.(mname).m-mparam.ktrue*ones(1,1+niter);
amse=amse.^2;
amse=sum(amse,1).^.5;
fmse.hp(pind)=plot(0:niter,amse,getptype(pind,2));
fmse.lbls{pind}='random';
end
lblgraph(fmse);