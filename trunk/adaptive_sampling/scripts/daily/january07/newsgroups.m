%txtstring='%s %s'

%for i=1:66
%    txtstring=sprintf('%s %%n',txtstring);
%end

fname=fullfile(RESULTSDIR, '20news','motor_windows_d200.txt');
fid= fopen(fname,'r') ;
if (fid~=-1)
tline=fgetl(fid);
docs=[];
ndocs=0;
classes=[];         %contain information about the classes
nclasses=0;
while (tline~=-1)
     ndocs=ndocs+1;
     ind=strfind(tline,' ');
     docs(ndocs).path=tline(1:(ind(1)-1));
     docs(ndocs).class=tline((ind(1)+1):(ind(2)-1));
     docs(ndocs).wcounts=(str2num(tline((ind(2)+1):end)))';

     %see if this class has already been identified 
    if (nclasses>0);
           cind=strmatch(docs(ndocs).class,{classes(:).class},'exact');
    else
        cind=[];
    end
     if ~isempty(cind)
         docs(ndocs).classid=classes(cind).classid;
     else
         %its a new class
         nclasses=nclasses+1;
         classes(nclasses).classid=nclasses;
         classes(nclasses).class=docs(ndocs).class;
     end
     tline=fgetl(fid);
end


fclose(fid);
else
    fprintf('couldnt open file %s \n',fname);
end
%wcounts = textscan(fid, '%s %s %n');
%wcounts = textscan(fid, txtstring);

%wcounts=dlmread('/tmp/test.txt',' ',0,2);

%fname=fullfile(RESULTSDIR, '20news','20news-bydate-train','motor_windows_d200.txt');
%wcounts=dlmread(fname,'',0,2);