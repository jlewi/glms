%1-08-07
%test my the optimizaiton procedure boundbox for doing a 2-d optimization
%of the objective function
glm=GLMModel('poisson',@glm1dlog);

simparam.truenonlinf=simparam.glm.fglmmu;   %the nonlinearity used to sample the data
simparam.sampdist=simparam.glm.sampdist;            %distribution from which observations are sampled.

%**************************************************************************
%Optimization:
%crate the object which specifies how we will we optimize the stimulus
%*********************************************************************
%parameters for numerical integration
pproc.numint.confint=.999;               %how much confidence for integrating the inner expectation
pproc.numint.inttol=10^-6;        % tolerance for numerical integration
simparam.finfo=FisherInfoObj('procedure','boundbox','pproc',pproc);


%***************************************************************
%create some sample data
post.m=[1;1];
post.c=[5 0;0 1];
mparam.klength=length(post.m);
mparam.alength=0;
mparam.mmag=1;
shist=[];
[einfo.evecs einfo.eigd]=svd(post.c);
einfo.esorted=-1;
einfo.eigd=diag(einfo.eigd);
[stim,fmax]=choosestim(simparam.finfo,post,shist,einfo,mparam,glm);

%plot the feasible region
%plotquadreg('name','value')
%       post -
%                   .m - mean
%                   .c  - covariance matrix
%       mmag - magnitude constraint
%       klength
%       alength
plotquadreg('post',post,'mmag',mparam.mmag,'klength',2,'alength',0);
