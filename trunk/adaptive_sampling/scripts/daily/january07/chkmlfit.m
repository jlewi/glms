%1-15-2007
%Estimated parameters don't seem to converge using ML
%test this by computing the likelihood at the true theta
%at the current estimate of the MAP

dfile=fullfile(RESULTSDIR,'numint','01_13','01_13_numint_001.mat');
dfile=fullfile(RESULTSDIR,'numint','01_15','01_15_numint_001.mat');
load(dfile);

mname='newton1d';

%*****************************************************************
%evaluate the derivative
glm=simparamrand.glm;
truep.grad=d2glm(mparam.ktrue,mparam.pinit,srrand.newton1d.nspikes,srrand.newton1d.y,simparamrand.glm);

truep.gradnoerr=d2glm(mparam.ktrue,mparam.pinit,glm.fglmmu(srrand.newton1d.y'*mparam.ktrue)',srrand.newton1d.y,simparamrand.glm);

%map gradient
mapest=prand.(mname).m(:,end);
map.grad=d2glm(prand.(mname).m(:,end),mparam.pinit,srrand.newton1d.nspikes,srrand.newton1d.y,simparamrand.glm);
map.gradnoerr=d2glm(mapest,mparam.pinit,glm.fglmmu(srrand.newton1d.y'*mparam.ktrue)',srrand.newton1d.y,simparamrand.glm);


glm.fglmmu(srrand.newton1d.y'*prand.newton1d.m(:,end))


fmatch.hf=figure;
fmatch.xlabel='epsilon';
fmatch.hp=[];
fmatch.lbls={};
hold on;

pind=0;
pind=pind+1;;
fmatch.hp(pind)=plot(simparamrand.glm.fglmmu(srrand.newton1d.y'*prand.newton1d.m(:,end)),srrand.newton1d.nspikes,'b.');
fmatch.lbls{pind}='MAP';
pind=pind+1;
fmatch.hp(pind)=plot(simparamrand.glm.fglmmu(srrand.newton1d.y'*mparam.ktrue),srrand.newton1d.nspikes,'g.');
fmatch.lbls{pind}='True Parameter';

lblgraph(fmatch);


fhist.hf=figure;
fhist.xlabel='$f(x^t\theta)$'
fhist.ylabel='Counts';
hist(sort(glm.fglmmu(srrand.newton1d.y'*mparam.ktrue)),45);
fhist=lblgraph(fhist);
set(fhist.hxlabel,'interpreter','latex');