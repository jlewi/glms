%computes the lower bounds of the feasible region using some data
%the point is to get an idea of the range on which the objective function
%should be precomputed
function [ffeas]=apfeasiblereg(varargin)

%default names
mname='newton1d';

for j=1:2:nargin
switch varargin{j}
    case {'fname','filename'}
        fname=varargin{j+1};
    otherwise
        if exist(varargin{j},'var')
            eval(sprintf('%s=varargin{j+1};',varargin{j}));
        else
        fprintf('%s unrecognized parameter \n',varargin{j});
        end
end
end

load(fname);

niter=simparamrand.niter;
mmag=mparam.mmag;
%region
for trial=1:niter
    mumag=prand.(mname).m(:,trial);
    mumag=(mumag'*mumag)^.5;
    
    if (mumag~=0)
        freg(trial).lbdotmu=-mmag*mumag;
        freg(trial).ubdotmu=mmag*mumag;
    end

    [eigd]=svd(prand.(mname).c{trial});

   freg(trial).lbsigmasq=eigd(end)*mmag^2;
    freg(trial).ubsigmasq=eigd(1)*mmag^2;
end

ffeas.hf=figure;
subplot(2,1,1);
hold on;
hp(1)=plot(1:niter,[freg.ubdotmu],getptype(1),'Linewidth',4,'MarkerSize',25);
hp(2)=plot(1:niter,[freg.lbdotmu],getptype(2),'Linewidth',4,'MarkerSize',10);
legend('upper bound','lower bound');
ylabel('dotmu','fontsize',15);

subplot(2,1,2);
hold on;
hp(1)=plot(1:niter,[freg.ubsigmasq],getptype(1),'Linewidth',4,'MarkerSize',25);
hp(2)=plot(1:niter,[freg.lbsigmasq],getptype(2),'Linewidth',4,'MarkerSize',10);
legend('upper bound','lower bound');
ylabel('sigmasq','fontsize',15);
xlabel('trial','fontsize',15);
