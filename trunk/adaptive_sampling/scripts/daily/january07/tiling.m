clear all;
setpaths
%create the nonlinearity
glm=GLMModel('poisson',@glm1dlog);

%now solve for epsilon such that 
%p(r|x,theta) <epsilon
%fix r as zero
r=0;
%pthresh - threshold for being able to reject theta
pthresh=.005;
f=@(glmproj)(glm.pdf(r,glm.fglmmu(glmproj))-pthresh);

optim=optimset('tolfun',10^-8);
gpinit=1;
[gplim]=fsolve(f,gpinit,optim);