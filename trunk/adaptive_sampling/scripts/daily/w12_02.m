%COde fragments from 12-02-2005

%make a plot of the log likelihood after observing all the datapoints
theta=linspace(1.5,2.5,100);
du=upts(2)-upts(1);

%compute the log likelihood of each observation
%under the different models
for index=1:length(sr.nspikes)
    r=glm1dexp(sr.y(index)*theta);
    lpobsrv(:,index)=hpoissonll(sr.nspikes(index),r,mparam.tresponse);
end

figure;
plot(theta,sum(lpobsrv,2));
xlabel('\theta');
ylabel('Log Likelihood');

%compute the true posterior after so many iterations
%use "batch updating"
%compute the log likelihood of the prior
lprior=-(theta-0).^2/(2*1);

figure;
niter=500;
plot(theta,lprior'+sum(lpobsrv(:,1:niter),2));
xlabel('\theta');
ylabel('Log of Posterior');
title(sprintf('%0.3g iterations',niter));

