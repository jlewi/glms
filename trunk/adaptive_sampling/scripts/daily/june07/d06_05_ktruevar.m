%compute the variance in the direction of the true theta

dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_05','06_05_poissexp_001.mat');
dsets(end).lbl='info. max. heuristic';
dsets(end).simvar='simmax';


dind=1;[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);

mparam=simdata.extra.mparam;
ktrue=mparam.ktrue;
nktrue=normmag(ktrue);

vtrue=[];
trial=[];

for j=1:length(pdata.c)
    if ~isempty(pdata.c{j})
        vtrue(end+1)=nktrue'*pdata.c{j}*nktrue;
        trial(end+1)=j-1;
    end
end
fmucorr.hf=figure;
fmucorr.xlabel='trial';
fmucorr.ylabel='\theta^T C_t \theta';
fmucorr.title='Variance in direction of ktrue';
fmucorr.name='varktrue';
fmucorr.fontsize=20;


fmucorr.hp(1)=plot(trial,vtrue);
lblgraph(fmucorr);