%make embs figures for the entropy
%
%run maxplots
%parameters for tracking entropy
whichdata=1;    %0 means tracking entrop
                          %1 means high d results
                          
if (whichdata==0)
  %  load(fullfile('results', 'tracking','03_31','03_31_maxtrack_003.mat'))
  %     load(fullfile('results', 'tracking','04_02','04_02_maxtrack_002.mat'));
       load(fullfile('results', 'tracking','04_18','04_18_maxtrack_001.mat'));
%    xlimits=[30 200];  %what xlimits to use
    monf{1}='newton1d'
    outfile=fullfile(pwd,'writeup','embs_abstract','trackentropy.eps');
    xlimits=[0 1000];

else
    %for high d entropy
  %load('results\maxinfo\03_22\03_22_max2d_001.mat')
%   load(fullfile('results', 'maxinfo','03_22','03_22_max2d_001.mat'))
   load(fullfile('results', 'tracking','04_18','04_18_maxtrack_005.mat'));
  monf{1}='newton1d';
   xlimits=[0  1000];  %what xlimits to use
    outfile=fullfile(pwd,'writeup','embs_abstract','highdentropy.eps');
end


%maxplots;

fentropy.hf=figure;
fentropy.hp=[];
fentropy.x=[0:simparam.niter];
fentropy.xlabel='Iteration';
fentropy.ylabel='Entropy';
fentropy.title='';
fentropy.name='Entropy';    %window name
fentropy.on=1;
fentropy.fname='entropy';
fentropy.axisfontsize=20;
fentropy.lgndfontsize=20;


        %***************************************************************
        %figure of the posterior entropy
        %*********************************************************
        
        pmethods=pmax;
        mindex=1;
        entropy=zeros(1,simparam.niter+1);
        
        %need to know dimensionlaity of filter to compute entropy
        filtlength=size(pmax.(monf{mindex}).m,1);
        for index=0:simparam.niter
            eigd=eig(pmethods.(monf{mindex}).c{index+1});
            %entropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(pmethods.(monf{mindex}).c{index+1})));
            %this computation avoids
            entropy(index+1)=1/2*filtlength*(1+log2(2*pi))+1/2*sum(log2(abs(eigd)));
        end
        randentropy=entropy;
        
        pmethods=prand;
        mindex=1;
        entropy=zeros(1,simparam.niter+1);
        %need to know dimensionlaity of filter to compute entropy
        filtlength=size(pmethods.(monf{mindex}).m,1);
        for index=0:simparam.niter
            eigd=eig(pmethods.(monf{mindex}).c{index+1});
            %entropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(pmethods.(monf{mindex}).c{index+1})));
            %this computation avoids
            entropy(index+1)=1/2*filtlength*(1+log2(2*pi))+1/2*sum(log2(abs(eigd)));
        end
        maxentropy=entropy;
        
        
figure(fentropy.hf);
hold on;
subsample=10;

pind=1;

fentropy.hp(pind)=plot(0:subsample:simparam.niter,maxentropy(1:subsample:end),'k.');
fentropy.hpline(pind)=plot(0:simparam.niter,maxentropy,'k-');
fentropy.lbls{pind}=sprintf('Random');

pind=2;
fentropy.hp(pind)=plot(0:subsample:simparam.niter,randentropy(1:subsample:end),'kx');
fentropy.hpline(pind)=plot(0:simparam.niter,randentropy,'k-');
fentropy.lbls{pind}=sprintf('Information Maximizing');

        
lblgraph(fentropy);

figure(fentropy.hf);
xlim(xlimits);
hold on;
        
%figure(fentropy.hf);
set(fentropy.hp(1),'Marker','x');
set(fentropy.hpline(1),'LineWidth',2);
set(fentropy.hp(1),'MarkerSize',15);
set(fentropy.hp(1),'Color',[0 0 0])

set(fentropy.hp(2),'Marker','.');
set(fentropy.hpline(2),'LineWidth',2);
set(fentropy.hp(2),'MarkerSize',25);
set(fentropy.hpline(2),'Color',[0 0 0])
%savegraph('entropy.eps',fentopy.hf)
exportfig(fentropy.hf,outfile,'bounds','tight');
