%04-02-07
%
% Prepare data for Machens review paper
fname=fullfile(RESULTSDIR,'tracking/06_03/06_03_gabor_001.mat');
load(fname);

%create two matrices to store the pmap estiamtes on each trial

infomax.m=zeros(mparam.gheight,mparam.gwidth,simparammax.niter);
iid.m=zeros(mparam.gheight,mparam.gwidth,simparamrand.niter);
true.m=reshape(mparam.ktrue,mparam.gheight,mparam.gwidth);

for j=1:(simparammax.niter+1)
    infomax.m(:,:,j)=reshape(pmax.newton1d.m(:,j),mparam.gheight,mparam.gwidth);
    iid.m(:,:,j)=reshape(prand.newton1d.m(:,j),mparam.gheight,mparam.gwidth);
end


fout=fullfile(RESULTSDIR,'machens.mat');
save(fout,'infomax','iid','true');

infomax=[];
iid=[];
infomax.entropy=pmax.newton1d.entropy;
iid.entropy=prand.newton1d.entropy;
fout=fullfile(RESULTSDIR,'machen_entropy.mat');
save(fout,'infomax','iid','true');



infomax.mse=(pmax.newton1d.m-mparam.ktrue*ones(1,simparammax.niter+1)).^2;
infomax.mse=sum(infomax.mse,1);
infomax.mse=infomax.mse.^.5;

iid.mse=(prand.newton1d.m-mparam.ktrue*ones(1,simparammax.niter+1)).^2;
iid.mse=sum(iid.mse,1);
iid.mse=iid.mse.^.5;
fmse.hf=figure;

hold on;
fmse.hp(1)=plot(1:simparammax.niter+1,infomax.mse,getptype(1));
fmse.lbls{1}='Info. max';

fmse.hp(2)=plot(1:simparammax.niter+1,iid.mse,getptype(2));
fmse.lbls{2}='I.I.D';

lblgraph(fmse);
