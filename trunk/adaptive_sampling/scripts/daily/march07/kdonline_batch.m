%03-08-2007
%
%Compute the KL distance between the batch and online approximations of the
%kl distance.

fname='/home/jlewi/cvs_ece/results/adaptive_sampling/results/nips/11_15/11_15_lowdcos_001.mat';

load(fname);

kl=[];
trials=[];
for j=1:size(pmax.newton1d.m,2)
    if (~isempty(pmax.newton1d.c{j}) && ~isempty(pmax.newton1d.c{j}))
       gbatch.m=pmax.allobsrv.m(:,j);
      gbatch.c=pmax.allobsrv.c{j};
      
      gonline.m=pmax.newton1d.m(:,j);
      gonline.c=pmax.newton1d.c{j};
%      gonline.m=gbatch.m;
      
      kl(end+1)=kldistgauss(gbatch,gonline);
      trials(end+1)=j-1;
    end
end


fkl.hf=figure;
fkl.xlabel='Trial';
fkl.ylabel='KL distance';
fkl.name='Kl Distance';
fkl.title='KL distance batch and online gaussian approximations';
fkl.hp=plot(trials,kl,getptype(2));
lblgraph(fkl);
