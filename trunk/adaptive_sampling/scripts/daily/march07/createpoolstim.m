%3-14-07
setpathvars
%create some stimuli to use in pool based simulations
%klength=25*33;
%klength=11*15;
klength=11*11;
klength=1600;

datetime=clock;
%RDIR=fullfile(RESULTSDIR,'poolbased');
%%set resultsdir to the appropriate directory otherwise we won't be able to
%find the poolfile.
RESULTSDIR='/mnt/ext_harddrive/adaptive_sampling_results_06_13_2008/allresults';

RDIR=fullfile(RESULTSDIR,'poolbased');
fname=fullfile(RDIR, sprintf('pooldata_%dd_%s.mat',klength,datestr(datetime,'yymmdd')));
fname=seqfname(fname);

nstim=1000;
%nstim=10^5;
%stimgen=RandStimBall('mmag',1,'klength',klength);
stimgen=RandStimNorm('mmag',1,'klength',klength);



stim=zeros(klength,nstim);

for j=1:nstim
    stim(:,j)=choosestim(stimgen);
end

%generate a random number to save with the file
%this will be used to determine if a file has changed
%i.e if an object uses a particular file. It can store the fileid number
%and if it accesses it again and the fileid has changed we know the file
%has changed.
%Initialize rand to a different state each time:
rand('state', sum(100*clock));
fileid=rand(1)+floor(rand(1)*10);
%save it
vnames.stim='';
vnames.fileid='';
sopts.cdir=1;
sopts.append=1;

savedata(fname,vnames,sopts);
fprintf('Data saved to: \n %s \n',fname);