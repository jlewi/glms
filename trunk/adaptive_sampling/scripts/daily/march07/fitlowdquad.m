%This script takes data from an experiment where the data was generated
%using the model
%E(r)=f((k1'*x)^2+(k2'*x)^2)
%
%It uses maximum likelihood (ignores the prior) to estimate k1, k2.
clear all;
setpaths;

fname=fullfile(RESULTSDIR,'poolbased','03_20','03_20_poissexp_004.mat');

[pmax simmax, mparam,srmax]=loadsim('simfile',fname,'simvar','simmax');
[prand simrand, mobj,srrand]=loadsim('simfile',fname,'simvar','simrand');

lowdupdater=BatchMLQuad2d();

tstep=10;
trials=[tstep:tstep:simrand.niter];
mparam=simrand.extra.mparam;
k1=zeros(mparam.kl,length(trials));
k2=zeros(mparam.kl,length(trials));
kinit=zeros(2*mparam.kl,length(trials));
Aest=zeros(mparam.kl^2,length(trials));
mse=zeros(1,length(trials));
for tind=1:length(trials)
    trial=trials(tind);
    fprintf('Trial %d \n',trial);
    
    %ki.m=4*rand(20,1).*sign(binornd(1,.5,20,1)-.5);
%    ki.m=normrnd(0,2,20,1);
%initialize using first 2 components of the svd of the current estimate
    [u s v]=svd(reshape(pmax.m(:,trial+1),mparam.kl,mparam.kl));
    ki.m=[u(:,1);v(:,1)];
    kinit(:,tind)=ki.m;
    
%intilization point for search is the true values
%kinit.m=[simrand.extra.mparam.k1;simrand.extra.mparam.k2];
%*****************************************************************
%compute MLE i.i.d data
%*****************************************************************
stim=srrand.y(:,1:trial);
obsrv=srrand.nspikes(:,1:trial);
[post,uinfo,einfo,iid.k1(:,tind),iid.k2(:,tind)]=update(lowdupdater,ki,stim,mobj,obsrv,[]);

%compute the M.S.E in the higher d space
%do it in the higher d space because then we don't have to worry about
%being in local min (i.e interchanging k1, k2);
At=iid.k1(:,tind)*iid.k1(:,tind)'+iid.k2(:,tind)*iid.k2(:,tind)';
iid.Aest(:,tind)=At(:);
iid.mse(tind)=(sum((iid.Aest(:,tind)-mparam.ktrue).^2))^.5;

%*****************************************************************
%compute MLE info. max data
%*****************************************************************
stim=srmax.y(:,1:trial);
obsrv=srmax.nspikes(:,1:trial);
[post,uinfo,einfo,imax.k1(:,tind),imax.k2(:,tind)]=update(lowdupdater,ki,stim,mobj,obsrv,[]);

%compute the M.S.E in the higher d space
%do it in the higher d space because then we don't have to worry about
%being in local min (i.e interchanging k1, k2);
At=imax.k1(:,tind)*imax.k1(:,tind)'+imax.k2(:,tind)*imax.k2(:,tind)';
imax.Aest(:,tind)=At(:);
imax.mse(tind)=(sum((imax.Aest(:,tind)-mparam.ktrue).^2))^.5;
end

% fmse.hf=figure;
% fmse.xlabel='Trial';
% fmse.ylabel='MSE';
% fmse.name='MSE';
% 
% hold on;
% plot(trials,mse);
% lblgraph(fmse);

%compute the mse of the predicted mean firing rates
%this side steps the issue of their being potential local minima in our
%model
%The full model has more parameters therefore it should give smaller errors
%on the test set
%Therefore use stimuli in the pool which were not used for training
%%
stimind=[srrand.stiminfo{:} srmax.stiminfo{:}];
stimind=[stimind.sind];
trainind=sort(stimind);

%set testind to stimuli which are not in the training set
testind=[1:size(simrand.stimchooser.Poolstim.stimpool,2)];
testind(trainind)=0;
testind=find(testind>0);

%how large to make the training set
ntest=300;
sind=ceil(length(testind)*rand(1,ntest));
sind=testind(sind); %indexes for the testset

stimtest=simrand.stimchooser.Poolstim.stimpool(:,sind);

testmse.fullmod=zeros(1,length(trials));
testmse.lowdmod=zeros(1,length(trials));
testmse.infomax=zeros(1,length(trials));
for tind=1:length(trials)
    trial=trials(tind)
    %true mean firing rate
    truerate=compexpr(mobj,stimtest,simrand.observer.theta);

    %compute the mse of the predicted firing rates under the fullmodel
    fullpred=compexpr(mobj,stimtest,prand.m(:,trial+1));

    %compute the mse of the predicted firing rates under the fullmodel
    lowdpred=compexpr(mobj,stimtest,iid.Aest(:,tind));

    %compute the mse of the predicted firing rates under the fullmodel
    imaxpred=compexpr(mobj,stimtest,pmax.m(:,trial+1));

    %compute the mse of the predicted firing rates under the fullmodel
    imaxlowdpred=compexpr(mobj,stimtest,imax.Aest(:,tind));
    
    testmse.fullmod(tind)=(sum((fullpred-truerate).^2))^.5;
    testmse.lowdpred(tind)=(sum((lowdpred-truerate).^2))^.5;
    testmse.imaxpred(tind)=(sum((imaxpred-truerate).^2))^.5;
    testmse.imaxlowdpred(tind)=(sum((imaxlowdpred-truerate).^2))^.5;
end

ftest.hf=figure;
ftest.xlabel='trial';
ftest.ylabel='MSE';
ftest.title='MSE on test set';
ftest.name='MSE Test';
hold on;
pind=1;
ftest.hp(pind)=plot(trials,testmse.fullmod,getptype(pind));
ftest.lbls{pind}='Full Model I.I.D';


pind=pind+1;
ftest.hp(pind)=plot(trials,testmse.imaxpred,getptype(pind));
ftest.lbls{pind}='Full D Model Info. Max';

pind=pind+1;
ftest.hp(pind)=plot(trials,testmse.lowdpred,getptype(pind));
ftest.lbls{pind}='Low D Model I.I.D';

pind=pind+1;
ftest.hp(pind)=plot(trials,testmse.imaxlowdpred,getptype(pind));
ftest.lbls{pind}='Low D Model Info. Max';

lblgraph(ftest);
set(gca,'yscale','log');

[PATHSTR,NAME,EXT,VERSN] = fileparts(fname);
outfile=fullfile(PATHSTR,sprintf('%s_fitlowdquad.eps',NAME));
%exportfig(ftest.hf,outfile,'bounds','tight','color','rgb');
%%
