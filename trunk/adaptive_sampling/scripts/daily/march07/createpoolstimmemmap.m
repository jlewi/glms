%3-14-07
%create pool stim to a memormapped file
%first two doubles in the file store the number of rows and number of
%columns.
setpathvars
%create some stimuli to use in pool based simulations

%*******************************************************
%stim info
%*********************************************************
%%stimgen=RandStimBall('mmag',1,'klength',klength);
nstim=100000;
klength=25*33;
mmag=.437827221563237;
datetime=clock;

%***********************************************************
%file info
%********************************************************
RDIR=fullfile(RESULTSDIR,'poolbased');
fname=fullfile(RDIR, sprintf('pooldata_d%2.2d_%2.2d_%2.2d.dat',klength,datetime(2),datetime(3)));
fname=seqfname(fname);


stimgen=RandStimNorm('mmag',mmag,'klength',klength);
stim=zeros(klength,nstim);

for j=1:nstim
    if (mod(j,10)==0)
        fprintf('j=%d \n',j);
    end
    stim(:,j)=choosestim(stimgen);
end


%create the file for the memory map by writing to it
%we have to write zeros for all the data
fid = fopen(fname,'w');
fwrite(fid, [klength;nstim;stim(:)], 'double'); 
fclose(fid);

%to test memory map
%memfile = memmapfile(fname,'Format', {'double' [1] 'klength'; 'double' [1] 'nstim';'double' [klength,nstim] 'stim'},'Writable',true);
