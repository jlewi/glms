%this script sets up a simulation
%where we draw stimuli by sampling from the optimal Gaussian process.
%   We compare this to our greedy info. max. algorithm
%   And an algorithm which samples from the uniform ball.
%01-10-2009
%
%
function dfiles=jan09setupgpoptns()

dfiles=struct();

setpathvars;


fdir='asympopt';
fbase='gpsims';

savecint=500; %how often to save the covariance matrix;


%***********************************************************
%should replace with code for GLM object.
%***********************************************************
glm=GLMPoisson('canon');



%***********************************************************************
%load an actual bird song strf
%***********************************************************************
strffile=FilePath('RESULTSDIR','bird_song/david_strfs/br2523_06_003_song_strf.mat');
dstrf=load(getpath(strffile));

klength=10;
ktlength=10;

%David learns non-causal STRF's
%i.e future stimuli can affect the response
%the zero point corresponds to the midpoint across columns
%all points after the midpoint correspond to past stimuli
%so we only take the left half of the strf and we flip it
%to so that the first column corresponds to the oldest stimulus
midpoint=(1+size(dstrf.strf,2))/2;

strf=dstrf.strf(:,midpoint:end);
strf=strf(:,end:-1:1);


f=(250+129/2):129:8000-129/2;

%its in milliseconds
t=([-size(strf,2):1:-1]+1)*10^-3;

%scale the strf by 10^6. Otherwise the power constraint ends up being
%really large and this causes things to start blowing up
strf=strf*10^6;

%trunct the strf based on klength and ktlength
strf=strf(:,end-ktlength+1:end);
fi=[0:length(f)/klength:length(f)];
fi=fi(2:end);

strf=strf(fi,:);

f=f(fi);
t=t(end-ktlength+1:end);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mp.glm=GLMPoisson('canon');
maxrate=10;
mp.mmag=SimSetup.mmagformaxrate(mp.glm, (strf(:)'*strf(:))^.5,maxrate);
mp.klength=size(strf,1);
mp.ktlength=size(strf,2);
mobj=MParamObj(mp);






%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
%pick two orthonormal vectors for theta
theta=strf(:);
observer=GLMSimulator('model',mobj,'theta', theta);

%**************************************************************************
%Pool Stimuli
%***********************************************************************

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d('maxiter',1000,'compeig',false);
uoptim=getoptim(updater);
uoptim.TolFun=10^-5;
updater=setoptim(updater,uoptim);

% 
% updater=BatchML();

sparam.updater=updater;
sparam.mobj=mobj;
sparam.observer=observer;
sparam.fbase=fbase;
sparam.fdir=fdir;
sparam.compentropy=true;
sparam.savecint=savecint;

%save the time and frequency bins corresponding to the strf
sparam.extra.t=t;
sparam.extra.f=f;
sparam.extra.maxrate=maxrate;
sparam.extra.strffile=strffile;

%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using the psopt distribution saved in psfile
%      2) Using a uniform distribution on the same stimuli
%**************************************************************************

dind=0;
%*****************************************************************
%Sim: Estimate the optimal distribution using the posterior
%******************************************************************
dind=dind+1;
lbl='optgp';
stimobj=POptAsymGPPCanonLB('powcon',mobj.mmag^2,'normalize',false);



sparam.stimchooser=stimobj;
sparam.label=lbl;
about=sprintf('Compute the optimal gaussian distribution without enforcing stationarity constraints.');
about=sprintf('%s\n Sample this distribution every tk trials where tk is the temporal duration of the strf.',about);
about=sprintf('%s\n The true parameters are an STRF fitted to real data.',about);
sparam.about=about;
dnew=SimSetup.setup(sparam);
dfiles=copystruct(dfiles,dind,dnew);


%*****************************************************************
%Sim: Greedy Info. Max.
%******************************************************************
% dind=dind+1;
% lbl='optgreedy';
% 
% spgreedy=sparam;
% 
% 
% spgreedy.updater.compeig=true;
% 
% 
% 
% stimobj=PoissExpMaxMI();
% 
% spgreedy.stimchooser=stimobj;
% spgreedy.label=lbl;
% 
% dnew=SimSetup.setup(spgreedy);
% dfiles=copystruct(dfiles,dind,dnew);

%*****************************************************************
%Sim: Sample a white gp
%******************************************************************
dind=dind+1;
lbl='whitenoise';

spwhite=sparam;


tdim=getparamlen(mobj);

stimobj=WhiteNoise('var',(mobj.mmag^2)/tdim);

spwhite.stimchooser=stimobj;
spwhite.label=lbl;

about=sprintf('Draw white noise stimuli.');
about=sprintf('%s\n Sample this distribution every tk trials where tk is the temporal duration of the strf.',about);
about=sprintf('%s\n The true parameters are an STRF fitted to real data.',about);
spwhite.about=about;

dnew=SimSetup.setup(spwhite);
dfiles=copystruct(dfiles,dind,dnew);



