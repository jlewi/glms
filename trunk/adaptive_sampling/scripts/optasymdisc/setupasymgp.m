%this script sets up a simulation
%where we draw stimuli by sampling from the optimal Gaussian process.
%   We compare this to our greedy info. max. algorithm
%   And an algorithm which samples from the uniform ball.
%01-10-2009
%
%
function dfiles=setupoasym()

dfiles=struct();

setpathvars;


fdir='asympopt';
fbase='gpsims';

savecint=500; %how often to save the covariance matrix;


%***********************************************************
%should replace with code for GLM object.
%***********************************************************
glm=GLMPoisson('canon');



%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mp.glm=GLMPoisson('canon');
mp.mmag=3;
mp.klength=20;
mp.ktlength=1;
mobj=MParamObj(mp);






%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
%pick two orthonormal vectors for theta
theta=zeros(mp.klength,1);
theta(1)=1;
observer=GLMSimulator('model',mobj,'theta', theta);

%**************************************************************************
%Pool Stimuli
%***********************************************************************

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d('maxiter',1000,'compeig',false);
uoptim=getoptim(updater);
uoptim.TolFun=10^-5;
updater=setoptim(updater,uoptim);


%updater=BatchML();

sparam.updater=updater;
sparam.mobj=mobj;
sparam.observer=observer;
sparam.fbase=fbase;
sparam.fdir=fdir;
sparam.compentropy=true;
sparam.savecint=savecint;
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using the psopt distribution saved in psfile
%      2) Using a uniform distribution on the same stimuli
%**************************************************************************

dind=0;
%*****************************************************************
%Sim: Estimate the optimal distribution using the posterior
%******************************************************************
dind=dind+1;
lbl='optgp(unnormalized)';
stimobj=POptAsymGPPCanonLB('powcon',mobj.mmag^2,'normalize',false);



sparam.stimchooser=stimobj;
sparam.label=lbl;

dnew=SimSetup.setup(sparam);
dfiles=copystruct(dfiles,dind,dnew);


%*****************************************************************
%Sim: Greedy Info. Max.
%******************************************************************
% dind=dind+1;
% lbl='optgreedy';
% 
% spgreedy=sparam;
% 
% 
% spgreedy.updater.compeig=true;
% 
% 
% 
% stimobj=PoissExpMaxMI();
% 
% spgreedy.stimchooser=stimobj;
% spgreedy.label=lbl;
% 
% dnew=SimSetup.setup(spgreedy);
% dfiles=copystruct(dfiles,dind,dnew);

%*****************************************************************
%Sim: Sample a white gp
%******************************************************************
dind=dind+1;
lbl='whitenoise';

spwhite=sparam;





tdim=getparamlen(mobj);
mus=zeros(tdim,1);
cs=eye(tdim)*(mobj.mmag^2)/tdim;
stimobj=SampStimDist('ps',StimGP('mu',mus,'c',{cs}));

spwhite.stimchooser=stimobj;
spwhite.label=lbl;

dnew=SimSetup.setup(spwhite);
dfiles=copystruct(dfiles,dind,dnew);


% %*****************************************************************
% %Sim: Sample a uniform process
% %******************************************************************
% dind=dind+1;
% lbl='uniform';
%
%
%
% stimobj=RandStimNorm('mmag',mobj.mmag,'klength',mobj.klength);
%
% sparam.stimchooser=stimobj;
% sparam.label=lbl;
%
% dnew=SimSetup.setup(sparam);
% dfiles=copystruct(dfiles,dind,dnew);
