%this script sets up a simulation
%where we draw stimuli by sampling from a discrete distribution
%The distribution we sample from is fixed before the experiment starts
%
%01-10-2009
%
%
function dfiles=setupoasym()

dfiles=struct();

setpathvars;


fdir='asympopt';
fbase='psdisc';

savecint=10; %how often to save the covariance matrix;

%************************************************************
%load the file containing the StatDistDisc distribution object
%************************************************************
%psfile=FilePath('RESULTSDIR',fullfile('asympopt','090110','psopt_001.mat'));

%3 stimuli
%psfile=FilePath('RESULTSDIR',fullfile('asympopt','090110','psopt_002.mat'));

%31 stimuli linearly spaced on interval [-2 2];
%psfile=FilePath('RESULTSDIR',fullfile('asympopt','090110','psopt_003.mat'));

%10 stimuli ktlength=2, klength=2;
psfile=FilePath('RESULTSDIR',fullfile('asympopt','090117','psopt_kl2_kt2_nx10_001.mat'));

psvar=load(getpath(psfile));

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMPoisson('canon');



%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mobj=psvar.mobj;





%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', psvar.theta);

%**************************************************************************
%Pool Stimuli
%***********************************************************************

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d('maxiter',1000,'compeig',false);
uoptim=getoptim(updater);
uoptim.TolFun=10^-5;
updater=setoptim(updater,uoptim);


updater=BatchML();

sparam.updater=updater;
sparam.mobj=mobj;
sparam.observer=observer;
sparam.fbase=fbase;
sparam.fdir=fdir;
 sparam.compentropy=true;
sparam.savecint=savecint;
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using the psopt distribution saved in psfile
%      2) Using a uniform distribution on the same stimuli
%**************************************************************************

dind=0;
%*****************************************************************
%Sim: Estimate the optimal distribution using the posterior
%******************************************************************
dind=dind+1;
lbl='infomax';
stimobj=POptAsymDistDisc('stim',psvar.psopt.stim,'ktlength',mobj.ktlength,'nthetasamps',10);


sparam.stimchooser=stimobj;

sparam.label=lbl;

 dnew=SimSetup.setup(sparam);
 dfiles=copystruct(dfiles,dind,dnew);

%*****************************************************************
%Sim: Sample the optimal GP
%******************************************************************
lbl='truetheta';

stimobj=SampStimDistDisc('ps',psvar.psopt);

sparam.label=lbl;
sparam.stimchooser=stimobj;
dnew=SimSetup.setup(sparam);
dind=length(dfiles)+1;
dfiles=copystruct(dfiles,dind,dnew);
 
% %*****************************************************************
% %Sim: Sample a uniform process
% %******************************************************************
dind=length(dfiles)+1;
lbl='uniform';
psuni=StatDistDisc('stim',psvar.psopt.stim,'ktlength',mobj.ktlength);

stimobj=SampStimDistDisc('ps',psuni);

 sparam.label=lbl;
sparam.stimchooser=stimobj;
dnew=SimSetup.setup(sparam);
dfiles=copystruct(dfiles,dind,dnew);
