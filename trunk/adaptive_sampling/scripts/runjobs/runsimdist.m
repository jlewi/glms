%function runsimdist(dfiles, ntorun)
%       dfiles - a structure array
%              - each element is a structure specifying the files for one simulation
%       ntorun - number of trials to run
%              - this can be an array in which case each element of ntorun is a unique job which can't run until
%               the previous job completed. This allows us to easily break up a long run into many subtrials so that we can
%               inspect intermediary trials.
%
%
%Explanation: This is a script which sets up a distributed job for
%running the info. max. algorithm
%
%   Running it as a distributed job isn't very efficient because we can
%   parallelize the optimization of the stimuli.
%
%   However this is useful for running simulations which randomly shuffle
%   the stimuli since we can't parallelize the stimulus selection in this
%   case.
function [siminfo]=runsimdist(dfiles,ntorun)
setpathvars;






%**************************************************************************
%parameters
%
%The values below should be customized for your setup
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/svn_glms/adaptive_sampling';

%whether or not to capture command window output
capcmdout=false;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=false;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
svndirs={'~/svn_glms'};



%name to use for the job
jname='simrun';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='~/svn_trunk/allresults';

%if there's some code you want to execute before starting
%the function (i.e setting the path) you can specify a handle to this
%function
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=@taskstartupfunc;

%The directory on your local computer where jobs should be stored
localjobdir='~/jobs';



%**************************************************************************
%update the node on the cluster
%*************************************************************************

if (svnupdate)
    wd=pwd;
    for j=1:length(svndirs)
        if ~exist(svndirs{j},'dir')
            error(['Cannot issue svn update in %s, directory does not ' ...
                'exist.'],svndirs{j});
        end
        cd(svndirs{j});
        fprintf('Issueing svn update in %s \n',svndirs{j});

        system('svn update');

    end
    cd(wd);
end


%*********************************************************************
%setup the job
%************************************************************************

%get the scheduler
%distsched.m is a function which is part of the Neurolab Matlab toolbox
%which creates a scheduler for use with the Neurolab Matlab cluster.
sched=distsched();





%****************************************************
%load the data set
%*****************************************************
%jobid's is a length(ntorun)*length(siminfo) structure
%storing the jobids of each run
%this is used to set jdep appropriately
jobids=nan(length(ntorun),length(dfiles));



for sindex=1:length(dfiles)

    for rind=1:length(ntorun)

        if (rind>1)
            if (isnan(jobids(rind-1,sindex)))
                jdep=[];
            else
                jdep=num2str(jobids(rind-1,sindex));
            end
        else
            jdep=[];
        end
       


        %%we use rsync to copy the dataset from bayes to hear
        %src.host='bayes.neuro.gatech.edu';
        %src.fname=siminfo(sindex).dsetfile;

        %copy the dataset to the current directory
        %dest.host='';
        %dest.fname=fullfile(pwd,'datasettorun.m');

        %rsyncfile(src,dest);

        %dsets=datasettorun;

        siminfo(sindex).dfiles=dfiles(sindex);
        dsets=dfiles(sindex);
        
        %***********************************************************
        %Determine the files to copy to the cluster
        %**********************************************************
        filesin=[];
        fnames=fieldnames(dsets);
        for j=1:length(fnames)
            fname=fnames{j};
            if isa(dsets.(fname),'FilePath');
                filesin=[filesin dsets.(fname)];
            end
        end


        %***********************************************************
        %Determine the files to copy from the cluster to the host
        %**********************************************************
        filesout=[dsets.datafile dsets.mfile dsets.cfile dsets.statusfile dsets.inputsfile dsets.obsrvfile];

        if isfield(dsets,'poolfile')
            filesout=[filesout dsets.poolfile];
        end

        psubmit=get(sched,'SubmitFcn');


        set(sched,'SubmitFcn',{'sgeSubmitFcn',jdep});

        %**************************************************************************
        %create the parallel job
        %************************************************************************
        [jobdata]=initjobdata(jname,filesin,filesout,startdir,startupfunc,localhost,localdatadir);
        pjob=createJob(sched);
        pjob.jobData=jobdata;




        %********************************************************************************************
        %create the task
        %*************************************************************************************************
        nargout=1;
        ptask=createTask(pjob,@SimulationBase.simcont,nargout,{dsets.datafile,ntorun(rind),dsets.statusfile});
        set(ptask,'CaptureCommandWindowOutput', capcmdout);
        
        %set the task name to be the existing job name + the name of the datafile and the run        
        %pjob.Tasks(1).UserData.taskname=sprintf('J%dT1.%s',getfilename(dsets.datafile));
        set(pjob,'JobData',jobdata);

        %submit the job
        submit(pjob);


        %get the job id of this job
        %we use qstat to do this
        cmd=sprintf('qstat | awk ''$3 ~/%s/ {print $1}'' ',get(pjob,'name'));
        [s,w]=system(cmd);

        jobids(rind,sindex)=str2num(w);

        siminfo(sindex).jobs(rind)=pjob;
        siminfo(sindex).sgejobid(rind)=str2num(w);
        %***************************************************************************
        %Print sumarry information
        %******************************************************************************
        fprintf('\nJob Name: \t %s\n',get(pjob,'name'));
        fprintf('datafile: \t %s \n',getpath(dsets.datafile));
        fprintf('ntorun: \t %d \n', ntorun(rind));
    end
end %loop for ntorun







