%02-10-2008
%
%We want to make a plot illustrating by how much we can reduce
%the amount of data needed to achieve a speedup in the results
%
%
%[seqfiles]=phdbsdata_all_neurons();
clear variables

[seqfiles]=phdbs_data_infomax_tspace_001(); 

%use the info. max and shuffled designs. Ignore the tangent space design.
seqfiles=seqfiles([1:3 5:end],1:2);


forpaper=false;
if (forpaper)
    width=6.1;
    height=4;

    gdir='~/svn_trunk/publications/adaptive_sampling/phdthesis/figs';

    fext='eps';
    ftype='epsc2';
else
    width=7.5;
    height=5;
    gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation/bsfigs/';
    fext='png';
    ftype='png';
    fsize=16;
end


sname=mfilename();
%cutoff - this is the threshold we use with the trial to determine the
%maximimum prediction acuracy
%*******************************************************************
%data - is  a structure storing the results for each pair of infomax
%shuffle  response sets
%     - each row corresponds to a different pair
%     - each column corresponds to a different wavefile in the test set
tcutoff=2*10^4;

%lldata is  a structure which stores information which is specific to each
%BSExpllike object that we process
%    dimensions - npairs, size(seqfiles,2)

%We partition the fraction of the max predictive accuracy into bins
%bwidth- the width of the bins
bwidth=.005;
%bcent=bwidth/2:bwidth:1-bwidth/2;

bsspeed=BSSpeedup('seqfiles',seqfiles,'tcutoff',tcutoff,'bwidth',bwidth);

data=bsspeed.data;
bcent=bsspeed.bcent;

nwave=size(data,2);
%%

width=6;
height=3;

%itouse - which bcenters we want to plot
%we don't want to plot bcenters > 95% because computing the
%point at which this level of accuracy is achieved is very error prone
%because the curves flatten out
itouse=logical(bcent<=.95);

xl=[0 100];
yl=[175 500];

fspeed=FigObj('name','Speedup','width',width,'height',height,'naxes',[1,nwave]);


for wind=1:nwave
    setfocus(fspeed.a,1,wind);
    for pind=1:size(seqfiles,1)
        hp=plot(bcent*100,data(pind,wind).speedup*100);

        if wind==1
            addplot(fspeed.a(1,wind),'hp',hp,'lbl',sprintf('n%02g',pind));            
        else
        addplot(fspeed.a(1,wind),'hp',hp);
        end
    end
    
    xlabel(fspeed.a(1,wind),'% Max Accuracy');

end

%make the y limits the same
% yl=get([fspeed.a.ha],'ylim');
% yl=cell2mat(yl);
% yl=[min(yl(:,1)) max(yl(:,2))];
%yl=[0 100];
set([fspeed.a.ha],'ylim',yl);
set([fspeed.a.ha],'xlim',xl);

title(fspeed.a(1,1),'Bird song');
title(fspeed.a(1,2),'Ripple noise');
ylabel(fspeed.a(1,1),'% Speedup');    
lblgraph(fspeed);




%*********************************************************************
%Make a plot of the mean and standard deviation
%   Average across neurons and wave files
%******************************************************************
%%
favg=FigObj('name','Speedup','width',width,'height',height,'fontsize',fsize);


%compute the average
%and std
speedup=nan(numel(data),length(bcent));

for rind=1:numel(data)
   speedup(rind,:)=data(rind).speedup; 
end

%multiply by a 100 so its %
mspeedup=mean(speedup*100,1);
stdspeedup=std(speedup*100);

hmean=plot(bcent(itouse)*100,mspeedup(itouse));
pms=[];
pms.color='b';
pms.linestyle='-';
pms.linewidth=4.5;

if (forpaper)
addplot(favg.a,'hp',hmean,'pstyle',pms);
else   
    addplot(favg.a,'hp',hmean,'pstyle',pms,'lbl','mean');    
end
pstd=[];

if (forpaper)
pstd.color='g';    
else
pstd.color='k';
end
pstd.linestyle='--';
pstd.linewidth=4;
hstd1=plot(bcent(itouse)*100,mspeedup(itouse)+stdspeedup(itouse));
hstd2=plot(bcent(itouse)*100,mspeedup(itouse)-stdspeedup(itouse));

if (forpaper)
addplot(favg.a,'hp',hstd1,'pstyle',pstd);
else
     addplot(favg.a,'hp',hstd1,'pstyle',pstd,'lbl','s.t.d');    
end
addplot(favg.a,'hp',hstd2,'pstyle',pstd);


ylabel(favg.a(1,1),'% Speedup');  
xlabel(favg.a(1,1),'% Converged');  

%yl=[0 100];
set(favg.a.ha,'ylim',yl);
set(favg.a.ha,'xlim',xl);
lblgraph(favg);


if ~(forpaper)
    %create room for the legend
    set(favg.a,'position',[.1315 .1815 .7 .7435]) ;
    setposition(favg.a.hlgnd,[.82 .75 .16 .2139]);
    settleft(favg.a.hlgnd,.35);
end
%%
%**************************************************************************
%OneNote table
%**************************************************************************

explain='The solid-blue line shows the average speedup. The average is across all datasets and wave files in the test set. Solid lines show 1 standard deviation. For a list of the datafiles used see below.';
oavg={favg,{'script',sname;'bwidth',bwidth;'explain',explain}};


%create a table describing the datasets
dinfo=cell(size(seqfiles,1),3);
for pind=1:size(seqfiles,1);
   dinfo{pind,1}=sprintf('n%02g',pind);
   dinfo{pind,2}=sprintf('%s',getfilename(seqfiles(pind,1).rawdatafile));
   dinfo{pind,3}={'shuffle',getrpath(seqfiles(pind,1).datafile);'info. max',getrpath(seqfiles(pind,2).datafile)};
end
%add title
dinfo=[{'label','neuron','datafiles'};dinfo];
%create a onennotetable
explain=sprintf('Max accuracy measures how close the estimated model is to the final converged model. We compute this on any given trial by dividing the expected likelihood on that trial by the expected likelihood for the final (i.e converged) model \n');
explain=sprintf('%sSpeedup measures how many more trials the shuffled design requires than the info. max. design to achieve the same level of accuracy. Speedup is the number of trials  required by shuffled design divided by the number of trials required by the info. max. design, needed to achieve a given level of accuracy. We measure this as the difference in trials required for the info. max. and shuffled designs to achieve this level of accuracy.',explain);
oinfo={{'script',sname};dinfo;{'Description',explain}};

oall={fspeed,oinfo};

onenotetable([oavg;oall],seqfname('~/svn_trunk/notes/speedup.xml'));


%save the plot
savefigs=false;
if (savefigs)
    fname=fullfile(gdir,['bs_speedup.' fext]);
saveas(favg.hf,fname,ftype);            
end