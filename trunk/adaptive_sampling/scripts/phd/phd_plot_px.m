%Date 03-04-2009
%
%Explanation: To illustrate how the infinite horizon simplifies things,
%   plot the distribution the possible distributions as b goes to infinity

%which bsizes to use
bsizes=[2 10 20];

height=2.75;
width=8.5;
fontsize=18;
fh=FigObj('height',height,'width',width,'fontsize',fontsize,'name','noncovexity','xlabel','p(x_1)','naxes',[1 length(bsizes)+1]);


for bind=1:length(bsizes)
   setfocus(fh.a,1,bind); 
    
   p=[0:bsizes(bind)]/bsizes(bind);
   
   plot(p,1-p,'o','MarkerFaceColor','b');
   
   set(fh.a(1,bind).ha,'xlim',[0 1]);
   set(fh.a(1,bind).ha,'ylim',[0 1]);

   title(fh.a(1,bind),sprintf('b=%d',bsizes(bind)));
end

set([fh.a(1,2:end).ha],'yticklabel',[]);

ylabel(fh.a(1,1),'p(x_2)');

%plot it for infinity
bind=length(bsizes)+1;
p=[0,1];
setfocus(fh.a,1,length(bsizes)+1);
plot(p,1-p,'b-','LineWidth',4);

title(fh.a(1,bind),'b=\infty');


savefigs=false;
if (savefigs)
   gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation';
   saveas(fh.hf,fullfile(gdir,'px.png'),'png');
end