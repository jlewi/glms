%date: 01-24-2009
%
%Explanation: Make a plot of the obsrevations
clear variables;

[dsets]=gp_data_infomax_090208_001();

dsets=dsets(5:6);
dfiles=dsets;

for dind=1:length(dsets)
    v=load(getpath(dsets(dind).datafile));
    simobj(dind)=v.simobj;
end

forpaper=true;
if (forpaper)

    width=6;
    height=3;
    gdir='~/svn_trunk/publications/adaptive_sampling/phdthesis/batchopt_figs';

    fontsize=12;
    fext='eps';
    ftype='epsc2';
else

    width=6;
    height=2;

    gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation/';

    fontsize=14;
    fext='png';
    ftype='png';
end
fprintf('Changing simulation labels make sure they are correct !!!\n');
setlabel(simobj(1),'opt. gp.');
setlabel(simobj(2),'white noise');
%%


%how often to subsample the data before plotting
subsamp=500;

%throw out early trials for which MSE is increasing
starttrial=1000;

%rscale the scaling constant to get the firing rate from
%the number of spikes
%rscale=1/(10*10^-3);


%flen- the length of the cosine filter to use should be an odd number
flen=7;
fcoeff=sin([0:flen-1]*pi/(flen-1));
fcoeff=fcoeff/sum(fcoeff);


fh=FigObj('name','Observations','width',width,'height',height,'xlabel','trial','naxes',[1 3],'fontsize',fontsize);


pstyle(1).linewidth=8;
pstyle(1).color='r';
pstyle(2).linewidth=4;
pstyle(2).color='g';

hold on;

%plot just the first plot
for j=1

   


    %   fh.a=addplot(fh.a(,'hp',hp,'lbl',lbl,'pstyle',pstyle(j));
    %set(fh.a.ha,'ylim',[0 1]);

    
    obsrv=getobsrv(simobj(j).obsrv);
    
    %plot first 1000 observations
    pind=1;
    setfocus(fh.a,1,pind);
    trials=[1:1000];
    hp=plot(trials,obsrv(trials));
    set(fh.a(1,pind).ha,'xticklabel',get(fh.a(1,pind).ha,'xtick')/1000);
    
     %plot all observats
    pind=2;
    setfocus(fh.a,1,pind);
    %    obsrv=slidefilter(obsrv,fcoeff);
    hp=plot(1:getniter(simobj(j)),obsrv);
    lbl=getlabel(simobj(j));
    set(fh.a(1,pind).ha,'xlim',[0 getniter(simobj)]);
   set(fh.a(1,pind).ha,'xticklabel',get(fh.a(1,pind).ha,'xtick')/1000);
         
    pind=3;
    setfocus(fh.a,1,pind);

    trials=424*10^3:425*10^3;
    hp=plot(trials,obsrv(trials));
    set(fh.a(1,pind).ha,'xticklabel',get(fh.a(1,pind).ha,'xtick')/1000);
 
end

%%
pind=1;
set(fh.a(1,pind).ha,'ylim',[0 4]);
set(fh.a(1,pind).ha,'xscale','linear');
xlabel(fh.a(1,pind).ha,'Trial (x10^3)');
ylabel(fh.a(1,pind).ha,'r_t');


pind=2;
xlabel(fh.a(1,pind).ha,'Trial');
set(fh.a(1,pind).ha,'ylim',[0 4]);
set(fh.a(1,pind).ha,'yticklabel',[]);
xlabel(fh.a(1,pind).ha,'Trial (x10^3)');


pind=3;
xlabel(fh.a(1,pind).ha,'Trial (x10^3)');
set(fh.a(1,pind).ha,'ylim',[0 4]);
set(fh.a(1,pind).ha,'yticklabel',[]);

fh=lblgraph(fh);
setfsize(fh);
sizesubplots(fh);

odata={fh,''};

%setposition(fh.a.hlgnd,.15,.5,[],[])

savefigs=false;
if (savefigs)
    saveas(fh.hf,fullfile(gdir,['gp_frate.' fext]),ftype);
end

% otble=[{'script',mfilename()};otble];
% onenotetable(otble,seqfname('~/svn_trunk/notes/mse.xml'));