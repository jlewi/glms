function [dsets]= phdbs_data_infomax_tspace_001() 
%********************************


dsets=[];
%************************************************
% neuron: aa0708_04_001-Ch1-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=1;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_005.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_005.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_005.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_005.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_005.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_005_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: aa0708_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=1;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_006.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_006.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_006.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_006.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_006.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_006_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: aa0708_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=1;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_003.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_003.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_003.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_003.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_003.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_003_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_003_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: bb2728_02_001-Ch2-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=2;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_007.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_007.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_007.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_007.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_007.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_007_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: bb2728_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=2;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_008.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_008.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_008.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_008.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_008.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_008_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: bb2728_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=2;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_004.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_004.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_004.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_004.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_004.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_004_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_004_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: br2523_06_003-Ch1-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=3;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_001.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_001.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_001.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_001.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_001.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_001_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: br2523_06_003-Ch1-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=3;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_002.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_002.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_002.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_002.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_002.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_002_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: br2523_06_003-Ch1-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=3;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_002.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_002.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_002.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_002.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_002.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_002_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_002_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k302_06_001-Ch2-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=4;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_003.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_003.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_003.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_003.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_003.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_003_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k302_06_001-Ch2-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=4;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_004.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_004.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_004.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_004.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_004.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_004_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k302_06_001-Ch2-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=4;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_001.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_001.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_001.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_001.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_001.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_001_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_001_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k306_03_013-Ch1-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=5;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_009.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_009.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_009.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_009.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_009.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_009_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k306_03_013-Ch1-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=5;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_010.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_010.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_010.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_010.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_010.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_010_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k306_03_013-Ch1-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=5;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_005.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_005.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_005.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_005.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_005.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_005_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_005_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_02_001-Ch2-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=6;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_011.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_011.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_011.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_011.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_011.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_011_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=6;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_012.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_012.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_012.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_012.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_012.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_012_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=6;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_006.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_006.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_006.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_006.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_006.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_006_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_006_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_04_001-Ch1-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=7;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_023.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_023.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_023.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_023.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_023.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_023_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=7;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_024.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_024.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_024.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_024.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_024.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_024_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=7;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_012.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_012.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_012.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_012.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_012.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_012_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_012_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_05_001-Ch1-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=8;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_013.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_013.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_013.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_013.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_013.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_013_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_05_001-Ch1-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=8;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_014.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_014.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_014.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_014.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_014.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_014_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_05_001-Ch1-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=8;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_007.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_007.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_007.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_007.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_007.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_007_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_007_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_06_001-Ch2-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=9;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_015.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_015.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_015.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_015.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_015.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_015_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_06_001-Ch2-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=9;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_016.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_016.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_016.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_016.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_016.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_016_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_06_001-Ch2-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=9;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_008.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_008.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_008.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_008.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_008.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_008_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_008_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_07_001-Ch1-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=10;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_017.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_017.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_017.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_017.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_017.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_017_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_07_001-Ch1-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=10;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_018.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_018.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_018.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_018.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_018.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_018_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_07_001-Ch1-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=10;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_009.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_009.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_009.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_009.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_009.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_009_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_009_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k319_04_001-Ch2-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=11;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_019.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_019.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_019.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_019.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_019.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_019_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k319_04_001-Ch2-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=11;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_020.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_020.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_020.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_020.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_020.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_020_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k319_04_001-Ch2-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=11;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_010.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_010.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_010.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_010.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_010.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_010_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_010_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: ps10454_10_001-Ch2-neuron1_corrected.mat  
% Shuffled Design 
%  
%********************************************
rind=12;
cind=1;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_021.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_021.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_021.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_021.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_021.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_021_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: ps10454_10_001-Ch2-neuron1_corrected.mat  
% Info. Max. Design 
%  
%********************************************
rind=12;
cind=2;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090130/bsinfomax_setup_022.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_022.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_022.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_022.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_022.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_022_expllike.mat','isdir',0);

dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: ps10454_10_001-Ch2-neuron1_corrected.mat  
% Info. Max. Tan. Space. Rank=2 
%  
%********************************************
rind=12;
cind=3;
dsets(rind,cind).setupfile='/home/jlewi/cvs_ece/results/adaptive_sampling/results/bird_song/090227/bsinfomax_setup_011.m';
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_011.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_011.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_011.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_011.txt','isdir',0);
dsets(rind,cind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(rind,cind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(rind,cind).nwindexes=28;
dsets(rind,cind).exllfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_011_expllike_001.mat','isdir',0);
dsets(rind,cind).exlltanfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_011_expllike_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


