function [seqfiles,bfiles]= phdbsdata_other_neurons() 
%********************************



nind=0;
seqfiles=[];


%************************************************
% neuron: br2523_06_003-Ch1-neuron1_corrected.mat 
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_001.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_001.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_001.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_001.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_001.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];

%************************************************
% neuron: br2523_06_003-Ch1-neuron1_corrected.mat 
% infomax design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_002.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_002.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_002.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_002.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_002.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];

%************************************************
% neuron: br2523_06_003-Ch1-neuron1_corrected.mat  
% Info. Max. design with tangent space. Rank=2 
%  
%********************************************
sind=3;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_setup_002.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_data_002.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_mfile_002.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_cfile_002.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090227/bsinfomax_status_002.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];

%************************************************
% neuron: aa0708_04_001-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_005.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_005.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_005.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_005.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_005.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: aa0708_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_006.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_006.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_006.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_006.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_006.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: bb2728_02_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_007.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_007.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_007.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_007.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_007.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: bb2728_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_008.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_008.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_008.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_008.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_008.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k306_03_013-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_009.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_009.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_009.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_009.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_009.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k306_03_013-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_010.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_010.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_010.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_010.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_010.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_02_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_011.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_011.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_011.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_011.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_011.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_02_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_012.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_012.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_012.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_012.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_012.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_05_001-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_013.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_013.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_013.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_013.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_013.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_05_001-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_014.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_014.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_014.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_014.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_014.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_06_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_015.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_015.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_015.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_015.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_015.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_06_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_016.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_016.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_016.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_016.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_016.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_07_001-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_017.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_017.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_017.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_017.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_017.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_07_001-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_018.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_018.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_018.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_018.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_018.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k319_04_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_019.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_019.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_019.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_019.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_019.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k319_04_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_020.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_020.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_020.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_020.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_020.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: ps10454_10_001-Ch2-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_021.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_021.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_021.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_021.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_021.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: ps10454_10_001-Ch2-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_022.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_022.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_022.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_022.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_022.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_04_001-Ch1-neuron1_corrected.mat  
% shuffled design 
%  
%********************************************
nind=nind+1;
sind=1;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_023.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_023.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_023.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_023.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_023.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%************************************************
% neuron: k313_04_001-Ch1-neuron1_corrected.mat  
% Info. Max. design 
%  
%********************************************
sind=2;
seqfiles(nind,sind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_setup_024.m','isdir',0);
seqfiles(nind,sind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_data_024.mat','isdir',0);
seqfiles(nind,sind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_mfile_024.dat','isdir',0);
seqfiles(nind,sind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_cfile_024.dat','isdir',0);
seqfiles(nind,sind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsinfomax_status_024.txt','isdir',0);
seqfiles(nind,sind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
seqfiles(nind,sind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
seqfiles(nind,sind).nwindexes=28;
seqfiles(nind,sind).setupfile=[mfilename('fullpath') '.m'];


%**************************************************************************
%Batch Fit files
%**********************************************************************
bfiles=[];
dind=0;

dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_setup_002.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_data_002.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_mfile_002.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_cfile_002.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090130/bsglmfit_status_002.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];

dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_001.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_001.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_001.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_001.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_001.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_002.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_002.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_002.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_002.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_002.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_003.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_003.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_003.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_003.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_003.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_004.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_004.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_004.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_004.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_004.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_005.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_005.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_005.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_005.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_005.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_006.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_006.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_006.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_006.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_006.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_007.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_007.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_007.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_007.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_007.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_008.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_008.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_008.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_008.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_008.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_009.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_009.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_009.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_009.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_009.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


dind=dind+1;
bfiles(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_010.m','isdir',0);
bfiles(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_010.mat','isdir',0);
bfiles(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_010.dat','isdir',0);
bfiles(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_010.dat','isdir',0);
bfiles(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_010.txt','isdir',0);
bfiles(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
bfiles(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
bfiles(dind).fmapiterf=[];
bfiles(dind).nwindexes=28;
bfiles(dind).setupfile=[mfilename('fullpath') '.m'];


if (nargout>=3)
    for nind=1:size(seqfiles,1)
        v=load(getpath(seqfiles(nind,sind).datafile));
        simobj(nind,sind)=v.bssimobj;
    end
    varargout{1}=simobj;
end