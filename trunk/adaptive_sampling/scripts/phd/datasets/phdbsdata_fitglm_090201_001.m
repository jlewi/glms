function [dsets]= phdbsdata_fitglm_090201_001() 
%********************************


dsets=[];
dind=1;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_001.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_001.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_001.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_001.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_001.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=2;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_002.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_002.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_002.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_002.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_002.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=3;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_003.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_003.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_003.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_003.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_003.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=4;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_004.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_004.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_004.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_004.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_004.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=5;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_005.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_005.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_005.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_005.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_005.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=6;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_006.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_006.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_006.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_006.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_006.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=7;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_007.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_007.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_007.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_007.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_007.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=8;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_008.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_008.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_008.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_008.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_008.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=9;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_009.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_009.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_009.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_009.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_009.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=10;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_setup_010.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_data_010.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_mfile_010.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_cfile_010.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090201/bsglmfit_status_010.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=28;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


