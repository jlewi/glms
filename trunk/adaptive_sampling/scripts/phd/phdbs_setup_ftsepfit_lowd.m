%**************************************************************************
%
%Explanation: Setup the simulation for fitting a low d model using batch
%updates
function dsets=fbsetupftsepfitlowd()


dsets=struct;
dind=0;
    

scale=100;
allparam.prior.stimvar=10^-2*scale;
allparam.prior.shistvar=1*scale;
allparam.prior.biasvar=10*scale;

%try decreasing the bin size and increasing the number of time bins
allparam.obsrvwindowxfs=250;
allparam.stimnobsrvwind=10;
allparam.freqsubsample=2;
allparam.model='MBSFTSep';
allparam.windexes=[2 16];
allparam.rawdatafile=FilePath('RESULTSDIR','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat');

%use rank 1 eigen update because woodbury is proving unstable
%allparam.compeig=false;


%how many repeats of the data to use
allparam.stimparam.nrepeats=1;

%create a bdata an mobject to use to decide which frequency components to
%include
bdata=BSSetup.initbsdata(allparam);

strfdim=getstrfdim(bdata);
mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.alength=0;
mparam.hasbias=true;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mobj=MBSFTSep(mparam);


nfreq=10;
ntime=2;
subind=bindexinv(mobj,1:mobj.nstimcoeff);
btouse=subind(:,1)<=nfreq & subind(:,2)<=ntime;
btouse=find(btouse==1);

allparam.mparam.btouse=btouse;



dind=dind+1;
dnew=BSSetup.setupfitpoiss(allparam);
dsets=copystruct(dsets,dind,dnew);
