%Explanation:
%  Make Plots used to illustrate computing the mutual information 
%
gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation/mutual_info';
width=2;
height=2;
fontsize=20;

fh=FigObj('name','before','width',width, 'height',height,'fontsize',fontsize);


%make a plot of some polynomials
clear y;
x=[0:.25:4];

y=zeros(3,length(x));
%linear
y(1,:)=x;

%quadratic
y(2,:)=1/2* x.^2+1/2;

%cubic
y(3,:)=1/6*x+x/2+1/3;

plot(x,y(1,:),'-r','LineWidth',2);
plot(x,y(2,:),'-g','LineWidth',2);
plot(x,y(3,:),'-b','LineWidth',2);
xlabel('\vec{s}');
xlabel(fh.a,'$\vec{s}$');

set(fh.a.xlbl.h,'interpreter','latex');

lblgraph(fh);
ylabel('r');
xlim([0 3]);

set(fh.a.ha,'xticklabel',[]);
set(fh.a.ha,'yticklabel',[]);

lblgraph(fh);
sizesubplots(fh,[],[],[],[0 .07 0 0]);
saveas(fh.hf,fullfile(gdir,'polyd1tod3.png'));

%now make a plot of just 1st and 2nd degree polynomials
f2=FigObj('name','before','width',width, 'height',height,'fontsize',fontsize);

plot(x,y(1,:),'-r','LineWidth',2);
plot(x,y(2,:),'-g','LineWidth',2);
xlim([0 3]);

set(f2.a.ha,'xticklabel',[]);
set(f2.a.ha,'yticklabel',[]);
xlabel(f2.a,'$\vec{s}$');
ylabel('r');

set(f2.a.xlbl.h,'interpreter','latex');
lblgraph(f2);
sizesubplots(f2,[],[],[],[0 .07 0 0]);

saveas(f2.hf,fullfile(gdir,'polyd1tod2.png'));


%**************************************************************************
%Explanation: Plot a 1-d Gaussian posterior
fh=FigObj('fontsize',fontsize,'name','Gauss','width',width,'height',height,'xlabel','$\vec{\theta}$');
x=[-4:.05:4];
y=normpdf(x);

hp=plot(x,y,'LineWidth',4);

title(fh.a,'$p(\vec{\theta}|s_{1:t},r_{1:t})$');
set(fh.a.ha,'xticklabel',[]);
set(fh.a.ha,'yticklabel',[]);
set(fh.a.xlbl.h,'interpreter','latex');
set(fh.a.tlbl.h,'interpreter','latex');

lblgraph(fh);
sizesubplots(fh,[],[],[],[0 .12 0 .12]);
ylim([0 .65]);
saveas(fh.hf,fullfile(gdir,'gauss_prior.png'));


%**************************************************************************
%Explanation: Plot a 1-d Gaussian posterior 
fpost=FigObj('fontsize',fontsize,'name','Gauss','width',width,'height',height,'xlabel','$\vec{\theta}$');
x=[-4:.05:4];
y=normpdf(x,0,.5);

hp=plot(x,y,'LineWidth',4);

title(fpost.a,'$p(\vec{\theta}|s_{1:t},r_{1:t})$');
set(fpost.a.ha,'xticklabel',[]);
set(fpost.a.ha,'yticklabel',[]);
set(fpost.a.xlbl.h,'interpreter','latex');
set(fpost.a.tlbl.h,'interpreter','latex');

lblgraph(fpost);
sizesubplots(fpost,[],[],[],[0 .12 0 .12]);

saveas(fpost.hf,fullfile(gdir,'gauss_post.png'));


