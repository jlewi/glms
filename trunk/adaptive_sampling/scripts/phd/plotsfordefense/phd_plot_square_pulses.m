%Date 03-04-2009
%
%Explanation: Make plots of square pulse trains.

%pulse widths
pwidths=[10 15 25];
pspace=10;

height=1.5;
width=4;
fontsize=18;

lwidth=4;
msize=7;

for fi=1:2

    fh(fi)=FigObj('height',height,'width',width,'fontsize',fontsize);



    %each row of x specifies a different start and stop point for one of the
    %segements
    x=zeros(2,(length(pwidths)+1));



    if fi==1
        pw=pwidths;
    else
        pw=[pwidths(2) pwidths(3) pwidths(1)];
    end
    start=0;

    for pind=1:length(pw)
        cind=(pind-1)*2+1;
        x(1,cind)=start;
        x(2,cind)=start+pspace;
        y(1,cind)=0;
        y(2,cind)=0;

        cind=cind+1;
        x(1,cind)=start+pspace;
        x(2,cind)=start+pspace+pw(pind);

        y(1,cind)=1;
        y(2,cind)=1;

        start=start+pw(pind)+pspace;
    end

    %final silence
    cind=(length(pw))*2+1;
    x(1,cind)=start;
    x(2,cind)=start+pspace;
    y(1,cind)=0;
    y(2,cind)=0;
    
     
    
    plot(x(:),y(:),'-','LineWidth',lwidth,'MarkerSize',msize);

    %find points at which to add extra points
    pind=find(y(1,:)==1);
    for ind=pind
        xpts=x(1,ind):pspace/2:x(2,ind);
       plot(xpts,ones(1,length(xpts)),'o-','MarkerFaceColor','b','MarkerSize',msize); 
    end
    
   pind=find(y(1,:)==0);
    for ind=pind
        xpts=(x(1,ind)+x(2,ind))/2;
       plot(xpts,zeros(1,length(xpts)),'o-','MarkerFaceColor','b','MarkerSize',msize); 
    end
    
    xlabel('t');
    ylabel('x_t');

    set(fh(fi).a.ha,'xticklabel',[]);
    set(fh(fi).a.ha,'yticklabel',[]);

    
lblgraph(fh(fi));
end


savefigs=false;
if (savefigs)
    gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation';
    saveas(fh(1).hf,fullfile(gdir,'pulse1.png'),'png');
    saveas(fh(2).hf,fullfile(gdir,'pulse2.png'),'png');
end