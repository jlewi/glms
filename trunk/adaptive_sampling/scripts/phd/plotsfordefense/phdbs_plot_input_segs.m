%Date: 03-05-2009
%
%Explanation: For the bird song data. Slide a window over the spectrogram
%and generate the individual inputs
clear variables;


[seqfiles]=phdbsdata_all_neurons();
seqfiles=seqfiles(1,:);

%duration of the window
tk=20;
tstart=[1 10 20 30];



%which wave file to use
windex=1;

width=1;
height=1;

cl=[0 80];
for rind=1:size(seqfiles,1)
    %use the info. max. design
    v=load(getpath(seqfiles(rind,2).datafile));
    bssimobj(rind)=v.bssimobj;

    bdata=bssimobj(rind).stimobj.bdata;

  

     [ntrials,cstart]=getntrialsinwave(bdata,windex);
     [obj,spec,outfreqs]=getwavespec(bdata,windex);

    for tind=1:length(tstart)
        
        fh(tind)=FigObj('name','BS Input segs', 'width',width,'height',height);


        

        imagesc(1:tk+1,outfreqs,spec(:,tstart(tind):tstart(tind)+tk+1));
        
        
        set(fh(tind).a(1,1).ha,'position',[0 0 1 1]);
        
        set(fh(tind).a(1,1).ha,'clim',cl);

        set(fh(tind).a(1,1).ha,'xlim',[1 tk+1]);

        set(fh(tind).a(1,1).ha,'xtick',[]);
        set(fh(tind).a(1,1).ha,'ytick',[]);
        set(fh(tind).a(1,1).ha,'ylim',[outfreqs(1) outfreqs(end)]);
       
    end
end

    gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation';
    fext='png';
    ftype='png';
    
    savefigs=false;

if (savefigs)
    for tind=1:length(fh)
        saveas(fh(tind).hf,fullfile(gdir,sprintf('bs_wave_seg_%1g.%s',tind, fext)),ftype);
    end
end
