%
%Plot a simple threshold function for my defense
fh=FigObj('name','threshold','width',2.5,'height',1.25,'fontsize',18);

dx=5;
x=[-dx:-1 0 0 1:dx];
y=[zeros(1,dx+1) ones(1,dx+1)];
hp=plot(x,y,'LineWidth',4);
set(fh.a,'xticklabel',[]);
set(fh.a,'yticklabel',[]);
xlabel(fh.a,'s_t')
ylabel(fh.a,'r_t')

lblgraph(fh);
saveas(fh.hf,'~/svn_trunk/publications/adaptive_sampling/phdpresentation/threshold.png');