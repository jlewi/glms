%4-19-2007
%
% Create an image with three rows and several columns
% each row shows plots of the MAP estimate of a 2-d receptive field for a
%       different method
% We will have 3 rows corresponding to
%   1. infomax - under power constraint
%   2. infomax - using pool based stimuli
%   3. iid
%
% Last column displays the true receptive field



dsets=[];
dind=0;
bdir=fullfile('gabor','071219');
bfile='gabor2d';
dind=dind+1;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,1)));
dsets(dind).simvar='max';
dsets(dind).lbl=sprintf('vary lagrange');
%dsets(dind).niter=niter;

dind=dind+1;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,2)));
dsets(dind).simvar='max';
dsets(dind).lbl=sprintf('vary mu');
%dsets(dind).niter=niter;


%new data set


dsets=[];
nc06gabors40x40
%***************************************************
%parameters for the graph
%***************************************************
gparam.trials=[200 400 800 1000 4000];    %trials on which to plot the means
gparam.trials=[100:100:500 1000];
gparam.trials=[1000:1000:5000];
%gparam.trials=[200 300 600 800 1000 3000];    %trials on which to plot the means
fgabor=[];
fgabor.hf=figure;
fgabor.a={};
fgabor.name='Gabor MAPs';
fgabor.FontSize=12;         %force all fonts to this size
hold on;

%how columns and rows of axes we will need
gparam.ncols=length(gparam.trials)+1;
gparam.nrows=length(dsets);
gparam.fontsize=12;
gparam.clim=[-1 1];       %limits for color mapping
colormap(gray);

data=struct('m',cell(1,length(gparam.trials)),'lbl',[],'theta',[]);
data=repmat(data,1,length(dsets));

%**************************************************************************
%Make plots of mean
%************************************************************************
%loop over the data sets
clear simobj;

%run this loop in parallel using parfor
%matlabpool('open','conf',2);

for dind = 1:length(dsets)
    [simobj]=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);
    extra=getextra(simobj);
    mparam=extra.mparam;
    
    %allocate space to save the results
    for tind=1:length(gparam.trials)
        trial=gparam.trials(tind);
      
        %check mean exists
        if (trial>getniter(simobj))
            fprintf('Data set %s: trial %d exceeds number of iterations \n;',getlabel(simobj),trial);
            break;   %stop plotting for this dataset
        else
            data(dind).m{tind}=reshape(getpostm(simobj,trial),mparam.gheight,mparam.gwidth);
        end
        %turn off tick marks
    end %end loop over trials

    %the true parameter
    data(dind).theta=reshape(gettheta(getobserver(simobj)),mparam.gheight,mparam.gwidth);
end


%************************************************************************
%Plot the results
%************************************************************************
%%
height=mparam.gheight;
width=mparam.gwidth;
fgabor=FigObj('name','Gabor means','width',6.25,'height',5);
fgabor.a=AxesObj('nrows',length(dsets),'ncols',length(gparam.trials)+1);

%dorder is the order in which we want to plot the graphs
dorder=[dnames.max dnames.maxheur dnames.maxpool dnames.rand dnames.maxmean dnames.maxevec dnames.grad];
row=0;
colormap(gray);
for dind=dorder
    row=row+1;
   for tind=1:length(gparam.trials)
      if ~isempty(data(dind).m{tind})
         setfocus(fgabor.a,row,tind);
         imagesc(data(dind).m{tind},gparam.clim);
         set(gca,'xlim',[1 width],'ylim',[1 height]);
        set(gca,'xtick',[]);
        set(gca,'ytick',[]);
      end
   end    
   
   %plot the true parameter   
   setfocus(fgabor.a,row,length(gparam.trials)+1);
   imagesc(data(dind).theta,gparam.clim);
   set(gca,'xlim',[1 width],'ylim',[1 height]);
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
    
    %add a ylabel to each row

    fgabor.a(row,1)=ylabel(fgabor.a(row,1),dsets(dind).lbl);
    hylbl=getylabel(fgabor.a(row,1));
    set(hylbl.h,'interpreter','latex');
end

%add colorbar to the last plot
%plot the true parameter
setfocus(fgabor.a,length(dsets),length(gparam.trials)+1);
hc=colorbar;
fgabor.a(length(dsets),length(gparam.trials)+1)=sethc(fgabor.a(length(dsets),length(gparam.trials)+1),hc);
  

%add a title to each graph in the top row
for col=1:length(gparam.trials)
    fgabor.a(1,col)=title(fgabor.a(1,col),sprintf('trial %d',gparam.trials(col)));
end
fgabor.a(1,length(gparam.trials)+1)=title(fgabor.a(1,length(gparam.trials)+1),'\theta true');



%label the graphs
fgabor=lblgraph(fgabor);
fgabor=sizesubplots(fgabor,[],[],[],[.05 0 0 0]);



%previewfig(fgabor.hf,'bounds','tight','FontMode','Fixed','FontSize',fgabor.FontSize,'Width',gparam.width,'Height',gparam.height)
fgoutfile='~/svn_trunk/publications/adaptive_sampling/writeup/NC06/images_eps/gabormaps.eps';
%saveas(gethf(fgabor),fgoutfile,'epsc2');

odata={'data sets',[{'label'},{'variable'},{'file'};{dsets.lbl}' {dsets.simvar}' {dsets.fname}'];fgabor,'stimulus coefficients';};
onenotetable(odata,seqfname('~/svn_trunk/notes/gabors.xml'));