%02-10-2009
%
%Make a plot of some of the wavefiles and a raster plot of the responses
%to that wavefile. Use 1 wave file which is bird-song and 1 which is a
%noise file.
%
%
clear variables;


[seqfiles]=phdbsdata_all_neurons();
seqfiles=seqfiles(1,:);



%%

%are the figures for the paper or the poster
forpaper=false;

if (forpaper)
    width=6.1;
    height=4;

    gdir='~/svn_trunk/publications/adaptive_sampling/phdthesis/figs';

    fext='eps';
    ftype='epsc2';
else
    width=3.5;
    height=2.5;
    gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation';
    fext='png';
    ftype='png';
end



for rind=1:size(seqfiles,1)
    %use the info. max. design
    v=load(getpath(seqfiles(rind,2).datafile));
    bssimobj(rind)=v.bssimobj;

    bdata=bssimobj(rind).stimobj.bdata;

    %determine the test set
    windexes=ones(1,max(bssimobj(rind).stimobj.bspost.windexes));
    windexes(bssimobj(rind).stimobj.windexes)=false;
    wtest=find(windexes==1);

    for ind=1:length(wtest)
        waveind=wtest(ind);
        fh(rind,ind)=FigObj('name','BS Input', 'width',width,'height',height);


        %plot the wave file

        [ntrials,cstart]=getntrialsinwave(bdata,waveind);
        [obj,spec,outfreqs,t]=getfullspec(bdata,waveind);

        %truncate spec and t by cstart
        spec=spec(:,cstart:end);
        t=t(1:ntrials);

        %convert to Khz
        outfreqs=outfreqs/1000;
        setfocus(fh(rind,ind).a,1,1);
        imagesc(t,outfreqs,spec);
        ylabel(fh(rind,ind).a(1,1),'Frequency (Khz)');

        if waveissong(bdata,waveind);
            title(fh(rind,ind).a(1,1),'Bird song');
        else
            title(fh(rind,ind).a(1,1),'ML noise');
        end

        hc=colorbar;
        sethc(fh(rind,ind).a(1,1),hc);

        set(fh(rind,ind).a(1,1).ha,'xlim',[t(1) t(end)]);

        set(fh(rind,ind).a(1,1).ha,'ylim',[outfreqs(1) outfreqs(end)]);

        xlabel(fh(rind,ind).a,'Time(s)');


        lblgraph(fh(rind,ind));

    end
end

savefigs=false;

if (savefigs)
    saveas(fh(1,1).hf,fullfile(gdir,['bs_wave_birdsong_spec.' fext]),ftype);
    saveas(fh(1,2).hf,fullfile(gdir,['bs_wave_mlnoise_spec.',fext]),ftype);
end
