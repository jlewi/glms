%03-08-2009
%
%Make a plot of the expected firing and variance.
%Use this to explain why information is 2-d

fh=FigObj('name','expr','width',3,'height',3,'fontsize',16);
lwidth=3;

x=[-2:.5:5];

exr=exp(x);

plot(x,exr,'-','linewidth',lwidth);

plot(x,exr+exr.^.5,'g--','linewidth',lwidth);
plot(x,exr-exr.^.5,'g--','linewidth',lwidth);

xlabel(fh.a,'$\rho_t=\vec{s}_t^T\vec{\theta}$');
ylabel(fh.a,'r_t');

xlim([-1 3])
set(fh.a.xlbl.h,'interpreter','latex')
set(gca,'xticklabel',[]);
set(gca,'yticklabel',[]);

savefigs=false;

lblgraph(fh)
sizesubplots(fh,[],[],[],[0 .1 .05 .05])
if (savefigs)
   saveas(fh.hf,'~/svn_trunk/publications/adaptive_sampling/phdpresentation/expr.png');
    
end