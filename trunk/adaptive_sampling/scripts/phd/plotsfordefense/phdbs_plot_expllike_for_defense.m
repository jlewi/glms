%******************************************************************
%Date: 02-18-2009
%
%Explanation: Make figures for a schematic of how we compute speedup
%
%
% We make 4 figures
%   1. A plot of the Expected log likelihood vs. trial
%   2. A plot of the % Converged vs. trial
%   3. A plot of the trial vs. % converged
%   4. speedup vs. % Converged
clear variables;
setpathvars

%[seqfiles]=phdbsdata_all_neurons();
[seqfiles]=phdbs_data_infomax_tspace_001(); 

%use the info. max and shuffled designs. Ignore the tangent space design.
seqfiles=seqfiles(2,1:2);

forpaper=false;
if (forpaper)
    width=6.1;
    height=4;

    gdir='~/svn_trunk/publications/adaptive_sampling/phdthesis/figs';

    fext='eps';
    ftype='epsc2';
else
    width=1.5;
    height=1.5;
    gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation/bsfigs';
    fext='png';
    ftype='png';
end


%create one file per dataset
for sind=1:size(seqfiles,1)
    for dind=1:2
        fprintf('Design %d of %d \n',(sind-1)*2+dind, numel(seqfiles));
        
        bsllike(sind,dind)=BSExpLLike('exllfile',[seqfiles(sind,dind).exllfile]);


    %compute the expected mse 
     % compellike(bsllike(sind,dind));
    end
end

pind=0;



%%
%************************************************************
%Parameters for the graphs
%*************************************************************

width=4.5;
height=2.5;

%font size
fsize=10;
%which wave file in the test set to use
tind =1;

lbls={'Shuffled','Info. Max.'};

%define the plot styles 
%for the shuffled and info. max designs
pstyles=PlotStyles();
ps(1)=pstyles.plotstyle(1);
ps(2)=pstyles.plotstyle(2);
ps(2).color=[0 1 0];
ps(2).markerfacecolor=[0 1 0];

%***********************************************************************
%Make figure of Expected log-likelihood
%************************************************************************
%%


    [fllike]=plot(bsllike);




for rind=1:size(fllike,1)
    for cind=1:size(fllike,2)
        %change the labels
        fllike(rind,cind).a.hlgnd.lbls={'Shuffled','Info. Max.'};
        
        fllike(rind,cind).width=width;
        fllike(rind,cind).height=height;
        fllike(rind,cind).fontsize=fsize;
        setfsize(fllike(rind,cind));
        plotlegend(fllike(rind,cind).a.hlgnd);
        autosize(fllike(rind,cind).a.hlgnd);
        setfontsize(fllike(rind,cind).a.hlgnd,9);
        setvoffset(fllike(rind,cind).a.hlgnd,-.1);

        setposition(fllike(rind,cind).a.hlgnd,.59,.32,.39,.18);
        
        if (cind==1)
           title(fllike(rind,cind).a,'Bird song'); 
           ttl='birdsong';
        else
           title(fllike(rind,cind).a,'ML-noise');
           ttl='mlnoise';
        end

        %fname=sprintf('~/svn_trunk/publications/adaptive_sampling/phdthesis/figs/bs_expllike_n%02g_%s.eps',rind,ttl);
        %saveas(fllike(rind,cind).hf,fname,'epsc2');
        
        fllike(rind,cind).fontsize=16;
    end
end

xlabel(fllike(tind).a,'Trial');
lblgraph(fllike(tind));

pind=pind+1;
figs(pind)=fllike(tind);

%**************************************************************************
%save figures
%**************************************************************************
%*
savefigs=false;

if (savefigs)
   fname=['bs_expllike_birdsong.' fext];
   saveas(fllike(1).hf,fullfile(gdir,fname),ftype); 

   fname=['bs_expllike_noise.' fext];
   saveas(fllike(2).hf,fullfile(gdir,fname),ftype); 

end
