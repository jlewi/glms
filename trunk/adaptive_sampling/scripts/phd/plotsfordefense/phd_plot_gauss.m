%
%Date 03-12-2009
%Explanation: Plot a 1-d Gaussian posterior

width=2;
height=2;
fh=FigObj('fontsize',18,'name','Gauss','width',width,'height',height,'xlabel','$\vec{\theta}$');

x=[-4:.05:4];
y=normpdf(x);

hp=plot(x,y,'LineWidth',4);

title(fh.a,'$p(\vec{\theta}|s_{1:t},r_{1:t})$');
set(fh.a.ha,'xticklabel',[]);
set(fh.a.ha,'yticklabel',[]);
set(fh.a.xlbl.h,'interpreter','latex');
set(fh.a.tlbl.h,'interpreter','latex');

lblgraph(fh);
sizesubplots(fh,[],[],[],[0 .1 0 .12]);

saveas(fh.hf,'~/svn_trunk/publications/adaptive_sampling/phdpresentation/gauss.png');


%make a plot of several Gaussian Distributions to represent the updated
%posteriors after the experiment
%%
fh=FigObj('fontsize',18,'name','Post','width',width,'height',height,'xlabel','$\vec{\theta}$');

voffset=.25;
hoffset=1;
mu=[.75:-.75:-.75];
mu=zeros(1,3);
%sigma=[.25 .25 .25];
sigma=[.5 .75 1];

x=[-4:.05:4];
ls={'-r','-g','-b'};
for ind=1:length(mu)
y=normpdf(x,mu(ind),sigma(ind));

hp=plot(x+hoffset*(ind-1),y+voffset*(ind-1),'-b','LineWidth',4);

%plot the axes
plot([x(1) x(end)]+hoffset*(ind-1),voffset*(ind-1)*[1 1],'-k','Linewidth',2);
plot([x(1) x(1)]+hoffset*(ind-1),[0 10]+voffset*(ind-1),'-k','Linewidth',2);

end

title(fh.a,'$p(\vec{\theta}|s_{1:t+b},r_{1:t+b})$');
set(fh.a.ha,'xticklabel',[]);
set(fh.a.ha,'yticklabel',[]);
set(fh.a.xlbl.h,'interpreter','latex');
set(fh.a.tlbl.h,'interpreter','latex');

lblgraph(fh);
sizesubplots(fh,[],[],[],[0 .1 0 .12]);

ylim([0 1]);
xlim([x(1) x(end)+hoffset]);
saveas(fh.hf,'~/svn_trunk/publications/adaptive_sampling/phdpresentation/gauss_many.png');
