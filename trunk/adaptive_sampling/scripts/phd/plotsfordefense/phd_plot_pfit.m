%Explanation:
%  Make a plot used to illustrate the problem of fitting the curves.

fh=FigObj('name','curve fit','width',2.5, 'height',2.5,'fontsize',24);


%make a plot of some curves 
x=[0:.25:4];
y=1/2* x.^2+1/2;

y=[y; x];

plot(x,y(1,:),'--k','LineWidth',2);
plot(x,y(2,:),'--k','LineWidth',2);

xlim([0 2]);

set(fh.a.ha,'xticklabel',[]);
set(fh.a.ha,'yticklabel',[]);

saveas(fh.hf,'~/svn_trunk/publications/adaptive_sampling/phdpresentation/pfit.png');