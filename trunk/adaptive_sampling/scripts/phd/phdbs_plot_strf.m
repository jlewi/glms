%date: 10-29-2008
%
%Explanation: For the bird song FFT applied separatly, plot the strf on
%several trials
setpathvars;


%[seqfiles]=phdbsdata_all_neurons();
[seqfiles]=phdbs_data_infomax_tspace_001(); 

%use the info. max and shuffled designs. Ignore the tangent space design.
dsets=seqfiles(2,1:2);

%         %Flip the info. max and shuffled designs
%hard code the labels; make sure they are correct
lbls={'Info. Max.'; 'Shuffled'};
dsets=[dsets(2) dsets(1)];

% switch neuron
%     case 'br2523'
%          [dsets]=phdbsdata_br2523();

        %trials=[5000:5000:35000];        
        %trials=[1000 2000:2000:10000];
        trials=[100 2500 5000 7500 10^4 2*10^4 3*10^4 3.5*10^4];
       



        clim=[-4*10^-3 4*10^-3];
        
%     case 'aa0708'
%         [dsets]=phddata_bsinfomax_090130_001;
%         dsets=dsets(1:2);
%         
%         trials=[1000 2000:2000:10000];
%         trials=[10000:2000:20000];
%     otherwise
%         error('unrecognized neuron');
% 
% end

%extension for graphics
fext='.png';
%make plots for poster or paper
paper=false;
fsize=10;

if (paper)
    fsize=10;
    width=9;
    heigh=5;
else
    fsize=14;
    width=9.4;
    height=5;
    gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation/bsfigs/';
end
nrows=length(dsets);

fh=FigObj('name','STRFs','width',width,'height',height,'naxes',[nrows length(trials)],'fontsize',fsize);

%**************************************************************************
%loop over the datesets and plot the strfs
%*********************************************************
for dind=1:length(dsets)
    v=load(getpath(dsets(dind).datafile));
    bssimobj(dind)=v.bssimobj;

    for tind=1:length(trials)
        trial= trials(tind);


        [t,freqs]=getstrftimefreq(v.bssimobj.stimobj.bdata);
        if (trial<=v.bssimobj.niter)
            setfocus(fh.a,dind,tind);
            strf=getstrf(v.bssimobj.mobj,getm(v.bssimobj.allpost,trial));

            %multiply t by 1000 so its in ms
            t=t*1000;

            %divide freqs by 1000 so its in khz
            freqs=freqs/1000;
            imagesc(t,freqs,strf);
        end



    end
end
%add a colorbar to the final image in the first row
dind=1;
tind=length(trials);

setfocus(fh.a,dind,tind);
%add a colorbar
fh.a(dind,tind).hc=colorbar;



%%
%turn off all tickmars
set(fh.a,'xtick',[]);
set(fh.a,'ytick',[]);

%set x and y limits
set(fh.a,'xlim',[t(1) t(end)]);
set(fh.a,'ylim',[freqs(1) freqs(end)]);

%turn on xtick,ytick for appropriate graphs
set([fh.a(length(dsets),:)],'xtickmode','auto');
set([fh.a(:,1)],'ytickmode','auto');
set([fh.a(length(dsets),:)],'xticklabel',[]);
%********************************************************
%adjust labels

%make the color limits the same for all axes
% clim=get(fh.a,'clim');
% clim=cell2mat(clim);
% clim=[min(clim(:,1)) max(clim(:,2))];

%clim=[-5*10^-3 10^-2];
set(fh.a,'clim',clim);


%add ylabels
for dind=1:length(dsets)
  
    ylbl=lbls{dind};

    if (dind==length(dsets))
        ylbl=sprintf('%s\nFrequency (KHz)',ylbl);
    else
        %don't add ticklables
        set(fh.a(dind,1),'YTickLabel',[]);
    end

    ylabel(fh.a(dind,1),ylbl);

end

%manually align the ylabels
% pos=get(fh.a(length(dsets),1).ylbl.h,'position');
% leftpos=-79;
% for rind=1:length(dsets)-1
%     lpos= get(fh.a(rind,1).ylbl.h,'position');
%     lpos(1)=leftpos;
%     set(fh.a(rind,1).ylbl.h,'position',lpos);
% end

%add titles
for tind=1:length(trials)
    if (trials(tind)>=10000)
        tl=sprintf('Trial %02gk',trials(tind)/1000);
    else
        tl=sprintf('Trial %03g',trials(tind));
    end
    title(fh.a(1,tind),tl);
end

%add xlabels
for tind=1:1
    %add xlabels
    set(fh.a(nrows,tind),'xtick',[-40 -20 0]);
    set(fh.a(nrows,tind),'xticklabel',[-40 -20 0]);
    xlabel(fh.a(nrows,tind),sprintf('Time(ms)'));
end


lblgraph(fh);
space.cbwidth=.15;
sizesubplots(fh,space);

odinfo=cell(length(dsets),1);

for dind=1:length(dsets)
    odinfo{dind,1}=bssimobj(dind).label;
    odinfo{dind,2}=getrpath(dsets(dind).datafile);
end

otbl={fh,[{'script',mname};odinfo]};

onenotetable(otbl,seqfname('~/svn_trunk/notes/bsstrf.xml'));

savefigs=false;
if (savefigs)
     saveas(fh.hf,fullfile(gdir,'bs_strf.png'));
end

% bname='bsstrf';
%
% fname1=fullfile(gdir,[bname fext]);
% saveas(fh(1).hf,fname1,gtype);
