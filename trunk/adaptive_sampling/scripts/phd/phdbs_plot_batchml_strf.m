%02-12-2009
%Make a plot of the BatchML estimates of the strf and spike history
%to include in my phd thesis
%
%Here we make plots of the STRF when we use the fourier repersentation to
%smooth the STRF in time.
setpathvars;


neuron='thesis';

mname=mfilename();
%set the parameters appropriately
switch neuron
    case 'br2523'
        [seqfiles bfile]=phdbsdata_br2523();


    case 'all'
        [seqfiles bfile]=phdbsdata_all_neurons();

    case 'thesis'
        [seqfiles bfile]=phdbsdata_all_neurons();
        bfile=bfile(1:2);
    otherwise
        error('unrecognized neuron');

end


forpaper=true;

if (forpaper)
    width=3;
    height=3;
    
    gdir='~/svn_trunk/publications/adaptive_sampling/phdthesis/figs';
    
    fext='eps';
    ftype='epsc2';
    fontsize=12;
else
   width=1.5;
   height=1.5;
   gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation/bsfigs';
   fext='png';
   ftype='png';
   fontsize=10;
end


%**************************************************************************
%Make the plots
%*************************************************************************
%%
%load the plots
for sind=1:length(bfile)
    v=load(getpath(bfile(sind).datafile));

    bssim(sind)=v.bssimobj;
end


%%
for sind=1:length(bfile)


    %**************************************************************************
    %plot the strf
    %**************************************************************************
    fh(sind,1)=FigObj('name','Fourier Coefficents','width',width,'height',height,'fontsize',fontsize);

    mobj=bssim(sind).mobj;
    [t,freqs]=getstrftimefreq(bssim(sind).bdata);

    %convert to ms and hz
    t=t*1000;
    freqs=freqs/1000;
    title(fh(sind,1).a,'STRF');


    [stimcoeff,shistcoeff,bias]=parsetheta(mobj,bssim(sind).results(end).theta);

    row=sind;
    col=1;
    imagesc(t,freqs,tospecdom(mobj,stimcoeff));
    xlabel(fh(row,col).a,'Time(ms)');
    ylabel(fh(row,col).a,'Frequency(kHz)');
    hc=colorbar;
    xlim([t(1) t(end)]);
    ylim([freqs(1) freqs(end)]);
    sethc(fh(sind,1).a,hc);

    yt=get(hc,'ytick');
    set(hc,'ytick',yt(1:2:end));
    
    lblgraph(fh(sind,1));

    sizesubplots(fh(sind,1));
    %************************************************************************
    %Plot the spike history
    %***********************************************************************
    fh(sind,2)=FigObj('name','Fourier Coefficents','width',width,'height',height);


    row=sind;
    col=2;
    
    t=-1*[getshistlen(mobj):-1:1];
    hp=plot(t,shistcoeff);
    pstyle.marker='o';
    pstyle.markerfacecolor='b';
    pstyle.linewidth=3;
    addplot(fh(row,col).a,'hp',hp,'pstyle',pstyle);
    xlabel(fh(row,col).a,'Time (ms)');
      ylabel(fh(row,col).a,'Value');
    title(fh(row,col).a,'Spike History Filter');
    xlim([t(1) t(end)]);


    lblgraph(fh(sind,2));

end


%************************************************************************
%Compuct the expected log-likelihood
%************************************************************************
ntest=2;
for bind=1:length(bfile)
   bll(bind)=BSLogLikeBatchFit('bssim',bssim(bind));
   
   for wind=1:ntest
      ll(bind,wind)=getllike(bll(bind),length(bssim(bind).results),wind); 
   end
end
%***********************************************************************
%Save the plots
%***********************************************************************
savedata=false;

if (savedata)
for sind=1:size(fh,1)
    
    fstrf=sprintf('bs_batch_strf_n%02g',sind);
    fshist=sprintf('bs_batch_shist_n%02g',sind);
    saveas(fh(sind,1).hf,fullfile(gdir,[fstrf '.' fext]),ftype);
    saveas(fh(sind,2).hf,fullfile(gdir,[fshist '.' fext]),ftype);    
end

end