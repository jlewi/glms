%script to make the plot of the fisher information which I use in my
%shcematic diagram
post.m=[1;0];
post.c=[2 0; 0 1];
[finfo]=fonucirc(post,[],1);

finfo=[finfo(40:end) finfo(1:39)];
figure;
h=plot(finfo,'k');

set(gca,'xtick',[]);
set(gca,'ytick',[]);
set(h,'Linewidth',10);
set(gca,'Box','off');
set(gca,'Visible','off');
axis tight;