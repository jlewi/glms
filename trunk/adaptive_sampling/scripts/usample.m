% 8-30-2005
%
% This script performs uniform random sampling of the 1-d model
% so that we can compare to adaptive sampling.
clear all;

%*************************************************************************
%Options
%************************************************************************
%number of samples to generate = number of trials to run 
%1 sample is collected on each trial
numtrials=100;

%*************************************************************************
%set up figures
%*************************************************************************
h_f=figure;                     %figure to plot mutual information after each trial
num_figs=3;
h_ptheta=subplot(num_figs,1,1);        %h_ptheta
h_x=subplot(num_figs,1,2);          
h_y=subplot(num_figs,1,3);


%*************************************************************************
%Probability Distributions
%*************************************************************************
%probability of p(y=1|x,theta) is a gaussian
sigma=.1;
truetheta=.25;           %true theta.
binsize=.01;            %sampling interval
maxp=.75;
pyxt=@(y,x,theta)llhood(y,x,theta,sigma,binsize,maxp);

%limits on theta
tmin=0;
tmax=1;
tinterval=binsize;      %resolution for sampling the interval
theta=tmin:tinterval:tmax;
thetalength=length(theta);

%**************************************************************************
%Initialization
%**************************************************************************
%data to store the data from each trial
%x - the point at which we sample
%y - the observation generated for that location
%theta - the best guess of theta after collecting this sample
%        this is used to look for convergence
data=struct('y',0,'x',0,'theta',0);

%store the product p(y1|x1,theta)*....p(yn-1|xn-1,theta)
%b\c we need it to recompute the distribution ptheta after each trial.
likelihood=ones(1,thetalength);

%we will calculate ptheta 
ptheta=zeros(numtrials,thetalength);
ptheta(1,:)=ones(1,thetalength)*1/thetalength;


%**************************************************************************
% Main Loop = Repeated Sampling
%**************************************************************************
%variable signifying whether or not to keep iterating
trial=1;    %we will start with trial 2

%trial 1 represents the prior case used to store prior
for trial=2:numtrials

    fprintf('Trial %d \n',trial);
    
    %sample the data using xopt
    data(trial).x=rand(1)*(tmax-tmin)+tmin;
    rndnum=rand(1);
    if (rndnum <= pyxt(1,data(trial).x,truetheta));
        data(trial).y=1;
    else
        data(trial).y=0;
    end
    
    %recomptue the posterior distribution of the model
    likelihood=likelihood.*pyxt(data(trial).y,data(trial).x,theta);
    ptheta(trial,:)=likelihood;
    
    %make sure all entries are at least eps
    %to avoid numerical errors when we compute the log
    ptheta(trial,:)=ptheta(trial,:)/sum(ptheta(trial,:))+eps;   %normalize it

    
   figure(h_f);
   axes(h_ptheta)
   plot(theta,ptheta(trial,:));
   xlabel('theta');
   ylabel('ptheta');
   %fprintf('I(y;theta|x)=%d \n', icond');
   
   axes(h_x);
   plot(1:trial,[data.x]);
   xlabel('Trial');
   ylabel('x');
   
   axes(h_y);
   plot(1:trial,[data.y]);
   xlabel('trial');
   ylabel('y');
   
   
   %test for convergence
   %get the theta which maximizes ptheta
   %consider this our best guess
   %[lmin,lmax]=lminmax(ptheta(trial,:));
   [pthetabest,indbest]=max(ptheta(trial,:));
   data(trial).theta=theta(indbest);
   
   
   
   
   %keyboard
   pause(.05);
   
   
end