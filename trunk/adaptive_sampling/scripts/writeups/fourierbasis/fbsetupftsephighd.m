%**************************************************************************
%
%Explanation: Setup the info. max. simulations when we represent theta in
%the fourier space and we apply the fourier transform to time and frequency
%separatly
%Theta is represented in the fourier domain
function dsets=fbsetupftsephighd()


dsets=struct;
dind=0;

allparam.pcutoff=[];

%try decreasing the bin size and increasing the number of time bins
allparam.obsrvwindowxfs=124;
allparam.stimnobsrvwind=20;
allparam.freqsubsample=1;
allparam.model='MBSFTSep';


%for the updater use BSNewtonLinCon
allparam.updater.type='BSNewtonLinCon';
allparam.updater.rmax=100;
allparam.updater.windexes=[2 16];

%use rank 1 eigen update because woodbury is proving unstable
allparam.compeig=true;

%create a bdata an mobject to use to decide which frequency components to
%include
bdata=BSSetup.initbsdata(allparam);

strfdim=getstrfdim(bdata);
mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.alength=0;
mparam.hasbias=0;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mobj=MBSFTSep(mparam);


nfreq=20;
ntime=4;
subind=bindexinv(mobj,1:mobj.nstimcoeff);
btouse=subind(:,1)<=nfreq & subind(:,2)<=ntime;
btouse=find(btouse==1);

allparam.mparam.btouse=btouse;

%************************************************************
%Shuffled
%************************************************************
shparam=allparam;

shparam.stimtype='BSBatchShuffle';

dind=dind+1;
dnew=BSSetup.setupinfomax(shparam);
dsets=copystruct(dsets,dind,dnew);
% %************************************************************
% %Info. Max.
% %************************************************************
 imparam=allparam;
 imparam.stimtype='BSBatchPoissLB';
 BSSetup.setupinfomax(imparam);


%************************************************************
%Info. Max. Tan
%************************************************************
dind=dind+1;
imtan=allparam;
imtan.stimtype='BSBatchPoissLBTan';
imtan.model='MBSFTSepLowRank';
imtan.tanparam.rank=2;
imtan.tanparam.startfullmax=25;
dnew=BSSetup.setupinfomax(imtan);
dsets=copystruct(dsets,dind,dnew);


