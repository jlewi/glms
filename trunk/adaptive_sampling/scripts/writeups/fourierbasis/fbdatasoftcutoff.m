%These datasets all used infomax. But we varied the variance for the high
%frequencies. The cutoff frequency was the same for all models
%i.e for nf>4 and nt>3 we set the variance of our prior to a different
%value
function [dfiles,pstyles]=fbdatasoftcutoff()

dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'colors','lstyles','markers'};
pstyles.linewidth=pstyles.linewidth-.25;

smooth.nfcutoff=10;
smooth.ntcutoff=3;




%*************************************************************************
%info. max.: c=10^0
%************************************************************************
bdir='081018';
bfile='bsinfomax_setup_001';
cvar=10^0;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: %d',cvar);
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('or nf>4 or nt>2 we set the variance of our prior to %d',cvar)}];
dfiles(dind).explain=explain;


%*************************************************************************
%info. max.: c=10^-1
%************************************************************************
bdir='081018';
bfile='bsinfomax_setup_002';
cvar=10^-1;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: %d',cvar);
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('or nf>4 or nt>2 we set the variance of our prior to %d',cvar)}];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max.: c=10^-3
%************************************************************************
bdir='081018';
bfile='bsinfomax_setup_003';
cvar=10^-3;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: %d',cvar);
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('or nf>4 or nt>2 we set the variance of our prior to %d',cvar)}];
dfiles(dind).explain=explain;

%*************************************************************************
%info. max.: c=10^-6
%************************************************************************
bdir='081018';
bfile='bsinfomax_setup_004';
cvar=10^-6;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: %d',cvar);
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('or nf>4 or nt>2 we set the variance of our prior to %d',cvar)}];
dfiles(dind).explain=explain;


%*************************************************************************
%info. max.: c=10^-9
%************************************************************************
bdir='081019';
bfile='bsinfomax_setup_001';
cvar=10^-9;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Shuffled: %d',cvar);
dfiles(dind).usetanpost=false;
explain={'Method:', 'shuffled '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('or nf>4 or nt>2 we set the variance of our prior to %d',cvar)}];
dfiles(dind).explain=explain;


%*************************************************************************
%shuffled: c=proportional to stats
%************************************************************************
bdir='081019';
bfile='bsinfomax_setup_002';

dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Shuffled: stim. stats');
dfiles(dind).usetanpost=false;
explain={'Method:', 'shuffled '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('we set the variance of our prior based on the stimulus statistics')}];
dfiles(dind).explain=explain;

%*************************************************************************
%shuffled: c=proportional to stats
%************************************************************************
bdir='081019';
bfile='bsinfomax_setup_006';

dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl=sprintf('Info. Max.: stim. stats');
dfiles(dind).usetanpost=false;
explain={'Method:', 'Info. Max. '; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Prior:', sprintf('we set the variance of our prior based on the stimulus statistics')}];
dfiles(dind).explain=explain;

