%Datasets to compare the effect of online smoothing 
%to smoothing after the experiment is over. 
%
%This data is nfcutoff=10
function [dfiles,pstyles]=fbdatatan()

dfiles=[];
dind=0;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'colors','lstyles','markers'};
pstyles.linewidth=pstyles.linewidth;

smooth.nfcutoff=10;
smooth.ntcutoff=3;




%*************************************************************************
%info. max.: Fourier Representation. Tangent space, Rank 2
%  startfull=25
%
%nscale=1/2
%************************************************************************
bdir='081017';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl='Info. Max. Tan:full post';
dfiles(dind).usetanpost=false;
explain={'Method:', 'Infomax Tan. Space'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Use tanpost:', dfiles(dind).usetanpost}];



%*************************************************************************
%info. max.: Fourier Representation. Tangent space, Rank 2
%  startfull=25
%
%nscale=1/2
%************************************************************************
bdir='081017';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).lbl='Info. Max. Tan:tan post';
dfiles(dind).usetanpost=true;
explain={'Method:', 'Infomax. Tan Space'; 'Objective function', sprintf('Lower bound for mutual info of the batch.\nCompute the expectation of log(1+x) using numerical integration.')};
explain=[explain;{'Use tanpost:', dfiles(dind).usetanpost}];
dfiles(dind).explain=explain;

%**************************************************************************
%Info. Max. only use 1/2 the frequencies as our basis
%**************************************************************************
bdir='081007';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='STRF represented as fourier series. Use 1/2  the maximum # of frequencies. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max. Full:';
dfiles(dind).usetanpost=false;


%********************************************************
%shuffled: apply the smoothing online
%*********************************************************
bdir='081010';
bfile='bsinfomax_setup_003';
nscale=1/2;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
explain='Batch shuffled design';
explain=sprintf('%sUse frequency domain.\n',explain);
explain=sprintf('%snscale=%g',explain,nscale);
explain=sprintf('%snscale depetermines what frequencies to use. We use frequencies (0:nscale*nmax)(f_fundamental)  where nmax is the largest value possible without aliasing.',explain,nscale);
dfiles(dind).explain=explain;
dfiles(dind).lbl=sprintf('shuffled:');
dfiles(dind).usetanpost=false;

