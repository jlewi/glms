%10-19-2008
%
%Make plots illustrating that we can control the effct of the bias
%by setting our prior covariance to zero

clear all;
setpathvars;

%WARNING: index into dsets are likely to change you want the following
% Shuffled - use full posterior without smoothing
% Info. max using full posterior without smoothing

bdir='~/svn_trunk/publications/adaptive_sampling/fourierbasis';
[dsets,pstyles]=nips08databstanhighd();
dtoplot=1:length(dsets)
width=3.5;
height=3.5;
fontsize=10;
%%
%*****************************************************************
%make a plot of the STRF on the final trial for each simulation
%make a plot of the original STRF's in this case 
pind=0;
dtoplot=[2 4];
for dind=dtoplot
    v=load(getpath(dsets(dind).datafile));
    
    [t,f]=getstrftimefreq(v.bssimobj.stimobj.bdata);
    twidth=getobsrvtlength(v.bssimobj.stimobj.bdata);
    
    pind=pind+1;
    
    txt=sprintf('Simulation: %s\nTrial %d',dsets(dind).lbl,v.bssimobj.niter);
    fstrf(pind)=plottheta(v.bssimobj.mobj,getm(v.bssimobj.allpost,v.bssimobj.niter),txt);

    
    fstrf(pind).width=width;
    fstrf(pind).height=height;
    fstrf(pind).fontsize=fontsize;

end

for p=1:length(dtoplot)
   fname=fullfile(bdir,sprintf('bstanhighd_strf_%03g.eps',p));
   saveas(fstrf(p).hf,fname,'epsc2');
end


%%
%**************************************************************************
%
fh=BSExpLLike.plot(dsets,pstyles);

p=1;
fname=fullfile(bdir,sprintf('bstanhighd_ellike_%03g.eps',p));
fh(p).width=width;
fh(p).height=height;
fh(p).fontsize=10;
setposition(fh(p).a(1).hlgnd,.35,.15,.6,.25)
saveas(fh(p).hf,fname,'epsc2');

p=2;
fh(p).width=width;
fh(p).height=height;
fh(p).fontsize=10;
setposition(fh(p).a(1).hlgnd,.35,.15,.6,.25)
fname=fullfile(bdir,sprintf('bstanhighd_ellike_%03g.eps',p));
saveas(fh(p).hf,fname,'epsc2');