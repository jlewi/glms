%**************************************************************************
%
%Explanation: Setup the info. max. simulations when we represent theta in
%the fourier space and we apply the fourier transform to time and frequency
%separatly
%
% Updater uses batch methods on the first n trials
%Theta is represented in the fourier domain
function dsets=fbsetupftseplowdinitbatch()


dsets=struct;
dind=0;
    
pind=3;
%allparam.pcutoff=FilePath('RESULTSDIR',fullfile(bpdir,sprintf('%s_%03g.mat',bpcutoff,pind)));            
allparam.pcutoff=[];

%try decreasing the bin size and increasing the number of time bins
allparam.obsrvwindowxfs=250;
allparam.stimnobsrvwind=10;
allparam.freqsubsample=2;
allparam.model='MBSFTSep';

%use rank 1 eigen update because woodbury is proving unstable
allparam.compeig=true;

%create a bdata an mobject to use to decide which frequency components to
%include
bdata=BSSetup.initbsdata(allparam);

strfdim=getstrfdim(bdata);
mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.alength=0;
mparam.hasbias=0;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mobj=MBSFTSep(mparam);


nfreq=10;
ntime=2;
subind=bindexinv(mobj,1:mobj.nstimcoeff);
btouse=subind(:,1)<=nfreq & subind(:,2)<=ntime;
btouse=find(btouse==1);

allparam.mparam.btouse=btouse;


allparam.updater.type='BSNewtonBatchInit';
allparam.updater.ninit=length(btouse);
allparam.updater.online=Newton1d('compeig',true);
allparam.updater.batch=BatchML();



%************************************************************
%Shuffled
%************************************************************
% shparam=allparam;
% shparam.stimtype='BSBatchShuffle';
% 
% dind=dind+1;
% dnew=BSSetup.setupinfomax(shparam);
% dsets=copystruct(dsets,dind,dnew);


% %************************************************************
% %Info. Max.
% %************************************************************
dind=dind+1;
imparam=allparam;
imparam.stimtype='BSBatchPoissLB';
dnew=BSSetup.setupinfomax(imparam);
dsets=copystruct(dsets,dind,dnew);


% %************************************************************
% %Info. Max. Tan
% %************************************************************
dind=dind+1;
imtan=allparam;
imtan.stimtype='BSBatchPoissLBTan';
imtan.model='MBSFTSepLowRank';
imtan.tanparam.rank=2;
imtan.tanparam.startfullmax=25;
dnew=BSSetup.setupinfomax(imtan);
dsets=copystruct(dsets,dind,dnew);