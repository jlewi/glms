%10-11-2008
%
%Make plots illustrating the overfitting that results when we don't
%smooth the strf

clear all;
setpathvars;

%WARNING: index into dsets are likely to change you want the following
% Shuffled - use full posterior with smoothing
% Info. max using full posterior with smoothing

bdir='~/svn_trunk/publications/adaptive_sampling/fourierbasis';
[dsets,pstyles]=fbdataonlinesmooth();

dtoplot=1:length(dsets);

%width and height for the plots
width=4;
height=4;
%%
%select the data sets corresponding to shuffled designs
%and info. max.

%plot the expected log likelihood
fh=BSExpLLike.plot(dsets(dtoplot),pstyles);
fh(1).width=width;
fh(2).width=width;
fh(1).height=height;
fh(2).height=height;
bname='ellikeonlinesmoothing';

fname1=fullfile('~/svn_trunk/publications/adaptive_sampling/fourierbasis',[bname '_birdsong.eps']);
fname2=fullfile('~/svn_trunk/publications/adaptive_sampling/fourierbasis',[bname '_noise.eps']);
saveas(fh(1).hf,fname1,'epsc2');
saveas(fh(2).hf,fname2,'epsc2');
%%
%******************************************************************
%Make a plot of the STRF's in the fourier domain and spectral domain
%*******************************************************************
pind=0;

for dind=dtoplot
    v=load(getpath(dsets(dind).datafile));
    
    if ~isempty(dsets(dind).smooth)
        %apply post smoothing
        if ~isa(v.bssimobj.allpost,'ModBSSinewaves');
            
 msin=ModBSSinewaves('nfreq',dsets(dind).smooth.nfcutoff,'ntime',dsets(dind).smooth.ntcutoff,'mobj',v.bssimobj.mobj);;
    
        theta=getm(v.bssimobj.allpost,v.bssimobj.niter);
        theta=smooth(msin,theta,dsets(dind).smooth.nfcutoff,dsets(dind).smooth.ntcutoff);
        end
    else
        theta=getm(v.bssimobj.allpost,v.bssimobj.niter);
        msin=v.bssimobj.mobj;
    end
    
    [t,f]=getstrftimefreq(v.bssimobj.stimobj.bdata);
    twidth=getobsrvtlength(v.bssimobj.stimobj.bdata);
    
    pind=pind+1;
    
    fstrf(pind)=plottheta(msin,theta);

    title(fstrf(dind).a(1,1),sprintf('STRF\n%s', dsets(dind).lbl));
    fstrf(pind).width=width;
    fstrf(pind).height=height;
    

end
%%

 for p=1:pind
    fname=fullfile(bdir,sprintf('strfonlinesmooth_%03g.eps',p));
    saveas(fstrf(pind).hf,fname,'epsc2');
 end
