%date: 01-24-2009
%
%Explanation: Make line plots of the min, max, and median of the std across all
%   components
%
%several trials
clear variables;


%select the dataset
wdataset=2

if (wdataset==1)
    %These are the first results we used for the paper
    [dsets]=gp_data_infomax_090208_001();
    dsets=dsets(5:6);
    dsname='d090208'

elseif (wdataset==2)

    %these are the second results
    %08-23-2009
    %these results should have used the second RF that I plotted the batch
    %estimate for in the paper
    dsets=gp_data_infomax_090809_001
    dsname='d090809'

end

forpaper=true;

if (forpaper)
    width=6.1;
    height=4;

    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';

    fext='eps';
    ftype='epsc2';

    %width for the plot of the strf's
    pstrf.width=6.2;
    pstrf.height=5;

    %widht and height for plot of stimulus coefficients
    pshist.width=3;
    pshist.height=3;

    pbias.width=pshist.width;
    pbias.height=pshist.height;


else

    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';
    fext='png';
    ftype='png';


    %width for the plot of the strf's
    pstrf.width=5;
    pstrf.height=3;

    %widht and height for plot of stimulus coefficients
    pshist.width=3;
    pshist.height=3;

    pbias.width=pshist.width;
    pbias.height=pshist.height;
end

fsize=10;
savedfigs=false;

for dind=1:length(dsets)
    v=load(getpath(dsets(dind).datafile));
    simobj(dind)=v.simobj;
end


fprintf('Changing simulation labels make sure they are correct !!!\n');
setlabel(simobj(1),'opt. gp.');
setlabel(simobj(2),'white noise');

scriptfile=mfilename();
%%




%trials=[1000 5000 25000 50000 100000];
trials=[10*10^3 50*10^3 100*10^3 200*10^3 300*10^3 400*10^3];
trials=[300:300:2400];
%trials=[300, 1000, 3000, 10000, 30000, 100*10^3, 300*10^3];
trials=[2000:2000:10000 2*10^4:10^4:10^5 1.5*10^5:50*10^4: 3*10^5];

nrows=length(simobj);

oinfo=cell(ceil(length(simobj)/2),4);

%limits for the colorbars
clim.stim=[-2.5 2.5]*10^-3;
clim.stimstd=[-2.5 2.5]*10^-3;

clim.shist=[-2 .5];

%number of sims per set
nset=2;

%initialize the structure to store the min/max/median var
data=struct('minvar',nan(1,length(trials)),'maxvar',nan(1,length(trials)),'medvar',nan(1,length(trials)),'lbl',[]);
data=repmat(data,length(simobj),1);

%**********************************************************************
%loop over the datesets and plot the strfs
%*********************************************************
%for sall=1:2:length(simobj)
for sall=1:2:1
    ncols=length(trials)+1;
 
    for sind=1:length(simobj);
        aind=sind-sall+1;

        data(sind).lbl=simobj(sind).label;
        for tind=1:length(trials)
            trial= trials(tind);


            t=simobj(sind).extra.t;
            f=simobj(sind).extra.f;
            %multiply t by 1000 so its in ms
            t=t*1000;

            %divide freqs by 1000 so its in khz
            f=f/1000;

            if (trial<=simobj(sind).niter)
          
                c=getc(simobj(sind).allpost,trial);

                if ~isempty(c)


                    %theta contains the coefficients in the frequency representation
                    fcoefvar=c(simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2),simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2));

                    %convert to spectrogram covariance
                    strfvar=simobj(sind).mobj.basis*fcoefvar*simobj(sind).mobj.basis';

                    %
                    data(sind).minvar(tind)=min(strfvar(:));
                    data(sind).maxvar(tind)=max(strfvar(:));
                    data(sind).medvar(tind)=median(strfvar(:));                    

                end



            end

        end

    end

end
%%
%**************************************************************************
%make the plot
%**************************************************************************
fh=FigObj('name','STRF std','width',pstrf.width,'height',pstrf.height,'naxes',[1 1],'fontsize',fsize);

ps(1).color=[1 0 0];
ps(1).LineWidth=4;
ps(2).color=[0 1 0];
ps(2).LineWidth=4;

for dind=1:length(data)
   ind=find(~isnan(data(dind).medvar ));
   

   %plot minimum
   hp1=plot(trials(ind),data(dind).minvar(ind).^.5,'--');
   hp2=plot(trials(ind),data(dind).maxvar(ind).^.5,'--');
   set([hp1,hp2],'color',ps(dind).color);
   set([hp1,hp2],'linewidth',ps(dind).LineWidth);
   
   
   hp=plot(trials(ind),data(dind).medvar(ind).^.5);
   addplot(fh.a,'hp',hp,'pstyle',ps(dind),'lbl',data(dind).lbl);   
end

xlabel(fh.a,'Trial');
ylabel(fh.a,'Standard Deviation');

set(fh.a,'yscale','log');
set(fh.a,'xscale','log');


lblgraph(fh);
pos=getposition(fh.a.hlgnd);
pos(1)=pos(1)+.1;
pos(2)=pos(2)+.34;
setposition(fh.a.hlgnd,pos);

set(fh.a,'xlim',[10^3 10^5]);



savefigs=false;
if (savefigs)
    saveas(fh.hf,fullfile(gdir,['gpopt_std_' dsname '.' fext]),ftype);
end

