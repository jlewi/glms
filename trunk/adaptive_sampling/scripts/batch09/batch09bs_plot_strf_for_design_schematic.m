%02-10-2009
%
%Make a plot of some of the wavefiles and a raster plot of the responses
%to that wavefile. Use 1 wave file which is bird-song and 1 which is a
%noise file.
%
%
%Make synthetic raster plots as well
%
%Revisions:
%   01-03-2009 - Remove the pre and post stim silences
clear variables;


[seqfiles]=phdbsdata_all_neurons();
seqfiles=seqfiles(1,:);




%***********************************************************************
%Compute data for fake raster plots
%***********************************************************************
%%

for dind=1:size(seqfiles,1)
    %use the info. max. design
    v=load(getpath(seqfiles(dind,2).datafile));
    bssimobj(dind)=v.bssimobj;
    bspost=bssimobj(dind).stimobj.bspost;
    
    %determine the test set
    windexes=ones(1,max(bspost.windexes));
    windexes(bssimobj(dind).stimobj.windexes)=false;
    wtest=find(windexes==1);
    
    for wind=1:length(wtest)

        [ntrials,cstart]=getntrialsinwave(bspost.bdata,wtest(wind));
        data(dind,wind).wind=wtest(wind);
    end
end
%%

%are the figures for the paper or the poster
forpaper=true;

if (forpaper)
    width=2;
    height=2;
    
    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';
    
    fext='png';
    ftype='png';
else

end

for rind=1:size(seqfiles,1)
for ind=1:1
    
   %use the info. max. design
   v=load(getpath(seqfiles(dind,2).datafile));
   bssimobj(dind)=v.bssimobj;
    
   bdata=bssimobj(rind).stimobj.bdata;
   waveind=data(rind,ind).wind;
   fh(rind,ind)=FigObj('name','BS Input and Raster', 'naxes',[1,1],'width',width,'height',height);

   
   %plot the wave file 
   
   [ntrials,cstart]=getntrialsinwave(bdata,waveind);
   [obj,spec,outfreqs,t]=getfullspec(bdata,waveind);

   %remove the pre and post stim silences
   %get the length of the silences before and after the stimulus;
   [obj,npre,npost]=nframesinsilence(obj);
   cstart=cstart+npre;
   ntrials=ntrials-npost-npre;

   %truncate spec and t by cstart
   %truncate cstart even more
   cstart=cstart+120;
   cend=760;
   spec=spec(:,cstart:cend);
   t=t(1:ntrials);
   
   %convert to Khz
   outfreqs=outfreqs/1000;
   setfocus(fh(rind,ind).a,1,1);
   imagesc(t,outfreqs,spec);

   
   set(fh(rind,ind).a(1,1).ha,'xlim',[t(1) t(end)/2]);   
   set(fh(rind,ind).a(1,1).ha,'xtick',[]);
   set(fh(rind,ind).a(1,1).ha,'ytick',[]);
   set(fh(rind,ind).a(1,1).ha,'ylim',[outfreqs(1) outfreqs(end)]);
   
   sizesubplots(fh(rind,ind),[],[],[1]);
end
end

savefigs=false;

if (savefigs)
    saveas(fh(1,1).hf,fullfile(gdir,['bs_wave_birdsong_spect.' fext]),ftype);
    %saveas(fh(1,2).hf,fullfile(gdir,['bs_wave_mlnoise_nopred.',fext]),ftype);
end
