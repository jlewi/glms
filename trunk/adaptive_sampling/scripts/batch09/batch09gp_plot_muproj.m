%date: 01-24-2009
%
%Explanation: Make a plot of the projection of the Mean on the true strf
%
clear variables;

[dsets]=gp_data_infomax_090208_001();

dsets=dsets(5:6);
dfiles=dsets;

for dind=1:length(dsets)
    v=load(getpath(dsets(dind).datafile));
    simobj(dind)=v.simobj;
end

forpaper=true;
if (forpaper)

    width=6;
    height=3;
    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';

    fontsize=12;
    fext='eps';
    ftype='epsc2';
else

    width=6;
    height=2;

    gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation/';

    fontsize=14;
    fext='png';
    ftype='png';
end
fprintf('Changing simulation labels make sure they are correct !!!\n');
setlabel(simobj(1),'opt. gp.');
setlabel(simobj(2),'white noise');

%%************************************************************************
%compute the projection of the Mean on the true STRF
%only do it for the optimized gaussian process.
for sind=1:1
    mobj=simobj(sind).mobj;
    m=getm(simobj(sind).allpost);
    truestrf=simobj(sind).observer.theta(mobj.indstim(1):mobj.indstim(2),:);
    m=m(mobj.indstim(1):mobj.indstim(2),:);
    muproj{sind}=truestrf'*m;
end
%%


%how often to subsample the data before plotting
subsamp=500;

%throw out early trials for which MSE is increasing
starttrial=1000;

%rscale the scaling constant to get the firing rate from
%the number of spikes
%rscale=1/(10*10^-3);


%flen- the length of the cosine filter to use should be an odd number
flen=7;
fcoeff=sin([0:flen-1]*pi/(flen-1));
fcoeff=fcoeff/sum(fcoeff);


fh=FigObj('name','projection of MAP','width',width,'height',height,'xlabel','Time bin','ylabel','Projection of MAP','naxes',[1 1],'fontsize',fontsize);


pstyle(1).linewidth=8;
pstyle(1).color='r';
pstyle(2).linewidth=4;
pstyle(2).color='g';

hold on;

%plot just the first plot
for j=1

   mup=muproj{j};


    %   fh.a=addplot(fh.a(,'hp',hp,'lbl',lbl,'pstyle',pstyle(j));
    %set(fh.a.ha,'ylim',[0 1]);

    
    
    %plot first 1000 observations
%     pind=1;
%     setfocus(fh.a,1,pind);
%     trials=[1:1000];
%     hp=plot(trials,mup(trials));
%     set(fh.a(1,pind).ha,'xticklabel',get(fh.a(1,pind).ha,'xtick')/1000);
%     
     %plot all observats
    pind=1;
    setfocus(fh.a,1,pind);
    %    obsrv=slidefilter(obsrv,fcoeff);
    hp=plot(0:getniter(simobj(j)),mup);
    lbl=getlabel(simobj(j));
    set(fh.a(1,pind).ha,'xlim',[0 getniter(simobj)]);
   set(fh.a(1,pind).ha,'xticklabel',get(fh.a(1,pind).ha,'xtick')/1000);
         
%     pind=3;
%     setfocus(fh.a,1,pind);
% 
%     trials=424*10^3:425*10^3;
%     hp=plot(trials,mup(trials));
%     set(fh.a(1,pind).ha,'xticklabel',get(fh.a(1,pind).ha,'xtick')/1000);
%  
end

%%
% pind=1;
% 
% set(fh.a(1,pind).ha,'ylim',[0 6]*10^-4);
% set(fh.a(1,pind).ha,'xscale','linear');
% xlabel(fh.a(1,pind).ha,'Trial (x10^3)');
% ylabel(fh.a(1,pind).ha,'r_t');


pind=1;
xlabel(fh.a(1,pind).ha,'Time bin');
%set(fh.a(1,pind).ha,'ylim',[0 6]*10^-4);
xlabel(fh.a(1,pind).ha,'Time bin (x10^3)');


% pind=3;
% xlabel(fh.a(1,pind).ha,'Trial (x10^3)');
% set(fh.a(1,pind).ha,'ylim',[0 6]*10^-4);
% set(fh.a(1,pind).ha,'yticklabel',[]);

fh=lblgraph(fh);
setfsize(fh);
sizesubplots(fh);

odata={fh,''};

%setposition(fh.a.hlgnd,.15,.5,[],[])

savefigs=false;
if (savefigs)
    saveas(fh.hf,fullfile(gdir,['gp_mapproj.' fext]),ftype);
end

% otble=[{'script',mfilename()};otble];
% onenotetable(otble,seqfname('~/svn_trunk/notes/mse.xml'));