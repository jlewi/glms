%05-03-2009
%
%Get the names of the wave files in each simulation used as the test set

[seqfiles]=phdbs_data_infomax_tspace_001(); 

seqfiles=seqfiles(:,1:2);

%store the number of trials
tsetfname=cell(size(seqfiles,1),2);


for row=1:size(seqfiles,1);
    for col=1:size(seqfiles,2)
        
        v=load(getpath(seqfiles(row,col).datafile));
        bssimobj=v.bssimobj;
        
        bdata=bssimobj.stimobj.bdata;
        
        nwaves=getnwavefiles(bdata);
        
        tset=1:nwaves;
        tset(bssimobj.stimobj.windexes)=nan;
        tset=find(~isnan(tset));
        
        fname={bdata.stimspec.fname};
        tsetfname{row,col}=fname(tset);
        
        fprintf('%s \t %s \n', fname{1,1},fname{1,2});

    end
end


