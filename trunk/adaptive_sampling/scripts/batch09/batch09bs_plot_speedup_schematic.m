%******************************************************************
%Date: 04-22-2009
%
%Explanation: Make a  schematic of how we compute speedup
%
%
% We make a single figure and label the various quantities
%
clear variables;
setpathvars

%[seqfiles]=phdbsdata_all_neurons();
[seqfiles]=phdbs_data_infomax_tspace_001(); 

%use the info. max and shuffled designs. Ignore the tangent space design.
seqfiles=seqfiles(2,1:2);

forpaper=true;
if (forpaper)
    width=3.1;
    height=3;

    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';

    fext='eps';
    ftype='epsc2';
    
    fsize=10;
else
    width=2.3;
    height=2.35;
    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figsagain/speedup_schematic';
    fext='png';
    ftype='png';
    fsize=14;
    
    %position for inner axes
    %to make them all the same
    %bottom, width, height
    innerpos=[.5 1.5 1.5];
end


%create one file per dataset
for sind=1:size(seqfiles,1)
    for dind=1:2
        fprintf('Design %d of %d \n',(sind-1)*2+dind, numel(seqfiles));
        
        bsllike(sind,dind)=BSExpLLike('exllfile',[seqfiles(sind,dind).exllfile]);


    %compute the expected mse 
     % compellike(bsllike(sind,dind));
    end
end

pind=0;


%************************************************************************
%Create the speedup object
%*************************************************************************
tcutoff=2*10^4;
%We partition the fraction of the max predictive accuracy into bins
%bwidth- the width of the bins
bwidth=.005;

bsspeed=BSSpeedup('seqfiles',seqfiles,'tcutoff',tcutoff,'bwidth',bwidth);

data=bsspeed.data;
bcent=bsspeed.bcent;
%%
%************************************************************
%Parameters for the graphs
%*************************************************************


%which wave file in the test set to use
tind =1;

lbls={'Shuffled','Info. Max.'};

%define the plot styles 
%for the shuffled and info. max designs
pstyles=PlotStyles();
ps(1)=pstyles.plotstyle(1);
ps(2)=pstyles.plotstyle(2);
ps(2).color=[0 1 0];
ps(2).markerfacecolor=[0 1 0];

%wavefiles in the test set
bslike=bsspeed.bsllike;
%**************************************************************************
%Compute the maximum accuracy
%*************************************************************************
%     for cind=1:size(seqfiles,2);
%      
%         %select just those trials for which the predicted accuracy is a
%         %double
%         ntrials=length(bsllike(1,cind).trials);
% 
% 
% 
% 
%         [ldata(pind,cind).exllike,ldata(pind,cind).trials]=getexmeand(bsllike(1,cind));
% 
% 
%         tind=find(ldata(pind,cind).trials>bsspeed.tcutoff);
% 
% 
%         %different wavfile
%         ldata(pind,cind).maxaccuracy=mean(ldata(pind,cind).exllike(:,tind),2);
%     end

    %average the maxaccuracy across the datasets
%    maxaccuracy=mean([ldata(pind,:).maxaccuracy],2);
    maxaccuracy=exp(max([bsspeed.data.maxaccuracy]));
    
%************************************************************************
%Make a plot of the expected log-likelihood in the linear domain
%*************************************************************************
%%


wavinfo=[bsllike.wavinfo];

[wind,index]=unique([wavinfo.wind],'first');
wavinfo=wavinfo(index);

nplots=length(wavinfo);
clear fexp;

wind=tind;
    %plot the mse for al the files in the test set
    fexp(wind)=FigObj('name','%Explainable Variance','width',width,'height',height,'xlabel','Time bin','ylabel', 'exp[Q(t)]','fontsize',fsize );

    figure(fexp(wind).hf);
    %loop over the simulations
    pind=0;
    for dind=1:length(bsllike)
        pind=pind+1;
        setfocus(fexp(wind).a);

       
        rind=find([bsllike(dind).wavinfo.wind]==wavinfo(wind).wind);
        
        %throw out any points for the log likelihood is close to zero
        %that we needed to use a multi precision object to store it
        keep=zeros(1,rind);
        

        if isfield(bsllike(dind).exllike(rind),'isdouble')      
            isd=[bsllike(dind).exllike(rind,:).isdouble];
        else
            isd=[];
        end
       
        
        isd=logical([bsllike(dind).exllike(rind,:).isdouble]);
        
        
        stats=[bsllike(dind).exllike(rind,isd).stats];
        mean=[stats.mean];
      
        
       
        %mse as a function of trial
        ttrial=[bsllike(dind).trials(isd)];

        [ttrial ind]=sort(ttrial);
        mean=mean(ind);
      

        %throw out any trials for which mean <-10^8
        %otherwise the plot will show an asymptote that will make the plot
        %look bad
       ind=find(mean>-10^8);
        mean=mean(ind);
       
        ttrial=ttrial(ind);
        
        
      
        hp=plot(ttrial,exp(mean));
      
        %ps=pstyles.plotstyle(pind);
   
        addplot(fexp(wind).a,'hp',hp,'pstyle',ps(dind));
    end


    set(fexp(wind).a,'xscale','log');
    
    set(fexp(wind).a.ha,'units','inches');
    pos=get(fexp(wind).a.ha,'position');
  
    set(fexp(wind).a.ha,'position',pos);
    
%    set(fexp(wind).a,'xlim',[10^3 10^4]);
    lblgraph(fexp(wind));
    set(fexp(wind).a,'ylim',[0 1]);

    %***********************************************
    %add a dashed line to indicate V_{max}
    %****************************************************
    xl=get(fexp(wind).a,'xlim');
    plot(xl,maxaccuracy*ones(1,2),'b--','LineWidth',2);
    
    tvoffset=.02;
    thoffset=50;
    text(xl(1)+thoffset,tvoffset+maxaccuracy,'V_{max}');

    %*************************************************************
    %Plot lines to indicate V_e and t_shuffled, t_info_max
    %************************************************************
    bind=101;
    ve=bsspeed.bcent(bind);
    trials=bsspeed.data(1).trialvacc(:,bind);
    plot([xl(1) trials(1)],ve*ones(1,2),'b--','LineWidth',2);

    text(xl(1)+thoffset,ve+tvoffset,'V_e');
    
    %make veritial lines
    plot(trials(1)*ones(1,2), [0 ve],'b--','LineWidth',2);
    text(trials(1),-3*tvoffset,'t_{shuffled}');
    
    plot(trials(2)*ones(1,2), [0 ve],'b--','LineWidth',2);
    text(trials(2),-3*tvoffset,'t_{info. max.}');
    
pind=pind+1;
figs(pind)=fexp;

%remove the labels because it interferes with t_shuffled label
set(fexp(wind).a.ha,'xticklabel',[]);
%reposition the x-axis label
pos=get(fexp(wind).a.xlbl.h,'position');
pos(2)=-.11;
set(fexp(wind).a.xlbl.h,'position',pos);

set(fexp(wind).a.ha,'xlim',[10^3 5*10^4]);

%**************************************************************************
%save figures
%**************************************************************************
%%


savefigs=false;

if (savefigs)
   fname=['bs_speedschem.' fext];
   saveas(fexp.hf,fullfile(gdir,fname),ftype); 


end
