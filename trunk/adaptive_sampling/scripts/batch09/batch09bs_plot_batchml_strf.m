%02-12-2009
%Make a plot of the BatchML estimates of the strf and spike history
%to include in my phd thesis
%
%Here we make plots of the STRF when we use the fourier repersentation to
%smooth the STRF in time.
setpathvars;


neuron='thesis';

mname=mfilename();
%set the parameters appropriately
switch neuron
    case 'br2523'
        [seqfiles bfile]=phdbsdata_br2523();


    case 'all'
        [seqfiles bfile]=phdbsdata_all_neurons();

    case 'thesis'
        [seqfiles bfile]=phdbsdata_all_neurons();
        bfile=bfile(1:2);
    otherwise
        error('unrecognized neuron');

end


forpaper=true;

if (forpaper)
    stimwidth=3;
    stimheight=3;
    
    shistwidth=3;
    shistheight=stimheight;

    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';

    fext='eps';
    ftype='epsc2';
    fontsize=12;
else

end

%indicates whether we want to make an image of the error bars for the
%stimulus coefficients
stimebars=false;

%indicates whether we want to include error bars for the spike history
%terms
shistebars=true;

%**************************************************************************
%Make the plots
%*************************************************************************
%%
%load the plots
for sind=1:length(bfile)
    v=load(getpath(bfile(sind).datafile));

    bssim(sind)=v.bssimobj;
end


%%
for sind=1:length(bfile)

    col=1;
    %**************************************************************************
    %plot the strf
    %**************************************************************************
    naxes=1;
    if (stimebars)
        naxes=naxes+1;
    end
    fh(sind,col)=FigObj('name','STRF Coefficents','naxes',[1 naxes],'width',stimwidth,'height',stimheight,'fontsize',fontsize);
    figure(fh(sind,col).hf);

    mobj=bssim(sind).mobj;
    [t,freqs]=getstrftimefreq(bssim(sind).bdata);

    %convert to ms and hz
    t=t*1000;
    freqs=freqs/1000;

    %************************************************
    %Plot the STRF
    %**************************************************
    aind=1;
    title(fh(sind,col).a(aind),'STRF');


    [stimcoeff,shistcoeff,bias]=parsetheta(mobj,bssim(sind).results(end).theta);

    setfocus(fh(sind,col).a,1,aind);
    sind=sind;
    imagesc(t,freqs,tospecdom(mobj,stimcoeff));
    xlabel(fh(sind,col).a(aind),'Time(ms)');
    ylabel(fh(sind,col).a(aind),'Frequency(kHz)');
    hc=colorbar;
    xlim([t(1) t(end)]);
    ylim([freqs(1) freqs(end)]);
    sethc(fh(sind,1).a(aind),hc);

    yt=get(hc,'ytick');
    set(hc,'ytick',yt(1:2:end));


    %************************************************
    %Plot the STRF errorbars
    %**************************************************
    aind=2;

    if (stimebars)

        setfocus(fh(sind,col).a(1,aind));
        title(fh(sind,col).a(aind),'Error bars');

        h=(bssim(sind).results(end).hessian);
        c=-inv(h);

        %theta contains the coefficients in the frequency representation
        fcoefvar=c(bssim(sind).mobj.indstim(1):bssim(sind).mobj.indstim(2),bssim(sind).mobj.indstim(1):bssim(sind).mobj.indstim(2));

        %convert to spectrogram covariance
        strfvar=bssim(sind).mobj.basis*fcoefvar*bssim(sind).mobj.basis';

        %
        strfvar=reshape(diag(strfvar),bssim(sind).mobj.klength,bssim(sind).mobj.ktlength);

        imagesc(t,freqs,strfvar.^.5);
        xlabel(fh(sind,col).a(aind),'Time(ms)');
        hc=colorbar;

        sethc(fh(sind,1).a(aind),hc);
        xlim([t(1) t(end)]);
        ylim([freqs(1) freqs(end)]);
        set(fh(sind,1).a(aind).ha,'yticklabel',[]);
    end


    lblgraph(fh(sind,1));
    sizesubplots(fh(sind,1));
    %************************************************************************
    %Plot the spike history
    %***********************************************************************
    col=col+1;

    fh(sind,col)=FigObj('name','Spike History Coefficents','width',shistwidth,'height',shistheight);
    figure(fh(sind,col).hf);


    t=-1*[getshistlen(mobj):-1:1];
    hp=plot(t,shistcoeff);
    if (shistebars)
        pstyle.marker='none';
    else
        pstyle.marker='o';
    end
    pstyle.markerfacecolor='b';
    pstyle.linewidth=3;

    if (shistebars)
        %add error bars
        h=(bssim(sind).results(end).hessian);
        c=-inv(h);
        thetavar=diag(c);
        shistvar=thetavar(bssim(sind).mobj.indshist(1):bssim(sind).mobj.indshist(2));
        errorbar(t,shistcoeff,shistvar.^.5);
    end
    addplot(fh(sind,col).a,'hp',hp,'pstyle',pstyle);
    xlabel(fh(sind,col).a,'Time (ms)');
    ylabel(fh(sind,col).a,'Value');
    title(fh(sind,col).a,'Spike History Filter');
    xlim([t(1) t(end)]);


    lblgraph(fh(sind,col));

end


%************************************************************************
%Compuct the expected log-likelihood
%************************************************************************
ntest=2;
for bind=1:length(bfile)
    bll(bind)=BSLogLikeBatchFit('bssim',bssim(bind));

    for wind=1:ntest
        ll(bind,wind)=getllike(bll(bind),length(bssim(bind).results),wind);
    end
end
%***********************************************************************
%Save the plots
%***********************************************************************
savedata=false;

if (savedata)
    for sind=1:size(fh,1)

        fstrf=sprintf('bs_batch_strf_n%02g',sind);
        fshist=sprintf('bs_batch_shist_n%02g',sind);
        saveas(fh(sind,1).hf,fullfile(gdir,[fstrf '.' fext]),ftype);

        saveas(fh(sind,end).hf,fullfile(gdir,[fshist '.' fext]),ftype);
    end

end