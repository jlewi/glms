%date: 01-24-2009
%
%Explanation: Make a plot of the mean squared error of the estimated STRF
%  for my phd.
%
%Assumptions
%  We assume theta is the same for all simulations because we only get the
%  value of theta once
clear variables;



%select the dataset
wdataset=1;

%maximum number of random datasets to use to compute the error and
%standard deviation
maxnrand=10;

%which gpupdates to exclude
gpexclude=[20000];

if (wdataset==1)
    %These are the first results we used for the paper
    [dsets]=gp_data_infomax_091110_001();
    dsname='091110'
end

forpaper=true;
if (forpaper)

    width=6.2;
    height=4.5;
    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs/';

    ftype='epsc2';
    fext='eps';
else
    width=6.2;
    height=3;

    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs/';
    ftype='png';
    fext='png';
end


%**************************************************************************
%Group the data we group the data by the update interval and whether its
%infomax or iid design.
%**************************************************************************
%simerr cell array of simerr objects
dinfomax=struct('simerrfile',[],'simerr',{},'infomax',[],'gpupdateint',[],'thetamag',[],'stimmag',[]);
diid=struct('simerrfile',[],'simerr',{},'thetamag',[],'stimmag',[]);


pdata=struct('mustim',[],'stdstim',[],'trials',[],'label',[],'nsets',[],'hp',[],'herr',[],'gpupdate',[]);

for dind=1:length(dsets)

    try
        %load might throw errors because we didn't copy the files
        %containing the inputs
        v=load(dsets(dind).datafile.getpath());
    catch

    end
    simobj=v.simobj;
    stimobj=simobj.stimchooser;
    if isa(stimobj,'POptAsymGPPCanonLB')
        if (length(dinfomax)>0)
            ind=find([dinfomax.gpupdateint]==stimobj.gpupdateint);
        else
            ind=[];
        end

        if isempty(ind)
            ind=length(dinfomax)+1;
        end

        if (ind>length(dinfomax))
            dinfomax(ind).simerrfile=[dsets(dind).errfile];
            dinfomax(ind).infomax=true;
            dinfomax(ind).gpupdateint=stimobj.gpupdateint;
            theta=simobj.observer.theta;
            dinfomax(ind).thetamag=(theta'*theta)^.5;
            
            stimcoeff=theta(simobj.mobj.indstim(1):simobj.mobj.indstim(2));
            stimmag=(stimcoeff'*stimcoeff)^.5;
            
            dinfomax(ind).stimmag=stimmag;
            
        else
            dinfomax(ind).simerrfile=[dinfomax(ind).simerrfile, dsets(dind).errfile];
        end

    else

        if (length(diid)>0)
            diid.simerrfile=[diid.simerrfile, dsets(dind).errfile];
            theta=simobj.observer.theta;
            diid.thetamag=(theta'*theta)^.5;
            
            stimcoeff=theta(simobj.mobj.indstim(1):simobj.mobj.indstim(2));
            stimmag=(stimcoeff'*stimcoeff)^.5;
            diid.stimmag=stimmag;
            
        else
            diid(1).simerrfile=[dsets(dind).errfile];
        end

    end

end
%%*************************************************************************
%%
%Compute the mean and standard deviation of the error
%

pind=0;
for ind=1:length(dinfomax)

    %load the first file and get the trials
    v=load(dinfomax(ind).simerrfile(1).getpath);
    simerr=v.simerr;

    trials=simerr.experr.trials;

    err=zeros(length(dinfomax(ind).simerrfile),length(trials));

    stimmag=dinfomax(ind).stimmag;
    err(1,:)=simerr.experr.errstim/stimmag^2;

    nerr=simerr.nerr;
    row=1;
    for findex=2:length(dinfomax(ind).simerrfile)
        v=load(dinfomax(ind).simerrfile(findex).getpath);
        simerr=v.simerr;

        if (simerr.nerr==nerr)
            row=row+1;
            err(row,:)=simerr.experr.errstim;
        end
    end

    err=err(1:row,:);

    pind=pind+1;

    pdata(pind).trials=trials;
    pdata(pind).mustim=mean(err,1);
    pdata(pind).stdstim=std(err,0,1);
    pdata(pind).nsets=row;
    pdata(pind).label=sprintf('gp. int.=%d',dinfomax(ind).gpupdateint);
    pdata(pind).gpupdate=dinfomax(ind).gpupdateint;
end

%compute the stastics for the iid design


%load the first file and get the trials
v=load(diid.simerrfile(1).getpath);
simerr=v.simerr;

trials=simerr.experr.trials;

err=zeros(length(diid.simerrfile),length(trials));

simmag=dinfomax(ind).stimmag;
err(1,:)=simerr.experr.errstim/stimmag^2;


nerr=simerr.nerr;
row=1;

nfiles=min([length(diid.simerrfile),maxnrand]);
for findex=2:nfiles
    v=load(diid.simerrfile(1).getpath);
    simerr=v.simerr;

    if (simerr.nerr==nerr)
        row=row+1;
        err(row,:)=simerr.experr.errstim;
    end
end

err=err(1:row,:);

pind=pind+1;

pdata(pind).trials=trials;
pdata(pind).mustim=mean(err,1);
pdata(pind).stdstim=std(err,0,1);
pdata(pind).nsets=row;
pdata(pind).label=sprintf('iid');

%%
%make a plot of the err
nplots=1;


ferr=FigObj('name','Expected Error','width',width,'height',height,'naxes',[1,nplots]);


%loop over the simulations because we only want to read the maps once
for pind=1:length(pdata)

    
    plotpind=true;
    if ~(isempty(pdata(pind).gpupdate)) 
       if ~(isempty(find(gpexclude==pdata(pind).gpupdate)))
          plotpind=false ;
       end
    end
    
    if (plotpind)
        rind=1;


        %*********************************************************************
        %Plot the err of the stimulus coefficients
        %*****************************************************************
        setfocus(ferr.a,1,rind);
        hold on;

        hp=plot(pdata(pind).trials,pdata(pind).mustim);
        pdata(pind).hp=hp;


        stdint=100;
        %linearly space the points on a log scale
        nepts=50;
        pts=linspace(0,log10(pdata(pind).trials(end)),nepts);
        pts=floor(10.^pts);

        %pts=[1:stdint:length(pdata(pind).trials);

        lb=zeros(1,length(pts));
        %we only show +1 std not -1 std because the error is always positive
        %and -1std would be a negative number and won't display right on a log
        %scale
        herr=errorbar(pdata(pind).trials(pts),pdata(pind).mustim(pts),lb,pdata(pind).stdstim(pts));    
        pdata(pind).herr=herr;
        %set(herr,'LineStyle','None')
        set(herr,'LineWidth',2)
        set(ferr.a(1,rind).ha,'yscale','log');

        addplot(ferr.a(1,rind),'hp',hp,'lbl',pdata(pind).label);


    end
end


%*********************************************************************
%add labels
%*********************************************************************
rind=1;
%title(ferr.a(1,rind),sprintf('Stimulus\ncoefficients'));
%ylabel(ferr.a(1,rind),'$E_{\vec{\theta}}||\vec{\theta} - \vec{\theta}_o||^2$','Interpreter','latex');
ylabel(ferr.a(1,rind),'Normalized Error');
set(ferr.a(1,rind),'xscale','log');
xlabel(ferr.a(1,rind),'time bin');


lblgraph(ferr);


for pind=1:length(pdata)
   color=get(pdata(pind).hp,'Color'); 
   set(pdata(pind).herr,'Color',color)
end
setposition(ferr.a(1,1).hlgnd,.72,.78,.25,[]);

set(ferr.a(1,rind),'xlim',[100 10^6])


savefigs=false;
if (savefigs)
    saveas(ferr.hf,fullfile(gdir,['gp_mse_' dsname '.' fext]),ftype);
end

