%02-10-2009
%
%Make a plot of some of the wavefiles and a raster plot of the responses
%to that wavefile. Use 1 wave file which is bird-song and 1 which is a
%noise file.
%
%
%Make synthetic raster plots as well
%
%Revisions:
%   01-03-2009 - Remove the pre and post stim silences
clear variables;


[seqfiles]=phdbsdata_all_neurons();
seqfiles=seqfiles(1,:);

%***********************************************************************
%Compute data for fake raster plots
%***********************************************************************
%%
%rdata= size(seqfiles(1,))xsize of test set
nrepeats=10;
for dind=1:size(seqfiles,1)
    %use the info. max. design
    v=load(getpath(seqfiles(dind,2).datafile));
    bssimobj(dind)=v.bssimobj;
    
    %get the final estimate of theta
    theta=getm(bssimobj(dind).allpost,bssimobj(dind).niter);
    shistfilt=theta(bssimobj(dind).mobj.indshist(1):bssimobj(dind).mobj.indshist(2));
    bspost=bssimobj(dind).stimobj.bspost;

    glm=bssimobj(dind).mobj.glm;
    
    %determine the test set
    windexes=ones(1,max(bspost.windexes));
    windexes(bssimobj(dind).stimobj.windexes)=false;
    wtest=find(windexes==1);
    
    for wind=1:length(wtest)

        [ntrials,cstart]=getntrialsinwave(bspost.bdata,wtest(wind));
        data(dind,wind).raster=zeros(nrepeats,ntrials);
        data(dind,wind).wind=wtest(wind);
        
        %compute glmproj ignoring shist
        [glmproj,varargout]=compglmprojnoshist(bspost,wtest(wind),bssimobj(dind).mobj,theta);


        for rind=1:nrepeats
            %initialize the spike history
            shist=zeros(bssimobj(dind).mobj.alength,1);
            
            for tind=1:ntrials
                %compute the expected firing rate and sample it
                rexp=fglmmu(glm,glmproj(tind)+shist'*shistfilt);
                obsrv=sampdist(glm,rexp);
                data(dind,wind).raster(rind,tind)=obsrv;
                
                %update shist
                shist=[shist(1:end-1); obsrv];
            end
        end
    end
end


%%

%are the figures for the paper or the poster
forpaper=true;

if (forpaper)
    width=6.1;
    height=4;
    
    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';
    
    fext='eps';
    ftype='epsc2';
else

end

for rind=1:numel(bssimobj)
for ind=1:size(data,2)
    
   bdata=bssimobj(rind).stimobj.bdata;
   waveind=data(rind,ind).wind;
   fh(rind,ind)=FigObj('name','BS Input and Raster', 'naxes',[3,1],'width',width,'height',height);

   
   %plot the wave file 
   
   [ntrials,cstart]=getntrialsinwave(bdata,waveind);
   [obj,spec,outfreqs,t]=getfullspec(bdata,waveind);

   %remove the pre and post stim silences
   %get the length of the silences before and after the stimulus;
   [obj,npre,npost]=nframesinsilence(obj);
   cstart=cstart+npre;
   ntrials=ntrials-npost-npre;

   %truncate spec and t by cstart
   spec=spec(:,cstart:end-npost);
   t=t(1:ntrials);
   
   %convert to Khz
   outfreqs=outfreqs/1000;
   setfocus(fh(rind,ind).a,1,1);
   imagesc(t,outfreqs,spec);
   ylabel(fh(rind,ind).a(1,1),'Frequency (Khz)');

   if waveissong(bdata,waveind);
      title(fh(rind,ind).a(1,1),'Bird song'); 
   else
      title(fh(rind,ind).a(1,1),'ML noise');        
   end
   
   hc=colorbar;
   sethc(fh(rind,ind).a(1,1),hc);
   
   set(fh(rind,ind).a(1,1).ha,'xlim',[t(1) t(end)]);
   
   set(fh(rind,ind).a(1,1).ha,'xtick',[]);
   set(fh(rind,ind).a(1,1).ha,'ylim',[outfreqs(1) outfreqs(end)]);
   
   %**********************************************************************
   %make a raster plot
   setfocus(fh(rind,ind).a,2,1);
   obsrv=getobsrvmat(obj,waveind);
   obsrv=obsrv(:,cstart:end-npost);
   
   hold on;
   for r=1:size(obsrv,1);
       sind=find(obsrv(r,:)>0);
    plot(t(sind),r*ones(1,length(sind)),'MarkerSize',4,'Marker','o','MarkerFaceColor','b','line','none');
   end

   ylabel(fh(rind,ind).a(2,1),'True');
   set(fh(rind,ind).a(2,1).ha,'ytick',[]);
   set(fh(rind,ind).a(2,1).ha,'xtick',[]);
   set(fh(rind,ind).a(2,1).ha,'xlim',[t(1) t(end)]);

   
   %*******************************************************************
   %Make a raster plot using the model
   %**********************************************************************
    setfocus(fh(rind,ind).a,3,1);
   obsrv=data(rind,ind).raster;

   
   %remove the pre and post silences
   obsrv=obsrv(:,npre+1:end-npost);
   hold on;
   for r=1:size(obsrv,1);
       sind=find(obsrv(r,:)>0);
      plot(t(sind),r*ones(1,length(sind)),'MarkerSize',4,'Marker','o','MarkerFaceColor','b','line','none');
   end

   xlabel(fh(rind,ind).a(3,1),'Time(s)');
   ylabel(fh(rind,ind).a(3,1),'Predicted');
   set(fh(rind,ind).a(3,1).ha,'ytick',[]);
   
   set(fh(rind,ind).a(3,1).ha,'xlim',[t(1) t(end)]);
   
   
   lblgraph(fh(rind,ind));
   sizesubplots(fh(rind,ind),[],[],[.6 .2 .2]);
end
end

savefigs=false;

if (savefigs)
    saveas(fh(1,1).hf,fullfile(gdir,['bs_wave_birdsong.' fext]),ftype);
    saveas(fh(1,2).hf,fullfile(gdir,['bs_wave_mlnoise.',fext]),ftype);
end
