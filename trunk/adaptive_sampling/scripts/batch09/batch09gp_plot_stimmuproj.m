%date: 12-15-2009
%
%Explanation: Make a plot of the projection of the stimulus mean onto the
%true strf
%
clear variables;

    fname='~/gpdata_002.mat';
    v=load(fname);
forpaper=true;
if (forpaper)

    width=6;
    height=3;
    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';

    fontsize=12;
    fext='eps';
    ftype='epsc2';
else

    width=6;
    height=2;

    gdir='~/svn_trunk/publications/adaptive_sampling/phdpresentation/';

    fontsize=14;
    fext='png';
    ftype='png';
end



%how often to subsample the data before plotting
subsamp=500;





fh=FigObj('name','projection of Stimulus Mean ','width',width,'height',height,'xlabel','trial','ylabel','Projection of Stim. Mean','naxes',[1 1],'fontsize',fontsize);


pstyle(1).linewidth=8;
pstyle(1).color='r';
pstyle(2).linewidth=4;
pstyle(2).color='g';

hold on;

%plot just the first plot
for j=1
    
    musproj=v.musave.musproj(1:v.musave.index);
    trial=v.musave.trial(1:v.musave.index);
     %plot all observats
    pind=1;
    setfocus(fh.a,1,pind);
    %    obsrv=slidefilter(obsrv,fcoeff);
    hp=plot(trial,musproj);

    %set(fh.a(1,pind).ha,'xlim',[0 getniter(simobj)]);
   set(fh.a(1,pind).ha,'xticklabel',get(fh.a(1,pind).ha,'xtick')/1000);
    set(fh.a(1,pind).ha,'ylim',[0,5])
%  
end

%%
% pind=1;
% 
% set(fh.a(1,pind).ha,'ylim',[0 6]*10^-4);
% set(fh.a(1,pind).ha,'xscale','linear');
% xlabel(fh.a(1,pind).ha,'Trial (x10^3)');
% ylabel(fh.a(1,pind).ha,'r_t');


pind=1;
xlabel(fh.a(1,pind).ha,'Trial');
%set(fh.a(1,pind).ha,'ylim',[0 6]*10^-4);
xlabel(fh.a(1,pind).ha,'Trial (x10^3)');


% pind=3;
% xlabel(fh.a(1,pind).ha,'Trial (x10^3)');
% set(fh.a(1,pind).ha,'ylim',[0 6]*10^-4);
% set(fh.a(1,pind).ha,'yticklabel',[]);

fh=lblgraph(fh);
setfsize(fh);
sizesubplots(fh);

odata={fh,''};

%setposition(fh.a.hlgnd,.15,.5,[],[])

savefigs=false;
if (savefigs)
    saveas(fh.hf,fullfile(gdir,['gp_mapproj.' fext]),ftype);
end

% otble=[{'script',mfilename()};otble];
% onenotetable(otble,seqfname('~/svn_trunk/notes/mse.xml'));