%date: 01-24-2009
%
%Explanation: Plot the strf on plot the STD as well.
%
%
%several trials
clear variables;


%select the dataset
wdataset=1

if (wdataset==1)
    %These are the first results we used for the paper
    [dsets]=gp_data_infomax_090208_001();
    dsets=dsets(5:6);
    dsname='d090208'

elseif (wdataset==2)

    %these are the second results
    %08-23-2009
    %these results should have used the second RF that I plotted the batch
    %estimate for in the paper
    dsets=gp_data_infomax_090809_001
    dsname='d090809'

end

forpaper=true;

if (forpaper)
    width=6.1;
    height=4;

    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';

    fext='eps';
    ftype='epsc2';

    %width for the plot of the strf's
    pstrf.width=6.2;
    pstrf.height=5;

    %widht and height for plot of stimulus coefficients
    pshist.width=3;
    pshist.height=3;

    pbias.width=pshist.width;
    pbias.height=pshist.height;


else

    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs';
    fext='png';
    ftype='png';


    %width for the plot of the strf's
    pstrf.width=5;
    pstrf.height=3;

    %widht and height for plot of stimulus coefficients
    pshist.width=3;
    pshist.height=3;

    pbias.width=pshist.width;
    pbias.height=pshist.height;
end

fsize=10;
savedfigs=false;

for dind=1:length(dsets)
    v=load(getpath(dsets(dind).datafile));
    simobj(dind)=v.simobj;
end


fprintf('Changing simulation labels make sure they are correct !!!\n');
setlabel(simobj(1),'opt. gp.');
setlabel(simobj(2),'white noise');

scriptfile=mfilename();
%%




%trials=[1000 5000 25000 50000 100000];
trials=[10*10^3 50*10^3 100*10^3 200*10^3 300*10^3 400*10^3];
trials=[300:300:2400];
%trials=[300, 1000, 3000, 10000, 30000, 100*10^3, 300*10^3];
trials=[300, 2000, 4000, 10000, 30000, 100*10^3, 300*10^3];

nrows=length(simobj);

oinfo=cell(ceil(length(simobj)/2),4);

%limits for the colorbars
clim.stim=[-2.5 2.5]*10^-3;
clim.stimstd=[-2.5 2.5]*10^-3;

clim.shist=[-2 .5];

%number of sims per set
nset=2;

%whether or not to plot the variance of the coefficients or just the means
plotvar=false

if (plotvar)
    nrows=4
else
    nrows=2
end

%**************************************************************************
%loop over the datesets and plot the strfs
%*********************************************************
%for sall=1:2:length(simobj)
for sall=1:2:1
    ncols=length(trials)+1;
    fh=FigObj('name','STRFs','width',pstrf.width,'height',pstrf.height,'naxes',[nrows ncols],'fontsize',fsize);

    for sind=1:length(simobj);
        aind=sind-sall+1;

        for tind=1:length(trials)
            trial= trials(tind);


            t=simobj(sind).extra.t;
            f=simobj(sind).extra.f;
            %multiply t by 1000 so its in ms
            t=t*1000;

            %divide freqs by 1000 so its in khz
            f=f/1000;

            if (trial<=simobj(sind).niter)
                %plot the map
                setfocus(fh.a,aind,tind);
                strf=getm(simobj(sind).allpost,trial);
                strf=strf(simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2));
                strf=tospecdom(simobj(sind).mobj,strf);
                %            strf=reshape(strf,[simobj(sind).mobj.klength simobj(sind).mobj.ktlength]);



                imagesc(t,f,strf);

                %**********************************************************
                %plot the std (if c was saved)
                %**********************************************************
                if(plotvar)
                    crind=aind+2;
                    setfocus(fh.a(crind,tind));

                    c=getc(simobj(sind).allpost,trial);

                    if ~isempty(c)


                        %theta contains the coefficients in the frequency representation
                        fcoefvar=c(simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2),simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2));

                        %convert to spectrogram covariance
                        strfvar=simobj(sind).mobj.basis*fcoefvar*simobj(sind).mobj.basis';

                        %
                        strfvar=reshape(diag(strfvar),simobj(sind).mobj.klength,simobj(sind).mobj.ktlength);

                        imagesc(t,f,log10(strfvar.^.5));
                        fprintf('plotting on a log color scale \n');
                    else
                        %c wasn't saved so turn the axes off
                        set(fh.a(crind,tind),'visible','off');
                    end


                end
            end

        end

        setfocus(fh.a,aind,ncols);
        %plot the true strf
        theta=simobj(sind).observer.theta;
        theta=theta(simobj(sind).mobj.indstim(1):simobj(sind).mobj.indstim(2));
        theta=tospecdom(simobj(sind).mobj,theta);

        imagesc(t,f,theta);


    end


    %add a colorbar to the final image in the first row
    dind=1;
    tind=ncols;



    setfocus(fh.a,dind,tind);
    %add a colorbar
    fh.a(dind,tind).hc=colorbar;
    title(fh.a(dind,tind),'true');



    addtext(fh,[.5,.95],'STRF');


    if(plotvar)
        %add a color bar for the standard deviation
        setfocus(fh.a,3,ncols);
        fh.a(dind,ncols).hc=colorbar;

        %turn off the axes for the true STRF in the rows corresponding the STD
        set(fh.a(3,ncols),'visible','off');
        set(fh.a(4,ncols),'visible','off');
    end

    %*****************************************************************
    %adjust the labels
    %************************************************************
    %turn off all tickmars
    set(fh.a,'xtick',[]);
    set(fh.a,'ytick',[]);

    %set x and y limits
    set(fh.a,'xlim',[t(1) t(end)]);
    set(fh.a,'ylim',[f(1) f(end)]);

    %turn on xtick,ytick for appropriate graphs
    rind=2;
    set([fh.a(rind,1)],'ytickmode','auto');
    set(fh.a(rind,1),'xtickmode','auto');

    %********************************************************
    %Adjust the color limits
    %         set([fh.a(1:2,:)],'clim',clim.stim);
    %         set([fh.a(3:4,:)],'clim',clim.stimstd);

    %add ylabels
    ylbl=sprintf('%s\n',simobj(1).label);
    ylabel(fh.a(1,1),ylbl);

    ylbl=sprintf('%s\n',simobj(2).label);
    ylabel(fh.a(2,1),ylbl);


    if (plotvar)
        ylbl=sprintf('%s\n',simobj(1).label);
        ylabel(fh.a(3,2),ylbl);

        ylbl=sprintf('%s\nFrequency (kHz)',simobj(2).label);
        ylabel(fh.a(4,2),ylbl);
    end


    ylabel(fh.a(dind,1),ylbl);
    for rind=1:nrows*2

        dind=mod(rind-1,2)+1;
        ylbl='';
        lbl=simobj(dind).label;
        %split the lbl based on :
        sind=strfind(lbl,':');
        if isempty(sind)
            ylbl=lbl;
        else
            while ~isempty(sind)
                ylbl=sprintf('%s\n%s',ylbl,lbl(1:sind(1)-1));
                lbl=lbl(sind(1)+1:end);
                sind=sind(2:end);
            end
            ylbl=sprintf('%s\n%s',ylbl,lbl);
        end


        ylabel(fh.a(dind,1),ylbl);

    end

    %for the last row


    %add titles
    for tind=1:length(trials)
        if (trials(tind)>=1000)
            tl=sprintf('Time bin\n %2gk',trials(tind)/1000);
        else
            tl=sprintf('Time bin\n %3g',trials(tind));
        end
        title(fh.a(1,tind),tl);
    end

    %add xlabels
    for tind=1:1
        xlabel(fh.a(2,tind),sprintf('Time(ms)'));
    end


    lblgraph(fh);
    space.cbwidth=.15;
    sizesubplots(fh,space,[],[],[0 0 0 .1]);


end


%don't bother with plots of bias and spike history
return;

for sall=1:2:1


    %**************************************************************************
    %make plots of the spike history and bias coefficients
    %**************************************************************************
    fshist=FigObj('name','Spike history','width',pshist.width,'height',pshist.height,'naxes',[2 2]);

    %make a plot of the spike history on each trial

    ntrials=min([simobj(sall:sall+1).niter]);
    for sind=sall:sall+nset-1;
        aind=sind-sall+1;
        setfocus(fshist.a,1,aind);

        theta=getm(simobj(sind).allpost);
        theta=theta(:,1:ntrials);

        shistcoeff=theta(simobj(sind).mobj.indshist(1):simobj(sind).mobj.indshist(2),:);

        imagesc([1:simobj(sind).mobj.alength],1:ntrials,shistcoeff');
        title(fshist.a(1,aind),simobj(sind).label);

        set(gca,'ylim',[1 ntrials]);
        set(gca,'xlim',[.5 simobj(sind).mobj.alength+.5]);
        set(gca,'ydir','reverse');
        set(gca,'yscale','log');
        set(gca,'xtick',[]);

        %for the second row plot the true value
        setfocus(fshist.a,2,aind);
        theta=simobj(sind).observer.theta;
        theta=theta(simobj(sind).mobj.indshist(1):simobj(sind).mobj.indshist(2));
        imagesc([1:simobj(sind).mobj.alength],[0:1],ones(2,1)*theta');
        set(gca,'ytick',[]);
        set(gca,'xlim',[1 simobj(sind).mobj.alength]);
        set(gca,'ylim',[0 1]);

        set(fshist.a(2,aind).ha,'xlim',[.5 simobj(sind).mobj.alength+.5])

        xlabel(fshist.a(2,aind),'i');
    end
    ylabel(fshist.a(1,1),'Trial');
    ylabel(fshist.a(2,1),'True');
    set(fshist.a(1,1).ha,'ytick',10.^[1:2:5]);
    set(fshist.a(1,2).ha,'ytick',[]);
    %
    % clim=get([fshist.a.ha],'clim');
    % clim=cell2mat(clim);
    % clim=[min(clim(:,1)) max(clim(:,2))];

    set([fshist.a.ha],'clim',clim.shist);

    %add a colorbar
    fshist.a(1,2).hc=colorbar;


    lblgraph(fshist);

    addtext(fshist,[.5 .95], 'Spike History Coeff');
    space.cbwidth=.1;
    sizesubplots(fshist,space,[],[.8 .2],[0 0 0 .06]);


    %**************************************************************************
    %plot the bias
    %**************************************************************************
    fbias=FigObj('name','Bias','width',pbias.width,'height',pbias.height,'title','bias','xlabel','trial','ylabel','bias','naxes',[1 1]);

    for sind=sall:sall+1;
        theta=getm(simobj(sind).allpost);

        bias=theta(simobj(sind).mobj.indbias,1:simobj(sind).niter+1);

        hp=plot(0:simobj(sind).niter,bias);
        addplot(fbias.a,'hp',hp,'lbl',simobj(sind).label);
    end

    %plot the true bias
    truebias=simobj(1).observer.theta(simobj(1).mobj.indbias);
    ltrial=max([simobj(sall:sall+1).niter]);
    hp=plot(0:ltrial,truebias*ones(1,1+ltrial));
    addplot(fbias.a,'hp',hp,'lbl','true');
    set(fbias.a.ha,'xscale','log');
    lblgraph(fbias);
    set(fbias.a.ha,'xlim',[1 ltrial]);

    set(fbias.a.ha,'xtick',10.^[1:5]);
    autosizetight(fbias.a.hlgnd)
    setposition(fbias.a.hlgnd,.21,.75,.39,[]);



    %%
    %**************************************************************
    %Create a onenote table of the results
    %**************************************************************

    oinfo{ceil(sall/2),1}=fh;
    oinfo{ceil(sall/2),2}=fshist;
    oinfo{ceil(sall/2),3}=fbias;

    oinfo{ceil(sall/2),4}=[{simobj(sall).label, getrpath(dsets(sall).datafile);simobj(sall+1).label, getrpath(dsets(sall+1).datafile); 'mmag', simobj(sall).mobj.mmag;'maxrate', simobj(sall).extra.maxrate}];
end


oinfo=[{'script',scriptfile,'',''}; oinfo];

onenotetable(oinfo,seqfname('~/svn_trunk/notes/optgp.xml'));
%%

savefigs=false;
if (savefigs)
    saveas(fh.hf,fullfile(gdir,['gpopt_strf_' dsname '.' fext]),ftype);
    saveas(fshist.hf,fullfile(gdir,['gpopt_shist_' dsname '.' fext]),ftype);
    saveas(fbias.hf,fullfile(gdir,['gpopt_bias_' dsname '.' fext]),ftype);
end

