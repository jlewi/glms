%date: 01-24-2009
%
%Explanation: Make a plot of the mean squared error of the estimated STRF
%  for my phd.
clear variables;



%select the dataset
wdataset=2

if (wdataset==1)
    %These are the first results we used for the paper
    [dsets]=gp_data_infomax_090208_001();
    dsets=dsets(5:6);
    dsname='d090208'

elseif (wdataset==2)

    %these are the second results
    %08-23-2009
    %these results should have used the second RF that I plotted the batch
    %estimate for in the paper
    dsets=gp_data_infomax_090809_001
    dsname='d090809'

end

forpaper=true;
if (forpaper)

    width=6.2;
    height=4.5;
    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs/';

    ftype='epsc2';
    fext='eps';
else
    width=6.2;
    height=3;

    gdir='~/svn_trunk/publications/adaptive_sampling/batch09/figs/';
    ftype='png';
    fext='png';
end


for dind=1:length(dsets)
    seobj(dind)=SimExpError('simfile',dsets(dind).datafile);

end

%09-27-2009 Should be accurate for both datasets.
fprintf('Changing simulation labels make sure they are correct !!!\n');
seobj(1).label='opt. gp.';
seobj(2).label='white noise';

%%


%make a plot of the err
nplots=1;

errdata=compexerr(seobj(1));

hasshist=false;
hasbias=false;
if ~isempty(errdata.errshist)
    nplots=nplots+1;
    hasshist=true;
end

if ~isempty(errdata.errbias)
    nplots=nplots+1;
    hasbias=true;
end

ferr=FigObj('name','Expected Error','width',width,'height',height,'naxes',[1,nplots]);


%loop over the simulations because we only want to read the maps once
for sind=1:length(seobj)

    rind=1;
    
    %get theta for this simulation
    lbl=seobj(sind).label;
    
    errdata=compexerr(seobj(sind));
    
    %*********************************************************************
    %Plot the err of the stimulus coefficients
    %*****************************************************************
    setfocus(ferr.a,1,rind);
    
    
    hp=plot(errdata.trials,errdata.errstim);
    set(ferr.a(1,rind).ha,'yscale','log');
    
    addplot(ferr.a(1,rind),'hp',hp,'lbl',lbl);

    %*********************************************************************
    %Plot the err of the spike history coefficients
    %*****************************************************************
    if ~isempty(errdata.errshist)
        rind=rind+1;
        setfocus(ferr.a,1,rind);
    
        hp=plot(errdata.trials,errdata.errshist);
    
        set(ferr.a(1,rind).ha,'yscale','log');
        addplot(ferr.a(1,rind),'hp',hp);
    end
    
    %*********************************************************************
    %Plot the err of the bias
    %*****************************************************************
    if (hasbias);
       rind=rind+1;
        setfocus(ferr.a,1,rind);
        hp=plot(errdata.trials,errdata.errbias);
        
        set(ferr.a(1,rind).ha,'yscale','log');
        
    set(ferr.a(1,rind).ha,'yscale','log');
        addplot(ferr.a(1,rind),'hp',hp);
    end
    
end


%*********************************************************************
%add labels
%*********************************************************************
%%
rind=1;
title(ferr.a(1,rind),sprintf('Stimulus\ncoefficients'));
ylabel(ferr.a(1,rind),'$E_{\vec{\theta}}||\vec{\theta} - \vec{\theta}_o||^2$','Interpreter','latex');
set(ferr.a(1,rind),'xscale','log');
xlabel(ferr.a(1,rind),'trial');
    
if (hasshist)
    rind=rind+1;
    title(ferr.a(1,rind),sprintf('Spike history\ncoefficients'));
    %ylabel(ferr.a(1,rind),'$E_\theta||\theta - \theta_o||^2$','Interpreter','latex');
    set(ferr.a(1,rind),'xscale','log');    
    xlabel(ferr.a(1,rind).ha,'trial');
end

if (hasbias)
    rind=rind+1;
    title(ferr.a(1,rind),'Bias');
    %ylabel(ferr.a(1,rind),'$E_\theta||\theta- \theta_o||^2$','Interpreter','latex');  
    set(ferr.a(1,rind),'xscale','log');
   xlabel('trial');
end


lblgraph(ferr);

setposition(ferr.a(1,1).hlgnd,.42,.78,[],[]);




savefigs=false;
if (savefigs)
    saveas(ferr.hf,fullfile(gdir,['gp_mse_' dsname '.' fext]),ftype);
end

% otble=[{'script',mfilename()};otble];
% onenotetable(otble,seqfname('~/svn_trunk/notes/mse.xml'));