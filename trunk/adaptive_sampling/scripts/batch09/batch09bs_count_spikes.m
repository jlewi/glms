%05-03-2009
%
%Count how many spikes on average appear in each training set.

[seqfiles]=phdbs_data_infomax_tspace_001(); 

seqfiles=seqfiles(:,1:2);

%store the number of trials
ntrials=zeros(size(seqfiles,1),2);
nspikes=zeros(size(seqfiles,1),2);


for row=1:size(seqfiles,1);
    for col=1:size(seqfiles,2)
        
        v=load(getpath(seqfiles(row,col).datafile));
        bssimobj=v.bssimobj;
        
        ntrials(row,col)=bssimobj.niter;
        %3xntrials matrix of the trials selected 
        paststim=v.bssimobj.paststim;
        
        paststim=paststim';
        paststim=sortrows(paststim,[1 2 3]);
        
        while ~isempty(paststim)
           %find all trials taken from this wave file
           wind=paststim(1,1);
           
           lastrow=find(paststim(:,1)~=wind,1,'first');
           lastrow=lastrow-1;
           
           repeat=paststim(1:lastrow,2);
           rtrial=paststim(1:lastrow,3);
           
           
           %get the matrix of all responses for this wavefile
           obsrvmat=getobsrvmat(bssimobj.stimobj.bdata,wind);
           
           %get the observations on the selected trials
           ind=sub2ind(size(obsrvmat),repeat,rtrial);
           
           nspikes(row,col)=nspikes(row,col)+sum(obsrvmat(ind));
           
           paststim=paststim(lastrow+1:end,:);
           
           
        end
    end
end


