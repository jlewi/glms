%10-25-2007
%
%Make plots of the points in (mglmproj,sglmproj) space corresponding to our
%heuristic vs. i.i.d sampling
%
%Revisions:
%   10-01-2008 
%       Redo plots for NC06 paper. Fix the colorbar.
dsets=[];


feature jitallow mcos off;
%set resultsdir to the appropriate directory otherwise we won't be able to
%find the poolfile.
RESULTSDIR='/mnt/ext_harddrive/adaptive_sampling_results_06_13_2008/allresults';
dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212','audnonlin_016.mat');
%dsets(end+1).fname=fullfile(NDIR,'inpnonlin','071212','audnonlin_016.mat');
dsets(end).lbl='info. max. heuristic';
dsets(end).simvar='max';
%dsets(end).maxtrial=10000; %max trial to plot
%
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_newver.mat');
% dsets(end).lbl='info. max. i.i.d.';
% dsets(end).simvar='unif';

%needs to be a trial for which we have the covariance matrix
trials=498;

%****************************************
%load the simulation from which we extract the parameters
[simobj]=SimulationBase('fname',dsets(1).fname,'simvar',dsets(1).simvar);
mobj=getmobj(simobj);
stimchooser=getstimchooser(simobj);
magcon=stimchooser.magcon;
srescale.sfactor=magcon;


xl=[-1 10];
yl=[0 .3];
mitick=[-4 -3.5 -3];
%*******************************************************************
%Create the different stim chooser objects we want to compare
%nstim=10000;
%stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_60d_071025_001.mat');

nstim=1000;

stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_d60_06_07_001.mat'));

sobj={};
modheur='mu';
[sobj{1}]=PoolstimQuadInpHeur('fname',stimfile,'miobj',PoissExpCompMI(),'model',mobj,'stimrescale',srescale,'debug',true,'magcon',magcon,'nstim',nstim,'modheur',modheur,'miobj',getmiobj(stimchooser));
[sobj{2}]=PoolstimNonLinInp('fname',stimfile,'miobj',PoissExpCompMI(),'model',mobj,'stimrescale',srescale);
[sobj{3}]= PureTones('dim',getstimlen(mobj),'period',[.1 120]);


% nstim=10000;
% stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_60d_071112_002.mat');
% [sobj{3}]=PoolstimNonLinInp('fname',stimfile,'miobj',PoissExpCompMI(),'model',mobj,'stimrescale',srescale);

%lbls for plots
lbl{1}='$\hat{\mathcal{X}}_{heur,t+1}$';
lbl{2}='$\hat{\mathcal{X}}_{iid,t+1}$';
lbl{3}='$\hat{\mathcal{X}}_{tones,t+1}$';
%%


%an inline function which automatically computes the linear index
%of a subplot from its row and column
%subplot selects in row order so to use  sub2ind we transpose rows and
%columns
nrows=length(trials);
ncols=length(sobj);

spind=@(row,col)(sub2ind([ncols nrows],col,row));

%***********************************************************
%get the data
%***********************************************************
for tind=1:length(trials)
    trial=trials(tind);

    post=getpost(simobj,trial);

    if ~isempty(getc(post))


        %we want to use the same posterior to compute the feasible region for
        %all stimulus sets

        for sind=1:length(sobj)
            if isa(sobj{sind},'Poolstim')
            shist=[];
            mobj=getmobj(simobj);
            [xmax,oreturn,sobj{sind},reg(tind,sind)]=choosestim(sobj{sind},post,shist,mobj,[]);
            else
               %choose the stimuli and compute mu and sigma 
               clear stim;
               for xind=1:nstim
                  stim(xind)=choosestim(sobj{sind},post,[],mobj);                  
               end
         
               stim=getData(stim);

               %project the stimuli through the nonlinearity.
               stim=projinp(mobj,stim);
               reg(tind,sind).muproj=getm(post)'*stim;
               
               reg(tind,sind).sigma=stim'*getc(post);
               reg(tind,sind).sigma= reg(tind,sind).sigma.*stim';
               reg(tind,sind).sigma=sum(reg(tind,sind).sigma,2)';
               
               %compute the mutual information
               reg(tind,sind).mi=compmi(getmiobj(sobj{1}),reg(tind,sind).muproj,reg(tind,sind).sigma);

            end
        end
    end
end

%%
%****************************************************************
%Make the plots
%****************************************************************
freg=FigObj('width',6,'height',3);

%we get the posteri from the infomax set
cmap=colormap('gray');
%reverse the colormap so darker colors are more informative
cmap=cmap(end:-1:1,:);
colormap(cmap);
%get the limits for the mutual info colorbar
%scale the mi for each point to map it to a color to show the
%information
mimin=min([reg(:).mi]);
mimax=max([reg(:).mi]);

%create the axes object
freg.a=AxesObj('nrows',length(trials),'ncols',length(sobj));

for tind=1:length(trials)
    trial=trials(tind);
    if ~isempty(getc(post))
        for sind=1:length(sobj)



            %compute the feasible region
            %1. map the stimuli into
            setfocus(freg.a,tind,sind);
            hold on;

            cscale=(size(cmap,1)-1)/(mimax-mimin);
            micind=(reg(tind,sind).mi-mimin)*cscale;

            for pind=1:length(micind);
                hp=plot(reg(tind,sind).muproj(pind),reg(tind,sind).sigma(pind),'.');
                set(hp,'MarkerFaceColor',cmap(floor(micind(pind))+1,:));
                set(hp,'MarkerEdgeColor',cmap(floor(micind(pind))+1,:));
            end
            freg.a(tind,sind)=xlabel(freg.a(tind,sind),'\mu_\rho');

            set(gca,'ytick',[]);
             xlim(xl);
             ylim(yl);


        end
        %add a colorbar
        hc=colorbar;
        freg.a(tind,sind)=sethc(freg.a(tind,sind),hc);
        %adjust the ticklabels of the colorbar
        %mitick=linspace(mimin,mimax,10);
        set(hc,'ytick',(mitick-mimin)*cscale);
        set(hc,'yticklabel',mitick);
        
    else
        fprintf('Skipping trial %d \n, posterior covariance was not saved. \n',trial  );
    end
end

%**********************************************
%adjust labels


freg.a(1,1)=ylabel(freg.a(1,1),'\sigma^2');
set(freg.a(1,1),'ytickmode','auto');

for sind=1:length(sobj)   
   freg.a(1,sind)=title(freg.a(1,sind),lbl{sind});
   tlbl=gettlbl(freg.a(1,sind));
   set(tlbl.h,'interpreter','latex');
end
%call lblgraph first to adjust fontsizes
freg=lblgraph(freg);
freg=sizesubplots(freg,[],[],[],[0 0 0 .05]);


outfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/audfeasreg.eps';
%saveas(gethf(freg),outfile,'epsc2');
%previewfig(gethf(freg),'bounds','tight')
%Use exportfig b\c saveas cuts off labels
%exportfig(gethf(freg),outfile,'bounds','tight','color','rgb')