%03-17-2008
%
% This script redoes the plots of the MAPS on trials 0 and 200
%
% I had to rewrite this script to remake the plots for the accepted version
% of our neural comp paper.

fdata=fullfile(RESULTSDIR,'nips','11_27','11_27_gabortrack_001.mat');
alldata=load(fdata);
%%
fmaps=FigObj('name','time varying: MAPS 1d plots','width',2.5,'height',2.5,'fontsize',9);

fmaps.a=AxesObj('nrows',3,'ncols',1);

yl=[-1.5 1.5];

trials=[0 200 400];

%loop over the trials and make a plot of the three datasets

for tind=1:length(trials)
    trial=trials(tind);
    setfocus(fmaps.a,tind,1);
    hold on;
    
    pind=1;
    %plot random data
    data=alldata.prand.newton1d;
    hp=plot([1:100],data.m(:,trial+1)');
    lstyle=[];
    lstyle.linewidth=2;
    lstyle.color=.75*[1 1 1];    
    fmaps.a(tind,1)=addplot(fmaps.a(tind,1),'hp',hp,'pstyle',lstyle,'lbl','random');
    lbls{pind}='random';
    
    pind=pind+1;
 
    %\theta true
    data=alldata.mparam;
    hp=plot([1:100],data.ktrue(:,trial+1));
    lstyle=[];
    lstyle.linewidth=2;
    lstyle.color=[0 0 0]; 
    lstyle.linestyle='--';
    fmaps.a(tind,1)=addplot(fmaps.a(tind,1),'hp',hp,'pstyle',lstyle,'lbl','\theta true');
    lbls{pind}='\theta true';
    
    pind=pind+1;
    %plot info.max
     data=alldata.pmax.newton1d;
    hp=plot([1:100],data.m(:,trial+1)');
        lstyle=[];
    lstyle.linewidth=3;
    lstyle.color=[0 0 0];    
    fmaps.a(tind,1)=addplot(fmaps.a(tind,1),'hp',hp,'pstyle',lstyle,'lbl','info. max');
    
    lbls{pind}='info. max.';
    %ylabel
    fmaps.a(tind,1)=ylabel(fmaps.a(tind,1),sprintf('trial %d',trial));
end
    




%turn off xlabels
for rind=1:2
   set(getha(fmaps.a(rind,1)),'xtick',[]);
end


%set xlabels
set(getha(fmaps.a(3,1)),'xtick',[20:20:100]);
fmaps.a(3,1)=xlabel(fmaps.a(3,1),'i');

%ylabels
for rind=1:3
   set(getha(fmaps.a(rind,1)),'ytick',[-1 0 1]);
   set(getha(fmaps.a(rind,1)),'ylim',yl);
   
   fmaps.a(rind,1)=ylabel(fmaps.a(rind,1),'\theta_i');
end
fmaps=lblgraph(fmaps);




%turn off legends
for rind=1:3
   hlgnd=gethlgnd(fmaps.a(rind,1));
   legend(hlgnd,'off');
end

%create a custom legend to save space
hp=gethp(fmaps.a(rind,1))
lobj=LegendObj('hlines',hp([1 3 2]),'lbls',lbls([1 3 2]));
pos=get(getha(lobj),'position');
%squeeze space
lobj=setvborder(lobj,.15,.17);
lobj=setvoffset(lobj, -.015);
lobj=setfontsize(lobj,8);
pos=[.65 .8 .33 .17];
lobj=setposition(lobj,pos);
lobj=settleft(lobj,.35);

%leave some space for the legend
fmaps=sizesubplots(fmaps,[],[],[],[0 0 .025 .1]);

outfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/gabortrack_1dplots_001.eps';
%saveas(gethf(fmaps),outfile,'epsc2');
