% 03-17-2008
%   Fixed some bugs in this script so I could redo plots with Helvetica
%   fonts
% 4-24-2005
%
% Make a plot of the asymptotic covariance
clear all;
setpathvars;
fname=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001.mat');
%%
%****************************************************************
%parameters
gparam.subsamp=1;       %subsampling for computing the empirical variances

gparam.ylim=[10^-11 10];
%%
if (true)
    %load the version which has been converted to new file format
    %and for which we saved the posterior covariance matrices

    fmax=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001_max_withc.mat');
    frand=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001_rand_withc.mat');

    simmax=SimulationBase('fname',fmax,'simvar','max');
    simrand=SimulationBase('fname',frand,'simvar','rand');

else
    simmax=SimulationBase('fname',fname,'simvar','simmax');
    simrand=SimulationBase('fname',fname,'simvar','simrand');

   
    [simmax]=fillinlowmem(getupdater(simmax),simmax,[]);

    [simrand]=fillinlowmem(getupdater(simrand),simrand,[]);
    %save it
    if (false)
        [fdir, fn, fext]=fileparts(fname);
        sinfo=getfield(simmax,'sinfo');
        sinfo.allowed=true;
        simmax=setfield(simmax,'sinfo',sinfo);
        savesim(simmax,fullfile(fdir,[fn '_max_withc.mat']));

        [fdir, fn, fext]=fileparts(fname);
        sinfo=getfield(simrand,'sinfo');
        sinfo.allowed=true;
        simrand=setfield(simrand,'sinfo',sinfo);
        savesim(simrand,fullfile(fdir,[fn '_rand_withc.mat']));
    end
end

%compute the asymptotic variance
%%
[imax,imaxemp,irand,irandemp]=compasymvar('simmax',simmax,'simrand',simrand)
%%*************************************************************************
%compute the ratio of the variance in the iid and infomax case
rparallel=irand.asymevals(1)/imax.asymevals(1);
rorthog=irand.asymevals(2)/imax.asymevals(2);
fprintf('Ratio parallel (iid/imax): \t %d \n',rparallel);
fprintf('Ratio orthogonal (iid/imax): \t %d \n',rorthog);
%%

%param.file=fullfile(RESULTSDIR, 'tracking','08_24','08_24_maxtrack_001.mat');

clear('fcovar');
fcovar=FigObj('name','Asymptotic Covariance','width',5,'height',3.71);

hold on;
nrows=2;
ncols=1;
fcovar.a=AxesObj('nrows',nrows,'ncols',ncols);

lbls=cell(1,4);
%**************************************************************************
%two panels
%1. panel components parallel to mean
%2. other panel orthogonal to mean
fcovar.a=setfocus(fcovar.a,1,1);
hold on;
fcovar.a(1,1)=title(fcovar.a(1,1),'parallel');
fcovar.a(1,1)=ylabel(fcovar.a(1,1),'variance');

%**************************************************************************
%parallel to mean
%**************************************************************************


pind=0;


%***********************************************
%I.I.D
%empirical
%Parallel to theta
%**************************************************
%plot the i.i.d asymptotic variance
hp=plot(irandemp.trials,irandemp.var(1,:),getptype(pind,2));
pstyle=getbwlstyle(2);
fcovar.a(1,1)=addplot(fcovar.a(1,1), 'hp',hp,'pstyle',pstyle);
lbls{1,1}='i.i.d. emp.';

%***********************************************
%I.I.D
%Predicted
%Parallel to theta
%**************************************************
hp=plot(1:getniter(simrand),(1:getniter(simrand)).^-1*irand.asymevals(1));
lbls{1,2}='i.i.d. pred.';
pstyle=getbwlstyle(4);
fcovar.a(1,1)=addplot(fcovar.a(1,1), 'hp',hp,'pstyle',pstyle);

%***********************************************
%Info. Max
%empirical
%Parallel to theta
%**************************************************
%plot the i.i.d asymptotic variance
hp=plot(imaxemp.trials,imaxemp.var(1,:));
lbls{1,3}='info. max. emp.';
pstyle=getbwlstyle(1);
fcovar.a(1,1)=addplot(fcovar.a(1,1), 'hp',hp,'pstyle',pstyle);
%dexp=diag(iidrand.covarexp);
%dexp=mean(dexp(2:end));

%**********************************************
%Info. Max
%Predicted
%Parallel to theta
%*********************************************
%plot the asymptotic variance

%cpred=imax.)*(1./(imax.trials(2:end)));

hp=plot(1:getniter(simmax),(1:getniter(simmax)).^-1*imax.asymevals(1));
lbls{1,4}='info. max. pred.';
pstyle=getbwlstyle(3);
fcovar.a(1,1)=addplot(fcovar.a(1,1), 'hp',hp,'pstyle',pstyle);

set(gca,'yscale','log');
set(gca,'xscale','log');

%****************************************************************
%orthogonal to mean
fcovar.a=setfocus(fcovar.a,2,1);
focvar.a(2,1)=title(fcovar.a(2,1),'orthogonal');
fcovar.a(2,1)=xlabel(fcovar.a(2,1),'trial');
fcovar.a(2,1)=ylabel(fcovar.a(2,1),'variance');
hold on;


%****************************************
%***********************************************
%I.I.D
%empirical
%orthogonal to theta
%**************************************************
%plot the i.i.d asymptotic variance
%compute the geometric mean
vexp=sum(log(irandemp.var(2:end,:)),1);
%vexp=vexp.^(1/(iidrand.thetalength-1));
vexp=(vexp/(size(irandemp.var,1)-1));
vexp=exp(vexp);
hp=plot(irandemp.trials,vexp,getptype(pind,2));

pstyle=getbwlstyle(2);
fcovar.a(2,1)=addplot(fcovar.a(2,1), 'hp',hp,'pstyle',pstyle);


%***********************************************
%I.I.D
%Predicted
%orthgonal to theta
%**************************************************

hp=plot(1:getniter(simrand),(1:getniter(simrand)).^-1*irand.asymevals(2));
pstyle=getbwlstyle(4);
fcovar.a(2,1)=addplot(fcovar.a(2,1), 'hp',hp,'pstyle',pstyle);

%***********************************************
%Info. Max
%empirical
%orthogonal to theta
%**************************************************
%compute the geometric mean
vexp=sum(log(imaxemp.var(2:end,:)),1);
%vexp=vexp.^(1/(iidrand.thetalength-1));
vexp=(vexp/(size(imaxemp.var,1)-1));
vexp=exp(vexp);

hp=plot(imaxemp.trials,vexp);

pstyle=getbwlstyle(1);
fcovar.a(2,1)=addplot(fcovar.a(2,1), 'hp',hp,'pstyle',pstyle);

%******
%Info. Max
%Predicted
%orthogonal to theta
%*********************************************
%plot the asymptotic variance

hp=plot(1:getniter(simmax),(1:getniter(simmax)).^-1*imax.asymevals(2),getptype(pind,2));
pstyle=getbwlstyle(3);
fcovar.a(2,1)=addplot(fcovar.a(2,1), 'hp',hp,'pstyle',pstyle);


set(gca,'yscale','log');
set(gca,'xscale','log');

%**********************************************************
%Labels & Tick marks
%*******************************************************
if nrows==2
    set(getha(fcovar.a(1,1)),'xtick',[]);

    set(getha(fcovar.a(1,1)),'ytick',10.^[-10 -5 0])
    set(getha(fcovar.a(2,1)),'ytick',10.^[-10 -5 0]);



else
    set(getha(fcovar.a(1,1)),'ytick',10.^[-10 -5 0])
    set(getha(fcovar.a(2,1)),'ytick',[]);
end

fcovar=lblgraph(fcovar);

%siz sizesubplots(fcovar)
%leave extra space on the right for the legend
sizesubplots(fcovar,[],[],[],[0 0 .28 0]);



set(getha(fcovar.a(1,1)),'ylim',gparam.ylim);
set(getha(fcovar.a(2,1)),'ylim',gparam.ylim);

%create the legend
%create the legend
%hlgnd=LegendObj('hlines',gethp(fcovar.a(1,1)),'lbls',lbls(1,:));

hlgnd=legend(gethp(fcovar.a(1,1)),lbls(1,:));
%outfile
%exportfcovar(fcovar.hf,fullfile(RESULTSDIR,'asymvar.eps'),'bounds','tight','color','rgb');
foutfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/gaborasymvar.eps';
%saveas(gethf(fcovar),foutfile,'epsc2')
%exportfig(fcovar.hf,outfile,'bounds','tight','FontMode','Fixed','FontSize',fcovar.fontsize,'Width',gparam.width,'Height',gparam.height,'color','rgb')