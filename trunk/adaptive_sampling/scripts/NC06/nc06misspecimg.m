%03-16-2008
%   changed the fonts before realizing we no longer use this image in our
%   paper
%03-28-2007
%Look at the results for a misspecified model
%Since the estimate will only be accurate up to a scaling constant
%normalize the true parameter vector and the estimates in order to
%eliminate effects of the scaling factor
clear all
setpathvars;
%simfiles{1}=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_001.mat');

%simfiles{1}=fullfile(RESULTSDIR,'misspec','03_28','03_28_poisspowercanon_001.mat');
%simfiles{2}=fullfile(RESULTSDIR,'misspec','03_28','03_28_poisslogcanon_001.mat');
simfiles{1}=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisspowercanon_001.mat');
simfiles{2}=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisslogcanon_001.mat');


simvars={'simrand','simmax'};

gparam.ncols=length(simvars);
gparam.nrows=2;
gparam.maxiter=3000;    %last trial to show
                        %we need this to ensure we use the same scales for
                        %both i.i.d. and info. max. data
%an inline function which automatically computes the linear index
%of a subplot from its row and column
%subplot selects in row order so to use  sub2ind we transpose rows and
%columns
spind=@(row,col)(sub2ind([gparam.ncols gparam.nrows],col,row));


ferr=[];
for find=1:length(simfiles)
    data(1).simfile=simfiles{find};
    data(1).clim=[-.4 .4];
    data(1).xticks=[1 25 50];
    %data(1).yticks=[1000:1000:4000];
    data(1).yticks=10.^[1:3];
    dind=1;
    outbname='misspecimg';
    %labels for the graphs
    %glabels={'I.I.D.: misspecified', 'Info. Max.: misspecified'};
    glabels={'i.i.d.', 'info. max.'};
    nplots=length(simvars);



    ferr(find).hf=figure;
    ferr(find).name='Misspecified Images';
    ferr(find).a={};
    ferr(find).fontsize=12;
    ferr(find).width=3;
    ferr(find).height=3;

    hold on;
    colormap(gray);
    %**************************************
    %generate all the axes for the subplots
    for i=1:nplots
        ferr(find).hpost(i)=subplot(2,nplots,i);
    end
    for i=1:nplots
        ferr(find).htrue(i)=subplot(2,nplots,i+nplots);
    end
    %name for output file
    [pathstr,sfname,EXT,VERSN] = fileparts(data(dind).simfile);
    ferr(find).outfile=fullfile('~/svn_trunk/adaptive_sampling/writeup/NC06/',sprintf('%s_%s.eps',sfname,outbname));

    ferr(find).fname=ferr(find).outfile;

    %how many rows do we want to have in our image
    nrows=400;
    %we need to create a mapping from rows to trials
    %we want this mapping to follow a log scale.
    slope=log10(gparam.maxiter)/(nrows-1);
    trials=10.^(slope*((1:nrows)-1));
    %take ceiling so that trials is valid
    trials=floor(trials);

    for vind=1:length(simvars)
        [pdata simobj, mobj,sr]=loadsim('simfile',data(dind).simfile,'simvar',simvars{vind});


        %normalize the mean estimate
        mvecs=normmag(pdata.m);
        
        observer=getobserver(simobj);
        truevec=normmag(gettheta(observer));

       %trials will not correspond to actual trial numbers
        %we need to interpolate to get these values
        %interpmvecs=zeros(size(pdata.m,1),nrows);
        %for j=1:size(pdata.m,1)
        %   interpmvecs(j,:)=interp1(0:simobj.niter,mvecs(j,:),trials,'linear');
        %end

        %*********************************************
        %plot it
        %**********************************************
        rind=vind;
        %***********************************************
        %the data
        %********************************************
        %niter(vind)=simobj.niter;



        ferr(find).a{spind(1,rind)}.ha=subplot(ferr(find).hpost(rind));
       imagesc(mvecs(:,trials)', data(1).clim);
        %imagesc(interpmvecs', data(1).clim);
        %if its the last axes add a colorbar

        %set the position
        %position=[left bottom width height]
        %         left=(rind-1)*(gspace.wspace+gspace.wpost)+gspace.border(1);
        %         bottom=1-gspace.border(4)-gspace.hpost;
        %         set(ferr(find).hpost(rind),'position',[left bottom gspace.wpost gspace.hpost]);
        %         set(ferr(find).hpost(rind),'fontsize',ferr(find).axisfontsize);
        drawnow;
        %turn of tick marks
        set(ferr(find).hpost(rind),'xtick',[]);
        
       
            set(ferr(find).hpost(rind),'ytick',[]);
        
        ferr(find).a{spind(1,rind)}.title=glabels{vind};

        %**************************************
        %plot the true parameters below it
        %********************************
        ferr(find).a{spind(2,rind)}.ha=subplot(ferr(find).htrue(rind));
        %ferr(find).a{spind(1,1)}=glabels{vind};
        imagesc(truevec',   data(1).clim);

        set(ferr(find).htrue(rind),'fontsize',ferr(find).fontsize);
        %turn off xticks
        xlabel('i');
        set(ferr(find).htrue(rind),'ytick',[]);
        set(ferr(find).htrue(rind),'xtick',data(dind).xticks);
        clear('mvecs','truevec','mag');

    end
    drawnow;
    %***********************************
    %label axes
    aind=spind(1,1);
    ferr(find).a{aind}.ylabel='trial';
    subplot(gparam.nrows,gparam.ncols,aind);
     %adjust the tick labels
            ytlbls=10.^[1:floor(log10(gparam.maxiter))];
            ytick=floor(log10(ytlbls)/slope+1);
            set(ferr(find).a{1}.ha,'ytick',ytick);
            set(ferr(find).a{1}.ha,'yticklabel',ytlbls);
        
    %
    aind=spind(2,1);
    subplot(gparam.nrows,gparam.ncols,aind);
    ylabel('\theta');

    %add a color bar
    aind=spind(1,gparam.ncols);
    subplot(gparam.nrows,gparam.ncols,aind);
    ferr(find).hc=colorbar;

    lblgraph(ferr(find));
    %********************************************************************
    %position the subplots
    %***********************************************************************
    %compute information for spacing the subplots
    %***********************************************************************

    for j=1:length(ferr(find).a)
        ha(j)=ferr(find).a{j}.ha;
    end
    tinset=get(ha,'tightinset');
    tinset=cell2mat(tinset);
    tin=get(ferr(find).hc,'tightinset');

    gspace.vspace=.05;         %vertical spacing between graphs)
    gspace.wspace=.08;         %horizontal spacing

    border.left=max(tinset(:,1));
    border.bottom=max(tinset(:,2));
    border.top=max(tinset(:,4));
    border.right=max(tin(:,3));



    gspace.wcb=.03;             %width for colorbar
    gspace.wtocb=.02;          %distance from edge of random to colorbar

    gspace.htrue=.12;            %height for graph of true parameters
    gspace.hpost=(1-gspace.vspace-gspace.htrue-border.top-border.bottom);
    gspace.wpost=(1-border.left-gspace.wspace-gspace.wtocb-gspace.wcb-border.right)/gparam.ncols;


    %loop across top row
    %and set position
    for cind=1:2
        %subplot(gparam.nrows,gparam.ncols,spind(1,cind));
        pos=get(ferr(find).a{spind(1,cind)}.ha,'position');
        pos(1)=border.left+(cind-1)*(gspace.wspace+gspace.wpost);
        pos(2)=border.bottom+gspace.htrue+gspace.vspace;
        pos(3)=gspace.wpost;
        pos(4)=gspace.hpost;
        set(ferr(find).a{spind(1,cind)}.ha,'position',pos);
    end

    %bottom row;
    for cind=1:2
        %subplot(gparam.nrows,gparam.ncols,spind(2,cind));
        pos=get(ferr(find).a{spind(2,cind)}.ha,'position');
        pos(1)=border.left+(cind-1)*(gspace.wspace+gspace.wpost);
        pos(2)=border.bottom;
        pos(3)=gspace.wpost;
        pos(4)=gspace.htrue;
        set(ferr(find).a{spind(2,cind)}.ha,'position',pos);
    end

    %color bar
    %******************
    %position the colorbar
    pos=get(ferr(find).hc,'position');
    %tin=get(ferr(find).hc,'tightinset');
    % bottom=1-gspace.border(4)-gspace.hpost;
    %set(ferr(find).hc,'position',[1-tin(3)-cpos(3), bottom,  cpos(3) gspace.hpost]);
    pos(1)=border.left+(gparam.ncols-1)*gspace.wspace+gparam.ncols*gspace.wpost+gspace.wtocb;
    pos(2)=border.bottom+gspace.htrue+gspace.vspace;
    pos(3)=gspace.wcb;
    pos(4)=gspace.hpost;
    set(ferr(find).hc,'position',pos);


    %set(ferr(find).a{1}.ha,'yscale','log');


    %set(ferr(find).a{2}.ha,'yscale','log');
    %exportfig(ferr(find).hf,ferr(find).outfile,'bounds','tight','color','rgb');
%    saveas(ferr(find).hf,ferr(find).outfile,'epsc2')
end