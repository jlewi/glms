%4-29-07
%
%Make image plots of the stimulus and spike history coefficients
%create a structure which contains the relevant fields for different data
%sets we then select which element of the structure we want to select the
%data. if we want to use data in the current workspace set dind=0;
%
%Revisions
%   11-09-2007: use new object model

dind=0;

% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071109','powernonlin_001.mat');
% dsets(end).lbl=sprintf('i.i.d.');
% dsets(end).simvar='rand';


%cmap=colormap(gray);
colormap('default');
cmap=colormap;


% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071109','powernonlin_001.mat');
% %dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
% dsets(end).lbl=sprintf('info. max.');
% dsets(end).simvar='max';
% 
% 
% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071111','powernonlin_001.mat');
% dsets(end).lbl=sprintf('i.i.d.');
% dsets(end).simvar='rand';
% 
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071111','powernonlin_001.mat');
% %dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
% dsets(end).lbl=sprintf('info. max.');
% dsets(end).simvar='max';
% 
% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071210','powernonlin_004.mat');
% dsets(end).lbl=sprintf('i.i.d.');
% dsets(end).simvar='rand';
% 
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071210','powernonlin_004.mat');
% %dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
% dsets(end).lbl=sprintf('info. max.');
% dsets(end).simvar='max';
% 
% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071211','expnonlin_017.mat');
% dsets(end).lbl=sprintf('i.i.d.');
% dsets(end).simvar='rand';
% 
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071211','expnonlin_017.mat');
% dsets(end).lbl=sprintf('info. max.');
% dsets(end).simvar='max';

%colormap('default');
cmap=colormap;


clim.stim=[-1 1];
clim.shist=[-10^-5 -10^-6];
clim.shist=[-4 0];

%**************************************************************************
%Grapha Parameters
%**************************************************************************

fshist(1)=FigObj('name','Stimulus filter','width',3,'height',3);
fshist(1).a=AxesObj('nrows',2,'ncols',length(dsets));
fshist(2)=FigObj('name','Spike history filter','width',3,'height',3);
fshist(2).a=AxesObj('nrows',2,'ncols',length(dsets));
%gparam.maxiter=800;

%loop over the datasetsy
for dind=1:length(dsets)
    %load this simulation
    simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);
   %colormap('gray');
    %get the estimates
    niter=getniter(simobj);
    post=getpost(simobj,[1:getniter(simobj)]);
    mfull=getm(post,[1:getniter(simobj)]);
    mstim=mfull(1:getstimlen(getmobj(simobj)),:);
    mshist=mfull(getstimlen(getmobj(simobj))+1:end,:);

    %get true value
    theta=gettheta(getobserver(simobj));
    kfilt=theta(1:getstimlen(getmobj(simobj)));
    afilt=theta(getstimlen(getmobj(simobj))+1:end);

    %*************************************
    %stimulus coefficients
    setfocus(fshist(1).a,1,dind);
    set(gca,'ydir','reverse');
    %  colormap('gray');
    imagesc(1:length(kfilt),1:niter,mstim',clim.stim)%,cmap);
    set(gca,'ylim',[1 niter]);
    set(gca,'xlim',[1 length(kfilt)]);
    set(gca,'yscale','log');
    
    %true values
    setfocus(fshist(1).a,2,dind);
    imagesc(1:length(kfilt),1,kfilt',clim.stim)%,cmap);
     set(gca,'xlim',[1 length(kfilt)]);

    %*************************************
    %spike history coefficients
    setfocus(fshist(2).a,1,dind);
    set(gca,'ydir','reverse');
    imagesc(1:length(afilt),1:niter,mshist',clim.shist)%,cmap);
    set(gca,'ylim',[1 niter]);
    set(gca,'xlim',[1 length(afilt)]);
    set(gca,'yscale','log');
    %true values
    setfocus(fshist(2).a,2,dind);
    imagesc(1:length(afilt),1,afilt',clim.shist)%,cmap);
    set(gca,'xlim',[1 length(afilt)]);
end

%add colorbars
setfocus(fshist(1).a,1,length(dsets));
hc=colorbar;
fshist(1).a(1,length(dsets))=sethc(fshist(1).a(1,length(dsets)),hc);


setfocus(fshist(2).a,1,length(dsets));
hc=colorbar;
fshist(2).a(1,length(dsets))=sethc(fshist(2).a(1,length(dsets)),hc);

%**********************************************
%adjust labels
for pind=1:2
    fshist(pind).a(1,1)=ylabel(fshist(pind).a(1,1),'trial');
    set(fshist(pind).a(1,1),'ytickmode','auto');

    for dind=1:length(dsets)
        fshist(pind).a(1,dind)=title(fshist(pind).a(1,dind),dsets(dind).lbl);

        %set titles to latex interpreter
        tlbl=gettlbl(  fshist(pind).a(1,dind));
        set(tlbl.h,'interpreter','latex');

        %turn off ylabels on estimate
        setfocus(fshist(pind).a,1,dind);
        set(gca,'xtick',[]);

        %turn off ylabels on true
        setfocus(fshist(pind).a,2,dind);
        set(gca,'ytick',[]);

    end

    for dind=1:length(dsets)
        fshist(pind).a(2,dind)=xlabel(fshist(pind).a(2,dind),'i');
    end

    for dind=2:length(dsets)
        setfocus(fshist(pind).a,1,dind);
        set(gca,'ytick',[]);
    end
end


%call lblgraph first to adjust fontsizes
lblgraph(fshist(1));
lblgraph(fshist(2));
sizesubplots(fshist(1),[],.5*ones(1,length(dsets)),[.8 .2]);

sizesubplots(fshist(2),[],.5*ones(1,length(dsets)),[.8 .2]);

odata={'data sets',[{'label'},{'variable'},{'file'};{dsets.lbl}' {dsets.simvar}' {dsets.fname}'];fshist(1),'stimulus coefficients';fshist(2),'spike history coefficients'};
onenotetable(odata,seqfname('~/svn_trunk/notes/nc06shistimg.xml'));
return;
