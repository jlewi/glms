%4-24-2007
%For the plots of the complex cell we want to plot the singular values
%of the MAP estimate because the true Quadratic matrix is a rank 2 matrix
%so plotting the SVD should make it clear the MAP converges to a rank 2
%%
%matrix

%old data
% dsets(1).fname=fullfile(RESULTSDIR,'poolbased','04_29','04_29_poissexp_001.mat');
% dsets(1).simvar='simmax';
% dsets(1).lbl='info. max.';
% 
% dsets(2).fname=fullfile(RESULTSDIR,'poolbased','04_29','04_29_poissexp_001.mat');
% dsets(2).simvar='simrand';
% dsets(2).lbl='i.i.d.';

%*******************************************
%20d 1-d auditory data
dsets=[]


dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_005.mat');
dsets(end).lbl='i.i.d.';
dsets(end).simvar='simrand';


dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_005.mat');
dsets(end).lbl='info. max. i.i.d';
dsets(end).simvar='simunif';

dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_005.mat');
dsets(end).lbl='info. max. heuristic';
dsets(end).simvar='simmax';



for dind=1:length(dsets)
    fprintf('Dataset dind=%d \n',dind);
    fsv.a{dind}.ha=subplot(1,length(dsets),dind);
    
    %load the data and compute
    %the singular values
    [pdata, sim, mp, sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
    
    if ~isequal(mp.ninpfunc,@projinpquad)
        error('Script only works for quadratic nonlinearities');
    end
   
    thetalength=length(gettheta(sim.observer));
    dsets(dind).sv=zeros(thetalength.^.5,sim.niter);
    
    %on each trial we reshape the map to be a matrix and find its singualr
    %values
    for t=1:sim.niter
        if (mod(t,10)==0)
            fprintf('Trial %d \n', t);
        end
        map=reshape(pdata.m(:,t+1),thetalength^.5,thetalength^.5);
       dsets(dind).sv(:,t)= svd(map);
    end

end
%%


%********************************************************************
%make the plot
fsv=[];
fsv.hf=figure;
fsv.name='Complex Cell: SVD';
fsv.fontsize=12;

tmax=200;       %max trial
gparam.ncols=length(dsets);
gparam.width=3;
gparam.height=3;

colormap(gray);
clim=zeros(length(dsets),2);    %to ensure color limits are the same
ha=zeros(1,length(dsets));
for dind=1:length(dsets)
   fsv.a{dind}.ha=subplot(1,gparam.ncols,dind);
   fsv.a{dind}.hi=imagesc(log10(dsets(dind).sv(:,1:tmax))');
   
   fsv.a{dind}.title=dsets(dind).lbl;
   fsv.a{dind}.xlabel='i';   
   clim(dind,:)=get(fsv.a{dind}.ha,'clim');
   ha(dind)=fsv.a{dind}.ha;
end

%add a colorbar
hc=colorbar;

%******************************
%make the color limits the same
clim=[min(clim(:,1)),max(clim(:,2))];

for dind=1:length(dsets)
   set(fsv.a{dind}.ha,'clim',clim); 
end


%turn off the yticks
for dind=2:length(dsets)
   set(fsv.a{dind}.ha,'ytick',[]);
end

fsv.a{1}.ylabel='trial';

fsv=lblgraph(fsv);

%***********************************************************************
%adjust the size and spacing of the figures
space.hcwidth=.05;      %widht of colorbar
space.hspace=.05;       %horizontal spacing
setfsize(fsv.hf,gparam);
tinset=get(ha,'tightinset');
tinset=cell2mat(tinset);
thc=get(hc,'tightinset');
border.left=tinset(1,1);
border.right=thc(3);
border.top=max(tinset(:,end));
border.bottom=max(tinset(:,2));

awidth=(1-border.left-border.right-space.hcwidth-(gparam.ncols)*space.hspace)/gparam.ncols;
aheight=1-border.top-border.bottom;

for dind=1:gparam.ncols
   pos=get(fsv.a{dind}.ha,'position');
   pos(1)=border.left+(space.hspace+awidth)*(dind-1);
   pos(2)=border.bottom;
   pos(3)=awidth;
   pos(4)=aheight;
   set(fsv.a{dind}.ha,'position',pos);
end

pos=get(hc,'position');
pos(1)=border.left+(space.hspace+awidth)*(gparam.ncols);
pos(2)=border.bottom;
pos(3)=space.hcwidth;
pos(4)=aheight;
set(hc,'position',pos);

outfile='~/cvs_ece/writeup/NC06/complexcellsvd.eps';
%exportfig(fsv.hf,outfile,'bounds','tight','FontMode','Fixed','FontSize',fsv.fontsize,'Width',gparam.width,'Height',gparam.height)
