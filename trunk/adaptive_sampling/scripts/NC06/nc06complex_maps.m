%03-28-2007
%Look at the MAPS for the complex cell
%Since the estimate will only be accurate up to a scaling constant
%normalize the true parameter vector and the estimates in order to
%eliminate effects of the scaling factor
clear all
setpathvars;

dsets(1).fname=fullfile(RESULTSDIR,'poolbased','04_29','04_29_poissexp_001.mat');
dsets(1).simvar='simmax';
dsets(1).lbl='info. max.';

dsets(2).fname=fullfile(RESULTSDIR,'poolbased','04_29','04_29_poissexp_001.mat');
dsets(2).simvar='simrand';
dsets(2).lbl='i.i.d';




%***********************************************************************
%compute information for spacing the subplots
%***********************************************************************
% space.hspace=.05;         %vertical spacing between graphs)
% space.wspace=.08;         %horizontal spacing
% space.htrue=.12;            %height for graph of true parameters
% gspace.wcb=.05;             %width for colorbar
% gspace.wtocb=.01;          %distance from edge of random to colorbar
%
% gspace.hpost=(1-gspace.hspace-gspace.htrue-gspace.border(2)-gspace.border(4));
% gspace.wpost=(1-(nplots-1)*gspace.wspace-gspace.border(1)-gspace.border(3))/nplots;
gparam.ncols=length(dsets);
gparam.nrows=2;
gparam.width=3;
gparam.height=3;
tmax=200;               %max trial to display
fmaps=[];
fmaps.hf=figure;
fmaps.a={};
fmaps.fontsize=12;
yticks=[50:50:tmax];
xticks=[50 100];

hold on;
colormap(gray);
%an inline function which automatically computes the linear index
%of a subplot from its row and column
%subplot selects in row order so to use  sub2ind we transpose rows and
%columns
spind=@(row,col)(sub2ind([gparam.ncols gparam.nrows],col,row));

for dind=1:length(dsets);
    [pdata simobj, mobj,sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);

    %name for output file
    [pathstr,sfname,EXT,VERSN] = fileparts(dsets(dind).fname);
    %fmaps.fname=fullfile(pathstr,sprintf('%s_%s.eps',sfname,outbname));


    %*************************************
    %Plot: infomax
    %***************************************
    aind=spind(1,dind);
    fmaps.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);

    fmaps.a{aind}.hp=imagesc(pdata.m(:,1:tmax)');
    fmaps.a{aind}.title=dsets(dind).lbl;
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
    %*****************************************
    %Plot: True below infomax
    %****************************************
    aind=spind(2,dind);
    fmaps.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);
    imagesc(gettheta(simobj.observer)');
    fmaps.a{aind}.xlabel='i';
    set(gca,'ytick',[]);
    set(gca,'xtick',xticks);
end

%****************************************
%add y label
aind=spind(1,1);
fmaps.a{aind}.ylabel='trial';
set(fmaps.a{aind}.ha,'ytick',yticks);

%*******************************
%add colorbar
aind=spind(1,length(dsets));
subplot(gparam.nrows,gparam.ncols,aind);
hc=colorbar;

lblgraph(fmaps);
%**************************************************************************
%Position the figure
%**************************************************************************
%space a structure to describe spacing settings
%this should be expressed as %
space.hspace=.025; %horizontal spacing
space.vspace=.05; %vertical spacing
space.cbwidth=.025;  %width of colorbar
space.htrue=.1;      %height of true plots

%set the height and width of the figure
setfsize(fmaps.hf,gparam);


%*********************************************************************
%determine how much of a border we need to leave on left bottom right top
%so that we don't cut off figure labels
ha=length(fmaps.a);
for i=1:length(fmaps.a)
    ha(i)=fmaps.a{i}.ha;
end
ha=[ha hc];

%ha has handles to all the axes
tinset=get(ha,'tightinset');
tinset=cell2mat(tinset);

border.left=max(tinset(:,1));
border.bottom=max(tinset(:,2));
border.right=max(tinset(:,3));
border.top=max(tinset(:,4));

%determine the size for each of the axes
asize.width=(1-border.left-border.right-space.hspace*gparam.ncols-space.cbwidth)/(gparam.ncols);
asize.height=(1-border.top-border.bottom-space.vspace-space.htrue);

%loop through the axes and poisition them
row=1;
for col=1:gparam.ncols
    aind=spind(row,col);
    pos=get(fmaps.a{aind}.ha,'position');
    pos(1)=border.left+(col-1)*(asize.width+space.hspace);
    pos(2)=border.bottom+space.htrue+space.vspace;
    pos(3)=asize.width;
    pos(4)=asize.height;
    set(fmaps.a{aind}.ha,'position',pos);
end

row=2;
for col=1:gparam.ncols
    aind=spind(row,col);
    pos=get(fmaps.a{aind}.ha,'position');
    pos(1)=border.left+(col-1)*(asize.width+space.hspace);
    pos(2)=border.bottom;
    pos(3)=asize.width;
    pos(4)=space.htrue;
    set(fmaps.a{aind}.ha,'position',pos);
end

%size the color bars
for j=1:length(hc)
    pos=get(hc(j),'position');
    pos(1)=border.left+(gparam.ncols)*(asize.width+space.hspace);
    pos(2)=border.bottom+space.htrue+space.vspace;
    pos(3)=space.cbwidth;
    pos(4)=asize.height;
    set(hc(j),'position',pos);
end

fmaps.outfile='~/cvs_ece/writeup/NC06/complexcell_maps.eps';
exportfig(fmaps.hf,fmaps.outfile,'bounds','tight','FontMode','Fixed','FontSize',fmaps.fontsize,'Width',gparam.width,'Height',gparam.height)
