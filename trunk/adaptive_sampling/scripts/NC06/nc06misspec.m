%3_28_07
%Look at the results for a misspecified model
%Since the estimate will only be accurate up to a scaling constant plot the
%angle between the true parameter and the estimated parameter
clear all
setpaths
%simfiles{1}=fullfile(RESULTSDIR,'nips','11_17','11_17_misspecified_001.mat');


%simfiles{1}=fullfile(RESULTSDIR,'misspec','03_28','03_28_poisspowercanon_001.mat');
%simfiles{2}=fullfile(RESULTSDIR,'misspec','03_28','03_28_poisslogcanon_001.mat');
simfiles{1}=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisspowercanon_001.mat');
simfiles{2}=fullfile(RESULTSDIR,'misspec','05_15','05_15_poisslogcanon_001.mat');

for find=1:length(simfiles)
outbname='misspec';
simvars={'simrand','simmax'};
%labels for the graphs
%glabels={'I.I.D.: misspecified', 'Info. Max.: misspecified'};
glabels={'i.i.d.', 'info. max.'};
%compute the angle
results=[];
rind=0;

% lstyle(1).lw=3;
% lstyle(1).lcolor=[0 0 0];
% lstyle(1).lcolor=[1 0 0];
% lstyle(1).lstyle='-'
% 
% lstyle(2).lw=2;
% lstyle(2).lcolor=[.75 .75 .75];
% lstyle(2).lcolor=[0 0 1];
% lstyle(2).lstyle='-';
% 
% 
% lstyle(3).lw=3;
% lstyle(3).lcolor=[0 0 0];
% lstyle(3).lcolor=lstyle(1).lcolor;
% lstyle(3).lstyle='-.';
% 
% 
% lstyle(4).lw=2;
% lstyle(4).lcolor=[.75 .75 .75];
% lstyle(4).lcolor=lstyle(2).lcolor;
% lstyle(4).lstyle='-.';


for vind=1:length(simvars)
  [pdata simobj, mobj,sr]=loadsim('simfile',simfiles{find},'simvar',simvars{vind});
 
   %name for output file
    [pathstr,sfname,EXT,VERSN] = fileparts(simfiles{find});
%    outfile=seqfname(fullfile(pathstr,sprintf('%s_%s.eps',outbname,simparam.trialindex)));
   % outfile=fullfile(pathstr,sprintf('%s_%s.eps',sfname,outbname));
    outfile=fullfile('~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/',sprintf('%s_%s.eps',sfname,outbname));

        rind=rind+1;

        

        %normalize the mean estimate
        mvecs=normmag(pdata.m);
        observer=getobserver(simobj);
        truevec=normmag(gettheta(observer));

        results(rind).angle=acos(truevec'*mvecs)*180/pi;
        results(rind).lbl=glabels{vind}
        
      
end


ferr=[];
ferr.fname=outfile;
ferr.hf=figure;
ferr.xlabel='Trials';
ferr.ylabel='Angle (degrees)';
ferr.hp=[];
ferr.lbls={};
ferr.markersize=10;
ferr.fontsize=12;
ferr.width=3;
ferr.height=3;
%setfsize(ferr.hf,gparam);

%ferr.linewidth=2;
hold on;

for rind=1:length(results)
    %plot(ksamps,dk);
    %[c,m]=getptype(1,1)
%    ferr.hp(rind)=plot(0:(length(results(rind).angle)-1),results(rind).angle,getptype(rind,1));
    ferr.hl(rind)=plot(0:(length(results(rind).angle)-1),results(rind).angle,getptype(rind,2));
%      set(ferr.hl(rind),'Color', lstyle(rind).lcolor);
%   %  set(fkl.hp(mind),'Marker', mark);
%     set(ferr.hl(rind),'LineStyle',lstyle(rind).lstyle);
%     %set(fkl.hp(mind),'MarkerSize',fkl.markersize);
%     set(ferr.hl(rind),'LineWidth',lstyle(rind).lw);
 
ferr.lbls{rind}=results(rind).lbl;
    lstyle=getbwlstyle(rind);
    setlstyle(ferr.hl(rind),lstyle);
end

%ferr.hlgn=legend(ferr.hp,{results.lbl});
ferr=lblgraph(ferr);
set(ferr.hlgn,'Location','southwest')
xlim([0 (length(results(rind).angle)-1)]);
set(gca,'xscale','log');
set(gca,'xtick',10.^[0:3]);
%exportfig(ferr.hf,outfile,'bounds','tight','color','rgb');
saveas(ferr.hf,outfile,'epsc2');
end