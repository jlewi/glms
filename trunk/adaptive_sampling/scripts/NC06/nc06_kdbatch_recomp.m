%04_19_2008
%
%Compute the KL distance between the batch and online approximations of the
%kl distance.
% fname=fullfile(RESULTSDIR,'poissexp','04_19','poissexp_04_19_001.mat');
 % fname=fullfile(RESULTSDIR,'poissexp','04_19','poissexp_04_19_002.mat');
  fname=fullfile(RESULTSDIR,'poissexp','04_23','poissexp_04_23_001.mat');


[ponline simonline mparam sr]=loadsim('simfile',fname,'simvar','simmax');
return

%restimate the mean and covariance using all observations
bupdater=BatchML();
%%

post.m=ponline.m(:,1);
post.c{1}=ponline.c{1};
pbatch.m=zeros(mparam.klength,simonline.niter+1);
pbatch.c=cell(1,simonline.niter+1);
pbatch.c{1}=ponline.c{1};
for t=[100 1000 10000 50000 100000]
    if (mod(t,10)==0)
        fprintf('trial %d \n',t);
    end
    [post,uinfo,einfo]=update(bupdater,post,sr.y(:,1:t),mparam,sr.nspikes(1:t),[]);
    pbatch.m(:,t+1)=post.m;
    pbatch.c{t+1}=post.c;
end
%%
%This code is if we have already created pbatch and just need to recompute
%some trials
[ponline simonline mparam sr]=loadsim('simfile',fname,'simvar','simmax');
[pathstr,name] =fileparts(fname);
fbatch=fullfile(pathstr,sprintf('%s_batch.mat',name));
[pbatch simbatch mpbatch srbatch]=loadsim('simfile',fbatch,'simvar','simbatch');

bupdater=BatchML();
%copy m and c
m=zeros(size(ponline.m));
c=cell(1,length(ponline.c));
m(:,1:size(pbatch.m,2))=pbatch.m;
for j=1:length(pbatch.c)
    c{j}=pbatch.c{j};
end
pbatch.m=m;
pbatch.c=c;

post=[];
post.m=ponline.m(:,1);
post.c{1}=ponline.c{1};
for t=1:100
    if (mod(t,10)==0)
        fprintf('trial %d \n',t);
    end
    if ~isempty(pbatch.c{t+1})
        fprintf('Skipping trial %d already exists \n',t);
    else
    [post,uinfo,einfo]=update(bupdater,post,sr.y(:,1:t),mparam,sr.nspikes(1:t),[]);
    pbatch.m(:,t+1)=post.m;
    pbatch.c{t+1}=post.c;
    end
end

%save batch to file
simbatch=simonline;
simbatch.simid='batch';
simbatch=changesim(simbatch,'updater',bupdater);


savesim('simfile',fbatch,'pdata',pbatch,'sr',sr,'simparam',simbatch,'mparam',mparam);

%%
%Compute kl distance between the 2 gaussian approximations
[ponline simonline mparam sr]=loadsim('simfile',fname,'simvar','simmax');
[pathstr,name] =fileparts(fname);
fbatch=fullfile(pathstr,sprintf('%s_batch.mat',name));
[pbatch simbatch mpbatch srbatch]=loadsim('simfile',fbatch,'simvar','simbatch');

trials=10000:10000:80000;
klg=zeros(1,length(trials));
for tind=1:length(trials)
    trial=trials(tind);
po.m=ponline.m(:,trial+1);
po.c=ponline.c{trial+1};
pb.m=pbatch.m(:,trial+1);
pb.c=pbatch.c{trial+1};
klg(tind)=kldistgauss(po,pb);
end