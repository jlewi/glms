%7-16-2007
%   Plot it on a log scale. We do this manually because we compute the
%   filters on a log increment
%5-30-2007
%
%Make an image of the best recovered auditory filter on each trial
%To do this we reshape the mean as a matrix and get the first two principal
%eigen vectors. We then project the true filters onto these vectors
%This represents our MSE guess of the best filters
%******************************************

%60 d gammatone
% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_simmax.mat');
% dsets(end).lbl='info. max. heuristic';
% dsets(end).simvar='simmax';
% %dsets(end).maxtrial=10000; %max trial to plot
%
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001.mat');
% dsets(end).lbl='info. max. i.i.d.';
% dsets(end).simvar='simunif';
% %dsets(end).maxtrial=10000;
%
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_simrand.mat');
% dsets(end).lbl='i.i.d.';
% dsets(end).simvar='simrand';

% dsets=[];
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','071030','audnonlin_001.mat');
% dsets(end).lbl='info. max.';
% dsets(end).simvar='max';
% %dsets(end).maxtrial=10000; %max trial to plot
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','071029','audnonlin_001.mat');
% dsets(end).lbl='tones';
% dsets(end).simvar='tones';

%dsets(end).maxtrial=10000; %max trial to plot

dsets=[];
ind=16;
 dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
 dsets(end).lbl=sprintf('$\\hat{\\mathcal{X}}_{heur,t+1}$') ;
 dsets(end).simvar='max';
dsets(end).maxtrial=10000; %max trial to plot

ind=ind+1;
%  dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
%  dsets(end).lbl='max: heur muorig';
%  dsets(end).simvar='max';
% dsets(end).maxtrial=10000; %max trial to plot

ind=ind+1;
 dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
 dsets(end).lbl='tones';
 dsets(end).simvar='tones';
dsets(end).maxtrial=10000; %max trial to plot


ind=ind+1;
 dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
 dsets(end).lbl=sprintf('$\\hat{\\mathcal{X}}_{iid,t+1}$') ;
 dsets(end).simvar='unif';
dsets(end).maxtrial=10000; %max trial to plot

ind=ind+1;
 dsets(end+1).fname=fullfile(RESULTSDIR,'inpnonlin','071212',sprintf('audnonlin_%03.3g.mat',ind));
 dsets(end).lbl='i.i.d.';
 dsets(end).simvar='rand';
dsets(end).maxtrial=10000; %max trial to plot


gparam.subsample=100;


%********************************************************************
%compute the subspace angle
for dind=1:length(dsets)
    %[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
    simdata=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);
  
    
    extra=getextra(simdata);
    mparam=extra.mparam;
    kl=mparam.kl;
    k1=mparam.k1';
    k2=mparam.k2';
    
    ktrue=[k1 k2];
    if (size(k1,2)>1)
        k1=k1';
    end
    if (size(k2,2)>1)
        k2=k2';
    end

    dsets(dind).niter=getniter(simdata);
    dsets(dind).trials=1:gparam.subsample:getniter(simdata);

    ninpobj=getninpobj(getmobj(simdata));
    for ind=1:length(dsets(dind).trials);
        trial=dsets(dind).trials(ind);
        [evecs, eigval]=svd(qmatquad(ninpobj,getpostm(simdata,trial)));
        %project the truth onto each eigenvector
        kproj=evecs(:,1:2)'*[k1 k2];
        dsets(dind).k1est(:,ind)=evecs(:,1:2)*kproj(:,1);
        dsets(dind).k2est(:,ind)=evecs(:,1:2)*kproj(:,2);
    end

    %compute the mse
    dsets(dind).k1mse=dsets(dind).k1est-k1*ones(1,length(dsets(dind).trials));
    dsets(dind).k1mse=sum(dsets(dind).k1mse.^2,1).^.5;

    dsets(dind).k2mse=dsets(dind).k2est-k2*ones(1,length(dsets(dind).trials));
    dsets(dind).k2mse=sum(dsets(dind).k2mse.^2,1).^.5;
end

%%
%plotit
clear fmse;
for kind=1:2
    %for onenote use width and height of 5
    %for paper use 3 for width and height
    fmse(kind)=FigObj('width',3,'height',3,'name',sprintf('MSE Filter %d',kind));
    fmse(kind).a=AxesObj('xlabel','trial','ylabel','M.S.E.','title',sprintf('$\\vec{\\phi}^%d$',kind));
    tlbl=gettitle(fmse(kind).a);
    set(tlbl.h,'interpreter','latex');
    hold on;
    for dind=1:length(dsets)
        if kind==1
            hp=plot(dsets(dind).trials,dsets(dind).k1mse);
        else
            hp=plot(dsets(dind).trials,dsets(dind).k2mse);
        end
        fmse(kind).a=addplot(fmse(kind).a,'hp',hp,'lbl',dsets(dind).lbl,'lstyle',getbwlstyle(dind));
    end
    fmse(kind)=lblgraph(fmse(kind))
    set(gca,'xscale','log');
    
    %make the legend latex
    hlgnd=gethlgnd(fmse(kind).a);
    set(hlgnd,'interpreter','latex');
   
%    set(gca,'xtick',10.^[1:floor(log10(dsets(dind).trials(end)))]);
    set(gca,'xtick',10.^[1:4]);
    %remove the lgnd
    delete(hlgnd);
    
    %add a little extra space to the figure
    fmse(kind)=sizesubplots(fmse(kind),[],[],[],[0 0 0 .05]);
end

%add a custom legend to the second figure

hlgn=LegendObj('hlines',gethp(fmse(2).a),'lbls',{dsets.lbl});
hlgn=setlatex(hlgn);
hlgn=settleft(hlgn,.45);
hlgn=setposition(hlgn,.18 ,.2 ,.39 ,.35);
hlgn=setorder(hlgn,[4 2 3 1]);

fk1='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/auditory_mse_k1.eps';
fk2='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/auditory_mse_k2.eps';


odata={fmse(1),[{'label'},{'variable'},{'file'};{dsets.lbl}' {dsets.simvar}' {dsets.fname}'];fmse(2),''};
onenotetable(odata,seqfname('~/svn_trunk/notes/nc06audfilts.xml'));
%saveas(gethf(fmse(1)),fk1,'epsc2');
%saveas(gethf(fmse(2)),fk2,'epsc2');
%exportfig(ffilt.hf,ffilt.outfile,'bounds','tight','FontMode','Fixed','FontSize',ffilt.fontsize,'Width',gparam.width,'Height',gparam.height)
%saveas(ffilt.hf,ffilt.outfile,'epsc2');