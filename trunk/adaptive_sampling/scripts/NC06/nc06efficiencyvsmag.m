%Explanation: This script plots the efficiency measured as the ratio of the
%asymptotic variance for the iid to infomax design as a function of the
%dimensionality.

clear all;
setpathvars;


%the asymptotic variances depend on magnitude of theta and magnitude of
%stimuli
thetamag=1;
magcon=1;

%dimensionality to compute ratios for. 
dims=100;
mind=1;
mags=[.1:.25:1 2.5:2.5:10];

%compute the efficiency
for mind=1:length(mags)
   %compute asymptotic covariance for infomax
   [imax(mind)]=compimaxasymvar(dims,thetamag, mags(mind));
   %compute iid asymptotic variance   
   [iidvar(mind)]=compiidasymvar(dims,thetamag, mags(mind))
   
   %compute the ratios
   %%*************************************************************************
    %compute the ratio of the variance in the iid and infomax case
    r.parallel(mind)=iidvar(mind).asymevals(1)/imax(mind).asymevals(1);
    r.orthog(mind)=iidvar(mind).asymevals(2)/imax(mind).asymevals(2);
    
    %compute the ratio of the determinants
    r.det(mind)=sum(log10(iidvar(mind).asymevals))-sum(log10(imax(mind).asymevals));
end

%******************************************************************
%%
%plot it
clear lstyle

lstyle(1).color=[0 0 0];
lstyle(1).linestyle='-';
lstyle(1).linewidth=3;
lstyle(1).marker='.';


lstyle(2).color=[0 0 0];
lstyle(2).linestyle='--';
lstyle(2).linewidth=3;
lstyle(2).marker='none';

feff=FigObj('name','efficiency','width',3,'height',3);
feff.a=AxesObj('xlabel','$m$','ylabel','Ratio');
hold on;
%parallel
hp=plot(mags,r.parallel,'k.')
feff.a=addplot(feff.a,'hp',hp,'pstyle',lstyle(1),'lbl','parallel');

%fit a line
% [pcoeff]=polyfit(dims,r.parallel,1)
% x=[0 dims(end)];
% y=pcoeff(1)*x+pcoeff(2);
% plot(x,y,'k-');

%orthogonal
hp=plot(mags,r.orthog)
feff.a=addplot(feff.a,'hp',hp,'pstyle',lstyle(2),'lbl','orthogonal');

feff=lblgraph(feff)
hx=getxlabel(feff.a)
set(hx.h,'interpreter','latex')
feff=sizesubplots(feff,[],[],[],[0 .05, 0 0]);

%saveas(gcf,'~/svn_trunk/adaptive_sampling/writeup/NC06/asymefficiencyvsmag.eps','epsc2');


% fdet=FigObj('name','Ratio of determinants','width',3,'height',3);
% fdet.a=AxesObj('xlabel','$\dim(\vec{\theta})$','ylabel','log(Ratio)');
% hp=plot(dims,r.det,'k.')