% 03-16-2008
%   updated to new object model so I can redo plot for NC06
% 4-30-2007
%
% Make a plot of the MSE between the empirical variance and the asymptotic covariance
%   matrices
clear all;
setpathvars;



%%
%****************************************************************
%parameters
gparam.subsamp=1;       %subsampling for computing the empirical variances
gparam.ylim=[10^-14 10];
%%

%we can either load the data from the gabor_04_24_001.mat
%and recompute the covariance matrices because they were not saved
%or else we can load the data from fmax and frand which are files in which
%I computed and saved the covariance matrices
fname=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001.mat');
fmax=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001_max_withc.mat');
frand=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001_rand_withc.mat');

if (true)
    simmax=SimulationBase('fname',fmax,'simvar','max');
    simrand=SimulationBase('fname',frand,'simvar','rand');
else

    %[pmax, simmax, mparammax,srmax]=loadsim('simfile',fname,'simvar','simmax');
    %[prand, simrand, mparamrand,srrand]=loadsim('simfile',fname,'simvar','simrand');
    simmax=SimulationBase('fname',fname,'simvar','max');
    simrand=SimulationBase('fname',fname,'simvar','rand');
    %fill in low memory
    %[pmax]=fillinlowmem(fname,'simmax',[]);
    %[prand]=fillinlowmem(fname,'simrand',[]);
    %[pmax]=fillinlowmem(fname,'simmax',[]);
    updater=getupdater(simmax);
    %[simmax]=fillinlowmem(updater, pmax,mparammax,srmax,[]);
    [simmax]=fillinlowmem(updater,simmax,[]);
    updater=getupdater(simrand);
    %[simrand]=fillinlowmem(updater,prand,mparamrand,srrand,[]);
    [simrand]=fillinlowmem(updater,simrand,[]);
end

%compute the asymptotic variance
%%
%[imax,imaxemp,irand,irandemp]=compasymvar('pmax',pmax,'simparammax',simmax,'mparammax',mparammax,'srmax',srmax,...
%       'prand',prand,'simparamrand',simrand,'mparamrand',mparamrand,'srrand',srrand,...
%       'subsamp',gparam.subsamp);

[imax,imaxemp,irand,irandemp]=compasymvar('simmax',simmax,'simrand',simrand);

%%
%compute the M.S.E
imaxemp.mse=zeros(1,getniter(simmax));
for t=1:getniter(simmax)
    me=(t*getpostc(simmax,t)-imax.covarexp).^2;
    imaxemp.mse(t)=sum(me(:)).^.5;
end
irandemp.mse=zeros(1,getniter(simrand));
for t=1:getniter(simrand)
    me=(t*getpostc(simrand,t)-irand.covarexp).^2;
    irandemp.mse(t)=sum(me(:)).^.5;
end

%param.file=fullfile(RESULTSDIR, 'tracking','08_24','08_24_maxtrack_001.mat');
%%
clear('fcovar');
fcovar.hf=figure;
fcovar.title='';
fcovar.name='Asymptotic Covariance MSE';
fcovar.fname='dircovar';
fcovar.fontsize=12;
fcovar.width=2.25;         %width of image
focvar.height=2.25;
%setfsize(fcovar,gparam);
hold on;
nrows=1;
ncols=2;
%**************************************************************************
%two panels
%1. panel components parallel to mean
%2. other panel orthogonal to mean
fcovar.a{1}.ha=gca;
hold on;
%***********************************************
%parallel to mean
fcovar.a{1}.hp=[];
fcovar.a{1}.lbls={};
fcovar.a{1}.ylabel='M.S.E';
fcovar.a{1}.xlabel='trial';
pind=0;


hold on;

pind=1;
fcovar.a{1}.hp(pind)=plot(1:getniter(simrand),irandemp.mse);
fcovar.a{1}.lbls{pind}='i.i.d.';
setlstyle(fcovar.a{1}.hp(pind),getbwlstyle(2));

pind=pind+1;
%3-17-2008
%For the infomax data we only plot the mse on every 1000th trial
%for trials 1000 and greater
fcovar.a{1}.hp(pind)=plot([1:1000 2010:1000:getniter(simmax)],imaxemp.mse([1:1000 2010:1000:getniter(simmax)]));
%fcovar.a{1}.hp(pind)=plot(1:getniter(simmax),imaxemp.mse);
setlstyle(fcovar.a{1}.hp(pind),getbwlstyle(1));
fcovar.a{1}.lbls{pind}='info. max';



lblgraph(fcovar);
set(gca,'yscale','log');
xlim([0 max([getniter(simmax),getniter(simrand)])]);


setfsize(gcf,fcovar);

foutmse='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/gabor_asymmse.eps';
%use savefig because we have tick marks in base 10
%saveas(fcovar.hf,foutmse,'epsc2');


