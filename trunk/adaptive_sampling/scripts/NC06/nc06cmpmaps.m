%4-22-2007
%   make a plot as an image of the maps on each trial of the MAP estimated
%   computed using the online vs. batch udpates
%

dsets=[];
%25-d results
% dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_19','poissexp_04_19_001.mat');
% dsets(end).lbl='online';
% dsets(end).simvar='simmax';
% 
% dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_19','poissexp_04_19_001_batch.mat');
% dsets(end).lbl='batch';
% dsets(end).simvar='simbatch';

%5-d results04_23_001
dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_23','poissexp_04_23_001.mat');
dsets(end).lbl='online';
dsets(end).simvar='simmax';

dsets(end+1).fname=fullfile(RESULTSDIR,'poissexp','04_23','poissexp_04_23_001.mat');
dsets(end).lbl='batch';
dsets(end).simvar='simbatch';
%***************************************************
%parameters for the graph
%***************************************************
gparam.trials=[1:10];    %trials on which to plot the means
gparam.clim=[-1 1];       %values we scale color limits to
gparam.xtick=[1 5];
gparam.ytick=[1 25:25:100];

fmean=[];

fmean.hf=figure;
fmean.fontsize=12;
fmean.a={};
fmean.hp=[];
fmean.name='MAP';
fmean.title='MAP';

hold on;

%how columns and rows of axes we will need
gparam.ncols=length(dsets);
gparam.nrows=2;
gparam.fontsize=12;
gparam.width=3;          %width of the image in inches
gparam.height=3;            %height of the image in inches
colormap(gray);

%an inline function which automatically computes the linear index 
%of a subplot from its row and column
%subplot selects in row order so to use  sub2ind we transpose rows and
%columns
spind=@(row,col)(sub2ind([gparam.ncols gparam.nrows],col,row));


%************************************************************************
%loop over the data sets
for dind=1:length(dsets)
    [pdata, sim, mp, sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
        %convert indexes to a linear index
        aind=spind(1,dind);
        fmean.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);       
         imagesc(pdata.m(:,gparam.trials)',gparam.clim);
        fmean.a{aind}.title=dsets(dind).lbl;
        %turn off tick marks
        set(fmean.a{aind}.ha,'xtick',[]);
        set(fmean.a{aind}.ha,'ytick',[]);
   
    %**********************************************
     %plot the true underneath
     aind=spind(2,dind);     
    fmean.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind); 
      imagesc(gettheta(sim.observer)',gparam.clim);

    fmean.a{aind}.xlabel='i';
    set(gca,'ytick',[]);
    set(gca,'xtick',gparam.xtick)
end %end loop over datasets


%*************************************************************
%add tick marks and labels
aind=spind(1,1);
fmean.a{aind}.ylabel='trial';
set(fmean.a{aind}.ha,'ytick',gparam.ytick);


%add a color bar to the last figure
aind=spind(1,2);
subplot(gparam.nrows,gparam.ncols,aind);
hc=colorbar;

lblgraph(fmean);



%%
%**************************************************************************
%Position the figure
%**************************************************************************
%space a structure to describe spacing settings
%this should be expressed as %
space.hspace=.025; %horizontal spacing
space.vspace=.05; %vertical spacing
space.cbwidth=.025;  %width of colorbar
space.htrue=.1;      %height of true plots

%set the height and width of the figure
setfsize(fmean.hf,gparam);


%*********************************************************************
%determine how much of a border we need to leave on left bottom right top
%so that we don't cut off figure labels
ha=length(fmean.a);
for i=1:length(fmean.a)
    ha(i)=fmean.a{i}.ha;
end
ha=[ha hc];

%ha has handles to all the axes
tinset=get(ha,'tightinset');
tinset=cell2mat(tinset);

border.left=max(tinset(:,1));
border.bottom=max(tinset(:,2));
border.right=max(tinset(:,3));
border.top=max(tinset(:,4));

%determine the size for each of the axes
asize.width=(1-border.left-border.right-space.hspace*gparam.ncols-space.cbwidth)/(gparam.ncols);
asize.height=(1-border.top-border.bottom-space.vspace-space.htrue);

%loop through the axes and poisition them
row=1;
for col=1:gparam.ncols
        aind=spind(row,col);
        pos=get(fmean.a{aind}.ha,'position');
        pos(1)=border.left+(col-1)*(asize.width+space.hspace);
        pos(2)=border.bottom+space.htrue+space.vspace;
        pos(3)=asize.width;
        pos(4)=asize.height;
        set(fmean.a{aind}.ha,'position',pos);
end

row=2;
for col=1:gparam.ncols
        aind=spind(row,col);
        pos=get(fmean.a{aind}.ha,'position');
        pos(1)=border.left+(col-1)*(asize.width+space.hspace);
        pos(2)=border.bottom;
        pos(3)=asize.width;
        pos(4)=space.htrue;
        set(fmean.a{aind}.ha,'position',pos);
end

%size the color bars
for j=1:length(hc)
    pos=get(hc(j),'position');
    pos(1)=border.left+(gparam.ncols)*(asize.width+space.hspace);
    pos(2)=border.bottom+space.htrue+space.vspace;
    pos(3)=space.cbwidth;
    pos(4)=asize.height;
    set(hc(j),'position',pos);
end
refresh;
%%

previewfig(fmean.hf,'bounds','tight','FontMode','Fixed','FontSize',fmean.fontsize,'Width',gparam.width,'Height',gparam.height)
outfile='~/cvs_ece/writeup/NC06/cmpmaps.eps';
%exportfig(fmean.hf,outfile,'bounds','tight','FontMode','Fixed','FontSize',fmean.fontsize,'Width',gparam.width,'Height',gparam.height)