%03-17-2008
%
% This script redoes the plots of the MAPS on trials 0 and 200
%
% I had to rewrite this script to remake the plots for the accepted version
% of our neural comp paper.
%
%Revisions:
%  10-01-2008
%       Rescaled the image for the final proofs
%       changed location of data
%       updated script to newest plotting objects
%
NDIR='/mnt/ext_harddrive/adaptive_sampling_results_06_13_2008/allresults';
fdata=fullfile(NDIR,'nips','11_27','11_27_gabortrack_001.mat');
alldata=load(fdata);
%%
fmaps=FigObj('name','time varying: entropy','width',.8750,'height',1.4,'fontsize',5);


subsamp=100;
tlast=800;

%size of the markers
msize=3;
%************************************************************************
%random
%*************************************************************************
hp=plot([1:subsamp:tlast],alldata.prand.newton1d.entropy(1:subsamp:tlast),'x');
plot(1:tlast,alldata.prand.newton1d.entropy(1:end-1),'k-');

lstyle=[];
lstyle.marker='x';
lstyle.markerfacecolor='k';
lstyle.linestyle='none';
lstyle.color='k';
lstyle.markersize=msize+1;
fmaps.a=addplot(fmaps.a,'hp',hp,'pstyle',lstyle);

%************************************************************************
%info max
%*************************************************************************
hp=plot([1:subsamp:tlast],alldata.pmax.newton1d.entropy(1:subsamp:tlast));
plot(1:tlast,alldata.pmax.newton1d.entropy(1:end-1),'k-');

lstyle=[];
lstyle.marker='o';
lstyle.markerfacecolor='k';
lstyle.linestyle='none';
lstyle.color='k';
lstyle.markersize=msize;
fmaps.a=addplot(fmaps.a,'hp',hp,'pstyle',lstyle);

fmaps.a=xlabel(fmaps.a,'Iteration');
fmaps.a=ylabel(fmaps.a,'Entropy');

xlim([0 800]);
ylim([140 300]);
set(gca,'xtick',[0:400:800]);
set(gca,'ytick',[150:50:250]);

lblgraph(fmaps);

lobj=LegendObj('hlines',gethp(fmaps.a),'lbls',{'random','info. max.'});
pos=get(getha(lobj),'position');
%squeeze space
lobj=setvborder(lobj,.23,.23);
lobj=setvoffset(lobj, -.05);
lobj=setfontsize(lobj,5);
pos=[.5 .7 .54 .17];
lobj=setposition(lobj,pos);
lobj=settleft(lobj,.35);

%create a custom legend so we can shrink space
outfile='~/svn_trunk/publications/adaptive_sampling/NC06/images_eps/gabortrack_entropy_001.eps';
%saveas(gethf(fmaps),outfile,'epsc2');
