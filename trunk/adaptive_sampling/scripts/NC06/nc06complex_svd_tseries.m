%4-24-2007
%For the plots of the complex cell we want to plot the singular values
%of the MAP estimate because the true Quadratic matrix is a rank 2 matrix
%so plotting the SVD should make it clear the MAP converges to a rank 2
%Since singular values are sorted in descending order we don't have to plot
%all of them to show they are going to zero
%%
%matrix

%old data
% dsets(1).fname=fullfile(RESULTSDIR,'poolbased','04_29','04_29_poissexp_001.mat');
% dsets(1).simvar='simmax';
% dsets(1).lbl='info. max.';
% 
% dsets(2).fname=fullfile(RESULTSDIR,'poolbased','04_29','04_29_poissexp_001.mat');
% dsets(2).simvar='simrand';
% dsets(2).lbl='i.i.d.';

%*******************************************
%20d 1-d auditory data
dsets=[]


% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_005.mat');
% dsets(end).lbl='i.i.d.';
% dsets(end).simvar='simrand';


% dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','05_30','05_30_poissexp_005.mat');
% dsets(end).lbl='info. max. i.i.d';
% dsets(end).simvar='simunif';

dsets(end+1).fname=fullfile(RESULTSDIR,'poolbased','06_07','06_07_poissexp_001_simmax.mat');
dsets(end).lbl='info. max. heuristic';
dsets(end).simvar='simmax';

gparam.width=3;
gparam.height=3;

for dind=1:length(dsets)
    fprintf('Dataset dind=%d \n',dind);
    fsv.a{dind}.ha=subplot(1,length(dsets),dind);
    
    %load the data and compute
    %the singular values
    [pdata, sim, mp, sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
    
    if ~isa(mp.ninpobj,'QuadNonLinInp')
        error('Script only works for quadratic nonlinearities');
    end
   
%    thetalength=length(gettheta(sim.observer));
    dsets(dind).sv=zeros(mp.stimlen,sim.niter);
    
    %on each trial we reshape the map to be a matrix and find its singualr
    %values
    for t=1:sim.niter
        if (mod(t,10)==0)
            fprintf('Trial %d \n', t);
        end
       dsets(dind).sv(:,t)= svd(qmatquad(mp.ninpobj,pdata.m(:,t+1)));
    end

end
%%


%********************************************************************
%make the plot
fsv=[];
fsv.hf=figure;
fsv.name='Complex Cell: SVD';
fsv.fontsize=12;
fsv.xlabel='trial';
fsv.ylabel='singular values';

setfsize(fsv,gparam);
hold on;
%tmax=200;       %max trial
nsvals=3;       %how many singular values to plot 
gparam.ncols=length(dsets);
gparam.width=3;
gparam.height=3;

colormap(gray);
clim=zeros(length(dsets),2);    %to ensure color limits are the same
ha=zeros(1,length(dsets));
for dind=1:length(dsets)
    for sind=1:nsvals
        fsv.hp(sind)=plot(dsets(dind).sv(sind,:));
        fsv.lbls{sind}=sprintf('%d',sind);
        ls=getbwlstyle(sind);
        setlstyle(fsv.hp(sind),ls);
    end
end

%for the third svd make it a dotted line
%by plotting every 100 point
%ls.linestyle=':';
%ls.LineWidth=ls.LineWidth-2;
delete(fsv.hp(sind));
fsv.hp(sind)=plot(1:400:size(dsets(end).sv,2),dsets(end).sv(3,1:400:end),'k.','MarkerFaceColor','k');
%xlim([1 1000]);
lblgraph(fsv);
%setlstyle(fsv.hp(sind),ls);
fsv.outfile='~/cvs_ece/writeup/NC06/auditory_svd.eps';
%exportfig(fsv.hf,fsv.outfile,'bounds','tight','FontMode','Fixed','FontSize',fsv.fontsize,'Width',gparam.width,'Height',gparam.height,'color','rgb')
