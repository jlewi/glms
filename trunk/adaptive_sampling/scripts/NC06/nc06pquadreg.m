%12-19-07 New script for plotting the region
%    1. no transparency effects
%    2.
%
% Plot the feasible region under the power constraint
% along with sample points chosen via i.i.d. sampling of stimuli and using
% our heuristic.
clear all;
setpathvars;
dsets=[];


dsets=[];
dind=0;
bdir=fullfile('gabor','071213');
bfile='gabor2d';
niter=0;


%*************************************************************
%max over power constraint
%*************************************************************
dind=dind+1;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,1)));
dsets(dind).simvar='max';
dsets(dind).lbl='info. max';
%switch back to _001

gparam.trials=[2998]; %trials on which to plot the quadratic region


gparam.npts=500;            %number of points to plot
gparam.nsigpts=500;         %number of sigma points to use to plot the manifold
gparam.ncontours=20;        %number of contours for the contour plot
gparam.nrows=2;
gparam.ncols=2;


clear simobj;

%********************************************************************
%Load the data
%****************************************************************
dind=1;
simobj(dind)=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

%**************************************************************************
%Stimulus choosers for which we want to compute the points
%   1st stimulus chooser uses the power constraint to compute the actual
%   region
%**********************************************************************
clear sobj;
stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_25d_071213_001.mat'));
nstim=100;
sobj{1}=PoolstimHeurMuMax('nstim',nstim,'miobj',PoissExpCompMi());
stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_1600d_071213_002.mat'));
srescale.sfactor=getmag(getmobj(simobj));
nstim=100;
sobj{2}=PoolstimHeurMuMaxBall('nstim',nstim,'miobj',PoissExpCompMi());
nstim=10000;
sobj{3}=Poolstim('fname',stimfile,'miobj',PoissExpCompMI(),'nstim',nstim,'debug',false,'stimrescale',srescale);


%store info about the stimuli

freg=cell(1,length(gparam.trials));
fpoly=cell(1,length(gparam.trials));

%**************************************************************************
%load the data
%*************************************************************************
%%
pind=0;
for trial=gparam.trials
    pind=pind+1;
    post(pind)=getpost(simobj(1),trial);
    p.m=getpostm(simobj(1),trial);
    p.c=getpostc(simobj(1),trial);
    %use plotquadreg to compute the boundary of the feasible region
    dind=1;
    mobj=getmobj(simobj(dind));
    [fp{pind}, freg{pind},data(pind)]=plotquadreg('post',p,'mmag',getmag(getmobj(simobj(dind))),'klength',getklength(mobj),'alength',getshistlen(mobj));
    fp{pind}.name=sprintf('Feasible Region: Trial %d',trial);
    %lblgraph(fp{pind});
    close(fp{pind}.hf);
    close(freg{pind}.hf);


    for sind=1:length(sobj)
        [xmax,oreturn,sobj{sind},musigma{sind,pind}]=choosestim(sobj{sind},getpost(simobj,trial),[],getmobj(simobj),[]);
    end
end
%%


%%
%*********************************************************************
%Draw Plots
%**************************************************************************
%define the plot styles for point
pstyle(1).marker='o';
pstyle(1).color='k';
pstyle(1).markersize=5;
pstyle(1).markerfacecolor=[0 0 0];

pstyle(2).marker='+';
pstyle(2).color=[0 0 0];
pstyle(2).markersize=5;

nrows=1;
ncols=length(gparam.trials);

lbls{1}=sprintf('$\\hat{\\mathcal{X}}_{heur,t+1}$');
lbls{2}=sprintf('$\\hat{\\mathcal{X}}_{ball,t+1}$');

clear fpoly;

%for pind=1:length(gparam.trials)
    fpoly(pind)=FigObj('name','Feasible Region','width',4,'height',4);
    fpoly(pind).a=AxesObj('xlabel','\mu_\rho','ylabel','\sigma^2');

    %*****************************************
%     [col row]=ind2sub([ncols,nrows],pind);
%     setfocus(fpoly.a,row,col);
    row=1;
    col=1;
    
    
    %fpoly(pind).a(row,col)=title(fpoly(pind).a(row,col),sprintf('Trial %d',gparam.trials(pind)));
    %round the trial up to a power of 10
    fpoly(pind).a(row,col)=title(fpoly(pind).a(row,col),sprintf('Trial %d',ceil(gparam.trials(pind)/10)*10));
    hold on;
    %*****************************************************************
    %plot the boundary under the power constraint
    %**************************************************************
    %we multiply alpha by the magnitude of the mean

    m=getm(post(pind));
    hp=plot((m'*m)^.5*[data(pind).alpha data(pind).alpha(end:-1:1)],[data(pind).qmax(1,:), data(pind).qmin(1,end:-1:1)],'k-');
    lstyle.linewidth=1;
    lstyle.color=[0 0 0];
    lstyle.linestyle='-';
    fpoly(pind).a(row,col)=addplot(fpoly(pind).a(row,col),'hp',hp,'lstyle',lstyle);

    %compute the bounds
    bounds(pind).xmax=(m'*m)^.5*data(pind).alpha(end);
    bounds(pind).xmin=(m'*m)^.5*data(pind).alpha(1);
    bounds(pind).ymax=max(data(pind).qmax);
    bounds(pind).ymin=min(data(pind).qmin);

    %**********************************************************************
    %make a surface plot of the optimization manifold (i.e mutual
    %information as function of mu, sigma)
    %******************************************************************
    optman(pind).glmproj=(m'*m)^.5*[data(pind).alpha];
    optman(pind).sigma=linspace(bounds(pind).ymin,bounds(pind).ymax,gparam.nsigpts);
    %optman(pind).optman=zeros(gparam.nsigpts,length(optman(pind).glmproj));
    nrows=gparam.nsigpts;
    ncols=length(optman(pind).glmproj);
    [rind,cind]=ind2sub([nrows, ncols],1:(nrows*ncols));

    %compute the information using the miobj of one of the Poolbased
    %objects
    miobj=getmiobj(sobj{1});
    mi=compmi(miobj,optman(pind).glmproj(cind),optman(pind).sigma(rind));
    optman(pind).optman=reshape(mi,[nrows ncols]);
    %set all points outside the feasible region to NAN
    for cind=1:ncols
        ilb=find(optman(pind).sigma>=data(pind).qmin(cind),1,'first');
        if ~isempty(ilb)
            optman(pind).optman(1:ilb-1,cind)=nan;
        end
        iub=find(optman(pind).sigma>data(pind).qmax(cind),1,'first');
        if ~isempty(iub)
            optman(pind).optman(iub:end,cind)=nan;
        end
    end
    colormap(gray);
    %remove some of the lighter colors
    cl=colormap;
    cl=cl(1:end-5,:);
    colormap(cl);
    [c optman(pind).hc]=contourf(optman(pind).glmproj,optman(pind).sigma,optman(pind).optman,gparam.ncontours,'EdgeColor','none');
    hc(pind)=colorbar;
    %**********************************************************************
    %we plot the sample points 
    %**********************************************************************
    hold on;
    %plot samples for our heuristic
    sind=1;
        npts=min(gparam.npts,length(musigma{sind,pind}.muproj));
        hp=plot(musigma{sind,pind}.muproj,musigma{sind,pind}.sigma,'k.');


        fpoly(pind).a(row,col)=addplot(fpoly(pind).a(row,col),'hp',hp,'pstyle',pstyle(sind));
  
    
        %*plot the second heuristic
        sind =2;
        
        npts=min(gparam.npts,length(musigma{sind,pind}.muproj));
        hp=plot(musigma{sind,pind}.muproj,musigma{sind,pind}.sigma,'k.');


        fpoly(pind).a(row,col)=addplot(fpoly(pind).a(row,col),'hp',hp,'pstyle',pstyle(sind));
  
    %********************************************************
    %plot a dotted line around the random pts to help indicate where they
    %are
    sind=3;
     k = convhull(musigma{sind,pind}.muproj,musigma{sind,pind}.sigma);
%     plot(musigma{2,pind}.muproj(k),musigma{2,pind}.sigma(k),'k--','Linewidth',1.5)
    patch(musigma{sind,pind}.muproj(k)',musigma{sind,pind}.sigma(k)',.35*[1 1 1])
    fpoly(pind)=lblgraph(fpoly(pind));

    

xlim([-6 6]);
ylim([0 .18]);
%%make a custom legend to squeeze space
%do this last so legend is on top
hp=gethp(fpoly.a);
hp=hp(2:3);
hlgn=LegendObj('hlines',hp,'lbls',lbls);

hlgn=setlatex(hlgn);
hlgn=setposition(hlgn,.53,.82,.2,.13);
hlgn=settleft(hlgn,.25);

%end
odata=cell(length(gparam.trials),2);

for pind=1:length(gparam.trials)
   odata{pind,1}=fpoly(pind); 
end


odata{1,2}={'script','pfeasreg.m'; 'Black squares', class(sobj{1});'Grey squares', class(sobj{2})};
onenotetable(odata,seqfname('~/svn_trunk/notes/feasreg.xml'));

   outfile='~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/quadreg.eps';
   %onenotetable({fpoly,);
    %saveas(gethf(fpoly),outfile,'epsc2');
    %this render does not show transparency effects but it prevents the fonts
    %from being pixelated.
    %set(gcf,'Renderer','painters')
    %saveas(fpoly.hf,fpoly.outfile,'epsc2');