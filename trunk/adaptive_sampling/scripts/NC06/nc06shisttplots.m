%11-09-07:
%   make time series plots 
%Revisions
%   11-09-2007: use new object model

dind=0;

data=[];
dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071111','powernonlin_001.mat');
dsets(end).lbl=sprintf('i.i.d.');
dsets(end).simvar='rand';


dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071111','powernonlin_001.mat');
%dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
dsets(end).lbl=sprintf('info. max.');
dsets(end).simvar='max';

data=[];
dsets=[];
dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071211','expnonlin_021.mat');
dsets(end).lbl=sprintf('i.i.d.');
dsets(end).simvar='rand';


dsets(end+1).fname=fullfile(RESULTSDIR,'shist','071211','expnonlin_021.mat');
%dsets(end).lbl=sprintf('info. max.  \n $\\hat{\\mathcal{S}}_{heur,t+1}$');
dsets(end).lbl='info. max.';
dsets(end).simvar='max';


%trials on which to plot the results
%trials=[100 250 500];
%trials=[500 1000 1500];
%trials=[500 1500];
trials=[100 200 500 1000];
trials=[10 50 100];
trials=[125 150 175];
trials=[500 1000];

%trials=150;

%line styles
lstyle=[];
lstyle=getbwlstyle(3);
lstyle(1).LineWidth=lstyle(1).LineWidth*.5;
lstyle(1).linestyle='-';
lstyle(1).color= [0.7778 0.7778 0.7778];
lstyle(2)=getbwlstyle(1);
lstyle(2).linestyle='-';
lstyle(3)=getbwlstyle(4);
lstyle(3).linestyle='--'
lstyle(3).LineWidth=lstyle(3).LineWidth*.75;
%**************************************************************************
%Grapha Parameters
%**************************************************************************

fshist(1)=FigObj('name','Stimulus filter','width',4,'height',3);
fshist(1).a=AxesObj('nrows',length(trials),'ncols',1);
fshist(2)=FigObj('name','Spike history filter','width',4,'height',3);
fshist(2).a=AxesObj('nrows',length(trials),'ncols',1);

%ylimits for each axis
clear yl;
yl(1,1).yl=[-2,2];
yl(2,1).yl=[-2,2];
yl(3,1).yl=[-1,1];

yl(1,2).yl=[-.2,.2];
yl(2,2).yl=[-.2,.2];
yl(3,2).yl=[-.01,.01];

%loop over the datasets
clear simobj;
for dind=1:length(dsets)
    %load this simulation
  simobj(dind)=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

    %loop over the trials to plot
    for tind=1:length(trials)
        if (trials(tind)>getniter(simobj))
            break;
        end
        
    %get the estimates
    mfull=getpostm(simobj(dind),trials(tind));
    mstim=mfull(1:getstimlen(getmobj(simobj(dind))),:);
    mshist=mfull(getstimlen(getmobj(simobj(dind)))+1:end,:);

  
    %*************************************
    %stimulus coefficients
    pind=1;
    setfocus(fshist(1).a,tind,1);
    hold on;
    hp=plot(1:length(mstim),mstim);
    fshist(1).a(tind,1)=addplot(fshist(1).a(tind,1),'hp',hp,'lbl',dsets(dind).lbl,'lstyle',lstyle(dind));
    
    %turn of xlabels
    set(gca,'xtick',[]);
    
    %set ylabel
     fshist(pind).a(tind,1)=ylabel(fshist(pind).a(tind,1),sprintf('trial %0.6g',trials(tind)));
  % fshist(pind).a(tind,1)=ylabel(fshist(pind).a(tind,1),sprintf('trial %0.6g \n \\theta_{k_i}',trials(tind)));
  %  hy=getylabel(fshist(pind).a(tind,1));
    %set(hy.h,'interpreter','latex');
    
  ylim(yl(tind,pind).yl);
    %*************************************
    %spike history coefficients
    pind=2;
    setfocus(fshist(2).a,tind,1);
    hold on;
    hp=plot(1:length(mshist),mshist);
    fshist(2).a(tind,1)=addplot(fshist(2).a(tind,1),'hp',hp,'lbl',dsets(dind).lbl,'lstyle',lstyle(dind));
    %turn of xlabels
    set(gca,'xtick',[]);
    
    %set ylabel
      fshist(pind).a(tind,1)=ylabel(fshist(pind).a(tind,1),sprintf('trial %0.6g',trials(tind)));
 %   fshist(pind).a(tind,1)=ylabel(fshist(pind).a(tind,1),sprintf('trial %0.6g \n \\theta_{a_i}',trials(tind)));
%    hy=getylabel(fshist(pind).a(tind,1));
    %set(hy.h,'interpreter','latex');
    
    
    ylim(yl(tind,pind).yl);
    end
end
dind=1;
 %get true value
    theta=gettheta(getobserver(simobj(dind)));
    kfilt=theta(1:getstimlen(getmobj(simobj(dind))));
    afilt=theta(getstimlen(getmobj(simobj(dind)))+1:end);

%plot the true value
for tind=1:length(trials)
    setfocus(fshist(1).a,tind,1);
    hp=plot(1:length(mstim),kfilt);
    fshist(1).a(tind,1)=addplot(fshist(1).a(tind,1),'hp',hp,'lbl','true','lstyle',lstyle(length(dsets)+1));
    
    setfocus(fshist(2).a,tind,1);
    hp=plot(1:length(afilt),afilt);
    fshist(2).a(tind,1)=addplot(fshist(2).a(tind,1),'hp',hp,'lbl','true','lstyle',lstyle(length(dsets)+1));
    
end


%**********************************************
%adjust labels
for pind=1:2
    
    %add xlabel to last plot
    if (pind==1)
        %title
    fshist(pind).a(1,1)=title(fshist(pind).a(1,1),'$\vec{\theta}_x$');   
    fshist(pind).a(length(trials),1)=xlabel(fshist(pind).a(length(trials),1),'i');
    else
    
            
        fshist(pind).a(1,1)=title(fshist(pind).a(1,1),'$\vec{\theta}_f$');   
            fshist(pind).a(length(trials),1)=xlabel(fshist(pind).a(length(trials),1),'i');
    end
    
    set(fshist(pind).a(length(trials),1),'xtickmode','auto');
    
        ht=gettitle(fshist(pind).a(1,1));  
    set(ht.h,'interpreter','latex');

end


%call lblgraph first to adjust fontsizes
fshist(1)=lblgraph(fshist(1));
fshist(2)=lblgraph(fshist(2));

%turn off legend on all but first plot
for pind=1:2
    for tind=1:length(trials)
        hl=gethlgnd(fshist(pind).a(tind,1));
        set(hl,'visible','off');
    end
    end
sizesubplots(fshist(1),[],[],[],[0 0 0 .05]);

sizesubplots(fshist(2),[],[],[],[0 0 0 .05]);

%create a custom legend
% figure(gethf(fshist(1)));
% lobj(1)=LegendObj('hlines',gethp(fshist(1).a(1)),'lbls', {dsets.lbl 'true'});
% lobj(1)=setposition(lobj(1),.6,.8,.3,.2);
%lobj=setvborder(lobj,0.05,0.05);

figure(gethf(fshist(2)));
lobj(2)=LegendObj('hlines',gethp(fshist(1).a(1)),'lbls', {dsets.lbl 'true'});
lobj(2)=setposition(lobj(2),.17,.8,.3,.2);
odata={'data sets',[{'label'},{'variable'},{'file'};{dsets.lbl}' {dsets.simvar}' {dsets.fname}'];fshist(1),'stimulus coefficients';fshist(2),'spike history coefficients'};
onenotetable(odata,seqfname('~/svn_trunk/notes/nc06shisttplots.xml'));
return;

%saveas(gethf(fshist(1)),'~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/kfiltlseries.eps','epsc2')
%saveas(gethf(fshist(2)),'~/svn_trunk/adaptive_sampling/writeup/NC06/images_eps/shistlseries.eps','epsc2')