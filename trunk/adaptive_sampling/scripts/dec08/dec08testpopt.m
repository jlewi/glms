%script to test object POptAsymGPLB

mp.glm=GLMPoisson('canon');
mp.mmag=1;
mp.klength=1;
mp.ktlength=2;
mobj=MParamObj(mp);

dimtheta=getparamlen(mobj);

post=GaussPost('m',[1; 0],'c',[.10 0; 0 1]);

mu=.2;
c={[1] [0]};
gp=StimGP('mu',mu,'c',c);

sobj=POptAsymGPPCanon('klength',mp.klength,'ktlength',mp.ktlength);

nsdir=1000;
nsproj=1000;
%ex=exlogdetexss(sobj,post,gp, nsdir, nsproj);

pcon=1;
[gp,fval,exitflag,output,thetadir,thetaproj]=optgp(sobj,post,pcon,nsdir,nsproj);
ex=exlogdetexss(sobj,post,gp, thetadir, thetaproj);