%script to test object POptAsymDistDisc
%
%Compute the optimal StatDistDisc for a specific theta. 
%Then compare that design to an i.i.d gp

mp.glm=GLMPoisson('canon');
mp.mmag=1;
mp.klength=1;
mp.ktlength=2;
mobj=MParamObj(mp);

dimtheta=getparamlen(mobj);

theta=[1;-1];


mu=0;

%choose the stimuli by sampling the ball
% nstim=5;
% stim=normrnd(0,1,1,nstim);
% 
% %append theta
% stim=[stim theta'];
% nstim=size(stim,2);

nstim=6;
stim=linspace(-1,1,nstim);

sobj=POptAsymDistDisc('stim',stim,'ktlength',mp.ktlength);


post=GaussPost([0;0],eye(2));

psuni=StatDistDisc('stim',stim,'ktlength',mp.ktlength);

%distribution with random marginal distribution
%and independence
pm=rand(nstim,1);
pm=pm/sum(pm);
psrind=StatDistDisc('stim',stim,'ktlength',mp.ktlength,'p',pm*pm');

%we want to make a copy of the distribution and not just a pointer
psinit=StatDistDisc('stim',stim,'ktlength',mp.ktlength,'p',psrind.p);

%[psopt,fval,exitflag,output,theta]=optdist(sobj,post,psinit,mp.glm,theta);
[psopt,fval,exitflag,output,theta]=optdistnull(sobj,post,psinit,mp.glm,theta);

[fopt]=exlogdetexss(sobj,post,psopt, mobj.glm, theta );
[funi]=exlogdetexss(sobj,post,psuni, mobj.glm, theta );
[frind]=exlogdetexss(sobj,post,psrind, mobj.glm, theta );

[fopt funi frind]

return;
%try doing some gradient ascent and seeing what happens

%%

psopt=StatDistDisc('stim',stim,'ktlength',mp.ktlength);

nsteps=100;
gradasc(sobj,psopt,nsteps,post,mobj.glm,theta);



%save it to a file
fname=FilePath('RESULTSDIR',fullfile('asympopt','090110','psopt.mat'));
fname=seqfname(fname);

save(getpath(fname),'psopt','mobj','theta','nstim');
