%**************************************************************************
%
%Explanation: Setup simulations which select the optimal batch by selecting
%either the batch which maximizes the lower or upper boundary of the mutual
%information.
function dec08setupbsinfomaxbounds()

%try decreasing the bin size and increasing the number of time bins
allparam.obsrvwindowxfs=124;
allparam.stimnobsrvwind=20;
allparam.freqsubsample=1;

allparam.model='MBSFTSep';




scale=1;
allparam.prior.stimvar=10^-2*scale;
allparam.prior.shistvar=1*scale;
allparam.prior.biasvar=10*scale^2;

%create a bdata an mobject to use to decide which frequency components to
%include
bdata=BSSetup.initbsdata(allparam);

strfdim=getstrfdim(bdata);
mparam.klength=strfdim(1);
mparam.ktlength=strfdim(2);
mparam.alength=0;
mparam.hasbias=0;
mparam.mmag=1;
mparam.glm=GLMPoisson('canon');
mobj=MBSFTSep(mparam);


%  nfreq=20;
% ntime=4;
nfreq=10;
ntime=2;
subind=bindexinv(mobj,1:mobj.nstimcoeff);
btouse=subind(:,1)<=nfreq & subind(:,2)<=ntime;
btouse=find(btouse==1);

allparam.mparam.btouse=btouse;


%how many repeats of the data to use
allparam.stimparam.nrepeats=5;

allparam.updater.compeig=false;


%************************************************************
%Info. Max: Maximize the lower boundary
%************************************************************
imparam=allparam;

imparam.stimtype='BSBatchPoissLB';
%imparam.stimtype='BSBatchInfoMax';
%imparam.stimparam.miobj=CompMIPoissCanonLB();
%imparam.stimparam.bsize=mparam.ktlength;
imparam.updater.type='Newton1d';
BSSetup.setupinfomax(imparam);


%************************************************************
%Info. Max: Maximize the upperboundar boundary
%************************************************************
% imparam=allparam;
% imparam.stimtype='BSBatchInfoMax';
% imparam.stimparam.miobj=CompMIPoissCanonUB();
% imparam.stimparam.bsize=mparam.ktlength;
% imparam.updater.type='Newton1d';
% BSSetup.setupinfomax(imparam);
