% function compasymvar('simmax',simmax,'simrand',simrand)
%       -the data structures describing the data
%
%
% Return value:
%   imax   - structure containing predicted asymptotic values for info.max
%           .covarexp - the asymptotic covariance matrix
%                      inverse of the expected fisher information over the
%                      optimal sampling distribution
%           .ljexp   - the entropy of covarexp           
%           .xopt    - optimal point of support in direction of theta
%           .asymevals - asymptotic eigenvalues of the covariance matrix
%                      - sorted in descending order
%           .asymevec - asymptotic eigenvectors of the covariance matrix
%   imaxemp - structure describing empirical results on each trial
%           .var     - klength x  ntrials
%                    - variance in direction of theta and directions
%                    orthogonal to theta
%           .trials  - trial on which values in var were computed
%                     computed
%
%   iidvar - structure containing predicted values for i.i.d design
%         .covarexp   - expected covariance matrix
%         
%          .asymevals - asymptotic eigenvalues of the covariance matrix
%                      - sorted in descending order
%          .asymevec - asymptotic eigenvectors of the covariance matrix
%         
%   iidvaremp - empirical variances in the case of i.i.d. design
%         .var        - variance in direction of eigenvectors
%                       of iidvar.covarexp
%         .trials     - trials
%
%   %Old structure
%   iidvar.truevar    - eigenvalues of covariance on each trial
%         .x1         -asymptotic variance in direction of theta
%         .xp         -asymptotic variance in direciton orthogonal to theta
%         .trials     -trials associated with truevar
%         .thetalength -length of theta
%
%Explantion: This function computes the emprical variance as well as the
%asymptotic variance. In order to compare the emprical to theoretical
%variance we need to change the basis for our empirical coordinates so that
%theta is parallel to the first coordinate. 
%
%03-17-2008
%   Fixed bugs. These bugs were created when I created the separate
%   functions compimaxasymvar and compiidasymvar.
%
%12-27-2007
%   adjusted it for new object model
%
%4-24-2007
%   split the return strcutures so that the asymptotic values are in 1
%   structure and the empirical values are in another
%
%4-18-2007
%   turned into a function which can load data from a structure.
function [imax,imaxemp,iidvar,iidvaremp]=compasymvar(varargin)

subsamp=1;

con(1).rparams={'simmax','simrand'};
%determine the constructor given the input parameters
[cind,params]=constructid(varargin,con);

if (cind==1)
    
   %must be the simulation data we are passing in
   %required parameters
   simmax=params.simmax;
   simrand=params.simrand;
   
end




%starttrial=3*10^4;          %which trial to start with for computing
%find the optimal p(x)
%assume its supported on a single point


%structure to store imax results
imax=[];
imaxemp=[];
iidvar=[];
iidvaremp=[];

%

if (getshistlen(getmobj(simmax))>0)
    error('Central limit theorem does not hold if we have spike-history terms');
end

[imax,imaxemp]=compimax(simmax,subsamp)
[iidvar,iidvaremp]=compiidvar(simrand,subsamp);


return;

%********************************************************************
%Function to compute the info. max. results
%**************************************************************************
function [imax,imaxemp]=compimax(simmax,subsamp)
niter=getniter(simmax);
%we need to definea new coordinate system
%in which theta is the first axis
theta=gettheta(getobserver(simmax));
theta1=(theta'*theta)^.5;
n=getklength(getmobj(simmax))+getshistlen(getmobj(simmax));
em=getmag(getmobj(simmax));

[imax]=compimaxasymvar(n,theta,em);


%compute the expected log determinant of the fisher information
%over the optimal p
%imax.ljexp=gettresponse(mobjmax)*(log2(exp(xopt*theta1)*xopt^2)+(n-1)*log2(exp(xopt*theta1)*(em^2-xopt^2)/(n-1)));


%compute the asymptotic entropy
%trials=1:getniter(simrand);

%4-18-07 doesn't appear to be used
%entexp=1/2*n*(log2(2*pi*exp(1)))-1/2*ljexp-1/2*n*log2(trials);


%********************************************
%Info. Max
%Empirical variance
%**********************************************
%we need an orthonormal basis
%first directorion parralel to theta
%4-24-2007
%Simplified computation of variances
%we need to compute the variance after each trial in the direction
%of the eigenvectors of the asymptotic covariance matrix.
imaxemp.var=zeros(getklength(getmobj(simmax)),length(0:subsamp:getniter(simmax)));
imaxemp.trials=zeros(1,length(0:subsamp:getniter(simmax)));
%keep track of trials on which covariance matrix is empty
cisempty=zeros(1,length(imaxemp.trials));
for trial=0:subsamp:getniter(simmax)
    if (mod(trial/subsamp,10)==0)
            fprintf('imax: trial %d \n',trial);
     end
    if ~isempty(getpostc(simmax,trial))
         varemp=getpostc(simmax,trial)*imax.asymevecs;
         varemp=imax.asymevecs.*varemp;
         varemp=sum(varemp,1);
         imaxemp.var(:,trial/subsamp+1)=varemp';
         imaxemp.trials(trial/subsamp+1)=trial;
    else
        cisempty(trial/subsamp+1)=1;
    end
end
%eliminate empty trials
ind=find(cisempty==0);
imaxemp.var=imaxemp.var(:,ind);
imaxemp.trials=imaxemp.trials(:,ind);


%********************************************************************
%I.I.D.
%asymptotic
%***********************************************************************
function [iidvar, iidvaremp]=compiidvar(simrand,subsamp)
mobj=getmobj(simrand);
niter=getniter(simrand);
%compute the asymptotic variance in the i.i.d case
mmag=getmag(mobj);
n=getklength(getmobj(simrand));

theta=gettheta(getobserver(simrand));
theta1=(theta'*theta)^.5;

%compute the asymptotic variance
[iidvar]=compiidasymvar(n,theta, mmag)

%***********************************************************************
% I.I.D
% empirical
%*************************************************************************
iidvaremp.trials=0:subsamp:getniter(simrand);

    iidvar.truevar=zeros(getklength(getmobj(simrand)),floor(getniter(simrand)/subsamp));;
%    iidvar.niter=getniter(simrand);
    %if (simparamrand.lowmem>0)
    %    iidvar.subsamp=simparamrand.lowmem;
    %else
    %end
    %iidvar.trials=[0 iidvar.subsamp:iidvar.subsamp:iidvar.niter];
    %keep track of trials on which covariance matrix is empty
    cisempty=zeros(1,length(iidvaremp.trials));
    for trial=0:subsamp:getniter(simrand)
        %******************************************************************
        %****
        if (mod(trial/subsamp,10)==0)
            fprintf('iidvar: trial %d \n',trial);
        end
      
            if ~isempty(getpostc(simrand,trial))
         varemp=getpostc(simrand,trial)*iidvar.asymevecs;
         varemp=iidvar.asymevecs.*varemp;
         varemp=sum(varemp,1);
         iidvaremp.var(:,trial/subsamp+1)=varemp';
         iidvaremp.trials(trial/subsamp+1)=trial;
            end
    end
    %eliminate empty trials
    ind=find(cisempty==0);
    iidvaremp.truevar=iidvaremp.var(:,ind);
    iidvaremp.trials=iidvaremp.trials(:,ind);
