%function empvar=empvar(fname,simvar)
%   fname - filename
%   simvar - name of simulation
%
%Return value:
%   empvar
%       .evals - eigenvalues of covariance matrix after choosing basis so
%       that one axis is aligned with theta
function empvar(fname,simvar)