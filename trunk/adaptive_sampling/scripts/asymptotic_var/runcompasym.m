%4-18-2007
%
%Helper script for computing the asymptotic variance
%
%If covariance matrices are missing, we will compute them first.
setpathvars;
fname=fullfile(RESULTSDIR,'poissexp','04_24','gabor_04_24_001.mat');

[pmax, simmax, mparammax,srmax]=loadsim('simfile',fname,'simvar','simmax');
[prand, simrand, mparamrand,srrand]=loadsim('simfile',fname,'simvar','simrand');

%fill in low memory
[pmax]=fillinlowmem(fname,'simmax',[]);
[prand]=fillinlowmem(fname,'simrand',[]);

%compute the asymptotic variance
%%
[imax,imaxemp,irand,irandemp]=compasymvar('pmax',pmax,'simparammax',simmax,'mparammax',mparammax,'srmax',srmax,...
       'prand',prand,'simparamrand',simrand,'mparamrand',mparamrand,'srrand',srrand,...
       'subsamp',50);
   
%%
   
%plot it
plotasymvar;