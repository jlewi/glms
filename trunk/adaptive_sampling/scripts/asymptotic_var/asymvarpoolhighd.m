%03-22-07
%Compute the asymptotic variance in the pool based setting.
% for high dimensions we have to do a different approach
%because we can't compute all the terms in the determinant because there
%are two many
function [imax,irand,cmat,minfo]=asymvarpoolhighd(fname)
imax=[];
irand=[];
%setpaths;
%fname=fullfile(,'poolbased','03_22','03_22_poolstim_001.mat');

[pmax simmax, mobjmax,srmax]=loadsim('simfile',fname,'simvar','simmax');
[prand simrand, mobjrand,srrand]=loadsim('simfile',fname,'simvar','simrand');

%get the expeted fisher information
stim=simmax.stimchooser.stimpool;
nstim=size(stim,2);

%compute the fisher information for each stimulus
jeps=jexp(mobjmax.glm,gettheta(simmax.observer)'*stim);

%*****************************************************
%create the matrices used to compute log |ExpJexp|
%and its derivative
%these matrices are the fisher inforation for each stimulus 
jexpmat=zeros(mobjmax.klength,mobjmax.klength,nstim);

for j=1:nstim
   jexpmat(:,:,j)=jeps(j)*stim(:,j)*stim(:,j)'; 
end

%**************************************************
%compute the optimal asymptotic distribution
%imax.pop
Aeq=ones(1,nstim);
beq=1;
lb=zeros(nstim,1);
ub=ones(nstim,1);
A=[];
B=[];
%initialize by putting all probability on the stimuli actually chosen
pinit=zeros(nstim,1);
%stiminfo=[srmax.stiminfo{:}];
%sind=[stiminfo(:).sind];
%pinit=1/nstim*ones(nstim,1);
%put all probability on stimuli maximally correlated with theta
[mcorr mind]=max(gettheta(simmax.observer)'*stim);

%put the weight on at least 2 points otherwise determinant
%is zero and log|A| is undefined
%This is because Jexp is rank 1 
%pick a vector orthogonal to mind
%[mcorr mminind]=min(normmag(stim(:,mind))'*normmag(stim));
%pinit(mind)=.5;
%pinit(mminind)=.5;

%initialize pinit to the emirical optimal sampling distribution
scounts=zeros(nstim,simmax.niter);
stiminfo=[srmax.stiminfo{:}];
srmax.sind=[stiminfo.sind];
ind=[srmax.sind',(1:simmax.niter)'];
scounts(sub2ind([nstim,simmax.niter],ind(:,1),ind(:,2)))=1;
scounts=sum(scounts,2);
pinit=scounts/simmax.niter;


%objfun=@(p)(negelogjexp(p,jmat));
optim=[];
%optim = optimset(optim,'GradObj','on');
optim=optimset(optim,'Display','on');
optim=optimset(optim,'TolX',10^-16);
optim=optimset(optim,'TolFun',10^-16);
keyboard;
objfun=@(p)(neglogdetjexp(p,jexpmat));
fprintf('Finished computing expected fisher information matrices \n');
[imax.popt,minfo.fval,minfo.exitflag,minfo.output]=fmincon(objfun,pinit,A,B,Aeq,beq,lb,ub,[],optim);

%compute the asymptotic variance;
ind=find(imax.popt>0);
imax.asymjexp=zeros(mobjmax.klength,mobjmax.klength);

for t=ind'
    imax.asymjexp= imax.asymjexp+imax.popt(t)*jeps(t)*stim(:,t)*stim(:,t)';
end
imax.asymvar=inv(imax.asymjexp);


%***********************************************************
%compute iid asymptotic variance
irand.popt=ones(nstim,1)/nstim; %uniform distribution
irand.asymjexp=zeros(mobjmax.klength,mobjmax.klength);
for t=ind'
    irand.asymjexp= irand.asymjexp+irand.popt(t)*jeps(t)*stim(:,t)*stim(:,t)';
end
irand.asymvar=inv(irand.asymjexp);


%compute the negative of the log determinant and the derivative
%we need the negative because we will use fmincon to maximize our objective
%function
function [ldjexp]=neglogdetjexp(p,jexpmat)
    %only select those points which have support on them
    ind=find(p>0);
    %sum the matrices
    jsum=zeros(size(jexpmat,1),size(jexpmat,2));
    for i=ind'
       jsum=jsum+p(i)*jexpmat(:,:,i); 
    end
	ldjexp=log(det(jsum));
    ldjexp=-ldjexp;

