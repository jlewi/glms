%run computatin of asymptotic variance in pool based case

%fname='/home/jlewi/cvs_ece/allresults/poolbased/03_24/03_24_poolstim_002.mat';
%fname='/home/jlewi/cvs_ece/allresults/poolbased/03_25/03_25_poolstim_001.mat';
%fname='/home/jlewi/cvs_ece/allresults/poolbased/03_26/03_26_poolstim_001.mat';
%fname='/home/jlewi/cvs_ece/allresults/poolbased/03_26/03_26_poolstim_001.mat';
%fname=fullfile(RESULTSDIR,'poolbased','04_23','poolgabor2d_04_23_002.mat');
fname=fullfile(RESULTSDIR,'poolbased','04_23','04_23_poolstim_001.mat');
fname=fullfile(RESULTSDIR,'poolbased','04_23','04_23_poolstim_002.mat');
[imax,irand,cmat]=asymvarpool(fname);
%[imax,irand,cmat]=asymvarpoolhighd(fname);
%[imaxjopt,irandjopt,cmat,jmat]=asymvarpool(fname);
%[imaxcgrad,irandcgrad,cmat,jmat]=asymvarpool(fname);

%%
[pmax simmax, mobjmax,srmax]=loadsim('simfile',fname,'simvar','simmax');
[prand simrand, mobjrand,srrand]=loadsim('simfile',fname,'simvar','simrand');

stiminfo=[srrand.stiminfo{:}];
srrand.sind=[stiminfo.sind];

stiminfo=[srmax.stiminfo{:}];
srmax.sind=[stiminfo.sind];

imax.mse=zeros(1,simmax.niter);
irand.mse=zeros(1,simrand.niter);
for j=1:simmax.niter
    me=(j*pmax.c{j+1}-imax.asymvar).^2;
    imax.mse(j)=sum(me(:)).^.5;
end
  
for j=1:simrand.niter
    me=(j*prand.c{j+1}-irand.asymvar).^2;
    irand.mse(j)=sum(me(:)).^.5;
end


stim=simmax.stimchooser.stimpool;
nstim=size(stim,2);
%bar graph of the stimuli
fbar.hf=figure;
fbar.xlabel='Stimulus index';
fbar.ylabel='Probability';
fbar.name='Popt';

scounts=zeros(nstim,simmax.niter);
ind=[srmax.sind',(1:simmax.niter)'];
scounts(sub2ind([nstim,simmax.niter],ind(:,1),ind(:,2)))=1;
scounts=sum(scounts,2);
scounts=scounts/simmax.niter;
ind=[find(scounts>.01)' find(imax.popt>.01)'];
ind=unique(ind);

hold on;
%fbar.hp=bar(ind',[scounts(ind)';imax.popt(ind)']);
fbar.hp=bar([scounts(ind)';imax.popt(ind)']);
set(gca,'xtick',1:length(ind));
set(gca,'xticklabel',ind);
set(fbar.hp(1),'facecolor','g');
set(fbar.hp(2),'facecolor',[1 0 0]);
fbar.lbls{1}='Info. Max: Empirical';
%fbar.hp(2)=bar([1:nstim]',imax.popt);
fbar.lbls{2}='Info. Max: Pred';
lblgraph(fbar);

%************************************************************
%Plot the mse on a root n scale
fasym.hf=figure;
fasym.xlabel='trial';
fasym.ylabel='t^{.5}MSE';
fasym.name='Asymptotic mse';

hold on;
pind=1;
fasym.hp(1)=plot(1:simmax.niter,(1:simmax.niter).^.5.*imax.mse,getptype(pind));
fasym.lbls{1}='Info. max';

pind=2;
fasym.hp(pind)=plot(1:simrand.niter,(1:simrand.niter).^.5.*irand.mse,getptype(pind));
fasym.lbls{pind}='I.I.D';

lblgraph(fasym);
set(gca,'yscale','log');
%%


%plot the angles of the eigenvectors of the covariance matrices
if (mparam.klength==2)
imax.eangles=zeros(2,simmax.niter);
for j=1:simmax.niter
    [u s]=svd(j*pmax.c{j+1});
    imax.eangles(1,j)=180/pi*atan2(u(2,1),u(1,1));
    imax.eangles(2,j)=180/pi*atan2(u(2,2),u(1,2));
end
imax.asymangles=zeros(2,1);
[u s]=svd(imax.asymvar);
imax.asymangles(1)=180/pi*atan2(u(2,1),u(1,1));
imax.asymangles(2)=180/pi*atan2(u(2,2),u(1,2));

fangle.hf=figure;
fangle.a{1}.ylabel='Angle';
fangle.a{2}.ylabel='Angle';
fangle.a{1}.xlabel='trial';
fangle.name='Angle';

fangle.a{1}.ha=subplot(2,1,1);
hold on;
pind=1;
fangle.a{1}.hp(pind)=plot(1:simmax.niter,imax.eangles(1,:),getptype(pind));
fangle.a{1}.lbls{pind}='Info. Max: Empirical: Angle 1';

pind=pind+1;
fangle.a{1}.hp(pind)=plot([1 simmax.niter],imax.asymangles(1)*[1 1],getptype(pind));
fangle.a{1}.lbls{pind}='Info. Max: Asymptotic: Angle 1';



fangle.a{2}.ha=subplot(2,1,2);
hold on;
pind=1;

fangle.a{2}.hp(pind)=plot(1:simmax.niter,imax.eangles(2,:),getptype(pind));
fangle.a{2}.lbls{pind}='Info. Max: Empirical: Angle 2';

pind=pind+1;
fangle.a{2}.hp(pind)=plot([1 simmax.niter],imax.asymangles(2)*[1 1],getptype(pind));
fangle.a{2}.lbls{pind}='Info. Max: Asymptotic: Angle 2';

lblgraph(fangle);
end
%save the data
[path fn]=fileparts(fname);
outfile=fullfile(path,sprintf('%s_asymvar.mat',fn));
%save(outfile,'imax','irand','cmat');