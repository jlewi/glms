%07-07-2007
%Test asymptotic variance
%THIS WON"T WORK. The variance computed by compasym var was for when
%stimuli always have magnitude =mparam.mmag.
%   1. compute variance for i.i.d under correct poisson model using
%   asymptoticvar
%   2. compute it using compasymvar and compare the two.


%test data a basic canonical poisson
fname='/home/jlewi/cvs_ece/allresults/poissexp/02_07/02_07_poissexp_001.mat';

[prand,simrand,mparam,srrand]=loadsim('fname',fname,'simvar','simrand');

sn=@(n)(2*pi^(n/2)/gamma(n/2));     %surface area of unit n dimenionsal hypersphere

%marginal distribution on x1 in n dimensions
px1=@(x,n)(n/(n-1)*sn(n-1)/(sn(n))*(mparam.mmag^2-x.^2).^((n-1)/2)/(mparam.mmag^n));
px=@(x)px1(x,mparam.klength);

%the expected variance in a direction orthogonal to x1 given x1
%and magnitude constraint m
%varorth=@(x,n,m)(2*(n-1)/(n-2)*sn(n-2)/sn(n-1)*(m^2-x.^2).^(n/2)*pi^.5*gamma(n/2)/(4*gamma(2+(n-1)/2)));
varorth=@(x,n,m)(2*(n-1)/(n-2)*sn(n-2)/sn(n-1)*(m^2-x.^2).^(n/2)*pi^.5*gamma(n/2)/(4*gamma(2+(n-1)/2)));
%new method
[avar]=asymptoticcovar(simrand.observer.glm,simrand.observer.glm,px,mparam.klength,mparam.mmag,(mparam.ktrue'*mparam.ktrue)^.5);
