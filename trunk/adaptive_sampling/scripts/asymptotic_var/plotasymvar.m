% 9-05-2006
%
% Make a plot of the asymptotic covariance
%
% You will ned to run
%   compasymvar first
%parameters

%param.file=fullfile(RESULTSDIR, 'tracking','08_24','08_24_maxtrack_001.mat');

clear('fcovar');
fcovar.hf=figure;
fcovar.title='';
fcovar.name='Asymptotic Covariance';
fcovar.fname='dircovar';
fcovar.fontsize=12;

hold on;
nrows=1;
ncols=2;
%**************************************************************************
%two panels
%1. panel components parallel to mean
%2. other panel orthogonal to mean
fcovar.a{1}.ha=subplot(nrows,ncols,1);

hold on;
%***********************************************
%parallel to mean
fcovar.a{1}.hp=[];
fcovar.a{1}.lbls={};
fcovar.a{1}.ylabel='variance';
fcovar.a{1}.xlabel='trial';
fcovar.a{1}.title='Parallel';
pind=0;


%***********************************************
%I.I.D
%empirical
%Parallel to theta
%**************************************************
%plot the i.i.d asymptotic variance
pind=length(fcovar.a{1}.hp)+1;

fcovar.a{1}.hp(end+1)=plot(irandemp.trials,irandemp.var(1,:),getptype(pind,2));
fcovar.a{1}.lbls{end+1}='i.i.d. emp.';
fcovar.a{1}.ls{pind}=getbwlstyle(2);
%***********************************************
%I.I.D
%Predicted
%Parallel to theta
%**************************************************
pind=pind+1;
fcovar.a{1}.hp(pind)=plot(1:simrand.niter,(1:simrand.niter).^-1*irand.asymevals(1));
fcovar.a{1}.lbls{end+1}='i.i.d. pred.';
fcovar.a{1}.ls{pind}=getbwlstyle(4);


%***********************************************
%Info. Max
%empirical
%Parallel to theta
%**************************************************
%plot the i.i.d asymptotic variance
pind=length(fcovar.a{1}.hp)+1;

fcovar.a{1}.hp(pind)=plot(imaxemp.trials,imaxemp.var(1,:));
fcovar.a{1}.lbls{pind}='i.i.d. emp.';
fcovar.a{1}.ls{pind}=getbwlstyle(1);
%dexp=diag(iidrand.covarexp);
%dexp=mean(dexp(2:end));

%**********************************************
%Info. Max
%Predicted
%Parallel to theta
%*********************************************
%plot the asymptotic variance
pind=pind+1;
%cpred=imax.)*(1./(imax.trials(2:end)));

%fcovar.a{1}.hp(end+1)=plot([0 simparam.niter],covarexp(1)*[1 1],getptype(pind,2));
fcovar.a{1}.hp(pind)=plot(1:simmax.niter,(1:simmax.niter).^-1*imax.asymevals(1));
fcovar.a{1}.lbls{pind}='info. max. pred.';
fcovar.a{1}.ls{pind}=getbwlstyle(3);






%dexp=diag(iidrand.covarexp);
%dexp=mean(dexp(2:end));


%hl=legend(fcovar.a{1}.hp,fcovar.a{1}.lbls);
%set(hl,'FontSize', fcovar.lgndfontsize);

set(gca,'yscale','log');
set(gca,'xscale','log');

%ylim([10^-6 10]);
%xlim([0 max([iidrand.trials imax.trials])]);


%****************************************************************
%orthogonal to mean
fcovar.a{2}.ha=subplot(nrows,ncols,2)
fcovar.a{2}.xlabel='trial';
fcovar.a{2}.title='orthogonal';
fcovar.a{2}.semilog=1;
fcovar.a{2}.semilogx=1;

hold on;
fcovar.a{2}.hp=[];
fcovar.a{2}.lbls={};
pind=0;



%****************************************
%***********************************************
%I.I.D
%empirical
%orthogonal to theta
%**************************************************
%plot the i.i.d asymptotic variance
pind=length(fcovar.a{2}.hp)+1;
%compute the geometric mean
vexp=sum(log(irandemp.var(2:end,:)),1);
%vexp=vexp.^(1/(iidrand.thetalength-1));
vexp=(vexp/(size(irandemp.var,1)-1));
vexp=exp(vexp);
fcovar.a{2}.hp(end+1)=plot(irandemp.trials,vexp,getptype(pind,2));
fcovar.a{2}.lbls{end+1}='i.i.d. emp.';
fcovar.a{2}.ls{pind}=getbwlstyle(2);

%***********************************************
%I.I.D
%Predicted
%orthgonal to theta
%**************************************************
pind=pind+1;
fcovar.a{2}.hp(pind)=plot(1:simrand.niter,(1:simrand.niter).^-1*irand.asymevals(2));
fcovar.a{2}.lbls{pind}='i.i.d. pred.';
fcovar.a{2}.ls{pind}=getbwlstyle(4);

%***********************************************
%Info. Max
%empirical
%orthogonal to theta
%**************************************************
%plot the i.i.d asymptotic variance
pind=length(fcovar.a{2}.hp)+1;


%compute the geometric mean
vexp=sum(log(imaxemp.var(2:end,:)),1);
%vexp=vexp.^(1/(iidrand.thetalength-1));
vexp=(vexp/(size(imaxemp.var,1)-1));
vexp=exp(vexp);

fcovar.a{2}.hp(pind)=plot(imaxemp.trials,vexp);
fcovar.a{2}.lbls{pind}='info. max. emp.';
fcovar.a{2}.ls{pind}=getbwlstyle(1);

%******
%Info. Max
%Predicted
%orthogonal to theta
%*********************************************
%plot the asymptotic variance
pind=pind+1;
%cpred=imax.)*(1./(imax.trials(2:end)));

%fcovar.a{2}.hp(end+1)=plot([0 simparam.niter],covarexp(1)*[1 1],getptype(pind,2));
fcovar.a{2}.hp(pind)=plot(1:simmax.niter,(1:simmax.niter).^-1*imax.asymevals(2),getptype(pind,2));
fcovar.a{2}.lbls{pind}='info. max. pred.';
fcovar.a{2}.ls{pind}=getbwlstyle(3);





lblgraph(fcovar);





%legend(fcovar.a{2}.hp,fcovar.a{2}.lbls);
return;
%exportfcovar(fcovar.hf,fullfile(RESULTSDIR,'asymvar.eps'),'bounds','tight','color','rgb');
