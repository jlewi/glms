%plots for results of running posterior2d when there are multiple dimension%make a plot of the least squared error between
%the mean of the gaussian and the true mean after every iteration
lse=pmethods.g.m-ktrue*ones(1,numiter+1);
lse=lse.^2;
lse=sum(lse,1);
figure;
hold on;
h=[];
pind=1;
h(pind)=plot(1:numiter+1,log(lse),getptype(pind,1));
plot(1:numiter+1,log(lse),getptype(pind,2));
xlabel('Iteration');
ylabel('log(Distance)');