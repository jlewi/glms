%11-18
%
%Test accuracy of gaussian approximations of posterior
%Initially based on brute_force.m 
%but this includes modifications to deal with multiple dimensions in
%particular 2
clear all
close all;

setpaths;

%which methods to turn on
mon.g=0;
mon.ekf=0;
mon.batchg=0;
mon.ekfmax=0;
mon.ekf1step.on=1;
mon.ekf1step.ekffunc=@ekfposteriormax;    %pointer to the function to compute the ekf update.
                                        %can be ekfposteriorglm or
                                        %ekfposteriormax
mon.ekf1step.onestep=1;
mon.gasc=0;
%**************************************************************************
%simulation parameters
%*************************************************************************
%reseed the random number generator
simparam.rstate=25;
simparam.niter=100;

%************************************************************************
%analysis what results/analysis to collect. 
%see analyzeposteriors.m
%***********************************************************************
aopts.getstats=1;
aopts.mckl=0;       %compute kl divergence using monte carlo
                    %can take a very long time. 

%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;
RDIR=fullfile(RESULTSDIR,'posteriors', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));

%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
ropts.fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_pdfs_data.mat',datetime(2),datetime(3)));
[ropts.fname,trialindex]=seqfname(ropts.fname);
%ropts.fname='';
%ropts.fname='';


%options whether to compute kl divergence by brute force
popts.klbrute=0;

%movie options
%set mname to non blank if want to create a movie 
popts.movie.mname=fullfile(RDIR,sprintf('%0.2d_%0.2d_pdfs.avi',datetime(2),datetime(3)));
%start the number to pad the data at the same trial index number as the
%data number so movie and data have same number
popts.movie.mname=seqfname(popts.movie.mname,3,trialindex);
popts.movie.mname='';

%options for plotting functions
if ~(isempty(ropts.fname))
    popts.sgraphs=1;    %save graphs
else
    popts.sgraphs=0;
end

popts.sdir=RDIR;    %directory where to save results



%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window

%krange specifies the minimum and maximum value for the coefficents
%of k in the glm. 
%dk says what stepsize to use for storing k in the posterior
%krange is used to plot the results of the gaussians
mparam.tresponse=20;
mparam.krange=[-3 3];
mparam.dk=.0005;
mparam.tlength=1;           %length of temporal filter
mparam.ktrue=2;
mparam.dt =.005;            %used for sampling the spike trains
                            %I should switch to homogenous sampling so I
                            %don't need dt.

%**************************************************************************
%Update the posteriors
%**************************************************************************
[pmethods,sr,simparam]=updateposteriors(mparam,simparam,mon,ropts);

%*************************************************************************
%analysis
%**************************************************************************
%if we intend to make a movie we need discrete representations
if ~isempty(popts.movie.mname)
    aopts.getsamps=1;
else
    aopts.getsamps=0;
end
%save the analysis to whatever we save results to 
aopts.fname=ropts.fname;

%if we're not making a movie we don't need to get sampled versions
%of the pdfs which can be very expensive. 
[pmethods]=analyzeposteriors(pmethods,sr,mparam,aopts,simparam);

%**************************************************************************
%Plotting
%*************************************************************************
%Call the script which makes the plots
%script called will depend on the dimensionality
if (mparam.tlength==1)
    plotspost1d
else
    plotspost2d
end
    

