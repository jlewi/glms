%11-10
%
%Test accuracy of gaussian approximation of posterior
clear all
close all;

setpaths;
%reseed the random number generator
rstate=5;
randn('state',rstate);
rand('state',rstate);

%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
dt=.005;
tresponse=20;

%*************************************************************************
%simulation options
%************************************************************************
%movie options
%set mname to non blank if want to create a movie (do not include extension
%in name
datetime=clock;
opts.movie.mname=[RESULTSDIR '\brute_force\' sprintf('%0.2g_%0.2g_pdfs',datetime(2),datetime(3))];


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%krange specifies the minimum and maximum value for the coefficents
%of k in the glm. 
%dk says what stepsize to use for storing k in the posterior
krange=[-2 4];              
dk=.01;
kpts=[krange(1):dk:krange(2)];

%**************************************************************************
%True Model: 1-d temporal gaussian
%**************************************************************************
%Length of temporal filter (in time not samples)
ntsamples=1;                %number of samples in temporal filter
tlength=0+(ntsamples-1)*dt;

%time constants for temporal filter
tau1=1;
tau2=2;


ktrue=([0:dt:tlength])';
ktrue=(exp(-ktrue/tau1)-exp(-ktrue/tau2));
ktrue=2;
%clear variables no longer needed
clear ('tau1','tau2');

%*************************************************************************
%Generate Sample data
%************************************************************************
%generate random stimuli
numiter=100;                  %how many iterations to try
x=rand(ntsamples,numiter)*2-1;
tinfo.dt=dt;
tinfo.twindow=tresponse;
spikes=samp1dglmh(@glm1dexp,ktrue,x,tinfo);
clear('tinfo');

%**************************************************************************
%Compute the posterior after each iteration using a gaussian
%approximation and brute force
%**************************************************************************
%mean and covariance of initial prior
pinit.m=zeros(ntsamples,1);
pinit.c=eye(ntsamples,ntsamples);

%pb and pg are cell arrays which store the posterior after each
%iteration/(update of the posterior), pb{1}, pg{0} is the initial prior
%pb{i} - is a K dimensional array with each dimension size of krange/dk
%        the value of that element is the probability of that model (value)
%        for the coefficents after i-1 iterations

%initialize pb
pb=cell(1,numiter+1);

%initialize pg
pg=cell(1,numiter+1);
pg{1}=pinit;

%initialize pekf
pekf=cell(1,numiter+1);
pekf{1}=pinit;

%number of points at which we compute k
numkpts=diff(krange,1,2)/dk+1;

%represent the initial gaussian
[pb{1} kvals]=mvgaussmatrix(krange,dk,pinit.m,pinit.c);
pb{1}=pb{1}*dk;     %continuous to discrete approximation.

%initialize pb to zero matrices
for tr=2:numiter+1
    if (ntsamples==1)
         pb{tr}=zeros(numkpts,1);
    else
        pb{tr}=zeros(numkpts*ones(1,ntsamples));
    end
end

%clean up temp variables
clear('m','dindex','tr');

%**************************************************************************
%EKF: Compute the Gaussian Approximation of the posterior using the EKF idea
%**************************************************************************
fprintf('Compute the ekf approximation of the posterior \n');
numspikes=sum(spikes,2);
obsrv.twindow=tresponse;

for tr=2:numiter+1
    fprintf('EKF iteration %0.2g \n',tr-1);
    if (tr==22)
        disp('22');
    end
    %construct the observations
    obsrv.n=numspikes(tr-1);
    
    [pekf{tr}]=ekfposteriorglm(pekf{tr-1},(x(:,tr-1)),obsrv,@glm1dexp);
    
end

%**************************************************************************
%Compute the Gaussian Approximation of the posterior
%**************************************************************************
fprintf('Compute the gaussian approximation of the posterior \n');

numspikes=sum(spikes,2);
obsrv.twindow=tresponse;

for tr=2:numiter+1
    fprintf('Gausss Approx: iteration %0.2g \n',tr-1);
    %construct the observations
    obsrv.n=numspikes(tr-1);
    
    [pg{tr}]=gposteriorglm(pg{tr-1},(x(:,tr-1)),obsrv,@glm1dexp);
    
end

%************************************************************************
%compute the likelihood of the data using brute force
%************************************************************************
%we want to compute the likelihood of each observation under each possible
%model
%llobsrv -= m x n
%        m - the different observations (correspond to cols of y)
%        n - different models under which likelihood is computed 
%

llobsrv=ll1dtemp(spikes,x',kvals',dt);

%***************************************************************
%compute the posterior by brute force
%***************************************************************
for tr=2:numiter+1
    %reshape the likelihood for this observation under all the different
    %models
    %and raise it to the exponential
    if (ntsamples==1)
        pobsrv=exp(reshape(llobsrv(tr-1,:),numkpts*ones(1,ntsamples),1));
    else
        pobsrv=exp(reshape(llobsrv(tr-1,:),numkpts*ones(1,ntsamples)));
    end
    
    pb{tr}=pobsrv.*pb{tr-1};
    
    %normalize it
    z=sum(pb{tr}(:));
    pb{tr}=1/z*pb{tr};
end

clear('tr');

%***************************************************************
%compute the posterior by brute force
%using the gaussian prior
%***************************************************************
%so for each update we use brute force but the prior is the gaussian prior
%not the exact prior
pbg=cell(1,numiter+1);
pbg{1}=pb{1};

for iter=2:numiter+1
    fprintf('Brute Force: Iteration %0.2g \n',iter-1);
    
    %reshape the likelihood for this observation under all the different
    %models
    %and raise it to the exponential
    if (ntsamples==1)
        pobsrv=exp(reshape(llobsrv(iter-1,:),numkpts*ones(1,ntsamples),1));
    else
        pobsrv=exp(reshape(llobsrv(iter-1,:),numkpts*ones(1,ntsamples)));
    end
    
    %construct a matrix to represent the gaussian prior
    %multiply by dk to handle the continous to discrete conversion
    priormat=mvgaussmatrix(krange,dk,pg{iter-1}.m,pg{iter-1}.c)*dk;
    pbg{iter}=pobsrv.*priormat;
    
    %normalize it
    z=sum(pbg{iter}(:));
    pbg{iter}=1/z*pbg{iter};
end

clear('priormat');




%compute the mean and variance of the prior
%under all three forms and plot as function of iteration
%each row is a different trial
prior=[];
prior.mg=zeros(numiter,ntsamples);
prior.mb=zeros(numiter,ntsamples);
prior.mbg=zeros(numiter,ntsamples);
prior.cg=cell(1,numiter);
prior.cb=cell(1,numiter);
prior.cbg=cell(1,numiter);

for tr=1:numiter+1
    prior.mg(tr,:)=pg{tr}.m;
    prior.mekf(tr,:)=pekf{tr}.m;
    prior.mb(tr,:)=sum(pb{tr}.*kpts');
    prior.mbg(tr,:)=sum(pbg{tr}.*kpts');
    
    prior.cg{tr}=pg{tr}.c;
    prior.cekf{tr}=pekf{tr}.c;
    prior.cb{tr}=zeros(ntsamples,ntsamples);
    prior.cbg{tr}=zeros(ntsamples,ntsamples);
    for kind=1:numkpts
        prior.cb{tr}=prior.cb{tr}+pb{tr}(kind)*(kpts(:,kind)-prior.mb(tr,:)')*(kpts(:,kind)-prior.mb(tr,:)')';
        prior.cbg{tr}=prior.cbg{tr}+pbg{tr}(kind)*(kpts(:,kind)-prior.mbg(tr,:)')*(kpts(:,kind)-prior.mbg(tr,:)')';        
    end
end





%*****************
%plot the means
figure;
hold on;
t=[0:numiter];
h_p=zeros(1,3);

pind=1;
h_p(pind)=plot(t,prior.mg,getptype(pind,1));
plot(t,prior.mg,getptype(pind,2));
lbls{pind}='Gaussian approximation';

pind=2;
h_p(pind)=plot(t,prior.mb,getptype(pind,1));
plot(t,prior.mb,getptype(pind,2));
lbls{pind}='Brute force';

pind=3;
h_p(pind)=plot(t,prior.mbg,getptype(pind,1));
plot(t,prior.mbg,getptype(pind,2));
lbls{pind}='Brute force with gaussian';

pind=4;
h_p(pind)=plot(t,prior.mekf,getptype(pind,1));
plot(t,prior.mekf,getptype(pind,2));
lbls{pind}='EKF approx';

legend(h_p,lbls);
xlabel('Iteration');
ylabel('Mean of \theta');

%*****************
%plot the variances
figure;
hold on;
t=[0:numiter];
h_p=zeros(1,3);

pind=1;
h_p(pind)=plot(t,cell2mat(prior.cg),getptype(pind,1));
plot(t,cell2mat(prior.cg),getptype(pind,2));
lbls{1}='Gaussian Approx.';

pind=2;
h_p(pind)=plot(t,cell2mat(prior.cb),getptype(pind,1));
plot(t,cell2mat(prior.cb),getptype(pind,2));
lbls{2}='Brute Force';

pind=3;
h_p(pind)=plot(t,cell2mat(prior.cbg),getptype(pind,1));
plot(t,cell2mat(prior.cbg),getptype(pind,2));
lbls{pind}='Brute Force with Gaussian';

pind=4;
h_p(pind)=plot(t,cell2mat(prior.cekf),getptype(pind,1));
plot(t,cell2mat(prior.cekf),getptype(pind,2));
lbls{pind}='EKF approximation';

legend(h_p,lbls);
xlabel('Iteration');
ylabel('Variance of \theta');


%*********************************************************
%Plots
%*********************************************************

%plot the gaussian approximation and brute force on same graph
%compute the gaussian and account for discretization error
%construct the data for slideshow
sdata=[];
sdata.pb=pb;
sdata.pbg=pbg;
pparam.x=kpts;
% Compute the KL divergence
kldist=zeros(2,numiter+1);

for tr=1:numiter+1
    %multiply by dk to handle continuous to discrete approximation.
    sdata.pg{tr}=mvgauss(kpts,pg{tr}.m,pg{tr}.c)*dk;
    sdata.pekf{tr}=mvgauss(kpts,pekf{tr}.m,pekf{tr}.c)*dk;
    kldist(1,tr)=kldiv((pb{tr})',sdata.pg{tr});
    kldist(2,tr)=kldiv((pb{tr})',sdata.pekf{tr});
    
end

figure;
%h_p=[];
pind=1;
subplot(2,1,1);
hold on;
h_p(pind)=plot([0:numiter],kldist(1,:),getptype(pind,2));
plot([0:numiter],kldist(1,:),getptype(pind,1));
xlabel('Iteration');
ylabel('KL Divergence');
title('Gaussian approximation');


subplot(2,1,2);
hold on;
pind=2;
h_p(pind)=plot([0:numiter],kldist(2,:),getptype(pind,2));
plot([0:numiter],kldist(2,:),getptype(pind,1));
xlabel('Iteration');
ylabel('KL Divergence');
title('EKF');



%plot the stimulus
figure;
plot(x,'b');
xlabel('Iteration');
ylabel('Stimulus');


slideshowf(@compare1d,sdata,pparam,opts);
