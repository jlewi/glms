%12_07
%Script to fit a line of 1/n^.5 to the means
%to test thats how it decreases

%fit line of y(n)=c/n^.5+b
%where n is iteration to the mean
%
if ~(exist('fmean','var'))
    fmean.hf=figure;
end
strain=100;
ytrain=pmethods.ekfmax.m(1,strain+1:simparam.niter);
ytrain=ytrain';
xtrain=ones(simparam.niter-strain,2);
xtrain(:,1)=1./(strain:simparam.niter-1);

%vector of parameters [c b]
cp=inv(xtrain'*xtrain)*xtrain'*ytrain;

x=ones(simparam.niter,2);
x(:,1)=1./(1:simparam.niter);

yfit=x*cp;
figure;
plot(1:simparam.niter,yfit);

figure(fmean.hf);
hold on;
plot(1:simparam.niter,yfit,'k-');


%plot sqrt(n)*(pmethods.m-pmethods.batchg.m
figure;
hold on;
y=(0:simparam.niter).^.5;
%y=y.*(pmethods.ekfmax.m-pmethods.batchg.m);
y=y.*(pmethods.ekf1step.m-pmethods.batchg.m);
plot(0:simparam.niter,y,'k-');
xlabel('Iteration');
ylabel('n^.^5(\mu_E_K_F-\mu_b_a_t_c_h)')
