%11-20 
%Script to handle the plotting of results from posterior2d
%in the case date is two dimensions

%construct a list of enabled methods
mon={};
mnames=fieldnames(pmethods);
for index=1:length(fieldnames(pmethods))
    if pmethods.(mnames{index}).on >0
        mon{length(mon)+1}=mnames{index};
    end
end

%*****************
%figures

fkl.hf=[];      %only create the figure if we have something to plot
fkl.hp=[];
fkl.x=[0:simparam.niter];
fkl.xlabel='Iteration';
fkl.ylabel='Distance';
fkl.title='Kl Divergence';
fkl.on=0;
fkl.lbls={};

fmdist.hf=figure;
fmdist.hp=[];
fmdist.x=[0:simparam.niter];
fmdist.xlabel='Iteration';
fmdist.ylabel='Distance';
fmdist.title='Distance From Mean to True Value';
fmdist.on=1;
fmdist.fname='mdist';
fmdist.lbls={};

%entropy
fentropy.hf=figure;
fentropy.hp=[];
fentropy.x=[0:simparam.niter];
fentropy.xlabel='Iteration';
fentropy.ylabel='Entropy';
fentropy.title='';
fentropy.on=1;
fentropy.fname='entropy';
fentropy.lbls={};

%mean squared error
fmse.hf=figure;
fmse.hp=[];
fmse.x=[0:simparam.niter];
fmse.xlabel='Iteration';
fmse.ylabel='MSE';
fmse.title='';
fmse.on=1;
fmse.fname='mse';
fmse.lbls={};
pind =0;

%loop through enabled methods
for mindex=1:length(mon)
         pind=pind+1;
         %plot the kl distance
        
        
        if isfield(pmethods.(mon{mindex}),'dk')
            if (isempty(fkl.hf))
                fkl.hf=figure;
            else
                figure(fkl.hf);
            end
            hold on;
            fkl.on=1;
            fkl.hp(pind)=plot(fkl.x,pmethods.(mon{mindex}).dk(1,:),getptype(pind,1));
            plot(fkl.x,pmethods.(mon{mindex}).dk(1,:),getptype(pind,2));
            fkl.lbls{pind}=sprintf('Monte Carlo: %s', pmethods.(mon{mindex}).label);   
        end

        %***************************************************************
        %figure of the least mean squared distance of mean and true mean
        %*********************************************************
        figure(fmdist.hf);
        pind=length(fmdist.hp)+1;
        hold on;
        lse=pmethods.(mon{mindex}).m-mparam.ktrue*ones(1,simparam.niter+1);
        lse=lse.^2;
        lse=sum(lse,1);
        lse=lse.^.5;
        fmdist.lbls{pind}=pmethods.(mon{mindex}).label;
        
        fmdist.hp(pind)=plot(fmdist.x,lse,getptype(pind,1));
        plot(fmdist.x,lse,getptype(pind,2));
        
        %***************************************************************
        %figure of the posterior entropy
        %*********************************************************
        figure(fentropy.hf);
        if (strcmp(mon{mindex},'gasc')~=1)
        pind=length(fentropy.hp)+1;
        hold on;
        entropy=zeros(1,simparam.niter+1);
        for index=0:simparam.niter
            entropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(pmethods.(mon{mindex}).c{index+1})));
        end
        fentropy.hp(pind)=plot(fentropy.x,entropy,getptype(pind,1));
        plot(fentropy.x,entropy,getptype(pind,2));

		fentropy.lbls{pind}=pmethods.(mon{mindex}).label;
        
          %***************************************************************
        %Mean Squared Error
        %*********************************************************
        figure(fmse.hf);
        pind=length(fmse.hp)+1;
        hold on;
        err=exmse(pmethods.(mon{mindex}),mparam.ktrue);
        fmse.hp(pind)=plot(fmse.x,err,getptype(pind,1));
        plot(fmse.x,err,getptype(pind,2));

		fmse.lbls{pind}=sprintf('%s',pmethods.(mon{mindex}).label);
        end
end

%if (fkl.on~=0)
%figure(fkl.hf);
%legend(fkl.hp,fkl.lbls);
%xlabel(fkl.xlabel);
%ylabel(fkl.ylabel);
%title(fkl.title);
%end

%label all the figures
figs={fmdist,fentropy,fkl,fmse};
for index=1:length(figs)
    if (figs{index}.on~=0)
        figure(figs{index}.hf);
        legend(figs{index}.hp,figs{index}.lbls);
        xlabel(figs{index}.xlabel);
        ylabel(figs{index}.ylabel);
        title(figs{index}.title);
       if (popts.sgraphs ~=0)
        %create filename for graph
        if ~(isempty(figs{index}.fname))
        fname=fullfile(popts.sdir,sprintf('%0.2d_%0.2d_%s.jpg',datetime(2),datetime(3),figs{index}.fname));
        fname=seqfname(fname);
        savegraph(fname,figs{index}.hf);
        end
       end
    end
end


%**************************************************************************
%Movie of PDF's?
%**************************************************************************
if ~isempty(popts.movie.mname)
%slideshowf(@compare1d,sdata,pparam,opts);
for mindex=1:length(mon)
        sdata=cell(1,simparam.niter+1);
        for index=1:simparam.niter+1
            pdata.m{index}=pmethods.(mon{mindex}).m(:,index);
            pdata.c{index}=pmethods.(mon{mindex}).c{index};
        end
        pparam=[];
        opts=[];
        mparam.ktrue=ktrue;
        mparam.title=pmethods.(mon{mindex}).label;
        slideshowf(@gausspost2d,pdata,pparam,opts);
end
end