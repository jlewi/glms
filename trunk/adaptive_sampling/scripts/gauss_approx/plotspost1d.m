%11-20
%
%Script to handle plotting of results from posterior2d 
%in case data is 1dimensionals

%************************************************************************
%Set Default Options
%**************************************************************************
%*
%construct a list of enabled methods
mon={};
mnames=fieldnames(pmethods);
for index=1:length(fieldnames(pmethods))
    if pmethods.(mnames{index}).on >0
        mon{length(mon)+1}=mnames{index};
    end
end

if ~isfield(popts,'sgraphs')
    popts.sgraphs=0;
end

if (popts.sgraphs~=0)
    if ~isfield(popts,'sdir')
        %this will end up causing it to be saved to the working director;
        popts.sdir='';
    end
end

if ~isfield(popts,'movie')
    popts.movie.mname='';
end

if ~isfield(popts.movie,'mname')
    popts.movie.mname='';
end

clear('mnames');
%**************************************************************************
%Set up the structures to store parameters of each graph
%**************************************************************************
clear('fmean','fvar','fkl','fll','fstim','flldiff');
%figures
fmean.hf=figure;
fmean.xlabel='Iteration';
fmean.title='Mean';
fmean.name='post_mean';  %used for saving graph
fmean.ylabel='';
fmean.on=1;

fvar.title='';  
fvar.xlabel='Iteration';
fvar.ylabel='Variance';
fvar.name ='post_var';  
fvar.hp=[];
fvar.hf=figure;
fvar.on=1;

fkl.hf=figure;
fkl.xlabel='Iteration';
fkl.title='KL Divergence';
fkl.name ='post_kl';
fkl.ylabel='';

fmean.hp=[];
fvar.hp=[];
fkl.hp=[];

fmean.x=[0:simparam.niter];
fvar.x=[0:simparam.niter];
fkl.x=[0:simparam.niter];
fkl.on=0;

%plot of the stimuluus and response
fstim.hf=figure;
fstim.x=[1:simparam.niter];
fstim.name='stimresponse';
fstim.hp=[];
fstim.xlabel='iteration';
fstim.ylabel='Stimulus';
fstim.title='';
fstim.on=1;

%plot of the mean of the likelihood funciton on each iteration
fll.hf=figure;
fll.x=[1:simparam.niter];
fll.name='stimresponse';
fll.hp=[];
fll.xlabel='iteration';
fll.ylabel='Mean of Likelihood';
fll.title='Peak of Taylor Approximation of Likelihood Fuction';
fll.on=0;

%plot of the difference of the peak of the taylor approximation
%and the true ML estimate on each trial
flldiff.hf=figure;
flldiff.x=[1:simparam.niter];
flldiff.name='stimresponse';
flldiff.hp=[];
flldiff.xlabel='iteration';
flldiff.ylabel='Difference';
flldiff.title='Difference Peak of Taylor Approximation of Likelihood Fuction and True ML Estimate';
flldiff.on=0;

%mean squared error
fmse.hf=figure;
fmse.hp=[];
fmse.x=[0:simparam.niter];
fmse.xlabel='Iteration';
fmse.ylabel='MSE';
fmse.title='';
fmse.on=1;
fmse.fname='mse';
%**************************************************************************
%Plot stimulus
%**************************************************************************
figure(fstim.hf);
subplot(2,1,1);
plot(fstim.x,sr.y);
ylabel('Stimulus');
subplot(2,1,2);
plot(fstim.x,sr.nspikes);
ylabel('#spikes');
xlabel('Iteration');

%**************************************************************************
%Plot Posterior Mean and Variance
%Plot KL Divergence
%**************************************************************************
pind =0;
%loop through enabled methods
%as we do this we want to calculate what the ylimits should be to capture
%some percent of the data given by popts.yplim;
%popts.yplim;
for mindex=1:length(mon)
    pind=pind+1;
    %plot mean
    figure(fmean.hf);
    hold on;
    fmean.hp(pind)=plot(fmean.x,pmethods.(mon{mindex}).m(1,:),getptype(pind,1));
    plot(fmean.x,pmethods.(mon{mindex}).m,getptype(pind,2));
    fmean.lbls{pind}=pmethods.(mon{mindex}).label;


    %plot variance
    figure(fvar.hf);
    hold on;
    fvar.hp(pind)=plot(fvar.x,cell2mat(pmethods.(mon{mindex}).c),getptype(pind,1));
    plot(fmean.x,cell2mat(pmethods.(mon{mindex}).c),getptype(pind,2));
    fvar.lbls{pind}=pmethods.(mon{mindex}).label;


    %plot the kl distance
    figure(fkl.hf);
    hold on;
    if isfield(pmethods.(mon{mindex}),'dk')
        fkl.on=1;
        kpind=length(fkl.hp)+1;
        fkl.hp(kpind)=plot(fkl.x,pmethods.(mon{mindex}).dk(1,:),getptype(pind,1));
        plot(fkl.x,pmethods.(mon{mindex}).dk(1,:),getptype(pind,2));
        fkl.lbls{kpind}=sprintf('Monte Carlo: %s', pmethods.(mon{mindex}).label);
    end

     %***************************************************************
        %Mean Squared Error
        %*********************************************************
        figure(fmse.hf);
        hold on;
        err=exmse(pmethods.(mon{mindex}),mparam.ktrue);
        fmse.hp(pind)=plot(fmse.x,err,getptype(pind,1));
        plot(fmse.x,err,getptype(pind,2));

		fmse.lbls{pind}=sprintf('%s',pmethods.(mon{mindex}).label);

end

%add the true parameter to the plot of the mean
figure(fmean.hf)
pind=pind+1;
fmean.hp(pind)=plot(fmean.x,mparam.ktrue*ones(1,length(fmean.x)),getptype(pind,1));
plot(fmean.x,mparam.ktrue*ones(1,length(fmean.x)),getptype(pind,2));
fmean.lbls{pind}='\theta_t_r_u_e';

figure(fkl.hf);
hold on;

%plot the brute force measurement of kl as well
if exist('kldist','var')
    if isfield(kldist,'gb');
        fkl.on=1;
        pind=length(fkl.lbls)+1;
        fkl.hp(pind)=plot(fkl.x,kldist.gb,getptype(pind,1));
        plot(fkl.x,kldist.gb,getptype(pind,2));
        fkl.lbls{pind}=sprintf('Brute Force: %s Gaussian');
    end
end

if (fkl.on~=0)
    figure(fkl.hf);
    legend(fkl.hp,fkl.lbls);
    xlabel(fkl.xlabel)
    title(fkl.title);
else
    close(fkl.hf);
    fkl.hf=[];
end


%*************************************************************************
%compare the ML estimates after each iteration using
%the ekf update and the true likelihood via brute force
%*************************************************************************
fml.hf=figure;
fml.hp=[];
fml.xlabel='Iteration';
fml.ylabel='ML Estimate';
fml.name='max_ll';
fml.on=1;
fml.title='';
%add the fields to plo
pind=0;
figure(fml.hf);
hold on;
if (pmethods.ekf.on~=0)
    pind=pind+1;
    fml.hp(pind)=plot(1:simparam.niter,pmethods.ekf.ll.m,getptype(pind,1));
       
    plot(1:simparam.niter,pmethods.ekf.ll.m,getptype(pind,2));
    fml.lbls{pind}='EKF';
end

if (pmethods.batchg.on~=0)
    pind=pind+1;
    fml.hp(pind)=plot(1:simparam.niter,pmethods.batchg.mll,getptype(pind,1));
    plot(1:simparam.niter,pmethods.batchg.mll,getptype(pind,2));
    fml.lbls{pind}='batch update';
end

%********************************************************************
%Plot the Peak of the taylor approximation of the ekf
%***********************************************************************
if (pmethods.ekf.on~=0)
    if isfield(pmethods.ekf,'ll')
        fll.on=1;
        pind=1;
        figure(fll.hf);
        hold on;
        fll.hp(pind)=plot(1:simparam.niter,pmethods.ekf.ll.m,getptype(pind,1));
        plot(1:simparam.niter,pmethods.ekf.ll.m,getptype(pind,2));
        fll.lbls{pind}='Taylor';
        
        
        %true mle estimates 
        mle=log(sr.nspikes/mparam.tresponse).*(sr.y.^-1);
        pind=pind+1;
        fll.hp(pind)=plot(1:simparam.niter,mle,getptype(pind,1));
        plot(1:simparam.niter,mle,getptype(pind,2));
        fll.lbls{pind}='True MLE';
        
        
    end
end
%********************************************************************
%Plot the difference Peak of the taylor approximation of the ekf
%and true mle
%***********************************************************************
if (pmethods.ekf.on~=0)
    if isfield(pmethods.ekf,'ll')
        flldiff.on=1;
        pind=1;
        figure(flldiff.hf);
        hold on;
        flldiff.hp(pind)=plot(1:simparam.niter,pmethods.ekf.ll.m-mle,getptype(pind,1));
        plot(1:simparam.niter,pmethods.ekf.ll.m-mle,getptype(pind,2));
        flldiff.lbls{pind}='Taylor';
          
    end
end

%**************************************************************************
%save+Label the graphs if desired
%*********************************************************************
%create structure of all the graphs
figs={fstim,fkl,fmean,fvar,fml,fll,flldiff};

datetime=clock;



for index=1:length(figs)
    if (figs{index}.on~=0)
   %label the plot
    figure(figs{index}.hf);
    if ~isempty(figs{index}.hp)
        legend(figs{index}.hp,figs{index}.lbls);
    end
    xlabel(figs{index}.xlabel);
    title(figs{index}.title);
    ylabel(figs{index}.ylabel);

    

    if (popts.sgraphs ~=0)
        %create filename for graph
        fname=fullfile(popts.sdir,sprintf('%0.2d_%0.2d_%s.eps',datetime(2),datetime(3),figs{index}.name));
        fname=seqfname(fname);
        savegraph(fname,figs{index}.hf);
    end
    
    end
end


%**************************************************************************
%Movie of PDF's?
%**************************************************************************
if ~isempty(popts.movie.mname)
    %we need to construct the data structure for the movie
    %plot the gaussian approximation and brute force on same graph
    sdata=[];
    pparam=[];
    %max value for plotting true k
    
    for tr=1:simparam.niter
        sdata.ttrue{tr}.y=0;
    if (pmethods.b.on~=0)
        sdata.pb{tr}.y=pmethods.b.p{tr};
        sdata.pb{tr}.x=pmethods.b.theta{tr};
    
        %height for true k
        sdata.ttrue{tr}.y=[sdata.ttrue{tr}.y, max(sdata.pb{tr}.y)];
        
    end
        
    if (pmethods.bg.on~=0)
        sdata.pbg{tr}.y=pmethods.bg.p{tr};
        sdata.pbg{tr}.x=pmethods.bg.theta{tr};
        
        %height for true k
        sdata.ttrue{tr}.y=[sdata.ttrue{tr}.y, max(sdata.pbg{tr}.y)];
        
    end
    
    %the following methods all get processed the same
    %use the structure to apply processing to all of them
    smsame={'g','batchg','ekf'};
    for findex=1:length(smsame)
        if (pmethods.(smsame{findex}).on~=0)
            sdata.(smsame{findex}){tr}.y=pmethods.(smsame{findex}).p{tr};
            sdata.(smsame{findex}){tr}.x=pmethods.(smsame{findex}).theta{tr};
            %height for true k
            sdata.ttrue{tr}.y=[sdata.ttrue{tr}.y, max(sdata.(smsame{findex}){tr}.y)];
        end
    end
    
   
    sdata.ttrue{tr}.x=mparam.ktrue*ones(1,length(sdata.ttrue{tr}.y));
    
    end
    slideshowf(@compare1d,sdata,pparam,popts);
end