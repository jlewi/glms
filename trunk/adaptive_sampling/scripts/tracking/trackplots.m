%3-30-2006
%Tracking plots


%11-20
%Script to handle the plotting of results from posterior2d
%in the case date is two dimensions



%*****************
%figures
axisfontsize=30;
lgndfontsize=10;


ftheta.hf=figure;
ftheta.hp=[];
ftheta.x=[0:simparam.niter];
ftheta.xlabel='Iteration';
ftheta.ylabel='';
ftheta.title='';
ftheta.on=1;
ftheta.fname='theta';
ftheta.name='Components of Theta';
ftheta.semilog=0;
if (prod(size(mparam.diffusion))==1)
    ftheta.on=0;
end

femax.hf=figure;
femax.hp=[];
femax.x=[0:simparam.niter];
femax.xlabel='Iteration';
femax.ylabel='Correlation';
femax.title='';
femax.on=1;
femax.fname='emax';
femax.name='Correlation stim and max evector';
femax.semilog=0;


fevals.hf=figure;
fevals.hp=[];
fevals.x=[0:simparam.niter];
fevals.xlabel='Iteration';
fevals.ylabel='Max Eigenvalue';
fevals.title='';
fevals.on=1;
fevals.fname='evalmax';
fevals.name='Max Eigen values';
fevals.semilog=0;
%entropy
pind =0;

%methods structure
%specifies which method structures to check for
%i.e prand - contains data when random stimuli are sued
%     pmax - contains results when optimal stimuli are used
%     pignore - ignores spike history.
methods.pmax.vname='pmax';
methods.pmax.label='InfoMax';
methods.prand.vname='prand';
methods.prand.label='Random';
methods.pignore.vname='pignore';
methods.pignore.label='Ignore spike history';


%if (methodind==1)
%   pmethods=pmax;
%  lbltxt='Max';
%else
%    pmethods=prand;
%    lbltxt='Rnd';
%end

%construct a list of enabled methods
monf={};
mnames=fieldnames(pmax);
for index=1:length(fieldnames(pmax))
    if pmax.(mnames{index}).on >0
        monf{length(monf)+1}=mnames{index};
    end
end

%compute thetatrue a matrix to represent true parameter value
%at each iteration
%need to take into account whether we have spike history terms
%and if we allow parameters to change over time.
if isfield(mparam,'atrue')
    thetatrue=[mparam.ktrue; mparam.atrue];
else
    thetatrue=[mparam.ktrue];
end

if (size(mparam.ktrue,2)>1)
    thetatrue=[mparam.ktrue];
else
    thetatrue=thetatrue* ones(1,simparam.niter+1);
end


%make a plot of theta on each iteration
if (ftheta.on==1)
figure(ftheta.hf);
tpind=0;
for (tindex=1:size(thetatrue,1));
    hold on
    tpind=tpind+1;
    ftheta.lbls{tpind}=sprintf('\\theta_%d', tindex);
    ftheta.hp(tpind)=plot(1:simparam.niter,thetatrue(tindex,:),getptype(tpind,1));
    plot(1:simparam.niter,thetatrue(tindex,:),getptype(tpind,2));
end
end

%**************************************************************************
%Eigenvalues
%**************************************************************************
%Plot the maximum eigenvalue of the diffusion matrix vs. that of the
%posterior
figure(fevals.hf);
hold on;
    cevals=zeros(1,simparam.niter);
    for index=1:simparam.niter
       [d]=svd(pmax.newton1d.c{index+1});
       cevals(1,index)=d(1);
    end
    pind=1;
    fevals.lbls{pind}='\lambda_0 of posterior';
    fevals.hp(pind)=plot(1:simparam.niter,cevals,getptype(pind,1));
    plot(1:simparam.niter,cevals,getptype(pind,2));

    %plot the max eigenvalue of the diffusion matrix
    pind=2;
    d=svd(mparam.diffusion);
    fevals.lbls{pind}='\lambda_0 of diffusion';
    fevals.hp(pind)=plot(1:simparam.niter,d(1)*ones(1,simparam.niter),getptype(pind,1));
    plot(1:simparam.niter,d(1)*ones(1,simparam.niter),getptype(pind,2));

    
   %***********************************************************************
%make a plot of the correlation of the stimulus on each iteration and the
%max eigenvector;
figure(femax.hf);
hold on;
[evecs d]=svd(mparam.diffusion);
emaxcorr=evecs(:,1)'*srmax.newton1d.y;

    femax.hp(1)=plot(1:simparam.niter,emaxcorr,getptype(1,1));
    femax.lbls{1}='Optimal Stimulus';
    plot(1:simparam.niter,emaxcorr,getptype(1,2));

    pind=2;
emaxcorr=evecs(:,1)'*(thetatrue./((ones(size(thetatrue,1),1))*(sum(thetatrue.^2,1)).^.5));
    femax.lbls{pind}='\theta';
    femax.hp(pind)=plot(1:simparam.niter,emaxcorr,getptype(pind,1));
    plot(1:simparam.niter,emaxcorr,getptype(pind,2));

   %add the correlation of the principal eigenvector of the posterior
   %(After incorporating the diffusion) and the max eigenvector of the
   %diffsuion 
   %also plot the correlation of the principal eigenvector with the
   %previous principal eigenvector
   pind=3;
   ecorr=zeros(1,simparam.niter);
   index=1;
    [dvecs s]=svd(pmax.newton1d.c{index+1}+mparam.diffusion);
    ecorr=dvecs(:,1)'*evecs(:,1);
   smaxlast=dvecs(:,1);
   
   emaxitself=zeros(1,simparam.niter);
   for index=2:simparam.niter
       [dvecs s]=svd(pmax.newton1d.c{index+1}+mparam.diffusion);
       ecorr=dvecs(:,1)'*evecs(:,1);
       emacitself=dvecs(:,1)'*smaxlast;
       smaxlast=dvecs(:,1);
   end
    femax.lbls{pind}='Principal eigenvector of posterior with diffusion';
    femax.hp(pind)=plot(1:simparam.niter,emaxcorr,getptype(pind,1));
    plot(1:simparam.niter,emaxcorr,getptype(pind,2));
   
    pind=4;
    femax.lbls{pind}='Principal eigenvector of posterior on this and last itetration';
    femax.hp(pind)=plot(1:simparam.niter,emaxcorr,getptype(pind,1));
    plot(1:simparam.niter,emaxcorr,getptype(pind,2));
   
%if (fkl.on~=0)
%figure(fkl.hf);
%legend(fkl.hp,fkl.lbls);
%xlabel(fkl.xlabel);
%ylabel(fkl.ylabel);
%title(fkl.title);
%end

%label all the figures
figs={ftheta,femax,fevals};
%which ones to do on semilog axis

for index=1:length(figs)
    if (figs{index}.on~=0)
        %specify any common properties
        figs{index}.axisfontsize=axisfontsize;
        figs{index}.lgndfontsize=lgndfontsize;
        lblgraph(figs{index});
        %figure(figs{index}.hf);
        %set(gca,'FontSize',axisfontsize);
        %if isfield(figs{index},'lbls')
        %hl=legend(figs{index}.hp,figs{index}.lbls);
        %end
        %set(hl,'FontSize',lgndfontsize);
        %xlabel(figs{index}.xlabel);
        %ylabel(figs{index}.ylabel);

        %title(figs{index}.title);
        %set(figs{index}.hf,'name',figs{index}.name);
        %if (isfield(figs{index},'semilog'))
      %      if (figs{index}.semilog~=0)
       %         set(gca,'YScale','Log');
     %       end
   %     end

        if (popts.sgraphs ~=0)
            %create filename for graph
            fname=fullfile(popts.sdir,sprintf('%0.2d_%0.2d_%s.eps',datetime(2),datetime(3),figs{index}.fname));
            fname=seqfname(fname);
            savegraph(fname,figs{index}.hf);

        end

    end


end