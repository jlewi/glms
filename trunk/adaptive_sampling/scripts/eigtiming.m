%**************************************************************************
%Script to look at the timing of an eigendecomposition 
%when we use the rank 1 update
%**************************************************************************

dmat=d*ones(1,2)-ones(2,1)*eigd';
pdmat=prod(dmat,2);


n=254;
index=1;
    %we need to sort the eigenvalues in order for the call to dlaed to work
    d=sort(rand(n,1));
    %truncate the eigenvalues at 3 pts of precision

    d=floor(d*10^4)/1000;

    u=floor(rand(n,1)*10^4)/1000;
    
    %try normalizing it so d and u are normal
    d=d/(d'*d)^.5;
    u=u/(u'*u)^.5;
    %generate some old eigenvectors
    evold=orth(rand(n,n));
    %evold=eye(n);
 
    
    %make 3 and 2 diagonal element the same
    %do this to test deflation
    d(3)=d(2);
    
       %[eigd,delta]=mexRankOneEig(d,u);
       [eigd,evecs]=rankOneEigUpdate(d,evold'*u,evold);
    
       %compute the mse
       mse=(evold*diag(d)*evold'+u*u')*evecs-evecs*diag(eigd);
       mse=sum(mse.^2,1);
       
       dmat=d*ones(1,n)-ones(n,1)*eigd';
       
       %compare 
       eigtrue=eig(evold*diag(d)*evold'+u*u');
       eigtrue=sort(eigtrue);
       
       %diffevals
       diffevals=eigtrue-eigd;
       
       [eigtrue evtrue]=eig(evold*diag(d)*evold'+u*u');
       mag=(sum(evtrue.^2,1)).^.5;
       mag=ones(n,1)*mag;
       evtrue=mag.*evtrue;
       [eigtrue sind]=sort(diag(eigtrue));
       evtrue=evtrue(:,sind);
       
       [evtrue etrue]=svd(evold*diag(d)*evold'+u*u');
       etrue=diag(etrue);
       etrue=etrue(end:-1:1);
       evtrue=evtrue(:,end:-1:1);
