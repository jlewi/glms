%script to look at the correlation of stimulus on different trials
%get the stim
y=srmax.newton.y;
stimcorr=y'*y;
figure;
imagesc(stimcorr);
colorbar;
title('Newton Infomax');

y=srrand.y;
stimcorr=y'*y;
figure;
imagesc(stimcorr);
colorbar;
title('Newton Rand');

%**************************************************************************
%Measure the energy projection onto the eigenvalues
eigenergy=zeros(size(srmax.newton.y));
for index=1:size(srmax.newton.y,2)
    %get eigenvectors from previous iteration as these are the ones
    %that affect the fmaxlambda function
    [u,s]=svd(pmax.newton.c{index});
    eigenergy(:,index)=u'*srmax.newton.y(:,index);
end

numplots=min(4,mparam.tlength);
figure;

for index=1:numplots
subplot(numplots,1,index);
plot(eigenergy(index,:));
ylim([-1.1 1.1]);
end
hold on
subplot(numplots,1,1);
title('Projection onto Largest Eigenvector');

%**************************************************************************
%Measure correlation mean and max eigenvector
meancorr=zeros(1,simparam.niter+1);
for index=1:simparam.niter+1
    %get eigenvectors from previous iteration as these are the ones
    %that affect the fmaxlambda function
    [u,s]=svd(pmax.newton.c{index});
    %meancorr(index)=pmax.newton.m(:,index)'/(pmax.newton.m(:,index)'*pmax.newton.m(:,index))^.5*u(:,1);
    meancorr(index)=pmax.newton.m(:,index)'*u(:,1);
end

figure;
plot(meancorr);
ylabel('Correlation Mean and Max Eigenvector');

%*********************************************************
%measure correlation with mean
%normalize the mean so we know what percent is along mean
meanenergy=srmax.newton.y.*pmax.newton.m(:,1:end-1);
meanenergy=sum(meanenergy,1)./(sum((pmax.newton.m(:,1:end-1)).^2,1)).^.5;

figure;
plot(meanenergy);
xlabel('Iteration');
ylabel('Correlation with mean');

figure;
hist(meanenergy);
title('Histogram of Correlation with Mean');
%*****************************************************************

normmean=pmax.newton.m./(ones(size(pmax.newton.m,1),1)*(sum(pmax.newton.m.^2,1)).^.5);
%normean=pmax.newton.m;
corrmean=normmean'*normmean;
figure;
imagesc(corrmean);
colorbar;
title('Correlation of direction of mean');


%***************************************
%look for cycles
off=1;
cyclesize=size(srmax.newton.y,1)-1;
y=srmax.newton.y(:,off:cyclesize:end);
stimcorr=y'*y;
figure;
imagesc(stimcorr);
colorbar;
title('Newton Infomax');

%***************************************
%compute correlation between eigenvector with max eigenvalue
%and 1st largest eigenvector from previous iteration. 
%Measure the energy projection onto the eigenvalues
c=pmax.newton.c;
eigcorr=zeros(1,length(c)-1);
[lastu s]=svd(c{1});
for index=2:size(srmax.newton.y,2)
    [curru s]=svd(c{index});
    
    eigcorr(index-1)=curru(:,1)'*lastu(:,2);
    lastu=curru;
end
xlabel('Iteration');
ylabel('Correlation');


%***************************************
%plot the eigenvalues
meths={prand.newton, pmax.newton}
lbls={'Rand','Max'};
c=prand.newton.c;

figure;
hold on;
for mindex=1:length(meths)
    c=meths{mindex}.c;
    eigvalues=zeros(mparam.tlength,simparam.niter+1);
    for index=1:size(srmax.newton.y,2)
        [curru s]=svd(c{index});

        eigvalues(:,index)=diag(s);
    end    



    %(mparam.tlength,1,1);
    for index=1:mparam.tlength
        pind=(mindex-1)*mparam.tlength+index;
        h(pind)=plot(1:simparam.niter+1,eigvalues(index,:),getptype(pind,1));
        plot(1:simparam.niter+1,eigvalues(index,:),getptype(pind,2));
        lgnd{pind}=(sprintf('%0.2g Eigenvalue (%s)',index,lbls{mindex}));    
    end
end
set(gca,'YScale','Log');
xlabel('Iteration');
ylabel('Eigenvalue');
legend(h,lgnd);