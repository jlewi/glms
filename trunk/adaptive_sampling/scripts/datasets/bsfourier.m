%These are some data sets comparing the MSE when we represent the STRF as a
%2-d fourier series
function [dfiles,pstyles]=bsfourier()
dind=0;
dfiles=struct();

smooth.nfcutoff=15;
smooth.ntcutoff=3;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'colors','lstyles','markers'};
pstyles.linewidth=pstyles.linewidth;

%*************************************************************************
%info. max.
%************************************************************************
bdir='080921';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max.: Full post.';
dfiles(dind).usetanpost=false;


%************************************************************************
%Freq domain
%**************************************************************************
bdir='081007';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='STRF represented as fourier series. Use all frequencies. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max.: Fourier all freq';
dfiles(dind).usetanpost=false;

bdir='081007';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);

explain=sprintf(' STRF represented as fourier series \n');
explain=sprintf('%s Use all frequencies during the experiment. \n',explain);
explain=sprintf('%s Before computing MSE set coefficients for nf>=%d and nt>=%d to 0. \n',explain,smooth.nfcutoff, smooth.ntcutoff);
explain=sprintf('%s Variance in those directions is set to zero as well \n',explain);
explain=sprintf('%s Info. Max. using lower bound for mutual info of the batch \n',explain);
explain=sprintf('%s Compute expectation of log (1+x) using numerical integration  \n',explain);
dfiles(dind).explain=explain;
dfiles(dind).lbl=sprintf('Info. Max.: nf<%d nt<%d',smooth.nfcutoff,smooth.ntcutoff);
dfiles(dind).usetanpost=false;
dfiles(dind).smooth=smooth;

bdir='081007';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='STRF represented as fourier series. Use 1/2  the maximum # of frequencies. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max.: 1/2 freq';
dfiles(dind).usetanpost=false;


bdir='081007';
bfile='bsinfomax_setup_003';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='STRF represented as fourier series. Use 1/4 the maximum # of frequencies. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max.: 1/4 freq';
dfiles(dind).usetanpost=false;


bdir='081008';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='STRF represented as fourier series. Cutoff frequency nfreq=4 ntime=2, cinit=10-3.';
dfiles(dind).lbl='Info. Max.: C=10^-3';
dfiles(dind).usetanpost=false;


bdir='081008';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='STRF represented as fourier series. Cutoff frequency nfreq=4 ntime=2, cinit=10-6.';
dfiles(dind).lbl='Info. Max.: C=10^-6';
dfiles(dind).usetanpost=false;


%**************************************************************************
%Non-infomax: Frequency domain
%**************************************************************************
bdir='081010';
bfile='bsinfomax_setup_002';
nscale=1;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
explain='Batch shuffled design';
explain=sprintf('%sUse frequency domain.\n',explain);
explain=sprintf('%snscale=%g',explain,nscale);
explain=sprintf('%snscale depetermines what frequencies to use. We use frequencies (0:nscale*nmax)(f_fundamental)  where nmax is the largest value possible without aliasing.',explain);
dfiles(dind).explain=explain;
dfiles(dind).lbl=sprintf('shuffled:nscale=%g',nscale);
dfiles(dind).usetanpost=false;

bdir='081010';
bfile='bsinfomax_setup_003';
nscale=1/2;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
explain='Batch shuffled design';
explain=sprintf('%sUse frequency domain.\n',explain);
explain=sprintf('%snscale=%g',explain,nscale);
explain=sprintf('%snscale depetermines what frequencies to use. We use frequencies (0:nscale*nmax)(f_fundamental)  where nmax is the largest value possible without aliasing.',explain,nscale);
dfiles(dind).explain=explain;
dfiles(dind).lbl=sprintf('shuffled:nscale=%g',nscale);
dfiles(dind).usetanpost=false;

bdir='081010';
bfile='bsinfomax_setup_004';
nscale=1/4;
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
explain='Batch shuffled design';
explain=sprintf('%sUse frequency domain.\n',explain);
explain=sprintf('%snscale=%g',explain,nscale);
explain=sprintf('%snscale depetermines what frequencies to use. We use frequencies (0:nscale*nmax)(f_fundamental)  where nmax is the largest value possible without aliasing.',explain,nscale);
dfiles(dind).explain=explain;
dfiles(dind).lbl=sprintf('shuffled:nscale=%g',nscale);
dfiles(dind).usetanpost=false;