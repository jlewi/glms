%******************************************************************
%define the files of 40x40 gabor files for my NC06 results
%*******************************************************************
dsets=[];


niter=5000;


dsets=[];
dind=0;
bdir=fullfile('gabor','071213');
bfile='gabor2d';
niter=2500;

%name of the simulation variables
simvars={'max','maxevec','maxmean','maxheur','maxpool','maxpool','maxpool','grad','rand'};
%start index for the sequential indexing of the files
startind=1;


for sind=[2 3 4 5 6 8]
  dind=dind+1;
dsets(dind).fname=fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,sind-1+startind));
dsets(dind).simvar=simvars{sind};
dsets(dind).niter=niter;

end
