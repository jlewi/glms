%******************************************************************
%define these data files are used to illustrate enforcing of the low rank
%prior by using the tangent space, for a space-time separable receptive
%field.
%*******************************************************************
dsets=[];









dsets=[];
dind=0;
bdir=fullfile('tanspace','080118');
bfile='gabor1dAC';
niter=1000;


%************************************************************
%infomax using tangent space
%*************************************************************
dind=dind+1;
findex=1;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='infomax';
dsets(dind).niter=niter;
dsets(dind).lbl='Info. Max.';

%****************************************************************
%i.i.d design
%****************************************************************
dind=dind+1;
findex=2;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='rand';
dsets(dind).niter=niter;
dsets(dind).lbl='i.i.d.';

%****************************************************************
%info max on full posterior
%****************************************************************
dind=dind+1;
findex=3;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='infomaxfull';
dsets(dind).niter=niter;
dsets(dind).lbl='Info. Max. Full';



%****************************************
%create a structure whose fieldnames are simvar and whose values
%are the corresponding index into dsets
 dnames=[];
 for dind=1:length(dsets)
     dnames.(dsets(dind).simvar)=dind;
 end

