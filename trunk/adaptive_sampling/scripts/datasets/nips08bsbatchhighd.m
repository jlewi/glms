%function [dfiles,pstyles]=nips08bstanhighd()
%   
%return value:
%   dfiles - array of dsets
%   pstyles - object defining the plot styles for this class
%
%Explanation: The birdsong datasets for nips08.
%
function [dfiles,pstyles]=nips08bsbatchhighd()
dfiles=[];
dind=0;

taninfo.class='MTLowRank';
taninfo.rank=2;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'lstyles','colors','markers'};
pstyles.linewidth=pstyles.linewidth*2;
%**********************************************************
%Batch Shuffle
%**************************************************************
dind=dind+1;
bdir='080924';
bfile='bsinfomax_setup_003';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=false;
dfiles(dind).explain='Shuffled. Use full posterior.';
dfiles(dind).lbl='Shuffled:';

dind=dind+1;
bdir='080924';
bfile='bsinfomax_setup_003';
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='shuffle';
dfiles(dind).lbl='Shuffled: Tan. post';
dfiles(dind).taninfo=taninfo;

%**********************************************************************
%BatchPoissLB Tan Space with Numerical Integration
%**************************************************************************
bdir='080924';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. using tangent space and lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max. Tan: Full post.';
dfiles(dind).usetanpost=false;

%use the posterior on the tangent space
bdir='080924';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. using tangent space and lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration. Use the posterior on the tangent space.';
dfiles(dind).lbl='Info. Max. Tan: Tan post.';
dfiles(dind).usetanpost=true;

%*************************************************************************
%info. max.
%************************************************************************
bdir='080924';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='Info. Max.: Full post.';
dfiles(dind).usetanpost=false;

%use the posterior on the tangent space
bdir='080924';
bfile='bsinfomax_setup_001';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration. Use the posterior on the tangent space.';
dfiles(dind).lbl='Info. Max.: Tan post.';
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo=taninfo;

%*************************************************************************
%info. max. start fullmax=100
%************************************************************************
bdir='080925';
bfile='bsinfomax_setup_004';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. Tangent Space. First 100 trials use full posterior. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='IM Tan: Startfull=100';
dfiles(dind).usetanpost=false;

%use the posterior on the tangent space
bdir='080925';
bfile='bsinfomax_setup_004';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. Tangent Space. First 100 trials use full posterior. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration. Use the posterior on the tangent space.';
dfiles(dind).lbl='IM Tan: Startfull=100';
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo=taninfo;

%*************************************************************************
%info. max. start fullmax=50
%************************************************************************
bdir='080925';
bfile='bsinfomax_setup_005';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. Tangent Space. First 50 trials use full posterior. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='IM Tan: Startfull=50';
dfiles(dind).usetanpost=false;

%use the posterior on the tangent space
bdir='080925';
bfile='bsinfomax_setup_005';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. Tangent Space. First 50 trials use full posterior. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration. Use the posterior on the tangent space.';
dfiles(dind).lbl='IM Tan: Startfull=50';
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo=taninfo;

%*************************************************************************
%info. max. start fullmax=25
%************************************************************************
bdir='080925';
bfile='bsinfomax_setup_006';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).explain='Info. Max. Tangent Space. First 25 trials use full posterior. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration.';
dfiles(dind).lbl='IM Tan: Startfull=25';
dfiles(dind).usetanpost=false;

%use the posterior on the tangent space
bdir='080925';
bfile='bsinfomax_setup_006';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
dfiles(dind).usetanpost=true;
dfiles(dind).explain='Info. Max. Tangent Space. First 25 trials use full posterior. Info. Max. using lower bound for the mutual info of the batch. Compute the expectation of log(1+x) using numerical integration. Use the posterior on the tangent space.';
dfiles(dind).lbl='IM Tan: Startfull=25';
dfiles(dind).usetanpost=true;
dfiles(dind).taninfo=taninfo;