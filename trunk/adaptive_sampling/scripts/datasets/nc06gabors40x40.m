%******************************************************************
%define the files of 40x40 gabor files for my NC06 results
%These results use my new heuristic
%lbl - a multi-line version of the label
%slbl- a one-line version of the label
%*******************************************************************
dsets=[];






niter=5000;


dsets=[];
dind=0;
bdir=fullfile('gabor','071213');
bfile='gabor2d';
niter=0;


%*************************************************************
%max over power constraint
%*************************************************************
dind=dind+1;
bdir=fullfile('gabor','071219');
findex=1;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='max';
dsets(dind).lbl=sprintf('info.\nmax.\n    $\\mathcal{X}$');
dsets(dind).slbl=sprintf('$\\mathcal{X}_{t+1}$');
dsets(dind).niter=niter;

%***************************************************************
%stim=max evec
%***************************************************************
dind=dind+1;
bdir=fullfile('gabor','071213');
findex=2;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='maxevec';
dsets(dind).niter=niter;
dsets(dind).lbl=sprintf('eig.\nvec.');
dsets(dind).slbl=sprintf('eig. vec.');
dsets(dind).niter=5000;


%***************************************************************
%stim=mean
%***************************************************************
dind=dind+1;
bdir=fullfile('gabor','071213');
findex=3;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).lbl=sprintf('$\\vec{x}_{t+1}=\\vec{\\mu}_t$');
dsets(dind).slbl=sprintf('$\\vec{x}_{t+1}=\\vec{\\mu}_t$');
dsets(dind).simvar='maxmean';
dsets(dind).niter=niter;


%***************************************************************
%stim=rand
%***************************************************************
dind=dind+1;
bdir=fullfile('gabor','071213');
findex=9;
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_%03.3g.mat',bfile,findex)));
dsets(dind).simvar='rand';
dsets(dind).niter=niter;
dsets(dind).lbl='i.i.d.';
dsets(dind).slbl='i.i.d.';



%***************************************************************
%stim=random spatial gratings
%***************************************************************
dind=dind+1;
bdir=fullfile('gabor','071218');
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_002.mat',bfile)));
dsets(dind).lbl='gratings';
dsets(dind).slbl='gratings';
dsets(dind).simvar='grad';
dsets(dind).niter=5000;


%********************************************************
%stim = Heuristic
%*****************************************************
dind=dind+1;
bdir=fullfile('gabor','071218');
dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_001.mat',bfile)));
dsets(dind).simvar='maxheur';
dsets(dind).lbl=sprintf('    info.\n    max.\n$\\hat{\\mathcal{X}}_{heur,t+1}$');
dsets(dind).slbl=sprintf('$\\hat{\\mathcal{X}}_{heur,t+1}$');
dsets(dind).niter=niter;

%********************************************************
%stim = max i.i.d pool
%*****************************************************
dind=dind+1;
bdir=fullfile('gabor','071213');

dsets(dind).fname=FilePath('bcmd','RESULTSDIR','rpath',fullfile(bdir,sprintf('%s_005.mat',bfile)));
dsets(dind).simvar='maxpool';
dsets(dind).lbl=sprintf(' info.\n max.\n$\\hat{\\mathcal{X}}_{iid,t+1}$');
dsets(dind).slbl=sprintf('$\\hat{\\mathcal{X}}_{iid,t+1}$');
dsets(dind).niter=niter;


%****************************************
%create a structure whose fieldnames are simvar and whose values
%are the corresponding index into dsets
dnames=[];
for dind=1:length(dsets)
    dnames.(dsets(dind).simvar)=dind;
end