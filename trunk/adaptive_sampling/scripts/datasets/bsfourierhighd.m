%These are some data sets comparing the MSE when we represent the STRF as a
%2-d fourier series
function [dfiles,pstyles]=bsfourier()
dind=0;
dfiles=struct();

smooth.nfcutoff=15;
smooth.ntcutoff=3;

%Set the plot styles for this dataset
pstyles=PlotStyles();
pstyles.lstyles={'-','--'};
pstyles.order={'colors','lstyles','markers'};
pstyles.linewidth=pstyles.linewidth;



%************************************************************************
%Freq domain
%**************************************************************************
bdir='081009';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
explain=sprintf(' STRF represented as fourier series \n');
explain=sprintf('%s Use all frequencies during the experiment. \n',explain);
explain=sprintf('%s Info. Max. using lower bound for mutual info of the batch \n',explain);
explain=sprintf('%s Compute expectation of log (1+x) using numerical integration  \n',explain);
dfiles(dind).explain=explain;
dfiles(dind).lbl='Info. Max.: Fourier all freq';
dfiles(dind).usetanpost=false;

bdir='081009';
bfile='bsinfomax_setup_002';
dind=dind+1;
setupfile=fullfile('/home/jlewi/svn_trunk/allresults/bird_song',bdir,bfile);
[data]=xfunc(setupfile);
dfiles=copystruct(dfiles,dind,data);
explain=sprintf(' STRF represented as fourier series \n');
explain=sprintf('%s Use all frequencies during the experiment. \n',explain);
explain=sprintf('%s Before computing MSE set coefficients for nf>=%d and nt>=%d to 0. \n',explain,smooth.nfcutoff, smooth.ntcutoff);
explain=sprintf('%s Variance in those directions is set to zero as well \n',explain);
explain=sprintf('%s Info. Max. using lower bound for mutual info of the batch \n',explain);
explain=sprintf('%s Compute expectation of log (1+x) using numerical integration  \n',explain);
dfiles(dind).explain=explain;
dfiles(dind).lbl=sprintf('Info. Max.: nf<%d nt<%d',smooth.nfcutoff,smooth.ntcutoff);
dfiles(dind).usetanpost=false;
dfiles(dind).smooth=smooth;
