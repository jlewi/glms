%For the tangent space data from nips
%
%Compute the expected log-likelihood using the tangent space
clear variables;
setpathvars;

[dsets]=phdbs_data_infomax_tspace_090227_001;

%speedup of rank 2 vs. infomax full
%seqfiles=[dsets(2) dsets(3)];

%continue the simulations
for dind=1:length(dsets)
    v=load(getpath(dsets(dind).datafile));
    if (v.bssimobj.niter>0)
        %check if the expllike file exists
        %**************************************************************
        %Expected log-likelihood on full space
        %***************************************************************
        fexp=BSAnalyze.filename(dsets(dind).datafile(1),'expllike');
        [fdir fbase]=fileparts(getrpath(fexp));
        [fexp]=fullfile(fdir,[fbase '_001.mat']);
        fexp=FilePath('RESULTSDIR',fexp);
        if exist(getpath(fexp),'file')
            exll(dind)=BSExpLLike('exllfile',fexp);
            compellike(exll(dind));
        else
             exll(dind)=BSExpLLike('newdata',dsets(dind).datafile,'usetanpost',false);
        end
        
        %**************************************************************
        %Expected log-likelihood on tan space
        %***************************************************************
        fexp=BSAnalyze.filename(dsets(dind).datafile(1),'expllike');
        [fdir fbase]=fileparts(getrpath(fexp));
        [fexp]=fullfile(fdir,[fbase '_002.mat']);
        fexp=FilePath('RESULTSDIR',fexp);
        if exist(getpath(fexp),'file')
            exlltan(dind)=BSExpLLike('exllfile',fexp);
            compellike(exlltan(dind));
        else
             exlltan(dind)=BSExpLLike('newdata',dsets(dind).datafile,'usetanpost',true);
        end
    end
end