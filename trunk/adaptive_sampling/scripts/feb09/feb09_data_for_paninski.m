%**************************************************************************
%Create a dataset for paninski
%*********************************************************************
clear variables;
setpathvars;
odir=fullfile(RESULTSDIR,'bdata_paninski');


testset='noise';

switch testset
    case 'noise'
        %trained on just the noise stimuli
        [dfiles]=feb09_data_fitsep_noise_090225_001;
        dfiles=dfiles';
        
        fsuffix='noise';
    case 'song'
        [dfiles]=feb09_data_fitsep_birdsong_090225_001;
        dfiles=dfiles';

        fsuffix='song';
end

%seqfiles=seqfiles(1,:);

sname=mfilename('full');

%***********************************************************************
%Compute data for fake raster plots
%***********************************************************************
%%
%rdata= size(seqfiles(1,))xsize of test set

%neuron is a structure array containing the data for each neuron
%   .strf - 2 dimensional strf    
%   .t    - the time to associate with each column of the strf
%   .freqs - the frequency to associate with each row of the srf
%   .shistcoeff - the spike history coefficient
%   .tshist     - the time bins to associate with the spike history
%                 coefficients
%   .datafile - (the datafile from which this neuron was extracted)
%   .rawdatafile - (the raw datafile for this neuron)
%   .rho(1xnwaves)  - this is a cell array containing rho for each
%                       wave file
%                    - this is the convolution of the strf with the
%                    spectrogram of the wave file + the BIAS.
%   .trainset(1xnwaves) - this is 1xnwave files array
%                       - a 1 indicates that wave file was used to train
%                       this strf. 0 means it was not used
%
%   .obsrv(1xnwaves) - cell array. Each element is matrix of the responses
%           to corresponding wave file
%          - each row is a different repeat of this trial
%          - each column corresponds to the response to the corresponding
%          column in rho
%   .spiketimes(nwaves,nrepeats)-cell array 
%          -i'th j'th element is a matrix storing the spike
%           time on the jth repeat of the ith wavefile
%          - some entries are empty matrices because some wavefiles
%           were repeated fewer times than others
%                       
%
%   .wavename(1xnwaves)  - name of the wave file use this to
%           that the corresponding entry in wavefiles is the same wavefile
%
%wavefiles - a structure array describing the wave files 
%   .spec  - the spectrogram
%   .t     - the time associated with each column
%   .f     - frequency associated with each row
%   .isbirdsong - true if this wave file is birdsong
%               - false if its ML noise
%   .wavename - name of the wavile
for dind=1:size(dfiles,1)
    %use the info. max. design
    v=load(getpath(dfiles(dind).datafile));
    bssimobj(dind)=v.bssimobj;

    %get the final estimate of theta
    if ~isempty(bssimobj(dind).results)
        theta=bssimobj(dind).results(end).theta;
        shistfilt=theta(bssimobj(dind).mobj.indshist(1):bssimobj(dind).mobj.indshist(2));

        
        %***************************************************************
        %extract the strf
        %***************************************************************
        [stimcoeff,shistcoeff,bias]=parsetheta(bssimobj(dind).mobj,bssimobj(dind).results(end).theta);
        
        
        %project the stimulus coefficients into the spectral domain
        [strf]=tospecdom(bssimobj(dind).mobj,stimcoeff);
        
        neuron(dind).strf=strf;
        neuron(dind).shistcoeff=shistcoeff;
        neuron(dind).bias=bias;
        [neuron(dind).t,neuron(dind).freqs]=getstrftimefreq(bssimobj(dind).bdata);        

        
        neuron(dind).tshist=-1*[getshistlen(bssimobj(dind).mobj):-1:1];
        
        neuron(dind).datafile=getrpath(dfiles(dind).datafile);
        neuron(dind).rawfile=getrpath(dfiles(dind).rawdatafile);
        
        
        
        bspost=bssimobj(dind).updater.bpost;
       
                    
        
                    
        glm=bssimobj(dind).mobj.glm;

        %determine the training set
        neuron(dind).trainset=zeros(1,getnwavefiles(bssimobj(dind).bdata));
        neuron(dind).trainset(bssimobj(dind).updater.windexes)=true;
       


        neuron(dind).spiketimes=cell(getnwavefiles(bssimobj(dind).bdata),10);
        for wind=1:getnwavefiles(bssimobj(dind).bdata)
                [glmproj]=compglmprojnoshist(bspost,wind,bssimobj(dind).mobj,theta);
                neuron(dind).rho{wind}=glmproj;       
                
                
                [ntrials,cstart]=getntrialsinwave(bssimobj(dind).bdata,wind);
                obsrv=getobsrvmat(bssimobj(dind).bdata,wind);
                neuron(dind).obsrv{wind}=obsrv(:,cstart:end);
                
                neuron(dind).wavename{wind}=bssimobj(dind).bdata.stimspec(wind).fname;
                
                spiketimes=getstimresp(bssimobj(dind).bdata,wind);
                neuron(dind).spiketimes(wind,1:length(spiketimes))=spiketimes';
        end
    end
end


dind=length(bssimobj);

bdata=bssimobj(dind).bdata;
for wind=1:getnwavefiles(bdata)
     wavefiles(wind).isbirdsong=waveissong(bdata,wind);
   
     
      [ntrials,cstart]=getntrialsinwave(bdata,wind);
      [obj,spec,outfreqs,t]=getfullspec(bdata,wind);

      wavefiles(wind).spec=spec;
      wavefiles(wind).f=outfreqs;
      wavefiles(wind).t=t;
      wavefiles(wind).wavename=bdata.stimspec(wind).fname;
end

fout=fullfile(odir,[fsuffix '.mat']);
save(fout,'neuron','wavefiles');