function [dsets]= feb09_data_noft_noise_090225_001() 
%********************************


dsets=[];
dind=1;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_025.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_025.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_025.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_025.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_025.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k302_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=2;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_026.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_026.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_026.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_026.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_026.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/br2523_06_003-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=3;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_027.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_027.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_027.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_027.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_027.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/aa0708_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=4;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_028.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_028.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_028.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_028.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_028.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/bb2728_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=5;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_029.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_029.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_029.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_029.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_029.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k306_03_013-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=6;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_030.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_030.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_030.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_030.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_030.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_02_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=7;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_031.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_031.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_031.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_031.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_031.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_05_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=8;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_032.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_032.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_032.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_032.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_032.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_06_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=9;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_033.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_033.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_033.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_033.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_033.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_07_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=10;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_034.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_034.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_034.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_034.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_034.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k319_04_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=11;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_035.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_035.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_035.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_035.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_035.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/ps10454_10_001-Ch2-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=12;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_setup_036.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_data_036.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_mfile_036.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_cfile_036.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/bird_song/090225/bsglmfit_status_036.txt','isdir',0);
dsets(dind).wavefilesdir=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',1);
dsets(dind).rawdatafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/sorted/k313_04_001-Ch1-neuron1_corrected.mat','isdir',0);
dsets(dind).fmapiterf=[];
dsets(dind).nwindexes=9;
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


