%5-28-07
%
% Plot the feasible region under the power constraint
% along with sample points chosen via i.i.d. sampling of stimuli and using
% our heuristic.
clear all;
setpathvars;
dsets=[];

%This is the data set from which we get the posteriors
dsets(end+1).fname=fullfile(RESULTSDIR,'gabor','071217','gabor2d_001.mat');
dsets(end).lbl='power';
dsets(end).simvar='max';

%switch back to _001

gparam.trials=[50 100 500 1000]; %trials on which to plot the quadratic region
gparam.trials=[25 50 75 100];

gparam.npts=500;            %number of points to plot
gparam.nsigpts=500;         %number of sigma points to use to plot the manifold
gparam.ncontours=20;        %number of contours for the contour plot
gparam.nrows=2;
gparam.ncols=2;

%**************************************************************************
%Stimulus choosers for which we want to compute the points
%   1st stimulus chooser uses the power constraint to compute the actual
%   region
%**********************************************************************
clear sobj;
stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_25d_071213_001.mat'));
nstim=1000;
sobj{1}=PoolstimHeurMuMax('nstim',1000,'miobj',PoissExpCompMi());
sobj{2}=PoolstimSubMuProj('fname',stimfile,'miobj',PoissExpCompMI(),'nstim',nstim,'debug',true);

clear simobj;

%********************************************************************
%Load the data
%****************************************************************
dind=1;
simobj(dind)=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);


%store info about the stimuli

freg=cell(1,length(gparam.trials));
fpoly=cell(1,length(gparam.trials));
pind=0;
%**************************************************************************
%load the data
%*************************************************************************
for trial=gparam.trials
    pind=pind+1;
    post(pind)=getpost(simobj(1),trial);
    p.m=getpostm(simobj(1),trial);
    p.c=getpostc(simobj(1),trial);
    %use plotquadreg to compute the boundary of the feasible region
    dind=1;
    mobj=getmobj(simobj(dind));
    [fp{pind}, freg{pind},data(pind)]=plotquadreg('post',p,'mmag',getmag(getmobj(simobj(dind))),'klength',getklength(mobj),'alength',getshistlen(mobj));
    fp{pind}.name=sprintf('Feasible Region: Trial %d',trial);
    %lblgraph(fp{pind});
    close(fp{pind}.hf);
    close(freg{pind}.hf);


    for sind=1:length(sobj)
        [xmax,oreturn,s,musigma(sind,pind)]=choosestim(sobj{sind},getpost(simobj,trial),[],getmobj(simobj),[]);
    end
end
%%


%%
%*********************************************************************
%Draw Plots
%**************************************************************************

nrows=1;
ncols=length(gparam.trials);

clear fpoly;

for pind=1:length(gparam.trials)
    fpoly(pind)=FigObj('name','Feasible Region','width',4,'height',4);
    fpoly(pind).a=AxesObj('xlabel','\mu_\rho','ylabel','\sigma^2');

    %*****************************************
%     [col row]=ind2sub([ncols,nrows],pind);
%     setfocus(fpoly.a,row,col);
    row=1;
    col=1;
    
    fpoly(pind).a(row,col)=title(fpoly(pind).a(row,col),sprintf('Trial %d',gparam.trials(pind)));

    hold on;
    %*****************************************************************
    %plot the boundary under the power constraint
    %**************************************************************
    %we multiply alpha by the magnitude of the mean

    m=getm(post(pind));
    hp=plot((m'*m)^.5*[data(pind).alpha data(pind).alpha(end:-1:1)],[data(pind).qmax(1,:), data(pind).qmin(1,end:-1:1)],'k-');
    lstyle.linewidth=2;
    lstyle.color=[0 0 0];
    fpoly(pind).a(row,col)=addplot(fpoly(pind).a(row,col),'hp',hp,'lstyle',lstyle);

    %compute the bounds
    bounds(pind).xmax=(m'*m)^.5*data(pind).alpha(end);
    bounds(pind).xmin=(m'*m)^.5*data(pind).alpha(1);
    bounds(pind).ymax=max(data(pind).qmax);
    bounds(pind).ymin=min(data(pind).qmin);

    %**********************************************************************
    %make a surface plot of the optimization manifold (i.e mutual
    %information as function of mu, sigma)
    %******************************************************************
    optman(pind).glmproj=(m'*m)^.5*[data(pind).alpha];
    optman(pind).sigma=linspace(bounds(pind).ymin,bounds(pind).ymax,gparam.nsigpts);
    %optman(pind).optman=zeros(gparam.nsigpts,length(optman(pind).glmproj));
    nrows=gparam.nsigpts;
    ncols=length(optman(pind).glmproj);
    [rind,cind]=ind2sub([nrows, ncols],1:(nrows*ncols));

    %compute the information using the miobj of one of the Poolbased
    %objects
    miobj=getmiobj(sobj{1});
    mi=compmi(miobj,optman(pind).glmproj(cind),optman(pind).sigma(rind));
    optman(pind).optman=reshape(mi,[nrows ncols]);
    %set all points outside the feasible region to NAN
    for cind=1:ncols
        ilb=find(optman(pind).sigma>=data(pind).qmin(cind),1,'first');
        if ~isempty(ilb)
            optman(pind).optman(1:ilb-1,cind)=nan;
        end
        iub=find(optman(pind).sigma>data(pind).qmax(cind),1,'first');
        if ~isempty(iub)
            optman(pind).optman(iub:end,cind)=nan;
        end
    end
    colormap(gray);
    [c optman(pind).hc]=contourf(optman(pind).glmproj,optman(pind).sigma,optman(pind).optman,gparam.ncontours,'EdgeColor','none');
    hc(pind)=colorbar;
    %**********************************************************************
    %we plot the sample points as patches because we want to be able to do
    %transparency
    %********************************************************************
    %number of pts to plot


    %compute a square for each point
    %set width/height for each point to be 1% of the width/height of the
    %   region
    gparam.pwidth=.015*(bounds(pind).xmax-bounds(pind).xmin);
    gparam.pheight=.015*(bounds(pind).ymax-bounds(pind).ymin);

    %the matrices x, y specify the patches for each point
    %each column is a different pt
    %the rows correspond to
    %   lower left
    %   upper lefth
    %   upper right
    %   lower right
    for sind=1:length(sobj)
        npts=min(gparam.npts,length(musigma(sind,pind).muproj));

        pts.x=zeros(4,npts);
        pts.y=zeros(4,npts);
        pts.x(1:2,:)=[1;1]*musigma(sind,pind).muproj(1:npts)-gparam.pwidth/2;
        pts.x(3:4,:)=[1;1]*musigma(sind,pind).muproj(1:npts)+gparam.pwidth/2;
        pts.y(1,:)=musigma(sind,pind).sigma(1:npts)-gparam.pheight/2;
        pts.y(4,:)=pts.y(1,:);
        pts.y(2,:)=musigma(sind,pind).sigma(1:npts)+gparam.pheight/2;
        pts.y(3,:)=pts.y(2,:);

        %plot the patches
        %1. whatever we plot first is on top
        %2. make the uniform opaque and black and put it in back (plot it
        %second).
        %plot the patches
        %We make this gray and semi transparent
        %this wil be ontop
        %put uniform in back, black and opaque
        if (sind==1)
            hp=patch(pts.x,pts.y,0*[1 1 1],'FaceAlpha',1,'EdgeColor','none');
        else
            hp=patch(pts.x,pts.y,.75*[1 1 1],'FaceAlpha',1,'EdgeColor','none');
        end
        fpoly(pind).a(row,col)=addplot(fpoly(pind).a(row,col),'hp',hp);
    end
    
    fpoly(pind)=lblgraph(fpoly(pind));
end

odata=cell(length(gparam.trials),2);

for pind=1:length(gparam.trials)
   odata{pind,1}=fpoly(pind); 
end

odata{1,2}={'script','pfeasreg.m'; 'Black squares', class(sobj{1});'Grey squares', class(sobj{2})};
onenotetable(odata,seqfname('~/svn_trunk/notes/feasreg.xml'));

   outfile='~/svn_trunk/adaptive_sampling/writeup/NC06/quadreg.eps';
   %onenotetable({fpoly,);
    %saveas(fpoly.hf,fpoly.outfile,'epsc2');
    %this render does not show transparency effects but it prevents the fonts
    %from being pixelated.
    %set(gcf,'Renderer','painters')
    %saveas(fpoly.hf,fpoly.outfile,'epsc2');