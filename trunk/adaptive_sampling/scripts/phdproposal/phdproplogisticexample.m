%5-08-2008
%
% To illustrate the utility of an adaptive design,
% I consider the example of determining the slope and offset of a  logistic
% function.


fwidth=4;
fheight=2;
fsize=12;   %font size

%leave labels blank to add labels on the slide
xlbl='';    
ylbl='';
%**********************************************
%plot the actual logistic function
%************************************************
center=2;
m=1;

dx=10;
x=[center-dx:.1:center+dx];
y=1./(1+exp(-(x-center)/m));

ftrue=FigObj('width',fwidth,'height',fheight,'fontsize',fsize,'name',true');
ftrue.a=AxesObj('xlabel',xlbl,'ylabel',ylbl);

pstyle.linewidth=4;
hp=plot(x,y);
ftrue.a=addplot(ftrue.a,'hp',hp,'pstyle',pstyle);
set(gca,'xtick',[]);
set(gca,'ytick',[0 1]);
xlim([x(1) x(end)]);
ylim([-.25 1.25]);
saveas(gethf(ftrue),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/logistictrue.png');


%**************************************************
%plot a noisy version of the data
%****************************************************
%%

subsamp=10;
xsub=x(1:subsamp:end);
ysub=y(1:subsamp:end);
ynoisy=ysub+normrnd(zeros(1,length(ysub)),.05*ones(1,length(ysub)));

fn=FigObj('width',fwidth,'height',fheight);
fn.a=AxesObj('xlabel',xlbl,'ylabel',ylbl);

pstyle=[];
pstyle.linewidth=4;
pstyle.linestyle='none';
pstyle.marker='o';
pstyle.color='b';

hp=plot(xsub,ynoisy);
fn.a=addplot(fn.a,'hp',hp,'pstyle',pstyle);
set(gca,'xtick',[]);
set(gca,'ytick',[0 1]);
xlim([x(1) x(end)]);
ylim([-.25 1.25]);
saveas(gethf(fn),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/logisticobserve.png');

%**************************************************************************
%Plot several different logistic functions to illustrate the possible
%**************************************************************************
%%
centers=[-8:4:8];
ms=[.5, 1,2];

mcolor={'r' 'g' 'b'};
dx=10;
x=[centers(1)-dx:.5:centers(end)+dx];

fmany=FigObj('width',2,'height',1,'fontsize',18);
fmany.a=AxesObj('nrows',1,'ncols',1,'xlabel',xlbl,'ylabel',ylbl);
hold on;

pstyle=[];
for cind=1:length(centers)
    for mind=1:length(ms)
    c=centers(cind);
    m=ms(mind);
        yv=1./(1+exp(-(x-c)/m));
       
     pstyle.linewidth=1;
     pstyle.linestyle='--';
     pstyle.color=mcolor{mind};
     
    hp=plot(x,yv);
    fmany.a=addplot(fmany.a,'hp',hp,'pstyle',pstyle);
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
%    xlim([centers(1)-ds centers(end)+dx]);
    end
end
xlim([-dx dx])
lblgraph(fmany);

saveas(gethf(fmany),'~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/logisticobservemany.png');

%**************************************************************************
%Plot the possibilities after making an observation
%**************************************************************************
%%
centers=[-4, 0,4];
ms=[.5, 1,2];

mcolor={'r' 'g' 'b'};
dx=10;
x=[centers(1)-dx:.5:centers(end)+dx];

for cind=1:length(centers)
fmany=FigObj('width',2,'height',1);
fmany.a=AxesObj('nrows',1,'ncols',1,'xlabel',xlbl,'ylabel',ylbl);
hold on;

pstyle=[];

    for mind=1:length(ms)
    c=centers(cind);
    m=ms(mind);
        yv=1./(1+exp(-(x-c)/m));
       
     pstyle.linewidth=1;
     pstyle.linestyle='--';
     pstyle.color=mcolor{mind};
     
    hp=plot(x,yv);
    fmany.a=addplot(fmany.a,'hp',hp,'pstyle',pstyle);
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
%    xlim([centers(1)-ds centers(end)+dx]);
    end


%add a point to indicate the observation
hp=plot(centers(cind),.5,'ko');
set(hp,'MarkerFaceColor','k');

xlim([-dx dx])
lblgraph(fmany);

saveas(gethf(fmany),fullfile('~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs',sprintf('logisticpost%g.png',centers(cind))));
end