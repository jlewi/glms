%2-09
%
%Script to test that monte carlo sampling is working
%generate some sample data according to a true gaussian distribution
%Therefore the relative entropy should always be zero
%
%The likelihood of the data is a gaussian with unkown mean 
%and known covariance
ntrials=50;
thetatrue=2;
ctrue=1;
obsrv=mvgausssamp(thetatrue,ctrue,ntrials);

post=[];
%now estimate the mean and variance of this from the smaple mean and
%variance
post.m(:,1)=0;
post.c{1}=1;

for index=1:ntrials
    %the mean of the likelihood function is
    mll=mean(obsrv(1:index));
    %variance of likeliihood function is
    cll=1/(index);
   
    post.c{index+1}=inv(inv(post.c{1})+inv(cll));
     post.m(:,index+1)=post.c{index+1}*(inv(post.c{1})*post.m(:,1)+inv(cll)*mll);
end

%compute the kl distance using brute force and monte
%carlo sampling
%compue the KL divergence using monte carlo
index=0;
iter=[];
post.dk=[];
klparam=[];
klparam.nsamp=1000;
klparam.z=randn(size(post.m,1),klparam.nsamp);

%number of different samples to try
nsamps=[10 100 1000];

dk=cell(1,length(nsamps));
iter=cell(1,length(nsamps));
tstep=1;
for sindex=1:length(nsamps);
    klparam.nsamp=nsamps(sindex);
    klparam.z=randn(size(post.m,1),klparam.nsamp);

    
    iter{sindex}=1:tstep:ntrials;
    dk{sindex}=zeros(1,length(iter{sindex}));
    vsum{sindex}=zeros(1,length(iter{sindex}));
    index=0;
for tr=1:tstep:ntrials
    index=index+1;
    q.mu=post.m(:,tr+1);
    q.c=post.c{tr+1};


        %We are computing the KL distance of our approximation from the
        %true posterior. To do this, the KL divergence needs to consider
        %the likelihood of all observations so far.
        %So the llhood function is the batch update function
        %To compute the KL Divergence the KL divergence
        %needs to be able to compute the likelihood of the observation
        %under the different models
        %so we pass the information needed to compute the likelihood
        %in the structure ll
        
        ll.func=@loggaussmodel;
        %we need all the observations so far
        ll.obsrv=obsrv(1:tr);    
        ll.obsrv=ll.obsrv';                        %needs to be a column vector
                                                   %different observation
                                                   %in each row
        ll.param={ctrue};              
        
        %function to compute prior likleihood of model
        po.func=@logmvgauss;
         
        po.param={post.m(:,1),post.c{1}};
        
        [dk{sindex}(index), p, vsum{sindex}(index)]=dkl(q,po,ll,klparam);
        iter{sindex}(index)=tr;
        
end

end

figure;
hold on
for sindex=1:length(nsamps)
    plot3(nsamps(sindex)*ones(1,length(iter{sindex})),iter{sindex},dk{sindex},'.');
    xlabel('Num Samples');
    ylabel('Iteration');
    zlabel('Dk');
end