%4-20-2008
%Make a plot of the stimulus spectrogram and below it a raster plot of
%spikes

xscript('/home/jlewi/svn_trunk/allresults/bird_song/080526/bsglmfit_setup_002.m');

%index of the file to plot
wavindex=1;

bdata=load(getpath(dsets.datafile),'bdata');
bdata=bdata.bdata;


if ~exist('compvar','var')
    compvar=true;
end

%size of bins for psth in seconds
%this is also the binsize used for constructing the firing rate
psthbinsize=.001;

%length of the hanning window in seconds
%hwinlen=.015;
hwinlen=2.5
%for wavindex=1:getnwavefiles(bdata)
for wavindex=10
    %for wavindex=1:1
    [bdata,spec,outfreqs,t]=getfullspec(bdata,wavindex);

    
    fh=FigObj('name','Spike Triggered Average','height',6,'width',5,'naxes',[2 1]);

    
    setfocus(fh.a,1,1);
    xlabel(fh.a(1,1),'time(s)');
    ylabel(fh.a(1,1),'Frequenzy(hz)');
    
    t=[0:(size(spec,2)-1)]*getobsrvtlength(bdata);

    %add the pre and post silence to the spectrogram

    imagesc(t,outfreqs,spec);
    title(fh.a(1,1),'Spectrogram');
    xlim([t(1) t(end)]);
    ylim([outfreqs(1) outfreqs(end)]);
    hc=colorbar;
    sethc(fh.a(1,1),hc);

    %add a point and text to indicate end of pre and post
    % hold on
    % hp=plot(0,npre*getobsrvtlength(bdata),'k.');
    % pstyle.MarkerFaceColor='k';
    % pstyle.MarkerSize=8;
    % fh.a(1,1)=addplot(fh.a(1,1),'hp',hp,'pstyle',pstyle);

    %**************************************************************************
    %make a raster plot of the spike
    %**************************************************************************
   
     spiketimes=getstimresp(bdata,wavindex);
   setfocus(fh.a,2,1);
    hold on;
    for rep=1:length(spiketimes)
        %each cell array is the spike times on a different trial
        hp=plot(spiketimes{rep},rep*ones(1,length(spiketimes{rep})),'b.');
       addplot(fh.a(2,1),'hp',hp);
    end
title(fh.a(2,1),'Raster Plot');
    xlabel(fh.a(2,1),'time(s)');
    ylabel(fh.a(2,1),'trial');

    xlim([t(1) t(end)]);
    ylim([1 length(spiketimes)]);

    %******************************************************************
    %bin the responses and a plot the fraction of trials on which we get
    %a spike in that bin
%     fh.a=setfocus(fh.a,3,1);
% 
%     bc=[0:psthbinsize:(stiminfo.duration+(npre+npost)*getobsrvtlength(bdata))];
%     scounts=histc([spiketimes{:}],bc);
%     bar(bc,scounts/length(spiketimes));
%     fh.a(3,1)=title(fh.a(3,1),'Fraction trials with spike');
%     fh.a(3,1)=xlabel(fh.a(3,1),'time(s)');
% 
%     xlim([t(1) tlastspike]);
%     ylim([0 max(scounts/length(spiketimes))]);
    %fh.a(3,1)=addplot(fh.a(3,1),'hp',hp);


    %**********************************************************************
    %Plot the firing rate for each trial
    %**********************************************************************
%      pind=4;
%     spiketimes=getstimresp(bdata,wavindex);
%     fh.a=setfocus(fh.a,pind,1);
%     hold on;
% 
%     
%     %the locations of the bins;
%     bc=[0:psthbinsize:(stiminfo.duration+(npre+npost)*getobsrvtlength(bdata))];
%     
%     %create our hanning window normalized by the bin size and 
%     hwin=hann(hwinlen/psthbinsize);
%     hwin=hwin/(sum(hwin))/psthbinsize;
%     for rep=1:length(spiketimes)
%         %spike counts
%         scounts=histc(spiketimes{rep},bc);
% 
%         
%         %rt
%          frate=conv(scounts,hwin);
%         frate=frate(1:length(scounts));
%         %slide filter is anti-causaul
%         %so we delay scounts
% %        dscounts=[zeros(1,floor(length(hwin)/2)) scounts(1:(end-floor(length(hwin)/2)))];
%  %       frate=slidefilter(dscounts,hwin);
%         %each cell array is the spike times on a different trial
%         
%         hp=plot(bc,frate);
%         [c m]=getptype(rep);
%         pstyle.linewidth=2;
%         pstyle.Color=c;
% 
%         fh.a(pind,1)=addplot(fh.a(pind,1),'hp',hp,'pstyle',pstyle);
%         
%     end
% 
%     fh.a(pind,1)=xlabel(fh.a(pind,1),'time(s)');
%     fh.a(pind,1)=ylabel(fh.a(pind,1),'ISI(s)');
% 
%     xlim([t(1) tlastspike]);
%    ylim([0 misi]);
    %*************************************************************************
    %Make a plot of the ISI's as a funtion of time for each trial
    %**************************************************************************
%     pind=4;
%     spiketimes=getstimresp(bdata,wavindex);
%     fh.a=setfocus(fh.a,pind,1);
%     hold on;
% 
%     %max isi
%     misi=0;
%     for rep=1:length(spiketimes)
%         %each cell array is the spike times on a different trial
%         st=spiketimes{rep};
%         isi=[st(1) diff(st)];
%         hp=plot(st,isi);
%         [c m]=getptype(rep);
%         pstyle.linewidth=2;
%         pstyle.Color=c;
% 
%         fh.a(pind,1)=addplot(fh.a(pind,1),'hp',hp,'pstyle',pstyle);
%         misi=max([misi isi]);
%     end
% 
%     fh.a(pind,1)=xlabel(fh.a(pind,1),'time(s)');
%     fh.a(pind,1)=ylabel(fh.a(pind,1),'ISI(s)');
% 
%     xlim([t(1) tlastspike]);
%     ylim([0 misi]);
    
    [bdata,stiminfo]=getstiminfo(bdata,wavindex);
    lblgraph(fh);
    sizesubplots(fh);
    tbl={fh;{'function','bsplotstimraster'; 'Data File',getpath(dsets.datafile); 'Bin size for PSTH',psthbinsize; 'Wave File', stiminfo.fname; 'Explanation','The stimulus and the spiking response. Stimulus includes pre+post silences.'}};
    onenotetable(tbl,seqfname('~/svn_trunk/notes/bsstimraster.xml'));
end