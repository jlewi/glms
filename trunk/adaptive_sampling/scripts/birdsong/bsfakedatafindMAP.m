%find the MAP for some synthetic data

clear variables;
setpathvars;

datedir='080520';
setupfile='bsglmfit_setup_001.m';
xscript(fullfile(RESULTSDIR,'bird_song/fakedata',datedir,setupfile));


rdata=load(getpath(dsets.rawdatafile));
fakemodel=rdata.fakemodel;
mobj=fakemodel.mobj;

%create the upbdate object
uobj=BSBatchFitGLM();


%call update
%initialize with true parameters
thetainit=[fakemodel.strfcoeff;fakemodel.bias];
%thetainit=[];

%we call update several times. Each time we use a larger subset of the data
%On each call we use the value returned from the previous iteration as the
%initialization point
%repeats=unique(ceil(linspace(1,fakemodel.nrepeats,5)));
%repeats=fakemodel.nrepeats;


%use fminunc
bsfit=findMAP(uobj,dsets.datafile,thetainit);

return
bsfit=update(uobj,dsets.datafile,thetainit);
thetainit=bsfit(end).theta;
    
repeats=unique(ceil(linspace(1,fakemodel.nrepeats,5)));
for nrepeats=repeats;
    opt.windexes=[1:fakemodel.nwavefiles];
    opt.nrepeats=nrepeats;
    bsfit=update(uobj,dsets.datafile,thetainit);
    thetainit=bsfit(end).theta;
end

%**************************************************************************
%compute the magnitude of the posterior at the true parameters
%*********************************************************************
%%
data=load(getpath(dsets.datafile));
bdata=data.bdata;
allpost=data.allpost;
prior=getpost(allpost,0);

bpost=BSlogpost('bdata',bdata);
[lpost dlpost]=complpost(bpost,mobj,[fakemodel.strfcoeff;fakemodel.bias],prior);

%%
%compare it to the true theta
fcomp=FigObj('name','Compare true theta and MAP','height',6,'width',4);

fcomp.a=AxesObj('nrows',2,'ncols',1);

%MAP

bdata=data.bdata;

[t,freqs]=getstrftimefreq(bdata);

fcomp.a=setfocus(fcomp.a, 1,1);
imagesc(t,freqs,reshape(bsfit(end).theta(1:getklength(mobj)*getktlength(mobj)),getklength(mobj),getktlength(mobj)));
set(gca,'ydir','reverse');
hc=colorbar;
fcomp.a(1,1)=sethc(fcomp.a(1,1),hc);
fcomp.a(1,1)=title(fcomp.a(1,1),'STRF MAP');

dt=getobsrvtlength(bdata)/2;
df=(freqs(2)-freqs(1))/2;
xlim([t(1)-dt t(end)+dt]);
ylim([freqs(1)-df freqs(end)+df]);
set(gca,'xtick',t);

%True theta
fcomp.a=setfocus(fcomp.a, 2,1);
imagesc(t,freqs,reshape(fakemodel.strfcoeff,getklength(mobj),getktlength(mobj)));
set(gca,'ydir','reverse');
hc=colorbar;
fcomp.a(2,1)=sethc(fcomp.a(2,1),hc);
fcomp.a(2,1)=title(fcomp.a(2,1),'STRF: True');

xlim([t(1)-dt t(end)+dt]);
ylim([freqs(1)-df freqs(end)+df]);
set(gca,'xtick',t);

fcomp=lblgraph(fcomp);
fcomp=sizesubplots(fcomp);

