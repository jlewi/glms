DATADIR=fullfile(RESULTSDIR,'bird_song');
fname='sorted/br2523_06_003-Ch1-neuron1.mat';

bdata=load(fullfile(DATADIR,fname));

%make plots of the spikes on a trial
%each axes is rastergram for a different stimulus
%%
trial=1;

nstim=length(bdata.spikeIndex{trial});
fraster=FigObj('name','Rastergrams');
fraster.a=AxesObj('nrows',nstim,'ncols',1);
for sind=1:nstim
    setfocus(fraster.a,sind,1);
    stimes=bdata.spikeTimes(bdata.spikeIndex{trial}{sind});
    for j=1:length(stimes)
        hp=plot(stimes(j)*[1 1],[0 1]);
    end 
end

%***********************************************************************
%%
% Compute the number of spikes in response to each stimulus on each trial
stim=struct('file',[],'scounts',nan*ones(1,length(bdata.trialCount)));
stim=repmat(stim,1,length(bdata.waves));

for sind=1:length(bdata.waves)
    stim(sind).file=bdata.waves{sind};
end

so=0;
for trial=1:length(bdata.trialCount)
    %loop over stimuli in this tria
   for sind=1:bdata.trialCount(trial)
       so=so+1;
        stim(bdata.mStimOrder(so,1)).scounts(trial)=length(bdata.spikeIndex{trial}{sind});
   end
end

%make a plot of the spike counts for each trial 
fscounts=FigObj('name','spike counts');
fscounts.a=AxesObj('ylabel','spike counts','xlabel','trial');
for sind=1:length(stim)
   hp=plot(1:length(stim(sind).scounts),stim(sind).scounts) ;
   pstyle=plotstyle(sind);
   fscounts.a=addplot(fscounts.a,'hp',hp,'pstyle',pstyle,'lbl',stim(sind).file);
end
%lblgraph(fscounts)
odata={fscounts,{'script:', 'birddata.m';'data',fullfile(DATADIR,fname)}};
onenotetable(odata,'~/svn_trunk/notes/scounts.xml');