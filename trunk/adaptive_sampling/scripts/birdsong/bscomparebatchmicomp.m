%11-26-2008
%Author: Jeremy Lewi
%
%Explanation: We want to compare the different methods we have for
%computing the mutual information for batches of stimuli. I.e we want to
%compare our lower bound, with our upper bound, and taylor approximations
%of the mutual information.
%
%Note: when bsize=1 the lower bound should= the true mutual information
%
%We do this by computing the mutual information using all methods
%for the stimuli selected in one of our simulations

%clear variables;

datafile=FilePath('RESULTSDIR',fullfile('bird_song','081125','bsinfomax_data_001.mat'));

%which trials to compute the mutual information on
%these must be trials on which the posterior was stored
trials=[500:500:10000];

%what size batches to use
bsize=1;


%miobj is a structure array of the different objects to use to compute
%the mutual information
miobj.lb=CompMIPoissCanonLB('method','numerical','confint',1-10^-12);
miobj.ub=CompMIPoissCanonUB();
%miobj.taylor=CompMIPoissExpTaylor('nterms',2);
%miobj.taylor10=CompMIPoissExpTaylor('nterms',10);
%miobj.taylor30=CompMIPoissExpTaylor('nterms',30);

%data is a structure array which stores the mi for each object
data=struct();

%tconverge - true or false indicating whether the Taylor approximation
%converges for that trial
tconverge=nan(1,length(trials));

%entropy of the posterior
pentropy=zeros(1,length(trials));

minames=fieldnames(miobj)';

for fname=minames
    data.(fname{1}).mi=nan(1,length(trials));
end

%load the simulation
v=load(getpath(datafile));

%loop over the trials
for tind=1:length(trials)
    trial=trials(tind);
    fprintf('Trial %d \n', trial);
    post=getpost(v.bssimobj.allpost,trial);

    %get the inputs
    inpbatch=zeros(getparamlen(v.bssimobj.mobj),bsize);

    for bind=1:bsize
        windex=v.bssimobj.paststim(1,trial+bind-1);
        repeat=v.bssimobj.paststim(2,trial+bind-1);
        rtrial=v.bssimobj.paststim(3,trial+bind-1);
        [stim]=gettrialwfilerepeat(v.bssimobj.stimobj.bdata,windex,repeat,rtrial,v.bssimobj.mobj);
        inpbatch(:,bind)=projinp(v.bssimobj.mobj,stim,zeros(getshistlen(v.bssimobj.mobj),1));
    end

    if ~isempty(getc(post))
        %loop over the different miobjects
        for miname=minames
            min=miname{1};
            data.(min).mi(tind)=compminfo(miobj.(min),inpbatch,post);

        end

        %compute the posterior entropy
        pentropy(tind)=compentropy(post);
        %determine whether the taylor approximation converges
        %imat=compimat(miobj.ub,inpbatch,post);

%         if (max(eig(imat))>=1)
%             tconverge(tind)=false;
%         else
%             tconverge(tind)=true;
%         end
    else
        fprintf('Skipping trial %d covariance matrix of posterior was not stored \n', trial);
    end
end

%%
%**************************************************************************
%plot the difference between the upper and lower bounds
%**************************************************************************
fdiff=FigObj('title','Difference between lower and upper bounds', 'naxes',[3 1],'height',4,'width',4);

setfocus(fdiff.a,1,1)
ps=PlotStyles();
ps.markers={'o'};
ps.lstyles={'-'};
ps.colors=[0 0 1];

hold on;


hp=plot(trials,data.ub.mi-data.lb.mi,'o');
pstyle=plotstyle(ps,1);
pstyle.markersize=4;
addplot(fdiff.a(1,1),'hp',hp,'pstyle',pstyle);

%set(fdiff.a(1,1),'ytick', 10.^[-4:1]);

ylabel(fdiff.a(1,1),'ub-lb (bits)');

%plot it as a percent
setfocus(fdiff.a,2,1)

hp=plot(trials,100*(data.ub.mi-data.lb.mi)./data.lb.mi,'o');
pstyle=plotstyle(ps,1);
pstyle.markersize=4;
addplot(fdiff.a(2,1),'hp',hp,'pstyle',pstyle);

ylabel(fdiff.a(2,1),'ub-lb/lb*100');

pind=1;
switch bsize
    case 1
        yl=[-4 2];
    otherwise
        yl=[-4 1];
end
set(fdiff.a(pind,1),'ytick',10.^[yl(1):yl(2)]);
set(fdiff.a(pind,1),'ylim',10.^yl);

pind=2;

switch bsize
    case 1
        yl=[-2 2];
    case 3
        yl=[-1 3];
    otherwise
    yl=[2 4];
end
set(fdiff.a(pind,1),'ytick',10.^[yl(1):yl(2)]);
set(fdiff.a(pind,1),'ylim',10.^yl);

xlabel(fdiff.a(2,1),'trial');
set(fdiff.a(1,1),'yscale','log');
set(fdiff.a(2,1),'yscale','log');


%************************************************************
%on the third axes plot the entropy of the posterior
%*************************************************************
%plot it as a percent
pind=3;
setfocus(fdiff.a,pind,1)
hp=plot(trials,pentropy,'o');

addplot(fdiff.a(2,1),'hp',hp,'pstyle',pstyle);

ylabel(fdiff.a(pind,1),'Posterior Entropy (bits)');


lblgraph(fdiff);

%%
return;
%**************************************************************************
%plot the results
%**************************************************************************
fmi=FigObj('xlabel','trial','ylabel','Mutual Information');

ps=PlotStyles();
ps.markers={'o'};
ps.lstyles={'none'};

hold on;
%plot the results for each method
%make the marker sizes decreasing in size so that we can see the overlap in
%the markers
msize=4*(length(minames)+1);

for mind=1:length(minames);
    min=minames{mind};
    hp=plot(trials,data.(min).mi,'o');
    pstyle=plotstyle(ps,mind);
    pstyle.markersize=msize;
    fmi.a=addplot(fmi.a,'hp',hp,'lbl',min,'pstyle',pstyle);
    msize=msize-4;
end
set(fmi.a,'ylim',[0 1]);
set(fmi.a,'yscale','log');
lblgraph(fmi);
