%make a spectrogram of the input
DATADIR=fullfile(RESULTSDIR,'bird_song');
fname='stimuli/pipi0111-10_48K_norm.wav';

%fname='pure_tones/500hz.wav';
%fname='pure_tones/1k.wav';

[data, fs,nbits]=wavread(fullfile(DATADIR,fname));

%presumably if there is two columns to data its because sound is stereo
%(left and right)
data=data(:,1);

%take 1 second of the sound 10 seconds into the data
% tstart=10;  %how many seconds at which to start segment
% slen=1;     %length of segment in seconds
% data=data(tstart*fs:tstart*fs+fs*slen,1);

%we want to sample the temporal axis of the spectrom at tsamp intervals
tsamp=.01; %sample at 1ms

%noverlap is amount of overlap between adjacent windows in which we compute
%the frequency decomposition
noverlap=0;
%number of frequency points used to compute the dft
%we set the number of points to a power 2.
%we use the maximum of 256 or the number of points in each time windo (i.e
%fs*tsamp)
nfft=max(256,2^(floor(log2(fs*tsamp))+1));


fh=figure;
spectrogram(data,fs*tsamp,noverlap,nfft,fs,'yaxis');
title(sprintf('nfft=%d',nfft));
set(gca,'yscale','linear')
ylim([0 2000])
