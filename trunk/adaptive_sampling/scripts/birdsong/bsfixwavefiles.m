%05-23-2008
%
%Script to fix the fact that the sample rate for various wave files was
%saved incorrectly.

%rawfile='br2523_06_003-Ch1-neuron1.mat';
%rawfile='k302_06_001-Ch2-neuron1.mat';
%rawfile='k314_01_001-Ch1-neuron1.mat';

%rfiles=phdbsdata_neurons_uncorrected();


rdir=fullfile('bird_song','sorted');

rfiles=FilePath('RESULTSDIR',fullfile(rdir,'k302_02_004-Ch1-neuron1'));
for rind=1:length(rfiles)

    stimdir=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','stimuli'),'isdir',true);

    rawdatafile=rfiles(rind);

    data=load(getpath(rawdatafile));

    %file to save raw data to
    [fpath, fname]=fileparts(getpath(rawdatafile));
    outraw=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted',[fname '_corrected.mat']));

    %new data structure with the name of the wave files set to the corrected
    %wave file
    corrdata=data;
    corrdata.waves=cell(1,length(corrdata.waves));

    wind=1;


    %make sure all wave files exist
    allexist=true;

    for wind=1:length(data.waves)
        if ~exist(fullfile(getpath(stimdir),[data.waves{wind} '.wav']),'file')
            fprintf('Missing: %s \n',data.waves{wind});
            allexist=false;
        end

    end

    if ~(allexist)
        fprintf('Aborting conversion missing wave files. \n');
    end
    for wind=1:length(data.waves)
        [wsig,fs,nbits]=wavread(fullfile(getpath(stimdir),data.waves{wind}));


        %savethe wave file
        [fpath, fname]=fileparts(data.waves{wind});

        fnamecorr=[fname '_corrected' '.wav'];
        outwfile=fullfile(getpath(stimdir),fnamecorr);

        corrdata.waves{wind}=fnamecorr;
        fs=48828;

        if ~exist(outwfile,'file')
            wavwrite(wsig,fs,nbits,outwfile);
        else
            fprintf('Not creating the corrected wave file because it already exists. \n' );
        end

        %check it
        [cf.wsig,cf.fs,cf.nbits]=wavread(outwfile);


    end

    %save it
    save(getpath(outraw),'-struct','corrdata');



end

return;
%**********************************************************************8
%for testing purposes

freqsubsample=1;
obsrvwindow=.005;
stimdur=obsrvwindow;
oxfs=10;
snobsrv=10;
bodata=BSData('fname',rawdatafile,'stimnobsrvwind',snobsrv,'freqsubsample',freqsubsample,'obsrvwindowxfs',oxfs);

obsrvwindowcorr=1/fs*200;
stimdursamps=200;
bcorrdata=BSData('fname',outraw,'stimnobsrvwind',snobsrv,'freqsubsample',freqsubsample,'obsrvwindowxfs',oxfs);
