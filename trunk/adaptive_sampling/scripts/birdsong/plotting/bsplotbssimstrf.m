%09-24-2008
%   Make plots for NIPS
%
%07-17-2008
%
%plot the STRF in a bssimobj
%
%Explanation: This function plots the Stimulus and the STRF after the
%trials you specify
clear all;
setpathvars;

%alldsets=nips08bsbatchlowd;
%dsets=alldsets(6);
%dsets=xfunc('~/svn_glms/allresults/bird_song/080924/bsinfomax_setup_004.m');

dsets.setupfile='/home/jlewi/svn_glms/allresults/bird_song/081009/bsinfomax_setup_001.m';
dsets=xfunc(dsets.setupfile);

v=load(getpath(dsets.datafile));

bssimobj=v.bssimobj;

%what trial to plot the posterior on
trials=[0:10];
trials=[10 50 100 500 1000 2000 3000];
trials=[100 500 1000 10000];
trials=[25:25:100 500 1000];

%trials=[1:10];


nfstim=0;

plotstim=false;

tind=0;

%structure to store info about each plot
pinfo=struct([]);
for trial=rowvector(trials)


    %************************************************
    %check if we are just plotting the prior
    %***********************************************
    if (trial==0)
        nplots=1;
    else
        if (plotstim)
            nplots=2;
        else
            nplots=1;
        end
    end


    if (nplots==2)
        nfstim=nfstim+1;
        fstim(nfstim)=FigObj('name','stimulus','width',5,'height',8,'xlabel','time(s)','ylabel','Frequenzy(hz)','title','Stimulus');

        %*************************************************************
        %plot the stimulus
        %***************************************************************
        windex=bssimobj.paststim(1,trial);
        repeat=bssimobj.paststim(2,trial);
        stimtrial=bssimobj.paststim(3,trial);
        [stim,shist,obsrv]=gettrialwfilerepeat(bssimobj.stimobj.bdata,windex,repeat,stimtrial,bssimobj.mobj);

        stimmat=reshape(stim,getklength(bssimobj.mobj),getktlength(bssimobj.mobj));

        %plot the strf

        [t,f]=getstrftimefreq(getbdata(bssimobj.stimobj));
        imagesc(t,f,stimmat);
        hc=colorbar;
        sethc(fstim(nfstim).a,hc);
        xlim([t(1) t(end)]);
        ylim([f(1) f(end)]);
    end

    %*************************************************************
    %plot the STRF
    %***************************************************************
    if (isa(bssimobj.mobj,'MTanSpace') && dsets.usetanpost)
        fpost=getpost(bssimobj.allpost,trial);
        if isempty(getc(fpost))
            warning('Trial %d: Can''t compute MAP on tangent space b\c C_t wasn''t saved',trial);
            theta=[];
        else
            post=PostTanSpace(fpost,bssimobj.mobj);
            theta=getm(post.infopost);
        end

    else
        dsets.usetanpost=false;
        theta=getm(bssimobj.allpost,trial);
    end

    if ~isempty(theta)
        nrows=2;
        mobj=bssimobj.mobj;
        %get rid of the bias term
        %strf=reshape(theta(1:getklength(mobj)),getstimdim(bdata));
        %shistcoeff = theta(getklength(mobj)+1:getklength(mobj)+getshistlen(mobj),1);
        [stimcoeff shistcoeff bias]=parsetheta(mobj,theta);
        strf=getstrf(mobj,theta);



        %plot the strf
        tind=tind+1;
        
        pinfo(tind).trial=trial;
        fstrf(tind)=FigObj('name','STRF','width',5,'height',8,'xlabel','time(s)','ylabel','Frequenzy(hz)','title','STRF','naxes',[nrows,1]);

        setfocus(fstrf(tind).a,1,1);
        [t,f]=getstrftimefreq(bssimobj.stimobj.bdata);
        imagesc(t,f,strf);
        hc=colorbar;
        sethc(fstrf(tind).a(1,1),hc);
        title(fstrf(tind).a(1,1),'STRF');
        xlim([t(1) t(end)]);
        ylim([f(1) f(end)]);


        %spike history coefficients
        row=nrows;
        if (getshistlen(mobj)>0)
            setfocus(fstrf(tind).a,row,1);
            t=-getobsrvtlength(bssimobj.stimobj.bdata)*[getshistlen(mobj):-1:1];
            hp=plot(t,shistcoeff);
            pstyle.marker='.';
            pstyle.markerfacecolor='b';
            addplot(fstrf(tind).a(row,1),'hp',hp,'pstyle',pstyle);
            xlabel(fstrf(tind).a(row,1),'time(s)');
            ylabel(fstrf(tind).a(row,1),'spike history coefficient');
            xlim([t(1) t(end)]);
        end

        lblgraph(fstrf(tind));

        %set the heights of the plot
        %make spike history 1/4 the height of the strf plots
        heights=ones(1,nrows);

        if (getshistlen(mobj)>0)
            heights(end)=heights(1)/4;
            heights=heights/sum(heights);
        end
        sizesubplots(fstrf(tind),[],[],heights);

    end
end

scriptname=mfilename();
%%
%**********************************************************************
%Create output xml file
%*********************************************************************
otbl={};

%construct the data to be printed just once
explain={'A plot of the input and the MAP on several trials. Use tanpost indicates we are using the posterior regularized using the tangent space'};
odata={'scriptname',scriptname;'setupfile',dsets.setupfile;'Simulation Label',bssimobj.label; 'use tanpost', dsets.usetanpost; 'explanation',explain};

nfstim=0;
for tind=1:length(pinfo)
    if (tind==1)
        odata=[odata;{'trial',pinfo(tind).trial}];
    else
        odata={'trial',pinfo(tind).trial};
    end
    if ((trials(tind)~=0) && plotstim)
        nfstim=nfstim+1;
        otbl=[otbl;{fstim(nfstim) odata;fstrf(tind),{'trial',pinfo(tind).trial}}];
    else
        otbl=[otbl; {fstrf(tind) odata}];
    end

end
onenotetable(otbl,seqfname('~/svn_trunk/notes/stim.xml'));