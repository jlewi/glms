%Author: Jeremy Lewi
%
%Explanation: This script finds the MAP estimate of the STRF fitted to some
%birdsong data.
%
%To setup a simulation use BSSetup.setupfitpoiss. This creates the
%appropriate datafiles containing a BSBatchMLSim object. This function
%then calls findMAP to find the MAP.
clear variables;
setpathvars;


%dsets=xfunc(fullfile(RESULTSDIR,'bird_song',datedir,setupfile));
datafile=FilePath('RESULTSDIR','bird_song/090415/bsglmfit_data_003.mat');


v=load(getpath(datafile));

bssimobj=v.bssimobj;
[results]=findMAP(bssimobj,[],datafile,[]);





return

%**************************************************************************
%compute the magnitude of the posterior at the true parameters
%*********************************************************************
%%
data=load(getpath(dsets.datafile));
bdata=data.bdata;
allpost=data.allpost;
prior=getpost(allpost,0);

bpost=BSlogpost('bdata',bdata);
[lpost dlpost]=complpost(bpost,mobj,[fakemodel.strfcoeff;fakemodel.bias],prior);

%%
%compare it to the true theta
fcomp=FigObj('name','Compare true theta and MAP','height',6,'width',4);

fcomp.a=AxesObj('nrows',2,'ncols',1);

%MAP

bdata=data.bdata;

[t,freqs]=getstrftimefreq(bdata);

fcomp.a=setfocus(fcomp.a, 1,1);
imagesc(t,freqs,reshape(bsfit(end).theta(1:getklength(mobj)*getktlength(mobj)),getklength(mobj),getktlength(mobj)));
set(gca,'ydir','reverse');
hc=colorbar;
fcomp.a(1,1)=sethc(fcomp.a(1,1),hc);
fcomp.a(1,1)=title(fcomp.a(1,1),'STRF MAP');

dt=getobsrvtlength(bdata)/2;
df=(freqs(2)-freqs(1))/2;
xlim([t(1)-dt t(end)+dt]);
ylim([freqs(1)-df freqs(end)+df]);
set(gca,'xtick',t);

%True theta
fcomp.a=setfocus(fcomp.a, 2,1);
imagesc(t,freqs,reshape(fakemodel.strfcoeff,getklength(mobj),getktlength(mobj)));
set(gca,'ydir','reverse');
hc=colorbar;
fcomp.a(2,1)=sethc(fcomp.a(2,1),hc);
fcomp.a(2,1)=title(fcomp.a(2,1),'STRF: True');

xlim([t(1)-dt t(end)+dt]);
ylim([freqs(1)-df freqs(end)+df]);
set(gca,'xtick',t);

fcomp=lblgraph(fcomp);
fcomp=sizesubplots(fcomp);

