%***************************************************************
%Setup a parallel job which measures the time 
%of computing the log likelihood of the data
%********************************************************

%**************************************************************************
%parameters
%**************************************************************************

%whether to capture the output of the command window.
capcmdoutput=true;


freqsubsample=32;
obsrvwindowxfs=250;
stimnobsrvwind=20;
alength=0;
windexes=[1:30];


numworkers=[15 20 25 30];
numworkers=[5 20 25];


%THIS FILE MUST EXIST ON BAYES OR YOU WILL GET ERROR
bsdata=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1_corrected.mat'));
wavefiles=FilePath('bcmd','RESULTSDIR','rpath','bird_song/stimuli','isdir',true); 
datafile=FilePath('bcmd','RESULTSDIR','rpath','bird_song/runtime_080627.mat');
statusfile=FilePath('bcmd','NL_JOBDIR','rpath', sprintf('status.txt'));

%whether to issue svn update
svnupdate=false;


MATLABPATH='~/svn_trunk/matlab';
svndirs={pwd,MATLABPATH};


finfiles={datafile,bsdata,wavefiles};
foutfiles={datafile};



%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='/home/jlewi/svn_trunk/allresults';
startupfunc=@taskstartupfunc;

  jname='timebs';

  startdir='/Users/jlewi/svn_trunk/adaptive_sampling';
%**********************************************************************
%run svn update - but dont do it on bayes.
[s,hname]=system('hostname -s');

if ~(strcmp(deblank(hname),'bayes'))
   if (svnupdate)
    fprintf('Issueing svn update \n');
    system('svn update');
    wd=pwd;
    fprintf('svn update matlab path \n');
    cd(MATLABPATH);
    system('svn update');
    cd(wd);
    end
end

  
%**************************************************************************
%Setup the scheduler
sched=distsched();

[jobdata,bcmd]=initjobdata(jname,finfiles,foutfiles,startdir,startupfunc,localhost,localdatadir)


jdep=[];

%run the job once for each number of workers
for nw=numworkers
  set(sched,'ParallelSubmitFcn',{@sgeParallelSubmitFcn,'/Users/jlewi/svn_trunk/matlab/neuro_cluster/paralleljob','jdep',jdep});
pjob=createParallelJob(sched);

pjob.JobData=jobdata;
set(pjob,'MaximumNumberOfWorkers',nw);
set(pjob,'MinimumNumberOfWorkers',nw);

%include the path for matlab (i.e path containing setmatlabpath) because
 %my setpaths script isn't correcly adding the directory containing matlab.
%set(pjob,'PathDependencies',{'/Users/jlewi/svn_trunk/matlab/neuro_cluster/paralleljob'});


nargout=1;
%ptask=createTask(pjob,@fprintf,nargout,{'RESULTSDIR=%s \n',RESULTSDIR});
ptask=createTask(pjob,@timebslogpost,nargout,{datafile,freqsubsample,obsrvwindowxfs,stimnobsrvwind,alength,windexes});
set(ptask,'CaptureCommandWindowOutput', capcmdoutput);



%submit the job
submit(pjob);

jdep=get(pjob,'name');
end