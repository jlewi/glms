%script: createdist.job_template.m
%author: jeremy lewi - jeremy@lewi.us
%
%Explanation: This is a script which provides setups a distributed job for 
%running the info. max. algorithm
%   Running it as a distributed job isn't very efficient because we can
%   parallelize the optimization of the stimuli.
%
%   However this is useful for running simulations which randomly shuffle
%   the stimuli since we can't parallelize the stimulus selection in this
%   case.
clear variables;
setpathvars;

setupscript=fullfile(RESULTSDIR,'bird_song','080828','bsinfomax_setup_003.m');
dsets=xfunc(setupscript);

%number of trials to run
ntorun=50000;
%**************************************************************************
%parameters
%
%The values below should be customized for your setup
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/svn_trunk/adaptive_sampling';

%whether or not to capture command window output
capcmdout=false;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=true;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
MATLABPATH='~/svn_trunk/matlab';
svndirs={'~/svn_trunk/adaptive_sampling',MATLABPATH};



%name to use for the job
jname='jtemp';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='~/svn_trunk/allresults';

%if there's some code you want to execute before starting
%the function (i.e setting the path) you can specify a handle to this
%function
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=@taskstartupfunc;

%The directory on your local computer where jobs should be stored
localjobdir='~/jobs';

%directory on the cluster where jobs should be stored
%This path should be accessible from the head nodes and worker nodes
%use a different directory than the directory we use to store jobs
%submitted from the head node. This ensures no conflict when naming the job
clusterjobdir='/Users/jlewi/jobsremote';

clusterhost='cluster.neuro.gatech.edu';


finfiles={};

%an array of FilePath objects specfying the output files
foutfiles={};
%********************************************************************
%files to transfer
%an array of FilePath objects the input files
fnames=fieldnames(dsets);
for j=1:length(fnames)
    if isa(dsets.(fnames{j}),'FilePath')
        finfiles=[finfiles, {dsets.(fnames{j})}];
    end
end

foutfiles={dsets.datafile, dsets.mfile, dsets.cfile, dsets.statusfile};

%**************************************************************************
%update the node on the cluster
%*************************************************************************
if (svnupdate)
    wd=pwd;
    for j=1:length(svndirs)
        cmd =sprintf('ssh %s svn update "%s"',clusterhost, svndirs{j}');
        fprintf('Issueing svn update  on remote host in %s \n',svndirs{j});

        [status, msg]=system(cmd);
        fprintf('output: %s ',msg);
    end
end

%*********************************************************************
%setup the job
%************************************************************************

%get the scheduler
%distsched.m is a function which is part of the Neurolab Matlab toolbox
%which creates a scheduler for use with the Neurolab Matlab cluster.
sched=distsched(localjobdir);

%we have to modify the scheduler object's submit fcn property
%because we have to specify the directory on the cluster where 
%the data is stored
set(sched,'submitFcn',{@sgeremoteSubmitFcn,clusterhost,clusterjobdir})





%**************************************************************************
%create the jobs
%**************************************************************************
[jobdata]=initjobdata(jname,finfiles,foutfiles,startdir,startupfunc,localhost,localdatadir,localjobdir,clusterjobdir);



%create the job
job=createJob(sched,'name','imaxdist');

%set the jobdata
job.JobData=jobdata;



%number of output arguments
naout=1;


%input arguments
iparam={dsets.datafile,ntorun,dsets.statusfile};

task=createTask(job,@bsinfomaxcont,naout,iparam);

%task properties
set(task,'CaptureCommandWindowOutput',capcmdout);


submit(job);

