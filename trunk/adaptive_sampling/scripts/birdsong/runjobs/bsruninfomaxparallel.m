%function [pjob,dsets,dsetfile]=bsruninfomaxparallel(dsets,nruns)
%   dsets - an array of the datfiles to run
%   nruns - how many runs to do
%****************************************************************************
%Date:07-21-2008
%Author: Jeremy Lewi
%Explanation: Submit a parallel job for running infomax
%
%$Revision$
%
%
%10-10-2008
%   Create a loop so we can submit multiple jobs
%
%08-07-2008
%   Make it a function so that we don't have to worry about workspace variables
%   having the wrong value.
%
%**************************************************************************
%specify the setup script
%**************************************************************************
function [siminfo,dsets,dsetfile]=bsruninfomaxparallel(alldsets,nruns)


%how many trials to run.
%this can be an array in which case each element of ntorun is a unique job which can't run until
%the previous job completed. This allows us to easily break up a long run into many subtrials so that we can
%inspect intermediary trials.
%
%We alternate the jobs, so that we run each sim once before switching to the other simulation
ntorun=5000*ones(1,nruns);


%describe each simulation
%each row is a different data set
%each column is a different run
siminfo=struct('job',[],'dset',[],'sgejobid',[]);

%**************************************************************************
%parameters
%
%The values below should be customized for your setup
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/svn_glms/adaptive_sampling';

%whether or not to capture command window output
capcmdout=false;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=true;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
svndirs={'~/svn_glms'};


%an array of FilePath objects the input files
filesin={};

%an array of FilePath objects specfying the output files
filesout={};

%name to use for the job
jname='template';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='/home/jlewi/svn_trunk/allresults';

%if there's some code you want to execute before starting
%the function (i.e setting the path) you can specify a handle to this
%function
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=@taskstartupfunc;





%**************************************************************************
%update the node on the cluster
%*************************************************************************
if (svnupdate)
    wd=pwd;
    for j=1:length(svndirs)
        if ~exist(svndirs{j},'dir')
            error(['Cannot issue svn update in %s, directory does not ' ...
                'exist.'],svndirs{j});
        end
        cd(svndirs{j});
        fprintf('Issueing svn update in %s \n',svndirs{j});

        system('svn update');

    end
    cd(wd);
end

%****************************************************
%load the data set
%*****************************************************
%jobid's is a length(ntorun)*length(siminfo) structure
%storing the jobids of each run
%this is used to set jdep appropriately
jobids=nan(length(ntorun),length(siminfo));

for sindex=1:length(alldsets)
for rind=1:length(ntorun)
    

        dsets=alldsets(sindex);
        if (rind>1)
            if (isnan(jobids(rind-1,sindex)))
                jdep=[];
            else
                jdep=num2str(jobids(rind-1,sindex));
            end
        else
            jdep=[];
        end



        %***********************************************************
        %Determine the files to copy to the cluster
        %**********************************************************
        filesin=[];
        fnames=fieldnames(dsets);
        for j=1:length(fnames)
            fname=fnames{j};
            if isa(dsets.(fname),'FilePath');
                filesin=[filesin dsets.(fname)];
            end
        end


        %***********************************************************
        %Determine the files to copy from the cluster to the host
        %**********************************************************
        filesout=[dsets.datafile dsets.mfile dsets.cfile dsets.statusfile];

        if isfield(dsets,'poolfile')
            filesout=[filesout dsets.poolfile];
        end

        %**************************************************************************
        %Setup the scheduler
        sched=distsched();

        psubmit=get(sched,'ParallelSubmitFcn');

        if iscell(psubmit)
            psubmit=[psubmit; {'jdep';jdep}];
        else
            psubmit={psubmit, 'jdep',jdep};
        end

        set(sched,'ParallelSubmitFcn',psubmit);

        %**************************************************************************
        %create the parallel job
        %************************************************************************
        [jobdata]=initjobdata(jname,filesin,filesout,startdir,startupfunc,localhost,localdatadir);
        pjob=createParallelJob(sched);
        pjob.jobData=jobdata;



        %set the pathe dependencies
        %include the path for matlab (i.e path containing setmatlabpath) because
        %my setpaths script isn't correcly adding the directory containing matlab.
        %set(pjob,'PathDependencies',{'/Users/jlewi/svn_trunk/matlab/neuro_cluster/paralleljob','/Users/jlewi/svn_trunk/matlab'});


        nworkers=dsets.nwindexes;
        set(pjob,'MaximumNumberOfWorkers',nworkers);
        set(pjob,'MinimumNumberOfWorkers',nworkers);


        %********************************************************************************************
        %create the task
        %*************************************************************************************************
        nargout=1;
        ptask=createTask(pjob,@bsinfomaxcont,nargout,{dsets.datafile,ntorun(rind),dsets.statusfile});
        set(ptask,'CaptureCommandWindowOutput', capcmdout);
        jobdata.jobname=jname;
        set(pjob,'JobData',jobdata);

        %submit the job
        submit(pjob);

        
        %get the job id of this job
        %we use qstat to do this
        cmd=sprintf('qstat | awk ''$3 ~/%s/ {print $1}'' ',get(pjob,'name'));
        [s,w]=system(cmd);

        jobids(rind,sindex)=str2num(w);


        siminfo(sindex,rind).job=pjob;
        siminfo(sindex,rind).dset=dsets;
        siminfo(sindex,rind).sgejobid=jobids(rind,sindex);
        
        %***************************************************************************
        %Print sumarry information
        %******************************************************************************
        fprintf('\nJob Name: \t %s\n',get(pjob,'name'));
        fprintf('datafile: \t %s \n',getpath(dsets.datafile));
        fprintf('ntorun: \t %d \n', ntorun(rind));
    end
end %loop for ntorun