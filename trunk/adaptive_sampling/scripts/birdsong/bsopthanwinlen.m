%4-21-2008
%
%Optimize the length of the hanning windo
%
%we compute the average firing rate for each trial using various length
%hanning windows. We then evaluate the effectiveness of different window
%lengths by measuring the error.
%
%For each wave file we compute the trial and across trial average PSTH
%We then subtract out the average PSTH from each trial. We compute the
%energy of this resulting signal. We sum the error energies across all
%trials for all wave files and minimize the result

clear all;
setpathvars;
xscript('/home/jlewi/svn_trunk/allresults/bird_song/080422/bsglmfit_setup_001.m');

load(getpath(dsets.datafile),'bdata');

binsize=.001;

hwinlengths=[.001 .02 .1];



%which wave files to use optimize the correlation
waveindexes=[1:30];

data=[];
%for ind=1:length(waveindexes)
    
for ind=1:10
    fprintf('Wave file %d \n',ind);
    wind=waveindexes(ind);

    %get the spike times on all the trials for this wave file
    spiketimes=getstimresp(bdata,wind);


    tlastspike=max([spiketimes{:}]);
    for hind=1:length(hwinlengths)
        %create our hanning window normalized by the bin size and
        hwinlen=hwinlengths(hind)/binsize;
        hwin=hann(hwinlen);
        hwin=hwin/(sum(hwin))/binsize;


        bc=[0:binsize:(ceil(tlastspike/binsize)*binsize)];

        %psth on each trial
        data(ind,hind).trialpsth=zeros(length(spiketimes),length(bc));
        data(ind,hind).trialfrate=zeros(length(spiketimes),length(bc));

        for sind=1:length(spiketimes)
            scounts=histc([spiketimes{sind}],bc);
            data(ind,hind).trialpsth(sind,:)=scounts;
            %dscounts=[zeros(1,floor(length(hwin)/2)) scounts(1:(end-floor(length(hwin)/2)))];

            frate=conv(scounts,hwin);
            frate=frate(1:length(scounts));
            data(ind,hind).trialfrate(sind,:)=frate;

        end
        %average psth
        data(ind,hind).avgfrate=mean(data(ind,hind).trialfrate,1);

        %throw out the points where we get a zero firing rate across
        %all trials as otherwise there will be a bias the correlation towards small
        %window lengths.
        zpsth=data(ind,hind).trialfrate;
        nzind=find(sum(zpsth,1)>0);
        zpsth=zpsth(:,nzind);
        
        %compute the zero-mean correlation
        %zpsth=data(ind,hind).trialfrate-ones(length(spiketimes),1)* data(ind,hind).avgfrate;

        %don't zero mean
        %compute the correlation
        zcorr=zpsth*zpsth'/(length(nzind));
        %normalize the error by the number of samples
        %data(ind,hind).errenergy=sum(zpsth.^2,2);
        %data(ind,hind).toterrenergy= sum(data(ind,hind).errenergy);
        
        % sumthe cross correlations
        crosscor=triu(zcorr,1);
        data(ind,hind).tcrosscor=sum(crosscor(:));
    end
end

%sum the error energy across wave files
berrenergy=zeros(1,length(hwinlengths));

for bind=1:length(hwinlengths)

    berrenergy(bind)=sum([data(:,bind).tcrosscor]);
end

[merr bind]=max(berrenergy);
fprintf('Optimal hanning window length=%0.3g seconds\n',hwinlengths(bind));