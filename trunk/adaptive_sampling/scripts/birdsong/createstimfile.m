%3-25-2008
%
%This script chunks the bird song data into trials and saves an array of SR
% objects to a file

setpathvars;

%load the data
dfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','sorted','br2523_06_003-Ch1-neuron1.mat'));


%chunk the data into trials
%maxtrials=10000;

%how long to make the stimulus in time on each trial
stimdur=.050;
%subsample the frequencies of the spectrogram to reduce the dimensionality
%of the stimulus
freqsubsample=4;
bdata=BSData('fname',dfile,'stimdur',stimdur,'freqsubsample',freqsubsample);

chunkfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('bird_song','data_chunks.mat'));


[bdata,data]=chunkdata(bdata);
fname=getpath(seqfname(chunkfile));
%use version 7.3 because file can be larger than 2gb
save(fname,'data','-v7.3');

fprintf('Saved data to %s \n',fname);
