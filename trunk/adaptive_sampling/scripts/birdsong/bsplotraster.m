%04-21-2008
%
%Make a raster plot as David does so I can compare the data as I'm looking
%at it to Davids raster plot

% xscript('/home/jlewi/svn_trunk/allresults/bird_song/080420/bsglmfit_setup_001.m');
% 
% 
% bdata=load(getpath(dsets.datafile),'bdata');
% bdata=bdata.bdata;

fras=FigObj('name','rasterplot','height',8,'width',5);
fras.a=AxesObj('nrows',2,'ncols',1);
%*******************************************************************
%Make a raster plot of the non noise stimuli
%*********************************************************************
fras.a=setfocus(fras.a,1,1);

[bdata,npre,npost]=nframesinsilence(bdata);

nsong=0;
nnoise=0;
for wavindex=1:getnwavefiles(bdata)
    spiketimes=getstimresp(bdata,wavindex);

    hold on;
    
    if waveissong(bdata,wavindex)
        nsong=nsong+1;
        wcount=nsong;
       fras.a=setfocus(fras.a,1,1);
    else
        nnoise=nnoise+1;
        wcount=nnoise;
        fras.a=setfocus(fras.a,2,1);
    end
    %determine if its noise or song
    for rep=1:length(spiketimes)
        offset=11*(wcount-1)+rep-1;
        %each cell array is the spike times on a different trial
        x=[rowvector(spiketimes{rep});rowvector(spiketimes{rep})];
        y=[offset;offset+1]*ones(1,size(x,2));
        line(x,y,'Color','k');
    end
    
    %plot a line showing the stimulus duration
    [bdata,stiminfo]=getstiminfo(bdata,wavindex);
    y=11*(wcount-1);
    
    hp=plot(npre*getobsrvtlength(bdata)+[0, stiminfo.duration],y*[1 1],'k-','LineWidth',2);
    
    
% 
%     xlim([t(1) tlastspike]);
%     ylim([1 length(spiketimes)]);
end

fras.a(1,1)=title(fras.a(1,1),'Raster Plot');
    fras.a(1,1)=ylabel(fras.a(1,1),'Songs');
    fras.a(2,1)=ylabel(fras.a(1,1),'Noise');
    fras.a(2,1)=xlabel(fras.a(1,1),'time(s)');
    