%4-21-2008
%
%Optimize the bin size for the PSTH
%
%we compute the PSTH for each trial using various bin sizes
%we then select the bin size which maximizes the correlation between
%the PSTH's for each trial and the across trial average.
%
%For each wave file we compute the trial and across trial average PSTH
%We then subtract out the average PSTH from each trial. We compute the
%energy of this resulting signal. We sum the error energies across all
%trials for all wave files and minimize the result


binsizes=[.001:.001:.01 .015:.005:.05 .06:.01:.1];

%which wave files to use optimize the correlation
waveindexes=[1:30];

data=[];
for ind=1:length(waveindexes)
       wind=waveindexes(ind);
       
       %get the spike times on all the trials for this wave file
       spiketimes=getstimresp(bdata,wind);
       
       tlastspike=max([spiketimes{:}]);
       for bind=1:length(binsizes)
           bsize=binsizes(bind);
          bc=[0:bsize:(ceil(tlastspike/bsize)*bsize)];
          
          %psth on each trial
          data(ind,bind).trialpsth=zeros(length(spiketimes),length(bc));

          for sind=1:length(spiketimes)
              scounts=histc([spiketimes{sind}],bc);
          data(ind,bind).trialpsth(sind,:)=scounts;

          end
          %average psth
          data(ind,bind).psth=mean(data(ind,bind).trialpsth,1);
          
          %compute the zero-mean correlation
          zpsth=data(ind,bind).trialpsth-ones(length(spiketimes),1)* data(ind,bind).psth;

          %normalize the error by the number of samples
          data(ind,bind).errenergy=sum(zpsth.^2*bsize,2);
          data(ind,bind).toterrenergy= sum(data(ind,bind).errenergy);
       end
end

%sum the error energy across wave files
berrenergy=zeros(1,length(binsizes));

for bind=1:length(binsizes)

berrenergy(bind)=sum([data(:,bind).toterrenergy]);
end

[merr bind]=min(berrenergy);
fprintf('Optimal bin size=%0.3g\n',binsizes(bind));