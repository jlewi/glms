%2-15-07
%make plots of power law for slides for cosyne

fname='/home/jlewi/cvs_ece/allresults/powerlaw/02_15/02_15_powerlaw_001.mat';
xticks=[1 25];
yticks=[1 100 200];
fontsize=30;
postimgfix('fname',fname,'width',6,'height',4,'xticks',xticks,'fsize',fontsize,'yticks',yticks)

%plot of objective function
fobj=miimg(stimmax);
fobj.title='$I(r,\vec{\theta}|\vec{x})$'
fobj.axisfontsize=30;
fobj=lblgraph(fobj);

set(fobj.htitle,'Interpreter','latex');


