%5-08-2006
%
%This script looks at the timing of the regular eig method for computing
%the rank one update of the eigen values and compares it to the timing 
%of the gu and eisenstat method
clear all;
setpaths;
%what size matrices to try
dims=[10:10:100 200:100:1000];
dims=[10:10:100 200:100:500];
dims=[10:10:100 ]
%how many trials to average over when computing the time
ntrials=1;

times.eig=zeros(1,length(dims));
times.rank1=zeros(1,length(dims));

%store the matrices
data.m=[];


rank1.totmse=zeros(1,length(dims));
eigfunc.totmse=zeros(1,length(dims));
for index=1:length(dims)
    n=dims(index)
    
    %*******************************************************************
    %Note: The new matrix is
    %
    %M+ rho*u*u'
    %***********************************************************
    %generate a symmetric matrix
    %we need to sort the eigenvalues in order for the call to dlaed to work
    data.d{index}=sort(rand(n,1));
    %truncate the eigenvalues at 3 pts of precision
   data.d{index}=floor(data.d{index}*10^4)/10^4;
    data.evecs{index}=orth(rand(n,n));
    data.m{index}=data.evecs{index}*diag(data.d{index})*(data.evecs{index})';
    u=floor(rand(n,1)*10^4)/10^3;

    data.u{index}=u;
    
    data.rho{index}=-1;
    %the new matrix
    data.newm{index}=data.m{index}+data.rho{index}*data.u{index}*data.u{index}';
    %time the computation using the rank 1 update
    tic;
    for trial=1:ntrials
        z=data.evecs{index}'*data.u{index};
        [rank1.eigd{index} rank1.evecs{index} rank1.ndeflate(index)]=rankOneEigUpdate(data.d{index},z,data.evecs{index},data.rho{index});
    end
    times.rank1(index)=toc;

    
    %time the computation using the eigen method
    tic;
    for trial=1:ntrials
        [eigfunc.evecs{index} eigfunc.eigd{index}]=eig(data.m{index}+data.rho{index}*data.u{index}*data.u{index}');
    end
    times.eig(index)=toc;
    %sort the eigenvalues and eigenvectors to facilitate comparisons
    eigfunc.eigd{index}=diag(eigfunc.eigd{index});
    [eigfunc.eigd{index} inds]=sort(eigfunc.eigd{index});
    evecs=eigfunc.evecs{index};
    eigfunc.evecs{index}=evecs(:,inds);
    clear('evecs');
%**************************************************************************
%compute the error
%**************************************************************************

%Compute the M.S.E between A S and S Lambda
%each column is M.SE for a different column
   rank1.mse{index}=data.newm{index}*rank1.evecs{index}-rank1.evecs{index}*diag(rank1.eigd{index});
    rank1.mse{index}=sum((rank1.mse{index}).^2,1);
    rank1.totmse(index)=sum(rank1.mse{index});
    
    eigfunc.mse{index}=data.newm{index}*eigfunc.evecs{index}-eigfunc.evecs{index}*diag(eigfunc.eigd{index});
    eigfunc.mse{index}=sum((eigfunc.mse{index}).^2,1);
    eigfunc.totmse(index)=sum(eigfunc.mse{index});

end

%**************************************************************************
%

ftiming.hf=figure();
hold on;
ftiming.xlabel='Size';
ftiming.ylabel='Time(seconds)';
ftiming.hp=[];
ftiming.lgndfontsize=15;
ftiming.axisfontsize=15;
ftiming.title='Eigen Timing';
ftiming.fname=fullfile(RESULTSDIR,'rank1eig','rank1time.eps');

pind=1;
ftiming.hp(pind)=plot(dims, times.eig/ntrials,getptype(pind,1));
plot(dims, times.eig,getptype(pind,2));

ftiming.lbls{pind}='Regular Eigendecomposition';

pind=pind+1;
hold on;
ftiming.hp(pind)=plot(dims, times.rank1/ntrials,getptype(pind,1));
plot(dims, times.rank1,getptype(pind,2));

ftiming.lbls{pind}='Gu and Eisenstat';

lblgraph(ftiming);

%**************************************************************************
%mse
fmse.hf=figure();
hold on;
fmse.xlabel='Size';
fmse.ylabel='MSE';
fmse.hp=[];
fmse.lgndfontsize=15;
fmse.axisfontsize=15;
fmse.title='Eigen MSE';
fmse.fname=fullfile(RESULTSDIR,'rank1eig','rank1mse.eps');

pind=1;
fmse.hp(pind)=plot(dims,eigfunc.totmse);
plot(dims, eigfunc.totmse,getptype(pind,2));

fmse.lbls{pind}='Regular Eigendecomposition';

pind=pind+1;
hold on;
fmse.hp(pind)=plot(dims, rank1.totmse,getptype(pind,1));
plot(dims, rank1.totmse,getptype(pind,2));

fmse.lbls{pind}='Gu and Eisenstat';

lblgraph(fmse);

%**************************************************************************
%mse
fdeflate.hf=figure();
hold on;
fdeflate.xlabel='Size';
fdeflate.ylabel='#';
fdeflate.hp=[];
fdeflate.lgndfontsize=15;
fdeflate.axisfontsize=15;
fdeflate.title='Deflation';
fdeflate.fname=fullfile(RESULTSDIR,'rank1eig','ndeflate.eps');

pind=1;
fdeflate.hp(pind)=plot(dims,rank1.ndeflate);
plot(dims, eigfunc.totmse,getptype(pind,2));



pind=pind+1;
hold on;
%fdeflate.hp(pind)=plot(dims, rank1.totmse,getptype(pind,1));
%plot(dims, rank1.totmse,getptype(pind,2));
%fdeflate.lbls{pind}='Gu and Eisenstat';

lblgraph(fdeflate);


%************************************************************
%savegraphs?
savegraphs=0;
if (savegraphs~=0)
    exportfig(fdeflate.hf,fdeflate.fname,'bounds','tight','Color','rgb');
    exportfig(fmse.hf,fmse.fname,'bounds','tight','Color','rgb');
    exportfig(ftiming.hf,ftiming.fname,'bounds','tight','Color','rgb');
end