%look at stats related to mse and why it might not be so good.
meths={prand.newton, pmax.newton}
lbls={'Rand','Max'};
c=prand.newton.c;

figure;
hold on;
for mindex=1:length(meths)
    c=meths{mindex}.c;
    eigvalues=zeros(mparam.tlength,simparam.niter+1);
    for index=1:size(srmax.newton.y,2)
        [curru s]=svd(c{index});

        eigvalues(:,index)=diag(s);
    end    



    %(mparam.tlength,1,1);
    for index=1:mparam.tlength
        pind=(mindex-1)*mparam.tlength+index;
        h(pind)=plot(1:simparam.niter+1,eigvalues(index,:),getptype(pind,1));
        plot(1:simparam.niter+1,eigvalues(index,:),getptype(pind,2));
        lgnd{pind}=(sprintf('%0.2g Eigenvalue (%s)',index,lbls{mindex}));    
    end
end
set(gca,'YScale','Log');
xlabel('Iteration');
ylabel('Eigenvalue');
legend(h,lgnd);