%maximize the quadratic function which is a lower bound of the fisher
%information
pg.m=[-1;1];
pg.c=[.5 0; 0 .1];
[evec eval]=eig(pg.c);
%get an erray of eigen values 
dimstim=size(pg.c,1);
ind=[1:dimstim]'*ones(1,2);
diagind=indexoffset(ind,size(pg.c));
eval=eval(diagind);

%minimize the function
%x = fmincon(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon)
A=[];
b=[];
Aeq=[];
beq=[];
ub=1;
lb=-1;


%compute the parameters for our parameteric function
Gvec=eval/2+log(eval)/1;
g=eye(dimstim,dimstim);
g(diagind)=Gvec;
%now make G elements of a diagonal matrix
%d is projection of mu onto eigenbasis
d=evec'*pg.m;
%this gives the optimal vector using eigen vectors as basis
y = fmincon(@(x) fquadmin(x,g,d),pg.m,A,b,Aeq,beq,lb,ub,@nonlincon);

%compute optimal x
x=evec*y;
%compute the fisher information
finfo=exp(pg.m'*x+1/2*x'*pg.c*x)*x'*pg.c*x;
fprintf('Fisher Information: %0.2g \n',finfo);

%compare to result of maximizing F(x) using lagrange
xcomp=[-1/2^.5;1/2^.5];
finfocomp=exp(pg.m'*xcomp+(1/2)*xcomp'*pg.c*xcomp)*x'*pg.c*x;
fprintf('Fisher Information Lagrange: %0.2g \n',finfocomp);
