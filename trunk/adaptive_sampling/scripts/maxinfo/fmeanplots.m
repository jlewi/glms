%12-02
%script to make plots of fcurves - which is quantity we want to maximize
%make a whole bunch of plots on the same axis of F(theta), F(lambda)
%as we rotate the mean 
close all;
clear all;
setpaths;
m=[1;0];
pg.c=[.9 0;0 .1];

%angles for mean
dangle=90/5*pi/180;
%avoid an angle of 0 and pi/2 so that projection 
%of mean onto eigen vectors is never 0.
mangle=[dangle:dangle:pi/2-dangle];

%store fx as function of theta
ftheta.xpts=cell(1,length(mangle));;
ftheta.finfo=cell(1,length(mangle));

for tindex=1:length(mangle);
pg.m=15*[cos(mangle(tindex));sin(mangle(tindex))];

%**************************************************************************
[ftheta.finfo{tindex},ftheta.jlbound{tindex},ftheta.theta{tindex},ftheta.xpts{tindex}]=fisherucirc(pg);
ftheta.lbl{tindex}=sprintf('<\\mu=%0.2g',mangle(tindex)*180/pi);

%********************************************************
%make a plot of fisher information as function of lambda
%**********************************************************
[finfo,lambda,xpts]=fisherlambda(pg);
flambda.xpts{tindex}=xpts;
flambda.lbl{tindex}=sprintf('<\\mu=%0.2g',mangle(tindex)*180/pi);
flambda.finfo{tindex}=finfo;
flambda.lambda{tindex}=lambda;

end

%*******************************************************************
%plot all curves of theta 
%*******************************************************************
ftheta.hf=figure;
ftheta.hp=[];

for tindex=1:length(mangle)
    subplot(3,1,1);
    hold on
    ftheta.hp(tindex)=plot(ftheta.theta{tindex},ftheta.finfo{tindex},getptype(tindex,1));   
    %plot(ftheta.theta{tindex},ftheta.finfo{tindex},getptype(tindex,2));   
end
xlabel('theta');
ylabel('Fisher Info');
legend(ftheta.hp,ftheta.lbl);

%plot x and y
subplot(3,1,2);
plot(ftheta.theta{1},ftheta.xpts{1}(1,:));
ylabel('x');

subplot(3,1,3);
plot(ftheta.theta{1},ftheta.xpts{1}(2,:));
ylabel('y');

%**********************************************************************

%*******************************************************************
%plot all curves of lambda 
%*******************************************************************
flambda.hf=figure;
flambda.hp=[];

for tindex=1:length(mangle)
    subplot(3,1,1);
    hold on
    flambda.hp(tindex)=plot(flambda.lambda{tindex},flambda.finfo{tindex},getptype(tindex,1));   
    %plot(flambda.lambda{tindex},flambda.finfo{tindex},getptype(tindex,2)); 
    xlabel('lambda');
    ylabel('Fisher Info');
    
    %plot x and y
    subplot(3,1,2);
    hold on;
    plot(flambda.lambda{tindex},flambda.xpts{tindex}(1,:),getptype(tindex,1));
    %plot the location of the maximum
    [val,ind]=max(flambda.finfo{tindex});
    plot(flambda.lambda{tindex}(ind), flambda.xpts{tindex}(1,ind),getptype(tindex,1));
    ylabel('x');
    ylim([-1.25 1.25]);

    subplot(3,1,3);
    hold on;
    plot(flambda.lambda{tindex},flambda.xpts{tindex}(2,:),getptype(tindex,1));
    plot(flambda.lambda{tindex}(ind), flambda.xpts{tindex}(2,ind),getptype(tindex,1));
    ylabel('y');
end

legend(flambda.hp,flambda.lbl);



%**********************************************************************