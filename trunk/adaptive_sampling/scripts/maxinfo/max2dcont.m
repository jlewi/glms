%%11-18
%
%Open results from a previous simulation and continue running it for some
%number of iterations
rfile=fullfile(RDIR,'02_17','02_17_max2d_002.mat');

niter=100;  %how many additional iterations to run


%**************************************************************************
%Update the posteriors
%we do this twice once using maxupdate and 1 using regular update so we can
%compare
%**************************************************************************
%we need to get an index representing what rerun this is
nruns=length(simparammax.rstart)+1;

%
%need to modify maxupdate so that it takes in all the results from the
%previous iterations.












%set ropts.fname='' so we never call save from maxupdate or
%updateposteriors
fropts=ropts;
fropts.fname='';
[pmax,srmax,simparammax]=maxupdate(mparam,simparam,mon,fropts);

%get the current state of the random number generator
%make this cell arrays in case we start and stop many times
simparammax.rstart{1}=simparammax.rstate;
simparammax.rfinish{1}=randn('state');
simparammax=rmfield(simparammax,'rstate');

%save data before performing random sampling
if (~(isempty(ropts.fname)))
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end

[prand,srrand,simparamrand]=updateposteriors(mparam,simparam,mon,fropts);
%save the state of the random number generator
simparamrand.rstart{1}=simparamrand.rstate;
simparamrand.rfinish{1}=randn('state');
simparamrand=rmfield(simparamrand,'rstate');

%save data
if (~(isempty(ropts.fname)))
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end
