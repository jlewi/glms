%11-20
%Script to handle the plotting of results from posterior2d
%in the case date is two dimensions



%*****************
%figures
axisfontsize=16;
lgndfontsize=16;

fkl.hf=[];      %only create the figure if we have something to plot
fkl.hp=[];
fkl.x=[0:simparam.niter];
fkl.xlabel='Iteration';
fkl.ylabel='Distance';
fkl.title='Kl Divergence';
fkl.on=0;
fkl.name='KL Divergence';
fkl.axisfontsize=axisfontsize;
fkl.lgndfontsize=lgndfontsize;

fmdist.hf=[]; %figure gets created when we plot something
fmdist.hp=[];
fmdist.x=[0:simparam.niter];
fmdist.xlabel='Iteration';
fmdist.ylabel='Distance';
fmdist.title='Distance From Mean to True Value';
fmdist.on=1;
fmdist.fname='mdist';
fmdist.name='Mean Distance';
fmdist.semilog=1;
fmdist.axisfontsize=axisfontsize;
fmdist.lgndfontsize=lgndfontsize;

%entropy
fentropy.hf=[]; %figure gets created when we plot something
fentropy.hp=[];
fentropy.x=[0:simparam.niter];
fentropy.xlabel='Iteration';
fentropy.ylabel='Entropy';
fentropy.title='';
fentropy.name='Entropy';    %window name
fentropy.on=1;
fentropy.fname='entropy';
fentropy.axisfontsize=axisfontsize;
fentropy.lgndfontsize=lgndfontsize;
fentropy.lbls={};

%mean squared error
fmse.hf=[]; %figure gets created when we plot something
fmse.hp=[];
fmse.x=[0:simparam.niter];
fmse.xlabel='Iteration';
fmse.ylabel='MSE';
fmse.title='';
fmse.name='MSE';
fmse.on=0;
fmse.fname='mse';
fmse.semilog=1;
fmse.axisfontsize=axisfontsize;
fmse.lgndfontsize=lgndfontsize;
fmse.lbls={};

%timing
ftiming.hf=[]; %figure gets created when we plot something
ftiming.hp=[];
ftiming.x=[1:simparam.niter];
ftiming.xlabel='Iteration';
ftiming.ylabel='Time(Seconds)';
ftiming.title='';
ftiming.name='Timing';
ftiming.on=0;
ftiming.fname='timing';
ftiming.semilog=0;
ftiming.axisfontsize=axisfontsize;
ftiming.lgndfontsize=lgndfontsize;
ftiming.lbls={};

pind =0;

%methods structure
%specifies which method structures to check for
methods=[];
methods.pmax.vname='pmax';
methods.pmax.label='InfoMax';
if exist('simparammax','var')
    methods.pmax.simparam=simparammax;
end
methods.prand.vname='prand';
methods.prand.label='Random';
if exist('simparamrand','var')
    methods.prand.simparam=simparamrand;
end

methods.pignore.vname='pignore';
methods.pignore.label='Ignore spike history';
if exist('simparamignore','var')
    methods.pignore.simparam=simparamignore;
end


%loop through the max and random sampling methods
fmethods=fieldnames(methods);
for methodind=1:length(fieldnames(methods));
    %for each field name
    %check if the variable is defined in the workspace if it is
    %set it to pmethods

    if (exist(methods.(fmethods{methodind}).vname,'var'))
        evalin('base',sprintf('pmethods=%s',methods.(fmethods{methodind}).vname));
        lbltxt=methods.(fmethods{methodind}).label;

        %if (methodind==1)
        %   pmethods=pmax;
        %  lbltxt='Max';
        %else
        %    pmethods=prand;
        %    lbltxt='Rnd';
        %end

        %construct a list of enabled methods
        monf={};
        mnames=fieldnames(pmethods);
        for index=1:length(fieldnames(pmethods))
            if pmethods.(mnames{index}).on >0
                monf{length(monf)+1}=mnames{index};
            end
        end

        %compute thetatrue a matrix to represent true parameter value
        %at each iteration
        %need to take into account whether we have spike history terms
        %and if we allow parameters to change over time.
        if isfield(mparam,'atrue')
            thetatrue=[mparam.ktrue; mparam.atrue];
        else
            thetatrue=[mparam.ktrue];
        end

        %if the diffusion parameter is on we need to repeat the first
        %column of theta for the prior
        %this is because mparam.ktrue will have simparam.niter columns
        if (size(mparam.ktrue,2)>1)
            thetatrue=[mparam.ktrue(:,1) mparam.ktrue];
        else
            thetatrue=thetatrue* ones(1,simparam.niter+1);
        end


        %loop through enabled methods
        for mindex=1:length(monf)
            pind=pind+1;
            %plot the kl distance


            if isfield(pmethods.(monf{mindex}),'dktest')
                if (isempty(fkl.hf))
                    fkl.hf=figure;
                else
                    figure(fkl.hf);
                end
                hold on;
                fkl.on=1;
                fkl.hp(pind)=plot(fkl.x,pmethods.(monf{mindex}).dk(1,:),getptype(pind,1));
                plot(fkl.x,pmethods.(monf{mindex}).dk(1,:),getptype(pind,2));
                fkl.lbls{pind}=sprintf('monfte Carlo: %s', pmethods.(monf{mindex}).label);
            end

            %***************************************************************
            %figure of the  mean squared distance of mean and true mean
            %*********************************************************
            if isempty(fmdist.hf)
                fmdist.hf=figure;
            end
            figure(fmdist.hf);
            hold on;
            %to handle backwards compatibility
            if ~isfield(mparam,'klength')
                mparam.klength=mparam.tlength;
            end

            %compute the mse of just the stimulus coefficents
            lse=pmethods.(monf{mindex}).m(1:mparam.klength,:)-thetatrue(1:mparam.klength,:);
            lse=lse.^2;
            lse=sum(lse,1);

            %take into account the spike history terms if they exist
            if isfield(mparam,'atrue')
                lsea=pmethods.(monf{mindex}).m(mparam.klength+1:end,:)-thetatrue(mparam.klength+1:end,:);
                lsea=lsea.^2;
                lsea=sum(lsea,1);
            end
            lse=(lse+lsea).^.5;
            fmdist.lbls{pind}=sprintf('%s %s',lbltxt,pmethods.(monf{mindex}).label);

            fmdist.hp(pind)=plot(fmdist.x,lse,getptype(pind,1));
            plot(fmdist.x,lse,getptype(pind,2));

            %***************************************************************
            %figure of the posterior entropy
            %*********************************************************
            %compute the entropy if not already computed
            if ~isfield(pmethods.(monf{mindex}),'entropy')
                entropy=zeros(1,simparam.niter+1);
                %need to know dimensionlaity of filter to compute entropy
                filtlength=size(pmethods.(monf{mindex}).m,1);
                for index=0:simparam.niter
                    eigd=eig(pmethods.(monf{mindex}).c{index+1});
                    %entropy(index+1)=1/2*log((2*pi*exp(1))^(mparam.tlength)*abs(det(pmethods.(monf{mindex}).c{index+1})));
                    %this computation avoids
                    %should be base 2 for the log
                    entropy(index+1)=1/2*filtlength*(log2(2*pi*exp(1)))+1/2*sum(log2(abs(eigd)));
                end
                pmethods.(monf{mindex}).entropy=entropy;
            end

            if ~isempty(pmethods.(monf{mindex}).entropy)
                if isempty(fentropy.hf)
                    fentropy.hf=figure;
                end
                figure(fentropy.hf);
                hold on;
                plot(fentropy.x,pmethods.(monf{mindex}).entropy,getptype(pind,2));
                fentropy.hp(end+1)=plot(fentropy.x,pmethods.(monf{mindex}).entropy,getptype(pind,1));
                fentropy.lbls{end+1}=sprintf('%s %s',lbltxt,pmethods.(monf{mindex}).label);
                lblgraph(fentropy);
            end
            %***************************************************************
            %Mean Squared Error
            %*********************************************************
            %without covariance matrix we can compute the mean squared error
            %sometimes we don't save all the covariance matrices
            if ~isempty(pmethods.(monf{mindex}).c{2})
                if isempty(fmse.hf)
                    fmse.hf=figure;
                end
                figure(fmse.hf);
                hold on;

                err=exmse(pmethods.(monf{mindex}),thetatrue);
                fmse.hp(end+1)=plot(fmse.x,err,getptype(pind,1));
                plot(fmse.x,err,getptype(pind,2));

                fmse.lbls{end+1}=sprintf('%s %s',lbltxt,pmethods.(monf{mindex}).label);
            end
            %***************************************************************
            %timing figure
            %*********************************************************
            if(ftiming.on~=0)
                figure(ftiming.hf);
                hold on
                if isfield(pmethods.(monf{mindex}),'timing')
                    tpind=length(ftiming.hp)+1;
                    ftiming.lbls{tpind}=sprintf('%s: %s Update Posterior', methods.(fmethods{methodind}).label,monf{mindex});
                    ftiming.hp(tpind)=plot(1:simparam.niter,pmethods.(monf{mindex}).timing.update,getptype(tpind,1));
                    plot(1:simparam.niter,pmethods.(monf{mindex}).timing.update,getptype(tpind,2));

                    %don't plot the optimization time if we
                    %used random stimuli or if simparam doesn't exist
                    if (methods.(fmethods{methodind}).simparam.randstimuli==0)
                        tpind=length(ftiming.hp)+1;
                        ftiming.lbls{tpind}=sprintf('%s: %s Optimize Stimulus', methods.(fmethods{methodind}).label,monf{mindex});
                        ftiming.hp(tpind)=plot(1:simparam.niter,pmethods.(monf{mindex}).timing.optimize,getptype(tpind,1));
                        plot(1:simparam.niter,pmethods.(monf{mindex}).timing.optimize,getptype(tpind,2));
                    end

                    %plot the time per newton iteration
                    if isfield(pmethods.(monf{mindex}),'niter')
                        tpind=length(ftiming.hp)+1;
                        ftiming.lbls{tpind}=sprintf('%s: %s Update time/step', methods.(fmethods{methodind}).label,monf{mindex});
                        ftiming.hp(tpind)=plot(1:simparam.niter,pmethods.(monf{mindex}).timing.update./pmethods.(monf{mindex}).niter,getptype(tpind,1));
                        plot(1:simparam.niter,pmethods.(monf{mindex}).timing.update./pmethods.(monf{mindex}).niter,getptype(tpind,2));
                    end
                end
            end
        end
    end

end
%if (fkl.on~=0)
%figure(fkl.hf);
%legend(fkl.hp,fkl.lbls);
%xlabel(fkl.xlabel);
%ylabel(fkl.ylabel);
%title(fkl.title);
%end

%label all the figures
figs={fmdist,fentropy,fkl,fmse,ftiming};
%which ones to do on semilog axis



for index=1:length(figs)
    if (figs{index}.on~=0)

        figs{index}.axisfontsize=axisfontsize;
        figs{index}.lgndfontsize=lgndfontsize;
        lblgraph(figs{index});
        if (popts.sgraphs ~=0)

            %get the trial number of the file
            %if date and time not provided get it from the filename
            [PATHSTR,name,EXT,VERSN] =fileparts(ropts.fname);
            trialnum=name(end-2:end);   %this is a string not a number


            %create filename for graph
            if ~(exist('datetime','var'))
                datetime=zeros(1,4);
                datetime(2)=str2num(name(1:2));
                datetime(3)=str2num(name(4:5));
            end
            %create filename for graph
            fname=fullfile(popts.sdir,sprintf('%0.2d_%0.2d_%s.eps',datetime(2),datetime(3),figs{index}.fname));
            fname=seqfname(fname);
            savegraph(fname,figs{index}.hf);

        end
    else

    end


end



