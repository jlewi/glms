/*
 * MATLAB Compiler: 4.8 (R2008a)
 * Date: Fri Aug  1 15:15:38 2008
 * Arguments: "-B" "macro_default" "-m" "-W" "main" "-T" "link:exe"
 * "testcompile.m" 
 */

#include "mclmcrrt.h"

#ifdef __cplusplus
extern "C" {
#endif
const unsigned char __MCC_testcompile_session_key[] = {
    '1', 'E', 'B', 'C', '1', '3', '9', '4', '9', 'B', 'C', '3', 'A', '8', 'A',
    '5', '0', '9', 'E', 'C', '5', 'A', '5', '3', '5', '7', '1', 'D', '4', '7',
    '6', '6', '1', 'D', 'F', '2', 'A', 'E', '0', '5', '8', 'C', '1', '6', '4',
    '4', '0', '9', 'B', 'F', '4', '3', 'A', '6', '7', '2', '8', '3', 'A', '0',
    'E', '3', 'E', 'D', '3', '7', '0', '6', 'E', '0', '3', 'A', '4', 'F', '7',
    'D', 'F', '9', 'E', '5', '2', 'B', '9', '4', '2', 'C', 'E', 'D', 'A', '7',
    'F', '1', 'C', '9', '0', '9', '0', '0', '5', '8', '5', 'D', 'A', 'C', '8',
    '8', 'E', '2', '9', '3', '1', '1', '2', '9', '6', '1', 'A', '3', '2', '6',
    'A', '6', 'F', 'C', 'B', 'D', 'A', 'A', '8', 'C', 'C', '8', '5', '5', '9',
    '8', '3', 'F', '6', '8', '4', '4', '0', 'A', '6', '9', 'D', '4', '7', 'E',
    '9', '4', '1', 'F', 'F', 'F', 'A', '8', '9', 'F', '4', '9', '4', '3', '6',
    '2', '5', '7', '1', '3', 'E', '8', '1', 'C', 'C', '8', 'A', '8', 'B', '6',
    'A', '3', '5', '5', 'E', '1', '2', 'D', 'A', '4', 'C', 'F', '2', '3', '2',
    '3', 'A', '1', '2', 'B', '6', 'A', '3', 'C', '1', 'F', 'B', '6', '1', '0',
    '9', 'B', 'B', '4', '3', 'A', '8', '2', 'A', '3', '9', '8', 'F', '7', 'C',
    'D', 'E', '9', '6', 'B', '9', '0', '8', 'A', '2', '6', '3', 'D', 'C', '7',
    'F', '7', '2', 'E', 'F', '1', 'A', '7', '3', '2', '4', 'F', '4', '7', '2',
    'B', '\0'};

const unsigned char __MCC_testcompile_public_key[] = {
    '3', '0', '8', '1', '9', 'D', '3', '0', '0', 'D', '0', '6', '0', '9', '2',
    'A', '8', '6', '4', '8', '8', '6', 'F', '7', '0', 'D', '0', '1', '0', '1',
    '0', '1', '0', '5', '0', '0', '0', '3', '8', '1', '8', 'B', '0', '0', '3',
    '0', '8', '1', '8', '7', '0', '2', '8', '1', '8', '1', '0', '0', 'C', '4',
    '9', 'C', 'A', 'C', '3', '4', 'E', 'D', '1', '3', 'A', '5', '2', '0', '6',
    '5', '8', 'F', '6', 'F', '8', 'E', '0', '1', '3', '8', 'C', '4', '3', '1',
    '5', 'B', '4', '3', '1', '5', '2', '7', '7', 'E', 'D', '3', 'F', '7', 'D',
    'A', 'E', '5', '3', '0', '9', '9', 'D', 'B', '0', '8', 'E', 'E', '5', '8',
    '9', 'F', '8', '0', '4', 'D', '4', 'B', '9', '8', '1', '3', '2', '6', 'A',
    '5', '2', 'C', 'C', 'E', '4', '3', '8', '2', 'E', '9', 'F', '2', 'B', '4',
    'D', '0', '8', '5', 'E', 'B', '9', '5', '0', 'C', '7', 'A', 'B', '1', '2',
    'E', 'D', 'E', '2', 'D', '4', '1', '2', '9', '7', '8', '2', '0', 'E', '6',
    '3', '7', '7', 'A', '5', 'F', 'E', 'B', '5', '6', '8', '9', 'D', '4', 'E',
    '6', '0', '3', '2', 'F', '6', '0', 'C', '4', '3', '0', '7', '4', 'A', '0',
    '4', 'C', '2', '6', 'A', 'B', '7', '2', 'F', '5', '4', 'B', '5', '1', 'B',
    'B', '4', '6', '0', '5', '7', '8', '7', '8', '5', 'B', '1', '9', '9', '0',
    '1', '4', '3', '1', '4', 'A', '6', '5', 'F', '0', '9', '0', 'B', '6', '1',
    'F', 'C', '2', '0', '1', '6', '9', '4', '5', '3', 'B', '5', '8', 'F', 'C',
    '8', 'B', 'A', '4', '3', 'E', '6', '7', '7', '6', 'E', 'B', '7', 'E', 'C',
    'D', '3', '1', '7', '8', 'B', '5', '6', 'A', 'B', '0', 'F', 'A', '0', '6',
    'D', 'D', '6', '4', '9', '6', '7', 'C', 'B', '1', '4', '9', 'E', '5', '0',
    '2', '0', '1', '1', '1', '\0'};

static const char * MCC_testcompile_matlabpath_data[] = 
  { "testcompile/", "toolbox/compiler/deploy/", "$TOOLBOXMATLABDIR/general/",
    "$TOOLBOXMATLABDIR/ops/", "$TOOLBOXMATLABDIR/lang/",
    "$TOOLBOXMATLABDIR/elmat/", "$TOOLBOXMATLABDIR/elfun/",
    "$TOOLBOXMATLABDIR/specfun/", "$TOOLBOXMATLABDIR/matfun/",
    "$TOOLBOXMATLABDIR/datafun/", "$TOOLBOXMATLABDIR/polyfun/",
    "$TOOLBOXMATLABDIR/funfun/", "$TOOLBOXMATLABDIR/sparfun/",
    "$TOOLBOXMATLABDIR/scribe/", "$TOOLBOXMATLABDIR/graph2d/",
    "$TOOLBOXMATLABDIR/graph3d/", "$TOOLBOXMATLABDIR/specgraph/",
    "$TOOLBOXMATLABDIR/graphics/", "$TOOLBOXMATLABDIR/uitools/",
    "$TOOLBOXMATLABDIR/strfun/", "$TOOLBOXMATLABDIR/imagesci/",
    "$TOOLBOXMATLABDIR/iofun/", "$TOOLBOXMATLABDIR/audiovideo/",
    "$TOOLBOXMATLABDIR/timefun/", "$TOOLBOXMATLABDIR/datatypes/",
    "$TOOLBOXMATLABDIR/verctrl/", "$TOOLBOXMATLABDIR/codetools/",
    "$TOOLBOXMATLABDIR/helptools/", "$TOOLBOXMATLABDIR/demos/",
    "$TOOLBOXMATLABDIR/timeseries/", "$TOOLBOXMATLABDIR/hds/",
    "$TOOLBOXMATLABDIR/guide/", "$TOOLBOXMATLABDIR/plottools/",
    "toolbox/local/", "toolbox/shared/dastudio/",
    "$TOOLBOXMATLABDIR/datamanager/", "toolbox/compiler/" };

static const char * MCC_testcompile_classpath_data[] = 
  { "" };

static const char * MCC_testcompile_libpath_data[] = 
  { "" };

static const char * MCC_testcompile_app_opts_data[] = 
  { "" };

static const char * MCC_testcompile_run_opts_data[] = 
  { "" };

static const char * MCC_testcompile_warning_state_data[] = 
  { "off:MATLAB:dispatcher:nameConflict" };


mclComponentData __MCC_testcompile_component_data = { 

  /* Public key data */
  __MCC_testcompile_public_key,

  /* Component name */
  "testcompile",

  /* Component Root */
  "",

  /* Application key data */
  __MCC_testcompile_session_key,

  /* Component's MATLAB Path */
  MCC_testcompile_matlabpath_data,

  /* Number of directories in the MATLAB Path */
  37,

  /* Component's Java class path */
  MCC_testcompile_classpath_data,
  /* Number of directories in the Java class path */
  0,

  /* Component's load library path (for extra shared libraries) */
  MCC_testcompile_libpath_data,
  /* Number of directories in the load library path */
  0,

  /* MCR instance-specific runtime options */
  MCC_testcompile_app_opts_data,
  /* Number of MCR instance-specific runtime options */
  0,

  /* MCR global runtime options */
  MCC_testcompile_run_opts_data,
  /* Number of MCR global runtime options */
  0,
  
  /* Component preferences directory */
  "testcompile_F63194EE5EAA62CE7F47813E36D2F401",

  /* MCR warning status data */
  MCC_testcompile_warning_state_data,
  /* Number of MCR warning status modifiers */
  1,

  /* Path to component - evaluated at runtime */
  NULL

};

#ifdef __cplusplus
}
#endif


