%01-19-2007
%
%Make a plot of the Gabor receptive field
%co08gabors1dAC;
xscript('/home/jlewi/svn_trunk/allresults/tanspace/080218/co08gabors1dAC_080218_001.m');

dind=1;

simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);

extra=getextra(simobj);
mparam=extra.mparam;
ktrue=mparam.ktrue;


fgabor=FigObj('name','Gabor','width',4,'height',1.25);
fgabor.a=AxesObj('nrows',1,'ncols',1,'xlabel','i','ylabel','\theta_i');

x=[1:length(ktrue)];
x=x-(1+length(ktrue))/2;
hp=plot(x,ktrue);

ls.linewidth=4;

fgabor.a=addplot(fgabor.a,'hp',hp,'pstyle',ls);
lblgraph(fgabor);
ylim([-1 3.1]);
set(gca,'ytick',[0 3])

fname='~/svn_trunk/adaptive_sampling/writeup/cosyne08/gabor1dAC.eps';
%saveas(gethf(fgabor),fname,'epsc');