%11-25-2007
%
%Make a plot of the estimated parameters on each trial

dsets=[];

data=[];
%cosyne08gabors1d;
xscript('/home/jlewi/svn_trunk/allresults/tanspace/080218/co08gabors1dAC_080218_001.m');
%remove dsets 4 from the data
dsets=dsets([1 3 2]);
maxtrial=1000;


for dind=1:length(dsets)
    %[pdata, simdata mobj sr]=loadsim('simfile',dsets(dind).fname,'simvar',dsets(dind).simvar);
    simobj=SimulationBase('fname',dsets(dind).fname,'simvar',dsets(dind).simvar);


        data(dind).ktrue=gettheta(getobserver(simobj));
    %compute the mse
    postobj=getpost(simobj);
    if isa(postobj,'PostTanSpace')
        fullpost=getfullpost(postobj);
        data(dind).fullm=getm(fullpost);


        %compute mse of estimate on the subspace
        tanspace=gettanspace(postobj);
        tanpost=gettanpost(postobj);
        data(dind).tanm=getm(tanpost(2:end));
      


    else
        data(dind).fullm=getm(postobj);

        %we need to project the full stimulus onto the submanifold
        data(dind).tanm=zeros(size(data(dind).fullm));
        for j=1:size(data(dind).fullm,2)
            data(dind).tanm(:,j)=submanifold(tanspace(1),proj(tanspace(1),data(dind).fullm(:,j)));
        end
    end
    %last trial to plot
    data(dind).tlast=min(maxtrial,getniter(simobj));
end

%**************************************************************************
%plot the Recovered values
%*************************************************************
%%


ffilt=FigObj('width',5,'height',3.55,'name','Estimated Parameters','fontsize',10);
ffilt.a=AxesObj('nrows',2,'ncols',2*(length(dsets)));

%*********************************************
%plot the estimated filters
for dind=1:length(dsets)

    %plot the mean of the full posterior
    setfocus(ffilt.a,1,2*dind-1);

    imagesc(data(dind).fullm(:,1:data(dind).tlast)');
    set(gca,'ydir','reverse');
    ffilt.a(1,2*dind-1)=title(ffilt.a(1,2*dind-1),sprintf('%s',dsets(dind).tlbl));
    %turn off xtick and ytick
    set(gca,'xtick',[]);
    
    xlim([1 length(data(dind).ktrue)]);
    ylim([1 data(dind).tlast-1]);
    
    %set(gca,'ytick',[]);

    %plot the truth
    setfocus(ffilt.a,2,2*dind-1);
    imagesc(data(dind).ktrue');
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);

    xlim([1 length(data(dind).ktrue)]);
    
    %plot the mean of the projection on the submanifold posterior
    setfocus(ffilt.a,1,2*dind);

    if (size(data(dind).tanm,2)>1)
    imagesc(data(dind).tanm(:,1:data(dind).tlast)');
    set(gca,'ydir','reverse');
%    ffilt.a(1,2*dind)=title(ffilt.a(1,2*dind),sprintf('%s:\n tan space',dsets(dind).tlbl));
    %turn off xtick and ytick
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);

    xlim([1 length(data(dind).ktrue)]);
    ylim([1 data(dind).tlast-1]);
    %plot the truth
    setfocus(ffilt.a,2,2*dind);
    imagesc(data(dind).ktrue');
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
    xlim([1 length(data(dind).ktrue)]);

    end
end

%make the color limits the same
cl=get(ffilt.a,'clim');
cl=cell2mat(cl);
cl=[-2 5];
set(ffilt.a,'clim',cl);

%****************************************************************
%label graphs
%*******************************************************************
%add a colorbar
setfocus(ffilt.a,1,2*(length(dsets)));
hc=colorbar;
ffilt.a(1,2*length(dsets))=sethc(ffilt.a(1,2*length(dsets)),hc);
%left column
ffilt.a(1,1)=ylabel(ffilt.a(1,1),'Trial');
setfocus(ffilt.a,1,1);
set(gca,'ytickmode','auto');


ffilt.a(2,1)=ylabel(ffilt.a(2,1),'true');
% for j=1:length(dsets)*2
%     setfocus(ffilt.a,2,j);
%     set(gca,'xtickmode','auto');
% end

ffilt=lblgraph(ffilt);

space.cbwidth=.1;
space.vspace=.01;
ffilt=sizesubplots(ffilt,space,ones(1,2*length(dsets)),[.9 .1]);

odata={ffilt,{'Data',{dsets.fname}'; 'Script', 'cmptanresultsimg.m';'InfoMax Full', 'Infomax on full theta space as opposed to infomax on tangent space'}};
onenotetable(odata,seqfname('~/svn_trunk/notes/cmptanimg.xml'));
    
fname=fullfile('~/svn_trunk/adaptive_sampling/writeup/cosyne08',sprintf('gaborac_map.eps'));
    %saveas(gethf(ffilt),fname,'epsc');
