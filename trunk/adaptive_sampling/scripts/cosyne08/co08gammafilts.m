%7-16-2007
%   Plot it on a log scale. We do this manually because we compute the
%   filters on a log increment
%5-30-2007
%
%Make an image of the best recovered auditory filter on each trial
%To do this we reshape the mean as a matrix and get the first two principal
%eigen vectors. We then project the true filters onto these vectors
%This represents our MSE guess of the best filters
%******************************************
clear all;
setpathvars;
%co08datagammatones40d;
%dsetsind=[1 5 6];
%co08datagammatones30d;
%datalatest;
%dsets=d40dfeat;
%dsetsind=[1 2];
%dsetsind=[1:3];
%d=pwd;
xscript('/home/jlewi/svn_trunk/allresults/tanspace/080129/co08datagammatones_080129_001.,m');
dsets=dsets([1 2 5]);
gparam.maxtrial=2000;
dsetsind=1:length(dsets);

%********************************************************************
%compute the subspace angle

clear data;
for dind=1:length(dsetsind)
    
    simdata=SimulationBase('fname',dsets(dsetsind(dind)).fname,'simvar',dsets(dsetsind(dind)).simvar);
    extra=getextra(simdata);
    mparam=extra.mparam;
    kl=mparam.kl;
    k1=mparam.k1';
    k2=mparam.k2';
    ktrue=[k1 k2];
    if (size(k1,2)>1)
        k1=k1';
    end
    if (size(k2,2)>1)
        k2=k2';
    end

    data(dind).niter=getniter(simdata);

    %how many rows do we want to have in our image
    nrows=200;
    %we need to create a mapping from rows to trials
    %we want this mapping to follow a log scale.
    slope=log10(min(gparam.maxtrial,data(dind).niter))/(nrows-1);

    trials=10.^(slope*((1:nrows)-1));

    %take ceiling so that trials is valid
    trials=floor(trials);
    %compute the data on a log scale
    %how many points to plot in between each power
    %i.e logres 20 means we have 20 pts between successive powers
    %    logres=20;
    %    %mpower is the largest power of 10 for this data et
    %    lscale.mpower=floor(log10(data(dind).niter));
    %    %lscale .npts is how many at pts we have for the last power
    %    lscale.nlast=floor(mod(data(dind).niter,10^lscale.mpower)/(10^(lscale.mpower-1)/logres);
    %    lscale.npts=(lscale.mpower-1)*logres+lscale.nlast;
    %     data(dind).k1est=zeros(kl,lscale.npts);
    %     data(dind).k2est=zeros(kl,lscale.npts);
    % %   data(dind).k1est=zeros(kl,ceil(simdata.niter/gparam.subsample));
    %  %  data(dind).k2est=zeros(kl,ceil(simdata.niter/gparam.subsample));

    ninpobj=getninpobj(getmobj(simdata));
    knorm=normmag([k1 k2]);
    for ind=1:length(trials)
        %trial=1:gparam.subsample:simdata.niter;
        % trial=10^power+j*10^(power-1);
        %ind=10*power+j;
        %       ind=floor(trial/gparam.subsample)+1;
%         trial=trials(ind);
%         [evecs, eigval]=svd(qmatquad(ninpobj,getpostm(simdata,trial)));
%         %project the truth onto each eigenvector
        
        trial=trials(ind);
        tanspace=gettanspace(getpost(simdata,trial));
        %get the parameters of the submanifold
        [evecs ,eigd]=unpackvec(tanspace,getsparam(tanspace));
         kproj=evecs(:,1:2)'*[k1 k2];

       data(dind).k1est(:,ind)=evecs(:,1:2)*kproj(:,1);
        data(dind).k2est(:,ind)=evecs(:,1:2)*kproj(:,2);

%         data(dind).k1est(:,ind)=knorm*[kproj(:,1).*eigd];
%         data(dind).k2est(:,ind)=knorm*[kproj(:,2).*eigd];
    end
    data(dind).trials=trials;
    data(dind).slope=slope;
end
%**************************************************************************
%plot the Recovered values
%*************************************************************
%%
gparam.nfilt=2;
gparam.nrows=gparam.nfilt*2;

gparam.ncols=length(data);

%%

ffilt=FigObj('width',5,'height',3,'name','Auditory Filters');
ffilt.a=AxesObj('nrows',gparam.nrows,'ncols',gparam.ncols);
 

%*********************************************
%plot the estimated filters
for ind=1:length(dsetsind)
    dind=dsetsind(ind);
    for nf=1:gparam.nfilt
        %plot the filter
        setfocus(ffilt.a,2*nf-1,ind);
       
        %        aind=spind(2*nf-1,dind);
        tlast=min(gparam.maxtrial,data(ind).niter);
        %  ffilt.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);
        fname=sprintf('k%dest',nf);

        %indlast=ceil(tlast/gparam.subsample);
        imagesc(1:kl,1:nrows,data(ind).(fname)');
        %turn off xtick and ytick
        set(gca,'xtick',[]);
        set(gca,'ytick',[]);
        set(gca,'ydir','reverse');
        xlim([1 kl]);
        ylim([1 nrows]);
        %plot the truth
        %plot the filter
        %        aind=spind(2*nf,dind);
        %   ffilt.a{aind}.ha=subplot(gparam.nrows,gparam.ncols,aind);

        setfocus(ffilt.a,2*nf,ind);
        fname=sprintf('k%dest',nf);
        imagesc(ktrue(:,nf)');
        xlim([1 kl]);
        set(gca,'xtick',[]);
        set(gca,'ytick',[]);

    end
end

%make the color limits the same
cl=get(ffilt.a,'clim');
cl=cell2mat(cl);
cl=[min(cl(:,1)) max(cl(:,2))];
set(ffilt.a,'clim',cl);

%****************************************************************
%label graphs
%*******************************************************************
%left column
for nf=1:gparam.nfilt
   ffilt.a(2*nf-1,1)=ylabel(ffilt.a(2*nf-1,1),sprintf('   $\\vec{\\phi}^%d$ \ntrial',nf));   
    setfocus(ffilt.a,2*nf-1,1);
    %adjust the tick labels
    %set the tick marks
    ytlbls=10.^[1:floor(log10(gparam.maxtrial))];
    ytick=floor(log10(ytlbls)/data(1).slope+1);
    set(gca,'ytick',ytick);
    %ytick=get(ffilt.a{aind}.ha,'ytick');
    set(gca,'yticklabel',ytlbls);



    ffilt.a(2*nf,1)=ylabel(ffilt.a(2*nf,1),'true');

end

%bottom row
for d=1:length(data)
    dind=dsetsind(d);
    setfocus(ffilt.a,gparam.nrows,d);
%    ffilt.a(gparam.nrows,dind)=xlabel(ffilt.a(gparam.nrows,dind),sprintf('\\phi_i'));
    ffilt.a(gparam.nrows,d)=xlabel(ffilt.a(gparam.nrows,d),sprintf('i'));
   set(gca,'xtickmode','auto');
  
end

%top row
for i=1:length(data)
    dind=dsetsind(i);
    setfocus(ffilt.a,1,i);
    ffilt.a(1,i)=title(ffilt.a(1,i),dsets(dind).tlbl);
    
ht=gettitle(ffilt.a(1,i));
set(ht.h,'interpreter','latex');
end

%add colorbars to the last column
%right column
hc=[];
for nf=1:gparam.nfilt

    setfocus(ffilt.a,2*nf-1,gparam.ncols);
    hc=colorbar;
    ffilt.a(2*nf-1,gparam.ncols)=sethc(    ffilt.a(2*nf-1,gparam.ncols),hc);
end

% 
hy=getylabel(ffilt.a(1,1));
set(hy.h,'interpreter','latex');

hy=getylabel(ffilt.a(3,1));
set(hy.h,'interpreter','latex');


ffilt=lblgraph(ffilt);


ffilt=sizesubplots(ffilt,[],[],[.4 .1 .4 .1]',[.06 0 0 .08]);
ffiltoutfile='~/svn_trunk/adaptive_sampling/writeup/cosyne08/gammatones.eps';
%saveas(gethf(ffilt),ffiltoutfile,'epsc');
odata={ffilt,[{'label'},{'variable'},{'file'};{dsets(dsetsind).lbl}' {dsets(dsetsind).simvar}' {dsets(dsetsind).fname}'];};
onenotetable(odata,seqfname('~/svn_trunk/notes/co08gammatones.xml'));
return;
