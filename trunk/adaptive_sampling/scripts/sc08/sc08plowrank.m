%4-19-2007
%
% Create an image with three rows and several columns
% each row shows plots of the MAP estimate of a 2-d receptive field for a
%       different method
% We will have 3 rows corresponding to
%   1. infomax - under power constraint
%   2. infomax - using pool based stimuli
%   3. iid
%
% Last column displays the true receptive field



%new data set
dsets=[];
%co08separable;
%data for some post cosyne experiments
xscript(fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080311','dataset_lowrank_080311_001.m'));
%***************************************************
%parameters for the graph
%***************************************************
gparam.trials= [100 500 1000 1500 2000 2200];    
dsets=dsets([1 3 4]);
hold on;

%how columns and rows of axes we will need
gparam.ncols=length(gparam.trials)+1;
gparam.nrows=length(dsets);
gparam.clim=[-1 1];       %limits for color mapping


%create a data structure to store the data from all the simulations
data=struct('m',[],'lbl',[],'theta',[]);
data=repmat(data,1,length(dsets));

%**************************************************************************
%Make plots of mean
%************************************************************************
%loop over the data sets
clear simobj;

%run this loop in parallel using parfor
%matlabpool('open','conf',2);

for dind = 1:length(dsets)
    [simobj]=SimulationBase('fname',getpath(dsets(dind).fname),'simvar',dsets(dind).simvar);
    extra=getextra(simobj);
    height=length(extra.f1);
    width=length(extra.t1);
    
    %allocate space to save the results
    data(dind).m=cell(1,length(gparam.trials));
    for tind=1:length(gparam.trials)
        trial=gparam.trials(tind);
      
        %check mean exists
        if (trial>getniter(simobj))
            fprintf('Data set %s: trial %d exceeds number of iterations \n;',getlabel(simobj),trial);
            break;   %stop plotting for this dataset
        else
            data(dind).m{tind}=reshape(getpostm(simobj,trial),height,width);
        end
        %turn off tick marks
    end %end loop over trials

    %the true parameter
    data(dind).theta=reshape(gettheta(getobserver(simobj)),height,width);
end


%************************************************************************
%Plot the results
%************************************************************************
%%
height=height;
width=width;
%dimensions are for slideshow
flowrank=FigObj('name','Gabor means','width',8,'height',5,'fontsize',16);
flowrank.a=AxesObj('nrows',length(dsets),'ncols',length(gparam.trials)+1);

%dorder is the order in which we want to plot the graphs
%dorder=[dnames.infomax dnames.infomaxfull dnames.rand];
dorder=[1:length(dsets)]
row=0;


for dind=dorder
    row=row+1;
   for tind=1:length(gparam.trials)
      if ~isempty(data(dind).m{tind})
         setfocus(flowrank.a,row,tind);
         imagesc(data(dind).m{tind},gparam.clim);
         set(gca,'xlim',[1 width],'ylim',[1 height]);
        set(gca,'xtick',[]);
        set(gca,'ytick',[]);
        
    set(gca,'ydir','reverse');
      end
   end    
   
   %plot the true parameter   
   setfocus(flowrank.a,row,length(gparam.trials)+1);
   imagesc(data(dind).theta,gparam.clim);
   set(gca,'xlim',[1 width],'ylim',[1 height]);
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
    set(gca,'ydir','reverse');
    %add a ylabel to each row

   flowrank.a(row,1)=ylabel(flowrank.a(row,1),dsets(dind).lbl);
   hylbl=getylabel(flowrank.a(row,1));
   %set(hylbl.h,'interpreter','latex');
end

%add colorbar to the last plot
%plot the true parameter
setfocus(flowrank.a,length(dsets),length(gparam.trials)+1);
hc=colorbar;
flowrank.a(length(dsets),length(gparam.trials)+1)=sethc(flowrank.a(length(dsets),length(gparam.trials)+1),hc);
  

%add a title to each graph in the top row
for col=1:length(gparam.trials)
   flowrank.a(1,col)=title(flowrank.a(1,col),sprintf('t=%d',gparam.trials(col)));
end
flowrank.a(1,length(gparam.trials)+1)=title(flowrank.a(1,length(gparam.trials)+1),'\theta true');



%label the graphs
flowrank=lblgraph(flowrank);
flowrank=sizesubplots(flowrank,[],[],[],[.05 0 0 0]);



%fgoutfile='~/svn_trunk/adaptive_sampling/writeup/phdproposal/figs/lowrankmaps.png';
%saveas(gethf(flowrank),fgoutfile,'png');

%feps='~/svn_trunk/adaptive_sampling/writeup/sc08/lowrankmaps.eps'
%saveas(gethf(flowrank),feps,'epsc2');
%print -dpng -r300 '~/svn_trunk/adaptive_sampling/writeup/cosyne08/lowrankmaps_hires.png'
odata={'data sets',[{'label'},{'variable'},{'file'};{dsets.lbl}' {dsets.simvar}' {dsets.fname}'];flowrank,'stimulus coefficients';};
onenotetable(odata,seqfname('~/svn_trunk/notes/lowrank.xml'));