function [dsets,varargout]= gp_data_infomax_090204_003() 
%********************************


dsets=[];
dind=1;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_029.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_029.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_029.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_029.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_029.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_029.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_029.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=2;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_030.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_030.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_030.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_030.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_030.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_030.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_030.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=3;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_031.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_031.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_031.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_031.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_031.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_031.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_031.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=4;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_032.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_032.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_032.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_032.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_032.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_032.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_032.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=5;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_033.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_033.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_033.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_033.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_033.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_033.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_033.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=6;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_034.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_034.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_034.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_034.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_034.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_034.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_034.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=7;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_035.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_035.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_035.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_035.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_035.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_035.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_035.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=8;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_036.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_036.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_036.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_036.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_036.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_036.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_036.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=9;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_037.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_037.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_037.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_037.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_037.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_037.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_037.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=10;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_038.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_038.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_038.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_038.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_038.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_038.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_038.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=11;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_039.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_039.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_039.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_039.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_039.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_039.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_039.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=12;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_040.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_040.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_040.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_040.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_040.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_040.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_040.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=13;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_041.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_041.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_041.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_041.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_041.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_041.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_041.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


dind=14;
dsets(dind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_setup_042.m','isdir',0);
dsets(dind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_data_042.mat','isdir',0);
dsets(dind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_mfile_042.dat','isdir',0);
dsets(dind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_cfile_042.dat','isdir',0);
dsets(dind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_status_042.txt','isdir',0);
dsets(dind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_inputs_042.dat','isdir',0);
dsets(dind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090204/gpsims_obsrv_042.dat','isdir',0);
dsets(dind).setupfile=[mfilename('fullpath') '.m'];


if (nargout>=2)
   for dind=1:length(dsets)
      fprintf('Loading %d of %d \n', dind,length(dsets));
v=load(getpath(dsets(dind).datafile));
      simobj(dind)=v.simobj;
   end
   varargout{1}=simobj;
end
