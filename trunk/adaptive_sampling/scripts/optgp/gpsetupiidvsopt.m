%12-31-2008
%
%This script sets up two simulations
%   1. One samples a white Gaussian process to pick the stimuli
%   2. The other picks the stimulus by sampling an optimal GP 
%       for the true parameters
%   updated to new object model and to run sim using random gradients.
function dfiles=gpsetupiidvsopt()


dfiles=[];

setpathvars;


savecint=10; %how often to save the covariance matrix;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMPoisson('canon');






%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.klength=1;
mparam.ktlength=2;
mparam.alength=0;
mparam.mmag=1;
mparam.glm=glm;
mobj=MParamObj(mparam);





%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
ktrue=[1;-1];
observer=GLMSimulator('model',mobj,'theta', [ktrue]);

%**************************************************************************
%Pool Stimuli
%***********************************************************************

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d('maxiter',1000,'compeig',false);
uoptim=getoptim(updater);
uoptim.TolFun=10^-5;
updater=setoptim(updater,uoptim);

%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using the optimal GP for the true parameters
%      2) Using an IID Gaussian process
%**************************************************************************



%*****************************************************************
%Sim: Sample the optimal GP
%******************************************************************
mu=0;
c={[1] [0]};
gpiid=StimGP('mu',mu,'c',c);

stimobj=SampStimGP('gp',gpiid);

lbl='White GP';
dfiles=SimSetup.setup('updater',updater,'mobj',mobj,'observer',observer,'stimchooser',stimobj,'savecint',savecint,'label',lbl);

