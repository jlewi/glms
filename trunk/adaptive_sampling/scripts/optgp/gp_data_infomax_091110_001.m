function [dsets]= gp_data_infomax_091110_001() 
%********************************


dsets=[];
rind=1;
cind=1;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_001.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_001.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_001.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_001.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_001.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_001.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_001.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_001.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=2;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_002.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_002.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_002.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_002.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_002.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_002.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_002.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_002.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=3;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_003.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_003.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_003.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_003.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_003.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_003.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_003.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_003.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=4;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_004.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_004.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_004.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_004.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_004.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_004.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_004.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_004.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=5;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_005.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_005.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_005.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_005.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_005.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_005.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_005.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_005.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=6;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_006.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_006.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_006.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_006.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_006.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_006.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_006.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_006.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=7;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_007.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_007.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_007.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_007.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_007.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_007.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_007.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_007.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=8;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_008.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_008.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_008.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_008.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_008.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_008.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_008.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_008.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=9;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_009.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_009.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_009.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_009.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_009.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_009.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_009.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_009.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=10;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_010.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_010.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_010.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_010.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_010.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_010.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_010.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_010.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=11;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_011.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_011.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_011.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_011.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_011.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_011.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_011.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_011.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=12;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_012.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_012.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_012.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_012.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_012.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_012.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_012.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_012.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=13;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_013.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_013.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_013.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_013.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_013.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_013.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_013.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_013.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=14;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_014.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_014.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_014.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_014.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_014.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_014.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_014.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_014.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=15;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_015.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_015.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_015.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_015.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_015.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_015.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_015.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_015.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=16;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_016.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_016.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_016.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_016.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_016.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_016.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_016.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_016.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=17;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_017.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_017.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_017.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_017.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_017.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_017.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_017.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_017.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=18;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_018.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_018.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_018.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_018.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_018.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_018.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_018.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_018.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=19;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_019.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_019.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_019.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_019.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_019.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_019.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_019.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_019.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=20;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_020.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_020.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_020.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_020.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_020.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_020.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_020.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_020.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=21;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_021.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_021.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_021.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_021.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_021.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_021.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_021.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_021.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=22;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_022.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_022.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_022.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_022.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_022.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_022.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_022.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_022.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=23;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_023.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_023.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_023.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_023.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_023.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_023.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_023.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_023.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=24;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_024.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_024.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_024.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_024.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_024.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_024.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_024.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_024.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=25;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_025.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_025.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_025.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_025.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_025.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_025.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_025.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_025.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=26;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_026.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_026.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_026.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_026.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_026.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_026.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_026.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_026.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=27;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_027.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_027.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_027.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_027.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_027.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_027.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_027.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_027.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=28;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_028.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_028.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_028.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_028.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_028.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_028.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_028.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_028.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=29;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_029.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_029.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_029.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_029.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_029.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_029.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_029.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_029.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=30;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_030.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_030.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_030.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_030.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_030.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_030.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_030.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_030.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=31;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_031.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_031.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_031.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_031.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_031.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_031.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_031.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_031.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=32;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_032.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_032.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_032.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_032.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_032.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_032.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_032.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_032.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=33;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_033.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_033.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_033.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_033.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_033.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_033.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_033.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_033.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=34;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_034.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_034.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_034.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_034.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_034.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_034.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_034.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_034.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=35;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_035.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_035.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_035.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_035.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_035.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_035.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_035.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_035.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=36;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_036.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_036.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_036.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_036.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_036.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_036.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_036.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_036.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=37;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_037.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_037.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_037.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_037.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_037.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_037.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_037.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_037.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=38;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_038.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_038.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_038.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_038.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_038.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_038.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_038.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_038.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=39;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_039.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_039.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_039.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_039.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_039.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_039.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_039.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_039.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=40;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_040.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_040.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_040.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_040.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_040.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_040.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_040.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_040.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=41;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_041.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_041.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_041.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_041.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_041.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_041.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_041.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_041.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=42;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_042.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_042.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_042.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_042.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_042.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_042.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_042.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_042.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=43;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_043.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_043.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_043.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_043.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_043.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_043.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_043.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_043.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=44;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_044.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_044.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_044.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_044.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_044.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_044.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_044.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_044.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=45;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_045.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_045.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_045.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_045.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_045.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_045.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_045.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_045.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=46;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_046.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_046.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_046.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_046.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_046.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_046.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_046.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_046.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=47;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_047.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_047.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_047.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_047.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_047.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_047.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_047.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_047.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=48;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_048.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_048.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_048.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_048.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_048.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_048.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_048.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_048.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=49;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_049.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_049.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_049.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_049.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_049.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_049.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_049.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_049.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=50;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_050.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_050.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_050.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_050.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_050.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_050.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_050.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_050.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=51;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_051.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_051.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_051.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_051.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_051.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_051.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_051.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_051.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=52;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_052.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_052.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_052.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_052.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_052.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_052.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_052.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_052.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=53;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_053.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_053.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_053.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_053.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_053.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_053.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_053.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_053.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=54;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_054.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_054.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_054.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_054.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_054.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_054.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_054.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_054.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=55;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_055.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_055.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_055.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_055.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_055.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_055.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_055.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_055.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=56;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_056.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_056.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_056.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_056.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_056.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_056.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_056.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_056.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=57;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_057.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_057.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_057.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_057.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_057.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_057.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_057.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_057.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=58;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_058.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_058.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_058.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_058.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_058.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_058.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_058.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_058.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=59;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_059.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_059.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_059.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_059.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_059.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_059.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_059.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_059.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=60;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_060.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_060.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_060.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_060.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_060.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_060.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_060.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_060.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=61;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_061.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_061.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_061.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_061.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_061.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_061.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_061.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_061.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=62;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_062.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_062.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_062.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_062.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_062.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_062.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_062.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_062.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=63;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_063.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_063.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_063.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_063.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_063.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_063.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_063.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_063.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=64;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_064.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_064.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_064.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_064.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_064.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_064.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_064.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_064.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=65;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_065.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_065.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_065.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_065.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_065.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_065.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_065.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_065.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=66;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_066.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_066.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_066.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_066.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_066.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_066.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_066.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_066.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=67;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_067.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_067.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_067.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_067.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_067.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_067.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_067.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_067.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=68;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_068.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_068.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_068.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_068.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_068.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_068.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_068.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_068.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=69;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_069.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_069.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_069.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_069.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_069.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_069.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_069.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_069.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=70;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_070.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_070.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_070.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_070.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_070.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_070.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_070.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_070.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=71;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_071.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_071.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_071.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_071.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_071.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_071.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_071.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_071.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=72;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_072.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_072.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_072.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_072.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_072.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_072.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_072.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_072.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=73;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_073.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_073.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_073.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_073.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_073.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_073.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_073.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_073.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=74;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_074.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_074.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_074.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_074.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_074.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_074.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_074.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_074.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=75;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_075.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_075.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_075.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_075.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_075.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_075.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_075.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_075.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=76;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_076.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_076.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_076.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_076.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_076.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_076.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_076.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_076.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=77;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_077.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_077.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_077.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_077.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_077.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_077.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_077.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_077.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=78;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_078.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_078.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_078.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_078.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_078.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_078.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_078.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_078.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=79;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_079.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_079.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_079.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_079.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_079.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_079.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_079.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_079.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=80;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_setup_080.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_data_080.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_mfile_080.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_cfile_080.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_status_080.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_inputs_080.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_obsrv_080.dat','isdir',0);
dsets(rind,cind).errfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/091110/gpsims_simerr_080.mat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


