function [dsets]= gp_data_infomax_090809_001() 
%********************************


dsets=[];
rind=1;
cind=1;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_setup_001.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_data_001.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_mfile_001.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_cfile_001.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_status_003.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_inputs_001.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_obsrv_001.dat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


rind=1;
cind=2;
dsets(rind,cind).setupfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_setup_002.m','isdir',0);
dsets(rind,cind).datafile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_data_002.mat','isdir',0);
dsets(rind,cind).mfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_mfile_002.dat','isdir',0);
dsets(rind,cind).cfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_cfile_002.dat','isdir',0);
dsets(rind,cind).statusfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_status_004.txt','isdir',0);
dsets(rind,cind).inputsfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_inputs_002.dat','isdir',0);
dsets(rind,cind).obsrvfile=FilePath('bcmd','RESULTSDIR','rpath','/asympopt/090809/gpsims_obsrv_002.dat','isdir',0);
dsets(rind,cind).setupfile=[mfilename('fullpath') '.m'];


