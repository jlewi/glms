%this script sets up a simulation
%where we draw stimuli by sampling from the optimal Gaussian process.
%   We compare this to our greedy info. max. algorithm
%   And an algorithm which samples from the uniform ball.
%01-10-2009
%
%
%use a uniform prior. Set the hard cutoff of frequencies to
%be the same cutoff as used in the real values.
function dfiles=gp_setup_gpopt_simbs_ftsep_hard_cutoff()



dfiles=struct();

setpathvars;


fdir='asympopt';
fbase='gpsims';

savecint=2000; %how often to save the covariance matrix;


%table to describe these datasets
%extra row for header
odinfo=cell(1,2);
odinfo{1,1}='label';
odinfo{1,2}='information';

%***********************************************************
%should replace with code for GLM object.
%***********************************************************
glm=GLMPoisson('canon');



%***********************************************************************
%load the strf we fitted to one of the bird song data
%***********************************************************************
strffile=FilePath('RESULTSDIR','bird_song/090130/bsglmfit_data_002.mat');
v=load(getpath(strffile));
strfdata=v.bssimobj;



[t,f]=getstrftimefreq(strfdata.bdata);


%**************************************************************************
%The actual parameters
%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
%pick two orthonormal vectors for theta
theta=strfdata.results(end).theta;
%the strf is represented in the frequency domain so we need to project
%in the high domain
spec=theta(strfdata.mobj.indstim(1):strfdata.mobj.indstim(2));
stimcoeff=tospecdom(strfdata.mobj,spec);

%compute the bias and spike history coefficients
%amplitude of spike history is the value
shistcoeff=theta(strfdata.mobj.indshist(1):strfdata.mobj.indshist(2));
shistcoeff=shistcoeff(end-strfdata.mobj.alength+1:end);

bias=theta(strfdata.mobj.indbias);




%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d('maxiter',1000,'compeig',false);
uoptim=getoptim(updater);
uoptim.TolFun=10^-5;
updater=setoptim(updater,uoptim);

%do a sweep over maxrate
allmaxrates=[2 3 10 100 1000 10^4 10^5];

dind=0;

prior=[];

for magind=1:length(allmaxrates)
    maxrate=allmaxrates(magind);
    %*************************************************************************
    %Posterior/Model Parameters
    %************************************************************************
    %specify length of response
    %This characterizes the length of window in which we look at the spike
    %train after presenting the stimulus and the bin width for this window
    mp.glm=GLMPoisson('canon');

    initmag=.05; %initial guess
    mp.mmag=SimSetup.mmagformaxrate(mp.glm, (stimcoeff(:)'*stimcoeff(:))^.5,maxrate,bias,initmag);
    mp.klength=strfdata.mobj.klength;
    mp.ktlength=strfdata.mobj.ktlength;
    mp.alength=3;
    mp.hasbias=true;
    mp.btouse=MBSFTSep.btouseforcutoff([mp.klength mp.ktlength],10,4);
    mobj=MBSFTSep(mp);


    [nf,nt]=getmaxnfnt(mobj);


  if isempty(prior)
      stimvar=10^-4;
      shistvar=10;
      biasvar=100;
      
      cprior=[stimvar*ones(mobj.nstimcoeff,1); shistvar*ones(mobj.alength,1);biasvar];
     
      
      %set all those frequencies corresponding to frequencies less than 10
      %and 4 to to 10^-2;
      bind=bindexinv(mobj,1:mobj.nstimcoeff);
      
      bset=(bind(:,1)<=10) & (bind(:,2)<=4);
      bset=[bset;zeros(mobj.alength+1,1)];
      cprior(logical(bset))=10^-2;
      
      prior=GaussPost('m',zeros(getparamlen(mobj),1),'c',diag(cprior));

  end



    %**************************************************************************
    %The actual parameters
    %**************************************************************************
    %observer/active learner
    %create observer which actually generates the observations
    %pick two orthonormal vectors for theta
    theta=strfdata.results(end).theta;
    %the strf is represented in the frequency domain so we need to project
    %in the high domain
    spec=theta(strfdata.mobj.indstim(1):strfdata.mobj.indstim(2));
    stimcoeff=tospecdom(strfdata.mobj,spec);

    %compute the bias and spike history coefficients
    %amplitude of spike history is the value
    shistcoeff=theta(strfdata.mobj.indshist(1):strfdata.mobj.indshist(2));
    shistcoeff=shistcoeff(end-mobj.alength+1:end);

    trueparam=[stimcoeff(:);shistcoeff;bias];

    %**************************************************************************
    %Observer
    %***********************************************************************
    %the observer needs to use   the full model
    %porject the parameters into the specdom
    observer=GLMSimulator('model',mobj,'theta', mobj.fullbasis'*trueparam);


    % %
    % updater=BatchML();

    sparam.updater=updater;
    sparam.mobj=mobj;
    sparam.observer=observer;
    sparam.fbase=fbase;
    sparam.fdir=fdir;
    sparam.compentropy=true;
    sparam.savecint=savecint;

    sparam.prior=prior;
    
    %save the time and frequency bins corresponding to the strf
    sparam.extra.t=t;
    sparam.extra.f=f;
    sparam.extra.maxrate=maxrate;
    sparam.extra.strffile=strffile;
    sparam.extra.trueparam=trueparam;
    %**************************************************************************
    %SImulations: we create 1 simulation object for each simulation we want to
    %               run
    %      1) Using the psopt distribution saved in psfile
    %      2) Using a uniform distribution on the same stimuli
    %**************************************************************************

    %*****************************************************************
    %Sim: Estimate the optimal distribution using the posterior
    %******************************************************************
    dind=dind+1;
    lbl='optgp';
    stimobj=POptAsymGPPCanonLB('powcon',mobj.mmag^2,'normalize',false);



    sparam.stimchooser=stimobj;
    sparam.label=lbl;
    about=sprintf('Compute the optimal gaussian distribution without enforcing stationarity constraints.');
    about=sprintf('%s\n Sample this distribution every tk trials where tk is the temporal duration of the strf.',about);
    about=sprintf('%s\n The true parameters are an STRF fitted to real data.',about);
    sparam.about=about;
    dnew=SimSetup.setup(sparam);
    dfiles=copystruct(dfiles,dind,dnew);

    rind=size(odinfo,1)+1;
    odinfo{rind,1}=lbl;
    siminfo={'info',about;'datafile', getrpath(dnew.datafile)};
    siminfo=[siminfo;{'nf max' nf; 'nt max' nt; 'klength',mobj.klength;'ktlength',mobj.ktlength;'alength',mobj.alength;'mmag',mobj.mmag;'maxrate',maxrate;}];

    odinfo{rind,2}=siminfo;

    %*****************************************************************
    %Sim: Sample a white gp
    %******************************************************************
    dind=dind+1;
    lbl='whitenoise';

    spwhite=sparam;


    tdim=getparamlen(mobj);

    stimobj=WhiteNoiseBatch();

    spwhite.stimchooser=stimobj;
    spwhite.label=lbl;

    about=sprintf('Draw white noise stimuli.');
    about=sprintf('%s\n Sample this distribution every tk trials where tk is the temporal duration of the strf.',about);
    about=sprintf('%s\n The true parameters are an STRF fitted to real data.',about);
    spwhite.about=about;

    dnew=SimSetup.setup(spwhite);
    dfiles=copystruct(dfiles,dind,dnew);

    rind=size(odinfo,1)+1;
    odinfo{rind,1}=lbl;
    siminfo={'info',about;'datafile', getrpath(dnew.datafile)};
    siminfo=[siminfo; {'nf max' nf; 'nt max' nt;'klength',mobj.klength;'ktlength',mobj.ktlength;'alength',mobj.alength;'mmag',mobj.mmag;'maxrate',maxrate;}];

    odinfo{rind,2}=siminfo;


end
%***********************************************************
%get the filenums
opath=getpath(dfiles(1).datafile);
opath=fileparts(opath);

fname=getfilename(dfiles(1).datafile);

%get the startnum of the output files
%assume file is name 000.mat
startnum=fname(end-6:end-4);


fname=getfilename(dfiles(end).datafile);
endnum=fname(end-6:end-4);




%**************************************************************
%create a datasetfile
%**************************************************************
setupfile=seqfname(sprintf('~/svn_glms/adaptive_sampling/scripts/optgp/gp_data_infomax_%s.m',datestr(now,'yymmdd')));
writeminit(setupfile,dfiles,[]);

%*********************************************************
%xmlfile to descript this dataset
%********************************************************
oinfo=[{{'setupfile',setupfile}};{odinfo}];
oname=sprintf('%s_%s-%s.xml',mfilename(),startnum,endnum);
onenotetable(oinfo,fullfile(opath,oname));

