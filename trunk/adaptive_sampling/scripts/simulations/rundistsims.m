
%03-11-08
% change the job names
%11-12-07
%This script creates a bunch of distributed jobs and submits them to the
%cluster.


dsets=[];



setpaths;

%call the script to set dsets
%the path on bayes of the dataset script
%4 or 1 or 7
fsetoutname='lowrankbs_dataset_080605_009.m';
%dsetfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace','080605',fsetoutname);

%dsetfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace', ...
%                  '080606','nips08_bsmod.m');

%dsetfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace', ...
%                  '080606','nips08_bslowd.m');
dsetfile=fullfile('/home/jlewi/svn_trunk/allresults','tanspace', ...
                  '080624','lowrankbs_dataset_080624_001.m');





niter=3

%**************************************************************************
%parameters controlling the job
%
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/svn_trunk/adaptive_sampling';

%whether or not to capture command window output
capcmdout=false;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=false;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
svndirs={pwd,MATLABPATH};


%name to use for the job
jname='jtemp';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='~/svn_trunk/allresults';

%if there's some code you want to execute before starting 
%the function (i.e setting the path) you can specify a handle to this
%function 
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=@taskstartupfunc;
%****************************************************
%load the data set
%*****************************************************
%we use rsync to copy the dataset from bayes to hear
src.host=localhost;
src.fname=dsetfile;

%copy the dataset to the current directory
dest.host=jobdata.cluster.host;
dest.fname=fullfile(pwd,'datasettorun.m');


rerr=rsyncfile(src,dest);

if (rerr~=0)
  error('could not rsync the dataset.');
end
datasettorun;


%get the schedule
sched=distsched();

%***********************************************************
%Determine the required files
%**********************************************************
%we create a separate job for each dataset

for dind=1:length(dsets)

  if (dsets(dind).niter>0)



    %*******************************************************
    %initialize structure containing info about the job and the cluster
    %*********************************************************
    finfiles={dsets(dind).fname};
    foutfiles={dsets(dind).fname};
    [jobdata,bcmd]=initjobdata(jname,finfiles,foutfiles,startdir,startupfunc,localhost,localdatadir);
    
 
    %create the job
job(dind)=createJob(sched);

%set the jobdata
job(dind).JobData=jobdata;


%set(job(dind),'PathDependencies',{'/Users/jlewi/svn_trunk/matlab/neuro_cluster/distributedjob'});

niter=dsets(dind).niter;
naout=1;
statusfile=FilePath('bcmd','RESULTSDIR','rpath',sprintf('Job_%d.out',get(job(dind),'ID')));
iparam={dsets(dind).fname,dsets(dind).simvar,niter,statusfile};
task=createTask(job(dind),@simcontinue,naout,iparam);

set(task,'CaptureCommandWindowOutput',true);
submit(job(dind));
end
end




