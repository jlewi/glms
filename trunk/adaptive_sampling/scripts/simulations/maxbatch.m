%1-28-07
%   revised to use new object model
%Script to run optimization for canonical poisson 
clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/maxnumint.out';
diary(dfile);


niter=10;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'batch', sprintf('%s',datestr(datetime,'yymmdd')));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('batch_poissexp.mat'));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************
mparam.klength=2;
mparam.ktlength=2;
mparam.alength=0;
mparam.ktrue=zeros(mparam.klength,1);
%set ktrue to be a sine wave
%which decays exponentially
mparam.ktrue=2*[cos([0:mparam.klength-1]*pi/(mparam.klength-1))]'*exp(log(.1)/(mparam.ktlength-1)*[0:mparam.ktlength-1]);
mparam.ktrue=mparam.ktrue(:);
mparam.atrue=[];

%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength*mparam.ktlength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength*mparam.ktlength+mparam.alength);    

%set mparam.mmag so that when 50% of energy is along the true parameter
%the avg number of spikes is 5 
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(.5*m*(mparam.ktrue'*mparam.ktrue)^.5)-5);
mmag=fsolve(fsetmmag,1,optim);
mparam.mmag=mmag;

mobj=MParamObj('glm',glm,'klength',mparam.klength,'ktlength',mparam.ktlength,'alength',0,'pinit',mparam.pinit,'mmag',mmag,'glm',glm);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue;mparam.atrue]);

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d();
%updater=BatchML();
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);

%generate the stimulus pool
nstim=1000;
stim=uballrnd(1,mparam.klength,nstim);
stim=normmag(stim);
stimpool=GLMInputVec('data',stim(:,1)); 
for j=2:nstim
   stimpool(j)=GLMInputVec('data',stim(:,j)); 
end
[stimmax]=PoolstimBatchHeur('miobj',PoissExpCompMI(),'stimpool',stimpool,'magcon',mparam.mmag);

simmax=SimBatch('mobj',mobj,'stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid);
simmax=update(simmax,niter);
savesim(simmax,fname);
fprintf('Saved to %s \n',fname);

return;

%Code below is outdated
%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimrand=RandStimBall('mmag',mparam.mmag,'klength',mparam.klength);
simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',updater,'simid',simid);
[prand,srrand,simparamrand,mparamrand]=maxupdateobj('mparam',mobj,'simparam',simrand,'ntorun',niter);
savesim('simfile',fname,'pdata',prand,'sr',srrand,'simparam',simparamrand,'mparam',mparamrand);
fprintf('Saved to %s \n',fname);

%turn off diary
diary('off');
%open the file
edit(dfile);
return;

