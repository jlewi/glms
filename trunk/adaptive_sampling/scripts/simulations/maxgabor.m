%4-17-07
%   run canonical poisson under the power constraint on a 2-d gabor
%Script to run optimization for canonical poisson 
%
%10-22-2007
%   updated to new object model and to run sim using random gradients.
clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/maxnumint.out';
diary(dfile);


niter=50;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'gabor', datestr(datetime,'yymmdd'));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fbase=fullfile(RDIR, sprintf('gabor2d.mat'));
[fname, trialindex]=seqfname(fbase);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.lowmem=inf; %how often to save the covariance matrix;

%****************************************************************
%Stimulus features
%****************************************************************
fbase=fullfile(RDIR, sprintf('gabor2d_test.mat'));
mparam.gwidth=5;
mparam.gheight=5;
mparam.alength=0;
mparam.amp=1;
mparam.ktrue=gabortheta(mparam.gwidth,mparam.gheight,[0 0],mparam.amp);
mparam.klength=mparam.gwidth*mparam.gheight;
%normalize it so max is 1
mparam.ktrue=mparam.ktrue/max(mparam.ktrue);
mparam.atrue=[];


%mparam.ktrue(:,1)=2*mparam.ktrue/max(mparam.ktrue);
%mparam.ktrue=[1;0];


%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);    

%set mparam.mmag so that when 50% of energy is along the true parameter
%the avg number of spikes is 200
optim=optimset('TolX',10^-12,'TolF',10^-12);
maxrate=10000;
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag =@(m)(glm.fglmmu(m*(mparam.ktrue'*mparam.ktrue)^.5)-maxrate);
[mmag,fmag,exitflag]=fsolve(fsetmmag,1,optim);
mparam.mmag=mmag;

   %check it converged
    switch (exitflag)
        case {-3,0}
            error('fsolve did not converge for value of stimulus magnitude. Exited with message: \n %s \n trying changing the start point',fsout.message);
            fprintf('postnewton1d: Attempting to solve peak by varying the initializataion point \n');
            %turn messages on
            %increase number of iterations
            options=optimset(options,'Display','off');
            options=optimset(options,'MaxFunEvals',10000000);
            options=optimset(options,'MaxIter',10000000);
    end
    

mobj=MParamObj('glm',glm,'klength',mparam.klength,'pinit',mparam.pinit,'mmag',mparam.mmag);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);
%**************************************************************************
%Pool Stimuli
%***********************************************************************
%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d('maxiter',1000);
uoptim=getoptim(updater);
uoptim.TolFun=10^-5;
updater=setoptim(updater,uoptim);

%updater=BatchML();
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************
extra.mparam=mparam;
extra.maxrate=maxrate;

slist={};
return;

%*****************************************************************
%Stim: Info Max under Power constraint
%******************************************************************
simid='max';
searchmeth='varylagrange';
label=sprintf('Info. max. under power constraint. Use search method: %s.',searchmeth);
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);

stimmax=PoissExpMaxMI('searchmeth',searchmeth);
simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater, 'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simmax,slist(end+1,:)]=savesim(simmax,fname);

%**************************************************************
%stim max eigenvector
%**************************************************************
simid='maxevec';
label='Stimulus=max eigenvector';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimmevec=StimMaxEval();
simmevec=SimulationBase('stimchooser',stimmevec,'observer',observer,'updater',updater, 'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simmevec,slist(end+1,:)]=savesim(simmevec,fname);


%**************************************************************
%stim: choose mean
%**************************************************************
simid='maxmean';
label='Stimulus=posterior mean';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimmean=ChooseMean();
simmean=SimulationBase('stimchooser',stimmean,'observer',observer,'updater',updater, 'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simmean,slist(end+1,:)]=savesim(simmean,fname);


%**********************************************************************
%stim: Pool stimuli using heuristic PoolstimHeurMuMax
%**************************************************************************
simid='maxheur';
nstim=1000
label=sprintf('Info. Max. Using new heuristic PoolstimHeurMuMax %g stimuli',nstim);
[stimheur]=PoolstimHeurMuMax('miobj',PoissExpCompMI(),'nstim',nstim,'debug',false);
simheur=SimulationBase('stimchooser',stimheur,'observer',observer,'updater',updater, 'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simheur,slist(end+1,:)]=savesim(simheur,fname);

%**********************************************************************
%stim: Pool stimuli using our heuristic
%**************************************************************************
simid='maxheur';
nstim=10000
%stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_1600d_071217_001.mat'));
stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_1600d_071213_002.mat'));
label=sprintf('Info. Max. Using new heuristic heurmumaxevec with %g stimuli',nstim);
[stimheur]=PoolstimSubMuProj('fname',stimfile,'miobj',PoissExpCompMI(),'nstim',nstim,'debug',false);
simheur=SimulationBase('stimchooser',stimheur,'observer',observer,'updater',updater, 'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simheur,slist(end+1,:)]=savesim(simheur,fname);

%**********************************************************************
%stim: Pool stimuli using 1000 stimuli ranndomly
%**************************************************************************
%file containing pool of stimuli 
stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_1600d_071213_001.mat'));
simid='maxpool';
nstim=1000;
srescale.sfactor=getmag(mobj);
label=sprintf('Info. Max. Pool drawn randomly with %g stimuli',nstim);
[stimpool]=Poolstim('fname',stimfile,'miobj',PoissExpCompMI(),'nstim',nstim,'debug',false,'stimrescale',srescale);
simpool=SimulationBase('stimchooser',stimpool,'observer',observer,'updater',updater, 'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simpool,slist(end+1,:)]=savesim(simpool,fname);

%**********************************************************************
%stim: Pool stimuli using 10000 stimuli ranndomly
%**************************************************************************
%file containing pool of stimuli 
stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_1600d_071213_002.mat'));
simid='maxpool';
nstim=10000;
srescale.sfactor=getmag(mobj);
label=sprintf('Info. Max. Pool drawn randomly with %g stimuli',nstim);
[stimpool]=Poolstim('fname',stimfile,'miobj',PoissExpCompMI(),'nstim',nstim,'debug',false,'stimrescale',srescale);
simpool=SimulationBase('stimchooser',stimpool,'observer',observer,'updater',updater, 'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simpool,slist(end+1,:)]=savesim(simpool,fname);

%**********************************************************************
%stim: Pool stimuli using 100000 stimuli ranndomly
%**************************************************************************
%file containing pool of stimuli 
stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_1600d_071213_003.mat'));
simid='maxpool';
nstim=100000;
srescale.sfactor=getmag(mobj);
label=sprintf('Info. Max. Pool drawn randomly with %g stimuli',nstim);
[stimpool]=Poolstim('fname',stimfile,'miobj',PoissExpCompMI(),'nstim',nstim,'debug',false,'stimrescale',srescale);
simpool=SimulationBase('stimchooser',stimpool,'observer',observer,'updater',updater, 'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simpool,slist(end+1,:)]=savesim(simpool,fname);

%********************************************************
%stim: random Gradients
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus
simid='grad';
label='Random sinusoidal gratings. After changing how we sample the frequency.';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimgrad=SpatialGrating('dim',[mparam.gheight mparam.gwidth],'speriod',[.1 min([mparam.gheight, mparam.gwidth])])
simgrad=SimulationBase('stimchooser',stimgrad,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simgrad,slist(end+1,:)]=savesim(simgrad,fname);


%***********************************************************
%stim: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
label='i.i.d. design under power constraint.';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimrand=RandStimNorm('mmag',mparam.mmag,'klength',mparam.klength);
simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'extra',extra,'label',label);
fname=seqfname(fbase);
[simrand,slist(end+1,:)]=savesim(simrand,fname);


%create an xml file listing the saved simulations
onenotetable(slist,seqfname(fullfile(RDIR,'simlist.xml')));
