%-29-07
%   simulations of complicated auditory receptive fields with an input
%   nonlinearity
%   Inputs are pushed through a quadratic nonlinearity before being
%   inputted into the GLM
clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile=seqfname('/tmp/maxnumint.out');
diary(dfile);


niter=5;
lowmem=2000;
%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'tanspace', datestr(datetime,'yymmdd'));


%data.fname
%to save data to file
%specify where to save data
%each sim is saved to a different file
foutname='gammatoner500.mat';
fbase=FilePath('bcmd','RESULTSDIR','rpath', fullfile('tanspace', datestr(datetime,'yymmdd'),foutname));



%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************

%**********************************************************
%gammatones
mparam.kl=30;
mparam.width=mparam.kl/2;   %length of the filter
%frequency in hertz
%1/period
%period=(width/numcycles
mparam.freq=1/((mparam.width-1)/4); %1/period

%parameters for gammatone function
mparam.a=.1;
mparam.n=3;
mparam.alength=0;
%set decay rate for envelope
%based on how much we want it to decay over entire window
mparam.b=log (.01)/(-2*pi*(mparam.width));
%set ktrue to be gammatone filters
mparam.c1=.25*mparam.kl;
t=[0:mparam.kl-1];
mparam.k1=mparam.a*t.^(mparam.n-1).*sin(2*pi*mparam.freq*t).*exp(-2*pi*mparam.b*t);
%zero it out after width
mparam.k1(mparam.width+1:end)=0;

%k2 is just k1 shifted into the region where mparam.k1 is zero
%this guarantees the two filters are orthogonal
mparam.k2=zeros(1,mparam.kl);
mparam.k2(mparam.width+1:end)=mparam.k1(1:mparam.width);

mparam.ktrue=mparam.k1'*mparam.k1+mparam.k2'*mparam.k2;

%create the input nonlinearity object
nobj=QuadNonLinInp('d',mparam.kl);
mparam.ktrue=projqmat(nobj,mparam.ktrue);
mparam.atrue=[];
mparam.klength=length(mparam.ktrue);
glm=GLMModel('poisson','canon');

%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
%set mparam.mmag so that the max firing rate for the stimulus in the pool most
%correlated with the true parameters is 100.
optim=optimset([],'Display','off');
optim=optimset(optim,'TolX',10^-12);
optim=optimset(optim,'TolFun',10^-12);
optim=optimset(optim,'MaxIter',1000);



%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=TanSpaceUpdater('compeig',1,'optim',optim,'comptanentropy',true);


%we need to make the prior a Tangent Space representation
%rank is 2
rank=2;
evec=eye(mparam.kl);
evec=evec(:,1:rank);
tinit=QuadInpTanSpace('evec',evec,'eigd',[1 1]);
%
%full posterior for the quadratic nonlinearity has (d^2+2)/d parameters
%where d=dim(stim)

fprior=GaussPost('m',gettheta(tinit),'c',eye(getdimtheta(tinit)));

tparam=getparam(tinit);
mparam.pinit=PostTanSpace('fullpost',fprior,'tanspacetype','QuadInpTanSpace','tanparam',tparam);


%mmag is set later on
mobj=MGLMNonLinInp('klength',mparam.klength,'alength',0,'pinit',mparam.pinit,'mmag',nan,'glm',glm,'ninpobj',nobj,'stimlen',mparam.kl);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue;mparam.atrue]);

%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);


%*************************************************
%methods to run
infomax=true;
randdesign=true;
infomaxfull=true;


%optimize the stimulus in feature space and then project
%the optimal stimulus in feature space back to input space
infomaxfeat=true;
infomaxfullfeat=true;
slist={};

%create simulations for many different max rates
dsets=[];
dind=0;
for maxrate=[500]


    nstim=1000;
    %**********************************
    %if we use PoolstimQuadInpHeur we need to set the magnitude constraint
    %*********************************************************************
    %set mparam.mmag so that when 50% of energy is along the true parameter
    %the avg number of spikes is maxrate
    optim=optimset('TolX',10^-12,'TolF',10^-12);
    k1mag=sum(mparam.k1.^2)^.5;
    fsetmmag=@(m)(glm.fglmmu((m*k1mag)^2)-maxrate);
    [mmag,fval,exitflag,fsout]=fsolve(fsetmmag,.5,optim);

    %check it converged
    switch (exitflag)
        case {-3,-2,0}
            error('fsolve did not converge for value of stimulus magnitude. Exited with message: \n %s \n trying changing the start point',fsout.message);         
    end
    mparam.mmag=real(mmag);

    %adjust the magnitude constraint
    mobj=setmmag(mobj,mparam.mmag);

    %*******************************************************************
    %extra: is a structure belonging to the Simulation object which can save
    %   any extra structures we want. i.e it can save helper variables used to
    %   construct the parameters for the model
    extra.mparam=mparam;
    extra.maxrate=maxrate;


    %*************************************************************
    %Update: info. max. using heuristic to generate the set
    %********************************************************
    %use heuristic for choosing stimuli
    %how many trials to do full infomax on the start with
%    nstart=[10 100 300 400];
nstart=10;
    for nind=1:length(nstart)
        if (infomax)
            modheur='mu';
            simid=sprintf('infomax%d',nstart(nind));
            [fname]=seqfname(fbase);
            iterfullmax=20;
            label=sprintf('Info max using startfullmax=%d \t iterfullmax=%d',nstart(nind),iterfullmax);
            [stimmax]=TanSpaceQuadInpInfoMaxCanon('nstim',nstim,'startfullmax',nstart(nind),'iterfullmax',iterfullmax);

            simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'extra',extra,'lowmem',lowmem,'label',label);
            [simmax, slist(end+1,:)]=savesim(simmax,fname);
            
            dind=dind+1;
            dsets(dind).fname=fname;
            dsets(dind).simvar=simid;
                        dsets(dind).lbl=label;
        end
    end




    %********************************************************************
    %InfoMax: but infomax on full theta space not submanifold
    %*****************************************************
    if (infomaxfull)
        simid='infomaxfull'
        %reseed the random number generator
        rstate=10;
        randn('state',rstate);
        rand('state',rstate);
        stimfull=TanSpaceQuadInpInfoMaxCanon('nstim',nstim,'startfullmax',inf,'iterfullmax',1);

        label='Info. max using full posterior';
        simfull=SimulationBase('stimchooser',stimfull,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',lowmem,'label',label,'extra',extra);
        fname=seqfname(fbase);
        [simfull, slist(end+1,:)]=savesim(simfull,fname);
        
          dind=dind+1;
            dsets(dind).fname=fname;
            dsets(dind).simvar=simid;
            dsets(dind).lbl=label;
    end

    
     %*************************************************************
    %Update: info. max. to optimize the stimulus in feature space
    %********************************************************
    %use heuristic for choosing stimuli
    %how many trials to do full infomax on the start with
%    nstart=[10 100 300 400];
    nstart=10;
    for nind=1:length(nstart)
        if (infomaxfeat)
            modheur='mu';
            simid=sprintf('infomaxfeat%d',nstart(nind));
            [fname]=seqfname(fbase);
            iterfullmax=20;
            label=sprintf('Info max using startfullmax=%d \t iterfullmax=%d. Optimization is in feature space.',nstart(nind),iterfullmax);
            [stimmax]=TanSpaceQuadInpInfoMaxCanonFeature('startfullmax',nstart(nind),'iterfullmax',iterfullmax);

            simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'extra',extra,'lowmem',lowmem,'label',label);
            [simmax, slist(end+1,:)]=savesim(simmax,fname);
            
            dind=dind+1;
            dsets(dind).fname=fname;
            dsets(dind).simvar=simid;
            dsets(dind).lbl=label;
        end
    end

    %********************************************************************
    %InfoMax:update using full posterior. optimize stimulus in feature
    %space.
    %*****************************************************
    if (infomaxfullfeat)
        simid='infomaxfeatfull'
        %reseed the random number generator
        rstate=10;
        randn('state',rstate);
        rand('state',rstate);
        stimfull=TanSpaceQuadInpInfoMaxCanonFeature('startfullmax',inf,'iterfullmax',1);

        label='Info. max using full posterior. Optimization is done in feature space.';
        simfull=SimulationBase('stimchooser',stimfull,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',lowmem,'label',label,'extra',extra);
        fname=seqfname(fbase);
        [simfull, slist(end+1,:)]=savesim(simfull,fname);
        
        dind=dind+1;
            dsets(dind).fname=fname;
            dsets(dind).simvar=simid;
            dsets(dind).lbl=label;
    end
    
    %********************************************************
    %Update: random Stimuli
    %********************************************************
    %**************************************************************************
    %run the trials but when we optimize the stimulus
    if (randdesign)
        simid='rand';
        [fname]=seqfname(fbase);
        %reseed the random number generator
        rstate=10;
        randn('state',rstate);
        rand('state',rstate);

        label='iid design.';
        stimrand=RandStimNorm('mmag',getmag(mobj),'klength',getstimlen(mobj));


        simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'mobj',mobj,'updater',updater,'simid',simid,'extra',extra,'lowmem',lowmem,'label',label);

        [simrand, slist(end+1,:)]=savesim(simrand,fname);
     
        
        dind=dind+1;
            dsets(dind).fname=fname;
            dsets(dind).simvar=simid;
            dsets(dind).lbl=label;
    end

    %turn off diary
    diary('off');
end

%%
%create a .m file to recreate this data set
fsetoutname=sprintf('co08datagammatones_%s.m',datestr(datetime,'yymmdd'));
dsetfilebase=FilePath('bcmd','RESULTSDIR','rpath', fullfile('tanspace', datestr(datetime,'yymmdd'),fsetoutname));
dsetfile=seqfname(dsetfilebase);

info.maxrate=maxrate;
info.kl=mparam.kl;
info.lowmem=lowmem;
writedataset(dsetfile,dsets,info);
%create an xml file listing the saved simulations
onenotetable([{'Dataset', getpath(dsetfile),''};slist],seqfname(fullfile(RDIR,'simlist.xml')));
%fprintf('Creating xml description \n');
%createxmldescr(fname);
return;

