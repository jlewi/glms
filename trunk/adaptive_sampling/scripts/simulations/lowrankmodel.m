%11-18-2007
%
%Run a simulation using a theta which is a matrix but which is low rank
%
clear variables
%close all;

setpathvars;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile=seqfname('/tmp/maxnumint.out');
diary(dfile);


niter=50;

%***********************************************************
%should replace with code for GLM object.
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'lowrank', datestr(datetime,'yymmdd'));


%data.fname
%to save data to file
%specify where to save data
%leave blank not to save
fname=fullfile(RDIR, sprintf('lowrank.mat'));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.lowmem=1000; %how often to save the covariance matrix;

%****************************************************************
%Stimulus features
%****************************************************************
trank=2;
matdim=[10 10];

%create a matrix with the specified dimensions and rank
kmat=randn(matdim);
[u s v]= svd(kmat);
kmat=u(:,1:trank)*s(1:trank,1:trank)*v(:,1:trank)';
mparam.ktrue=kmat(:);


%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
fprior=GaussPost('m',zeros(prod(matdim),1),'c',3*eye(prod(matdim)));


submprior=GaussPost('m',zeros(trank,1),'c',3*eye(trank));

%*************************************************************
%Create the LRMixturePost prior
%*************************************************************
%randomly create the submodels
clear subm;
clear submpost;
nmodels=2;

basis=zeros([matdim trank]);
submprior=repmat(submprior,[1,nmodels]);
for mi=1:nmodels
    [u s v]=svd(rand(matdim));
    for r=1:trank
        basis(:,:,r)=u(:,r)*v(:,r)';

    end
    subm(mi)=MBiLinearSub('basis',basis,'pinit',submprior(mi));

end

mparam.pinit=LRMixturePost('fpost',fprior,'subm',subm,'submpost',submprior);

%set mparam.mmag so that when 50% of energy is along the true parameter
%the avg number of spikes is 200
optim=optimset('TolX',10^-12,'TolF',10^-12);
mparam.avgspike=200;
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(.5*m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.avgspike);
mmag=fsolve(fsetmmag,1,optim);
mparam.mmag=mmag;


mobj=MBiLinear('glm',glm,'matdim',matdim,'pinit',mparam.pinit,'mmag',mparam.mmag,'rank',trank,'submprior',submprior(1));

%**************************************************************************
%observer
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);
%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
%updater=Newton1d('maxiter',100);
%uoptim=getoptim(updater);
%uoptim.TolFun=10^-5;
%updater=setoptim(updater,uoptim);
updater=LRMixtureUpdate();

%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%**************************************************************************

%%**********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimrand=RandStimNorm('mmag',mparam.mmag,'klength',prod(matdim));
simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem);
savesim(simrand,fname);
fprintf('Saved to %s \n',fname);

%turn off diary
diary('off');
%open the file
edit(dfile);
return;

