%3-28-2007
%   revised for object model version simver-070318
%1-28-07
%   revised to use new object model
%Script to run optimization for canonical poisson 
function [fname]=misspecpowerexp
%clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/misspec.out';
diary(dfile);


niter=100;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
%model glm is used for the updater and picking sitmuli
modelglm=GLMModel('poisson','canon');

%true glm is used for the observer which generates the responses
simparam.alpha=2;
trueglm=GLMModel('poisson',@(glmproj)(nonlin_power(glmproj,simparam.alpha)))
%trueglm=GLMModel('poisson',@glm1dlog);



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'misspec', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_poisspowercanon.mat',datetime(2),datetime(3)));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************
mparam.klength=50;
mparam.alength=0;
mparam.ktrue=zeros(mparam.klength,1);
%set ktrue to be a sine wave
%mparam.ktrue=2*[cos([0:mparam.klength-1]*pi/(mparam.klength-1))]';
% gabor function
center=mparam.klength/2;
afreq=4*pi/mparam.klength;  %angular frequency;
mparam.ktrue=normpdf((1:mparam.klength)',center,mparam.klength/4).*cos(([1:mparam.klength]-center)'*afreq);
%zero out points more than 1.5pi/afreq away from the center
mparam.ktrue(floor(center+1.5*pi/afreq):end)=0;
mparam.ktrue(1:floor(center-1.5*pi/afreq))=0;


%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);    
mparam.atrue=[];
%set mparam.mmag so that when 50% of energy is along the true parameter
%the avg number of spikes is 5 
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(trueglm.fglmmu(.5*m*(sum(mparam.ktrue.^2)^.5))-5);
mparam.mmag=fsolve(fsetmmag,1,optim);


%mparam=MParamObj('ktrue',mparam.ktrue,'pinit',mparam.pinit,'mmag',mmag);
mobj=MParamObj('glm',modelglm,'klength',mparam.klength,'pinit',mparam.pinit,'mmag',mparam.mmag);

%*******************************************************************
%extra: is a structure belonging to the Simulation object which can save
%   any extra structures we want. i.e it can save helper variables used to
%   construct the parameters for the model
extra.mparam=mparam;
%*************************
%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
%model for the observer
mobserver=MParamObj('glm',trueglm,'klength',mparam.klength,'pinit',mparam.pinit,'mmag',mparam.mmag);
observer=GLMSimulator('model',mobserver,'theta', [mparam.ktrue;mparam.atrue]);

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d();

%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
%stimmax=PoissExpElliptic('ellipmeth',1);
stimmax=PoissExpMaxMI();;

simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid,'extra',extra);
[pmax,srmax,simparammax,mparammax]=maxupdateobj('mparam',mobj,'simparam',simmax,'ntorun',niter);
savesim('simfile',fname,'pdata',pmax,'sr',srmax,'simparam',simparammax,'mparam',mparammax);
fprintf('Saved to %s \n',fname);

%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimrand=RandStimBall('mmag',mparam.mmag,'klength',mparam.klength);
simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',updater,'simid',simid,'extra',extra);
[prand,srrand,simparamrand,mparamrand]=maxupdateobj('mparam',mobj,'simparam',simrand,'ntorun',niter);
savesim('simfile',fname,'pdata',prand,'sr',srrand,'simparam',simparamrand,'mparam',mparamrand);
fprintf('Saved to %s \n',fname);

%turn off diary
diary('off');
%open the file
%edit(dfile);
return;

