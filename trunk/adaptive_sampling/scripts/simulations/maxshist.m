%10-19-2007
%
%New script for spike history results. 
%Use a nonlinearity other than the exponential function. This should allow
%us to use large inhibitory gains for the spike history.
%
clear variables;
%close all;

setpathvars;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/maxnumint.out';
diary(dfile);


niter=0;

%***********************************************************
%GLM: Quadratic nonlinearity
%***********************************************************
simparam.alpha=2;
%glm=GLMModel('poisson',@(glmproj)(nonlin_power(glmproj,simparam.alpha)));
glm=GLMModel('poisson','canon');


%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'shist', sprintf('%s',datestr(datetime,'yymmdd')));


%to save data to file
%fname=fullfile(RDIR, sprintf('powernonlin.mat'));
fname=fullfile(RDIR, sprintf('expnonlin.mat'));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.lowmem=500; %how often to save the covariance matrix;

%****************************************************************
%Stimulus features
%****************************************************************
mparam.klength=400;

%for debugging start with no spike history
%I think there is bug with update for non-canonical poisson
%mparam.alength=50;
mparam.ktrue=gabor1d(mparam.klength,0)';
%mparam.ktrue=[1; -1];
mparam.alength=20;

%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);    

%**********************************************************************
%adjust the stimulus constraint
%**************************************************************************
%set mparam.mmag so that when 50% of energy is along the true parameter
%the avg number of spikes is 100
optim=optimset('TolX',10^-12,'TolF',10^-12);
%mparam.avgspike=50;
%do maximum spikes instead
mparam.maxspike=1000;
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.maxspike);
mmag=fsolve(fsetmmag,1,optim);
mparam.mmag=mmag;

%*****************************************************************
%set spike history filter
%******************************************************************
%set atrue as decaying exponential
%decay to 10 at final value
mparam.atrue=[0:mparam.alength-1]';
tau=(mparam.alength-1)/log(.10);
%adjust gain so that at avg spike rate firing, would perfectly
%set firing to zero assuming 50% of stimulus is along theta
mparam.atrue=-exp(mparam.atrue/tau);
%gain=(.5*mparam.mmag*(mparam.ktrue'*mparam.ktrue)^.5)/(mparam.avgspike*sum(mparam.atrue));
%mparam better be negative or we will get positive feedback
%mparam.atrue=-1*gain*mparam.atrue;
%for debugging multiply atrue to make it so small that it is effectively
%neglible
%mparam.atrue=mparam.atrue*10;
gain=.403;
mparam.atrue=gain*mparam.atrue;

%*********************************************************************
%Objects
mobj=MParamObj('glm',glm,'klength',mparam.klength,'alength',mparam.alength,'pinit',mparam.pinit,'mmag',mmag);
%**************************************************************************
%observer
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue;mparam.atrue]);
updater=Newton1d();
%updater=setdebug(updater,1);

%************************************************************************
%Info.Max using our heuristic
%**********************************************************************
%stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_100d_071111_001.mat'));
nstim=1000;
%[stimmax]=PoolstimSubMuProj('fname',stimfile,'miobj',PoissExpCompMI(),'nstim',nstim,'debug',false);
[stimmax]=PoissExpMaxMi();
simid='max';
simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid,'lowmem',mparam.lowmem,'mobj',mobj);
if (niter>0)
simmax=update(simmax,niter);
end
savesim(simmax,fname);
fprintf('Saved to %s \n',fname);
%************************************************************************
%Random stimuli
%**********************************************************************
stimrand=RandStimNorm('mmag',mparam.mmag,'klength',mparam.klength);
%stimrand=Poolstim('fname',stimfile);
simid='rand';
simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',updater,'simid',simid,'lowmem',mparam.lowmem,'mobj',mobj);
if (niter>0)
simrand=update(simrand,niter);
end
savesim(simrand,fname);
fprintf('Saved to %s \n',fname);

