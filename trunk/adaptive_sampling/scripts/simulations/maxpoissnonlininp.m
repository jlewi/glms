%3-19-07
%   Inputs are pushed through a quadratic nonlinearity before being
%   inputted into the GLM
clear all
%close all;

setpathvars;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile=seqfname('/tmp/maxnumint.out');
diary(dfile);


niter=2;

%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'poolbased', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_poissexp.mat',datetime(2),datetime(3)));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************
%k1 and k2 are 2-d gabor images which we rasterize to form vectors
%k1 and k2 should be 90 degree rotations of each other
%this should be square otherwise when we rotate we will get problems
mparam.gwidth=11;
mparam.gheight=11;
mparam.alength=0;
%k1
mparam.k1=gabortheta(mparam.gwidth,mparam.gheight,[0 0],10);
k1matrix=reshape(mparam.k1,mparam.gheight,mparam.gwidth);
%
%k2 rotate the matrix 90 by taking transpose
k2matrix=k1matrix';
mparam.kl=mparam.gwidth*mparam.gheight;
mparam.k2=k2matrix(:);
mparam.ktrue=mparam.k1*mparam.k1'+mparam.k2*mparam.k2';
mparam.ktrue=mparam.ktrue';             %doesn't matter in this case but do it for more general values of ktrue
mparam.ktrue=mparam.ktrue(:);
mparam.klength=length(mparam.ktrue);
mparam.atrue=[];
%mparam.ktrue(:,1)=2*mparam.ktrue/max(mparam.ktrue);
%mparam.ktrue=[1;0];

glm=GLMModel('poisson','canon');

%**************************************************************************
%initialize the posterior
%*******************************************simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimmax=PoissExpMaxMI();
%stimmax=ChooseMean;
%stimmax=StimMaxEval();
extra.mparam=mparam;
simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid,'lowmem',mparam.lowmem,'extra',extra);
[pmax,srmax,simparammax,mparammax]=maxupdateobj('mparam',mobj,'simparam',simmax,'ntorun',niter);
savesim('simfile',fname,'pdata',pmax,'sr',srmax,'simparam',simparammax,'mparam',mparammax);
fprintf('Saved to %s \n',fname);

return;*******************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);    

%set mparam.mmag so that the max firing rate for the stimulus in the pool most
%correlated with the true parameters is 100.
optim=optimset('TolX',10^-12,'TolF',10^-12);
mparam.maxrate=100;
nstim=1000;
%fsetmmag=@(m)(glm.fglmmu(.5*m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.avgspike);
%mmag=fsolve(fsetmmag,1,optim);
%mparam.mmag=mmag;

mobj=MGLMNonLinINp('klength',mparam.klength,'alength',0,'pinit',mparam.pinit,'mmag',1,'glm',glm,'ninpfunc',@projinpquad,'fparam',[],'stimlen',mparam.kl);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue;mparam.atrue]);

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d('compeig',0);
%updater=BatchML();

%*******************************************************************
%extra: is a structure belonging to the Simulation object which can save
%   any extra structures we want. i.e it can save helper variables used to
%   construct the parameters for the model
extra.mparam=mparam;
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d25_05_11_001.mat');

%specify the parameters for autmoatically rescaling the magnitudes of the
%stimuli
%srescale.avgrate=5; %median firing rate for stimuli
%set mparam.mmag so that when 90% of energy is along the true parameter
%the avg number of spikes is 5 
optim=optimset('TolX',10^-12,'TolF',10^-12);
k1mag=(mparam.k1'*mparam.k1)^.5;
fsetmmag=@(m)(glm.fglmmu((.7*m*k1mag)^2)-mparam.maxrate);
mmag=fsolve(fsetmmag,1,optim);
mparam.mmag=mmag;
srescale=[];
srescale.sfactor=mparam.mmag;

lowmem=inf;     %don't save covariance matrices
%*************************************************************
%Update: info. max. using heuristic to generate the set
%********************************************************
%use heuristic for choosing stimuli
[stimmax,mparam.scalefactor]=PoolstimQuadInpHeur('fname',stimfile,'miobj',PoissExpCompMI(),'model',mobj,'stimrescale',srescale,'debug',true,'magcon',mparam.mmag,'nstim',nstim);
%[stimmax,mparam.scalefactor]=PoolstimNonLinInp('fname',stimfile,'miobj',PoissExpCompMI(),'model',mobj,'stimrescale',srescale);


simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid,'extra',extra,'lowmem',lowmem);
[pmax,srmax,simparammax,mparammax]=maxupdateobj('mparam',mobj,'simparam',simmax,'ntorun',niter);
savesim('simfile',fname,'pdata',pmax,'sr',srmax,'simparam',simparammax,'mparam',mparammax);
fprintf('Saved to %s \n',fname);

%*************************************************************
%Update: info. max. set is uniformly sampled from sphere
%********************************************************
simid='unif';

[stimuni,mparam.scalefactor]=PoolstimNonLinInp('fname',stimfile,'miobj',PoissExpCompMI(),'model',mobj,'stimrescale',srescale,'nstim',nstim);

simuni=SimulationBase('stimchooser',stimuni,'observer',observer,'updater',updater,'simid',simid,'extra',extra,'lowmem',lowmem);
[puni,sruni,simparamuni,mparamuni]=maxupdateobj('mparam',mobj,'simparam',simuni,'ntorun',niter);
savesim('simfile',fname,'pdata',puni,'sr',sruni,'simparam',simparamuni,'mparam',mparamuni);
fprintf('Saved to %s \n',fname);

%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);

stimrand=PoolstimNonLinInp('fname',stimfile,'rand',1,'stimrescale',srescale,'nstim',nstim);



simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',updater,'simid',simid,'extra',extra,'lowmem',lowmem);
[prand,srrand,simparamrand,mparamrand]=maxupdateobj('mparam',mobj,'simparam',simrand,'ntorun',niter);
savesim('simfile',fname,'pdata',prand,'sr',srrand,'simparam',simparamrand,'mparam',mparamrand);
fprintf('Saved to %s \n',fname);

%turn off diary
diary('off');
%open the file
edit(dfile);
return;

