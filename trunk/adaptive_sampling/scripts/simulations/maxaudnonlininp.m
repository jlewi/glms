%-29-07
%   simulations of complicated auditory receptive fields with an input
%   nonlinearity
%   Inputs are pushed through a quadratic nonlinearity before being
%   inputted into the GLM
clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile=seqfname('/tmp/maxnumint.out');
diary(dfile);


niter=5;
lowmem=2000;
%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'inpnonlin', datestr(datetime,'yymmdd'));


%data.fname
%to save data to file
%specify where to save data
%each sim is saved to a different file

fbase=fullfile(RDIR, sprintf('audnonlin.mat'));



%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************
mparam.kl=60;
mparam.width=mparam.kl/2;   %length of the filter
%frequency in hertz
%1/period
%period=(width/numcycles
mparam.freq=1/((mparam.width-1)/4); %1/period

%parameters for gammatone function
mparam.a=.1;
mparam.n=3;
mparam.alength=0;
%set decay rate for envelope
%based on how much we want it to decay over entire window
mparam.b=log (.01)/(-2*pi*(mparam.width));
%set ktrue to be gammatone filters
mparam.c1=.25*mparam.kl;
t=[0:mparam.kl-1];
mparam.k1=mparam.a*t.^(mparam.n-1).*sin(2*pi*mparam.freq*t).*exp(-2*pi*mparam.b*t);
%zero it out after width
mparam.k1(mparam.width+1:end)=0;

%k2 is just k1 shifted into the region where mparam.k1 is zero
%this guarantees the two filters are orthogonal
mparam.k2=zeros(1,mparam.kl);
mparam.k2(mparam.width+1:end)=mparam.k1(1:mparam.width);

mparam.ktrue=mparam.k1'*mparam.k1+mparam.k2'*mparam.k2;
%create the input nonlinearity object
nobj=QuadNonLinInp('d',mparam.kl);
mparam.ktrue=projqmat(nobj,mparam.ktrue);
%mparam.ktrue=mparam.ktrue';             %doesn't matter in this case but do it for more general values of ktrue
%mparam.ktrue=mparam.ktrue(:);
mparam.atrue=[];
%mparam.ktrue(:,1)=2*mparam.ktrue/max(mparam.ktrue);
%mparam.ktrue=[1;0];
mparam.klength=length(mparam.ktrue);
glm=GLMModel('poisson','canon');

%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);

%set mparam.mmag so that the max firing rate for the stimulus in the pool most
%correlated with the true parameters is 100.
optim=optimset([],'Display','off');
optim=optimset(optim,'TolX',10^-12);
optim=optimset(optim,'TolFun',10^-12);
optim=optimset(optim,'MaxIter',1000);

%set mmag to nan to indicate its ignored
%10-29-2007. I think I originally set mmag to ignore
%because the rescaling was computed by Poolstim.
%For consistency I will set mobj mmag to have the same value later on.
mobj=MGLMNonLinINp('klength',mparam.klength,'alength',0,'pinit',mparam.pinit,'mmag',nan,'glm',glm,'ninpobj',nobj,'stimlen',mparam.kl);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue;mparam.atrue]);

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d('compeig',0,'optim',optim);
%updater=BatchML();


%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
%stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d10_03_14_001.mat');
%stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d20_05_30_001.mat');
%stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d50_05_29_001.mat');

stimfile=FilePath('bcmd','RESULTSDIR','rpath',fullfile('poolbased','pooldata_d60_06_07_001.mat'));

%stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d10_06_01_001.mat');
%specify the parameters for autmoatically rescaling the magnitudes of the
%stimuli
%srescale.avgrate=5; %median firing rate for stimuli
srescale=[];

%create simulations for many different max rates
for maxrate=[100 1000 10000]

    srescale.theta=gettheta(observer);

    nstim=1000;
    %**********************************
    %if we use PoolstimQuadInpHeur we need to set the magnitude constraint
    %*********************************************************************
    %set mparam.mmag so that when 50% of energy is along the true parameter
    %the avg number of spikes is 5
    optim=optimset('TolX',10^-12,'TolF',10^-12);
    k1mag=sum(mparam.k1.^2)^.5;
    fsetmmag=@(m)(glm.fglmmu((m*k1mag)^2)-maxrate);
    [mmag,fval,exitflag,fsout]=fsolve(fsetmmag,.01,optim);

    %check it converged
    switch (exitflag)
        case {-3,0}
            error('fsolve did not converge for value of stimulus magnitude. Exited with message: \n %s \n trying changing the start point',fsout.message);
            fprintf('postnewton1d: Attempting to solve peak by varying the initializataion point \n');
            %turn messages on
            %increase number of iterations
            options=optimset(options,'Display','off');
            options=optimset(options,'MaxFunEvals',10000000);
            options=optimset(options,'MaxIter',10000000);
    end
    mparam.mmag=real(mmag);
    srescale=[];
    srescale.sfactor=real(mparam.mmag);

    %adjust the magnitude constraint
    mobj=setmmag(mobj,mparam.mmag);

    %*******************************************************************
    %extra: is a structure belonging to the Simulation object which can save
    %   any extra structures we want. i.e it can save helper variables used to
    %   construct the parameters for the model
    extra.mparam=mparam;
    extra.maxrate=maxrate;


    %*************************************************************
    %Update: info. max. using heuristic to generate the set
    %********************************************************
    %use heuristic for choosing stimuli
    modheur='mu';
    simid='max';
    [fname]=seqfname(fbase);
    label=['Info max using heuristic: ' modheur]
    [stimmax,mparam.scalefactor]=PoolstimQuadInpHeur('fname',stimfile,'modheur',modheur,'miobj',PoissExpCompMI(),'model',mobj,'stimrescale',srescale,'debug',true,'magcon',mparam.mmag,'nstim',nstim);

    simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'extra',extra,'lowmem',lowmem,'label',label);
    savesim(simmax,fname);
    fprintf('Saved to %s \n',fname);
    %***************************************
    %use original heuristic for choosing stimuli
    modheur='muorig';
    simid='max';
    [fname]=seqfname(fbase);
    label=['Info max using heuristic: ' modheur]
    [stimmax,mparam.scalefactor]=PoolstimQuadInpHeur('fname',stimfile,'modheur',modheur,'miobj',PoissExpCompMI(),'model',mobj,'stimrescale',srescale,'debug',true,'magcon',mparam.mmag,'nstim',nstim);

    simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'extra',extra,'lowmem',lowmem,'label',label);
    savesim(simmax,fname);
    fprintf('Saved to %s \n',fname);


    %*************************************************************
    %Update: using sinusoidal tones
    %********************************************************
    simid='tones';
    [fname]=seqfname(fbase);
    [stimtones]=PureTones('dim',getstimlen(mobj),'period',[.1 120]);

    simtones=SimulationBase('stimchooser',stimtones,'observer',observer,'mobj',mobj,'updater',updater,'mobj',mobj,'simid',simid,'extra',extra,'lowmem',lowmem);
    savesim(simtones,fname);
    fprintf('Saved to %s \n',fname);

    %*************************************************************
    %Update: info. max. set is uniformly sampled from sphere
    %********************************************************
    simid='unif';
    [fname]=seqfname(fbase);
    [stimuni,mparam.scalefactor]=PoolstimNonLinInp('fname',stimfile,'miobj',PoissExpCompMI(),'model',mobj,'stimrescale',srescale);

    simuni=SimulationBase('stimchooser',stimuni,'observer',observer,'mobj',mobj,'updater',updater,'mobj',mobj,'simid',simid,'extra',extra,'lowmem',lowmem);
    savesim(simuni,fname);
    fprintf('Saved to %s \n',fname);

    %********************************************************
    %Update: random Stimuli
    %********************************************************
    %**************************************************************************
    %run the trials but when we optimize the stimulus
    simid='rand';
    [fname]=seqfname(fbase);
    %reseed the random number generator
    rstate=10;
    randn('state',rstate);
    rand('state',rstate);


    stimrand=PoolstimNonLinInp('fname',stimfile,'rand',1,'stimrescale',srescale,'nstim',nstim);



    simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'mobj',mobj,'updater',updater,'simid',simid,'extra',extra,'lowmem',lowmem);

    savesim(simrand,fname);
    fprintf('Saved to %s \n',fname);

    %turn off diary
    diary('off');
    %open the file
    %edit(dfile);
end

%fprintf('Creating xml description \n');
%createxmldescr(fname);
return;

