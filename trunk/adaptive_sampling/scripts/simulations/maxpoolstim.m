%3-14-07
%   select optimal stimuli from a pool
clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/maxnumint.out';
diary(dfile);


niter=20;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'poolbased', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_poolstim.mat',datetime(2),datetime(3)));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************

mparam.klength=5;
mparam.alength=0;
mparam.ktrue=zeros(mparam.klength,1);
%set ktrue to be a gabor function
%set ktrue to a Gabor function
center=(mparam.klength-1)/2;
afreq=3/4*2*pi/(mparam.klength-1-center);  %angular frequency;
mparam.ktrue=normpdf((0:mparam.klength-1)',center,(mparam.klength-1)/4).*cos(([0:mparam.klength-1]-center)'*afreq);

%mparam.ktrue=2*[cos([0:mparam.klength-1]*pi/(mparam.klength-1))]';
%mparam.ktrue(:,1)=2*mparam.ktrue/max(mparam.ktrue);
%mparam.ktrue=[1;0];


%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);   

%mparam.pinit.c=[0.0482    0.0438;0.0438    0.0663];
%mparam.pinit.m=[2; -2];

%max firing rate
mparam.maxrate=50;
mparam.nstim=100;           %number of stimuli to use in pool
mobj=MParamObj('glm',glm,'klength',mparam.klength,'pinit',mparam.pinit,'mmag',1);

%*******************************************************************
%extra: is a structure belonging to the Simulation object which can save
%   any extra structures we want. i.e it can save helper variables used to
%   construct the parameters for the model
extra.mparam=mparam;
%*************************

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d();
%updater=BatchML();
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
%use pool based stimuli
%stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d10_03_14_001.mat');
%stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d02_03_19_001.mat');
stimfile=fullfile(RESULTSDIR,'poolbased','pooldata_d05_04_23_001.mat');

srescale=[];
srescale.maxrate=100;
srescale.theta=gettheta(observer);
srescale.glm=mobj.glm;

%stimuli are selected randomly from a pool
[stimmax]=Poolstim('fname',stimfile,'miobj',PoissExpCompMI(),'stimrescale',srescale,'nstim',mparam.nstim);

simmax=SimulationBase('glm',glm,'stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid,'extra',extra);
[pmax,srmax,simparammax,mparammax]=maxupdateobj('mparam',mobj,'simparam',simmax,'ntorun',niter);
savesim('simfile',fname,'pdata',pmax,'sr',srmax,'simparam',simparammax,'mparam',mparammax);
fprintf('Saved to %s \n',fname);

%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);

%specify the parameters for autmoatically rescaling the magnitudes of the
%stimuli
%srescale.avgrate=5; %median firing rate for stimuli
srescale=[];
srescale.sfactor=stimmax.srescale.sfactor;

%stimuli are selected randomly from a pool
[stimrand]=Poolstim('fname',stimfile,'stimrescale',srescale,'nstim',mparam.nstim);
simrand=SimulationBase('glm',glm,'stimchooser',stimrand,'observer',observer,'updater',updater,'simid',simid,'extra',extra);
[prand,srrand,simparamrand,mparamrand]=maxupdateobj('mparam',mobj,'simparam',simrand,'ntorun',niter);
savesim('simfile',fname,'pdata',prand,'sr',srrand,'simparam',simparamrand,'mparam',mparamrand);
fprintf('Saved to %s \n',fname);

%turn off diary
diary('off');
%open the file
%edit(dfile);
return;

