%3-30-2005
%
%maximize the information when parameter is diffusing
clear all
%close all;

setpaths;

%which methods to turn on
mon.g=0;
mon.ekf=0;
mon.batchg=0;
mon.ekf1step.on=0;
mon.ekf1step.ekffunc=@ekfposteriormax;
mon.ekf1step.onestep=0;                 %how many steps between 1 step corrections.
mon.ekfmax=0;
mon.newton.on=0;
mon.newton.maxdm=.51;
mon.newton1d.on=1;
%mon.newton1d.maxdm=.51;               %maxium step size in an iteration of newton's method
mon.newton1d.maxdm=1;               %maxium step size in an iteration of newton's method
mon.allobsrv.on=1;
methods=mon;                                    %neeeded to continue simulations
%**************************************************************************
%simulation parameters
%*************************************************************************
%reseed the random number generator
simparam.rstate=10;
randn('state',simparam.rstate);
rand('state',simparam.rstate);


%create the optimzation structure to specify the options for calls to
%optimization functions
%use default values 
%simparam.optparam=optimset();
simparam.optparam=optimset('TolX',10^-12,'TolF',10^-12);
simparam.niter=100;

%specify what glm to use
simparam.glm.fglmnc=@glm1dexp;  %normalizing function for the distribution. 
simparam.glm.fglmetamu=@etamupoiss;   %function to convert mean into canonical parameter depends on the distribution

simparam.glm.fglmmu=@glm1dexp;          %link funciton - funciton to generate the mean
simparam.glm.sampdist=@poissrnd;
%simparam.glm.fglmnc=@glm1dexp;  %normalizing function for the distribution.
%simparam.glm.fglmcf=@lincanon;      %mapping from linear input to canonical parameter
%simparam.nonlinf=@glm1dlog;
simparam.truenonlinf=@glm1dlog;   %the nonlinearity used to sample the data
%simparam.truenonlinf=@glm1dexp;   %the nonlinearity used to sample the data
simparam.sampdist=@poissrnd;            %distribution from which observations are sampled.
simparam.finfofunc=[];                                        %leave it empty so it defaults to function for poisson.

%if ~isequal(simparam.truenonlinf,@glm1dexp)
%warning('Nonlinearity for simulation is not exponential');
%end

if ~isequal(simparam.glm.fglmmu,simparam.truenonlinf)
warning('True nonlinearity and actual nonlinearity are not the same');


end
%simparam.lowmem
%tells the simulation to try to cut down on the number of results it saves
%i.e it doesn't save old means and covarance matrices

%lowmem>0- means saver lowmem th iteration
simparam.lowmem=300000;

%************************************************************************
%analysis what results/analysis to collect. 
%see analyzeposteriors.m
%***********************************************************************
aopts.getstats=0;
aopts.mckl=0;       %compute kl divergence using monte carlo

%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'nips', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
ropts.fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_misspecified.mat',datetime(2),datetime(3)));
[ropts.fname,ropts.trialindex]=seqfname(ropts.fname);
ropts.trialdate=sprintf('%2.2d_%2.2d',datetime(2),datetime(3));
trialindex=ropts.trialindex;
ropts.savedata=1;   %nonzero save data
popts.sgraphs=0;

%*************************************************************************
%save
%************************************************************************
%specify which variables get saved
%vtosave
%name of variables to save
%we provide a cell array of the basenames
%for any variable that gets saved for a special suffix
suffixes={'max','rand'};
bvars={'p','simparam','sr','methods'};
vtosave={};
for sind=1:length(suffixes)
    for bind=1:length(bvars)
        vtosave.(sprintf('%s%s',bvars{bind},suffixes{sind}))='';
    end
end

%additional variables which don't have suffix
vtosave.simparam='';
vtosave.ropts='';
vtosave.mparam='';
vtosave.popts='';
vtosave.vtosave='';
vtosave.methods='';

%options whether to compute kl divergence by brute force
popts.klbrute=0;


%options for plotting functions
if (ropts.savedata~=0)
    popts.sgraphs=1;    %save graphs
else
    popts.sgraphs=0;
end

popts.sdir=RDIR;    %directory where to save results



%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window

%krange specifies the minimum and maximum value for the coefficents
%of k in the glm. 
%dk says what stepsize to use for storing k in the posterior
%krange is used to plot the results of the gaussians
mparam.tresponse=1;

mparam.klength=25;
mparam.alength=0;

%**********************************************************************
%generate diffusion matrix
%**********************************************************************
mparam.diffusionon=0;
%mparam.diffusionon=0;   %turn diffusion off
if (mparam.diffusionon==0)
    mparam.diffusion=[];
    mparam.diffc=0;
else

tconst=10^-3;
mparam.diffusion=tconst*(eye(mparam.klength,mparam.klength)+diag((1:mparam.klength)*.1));
mparam.diffc=1-tconst*1;
end
%%

%****************************************************************
%Stimulus features
%****************************************************************
%generate ktrue from a gabor patch
%mparam.ktrue=(round(rand(mparam.klength,1)*15)+1)/10;
%if (floor(mparam.klength^.5)~=mparam.klength^.5)
%   error('Can only set ktrue to be gabor if klength is a square number');
%end
%mparam.ktrue=gabortheta(mparam.klength^.5);

%set ktrue to be a sine wave
mparam.ktrue=[cos([0:mparam.klength-1]*pi/(mparam.klength-1))]';
mparam.ktrue(:,1)=2*mparam.ktrue/max(mparam.ktrue);


if (mparam.alength>0)
    ignoreshist=0;
else
    ignoreshist=1;
end

%mparam.atrue=[];
%force spike history to be inhibitory to prevent positive feedback
%make atrue a decaying exponential
%tdecay=-mparam.alength/log(.03);
%mparam.atrue=-.01*[exp(-[0:(mparam.alength-1)]/tdecay)]';
mparam.atrue=[];        
        
%make the initial mean slightly larger than zero
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
%mparam.pinit.m=[1;0];

%mparam.pinit.c=3*eye(mparam.klength+mparam.alength)+diag(rand(1,mparam.klength)/10);
%mparam.pinit.c=3*eye(mparam.klength+mparam.alength);
%set initial covariance to equal the asymptotic covariance
%make the coordinate axes such that ktrue is all along first axis
%mparam.pinit.m=m;
%mparam.pinit.c=diag([.4165 , 3.6982*ones(1,mparam.klength-1)]);

mparam.pinit.c=3*eye(mparam.klength+mparam.alength);
%we set the magnitude constraint so that the maximum firing rate is never
%larger than a 1000 this is because the bottleneck is sampling the poison
%process so we want to limit how many spikes we have to generate
%the maximum firing rate returned by the glm is if 

if isequal(@glm1dlog,simparam.truenonlinf)
mparam.maxrate=500;

%scale the spike history coefficents so that contribution from spike
%history terms is at most the same as max contribution from stimus
%first we compute the maximum value of the linear stage
stimmax=mparam.maxrate/mparam.tresponse;
%now we compute max on spike history contribution
%stim contribution

%now we compute the magnitude constraint on the stimulus
mparam.mmag= ((stimmax)/(mparam.ktrue'*mparam.ktrue)^.5);
mparam.mmag=mparam.mmag*2;
mparam.mmag=10;
else
mparam.maxrate=500;

%scale the spike history coefficents so that contribution from spike
%history terms is at most the same as max contribution from stimus
%first we compute the maximum value of the linear stage
smax=(log(mparam.maxrate/mparam.tresponse));
%now we compute max on spike history contribution
%stim contribution
shistmax=smax/2;
stimmax=smax/2;
%now we compute the magnitude constraint on the stimulus
mparam.mmag= ((stimmax)/(mparam.ktrue'*mparam.ktrue)^.5);
mparam.mmag=mparam.mmag*2;
end


%now we compute 
simparam.normalize=mparam.mmag; %we will normalize the stimuli to be same as magnitude
                                                            %constraint
                                                            %imposed on the
                                                            %stimuli  when
                                                            %optimizing the
                                                            %stimulus
                                                           
%*****************************************************************
%simulate diffusion of the parameter
%******************************************************************
if ~isempty(mparam.diffusion)
    if ~isempty(find(mparam.diffusion~=0))
        %make sure diffusion matrix is positive semi-definit
        if ~isempty(find(eig(mparam.diffusion)<0))
            error('Diffusion matrix must be positive definite');
        end
        %generate the parameter at each iteration
        theta=zeros(mparam.klength+mparam.alength,simparam.niter);
        theta(:,1)=[mparam.ktrue;mparam.atrue];
        for index=2:simparam.niter
            theta(:,index)=mparam.diffc*theta(:,index-1)+(mvgausssamp(zeros(mparam.klength+mparam.alength,1),mparam.diffusion,1));
        end
        mparam.ktrue=theta(1:mparam.klength,:);
        mparam.atrue=theta(mparam.klength+1:end,:);
    end
end

%**************************************************************************
%Update the posteriors
%we do this twice:
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************
%********************************************************
%Update: Optimized Stimuli
%********************************************************
%set ropts.fname='' so we never call save from maxupdate or
%updateposteriors
%we will call save from this script
fropts=ropts;
fropts.fname='';
tstart=clock;


simparam.ignoreshist=ignoreshist;     %don't want to ignore the spike history terms.
simparam.randstimuli=0; %generate the stimuli randomly
[pmax,srmax,simparammax,mparammax,methodsmax]=maxupdateshist(mparam,simparam,mon,fropts);
tstop=clock;
%pmax.newton.timing.totaltime= etime(tstop,tstart); 

%get the current state of the random number generator
%make this cell arrays in case we start and stop many times
simparammax.rstart{1}=simparammax.rstate;
simparammax.rfinish{1}=randn('state');
simparammax=rmfield(simparammax,'rstate');
[rdir]=fileparts(ropts.fname);
simparammax.rdir=rdir;
simparammax.trialindex=ropts.trialindex;
%save data before performing random sampling
if (ropts.savedata~=0)
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end




%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus
%ignore the spike history components.
%set ropts.fname='' so we never call save from maxupdate or
%updateposteriors
%we will call save from this script
fropts=ropts;
fropts.fname='';
tstart=clock;
simparam.ignoreshist=1; %so we optimize without spike history terms
simparam.randstimuli=1; %generate the stimuli randomly
[prand,srrand,simparamrand,mparamrand,methodsrand]=maxupdateshist(mparam,simparam,mon,fropts);
tstop=clock;
%prand.newton.timing.totaltime= etime(tstop,tstart); 

%get the current state of the random number generator
%make this cell arrays in case we start and stop many times
simparamrand.rstart{1}=simparamrand.rstate;
simparamrand.rfinish{1}=randn('state');
simparamrand=rmfield(simparamrand,'rstate');
[rdir]=fileparts(ropts.fname);
simparamrand.rdir=rdir;
simparamrand.trialindex=ropts.trialindex;
%********************************************************
%Post Process
%********************************************************
%save data before performing random sampling
if (ropts.savedata~=0)
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end



return;

%********************************************************
%Update: maximize using old method
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus
%ignore the spike history components.
%set ropts.fname='' so we never call save from maxupdate or
%updateposteriors
%we will call save from this script
fropts=ropts;
fropts.fname='';
tstart=clock;
simparam.ignoreshist=1; %so we optimize without spike history terms
simparam.randstimuli=0; %generate the stimuli randomly
[pold,sroldrand,simparamold,mparamold,methodsold]=maxupdateshist(mparam,simparam,mon,fropts);
tstop=clock;
pold.newton.timing.totaltime= etime(tstop,tstart); 

%get the current state of the random number generator
%make this cell arrays in case we start and stop many times
simparamold.rstart{1}=simparamold.rstate;
simparamold.rfinish{1}=randn('state');
simparamold=rmfield(simparamold,'rstate');

%********************************************************
%Post Process
%********************************************************
%save data before performing random sampling
if (ropts.savedata~=0)
    %save options
    saveopts.append=1;  %append results so that we don't create multiple files 
                    %each time we save data.
    saveopts.cdir =1; %create directory if it doesn't exist
    savedata(ropts.fname,vtosave,saveopts);
    fprintf('Saved to %s \n',ropts.fname);
end










