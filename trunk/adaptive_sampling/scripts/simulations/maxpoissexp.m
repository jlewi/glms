%09-14-2007
%    use new object model
%1-28-07
%   revised to use new object model
%Script to run optimization for canonical poisson 
clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/maxnumint.out';
diary(dfile);


niter=200;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'poissexp', sprintf('%s',datestr(datetime,'yymmdd')));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('poissexp.mat'));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%****************************************************************
%Stimulus features
%****************************************************************
mparam.klength=100;
mparam.alength=0;
%mparam.ktrue=zeros(mparam.klength,1);
%mparam.ktrue=[1;1];
%set ktrue to be a sine wave
mparam.ktrue=2*[cos([0:mparam.klength-1]*pi/(mparam.klength-1))]'+.01;

mparam.atrue=[];
%set ktrue to a Gabor function
% center=mparam.klength/2;
% afreq=4*pi/mparam.klength;  %angular frequency;
% mparam.ktrue=normpdf((1:mparam.klength)',center,mparam.klength/4).*cos(([1:mparam.klength]-center)'*afreq);
% %zero out points more than 1.5pi/afreq away from the center
% mparam.ktrue(floor(center+1.5*pi/afreq):end)=0;
% mparam.ktrue(1:floor(center-1.5*pi/afreq))=0;

%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);    

%set mparam.mmag so that max # of expected spikes is along the true parameter
%the avg number of spikes is 5 
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(m*(mparam.ktrue'*mparam.ktrue)^.5)-100);
mmag=fsolve(fsetmmag,1,optim);
mparam.mmag=mmag;

mobj=MParamObj('glm',glm,'klength',mparam.klength,'alength',0,'pinit',mparam.pinit,'mmag',mmag,'glm',glm);

%**************************************************************************
%observer/active learner
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue;mparam.atrue]);

%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=Newton1d();
%updater=BatchML();
%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimmax=PoissExpMaxMI();


simmax=SimulationBase('mobj',mobj,'stimchooser',stimmax,'observer',observer,'updater',updater,'simid',simid);
simmax=update(simmax,niter);
savesim(simmax,fname);
fprintf('Saved to %s \n',fname);


%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimrand=RandStimBall('mmag',mparam.mmag,'klength',mparam.klength);
simrand=SimulationBase('mobj',mobj,'stimchooser',stimrand,'observer',observer,'updater',updater,'simid',simid);
simrand=update(simrand,niter);
savesim(simrand,fname);
fprintf('Saved to %s \n',fname);

%turn off diary
diary('off');
%open the file
edit(dfile);
return;

