%11-18-2007
%
%Run a simulation using a theta which is a matrix but which is low rank
%
clear variables
%close all;

setpathvars;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile=seqfname('/tmp/maxnumint.out');
%diary(dfile);


niter=50;

%***********************************************************
%should replace with code for GLM object.
%***********************************************************
glm=GLMModel('poisson','canon');



%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'tanspace', datestr(datetime,'yymmdd'));


%data.fname
%to save data to file
%specify where to save data
%leave blank not to save
fname=fullfile(RDIR, sprintf('lowrank.mat'));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;

mparam.lowmem=1; %how often to save the covariance matrix;

%****************************************************************
%Stimulus features
%****************************************************************
mdim=10; %dimension of the matrix
randn('state',9);
rand('state',9);
m=normmag(rand(mdim,1))*normmag(rand(mdim,1))';
mparam.ktrue=m(:);


%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);


uinit=zeros(mdim,1);
uinit(1)=1;
tinit=LowRankTanSpace('u',uinit,'v',uinit);
fprior=GaussPost('m',gettheta(tinit),'c',eye(getdimtheta(tinit)));
tparam.dimsparam=getdimsparam(tinit);
tparam.rank=1;
mparam.pinit=PostTanSpace('fullpost',fprior,'tanspacetype','LowRankTanSpace','tanparam',tparam);


%set mparam.mmag so that when 100% of energy is along the true parameter
%the avg number of spikes is 1000
optim=optimset('TolX',10^-12,'TolF',10^-12);
mparam.avgspike=500;
optim=optimset('TolX',10^-12,'TolF',10^-12);
fsetmmag=@(m)(glm.fglmmu(m*(mparam.ktrue'*mparam.ktrue)^.5)-mparam.avgspike);
[mmag, fmag, exitflag]=fsolve(fsetmmag,.1,optim);
mparam.mmag=mmag;

if (exitflag<=0)
    error('Magnitude is not properly normalized');
end

mobj=MParamObj('glm',glm,'klength',getdimtheta(tinit),'pinit',mparam.pinit,'mmag',mparam.mmag);

%**************************************************************************
%observer
%create observer which actually generates the observations
observer=GLMSimulator('model',mobj,'theta', [mparam.ktrue]);
%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
updater=TanSpaceUpdater();

%**************************************************************************
%Simulations: we create 1 simulation object for each simulation we want to
%**************************************************************************
%which simulations to initialize
infomax=true;
simrand=true;
infomaxfull=true;
sgabor=false;
%**********************************************************************
%InfoMax simulation
%********************************************************************
%run the trials but when we optimize the stimulus
if (infomax)
simid='infomax';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimmax=TanSpaceInfoMaxCanon('iterfullmax',25);
simmax=SimulationBase('stimchooser',stimmax,'observer',observer,'updater',updater,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem, 'label','Info. Max.');
savesim(simmax,fname);
fprintf('Saved to %s \n',fname);
end
%%**********************************************************
%Simulation: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus
if (simrand)
simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimrand=RandStimNorm('mmag',mparam.mmag,'klength',getklength(mobj));
urand=TanSpaceUpdater('comptanpost',true);
simrand=SimulationBase('stimchooser',stimrand,'observer',observer,'updater',urand,'mobj',mobj,'simid',simid,'lowmem',mparam.lowmem,'label','Rand');
savesim(simrand,fname);
fprintf('Saved to %s \n',fname);
end
%********************************************************************
%InfoMax: but infomax on full theta space not submanifold
%*****************************************************
if (infomaxfull)
simid='infomaxfull'
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);
stimfull=PoissExpMaxMI();
ufull=Newton1d();
mobjfull=MParamObj('glm',glm,'klength',getdimtheta(tinit),'pinit',getfullpost(mparam.pinit),'mmag',mparam.mmag);
simmfull=SimulationBase('stimchooser',stimfull,'observer',observer,'updater',ufull,'mobj',mobjfull,'simid',simid,'lowmem',mparam.lowmem,'label','Info. Max. Full');
savesim(simmfull,fname);
end
return;

