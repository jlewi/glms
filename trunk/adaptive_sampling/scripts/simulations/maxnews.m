%1-28-07
%   revised to use new object model
%Script to run optimization for canonical poisson 
function [fname]=maxnews
%clear all
%close all;

setpaths;

%**************************************************************************
%simulation parameters
%*************************************************************************
%keep track of all output
dfile='/tmp/maxnews.out';
diary(dfile);


niter=20;

%***********************************************************
%should replace with code for GLM object. 
%***********************************************************
%model glm is used for the updater and picking sitmuli
modelglm=GLMModel('binomial','canon');

%************************************************************************
%save results
%************************************************************************
%directory where results are to be saved
%this will be created if it doesn't exist
datetime=clock;

RDIR=fullfile(RESULTSDIR,'20news', sprintf('%2.2d_%2.2d',datetime(2),datetime(3)));


%data.fname
%to save data to file
%specify where to save data 
%leave blank not to save
fname=fullfile(RDIR, sprintf('%2.2d_%2.2d_news.mat',datetime(2),datetime(3)));
[fname, trialindex]=seqfname(fname);


%*************************************************************************
%Posterior/Model Parameters
%************************************************************************
%specify length of response
%This characterizes the length of window in which we look at the spike
%train after presenting the stimulus and the bin width for this window
mparam.tresponse=1;


%**************************************************************************
%Stimulus optimizer
%**************************************************************************
pname=fullfile(RESULTSDIR,'precompmi','01_25_logisticobj_002.mat'); %file containing the precomputed objective function for logistic regression.
%docfile=fullfile(RESULTSDIR,'20news','motor_windows_d200.txt');
%docfile=fullfile(RESULTSDIR,'20news','motor_windows_d100wc.txt');
docfile=fullfile(RESULTSDIR,'20news','allgroups_d.mat');
classname='comp';
stimmax=News20Stim('fname',pname,'docfile',docfile,'nrand',139,'classname','comp');

%to pick the stimuli randomly from the training set just set nrand to inf
stimrand=News20Stim('fname',pname,'docfile',docfile,'nrand',inf,'classname','comp');
%****************************************************************
%Stimulus features
%****************************************************************
%we don't specify these values because we are using a training set.
mparam.klength=get(stimmax,'nwords');
mparam.alength=0;
%ktrue has no impace for these sims.
%we set to NaN's of length klength to get pass error checking of ModelParam
mparam.ktrue=zeros(mparam.klength,1);


%**************************************************************************
%initialize the posterior
%**************************************************************************
%make the initial mean slightly larger than zero
randn('state',9);
rand('state',9);
mparam.pinit.m=(round(rand(mparam.klength+mparam.alength,1)*10)+1)/1000;;
mparam.pinit.c=3*eye(mparam.klength+mparam.alength);    

%magnitude constraint is irrelevant because we
%are selecting stimulus from a pool of stimuli
%set it to 1 because of error checking in MParamObj
mmag=1;


mparam=MParamObj('ktrue',mparam.ktrue,'pinit',mparam.pinit,'mmag',mmag);


%**************************************************************************
%Specify how we want to update the posterior
%**************************************************************************
%*
updater=BatchML();

%**************************************************************************
%SImulations: we create 1 simulation object for each simulation we want to
%               run
%      1) Using optimized stimuli
%      2) Using Random Stimuli
%**************************************************************************

simid='max';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);

simmax=SimPoolBased('glm',modelglm,'stimchooser',stimmax,'updater',updater,'simid',simid);
[pmax,srmax,simparammax,mparammax]=maxupdateobj('mparam',mparam,'simparam',simmax,'ntorun',niter);
savesim('simfile',fname,'pdata',pmax,'sr',srmax,'simparam',simparammax,'mparam',mparammax);
fprintf('Saved to %s \n',fname);

%********************************************************
%Update: random Stimuli
%********************************************************
%**************************************************************************
%run the trials but when we optimize the stimulus

simid='rand';
%reseed the random number generator
rstate=10;
randn('state',rstate);
rand('state',rstate);


simrand=SimPoolBased('glm',modelglm,'stimchooser',stimrand,'updater',updater,'simid',simid);
[prand,srrand,simparamrand,mparamrand]=maxupdateobj('mparam',mparam,'simparam',simrand,'ntorun',niter);
savesim('simfile',fname,'pdata',prand,'sr',srrand,'simparam',simparamrand,'mparam',mparamrand);
fprintf('Saved to %s \n',fname);

%turn off diary
diary('off');
%open the file
edit(dfile);
return;

