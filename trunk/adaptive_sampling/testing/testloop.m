%scrap code
npts=100;       %number of pts to use
lambdainit=zeros(1,length(eigd)+1);
for eindex=1:length(eigd)-1
    %make sure eigenvalues are not the same
  
    if (abs(eigd(eindex+1))-abs(eigd(eindex))~=0)
    dlpts=(eigd(eindex+1)-eigd(eindex))/(npts+1);
    %leave out the eigen value b\c its singular there
    pts=eigd(eindex)+dlpts:dlpts:eigd(eindex+1)-dlpts;
    [fisherinfo]=fisherlambda(pg,pts);
    [fmax,ind]=max(fisherinfo);
   %maxe sure the maximum is valid
    lambdainit(eindex)=pts(ind(1));
    else
        %set lambdainit(eindex) to Nan so we can remove it later
        lambdainit(eindex)=NaN;
    end
    
    if (length(lambdainit)>25)
        keyboard
    end
end