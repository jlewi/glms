%test function for seeing how fsolve works
function [fval, fgrad]=ffsolve(x);

fval=2*x-3*x^2;
fgrad=2-6*x;
