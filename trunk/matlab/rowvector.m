%function x=row(x,fname)
%       x - vector to ensure its column vector
%       fname - optional name of calling function
%               used to print an error message if needed
%       vname - optional name of variable we're checking
%Explanation: Verifies x is a vector and then makes sure its a row
%vector. Really only works for 2dimensional vectors/matrices, higher
%dimensions will cause problems.

function x=rowvector(x,fname,vname)
    %make sure nonsingleton dimensions are 1
    s=size(x);
    ss=sort(s);
    
    if (ss(end-1)>1)
        if exist('fname','var')
            if exist('vname','var')
                error(sprintf('%s : %s is not a vector',fname,vname));
            else
            error(sprintf('%s : x is not a vector',fname));
            end
        else
            error('x is not a vector');
        end
    end
    
    if (s(2)==1)
        x=x';
    end
    
        