%function obj=setat(obj,index,val)
%	 obj=DynArray object
%    index = index or array of indexes of the element to set
%    val = the value to set it at
%Return value: 
%	 obj= the modified object 
%
%    if index exceeds the current length of the array then we automatically
%    grow the array. All new entries are initialized to nan
function obj=setat(obj,index,val)

    lastindex=max(index);
    obj=grow(obj,lastindex);
    
    obj.data(index)=val;
    
    if (lastindex>obj.length)
        obj.length=lastindex;
    end