%function obj=grow(obj,index)
%   index - index of an element we want to set
%
%Explanation: This function determines if we need to grow the array in
%order to set the element at position index and grows the array if
%necessary
%
function obj=grow(obj,index)

datalength=getdatalength(obj);

if (index>datalength)
       %grow the array  
       dlength=max(datalength*obj.grow,(index-datalength));

    
       data=nan*zeros(datalength+dlength,1);

       data(1:datalength)=obj.data;
       
        obj.data=data;

    end