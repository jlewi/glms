%function [cid,params]=constructid(var,con)
%   var - a cell array of field value pairs
%   con - a structure array describing each possible constructor
%       .rparams - cell array containing names of required parameters
%           -EMPTY - if this is empty then all constructors match
%
%
%Return Value:
%   cid - array of cfun value of the element in con whose required parameters
%         were supplied
%       - nan - no constructor matched
%   params - conversion of var to a structure field array
%
%Explanation: This function is used by my objects to implement multile
%constructors.
%
%Revisions:
%  11-03-2008
%       return -1 instead of nan to indicate no much (this is because
%           switch cannot handle -1)
%       return 0 - constructor is load obj constructor
%                - possibilities are:
%                 1. No input arguments
%                 2. A structure with no fields 
%       
%  10-17-2008
%       - Check if the structure passed in has no fields. If it does check
%       for an empty constructor
%  09-23-2008
%       -Don't match the blank constructor if any parameters are passed in.
%       -Don't require con.cfun anymore
%  07-31-2008
%       Allow for var to be an empty structure if there's a constructor
%       which take no input arguments
function [cind,params]=constructid(var,con)


if (numel(var)>1)
    params=parseinputs(var);
else
    %var should be a structure of named value pairs
    if (iscell(var) && ~isempty(var))
        var=var{1};
    end
    %check if its empty check for an empty constructor
    %if a structure has no fields isempty returns false
    %which is why we need the second check
    if (isempty(var) || isempty(fieldnames(var)))
        for ind=1:length(con)
            if isempty(con(ind).rparams)
                cind=ind;
                params=[];
                return;
            end
        end
    end
    %if ites empty
    if ~isstruct(var)
        error('expected a structure');
    end
    params=var;
end
%**********************************************
%determine which constructor was called
%Check the following:
%   1. all required parameters for one constructor
%   2. we can resolve the constructor
pnames=fieldnames(params);

%loop through each constructor and check if it matches
cind=[];  %index for constructor for which required parameters are supplied
for j=1:length(con)
    %only match this constructor if its not empty
    if (length(con(j).rparams)>0)
        ismatch=1;
        for f=1:length(con(j).rparams)
            %check if  parameter was passed in
            if isempty(strmatch(con(j).rparams{f},pnames))
                ismatch=0;
                break;
            end
        end
        %if constructor is blank; i.e no parameters required then it matches
        if (ismatch==1||isempty(con(j).rparams))
            cind=[cind j];
        end
    end
end

if isempty(cind)
    cind=nan;
elseif (length(cind)>1)
    %    error('Constructor:unknown','The calling syntax matches more than one possible constructor');
end

%**************************************************************************
%Parse the input arguments-
%the input arguments are stored in params.pname=val;
%***********************************
%This converts varargin from an array of ('key',value,...) pairs into a
%structure array
%       params.('key')=val
%
%Customization: Only customization required is if you want to allow mutiple
%keys to be used for the same field. i.e fname, filename etc..
function [params]=parseinputs(vars)

for j=1:2:length(vars)
    params.(vars{j})=vars{j+1};
end
