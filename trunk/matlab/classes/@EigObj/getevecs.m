%function [evecs]=geteig(eobj,ind)
%   eobj - the eigendecomposition
%   ind  - indexes of the eigenvectors to get
%Explanation:
%  This is a faster way to access the eigenvectors as opposed to using
%  eobj.eigd. Thats because eobj.eigd has to do string comparisons to
%  evaluate which field to get.
%
function [evecs]=geteevecs(eobj,ind)
    if ~exist('ind','var')
        evecs=eobj.evecs;
    else
        evecs=eobj.evecs(:,ind);
    end