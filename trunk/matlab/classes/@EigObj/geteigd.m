%function [eigd]=geteig(eobj,ind)
%   eobj - the eigendecomposition
%   ind  - indexes of the eigenvalues to get
%Explanation:
%  This is a faster way to access the eigenvalues as opposed to using
%  eobj.eigd. Thats because eobj.eigd has to do string comparisons to
%  evaluate which field to get.
%
function [eigd]=geteig(eobj,ind)
    if ~exist('ind','var')
        eigd=eobj.eigd;
    else
        eigd=eobj.eigd(ind);
    end