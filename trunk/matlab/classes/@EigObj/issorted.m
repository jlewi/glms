%function is=issorted(eobj,sdir)
%       eobj - EigObj
%       sdir - direction
%            +1 -ascending
%            -1 - descending
%Return:
%   True/false if sorted in the direction specified
%   
function is=issorted(eobj,sdir)
    sdir=sign(sdir);
    if (sdir==0)
        error('sdir should be +1 or -1');
    else
       if (eobj.esorted==sdir)
           is=true;
       else
           is=false;
       end
    end
        