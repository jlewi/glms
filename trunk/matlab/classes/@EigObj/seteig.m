%function seteig(obj,evecs,eigd)
%       evecs - eigenvectors
%       eigd  - eigenvalues
%
%Explanation: sets the eigenvalues and eigenvectors
%   Performs following checking:
%       1. dimensionality is consistent
%       2. A linear time search to see if eigenvalues are sorted
function obj=seteig(obj,evecs,eigd)
    obj.evecs=evecs;
    obj.eigd=eigd;    
    obj.esorted=0;
    if ~(isvector(eigd))
        obj.eigd=diag(eigd);
    end
    
    if (length(eigd)~=size(evecs,1))
        error('Number of eigenvalues and eigenvectors do not match');
    end
    if (size(evecs,1)~=size(evecs,2))
        error('Eigenvectors must be a square matrix');
    end
    if (length(size(evecs))>2)
        error('Eigenvectors needs to be a 2-d matrix');
    end
    
    %if its just a scalar then its already sorted
    if (length(obj.eigd)==1)
        obj.esorted=1;
        return;
    end
    %sort direction
    %find the first nonrepeated eigenvalue
    ind=1;
    while(obj.eigd(ind)==obj.eigd(ind+1))
       ind=ind+1;
       if (ind==length(obj.eigd))
           break;
       end
    end
    
    if (ind==length(obj.eigd))
        %eigenvalues are all the same
        %so just set sort to ascending
        obj.esorted=1;
    else        
        sdir=sign(obj.eigd(ind+1)-obj.eigd(ind));
    
        if (sdir>0)
            %eigenvalues appear to be sorted in ascending order
            obj.esorted=1;
            for index=1:length(obj.eigd)-1
                if (obj.eigd(index)>obj.eigd(index+1))
                    obj.esorted=0;
                    break;
                end
            end
        else
            %eigenvalues appear to be sorted in descending order
            obj.esorted=-1;
            for index=1:length(obj.eigd)-1
                if (obj.eigd(index)<obj.eigd(index+1))
                    obj.esorted=0;
                    break;
                end
            end
        end
    end
    
   
    

