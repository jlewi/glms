%function [eup, opt]=rankOneEigUpdate(eobj,vup,rho)
%   eobj - EigObj for the eigendecomposition to update
%   vup  - nx1 vector update
%        - This should be specified in the orginal coordinate basis
%           i.e it is not the porjection onto the eigenvectors
%   rho - scaling factor allows the sign of the update to be set
%
% Return Value:
%     eup   - updated eigendecomposition
%     opt   - optionally info
%       ndeflate        - number of deflated eigenvalues
%                       - deflation is number of entries of z which are
%                              zero
%                       -this is computed after rotating the perturbation
%                        so that for repeated eigenvalues only 1
%                        eigenvector is correlated with the perturbation
%
%       rho - value of rho
%       eigtime - time of update
%
% Explanation: computes a rank one update to the eigenvalue problem.
%   That is it computs the eigenvectors and values of
%   A+rho*vup*vup'
%   Q( d+ rho*zz')Q'= QdQ'+rho*Qzz'q=A+uu'
%   A is the original matrix, u is the original eigenvector
%
%  Does this by calling a mexfunction which calls a function in the LAPACK
%  library which implements the Gu and Eisenstat algorithm for the rank 1-d
%  update of the eigenvalue problem.
%
%
%Future Improvements: COuld significantly reduce the amount of memory
% There are several times when I copy matrix to new matrix in rearranging
% or otherwise manipulating the matrix rather than doing operation in
% place.
% I did this for debugging purposes
%
% Implementation details
% dlaed4.f requires
%       z - to be normalized
%       rho - to be positive
%       d  - to be sorted in ascending order
%
%       All but d being sorted is handled by this function user doesn't
%       need to worry about the other requirements
%
% Revisions
%    10-28-2008 - If the number of non-deflated eigenvalues is zero
%           then the updated matrix = the original matrix
%           this can happen if rho=0;
%     07-14-2008 -
%           we set threshold to the tolerance set in Dongarra87
% 05-18-2006  - checks d is sorted and if not sorts them
%
% 11-25-2007
%   Bug Fix - at end of function I needed to do the resorting if
%     ndeflate>=1 instead of ndeflate>1. Bug was preventing eigenvalues
%     from being properly sorted if a single eigenvalue was deflated.
%Revision: 1614
%   Speedup deflation. Instead of computing a permutation matrix, just
%   store a permuation array which gives the new order of the columns
%
%       use my c-code for rotating the eigenvectors so that for repeated
%       eigenvalues only 1 eigenvector is correlated with the perturbation
%Revision: 1462  - made this a member of EigObj
%       input is now the rank 1 modification not the projection on the
%       eigenvectors
function [eup,opt]=rankOneEigUpdate(eobj,vup,rho)

%declare a persistent variable to keep track of how often this warning
%is printed
persistent zwarn;
zwarninc=0;



%check if the compiled version of the rank 1 code exists
%if not print a warning and use the svd
if (exist('mexDeflate')~=3)
    %if (true)
    warning('EigObj:rankOneEigUpdate code does not exist (i.e not compiled) using svd instead. \n SVD has proved troublesome in past.');
    eup=EigObj('matrix',getmatrix(eobj)+rho*vup*vup');
    eup=sorteig(eup,1);
    opt=[];
else

    n=length(eobj.eigd);

    %$Revision$
    %preallocate gmat so that it doesn't grow inside loop
    %this saves a lot of time
    gmat=cell(1,n);


    %dlaed4.f requires d to be sorted in ascending order
    %check its sorted if not then sort d
    if (eobj.esorted==0 || eobj.esorted==-1)
        %resort the eigenvalues
        %if not sorted at all print warning
        if (eobj.esorted==0)
            warning('Eigenvalues were not sorted. This is suboptimal.');
            %keyboard
        end
        eobj=sorteig(eobj,1);

    end

    %coode has problems with rho=0
    %but if rho=0 eigendecomp is unchanged
    if (rho==0)
        eup=eobj;
        opt=[];
        opt.ndeflate=n;
        opt.eigtime=0;
        opt.rho=rho;
        return;
    end
    sdir=1; %sorted in ascending order
    tic;
    %normalize vup and pull its power (b\c update is vup*vup') into
    %rho
    vpow=vup'*vup;

    %12-20-2007
    %check if vpow=0
    %if it is eigendecomposition is unchanged
    if (vpow==0)
        eup=eobj;
        opt=[];
        opt.ndeflate=n;
        opt.eigtime=0;
        opt.rho=rho;
        return;
    end

    vup=vup/vpow^.5;
    rho=rho*vpow;
    srho=sign(rho);

    %dlaed4.f requires rho to be positive
    %if rho is negative multiply the eigenvalues by -1

    %srho - stores the direction in which the eigendecomposition is now
    %sorted
    %srho = -1 sorted in descending order
    %srho = 1 sorted in ascending order
    %we will use this to make sure we call dlaed4.f in order of increasing
    %eigenvalues


    if (srho<0)
        rho=abs(rho);
        eobj.eigd=-1*eobj.eigd;
        %this flips the direction of the sorting
        eobj.esorted=-1*eobj.esorted;
    end

    %project vup onto eigenvectors
    z=eobj.evecs'*vup;

    %we need to make the components of z positive
    %we do this by multiplying the corresponding eigenvectors of z by -1
    ind=find(z<0);
    for k=ind
        eobj.evecs(:,k)=-1*eobj.evecs(:,k);
        z(k)=-1*z(k);
    end


    opt.tpreproc=toc;

    %*******************************************************************
    %threshold:
    %in Dongarra87 threshold is set to the machine precision for l2norm
    %*******************************************************************
    %we multiply by 1/2 because eps is twice machine precision
    threshold=1/2*eps(l2norm(eobj));

    %**********************************************************************
    %*deflation
    %**********************************************************************
    %start deflation timing
    tic;
    %1. Find all entries di=di+1
    %2. Use givens rotations to zero out these entries of z
    [defeig defevecs defz ndeflate]=mexDeflate(eobj.eigd,eobj.evecs,z,rho,threshold,eobj.esorted);

    opt.tdeflate=toc;





    %**********************************************************************
    %find the undeflated eigenvalues and eigenvectors
    %by using gu an eisenstat
    %*********************************************************************
    %k is number of nondeflated values
    k=n-ndeflate;

    %check if the number of non-deflated eigenvalues is greater than zero.
    %If the number of non-deflated eigenvalues is zero (i.e rho=0) then the
    %updated eigendecomposition equals the original eigendecomp.
    if (k>0)
        %dk,zk,qf should be sorted in ascending order
        dk=defeig(1:k);
        zk=defz(1:k);

        %save time so far as deflate time
        opt.tdeflate=toc;
        tic;
        [eigd,delta]=mexSolveSecular(dk,rho,zk);
        %save time as secular time
        %as this is the amount of time required to solve the secular equation
        opt.tsecular=toc;

        tic;

        %for debugging this is the secular equation from Li94
        %secular equation assuming deflation
        %fsec=@(lam)(1/rho+sum(zk.^2./(dk-lam)));
        %secular equation if no deflation
        %fsec=@(lam)(1/rho+sum(zkdeflate.^2./(dkdeflate-lam)));
        %delta(i,j)= d(j)-eigd(i)
        %now we compute the approximate eigenvectors using the formulas in Gu
        %this is just the eigenvectors of the nondeflated values
        %and Eisenstat 94
        %first we compute zapprox which is the z vector for which the eigenvalues
        %are the true eigenvalues
        zapprox=zeros(k,1);


        %********************************************************
        %compute zapprox
        %***********************************************************
        %there are some issues with making the computation of zapprox
        %stable
        %each Zi is the result of dividing two products of n terms
        %depending on how we order these multiplications or divisions
        %the computation may not be stable.
        %For example if we do multiplications in numerator and then denominator
        %and then do division it tends to be unstable.
        %doing the divisions first should be more stable (see my notes
        %5-17-2006) for more detail
        %
        %We can always rescale zapprox to try to avoid these numerical issues
        %because we end up normalizaing zapprox anyway

        %because of the scaling I do to avoid multiplying by small numbers
        %zapprox won't equal zdeflate but will be a scaled version of z.
        %to compare them you could probably normalize both to 1.

        %compute the eigenvectors
        %if k (number of nondeflated eigenvalues) <= 2 then delta (from
        %dlaed4.f) contains the eigenvectors
        %if k>2 then delta(j,i)=d(j)-lambda(i) and we need to compute zapprox
        %    and then compute the eigenvectors

        if k<=2
            zapprox=1;
            evecs=delta;
        else

            %I'm not sure what dlaed4.f actually computes for delta so
            %compute the terms myself

            %denom(j,i)=dk(j)-dk(i)
            denom=ones(k,1)*dk'-dk*ones(1,k);

            %set the diagonal elements of denom to 1
            dind=([0:k-1])*k+[1:k];
            denom(dind)=1;

            %compute the approximate delta eqn 5 in Gu94
            %zapprox ~ zk up to A SCALING FACTOR
            %b\c we don't bother to normalize zapprox since we have to
            %normalize the eigenvectors
            zapprox(:,1)=prod(-delta./denom,2).^.5;
            evecs=zapprox*ones(1,k)./delta;
            mag=(sum(evecs.^2,1)).^(-.5);
            evecs=(ones(k,1)*mag).*evecs;

        end
        %if we flipped the order of the eigenvectors before calling
        %deflate then now we flip them back
        if (srho==-1)
            % eigd=eigd(end:-1:1);
            % evecs=evecs(:,end:-1:1);
        end

        %if you compare the eigenvalues to using eig
        %don't forget to include rho
        %also compare to zk not zx
        %zk will be a scaled version of zapprox

        %debug - check accuracy of eigenvalues and vectors
        %need to normalize zapproxdp for the following to be correct
        %mag=zapproxdp'*zapproxdp;
        %zapproxdp=zapproxdp/mag^.5;
        %mse=diag(dk)+rho*zapproxdp*zapproxdp'-evecsdp*diag(eigd)*evecsdp';
        %mse=sum(sum(mse.^2));
        %if (mse>10^-10)
        % keyboard;
        %end

        %**********************************************************************
        %eigd, evecs, are kx1 and kxk and they store the eigenvectors
        %of the 1:k elements of the diagonal matrix and the corresponding
        %eigenvalues
        %the remaining eigenvalues are ddeflate(k+1:end) and the eigenvectors
        %are just the elements of the identity matrix
        %we need to multiply by Qf by [ evecs 0; 0 I]
        %we want to take advantage of the block diagonal structure to do this
        %efficently
        evecsall=zeros(n,n);
        evecsall(:,1:k)=defevecs(:,1:k)*evecs;
        evecsall(:,k+1:end)=defevecs(:,k+1:end);


        eigdall=zeros(n,1);
        eigdall(1:k)=eigd;
        eigdall(k+1:end)=defeig(k+1:end);
        opt.tundeflate=toc;
        tic
        %**********************************************************************
        %now we want to resort the eigenvalues and eigenvectors
        %so that all of the eigenvalues are sorted rather than having the
        %sorted nondeflated eigenvalues in ascending order and then
        %the sorted deflated eigenvalues in descending order
        %We can do this efficently because of the interlacing property
        % d1 <lambda1 <d2< lambda2 ... [Gu94]
        %evsort=zeros(n,n);
        edsort=zeros(n,1);

        %we have to reinsert into our sorted listed ndeflated eigenvalues
        %we will create a 1xn vector sinds which stores
        %the order in which we take rows from evecsall and elements from eigd
        %in order to them in sorted order
        %we have two sorted lists kind
        %and deflateind and we bassically need to merge the lists.
        %when the two lists are equal deflateind goes first
        sinds=zeros(1,n);

        %To this in sorted order we need to rearrange the last n-k values
        %the i=n-k:n the eigenvalue should be inserted after element
        %deflateind(i-n+k)-1
        %
        %k - number nondeflated eigenvalues
        %This amounts to merging two lists
        %   List 1) eigdall(1:k) sorted in ascending order
        %   List 2) eigdall(k+1:end) - sorted in descending order
        %
        % we basically perform a merge operation as used in merge sort
        % there might be more  a slightly more efficient way to do this
        % using deflateind


        %The first k=n-ndfelate=#nondeflated eigenvalues
        %   are sorted in ascending order
        %The last ndeflate entries are sorted
        %   in reverse order
        %
        %we will sort everything in ascending order
        %i - smallest of nondeflated eigenvalues
        %j - smallest of deflated eigenvalues
        %we only need to do this if there was deflation
        if (ndeflate>=1)
            %  warning('this code has not been debugged')
            %     if (srho==1)
            i=1;
            j=n;
            %     else
            %         i=n-ndeflate;
            %         j=i+1;
            %     end
            for count=1:n
                if (eigdall(i)<eigdall(j))
                    sinds(count)=i;
                    %i pts to next element in asecending order
                    %i=i+srho;
                    i=i+1;
                else
                    sinds(count)=j;
                    %j=j-srho;
                    j=j-1;
                end
                %check if we have reached the end of the lists
                if ( i>k || i<1)
                    %we have merged all nondeflated eigenvalues
                    % if (srho==1)
                    sinds(count+1:end)=j:-1:k+1;
                    %else
                    %    sinds(count+1:end)=j:1:n;
                    %end
                    break;
                end
                if ((j<k+1) || (j>n))
                    %fprintf('i=%d, k=%d, count=%d \n',i,k,count);
                    % if (srho==1)
                    sinds(count+1:end)=i:k;
                    % else
                    %     sinds(count+1:end)=i:-1:1;
                    % end
                    break
                end

            end


            %now we select them in sorted order
            evsort=evecsall(:,sinds);
            edsort(:,1)=eigdall(sinds);
        else
            %now we select them in sorted order
            evsort=evecsall;
            edsort(:,1)=eigdall;
        end





        %stop timing - last timing block is for undeflating
        opt.tresort=toc;

    else
        %the number of nondeflated eigenvalues is zero
        %so the eigendecomposition is unchanged

        evsort=eobj.evecs;
        edsort=eobj.eigd;

        opt.tsecular=0;
        opt.tundeflate=0;
        opt.tresort=0;
    end

    %if rho was originally negative then we multiplied the eigenvalues by
    %-1
    %to undo this we multiply by -1
    if (srho<0)
        %        edsort(:,1)=-1;
        edsort(:,1)=-1*edsort;
        %edsort(:,1)=-1*edsort(end:-1:1);
        %evsort=evsort(:,[n:-1:1]);
    end

    %create object to return
    eup=EigObj('evecs',evsort,'eigd',edsort);
    opt.eigtime=opt.tdeflate +opt.tpreproc+opt.tsecular+opt.tundeflate+opt.tresort;
    opt.rho=srho*rho;
    opt.ndeflate=ndeflate;

end