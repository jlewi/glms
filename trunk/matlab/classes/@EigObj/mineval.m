%function [mval]=mineval(eobj) 
%       eobj - EigObj
%Return:
%   mval    - maximum eigenvalue
%   mind    - index of minimum eigenvalue
%
% Explanation: returns the minimum eigenvalue.
% Checks the eig structure to see if its sorted. If its sorted it returns
% the eigenvalue without having to do a search.
function [mval mind]=mineval(edec)
mind=0;

    if (edec.esorted==1)
        mind=1;
    elseif (edec.esorted==-1)
        mind=length(edec.eigd);
    else
    [mval mind]=min(edec.eigd);
    end
    [mval]= edec.eigd(mind);
