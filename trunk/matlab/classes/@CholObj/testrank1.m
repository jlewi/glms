%function testrank1(cobj)
%       cobj - blank cholObj object
%
%Explanation: This is a test harness for Mathias Seeger's rank 1 update
%code to make sure I properly call it
function testrank1(cobj)
iscorrect=true;

nmats=2;   %number of matrices to generate
d=2;       %dimensionality of matrices
sigma=2;
for mi=1:nmats
   %generate a random symetric matrix 
   matrix=normrnd(zeros(d,d),sigma*ones(d,d));
   matrix=matrix*matrix';
   
   %random update
   vup=normrnd(zeros(d,1),sigma*ones(d,1));
   %random value for rho
   rho=normrnd(0,sigma);

   %srho=sign(binornd(1,.5)-.5);
   
   corig=CholObj('matrix',matrix);
   
   %compute rank 1 update using 
   cup=rankOneUpdate(corig,vup,rho);
   
   %verify the update by computing the matrices
   uupmat=cup.l*cup.l';
   utrue=corig.l*corig.l'+rho*vup*vup';
   if any((uupmat-utrue)>10^-8)
       error('Rank 1 update is not correct');
   end
end

if (iscorrect)
   fprintf('Success: All rank 1 updates were accurate \n'); 
end