%function [cup, opt]=rankOneUpdate(cobj,vup,rho)
%   cobj - CholObj to update
%   vup  - nx1 vector update
%        - This should be specified in the orginal coordinate basis
%           i.e it is not the porjection onto the eigenvectors
%   rho - scaling factor allows the sign of the update to be set
%       
% Return Value:
%     cup   - updated cholesky decomposition
%     opt   - optionally info
%       ctime - time of update
%
% Explanation: computes a rank one update to the cholesky decomposition using Mathias Seeger's code,
%       http://www.kyb.tuebingen.mpg.de/bs/people/seeger/software.html#essential.
%   That is it computs the cholesky decomposition of
%   A+rho*vup*vup'
%   A is the original matrix
%
% $Revision$ : created but it DOES NOT WORK properly
function [cup,opt]=rankOneUpdate(cobj,vup,rho)
     %since the cholesky code edits the l in place we need to copy 
    %cobj.l before passing it in.
    cup=cobj;
    %to force matlab to make a copy we change one of the elements
    e1=cup.l(1);
    cup.l(1)=nan;
    cup.l(1)=e1;
    
    %lfact=zeros(size(cobj.l));
    %lfact(:)=cobj.l;

    tic;

    n=size(cobj.l,1);
    
    %allocate workspace
    cvec=zeros(n,1); 
    svec=zeros(n,1);
    wkvec=zeros(3*n,1);
   
    if (rho>0)
        vec=vup*rho^.5;
        
          b=randn(3*n,n); y=randn(3*n,1);
          z=b/cup.l';
%        choluprk1({cup.l,[1 1 n n],'L '},vec,cvec,svec,wkvec,z,y);
                choluprk1({cup.l,[1 1 n n],'L '},vec,cvec,svec,wkvec,z,y);
    else
        vec=vup*abs(rho)^.5;
        %double(1==2) - this is just a trick to get the numeric value of
        %false);
        b=randn(3*n,n); y=randn(3*n,1);
        z=(b+y*vec')/cup.l';
      isp=double(rand>=0.5);
%       choldnrk1({cup.l,[1 1 n n],'L '},vec,cvec,svec,wkvec, isp,z,y); 
             choldnrk1({cup.l,[1 1 n n],'L '},vec,cvec,svec,wkvec, 0); 
    end
    %cobj.l=lfact;
    opt.choltime=toc;
