%function xfunc(fname)
%   fname - name of a function whose path may not be on the matlab path
%
%Explanation: executes this script by switching to that directory and then
%cding back
function varargout=xfunc(fname)

if isa(fname,'FilePath')
[fdir, fname, fext]=fileparts(getpath(fname));
else
[fdir, fname, fext]=fileparts(fname);
end
odir=pwd;

cd(fdir);

try
    fhand=str2func(fname);
    varargout{1}=fhand();
catch e
    
    %if we threw an error still cd back to odir
        cd(odir);
%    if strcmp(e.identifier,'MATLAB:undefinedFunction')
        
    throw(e);
%    error(e);
end
    
cd(odir);
