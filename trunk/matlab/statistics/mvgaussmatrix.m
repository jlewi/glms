%function [p,x]=mvgaussmatrix(krange,dk,mu,sigma);
%   krange - range over which to compute the values
%   dk     - sampling interval
%   mu     - mean of the multivariate gaussian
%   sigma  - covariance matrix.
%
% Return Value:
%   X- n x d
%       each row is a different point at which gaussian is evaluated at
% Explanation - 
%   This function constructs a matrix which contains the values
%   of a multivariate gaussian. That is
%   p(i,j,...,k)=mvgauss(x,mu,sigma)
%       x  is a vector of size d
%   where 
%   x(1)=[krange(1,1)+(i-1)*dk]
%   x(2)=[krange(1,1)+(j-1)*dk]
%    .
%    .
%   x(k)=[krange(1,1)+(k-1)*dk]
%
% Explanation:
%   Warning: this returns the pdf sampled at the desired locations
%       not the probability. You still need to handle the continuous to
%       discrete conversion to get the probability.
function [p,kvals]=mvgaussmatrix(krange,dk,mu,sigma);
%num dims is number of dimensions
ndims=length(mu);
if (size(sigma,1)~=ndims)
    error('Sigma wrong size');
end

%if krange and dk are not the same as number of dims then repeat them
if (size(krange,1)<ndims)
    krange=ones(ndims,1)*krange(1,:);
end
if (size(dk,1)<ndims)
    dk=ones(ndims,1)*dk;
end
%number of points at which we compute k
%numkpts is the number of pts along each dimension

numkpts=diff(krange,1,2)./dk+1;
numkpts';
numkpts=ceil(numkpts);
krange(:,2)=krange(:,1)+(numkpts-1).*dk;



%dimensionality
%dims=size(mu,1);

%construct the initial prior from the gaussian
%for each dimension construct a matrix which represents value
%of that location in K space
%So if it was 2x2 matrix we would have 2 2x2 matrices x and y
%one which stored the x coordinate and one which stored the y
%kcoor=cell(1,dims);

%kvals is a 2 dimensional matrix
%each row corresponds to a point at which we will evaluate the gaussian
kvals=ones(prod(numkpts),ndims);

%rowindexes
rindexes=[1:prod(numkpts)]';

%dimmatrix
%each element stores number of pts along the dimension associated with that
%column
rsum=zeros(prod(numkpts),ndims);
%dimmatrix=ones(prod(numkpts),ndims)*numkpts;
kvals(:,ndims)=mod(rindexes-1,numkpts(ndims))+1;
rsum(:,ndims)=kvals(:,ndims);


for dindex=1:ndims
    kvals(:,dindex)=floor((rindexes-1)/prod(numkpts(dindex+1:end)));
    rindexes=rindexes-kvals(:,dindex)*prod(numkpts(dindex+1:end));
    kvals(:,dindex)=kvals(:,dindex)+1;
end

%now set the value of the pots
kvals=(kvals-1).*(ones(prod(numkpts,1),1)*dk')+ones(prod(numkpts),1)*(krange(:,1))';
    
    

%now we just evalute the gaussian on these points
p=mvgauss(kvals',mu,sigma);

%now we reshape it to get the initial values
if ndims>1
    p=reshape(p,numkpts');
else
    %make it a column vector if needed
    if (size(p,2)>1)
        p=p';
    end
    
end
%x=kvals;