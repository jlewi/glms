%function [ll]= hpoissonll(nevents, rate, twin)
%       nevents   - number of spikes that occur in window twin
%                 1xm
%           -rowvector for spike train
%           -each m/column is a different trial
%       rate - the rate of the homogenous poisson process 
%            - n x1
%            - each row corresponds to a different rate under which
%              to compute the likelihood
%       twin - length of the window
%
%Return Value:
%    ll = log likelihood of the data under a poisson process
%       = n xm
%           -each row is likelihood under a different rate
%
%Explanation: Computes the Log Likelihood of the data under an homogenous
%poisson process.
function ll= hpoissonll(nevents,rate,twin)
    ll=zeros(length(rate),size(nevents,2));
    for index=1:size(nevents,2)
        ll(:,index)=-rate*twin+nevents(1,index)*log(rate*twin);
        ll(:,index)=ll(:,index)-sum(log([1:nevents(index)]));
    end
    
    