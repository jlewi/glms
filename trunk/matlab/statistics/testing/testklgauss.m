ktrue=[.75; .75];
p.m=[1;.5];
%p.c=[ 1 2; 2 3];
evec=orth(rand(2,2));
eigd=[.5 1];
p.c=evec*diag(eigd)*evec';

q.m=[.5;1];
%q.c=[2 .5; .5 3];
eigd=[2;3];
evec=orth(rand(2,2));
q.c=evec*diag(eigd)*evec';

kl=kldistgauss(p,q)

%use a discrete appoximation to estimate the kl distance
%pts on which to evaluate the pdfs
theta=[];
df=15;
theta.t1=[(ktrue(1)-df:.005:ktrue(1)+df)]';
theta.t2=[(ktrue(2)-df:.005:ktrue(2)+df)];

theta.t1=theta.t1*ones(1,length(theta.t2));
theta.t2=ones(length(theta.t1),1)*theta.t2;

theta.t1=reshape(theta.t1,1,numel(theta.t1)); 
theta.t2=reshape(theta.t2,1,numel(theta.t2)); 

theta=[theta.t1;theta.t2];

    
%compute the pdf for gbatch 2
pdfp=mvgauss([theta],p.m,p.c);
pdfq=mvgauss([theta],q.m,q.c);
      
%normalize
pdfp=pdfp/(sum(pdfp));
pdfq=pdfq/(sum(pdfq));
klapprox=kldiv(pdfp,pdfq);


fprintf('kl = \t %d \n', kl);
fprintf('klapprox = \t %d \n', klapprox);

%KPMklgauss(p.c,q.c)