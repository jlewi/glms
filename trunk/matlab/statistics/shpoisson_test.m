%11-11-2005
%
%Script to test shpoisson
%
%Verify shpoisson samples a poisson distribution
%compute the mean spikes and variance and make sure it matches the rate
r=10;
twin=1000;
s=shpoisson(r,twin);

[nevents, bc]=hist(s,twin);

fprintf('rate= %d \t mean # events = %d \n', r, mean(nevents));
%for poisson mean and variance should be the smae
fprintf('rate= %d \t variance # events = %d \n', r, var(nevents));
