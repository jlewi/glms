%function [ll]= poissonll(y, rate, dt)
%       y - train of (0,1) indicating whether event occured in each bin or 
%           not
%           1xt
%           -rowvector for spike train
%       rate - the instantaneous rate of the poisson process at each time step
%            - n x t
%            - each row corresponds to a different set of rates under which
%              to compute the likelihood
%       dt - width of the time windows 
%
%Return Value:
%    ll = log likelihood of the data under a poisson process
%       = n x 1
%           -each row is likelihood under a different set of rates
%Explanation: Computes the Log Likelihood of the data under an inhomogenous
%poisson process.
function ll= poissonll(y,rate,dt)
    if (size(y) ~= size(rate))
        error('poissonll: y and rate should be same dimensions');
    end
    
    %make sure y is all ones and zeros
    ind=find(y~=0 & y~=1);
    if (~(isempty(ind)))
        error('poissonll: y should contain only 0s and 1s');
    end
    
    
    y=rowvector(y,'Poissonll line 28', 'y');
  
    %probability of getting spikes 
    sind=find(y==1);        %indexes of spikes
    numspikes=size(sind,2);
    nosind=find(y==0);      %indexes when spikes did not occur
    
    %probability of a spike at each time pt
    pspike=dt*rate;
    %check if pspike is >1
    ind=find(pspike>=1);
    if (~isempty(ind))
    
        fprintf('Warning: poissonll probability of spike >1 \n');
        %set the probability = 1 -eps to avoid log of 0 errors
        pspike(ind)=1-eps;
    end
    
    %we need to check that sind, and nosind are not empty
    if (~isempty(sind))
        ps=sum(log(pspike(sind)),2);
    else
        ps=0;
    end
    
    if (~isempty(nosind))
        pno=sum(log(1-pspike(:,nosind)),2);
    else
        pno=0;
    end
    
    ll=1/(factorial(numspikes))+ps+pno;
    