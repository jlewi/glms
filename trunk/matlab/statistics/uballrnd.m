%function [x]=uballrnd(r,d,n)
%   r        - radius of ball
%   d        - dimensionality of stimuli
%   n        - number of samples to draw
%
%Return value:
%   x       - dxn matrix 
%           - each column is a random vector drawn from the uniform ball of
%           radius r
%Explanation: Draw random samples from the uniform ball (||stim||<=r);
%
function [x]=uballrnd(r,d,n)

%1. select the direction by uniformly sampling the unit sphere
x=normrnd(0,1,d,n);

%normalize the magnitude
x=normmag(x);

%2. sample the radius for each stimulus
%to generate the radius we draw a sample from the beta
%distribution
radius=r*betarnd(d*ones(1,n),ones(1,n));

x=x*diag(radius);


