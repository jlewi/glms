% function s=strrepeat(str, rep)
%    str - string
%    rep - number of repetitions
%
% Return value
%   s= str repeated rep times and concatenated together
%
%
function s=strrepeat(str,rep)

if rep <1 
    s=[];
    return;
end

s=cell(1,rep);

for index=1:rep
    s{index}=str;
end
s=strcat(s{:});
