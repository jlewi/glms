%function x=colvector(x)
%       x - vector to ensure its column vector
%
%Explanation: Verifies x is a vector and then makes sure its a column
%vector. Really only works for 2dimensional vectors/matrices, higher
%dimensions will cause problems.

function x=colvector(x)
    %make sure nonsingleton dimensions are 1
    s=size(x);
    ss=sort(s);
    
    if (ss(end-1)>1)
        error('x is not a vector');
    end
    
    if (s(1)==1)
        x=x';
    end
    
        