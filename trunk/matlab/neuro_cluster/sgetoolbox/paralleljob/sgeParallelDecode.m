%
%
%Revisions:
%   05-28-2008: Don't check for file setpaths. This is now handled by
%   jobstartup
%
%   03-27-2008: check for a file "setpaths". If it exists execute it.
function sgeParallelDecode( runprop )
fprintf('Running sgeParallelDecode.m \n');
% This function is referenced by sgeParallelSubmitFcn. 
% THIS FUNCTION MUST BE ON THE PATH OF THE MATLAB WORKERS;
% typically, this is accomplished by putting this function into 
% $MATLABROOT/toolbox/local on the workers, or changing 
% to the directory where this function is, before starting those 
% MATLAB workers.

% Copyright 2006 The MathWorks, Inc.
% $Revision$   $Date: 2006/12/27 20:40:56 $

% Read environment variables into local variables. The names of
% the environment variables were determined by the submit function.
storageConstructor = getenv( 'MDCE_STORAGE_CONSTRUCTOR' );
storageLocation    = getenv( 'MDCE_STORAGE_LOCATION' );
jobLocation        = getenv( 'MDCE_JOB_LOCATION' );

% For a parallel job, the task location is derived from labindex
taskLocation       = [jobLocation filesep 'Task' num2str( labindex ) ];

%Lewi: Troubleshooting print the info 
fprintf('jobLocation %s \n',jobLocation);
fprintf('Working directory %s \n',pwd);

% Set runprop properties from the local variables:
set( runprop, ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ....
    'TaskLocation', taskLocation );


[s,hname]=system('hostname -s');
fprintf('sgeParallelDecode.m: labindex=%d \t host=%s \n',labindex,hname);