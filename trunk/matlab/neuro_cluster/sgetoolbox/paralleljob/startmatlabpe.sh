#!/bin/sh
#
#Author: Jeremy Lewi
#Date: 08-11-2008
#$Revision$
#
#This file was orginally based on the script provided by the Mathworks to start a parallel MPI environment
#for matlab.
#
# The orginal script just created the  hostfile.
#
# I have modified the script to setup the MPD ring by calling MPD boot. 
# Before mpdboot was being executed by the wrapper script used to start the matlab job.
# I moved this functionality into the start and stop scripts of the PE environment because I think its cleaner.
# In particular, we now have the stop script handle cleanup by issuing mpdkilljob and mpdallexit.
# This ensures if we delete the job in SGE the MPD ring is properly cleaned up.
#
# Since we want each job to have a unique MPD ring we set the environment variable MPD_CO_EXT
# to the SGE job id 
#
# 
# This PE start script expects to be called
# startmatlabpe.sh <pe_hostfile>
# Where pe_hostfile contains the nodes we've been assigned
# 
# We will create $WDIR/machines which contains a machinefile correctly
# formatted for execution
#
# Originally we created machines in $TMPDIR/machines where $TMPDIR is a directory
# created and maintained by SGE (SGE deletes the directory at the end of the job)
# Instead, I create machines in $WDIR. I do this because a bug in Matlab means we need
# to copy the machines file to every worker node. To keep the files organized
# I set WDIR to a directory named "sgejob_####" where #### is the SGE job ID.
# This makes it easy to keep track of the files and to write scripts which automatically cleanup the files.
#
# This file is based on SGE's example MPI PE startup script
#
# Revisions
#   08-26-2008: Use mpdbootsetcon and mpdsetcon so that the console extension is properly set
#     on each node
#
#the file containing the list of hosts is the first file passed in
pe_hostfile=$1;

#set the working directory where we will store files for this job
export WDIR="/tmp/sgejob_${JOB_ID}"

echo "********************************************************************************"
echo
echo "startmatlabpe.sh: Setting up parallel environment"
echo
echo "********************************************************************************"
echo
echo "Host= $HOSTNAME"
echo "pe_hostfile=${pe_hostfile}"
echo "TMP=$TMP"
echo "TMPDIR=$TMPDIR"


#**
#create the working directory
echo "Creating the working directory $WDIR"
mkdir $WDIR

#copy pe_hostfile to WDIR
#We do this for debugging. This makes it easy to rerun startmatlabpe.sh to diagnose problems
cp ${pe_hostfile} $WDIR/

#*********************************************************************************************
#Create the host file
#********************************************************************************************
PeHostfile2MachineFile()
{
   cat $1 | while read line; do
      host=`echo $line|cut -f1 -d" "|cut -f1 -d"."`
      nslots=`echo $line|cut -f2 -d" "`
      i=1
      while [ $i -le $nslots ]; do
          echo $host
          i=`expr $i + 1`
      done
   done
}


# ensure pe_hostfile is readable
if [ ! -r $pe_hostfile ]; then
   echo "$me: can't read $pe_hostfile" >&2
   exit 1
fi

# create machine-file
# remove column with number of slots per queue
# mpi does not support them in this form
machines="$WDIR/machines"
PeHostfile2MachineFile ${pe_hostfile} > ${machines}

#********************************************************************************
#Create a RING of MPD daemons
#****************************************************************************
#create a unique extension based on the sge JOB id to identify this
#ring of MPD's so that we can run multiple MPD's simultaneously
MPDEXT=sgejob_${JOB_ID}

# Create full paths to mw_smpd/mw_mpiexec if needed
#
MPD_BIN=/Applications/MATLAB_R2008a/mpich_mpd/bin
MPIEXEC_CODE=0


# Assert that we can read ${machines}
if [ ! -r ${machines} ]; then
    echo "Couldn't read ${machines}" >&2
    exit 1
fi



# Use mpd with rsh option to launch mpds
launchMpds() {
    # massage the node file for the purposes of getting 1 machine per line
    TMP_HOSTSFILE=${WDIR}/hosts.$$
    uniq ${machines} > ${TMP_HOSTSFILE}
    NUM_HOSTS=$(wc -l < ${TMP_HOSTSFILE})

    
    echo "Here's the nodefile: "
    cat ${WDIR}/machines

    # use mpdboot to setup a ring of mpd Daemons
    echo "Starting MPD from $(hostname)"

    #set MPD_CON_EXT in the environment to "" otherwise the console file isn't created with the proper extension
    export MPD_CON_EXT=""    
    # use the -v option to specify verbose output     
    echo Command is: ${MPD_BIN}/mpdbootsetcon -v -n ${NUM_HOSTS} -f ${TMP_HOSTSFILE} -r rsh -m ${MPD_BIN}/mpdsetcon --conext=$MPDEXT
    ${MPD_BIN}/mpdbootsetcon -v -n ${NUM_HOSTS} -f ${TMP_HOSTSFILE} -r rsh -m ${MPD_BIN}/mpdsetcon  --conext=$MPDEXT
    #set MPD_CON_EXT or else trace won't work
    echo "Setting MPD_CON_EXT=$MPDEXT"
    export MPD_CON_EXT=$MPDEXT
    echo "All MPD booted, about to call mpdtrace"   
    ${MPD_BIN}/mpdtrace
                
}


# create the MPD ring
launchMpds

echo
echo "********************************************************************************"
echo
echo "startmatlabpe.sh: Finished setting up parallel environment"
echo
echo "********************************************************************************"
echo
