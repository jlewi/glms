%script: createdist.job_template.m
%author: jeremy lewi - jeremy@lewi.us
%
%Explanation: This is a script which provides a template/sample
%  of how to submit a distributed job to the Matlab cluster.
%
%  A distributed job is a serial code that we want to run on the cluster.
%
%
% How the cluster must be setup to use this script:
%     1. This script is intended to be run on your local machine
%     2 We ssh into the head node to do the actual submission
%     2. You must have access to a shared filesystem with all the nodes
%          in the cluster. Matlab uses this shared filesystem to store
%          files which are used to pass data to the worker nodes.
%     3. We assume subversion is used to synchronize your code on your
%     development machine with your code on the cluster. This script will call
%     svn update to ensure we have the latest code.
%
%
% $Revision$
%
%**************************************************************************
%parameters
%
%The values below should be customized for your setup
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/svn_trunk/adaptive_sampling';

%whether or not to capture command window output
capcmdout=false;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=false;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
MATLABPATH='~/svn_trunk/matlab';
svndirs={pwd,MATLABPATH};

%an array of FilePath objects the input files
finfiles={};

%an array of FilePath objects specfying the output files
foutfiles={};

%name to use for the job
jname='jtemp';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir=[];

%if there's some code you want to execute before starting
%the function (i.e setting the path) you can specify a handle to this
%function
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=[];

%The directory on your local computer where jobs should be stored
localjobdir='~/jobs';

%directory on the cluster where jobs should be stored
%This path should be accessible from the head nodes and worker nodes
clusterjobdir='/Users/jlewi/jobsremote';

clusterhost='cluster.neuro.gatech.edu';

%your username on the cluster
clusteruser='jlewi';

%jdep a list of jobs which must complete prior to runing this job
%set to empty if this job doesn't depend on any previous jobs
jdep=[];

%**************************************************************************
%update the node on the cluster
%*************************************************************************
if (svnupdate)
    wd=pwd;
    for j=1:length(svndirs)
         cmd =sprintf('ssh "%s" svn update "%s"',clusterhost, svndirs{j}');
        fprintf('Issueing svn update  on remote host in %s \n',svndirs{j});

        [status, msg]=system(cmd);
        fprintf('output: %s ',msg);
    end
    cd(wd);
end

%*********************************************************************
%setup the job
%************************************************************************

%get the scheduler
%distsched.m is a function which is part of the Neurolab Matlab toolbox
%which creates a scheduler for use with the Neurolab Matlab cluster.
sched=distsched(localjobdir);

%we have to modify the scheduler object's submit fcn property
%because we have to specify the directory on the cluster where 
%the data is stored
set(sched,'submitFcn',{@sgeremoteSubmitFcn,clusterhost,clusterjobdir,jdep,clusteruser})





%**************************************************************************
%create the jobs
%**************************************************************************
[jobdata]=initjobdata(jname,finfiles,foutfiles,startdir,startupfunc,localhost,localdatadir,localjobdir,clusterjobdir);



%create the job
job=createJob(sched);

%set the jobdata
job.JobData=jobdata;



%number of output arguments
naout=1;


%input arguments
iparam={[1:100]};

task=createTask(job,@sum,naout,iparam);

%task properties
set(task,'CaptureCommandWindowOutput',capcmdout);


submit(job);

