%script: createdist.job_template.m
%author: jeremy lewi - jeremy@lewi.us
%
%Explanation: This is a script which provides a template/sample
%  of how to submit a distributed job to the Matlab cluster.
%
%  A distributed job is a serial code that we want to run on the cluster.
%
%
% How the cluster must be setup to use this script:
%     1. This script is intended to be run on the head node of your
%     cluster
%     2. You must have access to a shared filesystem with all the nodes
%          in the cluster. Matlab uses this shared filesystem to store
%          files which are used to pass data to the worker nodes.
%     3. We assume subversion is used to synchronize your code on your
%     development machine with your code on the cluster. This script will call
%     svn update to ensure we have the latest code.
%
%
% $Revision$
%
%**************************************************************************
%parameters
%
%The values below should be customized for your setup
%**************************************************************************
%set the startup directory
%The start directory specifies which directory we want Matlab
%to cd to once Matlab is started. We need to set this appropriately
%so that Matlab can find our code.
%
startdir='/Users/jlewi/svn_trunk/adaptive_sampling';

%whether or not to capture command window output
capcmdout=false;

%whether or not to issue svn update
%if we are running this script multiple times to submit different jobs
%we may not wish to rerun svn update on each trial because it takes time
%and we already know the code is updated.
svnupdate=false;

%svndirs is a cell array of the directories we want to run svn update
%in to make sure we have the latest code
MATLABPATH='~/svn_trunk/matlab';
svndirs={pwd,MATLABPATH};

%an array of FilePath objects the input files
%If you intent to use initjobdata and the SGEToolbox to transfer
%these files to/from your local machine then these filepath objects
%must specify the global variable RESULTSDIR as the base command.
%see the help for initjobsdata for more info.
finfiles={};

%an array of FilePath objects specfying the output files
foutfiles={};

%name to use for the job
jname='jtemp';

%local host is the hostname of the machine on which you develop
%and from which you will want to copy data files
localhost='bayes.neuro.gatech.edu';

%localdatadir - the base directory relative to which all input/output
%file paths are relative to on localhost
localdatadir='/home/jlewi/svn_trunk/allresults';

%if there's some code you want to execute before starting
%the function (i.e setting the path) you can specify a handle to this
%function
%this function will be called at the end of taskstartup.m
%THIS FUNCTION MUST BE IN STARTDIR otherwise matlab won't be able to find it
startupfunc=[];
%**************************************************************************
%update the node on the cluster
%*************************************************************************
if (svnupdate)
    wd=pwd;
    for j=1:length(svndirs)
        if ~exist(svndirs{j},'dir')
            error(['Cannot issue svn update in %s, directory does not ' ...
                'exist.'],svndirs{j});
        end
        cd(svndirs{j});
        fprintf('Issueing svn update in %s \n',svndirs{j});

        system('svn update');

    end
    cd(wd);
end

%*********************************************************************
%setup the job
%************************************************************************

%get the scheduler
%distsched.m is a function which is part of the Neurolab Matlab toolbox
%which creates a scheduler for use with the Neurolab Matlab cluster.
sched=distsched();








%**************************************************************************
%create the jobs
%**************************************************************************
%initjobdata is part of the SGEToolbox.
%This function initializes the datastructure properly so that we transfer files to/from
%the local machine to the cluster
[jobdata]=initjobdata(jname,finfiles,foutfiles,startdir,startupfunc,localhost,localdatadir);



%create the job
job=createJob(sched);

%set the jobdata
job.JobData=jobdata;





%task properties
set(task,'CaptureCommandWindowOutput',capcmdout);


submit(job);

end