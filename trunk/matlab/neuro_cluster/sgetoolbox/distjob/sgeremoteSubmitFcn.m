%function tinfo=sgeremoteSubmitFcn(scheduler,job,props, cluster, clusterdatadir,jdep)
%   cluster  - hostname of the cluster. This the name of the host
%              where qsub is executed
%   clusterdatadir - the directory on the cluster where the data should be
%             stored
%
%    jdep     - a comma separated list of job numbers which must
%               be completed before this job can run
%             -optional
%             - set to empty, [], if no dependencies and you want to
%             specify clusteruser
%   clusteruser     - Your username on the cluster
%
%
%Explanation:
%    This function is used to submit a job remotely- i.e from a machine
%    which does not have direct access to the shared filesystem used by
%    matlab on the cluster to pass job information to the nodes.
%
%   1. We create a script in the local job directory
%       . this script sets the appropriate environment variables
%         and then calls qsub
%   2. We use rsync to copy the files to the head node
%   3. we use ssh to run the script we created
%$Revision$
%
%Revisions:   
%   08-25-2008
%       -Modified the file so the username on the cluster can be different
%       from the user on the client.
%       -Allow the local and remote job directories to have different names
%
%   07-23-2008
%       when copying directories always add the filesep to the final path
%       do this instead of removing the final directory from the
%       destiantion path
%       this way we can change the name of the directory we are copying it
%       to
function sgeremoteSubmitFcn(scheduler, job, props, cluster,clusterdatadir,varargin)

%SUBMITFCN Submit a Matlab job to a SGE scheduler
%
% See also workerDecodeFunc.
%
% Assign the relevant values to environment variables, starting
% with identifying the decode function to be run by the worker:


[ocode, clientuser]=system('whoami');
clientuser=strtrim(clientuser);

if (length(varargin)>=1)
    jdep=varargin{1};
else
    jdep=[];
end


if (length(varargin)>=2)
    clusteruser=varargin{2};
else
    clusteruser=clientuser;
end

scheduler.UserData = { cluster ; clusterdatadir };
localdatalocation = scheduler.DataLocation;

jobdata=job.JobData;

    %the name of the script which starts Matlab and runs the actual job

scriptName=fullfile(get(scheduler,'ClusterMatlabRoot'),'toolbox','sgetoolbox','distjob','sgeWrapper.sh');
    

%**************************************************************
%for each task create the appropriate submit scripts
%******************************************************
for i = 1:props.NumberOfTasks

        
    %*******************************************************************
    %create the job script
    %   this is the script the grid engine will execute on the worke node
    %   to start the job.
    %This script does two things
    % 1. calls sgeWrapper.sh to start Matlab and run the actual job
    % 2. calls rsync to rsync the job directory back to the local machine
    %********************************************************************
    execscript=sprintf('task_%d_exec.sh',i);
    localexec=fullfile(localdatalocation,props.JobLocation,execscript);
    remoteexec=fullfile(clusterdatadir,props.JobLocation,execscript);
    fid=fopen(localexec,'w');

    fprintf(fid,'#!/bin/sh \n');
    fprintf(fid, '# Ensure that under SGE, were in /bin/sh too \n');
    fprintf(fid,'#$ -S /bin/sh \n');
    fprintf(fid, '#$ -v MDCE_DECODE_FUNCTION,MDCE_STORAGE_LOCATION,MDCE_STORAGE_CONSTRUCTOR,MDCE_JOB_LOCATION,MDCE_TASK_LOCATION,MDCE_MATLAB_EXE,MDCE_MATLAB_ARGS,MDCE_DEBUG,MDCE_START_DIR \n');

    fprintf(fid,'%s \n',scriptName);
%    fprintf(fid,'rsync -ae ssh %s %s:%s \n',fullfile(clusterdatadir,props.JobLocation),jobdata.client.host,localdatalocation);
    fprintf(fid, 'echo executing rsync on %s \n',clusterdatadir); 

    %make sure the local datadir ends in pathsep because we want to
    %synchronize the contents of the directory
    if (localdatalocation(end)~=filesep)
        localdatalocation=[localdatalocation filesep];
    end
    
    %
    
    %make sure clusterdatadir ends in file sep
    if (clusterdatadir(end)~=filesep)
        clusterdatadir=[clusterdatadir filesep];
    end
    fprintf(fid,'rsync -ae ssh %s %s@%s:%s \n',clusterdatadir,clientuser,jobdata.client.host,localdatalocation);
    fclose(fid);
    
    %make the script executable
    [s,w]=system(['chmod u+x ' localexec]);
    
    %********************************************************************
    %create the submit  script to be executed on the head node
    %1 script per task
    % this script sets the appropriate environment variables before calling
    % qsub
    %**********************************************************************
    
    submitscript=sprintf('task_%d_submit.sh',i);


    localsubmit{i}=fullfile(localdatalocation,props.JobLocation,submitscript);
    remotesubmit{i}=fullfile(clusterdatadir,props.JobLocation,submitscript);
    
    fid=fopen(localsubmit{i},'w');

    fprintf(fid,'#!/bin/sh \n');


    fprintf(fid, 'export MDCE_DECODE_FUNCTION=sgeDecodeFunc \n');


    % Set the other job-related environment variables:
    remotestorage = sprintf('PC{}:UNIX{%s}:', clusterdatadir);
    fprintf(fid,'export MDCE_STORAGE_LOCATION=%s \n', remotestorage);
    fprintf(fid,'export MDCE_STORAGE_CONSTRUCTOR=%s \n', props.StorageConstructor);
    fprintf(fid,'export MDCE_JOB_LOCATION=%s \n', props.JobLocation);
    % Ask the workers to print debug messages by default:
    fprintf(fid,'export MDCE_DEBUG=true \n');

    % Tell the script what it needs to run. These two properties will
    % incorporate ClusterMatlabRoot if it is set.
    fprintf(fid,'export MDCE_MATLAB_EXE=%s\n', props.MatlabExecutable );
    fprintf(fid,'export MDCE_MATLAB_ARGS=%s \n', props.MatlabArguments );
%    fprintf('Matlab Executable command %s \n', props.MatlabExecutable);





    fprintf('Submitting task %i\n', i);
    fprintf(fid,'export MDCE_TASK_LOCATION=%s \n', props.TaskLocations{i});

    %3-11-2008: change the name of the task

    if (isfield(job.Tasks(i).UserData,'taskname') && ~isempty(job.Tasks(i).UserData.taskname));
        tname=job.Tasks(i).UserData.taskname;
    else
        tname=sprintf('Job%d.%d',job.ID,job.Tasks(i).ID);
    end
    % Choose a file for the output. Please note that currently, DataLocation refers
    % to a directory on disk, but this may change in the future.
    %03_11-2008- change the name of the logfile to match the task name
    %    logFile = fullfile( scheduler.DataLocation, ...
    %                       sprintf( 'Job%d_Task%d.out', job.ID,
    %                       job.Tasks(i).ID ) );
    logfile = fullfile( clusterdatadir, sprintf('%s_%s.out',tname,datestr(clock,'yymmdd')));
    % Finally, submit to SGE. note the following:
    % "-N Job#" - specifies the job name
    % "-j yes" joins together output and error streams
    % "-o ..." specifies where standard output goes to
    %Lewi: Have qsub set the environment variable MDCE_START_DIR as well


    %cmdLine = sprintf( 'qsub  -N Job%d.%d', job.ID,job.Tasks(i).ID);
    cmdLine = sprintf( 'qsub  -N %s', tname);

    %we need to request a matlab license for each task
    cmdLine = sprintf('%s -l mlab=1 ',cmdLine);
   
    %if jdep is not empty then add the option -hold_jid
    % -hold_jid - list of job ids which must complete sucessfully
    %    before running this job
    if ~isempty(jdep)
        cmdLine=sprintf('%s -hold_jid %s',cmdLine,jdep);
    end

    cmdLine = sprintf( '%s -j yes -o "%s" "%s"', cmdLine, logfile, remoteexec);

    fprintf(fid,'%s \n', cmdLine);

    fclose(fid);
    
    %make the script executable
    [s,w]=system(['chmod u+x ' localsubmit{i}]);

    
 
end


   %********************************************************************
    %use rsync to transfer the job directory to the cluster
    % we do this after creating all of the scripts for all tasks
    %because we only want to issue one rsync
    %*********************************************************

    %transfer the directory containing all the job data
    src.host='';
%    src.fname=fullfile(localdatalocation,props.JobLocation);
    src.fname=localdatalocation;
    src.isdir=true;

    dest.host=[clusteruser '@' cluster];
%    dest.fname=fullfile(clusterdatadir,props.JobLocation);
    dest.fname=clusterdatadir;
    dest.isdir=true;

    ecode=rsyncfile(src,dest);

    if (ecode~=0)
        error('Unable to sync %s with the cluster. rsync exited with code %d',src.fname,ecode);
    end


    

    for i=1:props.NumberOfTasks
    %******************************************************************
    %Submit the job
    %   we do this by sshing into the cluster and executing the script we
    %   just created
    %******************************************************************

    sshcmd=sprintf('ssh %s@%s %s',clusteruser,cluster,remotesubmit{i});
    [s, w] = system(sshcmd);

    if s ~= 0
        warning( 'distcompexamples:generic:SGE', ...
            'Submit failed with the following message:\n%s', w);
    else
        % The output of successful submissions shows the SGE job identifier%
        fprintf( 1, 'Job output will be written to: %s\nQSUB output: %s\n', logfile, w );
    end
    end