%function tinfo=sgeSubmitFcn(scheduler,job,props,jdep)
%    jdep     - a comma separated list of job numbers which must 
%               be completed before this job can run
%             -optional
%
%
%
%Explanation:
%    To include a nonstandard name for the task specif that name in the
%    UserData.taskname field of the task object.
%
%$Revision$
%
%Revisions:
%   06-24-2008
%      set the path of the script to launch matlab to use 
%      clustermatlabroot property of the scheduler and the  neurolab
%      toolbox
%
%   06-12-2008
%      No longer set startdir. Instead the decode function should be
%      in /Applications/MATLAB/toolbox/neurolab
%      this should already be on the matlab path
%
%      I could also get the path to Matlab from the clusterMatlabroot
%      property of the scheduler
%
%   03-11-2008:
%      allow the user to specify names for the tasks
%
%   11-12-2007: added support for specifying jobs on which it depends.
%
function sgeSubmitFcn(scheduler, job, props, varargin) 
%SUBMITFCN Submit a Matlab job to a SGE scheduler
%
% See also workerDecodeFunc.
%
% Assign the relevant values to environment variables, starting 
% with identifying the decode function to be run by the worker:


%  startdir='/Users/jlewi/svn_trunk/matlab/neuro_cluster/distributedjob';

if (length(varargin)>=1)
    jdep=varargin{1};
else
    jdep=[];
end



%custom variable for setting the start dir
%06-22-2008 no longer set MDCE_START_DIR
%setenv('MDCE_START_DIR',startdir);

% Copyright 2006 The MathWorks, Inc.
setenv('MDCE_DECODE_FUNCTION', 'sgeDecodeFunc'); 
% 
% Set the other job-related environment variables:
setenv('MDCE_STORAGE_LOCATION', props.StorageLocation); 
setenv('MDCE_STORAGE_CONSTRUCTOR', props.StorageConstructor);
setenv('MDCE_JOB_LOCATION', props.JobLocation); 
% Ask the workers to print debug messages by default:
setenv('MDCE_DEBUG', 'true');

% Tell the script what it needs to run. These two properties will
% incorporate ClusterMatlabRoot if it is set.
setenv( 'MDCE_MATLAB_EXE', props.MatlabExecutable );
setenv( 'MDCE_MATLAB_ARGS', props.MatlabArguments );
fprintf('Matlab Executable command %s \n', props.MatlabExecutabl);

%the path name must resolve the same on the sumbit node as the worker node
[dirpart] = fileparts( mfilename( 'fullpath' ) );
scriptName = fullfile( dirpart, 'sgeWrapper.sh' );


% Submit the wrapper script to SGE once for each task, supplying a different
% environment each time.
for i = 1:props.NumberOfTasks
    fprintf('Submitting task %i\n', i);
    setenv('MDCE_TASK_LOCATION', props.TaskLocations{i});
    
     %3-11-2008: change the name of the task

    if (isfield(job.Tasks(i).UserData,'taskname') && ~isempty(job.Tasks(i).UserData.taskname));
      tname=job.Tasks(i).UserData.taskname;
    else
      tname=sprintf('Job%d.%d',job.ID,job.Tasks(i).ID);
      end
    % Choose a file for the output. Please note that currently, DataLocation refers
    % to a directory on disk, but this may change in the future.
    %03_11-2008- change the name of the logfile to match the task name
%    logFile = fullfile( scheduler.DataLocation, ...
 %                       sprintf( 'Job%d_Task%d.out', job.ID,
 %                       job.Tasks(i).ID ) );
     logFile = fullfile( scheduler.DataLocation, sprintf('%s.out',tname));
    % Finally, submit to SGE. note the following:
    % "-N Job#" - specifies the job name
    % "-j yes" joins together output and error streams
    % "-o ..." specifies where standard output goes to
    %Lewi: Have qsub set the environment variable MDCE_START_DIR as well
    
   
    %cmdLine = sprintf( 'qsub  -N Job%d.%d', job.ID,job.Tasks(i).ID);
   cmdLine = sprintf( 'qsub  -N %s', tname);
   
   %we need to request a matlab license for each task
   cmdLine = sprintf('%s -l mlab=1 ',cmdLine);
    %if jdep is not empty then add the option -hold_jid
   % -hold_jid - list of job ids which must complete sucessfully
   %    before running this job
    if ~isempty(jdep)
       cmdLine=sprintf('%s -hold_jid %s',cmdLine,jdep); 
    end

    cmdLine = sprintf( '%s -j yes -o "%s" "%s"', cmdLine, logFile, scriptName);
   
    fprintf('sgesubmitfcn.m cmdline: %s \n',cmdLine);
    [s, w] = system( cmdLine );

    if s ~= 0
        warning( 'distcompexamples:generic:SGE', ...
                 'Submit failed with the following message:\n%s', w);
    else
        % The output of successful submissions shows the SGE job identifier%
        fprintf( 1, 'Job output will be written to: %s\nQSUB output: %s\n', logFile, w );
    end
end
