%function tasktable(sched)
%    sched
%
%Explanation: creates a cell array describing all the tasks in the
%scheduler
function tinfo=tasktable(sched)
  
  %get the jobs
  jobs=get(sched,'jobs');
  
  
  %initialize the table
  %note this size could be too small but thats ok 
  %we will just resize dynamically
  tinfo=cell(1+length(jobs),6);
  
  
  %headers for the table

  cind=1;
  tinfo{1,cind}='Row';
  cind=cind+1;
  tinfo{1,cind}='Job Name';
   cind=cind+1;
  tinfo{1,cind}='task';
   cind=cind+1;
  tinfo{1,cind}='function'; 
  cind=cind+1;
  tinfo{1,cind}='args';
   cind=cind+1;
  tinfo{1,cind}='state';
  cind=cind+1;
  tinfo{1,cind}='Error';
  
  rind=1;
  %print out table in reverse job order 
  %i.e more recent jobs are in first rows
  for jind=length(jobs):-1:1
    tasks=get(jobs(jind),'tasks');
    
    for tind=1:length(tasks)
      rind=rind+1;
      cind=1;
      tinfo{rind,cind}=rind;
      cind=cind+1;
      tinfo{rind,cind}=get(jobs(jind),'Name');
      cind=cind+1;
      tinfo{rind,cind}=get(tasks(tind),'ID');
      cind=cind+1;
      tinfo{rind,cind}=get(tasks(tind),'Function');
      %convert the input arguments to a string array
      clear iarg;
      iarg.arg=get(tasks(tind),'InputArguments');
      iarg=structinfo(iarg);
      cind=cind+1;
      tinfo{rind,cind}=iarg{1,2}';
      cind=cind+1;
      tinfo{rind,cind}=get(tasks(tind),'State');
      
      e=get(tasks(tind),'Error');
%      tinfo{rind,6}=e.message;
      
      %if error is not blank print out the stack
      
     
      if ~isempty(e.stack)
        cind=cind+1;
         tinfo{rind,cind}=structinfo(e);
      end
    end
  end
  
  