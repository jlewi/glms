%FST_DSPAMMT2 Operator multiplication by A*B', A, B sparse
%  FST_DSPAMMT2(D,A,B,C,{ALPHA=1},{BETA=0})
%
%  D = ALPHA*A*B'*C + BETA*D
% 
%  Here, A, B are sparse matrices with the same number of cols, while
%  D, C are dense matrices. Structure codes are ignored. Intermed.
%  storage of size COLS(A) is used, but B'*C does not have to be
%  stored.

%  Copyright (C) 2005 Matthias Seeger
% 
%  This program is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
%  USA.
