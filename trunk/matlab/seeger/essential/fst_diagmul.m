%FST_DIAGMUL Diagonal of matrix-matrix multiplication
%  DG = FST_DIAGMUL(A,B,TRS,{D})
%
%  DG = DIAG(op(A)'*DIAG(D)*op(B)), op(X) == X or X', dep. on TRS
%
%  A, B must have the same size. If D is not given, it is all ones.
%  Structure codes are ignored.

%  Copyright (C) 2005 Matthias Seeger
% 
%  This program is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
%  USA.
