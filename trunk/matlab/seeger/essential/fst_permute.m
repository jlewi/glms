%FST_PERMUTE Operator row/column permutation
%  FST_PERMUTE(A,PERM,COL)
%
%  If COL==1, columns of A are permuted according to PERM: Col. i
%  moves to pos. PERM(i). If COL==0, rows are permuted.
%  Structure codes are ignored.

%  Copyright (C) 2005 Matthias Seeger
% 
%  This program is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
%  USA.
