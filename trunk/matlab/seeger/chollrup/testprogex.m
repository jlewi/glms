% Copyright (C) 2005 Matthias Seeger
%
%This program is free software; you can redistribute it and/or
%modify it under the terms of the GNU General Public License
%as published by the Free Software Foundation; either version 2
%of the License, or (at your option) any later version.
%
%This program is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU General Public License for more details.
%
%You should have received a copy of the GNU General Public License
%along with this program; if not, write to the Free Software
%Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
%02110-1301, USA.

n=100;

% Create matrix A with controlled spectrum
maxlam=2; minlam=0.1;
[q,r]=qr(randn(n,n));
a=muldiag(q,rand(n,1)*(maxlam-minlam)+minlam)*q';
rfact=chol(a);

% Test exchange updates
for i=1:20
  k=floor(rand*(n-1))+1;
  l=k+1+floor(rand*(n-k));
  b=randn(n,3*n);
  x=(rfact')\b;
  r=rfact; r(1,1)=r(1,1)+1; r(1,1)=r(1,1)-1;
  cholupexch(r,k,l,2,x);
  ind=[1:(k-1) (k+1):l k (l+1):n];
  apr=a(ind,ind);
  r_2=chol(apr);
  x_2=(r_2')\b(ind,:);
  fprintf(1,'k=%d, l=%d\n',k,l);
  fprintf(1,'Max. dist. R: %f\n',max(max(abs(r-r_2))));
  fprintf(1,'Max. dist. X: %f\n',max(max(abs(x-x_2))));
  if max(max(abs(r-r_2)))>(1e-6)
    imagesc(abs(r-r_2));
    ind=find(abs(r-r_2)>(1e-6))
    r(ind)
    r_2(ind)
    diag(r)
    pause;
  end
end
