%function y = IOTdaqGetInput(daq,chnls,fs,dur)
%       daq - structure which stores daq object
%              result of IOdaqInit
%       chnls- which channels we want to record from
%       fs   - sampling rate (samples/sec)
%       dur  - how long to record for (seconds)
function y = IOTdaqGetInput(daq,chnls,fs,dur)

% save the previous ai
ai_prev = daq.ai;

% reinitialize the inputs
daq.ai = eval(char(daq.hw.ObjectConstructorName(1)));
chnls = addchannel(daq.ai,chnls);

% reset sample information
daq.ai.SampleRate = fs;
daq.ai.SamplesPerTrigger = dur*fs;

% start DAQ
start(daq.ai);

% wait for samples to be finished
% daq.ai.SamplesAvailable
% while daq.ai.SamplesAvailable < 10,
%     daq.ai.SamplesAvailable
% end

y = transpose(getdata(daq.ai));

delete(daq.ai);
daq.ai = ai_prev;