%function IOTdaqOutputSingle(daq,chnl,value)
function IOTdaqOutputSingle(daq,chnl,value)

delete(daq.ao);

daq.ao = eval(char(daq.hw.ObjectConstructorName(2)));
addchannel(daq.ao,chnl);
putsample(daq.ao, value);
