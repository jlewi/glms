function [tau, Cm]=capacitance(Vm, Im, t)
%Capacitance calculation
%[tau, Cm]=capcitance(Vm, Im, t)
%where Vm= potential
%      Im= current (unfiltered)
%      t=  time
%      tau= time constant
%      Cm= membrane capacitance

% format long e
desired_Vm=min(Vm)+0.632*amplitude(Vm);
% filter and differentiate Im
der_Im=diff(lpbutterfilt(Im,1e5,2e3));
max_der_Im=max(der_Im);
t_min=1;
while max_der_Im~=der_Im(t_min)
    t_min=t_min+1;
end
% t_min = t(find(der_Im == max_der_Im))
t_amp=t_min;
while Vm(t_amp)<=desired_Vm
    t_amp=t_amp+1;
end
tau=t(t_amp)-t(t_min);
Cm=tau/500e6;