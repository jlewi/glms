%Scalecolor
%   scalecolor(num,min,max)
%
%   num -numbers to scale. this can be a single number or a 2x2 matrix
%   I think the same think can be accomplished by using imshow(x,colormap) which shows an indexed image using the specified colormap
%this function takes numbers >min and <max number and returns an RGB COlor which is scaled to the value of the number
% min is not strictly less than max then this function simply returns the
% first number in the colormap
function scolor=scalecolor(num,min,max)

    %get information about the current colormap
    map=colormap;
    numcolors=size(map,1);
    %get the size of the matrix
    sizem=size(num);

    if min < max
      
        %reshape it so it is a column vector this is necessary for when we access colormap
        num=reshape(num,sizem(1)*sizem(2),1);
    
        %shift the region of numbers so that it starts at zero
        num=num-min;
        max=max-min;
        min=0;
    
        %make sure number is within range this might not happen b\c of rounding errors
        %if (num < min)
        %    num=min;
        %end
        num(num<min)=min;
    
        %now shrink, or stretch the region of numbers so that the max value numcolors-1
        num=num *(numcolors-1)/max;
    
        %so  0<=num <=(numcolors-1)
        %so add 1 and take the floor in order to get a valid index for map
        index=floor(num+1);
    else
        index=floor(1);
    end
    
        %get the color and multiply by 255 since colormap has values 0-1 and we want to scale these to rgb values
        scolor=255 * map(index,:);
    
        %now reshape it back into same dimensions as original matrix x3 to represent the 3 rgb values
        scolor=reshape(scolor,sizem(1),sizem(2),3);