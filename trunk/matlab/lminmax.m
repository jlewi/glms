%function [lmin,lmax]=lminmax(y,ploton)
%       y - data
%       ploton - 1= plot
%                0= no plot
% Return -
%       lmin - indexes of local minima
%       lmax - indexes of local maxima
%
%Explanation: 
%   Finds the indexes at which the local minima and maxima occur in y.
%   Find local min and local max by looking for changes in the sign of the
%   derivative
%   2. take the sign of all the points
%   3. apply median filtering to the sign
%
% 8-09-2005
%   Modified so that endpoints are included
function [lmin, lmax]=lminmax(y,ploton)
    dim=size(y);
    if (dim(1))>1;
        %its a row vector take transpose
        y=y';
    end

    if(~exist('ploton','var'))
        ploton=0;
    end
    

    filtlen=1;      %length of smoothing filter
                    %works better with no averaging
    medfilt=3;      %length of median filter

    %smooth the data
    fvm=movavg(y,filtlen);

    %fvm=y;
    dvm=diff(fvm);

    %pad the derivative so it lines up properly
    dvm=[dvm(1) dvm];
    svm=sign(dvm);

    msvm=medfilt1(svm,medfilt);

    %get the zero crossings
    zc=fcrossing(1:length(msvm),msvm',0);

    %local max transition from positive to negative
    %return the index of the previous pt
    lmax=find(msvm(floor(zc))==1);
    lmax=floor(zc(lmax));
    lmin=find(msvm(ceil(zc))==1);
    lmin=floor(zc(lmin));
   
    %add the endpoints
    if (msvm(1)>0)
        lmin=[1 lmin];
    else
        lmax=[1 lmax];
    end
    
    if (msvm(end)<0)
        lmin=[lmin length(y)];
    else
        lmax=[lmax length(y)];
    end
    
    %plot the data and overlay max an min
    if (ploton>1)
    figure;
    plot(1:length(y),y,'b-',lmax,y(lmax),'ro',lmin,y(lmin),'rx');
    end