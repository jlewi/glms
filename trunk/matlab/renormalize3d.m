%renormalizes a 3dimensional matrix 
%my intention is to use this after applying smoothing

function result=renormalize3d(matrix)
    total=sum(matrix,3);
    total=sum(total,2);
    total=sum(total,1);
    result=(1/total) * matrix;