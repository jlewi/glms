%11-23-2007
%   Matlab addpath already checks for duplicate paths so we don't have to.
%3-08-07
%   use genpath to add all subpaths in Matlab directory

%make sure MATLABPATH is an absolute path because otherwise we will add
%relative paths to the matlab path which is a problem
MATLABPATH=abspath(MATLABPATH);

%we want to use the sarraytocell function which is in the
%MATLABPATH/strings folder
addpath(fullfile(MATLABPATH,'strings'));

mpaths=rmhiddendirs(genpath(MATLABPATH));  %paths for matlab directory

%exclude directories to exclude
dexclude=sarraytocell(genpath(fullfile(MATLABPATH,'templates')));

%***************************************************************
%Parallel jobs and distributed jobs
%********************************************************************
% if we are running on the cluster as a job we don't want to include
% both directiors paralleljob and distjob because they both contain
% taskstartup etc...
% These directories should be included as part of the pathdependcy of the 
% job so we exclude them here
j=getCurrentJob();

[stat, hname]=system('hostname -s');
hname=deblank(hname);

if ~isempty(j)
  dexclude{end+1}=fullfile(MATLABPATH,'neuro_cluster','paralleljob');
  dexclude{end+1}=fullfile(MATLABPATH,'neuro_cluster','distributedjob');
  dexclude{end+1}=fullfile(MATLABPATH,'neuro_cluster','sgetoolbox');
else

switch (hname)
 case 'cluster'
     %all the code we need should be in the neurolab toolbox
     dexclude{end+1}=fullfile(MATLABPATH,'neuro_cluster','paralleljob');
     dexclude{end+1}=fullfile(MATLABPATH,'neuro_cluster','distributedjob');
     dexclude{end+1}=fullfile(MATLABPATH,'neuro_cluster','sgetoolbox');
 case 'bayes'
     %we need the toolbox
     dexclude{end+1}=fullfile(MATLABPATH,'neuro_cluster','paralleljob');
%     dexclude{end+1}=fullfile(MATLABPATH,'neuro_cluster','distributedjob');
end
end


for ind=1:length(dexclude)    
   mpaths=regexprep(mpaths,[dexclude{ind} '[^:]*'],''); 
end


%turn warning about duplicat paths off
warning('off','MATLAB:dispatcher:pathWarning');
addpath(mpaths);
%turn warning about duplicat paths off
warning('on','MATLAB:dispatcher:pathWarning');

%add relevant directories to java path
%but only if jvm is loaded
if isempty(javachk('jvm'))
if exist('/opts/commons-jxpath-1.2/commons-jxpath-1.2.jar','file')
    javaaddpath('/opts/commons-jxpath-1.2/commons-jxpath-1.2.jar');
else
    warning('path to jxpath doesnt exist');
end

if exist('/opts/commons-codec/commons-codec-1.3.jar','file')
    javaaddpath('/opts/commons-codec/commons-codec-1.3.jar');
else
    warning('path to jxpath doesnt exist');
end
end




