
%function copyfields
%   source - source structure/object to copy fields from
%   dest   - destination object for fields
%   skip   - cell array of fields to skip
function dest=copyfields(source,dest,skip)
 %we just need to copy the fields
        %and handle any special cases if required
        fnames=fieldnames(source);

        %struct for object
        sobj=struct(dest);
        for j=1:length(fnames)
            switch fnames{j}
                case skip
                    %do nothing we skip this field
                otherwise
                    %set the new field this will cause an error
                    %if the field isn't a member of the new object
                      dest.(fnames{j})=source.(fnames{j});
            end
        end
    