%function writematrix(obj,mat,id,overwrite)
%       mat - matrix to write
%       id  -  id of the matrix to write
%               - these must be >=0
%       overwrite - tells how to handle case where matrix is already in
%                       -default - overwrite data
%                       ~0 - matrix in file is overwritten with new matrix
%       recnum   - record number for this matrix
%               - nan - means the matrix is new
%               - can also be the record number of a record storing a
%               matrix with this id
%               - or can be the record number of a record where the matrix
%               has been deleted
% Explanation: Writes a single matrix to the file
function obj=writematrix(obj,mat,id, overwrite,recindex)

%initialize fid
fid=nan;

if ~exist('overwrite','var')
    overwrite=1;
end

%create the filename
%fullf=getpath(obj.fname);

%don't check if the file exists each time we write the matrix
%b\c it is slow. We should just check file exists once when we create/load
%the object.
%if ~exist(fullf,'file')
%    error(sprintf('%s file does not exist \n',fullf));
%end

%we put a labbarier here because we don't want to start changing the files
%until all nodes have reached the call to writematrix
%otherwise we risk changing the file while some other node is reading it
labBarrier;

if (length(id)>1)
    error('There must be only 1 ID.');
end

%make sure ids are positive
if any(id<0)
    error('ids must be >=0');
end

if ~exist('recindex','var')
    %nan means this id is not in the file
    recindex=posofid(obj,id);
else
    if ~isnan(recindex)
        %verify id matches
        if (idofrec(obj,recindex)~=id && ~isdeleted(obj,recindex))
            error('The ID associated with this record number does not match the supplied id.')
        end
    end

end
%get the header
head=getheader(obj);




if isnan(recindex)
    %if there is a deleted record, use that record to store the new matrix
    if ~isempty(obj.finfo.deletedrecnums)
        recnum=obj.finfo.deletedrecnums(1);
        obj.finfo.deletedrecnums=obj.finfo.deletedrecnums(2:end);

        %call update matrix to update the matrix
        updatematrix(obj,mat,id,head,overwrite,recnum);

    else
        writenewmatrix(obj,mat,id,head);

    end

else
    updatematrix(obj,mat,id,head,overwrite,recindex);
end

if (obj.debug)
    validate(obj);
end


%************************************************************************
%function updatematrix(obj,mat,id,head,overwrite)
function updatematrix(obj,mat,id,head,overwrite,recindex)

%check if we are overwriting a deleted record
isdrec=isdeleted(obj,recindex);

if (isdrec)
    %since we are overwritting a deleted record the number of records in
    %the file increases
    head.nmat=head.nmat+1;
end

if (overwrite==0)
    warning('Matrices already in file were not updated because overwrite is set to false');
else



    if ((numlabs>1) && obj.distmatrix)

        %each lab reads the appropriate values of the covariance matrix from
        %the file
        %we get the columns of the covariance matrix stored on lab
        %this is a distributed vector, with the local part storing
        %the columns of c stored on each lab
        colind=dcolon(1,head.dim(2))

        clocal=localPart(colind)

        if isempty(clocal)
            %could be empty if number of labs is greater than number of
            %columns
            colind=[];
        else
        colind=[clocal(1) clocal(end)]
        end
    else
        colind=[1 head.dim(2)];
    end
end
%the write command should be executed in parallel
%parallel writes shouldn't be an issue because we are accessing different parts of a file.
%calling writeubmatrix might be subotimal when writing the
%entire matrix because it entails multiple fseeks, one for each
%column
if (obj.debug)
    fprintf('writematrix: colind= \n');
    colind
end
writesubmatrix(obj,mat,id,[1 head.dim(1)],colind,recindex);


%update the header
%update the header
    %still update the header
    writeheader(obj,head);

%we don't want execution to continue until header is updated

%halt execution until all threads get here.
labBarrier;


%*************************************************************
% function writenewmatrix(obj,mat,id,head)
%
% Explanation: write a new matrix to the file
function writenewmatrix(obj,mat,id,head)

ntowrite=1;

%update the header info since we are adding a new matrix.
head.nmat=head.nmat+1;
head.nrec=head.nrec+1;

%in a parallel environment only 1 node does the writing
if (labindex==1)
    if (size(mat,1)~=head.dim(1))
        error('Cant append data. Matrix doesnt have same number of rows as matrices in database');
    end
    if (size(mat,2)~=head.dim(2))
        error('Cant append data. Matrix doesnt have same number of cols as matrices in database');
    end



    %write the data
    %go to eof
    fseek(obj.fid,obj.fwidth*0,'eof');
    %pack the data
    %we create a 2d matrix to be written in column major order
    data=zeros(prod(head.dim)+1,ntowrite);

    data(1,1)=id;
    data(2:end,1)=mat(:);


    %write all the data
    fwrite(obj.fid,data,'double');
end

%write the updated header
writeheader(obj,head);

  

%update the ids
%this needs to execute on all nodes.
obj=addid(obj,id);


%make all threads wait until writing is complete
labBarrier;