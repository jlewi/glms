%function getfname=fname(obj)
%	 obj=RAccessFile object
% 
%Return value: 
%	 fname=obj.fname 
%
function fname=getfname(obj)
	 fname=obj.fname;
