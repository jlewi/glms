%function [ids]=readmatids(obj)
%       ids - ids associated with matrixes stored in the file
%
%Explanation: reads the ids associated with the record ins the file
%   some of the ids could be -1 indicating the record has been deleted
function ids=readmatids(obj)

head=getheader(obj);

%goto position of first trial
fseek(obj.fid, offset(obj,1,head.dim), 'bof');


%read just the indexes of the matrixes stored in the file
%but read all of them
skip=prod(head.dim)*obj.fwidth;
ids = fread(obj.fid, inf, 'double',skip);
