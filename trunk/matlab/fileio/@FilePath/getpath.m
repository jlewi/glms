%function p=getpath(obj)
%
%Explantion evaluates the path for this object
%

function p=getpath(obj)


     bcmd=evalbcmd(obj);
    p=fullfile(bcmd,obj.rpath);
end