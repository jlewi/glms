%function exist(fobj)
%
%Explantion: check if the file exists
function v=exist(fobj)
    v=exist(getpath(fobj),'file');