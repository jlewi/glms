%function writedataset(fname,data,info)
%   fname - file to write dataset to
%   data  - a structure array of field name value paris
%         - comment - a comment to place before the dataset
%
%   info  - field structure array
%         - written as comments in header of file
%
%Explanation: This creates a matlab function which can be executed
%   to create an array which specifies the parameters for a simulation
%
%Revision:
%   09-28-2008
%       the array created can be two dimensional
%   08-01-2008
%       Create functions instead of scripts
function writeminit(fname,dsets,info)

if isa(fname,'FilePath')
    fname=getpath(fname);
end
if exist(fname,'file')
    error('File already exists');
end

fid=fopen(fname,'w');

[fdir,funcname,fext]=fileparts(fname);

fprintf(fid,'function [dsets]= %s() \n',funcname);
fprintf(fid,'%%********************************\n');

if ~isempty (info)
fprintf(fid,'%%********************************\n');
fprintf(fid,'%%Data set info\n');
fprintf(fid,'%%********************************\n');
fnames=fieldnames(info);
for ind=1:length(fnames)
    if ~isstr(info.(fnames{ind}))
        data=num2str(info.(fnames{ind}));
    else
        data=info.(fnames{ind});
    end
   fprintf(fid,'%% %s=%s \n',fnames{ind},data);
end
end

fprintf(fid,'\n');
fprintf(fid,'\n');


fnames=fieldnames(dsets);

fprintf(fid,'dsets=[];\n');
%loop through the data set


for rind=1:size(dsets,1)
    for cind=1:size(dsets,2)
    %print a comment if there is one
    if (isfield(dsets(rind,cind),'comment') && ~isempty(dsets(rind,cind).comment))
       fprintf(fid,'%%************************************************\n');
       comment=dsets(rind,cind).comment;
       comment=regexp(comment,'\n','split');
       for j=1:length(comment)
          fprintf(fid,'%% %s \n',comment{j}); 
       end
       fprintf(fid,'%%********************************************\n');
    end
    fprintf(fid,'rind=%d;\n',rind);
    fprintf(fid,'cind=%d;\n',cind);
    for k=1:length(fnames)
        if ~strcmpi(fnames(k),'comment')
        if isa(dsets(rind,cind).(fnames{k}),'FilePath')
              fprintf(fid,'dsets(rind,cind).%s=FilePath(''bcmd'',''%s'',''rpath'',''%s'',''isdir'',%d);\n',fnames{k},getgvar(getbcmd(dsets(rind,cind).(fnames{k}))),getrpath(dsets(rind,cind).(fnames{k})),isdir(dsets(rind,cind).(fnames{k})));
        elseif isnumeric(dsets(rind,cind).(fnames{k}))
             if ~isempty(dsets(rind,cind).(fnames{k}))
               fprintf(fid,'dsets(rind,cind).%s=%d;\n',fnames{k},dsets(rind,cind).(fnames{k}));
             else
                              fprintf(fid,'dsets(dind).%s=[];\n',fnames{k});
             end
        elseif isempty(dsets(rind,cind).(fnames{k}))
             fprintf(fid,'dsets(rind,cind).%s=[];\n',fnames{k});
        else
               fprintf(fid,'dsets(rind,cind).%s=''%s'';\n',fnames{k},dsets(rind,cind).(fnames{k}));
        end
        end
    end
    
    %*************************************************
    %also print the setup file
    %************************************************
    fprintf(fid,'dsets(rind,cind).setupfile=[mfilename(''fullpath'') ''.m''];\n');
    fprintf(fid,'\n\n');
    
    end
end

fclose(fid);