%04-05-2008
%
%Test harness for RemtoeFilePath

bcmd.bayes='/home/jlewi/allresults';
bcmd.cluster='/home/jlewi/dataresults';

rfile=RemoteFilePath('bcmd',bcmd,'rpath','test.mat');

hostid=fieldnames(bcmd);

for j=1:length(hostid)
   if ~(strcmp(fullfile(bcmd.(hostid{j}),getrpath(rfile)),getpath(rfile,hostid{j})))
       error('Path is incorrect');
   end
end

fprintf('Success \n');