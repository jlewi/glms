%function params=testraccessgendata(dim,nmat)
%
%Explanation: generates some fake data for the tests
%   if dim and nmat are empty then we randomly generate these values
%
%Return value
%   params.dim
%   params.nmat
%   params.mat
%   params.ids
function params=testraccessgendata(dim,nmat)
%generate some data
params=[];
if labindex==1
    if ~isempty(dim)
        data.dim=dim;
        params.dim=dim;
    else
        data.dim=ceil(rand(1,2)*100);
        params.dim=data.dim;
    end

    if ~isempty(nmat)
        data.nmat=nmat;
        params.nmat=data.nmat;
    else
        %nmat is at least 2
        data.nmat=ceil(rand(1)*100)+1;
        params.nmat=data.nmat;
    end
    
    for j=1:params.nmat
        nums=(j-1)*prod(params.dim)+1:(j)*prod(params.dim);
       params.mat{j}=reshape(nums,params.dim);
       params.ids(j)=j;
    end
end


params=labBroadcast(1,params);