
%**************************************************************************
%function verifyheader(robj, head)
%   robj - Raccessfile object
%   head - the values the header should have
%        - if htrue does not have field fver than we don't check fver
% return value
%   true - header is valid
%   false - header is not valid
%Explanation: checks if the header is accurate
function vm=verifyheader(robj,htrue)
vm=true;
%get the header from the file
hinfile=getheader(robj);

if ~isfield(htrue,'fver')
    fprintf('verifyheader: not checking fver \n');
else
    if (hinfile.fver~=htrue.fver)
        fprintf('Error: fver in header is not correct. \n');
        vm=false;
    end
end

if (hinfile.nrec~=htrue.nrec)
    fprintf('Error: nrec in header is not correct. \n');
    vm=false;
end

if (hinfile.nmat~=htrue.nmat)
    fprintf('Error: in file nmat=%d but value should be=%d in header is not correct. \n',hinfile.nmat,htrue.nmat);
    vm=false;
end

if any(hinfile.dim~=colvector(htrue.dim))
    fprintf('Error: dim in header is not correct. \n');
    vm=false;
end

if ~(vm)
    error('Header is not correct');
end
