%function gvar=getgvar(obj)
%	 obj=PathGlobalVar object
% 
%Return value: 
%	 gvar=obj.gvar 
%
function gvar=getgvar(obj)
	 gvar=obj.gvar;
