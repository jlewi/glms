%function sim=PathGlobalVar(vname)
%   vname - name of the global variable representing the path
%
%$Revision$
%Revisions:
%   080829 - Convert to new OOP model
classdef (ConstructOnLoad=true) PathGlobalVar< FPathBaseCMD
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    %gvar      - the name of the global variable representing the path
    %declare the structure
    properties(SetAccess=private,GetAccess=public)
        version=080829;
        gvar='';
    end
    methods
        function obj=PathGlobalVar(varargin)

    

            %*****************************
            %call superclass constructor
            bparams=[];
            obj=obj@FPathBaseCMD(bparams);

            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required
                    params=[];

                case 1
                    obj.gvar=varargin{1};
            end


        end
    end
end
