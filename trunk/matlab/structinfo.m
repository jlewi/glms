%function sinfo=structinfo(s)
%   s - structure whos information we would like to get
%
% Return value
%   sinfo - 2d cell array
%         col 1 - names of fields of s
%         col 2 - string describing values of s
%
%Explanation:
%   I use this to create a table which describes an object
%   If its an object call the function objinfo - if this generates an error
%       then we print a message saying no info
%
%Revisions:
%   12-07-2007 - added function varinfo to get the info about a variable
%                   varinfo should probably be the higher funcition
%             - added ability to recursively process cell arrays
%   10-29-2007 - Fixed bugs with print out of sizes of arrays
function sinfo=structinfo(s)

fnames=fieldnames(s);
fnames=sortrows(fnames);
sinfo=cell(length(fnames),2);

for j=1:length(fnames)
    sinfo{j,1}=fnames{j};

    d=s.(fnames{j});
    sinfo{j,2}=varinfo(d);

end

%funcition which actual gets the string or value to store for this entry
function vinfo=varinfo(v)
if islogical(v)
    if (v)
        vinfo='true';
    else
        vinfo='false';
    end
elseif isnumeric(v)
    if isempty(v)
        vinfo='[]';
    else
        if (numel(v)>1)
            %array to big to print just print dimensions
            dsize=size(v);
            vinfo=sprintf('Size: %2.4g',dsize(1));
            for dind=2:length(dsize)
                vinfo=sprintf('%s x %2.4g',vinfo,dsize(dind));
            end

        else

            vinfo=v;
        end
    end
elseif isobject(v)
    try
        vinfo=objinfo(v);
    catch
        vinfo=sprintf('%s: noinfo',class(v));
    end
elseif isstruct(v)
    %recursively process
    vinfo=structinfo(v);
elseif ischar(v)
    vinfo=v;
elseif iscell(v)
    %if its a cell array, recursivel process it
    vinfo=cell(size(v));
    for ind=1:numel(v)
        vinfo{ind}=varinfo(v{ind});
    end
    
elseif  isa(v,'function_handle')
    vinfo=sprintf('@%s',func2str(v));
else
    vinfo='no. info.';
end