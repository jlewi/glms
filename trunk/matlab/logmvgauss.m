%function p=mvgauss(X,MU,SIGMA)
%
%   X      - (dxn) vector
%               -n different vectors to compute probability of
%   MU     - (dx1) vector
%   SIGMA  - (dxd) covariance matrix
%
%   Explanation:
%       Computes the log probability of X assuming a multivariate gaussian
%       distribution
%       with mean=mu and covarance matrix specified by sigma
%
%
%11-2005
%   -modified it so that x is now dxn
function p=logmvgauss(x,mu,sigma)
    dim=size(x,1);  %dimensionality
    nx=size(x,2);       %number of x
    %compute the eigendecomposition of sigma
    %we use this to compute the inverse and the determinant
    [evec,eigd]=svd(sigma);
    eigd=diag(eigd);
    dx=x-mu*ones(1,nx);

    %compute -1/2 (x-mu)'inv(sigma)*(x-mu)
    %inv(sigma)=evec*diag(1./eigd)*evec'
    %projext (x-mu) onto the eigenvectors and square the value along each
    %eigenvect
    p=(evec'*dx).^2;
    %columns of p will stor -1/2*eigd^-1*evec'*dx
    %for different x
    %now scale each component by -1/2 *1/eigd
    p=((-1/2)*(1./eigd)*ones(1,nx)).*p;
    %now sum across rows
    p=sum(p,1);
    
    %p=(-1/2)*inv(sigma)*dx;
    %p=p.*(dx);
    %p=sum(p,1);
    
    %p=(-1/2)*(x-mu)'*inv(sigma)*(x-mu);

    %compute the log of the determinant
    ldetc=sum(log(eigd));
    %p=log((2*pi)^(-size(x,1)/2)*(det(sigma))^(-1/2))+p;
    p=(-dim/2)*log(2*pi)-1/2*ldetc+p;
    