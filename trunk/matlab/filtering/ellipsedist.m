%ellipsedist
%
%   ellipseimg=ellipsedist(center,axisratio,angle, imgwidth,imgheight)
%
%   axisratio - specifies the rato between the major and minor axis
%               i.e majoraxis=axisratio * minoraxis
%   returns a matrix which stores the size of the major axis of ellipse each point would lie on
%   ellipse is centered at position center, with sizes of major and minor axis specified by major,minor
%   imgwidth,imgheight specify the size of the image to be returned
%   The ellipse is rotated so that major axis is perpindicular to the angle specified by angle.
%   angle should be specified in radians.
%
%   Note: The upper left corner of the image is position 0,0 
%         positions are specified in [y, x] Y increases going down 
%         so its consitent with coordinate systems as used by imshow
function results= ellipsedist(center,axisratio,angle,imgwidth,imgheight)

    %construct the rotation matrix to rotate an ellipse with major axis aligned with x-axis 
    %so that major axis is perpindicular to angle
    %rotatem=[cos(angle-pi/2) -sin(angle-pi/2); sin(angle-pi/2) cos(angle-pi/2)];

    %determine which dimension is larger. Do this because we must use square matrices b\c we do element by element multiplication
    %later we will truncate
    len=max([imgheight imgwidth]);
    %make the xvalues range  from -floor(imgwidth/2)-ceil(imgwidth/2)
    %x=-ceil(len/2)+1:ceil(len/2);
    x=0:len-1;
    
    %in images y=0 is upper left corner therefore y decreases in order for image to be properly displayed
    y=0:len-1;
    
    x=x' * ones(1,len);    y=ones(len,1)*y;
    
    x=x-center(1);
    y=y-center(2);
    %so x,y are matrices of dimensions = dimensions of output image 
    %value of x is the x-coordinate of the position at that point in the image, after coordinate system is recentered at center of ellipse
    
    %equation for ellipse is
    % [x y] Q Lambda Q^T [x; y]  
    %see section 6.5 on positive Definite Matrices in gilbert strangs book on linear algebra
    %lambda is eigenvalues where 1/sqrt(lambda1)=major axis  1/sqrt(lambda2)=minoraxis
    %Q is the rotation matrix
    
    %if you work this out you get the following expression==1 defining the ellipse
    %therefore 
    %compute this value for every pt. If the result =1 it belongs on the surface of the ellipse
    %pheta=angle-pi/2;
    %pi/2 has already been subtracted if it needs to be
    pheta=angle;
    %l1=(1/major)^2;
    %l2=(1/minor)^2;
    
    %
    results=x.^2 *((cos(pheta))^2 + (axisratio*sin(pheta))^2 ) + x .* y *sin(2*pheta)*(1 -axisratio^2) + y.^2*((sin(pheta))^2 +(axisratio*cos(pheta))^2);

    %the number at each point is just the major axis squared so take square rootit
    results=results.^(1/2);
    %now results stores value of major axis at every point

                        
    %ellipseimg is structured in the same way as rgb images. y coordinate first
    %so we will have to transpose the results
    results=results';
    %truncate the image, so that final width=imgwidth, imgheight
    %the upper left corner should correspond to position 0,0
    results=results(1:imgheight,:);
    results=results(:,1:imgwidth);
    
    