%setpaths
%to make it automattically platform independent of path separator character
%use fullfile to concate nate paths or else use the filesep command
%
% To add a path recursively use
% dtoadd=genpath(d)
% this creates a string which includes all the directories in d

PARENT=pwd;
LIBPATH=fullfile(PARENT,'lib');
SCRIPTS=fullfile(PARENT,'scripts');
%FIGURES=[PARENT '\figures'];
DATADIR=fullfile(PARENT, 'data');
PCDX=fullfile(PARENT,'\lib\pcdx');
    
paths={LIBPATH,PCDX,SCRIPTS};

cpaths=path;
for index=1:length(paths)
    r=strfind(cpaths,paths{index});
    if isempty(r)
        addpath(paths{index});
    end
end
