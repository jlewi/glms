%function qp= qprod(v,m)
%   v- dxn - matrix of column vectors
%   m - dxd - matrix
%
% qp = vector whose values are v(:,i)'*m*v(:,i)
function qp=qprod(v,m)

    qp=m*v;
    qp=v.*qp;
    qp=sum(qp,1);