/*************************************************************
Author: Jeremy Lewi
Date: 08-08-2007
Explanation: Header file for routines implementing the Gu-Eisenstat algorithm.
****************************************************************/
#ifndef RANKONEHEADER
  #define RANKONEHEADER


void deflate (int neig, double eigd[], double evecs[], double z[], double rho,
	double threshold, int sdir, double deigd[], double devecs[], double dz[], double* ndeflate);

#endif
