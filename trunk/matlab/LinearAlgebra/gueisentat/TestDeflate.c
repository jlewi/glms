// Purpose of this script is to test the function mexDeflate.c
//reads in matrices from the file testdeflate.mat

#include "mex.h"
#include "mat.h"
//#extern 

//declaration for the mexfunction
void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]);

int main(){
  
  	//integer indicating whether to continue
	int cont=1;
  //pointer to structure array containing the data to test
  mxArray* testdata; 	
  MATFile* fptr;
  char fname[]="testdeflate.mat";
  char vname[]="data";	//name of variable in .mat file
  fptr=matOpen(fname, "r");
  if (fptr!=NULL){
  	testdata=matGetVariable(fptr, vname);
  	if (testdata==NULL){
  		printf("Could not load %s from file. \n Unable to continue. \n", vname);
  		matClose(fptr);
  		exit(1);
  	}
  	
  	//index into testdata structure
  	int index=0;
  	//test data should be a structure with the following fields
  	//.eigd, .evecs, .sdir, .rho, threshold,
  	mxArray* eigd=mxGetField(testdata, index,"eigd");
  	mxArray* evecs=mxGetField(testdata, index,"evecs");
  	mxArray* rho=mxGetField(testdata,index,"rho");
  	mxArray* threshold=mxGetField(testdata, index,"threshold");
  	mxArray* sdir=mxGetField(testdata, index,"sdir");
  	mxArray* vup=mxGetField(testdata, index,"vup");
  	
  	if (eigd==NULL){
  		printf("eigd is null \n");
  		cont=0;	
  	}
  	if (evecs==NULL){
  		printf("evecs is null \n");
  		cont=0;	
  	}
  	if (threshold==NULL){
  		printf("threshold is null \n");
  		cont=0;	
  	}
  	if (sdir==NULL){
  		printf("sdir is null \n");
  		cont=0;	
  	}
  	if (vup==NULL){
  		printf("vup is null \n");
  		cont=0;	
  	}
  
  	if (cont==1){
  		int nlhs=4;
  		int nrhs=6;
  		mxArray* plhs[nlhs];
  		mxArray* prhs[nrhs];
  		prhs[0]=eigd;
  		prhs[1]=evecs;
  		prhs[2]=vup;
  		prhs[3]=rho;
  		prhs[4]=threshold;
  		prhs[5]=sdir;
  		
  		mexFunction(nlhs, plhs,nrhs, prhs);
  		
  	}
  	matClose(fptr); 
  		
  	  
  }
  else{
  	printf("Could not open file %s \n",fname);
  }
  
}