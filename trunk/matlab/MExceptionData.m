%function MExceptionData('identifier','message')
%
%function MExceptionData('identifier','message',data)
%   identifier - identifier for this error
%   message    - message for the error
%   data       - some optional data to return
%
%Explanation: I created this subclass of MException so that I could
%   attach data to an error. This way I can throw an error in a
%   function but also return intermediary data
%   
%
classdef MExceptionData < MException
    properties(SetAccess=public,GetAccess=public)
        data=[];
    end
    
     methods
      function e = MExceptionData(varargin)
         
          e = e@MException(varargin{1},varargin{2}); % call asset constructor
         if (nargin==3)
         e.data=varargin{3};
         end
      end
      
     end
end
