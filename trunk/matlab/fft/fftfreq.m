%11-26-2007
%
%Script to show proper computation of frequency for fft;

%frequency of the sinewave
%
period=[1 5 10];
A=[2 4 6];
freq=1./period;

%we need the duration to be equal to the period of the signal
%create a 1hz sinewave, 1 sec in duration
duration=period(1);
for j=2:length(freq);
    duration=lcm(duration,period(j));
end
%sampling frequency
Fs=100; %in hz

%sampling period
dt=1/Fs;

%data
t=0:dt:duration;

 
y=zeros(1,length(t));
for j=1:length(freq);
    omega=2*pi*freq(j);
    y=y+A(j)*sin(omega*t);
end




figure;
plot(t,y);

%**************************************
%compute and plot the fft
%subtract 1 from number of samples b\c we have to include the zero frequency
N=(length(y));
%radians per sample
dw=2 *pi/(N-1)/dt;
wmax=pi/dt;
w=-wmax:dw:wmax;

%now compute 
figure
 yfft=fft(y,N);
 freq=w./(2*pi);
 
 %we need to multiply by 2 so that the magnitude matches the coefficient in
 %the temporal domain.
 plot(freq,2*abs(fftshift(yfft))/N,'o');