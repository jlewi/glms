function B = eval_pdf_cond_gauss(data, mu, Sigma)
% COMP_LOCALEV_GAUSS Compute local evidence using Gaussian distribution
% B = comp_localev_gauss(data, mu, Sigma)
%
% Inputs:
% data(:,t) = observation vector at node t (can also be data(:,i,j) for 2D lattices)
% mu(:,j) = E[Y(t) | Q(t)=j]
% Sigma(:,:,j) = Cov[Y(t) | Q(t)=j]
%
% Output:
% B(i,t) = Pr(y(t) | Q(t)=i)

Q = size(mu, 2);
B = eval_pdf_cond_mixgauss(data, mu, Sigma, ones(Q,1));

if 0
[O T] = size(data);
B = zeros(Q,T);
for j=1:Q
  B(j,:) = gaussian_prob(data, mu(:,j), Sigma(:,:,j))';
end
end
