function [mu, Sigma, mixmat] = init_cond_mixgauss(Q, M, data, cov_type)
% INIT_COND_MIXGAUSS Initial parameter estimates for a conditional mixture of Gaussians
% function [mu, Sigma, mixmat] = init_cond_mixgauss(Q, M, data, cov_type)
%
% We use K-means (with the centres chosen from the data points).
% Needs netlab.
%
% INPUTS:
% data{ex}(:,t) or data(:,t,ex) if all sequences have the same length
% Q = num. hidden states
% M = num. mixture components
% cov_type = 'full', 'diag' or 'spherical'
%
% OUTPUTS:
% mixmat(j,k) = Pr(M(t)=k | Q(t)=j) where M(t) is the mixture component at time t
% mu(:,j,k) = mean of Y(t) given Q(t)=j, M(t)=k
% Sigma(:,:,j,k) = cov. of Y(t) given Q(t)=j, M(t)=k
%


if iscell(data)
  data = cat(2, data{:});
  O = size(data, 1);
else
  [O T nex] = size(data);
  data = reshape(data, [O T*nex]);
end

if 0
  Sigma = repmat(eye(O), [1 1 Q M]);
  % Initialize each mean to a random data point
  indices = randperm(T);
  mu = reshape(data(:,indices(1:(Q*M))), [O Q M]);
end

% Initialize using K-means, where K = Q*M
% We should really segment the sequence uniformly into Q strips,
% and run M-means on each segment.
mix = gmm(O, Q*M, cov_type);
options = foptions;
max_iter = 5;
options(14) = max_iter;
mix = gmminit(mix, data', options);
mu = reshape(mix.centres', [O Q M]);
mixmat = mk_stochastic(reshape(mix.priors, [Q M]));
i = 1;
for q=1:Q
  for m=1:M
    switch cov_type
      case 'diag',
	Sigma(:,:,q,m) = diag(mix.covars(i,:));
      case 'full',
	Sigma(:,:,q,m) = mix.covars(:,:,i);
      case 'spherical',
	Sigma(:,:,q,m) = mix.covars(i) * eye(O);
    end
    i = i + 1;
  end
end
