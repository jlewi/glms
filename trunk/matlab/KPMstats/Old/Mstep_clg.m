function [mu, Sigma, B] = Mstep_clg(varargin)
% MSTEP_CLG Compute ML/MAP estimates for a conditional linear Gaussian
% [mu, Sigma, B] = Mstep_clg(...)
%
% We assume
% P(Y|X,Q=i) = N(Y; B_i X + mu_i, Sigma_i) where
% w(i,t) = p(M(t)=i|y(t)) = posterior responsibility
% See www.ai.mit.edu/~murphyk/Papers/learncg.pdf.
%
% All inputs must be passed as 'param_name', param_value pairs.
%
% INPUTS:
% w(i) = sum_t w(i,t) = responsibilities for each mixture component
%  If there is only one mixture component (i.e., Q does not exist),
%  then w(i) = N = nsamples,  and 
%  all references to i can be replaced by 1.
% YY(:,:,i) = sum_t w(i,t) y(:,t) y(:,t)' = weighted outer product
% Y(:,i) = sum_t w(i,t) y(:,t) = weighted observations
% YTY(i) = sum_t w(i,t) y(:,t)' y(:,t) = weighted inner product
%   You only need to pass in YTY if Sigma is to be estimated as spherical.
%
% In the regression context, we must also pass in the following
% XX(:,:,i) = sum_t w(i,t) x(:,t) x(:,t)' = weighted outer product
% XY(i) = sum_t w(i,t) x(:,t)' y(:,t) = weighted cross product
% X(:,i) = sum_t w(i,t) x(:,t) = weighted inputs
%
% Optional inputs (default values in [])
%
% 'cov_type' - 'full', 'diag' or 'spherical' ['full']
% 'tied_cov' - 1 (Sigma) or 0 (Sigma_i) [0]
% 'tied_mean' - 1 (mu) or 0 (mu_i) [0]
% 'tied_weights' - 1 (B) or 0 (B_i) [0]
% 'clamped_cov' - pass in clamped value, or [] if unclamped [ [] ]
% 'clamped_mean' - pass in clamped value, or [] if unclamped [ [] ]
% 'clamped_weights' - pass in clamped value, or [] if unclamped [ [] ]
% 'cov_prior_type' - 'none' or 'wishart' ['wishart']
% 'cov_prior' - matrix [0.1*N*eye(d,d,Q)]
%
% If parameters are tied, the last dimension (i) will be omitted.
% But diagonal and spherical covariances are represented in full size.

[w, YY, Y, YTY, XX, XY, X, cov_type, tied_cov, tied_mean, tied_weights, ...
clamped_cov, clamped_mean, clamped_weights, ...
cov_prior_type, cov_prior] = ...
    process_options(varargin, 'w', [], 'YY', [], 'Y', [], 'YTY', [], ...
		    'XX', [], 'XY', [], 'X', [], ...
		    'cov_type', 'full', 'tied_cov', 0, 'tied_mean', 0, ...
		    'tied_weights', 0, 'clamped_cov', [], 'clamped_mean', [], ...
		    'clamped_weights', [], 'cov_prior_type', 'wishart', ...
		    'cov_prior', []);
[d Q] = size(Y);
N = sum(w);
if isempty(cov_prior) & strcmp(cov_prior_type(1), 'w')
  cov_prior = 0.1*N*eye(d,d,Q); 
end
Ysz = size(Y,1);
if isempty(X)
  Xsz = 0;
else
  Xsz = size(X,1);
end

% Estimate B
if isempty(X)
  B = [];
else
  if tied_weights
    B = zeros(Xsz, Ysz);
  end
end

  
  

mu = zeros(O,Q);
Sigma = zeros(O,O,Q);
for i=1:Q
  mu(:,i) = m(:,i) / weights(i);
  if cov_type(1) == 's'
    s2 = (1/O)*( (ip(i)/weights(i)) - mu(:,i)'*mu(:,i) );
    Sigma(:,:,i) = s2 * eye(O);
  else
    SS = op(:,:,i)/weights(i) - mu(:,i)*mu(:,i)';
    if cov_type(1)=='d'
      SS = diag(diag(SS));
    end
    Sigma(:,:,i) = SS;
  end
end
