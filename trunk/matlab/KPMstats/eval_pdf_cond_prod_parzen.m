function B = eval_pdf_cond_prod_parzen(data, mu, Sigma, N)
% EVAL_PDF_COND_PARZEN Evaluate the pdf of a conditional Parzen window
% function B = eval_pdf_cond_parzen(data, mu, Sigma, N)
%
% B(q,t) = Pr(data{1:C}(:,t) | Q=q)
%        = (1/N(q)) sum_{m=1}^{N(q)} prod_{c=1}^C K(data{c}(:,t) - mu{c}(:,q,m); Sigma(c))
% where K() is a Gaussian kernel with variance Sigma
% and N(q) is the number of mxiture components for case q (defaults to size(mu{1},3) if omitted)
%
% This is optimized for the case Q << min(T,M)

C = length(data);
[d1 Q M] = size(mu{1});
[d1 T] = size(data{1});

if nargin < 4, N = repmat(M, 1, Q); end

for c=1:C
  d = size(mu{c},1);
  const1(c) = (2*pi*Sigma(c))^(-d/2);
  const2(c) = -(1/(2*Sigma(c)));
end

B = zeros(T,Q);
for q=1:Q
  Dall = ones(T,N(q)); % Dall(t,m)
  for c=1:C
    D = sqdist(data{c}, permute(mu{c}(:,q,1:N(q)), [1 3 2])); % D(t,m)
    % permute is faster than squeeze, and does not convert row vectors into column vectors
    Dall = Dall * const1(c) .* exp(const2(c) * D);
  end
  B(:,q) = (1/N(q))*sum(Dall,2);
end
B = B';
