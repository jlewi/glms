function [B, B2] = eval_pdf_cond_mix_laplace(data, mu, Sigma, mixmat)
% EVAL_PDF_COND_MOG Evaluate the pdf of a conditional mixture of Laplacians
% function [B, B2] = eval_pdf_cond_mog(data, mu, Sigma, mixmat)
%
% Notation: Y is observation, M is mixture component, and both are conditioned on Q.
%
% INPUTS:
% data(:,t) = t'th observation vector 
% mu(:,j,k) = E[Y(t) | Q(t)=j, M(t)=k]
% Sigma(:,:,j,k) = Cov[Y(t) | Q(t)=j, M(t)=k]
% Currently must be a scalar
% mixmat(j,k) = Pr(M(t)=k | Q(t)=j) 
%
% OUTPUT:
% B(i,t) = Pr(y(t) | Q(t)=i)
% B2(i,k,t) = Pr(y(t) | Q(t)=i, M(t)=k)
%
%
% Set mixmat to ones(Q,1) if there is only 1 mixture component


[Q M] = size(mixmat);
[O T] = size(data);

%B2 = zeros(Q,M,T);
B2 = ones(Q,M,T);
B = zeros(Q,T);

assert(isscalar(Sigma))

mu = reshape(mu, [O Q*M]);
D = zeros(Q*M,T);
for t=1:T
  D(:,t) = sum(abs(reshape(mu, [O Q*M]) - repmat(data(:,t), [1 Q*M])))';
end
B2 = reshape(exp(-D/Sigma), [Q M T]);

% B(j,t) = sum_k B2(j,k,t) * Pr(M(t)=k | Q(t)=j) 
B = squeeze(sum(B2 .* repmat(mixmat, [1 1 T]), 2));
B = reshape(B, [Q T]); % in case Q = 1

