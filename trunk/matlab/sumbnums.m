%
%Explanation: sums a bunch of numbers
%. To avoid numerical issues
%       1. we sum the elements like in a binary tree we do this 
%           to avoid summing numbers of different magnitudes to try to
%           avoid numerical error which would happen if you had many
%           numbers and did a running sum

function tot=sumlnums(sdata)
num=length(sdata);

%number of passes we need to make through the data
nlevels=ceil(log2(num));

nelements=num;
for l=1:nlevels
    
    
    %number of summations we need to do
    nsums=floor(nelements/2);
    for sind=0:(nsums-1)
        %we sum the elements at 2*sind and 2*sind+1
        %and store at position sind
        s=sdata(2*(sind)+1)+sdata(2*(sind)+2);
        sdata(sind+1)=s;
    end
    %check if there is an odd element
    if (mod(nelements,2)==1)
        sdata(nsums+1)=sdata(nelements);
    end
    
    %update number of elements remaining to be summed
    nelements=floor(nelements/2)+mod(nelements,2);
end
tot=sdata(1);
