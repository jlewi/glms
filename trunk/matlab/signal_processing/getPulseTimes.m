%function ptimes=getPulseTimes(istim,ithresh)
%       istim   - stimulation current
%       ithresh - threshold for determining start of current
%               if <0 we look for negative pulses
%                   that is istim<ithresh
%       minwidth - minimum width for each pulse (in indexes)
%                 
%Return:
%   ptimes -[nx2] matrix of start and end indexes of I pulses
%           will not work for current pulses.
%	pulsePeak - [nx1] the average value of the peak of each pulse
function [ptimes,pulsePeak]=getPulseTimes(istim,ithresh, minwidth)
%if ithresh is negative we are looking for negative pulses
%so invert ithresh and istim;
isnegative=sign(ithresh);
pulsePeak=0;
ptimes=[];

if (ithresh<0)
istim=-istim;
ithresh=-ithresh;
end

if (~exist('minwidth'))
    minwidth=0;
end

%get the time of the stimulation pulses
ind=(istim>=ithresh);
%find positive edges
di=diff(ind);
%we add 1 to pedges because the diff operator means the index
%of pedge will be associated with the value that is below threshold
%which we want to exclude
pedges=find(di==1)+1;
nedges=find(di==-1);
%make sure edges line up
%i.e it starts before it begins
k=1;

if (isempty(nedges) | isempty(pedges))
   ptimes=[];
   pulsePeak=Inf;
   return
end
while (nedges(k)<pedges(1))
    k=k+1;
    if (k>length(k))
        break;
    end
end
nedges=nedges(k:end);

%make sure each pulse has an end
if (length(nedges)<length(pedges));
    %get rid of this pulse
    pedges=pedges(1:length(nedges));
end

if isempty(pedges)
    return;
end
ptimes=zeros(length(pedges),2);
ptimes(:,1)=pedges;

%end times

%compute the 
ptimes(:,2)=nedges;

%throw out all pulses shorter in duration than minwidth
pw=ptimes(:,2)-ptimes(:,1);
ind=find(pw>=minwidth);
ptimes=ptimes(ind,:);

%plot the pulses
if (size(ptimes,1)>0)
%compute the magnitude of the pulses
pulsePeak=zeros(size(ptimes,1),1);

for index=1:size(ptimes,1)
    pulsePeak(index)=mean(istim(ptimes(index,1):ptimes(index,2)));
end

%figure;
%hold on;
%plot(istim);
%for index=1:size(ptimes,1);
%    pulsePeak(index)=max(istim(ptimes(index,1):ptimes(index,2)));
%    plot(ptimes(index,1):ptimes(index,2),isnegative*istim(ptimes(index,1):ptimes(index,2)),'g');
%end

%pulsePeakAvg=pulsePeak/size(ptimes,1);
%check each pulse has max at least .9 of the PulsePeak
%ind=find(pulsePeak>=.9*pulsePeakAvg);
%ptimes=ptimes(ind,:);
%pulsePeak=isnegative*mean(pulsePeak(ind));
%title('Detected Pulses');

end
