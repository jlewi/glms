%function lblgraph(fig)
%   fig - structure which stores values to attach to graph
%          .hf - handle to graph
%          .name  - name to appear in figure titlebar
%          .fontsize - if this is supplied it
%                      overrides all other font settings and forces all
%                      objects to have that font
%   More than 1 subplot
%   fig.a - array of AxesObj
%
%   Previous Version
%       .a was a structure with following fields
%        - if graph has more than one axes
%          following fields should be fields of cell array fig.a
%               (use cell array so that entries don't have to have same
%               fields')
%           and should also have field ha
%          .lbls - lbls for legend
%          .hp   - plot handles to go with legend entries
%          .xlabel  - label for x axis
%          .ylabel - label for y axis
%          .title  - graph title
%          .semilog -  make y axis semilog
%          .lgndfontsize - font size for legend
%               depreciated 090807
%          .axisfontsize - axis font size
%               depreciated
%           .ls - linestyles
%
%$Revision$ - check for fields .width and .height use them to set
%   figure width if not use default values. This is useful for making fonts
%   show up correctly. This will cause problems with some of my other code
function [fig]=lblgraph(fig)

%list of fields which belong to the figure object and not 
%any of the sub structures
ffields={'hf','name','fontsize', 'width','height'};
copyaxes=0;

figure(fig.hf);
  if isfield(fig,'name')
        set(fig.hf,'name',fig.name);
  end
    
  
  
%********************************
%number of axes in graph
%***************************
if ~isfield(fig,'a')
    naxes=1;
    copyaxes=1;
else
    naxes=length(fig.a);
end

if (copyaxes==0)
    if ~isfield(fig,'a')
        error('if figure has multiple axes. Properties for each axis must be fields of structure array a');
    end
    
else
    figure(fig.hf);
    %haxes=gca;
    a=fig;
    fig.a{1}=a;
    fig.a{1}.ha=gca;
    %remove fields which are for the figure
    for j=1:length(ffields)
     if isfield(fig.a{1},ffields(j))
         fig.a{1}=rmfield(fig.a{1},ffields(j));
     end
    end
    
    %remove axes fields from figure
    afields=fieldnames(fig.a{1});
        for j=1:length(afields)
     if isfield(fig,afields(j))
         fig=rmfield(fig,afields(j));
     end
    end
    
    
end

for aind=1:naxes
    axes(fig.a{aind}.ha);
% 
%     fig.a{aind}=checkdef(fig.a{aind},'lgndfontsize',16);
%     fig.a{aind}=checkdef(fig.a{aind},'axisfontsize',16);    
    fig.a{aind}=checkdef(fig.a{aind},'linewidth',4);
 

    %use times because I don't think times-roman is scalable
    %03-16-2008
    %use helvetica because thats neural comp wants a sans serif
%    set(gca,'FontName','Times-Roman')
    set(gca,'FontName','Helvetica')
%     if isfield(fig.a{aind},'axisfontsize')
%         set(gca,'FontSize',fig.a{aind}.axisfontsize);
%     end
    if isfield(fig.a{aind}, 'lbls') 

        if isfield(fig,'fontsize')
       hl=legend(fig.a{aind}.hp,fig.a{aind}.lbls,'FontSize',fig.fontsize);
       
    %    hl=legend(hp,lbls);
        fig.a{aind}.hlgn=hl;
        end
    end

    if (isfield(fig.a{aind},'hp') & isfield(fig.a{aind},'ls'))
       for j=1:length(fig.a{aind}.hp)
           try
          setlstyle(fig.a{aind}.hp(j),fig.a{aind}.ls{j});
           catch
              fprintf('Count not set line style \n');
           end
       end
    end
    if isfield(fig.a{aind},'markersize')
        if isfield(fig.a{aind},'hp')
        nplots=length(fig.a{aind}.hp);
        for index=1:nplots
            set(fig.a{aind}.hp(index),'MarkerSize',fig.a{aind}.markersize);
        end
        end
    end

    if isfield(fig.a{aind},'linewidth')
        %linewidth will be set to false if we don't wantit to be set
        if (fig.a{aind}.linewidth>0)
        if isfield(fig.a{aind},'hl')
            nplots=length(fig.a{aind}.hl);
            for index=1:nplots
                set(fig.a{aind}.hl(index),'Linewidth',fig.a{aind}.linewidth);
            end
        end
        end
    end

    if isfield(fig.a{aind},'xlabel')
        fig.a{aind}.hxlabel=xlabel(fig.a{aind}.xlabel);
    end
    if isfield(fig.a{aind},'ylabel')
        fig.a{aind}.hylabel=ylabel(fig.a{aind}.ylabel);
    end
    if isfield(fig.a{aind},'title')
        fig.a{aind}.htitle=title(fig.a{aind}.title);
    end
  

    if (isfield(fig.a{aind},'semilog'))
        if (fig.a{aind}.semilog~=0)
            set(gca,'YScale','Log');
        end
    end

    if (isfield(fig.a{aind},'semilogx'))
        if (fig.a{aind}.semilogx~=0)
            set(gca,'XScale','Log');
        end
    end

    %          if (isfield(fig.a{aind},'linewidth'));
    hlines=findall(fig.a{aind}.ha,'type','Line');
    set(hlines,'linewidth',fig.a{aind}.linewidth);
    %        end
end %loop over axes

%set figure size
if ~isfield(fig,'width')
    fig.width=4.5;
    fprintf('Setting figure width to 4.5 \n');
end
if ~isfield(fig,'height')
    fig.height=3;
    fprintf('Setting figure height to 3 \n');
end
setfsize(fig.hf,fig);
%*******************************************************
%Set all font sizes
%don't want field name to be case sensitive
fsize=[];
fnames=fieldnames(fig);
fi=strmatch('fontsize',lower(fnames),'exact');
if ~isempty(fi)
   
    fsize=fig.(fnames{fi});
else
         fsize=12;
end

fprintf('Setting all fonts to Helvetica, size=%d \n',fsize);
    %Set all text objects
    alltext=findall(fig.hf,'type','text');
	%set(alltext,'FontName','Times');
    set(alltext,'FontName','Helvetica');    
	set(alltext,'FontSize',fsize);
	
    %et all axes
	allaxes=findall(fig.hf,'type','axes');
	%set(allaxes,'FontName','Times');
    %Change font to helevatica for Neural comp
    set(allaxes,'FontName','Helvetica');
	set(allaxes,'FontSize',fsize);

%backwards compatibility
%if only 1 axis make all fields of fig.a fields of fig
if (copyaxes==1)
    fnames=fieldnames(fig.a{1});
    for j=1:length(fnames)
      fig.(fnames{j})=fig.a{1}.(fnames{j});  
    end
end
rmfield(fig,'a');

%if field doesn't exist or is empty
%set it to the value val
    function s=checkdef(s,fname,val)
    
    if ~isfield(s,fname)
        s.(fname)=[];
    end
    if isempty(s.(fname))
        s.(fname)=val;
    end