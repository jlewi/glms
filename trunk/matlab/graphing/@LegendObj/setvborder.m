%function obj=setvborder(obj,bottom,top)
%	 obj=LegendObj object
%       bottom - amount of space to leave on bottom to first object
%       top    - amount of space to leave on top to top of boj
%Return value: 
%	 obj= the modified object 
%
function obj=setvborder(obj,bottom,top)
	 obj.linfo.tbottom=bottom;
    obj.linfo.ttop=top;
     

     %replot the legend
     obj=plotlegend(obj);