%function autosize(obj)
%
%Explanation: automatically resize the legend. This function tries to
%squeeze whitespace more than autosize
%
function autosizetight(obj)


            %width and height will be in data units
            %we set the x/y axes to be [0,1]
            %WARNING:get the position of the axes before getting
            %the extent of the text objects b\c the extent does not
            %appear to work correctly unless we get the axes position first
            %possibly because its a dependent property
            apos=get(obj.ha,'position');
            
            %set the position so that we squezee the txt
            %get the exten of all text objects and then add some border
            %extent returns left, bottom, width, height
            extent=get(obj.htxt,'extent');
            extent=cell2mat(extent);

            %compute the width
            width=max(extent(:,3));
            
            
            
            
            %compute the height using the fontsize
            %this assumes the font size is in points where 1 point= 1/72 of
            %an inch
            fsize=get(obj.htxt(1),'fontsize');
            %convert to inches
            fsize=fsize*1/72;
            
            %convert to normalized units in terms of figure height
            %get a handle to the figure
            hf=get(obj.ha,'parent');
            fpos=get(hf,'position');
            
            fsize=fsize/fpos(4);
            
            
            %convert the width to the same units as the width of the axes
            width=width*apos(3);
            %we need to account for the fact that only (1-obj.linfo.tleft)%
            %of the width is used for the text. The other part is left for
            %the line
            newawidth=width/(1-obj.linfo.tleft);
            
            
            
            setposition(obj,[],[],newawidth,fsize*length(obj.htxt));