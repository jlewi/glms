%%function LegendObj(hlines,lbls)
%   hlines - handles to the plots whose legend we are creating
%            we use these to get the line style
%
%          - this can also be a cell array of structures
%            each structure is field name value pairs for that line
%   lbls    -lbls
%
%Revisions:
%   10-21-2008 convert to new object model
classdef (ConstructOnLoad=true) LegendObj < handle

    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %
    % hlines  - handles to the lines, plots we want to create legend for
    % lbls    - lbls to associate with the plots
    % htxt    - handles to the text objects we create to label each plot
    % ha      - axes where legend is plotted.
    %
    % linfo   - structure storing attributes of the legend
    %         .latex - use latex interpreter
    %         .tleft - the position in relative coordinates at which to start
    %         the text objects
    %               - this controls the % of the width used for the marker vs.
    %               the text
    %         .tbottom - how much space to leave on the bottom to the first
    %         text object
    %         .ttop   - how much space to leave above the top text object
    %         .voffset - this value is added to the height of the text labels
    %                  - the point is to use this value to shift the text up or
    %                  down to align it with the plot lines
    %         .fontsize
    % order   - order is the order in which to list the entries of the labels
    %           i.e it is the index into hlines of each entry which goes next.
    %declare the structure


    properties(SetAccess=private,GetAccess=public)
        version=081021;
        hlines=[];
       
        htxt=[];
        ha=[];
        linfo=struct('latex',false,'tleft',.25,'position',[],'order',[],'tbottom',nan,'ttop',nan,'voffset',0,'fontsize',10);

    end

    properties(SetAccess=public,GetAccess=public)
        lbls=[]; 
    end
    methods
        function obj=LegendObj(varargin)
            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            con(1).rparams={'hlines','lbls'};
            con(1).cfun=1;


            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    %instantiate the base class if one is required

                    return
            end

            %determine the constructor given the input parameters
            [cind,params]=constructid(varargin,con);

            %**************************************************************************
            %Cind stores the index of the appropriate constructor
            %********************************************************************
            switch cind
                case 1
                    obj.hlines=params.hlines;
                    obj.lbls=params.lbls;

                otherwise
                    error('Constructor not implemented')
            end


            %*********************************************************
            %Create the object
            %****************************************
            %instantiate a base class if there is one


            if isfield(params,'fontsize')
                obj.linfo.fontsize=params.fontsize;
            end
            
            %store the current axes so we can return the focus to it
            %make the legend

            obj=plotlegend(obj);

            %autosize the legend.
            autosize(obj);
        end
    end
end


