%function getposition=position(obj)
%	 obj=LegendObj object
% 
%Return value: 
%	 position=obj.position 
%
function position=getposition(obj)
	 position=get(obj.ha,'position');
