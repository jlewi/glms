%function setposition(obj,left,bottom,width,height)
%function setposition(obj,pos)
%Explanation: set the position of the axes containing the legend
%   pass in [] to leave that coordinate unchanged
%
%   Coordinates should in general be in normalized units
function obj=setposition(obj,left,bottom,width,height)

pos=get(obj.ha,'position');

if (nargin==2)
    pos=left;
    set(obj.ha,'position',pos);

else
if ~isempty(left)
    pos(1)=left;
end
if ~isempty(bottom)
    pos(2)=bottom;
end

if ~isempty(width)
    pos(3)=width;
end

if ~isempty(height)
    pos(4)=height;
end


end
set(obj.ha,'position',pos);
obj.linfo.position=pos;
