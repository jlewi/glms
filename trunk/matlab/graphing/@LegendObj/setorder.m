%function obj=setorder(obj,val)
%	 obj=LegendObj object
%    val= an array which specifies which entry in the items show go 1st, 2nd, etc 
%Return value: 
%	 obj= the modified object 
%
function obj=setorder(obj,val)
    if (length(val)~=length(obj.hlines))
        error('Order should be same length as hlines and lbls');
    end
    obj.linfo.order=val;
    obj=plotlegend(obj);

