%function obj=plotlegend(hlines,lbls)
%   hlines - handles to the plots whose legend we are creating
%            we use these to get the line style
%
%          - this can also be a cell array of structures
%            each structure is field name value pairs for that line
%   lbls    -lbls
%
%Explanation: Create a custom axes and manually create the legend so that
%   we can customize the lengend, i.e squeeze space and put it outside plot
%   axis
%   
%   To squeeze space shrink the size of the axes 
%
%Revision: 
% 05-14-2008
%       If a plot consists of a linestyle and a marker
%       then we plot them separatly so that we only plot a single marker
%       and not multiple markers
function obj=plotlegend(obj)



%if we've already plotted the legend delete it first
if ~isempty(obj.ha)
    %save the figure  handle
    hf=get(obj.ha,'parent');
    delete(obj.ha);
    figure(hf);
else
    hf=gcf;
end


obj.htxt=zeros(1,length(obj.hlines));
ha=axes;
obj.ha=ha;

%set the current axes
set(gcf,'CurrentAxes',ha);
set(ha,'fontsize',obj.linfo.fontsize);

if ~isempty(obj.linfo.position)
    set(ha,'position',obj.linfo.position);
end

if isempty(obj.linfo.order)
   obj.linfo.order=1:length(obj.hlines); 
end

hlines=obj.hlines(obj.linfo.order);
lbls=obj.lbls(obj.linfo.order);
%turn the box on
axis on;
set(ha,'box','on');

set(ha,'xtick',[]);
set(ha,'ytick',[]);

hold on;
%set limits to 0 1 so that everything is easy and relative coordinate
%i.e we can space the objects out evenly and then we just need to shrink
%the axes to save space
xlim([0 1]);
ylim([0 1]);

linfo=obj.linfo;
nlbls=length(lbls);
if (~isnan(linfo.tbottom) && ~isnan(linfo.ttop))
    %leave equal spacing to first and above last label
    heights=[linfo.tbottom (1-linfo.tbottom-linfo.ttop)/(nlbls-1)*ones(1,nlbls-1)];
    heights=cumsum(heights);
elseif (~isnan(linfo.tbottom))
    heights=[linfo.tbottom (1-linfo.tbottom)/(nlbls)*ones(1,nlbls-1)];
    heights=cumsum(heights);
elseif (~isnan(linfo.ttop))
    heights=[(1-linfo.ttop)/(nlbls)*ones(1,nlbls-1)];
    heights=cumsum(heights);
    %reverse heights and subtract from 1 because heights gives distance
    %from top
    heights=1-heights(end:-1:1);
else
     %leave equal spacing to first and above last label
    heights=1/(nlbls+1)*ones(1,nlbls);
    heights=cumsum(heights);
end

%reverse the order of heights because we want first entry to be at top of
%graph
heights=heights(end:-1:1);

%tleft -point at which to start the text
tleft=obj.linfo.tleft;

%which xpts to use for plotting the linestyle
xptsline=[.02:.05:tleft-.05];

%if the plot is just a marker and no line 
%then we plot a single marker in the center
xptsmarker=(tleft-.05)/2;

for pind=1:nlbls
  
    %determine if its a marker or a line
    isline=false;
    
    if iscell(hlines)
        pstyle=hlines{pind};
        if (isfield(pstyle,'LineStyle') && ~isempty(pstyle.LineStyle))
            isline=true;
        end
    else
        lstyle=get(hlines(pind),'LineStyle');
        if ~strcmp(lstyle,'none')
           isline=true; 
        end
    end
    
    %*********************************************
    %05-14-2008
    %*********************************************
    %plot the line and markers of the plot separatly
    
    %set the points to plot
   % if (isline)
  %      xpts=xptsline;
 %   else
    %    xpts=xptsmarker;
%    end
    
    yptsline=heights(pind)*ones(1,length(xptsline));
    yptsmarker=heights(pind)*ones(1,length(xptsmarker));
    %ypts=heights(pind)*ones(1,length(xpts));  

    hl=plot(ha,xptsline,yptsline);
    hm=plot(ha,xptsmarker,yptsmarker);
    if iscell(hlines)
        fnames=fieldnames(hlines{pind});
        pstyle=hlines{pind};
        for f=1:length(fnames)
            set(hl,fnames{f},pstyle.(fnames{f}));
            set(hm,fnames{f},pstyle.(fnames{f}));
        end
    else
        fnames={'LineWidth';'LineStyle'; 'Color';'MarkerSize';'MarkerEdgeColor';'MarkerFaceColor';'Marker'};
        for f=1:length(fnames)
            set(hm,fnames{f},get(hlines(pind),fnames{f}));
           set(hl,fnames{f},get(hlines(pind),fnames{f}));
        end
    end
    
    %make hl and hm a line and markers respectively
    set(hl,'Marker','none');
    set(hm,'LineStyle','none');
    
        %create the text
        %3-17-2008
        %try top alighnment for better placing
        obj.htxt(pind)=text(tleft,heights(pind)+obj.linfo.voffset,lbls{pind},'verticalalignment','middle');
        
        if (obj.linfo.latex)
            set(obj.htxt(pind),'interpreter','latex'); 
        end
        
        %adjust the font sizes
        
    set(obj.htxt(pind),'fontsize',obj.linfo.fontsize);
end

        
