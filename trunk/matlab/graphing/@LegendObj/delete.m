%function delete(obj)
%   obj - LegendObj to delete
%   
%Explanation: delete the axis and the LegendObject
function obj=delete(obj)

%delete the axis
delete(obj.ha);
obj.ha=[];

obj=clear('obj');