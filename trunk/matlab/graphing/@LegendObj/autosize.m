%function autosize(obj)
%
%Explanation: automatically resize the legend. We may need to do this
%  if we change the size of the figure.
function autosize(obj)

            %width and height will be in data units
            %we set the x/y axes to be [0,1]
            %WARNING:get the position of the axes before getting
            %the extent of the text objects b\c the extent does not
            %appear to work correctly unless we get the axes position first
            %possibly because its a dependent property
            apos=get(obj.ha,'position');
            
            %set the position so that we squezee the txt
            %get the exten of all text objects and then add some border
            %extent returns left, bottom, width, height
            extent=get(obj.htxt,'extent');
            if iscell(extent)
            extent=cell2mat(extent);
            end
            %compute the width
            width=max(extent(:,3));
            
            %note the extent leaves some extra space around the text
            %so we might want to squeeze further
            height=sum(extent(:,4));
            
                        
            
            
            %convert the width to the same units as the width of the axes
            width=width*apos(3);
            %we need to account for the fact that only (1-obj.linfo.tleft)%
            %of the width is used for the text. The other part is left for
            %the line
            newawidth=width/(1-obj.linfo.tleft);
            setposition(obj,[],[],newawidth,apos(4)*height);
            