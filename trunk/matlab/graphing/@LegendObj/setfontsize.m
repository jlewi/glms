%function obj=setfontsize(obj,val)
%	 obj=LegendObj object
% 
%Return value: 
%	 obj= the modified object 
%
function obj=setfontsize(obj,val)
	 obj.linfo.fontsize=val;
     set(obj.ha,'fontsize',val);
    set(obj.htxt,'fontsize',val);
