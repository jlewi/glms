%function lblgraph(fig)
%   fig - structure which stores values to attach to graph
%          .hf - handle to graph
%   fig.axes  - a field array with one entry per axes
%                  -inherits  missing properties from parent fig
%                  which makes it easy to specify same properties for all
%                  axes
%          .lbls - lbls for legend
%          .hp   - plot handles to go with legend entries
%          .xlabel  - label for x axis
%          .ylabel - label for y axis
%          .title  - graph title
%          .name  - name to appear in figure titlebar
%          .semilog -  make y axis semilog
%          .lgndfontsize - font size for legend
%          .axisfontsize - axis font size
%           .lwidth - linewidth
% Return:
%       fig - contains handles to structures created
%           .hl - legend handle
%           .hxlabel - xlabel handel
%           .hylabel - ylabel handel
%           .htitle- handle to title
%           .ha -handle to axes
%
% Explanation label graph with multiple axes
function [fig]=lblgraph(fig)
if ~isfield(fig,'lgndfontsize')
    fig.lgndfontsize=16;
end
if ~isfield(fig,'axisfontsize')
    fig.axisfontsize=16;
end
if ~isfield(fig,'lwidth')
    fig.lwidth=5;
end

for pi=1:length(fig.hp)
    if isa(fig.hp,'lineseries')
        set(fig.hp(pi),'linewidth',fig.lwidth);
    end
end
figure(fig.hf);


%the problem with the code below
%is that legends are axes to and I don't know how to distinguish
%them from axes
%get handles to all of the axes
% allobjs=findall(fig.hf);
%haxesdup=findall(allobjs,'type','axes');
%haxesdup can have duplicates so remove them
%haxes=[];
%for ind=1:length(haxesdup)
%    if isempty(find(haxes==haxesdup(ind)))
%        haxes(end+1)=haxesdup(ind);
%    end
%end
%if ~isfield(fig,'ha')
%   fig.ha=haxes;
%else
%    if (length(fig.ha)~=length(haxes))
%       warning('Number of axes does not match number of user supplied axes');
%   end
%end

%We want to be able to handle the case of one axis
%and multiple axes consistently
%in the case of multiple axes hp,lbls, xlabel,title, will be cell
%arrays
%we want to convert the case of single axes to this format so we
%can handle them the same way
fnames={'xlabel','title','ylabel','lbls','hp'};

if ~isfield(fig,'ha')
    fig.ha(1)=gca;

    for find=1:length(fnames)
        if isfield(fig,fnames{find})
            gdata.(fnames{find}){1}=fig.(fnames{find});
        end
    end
else
    %there are multiple axes
    for find=1:length(fnames)
        if isfield(fig,fnames{find})
            gdata.(fnames{find}){1}=fig.(fnames{find});
        end
    end
end
%label each axes
for gind=1:length(fig.ha)
    %set the properties
    %xlabel,ylabel,title and legend are axes specific

    %**************************************************************
    %non specific properties
    %**********************************************************
    %all axes use same value
    %use times because I don't think times-roman is scalable
    set(gca,'FontName','Times-Roman');
    set(gca,'FontName','Times');
    %set(gca,'FontName','SansSerif');
    %set(gca,'FontName','Courier');
    if isfield(fig,'axisfontsize')
        set(gca,'FontSize',fig.axisfontsize);
    end




    


%********************************************************
%label a set of axes
%       ha - handle to the axes
%       prop -properties
%           .xlabel
%           .ylabel
%           .title
function faxes=lblaxes(ha,prop)
    %***************************************
    %create a legend
    if isfield(gdata{gind}, 'lbls')
        fig.hl{gind}=legend(gdata.hp{gind},gdata.lbls{gind});
    end
    if isfield(fig,'lgndfontsize')
        %set fontsize after font name
        %use TImes because I don't think times-roman is
        %scalable
        %set(hl,'FontName','Times-Roman');
        set(hl,'FontName','Times');
        set(hl,'FontSize',fig.lgndfontsize);
    end



nplots=length(gdata.hp{gind});
hp=gdata.hp{gind}

if isfield(fig,'markersize')
    for index=1:nplots
        set(hp(index),'MarkerSize',fig.markersize);
    end
end
if isfield(fig,'linewidth')
    for index=1:nplots
        set(hp(index),'Linewidth',fig.linewidth);
    end
end

if isfield(fig,'xlabel')
    fig.hxlabel=xlabel(fig.xlabel);
    %set the interpreter to latex
    %Latex fonts don't look too good for some reason
    %so only set it if its necessary
    %set(fig.hxlabel,'interpreter','latex');
end
if isfield(fig,'ylabel')
    fig.hylabel=ylabel(fig.ylabel);
    %set(fig.hylabel,'interpreter','latex');
end

if isfield(fig,'title')
    fig.htitle=title(fig.title);
    %set(fig.htitle,'interpreter','Latex');
end
if isfield(fig,'name')
    set(fig.hf,'name',fig.name);
end

if (isfield(fig,'semilog'))
    if (fig.semilog~=0)
        set(gca,'YScale','Log');
    end
end

if (isfield(fig,'semilogx'))
    if (fig.semilogx~=0)
        set(gca,'XScale','Log');
    end
end
end
