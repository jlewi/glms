% function setbwlstyle(ha)
%       ha - handle to the plot
%       lstyle - structure with fields
%           .lwidth
%           .color
%           .style
%
%Explanation- defines several linestyles for bw figures
%   set the style of a line
function [varargout]=setlstyle(ha,ls);

%names of fields we want to set
%fnames={'linewidth','color','linestyle','markersize','marker'};
fnames=fieldnames(ls);

for j=1:length(fnames)
    switch lower(fnames{j})
        case {'linewidth','lw','width'}
            set(ha,'LineWidth',ls.(fnames{j}));
        case {'color','linecolor','lcolor'}
            set(ha,'color',ls.(fnames{j}));
        case {'linestyle','ls','lstyle'}
            set(ha,'linestyle',ls.(fnames{j}));
        case {'markersize'}
            set(ha,'markersize',ls.(fnames{j}));
        case {'marker'}
            set(ha,'marker',ls.(fnames{j}));
        otherwise
            warning('Unrecognized field %s \n',fnames{j});
    end
end