% function setbwlstyle(index)
%       ha - handle to the plot
%       index - index into the plot
%
%Explanation- defines several linestyles for bw figures
function [l]=getbwstyle(index);

c=colormap(gray);


%black solid
lind=1;
ls(lind).LineWidth=4;
ls(lind).color=c(1,:);
ls(lind).linestyle='-';


%light gray solid
lind=lind+1;
ls(lind).LineWidth=4;
ls(lind).color=c(50,:);
ls(lind).linestyle='-';

%black dashed
lind=length(ls)+1;
ls(lind).LineWidth=4;
ls(lind).color=c(1,:);
ls(lind).linestyle='--';

%grey dashed
lind=length(ls)+1;
ls(lind).LineWidth=4;
ls(lind).color=c(50,:);
ls(lind).linestyle='--';

%black dash dot
lind=length(ls)+1;
ls(lind).LineWidth=4;
ls(lind).color=c(1,:);
ls(lind).linestyle='-.';

%grey dash dot
lind=length(ls)+1;
ls(lind).LineWidth=4;
ls(lind).color=c(50,:);
ls(lind).linestyle='-.';

%black dotted
lind=length(ls)+1;
ls(lind).LineWidth=4;
ls(lind).color=c(1,:);
ls(lind).linestyle=':';

%grey dotted
lind=length(ls)+1;
ls(lind).LineWidth=4;
ls(lind).color=c(50,:);
ls(lind).linestyle=':';




l=ls(mod(index-1,length(ls))+1);

