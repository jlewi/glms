%function slideshowf(func,data,pparam,opts)
%   func - pointer to function which does the plotting
%   data - structure containing cell arrays
%          .field1{}
%          .field2{}
%          ...
%  pparam - structure containing addtional fields for func
%  opts   - optional parameters
%          .fname - basename of file to save images to
%
%         -.movie  - move parameters
%          .movie.fps - frames per second -default 2
%          .movie.mname -mname (noextension)
%
%  Explanation:
%       Creates a slideshow of the data.
%       On each iteration it creates
%           d.field1=data.field1{index}
%           d.fieldi=data.fieldi{index}
%       To plot the data it then calls func(d,pparam)
function slideshowf(func,data,pparam,opts)
    
if ~(exist('opts','var'))
    opts=[];
end

if isfield(opts,'movie')
   if ~isfield(opts.movie,'fps');
       opts.movie.fps=2;
   end
   
   if ~isfield(opts.movie,'mname');
       fprintf('slideshowf: must specify mname if you want to create a movie \n');
       return;
   else
     %if file already exists get next name
     if exist(opts.movie.mname,'file')
         fprintf('Warning: file %s exists \n',opts.movie.mname);
         opts.movie.mname=seqfname(opts.movie.mname);
         fprintf('Warning: saving movie as %s \n',opts.movie.mname);
     end
     aviobj=avifile(opts.movie.mname,'fps',opts.movie.fps); %creates AVI file, test.avi 
   end
else
    aviobj=[];
end

      

    fields=fieldnames(data);
    
    numiter=length(data.(fields{1}));
    
    hf=figure;
    pparam.hf=hf;
    for index=1:numiter
        %construct d
        for find=1:length(fields)
            d.(fields{find})=data.(fields{find}){index};
        end
        %if index>1 then wait for keypress before plotting next one
        
        if (index>1 & ~isfield(opts,'fname') & isempty(aviobj))
            %ginput(1);
            pause;
        end
        clf;
        
        pparam=func(d,pparam);
        if (isfield(opts,'fname'))
            saveas(hf,seqfname(opts.fname));
        end
        figure(hf);
        title(sprintf('Iteration %0.3g', index));
        if (~isempty(aviobj))
            
            aviobj=addframe(aviobj,hf); %adds frames to the AVI file  
        end
        fprintf('Slideshow: Iteration %d \n', index);
        
        
    end

if (~isempty(aviobj))
    aviobj=close(aviobj); %closes the AVI file  
end