%function sizesubplots(fig,space,widths,heights,eborder)
%   fig - FigObj
%   space - structure specifing space settings
%         - these are specified as a % of the width and height of the
%         actual plot area
%         .vspace - vertical space
%         .hspace space
%         .cbwidth - width of colorbar this is expressed as percentage of
%           the actual plot area (inner position) of the axes
%         .stocb  - spacing to the colorbar expressed as a percentage
%                   of the plot area width
%   widths - 1xsize(fig.a,2) vector
%          - specifies relative width of each column
%          optional
%   heights -1xsize(fig.a,1)vector
%          -specifies relative height of each row
%          optional
%   eborder - extra border [left bottom right top]
%           -adds an extra border along top,bottom, left, and right
%           -do this because sometimes my latex labels are being cut off
%           which I think is problem with matlab's tight inset for latex
%           labels with math.
%           
%Explanation: This is my attempt to write a function to automatically
%   make subplots equal sizes when we have subplots
%
%   Currently cb's are specified as percentages of the area devoted to the
%   actual plot. Therefore smaller plots will have smaller colorbars. 
%
%Revisions
%   10-21-2008 - Store the arguments in sizesubs so if needed
%       we can call sizesubplots again with the same info
function fig=sizesubplots(fig,space,widths,heights,eborder)

if (nargin==1)
   if ~isempty(fig.sizesub)
      %load info from fig.sizesubs 
       space=fig.sizesub.space;
       widths=fig.sizesub.widths;
       heights=fig.sizesub.heights;
       eborder=fig.sizesub.eborder;
   end
end
if ~exist('widths','var')
    widths=[];
end
if isempty(widths)
    widths=ones(1,size(fig.a,2));;
end

if ~exist('heights','var')
    heights=ones(1,size(fig.a,1));
end

if isempty(heights)
    heights=ones(1,size(fig.a,1));
end

if ~exist('space','var')
    space=[];
end

if ~exist('eborder','var')
    eborder=[];
end

if isempty(eborder)
    eborder=zeros(1,4);
end

if ~isfield(space,'cbwidth')
    %set defualt colorbar widht to 5% of axes poisiton
    space.cbwidth=.05;
end
if ~isfield(space,'stocb')
    %set default spacing to colorbar to 1%
    space.stocb=.01;
end

if ~isfield(space,'hspace')
    space.hspace=.05;
end
if ~isfield(space,'vspace')
    space.vspace=.05;
end

%****************************************
%save the parameters for reuse
%***********************************
fig.sizesub.space=space;
fig.sizesub.widths=widths;
fig.sizesub.heights=heights;
fig.sizesub.eborder=eborder;


%normalize heights and widths
%only if they exceed 1
if (sum(widths)>1)
    widths=widths/sum(widths);
end
if (sum(heights)>1)
heights=heights/sum(heights);
end

setfsize(fig);

if ~isa(fig.a,'AxesObj')
    error('fig.a must be array of axesobj' );
end

nrows=size(fig.a,1);
ncols=size(fig.a,2);

%get the border for the innerposition of each axes
aborders=getborder(fig.a);
%get the border for each colorbar
ahcborders=gethcborder(fig.a);

%we take the maximum along the dimensions
%to get border for each column and row
borders.left=zeros(1,ncols);
borders.right=zeros(1,ncols);
borders.bottom=zeros(nrows,1);
borders.top=zeros(nrows,1);

hcborders.left=zeros(1,ncols);
hcborders.right=zeros(1,ncols);
hcborders.bottom=zeros(nrows,1);
hcborders.top=zeros(nrows,1);

%make borders.left, and borders.right - to arrays
% with number of elements equal to the number of col
% each element gives the amount of border needed for that column
for cind=1:ncols
    borders.left(cind)=max([aborders(:,cind).left]);
    borders.right(cind)=max([aborders(:,cind).right]);
    

    %we also need borders of hc
    hcborders.left(cind)=max([ahcborders(:,cind).left]);
    hcborders.right(cind)=max([ahcborders(:,cind).right]);
end


%add extra border to first and last column
borders.left(1)=borders.left(1)+eborder(1);
borders.right(end)=borders.right(end)+eborder(3);

%make borders.top, and borders.bottom - are arrays
% with number of elements equal to the number of rows
% each element gives the amount of border needed for that row
for rind=1:nrows   
borders.top(rind)=max([aborders(rind,:).top]);
borders.bottom(rind)=max([aborders(rind,:).bottom]);

hcborders.top(rind)=max([ahcborders(rind,:).top]);
hcborders.bottom(rind)=max([ahcborders(rind,:).bottom]);
end

%add extra border to first and last row
borders.top(1)=borders.top(1)+eborder(end);
borders.bottom(end)=borders.bottom(end)+eborder(2);


borders.top=max([borders.top hcborders.top],[],2);
borders.bottom=max([borders.bottom hcborders.bottom],[],2);

%ASSUMPTION: colorbars are to right or axes
%so bottom and top border of colorbars should not be cumulative
hcborders.top=0;
hcborders.bottom=0;
wborder=sum(hcborders.left)+sum(hcborders.right)+sum(borders.left)+sum(borders.right);
hborder=sum(borders.top)+sum(borders.bottom);



%adjust the widths to allow for colorbars;
%for each column with a colorbar multiple wplot by (1+space.cbwidth)
%then renormalize
colswithcb=getcolswithcb(fig.a);
wplot=widths.*(1+colswithcb*(space.cbwidth+space.stocb));

%wplot is the width of each column including spacing for the colorbars if
%they exist. We need this to compute left.
wplot=wplot/(sum(wplot.^2))^.5;

%compute the width of each axis plot



%this is the actual amount of spacing to use
if (ncols>1)
hspace=(1-wborder)*space.hspace/(ncols-1);
else
    hspace=0;
end

%this is the actual amount of spacing to use
if (nrows>1)
   vspace=(1-hborder)*space.vspace/(nrows-1);
else
    vspace=0;
end

%wplot should be a vector specifying width of each column
%we multiply the borders by what %of the left over space we want to
%devote to the actual plot areas as opposed to spacing
wplot=widths*(1-wborder-hspace*(ncols-1));
hplot=heights*(1-hborder-vspace*(nrows-1));

%determine the bottom of each row
bottom=zeros(1,nrows);
for rind=nrows:-1:1
    if (rind<nrows)
        bottom(rind)=bottom(rind+1);

        %add the height of the previous plot
        bottom(rind)=bottom(rind)+hplot(rind+1);
   
        %add the top of the previous row
        bottom(rind)=bottom(rind)+borders.top(rind+1);
        
        %add vertical space
        bottom(rind)=bottom(rind)+vspace;
    end
    
    %add the bottom border of the current row
    bottom(rind)=bottom(rind)+borders.bottom(rind,:);
end

%determine the left coordinate of each colum
left=zeros(1,ncols);
for cind=1:ncols
    if (cind>1)
        left(cind)=left(cind-1);

        %add the width of the previous plot
        left(cind)=left(cind)+wplot(cind-1);

        %add the right border of the previous one
         left(cind)=left(cind)+borders.right(cind-1);
         
         %if previous plot has color bar add the space needed
         %for border of colorbar. Width and spacing of colorbar 
         %is already accounted for
         %border is zero if no colorbar
         left(cind)=left(cind)+hcborders.left(cind-1)+hcborders.right(cind-1);
         %add horizontal space
         left(cind)=left(cind)+hspace;
    end
    
    %add the left border of the current column
    left(cind)=left(cind)+borders.left(cind);
end

%set the size of each plot
%we need to readjust wplot. We want wplotact to be
%the space alloted to the inner position. So the width not including
%spacing for the colorbar.
wplotact=wplot./(1+colswithcb*(space.cbwidth+space.stocb));


fig.a=setposition(fig.a,left,bottom,wplotact,hplot);
%*****************************************************************
%position the colorbars
%*********************************************************************
leftcb=zeros(1,ncols);
widthcb=zeros(1,ncols);

for cind=1:ncols
   if (colswithcb(cind)==1)
      
      leftcb(cind)=left(cind)+wplotact(cind)*(1+space.stocb); 
      %add border for this colorbar
      leftcb(cind)=leftcb(cind)+hcborders.left(cind);
      widthcb(cind)=wplotact(cind)*space.cbwidth;
   end
end

%set colorbar positions
fig.a=sethcposition(fig.a,leftcb,bottom,widthcb,hplot);