%function lblgraph(fig)
%   fig -
%
%
function [fig]=lblgraph(fig)

%structure of parameters to pass to the axes
%idea is that axes object will inherit these from the figure
%unless the are overridden in the axes object
aparam=fig.gparam;
gparam=fig.gparam;

%set the figure name
figure(fig.hf);
if ~isempty(fig.name)
    set(fig.hf,'name',fig.name);
end

%************************************************************
%Set all font sizes
%do this before calling lblaxes because this way lblaxes
%   can override this settings
%*******************************************************
%Set all text objects
alltext=findall(fig.hf,'type','text');
set(alltext,'FontName',gparam.fontname);
set(alltext,'FontSize',gparam.fontsize);

%et all axes
allaxes=findall(fig.hf,'type','axes');
set(allaxes,'FontName',gparam.fontname);
set(allaxes,'FontSize',gparam.fontsize);


%*****************************************************
%label each axes
%******************************************************
for aind=1:numel(fig.a)
    fig.a(aind)=lblaxes(fig.a(aind),aparam);
end








