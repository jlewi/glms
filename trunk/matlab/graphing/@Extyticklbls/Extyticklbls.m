% hplot - axes of the plot whose yticklabels we want to adjust
%
%Explanation: The point of this object is to allow us to specify ticklabels
%   as tex or latex objects. To do this we create a blank set of axes next
%   to the plot. And then fill it in with text objects for the tick labels
%
%
% Some issues: see my notes
function obj=Extyticklbls(hplot)

%create the axes where we place the text objects
ppos=get(hplot,'position');

%the initial width is set to some default value we should adjust this later
%on based on the width of the text objects
if (strcmp(get(hplot,'YAxisLocation'),'left')==0)
    haxes=axes('Position',[ppos(1)+ppos(3),ppos(2),.05,ppos(4)],'visible','off');
else
%    warning('code cant handle axes on left yet');
   haxes=axes('Position',[ppos(1)-.05,ppos(2),.05,ppos(4)],'visible','off');
end

%normalize the xunits to 0 1
xlim([0,1]);
%set the limits of haxes to match those of the ylimits
ylim(get(hplot,'ylim'));
%set the direction to match
set(haxes,'ydir',get(hplot,'ydir'));
%greate a text object for each tickmark
ytick=get(hplot,'Ytick');
yticklbls=get(hplot,'YTickLabel');
ntick=length(ytick);

axes(haxes);
htxt=zeros(ntick,1);
for k=1:ntick
    if(strcmp(get(hplot,'YAxisLocation'),'left')==0)
%        htxt(k)=text(0.15,ytick(k),yticklbls{k});
      htxt(k)=text(0.25,ytick(k),yticklbls{k});
        %set the bottom of the text to be aligned with the bottom of the
        %position rectangle
        %do this because it looks better when we have exponents and run
        %exportfig (export fig appears to modify the position);
        if (strcmp(get(haxes,'ydir'),'normal'))
        set(htxt(k),'verticalalignment','bottom');
        else
            set(htxt(k),'verticalalignment','middle');            
        end
    else
         htxt(k)=text(.95,ytick(k),yticklbls{k});
         set(htxt(k),'horizontalalignment','right');
    end
end

%turn off the existiclabels
set(hplot,'YtickLabel',[]);

%haxes- is the axes where we place the text objects
%htxt  - handles to the text objects
s=struct('hplot',hplot,'haxes',haxes,'htxt',htxt);


obj=class(s,'Extyticklbls');