%function ylabel(obj,text,propname,val,probname,val,etc..)
%   propname - name of label property to set e.g. interpreter
%   val      - value for the property
%Explanation: create an xlabel
function obj=title(obj,txt,varargin)
    obj.tlbl.h=title(obj.ha,txt);
    obj.tlbl.txt=txt;
    
    
      if (nargin>1)
       for j=1:2:length(varargin)
          set(obj.tlbl.h,varargin{j},varargin{j+1}); 
       end
    end
    