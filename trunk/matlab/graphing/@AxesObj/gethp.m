%function gethp=hp(obj)
%	 obj=AxesObj object
% 
%Return value: 
%	 hp=obj.hp 
%
function hp=gethp(obj)
	 hp=[obj.p.hp];
