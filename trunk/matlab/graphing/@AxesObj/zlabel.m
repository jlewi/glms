%function zlabel(obj,txt,propname,val,probname,val,etc..)
%   propname - name of label property to set e.g. interpreter
%   val      - value for the property
function obj=zlabel(obj,txt,varargin)
    obj.zlbl.h=zlabel(obj.ha,txt);
    obj.zlbl.txt=txt;
    
    if (nargin>1)
       for j=1:2:length(varargin)
          set(obj.zlbl.h,varargin{j},varargin{j+1}); 
       end
    end