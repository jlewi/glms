%function getcolswithcb=colswithcb(obj)
%	 obj=AxesObj object
% 
%Return value: 
%	 colswithcb=obj.colswithcb 
%           -a vector of 0's and 1's indicating which columns
%            have colorbars
function colswithcb=getcolswithcb(obj)
    colswithcb=zeros(1,size(obj,2));
    
    for cind=1:size(obj,2)
       if ~isempty([obj(:,cind).hc])
           colswithcb(cind)=1;
       end
    end
