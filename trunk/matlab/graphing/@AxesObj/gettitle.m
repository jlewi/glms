%function gettitle=title(obj)
%	 obj=AxesObj object
% 
%Return value: 
%	 title=obj.title 
%
function title=gettitle(obj)
	 title=obj.tlbl;
