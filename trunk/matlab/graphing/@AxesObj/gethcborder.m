%function gethcborder=hcborder(obj)
%	 obj=AxesObj object
% 
%Return value: 
%	 border - structure array specifying how much of a border
%             we need for the colorbar associated with each axes if there
%             is a colorbar
%
function border=gethcborder(obj)
	
nrows=size(obj,1);
ncols=size(obj,2);
     border=struct('left',0,'right',0,'bottom',0,'top',0);
     border=repmat(border,nrows,ncols);
for rind=1:nrows
    for cind=1:ncols
        if ~isempty(obj(rind,cind).hc)
            tinset=get(obj(rind,cind).hc,'tightinset');

        border(rind,cind).left=tinset(:,1);
        border(rind,cind).bottom=tinset(:,2);
        border(rind,cind).right=tinset(:,3);
        border(rind,cind).top=tinset(:,4);

        
        end
    end
end