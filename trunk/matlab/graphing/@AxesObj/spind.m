%function spind(obj,row,col)
%   obj - AxesObj array
%   row - row of plot
%   col - column of plot
%
% Explanation: return the linear index used by subplot based on row and col
function ind=spind(obj,row,col)

ind=sub2ind([getncols(obj(1)) getnrows(obj(1))],col,row);