%function getylabel=ylabel(obj)
%	 obj=AxesObj object
% 
%Return value: 
%	 ylabel=obj.ylabel 
%
function ylabel=getylabel(obj)
	 ylabel=obj.ylbl;
