%function obj=setfocus(obj,row,col)
%    -row
%	 obj=AxesObj object
% 
%Return value: 
%	 obj= the modified object 
%
%Explanation: make the axes with the specified row and col the current axes
function obj=setfocus(obj,row,col)

  
    
switch nargin
     case 1
        
         %don't use axes as this restacks the figures
    	%axes(obj.ha);
          hf=get(obj.ha,'parent');

        set(hf,'CurrentAxes',obj.ha);
     case 2
         
        warning('01-29-2009- this should be obsolete');
        ind=row;
        [col row]=ind2sub([obj.ncols,obj.nrows],ind);
    
     case 3
          hf=get(obj(row,col).ha,'parent');

         %don't use axes as this restacks the figures
                set(hf,'CurrentAxes',obj(row,col).ha);
    end
