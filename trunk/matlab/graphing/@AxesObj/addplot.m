%function addplot(obj,fieldname,value,...)
%
%      hp - handle to the plot
%      pstyle - structure of field name pairs which defines the line style
%      lbl  - label for the plot
%Explanation: add a plot to this axes
function obj=addplot(obj,varargin)

pind=length(obj.p)+1;
for fi=1:2:length(varargin)
    %special handlers for fieldnames
    switch lower(varargin{fi})
        otherwise
               obj.p(pind).(lower(varargin{fi}))=varargin{fi+1} ;
    end
end

%if pstyle was passed in adjust the style
    if (isfield(obj.p(pind),'pstyle') && ~isempty(obj.p(pind).pstyle))
          setpstyle(obj,pind,obj.p(pind).pstyle);
    end
%initialize default fields
if ~isfield(obj.p(pind),'lstyle')
    obj.p(pind).lstyle=[];
end