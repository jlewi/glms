%function sim=PlotStyles
%   fieldname, value - pairs of fields and values used to initialize the object
%
% Explanation: A class for generating plotstyles
%
%Revisions:
%   09-25-2008 - Add field markersize
classdef (ConstructOnLoad=true) PlotStyles < handle
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        version=080925;;

    end

    properties(SetAccess=public,GetAccess=public)
        %default values
        linewidth=4;
        markersize=4;

        colors=[];
        %lstyles needs to be a column vector
        lstyles={'-','--','-.',':'}';

        %markers
        %markers needs to be a column vector
        markers={'.','o','x','+','*','s','d','v','^','<','>','p','h'}';
        
        %order specifies how we cycle through the different styles
        %order is a cell array with the fields
        %'lstyles','colors','markers'
        order={'colors','lstyles','markers'};
        

    end
    methods(Access=public)
        [l]=plotstyle(obj,index);
    end
    methods
        function obj=set.lstyles(obj,lstyles)
           obj.lstyles=colvector(lstyles);
        end
        function obj=set.markers(obj,markers)
           obj.markers=colvector(markers);
        end
        function colors=get.colors(obj)
            if isempty(obj.colors)
                %we take combinations of colors, linestyles, and markers
                %below is a specification of the colors
                c=colormap(hsv);

                %shuffle the colors so that we maximize the difference of the colors
                %between successive plots


                %colors=c([1 [8:8:64]],:);
                %colors=colors([1:2:7 2:2:8],:);
               obj.colors=c([1 16:16:48],:);
            end
            colors=obj.colors;
        end
        function obj=PlotStyles(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %   .cfun   - function (or number) to indicate which constructor to call
            %blank constructor for loading the object.
            con(1).rparams={};



            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=struct();
                    cind=1;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=[];

                otherwise
                    if isnan(cind)
                        error('no constructor matched');
                    else
                        %remove fields which are for this class
                       
                    end
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 1
                    %do nothing used by load object
            
                otherwise
                    if isnan(cind)
                    else

                    error('Constructor not implemented')
                    end
            end



        end
       
        %overload the disp function to avoid creating a figure everytime we
        %display the object
%         function disp(obj)            
%         end
    end
end


