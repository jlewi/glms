% function plotstyle(index)
%   index - number of the plot style
%
%Explanation- defines several linestyles for figures
%   return value is a structure containing field name value pairs
%   This function cycles through combinations of line styles, colors, and
%   markers.
%   To change combinations, change the values of the static properties
%
%Revisions:
%   08-29-2008
%       Double the line width to 4
%       swap ':' and '--' in lstyles
function [l]=plotstyle(obj,index)


% ncolors=size(colors,1);
% nlstyles=length(obj.lstyles);
% nmarkers=length(obj.markers);

%we pick the color,lstyle, and markers by cycling through the possiblities.
%cycling through the styles is basically the equivalent of converting a
%linear index into a multi-dimensional index for an array whose dimensions 
%are ncolorsxnlstylesxnlmarxers

forder=obj.order;
matdim=[size(obj.(forder{1}),1) size(obj.(forder{2}),1) size(obj.(forder{3}),1)];

%wrap around if necessary
index=mod(index-1,prod(matdim))+1;

i=zeros(1,3);
[i(1),i(2),i(3)]=ind2sub(matdim,index);


for f=1:3
    switch lower(forder{f})
        case 'colors'
           colors=obj.colors;
l.color=colors(i(f),:);
        case 'lstyles'
            l.LineStyle=obj.lstyles{i(f)};
        case 'markers'
            l.Marker=obj.markers{i(f)};
    end
end

l.LineWidth=obj.linewidth;
l.markerfacecolor=l.color;
l.markersize=obj.markersize;