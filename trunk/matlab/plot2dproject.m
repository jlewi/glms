function plot2dproject(matrix)
    %plots two dimensional projections of 3 dimensional array
    %the dimensions labeled x,y,z where matrix(x,y,z)
    
    %% Add directory containing the simoncelli code to the:
    path('/home/jl1403/projects/orientation/matlabPyrTools',path);

    %create new figure
    figure;
    subplot(1,3,1);
    
    %project on to the x and y access
    projection=sum(matrix,3);
    %reshape it
    projection=reshape(projection,size(matrix,1),size(matrix,2));
    

    bar3(projection);
    xlabel('green');
    ylabel('red');
    xlim([0 size(matrix,1)]);
    ylim([0 size(matrix,1)]);
    
    subplot(1,3,2);
    
    %project on to the x and z access
    projection=sum(matrix,2);
    %reshape it
    projection=reshape(projection,size(matrix,1),size(matrix,3));
    bar3(projection);
    xlabel('blue');
    ylabel('red');
    xlim([0 size(matrix,1)]);
    ylim([0 size(matrix,1)]);    

    subplot(1,3,3);
    
    %project on to the y and z access
    projection=sum(matrix,1);
    %reshape it
    projection=reshape(projection,size(matrix,2),size(matrix,3));
    bar3(projection);
    xlabel('blue');
    ylabel('green');
    xlim([0 size(matrix,1)]);
    ylim([0 size(matrix,1)]);
