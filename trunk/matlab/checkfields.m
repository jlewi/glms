%function [var,err]=checkfields(var,temp)
%   var - the structure we want to check
%   temp - template for the structure
%        -has following structure
%        temp.field - can either have a value or be a strcuture it its
%        value its the default value
%        temp.field.value - default value
%                         - if none is supplied
%                           then an error is thrown if var doesn't have
%                           this field
%        temp.field.errmsg - override default error message
%
% 
%Explanation:
%   The purpose of this function is to verify that 
%   the structure var contains certain fields.
%   var must contain all the fields present in fieldnames
%       If var does not have a field that temp has one of two things
%       happens
%
%       if temp.field.value exists then var.field is set to that value
%       (consider it a default value). If not an error is thrown
%       If temp.field.msg is provided than that errors message is provided
function [var,err]=checkfields(var,temp)
    vfields=fieldnames(var);
    tfields=fieldnames(temp);
    
    for tindex=1:length(tfields)
        if ~isfield(var,tfields{tindex})
            if isstruct(temp.(tfields{tindex}))
                if isfield(temp.(tfields{tindex}),'value')
                    var.(tfields{tindex})=temp.(tfields{tindex}).value;

                else
                   if isfield(temp.(tfields{tindex}),'errmsg')
                       error(temo.(tfields{tindex}).errmsg);
                   else
                       error(sprintf('Field %s not supplied \n',tfields{tindex}));
                       err=0;
                   end
                end
            else
                var.(tfields{tindex})=temp.(tfields{tindex});
            end
        end
    end