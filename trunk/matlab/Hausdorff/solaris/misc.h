/*
 *
 *	$Header$
 *
 * Copyright (c) 1990, 1991, 1992, 1993 Cornell University.  All Rights
 * Reserved.
 *
 * Copyright (c) 1991, 1992 Xerox Corporation.  All Rights Reserved.
 *
 * Use, reproduction and preparation of derivative works of this software is
 * permitted.  Any copy of this software or of any derivative work must
 * include both the above copyright notices of Cornell University and Xerox
 * Corporation and this paragraph.  This software is made available AS IS, and
 * XEROX CORPORATION DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
 * WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE, AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED
 * HEREIN, ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF XEROX CORPORATION IS ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGES.
 */
/*
 *
 * misc.h - miscellaneous types and declarations
 *
 */

#ifndef WJR_MISC_H
#define	WJR_MISC_H

/*
 * Stuff for C*; these are snitched from their stdlib.h.
 * I don't include their stdlib.h because it declares malloc as returning
 * char *, which conflicts with the nice definition below.
 */
#ifdef	cstar
extern void psrand(unsigned seed);
extern int:current prand(void);
extern void deallocate_shape(shape *s);
extern void:void * palloc(shape s, int bsize);
extern void pfree(void:void *pvar);

/* They don't implement volatile, so we get lots of warnings... */
#define	volatile
#endif

typedef char boolean;
#ifndef TRUE
#define TRUE	((boolean)1)
#endif
#ifndef FALSE
#define FALSE	((boolean)0)
#endif

#ifndef __GNUC__
#ifdef	cstar

/* Use the C* extensions: >? (MAX) and <? (MIN) operators */
#ifndef	MAX
#define	MAX(x, y) ((x) >? (y))
#endif

#ifndef	MIN
#define	MIN(x, y) ((x) <? (y))
#endif

#ifndef	ABS
#define	ABS(x)	((x) >? (-(x)))
#endif

#else

#ifndef MAX
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#endif

#ifndef MIN
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#endif

#ifndef ABS
#define ABS(x) (((x) < 0) ? -(x) : (x))
#endif

#endif

#else

/* GCC allows new scopes inside expressions; use to get better MAX and MIN */

#ifndef MAX
#define MAX(a,b) \
    ({typedef _tamax = (a), _tbmax = (b);  \
	  _tamax _a = (a); _tbmax _b = (b);     \
	      _a > _b ? _a : _b; })
#endif

#ifndef MIN
#define MIN(a,b) \
    ({typedef _tamin = (a), _tbmin = (b);  \
	  _tamin _a = (a); _tbmin _b = (b);     \
	      _a < _b ? _a : _b; })
#endif

#ifndef ABS
#define ABS(a) \
    ({typedef _taabs = (a); \
	  _taabs _a = (a); \
	      _a < 0 ? -_a : _a; })
#endif

#endif

/* This is what malloc returns on error, and is also a null pointer */
#ifndef NULL
#define NULL    0
#endif

/*
 * Include common files. These come AFTER the defines above,
 * because mine are better.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <memory.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include "panic.h"
#include "dbug.h"

/* Some library routines that never seem to get declared */

/* I don't feel like including setjmp.h, so this is commented out for now */
/*
extern int _setjmp(jmp_buf env);
extern volatile void _longjmp(jmp_buf env, int val);
*/

/* String functions: string[s].h doesn't have prototypes for all of them. */
extern char *index(char *s, int c);

/* Raw IO functions */
extern int select(int width, fd_set *readfds, fd_set *writefds,
		  fd_set *exceptfds, struct timeval *timeout);
extern int flock(int fd, int operation);
extern caddr_t mmap(caddr_t addr, size_t len, int prot, int flags, int fd,
		    off_t off);
extern int munmap(caddr_t addr, int len);

/* Socket-fiddling functions */
extern int socket(int domaint, int type, int protocol);
extern int connect(int s, struct sockaddr *name, int namelen);
extern int bind(int s, struct sockaddr *name, int namelen);
extern int listen(int s, int backlog);
extern int accept(int s, struct sockaddr *addr, int *addrlen);

/* Random stuff */
extern long random(void);
extern void srandom(int seed);

extern double drand48();
extern long lrand48();
extern void srand48(long seedval);

/* Time stuff */

extern char *strptime(char *buf, char *fmt, struct tm *tm);
extern time_t timegm(struct tm *tm);
extern time_t timelocal(struct tm *tm);
extern void tzset(void);
extern void tzsetwall(void);

/* Memory fiddling routines */
extern void bcopy(char *b1, char *b2, int length);
extern void bzero(char *b, int length);
 
/* Shared memory stuff */
extern int shmget(key_t key, int size, int shmflg);
extern void *shmat(int shmid, void *shmaddr, int shmflg);
extern int shmdt(void *shmaddr);

/* Semaphore stuff */
extern int semget(key_t key, int nsems, int semflg);

/* and misc stuff */
extern char *optarg;
extern int optind, opterr;
extern void perror(const char *s);
extern int gethostname(char *name, int namelen);
extern int usleep(int time);

extern int fprintf(FILE *stream, const char *format, ...);
extern int printf(const char *format, ...);
extern int fscanf(FILE *stream, const char *format, ...);
extern int sscanf(const char *s, const char *format, ...);

extern int _filbuf();
extern int _flsbuf();
extern int fclose(FILE *stream);
extern int fflush(FILE *stream);
extern int ungetc(int c, FILE *stream);

static inline int nint(double f) { return ((int)(f + 0.5)); }
#define random rand
union semun {
    int val;
    struct semid_ds *buf;
    ushort *array;
    };


#endif
