/*
 *
 * $Header$
 *
 * Copyright (c) 1990, 1991, 1992, 1993 Cornell University.  All Rights
 * Reserved.  
 *  
 * Copyright (c) 1991, 1992 Xerox Corporation.  All Rights Reserved.  
 *  
 * Use, reproduction, preparation of derivative works, and distribution
 * of this software is permitted.  Any copy of this software or of any
 * derivative work must include both the above copyright notices of
 * Cornell University and Xerox Corporation and this paragraph.  Any
 * distribution of this software or derivative works must comply with all
 * applicable United States export control laws.  This software is made
 * available AS IS, and XEROX CORPORATION DISCLAIMS ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
 * AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
 * LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF XEROX CORPORATION IS ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGES.
 */

static char rcsid[] = "@(#)$Header$";

#include "chunk.h"
#include "list.h"
#include "panic.h"
#include "misc.h"
#include "r-h.h"

static Chunk transChunk = NULLCHUNK;

extern transval *
malloc_trans()
{
    transval *tv;

    if (transChunk == NULLCHUNK) {
	transChunk = chNew(sizeof(transval));
	if (transChunk == NULLCHUNK) {
	    return((transval *)NULL);
	    }
	}
    tv = (transval *)chAlloc(transChunk);
    return(tv);
    }

extern void
free_trans(transval *tv)
{
    assert(tv != (transval *)NULL);
    assert(transChunk != NULLCHUNK);

    chFree(transChunk, (void *)tv);
    }

extern void
free_translist(List l)
{
    ListNode ln;
    transval *t;

    assert(l != NULLLIST);

    foreach (ln, l) {
	t = (transval *)liGet(ln);
	assert(t != (transval *)NULL);
	free_trans(t);
	}
    liFree(l);
    }
