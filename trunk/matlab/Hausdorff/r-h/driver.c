/*
 *
 * $Header$
 *
 * Copyright (c) 1990, 1991, 1992, 1993 Cornell University.  All Rights
 * Reserved.  
 *  
 * Copyright (c) 1991, 1992 Xerox Corporation.  All Rights Reserved.  
 *  
 * Use, reproduction, preparation of derivative works, and distribution
 * of this software is permitted.  Any copy of this software or of any
 * derivative work must include both the above copyright notices of
 * Cornell University and Xerox Corporation and this paragraph.  Any
 * distribution of this software or derivative works must comply with all
 * applicable United States export control laws.  This software is made
 * available AS IS, and XEROX CORPORATION DISCLAIMS ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
 * AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
 * LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF XEROX CORPORATION IS ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGES.
 */

static char rcsid[] = "@(#)$Header$";

/*
 * This file drives the r-h code: it reads in options, images, models
 * etc, runs the code, and prints out the matches.
 */
#include <stdio.h>
#include "misc.h"
#include "r-h.h"
#include "list.h"

static char usage[] = "usage: %s [-forward thresh frac] [-reverse thresh frac]\n\
\timagepoints [modelpoints...]\n";

int
main(int argc, char **argv)
{
    char *image_fname;
    char *model_fname;
    FILE *image_file;
    FILE *model_file;
    image_info image;
    model_info *models;
    unsigned nmodels;
    ListNode ln;
    transval *t;
    int i;
    char *av0 = argv[0];
    double image_frac = 1.;
    double model_frac = 1.;
    long image_thresh = 100;
    long model_thresh = 100;

    /* Parse the command line */
    while ((argc >= 2) && (argv[1][0] == '-')) {
	switch(argv[1][1]) {
	  case 'f': {
	    /* Forward parameters */
	    if (argc < 4) {
		fprintf(stderr, usage, av0);
		exit(1);
		}
	    model_thresh = atoi(argv[2]);
	    model_frac = atof(argv[3]);
	    argc -= 3;
	    argv += 3;
	    break;
	    }

	  case 'r': {
	    /* Reverse parameters */
	    if (argc < 4) {
		fprintf(stderr, usage, av0);
		exit(1);
		}
	    image_thresh = atoi(argv[2]);
	    image_frac = atof(argv[3]);
	    argc -= 3;
	    argv += 3;
	    break;
	    }

	  default: {
	    fprintf(stderr, usage, av0);
	    exit(1);
	    break;
	    }
	    
	    }
	}

    if (argc < 2) {
	fprintf(stderr, usage, av0);
	exit(1);
	}

    image_fname = argv[1];
    nmodels = argc - 2;

    image_file = fopen(image_fname, "r");
    if (image_file == (FILE *)NULL) {
	perror(image_fname);
	exit(1);
	}

    image.pts = read_pts(image_file, &image.xsize, &image.ysize, &image.npts,
			 &image.im);
    if (image.pts == (point *)NULL) {
	fprintf(stderr, "can't read data from %s\n", image_fname);
	exit(1);
	}
    (void)fclose(image_file);
    image.dtrans = (LongImage)NULL;
    image.plusx_dtrans = (ShortImage)NULL;

    models = (model_info *)malloc(nmodels * sizeof(model_info));
    if (models == (model_info *)NULL) {
	fprintf(stderr, "can't allocate model info\n");
	exit(1);
	}

    for (i = 0; i < nmodels; i++) {
	model_fname = argv[i + 2];
	model_file = fopen(model_fname, "r");
	if (model_file == (FILE *)NULL) {
	    perror(model_fname);
	    exit(1);
	    }

	models[i].pts =
	    read_pts(model_file, &(models[i].xsize), &(models[i].ysize),
		     &(models[i].npts), &(models[i].im));
	if (models[i].pts == (point *)NULL) {
	    fprintf(stderr, "can't read data from %s\n", model_fname);
	    exit(1);
	    }
	(void)fclose(model_file);
	shuffle_pts(models[i].pts, models[i].npts);

	models[i].model_frac = model_frac;
	models[i].model_thresh = model_thresh;
	models[i].image_frac = image_frac;
	models[i].image_thresh = image_thresh;

	models[i].leftborder = models[i].rightborder = models[i].xsize - 1;
	models[i].topborder = models[i].bottomborder = models[i].ysize - 1;
	models[i].stepx = models[i].stepy = 1;

	models[i].dtrans = (LongImage)NULL;
	models[i].trans = NULLLIST;
	}

    findTransAllModels(&image, models, nmodels, REVERSE_BOX);

    for (i = 0; i < nmodels; i++) {
	if (models[i].trans == NULLLIST) {
	    fprintf(stderr, "can't generate trans for \"%s\"\n", argv[i + 2]);
	    exit(1);
	    }
	printf("Translations for \"%s\"\n", argv[i + 2]);
	foreach (ln, models[i].trans) {
	    t = (transval *)liGet(ln);
	    printf("(%4ld, %4ld) = (%ld, %g), (%ld, %g)\n",
		   t->transpos.x, t->transpos.y,
		   t->forward_val, t->forward_frac,
		   t->reverse_val, t->reverse_frac);
	    }
	}

    return(0);
    }
