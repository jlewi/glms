/*
 *
 * $Header$
 *
 * Copyright (c) 1990, 1991, 1992, 1993 Cornell University.  All Rights
 * Reserved.  
 *  
 * Copyright (c) 1991, 1992 Xerox Corporation.  All Rights Reserved.  
 *  
 * Use, reproduction, preparation of derivative works, and distribution
 * of this software is permitted.  Any copy of this software or of any
 * derivative work must include both the above copyright notices of
 * Cornell University and Xerox Corporation and this paragraph.  Any
 * distribution of this software or derivative works must comply with all
 * applicable United States export control laws.  This software is made
 * available AS IS, and XEROX CORPORATION DISCLAIMS ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
 * AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
 * LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF XEROX CORPORATION IS ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGES.
 */

static char rcsid[] = "@(#)$Header$";

#include <stdio.h>
#include "misc.h"
#include <math.h>
#include "image.h"
#include "r-h.h"
#include "list.h"
#include "chunk.h"
#include "transhash.h"
#include "dbug.h"

#include "prof.h"

static void findTransMtoI(image_info *image, model_info *model);
static void checkTransMtoI(image_info *image, model_info *model);
static void findTransItoM(image_info *image, model_info *model,
			  int revstyle);
static boolean findTransMtoISampled(image_info *image, model_info *model,
				    int lowX, int highX, int stepX,
				    int lowY, int highY, int stepY,
				    TransHash *th,
				    long thresh, boolean needdata);
static boolean probeRegions(image_info *image, model_info *model,
			    TransHash *lowth, TransHash *highth,
			    int lowX, int highX, int baseX, int stepX,
			    int oldBaseX, int oldStepX,
			    int lowY, int highY, int baseY, int stepY,
			    int oldBaseY, int oldStepY,
			    long thresh, boolean needdata);

static boolean ensure_dtrans(image_info *image, long thresh);

/* About how fine to slice each dimension at the lowest resolution */
#define	SLICE		4

/* What the resolution refinement between steps is */
#define	REFINE		2

/* The usual FP epsilon */
#define	EPSILON	(1e-5)

/* How much of the model to scan before deciding on early decision */
#define	EARLY_FRAC	0.2

/* How big a border to put on the model's distance transform (in pixels) */
#define	MODEL_BORD	4

void
findTransAllModels(image_info *image,
		   model_info *models, unsigned nmodels,
		   int revstyle)
{
    int i;
    int max_leftborder;
    int max_topborder;
    int max_rightborder;
    int max_bottomborder;

    assert(models != (model_info *)NULL);
    assert(image != (image_info *)NULL);
    assert((revstyle == REVERSE_BOX) || (revstyle == REVERSE_ALLIMAGE) ||
	   (revstyle == REVERSE_NONE));

    if (nmodels == 0) {
	return;		/* What's to do? */
	}

    max_leftborder = models[0].leftborder;
    max_topborder = models[0].topborder;
    max_rightborder = models[0].rightborder;
    max_bottomborder = models[0].bottomborder;
    for (i = 1; i < nmodels; i++) {
	max_leftborder = MAX(max_leftborder, models[i].leftborder);
	max_topborder = MAX(max_topborder, models[i].topborder);
	max_rightborder = MAX(max_rightborder, models[i].rightborder);
	max_bottomborder = MAX(max_bottomborder, models[i].bottomborder);
	}

    if ((image->dtrans == (LongImage)NULL) ||
	(image->leftborder < max_leftborder) ||
	(image->topborder < max_topborder) ||
	(image->rightborder < max_rightborder) ||
	(image->bottomborder < max_bottomborder)) {

	/* Were they being very silly? */
	if ((max_leftborder + max_rightborder + (int)image->xsize <= 0) ||
	    (max_topborder + max_bottomborder + (int)image->ysize <= 0)) {
	    /*
	     * They asked for borders which leave no part of the image.
	     * I refuse to deal with this case properly (i.e. deal with the
	     * model->trans fields appropriately). FIX someday?
	     */
	    return;
	    }
	/* The image's distance transform is not present, or is too small */
	if (image->dtrans != (LongImage)NULL) {
	    imFree(image->dtrans);
	    image->dtrans = (LongImage)NULL;
	    }

	if (image->plusx_dtrans != (ShortImage)NULL) {
	    imFree(image->plusx_dtrans);
	    image->plusx_dtrans = (ShortImage)NULL;
	    }

	/*
	 * Generate the image's distance transform. Allow space for the
	 * borders. Also, we need to make sure that every feasible translation
	 * can be reached by some cell whose centre may be outside the actual
	 * range of feasible translations: in order to cover everything, we
	 * may end up probing at a non-feasible translation.
	 */
	image->leftborder = max_leftborder;
	image->topborder = max_topborder;
	image->rightborder = max_rightborder;
	image->bottomborder = max_bottomborder;

	image->dtrans =
	    dtrans_pts((unsigned)ceil((image->leftborder + image->rightborder + image->xsize) *
				      (1. + .5 / SLICE)),
		       (unsigned)ceil((image->topborder + image->bottomborder + image->ysize) *
				      (1. + .5 / SLICE)),
		       -(int)image->leftborder, -(int)image->topborder,
		       image->npts, image->pts);
	if (image->dtrans == (LongImage)NULL) {
	    return;
	    }
	}

    for (i = 0; i < nmodels; i++) {
	/* We'll need the model's dtrans */
	if (models[i].dtrans == (LongImage)NULL) {
	    models[i].dtrans = dtrans_pts(2 * MODEL_BORD + models[i].xsize,
					  2 * MODEL_BORD + models[i].ysize,
					  -MODEL_BORD, -MODEL_BORD,
					  models[i].npts, models[i].pts);
	    if (models[i].dtrans == (LongImage)NULL) {
		return;
		}
	    }

	if (models[i].trans != NULLLIST) {
	    /* We've been handed a list of translations to look at */
	    checkTransMtoI(image, &models[i]);
	    }
	else {
	    findTransMtoI(image, &models[i]);
	    }

	if (models[i].trans == NULLLIST) {
	    return;
	    }

	if (revstyle != REVERSE_NONE) {
	    findTransItoM(image, &models[i], revstyle);
	    }

	if (models[i].trans == NULLLIST) {
	    return;
	    }
	}

    }

/*
 * Find all translations where the model->image distance is <= thresh.
 * Use pruning tricks. The tricks are complicated a little by the fact
 * that we're using a partial match. We therefore have to consider all the
 * model points.
 * Also, we can't simply abort out of a scan when we find a value that is
 * too large, though we can do something similar by counting. The rest of
 * the pruning is still possible, though.
 *
 * The space we search is the space of all translations. We discretise this
 * space to a resolution of one pixel in x and y.
 *
 * We do the whole mess in a multi-resolution manner:
 * the first pass is very coarse, with very high threshold. It tells us
 * what sections look interesting. Further passes refine these sections
 * at increasingly higher resolution. Since there is a definite speed
 * advantage to looking at a large area in transformation space as a
 * single chunk, as opposed to several smaller chunks, we try to merge
 * adjacent interesting sections before going on to the next pass.
 */
static void
findTransMtoI(image_info *image, model_info *model)
{
    TransHash *lowth = (TransHash *)NULL;
    TransHash *highth = (TransHash *)NULL;
    List trans = NULLLIST;
    ListNode newln;
    transval *newt;
    boolean needdata;
    int x, y;
    int lowX, highX, baseX, stepX, oldBaseX, oldStepX;
    int lowY, highY, baseY, stepY, oldBaseY, oldStepY;
    long curthresh;

    /* Stuff cached from image and model */
    int nmodel_pts;
    
    assert(image != (image_info *)NULL);
    assert(model != (model_info *)NULL);
    assert(model->pts != (point *)NULL);
    assert(image->dtrans != (LongImage)NULL);
    assert(model->stepx > 0);
    assert(model->stepy > 0);

    nmodel_pts = model->npts;

    trans = liNew();
    if (trans == NULLLIST) {
	goto bailout;
	}

    /* Get rid of an annoying case */
    if (nmodel_pts == 0) {
	model->trans = trans;
	return;		/* No transformations */
	}

    /* Now figure out what step size we're using at the lowest resolution */

    /*
     * Find out how big the box we're working in is (borders are inclusive
     * on the low side, exclusive on the high side)
     * and calculate lowX, highX, stepX, ...Y for
     * the lowest resolution.
     *
     * What we are determining here:
     * The absolute bounds on X are lowX..highX-1 inclusive. Ditto Y.
     * The lowest resolution cell is stepX by stepY.
     * The first cell is centred at (baseX, baseY).
     */
    /*
     * Possible bug here - we may not hit all the way up to the high end
     * due to truncation in the division by SLICE. Check and fix.
     *
     * No, I think it's right the way it is now, because there's no
     * number of iterations computed; ...Sampled() does however many it
     * needs to for the current stepX. We need to make stepX a multiple
     * of model->stepx though.
     */
    lowX = -image->leftborder;
    highX = (int)(image->xsize + image->rightborder);
    stepX = MAX(model->stepx, (highX - lowX) / SLICE);
    baseX = lowX;
    /* Round stepX down to a multiple of model->stepx */
    stepX = (stepX / model->stepx) * model->stepx;
    
    lowY = -image->topborder;
    highY = (int)(image->ysize + image->bottomborder);
    stepY = MAX(model->stepy, (highY - lowY) / SLICE);
    baseY = lowY;
    stepY = (stepY / model->stepy) * model->stepy;

    /* Allocate the lowest resolution hash table */
    lowth = thNew(lowX, highX, stepX,
		  lowY, highY, stepY);
    if (lowth == (TransHash *)NULL) {
	goto bailout;
	}

    /*
     * Work out the current threshold.
     *
     * We always probe at the centre of the box, so
     * the actual coordinates that we're probing for are:
     * (x - stepX / 2 .. x + stepX - stepX / 2 - 1) x (....Y)
     * so we need to find the distance to the farthest corner. We will be
     * probing at (x, y).
     * If stepX is odd, then stepX / 2 = (stepX - 1) / 2., and so
     * stepX - stepX / 2 - 1 = (stepX - 1) / 2.
     * If stepX is even, then stepX / 2 = stepX / 2., and so
     * stepX - stepX / 2 - 1 = stepX / 2. - 1. In either case, the
     * farther corner is stepX / 2 away.
     * The farthest away we can get in X is therefore stepX / 2, and
     * similarly in Y. Note that all of these are
     * integer divisions.
     * Assuming L_2, this gives us:
     * model_thresh +
     *	ceil(DSCALE * hypot((double)(stepX / 2), (double)(stepY / 2)))
     * However, we have to correct for the fact that stepX and stepY
     * are constrained to be at least model->stepx and model->stepy, and
     * that model->model_thresh *is the threshold to be used at
     * that finest resolution*.
     */
    curthresh = model->model_thresh +
	ceil(DSCALE * hypot((double)((stepX - model->stepx + 1) / 2),
			    (double)((stepY - model->stepy + 1) / 2)));

    VDEBUG("curthresh = %ld\n", curthresh, 5);

    if (!findTransMtoISampled(image, model,
			      lowX, highX, stepX,
			      lowY, highY, stepY,
			      lowth, curthresh, FALSE)) {
	/* Failed. Bah. */
	goto bailout;
	}

    /* We now have the lowest-resolution hash table. Refine... */
    while ((stepX > model->stepx) || (stepY > model->stepy)) {
	/* update stepX etc. keep old ones in oldStepX etc */
	oldStepX = stepX;
	oldBaseX = baseX;
	stepX = MAX(model->stepx, stepX / REFINE);
	baseX = lowX;
	stepX = (stepX / model->stepx) * model->stepx;
	
	oldStepY = stepY;
	oldBaseY = baseY;
	stepY = MAX(model->stepy, stepY / REFINE);
	baseY = lowY;
	stepY = (stepX / model->stepy) * model->stepy;

	/* Update the threshold */
	curthresh = model->model_thresh +
	    ceil(DSCALE * hypot((double)((stepX - model->stepx + 1) / 2),
				(double)((stepY - model->stepy + 1) / 2)));
	
	VDEBUG("curthresh = %ld\n", curthresh, 5);
	
	/*
	 * Determine if we're in highest res yet; store in needdata.
	 */
	if ((stepX == model->stepx) && (stepY == model->stepy)) {
	    needdata = TRUE;
	    }
	else {
	    needdata = FALSE;
	    }

	/* Allocate the new hash table */
	highth = thNew(lowX, highX, stepX,
		       lowY, highY, stepY);
	if (highth == (TransHash *)NULL) {
	    goto bailout;
	    }
	
	/* Pick out regions */
	if (!probeRegions(image, model, lowth, highth,
			  lowX, highX, baseX, stepX, oldBaseX, oldStepX,
			  lowY, highY, baseY, stepY, oldBaseY, oldStepY,
			  curthresh, needdata)) {
	    goto bailout;
	    }

	/* That generated highth for us - make it the new lowth */
	thFree(lowth);
	lowth = highth;
	highth = (TransHash *)NULL;
	}

    /*
     * We now have lowth - the hash table at highest resolution.
     * Read it out into trans.
     */
    while (thGetLowest(lowth, &x, &y)) {
	newt = (transval *)thGet(lowth, x, y);
	thRem(lowth, x, y);
	assert(newt != (transval *)NULL);

	newln = liAdd(trans, newt);
	if (newln == NULLLISTNODE) {
	    /* In this case, we really need to free all hooks... FIX. */
	    goto bailout;
	    }

	}

    /* Free useless stuff */
    thFree(lowth);

    model->trans = trans;

    return;

  bailout:

    if (lowth != (TransHash *)NULL) {
	thFree(lowth);
	}

    if (highth != (TransHash *)NULL) {
	thFree(highth);
	}

    if (trans != NULLLIST) {
	free_translist(trans);
	}

    model->trans = NULLLIST;
    }
    
/*
 * Check the translations that we were handed (in model->trans); remove
 * those where the thresholds aren't satisfied.
 */
static void
checkTransMtoI(image_info *image, model_info *model)
{
    ListNode ln;
    long pointval = 0;
    long *vals = (long *)NULL;
    transval *t;
    int x, y;
    int i;
    int model_required;

    int nover;
    int novermax;

    /* Stuff cached from image and model */
    point *model_pts;
    unsigned nmodel_pts;
    LongImage image_dtrans;
    long model_thresh;
    double model_frac;
    
    assert(image != (image_info *)NULL);
    assert(model != (model_info *)NULL);
    assert(image->dtrans != (LongImage)NULL);
    assert(model->trans != NULLLIST);

    model_pts = model->pts;
    nmodel_pts = model->npts;
    model_thresh = model->model_thresh;
    model_frac = model->model_frac;
    image_dtrans = image->dtrans;

    assert(model_pts != (point *)NULL);

    model_required = (int)(ceil(nmodel_pts * model_frac));
    novermax = nmodel_pts - model_required;

    if (model_frac != 1.) {
	vals = (long *)malloc(nmodel_pts * sizeof(long));
	if (vals == (long *)NULL) {
	    goto bailout;
	    }
	}

    for (ln = liFirst(model->trans); ln != NULLLISTNODE; ) {
	t = (transval *)liGet(ln);
	assert(t != (transval *)NULL);
	x = t->transpos.x;
	y = t->transpos.y;

	if (model_frac == 1.) {
	    pointval = 0;
	    for (i = 0; i < nmodel_pts; i++) {
		pointval = MAX(pointval, imRef(image_dtrans,
					       model_pts[i].x + x,
					       model_pts[i].y + y));
		if (pointval > model_thresh) {
		    break;
		    }
		}
	    }
	else {
	    nover = 0;
	    for (i = 0; i < nmodel_pts; i++) {
		/* Probe at this model point's translated location */
		pointval = vals[i] = imRef(image_dtrans,
					   model_pts[i].x + x,
					   model_pts[i].y + y);
		if (pointval > model_thresh) {
		    nover++;
		    if (nover > novermax) {
			break;
			}
		    }

		}
	    
	    if (i == nmodel_pts) {
		pointval = pickNth(vals, (int)nmodel_pts, model_required - 1);
		assert(pointval <= model_thresh);
		}
	    }

	if (pointval <= model_thresh) {
	    t->forward_val = pointval;
	    if (model_frac == 1.) {
		t->forward_frac = 1.;
		}
	    else {
		t->forward_frac = frac_lower(vals, (int)nmodel_pts,
					     model_thresh);
		assert(t->forward_frac >= model_frac - EPSILON);
		}
		
	    ln = liNext(ln);
	    }
	else {
	    free_trans(t);
	    ln = liRemAndNext(ln);
	    }
	}

    if (model_frac != 1) {
	assert(vals != (long *)NULL);
	free((void *)vals);
	}

    return;

  bailout:
    if (vals) {
	free((void *)vals);
	}

    free_translist(model->trans);
    model->trans = NULLLIST;
    return;
    }

/*
 * This routine scans the points listed in modeltrans, looking for points
 * where at least image_frac of the image points under the model lie within
 * image_thresh of model points.
 */
static void
findTransItoM(image_info *image, model_info *model, int revstyle)
{
    ListNode ln;
    transval *t;
    int x, y;
    int x2, y2;
    long pointval;
    int nvals;
    int nimage = 0;
    int whichval;
    long *vals;
    LongImage image_dtrans;
    LongImage model_dtrans;

    assert(image != (image_info *)NULL);
    assert(model != (model_info *)NULL);
    assert(model->trans != NULLLIST);
    assert(model->dtrans != (LongImage)NULL);
    assert(image->dtrans != (LongImage)NULL);

    /* Cache stuff */
    image_dtrans = image->dtrans;
    model_dtrans = model->dtrans;

    /* Figure out how many image pixels might be visible */
    vals = (long *)malloc(sizeof(*vals) *
			  MIN(image->npts,
			      (model->xsize + 2 * MODEL_BORD) *
			      (model->ysize + 2 * MODEL_BORD)));
    if (vals == (long *)NULL) {
	free_translist(model->trans);
	model->trans = NULLLIST;
	return;
	}

    for (ln = liFirst(model->trans); ln != NULLLISTNODE; ) {
	t = (transval *)liGet(ln);
	assert(t != (transval *)NULL);
	x = t->transpos.x;
	y = t->transpos.y;

	if (revstyle == REVERSE_BOX) {
	    
	    /*
	     * For this, it's almost always wrong to iterate over the
	     * point list, since most of them will be outside the domain
	     * of the model (not underneath it at the moment). We therefore
	     * scan the actual image looking for set points. Actually,
	     * we cheat: we scan the distance transform of the image,
	     * looking for 0s.
	     */
	    nvals = 0;
	    for (y2 = y; y2 < y + (int)model->ysize; y2++) {
		for (x2 = x; x2 < x + (int)model->xsize; x2++) {
		    if (imRef(image_dtrans, x2, y2) == 0) {
			vals[nvals++] = imRef(model_dtrans, x2 - x, y2 - y);
			}
		    }
		}
	    
	    whichval = (int)(ceil(model->image_frac * nvals));
	    pointval = pickNth(vals, nvals, whichval - 1);
	    }
	else {
	    assert(revstyle == REVERSE_ALLIMAGE);
	    /* We need to scan over the entire image. */
	    /* Oh, does my brain hurt... */

	    /*
	     * OK - the deal is that we've got the model's distance transform,
	     * which is the size of the model plus MODEL_BORD pixels border.
	     * If image_thresh is <= MODEL_BORD * DSCALE then we can use this
	     * distance transform: for each set pixel in the image, if it
	     * falls within the distance transform, probe and use the value.
	     * If it doesn't, then its value must be > image_thresh, and so
	     * we can use a large value instead of bothering to probe.
	     *
	     * If image_thresh is > MODEL_BORD, then you're out of luck -
	     * it's going to be hard to figure out some of these values.
	     */
	    if (model->image_thresh < DSCALE * MODEL_BORD) {
		nvals = 0;
		for (y2 = MAX(0, y - MODEL_BORD);
		     y2 < MIN(y + (int)model->ysize + MODEL_BORD,
			      image->ysize);
		     y2++) {
		    for (x2 = MAX(0, x - MODEL_BORD);
			 x2 < MIN(x + (int)model->xsize + MODEL_BORD,
				  image->xsize);
			 x2++) {
			if (imRef(image_dtrans, x2, y2) == 0) {
			    vals[nvals++] =
				imRef(model_dtrans, x2 - x, y2 - y);
			    }
			}
		    }
		nimage = image->npts;
		}
	    else {
		/*
		 * Do it the hard way.
		 * Scan the entire image. If something lies inside the
		 * model's dtrans, probe and use. If it lies far enough
		 * outside it that we know its value is large, ignore.
		 * If it lies somewhere in between, then we get to do
		 * a full find-nearest-neighbour with all the model points...
		 * yay.
		 */
		int allowed = (int)ceil(model->image_thresh / (double)DSCALE);
		int i;

		panic("untested code");	/* I want to know if someone uses this */

		nvals = 0;
		for (y2 = MAX(0, y - allowed);
		     y2 < MIN(y + (int)model->ysize + allowed,
			      image->ysize);
		     y2++) {
		    for (x2 = MAX(0, x - allowed);
			 x2 < MIN(x + (int)model->xsize + allowed,
				  image->xsize);
			 x2++) {
			if (imRef(image_dtrans, x2, y2) == 0) {
			    if ((x2 >= x - MODEL_BORD) &&
				(x2 < x + (int)model->xsize + MODEL_BORD) &&
				(y2 >= y - MODEL_BORD) &&
				(y2 < y + (int)model->ysize + MODEL_BORD)) {
				/* It falls inside - yay */
				vals[nvals++] = imRef(model_dtrans,
						      x2 - x, y2 - y);
				}
			    else {
				/* Go search. */
				/*
				 * N.B. This assumes that we're using L2
				 * as our underlying norm.
				 */
				assert(model->npts > 0);
				assert(model->pts != (point *)NULL);
				
				vals[nvals] =
				    (int)(DSCALE *
					  sqrt((double)
					       ((x2 - x - model->pts[0].x) *
						(x2 - x - model->pts[0].x) +
						(y2 - y - model->pts[0].y) *
						(y2 - y - model->pts[0].y))));
				for (i = 1; i < model->npts; i++) {
				    vals[nvals] =
					MIN(vals[nvals],
					    (int)(DSCALE *
						  sqrt((double)
						       ((x2 - x -
							 model->pts[0].x) *
							(x2 - x -
							 model->pts[0].x) +
							(y2 - y -
							 model->pts[0].y) *
							(y2 - y -
							 model->pts[0].y)))));
				    }
				nvals++;
				}
			    }
			}
		    }
		nimage = image->npts;
		}
	    /*
	     * OK - we have nvals values which have been probed
	     * and nimage - nvals which haven't been, but which would
	     * be large if they were.
	     */
	    whichval = (int)(ceil(model->image_frac * nimage));
	    if (whichval > nvals) {
		/* Reject */
		pointval = model->image_thresh * 2 + 1;
		}
	    else {
		/* Give it a chance */
		pointval = pickNth(vals, nvals, whichval - 1);
		}
	    }

	if (pointval <= model->image_thresh) {
	    /* A good point. Keep it. */
	    t->reverse_val = pointval;
	    if (revstyle == REVERSE_BOX) {
		if (nvals == 0) {
		    t->reverse_frac = 1.;
		    }
		else {
		    t->reverse_frac =
			frac_lower(vals, nvals, model->image_thresh);
		    }
		t->reverse_num = nvals;
		}
	    else {
		if (nvals == 0) {
		    if (nimage == 0) {
			t->reverse_frac = 1.;
			}
		    else {
			t->reverse_frac = 0.;
			}
		    }
		else {
		    t->reverse_frac =
			frac_lower(vals, nvals, model->image_thresh) * nvals /
			    nimage;
		    }
		t->reverse_num = nimage;
		}
	    assert(t->reverse_frac >= model->image_frac - EPSILON);

	    /* and advance */
	    ln = liNext(ln);
	    }
	else {
	    /* Free the hook from modeltrans */
	    free_trans(t);
	    ln = liRemAndNext(ln);
	    }
	}

    free((void *)vals);

    return;
    }

/*
 * Scan a region in transformation space from lowX to highX (inclusive,
 * exclusive) in X, stepping by stepX in X, and similarly for Y.
 * Probe at locations which are cell centres:
 * lowX + stepX / 2, lowX + 3 * stepX / 2, etc.
 *
 * Look for places where there may be something interesting within a cell
 * of size (stepX,stepY) centred around the current position. For
 * each interesting cell you find, add a flag to th. If needdata is TRUE,
 * attach a stransval structure to that flag. Cells are represented
 * by their top left corner position, not the centre position, even though
 * the centre position is what is actually probed.
 *
 * A cell is interesting if the centre position's value is <= thresh.
 */
static boolean
findTransMtoISampled(image_info *image, model_info *model,
		     int lowX, int highX, int stepX,
		     int lowY, int highY, int stepY,
		     TransHash *th,
		     long thresh, boolean needdata)
{
    int i;
    int x, y;
    long *vals = (long *)NULL;
    transval *newt;
    int model_required;
    int model_early;
    int model_early_required;
    int novermax;
    int valsunder;
    long minover;
    long pointval;
    boolean keep;
    
    /* Stuff cached from image and model */
    point *model_pts;
    unsigned nmodel_pts;
    unsigned image_plusx_width;
    LongImage image_dtrans;
    ShortImage image_xdtrans;
    long model_thresh;
    int maxx, maxy;
    
    assert(image != (image_info *)NULL);
    assert(model != (model_info *)NULL);
    assert(model->pts != (point *)NULL);
    assert(image->dtrans != (LongImage)NULL);
    assert(th != (TransHash *)NULL);

    DEBUG("sampling(%d..%d by %d, %d..%d by %d)\n", lowX, highX, stepX, lowY, highY, stepY);

    model_pts = model->pts;
    nmodel_pts = model->npts;
    model_thresh = model->model_thresh;
    maxx = model->xsize - 1;
    maxy = model->ysize - 1;

    image_dtrans = image->dtrans;

    model_required = (int)(ceil(nmodel_pts * model->model_frac));
    novermax = nmodel_pts - model_required;

    model_early = (int)(ceil(nmodel_pts * EARLY_FRAC));
    model_early_required = (int)(ceil(model_early * model->model_frac));

    vals = (long *)malloc(nmodel_pts * sizeof(long));
    if (vals == (long *)NULL) {
	goto bailout;
	}
    
    /* Generate the skip-in-x transform for this threshold */
    if (!ensure_dtrans(image, thresh)) {
	goto bailout;
	}

    assert(image->plusx_dtrans != (ShortImage)NULL);
    image_xdtrans = image->plusx_dtrans;
    image_plusx_width = imGetWidth(image_xdtrans);

    /* Perform the scan. */
    for (y = lowY + stepY / 2; y < highY + stepY / 2; y += stepY) {
	/* Is there a place in this cell where the model fits in the borders? */
	if (y - stepY / 2 + maxy >= (int)(image->ysize + image->bottomborder)) {
	    /* No - even at the top of the cell it goes too far */
	    break;
	    }

	for (x = lowX + stepX / 2;
	     x < highX + stepX / 2;
	     x += stepX) {
	    
	    /* Can this fit in? */
	    if (x - stepX / 2 + maxx >= (int)(image->xsize + image->rightborder)) {
		/* No - even at the left of the cell it goes too far */
		break;
		}
	    
	    valsunder = 0;
	    minover = -1;
	    
	    for (i = 0; i < model_early; i++) {
		pointval = imRef(image_xdtrans,
				 model_pts[i].x + x, model_pts[i].y + y);
		if (pointval == 0) {
		    valsunder++;
		    }
		else {
		    if ((minover < 0) || (pointval < minover)) {
			minover = pointval;
			}
		    if (valsunder + (nmodel_pts - i - 1) <
			model_required) {
			/* DIE YOU GRAVY-SUCKING PIG! */
			break;
			}
		    }

		}
	    /*
	     * At this point, check valsunder for early decision
	     * Note that we aren't allowed to take early decisions at
	     * highest resolution.
	     */
	    if ((needdata) || (valsunder < model_early_required)) {
		/* No early decision - keep probing */
		for (i = model_early; i < nmodel_pts; i++) {
		    pointval = imRef(image_xdtrans,
				     model_pts[i].x + x, model_pts[i].y + y);
		    if (pointval == 0) {
			valsunder++;
			}
		    else {
			if ((minover < 0) || (pointval < minover)) {
			    minover = pointval;
			    }
			if (valsunder + (nmodel_pts - i - 1) <
			    model_required) {
			    /* DIE YOU GRAVY-SUCKING PIG! */
			    break;
			    }
			}
		    }
		if (i == nmodel_pts) {
		    keep = TRUE;
		    }
		else {
		    keep = FALSE;
		    }
		}
	    else {
		keep = TRUE;
		}

	    if (keep) {
		VDEBUG("Found one at (%d ", x, 10);
		VDEBUG("%d)\n", y, 10);
		
		/* Make a mark in th */
		if (needdata) {
		    /* Create a transval to hold the info */
		    newt = malloc_trans();
		    if (newt == (transval *)NULL) {
			goto bailout;
			}
		    newt->transpos.x = x;
		    newt->transpos.y = y;
		    
		    /* Work out the actual pointval */
		    for (i = 0; i < nmodel_pts; i++) {
			vals[i] = imRef(image_dtrans,
					model_pts[i].x + x, model_pts[i].y + y);
			}
		    pointval = pickNth(vals, (int)nmodel_pts,
				       model_required - 1);
		    newt->forward_val = pointval;
		    newt->forward_frac =
			frac_lower(vals, (int)nmodel_pts,
				   model_thresh);
		    }
		else {
		    newt = (transval *)NULL;
		    }
		
		/* The mark goes at the top left corner of the box */
		if (!thIsIn(th, x - stepX / 2, y - stepY / 2)) {
		    if (!thAdd(th, x - stepX / 2, y - stepY / 2,
			       (void *)newt)) {
			if (newt != (transval *)NULL) {
			    free((void *)newt);
			    }
			goto bailout;
			}
		    }
		}
	    else {
		/* Do some simple pruning */
		if (minover > image_plusx_width) {
		    /* Big skip */
		    break;
		    }
		
		/* Round up to the next point */
		if (stepX != 1) {
		    minover += stepX - 1;
		    minover -= minover % stepX;
		    }
		/* Precompensate for the loop increment */
		x += minover - stepX;
		
		}
	    }
	}
    
    /* Free now-useless stuff */
    free((void *)vals);

    /* and done. */
    return(TRUE);

  bailout:
    if (vals != (long *)NULL) {
	free((void *)vals);
	}

    return(FALSE);
    }

/*
 * Given lowth, a hash table containing interesting regions at one
 * resolution (lowX..highX by oldStepX etc), we want to compute
 * highth, a hash table containing interesting regions at another resolution
 * (lowX..highX by stepX etc), and store this in highth.
 *
 * We do this by finding "rectangular" regions in lowth which are
 * interesting, and calling findTransMtoISampled() on each, telling
 * it to store the results in highth. Once every region flagged as interesting
 * in lowth has been processed, we're done.
 * Each region is represented by its top left corner.
 * thresh defines what the threshold of "interesting" is.
 */
static boolean
probeRegions(image_info *image, model_info *model,
	     TransHash *lowth, TransHash *highth,
	     int lowX, int highX,
	     int baseX, int stepX, int oldBaseX, int oldStepX,
	     int lowY, int highY,
	     int baseY, int stepY, int oldBaseY, int oldStepY,
	     long thresh, boolean needdata)
{
    int botX, botY;
    int topX, topY;
    int x, y;
    boolean blockedX, blockedY;
    int nLow;
    int nLowTot = 0;
    int nProbes = 0;

    assert(image != (image_info *)NULL);
    assert(model != (model_info *)NULL);
    assert(lowth != (TransHash *)NULL);
    assert(highth != (TransHash *)NULL);

    while (thGetLowest(lowth, &x, &y)) {
	/* Expand this in increasing x, y as far as possible */
	topX = botX = x;
	topY = botY = y;

	/* We're not blocked in any dimension */
	blockedX = blockedY = FALSE;

	while ((!blockedX) || (!blockedY)) {
	    /* Expand in X as far as we can go */
	    while (!blockedX) {
		/* Try to expand one tick in X */
		if (topX + oldStepX >= highX) {
		    /* We'd go out of bounds */
		    blockedX = TRUE;
		    }
		else {
		    for (y = botY; y <= topY; y += oldStepY) {
			if (!thIsIn(lowth, topX + oldStepX, y)) {
			    /* Can't expand any more in X */
			    blockedX = TRUE;
			    break;
			    }
			}
		    }

		if (!blockedX) {
		    topX += oldStepX;
		    }
		}
	    
	    if (!blockedY) {
		/* Try to expand one tick in Y */
		if (topY + oldStepY >= highY) {
		    /* We'd go out of bounds */
		    blockedY = TRUE;
		    }
		else {
		    for (x = botX; x <= topX; x += oldStepX) {
			if (!thIsIn(lowth, x, topY + oldStepY)) {
			    /* Can't expand any more in Y */
			    blockedY = TRUE;
			    break;
			    }
			}
		    }

		if (!blockedY) {
		    topY += oldStepY;
		    }
		}
	    
	    }

	/*
	 * We now have a rectangular region, all of which was interesting,
	 * in (botX..topX)(botY..topY). Now,
	 * these are actually top-left-corner of cell coordinates. We
	 * want to end up with botX being the lowest X coordinate in the
	 * region that we want scanned, and topX being one more than the
	 * highest one: we want to check transformations which have X values
	 * in the botX..topX-1 range; find...Sampled will actually check
	 * ones spaced every stepX apart, starting at botX + stepX / 2.
	 *
	 * However, we must ensure that each resolution level has consistent
	 * coordinates: no matter where the current region of interest is,
	 * we want to have the higher-resolution cells we generate be
	 * on spacings which are consistent with all other high-resolution
	 * cells we may generate for other regions of interest.
	 */

	/* First, remove this region from lowth */
	nLow = 0;
	for (x = botX; x <= topX; x += oldStepX) {
	    for (y = botY; y <= topY; y += oldStepY) {
		nLow++;
		thRem(lowth, x, y);
		}
	    }

	/* Now compensate for the cell coordinate frame */
	/*
	 * Round down botX to be a multiple of stepX above lowX, and
	 * round topX to be a multiple of stepX above botX. This ensures
	 * that everything will line up with cells generated for other regions
	 * of interest.
	 * We want topX to be the first translation *not* considered at higher
	 * resolution, so we make it one cell higher than we would if
	 * it were the inclusive limit.
	 * We also must increase topX by oldStepX - 1, since that
	 * is the highest actual translation considered in this region of
	 * interest; we then round it down so that it is the left corner
	 * of the cell containing that translation.
	 */
    DEBUG("interest(%d..%d by %d, %d..%d by %d) = %d\n", botX, topX, stepX, botY, topY, stepY, nLow);
	botX -= (botX - lowX) % stepX;
	topX += oldStepX + stepX - 1;
	topX -= (topX - lowX) % stepX;

	botY -= (botY - lowY) % stepY;
	topY += oldStepY + stepY - 1;
	topY -= (topY - lowY) % stepY;

	if (!findTransMtoISampled(image, model,
				  botX, topX, stepX,
				  botY, topY, stepY,
				  highth, thresh, needdata)) {
	    /* Something failed down the line */
	    return(FALSE);
	    }
	nLowTot += nLow;
	nProbes++;

	}
    DEBUG("level covered %d cells in %d probes (%f avg)\n",
	    nLowTot, nProbes, nLowTot / (double)nProbes);

    return(TRUE);
    }

/*
 * Make sure that the image's +x distance transform exists and is based
 * on the correct threshold.
 */
static boolean
ensure_dtrans(image_info *image, long thresh)
{
    DEBUG("ensure_dtrans(%d)\n", thresh);

    if ((image->plusx_dtrans == (ShortImage)NULL) ||
	(thresh != image->dtrans_thresh)) {
	DEBUG("rebuild from %d\n", image->dtrans_thresh);
	/* Need to rebuild the +x dtrans */
	if (image->plusx_dtrans != (ShortImage)NULL) {
	    imFree(image->plusx_dtrans);
	    }

	image->plusx_dtrans =
	    dtrans_plusx(image->dtrans, thresh);

	if (image->plusx_dtrans == (ShortImage)NULL) {
	    return(FALSE);
	    }

	image->dtrans_thresh = thresh;

	}
    return(TRUE);
    }

