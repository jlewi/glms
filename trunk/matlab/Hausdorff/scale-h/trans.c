/*
 *
 * $Header$
 *
 * Copyright (c) 1990, 1991, 1992, 1993 Cornell University.  All Rights
 * Reserved.  
 *  
 * Copyright (c) 1991, 1992 Xerox Corporation.  All Rights Reserved.  
 *  
 * Use, reproduction, preparation of derivative works, and distribution
 * of this software is permitted.  Any copy of this software or of any
 * derivative work must include both the above copyright notices of
 * Cornell University and Xerox Corporation and this paragraph.  Any
 * distribution of this software or derivative works must comply with all
 * applicable United States export control laws.  This software is made
 * available AS IS, and XEROX CORPORATION DISCLAIMS ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
 * AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED HEREIN, ANY
 * LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF XEROX CORPORATION IS ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGES.
 */

static char rcsid[] = "@(#)$Header$";

#include "chunk.h"
#include "list.h"
#include "panic.h"
#include "misc.h"
#include "scale-h.h"

/*
 * This module handles allocation and deallocation of stransval and pqHook
 * structures. Each one gets allocated from a Chunk, for minimal overhead.
 */

static Chunk stransChunk = NULLCHUNK;
static Chunk spqHookChunk = NULLCHUNK;

stransval *
malloc_strans(void)
{
    stransval *tv;

    if (stransChunk == NULLCHUNK) {
	stransChunk = chNew(sizeof(stransval));
	if (stransChunk == NULLCHUNK) {
	    return((stransval *)NULL);
	    }
	}
    tv = (stransval *)chAlloc(stransChunk);
    return(tv);
    }

void
free_strans(stransval *tv)
{
    assert(tv != (stransval *)NULL);
    assert(stransChunk != NULLCHUNK);

    chFree(stransChunk, (void *)tv);
    }

void
free_stranslist(List l)
{
    ListNode ln;
    stransval *t;

    assert(l != NULLLIST);

    foreach (ln, l) {
	t = (stransval *)liGet(ln);
	assert(t != (stransval *)NULL);
	free_strans(t);
	}
    liFree(l);
    }

spqHook *
malloc_spqHook(void)
{
    spqHook *spqh;

    if (spqHookChunk == NULLCHUNK) {
	spqHookChunk = chNew(sizeof(spqHook));
	if (spqHookChunk == NULLCHUNK) {
	    return((spqHook *)NULL);
	    }
	}
    spqh = (spqHook *)chAlloc(spqHookChunk);

    return(spqh);
    }

void
free_spqHook(spqHook *spqh)
{
    assert(spqh != (spqHook *)NULL);
    assert(spqHookChunk != NULLCHUNK);

    chFree(spqHookChunk, (void *)spqh);
    }

void
free_spqHooklist(PriQ spq)
{
    PriQNode spqn;
    spqHook *spqh;

    assert(spq != NULLPRIQ);

    for (spqn = pqFirst(spq); spqn != NULLPRIQNODE; spqn = pqFirst(spq)) {
	spqh = (spqHook *)pqGet(spqn);
	assert(spqh != (spqHook *)NULL);
	free_spqHook(spqh);
	pqRem(spqn);
	}
    pqFree(spq);
    }

