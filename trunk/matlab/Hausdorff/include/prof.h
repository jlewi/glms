/*
 *
 * $Header$
 *
 * Copyright (c) 1990, 1991 Cornell University.  All Rights Reserved.
 *
 * Copyright (c) 1991 Xerox Corporation.  All Rights Reserved.
 *
 * Use, reproduction and preparation of derivative works of this software is
 * permitted.  Any copy of this software or of any derivative work must
 * include both the above copyright notices of Cornell University and Xerox
 * Corporation and this paragraph.  This software is made available AS IS, and
 * XEROX CORPORATION DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
 * WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE, AND NOTWITHSTANDING ANY OTHER PROVISION CONTAINED
 * HEREIN, ANY LIABILITY FOR DAMAGES RESULTING FROM THE SOFTWARE OR ITS USE IS
 * EXPRESSLY DISCLAIMED, WHETHER ARISING IN CONTRACT, TORT (INCLUDING
 * NEGLIGENCE) OR STRICT LIABILITY, EVEN IF XEROX CORPORATION IS ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGES.
 */

/*
 * This file provides a GCC-compatible version of the MARK macro,
 * as used by prof(1).
 */

#ifndef	_PROF_H
#define	_PROF_H

#ifndef MARK

#define	MARK(L)	{}

#else

#ifndef	sparc

Sorry, this only works on sparcs today.

#else
#undef MARK

#define	MARK(L) {\
    asm(".data");\
    asm("	.align 4");\
    asm("." #L ".:");\
    asm("	.word 0");\
    asm(".text");\
    asm("M." #L ".:");\
    asm("	sethi %hi(." #L ".), %o0");\
    asm("	call mcount" : : : "%o0", "%o1", "%o2", "%o3", "%o4", "%o5");\
    asm("	or %o0, %lo(." #L ".), %o0");\
    }

#endif

#endif

#endif
