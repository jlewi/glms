/*
 *
 *	$Header$
 *
 */
/*
 *
 * debug.h - the ever-useful DEBUG() macro.
 *
 */

#ifndef DEBUG_H
#define DEBUG_H

#ifdef DBUG

/* Need stdio to output to stderr - include it to make sure */
#include <stdio.h>

#ifndef VERBLEVEL
#define	VERBLEVEL	0
#endif

#ifdef __GNUC__

#define DEBUG(s, v...)	(void)fprintf(stderr, s, ##v)

#else

#define	DEBUG(s, v)	(void)fprintf(stderr, s, v)

#endif

#define	VDEBUG(s, v, n)	do {if (n <= VERBLEVEL) DEBUG(s, v);} while(0)

#else

#ifdef	__GNUC__
#define	DEBUG(s, v...)
#else
#define	DEBUG(s, v)
#endif

#define	VDEBUG(s, v, n)

#endif

#endif
