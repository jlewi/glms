/*
 *
 *	$Header$
 *
 */
/*
 *
 * panic.h - declaration of panic functions.
 *
 */

#ifndef PANIC_H
#define PANIC_H

/*
 * These two are used for panics: internal consistency check failure
 */
extern void _panic(char *why, char *rcsid, int line, char *file) __attribute__ ((noreturn));
#define panic(x)        _panic((x),rcsid,__LINE__,__FILE__)
/*
 * assert is for internal checks. In the production version it can be
 * redefined to null, for speed.
 */
#define assert(x)       if (!(x)) panic("assert failure")
/* An expression version of assert */
#define	eassert(x)	(!(x) ? (panic("assert failure"), 0) : 0)

#endif
