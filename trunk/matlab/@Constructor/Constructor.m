
% Static class which provides functions for parsing the inputs to a clas and deciding which constructor should be called.
%
% Constructor.empty was returning [] instead of -1
%   so I renamed constructor.empty to emptyin.
classdef (ConstructOnLoad=true) Constructor
    %**********************************************************
    %Define Members of object
    %**************************************************************
    % version - version number for the object
    %           store this as yearmonthdate
    %           using 2 digit format. This way version numbers follow numerical
    %           order
    %           -version numbers make it easier to maintain compatibility
    %               if you add and remove fields
    %

    properties(SetAccess=private, GetAccess=public)
        version=090120;

    end

    properties(GetAccess=public,Constant)
        %what id to return when no input arguments;
        noargs=0;
        
        %input is []
        emptyin=-1;
        
        %input is struct()
        emptystruct=-2;
        
        %id for no match to constructor
        nomatch=-3;
    end
    methods(Static)
        %return the id of the constructor called a structure array of the
        %parameters passed
        [cind,params]=id(var,con);
        
    end
    methods
        function obj=Constructor(varargin)

            %**************************************************************
            %Required parameters/Multiple constructors
            %*****************************************************************
            %each element of the con array describes a constructor
            %   .rparams - string array of the required parameters
            %blank constructor for loading the object.
            con(1).rparams={};

            con(2).rparams={'field1','parm2'};



            switch nargin
                case 0
                    %***************************************************
                    %Blank Construtor: used by loadobj
                    %***************************************************
                    params=struct();
                    cind=0;
                otherwise
                    %determine the constructor given the input parameters
                    [cind,params]=constructid(varargin,con);
            end


            %**************************************************************************
            %otherwise parse out which parameters are for this class and
            %which are for the base class
            %********************************************************************
            switch cind
                case 0
                    %used by load object do nothing
                    bparams=struct();

                otherwise
                    if isnan(cind)
                        error('no constructor matched');
                    else
                        %remove fields which are for this class
                        try
                            bparams=rmfield(params,'field');
                        catch
                        end
                    end
            end


            %*****************************************************
            %Call SuperClass Constructor
            %*****************************************************
            %extract parameters for superclass
            %bparams=params;
            %obj=obj@BaseClass(bparams);

            %****************************************************
            %different constructors for this object
            %******************************************************
            switch cind
                case 0
                    %do nothing used by load object

                otherwise
                    error('Constructor not implemented')
            end



        end
    end
end


