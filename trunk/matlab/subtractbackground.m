%function img=subtractbackground(background,img,threshold)
%
%   background- the RGB image representing the background
%   img       - img from which to subtract the background
%   threshold - the amt of difference that must exist in one of the rgb channels for pixel to be considered
%               different from foreground
%
%   return    -
%           diff - 2 dimensional array that is same size as img. It is not an RGB image but is 0 or 1 which specifies
%                 whether pixel is background or foreground. To get actual pixel values of original image at these 
%                 location, you could take this matrix and multiply it term by term by each channel in the original image
function diff=subtractbackground(background,img,threshold)
    %default value for threshold
    if exist('threshold','var')==0
        threshold=20;
    end
    
    %subtract each color channel of the img from the background and take the absolute value
    diff=zeros(size(img));
    diff(:,:,1)=abs(background(:,:,1)-img(:,:,1));
    diff(:,:,2)=abs(background(:,:,2)-img(:,:,2));
    diff(:,:,3)=abs(background(:,:,3)-img(:,:,3));
    
    %take maximum difference in any channel for all pixels
    diff=max(diff,[],3);
    
    %not compare it threshold
    diff= diff >=threshold;
