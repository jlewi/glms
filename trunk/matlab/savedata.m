%function [fname]=savedata(fname,vnames,opts)
%   fname  - name of file to save to
%   vnames - structure whose fields correspond to names of variables
%            to save
%
%   opts   - Options for saving
%          - .append = 1
%          - .cdir   = 1  -create directory if it doesn't exist
% Explanation: Saves the data listed in vnames to fname.
%   Vnames is a structure whose fields are the names of the variables. 
%   To save. Data is not actually passed to the savedata function
%   instead I use the function "evalin" to execute the save command
%   in the namespace of the calling function. This avoids passing large
%   data.
%
%   By default it appends any variables to the file
%   Performs error checking:
%       1. If filename allready exists it gets the next availble filename
%           by using seqfname (This could potentially cause weird
%           numbering if the filename already has a number such as _0001
%           appended to it
%           unless opts.append=1 in which case data is appended
%       2. Checks the directory for the file exists. If it doesn't it will
%           save to the working directory
%  Return
%       fname - name to which it was saved
function [fname]=savedata(fname,vnames,opts)

optstr='';      %string of options
if ~(exist('opts','var'))
    opts=[];
end
if ~(isfield(opts,'append'))
    opts.append=0;
else
    if (strcmpi(opts.append,'y') | strcmpi(opts.append,'yes') | opts.append==1)
        opts.append=1;
    end
    if (opts.append=='n' | opts.append=='N' | opts.append=='No' | opts.append==0)
        opts.append=0;
    end
    
end

if ~(isfield(opts,'cdir'))
    opts.cdir=0;
end

[pathstr,name,ext] = fileparts(fname);

%check if directory exists
if ~(exist(pathstr,'dir'))
    fprintf('Warning: Directory %s does not exist. \n', pathstr);
    if (opts.cdir~=0)
         fprintf('Creating: Directory \n');
         r=mkdir(pathstr);
         if (r==0)
             fprintf('Could not create directory %s. \n', pathstr);
             fprintf('Warning: Saving to working directory %s \n', pwd);
            pathstr=pwd;
         end
    else
        fprintf('Warning: Saving to working directory %s \n', pwd);
        pathstr=pwd;
    end
end

%fullfile path
%this takes care of pathsepartor charactor for the platform
fname=fullfile(pathstr, [name ext]);

if (exist(fname,'file') & ~(opts.append>0))
    fprintf('Warning: File %s exists \n',fname);
    %get the next available filename
    fname=seqfname(fname);
    %make sure that file doesn't exist
    %that can happen 
    if exist(fname,'file')
        fname=seqfname(fname);
    end
    %check it again
    if exist(fname,'file')
        fprintf('Error: Could not save data. Could not find unused file \n');
        return;
    else
        fprintf('Saving to %s instead \n',fname);
    end
end

%append each variable name to the file
names=fieldnames(vnames);

%loop through each name and save it if it exits
%create the string of variables to save
%do this so we only have to execute save once
%presumably this more efficent than repeated calls to save
varstr='';

%create a structure of the saved variable names
vsaved=[];
for index=1:length(names)
    cmd=sprintf('exist(\''%s\'',\''var\'');',names{index});
    er=evalin('caller',cmd);
    %make sure variable exists
    if (er==1)
        %save it
        %cmd=sprintf('save(\''%s\'',\''%s\'',\''-Append\'');',fname,names{index})
        %cmd=sprintf('save(\''%s\'',\''%s\'')',fname,names{index})
        varstr=sprintf('%s,\''%s\''',varstr,names{index});        
        vsaved.(names{index})=1;    %indicate it was saved        
    else
        fprintf('Warning: Could not save %s. Variable does not exist \n',names{index});
    end
end

%build the string of options
%only append if file already exists
if (exist(fname,'file') & (opts.append~=0))
    optstr=strcat(optstr,sprintf(',''-Append'''));
end
%save variables if not emptry
if ~(isempty(varstr))
    cmd=sprintf('save(\''%s\''%s%s);',fname,varstr,optstr);
    try
        evalin('caller',cmd);
        catch
            fprintf('Error trying to save %s \n', names{index});
            fprintf('Error msg: %s \n',lasterr);
    end
else
    fprintf('No data to save \n');
end