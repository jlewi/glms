%function x=mvgausssamp (mu,covar,n)
%
%   mu- mean of gaussian
%   covar -covariance matrix of gaussian
%
%   n  -number of samples to generate
%       -optional
% Explanation: Sample a multivariate gaussian
%       code is based on homework 8 for Lucas Parra's signal modelling class
% Revision history:
%   4-02 - switched to svd. eig was giving me nearly zero imaginary parts
%   even when it shouldn't be
function x=mvgausssamp(mu,covar,numsamples)

if ~exist('numsamples' ,'var')
    numsamples=1;
end

n=size(mu,1);

%generate zero-mean, unit variance samples for the n Variables
z=randn(n,numsamples);


%Determine the transformation W to transform the n normally distributed variables
%into n variables with covariance matrix covar
% W= U D^.5    where U is the eigen vectors of Rp & D is the matrix of eigne values
%[U D]=eig( covar);
[U D]=svd( covar);
W= U * (D.^.5);

%transform the variables z into x
x= W * z;


%now add the mean
x=x+mu*ones(1,size(x,2));