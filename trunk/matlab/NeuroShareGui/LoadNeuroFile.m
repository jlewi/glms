%function LoadNeuroFile(handles)
%   handles - handles for the gui
%
% Explanation:
%   Gets a filename and loads the apropriate results.
%
% Author: Jeremy Lewi
% Date: 2-02-2005
function [handles]=LoadNeuroFile(handles)
nsLibFile=handles.ns.nsLibFile;   %set the library file
                                %this is the neuroshare api file to use
                                
%get the filename to open
currdir=pwd;
%switch to directory before opening dialog box if it exists
startdir='C:\Documents and Settings\jlewi\My Documents\my_files\work\Research\phd\rotation\working\neuron_classification\data';
if exist(startdir,'dir')==7
    cd(startdir);
end

[filename, pathname] = uigetfile('*.smr', 'Choose Spike2 Data File;');


cd(currdir);


%close any open file or else we will get an error
if isfield(handles,'ns')
    if isfield (handles.ns,'hfile')
    if (handles.ns.hfile>0)
        ns_CloseFile(handles.ns.hfile);
    end
    end
end

%verify the dll library file exists
if ~(exist(nsLibFile,'file'))
    emsg=sprintf('The dll file: \n %s \n', nsLibFile);
    emsg=sprintf('%s does not exist. \n',emsg);
    emsg=sprintf('%s Unable to open file.',emsg);
    errordlg(emsg,'DLL not found');
    return;
end

ns_SetLibrary(nsLibFile);
[ns_result,hfile]=ns_OpenFile([pathname '\' filename]);
set(handles.neurosharegui_fig,'name', sprintf('Neurosharegui: %s',filename));
handles.ns.hfile=hfile;
% Get file information
[nsresult, FileInfo] = ns_GetFileInfo(hfile);
% Gives you EntityCount, TimeStampResolution and TimeSpan
if (nsresult ~= 0)
    disp('Data file information did not load!');
    return
end

%get file info
handles.ns.FileInfo=FileInfo;
% Build catalogue of entities
[nsresult, EntityInfo] = ns_GetEntityInfo(hfile, [1 : 1 : FileInfo.EntityCount]);

handles.ns.EntityInfo=EntityInfo;

handles.ns.NeuralList = find([EntityInfo.EntityType] == 4);    % List of EntityIDs needed to retrieve the information and data
handles.ns.SegmentList = find([EntityInfo.EntityType] == 3);
handles.ns.AnalogList = find([EntityInfo.EntityType] == 2);
handles.ns.EventList = find([EntityInfo.EntityType] == 1);

%print out information about the file
fprintf('Number Neural Events \t %d \n',length(handles.ns.NeuralList));
fprintf('Number Segments \t %d \n',length(handles.ns.SegmentList));
fprintf('Number Analog Events \t %d \n',length(handles.ns.AnalogList));
fprintf('Number vents \t %d \n',length(handles.ns.EventList));

numAnalog=length(handles.ns.AnalogList);
numSegs=length(handles.ns.SegmentList);
if (numAnalog+numSegs) >0
    labels=cell(1,numAnalog + numSegs);
end

%the entries of the listbox are numbered consecutively
%therefore we need to maintain a separate list
%which lists the entityID of each element in the listbox
handles.guiData.listIDs=[];

if numAnalog >0
    %Add a list of entities to the listbox
    for index=1:length(handles.ns.AnalogList)
        eIndex=handles.ns.AnalogList(index);
        labels{index}=handles.ns.EntityInfo(eIndex).EntityLabel;
    end
    handles.guiData.listIDs=handles.ns.AnalogList;
end

if numSegs >0
%Add a list of entities to the listbox
for index=1:length(handles.ns.SegmentList)
    eIndex=handles.ns.SegmentList(index);
    labels{index}=handles.ns.EntityInfo(eIndex).EntityLabel;
end
    handles.guiData.listIDs=[handles.guiData.listIDs handles.ns.SegmentList];
end

if exist('labels','var')

 set(handles.cbo_entitylist,'String',labels,'Value',1) % Load listbox
    
 %update the data displayed in the gui
 handles=updateData(handles);
else
set(handles.cbo_entitylist,'String','','Value',1) % Load listbox
handles.guiData.listIDs=[];
end