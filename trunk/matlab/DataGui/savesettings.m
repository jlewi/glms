%function savesettings(handles)
%
% Explanation:
%   Saves settings for the datagui such as last directory
function savesettings(handles)
    param=handles.param;
    save(handles.param.settingsfname,'param');
end
