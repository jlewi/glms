%function handles=initdatagui(handles)
%       handles - handles object
%
%initializes the handles structure for the datagui
function handles=initdatagui (handles)

%**************************************************************************
%Add Structures to Handles
%**************************************************************************
%param is structure to hold info about how we look at data such as 
%notplotted means the currently selected data hasn't been plotted yet
param=struct('notplotted',1,'settingsfname','datagui_settings.mat');   

data=struct('signals',[],'numsignals',0,'filename','','timeWindow',[]);
handles.data=data;       %holds structure containing all the info and data
handles.param=param;    
handles.param.index=-1;  %points to signal which is actually being plotted
                         %which may not be the signal which is selected
                         %because we don't plot right away when a new
                         %signal is selected
                         %be very careful how this is used and how it is
                         %changed
handles.param.startdir=pwd; %start directory for opening files and other file operations

handles.guiData=[];    %holds information about data displayed in the gui
handles=setdefaults(handles);   %call function to set default values

%clear the axes from any previous runs
cla(handles.maxes);
set(handles.txt_info,'String','');

%***********************************************************
%Load any saved settings
%***********************************************************
%test if a settings file exists
if (exist(handles.param.settingsfname,'file'))
    settings=load(handles.param.settingsfname);
    handles.param.startdir=settings.param.startdir;
end
