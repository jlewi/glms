function varargout = datagui(varargin)
% datagui M-file for datagui.fig
%      datagui, by itself, creates a new datagui or raises the existing
%      singleton*.
%
%      H = datagui returns the handle to a new datagui or the handle to
%      the existing singleton*.
%
%      datagui('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in datagui.M with the given input arguments.
%
%      datagui('Property','Value',...) creates a new datagui or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before datagui_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to datagui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help datagui

% Last Modified by GUIDE v2.5 26-Nov-2005 19:17:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @datagui_OpeningFcn, ...
                   'gui_OutputFcn',  @datagui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before datagui is made visible.
function datagui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to datagui (see VARARGIN)

% Choose default command line output for datagui
handles.output = hObject;



%*************************************************************************
%Call initialization function
%which establishes all the variables for handles
%************************************************************************
handles=initdatagui(handles);

% UIWAIT makes datagui wait for user response (see UIRESUME)
% uiwait(handles.datagui_fig);

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = datagui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%file = uigetfile('*.fig');
%if ~isequal(file, 0)
%    open(file);
%end

%Load a file
handles=LoadFile(handles);
guidata(handles.datagui_fig,handles)  
%[nsresult, ContinuousCount, wave] = ns_GetAnalogData(hfile,3,1,5000);

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.datagui_fig)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.datagui_fig,'Name') '?'],...
                     ['Close ' get(handles.datagui_fig,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

%save settings
savesettings(handles);
%*****************************************************************
delete(handles.datagui_fig)


% --- Executes on selection change in cbo_entitylist.
function cbo_entitylist_Callback(hObject, eventdata, handles)
% hObject    handle to cbo_entitylist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns cbo_entitylist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cbo_entitylist

%change the index in handles.param.index
handles.param.index=get(handles.cbo_entitylist,'value');
handles.param.notplotted=1;                                %indicates we haven't plotted it yet
guidata(hObject, handles)
% --- Executes during object creation, after setting all properties.
function cbo_entitylist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cbo_entitylist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end




% --- Executes on button press in cmd_update.
function cmd_update_Callback(hObject, eventdata, handles)
% hObject    handle to cmd_update (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%make sure we've loaded results
if isfield(handles,'ns')
    handles=updateData(handles);
    %totaltime = handles.ns.FileInfo.TimeSpan; %seconds
    %[nsresult, ContinuousCount, wave] = ns_GetAnalogData(handles.ns.hfile,get(handles.cbo_entitylist,'value'),1,totaltime);
    %plot the data
    %axes(handles.maxes);
    %plot(wave);
end
guidata(hObject, handles);


% --------------------------------------------------------------------
function mnu_copyws_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_copyws (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Create a variable in the user workspace called data
% which stores whatever data is currently plotted in the axesh

% --------------------------------------------------------------------
function mnu_data_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%make sure we've loaded results
data=handles.data;
index=get(handles.cbo_entitylist,'value');
if data.numsignals>0
    assignin('base','data',get(data.signals{index},'data'));
end


% --------------------------------------------------------------------
function mnu_copyws_var_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_copyws_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%make sure we've loaded results
if isfield(handles,'ns')
    totaltime = handles.ns.FileInfo.TimeSpan; %seconds
    index=get(handles.cbo_entitylist,'value');
    eindex=handles.guiData.listIDs(index);
    
    %get the name of the variable to store it in
    varname = inputdlg('Name of variable to store data in','Save to Workspace');
    varname{1}=sprintf('%s',varname{1});
    varname{2}=sprintf('t%s',varname{1});
    assignin('base',varname{1},handles.guiData.wave);
    assignin('base',varname{2},handles.guiData.t);
end




function txt_tstart_Callback(hObject, eventdata, handles)
% hObject    handle to txt_tstart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_tstart as text
%        str2double(get(hObject,'String')) returns contents of txt_tstart as a double

%this should execute after the text box loses focus

h=handles.txt_tstart;      %handle to the textbox
val=get(h,'String');
val=str2num(val);
if (handles.data.numsignals>0)
    %make sure value is a number if not reset it to last valid value
    index=handles.param.index;
    if (~(isempty(val)))
        handles.data.timeWindow{index}=set(handles.data.timeWindow{index},'start',(val));
        %set the other text boxes
        %in case the values changed
        set(handles.txt_tstop,'String',get(handles.data.timeWindow{index},'stop'));
        set(handles.txt_twidth,'String',get(handles.data.timeWindow{index},'width'));
        updateData(handles);
    else
        set(h,'String',get(handles.data.timeWindow{index},'start'));
        msgbox('Invalid value for T Start','Param Error','error')
    end
end
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function txt_tstart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_tstart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function txt_tstop_Callback(hObject, eventdata, handles)
% hObject    handle to txt_tstop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_tstop as text
%        str2double(get(hObject,'String')) returns contents of txt_tstop as a double
%this should execute after the text box loses focus

h=handles.txt_tstop;      %handle to the textbox
val=get(h,'String');
val=str2num(val);
if (handles.data.numsignals>0)
    %make sure value is a number if not reset it to last valid value
    index=get(handles.cbo_entitylist,'value')
    if (~(isempty(val)))
        handles.data.timeWindow{index}=set(handles.data.timeWindow{index},'stop',(val));
        %set the other text boxes
        %in case the values changed
        set(handles.txt_tstart,'String',get(handles.data.timeWindow{index},'start'));
        set(handles.txt_twidth,'String',get(handles.data.timeWindow{index},'width'));
        updateData(handles);
        
    else
        set(h,'String',get(handles.data.timeWindow{index},'stop'));
        msgbox('Invalid value for T Stop','Param Error','error')
    end
end

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function txt_tstop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_tstop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end




function txt_twidth_Callback(hObject, eventdata, handles)
% hObject    handle to txt_twidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_twidth as text
%        str2double(get(hObject,'String')) returns contents of txt_twidth as a double
h=handles.txt_twidth;      %handle to the textbox
val=get(h,'String');
val=str2num(val);
if (handles.data.numsignals>0)
    %make sure value is a number if not reset it to last valid value
    index=get(handles.cbo_entitylist,'value')
    if (~(isempty(val)))
        handles.data.timeWindow{index}=set(handles.data.timeWindow{index},'width',(val));
        %set the other text boxes
        %in case the values changed
        set(handles.txt_tstart,'String',get(handles.data.timeWindow{index},'start'));
        set(handles.txt_tstop,'String',get(handles.data.timeWindow{index},'stop'));
        updateData(handles);
        
    else
        set(h,'String',get(handles.data.timeWindow{index},'width'));
        msgbox('Invalid value for T width','Param Error','error')
    end
end
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function txt_twidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_twidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end




% --------------------------------------------------------------------
function mnu_savefigure_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_savefigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%save the current plot
%prompt for filename
[filename, pathname] = uiputfile({'*.jpg'; 'Jpeg Files (*.jpg)'},'Save Figure');

%create a new figure and replot it so that we don't save all of our
%interface
h_f=figure;
h_a=axes;
updateData(handles,h_a);
saveas(h_f,[pathname filename]);
close(h_f);



function txt_ymin_Callback(hObject, eventdata, handles)
% hObject    handle to txt_ymax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_ymax as text
%        str2double(get(hObject,'String')) returns contents of txt_ymax as a double
updateData(handles);

function txt_ymax_Callback(hObject, eventdata, handles)
% hObject    handle to txt_ymax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_ymax as text
%        str2double(get(hObject,'String')) returns contents of txt_ymax as a double
updateData(handles);


% --- Executes during object creation, after setting all properties.
function txt_ymax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_ymax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end





% --- Executes during object creation, after setting all properties.
function txt_ymin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_ymin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end




% --- Executes on button press in cmd_addsig.
function cmd_addsig_Callback(hObject, eventdata, handles)
% hObject    handle to cmd_addsig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%add the current signal being plotted to the drop down box
%get index of currently plotted signal
index=handles.param.index;

%create a copy of the signal for just the selected window
%make sure signal is valid
if (index>0)
tw=handles.data.timeWindow{index};
s=Sig('sig',handles.data.signals{index},'tw',handles.data.timeWindow{index});

%prompt for the label
newlabel = inputdlg('Add Signal', 'Label for new signal?');
s=set(s,'label',newlabel);
handles=addsignal(handles,s,tw);
guidata(hObject, handles);
end


% --------------------------------------------------------------------
function mnu_cp_seg_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_cp_seg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%make sure we've loaded and plotted results
if (handles.param.index > -1)
    s=handles.data.signals{handles.param.index};
    newsig=Sig('Sig',s,'tw',handles.data.timeWindow{handles.param.index});
    
    %get the name of the variable to store it in
    varname = inputdlg('Name of variable to store data in','Save to Workspace');
    
    assignin('base',varname{1},newsig);
    
end





% --------------------------------------------------------------------
function mnu_save_ws_Callback(hObject, eventdata, handles)
% hObject    handle to mnu_save_ws (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%remember the current directory
%ldir=cd;
currdir=pwd;
if (exist(handles.param.startdir,'dir'))
    cd(handles.param.startdir);
end
[filename, pathname] = uiputfile({'*.mat', 'Matlab DataGui File (*.mat);'});

cd(currdir);

%if user hit cancel on the dialog box then exit 
if isequal(filename,0) | isequal(pathname,0)
      return;
end

handles.param.startdir=pathname;

handles.data.filename=filename;

data=[];
data.signals=handles.data.signals;
data.timeWindow=handles.data.timeWindow;

param=[];
param.index=handles.param.index;

save([pathname filename], 'data','param');


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background, change
%       'usewhitebg' to 0 to use default.  See ISPC and COMPUTER.
usewhitebg = 1;
if usewhitebg
    set(hObject,'BackgroundColor',[.9 .9 .9]);
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on slider movement.
function sld_tstart_Callback(hObject, eventdata, handles)
% hObject    handle to sld_tstart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%if there is a signal selected
%slider bar is a fraction between 0 & 1 which we conver to a tstart
%take the floor because slider value doesn't need to be integer
%but frame index does

tfrac=  get(handles.sld_tstart,'Value');
tmax=get(handles.sld_tstart,'Max');

if (handles.param.index>0)

    tend=get(handles.data.signals{handles.param.index},'tend');
    tstart=get(handles.data.signals{handles.param.index},'tstart');
    twidth=tend-tstart;
    tstart=twidth*tfrac+tstart;
   
    %set the slider increment based on twidth
    tw=get(handles.data.timeWindow{handles.param.index},'width');
    set(handles.sld_tstart,'SliderStep',[tw/(2*twidth) 2*tw/(2*twidth)]);
    
    %set the value of the tstart box
    set(handles.txt_tstart,'String',tstart);
   
    %alter tstart of the time window
    handles.data.timeWindow{handles.param.index}=set(handles.data.timeWindow{handles.param.index},'tstart',tstart);
        
    updateData(handles);                  
    guidata(handles.datagui_fig,handles) % Save the handles structure    
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over sld_tstart.
function sld_tstart_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to sld_tstart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

tstart=get(handles.sld_tstart,'value');
tmax=get(handles.sld_tstart,'max');
if (tstart>tmax);
    set(handles.sld_tstart,'value',tmax);
end
    


% --- Executes on key press over sld_tstart with no controls selected.
function sld_tstart_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to sld_tstart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on mouse press over axes background.
function maxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to maxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

 %rbbox returns coordinates relative to bottom left corner of the figure
    %therfore set the figure to use units of pixels
    set(gcf,'units','pixels')
    set(gca,'units','pixels')   %b\c input coordinates will be pixels when drawing boxes
    %get bottom left corner of the axes of the image 
    getfigpos=get(gca,'position');
    
    
    
    %draw a rectangle to select one of the regions in the image
    get(gcf,'units');
    finalRect = rbbox;                   % return figure unit
    
    %finalrect is [x y width height] x,y bottom left corner of the box relative to the figure
    %therefore we need to subtract relative coordinates of the bottom left corner of the axes
    finalRect(1:2)=finalRect(1:2)-getfigpos(1:2)
    
    %this gives us coordinates of box with 0,0 being left bottom corner of image.
  

    %Scaling
    %the width of the selection box is reported in pixels. But our actual may scale the image when displaying it.
    
    xconversion=diff(get(handles.maxes,'xlim'))/getfigpos(3);
    yconversion=diff(get(handles.maxes,'ylim'))/getfigpos(4);


    %also need to scale initial position
    finalRect(1)=xconversion* finalRect(1);
    finalRect(2)=yconversion* finalRect(2);
    
    %scale the heights and widths, and take floor
    finalRect(3)=   xconversion* finalRect(3);
    finalRect(4)=   yconversion* finalRect(4);
    
    
    
    tw=handles.data.timeWindow{handles.param.index};
    %we need to add back in the values of the lower left corner of the
    %axes which may not be the origin
    xlim=get(gca,'xlim');
    finalRect(1)=finalRect(1)+xlim(1);
    ylim=get(gca,'ylim');
    finalRect(2)=finalRect(2)+ylim(1);
    
    
    %finalrect is [x y width height] x,y top left corner of the box relative to the figure
    %set the timewindow
    tw=set(tw,'tstart',finalRect(1));
    tw=set(tw,'tend',finalRect(1)+finalRect(3));
    handles.data.timeWindow{handles.param.index}=tw;
    
    set(handles.txt_ymax,'string',finalRect(2)+finalRect(4));
    set(handles.txt_ymin,'string',finalRect(2));
    
    updateData(handles);
    %draw a box around the selected region
    %hold on
    %x = [finalRect(1) finalRect(1)+finalRect(3) finalRect(1)+finalRect(3) finalRect(1) finalRect(1)];
    %y = [finalRect(2) finalRect(2) finalRect(2)+finalRect(4) finalRect(2)+finalRect(4) finalRect(2)];

    
    %plot and save the handle to this box
    %newbox=plot(x,y,'r','linewidth',2);          % draw box around selected region
    
    



