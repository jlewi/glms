%timeWindow constructor
function timeWindow = TimeWindow (varargin)
%Class to represent the window in time of the data we are looking at
%start- start time of window (seconds)
%stop - end time of window (seconds)
%width - width of time window we are looking at
%Whenever we set one of the above variables we change the others to be
%consistent

timeWindow=struct('start',0,'stop',60,'width',60);

%we need to keep track of which values he sets to make sure they are
%consistent
%use a bit field
vset=0;
for index=1:2:nargin
    if strcmpi(varargin(index),'start')
        timeWindow.start=varargin{index+1};
        vset=bitor(vset,1);
    end
    if strcmpi(varargin(index),'stop')
        timeWindow.samplerate=varargin{index+1};
        vset=bitor(vset,2);
    end
    if strcmpi(varargin(index),'width')
        timeWindow.width=varargin{index+1};
        vset=bitor(vset,3);
    end
end

%if start and stop were set then set width
if (vset==3)
    timeWindow.width=timeWindow.stop-timeWindow.start;
end
if (vset==5)
    timeWindow.stop=timeWindow.start+timeWindow.width;
end
if (vset==6)
    timeWindow.start=timeWindow.stop-timeWindow.width;
end

timeWindow = class(timeWindow,'TimeWindow');
    
    