%   get(node,property_name,property_value)
%   Possible property names
%   Note all values are returned by value (this is the way matlab does it)
function n = set(n,varargin)
    % SET Set node properties and return the updated object
    property_argin = varargin;
    while length(property_argin) >= 2,
        prop = property_argin{1};
        val = property_argin{2};
        property_argin = property_argin(3:end);
        switch prop
        case {'start','tstart'}
            n.start =val;
            if (val>n.stop)                
                n.stop=n.start+n.width;
            else
                n.width=n.stop-n.start;
            end
       case {'stop','tstop','tend'}
           n.stop=val; 
           if (n.stop < n.start)
                n.start=n.stop-n.width;
           else 
                n.width=n.stop-n.start;
           end
        case 'width'
            n.width=val;
            n.stop=n.start+n.width;
        otherwise
        error('timewindow properties:...')
    end
end