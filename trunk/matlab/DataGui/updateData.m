% function handles=updateData(handles)
%   handles - handles to objects
%   h_axes - optional handle to axes where we want to plot
%       -this is used just if you want to plot a separate copy of the
%       figure for saving
%   Explanation: Updates the entries of the gui based on the current
%   selection of the listbox.
function handles=updateData(handles,h_axes)

%make sure there are signals loaded
if (handles.data.numsignals <1)
    return;
end

if (~(exist('h_axes','var')))
    h_axes=handles.maxes;
end



%get the index of the currently selected item
index=get(handles.cbo_entitylist,'value');
%change the index of the currently plotted signal
handles.param.index=index;

s=handles.data.signals{index};
%plot the data
 axes(h_axes);
 %t=linspace(1,1/get(s,'samplerate')*get(s,'subsample'),get(s,'length'));
 [data,t]=getData(s,handles.data.timeWindow{index});
 
 %its possible there is no data because of the way my slider bar works
 %its possible to slide past the end of the data a little bit
 if (~isempty(data))
    h_p=plot(t,data);
    xlabel('Time (Seconds)');
    xlim([t(1) t(end)]);
    
    set(handles.txt_tstart,'string',t(1));
   set(handles.txt_tstop,'string',t(end));
 end
 
 ylabel(sprintf('%s %s',get(s,'label'),get(s,'units')));
%if we've already plotted this data
%then use the ylimits in the textboxes
if (handles.param.notplotted==0)
    ylim([str2num(get(handles.txt_ymin,'String')) str2num(get(handles.txt_ymax,'String'))]);
else
    yl=get(h_axes,'ylim');
    set(handles.txt_ymin,'String',num2str(yl(1)));
    set(handles.txt_ymax,'String',num2str(yl(2)));
    set(h_axes,'YLimMode','auto');
    
end
 

 %display info about the analog data
 %einfo=sprintf('Type: Analog Event  \n');
 einfo=sprintf('Tmax \t %4.2f %s\n',get(s,'length')/get(s,'samplerate'),get(s,'tunits'));
 einfo=sprintf('%sSample Rate \t %d cycles/%s\n',einfo,get(s,'samplerate'),get(s,'tunits'));
 %einfo=sprintf('%s MinVal \t %d \n',einfo,nsAnalogInfo.MinVal);
 %einfo=sprintf('%s MaxVal \t %d \n',einfo,nsAnalogInfo.MaxVal);
 %einfo=sprintf('%s Units \t %s \n',einfo,nsAnalogInfo.Units);
 set(handles.txt_info,'String',einfo);
 
 handles.param.notplotted=0;
  %we need to reset the handle for the buttown down fcn of 
    %maxes so that we can zoom in on it
    set(handles.maxes,'ButtonDownFcn','datagui(''maxes_ButtonDownFcn'',gcbo,[],guidata(gcbo))');
    
    %also set the button down function for the actual plot so if we click
    %on it we also get the box
    %we need to reset the handle for the buttown down fcn of 
    %maxes so that we can zoom in on it
    if (exist('h_p','var'))
    set(h_p,'ButtonDownFcn','datagui(''maxes_ButtonDownFcn'',gcbo,[],guidata(gcbo))');
    end
    