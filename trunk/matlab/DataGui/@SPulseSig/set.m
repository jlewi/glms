%function n=set(n,varargin)
%
%   set(obj,property_name,property_value,...)
function n = set(n,varargin)
    % SET Set node properties and return the updated object
    property_argin = varargin;
    while length(property_argin) >= 2,
        prop = property_argin{1};
        val = property_argin{2};
        property_argin = property_argin(3:end);
        switch prop
        otherwise
              %default test if the name is the name of a field
              %if so adjust it
              if (isfield(n,prop))
                  n=setfield(n,prop,val);
              else
                 %call the parent 
                 n.sig=set(n.sig,prop,val);
              end   
              error('Node properties: p, motion')
    end
end