%Sig.get   Return value of a field
%
%   get(node,property_name)
%   Possible property names
%      
%   Note all values are returned by value (this is the way matlab does it)
function [val] = get(obj,prop_name)
switch prop_name
    case 'ptimes'
        val=obj.ptimes;
    case 'dclevel'
        val=obj.dclevel;
    case 'pamp'
        val=obj.pamp;
    otherwise
        %default call parent
                 val=get(obj.Sig,prop_name);
     end   
              
end