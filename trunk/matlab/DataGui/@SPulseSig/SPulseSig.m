%function s=SPulseSig(varargin)
% SPulseSig 
%   -represents a signal of square pulsees
%
% Constructors
%   sp = sig('sig',s,'threshold', thresh)
%       s      - signal in which we want to detect pulses 
%       thresh - threshold of the signal
%
%   optional constructor parameters 
%       specified as (paramaname,paramvalue)
%       'aunits' - string to label units of amplitude
%       'tunits' - string to label units of time
%
%   member variables
%		ptimes - nx2 array storing times of pulses
%              - these are stored as times
%		pamp   - nx1 array specifying amplitudes of pulses
%		aunits - string specifying units of pulse
%       tunits - string specifying units of time
%       dclevel - value for signal in between pulses
function ss = SPulseSig (varargin)

%square pulse signal structure
ss=struct('ptimes',[],'pamp',[],'aunits','','tunits','','dclevel',0);



%to store sigobject from which we create pulse signal
%and the threshold
sigobj=[];
thresh=0.0001;

for index=1:2:nargin    
    if strcmpi(varargin(index),'sig')
        sigobj=varargin{index+1};
        ss.aunits=get(sigobj,'units');
    end
    
    
    if strcmpi(varargin(index),'threshold')
        %we want to copy another signal
       thresh=varargin{index+1};       
    end
    
    if strcmpi(varargin(index),'aunits')
        s.aunits=varargin{index+1};
    end
    if strcmpi(varargin(index),'tunits')
        s.tunits=varargin{index+1};
    end
end

%get the pulse times
[ss.ptimes ss.pamp]=getPulseTimes(get(sigobj,'data'),thresh);

%convert indexes to times
ss.ptimes=ss.ptimes/get(sig,'samplerate')+get(sig,'starttime');

%since the signal object will be the parent object
%zero its data so we don't waste space
sigobj=set(sigobj,'data',[]);
ss = class(ss,'SPulseSig',sigobj);
        
