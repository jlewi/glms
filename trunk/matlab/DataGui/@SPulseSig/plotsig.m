%function plotsig(sig, h_axes,gparam)
%   h_axes - handle to axes where to plot
%            if blank creates new accessess
%   gparam - optional graph parameters
%            ptype - plot types can be array
%                    i.e if you want to plot signal as line and as markers
function plotsig(s, h_axes,gparam)
    
    %set defaults
    if (~(exist('h_axes','var')))
        figure;
        h=axes;
        hold on;
    end
    
    if (~(exist('gparam','var')))
        gparam=[];
    end
    
    if (~(isfield(gparam,'ptype')))
        gparam.ptype={'b-'};
    end
    
    %make ptype a cell array if its not one
    if (~(iscell(gparam.ptype)))
        gparam.ptype={gparam.ptype};
    end
    
    %to plot the square pulses we need to set 
    %the amplitude to the dclevel in between pulses
    ptimes=get(s,'ptimes');
    pamp=get(s,'pamp');
    dclevel=get(s,'dclevel');
    
    pltimes=zeros(size(ptimes,1)*2-1,2);
    plamp=zeros(size(ptimes,1)*2-1,1);
    for index=1:2:size(pltimes,1)-1;
        pltimes(index,1:2)=ptimes((index-1)/2+1,:);
        plamp(index)=pamp((index-1)/2+1);
        
        pltimes(index+1,:)=[ptimes((index-1)/2+1,2) ptimes((index+1)/2+1,1)];
        plamp(index+1)=dclevel;
    end
    %final pulse
    index=size(pltimes,1);
	if (index>0)
    pltimes(index,1:2)=ptimes((index-1)/2+1,:);
    plamp(index)=pamp((index-1)/2+1);
    end

    %plot the signal using all given types of plots
    hold on
    for pindex=1:size(gparam.ptype)
        for index=1:size(pltimes,1) 
        plot(pltimes(index,:),[plamp(index) plamp(index)],gparam.ptype{pindex});
        end
    end