%function [data,t]=getData(s,tw)
%       -s -signal object
%       -tw - timeWindow object describing what time to get
%
% Return value:
%   data - data
%   t - time
function [data,t]=getData(s,tw)

%get the indexes
%add 1 because t=0 is index 1
samplerate=get(s.Sig,'samplerate');
istart=floor(get(tw,'start')*samplerate+1);
if (istart<1)
    istart=1;
end

iend=ceil(get(tw,'stop')*samplerate+1);

if (iend>length(s.data));
    iend=length(s.data);
end

data=s.data(istart:iend);
t=[istart:iend]*1/get(s,'samplerate');

