%Sig.get   Return value of a field
%
%   get(node,property_name)
%   Possible property names
%      
%   Note all values are returned by value (this is the way matlab does it)
function [val] = get(obj,prop_name)
% GET Get asset properties from the specified object
% and return the value
switch prop_name
case 'subsample'
    val = obj.subsample;
case 'data'
    val=obj.data(1:obj.subsample:end);
case 'label'
    val = obj.label;
case 'starttime'
    val=obj.starttime;
case 'length'
    val=length(obj.data);
case 'samplerate'
    val=obj.samplerate;
case 'tstart'
    val=obj.starttime;
case 'tend'
    val=length(obj.data)*1/obj.samplerate;
case 'units'
        val=obj.units;
    case 'tunits'
        val=obj.tunits;
        
    otherwise
      %default test if the name is the name of a field
              %if so adjust it
              try
                  val=get(obj,prop);
              
              catch
                 error([prop_name,' Is not a valid Sig property'])
              end   
    
end