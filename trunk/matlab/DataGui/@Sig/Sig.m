function sig = Sig (varargin)
%Sig class constructor. 
%   -class to represent a generic time signal
%
% Constructors
% sig = sig('sig',s)
%       - copy the signal
% sig = sig('sig',s,'tw',tw);
%       - copy the signal but only take the data specified
%         order matters
% sig = Sig('data',data, 'paramname',val,...)
%      data - 1xn data
%      paramname - name of parameter
%      val - value of parameter
%
%   
%	member variables
%		data - 1xn array containing the data
%		units - string representing the units of the signal
%		samplerate - rate in hz at which signal was sampled
%		starttime - absolute time at which signal starts
%		subsample - indicates value to subsample the data at. The Sig object stores the complete
%					data. Some functions, however, will return subsampled versions.
%


sig=struct('data',[],'units','','samplerate',1,'label','','starttime',0,'subsample',1,'tunits','sec');
    %subsample - rate at which to subsample data when returning it
    

for index=1:2:nargin
    
    if strcmpi(varargin(index),'data')
        sig.data=varargin{index+1};
    end
    
    %are we copying another signal
    if strcmpi(varargin(index),'sig')
        %we want to copy another signal
       s=varargin{index+1};
       
       tw=[];
       for tindex=index+2:nargin
        %is there a time window provided
        if (strcmpi(varargin(tindex),'tw'))
            tw=varargin{tindex+1};
        end
       end
       
       %if a time window is provided we only represent that segment of data
       if isempty(tw)
        sig.data=get(s,'data');
        sig.starttime=get(s,'starttime');
       else
           [sig.data,t]=getData(s,tw);
           sig.starttime=t(1);
       end
       sig.units=get(s,'units');
       sig.samplerate=get(s,'samplerate');
       sig.label=get(s,'label');
       sig.subsample=get(s,'subsample');
       sig.samplerate=get(s,'samplerate');
              
    end
    
    if strcmpi(varargin(index),'units')
        sig.units=varargin{index+1};
    end
    if strcmpi(varargin(index),'samplerate')
        sig.samplerate=varargin{index+1};
    end
    if strcmpi(varargin(index),'label')
        sig.label=varargin{index+1};
    end
    
    if strcmpi(varargin(index),'subsample')
        sig.subsample=varargin{index+1};
    end
    
    if strcmpi(varargin(index),'starttime')
        sig.starttime=varargin{index+1};
    end
    
end

sig = class(sig,'Sig');
        
