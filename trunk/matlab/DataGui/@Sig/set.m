%Node.set   set value of a field
%
%   get(node,property_name,property_value)
%   Possible property names
%   Note all values are returned by value (this is the way matlab does it)
function n = set(n,varargin)
    % SET Set node properties and return the updated object
    property_argin = varargin;
    while length(property_argin) >= 2,
        prop = property_argin{1};
        val = property_argin{2};
        property_argin = property_argin(3:end);
        switch prop
        case 'label'
            n.label = val;
        case 'units'
            n.units=val;
        case 'subsample'
            n.subsample=val;
            case 'data'
                n.data=val;
            case 'tunits'
                n.tunits=val;
            otherwise
                 error('invalid property')
              end   
              
        
    end
end