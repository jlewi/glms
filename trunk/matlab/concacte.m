%this file determines the comparative amount of time to concatenate a row to a matrix using 3 diffferent
% methods
% 1. the cat command
% 2. [oldmatrix; new row]
% 3. [new row; old matrix]


NUMLOOPS=3000
row=1:100
matrix=[]

startime=cputime
for i=1: NUMLOOPS
    matrix=cat(1,matrix,row);
end
time=cputime -startime


matrix=[];
startime=cputime
for i=1: NUMLOOPS
    matrix=[matrix,row];
end
time=cputime -startime


matrix=[];
startime=cputime
for i=1: NUMLOOPS
    matrix=[row, matrix];
end
time=cputime -startime

%try saving it to a file
file='/home/jl1403/tmp/testtimes.mat'
matrix=[];
save(file,'matrix');
startime=cputime
for i=1:NUMLOOPS
    matrix=[row,matrix];
    if (mod(i,10)==9)
        %load from file concatenate and save + clear
        old_matrix=load(file);
        old_matrix=old_matrix.matrix;
        matrix=[old_matrix,matrix];
        save(file,'matrix')
        matrix=[];
    end
end
old_matrix=load(file);
old_matrix=old_matrix.matrix;
matrix=[old_matrix,matrix];
time=cputime-startime
    