%function [v]=callfunc(func,arg)
%       func - pointer to function
%       arg  - cell array of arguments to pass
%
% Explanation: calls the function
%   constructs an argument list from arg
%
function v=callfunc(func,arg)
    cmdstr=strcat('v=func(');
    
    for index=1:length(arg)
        if index>1
            cmdstr=strcat(cmdstr,',');
        end
        cmdstr=strcat(cmdstr,'arg{',num2str(index),'}');
    end
    cmdstr=strcat(cmdstr,');');
    
    eval(cmdstr);
    