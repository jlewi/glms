%function sind=subind(msize,lind)
%   msize - the dimensions of the matrix
%   lind - the linear indexes to get the multiple subscripts for
%
%sind - 1 row for each lind
%
%Explanation: Gets the multiple subscripts by callind ind2sub.
%   All this funciton is doing is calling ind2sub.
%   it automatically creates an approriate command string so that
%   the multiple subscripts for each linear index correspond to different
%   columns of sind.
function sind=subind(msize,lind)

ndim=size(msize,2);
sind=zeros(length(lind),ndim);

%form the output string to store the results in
cmd='[';
for mind=1:ndim
    cmd=sprintf('%ssind(:,%d)',cmd,mind);
    if (mind<ndim)
        cmd=sprintf('%s,',cmd);
    end
end
cmd=sprintf('%s]=ind2sub(msize,lind);',cmd);
eval(cmd);

