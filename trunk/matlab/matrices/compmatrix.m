% function c=compmatrix(m)
%   m - matrix with dimension dims
%
% Return value:
%   c - 2d matrix with colum j
%       c(:,j)=m(:,:,:,:,...,j)(:)
%
%Explanation:
%   useful for turning output of matrixindexes into a 2d matrix
%   where each row gives indexes of a different point
%   This is very ineeficent/and slow way to do it;
%   Would be better to calculate what indexes to set directly
function c = compmatrix(m)
    dims=size(m);

    c=zeros(prod(dims(1:end-1)),dims(end));
    
    for dindex=1:dims(end)
     
        %we construct a string to represent the dimensions over which we
        %want to look at all k
        %so if its 2x2 matrix
        %we are going to construct x=[1 2; 1 2], y=[1 1; 2 2]
        %To construct this type of matrix I use eval and construct the
        %command as a string because I want to take all elements from all
        %but one dimension so I want to use the x(:) notation but I want
        %the number of dimensions to be a variable
        cmd=strcat('col=m(',strrepeat(':,',length(dims)-1),num2str(dindex),');');
        eval(cmd);
        c(:,dindex)=col(:);
%        cmd=strcat('c(:,',num2str(dindex),')=m(',strrepeat(':,',length(dims)-1),num2str(dindex),');');
 %       eval(cmd);
    end
    
 end
